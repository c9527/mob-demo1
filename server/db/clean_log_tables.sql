-- MySQL dump 10.13  Distrib 5.5.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: log_android_4399_s999
-- ------------------------------------------------------
-- Server version	5.5.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbllog_activity`
--

DROP TABLE IF EXISTS `tbllog_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_activity` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内参与、完成功能时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_battle`
--

DROP TABLE IF EXISTS `tbllog_battle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_battle` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `battle_id` int(11) DEFAULT NULL,
  `time_duration` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏参与战场打斗时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_buff`
--

DROP TABLE IF EXISTS `tbllog_buff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_buff` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `channel_id` int(11) DEFAULT '0',
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `buff` int(11) DEFAULT NULL,
  `used_num` int(11) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_chat`
--

DROP TABLE IF EXISTS `tbllog_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_chat` (
  `platform` varchar(50) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `channel` int(11) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  `type` tinyint(3) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色创建日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_checkin`
--

DROP TABLE IF EXISTS `tbllog_checkin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_checkin` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内每日签到时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_complaints`
--

DROP TABLE IF EXISTS `tbllog_complaints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_complaints` (
  `complaint_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `role_name` varchar(40) NOT NULL,
  `account_name` varchar(40) NOT NULL,
  `device` varchar(255) DEFAULT NULL,
  `complaint_time` int(11) NOT NULL,
  `complaint_type` tinyint(4) NOT NULL,
  `complaint_title` varchar(40) NOT NULL,
  `complaint_content` text NOT NULL,
  `platform` varchar(40) NOT NULL,
  `log_time` int(11) DEFAULT '0',
  PRIMARY KEY (`complaint_id`),
  KEY `index_role_id` (`role_id`),
  KEY `index_role_name` (`role_name`),
  KEY `index_account_name` (`account_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_error`
--

DROP TABLE IF EXISTS `tbllog_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_error` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `error_msg` mediumtext,
  `did` varchar(255) DEFAULT NULL,
  `game_version` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `screen` varchar(255) DEFAULT NULL,
  `mno` varchar(255) DEFAULT NULL,
  `nm` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登陆日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_event`
--

DROP TABLE IF EXISTS `tbllog_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_event` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `did` varchar(255) DEFAULT NULL,
  `game_version` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `screen` varchar(255) DEFAULT NULL,
  `mno` varchar(255) DEFAULT NULL,
  `nm` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_family`
--

DROP TABLE IF EXISTS `tbllog_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_family` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `invite_player_cnt` int(8) DEFAULT '0' COMMENT '发起邀请人数',
  `invite_cnt` int(8) DEFAULT '0' COMMENT '发起邀请次数',
  `accept_cnt` int(8) DEFAULT '0' COMMENT '接受邀请次数',
  `member_cnt` int(8) DEFAULT '0' COMMENT '加入家族次数',
  `buy_pos_player_cnt` int(8) DEFAULT '0' COMMENT '购买位置人数',
  `buy_pos_cnt` int(8) DEFAULT '0' COMMENT '购买位置次数',
  `happend_time` int(11) DEFAULT '0' COMMENT '发生时间',
  `log_time` int(11) DEFAULT '0' COMMENT '写日志时间',
  PRIMARY KEY (`id`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='家族系统统计日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_fb`
--

DROP TABLE IF EXISTS `tbllog_fb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_fb` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `fb_id` int(11) DEFAULT NULL,
  `map_id` int(11) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `fb_level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `win_camp` tinyint(3) DEFAULT '0',
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内参与、完成副本时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_function`
--

DROP TABLE IF EXISTS `tbllog_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_function` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内参与、完成功能时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_game_time`
--

DROP TABLE IF EXISTS `tbllog_game_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_game_time` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_time` int(11) DEFAULT '0' COMMENT '战斗时间（秒）',
  `player_cnt` int(8) DEFAULT '0' COMMENT '战斗玩家数',
  `happend_time` int(11) DEFAULT '0' COMMENT '发生时间',
  `log_time` int(11) DEFAULT '0' COMMENT '写日志时间',
  PRIMARY KEY (`id`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='战斗时间统计日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_gold`
--

DROP TABLE IF EXISTS `tbllog_gold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_gold` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `dim_prof` int(11) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `money_remain` int(11) DEFAULT NULL,
  `opt` int(11) DEFAULT NULL,
  `action_1` int(11) DEFAULT NULL,
  `action_2` int(11) DEFAULT NULL,
  `item_number` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`),
  KEY `index_dim_level` (`dim_level`),
  KEY `index_dim_prof` (`dim_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内获得/消耗货币时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_items`
--

DROP TABLE IF EXISTS `tbllog_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_items` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `opt` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_number` int(11) DEFAULT NULL,
  `item_time` int(11) DEFAULT NULL,
  `map_id` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`),
  KEY `index_dim_level` (`dim_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内获得/消耗道具时的日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_level_up`
--

DROP TABLE IF EXISTS `tbllog_level_up`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_level_up` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `last_level` int(11) DEFAULT NULL,
  `current_level` int(11) DEFAULT NULL,
  `last_exp` int(11) DEFAULT NULL,
  `current_exp` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='等级变动日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_login`
--

DROP TABLE IF EXISTS `tbllog_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_login` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `login_map_id` int(11) DEFAULT NULL,
  `did` varchar(255) DEFAULT NULL,
  `game_version` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `screen` varchar(255) DEFAULT NULL,
  `mno` varchar(255) DEFAULT NULL,
  `nm` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`),
  KEY `index_dim_level` (`dim_level`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登陆日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_map_online`
--

DROP TABLE IF EXISTS `tbllog_map_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_map_online` (
  `platform` varchar(50) DEFAULT NULL,
  `map_id` int(11) DEFAULT NULL,
  `player_num` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_match`
--

DROP TABLE IF EXISTS `tbllog_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_match` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `match_mode` tinyint(3) DEFAULT '0' COMMENT '排位类型',
  `score_id` int(8) DEFAULT '0' COMMENT '排位段位id',
  `player_cnt` int(11) DEFAULT '0' COMMENT '该段位参与玩家数',
  `join_cnt` int(11) DEFAULT '0' COMMENT '该段位参与次数',
  `online_cnt` int(11) DEFAULT '0' COMMENT '该段位在线玩家数',
  `new_cnt` int(8) DEFAULT '0' COMMENT '该段位新增人数',
  `happend_time` int(11) DEFAULT '0' COMMENT '发生时间',
  `log_time` int(11) DEFAULT '0' COMMENT '写日志时间',
  PRIMARY KEY (`id`),
  KEY `index_match_mode` (`match_mode`),
  KEY `index_score_id` (`score_id`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='排位统计日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_match_team`
--

DROP TABLE IF EXISTS `tbllog_match_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_match_team` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `match_mode` tinyint(3) DEFAULT '0' COMMENT '排位类型',
  `score_id` int(8) DEFAULT '0' COMMENT '排位段位id',
  `team_member_cnt` int(11) DEFAULT '0' COMMENT '组队人数',
  `player_cnt` int(11) DEFAULT '0' COMMENT '该段位组队参与玩家数',
  `join_cnt` int(11) DEFAULT '0' COMMENT '该段位组队参与次数',
  `win_cnt` int(11) DEFAULT '0' COMMENT '该段位组队胜利次数',
  `happend_time` int(11) DEFAULT '0' COMMENT '发生时间',
  `log_time` int(11) DEFAULT '0' COMMENT '写日志时间',
  PRIMARY KEY (`id`),
  KEY `index_match_mode` (`match_mode`),
  KEY `index_score_id` (`score_id`),
  KEY `index_happend_time` (`happend_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='排位组队统计日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_misc`
--

DROP TABLE IF EXISTS `tbllog_misc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_misc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL DEFAULT '0',
  `platform` varchar(50) DEFAULT NULL,
  `ver` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `n1` int(11) DEFAULT NULL,
  `n2` int(11) DEFAULT NULL,
  `s1` varchar(255) DEFAULT NULL,
  `s2` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `ver` (`ver`),
  KEY `device` (`device`),
  KEY `type` (`type`),
  KEY `n1` (`n1`),
  KEY `n2` (`n2`),
  KEY `s1` (`s1`),
  KEY `s2` (`s2`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_money_remain`
--

DROP TABLE IF EXISTS `tbllog_money_remain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_money_remain` (
  `money_type` int(11) DEFAULT NULL,
  `money_remain` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内剩余的货币总额，数据每天记录一次';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_online`
--

DROP TABLE IF EXISTS `tbllog_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_online` (
  `platform` varchar(50) DEFAULT NULL,
  `people` int(11) DEFAULT NULL,
  `device_cnt` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_pay`
--

DROP TABLE IF EXISTS `tbllog_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_pay` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `pay_type` int(11) DEFAULT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `pay_money` float DEFAULT NULL,
  `pay_gold` int(11) DEFAULT NULL,
  `associator_type` int(11) DEFAULT NULL,
  `did` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `game_version` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`),
  KEY `index_dim_level` (`dim_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_player`
--

DROP TABLE IF EXISTS `tbllog_player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_player` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL DEFAULT '0',
  `role_name` varchar(255) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL COMMENT '平台唯一用户标志(UID)',
  `user_name` varchar(255) DEFAULT NULL COMMENT '玩家帐号名称',
  `dim_nation` varchar(255) DEFAULT NULL,
  `dim_prof` int(11) DEFAULT NULL,
  `dim_sex` int(11) DEFAULT NULL COMMENT '1-男，2-女，0-未知',
  `reg_time` int(11) DEFAULT NULL,
  `reg_ip` varchar(255) DEFAULT NULL,
  `did` varchar(255) DEFAULT NULL COMMENT '用户设备ID',
  `device` varchar(255) DEFAULT NULL COMMENT '用户设备',
  `dim_level` int(11) DEFAULT NULL,
  `dim_vip_level` int(11) DEFAULT NULL,
  `dim_exp` int(11) DEFAULT NULL,
  `dim_guild` varchar(255) DEFAULT NULL COMMENT '帮派名称',
  `dim_power` int(11) DEFAULT NULL,
  `gold_number` int(11) DEFAULT NULL,
  `bgold_number` int(11) DEFAULT NULL,
  `coin_number` int(11) DEFAULT NULL,
  `bcoin_number` int(11) DEFAULT NULL,
  `pay_money` int(11) DEFAULT NULL,
  `first_pay_time` int(11) DEFAULT NULL,
  `last_pay_time` int(11) DEFAULT NULL,
  `last_login_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `index_role_name` (`role_name`),
  KEY `index_account_name` (`account_name`),
  KEY `index_user_name` (`user_name`),
  KEY `index_dim_nation` (`dim_nation`),
  KEY `index_dim_prof` (`dim_prof`),
  KEY `index_dim_sex` (`dim_sex`),
  KEY `index_dim_level` (`dim_level`),
  KEY `index_dim_vip_level` (`dim_vip_level`),
  KEY `index_dim_exp` (`dim_exp`),
  KEY `index_dim_guild` (`dim_guild`),
  KEY `index_dim_power` (`dim_power`),
  KEY `index_reg_time` (`reg_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_pvp`
--

DROP TABLE IF EXISTS `tbllog_pvp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_pvp` (
  `platform` varchar(50) DEFAULT NULL,
  `pvp_id` int(11) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内参与PVP时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_quit`
--

DROP TABLE IF EXISTS `tbllog_quit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_quit` (
  `platform` varchar(50) DEFAULT NULL COMMENT '所属平台',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `account_name` varchar(50) DEFAULT NULL COMMENT '平台唯一用户标识',
  `login_level` int(11) DEFAULT NULL COMMENT '登录等级',
  `logout_level` int(11) DEFAULT NULL COMMENT '登出等级',
  `logout_ip` varchar(255) DEFAULT NULL COMMENT '登出IP',
  `login_time` int(11) DEFAULT NULL COMMENT 'log_time',
  `logout_time` int(11) DEFAULT NULL COMMENT '退出时间',
  `time_duration` int(11) DEFAULT NULL COMMENT '在线时长',
  `logout_map_id` int(11) DEFAULT NULL COMMENT '退出地图ID',
  `reason_id` int(11) DEFAULT NULL COMMENT '退出异常或者原因，reason 对应字典表(0表示正常退出)',
  `msg` varchar(200) DEFAULT NULL COMMENT '特殊信息',
  `did` varchar(255) DEFAULT NULL COMMENT '用户设备ID',
  `device` varchar(255) DEFAULT NULL COMMENT '用户设备',
  `game_version` varchar(255) DEFAULT NULL COMMENT '游戏版本号',
  `log_time` int(11) NOT NULL DEFAULT '0' COMMENT '写日志时间，索引字段',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志记录玩家在游戏内退出时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_report`
--

DROP TABLE IF EXISTS `tbllog_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '举报单号',
  `platform` varchar(50) NOT NULL COMMENT '平台',
  `report_id` bigint(20) NOT NULL COMMENT '举报者角色ID',
  `report_name` varchar(50) NOT NULL DEFAULT '' COMMENT '举报者角色名称',
  `report_account` varchar(50) NOT NULL COMMENT '举报者平台账号（UID）',
  `be_report_id` bigint(20) NOT NULL COMMENT '被举报者角色ID',
  `be_report_name` varchar(50) NOT NULL DEFAULT '' COMMENT '被举报者角色名称',
  `be_report_account` varchar(50) NOT NULL COMMENT '被举报者平台账号（UID）',
  `add_describe` varchar(300) NOT NULL COMMENT '附加说明',
  `report_type` int(11) NOT NULL COMMENT '违规类型：1.违法违规 2.线下交易 3.发布广告 4.严重侮辱 5.名字违规',
  `proof` varchar(300) NOT NULL COMMENT '违规证据一',
  `proof2` varchar(300) NOT NULL COMMENT '违规证据二',
  `proof3` varchar(300) NOT NULL COMMENT '违规证据三',
  `report_time` int(11) NOT NULL COMMENT '举报时间',
  `log_time` int(11) NOT NULL COMMENT '写日志时间',
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `be_report_id` (`be_report_id`),
  KEY `log_time` (`log_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='玩家举报不良信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_role`
--

DROP TABLE IF EXISTS `tbllog_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_role` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `dim_prof` int(11) DEFAULT NULL,
  `dim_sex` int(11) DEFAULT NULL,
  `did` varchar(255) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `game_version` varchar(255) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色创建日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_round`
--

DROP TABLE IF EXISTS `tbllog_round`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_round` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mode` varchar(50) NOT NULL COMMENT '模式',
  `map` int(11) NOT NULL COMMENT '地图',
  `level` int(11) NOT NULL COMMENT '难度',
  `round` int(11) NOT NULL COMMENT '回合',
  `pass_time` bigint(20) NOT NULL COMMENT '通过时间',
  `happend_time` bigint(20) NOT NULL COMMENT '发生时间',
  `log_time` int(11) NOT NULL COMMENT '写日志时间',
  `fb_id` tinyint(3) NOT NULL DEFAULT '0',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '频道',
  PRIMARY KEY (`id`),
  KEY `mode` (`mode`),
  KEY `map` (`map`),
  KEY `level` (`level`),
  KEY `round` (`round`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_shop`
--

DROP TABLE IF EXISTS `tbllog_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_shop` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `dim_prof` int(11) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `item_type_1` int(11) DEFAULT NULL,
  `item_type_2` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_number` int(11) DEFAULT NULL,
  `item_time` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`),
  KEY `index_role_id` (`role_id`),
  KEY `index_account_name` (`account_name`),
  KEY `index_dim_level` (`dim_level`),
  KEY `index_dim_prof` (`dim_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内商城购买物品时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_skill`
--

DROP TABLE IF EXISTS `tbllog_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_skill` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `used_num` int(11) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内每日签到时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_task`
--

DROP TABLE IF EXISTS `tbllog_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_task` (
  `platform` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  `device` varchar(255) DEFAULT NULL,
  `dim_prof` int(11) DEFAULT NULL,
  `dim_level` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `happend_time` int(11) DEFAULT NULL,
  `log_time` int(11) NOT NULL DEFAULT '0',
  KEY `_idx_log_time` (`log_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='该日志记录玩家在游戏内参与、完成任务时的信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_timelimit_gift`
--

DROP TABLE IF EXISTS `tbllog_timelimit_gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_timelimit_gift` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gift_id` tinyint(3) NOT NULL COMMENT '礼包等级',
  `buy_cnt` int(8) DEFAULT '0' COMMENT '购买数量',
  `push_cnt` int(8) DEFAULT '0' COMMENT '推送数量',
  `cost_diamond` int(11) DEFAULT '0' COMMENT '消耗钻石',
  `happend_time` int(11) NOT NULL COMMENT '发生时间',
  `log_time` int(11) NOT NULL COMMENT '写日志时间',
  `weapon_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbllog_weapon`
--

DROP TABLE IF EXISTS `tbllog_weapon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllog_weapon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `weapon_id` int(11) NOT NULL COMMENT '武器id',
  `name` varchar(255) NOT NULL COMMENT '武器名称',
  `type` varchar(50) NOT NULL COMMENT '武器类型',
  `weapon_class` tinyint(3) DEFAULT '0' COMMENT '武器详细分类',
  `use_players` bigint(20) NOT NULL COMMENT '使用该武器战斗的玩家数（去重）',
  `use_nodated_players` bigint(20) NOT NULL COMMENT '使用该武器（永久类型）战斗的玩家数',
  `use_master_players` bigint(20) NOT NULL COMMENT '使用该武器（师父赠送获得）战斗的玩家数',
  `use_cnt` bigint(20) NOT NULL COMMENT '该武器使用局数（整场算1局）',
  `kill_cnt` bigint(20) NOT NULL COMMENT '该武器击杀数',
  `dead_cnt` bigint(20) NOT NULL COMMENT '使用该武器时的死亡数',
  `channel` tinyint(3) NOT NULL COMMENT '频道',
  `happend_time` int(11) NOT NULL COMMENT '发生时间',
  `log_time` int(11) NOT NULL COMMENT '写日志时间',
  `rule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `type` (`type`),
  KEY `channel` (`channel`),
  KEY `happend_time` (`happend_time`),
  KEY `index_name` (`rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-22 14:03:01
