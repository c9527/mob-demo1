-- MySQL dump 10.13  Distrib 5.5.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: game_android_4399_s999
-- ------------------------------------------------------
-- Server version	5.5.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ban`
--

DROP TABLE IF EXISTS `ban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ban` (
  `player_id` bigint(20) unsigned NOT NULL,
  `acc` varchar(40) NOT NULL,
  `ban_date` int(11) DEFAULT NULL,
  `reason` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  KEY `acc` (`acc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corps_data`
--

DROP TABLE IF EXISTS `corps_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corps_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `corps_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `corps_id` (`corps_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `exchange_code`
--

DROP TABLE IF EXISTS `exchange_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_code` (
  `code` varchar(10) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `server_id` int(11) DEFAULT '0',
  `level_limit` int(11) DEFAULT '0',
  `player_id` bigint(20) NOT NULL DEFAULT '0',
  `begin_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `begin_time_ts` int(11) DEFAULT '0',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time_ts` int(11) DEFAULT '0',
  PRIMARY KEY (`code`,`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family`
--

DROP TABLE IF EXISTS `family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family` (
  `player_id` bigint(20) NOT NULL,
  `data` mediumblob NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `friend`
--

DROP TABLE IF EXISTS `friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend` (
  `player_id` bigint(20) NOT NULL,
  `data` mediumblob NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `global`
--

DROP TABLE IF EXISTS `global`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global` (
  `key` varchar(100) NOT NULL,
  `value` mediumblob NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `legion_data`
--

DROP TABLE IF EXISTS `legion_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legion_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `legion_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `legion_id` (`legion_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loginop`
--

DROP TABLE IF EXISTS `loginop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loginop` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` bigint(20) unsigned NOT NULL,
  `data` mediumblob NOT NULL,
  `raw_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `player_id` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail` (
  `player_id` bigint(20) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `master`
--

DROP TABLE IF EXISTS `master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master` (
  `player_id` bigint(20) NOT NULL,
  `data` mediumblob NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offline_msg`
--

DROP TABLE IF EXISTS `offline_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offline_msg` (
  `id` bigint(20) NOT NULL,
  `typ` int(11) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`,`typ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` bigint(20) NOT NULL,
  `acc` varchar(40) NOT NULL,
  `name` varchar(30) NOT NULL,
  `data` mediumblob NOT NULL,
  `server` int(11) NOT NULL,
  `platform` varchar(40) NOT NULL,
  `plat_uid` varchar(40) NOT NULL,
  `plat_acc` varchar(40) NOT NULL,
  `sex` int(11) NOT NULL,
  `icon` int(11) NOT NULL,
  `diamond` int(11) NOT NULL,
  `coin` int(11) NOT NULL,
  `medal` int(11) NOT NULL,
  `honor` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `power` int(11) NOT NULL,
  `vip_level` int(11) NOT NULL,
  `pay_money` int(11) NOT NULL,
  `first_pay_time` int(11) NOT NULL,
  `last_pay_time` int(11) NOT NULL,
  `last_login_time` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `reg_ip` varchar(40) NOT NULL,
  `stage` int(11) NOT NULL,
  `tag` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `acc` (`acc`),
  KEY `index_name` (`name`),
  KEY `index_platform` (`platform`),
  KEY `index_plat_uid` (`plat_uid`),
  KEY `index_plat_acc` (`plat_acc`),
  KEY `index_diamond` (`diamond`),
  KEY `index_coin` (`coin`),
  KEY `index_medal` (`medal`),
  KEY `index_honor` (`honor`),
  KEY `index_level` (`level`),
  KEY `index_vip_level` (`vip_level`),
  KEY `index_pay_money` (`pay_money`),
  KEY `index_first_pay_time` (`first_pay_time`),
  KEY `index_last_pay_time` (`last_pay_time`),
  KEY `index_last_login_time` (`last_login_time`),
  KEY `index_create_time` (`create_time`),
  KEY `index_reg_ip` (`reg_ip`),
  KEY `index_stage` (`stage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_sim_data`
--

DROP TABLE IF EXISTS `player_sim_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_sim_data` (
  `id` bigint(20) NOT NULL,
  `sim_data` mediumblob NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ranklist`
--

DROP TABLE IF EXISTS `ranklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranklist` (
  `name` varchar(40) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `record`
--

DROP TABLE IF EXISTS `record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record` (
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT '类型',
  `data` mediumblob NOT NULL COMMENT '数据',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zone_info`
--

DROP TABLE IF EXISTS `zone_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone_info` (
  `owner_id` bigint(20) unsigned NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `online_time` varchar(100) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-22 14:02:56
