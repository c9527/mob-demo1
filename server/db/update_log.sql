SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `tbllog_phone` (
`id`  bigint(20) NOT NULL COMMENT '玩家id' ,
`reg_time`  int(11) NULL DEFAULT 0 COMMENT '最后一次登记手机号时间' ,
`reg_number`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '最后一次登记手机号码' ,
`reg_cnt`  int(11) NULL DEFAULT 0 COMMENT '累计登记次数' ,
`happend_time`  int(11) NULL DEFAULT 0 COMMENT '发生时间' ,
`log_time`  int(11) NULL DEFAULT 0 COMMENT '写日志时间' ,
PRIMARY KEY (`id`),
INDEX `index_reg_time` (`reg_time`) USING BTREE ,
INDEX `index_happend_time` (`happend_time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Compact
;
CREATE TABLE `tbllog_phone_activity` (
`id`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
`get_code_cnt`  int(11) NULL DEFAULT 0 COMMENT '获取验证码玩家数' ,
`reg_cnt`  int(11) NULL DEFAULT 0 COMMENT '成功登记手机号玩家数' ,
`happend_time`  int(11) NULL DEFAULT 0 COMMENT '发生时间' ,
`log_time`  int(11) NULL DEFAULT 0 COMMENT '写日志时间' ,
PRIMARY KEY (`id`),
INDEX `index_happend_time` (`happend_time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Compact
;