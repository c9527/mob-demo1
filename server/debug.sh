#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# 脚本所在路径，请放到游戏服根目录
ROOT_DIR=$(cd $(dirname $0); pwd)
# 主配置文件
CFG=${ROOT_DIR}/cfg/system/server.config

if [ ! -e ${CFG} ];then
   echo "未找到配置文件${CFG}，请检查"
   exit 1
fi

# 获取 DbgPort 
PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();local i=tostring(DbgPort):find(':');print(i and DbgPort:sub(i+1) or DbgPort)") 
rlwrap telnet 127.0.0.1 ${PORT}
