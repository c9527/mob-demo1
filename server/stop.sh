#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Filename:stop.sh
# Revision:1.0
# Lastdate:2015/12/8
# Author:liuyong@4399.com,GZ1163
# Description:《终极枪战》从服停服脚本
#--------------------------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

# 脚本所在路径，请放到游戏服根目录
ROOT_DIR=$(cd $(dirname $0); pwd)

# 主配置文件
CFG=${ROOT_DIR}/cfg/system/server.config

#当且仅当从服主目录是主服主目录下的一级目录才准确。
SRV=`echo "$ROOT_DIR" | awk -F'/' '{print $(NF-1)}'`

#core检查脚本，从服不启动
#SCRIPT=/data/sh/mk_coredump2.py

if [ ! -e ${CFG} ];then
   echo "未找到配置文件${CFG}，请检查"
   exit 1
fi

#获取DbgPort 
PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();local i=tostring(DbgPort):find(':');print(i and DbgPort:sub(i+1) or DbgPort)") 

#从服无数据库,注释掉
#GAMEDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(MysqlConf.database)") 
#SERVER=$(echo ${GAMEDB}|sed 's/^game_//')

if [[ -z "${PORT}"  || -z "${SRV}" ]];then
    echo "未找到 DbgPort 或 不是位于从服主目录，请检查!"
    exit 1
fi

# 去除首尾空格
PORT=$(echo ${PORT})

echo "-------------------- stop server -------------------"
# ps aux|grep skynet|grep -v grep|awk '{print $2}'|xargs kill -9
# 关服确认，可以去除
read -p "现在要停止 ${SRV} 服吗?,输入y确认" OPTION
if [ "x${OPTION}" = "xy" ];then
    echo close | ncat 127.0.0.1 ${PORT}
else
    echo "不停服"
    exit 0
fi
ps aux|grep -v grep | grep skynet --col
echo "----------------------------------------------------"

#启动core探测脚本
if [ -f ${SCRIPT} ];then
    cd ${ROOT_DIR}/logs && python ${SCRIPT} ${SRV} stop >/dev/null 2>&1 &
fi
