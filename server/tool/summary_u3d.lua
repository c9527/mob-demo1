local mysql = require "mysql"
require "db"

local CFG = {
    "插件操作",
    {"打开插件页", 1002},
    {"点击下载插件", 1002, 1003},
    {"点击下载插件并成功启动游戏", 1002, 1003, 1007},
    {"下载插件并且创角", 1002, 1003, 1007, 1008},
    {"点击下载微端", 1002, 1001},
    {"点击下载微端并创角", 1002, 1001, 1009},
    
    "微端页操作",
    {"打开微端页", 1000},
    {"点击下载微端", 1000, 1010},
    {"点击下载微端并创角", 1000, 1010, 1009},
    
    "检测已安装插件",
    {"检测已经安装插件", 1006},
    {"启动游戏", 1006, 1007},
    {"已安装插件并创角", 1006, 1007, 1008},
}

local function process_row(file, row, all)
    if type(row)~="table" then
        file:write(row, "\r\n")
        return
    end
    
    local check_typ = {}
    for i = 2, #row do
        local t = row[i]
        check_typ[i-1] = all[t] or {}
    end
    local typ1, typ2, typ3, typ4, typ5 = table.unpack(check_typ)
    local check_fun
    if typ5 then
        function check_fun(uid)
            return typ2[uid] and typ3[uid] and typ4[uid] and typ5[uid]
        end
    elseif typ4 then
        function check_fun(uid)
            return typ2[uid] and typ3[uid] and typ4[uid]
        end
    elseif typ3 then
        function check_fun(uid)
            return typ2[uid] and typ3[uid]
        end
    elseif typ2 then
        function check_fun(uid)
            return typ2[uid]
        end
    else
        function check_fun(uid)
            return true
        end
    end
    local count = 0
    for uid in pairs(typ1) do
        if check_fun(uid) then
            count = count + 1
        end
    end
    file:write(row[1], "（", table.concat(row, "→", 2), "）：", count, "\r\n")
end

function sl(date, pid)
    local history = {}
    local file = io.open("tool/u3d_history.txt", "r")
    if file then
        for line in file:lines() do
            history[line] = true
        end
        file:close()
    end
    
    local db_conn = assert(mysql.connect(GetCfg("system.server").LogDBConf))
    local sql = sqlfmt("SELECT uid, type FROM global_log.client_log_20160406 where time>='%s 00:00:00' and time<'%s 24:00:00' and uid!=''", date, date)
    if pid then
        sql = sql .. " and platform=" .. pid
    end
    local res = db_conn:query( sql )
    db_conn:disconnect()
    
    local file = assert(io.open("tool/u3d_history.txt", "a+"))
    local all = {}
    local all_count = 0
    for _, record in ipairs(res) do
        local uid = record.uid
        if not history[uid] then
            local typ = all[record.type]
            if not typ then
                typ = {}
                all[record.type] = typ
            end
            typ[uid] = true
            file:write(uid, "\n")
            all_count = all_count + 1
        end
    end
    file:close()
    
    local file = assert(io.open("tool/u3d_install.txt", "a+"))
    file:write("U3D插件安装统计结果：", date, pid and (" - "..pid) or "", "\r\n")
    for _, row in ipairs(CFG) do
        process_row(file, row, all)
    end
    file:write("\r\n")
    file:close()
    return all_count
end
