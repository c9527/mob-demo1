#!/bin/sh
grep "fight result" logs/log_2016031* > all.log

echo "======== count by device ===========" > count.log
awk 'BEGIN{print "device", "count"} {if($7 == "web" || $7 == "android") a[$7]++;} END {for(i in a) print i, a[i];}' all.log >> count.log
echo "===================================" >> count.log

echo "========= count by device and channel: web ============" >> count.log
awk '{if($7 == "web") print $0;}' all.log | awk '{a[$5]++; b[$5] += $12; c[$5] += $13; if($14 == "true") d[$5]++ } END {print "1-fights: \n", "channel", "count"; for(i in a) print i, a[i]; print "2-kills: \n", "channel", "count"; for(i in b) print i, b[i]; print "\n 03-death: \n"; for(i in c) print i, c[i]; print "\n 04-wins: \n"; for(i in d) print i, d[i] }' >> count.log
echo "========= count by device and channel: android ============" >> count.log
awk '{if($7 == "android") print $0;}' all.log | awk '{a[$5]++; b[$5] += $12; c[$5] += $13; if($14 == "true") d[$5] ++} END {print "1-fights: \n", "channel", "count"; for(i in a) print i, a[i]; print "2-kills: \n", "channel", "count"; for(i in b) print i, b[i]; print "\n 03-death: \n"; for(i in c) print i, c[i]; print "\n 04-wins: \n"; for(i in d) print i, d[i] }' >> count.log
echo " " >> count.log
cat count.log
