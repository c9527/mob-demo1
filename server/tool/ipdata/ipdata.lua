local province_map = {
    "内蒙古",
    "西藏",
    "新疆",
    "北京",
    "广西",
    "湖南",
    "宁夏",
    "四川",
}

local format = string.format
local tonumber = tonumber
local match = string.match
local ipairs = ipairs
local concat = table.concat
local sub = string.sub
local find = string.find
local assert = assert

local function preprocess_ip(ip)
    local ips = {match(ip, "(%d+).(%d+).(%d+).(%d+)")}
    for i, n in ipairs(ips) do
        ips[i] = format("%02x", tonumber(n))
    end
    return tonumber("0x" .. concat(ips))
end

local function preprocess_area(str)
    for _, p in ipairs(province_map) do
        if find(str, p) then
            return p
        end
    end    
    local s, e = find(str, "省") or find(str, "市")
    if s then
        return sub(str, 1, s - 1)
    end    
    s, e = find(str, "大学")
    if s then
        return sub(str, 1, e)
    end
    return str
end

local function gen_ip_data()
    local file = assert(io.open("tool/ipdata/ipdata.txt", "r"))
    local iplist = {}
    local cnt = 0
    local last_ip = -1
    local last_area = nil
    for line in file:lines() do
        local pre_ip_start, pre_ip_end, pre_area = match(line, "(%g+)%s+(%g+)%s+(%S+)")
        local ip_start = preprocess_ip(pre_ip_start)
        local ip_end = preprocess_ip(pre_ip_end)
        local area = preprocess_area(pre_area)
        assert(ip_start == last_ip + 1, line)
        last_ip = ip_end
        if area==last_area then
            iplist[cnt][1] = ip_end
        else
            cnt = cnt + 1
            iplist[cnt] = {ip_end, area}
            last_area = area
        end
    end
    file:close()
    assert(last_ip == 0xffffffff)
    local luafile = assert(io.open("cfg/ip_data.config", "w+"))
    luafile:write("return {\r\n")
    for _, data in pairs(iplist)  do
        luafile:write(format("  {0x%x, \"%s\"},\r\n", data[1], data[2]))
    end 
    luafile:write("}\r\n")
    luafile:close()
end

gen_ip_data()

return "done"
