#!/bin/bash

tar_path=/data/sygame/server.tar.gz
server_path=/data/
server_folder=server

clear
while true
do
echo "------------------------------"
echo -e "\t请对比压缩文件md5sum"
md5sum $tar_path
echo -e "\t\t 1、确认解压"
echo -e "\t\t 2、重试"
echo -e "\t\t 3、重启服务器"
echo -e "\t\t 4、解压并重启"
echo -e "\t\t 5、热更新服务器"
echo -e "\t\t q、取消"

read -p "请选择:" choice
case $choice in
1)
echo -e "开始解压 $tar_path 到 $server_path"
cd $server_path
tar xzvf $tar_path
ls $server_folder -l -a
echo -e "解压完成！"
;;
2)
echo -e "\n\t retry now..."
sleep 1
clear
;;
3)
cd $server_path/server
./restart.sh
exit
;;
4)
cd $server_path
tar xzvf $tar_path
cd $server_path/server
./restart.sh
exit
;;
5)
cd $server_path/server
port=`cat cfg/system/server.config|grep DbgPort|awk '{print $3}'`
curl http://127.0.0.1:$port/reload
;;
q)
echo "退出"
exit
;;
esac
done

