#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# 脚本所在路径，请放到游戏服根目录
ROOT_DIR=$(cd $(dirname $0); pwd)
# 主配置文件
CFG=${ROOT_DIR}/cfg/system/server.config

if [ ! -e ${CFG} ];then
   echo "未找到配置文件${CFG}，请检查"
   exit 1
fi


GAMEDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(MysqlConf and MysqlConf.database or \"\")")
LOGDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(LogDBConf and LogDBConf.database or \"\")")
DICTDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(DictDBConf and DictDBConf.database or \"\")")
CENTERDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(CenterDBConf and CenterDBConf.database or \"\")")
if [ ! -z ${GAMEDB} ]; then
	DB=MysqlConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${GAMEDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysql -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${GAMEDB} < ${ROOT_DIR}/db/update_game.sql
fi
if [ ! -z ${CENTERDB} ]; then
	DB=CenterDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${CENTERDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysql -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${CENTERDB} < ${ROOT_DIR}/db/update_center.sql
fi
if [ ! -z ${LOGDB} ]; then
	DB=LogDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${LOGDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysql -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${LOGDB} < ${ROOT_DIR}/db/update_log.sql
fi
if [ ! -z ${DICTDB} ]; then
	DB=DictDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${DICTDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysql -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${DICTDB} < ${ROOT_DIR}/db/update_dict.sql
fi
