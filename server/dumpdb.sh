#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# 脚本所在路径，请放到游戏服根目录
ROOT_DIR=$(cd $(dirname $0); pwd)
# 主配置文件
CFG=${ROOT_DIR}/cfg/system/server.config

if [ ! -e ${CFG} ];then
   echo "未找到配置文件${CFG}，请检查"
   exit 1
fi


GAMEDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(MysqlConf and MysqlConf.database or \"\")")
LOGDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(LogDBConf and LogDBConf.database or \"\")")
DICTDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(DictDBConf and DictDBConf.database or \"\")")
CENTERDB=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print(CenterDBConf and CenterDBConf.database or \"\")")
if [ ! -z ${GAMEDB} ]; then
	DB=MysqlConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${GAMEDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysqldump -d -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${GAMEDB} | sed 's/ USING BTREE//g;s/ AUTO_INCREMENT=[0-9]*//g;s/AUTO_INCREMENT=[0-9]*//g' > ${ROOT_DIR}/db/clean_game_tables.sql
fi
if [ ! -z ${CENTERDB} ]; then
	DB=CenterDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${CENTERDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysqldump -d -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${CENTERDB} | sed 's/ USING BTREE//g;s/ AUTO_INCREMENT=[0-9]*//g;s/AUTO_INCREMENT=[0-9]*//g' > ${ROOT_DIR}/db/center_android_4399.sql
fi
if [ ! -z ${LOGDB} ]; then
	DB=LogDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${LOGDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysqldump -d -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${LOGDB} | sed 's/ USING BTREE//g;s/ AUTO_INCREMENT=[0-9]*//g;s/AUTO_INCREMENT=[0-9]*//g' > ${ROOT_DIR}/db/clean_log_tables.sql
fi
if [ ! -z ${DICTDB} ]; then
	DB=DictDBConf
	USER=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.user)") 
	PASSWORD=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.password)") 
	HOST=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.host)")
	PORT=$(cd ${ROOT_DIR} && lua -e "loadfile('cfg/system/server.config')();print($DB.port)")
	echo ${DICTDB} ${USER} ${PASSWORD} ${HOST} ${PORT}
	mysqldump -P ${PORT} -h ${HOST} -u ${USER} -p${PASSWORD} ${DICTDB} | sed 's/ USING BTREE//g;s/ AUTO_INCREMENT=[0-9]*//g;s/AUTO_INCREMENT=[0-9]*//g' > ${ROOT_DIR}/db/dict_android_4399.sql
fi
