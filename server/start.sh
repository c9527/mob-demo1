#!/bin/bash
# 脚本所在路径，请放到游戏服根目录
ROOT_DIR=$(cd $(dirname $0); pwd)
LOGS_DIR=logs
SRV=$(basename ${ROOT_DIR})
SCRIPT=/data/sh/mk_coredump2.py

ERR_LOG="err_201*.log"
mkdir -p /data/logs/chatlog/
echo "-------------------- start server --------------------"
ulimit -c unlimited
cd ${ROOT_DIR} && ${ROOT_DIR}/skynet/skynet cfg/game.config &
ps aux|grep skynet|grep -v grep
echo "-------------------------"
ls -l --color=always
#echo "-------------------------"
#ls -l ${LOGS_DIR}
echo "------------------------------------------------------"

if [ -f ${SCRIPT} ];then
   cd ${ROOT_DIR}/logs && python ${SCRIPT} ${SRV} start >/dev/null 2>&1 &

fi
