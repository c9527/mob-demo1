#include "skynet.h"
#include "skynet_malloc.h"
#include "skynet_timer.h"
#include "lua.h"
#include "lauxlib.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <md5.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>

#define MAXUNICODE  0x10FFFF
#define iscont(p)   ((*(p) & 0xC0) == 0x80)

extern uint64_t g_need_time_fix;
extern uint64_t g_cur_time_fix();

static char** senswords = NULL;
static lua_Integer* literalArray = NULL;
extern int g_worker_profile_ver;
extern int skynet_handle_profile(uint32_t handle, int64_t * count, int64_t * time);

extern int skynet_sleep_count();

static const char *
tolstring(lua_State *L, size_t *sz, int index) {
    const char * ptr;
    if (lua_isuserdata(L,index)) {
        ptr = (const char *)lua_touserdata(L,index);
        *sz = (size_t)luaL_checkinteger(L, index+1);
    } else {
        ptr = luaL_checklstring(L, index, sz);
    }
    return ptr;
}

static int
_copy_userdata(lua_State *L) {
    size_t size;
    const char *ptr = tolstring(L, &size, 1);
    void *buffer = lua_newuserdata(L, size);
    memcpy(buffer, ptr, size);
    return 1;
}

static int
_md5(lua_State *L) {
    size_t size;
    const char *ptr = tolstring(L, &size, 1);
    
    char temp[HASHSIZE];
    md5(ptr, (long)size, temp);
    
    char md5str[HASHSIZE*2];
    int i;
    for (i = 0; i < HASHSIZE; i++)
        snprintf(md5str + i + i, 3, "%02x", (unsigned char)temp[i]);
    
    lua_pushlstring(L, md5str, HASHSIZE*2);
    return 1;
}

int special_normal_literal_n( lua_Integer judgeLiteral ) {
	if( !literalArray )
	{
		return 0;
	}
    lua_Integer number = literalArray[0];
    for( int i = 1; i <= number; i++)
    {
        if ( literalArray[i] == judgeLiteral ) {
            return 1;
        }
    }
    return 0;
}

static int
_load_filterword(lua_State *L) {	
	int number = luaL_len(L, -1);
	literalArray = (lua_Integer*)malloc(sizeof(lua_Integer) * (number + 1));
    literalArray[0] = number;
	for(int i = 1; i <= number; ++i)
	{
		lua_pushnumber(L,i);
		lua_rawget(L,-2);
		lua_Integer literal = lua_tointeger(L,-1);
		literalArray[i] = literal;
		lua_pop(L,1);
	}
	return 0;
}

static int 
_count_utf8 (lua_State *L) {
    lua_Integer code = lua_tointeger(L, 1);
    lua_Integer lower_bound = lua_tointeger(L, 2);
    lua_Integer upper_bound = lua_tointeger(L, 3);
    lua_Integer language_code_n = lua_tointeger(L, 4);
    lua_Integer literal_n = lua_tointeger(L, 5);
    lua_Integer illegal_n = lua_tointeger(L, 6);
    lua_Integer special_literal_n = lua_tointeger(L, 7);
    if (code >= lower_bound && code <= upper_bound) {
        language_code_n++;
    } else if ((code >= 48 && code <= 57) || (code >= 65 && code <= 90) || (code >= 97 && code <= 122) ) {
        literal_n++;
    } else if( special_normal_literal_n(code) ) {
        special_literal_n++;
    } else {
        illegal_n++;
    }
    lua_pushinteger(L, language_code_n );
    lua_pushinteger(L, literal_n );
    lua_pushinteger(L, illegal_n );
    lua_pushinteger(L, special_literal_n);
    return 4;
}

static int 
find_sensword(const char *str1,char *str2) {  
    if (strstr(str1,str2)!=NULL) {  
        return 1;
    }
    return 0;
}

static int
_load_sensword(lua_State *L) {
    FILE *fp;
    char sensword[100];
    char tempFileBuffer[100] = "./cfg/";
    size_t len;
    char** newSenswords = (char**) malloc(sizeof(char*));;
    int newSize = 0;
    const char *fileName = luaL_checklstring(L, 1, NULL);
    strcat(tempFileBuffer, fileName);
    //fp = fopen("./cfg/wordfilter.txt","r+");
    fp = fopen(tempFileBuffer, "r+");
    if (fp == NULL) {
        return luaL_error(L, "_load_sensword : file open error");
    }
    fseek(fp, 0, SEEK_SET);
    while(!feof(fp)) {
        fgets(sensword, sizeof(sensword), fp);
        len = strlen(sensword);
        while (len > 0 && (sensword[len-1] == '\r' || sensword[len-1] == '\n'))
            sensword[--len] = '\0';
        if (len == 0)
            continue;

        newSenswords[newSize] = malloc(strlen(sensword) + 1);
        strcpy(newSenswords[newSize], sensword);
        ++newSize;
        newSenswords = (char**) realloc(newSenswords, (newSize + 1) * sizeof(char*));
    }
    newSenswords[newSize] = "";
    fclose(fp);
    senswords = newSenswords;
    lua_pushboolean(L, 1);
    return 1;
}

static int 
_is_contain_sensword(lua_State *L) {
    size_t size;
    const char *ptr = tolstring(L, &size, 1);
    int is_contain = 0;
    char** tmpSenswords = senswords;
	if (tmpSenswords != NULL)
	{
    	for (int i = 0; strcmp("", tmpSenswords[i]) != 0; ++i)
    	{
        	is_contain = find_sensword(ptr, tmpSenswords[i]);
        	if (is_contain)
        	{
            	break;
        	}
    	}
	}

    lua_pushboolean(L, is_contain);
    return 1;
}

static int 
_get_time(lua_State *L) {
	struct timespec ti;
	clock_gettime(CLOCK_MONOTONIC, &ti);
	lua_pushinteger(L, ti.tv_sec * 1000000000l + ti.tv_nsec);
	return 1;
}

static int
_list_dir(lua_State *L) {
    const char * dirname = luaL_checkstring(L, 1);
    DIR* dp = opendir(dirname);
    if (!dp)
        return 0;
    
    lua_newtable(L);
    struct dirent* dirp;
    while ((dirp = readdir(dp)) != NULL) {
        if (!strcmp(".", dirp->d_name) || !strcmp("..", dirp->d_name))
            continue;
    	lua_pushinteger(L, dirp->d_type);
        lua_setfield(L, -2, dirp->d_name);
    }
    closedir(dp);
    
    return 1;
}

static int
_stat_file(lua_State *L) {
    const char * path = luaL_checkstring(L, 1);
    struct stat st;
    if (stat(path, &st))
        return 0;
    
    lua_newtable(L);
    lua_pushinteger(L, st.st_mode);
    lua_setfield(L, -2, "mode");
    lua_pushboolean(L, S_ISDIR(st.st_mode));
    lua_setfield(L, -2, "isdir");
    lua_pushboolean(L, S_ISREG(st.st_mode));
    lua_setfield(L, -2, "isreg");
    lua_pushboolean(L, S_ISLNK(st.st_mode));
    lua_setfield(L, -2, "islnk");
    lua_pushinteger(L, st.st_size);
    lua_setfield(L, -2, "size");
    lua_pushinteger(L, st.st_mtime);
    lua_setfield(L, -2, "time");

    return 1;
}

static int 
_get_timefix(lua_State *L) {
	lua_pushinteger(L, g_cur_time_fix());
	return 1;
}

static int 
_set_timefix(lua_State *L) {
	g_need_time_fix = luaL_checkinteger(L, 1);
	return 0;
}

static int 
_time_now(lua_State *L) {
	lua_pushinteger(L, skynet_now());
	return 1;
}

static int 
_time_start(lua_State *L) {
	lua_pushinteger(L, skynet_starttime());
	return 1;
}

static int 
_sleep_count(lua_State *L) {
	lua_pushinteger(L, skynet_sleep_count());
	return 1;
}

static int 
_set_worker_profile(lua_State *L) {
    g_worker_profile_ver = (int)luaL_checkinteger(L, 1);
	return 0;
}

static int 
_get_handle_profile(lua_State *L) {
    uint32_t handle = (uint32_t)luaL_checkinteger(L, 1);
    int64_t count;
    int64_t time;
    if (!skynet_handle_profile(handle, &count, &time))
        return 0;
    lua_pushinteger(L, count);
    lua_pushinteger(L, time);
	return 2;
}

static int
_count_table(lua_State *L) {
    if (!lua_istable(L, 1))
        return luaL_argerror(L, 1, "need table");
	int count = 0;
	lua_settop(L, 1);
    lua_pushnil(L);
    while (lua_next(L, 1)) {
        count++;
        lua_settop(L, 2);
    }
    lua_pushinteger(L, count);
    return 1;
}

static int
_send_all(lua_State *L) {
    if (!lua_istable(L, 1))
        return luaL_argerror(L, 1, "need table");
    int dest_index = lua_toboolean(L, 2) ? 5 : 6;
	int type = luaL_checkinteger(L, 3);
	int mtype = lua_type(L, 4);
	size_t size = 0;
	void * msg = NULL;
	switch (mtype) {
	case LUA_TSTRING: {
		msg = (void *)lua_tolstring(L, 4, &size);
		if (size == 0)
			msg = NULL;
		break;
	}
	case LUA_TLIGHTUSERDATA: {
		msg = lua_touserdata(L, 4);
		size = luaL_checkinteger(L, 5);
		break;
	}
	default:
		return luaL_argerror(L, 4, "need string or userdata");
	}
	
	lua_getfield(L, LUA_REGISTRYINDEX, "skynet_context");
	struct skynet_context * context = lua_touserdata(L, -1);
	if (!context)
	    return luaL_error(L, "skynet_context not found!");
	
	int count = 0;
	
	lua_settop(L, 4);
	lua_pushnil(L);
    while (lua_next(L, 1)) {
        int dest = luaL_checkinteger(L, dest_index);
    	if (skynet_send(context, 0, dest, type, 0, msg, size) > 0)
    	    count++;
        lua_settop(L, 5);
    }
    
    if (mtype == LUA_TLIGHTUSERDATA)
        skynet_free(msg);
    
    lua_pushinteger(L, count);
    return 1;
}

int
luaopen_misc(lua_State *L) {
    luaL_checkversion(L);
    luaL_Reg l[] = { 
        { "copy_userdata", _copy_userdata },
        { "md5", _md5 },
        { "count_utf8", _count_utf8 },
        { "load_sensword", _load_sensword },
        { "load_filterword", _load_filterword },
        { "is_contain_sensword", _is_contain_sensword },
        { "get_time", _get_time },
        { "list_dir", _list_dir },
        { "stat_file", _stat_file },
        { "get_timefix", _get_timefix },
        { "set_timefix", _set_timefix },
        { "time_now", _time_now },
        { "time_start", _time_start },
        { "sleep_count", _sleep_count },
        { "set_worker_profile", _set_worker_profile },
        { "get_handle_profile", _get_handle_profile },
        { "count_table", _count_table },
        { "send_all", _send_all },
        { NULL, NULL },
    };  
    luaL_newlib(L,l);
    return 1;
}
