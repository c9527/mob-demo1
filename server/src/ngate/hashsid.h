#ifndef skynet_hashsid_h
#define skynet_hashsid_h

#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct hashsid_node {
	int sid;
    int fd;
	struct hashsid_node *next;
};

struct hashsid {
	int hashmod;
	int cap;
	int count;
	struct hashsid_node *arr; // hash数组
	struct hashsid_node **hash;  // 指向hash值相同的第一个节点
};

static void
hashsid_init(struct hashsid *hi, int max) {
	int i;
	int hashcap;
	hashcap = 16;
	while (hashcap < max) {
		hashcap *= 2;
	}
	hi->hashmod = hashcap - 1;
	hi->cap = max;
	hi->count = 0;
	hi->arr = skynet_malloc(max * sizeof(struct hashsid_node));
	for (i=0;i<max;i++) {
		hi->arr[i].sid = -1;
        hi->arr[i].fd = -1;
		hi->arr[i].next = NULL;
	}
	hi->hash = skynet_malloc(hashcap * sizeof(struct hashsid_node *));
	memset(hi->hash, 0, hashcap * sizeof(struct hashsid_node *));
}

static void
hashsid_clear(struct hashsid *hi) {
	skynet_free(hi->arr);
	skynet_free(hi->hash);
	hi->arr = NULL;
	hi->hash = NULL;
	hi->hashmod = 1;
	hi->cap = 0;
	hi->count = 0;
}

static int
hashsid_lookup(struct hashsid *hi, int sid) {
	int h = sid & hi->hashmod;
	struct hashsid_node * c = hi->hash[h];
	while(c) {
		if (c->sid == sid)
			return c->fd;   // c - hi->arr; 
		c = c->next;
	}
	return -1;
}

static int
hashsid_remove(struct hashsid *hi, int sid) {
	int h = sid & hi->hashmod;
	struct hashsid_node * c = hi->hash[h];
	if (c == NULL)
		return -1;
	if (c->sid == sid) {
		hi->hash[h] = c->next;
		goto _clear;
	}
	while(c->next) {
		if (c->next->sid == sid) {
			struct hashsid_node * temp = c->next;
			c->next = temp->next;
			c = temp;
			goto _clear;
		}
		c = c->next;
	}
	return -1;
_clear:
	c->sid = -1;
    int fd = c->fd;
    c->fd = -1;
	c->next = NULL;
	--hi->count;
	return fd;  // c - hi->arr;
}

static int  // 插入操作不影响现有的位置，每次寻找一个空位，只更新头指针
hashsid_insert(struct hashsid * hi, int sid, int fd) {
	struct hashsid_node *c = NULL;
	int i;
	for (i = 0; i < hi->cap; i++) {   // 跳跃式查找第一个空位
		int index = (i + sid) % hi->cap;
		if (hi->arr[index].sid == -1) {
			c = &hi->arr[index];
			break;
		}
	}
	assert(c);
	++hi->count;
	c->sid = sid;
    c->fd = fd;
	assert(c->next == NULL);
	int h = sid & hi->hashmod;
	if (hi->hash[h]) {  // 插入首位
		c->next = hi->hash[h];
	}
	hi->hash[h] = c;
	
	return fd;   // c - hi->arr;
}

static inline int
hashsid_full(struct hashsid *hi) {
	return hi->count == hi->cap;
}

#endif
