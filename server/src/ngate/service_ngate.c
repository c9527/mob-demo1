#include "skynet.h"
#include "skynet_socket.h"
#include "databuffer.h"
#include "hashsid.h"
#include "skynet_handle.h"
#include "skynet_timer.h"
#include "skynet_server.h"
#include "skynet_mq.h"
#include "lua.h"
#include "lauxlib.h"
#include "lua-seri.h"
#include "md5.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <memory.h>
#include <time.h>

#define BACKLOG 32
#ifndef CACHE_BUFF_SIZE
#define CACHE_BUFF_SIZE (1 << 16)    // 64K
#endif
#define TOKEN_SIZE 65

// TODO ngate 直接用agent的handle作为id，简化连接信息
extern void skynet_log(struct skynet_context * context, const char *msg, ...);

struct msg_cache {  // 缓存发往客户端的消息循环队列
    unsigned char buff[ CACHE_BUFF_SIZE ];
    unsigned char *head;         // 队首指针
    unsigned char *tail;         // 队尾指针
    int count;         // 队列中的协议数量
    int last_id;       // 最后一条协议的id
    int err_id;        // 连接出错时的下一条协议id
};

inline static int
get_msg_size(struct msg_cache *cache, unsigned char *msg) {
    unsigned char *limit = cache->buff + CACHE_BUFF_SIZE;
    if (msg >= limit)
        msg = msg - CACHE_BUFF_SIZE;
    int size = *msg++ << 8;
    if (msg >= limit)
        msg = msg - CACHE_BUFF_SIZE;
    size += *msg;
    return size;
}

struct connection {
	int sid;	// skynet_socket id
    int fd;     // 分配给上层的fd
    char token[TOKEN_SIZE]; // 重连的token
	uint32_t agent; // agent服务的handle
	char remote_name[32];   // 连接的ip跟port
    struct msg_cache cache;
	struct databuffer buffer;   // 接收数据的缓冲区
    int receive_id;
};

struct net_stat {
    int64_t count;
    int64_t size;
    int64_t total_time[3];
    int64_t max_time[3];
};

struct ngate {
	struct skynet_context *ctx;
	int listen_id;
	uint32_t watchdog;
	int client_tag;
	int header_size;
	int max_connection;
	struct hashsid hash;
	struct connection *conn;
	// todo: save message pool ptr for release
	struct messagepool mp;
    lua_State *L;
    int check_mqlen_time;
    uint32_t last_check_time;
    int64_t count_open;
    int64_t count_close;
    int64_t count_error;
    int64_t count_recon;
    int64_t count_reconok;
    struct net_stat send_stat;
    struct net_stat recv_stat;
    int socket_nodelay;
    int ss_id;  // socket server id
};

static int _rec_time = 1;

static int64_t 
_get_time() {
    if (!_rec_time)
        return 0;
	struct timespec ti;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ti);
	return ti.tv_sec * 1000000000 + ti.tv_nsec;
}

static void
_set_net_stat(struct net_stat * stat, int sz, int64_t* tm) {
    stat->count++;
    stat->size += sz;
    for (int i=0; i<3; i++)
    {
        int64_t t = tm[i+1] - tm[i];
        stat->total_time[i] += t;
        if (t > stat->max_time[i])
            stat->max_time[i] = t;
    }
}

static void _push_net_stat(struct net_stat * stat, lua_State *L)
{
    lua_pushinteger(L, stat->count);
    lua_pushinteger(L, stat->size);
    for (int i=0; i<3; i++)
    {
        lua_pushinteger(L, stat->total_time[i]);
        lua_pushinteger(L, stat->max_time[i]);
        stat->max_time[i] = 0;
    }
}

static void _close_socket(struct ngate *, struct connection *);

struct ngate *
ngate_create(void) {
	struct ngate *g = skynet_malloc(sizeof(*g));
	memset(g, 0, sizeof(*g));
	g->listen_id = -1;
	return g;
}

void
ngate_release(struct ngate *g) {
	int i;
	struct skynet_context *ctx = g->ctx;
	for (i = 0; i < g->max_connection; i++) {
		struct connection *c = &g->conn[i];
		if (c->sid >= 0) {
            _close_socket(g, c);
		}
	}
	if (g->listen_id >= 0) {
		qs_socket_close(ctx, g->ss_id, g->listen_id);
	}
	messagepool_free(&g->mp);
	hashsid_clear(&g->hash);
	skynet_free(g->conn);
    lua_close(g->L);
	skynet_free(g);
}

static void _mask_buf(void * buf, uint32_t id, int size) {
	uint32_t * buf1 = (uint32_t *)buf;
    uint32_t msk = (id * 0xabcd4399u) ^ 0x12345678u;
	int size1 = size >> 2;
	int i;
    for (i = 0; i < size1; i++)
        buf1[i] ^= msk;
	uint8_t * buf2 = (uint8_t *)(buf1 + size1);
	int size2 = size & 0x3; 
    for (i = 0; i < size2; i++)
        buf2[i] ^= ((uint8_t *)&msk)[i];
}


static uint32_t _adler32(void * buf, int size)
{
    uint32_t s1 = 1, s2 = 0;
    int i;
    for (i = 0; i < size; i++) {
        s1 = (s1 + ((uint8_t *)buf)[i]) % 65521;
        s2 = (s2 + s1) % 65521;
    }
    return (s2 << 16) + s1;
}

static int  // TODO 能否优化
get_free_fd(struct ngate *g) {
    int i;
    for (i = 0; i < g->max_connection; i++)
        if (g->conn[i].sid < 0)
            return i;
    return -1;
}

static int
_flush_msg(struct ngate * g, struct msg_cache *cache, int sid, int msg_id) {
    unsigned char *head = cache->head;
    int msg_cnt = cache->last_id - msg_id;
    int i, size;
    if (msg_cnt <= 0 || msg_cnt > cache->count)
        return 0;
    unsigned char *limit = cache->buff + CACHE_BUFF_SIZE;
    for (i = 0; i < cache->count - msg_cnt; ++i) {
        size = get_msg_size(cache, head);
        head = head + size + 2;
        if (head >= limit)
            head = head - CACHE_BUFF_SIZE;
    }
    char *msg;
    while (head < cache->tail) {
        size = get_msg_size(cache, head);
        msg = (char *)skynet_malloc(size + 2);
        if (head + size + 2 > limit) {
            memcpy(msg, head, limit - head);
            memcpy(msg + (limit - head), cache->buff, size + 2 - (limit - head));
        } else {
            memcpy(msg, head, size + 2);
        }
        qs_socket_send(g->ctx, g->ss_id, sid, (void *)msg, size + 2);
        head = head + size + 2;
        if (head >= limit)
            head = head - CACHE_BUFF_SIZE;
    }
    return 1;
}

static int
_pop_msg(struct msg_cache *cache) {
    unsigned char *head = cache->head;
    int size = get_msg_size(cache, head);
    if (cache->count <= 0) // 队列空
        return 0;
    --cache->count;
    head = head + size + 2;
    if (head - cache->buff >= CACHE_BUFF_SIZE)
        head = head - CACHE_BUFF_SIZE;
    cache->head = head;
    return 1;
}

static int
_push_msg(struct msg_cache *cache, const void *msg, int size) {
    if (cache->err_id > 0 && cache->last_id - cache->err_id >= cache->count) {
        return -1;
    }
    if (cache->head == 0 || cache->tail == 0) {
        return -2;
    }
    if (size > CACHE_BUFF_SIZE) {
        return -3;
    }
    
    if (cache->tail == cache->head && cache->count > 0)  // 队列满
        _pop_msg(cache);
    while ((cache->tail > cache->head && CACHE_BUFF_SIZE - (cache->tail - cache->head) < size) ||
            (cache->head > cache->tail && cache->head - cache->tail < size)) {
        if (!_pop_msg(cache))
            return 0;
    }
    size_t sz = size;
    unsigned char *limit = cache->buff + CACHE_BUFF_SIZE;
    if (sz > limit - cache->tail)
        sz = limit - cache->tail;
    memcpy( cache->tail, msg, sz);
    if (sz < size)
        memcpy(cache->buff, msg + sz, size - sz);
    cache->tail = cache->tail + size;
    if (cache->tail >= limit)
        cache->tail = cache->tail - CACHE_BUFF_SIZE;
    ++cache->count;
    ++cache->last_id;
    return 1;
}

static void
_parm(char *msg, int sz, int command_sz) {  // 移除第一个字符串
	while (command_sz < sz) {
		if (msg[command_sz] != ' ')
			break;
		++command_sz;
	}
	int i;
	for (i = command_sz; i < sz; i++) {
		msg[i - command_sz] = msg[i];
	}
	msg[i - command_sz] = '\0';
}

static void
_forward_agent(struct ngate * g, int fd, int sid, uint32_t agent_handle) {
    if (fd < 0 || fd > g->max_connection) {
        skynet_error(g->ctx, "forward_agent err fd:%d sid:%d agent:%u", fd, sid, agent_handle);
        return;
    }
    struct connection *c = &g->conn[fd];
    if (c->sid != sid) {   // 这里验证
        lua_State *L = g->L;
        lua_settop(L, 0);
        lua_pushstring(L, "accept");
        lua_pushstring(L, "socket_close");
        lua_pushstring(L, "missing");
        luaseri_pack(L);
        void *msg = lua_touserdata(L, -2);
        int sz = lua_tointeger(L, -1);
        skynet_send(g->ctx, 0, agent_handle, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, msg, sz);
        return;
    }
    c->agent = agent_handle;
    
    char *msg = (char *)skynet_malloc(256);
    snprintf(msg + 2, 254, "{\"ok\":true,\"id\":%d,\"token\":\"%s\"}", c->fd, c->token);
    short sz = (short)strlen(msg + 2);
    msg[0] = sz >> 8;
    msg[1] = sz & 255;
    qs_socket_send(g->ctx, g->ss_id, c->sid, (void *)msg, sz + 2);
    g->count_open++;
}

static void
_close_socket(struct ngate * g, struct connection *c) {
    if (c->agent)
        g->count_close++;
    qs_socket_close(g->ctx, g->ss_id, c->sid);
    databuffer_clear(&c->buffer, &g->mp);
    hashsid_remove(&g->hash, c->sid);
    memset(c, 0, sizeof(*c));
    c->sid = -1;
}

static void
_ctrl(struct ngate * g, const void * msg, int sz) {
	struct skynet_context * ctx = g->ctx;
	char tmp[sz+1];
	memcpy(tmp, msg, sz);
	tmp[sz] = '\0';
	char * command = tmp;
	int i;
	if (sz == 0)
		return;
	for (i = 0; i < sz; i++) {
		if (command[i] == ' ') {
			break;
		}
	}
	if (memcmp(command, "kick", i)==0) {
		_parm(tmp, sz, i);
		char * reason = tmp;
        char * fdstr = strsep(&reason, " ");
		int fd = strtol(fdstr, NULL, 10);
        int sid = 0;
		if (0 <= fd && fd < g->max_connection) {
            sid = g->conn[fd].sid;
            _close_socket(g, &g->conn[fd]);
		}
        skynet_log(ctx, "\033[1;33mngate kick sid:%d fd:%d %s\033[0m", sid, fd, reason);
		return;
	}
	if (memcmp(command, "forward", i) == 0) {
		_parm(tmp, sz, i);
		char * agent = tmp;
		char * fdstr = strsep(&agent, " ");
		if (agent == NULL) {
			return;
		}
		char * sidstr = strsep(&agent, " ");
		if (agent == NULL) {
			return;
		}
		int fd = strtol(fdstr, NULL, 10);
        int sid = strtol(sidstr, NULL, 10);
		uint32_t agent_handle = strtoul(agent, NULL, 10);
		_forward_agent(g, fd, sid, agent_handle);
		return;
	}
    if (memcmp(command, "check_mqlen", i) == 0) {
        _parm(tmp, sz, i);
        int time = strtol(tmp, NULL, 10);
        g->check_mqlen_time = time;
        skynet_log(ctx, "\033[1;33mngate set check mqlen time %d\033[0m", time);
        return;
    }
    if (memcmp(command, "get_count", i) == 0) {
        _parm(tmp, sz, i);
		char * param = tmp;
		char * rec = strsep(&param, " ");
		if (param) {
			_rec_time = strtol(rec, NULL, 10);
		}
        lua_State *L = g->L;
        lua_settop(L, 0);
        lua_pushstring(L, "accept");
        lua_pushstring(L, "get_count");
        lua_pushinteger(L, strtoul(skynet_command(ctx, "STAT", "mqlen"), NULL, 10));
        lua_pushinteger(L, g->count_open);
        lua_pushinteger(L, g->count_close);
        lua_pushinteger(L, g->count_error);
        lua_pushinteger(L, g->count_recon);
        lua_pushinteger(L, g->count_reconok);
        lua_pushinteger(L, g->count_recon - g->count_reconok);
        _push_net_stat(&g->send_stat, L);
        _push_net_stat(&g->recv_stat, L);
        luaseri_pack(L);
        void *msg = lua_touserdata(L, -2);
        int sz = lua_tointeger(L, -1);
        skynet_send(ctx, 0, g->watchdog, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, msg, sz);
		return;
	}
	if (memcmp(command,"start",i) == 0) {
		qs_socket_start(ctx, g->ss_id, g->listen_id);
		return;
	}
	if (memcmp(command, "nodelay", i) == 0) {
        _parm(tmp, sz, i);
        int nodelay = strtol(tmp, NULL, 10);
        g->socket_nodelay = nodelay;
        skynet_log(ctx, "\033[1;33mngate set socket_nodelay %d\033[0m", nodelay);
		return;
	}
    if (memcmp(command, "close", i) == 0) {
		if (g->listen_id >= 0) {
			qs_socket_close(ctx, g->ss_id, g->listen_id);
			g->listen_id = -1;
		}
		return;
	}
	skynet_error(ctx, "[gate] Unkown command : %s", command);
}

static void
_send_format_msg(struct ngate *g, int sid, char *fmt, ...) {
    char *msg = (char *)skynet_malloc(256);
    va_list ap;
    va_start(ap, fmt);
    int sz = vsnprintf(msg + 2, 254, fmt, ap);
    va_end(ap);
    msg[0] = sz >> 8;
    msg[1] = sz & 255;
    qs_socket_send(g->ctx, g->ss_id, sid, (void *)msg, sz + 2);
}

static void
_socket_error(struct ngate *g, struct connection *c, char *msg) {
    _send_format_msg(g, c->sid, "{\"ok\":false,\"msg\":\"%s\"}", msg);
    _close_socket(g, c);
}

static void
_process_predata(struct ngate *g, struct connection *c, void *data, int size) {
	struct skynet_context * ctx = g->ctx;
    char *pos = strstr(data, "\"token\":");
	if (pos != NULL) {
        char token[TOKEN_SIZE];
        memcpy(token, pos + 9, 32);
        token[33] = '\0';
        pos = strstr(data, "\"id\":");
        if (pos == NULL) { return; }
        int fd = strtol(pos + 5, NULL, 10);
        pos = strstr(data, "\"rid\":");
        if (pos == NULL) { return; }
        int msg_id = strtol(pos + 6, NULL, 10);
        pos = strstr(data, "\"sid\":");
        if (pos == NULL) { return; }
        int min_id = strtol(pos + 6, NULL, 10);
        
        g->count_recon++;
        struct connection *old = &g->conn[fd];
        if (old->sid >= 0 && old->sid != c->sid)
            qs_socket_close(ctx, g->ss_id, old->sid);

        if (fd < 0 || g->max_connection <= fd) {
            _socket_error(g, c, "err fd");
            return;
        }
        if (strncmp(old->token, token, 32)) {
            _socket_error(g, c, "err token");
            return;
        }
        if (!old->agent) {
            _socket_error(g, c, "err agent");
            return;
        }
        
        struct msg_cache *cache = &old->cache;
        if (cache->last_id - cache->count > msg_id || old->receive_id <= min_id - 1) {
            _socket_error(g, c, "lost pack");
            return;
        }

        int sid = c->sid;
        c->sid = -1;
        old->sid = sid;
        memcpy(old->remote_name, c->remote_name, sizeof(old->remote_name) - 1);
        hashsid_remove(&g->hash, sid);
        hashsid_insert(&g->hash, sid, fd);

        _send_format_msg(g, sid, "{\"ok\":true,\"rid\":%d}", old->receive_id);

        _flush_msg(g, cache, sid, msg_id);
        old->cache.err_id = 0;
        
        lua_State *L = g->L;
        lua_settop(L, 0);
        lua_pushstring(L, "accept");
        lua_pushstring(L, "socket_reopen");
        lua_pushinteger(L, old->fd);
        lua_pushinteger(L, old->sid);
        lua_pushstring(L, old->remote_name);
        luaseri_pack(L);
        void *msg = lua_touserdata(L, -2);
        int sz = lua_tointeger(L, -1);
        skynet_send(ctx, 0, old->agent, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, msg, sz);
        g->count_reconok++;
    } else {
        // 握手协议
        char token[256];
        char temp[HASHSIZE + 1];
        snprintf(token, 255, "%lu@%u#%d$%d", skynet_now(), skynet_starttime(), c->fd, c->sid);
        int len = strlen(token);
        md5(token, len, temp);
        int i;
        for (i = 0; i < HASHSIZE; i++)
            snprintf(token + i + i, 3, "%02x", (unsigned char)temp[i]);
        memcpy(c->token, token, HASHSIZE + HASHSIZE);
        
        lua_State *L = g->L;
        lua_settop(L, 0);
        lua_pushstring(L, "accept");
        lua_pushstring(L, "socket_open");
        lua_pushinteger(L, c->fd);
        lua_pushinteger(L, c->sid);
        lua_pushstring(L, c->remote_name);
        luaseri_pack(L);
        void *msg = lua_touserdata(L, -2);
        int sz = lua_tointeger(L, -1);
        skynet_send(ctx, 0, g->watchdog, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, msg, sz);
    }
}

static char
_v2c(unsigned char v) {
    if (v < 10)
        return '0' + v;
    return 'A' + v - 10;
}

static void
_forward(struct ngate *g, struct connection *c, int size, int64_t* tm) {
    struct skynet_context *ctx = g->ctx;
    void * temp = skynet_malloc(size);
    databuffer_read(&c->buffer, &g->mp, temp, size);
    
    tm[1] = _get_time();
    
    if (c->agent) { // lua层无需关心此message->id
        int id = c->receive_id + 1;
        if (size < 8) {
            skynet_error(ctx, "error_tcp_len:%d agent:%08x id:%d", size, c->agent, id);
            skynet_free(temp);
            return;
        }
        _mask_buf(temp, id, size);
        uint32_t crc = _adler32(temp + 4, size - 4);
        uint32_t * p = (uint32_t*)temp;
        tm[2] = _get_time();
        if (crc != p[0]) {
            uint32_t crc1 = p[0];
            uint32_t pid = p[1];
            _mask_buf(temp, id, size);
            char data[201];
            int end = (size >= 100) ? 100 : size;
            for (int i=0; i<end; i++) {
                unsigned char b = ((unsigned char *)temp)[i];
                data[i * 2] = _v2c(b >> 4);
                data[i * 2 + 1] = _v2c(b & 0xf);
            }
            data[end * 2] = 0;
            skynet_error(ctx, "error_tcp! agent:%08x len:%d id:%d crc:%x/%x pid:%x data:%s", c->agent, size, id, crc, crc1, pid, data);
            skynet_free(temp);
            return;
        }
        c->receive_id = id;
        skynet_send(ctx, 0, c->agent, g->client_tag | PTYPE_TAG_DONTCOPY, 0, temp, size);
        tm[3] = _get_time();
        _set_net_stat(&g->recv_stat, size, tm);
    } else {
        _process_predata(g, c, temp, size);
        skynet_free(temp);
    }
}

static void
dispatch_message(struct ngate *g, struct connection *c, int sid, void * data, int sz) {
    int64_t tm[4] = {_get_time()};
	databuffer_push(&c->buffer, &g->mp, data, sz);
	for (;;) {
		int size = databuffer_readheader(&c->buffer, &g->mp, g->header_size);
		if (size < 0) {
			return;
		} else if (size > 0) {
			if (size >= 0x1000000) {
				struct skynet_context * ctx = g->ctx;
                int sid = c->sid, fd = c->fd;
                _close_socket(g, c);
                g->count_error++;
				skynet_error(ctx, "Recv socket message > 16M sid:%d fd:%d", sid, fd);
				return;
			} else {
                _forward(g, c, size, tm);
                tm[0] = _get_time();
				databuffer_reset(&c->buffer);
			}
		}
	}
}

static int
dispatch_socket_message(struct ngate *g, const struct skynet_socket_message * message, int sz) {
	struct skynet_context * ctx = g->ctx;
    switch(message->type) {
	case SKYNET_SOCKET_TYPE_DATA: {
		int fd = hashsid_lookup(&g->hash, message->id);
		if (fd >= 0) {
            struct connection *c = &g->conn[fd];
            dispatch_message(g, c, message->id, message->buffer, message->ud);
        } else {
			skynet_error(ctx, "Drop unknown connection %d message", message->id);
			qs_socket_close(ctx, g->ss_id, message->id);
			skynet_free(message->buffer);
		}
		break;
	}
    case SKYNET_SOCKET_TYPE_WARNING: {
        int fd = hashsid_lookup(&g->hash, message->id);
        skynet_error(ctx, "fd (%d) sid (%d) send buffer (%d)K", fd, message->id, message->ud);
        if (fd >= 0) {
            skynet_error(ctx, "\033[1;33mngate kick sid:%d fd:%d %s\033[0m", g->conn[fd].sid, fd, "send buffer full");
            _close_socket(g, &g->conn[fd]);
            g->count_error++;
        }
        break;
    }
    case SKYNET_SOCKET_TYPE_CONNECT: {
        if (message->id == g->listen_id) {
			// start listening
			break;
		}
		int fd = hashsid_lookup(&g->hash, message->id);
		if (fd >= 0) {
			struct connection *c = &g->conn[fd];
            skynet_log(ctx, "socket %d open %s", message->id, c->remote_name);
		} else {
			skynet_error(ctx, "Close unknown connection %d", message->id);
			qs_socket_close(ctx, g->ss_id, message->id);
		}
		break;
	}
	case SKYNET_SOCKET_TYPE_CLOSE:
	case SKYNET_SOCKET_TYPE_ERROR: {
		int fd = hashsid_remove(&g->hash, message->id);
		if (fd < 0)
            break;
        struct connection *c = &g->conn[fd];
        if (c->agent) {
            //c->sid = -1;
            if (message->id == c->sid)
                c->cache.err_id = c->cache.last_id;
            lua_State *L = g->L;
            lua_settop(L, 0);
            lua_pushstring(L, "accept");
            lua_pushstring(L, "socket_error");
            lua_pushinteger(L, message->type);
            lua_pushstring(L, "missing");
            luaseri_pack(L);
            void *msg = lua_touserdata(L, -2);
            int sz = lua_tointeger(L, -1);
            skynet_send(ctx, 0, c->agent, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, msg, sz);
            g->count_error++;
        } else {    // 未建立对应的agent
            _close_socket(g, c);
        }
        skynet_log(ctx, "socket %d close", message->id);
        break;
	}
	case SKYNET_SOCKET_TYPE_ACCEPT:
		// report accept, then it will be get a SKYNET_SOCKET_TYPE_CONNECT message
		if (g->listen_id != message->id) {
            skynet_error(ctx, "\n\033[1;31mngate accept socket %d from %d, not from %d\033[0m\n", message->ud, message->id, g->listen_id);
            qs_socket_close(ctx, g->ss_id, message->ud);
            return 0;
        }
		if (hashsid_full(&g->hash)) {
            skynet_error(ctx, "ngate err, hashsid full!");
			qs_socket_close(ctx, g->ss_id, message->ud);
		} else {
            int fd = get_free_fd(g);
            if (fd < 0) {
                skynet_error(ctx, "ngate err:get_free_fd failed!");
                qs_socket_close(ctx, g->ss_id, message->ud);
                return 0;
            }
            hashsid_insert(&g->hash, message->ud, fd);
			struct connection *c = &g->conn[fd];
            int len = sz - sizeof(struct skynet_socket_message);
			if (len >= sizeof(c->remote_name)) {
				len = sizeof(c->remote_name) - 1;
			}
			c->sid = message->ud;
            c->fd = fd;
            c->receive_id = 0;
            //c->agent = 0;   // TODO
            struct msg_cache *cache = &c->cache;
            cache->head = cache->tail = cache->buff;
            cache->count = cache->last_id = cache->err_id = 0;
			memcpy(c->remote_name, message + 1, len);
			c->remote_name[len] = '\0';
            
            if (g->socket_nodelay)
                qs_socket_nodelay(ctx, g->ss_id, message->ud); // nodelay
			qs_socket_start(ctx, g->ss_id, message->ud);
            return 0;
		}
		break;
	}
    return 0;
}

static int
_cb(struct skynet_context * ctx, void * ud, int type, int session, uint32_t source, const void * msg, size_t sz) {
	struct ngate *g = ud;
    if (g->check_mqlen_time > 0) {
        uint32_t cur_time = skynet_now();
        if (cur_time - g->last_check_time >= g->check_mqlen_time) {
            g->last_check_time = cur_time;
            int len = strtoul(skynet_command(ctx, "STAT", "mqlen"), NULL, 10);
            skynet_error(ctx, "\033[1;33mngate mqlen: %d check time:%u\033[0m", len, cur_time);
        }
    }
    switch(type) {
	case PTYPE_TEXT:
		_ctrl(g, msg, (int)sz);
		break;
	case PTYPE_CLIENT: {
        int64_t tm[4] = {_get_time()};
        if (session < 0) {
			skynet_error(ctx, "Invalid client message from %x", source);
			break;
		}
        uint32_t fd = session;
		if (fd < 0 || g->max_connection <= fd) {
            skynet_error(ctx, "Invalid client id %d from %x", fd, source);
            break;
        }
        struct connection *c = &g->conn[fd];
        if (c->sid < 0)
            break;
        uint8_t * buf = (uint8_t *)msg;
        uint32_t len = (uint32_t)(sz - 2);

        buf[0] = (uint8_t)(len >> 8);
        buf[1] = (uint8_t)len;          // 包长度
        *(uint32_t*)(buf + 2) = _adler32(buf + 6, len - 4); // 校验
        _mask_buf(buf + 2, c->cache.last_id + 1, len);      // 加密
        tm[1] = _get_time();
        int ret = _push_msg(&c->cache, msg, sz);            // 将消息加入循环队列
        tm[2] = _get_time();
        if (ret > 0) {
            if (!c->cache.err_id) {
                qs_socket_send(ctx, g->ss_id, c->sid, (void*)msg, sz);
                tm[3] = _get_time();
                _set_net_stat(&g->send_stat, sz, tm);
                return 1;   // return 1 means don't free msg
            }
            break;
        }
        skynet_log(ctx, "\033[1;33m[NGate] push message failed:%d sid:%d fd:%d\033[0m", ret, c->sid, fd);
        g->count_error++;

        lua_State *L = g->L;
        lua_settop(L, 0);
        lua_pushstring(L, "accept");
        lua_pushstring(L, "socket_close");
        lua_pushstring(L, "overflow");
        luaseri_pack(L);
        void *message = lua_touserdata(L, -2);
        int size = lua_tointeger(L, -1);
        skynet_send(g->ctx, 0, c->agent, PTYPE_RESERVED_LUA | PTYPE_TAG_DONTCOPY, 0, message, size);
        _close_socket(g, c);
        break;
	}
	case PTYPE_SOCKET:
		// recv socket message from skynet_socket
		return dispatch_socket_message(g, msg, (int)sz);
	}
	return 0;
}

static int
_start_listen(struct ngate *g, int port) {
	struct skynet_context * ctx = g->ctx;
	g->listen_id = qs_socket_listen(ctx, g->ss_id, "0.0.0.0", port, BACKLOG);
	if (g->listen_id < 0) {
		skynet_error(ctx, "\033[1;33mNgate start_listen %d failed!\033[0m", port);
		return 1;
	}
	qs_socket_start(ctx, g->ss_id, g->listen_id);
    skynet_log(ctx, "\033[1;33mNGate listen on %d, listen_id:%d\033[0m", port, g->listen_id);
	return 0;
}

int
ngate_init(struct ngate *g , struct skynet_context * ctx, char * parm) {
	if (parm == NULL) {
		skynet_error(ctx, "Invalid parm null");
		return 1;
    }
    int port, max = 0, watchdog = 0, new_ss;	
    if (sscanf(parm, "%d %d %d %d", &port, &max, &watchdog, &new_ss) < 4) {
		skynet_error(ctx, "Invalid ngate parm %s",parm);
		return 1;
	} else if (max <= 0) {
		skynet_error(ctx, "Need max connection");
		return 1;
	}
	
	g->ctx = ctx;
	g->client_tag = PTYPE_SOCKET;
	g->header_size = 2;

    if (watchdog == 0) {
        skynet_error(ctx, "Invalid watchdog %d", watchdog);
        return 1;
    }
    g->watchdog = watchdog;

	hashsid_init(&g->hash, max);
	g->conn = skynet_malloc(max * sizeof(struct connection));
	memset(g->conn, 0, max * sizeof(struct connection));
	g->max_connection = max;
    g->socket_nodelay = 1;
	int i;
	for (i = 0; i < max; i++) {
		g->conn[i].sid = -1;
	}
    g->L = luaL_newstate();
	
	skynet_callback(ctx, g, _cb);
    g->ss_id = 0;
    if (new_ss) {
        g->ss_id = new_socket_server(ctx);
        skynet_log(ctx, "\033[1;33mNGate start socket server %d success\033[0m", g->ss_id);
    }

	return _start_listen(g, port);
}


