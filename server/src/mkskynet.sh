#!/bin/sh

copy_dir() {
    if [ ! -d $1 ]; then
        echo "Sorce dir does not exist"
        return
    fi  
    if [ ! -d $2 ]; then
        mkdir -p $2
    fi  
    for file in $1/*
    do  
        name=`basename $file`
        if [ -f $file ]; then
            cp $file $2/$name -fr
        elif [ -d $file ]; then
            if [ ! -d $2/$name ]; then
                mkdir -p $2/$name
            fi  
            copy_dir $file $2/$name
        fi  
    done
}

cd skynet
make cleanall
cd 3rd/jemalloc
make relclean
cd ../..
make linux
\cp skynet ../../skynet/skynet -fr

for dir in cservice luaclib service lualib
do
    copy_dir $dir ../../skynet/$dir
done

#\cp cservice/* ../../skynet/cservice/ -fr
#\cp luaclib/* ../../skynet/luaclib/ -fr
#\cp service/* ../../skynet/service/ -fr
#\cp lualib/* ../../skynet/lualib/ -fr
echo "============================================================"
make cleanall
cd 3rd/jemalloc
make relclean

