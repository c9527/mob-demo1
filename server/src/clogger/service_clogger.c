#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>

//#include "socket_poll.h"
#include "skynet.h"
#include "skynet_timer.h"

#define MAX_EVENT 64
#define pipe_count 2

uint32_t skynet_context_handle(struct skynet_context *);
int skynet_context_newsession(struct skynet_context *);

enum log_type {
    LOG_TYPE_LOG = 21,
    LOG_TYPE_MIN = LOG_TYPE_LOG,
    LOG_TYPE_ERR,
    //LOG_TYPE_PRF,
    LOG_TYPE_MAX = LOG_TYPE_ERR,
};

struct log_type_info {
    enum log_type type_id;
    char * type_str;
};

struct log_type_info log_types[] = {
    { LOG_TYPE_LOG, "log" },
    { LOG_TYPE_ERR, "err" },
    //{ LOG_TYPE_PRF, "prf" },
};

struct clogger {
	FILE * log_handles[LOG_TYPE_MAX - LOG_TYPE_MIN + 1];
    time_t files_create_time;
    struct skynet_context *ctx;
    int event_fd;
    pthread_t pid;
    int exit;
    int recvctrl_fd[pipe_count]; // 0:标准输出 1:标准错误
    int sendctrl_fd[pipe_count];
};

struct clogger *
clogger_create(void) {
	struct clogger * inst = skynet_malloc(sizeof(*inst));
    for (int i = 0; i < LOG_TYPE_MAX - LOG_TYPE_MIN + 1; i++)
	    inst->log_handles[i] = NULL;
    inst->files_create_time = 0;
    inst->pid = 0;
    inst->exit = 0;
    for (int i = 0; i < pipe_count; i++) {
        inst->recvctrl_fd[i] = 0;
        inst->sendctrl_fd[i] = 0;
    }
	return inst;
}

static void
_log2file(FILE *handle, const void *msg, size_t sz) {
    fwrite(msg, sz , 1, handle);
    fflush(handle);
}

void
clogger_release(struct clogger * inst) {
    for (int i = 0; i < pipe_count; i++) {
        if (inst->recvctrl_fd[i]) {
            close(inst->recvctrl_fd[i]);
        }
        if (inst->sendctrl_fd[i]) {
            close(inst->sendctrl_fd[i]);
        }
    }
    if (inst->pid != 0) {
        inst->exit = 1;
        pthread_join(inst->pid, NULL);
    }
    close(inst->event_fd);
    for (int i = 0; i < LOG_TYPE_MAX - LOG_TYPE_MIN + 1; i++) {
        if (inst->log_handles[i] != NULL)
            fclose(inst->log_handles[i]);
    }
    skynet_free(inst);
}

static void
_log_skynet_message(FILE *handle, uint32_t source, const void *msg, size_t sz) {
    uint32_t starttime = skynet_starttime();
    uint32_t currenttime = skynet_now();
    time_t ti = starttime + currenttime / 100;
    static char timestr[30] = {0};
    strftime(timestr, sizeof(timestr), "%H:%M:%S", localtime(&ti));

    fprintf(handle, "[%s][:%08x]\t", timestr, source);
    _log2file(handle, msg, sz);
}

static int
_check_log_files(struct clogger * inst) {
    uint32_t starttime = skynet_starttime();
    uint32_t currenttime = skynet_now();
    time_t ti = starttime + currenttime / 100;
    
    struct tm *tm = localtime(&ti);     // 多次调用tm可能导致之前的指针失效
    tm->tm_hour = 24;
    tm->tm_min = 0;
    tm->tm_sec = 1;
    skynet_timeout(skynet_context_handle(inst->ctx), (mktime(tm) - ti) * 100, 0);

    tm = localtime(&inst->files_create_time);
    int year = tm->tm_year, yday = tm->tm_yday;
    tm = localtime(&ti);
    if (inst->files_create_time > 0 && tm->tm_year == year && tm->tm_yday == yday) {
        return 1;
    }
    inst->files_create_time = ti;

    const char dirname[] = "logs";
    if (access(dirname, F_OK) != 0 && mkdir(dirname, S_IRWXU) == -1) {
        fprintf(stderr, "Create log dir '%s' failed\n", dirname);
        return 0;
    }

    char fname[40] = {0}, timestr[30] = {0};
    strftime(timestr, sizeof(timestr), "%Y%m%d_%H%M%S", tm);
    // sprintf(timestr + strlen(timestr), "_%u0", currenttime % 100);
    
    for (int i = 0; i < LOG_TYPE_MAX - LOG_TYPE_MIN + 1; i++) {
        if (inst->log_handles[i] != NULL) {
		    fclose(inst->log_handles[i]);   // 关闭旧文件
        }
        sprintf(fname, "%s/%s_%s.log", dirname, log_types[i].type_str, timestr);

        inst->log_handles[i] = fopen(fname, "a");
        if (inst->log_handles[i] == NULL) {
            return 0;
        }
        remove(log_types[i].type_str);
        symlink(fname, log_types[i].type_str);
    }
    return 1;
}

static int
_logger(struct skynet_context * context, void *ud, int type, int session, uint32_t source, const void * msg, size_t sz) {
	struct clogger * inst = ud;
    if (type == PTYPE_RESPONSE && source == 0) {
        _check_log_files(inst);
        return 0;
    } else if (type == PTYPE_TEXT) {
        _log_skynet_message(inst->log_handles[LOG_TYPE_LOG - LOG_TYPE_MIN], source, msg, sz);
        _log_skynet_message(inst->log_handles[LOG_TYPE_ERR - LOG_TYPE_MIN], source, msg, sz);
        return 0;
    }
    
    if (type < LOG_TYPE_MIN || LOG_TYPE_MAX < type) {
        skynet_error(context, "Unkonw log type:%d from:%0x", type, source);
        type = LOG_TYPE_ERR;
    }
    
    _log2file(inst->log_handles[type - LOG_TYPE_MIN], msg, sz);
    if (type == LOG_TYPE_ERR)
        _log2file(inst->log_handles[LOG_TYPE_LOG - LOG_TYPE_MIN], msg, sz);

	return 0;
}

static void *
thread_log(void *p) {
    struct clogger * inst = p;
    const int buf_size = 1024 * 1024;
    char buf[buf_size];
    int n, i, event_n, log_type;
    struct epoll_event events[MAX_EVENT];
    uint32_t handle = skynet_context_handle(inst->ctx);
    for (;;) {
        if (inst->exit)
            break;
        event_n = epoll_wait(inst->event_fd, events, MAX_EVENT, 10); // timeout:10ms
        for (i = 0; i < event_n; i++) {
            if ((events[i].events & EPOLLIN) == 0)
                continue;
            n = read(events[i].data.fd, buf, buf_size);
            if (n <= 0)
                continue;
            log_type = events[i].data.fd == inst->recvctrl_fd[0] ? LOG_TYPE_LOG : LOG_TYPE_ERR;
            skynet_send(inst->ctx, 0, handle, log_type, 0, buf, n);  // skynet copy
        }
    }
    return NULL;
}

int
clogger_init(struct clogger * inst, struct skynet_context *ctx, const char * parm) {
    inst->ctx = ctx;

    if (!_check_log_files(inst)) {
        return 1;
    }
    int efd = epoll_create(1024);
    if (efd == -1) {
        fprintf(stderr, "Clogger create event pool failed.\n");
        return 1;
    }
    inst->event_fd = efd;
    int fd[2];
    for (int i = 0; i < pipe_count; i++) {
        if (pipe(fd)) {
            close(efd);
            fprintf(stderr, "Create logger pipe failed");
            return 1;
        }
        int flags;
        if ((flags = fcntl(fd[0], F_GETFL, 0)) < 0) {
            close(efd);
            fprintf(stderr, "Get pipe fd flags failed");
            return 1;
        }
        flags |= O_NONBLOCK;
        if (fcntl(fd[0], F_SETFL, flags) < 0) {
            close(efd);
            fprintf(stderr, "Set pipe fd flags failed");
            return 1;
        }
        struct epoll_event ev;
        ev.data.fd = fd[0];
        ev.events = EPOLLIN;    // 默认LT
        if (epoll_ctl(efd, EPOLL_CTL_ADD, fd[0], &ev) == -1) {
            fprintf(stderr, "Clogger can't add server fd[%d] to event pool.\n", i);
            close(fd[0]);
            close(fd[1]);
            close(efd);
            return 1;
        }
        inst->recvctrl_fd[i] = fd[0];
        inst->sendctrl_fd[i] = fd[1];
    }
    if (pthread_create(&inst->pid, NULL, thread_log, inst)) {
        close(efd);
        fprintf(stderr, "Create logger thread failed");
        return 1;
    }
    if (inst->sendctrl_fd[0] && dup2(inst->sendctrl_fd[0], fileno(stdout)) == -1) {
        close(efd);
        fprintf(stderr, "Dup stdout failed");
        return 1;
    }
    if (inst->sendctrl_fd[1] && dup2(inst->sendctrl_fd[1], fileno(stderr)) == -1) {
        close(efd);
        fprintf(stderr, "Dup stderr failed");
        return 1;
    }
    skynet_callback(ctx, inst, _logger);
    skynet_command(ctx, "REG", ".logger");
    return 0;
}

#define LOG_MESSAGE_SIZE 256

void 
skynet_log(struct skynet_context * context, const char *msg, ...) {
	char tmp[LOG_MESSAGE_SIZE];
	char *data = NULL;

	va_list ap;
	va_start(ap, msg);
	int len = vsnprintf(tmp, LOG_MESSAGE_SIZE, msg, ap);
	va_end(ap);
	if (len < LOG_MESSAGE_SIZE) {
		data = skynet_strdup(tmp);
	} else {
		int max_size = LOG_MESSAGE_SIZE;
		for (;;) {
			max_size *= 2;
			data = skynet_malloc(max_size);
			va_start(ap,msg);
			len = vsnprintf(data, max_size, msg, ap);
			va_end(ap);
			if (len < max_size) {
				break;
			}
			skynet_free(data);
		}
	}
    
    uint32_t starttime = skynet_starttime();
    uint32_t currenttime = skynet_now();
    time_t ti = starttime + currenttime / 100;
    static char timestr[30] = {0};
    strftime(timestr, sizeof(timestr), "%H:%M:%S", localtime(&ti));

    uint32_t source = 0;
    if (context != NULL)
        source = skynet_context_handle(context);
    fprintf(stdout, "[%s][:%08x]\t", timestr, source);
    fwrite(data, len, 1, stdout);
    fprintf(stdout, "\n");
    fflush(stdout);
    skynet_free(data);
}

