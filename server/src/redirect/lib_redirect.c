#include <lua.h>
#include <lauxlib.h>
#include <memory.h>

extern uint8_t _type_map[0x100];

static int
_set_type_map(lua_State *L) {
    luaL_argcheck(L, lua_type(L, 1)==LUA_TTABLE, 1, "need table");
    memset(_type_map, 0, sizeof(_type_map));
    lua_pushnil(L);
    while (lua_next(L, -2)) {
        int t1 = luaL_checkinteger(L, -2);
        luaL_argcheck(L, t1>0 && t1<=0xff, -2, "out of range");
        int t2 = luaL_checkinteger(L, -1);
        luaL_argcheck(L, t2>0 && t2<=0xff, -1, "out of range");
        _type_map[t1] = t2;
        lua_pop(L, 1);
    }
    return 0;
}

static int
_get_type_map(lua_State *L) {
    lua_newtable(L);
    for (int i=0; i<sizeof(_type_map); i++) {
        if (_type_map[i]) {
            lua_pushinteger(L, _type_map[i]);
            lua_rawseti(L, -2, i);
        }
    }
    return 1;
}



int
luaopen_redirect(lua_State *L) {
    luaL_checkversion(L);
    luaL_Reg l[] = { 
        { "set_type_map", _set_type_map },
        { "get_type_map", _get_type_map },
        { NULL, NULL },
    };  
    luaL_newlib(L,l);
    return 1;
}
