#include "skynet.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct RedirectInfo {
    uint32_t destination;
    int env_index;
};

uint8_t _type_map[0x100];

static int
_cb(struct skynet_context * context, void *ud, int type, int session, uint32_t source, const void * msg, size_t sz) {
	struct RedirectInfo * inst = ud;
    if (type==PTYPE_ERROR && session==0)    // target exited
        return 0;
    
    if (type==PTYPE_TEXT) {
        const char * p = msg;
        char cmd[32];
        int i = 0;
        for (; i<sz && p[i]!=' ' && i<sizeof(cmd); i++)
            cmd[i] = p[i];
        if (i>=sizeof(cmd))
            return 0;
        cmd[i] = '\0';
        if (!strcmp(cmd, "exit")) {
            skynet_command(context, "EXIT", NULL);
        }
        else if (!strcmp(cmd, "getinfo")) {
            char info[32];
            int len = sprintf(info, "0x%x %d", inst->destination, inst->env_index);
            skynet_send(context, 0, source, PTYPE_RESPONSE, session, info, len);
        }
        return 0;
    }
    
    int new_type = 0;
    if (type>0 && type<=0xff)
        new_type = _type_map[type];
    if (!new_type) {
        skynet_error(context, "unknown prototype:%d from:0x%x sission:%d", type, source, session);
        return 0;
    }
    
    new_type = (new_type + inst->env_index) | PTYPE_TAG_DONTCOPY;
    skynet_send(context, source, inst->destination, new_type, session, (void *)msg, sz);
	return 1;
}

struct RedirectInfo *
redirect_create(void) {
	struct RedirectInfo * inst = skynet_malloc(sizeof(*inst));
    inst->destination = 0;
    inst->env_index = 0;
	return inst;
}

int
redirect_init(struct RedirectInfo * inst, struct skynet_context *ctx, const char * parm) {
    skynet_callback(ctx, inst, _cb);
    char * p = (char*)parm;
    inst->env_index = strtol(p, &p, 0);
    inst->destination = strtol(p, &p, 0);
    return 0;
}

void
redirect_release(struct RedirectInfo * inst) {
	skynet_free(inst);
}
