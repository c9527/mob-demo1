extern "C"
{
#include <lua.h>
#include <lauxlib.h>
#include <skynet_malloc.h>
}

#include <stdio.h>
#include <stdint.h>
#include <string.h>

typedef unsigned char BYTE;

struct SBinStream
{
	void * buffer;
	int offset;
	int size;
};

static SBinStream& _GetStream(lua_State *L)
{
	SBinStream* stream = (SBinStream*)lua_touserdata(L, 1);
	if (!stream)
		luaL_argerror(L, 1, "need stream");
	return *stream;
}

static const BYTE* _ReadStream(lua_State *L, SBinStream& stream, int count)
{
	if (count < 0)
	{
		lua_pushfstring(L, "read size error: %d", count);
		return NULL;
	}
	int offset = stream.offset + count;
	if (stream.size < offset)
	{
		lua_pushfstring(L, "read beyound size (%d + %d > %d)", stream.offset, count, stream.size);
		return NULL;
	}
	const BYTE* p = (BYTE*)stream.buffer + stream.offset;
	stream.offset = offset;
	return p;
}

static bool _WriteStream(lua_State *L, SBinStream& stream, const void* data, int count)
{
	if (count < 0)
	{
		lua_pushfstring(L, "write size error: %d", count);
		return false;
	}
	int offset = stream.offset + count;
	if (stream.size < offset)
	{
		int size = stream.size << 1;
		while (size < offset)
		{
			size <<= 1;
			if (size <= 0)
			{
				lua_pushfstring(L, "write size %d+%d out of memory", stream.offset, count);
				return false;
			}
		}
		void* p = skynet_malloc(size);
		memcpy(p, stream.buffer, stream.size);
		skynet_free(stream.buffer);
		stream.buffer = p;
		stream.size = size;
	}
	memcpy((BYTE*)stream.buffer + stream.offset, data, count);
	stream.offset = offset;
	return true;
}

static const BYTE* _ReadStream(lua_State *L, int count)
{
	const BYTE* p = _ReadStream(L, _GetStream(L), count);
	if (!p)
		lua_error(L);
	return p;
}

static void _WriteStream(lua_State *L, const void* data, int count)
{
	bool ok = _WriteStream(L, _GetStream(L), data, count);
	if (!ok)
		lua_error(L);
}


static int lCreateStream(lua_State *L)
{
	int size = (int)luaL_checkinteger(L, 1);

	SBinStream* stream = (SBinStream*)lua_newuserdata(L, sizeof(SBinStream));
	stream->buffer = skynet_malloc(size);
	stream->offset = 0;
	stream->size = size;

	return 1;
}

static int lBindStream(lua_State *L)
{
	SBinStream* stream = (SBinStream*)lua_newuserdata(L, sizeof(SBinStream));
	stream->buffer = lua_touserdata(L, 1);
	if (!stream->buffer)
		stream->buffer = (void*)luaL_checkstring(L, 1);
	stream->offset = 0;
	stream->size = (int)luaL_checkinteger(L, 2);

	return 1;
}

static int lGetBuffer(lua_State *L)
{
	SBinStream& stream = _GetStream(L);
	lua_pushlightuserdata(L, stream.buffer);
	lua_pushinteger(L, stream.offset);
	lua_pushinteger(L, stream.size);
	return 3;
}

static int lGetOffset(lua_State *L)
{
	SBinStream& stream = _GetStream(L);
	lua_pushinteger(L, stream.offset);
	return 1;
}

static int lSetOffset(lua_State *L)
{
	SBinStream& stream = _GetStream(L);
	int offset = (int)luaL_checkinteger(L, 2);
	if (offset < 0 || offset > stream.size)
		return luaL_argerror(L, 2, "out of range");
	stream.offset = offset;
	return 0;
}

static int lReadBool(lua_State *L)
{
	const BYTE* buf = _ReadStream(L, 1);
	lua_pushboolean(L, *buf);
	return 1;
}

static int lReadInt(lua_State *L)
{
	int size = (int)luaL_checkinteger(L, 2);

	const BYTE* buf = _ReadStream(L, size);
	lua_Integer value = 0;
	memcpy(&value, buf, size);
	if (value>>(size*8-1))
		value |= ((lua_Integer)-1) << (size*8);
	lua_pushinteger(L, value);

	return 1;
}

static int lReadUInt(lua_State *L)
{
	int size = (int)luaL_checkinteger(L, 2);
	if (size <= 0 || size > 7)	// 8字节整数不可以无符号
		return luaL_error(L, "size %d invalid", size);

	const BYTE* buf = _ReadStream(L, size);
	lua_Integer value = 0;
	memcpy(&value, buf, size);
	lua_pushinteger(L, value);

	return 1;
}

static int lReadFloat(lua_State *L)
{
	const BYTE* buf = _ReadStream(L, 4);
	lua_pushnumber(L, *(float*)buf);
	return 1;
}

static int lReadDouble(lua_State *L)
{
	const BYTE* buf = _ReadStream(L, 4);
	lua_pushnumber(L, *(double*)buf);
	return 1;
}

static int lReadStr(lua_State *L)
{
	int size = *(uint16_t*)_ReadStream(L, 2);
	const char * str = (const char *)_ReadStream(L, size);
	lua_pushlstring(L, str, size);
	return 1;
}

static int lWriteBool(lua_State *L)
{
	int value = lua_toboolean(L, 2);
	_WriteStream(L, &value, 1);
	return 0;
}

static int lWriteInt(lua_State *L)
{
	lua_Integer value = luaL_checkinteger(L, 2);
	int size = (int)luaL_checkinteger(L, 3);
	_WriteStream(L, &value, size);
	return 0;
}

static int lWriteFloat(lua_State *L)
{
	float value = (float)luaL_checknumber(L, 2);
	_WriteStream(L, &value, 4);
	return 0;
}

static int lWriteDouble(lua_State *L)
{
	double value = luaL_checknumber(L, 2);
	_WriteStream(L, &value, 8);
	return 0;
}

static int lWriteStr(lua_State *L)
{
	size_t size;
	const char * str = luaL_checklstring(L, 2, &size);
	if (size > 0xffff)
		return luaL_error(L, "str length %d out of range", size);
	_WriteStream(L, &size, 2);
	_WriteStream(L, str, (int)size);
	return 0;
}

const int64_t TV_BYTE	= *(int64_t*)"byte\0\0\0\0";
const int64_t TV_INT16	= *(int64_t*)"int16\0\0\0";
const int64_t TV_UINT16	= *(int64_t*)"uint16\0\0";
const int64_t TV_INT32	= *(int64_t*)"int32\0\0\0";
const int64_t TV_UINT32	= *(int64_t*)"uint32\0\0";
const int64_t TV_INT64	= *(int64_t*)"int64\0\0\0";
const int64_t TV_SINGLE	= *(int64_t*)"single\0\0";
const int64_t TV_DOUBLE	= *(int64_t*)"double\0\0";
const int64_t TV_BOOLEAN= *(int64_t*)"boolean\0";
const int64_t TV_STRING	= *(int64_t*)"string\0\0";
const int64_t TV_JSON	= *(int64_t*)"json\0\0\0\0";

static bool _ErrorPathConcat(lua_State *L, const char * format, ...)
{
	va_list args;
	va_start(args, format);
	lua_pushvfstring(L, format, args);
	va_end(args);
	lua_insert(L, lua_gettop(L) - 1);
	lua_concat(L, 2);
	return false;
}

static bool __ReadStream(lua_State *L, SBinStream& stream, int size, const BYTE* &buf)
{
	buf = _ReadStream(L, stream, size);
	if (buf)
		return true;
	return _ErrorPathConcat(L, " read error: ");
}

static bool __WriteStream(lua_State *L, SBinStream& stream, const void* data, int count)
{
	if (_WriteStream(L, stream, data, count))
		return true;
	return _ErrorPathConcat(L, " write error: ");
}

static int64_t _GetTypeValue(lua_State *L, int idx)
{
	if (!lua_isstring(L, idx))
		return 0;
	size_t len;
	const char * ts = lua_tolstring(L, idx, &len);
	int64_t tv = 0;
	if (len > sizeof(tv))
		len = sizeof(tv);
	memcpy(&tv, ts, len);
	return tv;
}

static bool _CheckIntType(int64_t tv, int &size, bool &sign)
{
	if (tv == TV_BYTE)
		size = 1, sign = false;
	else if (tv == TV_INT16)
		size = 2, sign = true;
	else if (tv == TV_UINT16)
		size = 2, sign = false;
	else if (tv == TV_INT32)
		size = 4, sign = true;
	else if (tv == TV_UINT32)
		size = 4, sign = false;
	else if (tv == TV_INT64)
		size = 8, sign = true;
	else
		return false;
	return true;
}

static bool _ReadProtoData(lua_State *L, SBinStream& stream, int64_t tv)
{
	const BYTE* buf;
	int size;
	bool sign;

	if (_CheckIntType(tv, size, sign))
	{
		if (!__ReadStream(L, stream, size, buf))
			return false;
		lua_Integer value = 0;
		memcpy(&value, buf, size);
		if (sign && value>>(size*8-1))
			value |= ((lua_Integer)-1) << (size*8);
		lua_pushinteger(L, value);
	}
	else if (tv == TV_SINGLE)
	{
		if (!__ReadStream(L, stream, 4, buf))
			return false;
		lua_pushnumber(L, *(float*)buf);
	}
	else if (tv == TV_DOUBLE)
	{
		if (!__ReadStream(L, stream, 8, buf))
			return false;
		lua_pushnumber(L, *(double*)buf);
	}
	else if (tv == TV_BOOLEAN)
	{
		if (!__ReadStream(L, stream, 1, buf))
			return false;
		lua_pushboolean(L, *buf);
	}
	else if (tv == TV_STRING || tv == TV_JSON)
	{
		if (!__ReadStream(L, stream, 2, buf))
			return false;
		size = *(uint16_t*)buf;
		if (!__ReadStream(L, stream, size, buf))
			return false;
		lua_pushlstring(L, (const char *)buf, size);
		if (tv == TV_JSON)
		{
			lua_pushvalue(L, 3);	// cjson.decode
			lua_insert(L, lua_gettop(L) - 1);
			if (lua_pcall(L, 1, 1, 0) != LUA_OK)
				return _ErrorPathConcat(L, " json error: ");
		}
	}
	else
	{
		lua_pushfstring(L, " read error: type name - %s", &tv);
		return false;
	}
	return true;
}

static bool _ReadProto(lua_State *L, SBinStream& stream)
{
	int top = lua_gettop(L);
	int64_t tv = _GetTypeValue(L, top);
	if (tv)
	{
		lua_settop(L, top - 1);
		return _ReadProtoData(L, stream, tv);
	}

	if (!lua_istable(L, top))
	{
		lua_pushfstring(L, " read error: def type - %s", luaL_typename(L, top));
		return false;
	}

	int at = lua_getfield(L, top, "array");
	if (at != LUA_TNIL)
	{
		if (!_ReadProtoData(L, stream, TV_INT16))
			return _ErrorPathConcat(L, "#");
		int count = (int)lua_tointeger(L, -1);
		lua_createtable(L, count, 0);
		lua_insert(L, top);
		for (int i = 1; i <= count; i++)
		{
			lua_pushvalue(L, top + 2);
			if (!_ReadProto(L, stream))
				return _ErrorPathConcat(L, "[%d]", i);
			lua_seti(L, top, i);
		}
		lua_settop(L, top);
		return true;
	}

	{
		lua_settop(L, top);
		lua_Integer count = luaL_len(L, top);
		lua_createtable(L, 0, count);
		for (lua_Integer i = 1; i <= count; i++)
		{
			int ft = lua_geti(L, top, i);
			if (ft != LUA_TTABLE)
			{
				lua_pushfstring(L, " read error: field type - %s", lua_typename(L, ft));
				return false;
			}
			lua_geti(L, top + 2, 1);
			lua_geti(L, top + 2, 2);
			if (!_ReadProto(L, stream))
				return _ErrorPathConcat(L, ".%s", lua_tostring(L, top + 3));
			lua_settable(L, top + 1);
			lua_settop(L, top + 1);
		}
		lua_remove(L, top);
		return true;
	}
}

static int lReadProto(lua_State *L)
{
	SBinStream& stream = _GetStream(L);
	luaL_checkany(L, 2);	// type def
	luaL_checkany(L, 3);	// cjson.decode
	lua_pushvalue(L, 2);
	bool ok = _ReadProto(L, stream);
	lua_pushboolean(L, ok);
	lua_insert(L, lua_gettop(L) - 1);
	return 2;
}

static bool _CheckDataType(lua_State *L, int type, const void * ts, int idx = -1)
{
	if (lua_type(L, idx)==type)
		return true;
	if (type == LUA_TNUMBER && lua_isnumber(L, idx))
		return true;
	if (type == LUA_TSTRING && lua_isstring(L, idx))
		return true;
	lua_pushfstring(L, " write error: need '%s', got '%s'!", ts, luaL_typename(L, idx));
	return false;
}

static bool _WriteProtoData(lua_State *L, SBinStream& stream, int64_t tv)
{
	int size;
	bool sign;

	if (_CheckIntType(tv, size, sign))
	{
		if (!_CheckDataType(L, LUA_TNUMBER, &tv))
			return false;
		lua_Integer value = lua_tointeger(L, -1);
		if (!__WriteStream(L, stream, &value, size))
			return false;
	}
	else if (tv == TV_SINGLE)
	{
		if (!_CheckDataType(L, LUA_TNUMBER, &tv))
			return false;
		float value = (float)lua_tonumber(L, -1);
		if (!__WriteStream(L, stream, &value, sizeof(value)))
			return false;
	}
	else if (tv == TV_DOUBLE)
	{
		if (!_CheckDataType(L, LUA_TNUMBER, &tv))
			return false;
		double value = lua_tonumber(L, -1);
		if (!__WriteStream(L, stream, &value, sizeof(value)))
			return false;
	}
	else if (tv == TV_BOOLEAN)
	{
		int value = lua_toboolean(L, -1);
		if (!__WriteStream(L, stream, &value, 1))
			return false;
	}
	else if (tv == TV_STRING || tv == TV_JSON)
	{
		if (tv == TV_JSON)
		{
			lua_pushvalue(L, 4);	// cjson.encode
			lua_insert(L, lua_gettop(L) - 1);
			if (lua_pcall(L, 1, 1, 0) != LUA_OK)
				return _ErrorPathConcat(L, " json error: ");
		}
		if (!_CheckDataType(L, LUA_TSTRING, &tv))
			return false;
		size_t len;
		const char * str = lua_tolstring(L, -1, &len);
		if (len > 0xffff)
		{
			lua_pushfstring(L, "str length %d out of range", len);
			return false;
		}
		_WriteStream(L, &len, 2);
		_WriteStream(L, str, (int)len);
	}
	else
	{
		lua_pushfstring(L, " write error: type name - %s", &tv);
		return false;
	}
	return true;
}

static bool _WriteProto(lua_State *L, SBinStream& stream)
{
	int base = lua_gettop(L) - 2;
	int64_t tv = _GetTypeValue(L, base + 1);
	if (tv)
	{
		if (!_WriteProtoData(L, stream, tv))
			return false;
		lua_settop(L, base);
		return true;
	}

	if (!lua_istable(L, base + 1))
	{
		lua_pushfstring(L, " write error: def type - %s", luaL_typename(L, base + 1));
		return false;
	}

	int at = lua_getfield(L, base + 1, "array");
	if (at != LUA_TNIL)
	{
		if (!_CheckDataType(L, LUA_TTABLE, "array", base + 2))
			return false;
		lua_Integer count = luaL_len(L, base + 2);
		if (!__WriteStream(L, stream, &count, 2))
			return _ErrorPathConcat(L, "#");
		for (int i = 1; i <= count; i++)
		{
			lua_pushvalue(L, -1);
			lua_geti(L, base + 2, i);
			if (!_WriteProto(L, stream))
				return _ErrorPathConcat(L, "[%d]", i);
		}
		lua_settop(L, base);
		return true;
	}

	{
		if (!_CheckDataType(L, LUA_TTABLE, "table", base + 2))
			return false;
		lua_Integer count = luaL_len(L, base + 1);
		for (lua_Integer i = 1; i <= count; i++)
		{
			lua_settop(L, base + 2);
			int ft = lua_geti(L, base + 1, i);
			if (ft != LUA_TTABLE)
			{
				lua_pushfstring(L, " write error: field type - %s", lua_typename(L, ft));
				return false;
			}
			lua_geti(L, base + 3, 2);	// 字段类型
			lua_geti(L, base + 3, 1);	// 字段名
			if (lua_gettable(L, base + 2) == LUA_TNIL)	// 无此字段
			{
				lua_pop(L, 1);
				lua_geti(L, base + 3, 3);	// 默认值
			}

			if (!_WriteProto(L, stream))
			{
				lua_geti(L, base + 3, 1);
				lua_pushvalue(L, -2);
				return _ErrorPathConcat(L, ".%s", lua_tostring(L, -2));
			}
		}
		lua_settop(L, base);
		return true;
	}
}

static int lWriteProto(lua_State *L)
{
	SBinStream& stream = _GetStream(L);
	luaL_checkany(L, 2);	// data
	luaL_checkany(L, 3);	// type def
	luaL_checkany(L, 4);	// cjson.encode
	lua_pushvalue(L, 3);
	lua_pushvalue(L, 2);
	if (_WriteProto(L, stream))
		return 0;
	return 1;
}


extern "C" int luaopen_binstream(lua_State *L) {
	luaL_Reg l[] = {
		{ "create", lCreateStream },
		{ "bind", lBindStream },
		{ "buffer", lGetBuffer },
		{ "getoff", lGetOffset },
		{ "setoff", lSetOffset },
		{ "readbool", lReadBool },
		{ "readint", lReadInt },
		{ "readuint", lReadUInt },
		{ "readfloat", lReadFloat },
		{ "readdouble", lReadDouble },
		{ "readstr", lReadStr },
		{ "writebool", lWriteBool },
		{ "writeint", lWriteInt },
		{ "writefloat", lWriteFloat },
		{ "writedouble", lWriteDouble },
		{ "writestr", lWriteStr },
		{ "readproto", lReadProto },
		{ "writeproto", lWriteProto },
		{ NULL, NULL },
	};
	luaL_checkversion(L);
	luaL_newlib(L, l);

	return 1;
}
