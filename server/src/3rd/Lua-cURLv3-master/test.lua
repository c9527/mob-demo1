local curl = require "lcurl"
local work = curl.easy()
if not work then
    print("create failed!")
else
    work:setopt_url("http://172.16.233.94:8002/index.html")
    work:setopt_httpheader({
        "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language:zh-CN,zh;q=0.8,en;q=0.6",
        "Cache-Control:max-age=0",
        "Connection:keep-alive",
        "Host:172.16.233.94:8001",
        "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36",
    })
    work:perform()
    work:close()
end

