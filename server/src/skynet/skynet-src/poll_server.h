#ifndef skynet_poll_server_h
#define skynet_poll_server_h

#include <stdint.h>
#include <stdbool.h>

struct poll_message {
	int id;
	int fd;
	int64_t ud;
	int flag;
};

#define POLLFLAG_READ   0x001
#define POLLFLAG_WRITE  0x004
#define POLLFLAG_ERROR  0x008

struct poll_server;

struct poll_server * poll_server_create(int max_count);
void poll_server_exit(struct poll_server * ps);
int poll_server_add_lite(struct poll_server * ps, int fd, int handle, int msg_etype, int msg_rtype, int msg_wtype);
int poll_server_add_full(struct poll_server * ps, int fd, int handle, int msg_type, int64_t ud, bool _write);
bool poll_server_modify(struct poll_server * ps, int id, bool _read, bool _write);
int poll_server_del(struct poll_server * ps, int id);
int poll_server_resume_read(struct poll_server * ps, int id);
int poll_server_resume_write(struct poll_server * ps, int id);

#endif
