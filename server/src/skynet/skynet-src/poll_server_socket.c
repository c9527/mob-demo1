#include "skynet.h"
#include "skynet_server.h"
#include "poll_server.h"

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>


static struct poll_server * PS = NULL;


static void
_nonblocking(int fd) {
	int flag = fcntl(fd, F_GETFL, 0);
	if (flag > 0)
	    fcntl(fd, F_SETFL, flag | O_NONBLOCK);
}


int
pss_connect_fd(const char * addr, int port) {
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) {
        perror("create connect socket");
        return -1;
    }

    struct sockaddr_in saddr;
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = inet_addr(addr);
    
    int r = connect(fd, (struct sockaddr *)&saddr, sizeof(saddr));
    if (r && errno != EINPROGRESS)
    {
        perror("connect error");
        return -1;
    }
    
    return fd;
}

int
pss_listen(const char * addr, int port, int backlog, int handle, int msg_type) {
    int listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(listen_fd < 0) {
        perror("create listen socket");
        return -1;
    }
    
    struct sockaddr_in saddr;
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    if (addr && addr[0])
        saddr.sin_addr.s_addr = inet_addr(addr);
    else
        saddr.sin_addr.s_addr = INADDR_ANY;
    
    if(bind(listen_fd, (struct sockaddr *)&saddr, sizeof(saddr))<0) {
        perror("bind listen socket error");
        return -1;
    }
    if(listen(listen_fd, backlog)<0) {
        perror("start listen error");
        return -1;
    }
    _nonblocking(listen_fd);
	return poll_server_add_full(PS, listen_fd, handle, msg_type, 0, false);
}

int
pss_accept_fd(int id, char * addr_str) {
    int fd = poll_server_resume_read(PS, id);
	struct sockaddr_in saddr;
	socklen_t len = sizeof(saddr);
	int client_fd = accept(fd, (struct sockaddr*)&saddr, &len);
	if (client_fd < 0) {
    	if (errno!=EAGAIN && errno!=EWOULDBLOCK)
    	    perror("accept error");
    	return -1;
    }
    if (addr_str) {
        if (inet_ntop(AF_INET, &saddr.sin_addr, addr_str, INET_ADDRSTRLEN))
            sprintf(addr_str + strlen(addr_str), ":%d", ntohs(saddr.sin_port));
        else
            sprintf(addr_str, "addr_error");
    }
    _nonblocking(client_fd);
    return client_fd;
}

ssize_t
pss_read(int id, void * buf, size_t sz) {
    int fd = poll_server_resume_read(PS, id);
    ssize_t len = read(fd, buf, sz);
    if (len > 0)
        return len;
    if (len == 0)
        return -1;
	if (errno==EAGAIN || errno==EWOULDBLOCK)
	    return 0;
	return -2;
}

ssize_t
pss_write(int id, const void * buf, size_t sz) {
    int fd = poll_server_resume_write(PS, id);
    return write(fd, buf, sz);
}

void
pss_close(int id) {
    int fd = poll_server_del(PS, id);
    if (fd >= 0)
        close(fd);
}

void
pss_init(int max_count) {
	assert(!PS);
	if (!PS)
        PS = poll_server_create(max_count);
}

void
pss_release() {
	assert(PS);
	if (PS) {
    	poll_server_exit(PS);
    	PS = NULL;
    }
}

struct poll_server *
pss_poll_server() {
    return PS;
}
