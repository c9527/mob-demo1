#ifndef skynet_poll_server_socket_h
#define skynet_poll_server_socket_h

#include <sys/types.h>

struct poll_server;

// 连接流程
//  1、调用pss_connect_fd，开始建立连接，返回fd
//  2、poll_server_add_lite/poll_server_add_full，开始监听fd
//  3、第一次收到可写/可读（对方发送数据）事件后，此fd可用
int pss_connect_fd(const char * addr, int port);

// 监听流程
//  1、调用pss_listen，开始监听端口，返回id
//  2、收到msg_type，循环调用pss_accept_fd建立fd，直到pss_accept_fd返回-1
//  3、对每个fd调用poll_server_add_lite/poll_server_add_full，开始监听fd
int pss_listen(const char * addr, int port, int backlog, int handle, int msg_type);
int pss_accept_fd(int id, char * addr_str); // addr_str可为NULL，否则至少需包含22字节空间


// 返回值：
//  >=0、接收长度
//  -1、关闭
//  -2、失败
ssize_t pss_read(int id, void * buf, size_t sz);

// 返回值：
//  >=0、发送长度
//  <0、失败
ssize_t pss_write(int id, const void * buf, size_t sz);

void pss_close(int id);


void pss_init(int max_count);
void pss_release();
struct poll_server * pss_poll_server();


#endif
