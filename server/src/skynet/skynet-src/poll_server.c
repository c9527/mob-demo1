#include "skynet.h"
#include "skynet_mq.h"
#include "skynet_server.h"

#include "poll_server.h"
#include "atomic.h"

#include <netdb.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>

struct poll_data {
	int id;
	int fd;
	int64_t ud;
	int msg_handle;
	int msg_rtype;
	int msg_wtype;
	int msg_etype;
	bool msg_full;
	bool read_sent;
	bool write_sent;
};

struct poll_server {
	int event_fd;
	int alloc_id;
	int max_count;
	struct epoll_event * evs;
	struct poll_data * pds;
	pthread_t pid;
};

static void *
malloc_zero(int size) {
    void * p = skynet_malloc(size);
    memset(p, 0, size);
    return p;
}

static void
send_message(struct poll_data * p, int type, int flag) {
	struct poll_message * pm = NULL;
	if (p->msg_full) {
    	pm = skynet_malloc(sizeof(*pm));
    	pm->id = p->id;
    	pm->fd = p->fd;
    	pm->ud = p->ud;
    	pm->flag = flag;
    }

	struct skynet_message message;
	message.source = 0;
	message.session = 0;
	message.data = pm;
	message.sz = (pm ? sizeof(*pm) : 0) | ((size_t)type << MESSAGE_TYPE_SHIFT);

	if (skynet_context_push(p->msg_handle, &message)) {
	    if (pm)
		    skynet_free(pm);
	}
}

static bool
do_poll(struct poll_server * ps) {
	int n = epoll_wait(ps->event_fd, ps->evs, ps->max_count, -1);
	if (n<0) {
	    if (errno==EINTR)
	        return true;
	    perror("epoll_wait error");
	    return false;
	}
	int i;
	for (i=0;i<n;i++) {
	    int flag = ps->evs[i].events;
		struct poll_data * p = ps->evs[i].data.ptr;
		if (flag & EPOLLERR) {
		    if (p->msg_etype)
		        send_message(p, p->msg_etype, flag);
		    continue;
		}
		int t1 = p->msg_rtype;
		if (flag & EPOLLIN && t1 && !p->read_sent)
		    p->read_sent = true;
		else
		    t1 = 0;
		int t2 = p->msg_wtype;
		if (flag & EPOLLOUT && t2 && !p->write_sent)
		    p->write_sent = true;
		else
		    t2 = 0;
		if (p->msg_full) {
		    if (t1 || t2)
		        send_message(p, p->msg_etype, flag);
		} else {
		    if (t1)
		        send_message(p, t1, flag);
		    if (t2 && t2!=t1)
		        send_message(p, t2, flag);
		}
	}
	return true;
}

static void *
thread_loop(void *p) {
	struct poll_server * ps = p;
    while (do_poll(ps));
	return NULL;
}

static int
poll_server_add(struct poll_server * ps, int fd, int handle, int msg_etype, int msg_rtype, int msg_wtype, bool msg_full, bool _read, bool _write, int64_t ud) {
    struct poll_data * p = NULL;
	int i;
	for (i=0;i<ps->max_count;i++) {
		int id = ATOM_INC(&ps->alloc_id);
		if (id <= 0) {
		    ATOM_CAS(&ps->alloc_id, id, 0);
		    continue;
		}
		p = &ps->pds[id%ps->max_count];
		if (!p->id && ATOM_CAS(&p->id, 0, id))
		    break;
		p = NULL;
	}
	if (!p)
	    return 0;
	p->fd = fd;
	p->ud = ud;
	p->msg_handle = handle;
	p->msg_rtype = msg_rtype;
	p->msg_wtype = msg_wtype;
	p->msg_etype = msg_etype;
	p->msg_full = msg_full;
	struct epoll_event ev;
	ev.events = EPOLLET | (_read && msg_rtype ? EPOLLIN : 0) | (_write && msg_wtype ? EPOLLOUT : 0);
	ev.data.ptr = p;
	epoll_ctl(ps->event_fd, EPOLL_CTL_ADD, fd, &ev);
	return p->id;
}

struct poll_server * 
poll_server_create(int max_count) {
	int efd = epoll_create(1024);
	if (efd<0) {
		perror("poll-server create event pool failed");
		return NULL;
	}

	struct poll_server * ps = malloc_zero(sizeof(*ps));
	ps->event_fd = efd;
	ps->max_count = max_count;
	ps->pds = malloc_zero(sizeof(*ps->pds) * max_count);
	ps->evs = malloc_zero(sizeof(*ps->evs) * max_count);
	
	if (pthread_create(&ps->pid, NULL, thread_loop, ps)) {
		perror("create thread failed");
		return NULL;
	}

	return ps;
}

void
poll_server_exit(struct poll_server * ps) {
    close(ps->event_fd);
    pthread_join(ps->pid, NULL);
    skynet_free(ps);
}

int
poll_server_add_lite(struct poll_server * ps, int fd, int handle, int msg_etype, int msg_rtype, int msg_wtype) {
    return poll_server_add(ps, fd, handle, msg_etype, msg_rtype, msg_wtype, false, true, true, 0);
}

int
poll_server_add_full(struct poll_server * ps, int fd, int handle, int msg_type, int64_t ud, bool _write) {
    return poll_server_add(ps, fd, handle, msg_type, msg_type, msg_type, true, true, _write, ud);
}

bool
poll_server_modify(struct poll_server * ps, int id, bool _read, bool _write) {
    struct poll_data * p = &ps->pds[id%ps->max_count];
    if (p->id != id)
        return false;
	struct epoll_event ev;
	ev.events = EPOLLET | ((_read && p->msg_rtype) ? EPOLLIN : 0) | ((_write && p->msg_wtype) ? EPOLLOUT : 0);
	ev.data.ptr = p;
	epoll_ctl(ps->event_fd, EPOLL_CTL_MOD, p->fd, &ev);
	return true;
}

int
poll_server_del(struct poll_server * ps, int id) {
    struct poll_data * p = &ps->pds[id%ps->max_count];
    if (p->id != id)
        return -1;
	epoll_ctl(ps->event_fd, EPOLL_CTL_DEL, p->fd, NULL);
	p->id = 0;
	return p->fd;
}

int
poll_server_resume_read(struct poll_server * ps, int id) {
    struct poll_data * p = &ps->pds[id%ps->max_count];
    if (p->id != id)
        return -1;
	p->read_sent = false;
	return p->fd;
}

int
poll_server_resume_write(struct poll_server * ps, int id) {
    struct poll_data * p = &ps->pds[id%ps->max_count];
    if (p->id != id)
        return -1;
	p->write_sent = false;
	return p->fd;
}
