#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <lua.h>
#include <execinfo.h>
#include <pthread.h>

#include "malloc_hook.h"
#include "skynet.h"
#include "atomic.h"

static size_t _used_memory[3] = {0};
typedef struct _mem_data {
	void* addr;
	char* funcname;
	ssize_t size;
	ssize_t block;
	ssize_t count;
	int type;
	int conflict;
} mem_data;

#define SLOT_SIZE 0x10000
//#define PREFIX_SIZE 24
#define PREFIX_SIZE 8

static int mem_hook_flag = 0;
static mem_data mem_stats[SLOT_SIZE];


#ifndef NOUSE_JEMALLOC

#include "jemalloc.h"

// for skynet_lalloc use
#define raw_realloc je_realloc
#define raw_free je_free

inline static mem_data *
get_mem_data(void* addr) {
	int h = (int)(size_t)addr & (SLOT_SIZE - 1);
	mem_data * data = &mem_stats[h];
	if (data->addr == addr)
	    return data;
	if(!data->addr && ATOM_CAS(&data->addr, 0, addr))
		return data;
    data->conflict = 1;
	return NULL;
}


static pthread_mutex_t _mutex;
static int _mutex_inited = 0;
static int _mem_recoding = 0;

inline static void * get_addr(int stack) {
    void* ary[10];
    pthread_mutex_lock(&_mutex);
    if (_mem_recoding) {
        pthread_mutex_unlock(&_mutex);
        return NULL;
    }
    _mem_recoding = 1;
    int n = backtrace(ary, 10);
    _mem_recoding = 0;
    pthread_mutex_unlock(&_mutex);
    if (n > stack)
        n = stack;
	return ary[n-1];
}

inline static void * get_funcname(void * addr) {
    pthread_mutex_lock(&_mutex);
    _mem_recoding = 1;
    char ** strs = backtrace_symbols(&addr, 1);
    _mem_recoding = 0;
    pthread_mutex_unlock(&_mutex);
    if (!strs)
        return "NULL";
	return strs[0];
}

inline static void
_on_alloc(void* ptr, size_t size, int type, int stack) {
	if(!ptr) {
    	fprintf(stderr, "xmalloc: Out of memory trying to allocate %zu bytes\n", size);
    	fflush(stderr);
    	abort();
	    return;
	}
	
	size = je_malloc_usable_size(ptr);
	int flag = mem_hook_flag;
	void* addr = NULL;
	if (flag == 1)
	    addr = (void*)(size_t)skynet_current_handle();
	else if (flag >= 2)
	    addr = get_addr(stack);
	int64_t * p = ptr + size - PREFIX_SIZE;
	p[0] = (int64_t)addr;
	//p[1] = ~(int64_t)addr;
	//p[2] = size;
    
	ATOM_ADD(&_used_memory[0], size);
	ATOM_INC(&_used_memory[1]);
	ATOM_INC(&_used_memory[2]);
	
	if (!addr)
	    return;
	
	mem_data * data = get_mem_data(addr);
	if(!data)
	    return;
	
	ATOM_ADD(&data->size, size);
	ATOM_INC(&data->block);
	ATOM_INC(&data->count);
	
	if (data->type)
	    return;
	
	data->type = type;
	
	if (flag == 1)
	    data->funcname = "@handle";
	else if (flag == 2)
	    data->funcname = "@addr";
	else
	    data->funcname = get_funcname(addr);
}

inline static void
_on_free(char* ptr) {
	if(!ptr)
	    return;
	
	size_t size = je_malloc_usable_size(ptr);
	ATOM_SUB(&_used_memory[0], size);
	ATOM_DEC(&_used_memory[1]);
	
	int64_t * p = (int64_t *)(ptr + size - PREFIX_SIZE);
	void* addr = (void *)p[0];
	//if (p[1] != ~(int64_t)addr)
	//    printf("!!!error addr %p  %lx  %lx", ptr, p[0], p[1]);
	//if (p[2] != size)
	//    printf("!!!error size %p  %lx  %lx", ptr, p[2], size);
	if (!addr)
	    return;
	
	mem_data * data = get_mem_data(addr);
	if(!data)
	    return;
    
	ATOM_SUB(&data->size, size);
	ATOM_DEC(&data->block);
}

void 
memory_info_dump(void) {
	je_malloc_stats_print(0,0,0);
}

size_t 
mallctl_int64(const char* name, size_t* newval) {
	size_t v = 0;
	size_t len = sizeof(v);
	if(newval) {
		je_mallctl(name, &v, &len, newval, sizeof(size_t));
	} else {
		je_mallctl(name, &v, &len, NULL, 0);
	}
	// skynet_error(NULL, "name: %s, value: %zd\n", name, v);
	return v;
}

int 
mallctl_opt(const char* name, int* newval) {
	int v = 0;
	size_t len = sizeof(v);
	if(newval) {
		int ret = je_mallctl(name, &v, &len, newval, sizeof(int));
		if(ret == 0) {
			skynet_error(NULL, "set new value(%d) for (%s) succeed\n", *newval, name);
		} else {
			skynet_error(NULL, "set new value(%d) for (%s) failed: error -> %d\n", *newval, name, ret);
		}
	} else {
		je_mallctl(name, &v, &len, NULL, 0);
	}

	return v;
}

// hook : malloc, realloc, free, calloc

inline static void *
_do_malloc(size_t size, int type, int stack) {
	void* ptr = je_malloc(size + PREFIX_SIZE);
	_on_alloc(ptr, size, type, stack);
	return ptr;
}

void *
skynet_malloc(size_t size) {
	return _do_malloc(size, 1, 2);
}

void *
skynet_realloc(void *ptr, size_t size) {
	if (ptr == NULL) return _do_malloc(size, 2, 2);

	_on_free(ptr);
	void *newptr = raw_realloc(ptr, size+PREFIX_SIZE);
	_on_alloc(newptr, size, 2, 2);
	return newptr;
}

void
skynet_free(void *ptr) {
	if (ptr == NULL) return;
	_on_free(ptr);
	raw_free(ptr);
}

void *
skynet_calloc(size_t nmemb,size_t size) {
	void* ptr = je_calloc(nmemb + ((PREFIX_SIZE+size-1)/size), size );
	_on_alloc(ptr, size, 3, 2);
	return ptr;
}

#else

// for skynet_lalloc use
#define raw_realloc realloc
#define raw_free free

void 
memory_info_dump(void) {
	skynet_error(NULL, "No jemalloc");
}

size_t 
mallctl_int64(const char* name, size_t* newval) {
	skynet_error(NULL, "No jemalloc : mallctl_int64 %s.", name);
	return 0;
}

int 
mallctl_opt(const char* name, int* newval) {
	skynet_error(NULL, "No jemalloc : mallctl_opt %s.", name);
	return 0;
}

#endif

size_t *
malloc_used_memory(void) {
	return _used_memory;
}

void
dump_c_mem() {
	int i;
	size_t total = 0;
	skynet_error(NULL, "dump all service mem:");
	for(i=0; i<SLOT_SIZE; i++) {
		mem_data* data = &mem_stats[i];
		if(data->addr && data->block) {
			total += data->size;
			skynet_error(NULL, "0x%x -> %zdkb", data->addr, data->size >> 10);
		}
	}
	skynet_error(NULL, "+total: %zdkb",total >> 10);
}

char *
skynet_strdup(const char *str) {
	size_t sz = strlen(str);
	char * ret = _do_malloc(sz+1, 4, 2);
	memcpy(ret, str, sz+1);
	return ret;
}

void * 
skynet_lalloc(void *ptr, size_t osize, size_t nsize) {
	if (nsize == 0) {
		raw_free(ptr);
		return NULL;
	} else {
    	return raw_realloc(ptr, nsize);
	}
}

int
dump_mem_lua(lua_State *L) {
	int i;
	lua_newtable(L);
	int count = 0;
	for(i=0; i<SLOT_SIZE; i++) {
		mem_data* data = &mem_stats[i];
		if(!data->addr)
		    continue;
		
		lua_newtable(L);
		lua_pushinteger(L, (lua_Integer)data->addr);
		lua_rawseti(L, -2, 1);
		if (data->funcname) {
    		lua_pushstring(L, data->funcname);
    		lua_rawseti(L, -2, 2);
    	}
		lua_pushinteger(L, (lua_Integer)data->size);
		lua_rawseti(L, -2, 3);
		lua_pushinteger(L, (lua_Integer)data->block);
		lua_rawseti(L, -2, 4);
		lua_pushinteger(L, (lua_Integer)data->count);
		lua_rawseti(L, -2, 5);
		lua_pushinteger(L, data->type);
		lua_rawseti(L, -2, 6);
		lua_pushboolean(L, data->conflict);
		lua_rawseti(L, -2, 7);
		
		lua_rawseti(L, -2, ++count);
	}
	return 1;
}

size_t
malloc_current_memory(void) {
	uint32_t handle = skynet_current_handle();
	int i;
	for(i=0; i<SLOT_SIZE; i++) {
		mem_data* data = &mem_stats[i];
		if((uint32_t)(uint64_t)data->addr == handle && data->size != 0) {
			return (size_t) data->size;
		}
	}
	return 0;
}

void
skynet_debug_memory(const char *info) {
	// for debug use
	uint32_t handle = skynet_current_handle();
	size_t mem = malloc_current_memory();
	fprintf(stderr, "[:%08x] %s %p\n", handle, info, (void *)mem);
}

void
clean_mem_stats() {
    memset(mem_stats, 0, sizeof(mem_stats));
}

void
set_mem_hook_flag(int flag) {
    if (!_mutex_inited) {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&_mutex, &attr);
        clean_mem_stats();
        _mutex_inited = 1;
    }
    
    mem_hook_flag = flag;
}
