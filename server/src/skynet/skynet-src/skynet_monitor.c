#include "skynet.h"

#include "skynet_monitor.h"
#include "skynet_server.h"
#include "skynet.h"
#include "atomic.h"

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdio.h>

struct skynet_monitor {
	int version;
	int check_version;
	uint32_t source;
	uint32_t destination;
    long tid;
};

struct skynet_monitor * 
skynet_monitor_new() {
	struct skynet_monitor * ret = skynet_malloc(sizeof(*ret));
	memset(ret, 0, sizeof(*ret));
	return ret;
}

void 
skynet_monitor_delete(struct skynet_monitor *sm) {
	skynet_free(sm);
}

void 
skynet_monitor_trigger(struct skynet_monitor *sm, uint32_t source, uint32_t destination) {
	sm->source = source;
	sm->destination = destination;
	ATOM_INC(&sm->version);
}

void
skynet_monitor_set_tid(struct skynet_monitor *sm, long tid) {
    sm->tid = tid;
}

#define MAX_STACK_LEN 10240
#define MAX_TASK_STACK_LEN 1048576

int dump_tread_stack(void)
{
    DIR *dp = opendir("/proc/self/task/");
    if (!dp) {
        fprintf(stderr, "open dir /proc/self/task/ faield!\n");
        return -1; 
    }   
    char task_stack[MAX_TASK_STACK_LEN] = {0};
    char stack[MAX_STACK_LEN] = {0}, str[MAX_STACK_LEN] = {0};
    char filename[128] = {0};
    struct dirent *dirp;
    while ((dirp = readdir(dp)) != NULL) { 
        if (!strcmp(".", dirp->d_name) || !strcmp("..", dirp->d_name))
            continue;           
        snprintf(filename, 128, "/proc/self/task/%s/stack", dirp->d_name);
        FILE *fp = fopen(filename, "r");
        if (!fp) {
            fprintf(stderr, "open task file %s failed", filename);
            break;
        }
        memset(stack, 0, sizeof(stack));
        memset(str, 0, sizeof(str));
        fread(stack, sizeof(char), MAX_STACK_LEN, fp);
        snprintf(str, MAX_STACK_LEN, "%s\n%s\n", dirp->d_name, stack);
        if (strlen(task_stack) + strlen(str) >= MAX_TASK_STACK_LEN)
            break;
        memcpy(task_stack + strlen(task_stack), str, strlen(str));
    }                                 
    closedir(dp);                     
    fprintf(stderr, task_stack); 
    return 0;
}

void 
skynet_monitor_check(struct skynet_monitor *sm) {
	if (sm->version == sm->check_version) {
		if (sm->destination) {
			skynet_context_endless(sm->destination);
			skynet_error(NULL, "A message from [ :%08x ] to [ :%08x ] maybe in an endless loop (version = %d) thread id: %lu", sm->source , sm->destination, sm->version, (unsigned long)sm->tid);
            dump_tread_stack();
		}
	} else {
		sm->check_version = sm->version;
	}
}
