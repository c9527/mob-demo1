#include "skynet_malloc.h"
#include "skynet_server.h"
#include "skynet.h"
#include "poll_server.h"
#include "poll_server_socket.h"

#include <lua.h>
#include <lauxlib.h>

#define PTYPE_PS_SOCKET 12
#define RECV_BUF_SIZE   (64 * 1024)

static int
self_handle(lua_State *L) {
	lua_getfield(L, LUA_REGISTRYINDEX, "skynet_context");
	struct skynet_context * ctx = lua_touserdata(L, -1);
	return skynet_context_handle(ctx);
}

// id = listen(host, port, backlog)
static int
llisten(lua_State *L) {
	const char * host = luaL_checkstring(L,1);
	int port = luaL_checkinteger(L,2);
	int backlog = luaL_checkinteger(L,3);
	int id = pss_listen(host,port,backlog,self_handle(L),PTYPE_PS_SOCKET);
	if (id < 0)
		return 0;
	lua_pushinteger(L,id);
	return 1;
}

// id, addr = begin_accept(listen_id)
// 返回id时，尚不可写，要等待可写事件
static int
lbegin_accept(lua_State *L) {
    int listen_id = luaL_checkinteger(L,1);
    char tmp[32];
    int fd = pss_accept_fd(listen_id, tmp);
    if (fd < 0)
        return 0;
    int id = poll_server_add_full(pss_poll_server(), fd, self_handle(L), PTYPE_PS_SOCKET, 0, true);
	if (id < 0)
		return luaL_error(L, "socket full!");
	lua_pushinteger(L,id);
	lua_pushstring(L,tmp);
	return 2;
}

// id = begin_connect(host, port)
// 返回id时，尚不可写，要等待可写事件
static int
lbegin_connect(lua_State *L) {
	const char * host = luaL_checkstring(L,1);
	int port = luaL_checkinteger(L,2);
    int fd = pss_connect_fd(host, port);
    if (fd < 0)
        return 0;
    int id = poll_server_add_full(pss_poll_server(), fd, self_handle(L), PTYPE_PS_SOCKET, 0, true);
	if (id < 0)
		return luaL_error(L, "socket full!");
	lua_pushinteger(L,id);
	return 1;
}

// ok = enable_write(id, enable)
static int
lenable_write(lua_State *L) {
    int id = luaL_checkinteger(L,1);
    bool enable = lua_toboolean(L,2);
    bool ok = poll_server_modify(pss_poll_server(), id, true, enable);
    lua_pushboolean(L,ok);
	return 1;
}

// str, err = recv(id)
static int
lrecv(lua_State *L) {
    int id = luaL_checkinteger(L,1);
	void * buffer = lua_touserdata(L, lua_upvalueindex(1));
	int len = pss_read(id, buffer, RECV_BUF_SIZE);
	if (len < 0) {
	    return 0;
	}
	if (len >= RECV_BUF_SIZE) { // 缓冲区读满了，给自己再发消息，再读一次
    	lua_getfield(L, LUA_REGISTRYINDEX, "skynet_context");
    	struct skynet_context * ctx = lua_touserdata(L, -1);
    	int handle = skynet_context_handle(ctx);
    	struct poll_message pm;
    	pm.id = id;
    	pm.fd = -1;
    	pm.ud = 0;
    	pm.flag = POLLFLAG_READ;
    	skynet_send(ctx, 0, handle, PTYPE_PS_SOCKET, 0, &pm, sizeof(pm));
    }
	
    lua_pushlstring(L, buffer, len);
    return 1;
}

// ok, len = send(id, str)
static int
lsend(lua_State *L) {
	int id = luaL_checkinteger(L, 1);
	size_t sz = 0;
	const void * buffer = luaL_checklstring(L, 2, &sz);
    ssize_t len = pss_write(id, buffer, sz);
	lua_pushboolean(L, len==sz);
	lua_pushinteger(L, len);
	return 2;
}

// close(id)
static int
lclose(lua_State *L) {
	int id = luaL_checkinteger(L, 1);
    pss_close(id);
	return 0;
}

// id, error, read, write = unpack_msg(msg, [sz])
static int
lunpack_msg(lua_State *L) {
    const struct poll_message * msg;
    size_t sz;
    if (lua_isuserdata(L, 1)) {
        msg  = lua_touserdata(L, 1);
        sz = luaL_checkinteger(L, 2);
    } else {
        msg = (const struct poll_message *)luaL_checklstring(L, 1, &sz);
    }
    luaL_argcheck(L, sz==sizeof(*msg), 2,"message size");
	lua_pushinteger(L, msg->id);
	lua_pushboolean(L, msg->flag & POLLFLAG_ERROR);
	lua_pushboolean(L, msg->flag & POLLFLAG_READ);
	lua_pushboolean(L, msg->flag & POLLFLAG_WRITE);
	return 4;
}

// fd = abandon_fd(id)
static int
labandon_fd(lua_State *L) {
	int id = luaL_checkinteger(L, 1);
	int fd = poll_server_del(pss_poll_server(), id);
	if (fd < 0)
	    return 0;
    lua_pushinteger(L, fd);
	return 1;
}

// id = bind_fd(fd, [write])
static int
lbind_fd(lua_State *L) {
    int fd = luaL_checkinteger(L,1);
    bool _write = lua_toboolean(L,2);
    int id = poll_server_add_full(pss_poll_server(), fd, self_handle(L), PTYPE_PS_SOCKET, 0, _write);
	if (id < 0)
		return luaL_error(L, "socket full!");
    lua_pushinteger(L, id);
	return 1;
}

// init(max_count)
static int
linit(lua_State *L) {
	int max_count = luaL_checkinteger(L, 1);
    pss_init(max_count);
	return 0;
}

// release()
static int
lrelease(lua_State *L) {
    pss_release();
	return 0;
}

int
luaopen_pss_driver(lua_State *L) {
	luaL_checkversion(L);
	
	luaL_Reg l[] = {
		{ "listen", llisten },
		{ "begin_accept", lbegin_accept },
		{ "begin_connect", lbegin_connect },
		{ "enable_write", lenable_write },
		{ "recv", lrecv },
		{ "send", lsend },
		{ "close", lclose },
		{ "unpack_msg", lunpack_msg },
		{ "abandon_fd", labandon_fd },
		{ "bind_fd", lbind_fd },
		{ "init", linit },
		{ "release", lrelease },
		{ NULL, NULL },
	};
	luaL_newlib(L,l);
	
	luaL_Reg l2[] = {
		{ "recv", lrecv },
		{ NULL, NULL },
	};
	void * buff = skynet_malloc(RECV_BUF_SIZE);
	lua_pushlightuserdata(L, buff);
	luaL_setfuncs(L,l2,1);

	return 1;
}
