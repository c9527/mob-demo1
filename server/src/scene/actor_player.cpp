#include "stdafx.h"
#include "scene_logic.h"
#include "scene_data.h"
#include "actor_player.h"

#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <stdarg.h>

CActorPlayer::CActorPlayer(CSceneLogic& logic, SSceneActor& actor) :
	mLogic(logic), mActor(actor), mBuffSize(0), mSendId(0), mIllegal(false), mOverMove(0.f), mOverMoveCnt(0), mLastMoveDt(0.f), mOverMoveTime(0.f), mNextFireDt(0.f), mEnterTime(logic.mNow)
{
	ZeroStruct(mAddr);
	mRandId = logic.Rand();
	if (!mRandId)	// 不要0
		mRandId = logic.Rand();
	mRandId += actor.id - 1 - mRandId % (uint32_t)CSceneLogic::MAX_ACTOR_COUNT;
	mWeaponVer = 0;
	mBulletCount = 0;
	mDelayPunishTime = 0;
	mGroundHeight = actor.pos[1];
}

CActorPlayer::~CActorPlayer()
{
}

void CActorPlayer::HandleProto(const sockaddr_in & addr, uint32_t rid, uint32_t pid, const void * buf, size_t sz)
{
	if (rid != mRandId)
		return;
	mAddr = addr;
	if (!mLogic.CanPlay())
		return;
	switch (pid)
	{
	case pid_fight_move:
		Tos_fight_move((const uint8_t *)buf, sz);
		break;
	case pid_fight_fire:
		Tos_fight_fire((const uint8_t *)buf, sz);
		break;
	default:
		break;
	}
}

void CActorPlayer::Tos_fight_move(const uint8_t * buf, size_t sz)
{
	const uint8_t* posBuf = buf;
	uint16_t posCount = *(uint16_t*)buf;
	buf += sizeof(uint16_t);
	if (posCount > 6)
	{
		return;
	}
	short pos[6];
	for (int i = 0; i < posCount; ++i)
	{
		pos[i] = *(short*)buf;
		buf += sizeof(short);
	}
	size_t posSize = posCount * sizeof(short) + sizeof(uint16_t);
	
	const tos_fight_move & tos = *(const tos_fight_move*)buf;
	if (tos.time <= mSendId)
	{
		return;
	}
	if (tos.timeStamp > mLogic.mNow - mEnterTime)
	{
		return;
	}
	mSendId = tos.time;
	float val[POS_SIZE];
	val[0] = pos[0] * 0.01f;
	val[1] = pos[1] * 0.01f;
	val[2] = pos[2] * 0.01f;
	val[3] = tos.anglePitch;
	val[4] = tos.angleY * 2;
	val[5] = tos.moveState;
	
	if (mActor.state != SSceneActor::StateAlive)
		return;
	const SActorProp& prop = mLogic.GetProp(mActor);
	if ((prop.LimitState & SSceneActor::LimitStateMove) > 0)
		return;

	if (g_CheckMaxHeight && val[1] > mLogic.mData.mYMax)
	{
		mIllegal = true;
		return;
	}
	else
	{
		mIllegal = false;
	}
	// 检测发包间隔
	float interval = mLastMoveDt > 0 ? tos.timeStamp - mLastMoveDt : 0.1f;
	mOverMoveTime += 0.1f - interval;
	if (mOverMoveTime < 0)
	{
		mOverMoveTime = 0.f;
	}
	if (mOverMoveTime > 0.1f)
	{
		mOverMoveTime -= 0.1f - interval;
		return;
	}
	mLastMoveDt = tos.timeStamp;

	bool ok = true;
	float x = val[0] - mActor.pos[0];
	float y = val[1] - mActor.pos[1];
	float z = val[2] - mActor.pos[2];
	if (x || y || z)
	{
		mLogic.UpdateActorActTime(mActor);
		float ml = mActor.move_speed * interval;
		float dls = x*x + z*z;
		float addMove = sqrt(dls) - ml;
		mOverMove += addMove;
		float maxMl = mActor.move_speed * 1.f;
		if (mOverMove > maxMl)
		{
			if (addMove > 0)
			{
				++mOverMoveCnt;
			}
			else
			{
				--mOverMoveCnt;
			}
		}
		else if (mOverMove < 0.f)
		{
			mOverMove = 0.f;
			mOverMoveCnt = 0;
		}
		if (mOverMoveCnt > 10 && mActor.move_speed > 0.f && g_AllowMoveDelay > 0)
		{
			mOverMoveCnt = 10;
			mOverMove = maxMl * 0.8;
			ok = false;
		}
	}

	if (ok)
	{
		float ny = val[1];
		if (ny < mGroundHeight)
			mGroundHeight = ny;
		float h = mLogic.mData.GetHeight(val[0], val[2]);
		if (h > mGroundHeight)
			mGroundHeight = h;
		if (g_CheckGroundHeight && (ny < mGroundHeight - .1f || ny > mGroundHeight + 1.5f))
			ok = false;
	}

	if (ok)
	{
		poscpy(mActor.pos, val);
		mLogic.SendMove(mActor, &tos, posBuf, posSize);
	}
	else
	{
		mLogic.SendMove(mActor, &tos);
		toc_fight_fixpos fix;
		fix.pos.x = mActor.pos[0];
		fix.pos.y = mActor.pos[1];
		fix.pos.z = mActor.pos[2];
		SendProto(&fix, sizeof(fix));
	}
}

void CActorPlayer::Tos_fight_fire(const uint8_t * buf, size_t sz)
{
	if (mActor.state != SSceneActor::StateAlive)
		return;
	if (mLogic.mNow < mDelayPunishTime)
		return;
	if (mIllegal)
		return;
	size_t ssz = sz * 2;	// 简单估算发送协议长度，够用

	if (sz < sizeof(uint16_t))
		return;
	int listCount = *(uint16_t*)buf;
	buf += sizeof(uint16_t);
	sz -= sizeof(uint16_t);

	const uint8_t * aryBuf = buf;
	size_t arySZ = sz;

	for (int i = 0; i < listCount; i++)
	{
		if (!HitInfoArrayTos::Read(buf, sz))
			return;
	}

	int redirectCount = *(uint16_t*)buf;
	const uint8_t * redirectBuf = buf;
	buf += sizeof(uint16_t) + sizeof(short) * redirectCount;
	sz -= sizeof(uint16_t) + sizeof(short) * redirectCount;

	if (sz != sizeof(tos_fight_fire))
		return;

	const tos_fight_fire& tos = *(const tos_fight_fire*)buf;
	if (tos.timeStamp > mLogic.mNow - mEnterTime)
	{
		return;
	}
	if (mNextFireDt > 0.f && tos.timeStamp < mNextFireDt)
	{
		return;
	}
	
	if (tos.weaponVer != mWeaponVer)
		return;

	if (tos.atkType < 0 || MAX_ATTACK_TYPE <= tos.atkType)
		return;
	const SActorAttack& attack = mActor.attack_list[tos.atkType];
	if (!attack.valid)
		return;

	bool can_fire = mLogic.DeductBullet(mActor, tos.atkType);
	CheckFixBullet(tos.atkType, tos.bulletCount, tos.relyBulletCount, tos.sid);
	if (!can_fire)
		return;

	mLogic.UpdateActorActTime(mActor);
	mActor.last_fire_time = mLogic.mNow;

	uint8_t * sendBuf = (uint8_t *)alloca(ssz);
	if (!sendBuf)
		return;
	toc_base * proto = (toc_base *)sendBuf;
	((toc_base *)sendBuf)->proto_id = pid_fight_fire;

	uint8_t * p = (uint8_t *)(proto + 1);

	HitInfoArrayToc * sndAry = (HitInfoArrayToc *)p;
	sndAry->len = 0;

	const SActorProp& fromProp = mLogic.GetProp(mActor, tos.atkType);
	if ((fromProp.LimitState & SSceneActor::LimitStateFire) > 0)
	{
		return;
	}
	int pier = fromProp.Pierceability;
	float firePos[3];
	firePos[0] = tos.firePos.x;
	firePos[1] = tos.firePos.y;
	firePos[2] = tos.firePos.z;
	float dx = mActor.pos[0] - firePos[0];
	float dz = mActor.pos[2] - firePos[2];
	float ml = mActor.move_speed * g_AllowFireDelay;
	if (ml < 0.1f)
	{
		ml = 0.1f;
	}
	if (dx * dx + dz * dz > ml * ml)
		return;
	float fireRate = fromProp.FireRate * (1 + fromProp.AttackSpeed);
	if (fireRate > 2000)
	{
		fireRate = 2000;
	}
	if (fireRate <= 0)
	{
		fireRate = fromProp.FireRate;
	}
	mNextFireDt = tos.timeStamp + 60.f / fireRate;

	for (int i = 0; i < listCount; i++)
	{
		const HitInfoArrayTos * recvAry = HitInfoArrayTos::Read(aryBuf, arySZ);

		float damageRadio = 1;
        bool across_wall = false;
		for (int i = 0; i < recvAry->len; i++)
		{
			const p_hit_info_tos &recvInfo = recvAry->ary[i];
            if (!recvInfo.actorId)
                across_wall = true;
            struct SMakeHitInfoParams params(&mActor, &sndAry->ary[sndAry->len], &recvInfo, tos.atkType, 0, damageRadio, tos.isZoom > 0, false, NULL, &fromProp, firePos, across_wall);
			if (recvInfo.actorId && mLogic.MakeHitInfo(params))
				sndAry->len++;
			if (recvInfo.matType > 0 && recvInfo.matType <= MATERIAL_MAX)
			{
				MaterialDef& mt = g_MaterialDef[recvInfo.matType - 1];
				if (pier > mt.penetrate)
					break;
				damageRadio *= mt.damageRadio;
			}
		}
	}

	p += sndAry->Size();

	memcpy(p, redirectBuf, sizeof(uint16_t) + sizeof(short) * redirectCount);
	p += sizeof(uint16_t) + sizeof(short) * redirectCount;
	
	toc_fight_fire& toc = *(toc_fight_fire*)p;
	toc.sid = 0;
	toc.from = mActor.id;
	toc.hitPos = tos.hitPos;
	toc.fire_len = 3;
	toc.firePos[0] = (short)(firePos[0] * 100);
	toc.firePos[1] = (short)(firePos[1] * 100);
	toc.firePos[2] = (short)(firePos[2] * 100);
	toc.notPlayAni = tos.notPlayAni;
	toc.atkType = tos.atkType;
	p += sizeof(toc);

	mLogic.BroadcastProto(proto, p - sendBuf);
}

bool CActorPlayer::CheckFixBullet(const int atk_type, const int bullet_count, const int rely_count, const int session)
{
	const SActorAttack& attack = mActor.attack_list[ atk_type ];
	if (!attack.valid)
		return false;

	int rely_bullet = attack.rely_bullet >= 0 ? mActor.attack_list[attack.rely_bullet].bullet_count : 0;
	if (bullet_count == attack.bullet_count && rely_count == rely_bullet)
		return false;

	toc_fight_fixbullet fix;
	fix.weaponVer = mWeaponVer;
	fix.bulletCountD = attack.bullet_count - bullet_count;
	fix.relyBulletCountD = rely_bullet - rely_count;
	fix.atkType = atk_type;
	fix.sid = session;
	SendProto(&fix, sizeof(fix));
	return true;
}

bool CActorPlayer::SendProto(toc_base * proto, size_t sz)
{
	if (!mAddr.sin_family)
		return false;

	mLogic.MaskToc(proto, sz);
	SendProtoBuff(proto, sz);
	return true;
}

void CActorPlayer::FlushProtoBuff()
{
	if (mBuffSize > 0)
	{
		mLogic.SendUdp(mAddr, mBuff, mBuffSize);
		mBuffSize = 0;
	}

}

void CActorPlayer::SendProtoBuff(void* data, short sz)
{
	if (mBuffSize + sz + sizeof(short) > 1024)
	{
		FlushProtoBuff();
	}
	memcpy(mBuff + mBuffSize, &sz, sizeof(short));
	mBuffSize += sizeof(short);
	memcpy(mBuff + mBuffSize, data, sz);
	mBuffSize += sz;
}
