#pragma once

#include "proto_def.h"
#include <netinet/in.h>


class CActorPlayer
{
public:
	CSceneLogic& mLogic;
	SSceneActor& mActor;

	sockaddr_in mAddr;
	uint32_t mRandId;

	char mBuff[2048];
	uint16_t mBuffSize;
	short mSendId;
	bool mIllegal;
		
	int mWeaponVer;
	int mBulletCount;
	float mGroundHeight;	// 反作弊，记录当前地面高度

	int mDelayPunishTime;	// 移动延迟惩罚结束时间

	CActorPlayer(CSceneLogic& logic, SSceneActor& actor);
	~CActorPlayer();

	void HandleProto(const sockaddr_in & addr, uint32_t rid, uint32_t pid, const void * buf, size_t sz);

	void Tos_fight_move(const uint8_t * buf, size_t sz);
	void Tos_fight_fire(const uint8_t * buf, size_t sz);

    bool CheckFixBullet(const int atk_type, const int bullet_count, const int rely_count, const int session = 0);
	bool SendProto(toc_base * proto, size_t sz);
	void FlushProtoBuff();
	void SendProtoBuff(void* buff, short sz);
	
	float mOverMove;
	int mOverMoveCnt;
	float mLastMoveDt;
	float mOverMoveTime;
	float mNextFireDt;

	float mEnterTime;
};

