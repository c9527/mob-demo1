#pragma once

const uint32_t pid_fight_move = 0x40004;
const uint32_t pid_fight_fire = 0x40007;
const uint32_t pid_fight_hit = 0x40026;
const uint32_t pid_fight_fixbullet = 0x4001a;
const uint32_t pid_fight_spectate = 0x40027;
const uint32_t pid_fight_atk = 0x40014;
const uint32_t pid_fight_actor_die = 0x40022;
const uint32_t pid_fight_fixpos = 0x40071;

#pragma pack(1)

template <typename T>
struct p_array
{
	uint16_t len;
	T ary[1];

	static const p_array<T> * Read(const uint8_t * &buf, size_t &sz)
	{
		if (sz < sizeof(uint16_t))
			return NULL;
		const p_array<T> * ary = (const p_array<T> *)buf;
		size_t size = ary->Size();
		if (sz < size)
			return NULL;
		buf += size;
		sz -= size;
		return ary;
	}

	size_t Size() const
	{
		return Size(len);
	}

	static size_t Size(int len)
	{
		return sizeof(p_array) + (len - 1) * sizeof(T);
	}
};

struct toc_base
{
	uint32_t send_id;
	uint32_t data_crc;
	uint32_t proto_id;
};

struct toc_fight_move
{
	BYTE anglePitch;
	BYTE angleY;
	BYTE moveState;
	BYTE moveSpeed;
	BYTE clientSpeed;
	short time;
	int from;
	uint32_t sid;
};

struct p_vector3
{
	float x, y, z;
};

struct tos_fight_move
{
	BYTE anglePitch;
	BYTE angleY;
	BYTE moveState;
	BYTE clientSpeed;
	short time;
	float timeStamp;
	uint32_t sid;
};

struct p_hit_info_tos
{
	int matType;
	int actorId;
	int bodyPart;
	p_vector3 pos;
};

struct p_hit_info_toc
{
	int actorId;
	int bodyPart;
	int life;
	int armor;
	uint8_t die;
	BYTE hurtType;
};

struct tos_fight_fire
{
	p_vector3 hitPos;
	uint8_t notPlayAni;
	uint8_t isZoom;
	int weaponVer;
	int bulletCount;
    int relyBulletCount;
    int atkType;
	p_vector3 firePos;
	float timeStamp;
	uint32_t sid;
};

struct toc_fight_atk : toc_base
{
    int firePose;
    p_vector3 fireDir;
    int index;
    int atkType;
    int from;
    uint32_t sid;
	toc_fight_atk()
	{
		proto_id = pid_fight_atk;
	}
};

struct toc_fight_fire
{
	p_vector3 hitPos;
	uint8_t notPlayAni;
    int atkType;
	uint16_t fire_len;
	short firePos[3];
	int from;
	uint32_t sid;
	toc_fight_fire()
	{
		fire_len = 3;
	}
};

struct toc_fight_hit
{
	int hitType;
    int atkIdx;
	int skillId;
	int from;
	uint32_t sid;
};

struct toc_fight_fixbullet : toc_base
{
	int weaponVer;
	int bulletCountD;
    int relyBulletCountD;
    int atkType;
	uint32_t sid;

	toc_fight_fixbullet()
	{
		proto_id = pid_fight_fixbullet;
	}
};

struct toc_fight_fixpos : toc_base
{
	p_vector3 pos;
	uint32_t sid;
	toc_fight_fixpos()
	{
		proto_id = pid_fight_fixpos;
	}
};

struct tos_fight_spectate
{
	int64_t player_id;
	uint32_t sid;
};

struct toc_fight_actor_die : toc_base    // actor������Ϣ
{
    int actor_id;    // ������
    int killer;      // ��ɱ��
    int weapon;      // ��ɱ����
    int part;        // ��ɱ��λ
    int multi_kill;  // ��ɱ��
    int stage;       // �����ǽ�
    bool across_wall;//�Ƿ�ǽ
    p_vector3 killerPos;
    p_vector3 diePos;
    int revive_time;	//����ʱ��
    int atk_type;
	float time_stamp;
	uint32_t sid;
};

typedef p_array<p_hit_info_tos> HitInfoArrayTos;
struct HitInfoArrayToc : p_array<p_hit_info_toc> {};

#pragma pack()
