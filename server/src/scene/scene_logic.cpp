
#include "stdafx.h"
#include "scene_logic.h"
#include "scene_data.h"
#include "actor_robot.h"
#include "actor_player.h"
#include "lua_def.h"
#include <time.h>
#include <math.h>
extern "C"
{
	#include <skynet_malloc.h>
}

void _dbg(const char * fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
}

CSceneLogic::CSceneLogic(CSceneData& data) : mData(data)
{
	mLua = NULL;
	ZeroArray(mActors);
	mState = GameStateInit;
	mNow = 0;
	mStatDamageTime = 0;
	mKillInfoCnt = 0;
	mDamageInfoCnt = 0;
    ZeroArray(mDisableAtkTypes);

	time_t t;
	time(&t);
	mRand = (uint32_t)t;

	mSendId = 0;
	mDelayWatch = false;
}

CSceneLogic::~CSceneLogic()
{
	for (int id = 0; id < MAX_ACTOR_COUNT; id++)
		RemoveActor(id);
}

void CSceneLogic::CallScript(const char* func, int param1, int param2, int param3)
{
	lua_getglobal(mLua, "core_callback");
	lua_pushstring(mLua, func);
	lua_pushinteger(mLua, param1);
	lua_pushinteger(mLua, param2);
	lua_pushinteger(mLua, param3);
	lua_call(mLua, 4, 0);
}

void CSceneLogic::HandleProto(const sockaddr_in & addr, uint32_t * protoBuf, size_t sz)
{
	if (sz <= 20)
		return;
	uint32_t rid = protoBuf[3];
	uint32_t pid = protoBuf[4];
	sz -= 20;
	if (rid == 0)
	{
		if (pid == pid_fight_spectate && sz == sizeof(tos_fight_spectate))
		{
			int64_t playerId = ((tos_fight_spectate*)(protoBuf + 5))->player_id;
			for (SepVector::iterator it = mSpectators.begin(); it!=mSpectators.end(); it++)
			{
				if ((*it).mPlayerId == playerId)
				{
					(*it).mAddr = addr;
					return;
				}
			}
			SSceneSpectator spectator = SSceneSpectator();
			spectator.mPlayerId = playerId;
			spectator.mAddr = addr;
			mSpectators.push_back(spectator);
		}
		return;
	}

	SSceneActor& actor = mActors[rid % MAX_ACTOR_COUNT];
	if (actor.player)
		actor.player->HandleProto(addr, rid, pid, protoBuf + 5, sz);
	if (mDamageInfoCnt > 0)
	{
		ProcessDamageInfo();
	}
    if (mKillInfoCnt > 0)
    {
        FlushProtoBuff();
        ProcessKillInfos();
    }
}

void CSceneLogic::Update()
{
	mNow += 1000 / COUNT_PER_SECOND;
	if (!CanPlay())
	{
	    FlushProtoBuff();
		return;
	}
	for (int i = 0; i < MAX_ACTOR_COUNT; i++)
	{
		SSceneActor& actor = mActors[i];
		if (!actor.id)
			continue;
		if (actor.robot && actor.state == SSceneActor::StateAlive)
			actor.robot->Update();
		int time = actor.callback_time;
		if (time && mNow >= time)
		{
			actor.callback_time = 0;
			CallScript("on_actor_timer", actor.id, time);
		}
	}
	FlushProtoBuff();
	if (mNow >= mStatDamageTime)
	{
		//mStatDamageTime = 0;
		mStatDamageTime = mNow + 1000;
		CallScript("on_update_damage");
	}
	ProcessDamageInfo();
    ProcessKillInfos();
}

void CSceneLogic::AddKillInfo(int killer_id, int killed_id, int atk_type, int hit_type, bool across_wall, const float* killerPos, const float* diePos)
{
    if (mKillInfoCnt >= MAX_ACTOR_COUNT - 1)
    {
        return;
    }
    SKillInfo& info = mKillInfos[ mKillInfoCnt++ ];
    info.killer_id = killer_id;
    info.killed_id = killed_id;
    info.atk_type = atk_type;
    info.hit_type = hit_type;
    info.across_wall = across_wall;
	memcpy(info.killerPos, killerPos, sizeof(float) * 3);
	memcpy(info.diePos, diePos, sizeof(float) * 3);
}

void CSceneLogic::ProcessKillInfos()
{
    if (mKillInfoCnt <= 0)
        return;
    
    lua_getglobal(mLua, "core_callback");
    lua_pushstring(mLua, "core_on_kill");

    lua_newtable(mLua);
    for (int i = 0; i < mKillInfoCnt; i++)
    {
        SKillInfo& info = mKillInfos[i];
        
        lua_newtable(mLua);
        lua_pushinteger(mLua, info.killer_id);
        lua_rawseti(mLua, -2, 1);
        lua_pushinteger(mLua, info.killed_id);
        lua_rawseti(mLua, -2, 2);
        lua_pushinteger(mLua, info.atk_type);
        lua_rawseti(mLua, -2, 3);
        lua_pushinteger(mLua, info.hit_type);
        lua_rawseti(mLua, -2, 4);
        lua_pushboolean(mLua, info.across_wall);
        lua_rawseti(mLua, -2, 5);
		lua_pushnumber(mLua, info.killerPos[0]);
		lua_rawseti(mLua, -2, 6);
		lua_pushnumber(mLua, info.killerPos[1]);
		lua_rawseti(mLua, -2, 7);
		lua_pushnumber(mLua, info.killerPos[2]);
		lua_rawseti(mLua, -2, 8);
		lua_pushnumber(mLua, info.diePos[0]);
		lua_rawseti(mLua, -2, 9);
		lua_pushnumber(mLua, info.diePos[1]);
		lua_rawseti(mLua, -2, 10);
		lua_pushnumber(mLua, info.diePos[2]);
		lua_rawseti(mLua, -2, 11);
        lua_rawseti(mLua, -2, i + 1);
    }
    mKillInfoCnt = 0;
    lua_call(mLua, 2, 0);
}

void CSceneLogic::AddDamageInfo(int attack_id, int hit_id, int damage)
{
	if (mDamageInfoCnt >= MAX_ACTOR_COUNT - 1)
	{
		return;
	}
	SDamageInfo& info = mDamageInfos[ mDamageInfoCnt++ ];
	info.attack_id = attack_id;
	info.hit_id = hit_id;
	info.damage = damage;
}

void CSceneLogic::ProcessDamageInfo()
{
	if (mDamageInfoCnt <= 0)
		return;
	lua_getglobal(mLua, "core_callback");
	lua_pushstring(mLua,"core_update_damage_info");
	lua_newtable(mLua);
	for( int i = 0; i < mDamageInfoCnt; i++)
	{
		SDamageInfo& info = mDamageInfos[i];
		lua_newtable(mLua);
		lua_pushinteger(mLua, info.attack_id);
		lua_rawseti(mLua, -2, 1);
		lua_pushinteger(mLua, info.hit_id);
		lua_rawseti(mLua, -2, 2);
		lua_pushinteger(mLua, info.damage);
		lua_rawseti(mLua, -2, 3);
		lua_rawseti(mLua, -2, i + 1);
	}
	mDamageInfoCnt = 0;
	lua_call(mLua, 2, 0);
}

SSceneActor* CSceneLogic::AddRobot(SRobotConfig*& cfg)
{
	for (int i = 0; i < MAX_ACTOR_COUNT; i++)
	{
		SSceneActor& actor = mActors[i];
		if (!actor.id)
		{
			actor.id = i + 1;
			actor.type = SSceneActor::TypeRobot;
			actor.is_monster = true;
			actor.is_boss = false;
			actor.robot = new CActorRobot(*this, actor, cfg);
			return &actor;
		}
	}
	return NULL;
}

SSceneActor* CSceneLogic::AddPlayer(uint32_t& rid)
{
	for (int i = 0; i < MAX_ACTOR_COUNT; i++)
	{
		SSceneActor& actor = mActors[i];
		if (!actor.id)
		{
			actor.id = i + 1;
			actor.type = SSceneActor::TypePlayer;
			actor.is_monster = false;
			actor.is_boss = false;
			actor.player = new CActorPlayer(*this, actor);
			rid = actor.player->mRandId;
			return &actor;
		}
	}
	return NULL;
}

SSceneActor* CSceneLogic::GetActor(int id)
{
	if (id <= 0 || id > MAX_ACTOR_COUNT)
		return NULL;
	SSceneActor* actor = mActors + id - 1;
	if (!actor->id)
		return NULL;
	return actor;
}

const SActorProp& CSceneLogic::GetProp(const SSceneActor& actor) const
{
    const SActorAttack& attack = actor.attack_list[actor.cur_attack_type];
    return attack.prop;
}

const SActorProp& CSceneLogic::GetProp(const SSceneActor& actor, const int atk_type) const
{
    const SActorAttack& attack = actor.attack_list[atk_type];
    return attack.prop;
}

int CSceneLogic::GetBullet(const SSceneActor& actor, const int atk_type) const
{
    if (atk_type < 0 || MAX_ATTACK_TYPE <= atk_type)
        return 0;
    const SActorAttack& attack = actor.attack_list[atk_type];
    if (!attack.valid)
        return 0;
    if (attack.rely_bullet < 0)
        return attack.bullet_count;
    int rely_count = GetBullet(actor, attack.rely_bullet);
    return rely_count <= attack.bullet_count ? rely_count : attack.bullet_count;
}

bool CSceneLogic::DeductBullet(SSceneActor& actor, const int atk_type)
{
    int count = GetBullet(actor, atk_type);
    if (count == 0)
        return false;
    SActorAttack& attack = actor.attack_list[atk_type];
    if (attack.bullet_count > 0)
        --attack.bullet_count;
    if (attack.rely_bullet >= 0)
        DeductBullet(actor, attack.rely_bullet);
    return true;
}

void CSceneLogic::RemoveActor(int id)
{
	if (id <= 0 || id > MAX_ACTOR_COUNT)
		return;
	SSceneActor& actor = mActors[id - 1];
	if (actor.robot)
		delete actor.robot;
	if (actor.player)
		delete actor.player;
	ZeroStruct(actor);
}

void CSceneLogic::UpdateActorActTime(SSceneActor& actor)
{
    actor.life = GetActorLife(actor);
    actor.last_act_time = mNow;
}

bool CSceneLogic::ActorReset(int id)
{
	SSceneActor* actor = GetActor(id);
	if (!actor || !actor->id)
		return false;

    const SActorProp& prop = GetProp(*actor);
	actor->state = SSceneActor::StateAlive;
	actor->life = prop.MaxHP;
	actor->last_armor = prop.MaxArmor;
	actor->last_damage_time = mNow;
    actor->last_act_time = mNow;
    actor->last_fire_time = 0;

	ZeroArray(actor->damage_info);

	if (actor->robot)
		actor->robot->Reset();

	return true;
}

bool CSceneLogic::MakeHitInfo(SMakeHitInfoParams& params)
{
    if (0 <= params.atk_type && params.atk_type < MAX_ATTACK_TYPE && mDisableAtkTypes[ params.atk_type ])
        return false;
    SSceneActor& actorFrom = *params.from;
    const p_hit_info_tos& recvInfo = *params.tos_info;
	SSceneActor * hitActor = GetActor(recvInfo.actorId);
	if (!hitActor || !hitActor->id)
		return false;

	SSceneActor &actor = *hitActor;
	if (actor.state != SSceneActor::StateAlive)
		return false;
    if (actor.god_end >= mNow)
        return false;
		
	SSceneActor * attackActor = params.from;
	if (actor.robot && actor.robot->IgnoreHurt(*attackActor))
		return false;
    const SActorProp *fromProp = params.from_prop;

	if (!params.force)
	{
		if (actor.id == actorFrom.id)
			return false;
		if (actor.camp && actor.camp == actorFrom.camp && fabs(mTeammateCure) < 1e-10 && fromProp->TeamHealRate < 1e-10)
			return false;
		float _dx = recvInfo.pos.x - actor.pos[0];
		float _dz = recvInfo.pos.z - actor.pos[2];
		float ml = actor.move_speed * g_AllowFireDelay;
		if (ml < 0.1f)
		{
			ml = 0.1f;
		}
		if (_dx*_dx + _dz*_dz > ml * ml)
			return false;
        if (actor.immune > 0 && actor.camp != actorFrom.camp)
        {
            actor.immune--;
            CallScript("actor_dec_immune", actor.id, actor.immune, actorFrom.id);
            return false;
        }
	}

	const SActorProp& myProp = GetProp(actor);
    const float *fromPos = params.from_pos;
	float x = fromPos[0] - actor.pos[0];
	float y = fromPos[1] - actor.pos[1];
	float z = fromPos[2] - actor.pos[2];
	float dis = sqrt(x*x + y*y + z*z);

	float damagef = (params.is_zoom ? fromProp->ZoomDamage + fromProp->Damage : fromProp->Damage) * params.damage_radio;
	if (dis >= fromProp->DamageDecayEndDis)
		damagef -= fromProp->DamageDecayMax;
	else if (dis >= fromProp->DamageDecayStartDis)
		damagef -= fromProp->DamageDecayMax * (dis - fromProp->DamageDecayStartDis) / (fromProp->DamageDecayEndDis - fromProp->DamageDecayStartDis);

    float damageFactor = 1, factorCut = 0;
	switch (recvInfo.bodyPart)
	{
	case 1:
		damageFactor = myProp.HeadDamageFactor + fromProp->HeadHurtFactor;
		break;
	case 2:
		damageFactor = myProp.BodyDamageFactor + fromProp->BodyHurtFactor;
		break;
	case 3:
		damageFactor = myProp.LimbsDamageFactor + fromProp->LimbsHurtFactor;
		break;
	}
	switch (params.atk_type)
	{
	case 6:
		damageFactor *= (1 + myProp.DaggerDamageFactor + fromProp->DaggerHurtFactor);
		break;
	case 9:
		damageFactor *= (1 + myProp.GrenadeDamageFactor + fromProp->GrenadeHurtFactor);
		break;
	}
	bool crit = false;
	if (fromProp->CritRate > 0 && fromProp->CritHurtFactor > 0 && Rand(100) < fromProp->CritRate * 100)
	{
		crit = true;
		damageFactor *= (1 + fromProp->CritHurtFactor);
		damagef += fromProp->CritHurt;
	}
    if (actor.is_protected && dis < mActorProtectDis && 1 <= recvInfo.bodyPart && recvInfo.bodyPart <= 3)
        factorCut = mActorDamageFactorCuts[recvInfo.bodyPart - 1];
    if (actor.player || !actor.is_monster)
	{
	    damagef += fromProp->RawPvpExtraDamage;
		damagef *= damageFactor - factorCut + myProp.HurtRate + fromProp->RawPvpDamageRate;
	}
	else if (actor.robot)
	{
        damagef += fromProp->RawPveExtraDamage;
        damagef *= damageFactor - factorCut + myProp.HurtRate + fromProp->RawPveDamageRate;
	}
    if (damagef < 0)
        damagef = 0;

	float armorRate = 0;
	switch (fromProp->DamageType)
	{
	case 1:
		armorRate = myProp.Type1ArmorRate;
		break;
	case 2:
		armorRate = myProp.Type2ArmorRate;
		break;
	case 3:
		armorRate = myProp.Type3ArmorRate;
		break;
	default:
		break;
	}

	int useArmor = (int)(damagef * (armorRate - fromProp->IgnoreArmorRate) + 0.99999);
	int restArmor = GetActorArmor(actor);
	if (useArmor > damagef)
		useArmor = (int)damagef;
    bool is_cure = actor.camp && actor.camp == actorFrom.camp && (fabs(mTeammateCure) > 1e-10 || fromProp->TeamHealRate > 1e-10);
	if (actor.is_monster || actorFrom.is_monster)
	{
		if (is_cure)
		{
			return false;
		}
		is_cure = false;
	}
	if (!is_cure && (myProp.LimitState & SSceneActor::LimitStateHit)> 0)
	{
		return false;
	}
	if (!is_cure && fromProp->Pierce > 0)
	{
		restArmor -= fromProp->Pierce;
		if (restArmor < 0) restArmor = 0;
	}
	if (useArmor <= 0 || is_cure)
	{
		useArmor = 0;
	}
	else if (useArmor <= restArmor)
	{
		restArmor -= useArmor;
	}
	else
	{
		useArmor = restArmor;
		restArmor = 0;
	}
	actor.last_armor = restArmor;
    int resetLife = GetActorLife(actor);
	actor.last_damage_time = mNow;
	actor.last_damage_part = recvInfo.bodyPart;
    int real_damage = (int)(damagef - useArmor);
    const SActorProp& actor_prop = GetProp(actor);
    if (actorFrom.id != actor.id)
	{
        if (!is_cure)
        {
			if (actor.camp == 0 || actor.camp != actorFrom.camp)
			{
				int damage_value = real_damage <= resetLife ? real_damage : resetLife;
			    actorFrom.damage_sum += damage_value;
				if (actor.player)
					actorFrom.damage_player += damage_value;
				if (actor.is_boss)
					actorFrom.damage_boss += damage_value;
				AddDamageInfo( actorFrom.id, actor.id, damage_value);
				//CallScript("on_update_damage", (*hitActor).id, actorFrom.id, damage_value);
			}
		}
        else
        {
			if (mTeammateCure > 1e-10)
	            real_damage = (int)(real_damage * (mTeammateCure + fromProp->TeammateCure));
			else
				real_damage = (int)(real_damage * fromProp->TeamHealRate);
            if (actor_prop.MaxHP - resetLife < real_damage)
                real_damage = actor_prop.MaxHP - resetLife;
            actorFrom.cure_sum += real_damage;
            real_damage = -real_damage;
        }	
		//if (!mStatDamageTime)
		//	mStatDamageTime = mNow + 1000;
	}
	else
	{
		if (!is_cure && fromProp->SelfDamageRate > 0)
		{
			real_damage = real_damage * fromProp->SelfDamageRate;		
		}
		else
		{
			if (mTeammateCure > 1e-10)
				real_damage = (int)(real_damage * (mTeammateCure + fromProp->TeammateCure));
			else
				real_damage = (int)(real_damage * fromProp->TeamHealRate * fromProp->SelfDamageRate);
			if (actor_prop.MaxHP - resetLife < real_damage)
				real_damage = actor_prop.MaxHP - resetLife;
			actorFrom.cure_sum += real_damage;
			real_damage = -real_damage;
		}
	}
    if ((actor_prop.MaxHP - resetLife < actor.critical_hp)
        && (actor_prop.MaxHP - (resetLife - real_damage) >= actor.critical_hp))
		CallScript("actor_hp_criticality", actor.id, actorFrom.id); // HP临界点
    
	if (real_damage > 0 && (actor_prop.BuffClearType & SSceneActor::ClearTypeHit) > 0)
		CallScript("actor_clear_buff", actor.id, SSceneActor::ClearTypeHit);

	if (real_damage > 0 && fromProp->AttackBuff > 0)
		CallScript("actor_add_buff", actor.id, fromProp->AttackBuff);

	actor.life = resetLife - real_damage;

	int minTime = mNow;
	SSceneActor::DamageInfo * dmif = NULL;
	for (int i = 0; i < MAX_DAMAGE_INFO_COUNT; i++)
	{
        if (is_cure)
            break;
		SSceneActor::DamageInfo& di = actor.damage_info[i];
		if (di.fromId == actorFrom.id)
		{
			dmif = &di;
			break;
		}
		else if (!di.fromId || !mActors[di.fromId - 1].id)
		{
			dmif = &di;
			minTime = 0;
		}
		else if (!dmif || di.time < minTime)
		{
			dmif = &di;
			minTime = di.time;
		}
	}

	if (dmif)
	{
		dmif->time = mNow;
		if (dmif->fromId == actorFrom.id)
		{
			dmif->damage += (int)damagef;
		}
		else
		{
			dmif->fromId = actorFrom.id;
			dmif->damage = (int)damagef;
		}
	}

	if (actor.life <= 0)
	{
		actor.life = 0;
		actor.state = SSceneActor::StateDie;
		const float *firePos = params.fire_pos;
		float deadPos[3];
		deadPos[0] = recvInfo.pos.x;
		deadPos[1] = recvInfo.pos.y;
		deadPos[2] = recvInfo.pos.z;
        AddKillInfo(actorFrom.id, actor.id, params.atk_type, params.hit_type, params.across_wall, firePos, deadPos);
	}
	else if (actor.robot)
	{
		actor.robot->OnDamage(actorFrom);
	}

    p_hit_info_toc& info = *params.toc_info;
	info.actorId = recvInfo.actorId;
	info.bodyPart = recvInfo.bodyPart;
	info.life = hitActor->life;
	info.armor = hitActor->last_armor;
	info.die = hitActor->state != SSceneActor::StateAlive;
	info.hurtType = crit ? 1 : 0;
	return true;
}

int CSceneLogic::GetActorArmor(const SSceneActor& actor)
{
	const SActorProp& prop = GetProp(actor);
	int armor = actor.last_armor;
	if (armor >= prop.MaxArmor)
		return prop.MaxArmor;
	if (armor < 0)
		armor = 0;
	int t = mNow - actor.last_damage_time - prop.ArmorRecoverWait;
	if (t <= 0)
		return armor;
	armor += (int)(t * prop.ArmorRecoverSpeed / 1000);
	if (armor >= prop.MaxArmor)
		return prop.MaxArmor;
	return armor;
}

int CSceneLogic::GetActorLife(const SSceneActor& actor)
{
    const SActorProp& prop = GetProp(actor);
    int life = actor.life;
    if (life >= prop.MaxHP)
        return prop.MaxHP;
    int t = actor.last_damage_time;
    if (t < actor.last_act_time)
        t = actor.last_act_time;
    t = mNow - t - prop.HPRecoverWait;
    if (t <= 0)
        return life;
    life += (int)(t * prop.HPRecoverSpeed / 1000);
    if (life >= prop.MaxHP)
        return prop.MaxHP;
    return life;
}

void CSceneLogic::RemoveSpectator(int64_t playerId)
{
	for (SepVector::iterator it = mSpectators.begin(); it!=mSpectators.end(); it++)
	{
		if ((*it).mPlayerId == playerId)
		{
			mSpectators.erase(it);
			return;
		}
	}
}

void CSceneLogic::DoActorHit(SSceneActor& actor, const int * idList, int idCount, int hitType, const float * fromPos, const float * firePos, float damageRadio, int hitPart, const SActorProp* fromProp, const int atkType, const int atkIdx)
{
	uint8_t * sendBuf = (uint8_t *)alloca(sizeof(uint32_t)+HitInfoArrayToc::Size(idCount) + sizeof(toc_fight_hit));
	if (!sendBuf)
		return;
	toc_base * proto = (toc_base *)sendBuf;
	((toc_base *)sendBuf)->proto_id = pid_fight_hit;
		
	uint8_t * p = (uint8_t *)(proto + 1);

	p_hit_info_tos info;
	info.bodyPart = hitPart;
	info.matType = 0;

	HitInfoArrayToc * sndAry = (HitInfoArrayToc *)p;
	sndAry->len = 0;
	for (int i = 0; i < idCount; i++)
	{
		info.actorId = idList[i];
   		SSceneActor * hitActor = GetActor(info.actorId);
		if (!hitActor || !hitActor->id)
			continue;
		info.pos.x = hitActor->pos[0];
		info.pos.y = hitActor->pos[1];
		info.pos.z = hitActor->pos[2];
		struct SMakeHitInfoParams params(&actor, &sndAry->ary[sndAry->len], &info, atkType, hitType, damageRadio, false, true, fromPos, fromProp, firePos);
        if (MakeHitInfo(params))
			sndAry->len++;
	}

	p += sndAry->Size();

	toc_fight_hit& toc = *(toc_fight_hit*)p;
	toc.hitType = hitType;
    toc.atkIdx = atkIdx;
	toc.skillId = 0;
	toc.sid = 0;
	toc.from = actor.id;
	p += sizeof(toc);

	BroadcastProto(proto, p - sendBuf);
	FlushProtoBuff();	
	ProcessDamageInfo();
	ProcessKillInfos();
}

void CSceneLogic::DoActorSkill(SSceneActor& actor, const int * idList, int idCount, int hitType, int skillId, int damage)
{
	uint8_t * sendBuf = (uint8_t *)alloca(sizeof(uint32_t)+HitInfoArrayToc::Size(idCount) + sizeof(toc_fight_hit));
	if (!sendBuf)
		return;
	toc_base * proto = (toc_base *)sendBuf;
	((toc_base *)sendBuf)->proto_id = pid_fight_hit;
		
	uint8_t * p = (uint8_t *)(proto + 1);

	p_hit_info_tos info;
	info.bodyPart = 0;
	info.matType = 0;

	HitInfoArrayToc * sndAry = (HitInfoArrayToc *)p;
	sndAry->len = 0;
	int realDamage = 0;
	for (int i = 0; i < idCount; i++)
	{
		info.actorId = idList[i];
   		SSceneActor * hitActor = GetActor(info.actorId);
		if (!hitActor || !hitActor->id)
			continue;
		if (hitActor->state != SSceneActor::StateAlive)
			continue;
		if (hitActor->god_end >= mNow)
			continue;
		if (hitActor->robot && hitActor->robot->IgnoreHurt(actor))
			continue;
		realDamage = damage <= hitActor->life ? damage : hitActor->life;
		hitActor->life = hitActor->life - realDamage;
		if (hitActor->life == 0)
		{
			hitActor->state = SSceneActor::StateDie;
			AddKillInfo(actor.id, hitActor->id, -1, hitType, false, actor.pos, hitActor->pos);
		}
		else if (hitActor->robot)
		{
			hitActor->robot->OnDamage(actor);
		}
		AddDamageInfo( actor.id, hitActor->id, realDamage );
		p_hit_info_toc& info_toc = sndAry->ary[sndAry->len];
		info_toc.actorId = info.actorId;
		info_toc.bodyPart = 0;
		info_toc.life = hitActor->life;
		info_toc.armor = hitActor->last_armor;
		info_toc.die = hitActor->state != SSceneActor::StateAlive;
		info_toc.hurtType = 0;
		sndAry->len++;
	}

	p += sndAry->Size();

	toc_fight_hit& toc = *(toc_fight_hit*)p;
	toc.hitType = hitType;
    toc.atkIdx = 0;
	toc.skillId = skillId;
	toc.sid = 0;
	toc.from = actor.id;
	p += sizeof(toc);

	BroadcastProto(proto, p - sendBuf);

	ProcessDamageInfo();
	ProcessKillInfos();
}

void CSceneLogic::SendMove(const SSceneActor& actor, const tos_fight_move * tos, const uint8_t * buf, size_t sz)
{
	uint16_t ssz = 2 * sizeof(tos_fight_move);
	uint8_t * sendBuf = (uint8_t *)alloca(ssz);
	if (!sendBuf)
		return;
	
	float val[POS_SIZE];
	poscpy(val, actor.pos);
	
	toc_base * proto = (toc_base *)sendBuf;
	((toc_base *)sendBuf)->proto_id = pid_fight_move;
	uint8_t * p = (uint8_t *)(proto + 1);
	if (buf)
	{
		memcpy(p, buf, sz);
		p += sz;
	}
	else
	{
		uint16_t& posSize = *(uint16_t *)p;
		posSize = 3;
		p += sizeof(uint16_t);
		short& px = *(short *)p;
		px = val[0] * 100;
		p += sizeof(short);
		short& py = *(short *)p;
		py = val[1] * 100;
		p += sizeof(short);
		short& pz = *(short *)p;
		pz = val[2] * 100;
		p += sizeof(short);
	}
	
	toc_fight_move& toc = *(toc_fight_move*)p;
	toc.sid = 0;
	toc.from = actor.id;
	toc.anglePitch = val[3];
	toc.angleY = val[4] / 2;
	toc.moveState = val[5];
	
	toc.moveSpeed = actor.move_speed * 10;
	if (tos)
	{
		toc.clientSpeed = tos->clientSpeed;
		toc.time = tos->time;
		toc.sid = tos->sid;
	}
	else
	{
		toc.clientSpeed = 0;
		toc.time = 0;
	}
	p += sizeof(toc);

	BroadcastProto(proto, p - sendBuf);
}

void CSceneLogic::BroadcastProto(toc_base * proto, size_t sz)
{
	if (mDelayWatch)
	{
		char* data = (char*)skynet_malloc(sz + 4 - 2 * sizeof(uint32_t));
		memcpy(data + 4, (const char*)proto + 2 * sizeof(uint32_t), sz - 2 * sizeof(uint32_t));   
		int* proto_id = (int*)(data + 4);
		*proto_id = proto->proto_id;
		lua_getglobal(mLua, "core_callback");
	    lua_pushstring(mLua, "save_broadcast_stream");
		lua_pushlstring(mLua, (const char*)data, sz + 4 - 2 * sizeof(uint32_t));
		lua_call(mLua, 2, 0);
		skynet_free(data);
	}
	MaskToc(proto, sz);
	for (int i = 0; i < MAX_ACTOR_COUNT; i++)
	{
		SSceneActor& actor = mActors[i];
		if (actor.id && actor.player && actor.player->mAddr.sin_family)
			actor.player->SendProtoBuff(proto, sz);
	}
	if (mDelayWatch)
	{
		return;
	}
 	for (SepVector::iterator it = mSpectators.begin(); it!=mSpectators.end(); it++)
	{
		if ((*it).mBuffSize + sz + sizeof(sz) > 1024)
		{
			SendUdp((*it).mAddr, (*it).mBuff, (*it).mBuffSize);
			(*it).mBuffSize = 0;
		}
		short buffSize = sz;
		memcpy((*it).mBuff + (*it).mBuffSize, &buffSize, sizeof(short));
		(*it).mBuffSize += sizeof(short);
		memcpy((*it).mBuff + (*it).mBuffSize, proto, sz); 
		(*it).mBuffSize += sz;
	}
}

void CSceneLogic::MaskToc(toc_base * proto, size_t sz)
{
	proto->send_id = ++mSendId;
	proto->data_crc = Adler32(&proto->proto_id, sz - 8);
	MaskBuf(&proto->data_crc, proto->send_id, sz - 4);
}

void CSceneLogic::FlushProtoBuff()
{
	for (int i = 0; i < MAX_ACTOR_COUNT; i++)
	{
		SSceneActor& actor = mActors[i];
		if (actor.id && actor.player && actor.player->mAddr.sin_family)
		actor.player->FlushProtoBuff();
	}
	for (SepVector::iterator it = mSpectators.begin(); it!=mSpectators.end(); it++)
	{
		if ((*it).mBuffSize > 0)
		{
			SendUdp((*it).mAddr, (*it).mBuff, (*it).mBuffSize);
			(*it).mBuffSize = 0;
		}
	}
}
