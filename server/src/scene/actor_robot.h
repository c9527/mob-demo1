#pragma once

#include "robotconfig.h"
#include "proto_def.h"
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <stdarg.h>

#define PI_F		3.14159265358979323846f
extern int g_AIUpdateInterval;

class CActorRobot
{
	struct Point
	{
		float x, z;
	};

	enum State
	{
		StateNone,
		StateMove,
		StatePatrol,
		StateAttack,
		StateRoute,
        StateLuaAI,
	};

	CSceneLogic& mLogic;
	SSceneActor& mActor;

	SRobotConfig mCfg;

	State mState;

	const SAIZone* mLastAi;
	const SAIZone* mTargetAi;
	float mAiDeg;

	const SAITarget* mPathTarget;
	Point mPathStart;
	Point mPathFactor;
	float mPathLen;
	float mPathLenC;
	float mCurPathLen;
	Point mTargetPoint;
	float mTargetDeg;
	bool mPathRouting;

	int mHurtWait;
	int mFindEnemyWait;
	int mMoveWait;
	int mAttackWait;
	int mAttackLostWait;
	int mAIUpdateTime;
	const SSceneActor* mAttackEnemy;

	bool mRouteReached[CSceneData::ROUTEPOINT_COUNT];

	struct SSearchPath {
		int parent;
		int id;
	};
	SSearchPath mRoutePath[CSceneData::ROUTEPOINT_COUNT];

public:
	CActorRobot(CSceneLogic& logic, SSceneActor& actor, SRobotConfig*& cfg) :
		mLogic(logic), mActor(actor)
	{
		mState = StateNone;
		cfg = &mCfg;
		ZeroArray(mRouteReached);
		Reset();
	}

	~CActorRobot()
	{
	}

private:
	void _dbg(const char * fmt, ...)
	{
		//va_list ap;
		//va_start(ap, fmt);
		//vprintf(fmt, ap);
		//va_end(ap);
	}

	float Rand()
	{
		return mLogic.Rand() / (float)0x100000000lu;
	}

	float Rand(const SRobotConfig::SRangef range)
	{
		return Rand() * (range.max - range.min) + range.min;
	}

	bool RandRate(int rate)
	{
		return mLogic.Rand(10000) < rate;
	}

	float MoveSpeed()
	{
		return mActor.move_speed * g_AIUpdateInterval / 1000;
	}

	float TargetDistance()
	{
		float x = mTargetPoint.x - mActor.pos[0];
		float z = mTargetPoint.z - mActor.pos[2];
		return sqrt(x*x + z*z);
	}

	bool IsNearTarget()
	{
		float x = mTargetPoint.x - mActor.pos[0];
		float z = mTargetPoint.z - mActor.pos[2];
		return x*x + z*z <= MoveSpeed() * MoveSpeed();
	}

	int WaitRandTime(const SRobotConfig::SRange& range)
	{
		return mLogic.mNow + mLogic.Rand(range.min, range.max);
	}

	float CalcTargetDeg(float x, float z)
	{
		float deg = 180 / PI_F * atan2f(x - mActor.pos[0], z - mActor.pos[2]);
		if (deg < 0)
			deg += 360;
		return deg;
	}

	float ViewPosDegAbs(float x, float z)
	{
		float deg = 180 / PI_F * atan2f(x - mActor.pos[0], z - mActor.pos[2]);
		deg = fabsf(fmodf(deg - mActor.pos[4], 360));
		if (deg > 180)
			deg = 360 - deg;
		return deg;
	}

	bool CanAttackTo(float x, float z)
	{
		if (mCfg.AttackType == 1)	// 近身攻击，可走到才可攻击
			return CanWalkTo(x, z);
		return mLogic.mData.PosCanAttack(mActor.pos[0], mActor.pos[2], x, z, 1.5);
	}

	bool CanWalkTo(float x, float z)
	{
		return mLogic.mData.PosCanArrive(mActor.pos[0], mActor.pos[2], x, z, mCfg.StepHeight.min, mCfg.StepHeight.max);
	}

	bool EnemyValid(const SSceneActor& enemy)
	{
		if (!enemy.id || enemy.id == mActor.id)
			return false;
		if (enemy.state != SSceneActor::StateAlive)
			return false;
		if (enemy.camp && enemy.camp == mActor.camp)
			return false;
		switch (mCfg.SelectType)
		{
		case 1:
			if (!enemy.player)
				return false;
			break;
		case 2:
			if (!enemy.robot)
				return false;
			break;
		}
		return true;
	}

	bool EnemyInView(const SSceneActor& enemy, bool checkDistance)
	{
		if (mCfg.ViewDeg <= 0)
			return false;
		if (!EnemyValid(enemy))
			return false;
		if (mCfg.ViewDeg < 180)
		{
			float deg = ViewPosDegAbs(enemy.pos[0], enemy.pos[2]);
			if (deg > mCfg.ViewDeg)
				return false;
		}
		if (checkDistance && mCfg.ViewDistance > 0)
		{
			float x = enemy.pos[0] - mActor.pos[0];
			float z = enemy.pos[2] - mActor.pos[2];
			if (x*x + z*z > mCfg.ViewDistance*mCfg.ViewDistance)
				return false;
		}
		if (mCfg.TraceDistance > 0 && mPathTarget)
		{
			float x = enemy.pos[0] - (mPathStart.x + mCurPathLen * mPathFactor.x);
			float z = enemy.pos[2] - (mPathStart.z + mCurPathLen * mPathFactor.z);
			if (x*x + z*z > mCfg.TraceDistance*mCfg.TraceDistance)
				return false;
		}
		return CanAttackTo(enemy.pos[0], enemy.pos[2]);
	}

	const SSceneActor* FindViewEnemy()
	{
		for (int i = 0; i < CSceneLogic::MAX_ACTOR_COUNT; i++)
		{
			const SSceneActor& enemy = mLogic.mActors[i];
			if (EnemyInView(enemy, true))
				return &enemy;
		}
		return NULL;
	}

	bool FindAndAttack()
	{
		if (mLogic.mNow < mFindEnemyWait)
			return false;
		mFindEnemyWait = mLogic.mNow + mCfg.FindEnemyWait;
		const SSceneActor* enemy = FindViewEnemy();
		if (!enemy)
			return false;
		return BeginAttack(*enemy);
	}

	void SetTarget(float x, float z)
	{
		mTargetPoint.x = x;
		mTargetPoint.z = z;
		mTargetDeg = CalcTargetDeg(x, z);
	}

	void SetNextPath(const SAITarget* target)
	{
		if (mPathTarget)
		{
			mPathStart.x = mPathTarget->posx;
			mPathStart.z = mPathTarget->posz;
		}
		else
		{
			mPathStart.x = mActor.pos[0];
			mPathStart.z = mActor.pos[2];
		}
		mPathTarget = target;
		mCurPathLen = 0;
		mPathLenC = sqrt(mPathStart.x*mPathStart.x + mPathStart.z*mPathStart.z);
		if (!target)
		{
			mPathLenC = mPathLen = 0;
			mPathFactor.x = mPathFactor.z = 0;
			return;
		}
		float x = target->posx + (Rand() - 0.5f) * target->sizex;
		float z = target->posz + (Rand() - 0.5f) * target->sizez;
		if (!CanWalkTo(x, z))
		{
			x = target->posx;
			z = target->posz;
		}
		float dx = x - mPathStart.x;
		float dz = z - mPathStart.z;
		mPathLen = sqrt(dx*dx + dz*dz);
		if (mPathLen > 0)
		{
			mPathFactor.x = dx / mPathLen;
			mPathFactor.z = dz / mPathLen;
		}
		else
		{
			mPathFactor.x = mPathFactor.z = 0;
		}

		_dbg("SetNextPath (%f,%f)->(%f,%f)\r\n", mPathStart.x, mPathStart.z, x, z);
	}

	void SetRandTarget(float x, float z, SRobotConfig::SRangef range)
	{
		float len = Rand(range);
		float rad = PI_F * 2 * Rand();
		x += sinf(rad) * len;
		z += cosf(rad) * len;
		SetTarget(x, z);
	}

	bool TriggerAI()
	{
        if (mCfg.IgnoreAI == 1)
            return false;
		const SAIZone* ai = mLogic.mData.GetAI(mActor.pos[0], mActor.pos[2]);
		if (ai == mLastAi)
			return false;
		mLastAi = ai;
		mAiDeg = -1;
		if (!ai)
			return false;
		_dbg("TriggerAI %d (%f,%f)\r\n", ai->ai_id, mActor.pos[0], mActor.pos[2]);
		if (ai->look_angle >= 0)
		{
			float deg = fabsf(fmodf(ai->look_angle - mActor.pos[4], 360));
			if (deg > 180)
				deg = 360 - deg;
			if (deg <= 90)
				mAiDeg = ai->look_angle;
		}
		if (ai->target_count > 0)
			mTargetAi = ai;
		if (ai->call_back)
			mLogic.CallScript("core_trigger_ai", mActor.id, ai->ai_id);
		return true;
	}

	bool DoShoot()
	{
		if (!mAttackEnemy)
			return false;

		// TODO: 弹药判断

		uint8_t buf[sizeof(toc_base)+sizeof(HitInfoArrayToc)+sizeof(toc_fight_fire)];	// robot最多命中一人
		toc_base * proto = (toc_base *)buf;
		((toc_base *)buf)->proto_id = pid_fight_fire;

		uint8_t * p = (uint8_t *)(proto + 1);

		HitInfoArrayToc* sndAry = (HitInfoArrayToc*)p;
		p_hit_info_tos recvInfo;
		recvInfo.actorId = mAttackEnemy->id;
		recvInfo.bodyPart = 2;
		recvInfo.matType = 0;
		recvInfo.pos.x = mAttackEnemy->pos[0];
		recvInfo.pos.y = mAttackEnemy->pos[1];
		recvInfo.pos.z = mAttackEnemy->pos[2];
        struct SMakeHitInfoParams params(&mActor, &sndAry->ary[0], &recvInfo, mActor.cur_attack_type);
		if (RandRate(mCfg.AttackDamageRate) && mLogic.MakeHitInfo(params))
			sndAry->len = 1;
		else
			sndAry->len = 0;
		p += sndAry->Size();
		
		uint16_t& redirectCount = *(uint16_t *)p;
		redirectCount = 0;
		p += sizeof(uint16_t);

		toc_fight_fire& toc = *(toc_fight_fire*)p;
		toc.from = mActor.id;
		toc.hitPos.x = mAttackEnemy->pos[0];
		toc.hitPos.y = mAttackEnemy->pos[1] + 1.52f;
		toc.hitPos.z = mAttackEnemy->pos[2];
		toc.fire_len = 3;
		toc.firePos[0] = (short)(mActor.pos[0] * 100);
		toc.firePos[1] = (short)(mActor.pos[1] * 100);
		toc.firePos[2] = (short)(mActor.pos[2] * 100);
		toc.notPlayAni = 0;
		toc.atkType = 1;
		toc.sid = 0;

		if (sndAry->len <= 0)	// 未命中，偏移子弹方向
		{
			float x = mAttackEnemy->pos[0] - mActor.pos[0];
			float z = mAttackEnemy->pos[2] - mActor.pos[2];
			float l = sqrt(x*x + z*z);
			if (l > 0)
			{
				l = (mLogic.Rand(2) - .5f) * .8f / l;
				toc.hitPos.x += l * z;
				toc.hitPos.z += l * x;
			}
		}

		p += sizeof(toc);

		mLogic.BroadcastProto(proto, p - buf);
		return true;
	}

	bool DoMelee()
	{
		if (!mAttackEnemy)
			return false;

		toc_fight_atk toc_atk;
		toc_atk.firePose = mLogic.Rand(mCfg.AttackPose);
		toc_atk.fireDir.x = 0;
		toc_atk.fireDir.y = 0;
		toc_atk.fireDir.z = 0;
		toc_atk.index = 0;
		toc_atk.atkType = 0;
		toc_atk.from = mActor.id;
		toc_atk.sid = 0;
		mLogic.BroadcastProto(toc_atk);

		uint8_t buf[sizeof(uint32_t)+sizeof(HitInfoArrayToc)+sizeof(toc_fight_fire)];	// robot最多命中一人
		toc_base * proto = (toc_base *)buf;
		((toc_base *)buf)->proto_id = pid_fight_fire;

		uint8_t * p = (uint8_t *)(proto + 1);

		HitInfoArrayToc* sndAry = (HitInfoArrayToc*)p;
		p_hit_info_tos recvInfo;
		recvInfo.actorId = mAttackEnemy->id;
		recvInfo.bodyPart = 2;
		recvInfo.matType = 0;
		recvInfo.pos.x = mAttackEnemy->pos[0];
		recvInfo.pos.y = mAttackEnemy->pos[1];
		recvInfo.pos.z = mAttackEnemy->pos[2];
        struct SMakeHitInfoParams params(&mActor, &sndAry->ary[0], &recvInfo, mActor.cur_attack_type);
		if (RandRate(mCfg.AttackDamageRate) && mLogic.MakeHitInfo(params))
			sndAry->len = 1;
		else
			sndAry->len = 0;
		p += sndAry->Size();
		
		uint16_t& redirectCount = *(uint16_t *)p;
		redirectCount = 0;
		p += sizeof(uint16_t);

		toc_fight_fire& toc = *(toc_fight_fire*)p;
		toc.from = mActor.id;
		toc.hitPos.x = mAttackEnemy->pos[0];
		toc.hitPos.y = mAttackEnemy->pos[1] + 1.52f;
		toc.hitPos.z = mAttackEnemy->pos[2];
		toc.notPlayAni = 0;
		toc.fire_len = 3;
		toc.firePos[0] = (short)(mActor.pos[0] * 100);
		toc.firePos[1] = (short)(mActor.pos[1] * 100);
		toc.firePos[2] = (short)(mActor.pos[2] * 100);
		toc.atkType = 1;
		toc.sid = 0;
		p += sizeof(toc);

		mLogic.BroadcastProto(proto, p - buf);

		return true;
	}

	void DoMove()
	{
		const SActorProp& prop = mLogic.GetProp(mActor);
		if ((prop.LimitState & SSceneActor::LimitStateMove) > 0)
			return;
				
		float deg;
		if (mState != StateAttack && mAiDeg >= 0)
			deg = mAiDeg;
		else
			deg = mTargetDeg;

		float * curpos = mActor.pos;
		float x = curpos[0];
		float z = curpos[2];
		if (MoveSpeed() <= 0 && mCfg.MoveSpeed > 0)
			deg = curpos[4];
		if (x == mTargetPoint.x && z == mTargetPoint.z && curpos[4] == deg)
			return;

		// 遍历所有活人，求斥力和
		float fx = 0, fz = 0;
		float ds_max = mCfg.RepulsionDistance * mCfg.RepulsionDistance;
		for (int i = 0; i < CSceneLogic::MAX_ACTOR_COUNT; i++)
		{
			const SSceneActor* actor = mLogic.GetActor(i);
			if (actor == NULL || actor->state != SSceneActor::StateAlive
				|| (mCfg.AttackType == 1 && actor->camp != mActor.camp))
				continue;
			float dx = x - actor->pos[0];
			float dz = z - actor->pos[2];
			float ds = dx*dx + dz*dz;
			if (ds <= 0)	// 两人完全重叠
				continue;	// TODO: 应该随机一下
			if (ds >= ds_max)	// 不在排斥半径内
				continue;
			float k = mCfg.RepulsionDistance / ds * mCfg.RepulsionForce;
			fx += dx * k;
			fz += dz * k;
		}

		if (fx*fx + fz*fz < 1 && IsNearTarget())	// 斥力合较小，且离目标较近，直接移至目标点
		{
			x = mTargetPoint.x;
			z = mTargetPoint.z;
		}
		else
		{
			float k = TargetDistance();
			if (k > 0)
			{
				// 目标引力
				fx += (mTargetPoint.x - x) / k;
				fz += (mTargetPoint.z - z) / k;
			}

			k = sqrt(fx*fx + fz*fz);
			if (k > 0)
			{
				k = MoveSpeed() / k;
				x += fx * k;
				z += fz * k;
			}
		}

		float y = mLogic.mData.GetHeight(x, z);

		float tmpX = mTargetPoint.x;
		float tmpZ = mTargetPoint.z;
		float d = y - curpos[1];
		if (d < mCfg.StepHeight.min || d > mCfg.StepHeight.max || !GoRoute(x, z))	// 路径不可走
		{
			_dbg("MoveBlock %d (%f,%f)\r\n", mState, curpos[0], curpos[2]);
			switch (mState)
			{
			case StateMove:	// 移动中遇障碍，开始巡逻
				BeginPatrol(curpos[0], curpos[2]);
				break;
			case StatePatrol:	// 巡逻中遇障碍，随机新目标点
				SetRandTarget(curpos[0], curpos[2], mCfg.PatrolDistance);
				break;
			case StateAttack:	// 攻击平移中遇障碍，停止平移
				//mTargetPoint.x = curpos[0];
				//mTargetPoint.z = curpos[2];
				SetRandTarget(curpos[0], curpos[2], mCfg.PatrolDistance);
				break;
			default:
				{
					SRobotConfig::SRangef range = mCfg.PatrolDistance;
					if (range.min == 0) range.min = 0.5f;
					if (range.max == 0) range.max = 2.f;
					SetRandTarget(curpos[0], curpos[2], range);
				}
				break;
			}
			return;	// 此步不走了，等待下一次Update
		}
		mTargetPoint.x = tmpX;
		mTargetPoint.z = tmpZ;

		curpos[0] = x;
		curpos[1] = y;
		curpos[2] = z;
		curpos[4] = deg;

		mLogic.SendMove(mActor);

		if (mPathLen <= 0)
			return;

		float cpl = x * mPathFactor.x + z * mPathFactor.z + mPathLenC;
		if (cpl > mPathLen)
			cpl = mPathLen;
		if (cpl > mCurPathLen)
			mCurPathLen = cpl;
	}

	const SAITarget* SelectTarget()
	{
		if (mTargetAi == NULL)
			return NULL;

		const int MAX_RTARS = 5;
		int rtars = 0;
		const SAITarget* rtar[MAX_RTARS];
		float mindeg = 180;
		const SAITarget* mintar = NULL;
		for (int i = 0; i < mTargetAi->target_count; i++)
		{
			const SAITarget& tar = mTargetAi->targets[i];
			if (!CanAttackTo(tar.posx, tar.posz))
				continue;
			float deg = ViewPosDegAbs(tar.posx, tar.posz);
			if (deg <= mCfg.TurnDeg && rtars < MAX_RTARS)
				rtar[rtars++] = &tar;
			if (deg < mindeg)
			{
				mindeg = deg;
				mintar = &tar;
			}
		}

		if (rtars > 0)
			return rtar[mLogic.Rand(rtars)];

		return mintar;
	}

	void BeginMove(const SAITarget* target)
	{
		if (target)
			SetNextPath(target);

		if (!mPathTarget)
		{
			BeginPatrol(mActor.pos[0], mActor.pos[2]);
			return;
		}

		float x = mPathStart.x + mPathLen * mPathFactor.x;
		float z = mPathStart.z + mPathLen * mPathFactor.z;
		_dbg("BeginMove (%f,%f)->(%f,%f)\r\n", mActor.pos[0], mActor.pos[2], x, z);

		if (!CanWalkTo(x, z) && mCfg.RoutePath > 0 && GoRoute(x, z))
		{
			mPathRouting = true;
		}
		else
		{
			mTargetPoint.x = mActor.pos[0];
			mTargetPoint.z = mActor.pos[2];
		}

		mState = StateMove;
	}

	void BeginPatrol(float x, float z)
	{
        if (mCfg.PatrolDistance.max <= 0)
            return;
		_dbg("BeginPatrol (%f,%f)->(%f,%f)\r\n", mActor.pos[0], mActor.pos[2], x, z);
		SetRandTarget(x, z, mCfg.PatrolDistance);
		mMoveWait = WaitRandTime(mCfg.PatrolWait);
		mState = StatePatrol;
	}

	bool BeginAttack(const SSceneActor& enemy)
	{
		if (mCfg.AttackDamageRate < 0)
			return false;
		_dbg("BeginAttack (%f,%f)->(%f,%f)%d\r\n", mActor.pos[0], mActor.pos[2], enemy.pos[0], enemy.pos[2], enemy.id);
		mTargetPoint.x = mActor.pos[0];
		mTargetPoint.z = mActor.pos[2];
		mTargetDeg = CalcTargetDeg(enemy.pos[0], enemy.pos[2]);
		mAttackEnemy = &enemy;
		mAttackWait = WaitRandTime(mCfg.FirstAttackWait);
		mMoveWait = WaitRandTime(mCfg.FirstAttackMoveWait);
		mAttackLostWait = 0;
		mState = StateAttack;
		return true;
	}

	bool BeginRoute(const SSceneActor& enemy)
	{
		_dbg("BeginRoute (%f,%f)->(%f,%f)%d\r\n", mActor.pos[0], mActor.pos[2], enemy.pos[0], enemy.pos[2], enemy.id);
		if (!GoRoute(enemy.pos[0], enemy.pos[2]))
			return false;
		mAttackEnemy = &enemy;
		mState = StateRoute;
		return true;
	}

	void UpdateMove()
	{
		if (FindAndAttack())
			return;

		const SAIZone* ai = mLogic.mData.GetAI(mPathTarget->posx, mPathTarget->posz);
		if ((ai && ai == mLastAi)	// 到达目标ai区域
			|| (!ai && IsNearTarget()))	// 接近目标点
		{
			OnIdle();
			return;
		}

		if (mPathRouting)
		{
			if (!IsNearTarget())
				return;

			float x = mPathStart.x + mPathLen * mPathFactor.x;
			float z = mPathStart.z + mPathLen * mPathFactor.z;
			if (GoRoute(x, z))
				return;

			mPathRouting = false;
		}

		float len = mCurPathLen + MoveSpeed() * 10;
		if (len > mPathLen)
			len = mPathLen;

		SetTarget(mPathStart.x + mPathFactor.x * len, mPathStart.z + mPathFactor.z * len);
	}

	void UpdatePatrol()
	{
		if (FindAndAttack())
			return;
		if (mLogic.mNow >= mMoveWait)
			OnIdle();
	}

	void UpdateAttack()
	{
		const SActorProp& prop = mLogic.GetProp(mActor);
		if ((prop.LimitState & SSceneActor::LimitStateFire) > 0)
			return;

	    const SSceneActor* enemy = mAttackEnemy;
        if (enemy)
        {   
	        if (!EnemyInView(*enemy, false))
		        enemy = NULL;
		}
		if (mCfg.CheckHate > 0)
		{
			int maxHate = 0;
			float minDis = 0;
			const SSceneActor* hateEnemy = NULL;
			for (int i = 0; i < CSceneLogic::MAX_ACTOR_COUNT; ++i)
			{
				const SSceneActor* actor = mLogic.GetActor(i);
				if ( actor == NULL || !EnemyInView(*actor, true) )
					continue;
				int hate = 0;
				if ( actor->player )
				{
					hate = 100;
				}
				else if (actor->robot)
				{
					hate = actor->robot->mCfg.Hate;
				}
				if (hateEnemy == NULL || hate > maxHate)
				{
					hateEnemy = actor;
					maxHate = hate;
					float dx = mActor.pos[0] - actor->pos[0];
					float dz = mActor.pos[2] - actor->pos[2];
					minDis = dx * dx + dz * dz;					
				}
				else if (hate == maxHate)
				{
					float dx = mActor.pos[0] - actor->pos[0];
					float dz = mActor.pos[2] - actor->pos[2];
					float dis = dx * dx + dz * dz;
					if (dis < minDis)
					{
						hateEnemy = actor;
						minDis = dis;
					}
				}
			}
			if (hateEnemy)
				enemy = hateEnemy;
		}
		if (!enemy)
			enemy = FindViewEnemy();
		if (enemy)
		{
			mAttackEnemy = enemy;
			int atktype = mCfg.AttackType;
			if (atktype == 1)	// 近战
			{
				float dx = enemy->pos[0] - mActor.pos[0];
				float dz = enemy->pos[2] - mActor.pos[2];

				if (dx * dx + dz * dz >= mCfg.AttackDist * mCfg.AttackDist)    // 距离太远，移向目标
				{
					SetTarget(enemy->pos[0], enemy->pos[2]);
					mMoveWait = WaitRandTime(mCfg.AttackMoveWait);
					enemy = NULL;
				}
				else
				{
					mTargetPoint.x = mActor.pos[0];
					mTargetPoint.z = mActor.pos[2];
				}
			}
			if (enemy)
			{
				mTargetDeg = CalcTargetDeg(enemy->pos[0], enemy->pos[2]);
				if (mLogic.mNow >= mAttackWait)
				{
					if (atktype == 0)
						DoShoot();
					else if (atktype == 1)
						DoMelee();
					mAttackWait = WaitRandTime(mCfg.AttackWait);
				}
			}
		}
		else if (mAttackLostWait > 0)
		{
			if (mLogic.mNow > mAttackLostWait)
			{
				if (mCfg.RoutePath <= 0 || !mAttackEnemy || !BeginRoute(*mAttackEnemy))
				{
					mAttackEnemy = NULL;
					OnIdle();
					return;
				}
			}
		}
		else
		{
			mAttackLostWait = WaitRandTime(mCfg.AttackLostWait);
		}
		if (enemy && mLogic.mNow >= mMoveWait)
		{
			float x = enemy->pos[0] - mActor.pos[0];
			float z = enemy->pos[2] - mActor.pos[2];
			float l = sqrt(x*x + z*z);
			if (l<.1f)
			{
				x = l = 1;
				z = 0;
			}
			l = Rand(mCfg.AttackMoveDistance) / l;
			if (mLogic.Rand(2))
				l = -l;
			x = mActor.pos[0] + l * z;
			z = mActor.pos[2] + l * x;
			if (mLogic.mData.PosCanArrive(x, z, enemy->pos[0], enemy->pos[2], -1.5, 1.5))	// 保证移动后敌人仍在视野
			{
				mTargetPoint.x = x;
				mTargetPoint.z = z;
			}
			mMoveWait = WaitRandTime(mCfg.AttackMoveWait);
		}
	}

	void UpdateRoute()
	{
		if (FindAndAttack())
			return;
		if (mAttackEnemy && !EnemyValid(*mAttackEnemy))
			mAttackEnemy = NULL;
		if (!mAttackEnemy)
		{
			OnIdle();
			return;
		}
		bool near = IsNearTarget();
		if (near || mFindEnemyWait == mLogic.mNow + mCfg.FindEnemyWait)	// 到了寻敌时间（刚刚做过寻敌）
		{
			if (!GoRoute(mAttackEnemy->pos[0], mAttackEnemy->pos[2]))
			{
				if (near)
				{
					mAttackEnemy = NULL;
					OnIdle();
				}
			}
		}
	}

	bool GoRoute(float x, float z)
	{
		const SRoutePoint* myrp = mLogic.mData.GetRoutePoint(mActor.pos[0], mActor.pos[2]);
		const SRoutePoint* tarrp = mLogic.mData.GetRoutePoint(x, z);
		if (!myrp || !tarrp || myrp->zone_id != tarrp->zone_id)
		{
			_dbg("GoRoute failed (%f,%f)[%d] -> (%f,%f)[%d]\r\n", mActor.pos[0], mActor.pos[2], myrp?myrp->point_id:0, x, z, tarrp?tarrp->point_id:0);
			return false;
		}
		if (CanWalkTo(x, z))
		{
			_dbg("GoRoute direct (%f,%f) -> (%f,%f)\r\n", mActor.pos[0], mActor.pos[2], x, z);
			SetTarget(x, z);
			return true;
		}
		const SRoutePoint* next = FindRoute(myrp->point_id, tarrp->point_id);
		if (!next)
		{
			_dbg("FindRoute failed %d-%d -> %d-%d\r\n", myrp->zone_id, myrp->point_id, tarrp->zone_id, tarrp->point_id);
			return false;
		}
		float dx = myrp->posx - mActor.pos[0];
		float dz = myrp->posx - mActor.pos[2];
		if (dx*dx + dz*dz <= MoveSpeed() * MoveSpeed() || CanWalkTo(next->posx, next->posz))
		{
			SetTarget(next->posx, next->posz);
			_dbg("GoRoute next (%f,%f)[%d] -> (%f,%f)[%d] -> (%f,%f)[%d]\r\n", mActor.pos[0], mActor.pos[2], myrp->point_id, next->posx, next->posz, next->point_id, x, z, tarrp->point_id);
		}
		else
		{
			SetTarget(myrp->posx, myrp->posz);
			_dbg("GoRoute cur (%f,%f)[%d] -> (%f,%f)[%d] -> (%f,%f)[%d]\r\n", mActor.pos[0], mActor.pos[2], myrp->point_id, next->posx, next->posz, next->point_id, x, z, tarrp->point_id);
		}
		return true;
	}

	const SRoutePoint* FindRoute(int fromid, int toid)
	{
		const SRoutePoint* idpt = mLogic.mData.mRoutePointArray - 1;
		if (fromid == toid)
			return &idpt[fromid];
		bool* idrh = mRouteReached - 1;
		SSearchPath * last = mRoutePath;
		last->parent = 0;
		last->id = fromid;
		idrh[fromid] = true;
		last++;
		for (SSearchPath* cur = mRoutePath; cur < last; cur++)
		{
			const SRoutePoint& pt = idpt[cur->id];
			for (int n = 0; n < pt.reach_count; n++)
			{
				int id = pt.reaches[n];
				if (id == toid)
				{
					while (--last >= mRoutePath)
						idrh[last->id] = false;
					if (cur == mRoutePath)
						return &idpt[id];
					while (cur->parent)
						cur = &mRoutePath[cur->parent];
					return &idpt[cur->id];
				}
				if (idrh[id])
					continue;
				last->parent = cur - mRoutePath;
				last->id = id;
				idrh[id] = true;
				last++;
			}
		}
		while (--last >= mRoutePath)
			idrh[last->id] = false;
		return NULL;
	}

	void OnIdle()
	{
		_dbg("OnIdle %d (%f,%f)\r\n", mState, mActor.pos[0], mActor.pos[2]);
		if (mCfg.RoutePath >= 2)
		{
			float minds = 0;
			const SSceneActor* mina = NULL;
			for (int i = 0; i < CSceneLogic::MAX_ACTOR_COUNT; i++)
			{
				const SSceneActor* actor = mLogic.GetActor(i);
				if (actor == NULL || !EnemyValid(*actor))
					continue;
				float dx = actor->pos[0] - mActor.pos[0];
				float dz = actor->pos[2] - mActor.pos[2];
				float ds = dx*dx + dz*dz;
				if (mCfg.RoutePath == 3 && actor->robot)	// 降低寻到ai的优先级
					ds += 10000;
				if (mina == NULL || ds < minds)
				{
					minds = ds;
					mina = actor;
				}
			}

			if (mina && BeginRoute(*mina))
				return;
		}

		if (mState != StateMove && mPathTarget)
		{
			BeginMove(NULL);
			return;
		}

		const SAITarget* tar = NULL;
		if (!RandRate(mCfg.MovePatrolRate))
			tar = SelectTarget();
		if (tar)
			BeginMove(tar);
		else if (mPathTarget)
			BeginPatrol(mPathStart.x + mCurPathLen * mPathFactor.x, mPathStart.z + mCurPathLen * mPathFactor.z);
		else
			BeginPatrol(mActor.pos[0], mActor.pos[2]);
	}


public:
	void Reset()
	{
		mState = StateNone;
		mLastAi = NULL;
		mTargetAi = NULL;
		mTargetPoint.x = mActor.pos[0];
		mTargetPoint.z = mActor.pos[2];
		mActor.pos[1] = mLogic.mData.GetHeight(mTargetPoint.x, mTargetPoint.z);
		mTargetDeg = mActor.pos[4];
		mPathTarget = NULL;
		mPathRouting = false;
		SetNextPath(NULL);
		mAiDeg = -1;
		mHurtWait = 0;
		mFindEnemyWait = 0;
		mMoveWait = 0;
		mAttackWait = 0;
		mAttackLostWait = 0;
		mAttackEnemy = NULL;
		mAIUpdateTime = 0;
	}

    void UpdateAIState(int state = StateNone)
    {
        mState = (State)state;
    }

	void Update()
	{
		if (mLogic.mNow < mHurtWait)
			return;
        else if (mState == StateLuaAI)
            return;
        else if (mLogic.mNow - mAIUpdateTime < g_AIUpdateInterval)
            return;

        mAIUpdateTime = mLogic.mNow;
		TriggerAI();

		if (mState == StateNone)
			OnIdle();

		switch (mState)
		{
		case StateNone:
			return;
		case StateMove:
			UpdateMove();
			break;
		case StatePatrol:
			UpdatePatrol();
			break;
		case StateAttack:
			UpdateAttack();
			break;
		case StateRoute:
			UpdateRoute();
			break;
		default:
			return;
		}
		mActor.last_act_time = mLogic.mNow;
		DoMove();
	}

	void OnDamage(const SSceneActor& from)
	{
		mHurtWait = mLogic.mNow + mCfg.HurtWait;
		if ((mState != StateAttack || mAttackLostWait > 0) && mState != StateRoute && mState != StateLuaAI)
			BeginAttack(from);
	}
		
	bool IgnoreHurt(const SSceneActor& enemy)
	{
		switch (mCfg.IgnoreHurt)
		{
		case 1:
			if (enemy.player)
				return true;
			break;
		case 2:
			if (enemy.robot)
				return true;
			break;
		}
		return false;
	}
};

