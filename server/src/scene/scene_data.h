#pragma once


struct SAITarget
{
	float posx, posz;
	float sizex, sizez;
};

struct SAIZone
{
	int ai_id;
	bool call_back;
	float look_angle;
	int target_count;
	SAITarget targets[10];
};

struct SRoutePoint
{
	int point_id;
	int zone_id;
	float posx, posz;
	int reach_count;
	int reaches[20];
};

class CSceneData
{
public:
	const static int ROUTEPOINT_COUNT = 4000;
	const static int POINT_LEN = 10;

	int mSizeX;
	int mSizeZ;
	float mXMin, mXMax;
	float mYMin, mYMax;
	float mZMin, mZMax;

	BYTE* mHeightData;
	BYTE* mAIData;
	uint16_t* mRoutePointData;

	SAIZone mAIArray[1000];
	SRoutePoint mRoutePointArray[ROUTEPOINT_COUNT];

	CSceneData();

	~CSceneData();

	void InitData();

	void FillAIData(int id, float posx, float posz, int shape, float sizex, float sizez);

	inline float GetHeight(float x, float z) const
	{
		int px = (int)((x - mXMin) * POINT_LEN);
		int pz = (int)((z - mZMin) * POINT_LEN);
		if (px < 0 || px >= mSizeX || pz < 0 || pz >= mSizeZ)
			return mYMax;
		return mHeightData[pz * mSizeX + px] * (mYMax - mYMin) / 255 + mYMin;
	}

	inline const SAIZone* GetAI(float x, float z) const
	{
		int px = (int)((x - mXMin) * POINT_LEN);
		int pz = (int)((z - mZMin) * POINT_LEN);
		if (px < 0 || px >= mSizeX || pz < 0 || pz >= mSizeZ)
			return NULL;
		int id = mAIData[pz * mSizeX + px];
		if (id <= 0)
			return NULL;
		return &mAIArray[id - 1];
	}

	inline const SRoutePoint * GetRoutePoint(float x, float z) const
	{
		int px = (int)((x - mXMin) * POINT_LEN);
		int pz = (int)((z - mZMin) * POINT_LEN);
		if (px < 0 || px >= mSizeX || pz < 0 || pz >= mSizeZ)
			return NULL;
		int id = mRoutePointData[pz * mSizeX + px];
		if (id <= 0 || id > countof(mRoutePointArray))
			return NULL;
		return &mRoutePointArray[id - 1];
	}

	bool PosCanArrive(float fromx, float fromz, float tox, float toz, float mindh, float maxdh) const;
	bool PosCanAttack(float fromx, float fromz, float tox, float toz, float maxdh) const;
};

#define MATERIAL_MAX 100

struct MaterialDef
{
	int penetrate;
	float damageRadio;
};

extern MaterialDef g_MaterialDef[MATERIAL_MAX];
extern float g_AllowMoveDelay;
extern float g_MaxMoveDelayPunish;
extern float g_AllowFireDelay;
extern bool g_CheckGroundHeight;
extern bool g_CheckMaxHeight;
