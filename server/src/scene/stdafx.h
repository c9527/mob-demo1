#include <stdint.h>
#include <stddef.h>
#include <memory.h>

#define POS_SIZE 6

#define PTYPE_UDP_DATA 200

typedef unsigned char BYTE;

#define ZeroStruct(x) memset(&(x), 0, sizeof(x))
#define ZeroArray(x) memset(&(x)[0], 0, sizeof(x))

#define countof(x) ((int)(sizeof(x)/sizeof((x)[0])))

inline void poscpy(float dst[], const float src[])
{
	memcpy(dst, src, sizeof(float)* POS_SIZE);
}
