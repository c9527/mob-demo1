
#include "stdafx.h"
#include "lua_def.h"
#include "scene_data.h"
#include "scene_logic.h"
#include "robotconfig.h"
#include "actor_player.h"
#include "actor_robot.h"
#include <zlib.h>

extern "C"
{
#include <skynet.h>
#include <skynet_socket.h>
#include <skynet_timer.h>
	//#include <skynet_server.h>
	uint32_t skynet_context_handle(struct skynet_context *);
	int skynet_context_newsession(struct skynet_context *);
}

MaterialDef g_MaterialDef[MATERIAL_MAX] = {};
float g_AllowMoveDelay = 2;
float g_MaxMoveDelayPunish = 5;
float g_AllowFireDelay = 1;
bool g_CheckGroundHeight = false;
int g_AIUpdateInterval = 1000 / CSceneLogic::COUNT_PER_SECOND;
bool g_CheckMaxHeight = false;

class SceneEnv
{
public:
	skynet_context * ctx;
	skynet_cb oldCb;
	lua_State * mainL;
	CSceneLogic logic;
	int timer;

	//struct skynet_context {
	//	void * instance;
	//	struct skynet_module * mod;
	//	void * cb_ud;
	//	skynet_cb cb;
	//	...

	SceneEnv(CSceneData& data, lua_State *L) : logic(data)
	{
		ctx = (skynet_context *)lua_touserdata(L, lua_upvalueindex(1));
		mainL = ((lua_State **)ctx)[2];
		oldCb = ((skynet_cb *)ctx)[3];
		logic.SetLuaState(mainL);
		RegUpdateTimeOut();
	}

	void RegUpdateTimeOut()
	{
		uint32_t source = skynet_context_handle(ctx);
		timer = skynet_context_newsession(ctx);
		skynet_timeout(source, 10, timer);
	}

	~SceneEnv()
	{
	}
};

static int _cb(skynet_context * context, void *ud, int type, int session, uint32_t source, const void * msg, size_t sz)
{
	SceneEnv * env = (SceneEnv *)ud;
	if (type == PTYPE_UDP_DATA)
	{
		const sockaddr_in * addr = (const sockaddr_in *)msg;
		env->logic.HandleProto(*addr, (uint32_t *)(addr + 1), sz - sizeof(sockaddr_in));
		return 0;
	}
	if (type == PTYPE_RESPONSE && session == env->timer)
	{
		env->logic.Update();
		env->RegUpdateTimeOut();
		return 0;
	}

	if (!env->oldCb)
		return 0;
	return env->oldCb(context, env->mainL, type, session, source, msg, sz);
}

static int lUdpGateStart(lua_State *L)
{
	extern bool start_udp_gate(int port);
	int port = (int)luaL_checkinteger(L, 1);
	if (!start_udp_gate(port))
		luaL_error(L, "udp gate start error!");
	return 0;
}

static int lUdpGateStop(lua_State *L)
{
	extern bool stop_udp_gate();
	stop_udp_gate();
	return 0;
}

static CSceneData& _GetData(lua_State *L)
{
	void * p = lua_touserdata(L, 1);
	luaL_argcheck(L, p != NULL, 1, "need scene data");
	return *(CSceneData*)p;
}

static SceneEnv& _GetEnv(lua_State *L)
{
	skynet_context * ctx = (skynet_context *)lua_touserdata(L, lua_upvalueindex(1));
	SceneEnv * env = ((SceneEnv **)ctx)[2];
	if (!env || env->ctx != ctx)
		luaL_error(L, "SceneEnv error!");
	return *env;
}

static CSceneLogic& _GetLogic(lua_State *L)
{
	return _GetEnv(L).logic;
}

static int _copy_data(BYTE * dest, uLongf dest_len, const BYTE * src, size_t src_len)
{
    if ((*(const uint16_t *)src) != 0xda78)    // zip 头
    {
		if (src_len != dest_len)
		    return -1;
        memcpy(dest, src, src_len);
        return 0;
    }
    
    int ret = ::uncompress(dest, &dest_len, src, (uLong)src_len);
    if (ret != Z_OK)
        return -2;
    return 0;
}

static int lCreateData(lua_State *L)
{
	CSceneData* scene = new CSceneData();
	scene->mSizeX = lua_toint(L, 1);
	scene->mSizeZ = lua_toint(L, 2);
	scene->mXMin = lua_tofloat(L, 3);
	scene->mXMax = lua_tofloat(L, 4);
	scene->mYMin = lua_tofloat(L, 5);
	scene->mYMax = lua_tofloat(L, 6);
	scene->mZMin = lua_tofloat(L, 7);
	scene->mZMax = lua_tofloat(L, 8);
	scene->InitData();

	// 注：以下如果出错，没有释放内存，建议重启服务器

	size_t heightlen;
	const BYTE* heightdata = (const BYTE*)luaL_checklstring(L, 9, &heightlen);
	if (_copy_data(scene->mHeightData, scene->mSizeX * scene->mSizeZ, heightdata, heightlen))
	    luaL_argerror(L, 9, "height data error");

	size_t routelen;
	const BYTE* routedata = (const BYTE*)lua_tolstring(L, 10, &routelen);
	if (routedata)
	{
		if (_copy_data((BYTE *)scene->mRoutePointData, scene->mSizeX * scene->mSizeZ * 2, routedata, routelen))
		   luaL_argerror(L, 10, "route data error");

		int pointlen = (int)luaL_len(L, 11);
		luaL_argcheck(L, pointlen <= countof(scene->mRoutePointArray), 11, "route point count out of range");
		for (int i = 0; i < pointlen; i++)
		{
			lua_geti(L, 11, i + 1);
			SRoutePoint& rp = scene->mRoutePointArray[i];
			rp.point_id = i + 1;
			rp.zone_id = lua_getiint(L, -1, 2);
			rp.posx = lua_getifloat(L, -1, 3);
			rp.posz = lua_getifloat(L, -1, 4);
			lua_geti(L, -1, 5);
			rp.reach_count = (int)luaL_len(L, -1);
			luaL_argcheck(L, rp.reach_count <= countof(rp.reaches), 11, "reach count out of range");
			for (int j = 0; j < rp.reach_count; j++)
				rp.reaches[j] = lua_getiint(L, -1, j + 1);
			lua_pop(L, 2);
		}
	}

	lua_pushlightuserdata(L, scene);

	return 1;
}

static int lSceneAddAI(lua_State *L)
{
	CSceneData& scene = _GetData(L);

	int id = lua_toint(L, 2);
	luaL_argcheck(L, id > 0 && id <= countof(scene.mAIArray), 2, "ai id out of range");
	SAIZone& ai = scene.mAIArray[id - 1];

	int len = (int)luaL_len(L, 9);
	luaL_argcheck(L, len <= countof(ai.targets), 9, "target count out of range");

	scene.FillAIData(id, lua_tofloat(L, 3), lua_tofloat(L, 4), lua_toint(L, 5), lua_tofloat(L, 6), lua_tofloat(L, 7));

	ai.ai_id = id;
	ai.call_back = lua_toboolean(L, 10);
	ai.look_angle = lua_tofloat(L, 8);
	ai.target_count = len;
	for (int i = 0; i < len; i++)
	{
		lua_geti(L, 9, i + 1);
		SAITarget& target = ai.targets[i];
		target.posx = lua_getifloat(L, -1, 1);
		target.posz = lua_getifloat(L, -1, 2);
		target.sizex = lua_getifloat(L, -1, 3);
		target.sizez = lua_getifloat(L, -1, 4);
		lua_pop(L, 1);
	}

	lua_pushboolean(L, 1);
	return 1;
}

static int lSceneRelease(lua_State *L)
{
	CSceneData* scene = &_GetData(L);
	delete scene;
	return 0;
}

static int lLogicInit(lua_State *L)
{
	CSceneData& scene = _GetData(L);
	SceneEnv * env = new SceneEnv(scene, L);
	skynet_callback(env->ctx, env, _cb);
	return 0;
}

static int lLogicRelease(lua_State *L)
{
	SceneEnv * env = &_GetEnv(L);
	skynet_callback(env->ctx, env->mainL, env->oldCb);
	delete env;
	return 0;
}

#define _READ_INT_CFG(NAME) cfg->NAME = lua_getsint(L, 1, #NAME)
#define _READ_FLOAT_CFG(NAME) cfg->NAME = lua_getsfloat(L, 1, #NAME)
#define _READ_RANGE_CFG(NAME) {	\
	cfg->NAME.min = lua_getsint(L, 1, #NAME"Min");	\
	cfg->NAME.max = lua_getsint(L, 1, #NAME"Max");	\
}
#define _READ_RANGEF_CFG(NAME) {	\
	cfg->NAME.min = lua_getsfloat(L, 1, #NAME"Min");	\
	cfg->NAME.max = lua_getsfloat(L, 1, #NAME"Max");	\
}

static int lLogicSetState(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	lua_Integer state = luaL_checkinteger(L, 1);
	logic.SetState((CSceneLogic::GameState)state);
	return 0;
}

static int lLogicGetTime(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	lua_pushinteger(L, logic.Now());
	return 1;
}

static int lAddLogicRobot(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	luaL_checktype(L, 1, LUA_TTABLE);

	SRobotConfig* cfg;
	SSceneActor* actor = logic.AddRobot(cfg);
	if (!actor)
		return 0;

	_READ_FLOAT_CFG(MoveSpeed);
	_READ_RANGEF_CFG(StepHeight);
	_READ_FLOAT_CFG(RepulsionDistance);
	_READ_FLOAT_CFG(RepulsionForce);
	_READ_INT_CFG(MovePatrolRate);
	_READ_RANGE_CFG(PatrolWait);
	_READ_RANGEF_CFG(PatrolDistance);
	_READ_INT_CFG(FindEnemyWait);
	_READ_INT_CFG(RoutePath);
	_READ_FLOAT_CFG(ViewDeg);
	_READ_FLOAT_CFG(ViewDistance);
	_READ_FLOAT_CFG(TurnDeg);
	_READ_INT_CFG(HurtWait);
	_READ_INT_CFG(AttackDamageRate);
	_READ_INT_CFG(AttackType);
	_READ_FLOAT_CFG(AttackDist);
	_READ_INT_CFG(AttackPose);
	_READ_FLOAT_CFG(TraceDistance);
	_READ_RANGE_CFG(FirstAttackWait);
	_READ_RANGE_CFG(AttackWait);
	_READ_RANGE_CFG(FirstAttackMoveWait);
	_READ_RANGE_CFG(AttackMoveWait);
	_READ_RANGEF_CFG(AttackMoveDistance);
	_READ_RANGE_CFG(AttackLostWait);
	_READ_INT_CFG(IgnoreAI);
	_READ_INT_CFG(SelectType);
	_READ_INT_CFG(IgnoreHurt);
	_READ_INT_CFG(Hate);
	_READ_INT_CFG(CheckHate);

	actor->move_speed = cfg->MoveSpeed;

	lua_pushinteger(L, actor->id);
	return 1;
}

static int lAddLogicPlayer(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	uint32_t rid;
	SSceneActor* actor = logic.AddPlayer(rid);
	if (!actor)
		return 0;
	lua_pushinteger(L, actor->id);
	lua_pushinteger(L, rid);
	return 2;
}

static int lRemoveLogicActor(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	logic.RemoveActor(lua_toint(L, 1));
	return 0;
}

static int lRemoveLogicSpectator(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	logic.RemoveSpectator(lua_tointeger(L, 1));
	return 0;
}

static int lSetTeammateCure(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	logic.SetTeammateCure(lua_tonumber(L, 1));
	return 0;
}

static int lNearActors(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	float x = lua_tofloat(L, 1);
	float z = lua_tofloat(L, 2);
	float dis = lua_tofloat(L, 3);
	int camp = luaL_optinteger(L, 4, -1);
    bool same_camp = lua_toboolean(L, 5);
	lua_newtable(L);
	int count = 0;
	dis *= dis;
	for (int i = 0; i <  CSceneLogic::MAX_ACTOR_COUNT; i++)
	{
		SSceneActor * actor = logic.GetActor(i);
		if (!actor)
			continue;
		if (camp >= 0 && ((actor->camp == camp) != same_camp))
			continue;
		float dx = actor->pos[0] - x;
		float dz = actor->pos[2] - z;
		if(dx*dx + dz*dz <= dis)
			lua_setiint(L, -1, ++count, actor->id);
	}
	return 1;
}

static int lPosHeight(lua_State *L)
{
    const CSceneData& scene = _GetLogic(L).GetSceneData();
	float x = lua_tofloat(L, 1);
	float z = lua_tofloat(L, 2);
    float y = scene.GetHeight(x, z);
    lua_pushnumber(L, y);
    return 1;
}

static int lFixPosListHeight(lua_State *L)
{
    if (!lua_istable(L, 1))
        return 0;
    const CSceneData& scene = _GetLogic(L).GetSceneData();
    int count = (int)luaL_len(L, 1);
    for (int i = 1; i <= count; i++)
    {
        lua_geti(L, 1, i);
        float x = lua_getifloat(L, -1, 1), z = lua_getifloat(L, -1, 3);
        float y = scene.GetHeight(x, z);
        lua_setifloat(L, -1, 2, y);
        lua_pop(L, 1);
    }
    return 0;
}

static int lPosCanArrive(lua_State *L)
{
    float fx = lua_tofloat(L, 1);       // from
    float fz = lua_tofloat(L, 2);
    float tx = lua_tofloat(L, 3);       // to
    float tz = lua_tofloat(L, 4);
    float h_min = lua_tofloat(L, 5);    // step height
    float h_max = lua_tofloat(L, 6);
    const CSceneData& scene = _GetLogic(L).GetSceneData();
    bool can = scene.PosCanArrive(fx, fz, tx, tz, h_min, h_max);
    lua_pushboolean(L, can);
    float y = scene.GetHeight(tx, tz);
    lua_pushnumber(L, y);
    return 2;
}

static int lPosCanAttack(lua_State *L)
{
	float fx = lua_tofloat(L, 1);
	float fz = lua_tofloat(L, 2);
	float tx = lua_tofloat(L, 3);
	float tz = lua_tofloat(L, 4);
    const CSceneData& scene = _GetLogic(L).GetSceneData();
	bool can = scene.PosCanAttack(fx, fz, tx, tz, 1.5);
	lua_pushboolean(L, can);
	return 2;
}

static int lSetActorProtectData(lua_State *L)
{
    CSceneLogic& logic = _GetLogic(L);
    float dis = lua_tofloat(L, 1);
    luaL_argcheck(L, lua_istable(L, 2), 2, "need table");
    int count = (int)luaL_len(L, 2);
    luaL_argcheck(L, count == 3, 2, "table length must be 3");
    float cuts[3];
    for (int i = 0; i < 3; i++)
        cuts[i] = lua_getifloat(L, 2, i + 1);
    logic.SetActorProtectData(dis, cuts);
    return 0;
}

static int lSetDisableAttacks(lua_State *L)
{
    CSceneLogic& logic = _GetLogic(L);
    bool* disable_attacks = logic.GetDisableAttacks();
    if (!lua_istable(L, 1))
        return 0;
    lua_pushnil(L);
    while (lua_next(L, 1) != 0)
    {
        int atk_type = (int)lua_tointeger(L, -2);
        bool disable = lua_toboolean(L, -1);
        if (0 <= atk_type && atk_type < MAX_ATTACK_TYPE)
            disable_attacks[ atk_type ] = disable;
        lua_pop(L, 1);
    }
    return 0;
}

static int lSetDelayWatch(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	bool delayWatch = lua_toboolean(L, 1);
	logic.setDelayWatch(delayWatch);
	return 0;
}

static SSceneActor& _GetActor(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	int id = lua_toint(L, 1);
	SSceneActor* actor = logic.GetActor(id);
	if (!actor || !actor->id)
		luaL_error(L, "actor[%d] not found!", id);
	return *actor;
}

static int lUpdateActorDamageTime(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);
	actor.last_armor = logic.GetActorArmor(actor);
	actor.last_damage_time = logic.Now();
	return 0;
}

static int lUpdateActorActTime(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);
	logic.UpdateActorActTime(actor);
	return 0;
}

static int lSaveActorGrenadeProp(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	actor.prop_grenade = actor.prop;
	return 0;
}

static int lSaveActorAttackProp(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	int index = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= index && index < MAX_CACHED_ATKS, 2, "index error");
	int atk_type = (int)luaL_checkinteger(L, 3);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 3, "attack type error");
	luaL_argcheck(L, actor.attack_list[ atk_type ].valid, 2, "attack type not valid");
	actor.cached_atks[ index ] = actor.attack_list[ atk_type ];
	return 0;
}

// do_hit(idList, hitType[, fromPos=actor.pos, damageRadio=1, hitPart=2(body), useGrenadeProp=false])
static int lDoActorHit(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);

	luaL_argcheck(L, lua_istable(L, 2), 2, "need idList");
	int hitType = (int)luaL_checkinteger(L, 3);
	int ok;
	float damageRadio = (float)lua_tonumberx(L, 6, &ok);
	if (!ok) damageRadio = 1;
	int hitPart = (int)lua_tointegerx(L, 7, &ok);
	if (!ok) hitPart = 2;

	const SActorProp * fromProp = NULL;
    int atk_type = 0;
	int index = 0;
	if (lua_toboolean(L, 8)) {
		index = (int)luaL_checkinteger(L, 9);
		if (index < 0 || MAX_CACHED_ATKS <= index) {
            skynet_error(NULL, "\033[1;33mlDoActorHit index error %d\033[0m", index);
            return 0;
        } else if (!actor.cached_atks[ index ].valid) {
            skynet_error(NULL, "\033[1;33mlDoActorHit attack type not valid %d\033[0m", index);
            return 0;
        }
        actor.cached_atks[ index ].valid = false;
		fromProp = &actor.cached_atks[ index ].prop;
        atk_type = actor.cached_atks[ index ].atk_type;
	}

	int idList[CSceneLogic::MAX_ACTOR_COUNT];
	int count = (int)luaL_len(L, 2);
	luaL_argcheck(L, count >= 0 && count <= CSceneLogic::MAX_ACTOR_COUNT, 1, "not a player");
	for (int i = 0; i < count; i++)
		idList[i] = lua_getiint(L, 2, i + 1);

	const float * fromPos = NULL;
	float posAry[POS_SIZE];
	if (lua_istable(L, 4))
	{
		for (int i = 0; i < POS_SIZE; i++)
			posAry[i] = lua_getifloat(L, 4, i + 1);
		fromPos = posAry;
	}
	float firePos[3];
	if (lua_istable(L, 5))
	{
		firePos[0] = lua_getifloat(L, 5, 1);
		firePos[1] = lua_getifloat(L, 5, 2);
		firePos[2] = lua_getifloat(L, 5, 3);
	}

	logic.DoActorHit(actor, idList, count, hitType, fromPos, firePos, damageRadio, hitPart, fromProp, atk_type, index);

	return 0;
}

// do_hit(idList, hitType, effect_id, damage)
static int lDoActorSkill(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);

	luaL_argcheck(L, lua_istable(L, 2), 2, "need idList");
	int hitType = (int)luaL_checkinteger(L, 3);
	int ok;
	int skillId = (float)lua_tointegerx(L, 4, &ok);
	if (!ok) skillId = 1;
	int damage = (int)lua_tointegerx(L, 5, &ok);
	if (!ok) damage = 2;

	int idList[CSceneLogic::MAX_ACTOR_COUNT];
	int count = (int)luaL_len(L, 2);
	luaL_argcheck(L, count >= 0 && count <= CSceneLogic::MAX_ACTOR_COUNT, 1, "not a player");
	for (int i = 0; i < count; i++)
		idList[i] = lua_getiint(L, 2, i + 1);

	logic.DoActorSkill(actor, idList, count, hitType, skillId, damage);

	return 0;
}

static int lBroadcastActorDie(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	//SSceneActor& actor = _GetActor(L);
	
	toc_fight_actor_die actor_die;
	actor_die.proto_id = pid_fight_actor_die;
	actor_die.actor_id = (int)luaL_checkinteger(L, 2);
	actor_die.killer = (int)luaL_checkinteger(L, 3);
	actor_die.weapon = (int)luaL_checkinteger(L, 4);
	actor_die.part = (int)luaL_checkinteger(L, 5);
	actor_die.multi_kill = (int)luaL_checkinteger(L, 6);
	actor_die.stage = (int)luaL_checkinteger(L, 7);
	actor_die.across_wall = lua_toboolean(L, 8);
	if (lua_istable(L, 9))
	{
		actor_die.killerPos.x = lua_getifloat(L, 9, 1);
		actor_die.killerPos.y = lua_getifloat(L, 9, 2);
		actor_die.killerPos.z = lua_getifloat(L, 9, 3);
	}
	if (lua_istable(L, 10))
	{
		actor_die.diePos.x = lua_getifloat(L, 10, 1);
		actor_die.diePos.y = lua_getifloat(L, 10, 2);
		actor_die.diePos.z = lua_getifloat(L, 10, 3);
	}
	actor_die.revive_time = (int)luaL_checkinteger(L, 11);
	actor_die.atk_type = (int)luaL_checkinteger(L, 12);
	actor_die.time_stamp = lua_tofloat(L, 13);
	logic.BroadcastProto(actor_die);
	return 0;
}

template<typename TDataType, TDataType SSceneActor::*TMember> static int _GetActorData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushinteger(L, (LUA_INTEGER)(actor.*TMember));
	return 1;
}

template<typename TDataType, TDataType SSceneActor::*TMember> static int _SetActorData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	actor.*TMember = (TDataType)lua_tointeger(L, 2);
	return 0;
}

template<typename TDataType, TDataType SSceneActor::*TMember> static int _GetfActorData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushnumber(L, (LUA_NUMBER)(actor.*TMember));
	return 1;
}

template<typename TDataType, TDataType SSceneActor::*TMember> static int _SetfActorData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	actor.*TMember = (TDataType)lua_tonumber(L, 2);
	return 0;
}

template<typename TDataType, TDataType CActorPlayer::*TMember> static int _GetPlayerData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	luaL_argcheck(L, actor.player != NULL, 1, "not a player");
	lua_pushinteger(L, (LUA_INTEGER)(actor.player->*TMember));
	return 1;
}

template<typename TDataType, TDataType CActorPlayer::*TMember> static int _SetPlayerData(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	luaL_argcheck(L, actor.player != NULL, 1, "not a player");
	actor.player->*TMember = (TDataType)lua_tointeger(L, 2);
	return 0;
}

static int lGetActorLife(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);
	lua_pushinteger(L, (LUA_INTEGER)logic.GetActorLife(actor));
	return 1;
}

static int lGetActorPos(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_newtable(L);
	for (uint32_t i = 0; i < POS_SIZE; i++)
		lua_setifloat(L, -1, i + 1, actor.pos[i]);
	return 1;
}

static int lSetActorPos(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	for (uint32_t i = 0; i < POS_SIZE; i++)
		actor.pos[i] = lua_getifloat(L, 2, i + 1);
	if (actor.player)
		actor.player->mGroundHeight = actor.pos[1];
	lua_pushboolean(L, 1);
	return 1;
}

static int lGetDamageTime(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);
	lua_newtable(L);
	for (int i = 0; i < MAX_DAMAGE_INFO_COUNT; i++)
	{
		SSceneActor::DamageInfo& di = actor.damage_info[i];
		if (logic.GetActor(di.fromId))
			lua_setiint(L, -1, di.fromId, di.time);
	}
	return 1;
}

static int lUpdateAIState(lua_State *L)
{
    SSceneActor& actor = _GetActor(L);
    if (!actor.robot)
        return 0;
    actor.robot->UpdateAIState(lua_tointeger(L, 2));
    return 0;
}

static int lGetActorBullet(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	SActorAttack& attack = actor.attack_list[ atk_type ];
	if (attack.valid)
	    lua_pushinteger(L, attack.bullet_count);
	else
	    lua_pushnil(L);
	return 1;
}

static int lSetActorBullet(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	SActorAttack& attack = actor.attack_list[ atk_type ];
	int count = (int)luaL_checkinteger(L, 3);
	luaL_argcheck(L, attack.valid || count == 0, 2, "attack not valid");
	attack.bullet_count = count;
	return 0;
}

static int lDecActorBullet(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	SSceneActor& actor = _GetActor(L);
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	bool ok = logic.DeductBullet(actor, atk_type);
	lua_pushboolean(L, ok);
	return 1;
}

static int lCheckFixBullet(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	if (actor.type != SSceneActor::TypePlayer) {
		lua_pushboolean(L, false);
		return 1;
	}
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	int bullet_count = lua_tointeger(L, 3);
	int rely_count = lua_tointeger(L, 4);
	int session = lua_tointeger(L, 5);
	bool ok = actor.player->CheckFixBullet(atk_type, bullet_count, rely_count, session);
	lua_pushboolean(L, ok);
	return 1;
}

static int lSetActorAttack(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	SActorAttack& attack = actor.attack_list[ atk_type ];
	attack.valid = true;
    attack.atk_type = atk_type;

#define _PROP_INT(NAME, VALUE)		attack.prop.NAME=lua_getsint(L, 3, #NAME, VALUE);
#define _PROP_FLOAT(NAME, VALUE)	attack.prop.NAME=lua_getsfloat(L, 3, #NAME, VALUE);
#include "prop_def.h"

	attack.bullet_count = (int)luaL_checkinteger(L, 4);
	attack.rely_bullet = (int)luaL_checkinteger(L, 5);
	return 0;
}

static int lSetActorAtkType(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	int atk_type = (int)luaL_checkinteger(L, 2);
	luaL_argcheck(L, 0 <= atk_type && atk_type < MAX_ATTACK_TYPE, 2, "attack type error");
	luaL_argcheck(L, actor.attack_list[ atk_type ].valid, 2, "attack type not valid");
	actor.cur_attack_type = atk_type;
	return 0;
}

static int lGetActorAlive(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushboolean(L, actor.state == SSceneActor::StateAlive);
	return 1;
}

static int lGetActorIsRobot(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushboolean(L, actor.type == SSceneActor::TypeRobot);
	return 1;
}

static int lGetActorIsPlayer(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushboolean(L, actor.type == SSceneActor::TypePlayer);
	return 1;
}

static int lGetActorIsMonster(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushboolean(L, actor.is_monster);
	return 1;
}

static int lGetActorIsBoss(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	lua_pushboolean(L, actor.is_boss);
	return 1;
}

static int lSetActorIsMonster(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	actor.is_monster = lua_toboolean(L, -1);
	return 0;
}

static int lSetActorIsBoss(lua_State *L)
{
	SSceneActor& actor = _GetActor(L);
	actor.is_boss = lua_toboolean(L, -1);
	return 0;
}

static int lActorReset(lua_State *L)
{
	CSceneLogic& logic = _GetLogic(L);
	logic.ActorReset(lua_toint(L, 1));
	return 0;
}

static int lSetMaterial(lua_State *L)
{
	int id = (int)luaL_checkinteger(L, 1);
	luaL_argcheck(L, id > 0 && id <= MATERIAL_MAX, 1, "out of range");
	MaterialDef &def = g_MaterialDef[id - 1];
	def.penetrate = (int)luaL_checkinteger(L, 2);
	def.damageRadio = (float)luaL_checknumber(L, 3);
	return 0;
}

static int lSetCfg(lua_State *L)
{
	const char * type = luaL_checkstring(L, 1);
	if (!strcmp("AllowMoveDelay", type))
		g_AllowMoveDelay = (float)luaL_checknumber(L, 2);
	else if (!strcmp("MaxMoveDelayPunish", type))
		g_MaxMoveDelayPunish = (float)luaL_checknumber(L, 2);
	else if (!strcmp("AllowFireDelay", type))
		g_AllowFireDelay = (float)luaL_checknumber(L, 2);
	else if (!strcmp("CheckGroundHeight", type))
		g_CheckGroundHeight = lua_toboolean(L, 2);
	else if (!strcmp("AIUpdateInterval", type))
		g_AIUpdateInterval = lua_tointeger(L, 2);
	else if (!strcmp("CheckMaxHeight", type))
		g_CheckMaxHeight = lua_toboolean(L, 2);
	else
		luaL_argerror(L, 1, "unknown type");
	return 0;
}


extern "C" int luaopen_scene_core(lua_State *L) {
	luaL_checkversion(L);

	luaL_Reg l[] = {
		{ "udpgate_start", lUdpGateStart },
		{ "udpgate_stop", lUdpGateStop },
		{ "createdata", lCreateData },
		{ "dataaddai", lSceneAddAI },
		{ "datarelease", lSceneRelease },

		{ "logicinit", lLogicInit },
		{ "logicrelease", lLogicRelease },
		{ "logicsetstate", lLogicSetState },
		{ "logicgettime", lLogicGetTime },
		{ "logicaddrobot", lAddLogicRobot },
		{ "logicaddplayer", lAddLogicPlayer },
		{ "logicremovespectator", lRemoveLogicSpectator },
		{ "logicsetteammatecure", lSetTeammateCure },
		{ "logicnear_actors", lNearActors },
        { "logic_pos_height", lPosHeight },
        { "logicfix_pos_list_height", lFixPosListHeight },
        { "logicpos_can_arrive", lPosCanArrive },
        { "logicsetactorprotectdata", lSetActorProtectData },
        { "logicset_disable_attacks", lSetDisableAttacks },
		{ "logicsetdelaywatch", lSetDelayWatch },
		{ "logicpos_can_attack", lPosCanAttack },

		{ "logicactor_get_state", _GetActorData<SSceneActor::State, &SSceneActor::state> },
		{ "logicactor_set_state", _SetActorData<SSceneActor::State, &SSceneActor::state> },
		{ "logicactor_get_camp", _GetActorData<int, &SSceneActor::camp> },
		{ "logicactor_set_camp", _SetActorData<int, &SSceneActor::camp> },
		{ "logicactor_get_immune", _GetActorData<int, &SSceneActor::immune> },
		{ "logicactor_set_immune", _SetActorData<int, &SSceneActor::immune> },
		{ "logicactor_get_life", lGetActorLife },
		{ "logicactor_set_life", _SetActorData<int, &SSceneActor::life> },
		{ "logicactor_get_pos", lGetActorPos },
		{ "logicactor_set_pos", lSetActorPos },
		{ "logicactor_get_last_armor", _GetActorData<int, &SSceneActor::last_armor> },
		{ "logicactor_get_god_end", _GetActorData<int, &SSceneActor::god_end> },
		{ "logicactor_set_god_end", _SetActorData<int, &SSceneActor::god_end> },
		{ "logicactor_get_last_damage_time", _GetActorData<int, &SSceneActor::last_damage_time> },
		{ "logicactor_get_last_act_time", _GetActorData<int, &SSceneActor::last_act_time> },
		{ "logicactor_get_last_fire_time", _GetActorData<int, &SSceneActor::last_fire_time> },
		{ "logicactor_get_last_damage_part", _GetActorData<int, &SSceneActor::last_damage_part> },
		{ "logicactor_get_damage_sum", _GetActorData<int, &SSceneActor::damage_sum> },
		{ "logicactor_set_damage_sum", _SetActorData<int, &SSceneActor::damage_sum> },
		{ "logicactor_get_damage_player", _GetActorData<int, &SSceneActor::damage_player> },
		{ "logicactor_set_damage_player", _SetActorData<int, &SSceneActor::damage_player> },
		{ "logicactor_get_damage_boss", _GetActorData<int, &SSceneActor::damage_boss> },
		{ "logicactor_set_damage_boss", _SetActorData<int, &SSceneActor::damage_boss> },
		{ "logicactor_get_cure_sum", _GetActorData<int, &SSceneActor::cure_sum> },
		{ "logicactor_set_cure_sum", _SetActorData<int, &SSceneActor::cure_sum> },
		{ "logicactor_get_damage_time", lGetDamageTime },
		{ "logicactor_get_critical_hp", _GetActorData<int, &SSceneActor::critical_hp> },
		{ "logicactor_set_critical_hp", _SetActorData<int, &SSceneActor::critical_hp> },
		{ "logicactor_get_alive", lGetActorAlive },
		{ "logicactor_get_is_robot", lGetActorIsRobot },
		{ "logicactor_get_is_player", lGetActorIsPlayer },
		{ "logicactor_get_is_boss", lGetActorIsBoss },
		{ "logicactor_set_is_boss", lSetActorIsBoss },
		{ "logicactor_set_move_speed", _SetfActorData<float, &SSceneActor::move_speed> },
		{ "logicactor_get_move_speed", _GetfActorData<float, &SSceneActor::move_speed> },
		{ "logicactor_get_weapon_ver", _GetPlayerData<int, &CActorPlayer::mWeaponVer> },
		{ "logicactor_set_weapon_ver", _SetPlayerData<int, &CActorPlayer::mWeaponVer> },
		//{ "logicactor_get_bullet_count", _GetPlayerData<int, &CActorPlayer::mBulletCount> },
		//{ "logicactor_set_bullet_count", _SetPlayerData<int, &CActorPlayer::mBulletCount> },
		{ "logicactor_get_callback_time", _GetActorData<int, &SSceneActor::callback_time> },
		{ "logicactor_set_callback_time", _SetActorData<int, &SSceneActor::callback_time> },
		{ "logicactor_get_is_protected", _GetActorData<int, &SSceneActor::is_protected> },
		{ "logicactor_set_is_protected", _SetActorData<int, &SSceneActor::is_protected> },
		{ "logicactor_get_is_monster", lGetActorIsMonster },
		{ "logicactor_set_is_monster", lSetActorIsMonster },

        { "logicactor__update_ai", lUpdateAIState},
		{ "logicactor_getbullet", lGetActorBullet },
		{ "logicactor_setbullet", lSetActorBullet },
		{ "logicactor_decbullet", lDecActorBullet },
		{ "logicactor_check_fix_bullet", lCheckFixBullet },
		{ "logicactor__setattack", lSetActorAttack },
		{ "logicactor_setatktype", lSetActorAtkType },
		{ "logicactor_reset", lActorReset },
		{ "logicactor_remove", lRemoveLogicActor },
		{ "logicactor_update_damage_time", lUpdateActorDamageTime },
		{ "logicactor_update_act_time", lUpdateActorActTime },
		{ "logicactor_save_grenade_prop", lSaveActorGrenadeProp }, // TODO
		{ "logicactor_do_hit", lDoActorHit },
		{ "logicactor_do_skill", lDoActorSkill },
		{ "logicactor_save_attack_prop", lSaveActorAttackProp },
		{ "logicactor_broadcast_actor_die", lBroadcastActorDie },

		{ "setmaterial", lSetMaterial },
		{ "setcfg", lSetCfg },

		{ NULL, NULL },
	};

	luaL_Reg ll[] = { { NULL, NULL } };
	luaL_newlib(L, ll);

	lua_getfield(L, LUA_REGISTRYINDEX, "skynet_context");
	void * ctx = lua_touserdata(L, -1);
	if (ctx == NULL)
		return luaL_error(L, "Init skynet context first");
	luaL_setfuncs(L, l, 1);

	return 1;
}
