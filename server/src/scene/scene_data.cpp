
#include "stdafx.h"
#include <algorithm>
#include <math.h>
#include "scene_data.h"

CSceneData::CSceneData()
{
	mHeightData = NULL;
	mAIData = NULL;
	mRoutePointData = NULL;
}

template<typename T>
static void _FreeData(T* &data)
{
	if (data)
	{
		delete[] data;
		data = NULL;
	}
}

template<typename T>
static void _AllocNewSize(T* &data, size_t size)
{
	_FreeData(data);
	data = new T[size];
	memset(data, 0, sizeof(T) * size);
}

CSceneData::~CSceneData()
{
	_FreeData(mHeightData);
	_FreeData(mAIData);
	_FreeData(mRoutePointData);
}

void CSceneData::InitData()
{
	_AllocNewSize(mHeightData, mSizeX * mSizeZ);
	_AllocNewSize(mAIData, mSizeX * mSizeZ);
	_AllocNewSize(mRoutePointData, mSizeX * mSizeZ);
	ZeroArray(mAIArray);
	ZeroArray(mRoutePointArray);
}

void CSceneData::FillAIData(int id, float posx, float posz, int shape, float sizex, float sizez)
{
	const int shapeCube = 1;
	const int shapeCylinder = 2;
	float w = fabsf(sizex) / 2;
	float h = fabsf(sizez) / 2;
	if (shape == shapeCylinder)
		w = h = std::max(w, h);
	else if (shape != shapeCube)
		return;
	int left = std::max((int)((posx - mXMin - w) * POINT_LEN), 0);
	int right = std::min((int)((posx - mXMin + w) * POINT_LEN), mSizeX - 1);
	int bottom = std::max((int)((posz - mZMin - h) * POINT_LEN), 0);
	int top = std::min((int)((posz - mZMin + h) * POINT_LEN), mSizeZ - 1);
	BYTE bid = (BYTE)id;
	for (int z = bottom; z <= top; z++)
	{
		BYTE* line = mAIData + z * mSizeX;
		for (int x = left; x <= right; x++)
		{
			if (shape == shapeCylinder)
			{
				float dx = (float)x / POINT_LEN + mXMin - posx;
				float dz = (float)z / POINT_LEN + mZMin - posz;
				if (dx * dx + dz * dz > w * w)
					continue;
			}
			line[x] = bid;
		}
	}
}

bool CSceneData::PosCanArrive(float fromx, float fromz, float tox, float toz, float mindh, float maxdh) const
{
	if (mYMax <= mYMin)	// 纯平面地图
		return true;

	int fx = (int)((fromx - mXMin) * POINT_LEN);
	int fz = (int)((fromz - mZMin) * POINT_LEN);
	if (fx < 0 || fx >= mSizeX || fz < 0 || fz >= mSizeZ)
		return false;
	int tx = (int)((tox - mXMin) * POINT_LEN);
	int tz = (int)((toz - mZMin) * POINT_LEN);
	if (tx < 0 || tx >= mSizeX || tz < 0 || tz >= mSizeZ)
		return false;

	int dx = tx - fx;
	int dz = tz - fz;
	if (!dx && !dz)
		return true;

	int mind = (int)(mindh * 255 / (mYMax - mYMin));
	int maxd = (int)(maxdh * 255 / (mYMax - mYMin));
	int oh = mHeightData[fz * mSizeX + fx];

	if (abs(dx) > abs(dz))
	{
		int step = dx > 0 ? 1 : -1;
		for (int x = step; x != dx; x += step)
		{
			int h = mHeightData[(x * dz / dx + fz) * mSizeX + x + fx];
			if (h - oh < mind || h - oh > maxd)
				return false;
			oh = h;
		}
	}
	else
	{
		int step = dz > 0 ? 1 : -1;
		for (int z = step; z != dz; z += step)
		{
			int h = mHeightData[(z + fz) * mSizeX + z * dx / dz + fx];
			if (h - oh < mind || h - oh > maxd)
				return false;
			oh = h;
		}
	}

	return true;
}

bool CSceneData::PosCanAttack(float fromx, float fromz, float tox, float toz, float maxdh) const
{
	if (mYMax <= mYMin)	// 纯平面地图
		return true;

	int fx = (int)((fromx - mXMin) * POINT_LEN);
	int fz = (int)((fromz - mZMin) * POINT_LEN);
	if (fx < 0 || fx >= mSizeX || fz < 0 || fz >= mSizeZ)
		return false;
	int tx = (int)((tox - mXMin) * POINT_LEN);
	int tz = (int)((toz - mZMin) * POINT_LEN);
	if (tx < 0 || tx >= mSizeX || tz < 0 || tz >= mSizeZ)
		return false;

	int dx = tx - fx;
	int dz = tz - fz;
	if (!dx && !dz)
		return true;

	int maxd = (int)(maxdh * 255 / (mYMax - mYMin));
	int fh = mHeightData[fz * mSizeX + fx];
	int th = mHeightData[tz * mSizeX + tx];
	int dh = th - fh;
	int oh = mHeightData[fz * mSizeX + fx];

	if (abs(dx) > abs(dz))
	{
		int step = dx > 0 ? 1 : -1;
		for (int x = step; x != dx; x += step)
		{
			int h = mHeightData[(x * dz / dx + fz) * mSizeX + x + fx];
			if (h - oh > maxd)
				return false;
			oh = fh + x * dh / dx;
		}
	}
	else
	{
		int step = dz > 0 ? 1 : -1;
		for (int z = step; z != dz; z += step)
		{
			int h = mHeightData[(z + fz) * mSizeX + z * dx / dz + fx];
			if (h - oh > maxd)
				return false;
			oh = fh + z * dh / dz;
		}
	}

	return true;
}
