#pragma once

struct SRobotConfig
{
	struct SRange
	{
		int min, max;
	};
	struct SRangef
	{
		float min, max;
	};

	float MoveSpeed;
	SRangef StepHeight;
	float RepulsionDistance;
	float RepulsionForce;
	int MovePatrolRate;
	SRange PatrolWait;
	SRangef PatrolDistance;
	int FindEnemyWait;
	int RoutePath;
	float ViewDeg;
	float ViewDistance;
	float TurnDeg;
	int HurtWait;

	int AttackDamageRate;
    int AttackType; // 0 射击; 1 近程攻击; 2 投掷
    float AttackDist;   // 攻击距离
    int AttackPose;     // 攻击动作
	float TraceDistance;
	SRange FirstAttackWait;
	SRange AttackWait;
	SRange FirstAttackMoveWait;
	SRange AttackMoveWait;
	SRangef AttackMoveDistance;
	SRange AttackLostWait;
    int IgnoreAI;   // 1 忽略AI
    int SelectType;
	int IgnoreHurt;	// 忽略伤害
	int Hate;	// 仇恨
	int CheckHate;
};

