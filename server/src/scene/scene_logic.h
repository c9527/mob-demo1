#pragma once

#include <vector>
#include <netinet/in.h>
#include <cstdio>

class CActorRobot;
class CActorPlayer;
class CSceneData;
struct SRobotConfig;
struct lua_State;
struct p_hit_info_toc;
struct p_hit_info_tos;
struct toc_base;
struct tos_fight_move;

struct SActorProp
{
#define _PROP_INT(NAME, VALUE)		int NAME;
#define _PROP_FLOAT(NAME, VALUE)	float NAME;
#include "prop_def.h"
};

#define MAX_DAMAGE_INFO_COUNT	4
#define MAX_ATTACK_TYPE 18
#define MAX_CACHED_ATKS 5

struct SActorAttack
{
	SActorProp prop;
	bool valid;
	int bullet_count;
	int rely_bullet; // 依赖的子弹类型，如穿甲弹依赖普通子弹
    int atk_type;
};

struct SSceneActor
{
	enum State
	{
		StateLock,
		StateAlive,
		StateDie,
	};

	enum Type
	{
		TypeRobot,
		TypePlayer,
	};

	enum LimitState
	{
		LimitStateNone = 0,
		LimitStateFire = 1,
		LimitStateSkill = 2,
		LimitStateMove = 4,
		LimitStateHit = 8,
	};

	enum ClearType
	{
		ClearTypeNone = 0,
		ClearTypeHit = 1,
	};

	struct DamageInfo
	{
		int fromId;
		int time;
		int damage;
	};

	int id;
	State state;
	Type type;
	bool is_monster;
	bool is_boss;
	int camp;
	int immune;
	int life;
	int last_armor;
	int god_end;
	int last_damage_time;
	int last_act_time;
	int last_fire_time;
	int last_damage_part;
	int damage_sum;
	int damage_player;
	int damage_boss;
	int cure_sum;
	int critical_hp;
	float pos[POS_SIZE];
	float move_speed;
	int is_protected;
	int hate;

	SActorProp prop;
	SActorProp prop_grenade;	// 持雷属性
	// TODO
	int cur_attack_type;
	SActorAttack attack_list[MAX_ATTACK_TYPE];
	SActorAttack cached_atks[MAX_CACHED_ATKS];    // 延迟伤害缓存

	DamageInfo damage_info[MAX_DAMAGE_INFO_COUNT];

	CActorRobot* robot;
	CActorPlayer* player;

	int callback_time;
};

struct SKillInfo
{
	int killer_id;
	int killed_id;
    int atk_type;
    int hit_type;
	bool across_wall;
	
	float killerPos[3];
	float diePos[3];
};

struct SDamageInfo
{
	int attack_id;
	int hit_id;
	int damage;
};

struct SMakeHitInfoParams
{
	SSceneActor *from;
	p_hit_info_toc *toc_info;
	const p_hit_info_tos *tos_info;
    int atk_type;
    int hit_type;
	float damage_radio;
	bool is_zoom;
	bool force;
	const float *from_pos;
	const SActorProp *from_prop;
	const float *fire_pos;
	bool across_wall;
	SMakeHitInfoParams(SSceneActor *actorFrom, p_hit_info_toc *tocInfo, const p_hit_info_tos *tosInfo, const int atkType = 0, const int hitType = 0, float damageRadio = 1, bool isZoom = false, bool isForce = false, const float *fromPos = NULL, const SActorProp *fromProp = NULL, const float *firePos = NULL, bool acrossWall = false)
		: from(actorFrom), toc_info(tocInfo), tos_info(tosInfo), atk_type(atkType), hit_type(hitType), damage_radio(damageRadio), is_zoom(isZoom), force(isForce), from_pos(fromPos), from_prop(fromProp), fire_pos(firePos), across_wall(acrossWall)
	{
		if (!from_pos)
			from_pos = from->pos;
		if (!from_prop)
			from_prop = &from->attack_list[from->cur_attack_type].prop;
		if (!fire_pos)
			fire_pos = from->pos;
	}
};

class CSceneLogic
{
	friend class CActorRobot;
	friend class CActorPlayer;

public:
	enum GameState
	{
		GameStateInit,
		GameStateBegin,
		GameStatePlay,
		GameStateDone,
		GameStateOver,
	};

	const static int MAX_ACTOR_COUNT = 30;
	const static int COUNT_PER_SECOND = 10;

private:
	struct SSceneSpectator
	{
		int64_t mPlayerId;
		sockaddr_in mAddr;
		char mBuff[2048];
		uint16_t mBuffSize;
		SSceneSpectator():mBuffSize(0){}
	};

	const CSceneData& mData;
	lua_State* mLua;

	SSceneActor mActors[MAX_ACTOR_COUNT];

	typedef std::vector<SSceneSpectator> SepVector;

	SepVector mSpectators;

	SKillInfo mKillInfos[MAX_ACTOR_COUNT];
	int mKillInfoCnt;
	SDamageInfo mDamageInfos[MAX_ACTOR_COUNT];
	int mDamageInfoCnt;
	double mTeammateCure;
	float mActorProtectDis;
	float mActorDamageFactorCuts[3];
    bool mDisableAtkTypes[MAX_ATTACK_TYPE];

	GameState mState;
	int mNow;	// 当前毫秒数

	int mStatDamageTime;	// 需要统计伤害的时间（有人受伤后1秒钟）

	uint32_t mRand;
	uint32_t mSendId;

	bool mDelayWatch;

	void CallScript(const char* func, int param1 = 0, int param2 = 0, int param3 = 0);

public:
	CSceneLogic(CSceneData& data);
	~CSceneLogic();

	void SetLuaState(lua_State *L) { mLua = L; }

	void HandleProto(const sockaddr_in & addr, uint32_t * protoBuf, size_t sz);

	void Update();
	void AddKillInfo(int killer_id, int killed_id, int atk_type, int hit_type, bool across_wall, const float* killerPos, const float* diePos);
	void ProcessKillInfos();
	void AddDamageInfo( int attack_id, int hit_id, int damage);
	void ProcessDamageInfo();
	const CSceneData& GetSceneData() { return mData; }
    bool* GetDisableAttacks() { return mDisableAtkTypes; }

	SSceneActor* AddRobot(SRobotConfig*& cfg);
	SSceneActor* AddPlayer(uint32_t& rid);
	SSceneActor* GetActor(int id);
	const SActorProp& GetProp(const SSceneActor& actor) const;
	const SActorProp& GetProp(const SSceneActor& actor, const int atk_type) const;
	int GetBullet(const SSceneActor& actor, const int atk_type) const;
	bool DeductBullet(SSceneActor& actor, const int atk_type);
	void RemoveActor(int id);
	void UpdateActorActTime(SSceneActor& actor);
	bool ActorReset(int id);
	int GetActorArmor(const SSceneActor& actor);
	int GetActorLife(const SSceneActor& actor);

	void RemoveSpectator(int64_t playerId);

	GameState GetState(){ return mState; }
	void SetState(GameState state){ mState = state; }
	void SetTeammateCure(double teammate_cure){ mTeammateCure = teammate_cure; }
	void SetActorProtectData(const float dis, const float factors[])
	{
		mActorProtectDis = dis;
		for (int i = 0; i < 3; i++)
			mActorDamageFactorCuts[i] = factors[i];
	}

	bool CanPlay(){ return mState == GameStateInit || mState == GameStatePlay || mState == GameStateOver; }

	int Now(){ return mNow; }

	void DoActorHit(SSceneActor& actor, const int * idList, int idCount, int hitType, const float * fromPos = NULL, const float * firePos = NULL, float damageRadio = 1, int hitPart = 2, const SActorProp* fromProp = NULL, const int atkType = 0, const int atkIdx = 0);
	void DoActorSkill(SSceneActor& actor, const int * idList, int count, int hitType, int skillId, int damage);
	
	static void MaskBuf(void * buf, uint32_t id, int size)
	{
		uint32_t * buf1 = (uint32_t *)buf;
		uint32_t msk = (id * 0xabcd4399u) ^ 0x12345678u;
		int size1 = size >> 2;
		for (int i = 0; i < size1; i++)
			buf1[i] ^= msk;
		uint8_t * buf2 = (uint8_t *)(buf1 + size1);
		int size2 = size & 0x3; 
		for (int i = 0; i < size2; i++)
			buf2[i] ^= ((uint8_t *)&msk)[i];
	}

	static uint32_t Adler32(const void * buf, int size)
	{
		uint32_t s1 = 1, s2 = 0;
		int i;
		for (i = 0; i < size; i++) {
			s1 = (s1 + ((uint8_t *)buf)[i]) % 65521;
			s2 = (s2 + s1) % 65521;
		}
		return (s2 << 16) + s1;
	}

	void setDelayWatch(bool delayWatch)
	{
		mDelayWatch = delayWatch;
	}
	
	void BroadcastProto(toc_base * proto, size_t sz);

	template<class T>
	void BroadcastProto(T& proto)
	{
		BroadcastProto(&proto, sizeof(proto));
	}

private:
	// 随机一个32位无符号整数
	uint32_t Rand()
	{
		return mRand = (mRand * 1103515245 + 12345);
	}

	// 随机小于n的非负数（不会取到n）
	int Rand(int n)
	{
		if (n <= 0)
			return 0;
		return (int)(Rand() * (uint64_t)n / 0x100000000lu);
	}

	// 随机范围内的数（不会取到m，会取到n）
	int Rand(int n, int m)
	{
		return Rand(m - n) + n;
	}

	void SendMove(const SSceneActor& actor, const tos_fight_move * tos = NULL, const uint8_t * buf = NULL, size_t sz = 0);

	bool MakeHitInfo(SMakeHitInfoParams& params);

	void MaskToc(toc_base * proto, size_t sz);

	bool SendUdp(const sockaddr_in & addr, const void * buf, size_t sz);
	void FlushProtoBuff();
};

