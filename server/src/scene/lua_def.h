#pragma once

extern "C"
{
#include <lua.h>
#include <lauxlib.h>
}

static inline int lua_toint(lua_State *L, int index)
{
	return (int)lua_tonumber(L, index);
}

static inline float lua_tofloat(lua_State *L, int index)
{
	return (float)lua_tonumber(L, index);
}

static inline int lua_getiint(lua_State *L, int index, lua_Integer n)
{
	lua_geti(L, index, n);
	int ret = lua_toint(L, -1);
	lua_pop(L, 1);
	return ret;
}

static inline float lua_getifloat(lua_State *L, int index, lua_Integer n)
{
	lua_geti(L, index, n);
	float ret = lua_tofloat(L, -1);
	lua_pop(L, 1);
	return ret;
}

static inline int lua_getsint(lua_State *L, int index, const char* key, int val = 0)
{
	lua_pushstring(L, key);
	if (index < 0)
		index--;
	lua_gettable(L, index);
	if (!lua_isnil(L, -1))
		val = lua_toint(L, -1);
	lua_pop(L, 1);
	return val;
}

static inline float lua_getsfloat(lua_State *L, int index, const char* key, float val = 0)
{
	lua_pushstring(L, key);
	if (index < 0)
		index--;
	lua_gettable(L, index);
	if (!lua_isnil(L, -1))
		val = lua_tofloat(L, -1);
	lua_pop(L, 1);
	return val;
}

static inline void lua_setiint(lua_State *L, int index, lua_Integer n, lua_Integer value)
{
	lua_pushinteger(L, value);
	if (index < 0)
		index--;
	lua_seti(L, index, n);
}

static inline void lua_setifloat(lua_State *L, int index, lua_Integer n, lua_Number value)
{
	lua_pushnumber(L, value);
	if (index < 0)
		index--;
	lua_seti(L, index, n);
}
