
#include "stdafx.h"
#include "lua_def.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "scene_logic.h"

extern "C"
{
#include <skynet.h>
#include <skynet_mq.h>
#include <skynet_server.h>
}

static int s_sk = -1;
static uint8_t s_recvBuf[65536];

static bool udp_listen(int port)
{
	s_sk = socket(AF_INET, SOCK_DGRAM, 0);
	if (s_sk < 0)
	{
		skynet_error(NULL, "udp create error: %d", errno);
		return false;
	}
	sockaddr_in addr;
	ZeroStruct(addr);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;

	if (bind(s_sk, (sockaddr*)&addr, sizeof(addr)))
	{
		skynet_error(NULL, "udp bind error: %d", errno);
		close(s_sk);
		s_sk = -1;
		return false;
	}

	return true;
}

static bool _udp_recv(int sk)
{
	sockaddr_in addr = {0};
	socklen_t addrLen = sizeof(addr);
	int len = recvfrom(sk, s_recvBuf, sizeof(s_recvBuf), 0, (sockaddr*)&addr, &addrLen);
	if (len < 0)
	{
		skynet_error(NULL, "error_udp_recv: %d", errno);
		return false;
	}

	if (len < 20 || len > 1024)
	{
		//const uint8_t * ip = (const uint8_t *)&addr.sin_addr;
		//skynet_error(NULL, "error_udp_len:%d addr:%d.%d.%d.%d:%d\n", len, ip[0], ip[1], ip[2], ip[3], ntohs(addr.sin_port));
		return false;
	}
	
	uint32_t* p = (uint32_t*)s_recvBuf;
	CSceneLogic::MaskBuf(p + 1, p[0] ^ (uint32_t)len, len);
	uint32_t crc = CSceneLogic::Adler32(p + 2, len - 8);
	uint32_t handle = p[2];
	if (crc != p[1] || handle <= 8 || handle > 0xffffff)
	{
		//const uint8_t * ip = (const uint8_t *)&addr.sin_addr;
		//skynet_error(NULL, "error_udp! len:%d id:%d crc:%x/%x hid:%x rid:%x pid:%x addr:%d.%d.%d.%d:%d\n",
		//	len, p[0], crc, p[1], handle, p[3], p[4], ip[0], ip[1], ip[2], ip[3], ntohs(addr.sin_port));
		return false;
	}

	size_t sz = sizeof(addr) + len;
	uint8_t * data = (uint8_t *)skynet_malloc(sz);
	memcpy(data, &addr, sizeof(addr));
	memcpy(data + sizeof(addr), s_recvBuf, len);

	skynet_message smsg;
	smsg.source = 0;
	smsg.session = 0;
	smsg.data = data;
	smsg.sz = sz | ((size_t)PTYPE_UDP_DATA << MESSAGE_TYPE_SHIFT);

	if (!skynet_context_push(handle, &smsg))
		return true;

	skynet_free(data);
	return false;
}

static void * udp_worker(void * p)
{
	while (true)
	{
		int sk = s_sk;
		if (sk < 0)
			break;
		_udp_recv(sk);
	}

	return NULL;
}

void stop_udp_gate()
{
	if (s_sk >= 0)
	{
		int sk = s_sk;
		s_sk = -1;
		close(sk);
	}
}

bool start_udp_gate(int port)
{
	stop_udp_gate();

	if (!udp_listen(port))
		return false;

	pthread_t id;
	if (pthread_create(&id, NULL, udp_worker, NULL))
	{
		skynet_error(NULL, "thread create error: %d", errno);
		stop_udp_gate();
		return false;
	}

	return true;
}

bool CSceneLogic::SendUdp(const sockaddr_in & addr, const void * data, size_t sz)
{
	int sk = s_sk;
	if (sk < 0)
		return false;
	int len = sendto(sk, data, sz, 0, (sockaddr*)&addr, sizeof(sockaddr_in));
	if (len < 0)
	{
		skynet_error(NULL, "error_udp_send: %d", errno);
		return false;
	}
	
	return true;
}
