
#include "skynet_malloc.h"
#include "lua-seri.c"


void _my_unpack(lua_State *L, void* buffer, int len) {
	struct read_block rb;
	rball_init(&rb, buffer, len);

	int i;
	for (i = 0;; i++) {
		if (i % 16 == 15) {
			lua_checkstack(L, i);
		}
		uint8_t type = 0;
		uint8_t *t = rb_read(&rb, sizeof(type));
		if (t == NULL)
			break;
		type = *t;
		push_value(L, &rb, type & 0x7, type >> 3);
	}
}

void _my_pack(lua_State *L, int from, void ** buffer, int* sz) {
	struct block temp;
	temp.next = NULL;
	struct write_block wb;
	wb_init(&wb, &temp);
	pack_from(L, &wb, from);
	assert(wb.head == &temp);
	uint8_t * ptr = skynet_malloc(wb.len);
	int len = wb.len;
	*buffer = ptr;
	*sz = len;
	struct block *b = &temp;
	while (len > 0) {
		if (len >= BLOCK_SIZE) {
			memcpy(ptr, b->buffer, BLOCK_SIZE);
			ptr += BLOCK_SIZE;
			len -= BLOCK_SIZE;
			b = b->next;
		}
		else {
			memcpy(ptr, b->buffer, len);
			break;
		}
	}

	wb_free(&wb);
}

void _my_free(void * buffer){
	skynet_free(buffer);
}
