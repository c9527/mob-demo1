#include <map>
#include <pthread.h>
#include <sched.h>

#include <string.h>

extern "C"
{
#include <lua.h>
#include <lauxlib.h>
#include "skynet_malloc.h"

void _my_unpack(lua_State *L, void* buffer, int len);
void _my_pack(lua_State *L, int from, void ** buffer, int* sz);
void _my_free(void * buffer);
}

struct SSKBuf
{
	void * buffer;
	int len;
};

class CShareIdMap
{
	pthread_rwlock_t lock;
	bool trying_write;

public:
	typedef std::map<lua_Integer, SSKBuf> TMAP;
	TMAP map;
	
	CShareIdMap()
	{
	    pthread_rwlock_init(&lock, NULL);
	    trying_write = false;
	}

	SSKBuf LockRead(lua_Integer key)
	{
		LockRead();
		TMAP::const_iterator it = map.find(key);
		if (it == map.end())
			return SSKBuf();
		return it->second;
	}

	void LockRead()
	{
	    while (trying_write) {
	        sched_yield();
            __sync_synchronize();
        }
		pthread_rwlock_rdlock(&lock);
	}

	void UnlockRead()
	{
		pthread_rwlock_unlock(&lock);
	}

	SSKBuf Set(lua_Integer key, SSKBuf& value)
	{
		SSKBuf old = SSKBuf();
		while (pthread_rwlock_trywrlock(&lock))
		{
		    trying_write = true;
		    sched_yield();
		}
		TMAP::iterator it = map.find(key);
		if (it != map.end())
		{
			old = it->second;
			if (value.buffer)
				it->second = value;
			else
				map.erase(it);
		}
		else if (value.buffer)
		{
			map.insert(TMAP::value_type(key, value));
		}
		pthread_rwlock_unlock(&lock);
		trying_write = false;
		
		return old;
	}
};

#define MAP_COUNT 2000
static CShareIdMap maps[MAP_COUNT];

static CShareIdMap& _GetMap(lua_State *L)
{
	int id = (int)luaL_checkinteger(L, 1);
	if (id <= 0 || id > MAP_COUNT)
		luaL_argerror(L, 1, "out of range");
	return maps[id - 1];
}

static inline int _push_values(lua_State *L, SSKBuf& buf)
{
	if (!buf.buffer)
		return 0;

	lua_settop(L, 0);

	_my_unpack(L, buf.buffer, buf.len);

	return lua_gettop(L);
}

static int lSetKV(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);
	lua_Integer key = luaL_checkinteger(L, 2);

	SSKBuf value = SSKBuf();

	if (lua_gettop(L) > 2)
		_my_pack(L, 2, &value.buffer, &value.len);

	SSKBuf old = map.Set(key, value);

	int ret = _push_values(L, old);

	if (old.buffer)
		_my_free(old.buffer);

	return ret;
}

static int lSetKBuf(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);
	lua_Integer key = luaL_checkinteger(L, 2);
    SSKBuf value = SSKBuf();

    size_t len = 0;
    const char *ptr;
    if (lua_gettop(L) > 2 && (ptr = luaL_checklstring(L, 2, &len)))
    {
	    void *buffer = skynet_malloc(len);
        memcpy(buffer, ptr, len);
        value.buffer = buffer;
        value.len = len;
    }
    SSKBuf old = map.Set(key, value);

    int ret = _push_values(L, old);
    if (old.buffer)
        _my_free(old.buffer);
    return ret;
}

static int lGetKV(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);
	lua_Integer key = luaL_checkinteger(L, 2);

	SSKBuf buf = map.LockRead(key);
	int ret = _push_values(L, buf);
	map.UnlockRead();

	return ret;
}

static int lGetKBuf(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);
	lua_Integer key = luaL_checkinteger(L, 2);
    
    SSKBuf buf = map.LockRead(key);
    if (!buf.buffer) {
        map.UnlockRead();
        return 0;
    }
    lua_pushlstring(L, (const char *)buf.buffer, buf.len);
    map.UnlockRead();
    return 1;
}

static int lGetAllKey(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);

	map.LockRead();

	lua_newtable(L);
	lua_Integer n = 0;
	for (CShareIdMap::TMAP::const_iterator it = map.map.begin(); it != map.map.end(); it++)
	{
		lua_pushinteger(L, it->first);
		lua_seti(L, -2, ++n);
	}

	map.UnlockRead();

	return 1;
}

static int lGetAllMem(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);
    
	map.LockRead();

	lua_newtable(L);
	lua_Integer size = 0;
	for (CShareIdMap::TMAP::const_iterator it = map.map.begin(); it != map.map.end(); it++)
		size += it->second.len;

	map.UnlockRead();

	lua_pushinteger(L, size);

	return 1;
}

static int lGetCount(lua_State *L)
{
	CShareIdMap& map = _GetMap(L);

	map.LockRead();
	lua_pushinteger(L, map.map.size());
	map.UnlockRead();

	return 1;
}


extern "C" int luaopen_shareidmap(lua_State *L) {
	luaL_Reg l[] = {
		{ "setkv", lSetKV },
        { "setkbuf", lSetKBuf },
		{ "getkv", lGetKV },
        { "getkbuf", lGetKBuf },
		{ "allkey", lGetAllKey },
		{ "allmem", lGetAllMem },
		{ "count", lGetCount },
		{ NULL, NULL },
	};
	luaL_checkversion(L);
	luaL_newlib(L, l);

	return 1;
}
