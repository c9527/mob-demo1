﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class PluginUtil
{
    public static string Excute(string exeName, string args, bool hide = false, bool redirect = false, bool waitForExit = false)
    {
        var p = new Process();
        var result = Excute(p, exeName, args, hide, redirect, waitForExit);
        p.Dispose();
        return result;
    }

    public static string Excute(Process p, string exeName, string args, bool hide = false, bool redirect = false, bool waitForExit = false)
    {
        p.StartInfo.FileName = exeName;
        p.StartInfo.Arguments = args;

        if (hide || redirect)
        {
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.UseShellExecute = false;
        }

        string result = "";
        if (redirect)
        {
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.StandardOutputEncoding = Encoding.GetEncoding("GB2312");
        }

        try
        {
            p.Start();
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }

        if (redirect)
            result = p.StandardOutput.ReadToEnd();

        if (redirect || waitForExit)
            p.WaitForExit();
        
        return result;
    }

    public static string ExcuteCmd(string[] cmds)
    {
        var p = new Process();
        p.StartInfo.FileName = "cmd.exe";

        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.RedirectStandardInput = true;
        p.StartInfo.RedirectStandardOutput = true;

        try
        {
            p.Start();
            p.StandardInput.WriteLine();
            for (int i = 0; i < cmds.Length; i++)
            {
                p.StandardInput.WriteLine(cmds[i]);
            }
            p.StandardInput.WriteLine("exit");
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }

        var result = p.StandardOutput.ReadToEnd();
        p.WaitForExit();
        p.Close();
        return result;
    }

    /// <summary>
    /// 如果所给路径的目录不存在，则创建
    /// </summary>
    /// <param name="filePath"></param>
    public static void CreateFileDirectory(string filePath)
    {
        string dir = Path.GetDirectoryName(filePath);
        if (dir != null && !Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
    }

    /// <summary>
    /// 可读取打开的文件的ReadAllLines
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static List<string> ReadAllLinesWriteSafe(string path)
    {
        using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (var sr = new StreamReader(fs, Encoding.Unicode))
            {
                var lines = new List<string>();
                while (!sr.EndOfStream)
                {
                    lines.Add(sr.ReadLine());
                }
                return lines;
            }
        }
    }

    /// <summary>
    /// 可读取打开的文件的ReadAllText
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string ReadAllTextWriteSafe(string path)
    {
        using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (var sr = new StreamReader(fs, Encoding.Unicode))
            {
                return sr.ReadToEnd();
            }
        }
    }

    /// <summary>
    /// 解析简单配置格式
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    public static Dictionary<string, string> ReadSimpleConfig(string content)
    {
        var dic = new Dictionary<string, string>();
        var lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            var kv = lines[i].Split(new char[] { '=' });
            if (kv.Length == 0)
                continue;
            if (!dic.ContainsKey(kv[0]))
                dic.Add(kv[0], kv.Length > 1 ? kv[1] : "");
        }

        return dic;
    }

    /// <summary>
    /// 字符串首字母大写
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    public static string ToUpTitle(this string content)
    {
        if (String.IsNullOrEmpty(content))
            return content;
        if (content.Length <= 1)
            return content.ToUpper();
        return content[0].ToString().ToUpper() + content.Substring(1);
    }

    /// <summary>
    /// 获取目录下的所有对象路径，去掉了.meta
    /// </summary>
    /// <param name="path">目录路径</param>
    /// <param name="recursive">是否递归获取</param>
    /// <returns></returns>
    public static string[] GetFiles(string path, bool recursive = true)
    {
        if (!Directory.Exists(path))
            return new string[0];

        var resultList = new List<string>();
        var dirArr = Directory.GetFiles(path, "*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
        for (int i = 0; i < dirArr.Length; i++)
        {
			var ext = Path.GetExtension(dirArr[i]);
            if (ext != ".meta" && ext != ".DS_Store")
                resultList.Add(dirArr[i].Replace('\\','/'));
        }
        return resultList.ToArray();
    }

    public static string[] GetDirectory(string path, bool recursive = true)
    {
        var resultList = new List<string>();
        var dirArr = Directory.GetDirectories(path, "*", recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
        for (int i = 0; i < dirArr.Length; i++)
        {
            if (Path.GetExtension(dirArr[i]) != ".meta")
                resultList.Add(dirArr[i].Replace('\\','/'));
        }
        return resultList.ToArray();
    }

    public static void ClearDirectory(string path)
    {
        var dirArr = Directory.GetDirectories(path);
        for (int i = 0; i < dirArr.Length; i++)
        {
            FileUtil.DeleteFileOrDirectory(dirArr[i]);
        }

        var fileArr = Directory.GetFiles(path);
        for (int i = 0; i < fileArr.Length; i++)
        {
            FileUtil.DeleteFileOrDirectory(fileArr[i]);
        }
    }

    private static string[] m_removeStr = new []{"Assets/Resources/", "Assets/ResourcesLib/", "Assets/StreamingAssets/"};
    /// <summary>
    /// 获得与打包加载方式无关的统一访问路径
    /// </summary>
    /// <param name="path"></param>
    /// <param name="ext"></param>
    /// <returns></returns>
    public static string GetUnifyPath(string path, bool ext = false)
    {
        int index;
        for (int i = 0; i < m_removeStr.Length; i++)
        {
            index = path.IndexOf(m_removeStr[i], StringComparison.Ordinal);
            if (index > -1)
                path = path.Remove(0, index + m_removeStr[i].Length);
        }

        var removeStr = GetEditorStreamingAssetsLibPath();
        index = path.IndexOf(removeStr, StringComparison.Ordinal);
        if (index > -1)
            path = path.Remove(0, index + removeStr.Length);

        if (!ext)
        {
            index = path.LastIndexOf('.');
            if (index > -1)
                path = path.Substring(0, index);
        }
        
        path = path.TrimEnd(new[] { '/', '\\' });
        return path;
    }

    public static string GetProjectPath(string path, bool meta = true)
    {
        int index;
        if (!meta)
        {
            index = path.LastIndexOf(".meta", StringComparison.OrdinalIgnoreCase);
            if (index > -1)
                path = path.Substring(0, index);
        }
        index = path.IndexOf("/Assets/", StringComparison.Ordinal);
        if (index > -1)
            return path.Substring(index+1);
        else
            return path;
    }

    public static AssetBundle LoadAssetBundle(string path)
    {
        var bytes = File.ReadAllBytes(path);
        return AssetBundle.CreateFromMemoryImmediate(bytes);
    }

    /// <summary>
    /// 把所有默认字体替换成自己的字体
    /// </summary>
    public static void ReplaceAllFont()
    {
        var paths = GetFiles("Assets/Resources/UI/");
        for (int i = 0; i < paths.Length; i++)
        {
            var path = paths[i];
            var content = File.ReadAllText(path);
            content = content.Replace("{fileID: 10102, guid: 0000000000000000e000000000000000, type: 0}", "{fileID: 12800000, guid: a1e0d00e48e8b37468f0f4030d18cc6a, type: 3}");
            File.WriteAllText(path, content);
        }
        AssetDatabase.Refresh();
    }

    public static void DrawSeparator()
    {
        GUILayout.Space(10);
        if (Event.current.type == EventType.Repaint)
        {
            Texture2D tex = EditorGUIUtility.whiteTexture;
            Rect rect = GUILayoutUtility.GetLastRect();
            GUI.color = new Color(0f, 0f, 0f, 0.25f);
            GUI.DrawTexture(new Rect(0f, rect.yMin + 6f, Screen.width, 4f), tex);
            GUI.DrawTexture(new Rect(0f, rect.yMin + 6f, Screen.width, 1f), tex);
            GUI.DrawTexture(new Rect(0f, rect.yMin + 9f, Screen.width, 1f), tex);
            GUI.color = Color.white;
        }
    }

    /// <summary>
    /// StreamingAssts地址，只给编辑器下C#的文件操作类用的地址，没有协议头
    /// </summary>
    /// <returns></returns>
    public static string GetEditorStreamingAssetsLibPath()
    {
        return GetEditorStreamingAssetsLibRootPath() + EditorUserBuildSettings.activeBuildTarget + "/";
    }

    public static string GetEditorStreamingAssetsLibRootPath()
    {
        return "StreamingAssetsLib/";
    }

    public static string GetEditorStreamingAssetsPath()
    {
        return "Assets/StreamingAssets/"; ;
    }

    /// <summary>
    /// 将AtlasAlpha改为带平台分目录的目录
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string ChangeAtlasAlphaPathWithPlatform(string path)
    {
        if (Regex.IsMatch(path, "(^|/)AtlasAlpha/"))
            return Regex.Replace(path, "(^|/)AtlasAlpha/(.+?)(\\.|$|/)", "$1AtlasAlpha/" + BuildAB.GetBuildTarget() + "/$2$3");
        return path;
    }

    public static string GetUnityPort()
    {
        try
        {
            var pid = Process.GetCurrentProcess().Id;
            var result = ExcuteCmd(new string[] { "netstat -ano | findstr LISTENING" });
            var lines = Util.StrToLines(result);
            var unityPort = "";
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].StartsWith("  TCP"))
                {
                    var arr = lines[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (arr.Length < 5)
                        continue;
                    if (arr[4] == pid.ToString())
                    {
                        unityPort += "PID:" + pid + " Port:" + arr[1].Split(':')[1] + "\n";
                    }
                }
            }

            if (string.IsNullOrEmpty(unityPort))
                return "未找到进程对应的监听端口，请重启该Unity\nPID:"+pid;
            else
                return unityPort;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        
        return "";
    }

    #region 编辑器下用，替代PlayerPrefs
#if UNITY_EDITOR_OSX
    private const string UNITY_EDITOR_CONFIG = "UnityEditorConfig.txt";
#else
    private const string UNITY_EDITOR_CONFIG = "D:/UnityEditorConfig.txt";
#endif
    private static Dictionary<string, string> m_valueDic = new Dictionary<string, string>(); 
    public static void SetValue(string key, string value)
    {
        ReadValues();
        m_valueDic[key] = value;
        SaveValues();
    }

    public static string GetValue(string key)
    {
        ReadValues();
        if (m_valueDic.ContainsKey(key))
            return m_valueDic[key];
        return "";
    }

    public static bool HasKey(string key)
    {
        ReadValues();
        return m_valueDic.ContainsKey(key);
    }

    private static void ReadValues()
    {
        if (!File.Exists(UNITY_EDITOR_CONFIG))
            File.WriteAllText(UNITY_EDITOR_CONFIG, "", Encoding.UTF8);

        m_valueDic.Clear();
        var lines = File.ReadAllLines(UNITY_EDITOR_CONFIG, Encoding.UTF8);
        for (int i = 0; i < lines.Length; i++)
        {
            var kv = lines[i].Split(new char[] { '=' });
            if (kv.Length > 0)
                m_valueDic[kv[0]] = kv.Length > 1 ? kv[1] : "";
        }
    }

    private static void SaveValues()
    {
        var result = new StringBuilder();
        foreach (var kv in m_valueDic)
        {
            result.AppendLine(kv.Key + "=" + kv.Value);
        }
        File.WriteAllText(UNITY_EDITOR_CONFIG, result.ToString(), Encoding.UTF8);
    }
    #endregion
}
