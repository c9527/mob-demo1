﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AtlasTools : EditorWindow
{
    //搜索图片分页
    private string m_searchImagePath = "";
    private Texture2D m_searchResult;

    //查重分页
    private Vector2 m_scrollPos;


    private int tabbarIndex;
    private Dictionary<string, List<string>> m_dic = new Dictionary<string, List<string>>(); 
    private GUIStyle btnStyle;

    public static void Open()
    {
        GetWindowWithRect<AtlasTools>(new Rect(0, 0, 400, 550), true);
    }

    void OnGUI()
    {
        btnStyle = new GUIStyle(GUI.skin.button);
        btnStyle.alignment = TextAnchor.MiddleLeft;

        GUI.backgroundColor = m_dic.Count == 0 ? Color.red : Color.green;
        if (GUILayout.Button("计算图集图片MD5"))
            m_dic = EditorUIHelper.GenerateAtlasMd5();
        GUI.backgroundColor = Color.white;

        if (m_dic.Count == 0)
            return;
        tabbarIndex = GUILayout.Toolbar(tabbarIndex, new[] { "查找图片", "检查重复" });
        GUILayout.Space(5);
        switch (tabbarIndex)
        {
            case 0:GUISearchImage();break;
            case 1:GUISearchRepeatImage();break;
        }
    }

    private void GUISearchImage()
    {
        GUILayout.BeginHorizontal();
        var pathChanged = false;
        if (GUILayout.Button("浏览要搜索的图片，或者拖拽进来"))
        {
            var newPath = EditorUtility.OpenFilePanel("浏览", "Assets/ResourcesLib/Atlas/", "*");
            if (!string.IsNullOrEmpty(newPath))
            {
                pathChanged = newPath != m_searchImagePath;
                m_searchImagePath = newPath;
            }
        }
        if (DragAndDrop.paths.Length > 0)
        {
            if (Event.current.type == EventType.DragExited && this == mouseOverWindow)
            {
                pathChanged = m_searchImagePath != DragAndDrop.paths[0];
                m_searchImagePath = DragAndDrop.paths[0];
                Event.current.Use();
            }
        }
        GUILayout.EndHorizontal();

        if (m_dic.Count > 0 && !string.IsNullOrEmpty(m_searchImagePath))
        {
            //搜索的目标图片有变更时
            if (m_searchResult == null || pathChanged)
            {
                var targetMd5 = EditorUIHelper.GenerateMd5(m_searchImagePath);
                if (m_dic.ContainsKey(targetMd5))
                    m_searchResult = AssetDatabase.LoadAssetAtPath(m_dic[targetMd5][0], typeof (Texture2D)) as Texture2D;
                else
                    m_searchResult = null;
            }

            if (m_searchResult)
            {
                GUILayout.Space(20);
                GUILayout.Label("已找到相同资源，点击图片可以跳转到资源目录");
                if (GUILayout.Button(m_searchResult, GUI.skin.box))
                {
                    Selection.activeObject = m_searchResult;
                }
            }
        }
        else
            m_searchResult = null;
    }

    private void GUISearchRepeatImage()
    {
        m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false, GUILayout.Height(600));
        foreach (var kv in m_dic)
        {
            if (kv.Value.Count == 1)
                continue;

            GUILayout.BeginHorizontal();
            GUILayout.Box(AssetDatabase.LoadAssetAtPath(kv.Value[0], typeof(Texture2D)) as Texture2D, GUILayout.Width(50), GUILayout.Height(50));
            GUILayout.BeginVertical();
            for (int i = 0; i < kv.Value.Count; i++)
            {
                if (GUILayout.Button(GetDisplayPath(kv.Value[i]), btnStyle, GUILayout.Width(300)))
                    Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(kv.Value[i]);
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }
        GUILayout.EndScrollView();
    }

    private string GetDisplayPath(string path)
    {
        var removeStr = "Assets/ResourcesLib/Atlas";
        var index = path.IndexOf(removeStr);
        if (index > -1)
            path = path.Remove(0, index + removeStr.Length);
        return path;
    }
}