﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CanEditMultipleObjects]
[CustomEditor(typeof(MatInfo))]

public class MatInfoEditor : Editor {

    
    public override void OnInspectorGUI()
    {
       
        MatInfo mi = target as MatInfo;

        serializedObject.Update();

        EMat oldValue = mi.matValue;
        mi.matValue = (EMat)EditorGUILayout.EnumPopup("建筑材质", mi.matValue);

        if(oldValue != mi.matValue)
        {
            foreach (Object obj in targets)
            {
                MatInfo kMI = obj as MatInfo;
                kMI.matValue = mi.matValue;
            }
        }

        ESceneItemType OldItemType = mi.itemType;
        mi.itemType = (ESceneItemType)EditorGUILayout.EnumPopup("物件类型", mi.itemType);
        if(OldItemType != mi.itemType)
        {
            foreach (Object obj in targets)
            {
                MatInfo kMI = obj as MatInfo;
                kMI.itemType = mi.itemType;
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

 

	 
}
