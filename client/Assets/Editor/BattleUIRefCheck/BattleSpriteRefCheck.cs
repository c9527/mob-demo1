﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class BattleSpriteRefCheck {

    private Regex m_regexSprite = new Regex(@"m_Sprite: {fileID: [\d]*, guid: ([\w\d]*), type: \d}");

    [MenuItem("Plugins/UI/战斗中Panel引用图集")]
    public static void DoCheck()
    {
        BattleSpriteRefCheck check = new BattleSpriteRefCheck();
        List<FileInfo> pathList= check.GetAllPrefab();
        List<PrefabItem> prefabItemList=  check.Check(pathList);
        //foreach(PrefabItem item in prefabItemList )
        //{           
        //    foreach(KeyValuePair<string ,string> pair in item.atlasList)
        //    {
        //        Debug.LogError(string.Format("panelName:{0},Atlas:{1},Sprite{2}", item.panelName, pair.Key, pair.Value));
        //    }
        //}
        RefSpriteShow.Open(prefabItemList.ToArray());
    }

    private List<FileInfo> GetAllPrefab()
    {
        List<FileInfo> list = new List<FileInfo>();
        string path = Application.dataPath + "/Resources/UI/Battle";
        DirectoryInfo info = new DirectoryInfo(path);
        var arr = info.GetFiles();
        for (int i = 0; i < arr.Length;i++ )
        {
            if (arr[i].Extension == ".prefab")
                list.Add(arr[i]);
        }
        return list;
    }

    private List<PrefabItem> Check(List<FileInfo> fileInfos)
    {
        List<PrefabItem> list = new List<PrefabItem>();
        foreach(FileInfo info in fileInfos )
        {
            StreamReader sr = info.OpenText();
            string content = sr.ReadToEnd();
            sr.Dispose();
            sr = null;
            PrefabItem item = null;
            Match math = m_regexSprite.Match(content);
            while(math.Success)
            {
                string guid = math.Groups[1].ToString();
                string path = AssetDatabase.GUIDToAssetPath(guid);                
                if (path.EndsWith(".png"))
                {
                    var arr = path.Split('/');
                    string atlas = arr[arr.Length - 2];
                    string spriteName = arr[arr.Length - 1];
                    if (item == null)
                    {
                        item = new PrefabItem(info.Name);
                        list.Add(item);
                    }
                    item.Add(atlas, spriteName);
                }
                math = math.NextMatch();                
            }
        }
        return list;
    }


}

public class PrefabItem
{
    public string panelName;
    public Dictionary<string, List<string>> atlasDic = new Dictionary<string, List<string>>();   
    public PrefabItem(string name)
    {
        panelName = name;
    }

    public void Add(string atlas,string sprite)
    {
        if (atlasDic.ContainsKey(atlas))
            atlasDic[atlas].Add(sprite);
        else
        {
            atlasDic.Add(atlas, new List<string>() { sprite });
        }
    }

}
