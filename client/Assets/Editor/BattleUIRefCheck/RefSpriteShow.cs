﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class RefSpriteShow:EditorWindow {

    private static  PrefabItem[] m_itemArray;
    private Vector2 m_scrollPos;
    public static void Open(PrefabItem[] arr)
    {
        m_itemArray = arr;
        GetWindowWithRect<RefSpriteShow>(new Rect(0, 0, 700, 400), true);
    }


    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Panel", GUILayout.Width(300));
        GUILayout.Label("Atlas", GUILayout.Width(100));
        GUILayout.Label("Sprite", GUILayout.Width(300));
        GUILayout.EndHorizontal();
        m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false, GUILayout.Height(300));
        for(int i=0;i<m_itemArray.Length;i++)
        {            
            PrefabItem item = m_itemArray[i];            
            //for (int j = 0; j < arr.Length;j++ )
            //{
            //    GUILayout.BeginHorizontal();
            //    GUILayout.Label(item.panelName, GUILayout.Width(300));
            //    GUILayout.Label(arr[j].Key, GUILayout.Width(100));
            //    GUILayout.Label(arr[j].Value, GUILayout.Width(300));
            //    GUILayout.EndHorizontal();
            //}       
            foreach(var key in item.atlasDic.Keys )
            {
                GUILayout.BeginHorizontal();
                var value = item.atlasDic[key];
                GUILayout.Label(item.panelName, GUILayout.Width(300));
                GUILayout.Label(key, GUILayout.Width(100));
                EditorGUILayout.Popup(0, value.ToArray());
                GUILayout.EndHorizontal();
            }
        }
        GUILayout.EndScrollView();
    }

}
