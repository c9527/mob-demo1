﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using UnityEditor;
using UnityEngine;

public enum NextVersionType
{
    Current,
    NextVersion,
    NextAppStoreVersion
}

/// <summary>
/// 版本号管理
/// </summary>
public static class VersionManager
{
//    private const string PATH_BRANCH_VERSION_XML = @"\\172.16.10.158\zjqz\BranchVersion.xml";
    private const string PATH_BRANCH_VERSION_XML = @"BranchVersion.xml";
//    private const string PATH_RES_LIB = "F:/union/";
    private const string PATH_RES_LIB = "";

    public static void HotUpdate(string publishName, string branchName, bool officialPublish)
    {
        string version;
        string bigVersion;
        GetVersion(publishName, branchName, officialPublish, out version, out bigVersion);

        HotUpdateModifyCache(publishName, branchName, version, bigVersion, officialPublish);
        HotUpdateModifyOfficial(publishName, branchName, officialPublish);
    }

    public static void PublishPlayer(string publishName, string branchName, string serverlist="")
    {
        string version;
        string bigVersion;
        GetVersion(publishName, branchName, true, out version, out bigVersion);

        PublishPlayerModifyCache(publishName, branchName, version, bigVersion, serverlist);
    }

    public static void GetVersion(string publishName, string branchName, bool officialPublish, out string version, out string bigVersion, NextVersionType versionType = NextVersionType.Current)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        var strVersion = officialPublish ? "Version" : "PrepareVersion";
        version = branchElm[strVersion].GetAttribute("value");
        bigVersion = branchElm[strVersion].GetAttribute("value2");

        var arr = version.Split('.');
        var buildCode = int.Parse(arr[3]);
        switch (versionType)
        {
            case NextVersionType.NextVersion:
                buildCode += 1;
                break;
            case NextVersionType.NextAppStoreVersion:
                buildCode += 100;
                break;
            case NextVersionType.Current:
                break;
        }
        arr[3] = buildCode.ToString("000");
        version = string.Join(".", arr);
    }

    public static void SetVersion(string publishName, string branchName, bool officialPublish, string version, string bigVersion)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        var strVersion = officialPublish ? "Version" : "PrepareVersion";

        branchElm[strVersion].SetAttribute("value", version);
        branchElm[strVersion].SetAttribute("value2", bigVersion);
        Debug.Log("SetVersion:" + version + " " + bigVersion);

        SaveXml(xml, PATH_BRANCH_VERSION_XML);
    }

    public static void SetLastResDir(string publishName, string branchName, string resDir)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        branchElm.SetAttribute("lastResDir", resDir);
        Debug.Log("SetLastResDir:" + resDir);

        SaveXml(xml, PATH_BRANCH_VERSION_XML);
    }

    public static string GetLastResDir(string publishName, string branchName)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        return branchElm.GetAttribute("lastResDir");
    }

    public static bool CheckBranchExist(string publishName, string branchName)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);
        var result = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") != null;
        if (!result)
            Debug.LogError("分支不存在：[" + publishName + "] [" + branchName+"]");
        return result;
    }

    /// <summary>
    /// 修改版本号本地缓存文件
    /// </summary>
    /// <param name="publishName"></param>
    /// <param name="branchName"></param>
    /// <param name="version"></param>
    /// <param name="bigVersion"></param>
    /// <param name="officialPublish"></param>
    private static void HotUpdateModifyCache(string publishName, string branchName, string version, string bigVersion, bool officialPublish)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var onlineBranchName = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/@onlineBranch").Value;
        var isOnlineBranch = branchName == onlineBranchName;
        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        var strVersion = officialPublish ? "Version" : "PrepareVersion";

        branchElm[strVersion].SetAttribute("value", version);
        branchElm[strVersion].SetAttribute("value2", bigVersion);

        if (!isOnlineBranch)
        {
            var nodeList = branchElm.SelectNodes("descendant::Package[@enable='true'and@auto='true']");
            for (int i = 0; i < nodeList.Count; i++)
            {
                var packageElm = nodeList[i] as XmlElement;
                packageElm.SetAttribute("updateVer", version);
            }
        }
        Debug.Log("HotUpdateModifyCache");

        SaveXml(xml, PATH_BRANCH_VERSION_XML);
    }

    /// <summary>
    /// 修改正式要发布的版本号文件
    /// </summary>
    /// <param name="publishName"></param>
    /// <param name="branchName"></param>
    /// <param name="officialPublish"></param>
    private static void HotUpdateModifyOfficial(string publishName, string branchName, bool officialPublish)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var onlineBranchName = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/@onlineBranch").Value;
        var isOnlineBranch = branchName == onlineBranchName;
        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;

        if (isOnlineBranch)
        {
            //修改现网版本号
            var lines = File.ReadAllLines(PATH_RES_LIB + "Version.txt");
            var strVersion = officialPublish ? "Version" : "PrepareVersion";
            var strBigVersion = officialPublish ? "BigVersion" : "PrepareBigVersion";

            var version = branchElm[strVersion].GetAttribute("value");
            var bigVersion = branchElm[strVersion].GetAttribute("value2");
            
            for (int i = 0; i < lines.Length; i++)
            {
                var arr = lines[i].Split('=');
                if (arr[0] == strVersion)
                {
                    lines[i] = strVersion + "=" + version;
                    Debug.Log(string.Format("已修改{2}：{0} => {1}", arr[1], version, strVersion));
                }
                else if (arr[0] == strBigVersion)
                {
                    lines[i] = strBigVersion + "=" + bigVersion;
                    Debug.Log(string.Format("已修改{2}：{0} => {1}", arr[1], bigVersion, strBigVersion));
                }
            }
            File.WriteAllLines(PATH_RES_LIB + "Version.txt", lines);
        }
        else
        {
            //修改非现网版本号
            var lines = File.ReadAllLines(PATH_RES_LIB + "VersionCheckList.txt");

            var nodeList = branchElm.SelectNodes("descendant::Package");
            for (int i = 0; i < nodeList.Count; i++)
            {
                var packageElm = nodeList[i] as XmlElement;
                var buildVer = packageElm.GetAttribute("buildVer");
                var updateVer = packageElm.GetAttribute("updateVer");
                var serverlist = packageElm.GetAttribute("serverlist");

                if (packageElm.GetAttribute("enable") == "true")
                {
                    var line = buildVer + "=" + updateVer + (!string.IsNullOrEmpty(serverlist) ? "=" + serverlist : "");
                    var has = false;
                    for (int j = 0; j < lines.Length; j++)
                    {
                        var arr = lines[j].Split('=');
                        if (arr[0] == buildVer)
                        {
                            has = true;
                            Debug.Log(string.Format("已修改VersionCheckList：{0} => {1}", lines[j], line));
                            lines[j] = line;
                            break;
                        }
                    }

                    if (!has)
                    {
                        lines = lines.ArrayConcat(line);
                        Debug.Log(string.Format("已新增VersionCheckList：{0}", line));
                    }
                }
                else
                {
                    for (int j = 0; j < lines.Length; j++)
                    {
                        var arr = lines[j].Split('=');
                        if (arr[0] == buildVer)
                        {
                            Debug.Log(string.Format("已删除VersionCheckList：{0}", lines[j]));
                            lines = lines.ArrayDelete(j);
                            break;
                        }
                    }
                }
                
            }
            File.WriteAllLines(PATH_RES_LIB + "VersionCheckList.txt", lines);
        }
    }

    private static void PublishPlayerModifyCache(string publishName, string branchName, string version, string bigVersion, string serverlist="")
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var onlineBranchName = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/@onlineBranch").Value;
        var isOnlineBranch = branchName == onlineBranchName;
        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        
        branchElm["Version"].SetAttribute("value", version);
        branchElm["Version"].SetAttribute("value2", bigVersion);

        if (!isOnlineBranch)
        {
            var packageElm = branchElm.SelectSingleNode("descendant::Package[@buildVer='" + version + "']") as XmlElement;
            if (packageElm == null)
            {
                packageElm = xml.CreateElement("Package");
                packageElm.SetAttribute("enable", "true");
                packageElm.SetAttribute("auto", "true");
                packageElm.SetAttribute("buildVer", version);
                packageElm.SetAttribute("updateVer", version);
                packageElm.SetAttribute("serverlist", serverlist);
                branchElm.AppendChild(packageElm);
            }
            else
            {
                packageElm.SetAttribute("serverlist", serverlist);
            }
        }
        Debug.Log("PublishPlayerModifyCache");

        SaveXml(xml, PATH_BRANCH_VERSION_XML);
    }

    public static void ChangeOnlineBranch(string publishName, string branchName)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var onlineBranchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/@onlineBranch") as XmlElement;
        if (branchName == onlineBranchElm.Value)
            return;

        var branchElm = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/Branch[@name='" + branchName + "']") as XmlElement;
        var nodeList = branchElm.SelectNodes("descendant::Package");
        for (int i = 0; i < nodeList.Count; i++)
        {
            var packageElm = nodeList[i] as XmlElement;
            packageElm.SetAttribute("enable", "false");
        }
        onlineBranchElm.Value = branchName;
        Debug.Log("ChangeOnlineBranch:" + branchName);

        SaveXml(xml, PATH_BRANCH_VERSION_XML);
    }

    private static void SaveXml(XmlDocument xml, string path)
    {
        var xmlTextWriter = new XmlTextWriter(path, new UTF8Encoding(false));
        xmlTextWriter.Formatting = Formatting.Indented;
        xmlTextWriter.IndentChar = '\t';
        xmlTextWriter.Indentation = 1;

        xml.WriteContentTo(xmlTextWriter);
        xmlTextWriter.Close();
    }

    /// <summary>
    /// 备份BranchVersion文件
    /// </summary>
    /// <param name="desc">因为要写到文件中，所以注意不要有非法字符</param>
    public static void Backup(string desc = "")
    {
        var backupPath = @"BranchVersion/BranchVersion_" + DateTime.Now.ToString("yyMMddHHmmss") + (string.IsNullOrEmpty(desc) ? "" : "_"+desc) + ".xml";
        PluginUtil.CreateFileDirectory(backupPath);

        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);
        var xmlTextWriter = new XmlTextWriter(backupPath, new UTF8Encoding(false));
        xmlTextWriter.Formatting = Formatting.Indented;
        xmlTextWriter.IndentChar = '\t';
        xmlTextWriter.Indentation = 1;

        xml.WriteContentTo(xmlTextWriter);
        xmlTextWriter.Close();
    }

    public static bool IsOnlineBranch(string publishName, string branchName)
    {
        var xml = new XmlDocument();
        xml.Load(PATH_BRANCH_VERSION_XML);

        var onlineBranchName = xml.SelectSingleNode("/Publish[@name='" + publishName + "']/@onlineBranch").Value;
        var isOnlineBranch = branchName == onlineBranchName;
        return isOnlineBranch;
    }
}