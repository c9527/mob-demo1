﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Sprites;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// 资源打包工具
/// </summary>
public static class ResourcePacker
{
    private static readonly Regex REG_GUID = new Regex(@"guid: ([\w\d]*)");
    private static readonly string[] LEAF_ASSET_EXT = new[] { ".fbx", ".wav", ".anim", ".shader", ".png", ".jpg", ".gif", ".tga", ".exr", ".ttf", ".mask", ".rendertexture" };
    private static Dictionary<string, AssetNode> m_fileDic = new Dictionary<string, AssetNode>();//资源路径、资源
    private static Dictionary<string, BundleNode> m_bundleDic = new Dictionary<string, BundleNode>(); //bundle路径、bundle
    private static Dictionary<string, List<string>> m_unBundleDic = new Dictionary<string, List<string>>();//资源路径、bundle路径列表
    private static Dictionary<int, List<BundleNode>> m_bundleGroupDic = new Dictionary<int, List<BundleNode>>();//分组id、bundle列表
    private static Dictionary<string, string> m_bundleInfo = new Dictionary<string, string>(); //资源路径、bundle路径
    private static Dictionary<string, HashSet<string>> m_lastDependDic; //上一次的依赖关系字典

    private static bool DEBUG_LOG = false;  //是否输出额外的日志信息
    private static string m_prevResDir;
    private const string SUBPATH_ASSETSDIGEST = "Editor/AssetsDigest.txt";
    private const string SUBPATH_CONFIG_BUILD_LINE = "Editor/ConfigBuildLine.csv";
    private const string SUBPATH_MAPPING = "Editor/Mapping.xml";
    private const string SUBPATH_CODE_VERSION = "Editor/CodeVersion.txt";
    private static readonly string[] FILELIST_SKIP_START_WITH_STR_ARR = { "VersionInfo/", "Editor/", "UpdateLog.txt" };
    private static readonly string[] AB_DELETE_SKIP_START_WITH_STR_ARR = {"Managed/", "VersionInfo/", "Editor/", "Config", "Depend", "UpdateLog"};

    /// <summary>
    /// 带增量功能的资源打包函数，但要确保prevResPath目录下有相关文件（ConfigBuildLine.csv, AssetsDigest.txt, Depend.assetbundle等）
    /// 资源打包完后是存放在本地的，没有提交
    /// </summary>
    /// <param name="key"></param>
    /// <param name="prevResDir">资源增量打包的前置资源目录，留空则全部重新打包;不可以传PluginUtil.GetEditorStreamingAssetsLibPath()作为目录，因为ab不再入库，一旦打包失败，无法撤销</param>
    /// <param name="checkLocalChange">打包前是否检查svn是否有本地修改，可防止后续的误提交</param>
    /// <returns></returns>
    public static bool Pack(string key, string prevResDir, bool checkLocalChange = true)
    {
        if (!CanRun())
            return false;

        m_prevResDir = prevResDir.TrimEnd(new[] {'/', '\\'}) + "/"; //确保路径是带斜杠结尾
        var isAdd = !string.IsNullOrEmpty(m_prevResDir); //是否是增量打包
        var config = LoadConfig("AutoBuild/ConfigBuild.csv", key);
        var prevConfig = isAdd ? LoadConfig(m_prevResDir + SUBPATH_CONFIG_BUILD_LINE, 0) : null;
        var time = InitCostTime();
        if (config == null)
        {
            Debug.LogError("打包配置key不存在："+key);
            return false;
        }

        //检查工程平台和目标平台是否一致
        var activeTarget = EditorUserBuildSettings.activeBuildTarget;
        var configTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), config.Platform);
        if (activeTarget != configTarget)
        {
            Debug.LogError(string.Format("平台不匹配，当前项目平台为{0}，配置目标平台为{1}", activeTarget, configTarget));
            return false;
        }

        //检查工程目录下是否有未处理的修改
        if (checkLocalChange)
        {
            var result = PluginUtil.Excute("svn", "status ../", true, true, true);
            if (!string.IsNullOrEmpty(result))
            {
                Debug.LogError("本地工作目录中有修改，请处理后再执行\n" + result);
                return false;
            }
        }

        //清理
        Clean();

        //获取变更文件列表
        string diffFileList = null; //注意：null代表全部重新打包，空字符串代表无变更
        if (isAdd)
            diffFileList = GetDiffFileList(m_prevResDir + SUBPATH_ASSETSDIGEST);

        //检查变更情况
        bool codeChanged, configChanged, resourceChanged;
        CheckChange(diffFileList, config, prevConfig, out codeChanged, out configChanged, out resourceChanged);

        //检查变更文件列表，web的js脚本版本号自动递增，防止玩家加载的缓存的旧资源
        if (configTarget == BuildTarget.WebPlayer)
            WebJsNoCdnCache.CheckWebJsVersion(diffFileList);

        //将增量打包的前置资源拷贝到本地
        FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath());
        if (isAdd)
        {
            FileUtil.CopyFileOrDirectory(m_prevResDir.TrimEnd(new[] { '/', '\\' }), PluginUtil.GetEditorStreamingAssetsLibPath().TrimEnd(new[] { '/', '\\' }));
            PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsLibPath() + "Editor");
        }

        //代码、配置、资源按需打包
        if (codeChanged && !PackDll(!string.IsNullOrEmpty(config.SDKName), config.Platform, config.Obfuscate, config.Encrypt))
            return false;
        if (configChanged && !PackConfig(config.Encrypt))
            return false;
        if (resourceChanged && !PackResource(diffFileList))
            return false;

        //导出相关文件
        if (!ExportUpdateLog())
            return false;
        if (!ExportCodeBackup())
            return false;
        if (!ExportConfigBuildLine(config))
            return false;
        if (!ExportAssetsDigest())
            return false;
        if (!ExportFileList(config.Platform))
            return false;

        LogCostTime("================打包完毕，总计用时", time);
        return true;
    }

    public static void Clean()
    {
        PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsLibPath());
        SvnOperation.ExeSvnFullRevert("Assets/ResourcesLib/AtlasAlpha/");
        SvnOperation.ExeSvnFullRevert("../server/cfg/crc/");
    }

    #region 代码处理
    private static bool PackDll(bool activeSdk, string platform, bool obfuscate, bool encrypt)
    {
        Debug.Log("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
        Debug.Log("代码打包开始...");

        if (platform == BuildTarget.WebPlayer.ToString() || platform == BuildTarget.iPhone.ToString())
            return true;

        var time = InitCostTime();
        var definesList = new List<string>();
        if (activeSdk)
            definesList.Add("ActiveSDK");
        definesList.Add("Assetbundle");
        var definesArr = definesList.Count == 0 ? null : definesList.ToArray();
        if (!PackDllCore("UnityVS.UpdateWorker.CSharp.xml", "UpdateWorker.dll.bytes", platform, definesArr))
            return false;
        if (!PackDllCore("UnityVS.client.CSharp.csproj", "Client.dll.bytes", platform, definesArr))
            return false;

        var outputPath = PluginUtil.GetEditorStreamingAssetsLibPath() + "Managed/Client.dll.bytes";
        PluginUtil.CreateFileDirectory(outputPath);
        var dllPath = outputPath;
        var dllName = "Client.dll.bytes";

        if (obfuscate && !ObfuscateDll(dllPath, dllName)) 
            return false;

        if (encrypt)
            EncryptDll(dllPath);

        if (!CompressDll(dllPath, dllName))
            return false;

        LogCostTime("代码打包用时", time);
        Debug.Log("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
        return true;
    }

    private static bool PackDllCore(string projectName, string dllName, string buildTarget, string[] defineConstants = null, string[] exceptCodePaths = null)
    {
        if (!CanRun())
            return false;

        var name = Path.GetFileNameWithoutExtension(projectName);
        var projNew = name + "-new.csproj";
        var content = File.ReadAllText(projectName);

        content = content.Replace("ENABLE_PROFILER", "");

        var platformArr = new[] { "UNITY_EDITOR", "UNITY_EDITOR_WIN", "UNITY_ANDROID", "UNITY_IPHONE", "UNITY_STANDALONE", "UNITY_STANDALONE_WIN" };
        var platformDefines = new[] { "UNITY_EDITOR", "UNITY_EDITOR_WIN" };
        if (buildTarget == BuildTarget.Android.ToString())
            platformDefines = new[] { "UNITY_ANDROID" };
        else if (buildTarget == BuildTarget.iPhone.ToString())
            platformDefines = new[] { "UNITY_IPHONE", "UNITY_IOS" };
        else if (buildTarget == BuildTarget.StandaloneWindows.ToString())
            platformDefines = new[] { "UNITY_STANDALONE", "UNITY_STANDALONE_WIN" };
        else if (buildTarget == BuildTarget.WebPlayer.ToString())
            platformDefines = new[] { "UNITY_WEBPLAYER" };

        var regex = new Regex("(<DefineConstants>)(.*?)(</DefineConstants>)");
        content = regex.Replace(content, match =>
        {
            var definesString = match.Groups[2].Value;
            for (int i = 0; i < platformArr.Length; i++)
            {
                definesString = definesString.Replace(platformArr[i], "");
            }
            var arr = definesString.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            arr = arr.ArrayConcat(platformDefines);
            if (defineConstants != null)
                arr = arr.ArrayConcat(defineConstants);
            return match.Groups[1].Value + string.Join(";", arr) + match.Groups[3].Value;
        });

        if (exceptCodePaths != null)
        {
            for (int i = 0; i < exceptCodePaths.Length; i++)
            {
                content = content.Replace(string.Format("    <Compile Include=\"{0}\" />\r\n", exceptCodePaths[i]), "");
            }
        }
        var dllPath = Regex.Match(content, @"<OutputPath>(.+?)</OutputPath>").Groups[1].Value + Regex.Match(content, @"<AssemblyName>(.+?)</AssemblyName>").Groups[1].Value + ".dll";

        if (File.Exists(dllPath))
            File.Delete(dllPath);

        File.WriteAllText(projNew, content);

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        var exeName = Environment.GetEnvironmentVariable("WinDir") + @"\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe";
        var log = PluginUtil.Excute(exeName, projNew + " /t:rebuild /p:Optimize=true", true, true);

        var destDir = PluginUtil.GetEditorStreamingAssetsLibPath() + "Managed/";
        Directory.CreateDirectory(destDir);

        var result = File.Exists(dllPath);
        if (result)
        {
            File.Copy(dllPath, Path.Combine(destDir, dllName), true);
            Debug.Log(string.Format("编译DLL成功\t{0}", dllName));
        }
        else
        {
            var startIndex = Mathf.Max(0, log.Length - 10000);
            var length = Mathf.Min(log.Length, startIndex + 10000);
            Debug.LogError(string.Format("编译DLL失败\t{0}\n{1}", dllName, log.Substring(startIndex, length - startIndex)));
        }

        File.Delete(projNew);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        return result;
    }

    private static bool ObfuscateDll(string dllPath, string dllName)
    {
        const string exeName = "AutoBuild/obfuscar/Obfuscar.Console.exe";
        const string paramArg = "AutoBuild/obfuscar/ObfuscarUnity.xml";

        var inDll = "AutoBuild/obfuscar/Input/" + dllName;
        File.Copy(dllPath, inDll, true);
        
        var log = PluginUtil.Excute(exeName, paramArg, true, true);
        if (log.IndexOf("Completed,", StringComparison.Ordinal) != -1)
        {
            var targetFilePath = PluginUtil.GetEditorStreamingAssetsLibPath() + SUBPATH_MAPPING;
            if (File.Exists(targetFilePath))
                File.Delete(targetFilePath);
            if (File.Exists(dllPath))
                File.Delete(dllPath);
            File.Move("AutoBuild/obfuscar/Output/Mapping.xml", targetFilePath);
            File.Move("AutoBuild/obfuscar/Output/Client.dll.bytes", dllPath);
            Debug.Log(string.Format("混淆DLL成功\t{0}", dllName));
            return true;
        }
        
        Debug.LogError(string.Format("混淆DLL失败\t{0}\n{1}", dllName, log));
        return false;
    }

    private static void EncryptDll(string dllPath)
    {
        var exeName = Application.dataPath + "/../AutoBuild/EncryptDll.exe";
        string param = "-e " + dllPath + " " + dllPath;
        var log = PluginUtil.Excute(exeName, param, true, true);
        Debug.Log("加密DLL log=" + log);
    }

    private static bool CompressDll(string dllPath, string dllName)
    {
        var projectTargetPath = "Assets/Editor/" + dllName;
        File.Copy(dllPath, projectTargetPath, true);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        var result = BuildAssetBundle(AssetDatabase.LoadMainAssetAtPath(projectTargetPath), "Managed/" + dllName, false, "");
        if (result)
            Debug.Log(string.Format("压缩DLL成功\t{0}", dllName));
        else
            Debug.LogError(string.Format("压缩DLL失败\t{0}", dllName));
        if (File.Exists(projectTargetPath))
            AssetDatabase.DeleteAsset(projectTargetPath);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        return result;
    }
    #endregion

    #region 配置处理
    /// <summary>
    /// 打包配置文件
    /// </summary>
    /// <param name="encrypt"></param>
    private static bool PackConfig(bool encrypt)
    {
        if (!CanRun())
            return false;

        var time = InitCostTime();
        var assetsList = new List<Object>();
        var configList = encrypt ? TempConfig.GetTempCfgs() : Resources.LoadAll("Config/");
        for (int i = 0; i < configList.Length; i++)
        {
            var configName = configList[i].name;

            if (!configName.StartsWith("Config"))
                continue;

            assetsList.Add(configList[i]);
        }
        var result = BuildAssetBundle(assetsList.ToArray(), "Config");
        if (result)
            LogCostTime("打包配置用时", time);
        else
            Debug.LogError("打包配置失败");
        TempConfig.Clear();
        return result;
    }
    #endregion

    #region 资源处理
    /// <summary>
    /// 增量打包资源
    /// </summary>
    /// <param name="diffFileList">注意：null代表全部重新打包，空字符串代表无变更</param>
    /// <returns></returns>
    private static bool PackResource(string diffFileList)
    {
        Debug.Log("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
        Debug.Log("资源打包开始...");

        var time = InitCostTime();
        m_fileDic.Clear();
        m_bundleDic.Clear();
        m_unBundleDic.Clear();
        m_bundleGroupDic.Clear();
        m_bundleInfo.Clear();
        var isAdd = !string.IsNullOrEmpty(m_prevResDir); //是否是增量打包
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

        //把资源文件划归到对应Bundle
        AssignFileToBundle();

        //检查Bundle包名唯一性
        if (!CheckPrefabNameUnique())
            return false;

        //构建Bundle之间的依赖图
        BuildBundleGraphic();

        //导出本次Bundle间依赖关系
        ExportBundleDepend();

        //对Bundle进行分组分层
        GroupAndLayerBundle();

        //加载前一次的依赖关系
        if (isAdd)
            m_lastDependDic = GetDepend(m_prevResDir + "Depend.assetbundle");

        //根据文件变更情况来标记需要重新打包的Bundle
        MarkChangedBundle(diffFileList);

        //安卓平台下对etc格式图集进行重新通道分离
        if (!TryGenerateEtcTex())
            return false;

        //删除无用的Bundle文件
        RemoveUselessBundle();

        //按文件变更情况打包
        if (BuildByLayer())
        {
            //打包成功后保存新的版本文件
            LogCostTime("资源打包用时小计", time);
            Debug.Log("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
        }
        else
            return false;//打包有报错则提示还原

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        return true;
    }

    private static void GroupAndLayerBundle()
    {
        var time = InitCostTime("Bundle分组分层");
        //构建Bundle分组，有相互引用关系的划分为同一组
        var groupId = 1;
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (GroupBundle(bundleNode, groupId))
                groupId++;
        }

        //对每个分组内从root节点分层
        foreach (var list in m_bundleGroupDic.Values)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var bundleNode = list[i];
                if (bundleNode.IsRoot)
                    LayerBundle(bundleNode, 1);
            }
        }

        var result = new StringBuilder();
        if (DEBUG_LOG)
        {
            //输出Bundle间依赖分组，和组内的层级
            foreach (var list in m_bundleGroupDic.Values)
            {
//                if (list.Count == 1)
//                    continue;
                list.Sort((a, b) => b.Layer - a.Layer);
                for (int i = 0; i < list.Count; i++)
                {
                    var bundleNode = list[i];
                    result.AppendLine(bundleNode.Layer+"\t"+bundleNode.AssetPath);
                }
                result.AppendLine();
            }
        }

        LogCostTime("Bundle分组分层用时", time, DEBUG_LOG ? result.ToString() : "");
    }

    private static void AssignFileToBundle()
    {
        BuildAssetDic("Assets/Resources/LuaRoot/", true, null, true);
        BuildAssetDic("Assets/Resources/Music/", true, typeof (AudioClip));
        BuildAssetDic("Assets/Resources/Sounds/", true, typeof (AudioClip));
        BuildAssetDic("Assets/Resources/Font/", false, typeof (Font));
        BuildAssetDic("Assets/Resources/Effect/", false, null);
        BuildAssetDic("Assets/Resources/CameraAni/", false, typeof (AnimationClip));
        BuildAssetDic("Assets/Resources/Text/", false, null);
        BuildAssetDic("Assets/Resources/Shader/", true, typeof (Shader));
        BuildAssetDic("Assets/Resources/Materials/", false, typeof (Material));
        BuildAssetDic("Assets/Resources/Textures/", false, typeof (Texture2D));
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/Atlas/", typeof (Sprite));
#if UNITY_ANDROID
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/AtlasAlpha/Android/", typeof (Texture2D));
#elif UNITY_IPHONE
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/AtlasAlpha/iPhone/", typeof(Texture2D));
#endif
        BuildAssetDic("Assets/Resources/UI/", true, null);
//      BuildAssetDic("Assets/Resources/UI/PanelUpdate.prefab", false, null);
//      BuildAssetDic("Assets/Resources/Roles/Mask/", false, typeof(AvatarMask), true);   //AvatarMask只能包含到AnimatorController里，无法单独打成包
        BuildRoleAnimationAssetDic("Assets/Resources/Roles/Animation/");
        BuildAssetDic("Assets/Resources/Roles/AnimatorController/", false, typeof (AnimatorController));
        BuildAssetDic("Assets/Resources/Roles/Fbx/", false, null, false, true);
        BuildAssetDic("Assets/Resources/Roles/", false, null);
        BuildAssetDic("Assets/Resources/Roles/Textures", false, typeof (Texture2D));
        BuildAssetDic("Assets/Resources/Weapons/WeaponsAnimation/", false, typeof (AnimationClip));
        BuildAssetDic("Assets/Resources/Weapons/Fbx/", false, null, false, true);
        BuildAssetDic("Assets/Resources/Weapons/", false, null);
        BuildAssetDic("Assets/Resources/Weapons/Textures/", false, typeof (Texture2D));
        BuildAssetDic("Assets/Resources/Scenes/", true, null);
        BuildMapAssetDic("Assets/ResourcesLib/Scenes/");
        BuildAssetDic("Assets/Resources/SceneItem/", false, null);
        BuildAssetDic("Assets/Resources/SceneItem/Texture/", false, typeof (Texture2D));
        BuildAssetDic("Assets/Resources/SceneItem/Fbx/Materials/", false, typeof (Material));
    }

    /// <summary>
    /// 检查prefab命名的唯一性
    /// </summary>
    private static bool CheckPrefabNameUnique()
    {
        var result = true;
        var bundleNameDic = new Dictionary<string, string>();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            var name = Path.GetFileName(bundleNode.AssetPath);
            if (name == null)
            {
                Debug.LogError(bundleNode.AssetPath);
                result = false;
                continue;
            }

            name = name.ToLower();
            if (name.IndexOf(' ') != -1)
            {
                Debug.LogError("Bundle名中有空格：\n" + bundleNode.AssetPath);
                result = false;
            }
            if (!bundleNameDic.ContainsKey(name))
                bundleNameDic.Add(name, bundleNode.AssetPath);
            else
            {
                Debug.LogError("Bundle名有重复：\n" + bundleNode.AssetPath + "\n" + bundleNameDic[name]);
                result = false;
            }
        }
        return result;
    }

    /// <summary>
    /// 构建Bundle之间的依赖图
    /// </summary>
    private static void BuildBundleGraphic()
    {
        var time = InitCostTime("构建Bundle间依赖图");
        foreach (var bundleNode in m_bundleDic.Values)
        {
            var files = FindDirectDenpendencies(bundleNode.AssetPath);
            var bundleFile = new List<string>();
            FindDirectBundleDenpendencies(files, bundleFile);
            files = bundleFile.ToArray();
            for (int i = 0; i < files.Length; i++)
            {
                if (!m_fileDic.ContainsKey(files[i]))
                {
                    if (!m_unBundleDic.ContainsKey(files[i]))
                        m_unBundleDic.Add(files[i], new List<string>());
                    m_unBundleDic[files[i]].Add(bundleNode.AssetPath);
                    continue;
                }
                var dependBundleNode = m_bundleDic[m_fileDic[files[i]].BundlePath];

                if (bundleNode.DependBundles == null)
                    bundleNode.DependBundles = new List<BundleNode>();
                if (!bundleNode.DependBundles.Contains(dependBundleNode))
                    bundleNode.DependBundles.Add(dependBundleNode);

                if (dependBundleNode.UsagesBundles == null)
                    dependBundleNode.UsagesBundles = new List<BundleNode>();
                if (!dependBundleNode.UsagesBundles.Contains(bundleNode))
                    dependBundleNode.UsagesBundles.Add(bundleNode);
            }
        }
        LogCostTime("构建Bundle间依赖图用时", time);
        
    }

    /// <summary>
    /// 查找直接引用文件
    /// </summary>
    private static string[] FindDirectDenpendencies(string path)
    {
        string[] paths;
        var hash = new HashSet<string>();
        if (!Path.HasExtension(path))
        {
            if (!m_bundleDic.ContainsKey(path))
                Debug.Log(path);
            var bundleNode = m_bundleDic[path];
            var list = new List<string>();
            for (int i = 0; i < bundleNode.IncludeAssets.Count; i++)
            {
                AssetNode assetNode = bundleNode.IncludeAssets[i];
                list.Add(assetNode.FilePath);
            }
            paths = list.ToArray();
        }
        else
            paths = new[] { path };
        for (int i = 0; i < paths.Length; i++)
        {
            var subPath = paths[i];
            var subPathExt = Path.GetExtension(subPath);
            if (subPathExt != null && LEAF_ASSET_EXT.Contains(subPathExt.ToLower()))
                continue;
            var prefab = File.ReadAllText(subPath);
            var match = REG_GUID.Match(prefab);
            while (match.Success)
            {
                var guid = match.Groups[1].ToString();
                var resPath = AssetDatabase.GUIDToAssetPath(guid);
                if (!string.IsNullOrEmpty(resPath) && !resPath.EndsWith(".dll") && !resPath.EndsWith(".cs") && resPath != subPath)
                {
                    if (resPath == "Library/unity default resources")
                    {
                        //Debug.LogWarning("引用了系统默认资源 guid:" + guid + "\n" + subPath);
                    }
                    else if (resPath == "Resources/unity_builtin_extra")
                    {
                        //Debug.LogWarning("引用了unity内置额外资源 guid:" + guid + "\n" + subPath);
                    }
                    else
                        hash.Add(resPath);
                }
                match = match.NextMatch();
            }
        }
        return hash.ToArray();
    }

    /// <summary>
    /// 根据文件列表，查找依赖的文件列表
    /// </summary>
    /// <param name="files"></param>
    /// <param name="bundleFile"></param>
    private static void FindDirectBundleDenpendencies(string[] files, List<string> bundleFile)
    {
        for (int i = 0; i < files.Length; i++)
        {
            if (!bundleFile.Contains(files[i]))
                bundleFile.Add(files[i]);
            if (!m_fileDic.ContainsKey(files[i]))
            {
                var arr = FindDirectDenpendencies(files[i]);
                FindDirectBundleDenpendencies(arr, bundleFile);
            }
        }
    }

    private static bool GroupBundle(BundleNode bundleNode, int groupId)
    {
        if (bundleNode.Group != 0)
            return false;

        bundleNode.Group = groupId;
        if (!m_bundleGroupDic.ContainsKey(groupId))
            m_bundleGroupDic.Add(groupId, new List<BundleNode>());
        m_bundleGroupDic[groupId].Add(bundleNode);

        if (!bundleNode.IsLeaf)
            for (int i = 0; i < bundleNode.DependBundles.Count; i++)
            {
                var dependBundle = bundleNode.DependBundles[i];
                GroupBundle(dependBundle, groupId);
            }

        if (!bundleNode.IsRoot)
            for (int i = 0; i < bundleNode.UsagesBundles.Count; i++)
            {
                var usagesBundle = bundleNode.UsagesBundles[i];
                GroupBundle(usagesBundle, groupId);
            }
        return true;
    }

    private static void LayerBundle(BundleNode bundleNode, int layer)
    {
        if (layer > bundleNode.Layer)
            bundleNode.Layer = layer;
        if (bundleNode.IsLeaf)
            return;

        foreach (var dependBundle in bundleNode.DependBundles)
        {
            LayerBundle(dependBundle, layer + 1);
        }
    }

    /// <summary>
    /// 获取依赖文件
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private static Dictionary<string, HashSet<string>> GetDepend(string path)
    {
        var dic = new Dictionary<string, HashSet<string>>();
        var ab = PluginUtil.LoadAssetBundle(path);
        if (ab == null)
        {
            Debug.Log("Depend文件不存在："+path);
            return null;
        }
        var dependLines = ab.mainAsset.ToString().Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < dependLines.Length; i++)
        {
            var arr = dependLines[i].Split(':');
            var depends = arr[1].Split('|');
            dic.Add(arr[0], new HashSet<string>(depends));
        }

        ab.Unload(false);
        return dic;
    }

    private static void ExportBundleDepend()
    {
        var time = InitCostTime();
        var result = new StringBuilder();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (bundleNode.IsLeaf)
                continue;
            result.Append(PluginUtil.GetUnifyPath(bundleNode.AssetPath + ":"));
            var index = 0;
            foreach (var dependBundleNode in bundleNode.DependBundles)
            {
                if (index++ > 0)
                    result.Append("|");
                result.Append(PluginUtil.GetUnifyPath(dependBundleNode.AssetPath));
            }
            result.AppendLine();
        }

        File.WriteAllText("Assets/Resources/Depend.txt", result.ToString());
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        BuildAssetBundle(Resources.Load("Depend"), "Depend", false);
        AssetDatabase.DeleteAsset("Assets/Resources/Depend.txt");

        LogCostTime("导出bundle包间依赖用时", time, DEBUG_LOG ? result.ToString() : "");

        if (DEBUG_LOG)
        {
            var hasFile = false;
            result = new StringBuilder();
            result.AppendLine("有文件未被划分到Bundle且被引用不止一次");
            foreach (var kv in m_unBundleDic)
            {
                if (kv.Value.Count > 1)
                {
                    hasFile = true;
                    result.AppendLine(string.Format("【{0}次】{1}\n\t{2}", kv.Value.Count, kv.Key, string.Join("\n\t", kv.Value.ToArray())));
                }
            }
            if (hasFile)
                Debug.Log(result);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="diffFileList">注意：null代表全部重新打包，空字符串代表无变更</param>
    private static void MarkChangedBundle(string diffFileList)
    {
        var time = InitCostTime("标记变更Bundle");
        if (diffFileList != null)
        {
            //文件增、改导致Bundle变化，若Bundle内只有文件删除且没全部删除，则不会重新打
            var fileArr = diffFileList.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fileArr.Length; i++)
            {
                if (fileArr[i].IndexOf("Assets/", StringComparison.Ordinal) == -1)
                    continue;
                var path = PluginUtil.GetProjectPath(fileArr[i], false);

                if (m_bundleInfo.ContainsKey(path)) //已经划分到包的，加上标记(将其本身及所依赖的)
                {
                    SetChangeProp(m_bundleInfo[path]);

                    //处理因为Bundle名称变化的情况，这种情况需要额外的把依赖它的也重新打，因为名称是查找依赖的根据
                    var fileBundlePath = PluginUtil.GetUnifyPath(m_bundleInfo[path]);
                    var usagesNodes = m_bundleDic[m_fileDic[path].BundlePath].UsagesBundles;
                    if (usagesNodes == null)
                        continue;
                    for (int j = 0; j < usagesNodes.Count; j++)
                    {
                        var bundlePath = PluginUtil.GetUnifyPath(usagesNodes[j].AssetPath);
                        var hasDepend = m_lastDependDic.ContainsKey(bundlePath) && m_lastDependDic[bundlePath].Contains(fileBundlePath);
                        if (!hasDepend)
                            SetChangeProp(usagesNodes[j].AssetPath, true);
                    }
                }
                else if (m_unBundleDic.ContainsKey(path)) //若是没有划分包的文件，则将所有依赖它的包加上标记(将其本身及所依赖的)
                {
                    for (int j = 0; j < m_unBundleDic[path].Count; j++)
                    {
                        SetChangeProp(m_unBundleDic[path][j]);
                    }
                }
            }
        }
        else
        {
            foreach (var bundleNode in m_bundleDic.Values)
            {
                bundleNode.Changed = true;
            }
        }
        LogCostTime("标记变更Bundle用时", time);
    }

    private static void RemoveUselessBundle()
    {
        //文件删导致Bundle删除
        var time = InitCostTime("删除无用的Bundle");
        var result = new StringBuilder();
        var fileArr = PluginUtil.GetFiles(PluginUtil.GetEditorStreamingAssetsLibPath());
        var bundleUnifyPathDic = new Dictionary<string, bool>();
        foreach (var bundleNode in m_bundleDic)
        {
            bundleUnifyPathDic.Add(PluginUtil.GetUnifyPath(bundleNode.Key), true);
        }
        for (int i = 0; i < fileArr.Length; i++)
        {
            var path = PluginUtil.GetUnifyPath(fileArr[i]);
            path = PluginUtil.ChangeAtlasAlphaPathWithPlatform(path);
            if (AB_DELETE_SKIP_START_WITH_STR_ARR.Any(path.StartsWith))
                continue;
            if (!bundleUnifyPathDic.ContainsKey(path))
            {
                File.Delete(fileArr[i]);
                result.AppendLine("【删除】" + fileArr[i]);

#if UNITY_ANDROID || UNITY_IPHONE
                var pathAltasAlpha = UIHelper.TryGetAtlasAlphaPath(fileArr[i]);
                if (!string.IsNullOrEmpty(pathAltasAlpha))
                {
                    var libDir = "Assets/ResourcesLib/" + UIHelper.TryGetAtlasAlphaPath(path);
                    FileUtil.DeleteFileOrDirectory(libDir);
                    FileUtil.DeleteFileOrDirectory(libDir + ".meta");
                    File.Delete(pathAltasAlpha);

                    result.AppendLine("【删除】" + pathAltasAlpha);
                    result.AppendLine("【删除】" + libDir);
                }
#endif
            }
        }

        if (result.Length > 0)
            Debug.Log(result);
        LogCostTime("删除无用的Bundle用时", time);
    }

    private static bool TryGenerateEtcTex()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        var list = new List<string>();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (bundleNode.IsEtcAtlas)
                list.Add(bundleNode.AssetPath);
        }

        if (list.Count == 0)
            return true;

        var time = InitCostTime();
        Packer.RebuildAtlasCacheIfNeeded(GetBuildTarget(), true, Packer.Execution.ForceRegroup);
        LogCostTime("重新构建图集用时", time);

        time = InitCostTime();
        for (int i = 0; i < list.Count; i++)
        {
            EditorUtility.DisplayProgressBar("分离图集透明通道", list[i], i * 1f / list.Count);
            if (!AtlasBuild.GenerateEtcTex(list[i]))
            {
                EditorUtility.ClearProgressBar();
                return false;
            }
        }
        Debug.Log("分离图集透明通道：" + string.Join("\n", list.ToArray()));
        LogCostTime("分离图集透明通道用时", time);

        time = InitCostTime();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        LogCostTime("图集透明通道导入工程用时", time);

        time = InitCostTime();
        for (int i = 0; i < list.Count; i++)
        {
            var dirSplit = list[i].Split('/');
            var dirName = dirSplit[dirSplit.Length - 1];
            var filePaths = PluginUtil.GetFiles(string.Format("Assets/ResourcesLib/AtlasAlpha/{0}/{1}_a", GetBuildTarget(), dirName));
            var buildList = new List<Object>();
            for (int j = 0; j < filePaths.Length; j++)
            {
                buildList.Add(AssetDatabase.LoadAssetAtPath(filePaths[j], typeof(Texture2D)));
            }
            if (buildList.Count > 0)
                BuildAssetBundle(buildList.ToArray(), string.Format("AtlasAlpha/{0}_a", dirName));
        }
        LogCostTime("图集透明通道打包用时", time);
#endif
        return true;
    }

    private static bool BuildByLayer()
    {
        var result = true;
        var resultDelete = new StringBuilder();
        var resultChanged = new StringBuilder();
        var resultDepend = new StringBuilder();
        var time = InitCostTime();
        foreach (var list in m_bundleGroupDic.Values)
        {
            list.Sort((a, b) => b.Layer - a.Layer);
            var maxLayer = list[0].Layer;
            var curLayer = maxLayer;
            if (maxLayer > 1)
                BuildPipeline.PushAssetDependencies();

            for (int i = 0; i < list.Count; i++)
            {
                var bundleNode = list[i];
                if (bundleNode.Layer < curLayer)
                {
                    BuildPipeline.PushAssetDependencies();
                    curLayer = bundleNode.Layer;
                }
                if (!bundleNode.NeedRebuild || bundleNode.IsEtcAlphaAtlas)
                    continue;
                if (!bundleNode.IsScene)
                {
                    var assets = new List<Object>();
                    for (int j = 0; j < bundleNode.IncludeAssets.Count; j++)
                    {
                        var includeAsset = bundleNode.IncludeAssets[j];
                        if (includeAsset.AssetType == null)
                            assets.Add(AssetDatabase.LoadMainAssetAtPath(includeAsset.FilePath));
                        else
                            assets.Add(AssetDatabase.LoadAssetAtPath(includeAsset.FilePath, includeAsset.AssetType));
                    }
                    var outputPath = PluginUtil.GetUnifyPath(bundleNode.AssetPath);
#if UNITY_IPHONE
                    if (outputPath.IndexOf("/iPhone/") != -1)
                        outputPath = outputPath.Replace("/iPhone/", "/");
#endif
#if UNITY_ANDROID
                    if (outputPath.IndexOf("/Android/") != -1)
                        outputPath = outputPath.Replace("/Android/", "/");
#endif
                    if (bundleNode.Changed)
                    {
                        if (!bundleNode.IsPassivityChanged)
                            resultChanged.AppendLine("【变更】" + outputPath);
                        else
                            resultChanged.AppendLine("【被动变更】" + outputPath);
                    }
                    if (bundleNode.BeDepend)
                        resultDepend.AppendLine("【依赖】" + outputPath);
                    result &= BuildAssetBundle(assets.ToArray(), outputPath, curLayer == 1, !bundleNode.Changed);
                }
                else
                {
                    var paths = new List<string>();
                    for (int j = 0; j < bundleNode.IncludeAssets.Count; j++)
                    {
                        var includeAsset = bundleNode.IncludeAssets[j];
                        paths.Add(includeAsset.FilePath);
                    }
                    var outputPath = PluginUtil.GetUnifyPath(bundleNode.AssetPath);
                    if (bundleNode.Changed)
                    {
                        if (!bundleNode.IsPassivityChanged)
                            resultChanged.AppendLine("【场景变更】" + outputPath);
                        else
                            resultChanged.AppendLine("【被动场景变更】" + outputPath);
                    }
                    if (bundleNode.BeDepend)
                        resultDepend.AppendLine("【场景依赖】" + outputPath);
                    result &= BuildSceneAssetBundle(paths.ToArray(), outputPath);
                }

                if (!result)
                {
                    Debug.LogError("资源打包失败");
                    return false;
                }
            }
            if (maxLayer > 1)
            {
                for (int i = 0; i < maxLayer; i++)
                {
                    BuildPipeline.PopAssetDependencies();
                }
            }

        }

        if (resultDelete.Length > 0)
            Debug.Log(resultDelete);
        if (resultDepend.Length > 0)
            Debug.Log(resultDepend);
        if (resultChanged.Length > 0)
            Debug.Log(resultChanged);

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);   //删除操作中有可能删除工程内的资源，比如图集透明通道文件
        LogCostTime("Assetbundle增量打包用时", time);
        return true;
    }

    /// <summary>
    /// 设置Bundle的依赖、变化属性
    /// </summary>
    /// <param name="bundlePath"></param>
    /// <param name="isPassivityChanged">是否是被动改变：所依赖的包更名造成的变更</param>
    /// <returns></returns>
    private static void SetChangeProp(string bundlePath, bool isPassivityChanged = false)
    {
        if (string.IsNullOrEmpty(bundlePath)) 
            return;
        m_bundleDic[bundlePath].Changed = true;
        m_bundleDic[bundlePath].IsPassivityChanged = isPassivityChanged;

        var dependList = new List<string>();
        GetAllDepend(bundlePath, dependList);
        for (int j = 0; j < dependList.Count; j++)
        {
            m_bundleDic[dependList[j]].BeDepend = true;
        }

#if UNITY_ANDROID || UNITY_IPHONE
        var pathAtlasAlpha = UIHelper.TryGetAtlasAlphaPath(bundlePath);
        if (!string.IsNullOrEmpty(pathAtlasAlpha))
        {
            m_bundleDic[bundlePath].IsEtcAtlas = true;
            if (m_bundleDic.ContainsKey(pathAtlasAlpha))
                m_bundleDic[pathAtlasAlpha].IsEtcAlphaAtlas = true;
        }
#endif
    }

    private static void GetAllDepend(string path, List<string> dependList)
    {
        if (!m_bundleDic.ContainsKey(path) || m_bundleDic[path].IsLeaf)
            return;

        var bundleNode = m_bundleDic[path];
        foreach (var dependNode in bundleNode.DependBundles)
        {
            dependList.Add(dependNode.AssetPath);
            GetAllDepend(dependNode.AssetPath, dependList);
        }
    }

    /// <summary>
    /// 把资源文件划归到对应Bundle
    /// </summary>
    /// <param name="path"></param>
    /// <param name="recursive"></param>
    /// <param name="type"></param>
    /// <param name="combine"></param>
    /// <param name="uniqueName"></param>
    private static void BuildAssetDic(string path, bool recursive, Type type, bool combine = false, bool uniqueName = false)
    {
        var isFile = Path.HasExtension(path);
        var pathArr = !isFile ? PluginUtil.GetFiles(path, recursive) : new[] { path };
        for (int i = 0; i < pathArr.Length; i++)
        {
            var assetNode = new AssetNode();
            assetNode.FilePath = pathArr[i];
            assetNode.AssetType = type;
            var dotIndex = pathArr[i].LastIndexOf('.');
            assetNode.BundlePath = !combine ? pathArr[i].Substring(0, dotIndex > -1 ? dotIndex : pathArr[i].Length) : path.TrimEnd(new[] { '/', '\\' });
            if (uniqueName)
            {
                var suffix = Path.GetDirectoryName(PluginUtil.GetUnifyPath(!combine ? pathArr[i] : path)).Replace('/', '_');
                assetNode.BundlePath += "+" + suffix;
            }
            m_bundleInfo[assetNode.FilePath] = assetNode.BundlePath;
            m_fileDic[assetNode.FilePath] = assetNode;

            if (!m_bundleDic.ContainsKey(assetNode.BundlePath))
                m_bundleDic[assetNode.BundlePath] = new BundleNode();

            var bundleNode = m_bundleDic[assetNode.BundlePath];
            bundleNode.AssetPath = assetNode.BundlePath;
            if (bundleNode.IncludeAssets == null)
                bundleNode.IncludeAssets = new List<AssetNode>();
            if (!bundleNode.IncludeAssets.Contains(assetNode))
                bundleNode.IncludeAssets.Add(assetNode);
            if (pathArr[i].EndsWith(".unity"))
                bundleNode.IsScene = true;
        }
    }

    /// <summary>
    /// 对目录里的每个子目录进行合并打包
    /// </summary>
    /// <param name="dirPath"></param>
    /// <param name="type"></param>
    private static void BuildSecondaryCombineAssetDic(string dirPath, Type type)
    {
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }
        BuildAssetDic(dirPath, false, type, true);

        var dirPathArr = Directory.GetDirectories(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            BuildAssetDic(dirPathArr[i], false, type, true);
        }
    }

    private static void BuildRoleAnimationAssetDic(string dirPath)
    {
        var type = typeof(AnimationClip);
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }
        BuildAssetDic(dirPath, false, type, true);

        var dirPathArr = Directory.GetDirectories(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            var subDirPathArr = PluginUtil.GetDirectory(dirPathArr[i], false);
            for (int j = 0; j < subDirPathArr.Length; j++)
            {
                BuildAssetDic(subDirPathArr[j], true, type, true, true);
            }
        }
    }

    private static void BuildMapAssetDic(string dirPath)
    {
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }

        var dirPathArr = PluginUtil.GetDirectory(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            var subDirPathArr = PluginUtil.GetDirectory(dirPathArr[i]);
            for (int j = 0; j < subDirPathArr.Length; j++)
            {
                var combine = false;
                var directoryName = Path.GetFileName(subDirPathArr[j]);
                if (directoryName.Equals("Prefab", StringComparison.CurrentCultureIgnoreCase))
                    continue;
                if (directoryName.Equals("FBX", StringComparison.CurrentCultureIgnoreCase))
                    combine = true;
                if (directoryName.Equals("Materials", StringComparison.CurrentCultureIgnoreCase))
                    combine = true;
                BuildAssetDic(subDirPathArr[j], false, null, combine, true);
            }

        }
    }

    #endregion

    #region 通用处理
    /// <summary>
    /// 打包AssetBundle
    /// </summary>
    /// <param name="mainAsset">要打包的资源</param>
    /// <param name="outputPath">相对于streamingAssetsPath的保存路径</param>
    /// <param name="seal">密封打包，即不可能再被其他包依赖</param>
    /// <param name="suffix">打包后缀</param>
    /// <returns></returns>
    private static bool BuildAssetBundle(Object mainAsset, string outputPath, bool seal = false, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;

        return BuildAssetBundle(new[] { mainAsset }, outputPath, seal, false, suffix);
    }

    /// <summary>
    /// 打包AssetBundle
    /// </summary>
    /// <param name="assets">要打包到一起的资源列表</param>
    /// <param name="outputPath">相对于streamingAssetsPath的保存路径</param>
    /// <param name="seal">是否密封，如果密封，则会Push到高层打包，然后再Pop</param>
    /// <param name="temp">输出到临时路径，打包后再删除</param>
    /// <param name="suffix">打包后缀</param>
    /// <returns></returns>
    private static bool BuildAssetBundle(Object[] assets, string outputPath, bool seal = false, bool temp = false, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;
        var options = BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.DeterministicAssetBundle;
        outputPath = string.Format("{0}{1}{2}", PluginUtil.GetEditorStreamingAssetsLibPath(), outputPath, suffix);
        Object mainAsset = assets.Length == 1 ? assets[0] : null;
        assets = mainAsset == null ? assets : null;
        if (temp)
            outputPath = Path.Combine("Temp", outputPath);
        PluginUtil.CreateFileDirectory(outputPath);
        if (seal)
            BuildPipeline.PushAssetDependencies();
        var result = BuildPipeline.BuildAssetBundle(mainAsset, assets, outputPath, options, GetBuildTarget());
        if (seal)
            BuildPipeline.PopAssetDependencies();

        if (temp && File.Exists(outputPath))
            File.Delete(outputPath);

        return result;
    }

    /// <summary>
    /// 打包场景
    /// </summary>
    /// <param name="paths"></param>
    /// <param name="outputPath"></param>
    /// <param name="suffix"></param>
    /// <returns></returns>
    private static bool BuildSceneAssetBundle(string[] paths, string outputPath, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;
        outputPath = string.Format("{0}{1}{2}", PluginUtil.GetEditorStreamingAssetsLibPath(), outputPath, suffix);
        PluginUtil.CreateFileDirectory(outputPath);
        var result = BuildPipeline.BuildStreamedSceneAssetBundle(paths, outputPath, GetBuildTarget());
        if (!string.IsNullOrEmpty(result))
            Debug.LogError(result);
        return string.IsNullOrEmpty(result);
    }

    private static ConfigBuildLine LoadConfig(string path, string key)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe(path));
        var line = configFile.GetLine(key);
        Object.DestroyImmediate(configFile);
        return line;
    }

    private static ConfigBuildLine LoadConfig(string path, int index)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe(path));
        ConfigBuildLine line = null;
        if (index < configFile.m_dataArr.Length)
            line = configFile.m_dataArr[index];
        Object.DestroyImmediate(configFile);
        return line;
    }

    private static string GetDiffFileList(string prevAssetsDigestFilePath)
    {
        var time = InitCostTime("获取变更文件列表");
        var lines = File.ReadAllLines(prevAssetsDigestFilePath);
        var modifyTimeDic = new Dictionary<string, long>();
        var crcDic = new Dictionary<string, string>();
        for (int i = 0; i < lines.Length; i++)
        {
            var arr = lines[i].Split('=');
            modifyTimeDic.Add(arr[0], long.Parse(arr[1]));
            crcDic.Add(arr[0], arr[2]);
        }
        var files = PluginUtil.GetFiles("Assets/");
        var sb = new StringBuilder();
        for (int i = 0; i < files.Length; i++)
        {
            var path = files[i];
            if (modifyTimeDic.ContainsKey(path) && modifyTimeDic[path] == File.GetLastWriteTime(path).Ticks)
                continue;
            if (crcDic.ContainsKey(path) && Util.CRC32String(File.ReadAllBytes(path)) == crcDic[path])
                continue;
            sb.AppendLine(files[i]);
        }
        var result = sb.ToString();
        LogCostTime("获取变更文件列表用时", time, DEBUG_LOG ? result : "");
        return result;
    }

    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="diffFileList">注意：null代表全部重新打包，空字符串代表无变更</param>
    /// <param name="config"></param>
    /// <param name="prevConfig"></param>
    /// <param name="codeChanged"></param>
    /// <param name="configChanged"></param>
    /// <param name="resourceChanged"></param>
    private static void CheckChange(string diffFileList, ConfigBuildLine config, ConfigBuildLine prevConfig, out bool codeChanged, out bool configChanged, out bool resourceChanged)
    {
        codeChanged = false;
        configChanged = false;
        resourceChanged = false;
        if (diffFileList != null)
        {
            var fileArr = diffFileList.Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fileArr.Length; i++)
            {
                if (fileArr[i].IndexOf("Assets/", StringComparison.Ordinal) == -1)
                    continue;
                var path = PluginUtil.GetProjectPath(fileArr[i]);
                if (path.StartsWith("Assets/Scripts/") || path.StartsWith("Assets/Plugins/Game"))
                    codeChanged = true;
                if (path.StartsWith("Assets/Resources/Config/"))
                    configChanged = true;
                else if (path.StartsWith("Assets/Resources/") || path.StartsWith("Assets/ResourcesLib/"))
                    resourceChanged = true;
            }
        }
        else
        {
            //若变更列表为空，则全部重新打包
            codeChanged = true;
            configChanged = true;
            resourceChanged = true;
        }

        //本次和前置资源打包某些配置不同时，也认为代码变更，需要重新打代码
        if (prevConfig != null)
        {
            if (config.SDKName != prevConfig.SDKName || config.Encrypt != prevConfig.Encrypt || config.Obfuscate != prevConfig.Obfuscate)
                codeChanged = true;
        }

        //ios和web不会单独进行代码编译
        if (config.Platform == BuildTarget.iPhone.ToString() || config.Platform == BuildTarget.WebPlayer.ToString())
            codeChanged = false;
    }

    private static bool ExportUpdateLog()
    {
        File.Copy("Assets/Resources/UpdateLog.txt", PluginUtil.GetEditorStreamingAssetsLibPath() + "UpdateLog.txt", true);
        Debug.Log("导出更新日志文件");
        return true;
    }

    private static bool ExportCodeBackup()
    {
        //为了方便调试指定版本的资源，需要用到对应版本的代码
//        FileUtil.CopyFileOrDirectory("Assets/Scripts", "Scripts");
        return true;
    }

    private static bool ExportConfigBuildLine(ConfigBuildLine config)
    {
        try
        {
            var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
            configFile.m_dataArr = new ConfigBuildLine[] { config };
            var result = configFile.Export();
            var path = PluginUtil.GetEditorStreamingAssetsLibPath() + SUBPATH_CONFIG_BUILD_LINE;
            PluginUtil.CreateFileDirectory(path);
            File.WriteAllText(path, result, new UnicodeEncoding(false,true));
            Object.DestroyImmediate(configFile);
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            Debug.Log("导出ConfigBuildLine成功");
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("导出ConfigBuildLine失败");
            Debug.LogException(e);
            return false;
        }
    }

    private static bool ExportAssetsDigest()
    {
        try
        {
            var time = InitCostTime("导出AssetsDigest文件");
            var arr = PluginUtil.GetFiles("Assets/");
            var sb = new StringBuilder();
            for (int i = 0; i < arr.Length; i++)
            {
                sb.Append(arr[i]);
                sb.Append("=");
                sb.Append(File.GetLastWriteTime(arr[i]).Ticks);
                sb.Append("=");
                sb.AppendLine(Util.CRC32String(File.ReadAllBytes(arr[i])));
            }
            var path = PluginUtil.GetEditorStreamingAssetsLibPath() + SUBPATH_ASSETSDIGEST;
            PluginUtil.CreateFileDirectory(path);
            File.WriteAllText(path, sb.ToString());
            EditorUtility.ClearProgressBar();
            LogCostTime("导出AssetsDigest用时", time);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("导出AssetsDigest失败");
            Debug.LogException(e);
            return false;
        }
        
    }

    private static bool ExportFileList(string platform)
    {
        try
        {
            var time = InitCostTime();
            var needIgnoreMap = platform == BuildTarget.Android.ToString() || platform == BuildTarget.iPhone.ToString();
            var packageOutsideMapPathArr = new string[0];
            if (needIgnoreMap)
            {
                var mapList = Resources.Load<TextAsset>("Config/ConfigMapList").text;
                var configMapList = ScriptableObject.CreateInstance<ConfigMapList>();
                configMapList.Load(mapList);
                var packageOutsideMapPathList = new List<string>();
                for (int i = 0; i < configMapList.m_dataArr.Length; i++)
                {
                    if (configMapList.m_dataArr[i].PackageOutside)
                        packageOutsideMapPathList.Add("Scenes/map_" + configMapList.m_dataArr[i].MapId + "/");
                }
                Object.DestroyImmediate(configMapList);
                packageOutsideMapPathArr = packageOutsideMapPathList.ToArray();
            }

            var files = PluginUtil.GetFiles(PluginUtil.GetEditorStreamingAssetsLibPath());
            var fileList = new StringBuilder();
            var fileSize = new StringBuilder();
            for (int i = 0; i < files.Length; i++)
            {
                var path = PluginUtil.GetUnifyPath(files[i], true);
                if (FILELIST_SKIP_START_WITH_STR_ARR.Any(path.StartsWith))
                    continue;

                var bytes = File.ReadAllBytes(files[i]);
                var crc = Util.CRC32String(bytes);
                //根据配置对指定的地图资源加上标记
                if (needIgnoreMap && packageOutsideMapPathArr.Any(path.StartsWith))
                    fileList.AppendLine(path + "=" + crc + "=outside");
                else
                    fileList.AppendLine(path + "=" + crc);
                fileSize.AppendLine(path + "=" + bytes.Length);
            }
            
            var targetPath = PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/";
            PluginUtil.CreateFileDirectory(targetPath);
            File.WriteAllText(targetPath + "FileList.txt", fileList.ToString());
            File.WriteAllText(targetPath + "FileSize.txt", fileSize.ToString());
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            LogCostTime("生成FileList用时", time);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("生成FileList失败");
            Debug.LogException(e);
            return false;
        }
    }

    private static BuildTarget GetBuildTarget()
    {
        var buildTarget = BuildTarget.StandaloneWindows;
#if UNITY_ANDROID
        buildTarget = BuildTarget.Android;
#elif UNITY_IPHONE
        buildTarget = BuildTarget.iPhone;
#elif UNITY_WEBPLAYER
        buildTarget = BuildTarget.WebPlayer;
#elif UNITY_STANDALONE || UNITY_STANDALONE_WIN
        buildTarget = BuildTarget.StandaloneWindows;
#else
        throw new Exception("未支持的平台");
#endif
        return buildTarget;
    }

    private static bool CanRun()
    {
        if (Application.isPlaying)
        {
            Debug.LogError("不可在运行时执行");
            return false;
        }
        return true;
    }

    private static int InitCostTime(string desc = null)
    {
        if (!string.IsNullOrEmpty(desc))
            EditorUtility.DisplayProgressBar(desc, "请耐心等待...", 0);
        return TimeUtil.GetNowTimeStamp();
    }

    private static void LogCostTime(string desc, int timeStamp, string detail = "")
    {
        var dtTime = TimeUtil.GetPassTime(timeStamp);
        Debug.Log(string.Format("{0}：{1}分{2}秒\n{3}", desc, dtTime.Minutes, dtTime.Seconds, detail));
        EditorUtility.ClearProgressBar();
    }

    #endregion

    #region 数据结构
    class BundleNode
    {
        public string AssetPath;        //Bundle的路径
        public bool Changed = false;    //已改变
        public bool IsPassivityChanged = false; //被动改变
        public bool BeDepend = false;   //被依赖标记
        public bool IsScene = false;    //是否是场景
        public bool IsEtcAtlas = false;    //是否是采用ETC格式压缩的图集，打包时要根据这个做进一步处理
        public bool IsEtcAlphaAtlas = false;
        public List<AssetNode> IncludeAssets;
        public List<BundleNode> DependBundles;//引用
        public List<BundleNode> UsagesBundles;//被引用

        public bool IsRoot { get { return UsagesBundles == null; } }
        public bool IsLeaf { get { return DependBundles == null; } }
        public int Group { get; set; }
        public int Layer { get; set; }
        public bool NeedRebuild { get { return Changed | BeDepend; } }
    }

    class AssetNode
    {
        public string FilePath;     //文件路径
        public Type AssetType;      //需要打包的资源类型
        public string BundlePath;   //归属Bundle的路径
    }
    #endregion
}