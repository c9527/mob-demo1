﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 改变脚本的版本号以绕过cdn的缓存机制
/// </summary>
public static class WebJsNoCdnCache
{
    private const string WEB_STYLE_CSS_PATH = "/trunk/client/AutoBuild/web/css/styles.css";
    private const string WEB_GAME_JS_PATH = "/trunk/client/AutoBuild/web/game.js";
    private const string WEB_JS_ROOT_PATH = "/trunk/client/AutoBuild/web/Scripts";

    private static bool CSS_CHANGE;
    private static bool GAME_JS_CHANGE;
    private static bool OTHER_JS_CHANGE;
    private static List<string> jschange_index;

    /// <summary>
    /// 检查是否需要对js文件中的版本号进行自增
    /// </summary>
    /// <param name="diffFileList"></param>
    public static void CheckWebJsVersion(string diffFileList)
    {
        if (diffFileList.Contains(WEB_GAME_JS_PATH))
        {
            GAME_JS_CHANGE = true;
        }
        if (diffFileList.Contains(WEB_STYLE_CSS_PATH))
        {
            CSS_CHANGE = true;
        }
        if (diffFileList.Contains(WEB_JS_ROOT_PATH))
        {
            OTHER_JS_CHANGE = true;
            var list = diffFileList.Split(new char[] { '\n' });
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Contains(WEB_JS_ROOT_PATH))
                {
                    string path = list[i].Replace(WEB_JS_ROOT_PATH + "/", "");
                    path = path.Replace("\r", "");
                    jschange_index.Add(path);
                }
            }
        }
        CheckWebVersion();
    }

    private static void CheckWebVersion()
    {
        string[] index = File.ReadAllLines("AutoBuild/web/index.html.template");
        if (GAME_JS_CHANGE)
        {
            int gameline = GetLine(index, "game.js");
            int lastVersion = GetVersionWeb(index[gameline]);

            int version = lastVersion + 1;
            if (index[gameline].Contains(lastVersion.ToString()))
            {
                index[gameline] = index[gameline].Replace(lastVersion.ToString(), version.ToString());
            }
            Debug.Log("检测到game.js有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        if (CSS_CHANGE)
        {
            int gameline = GetLine(index, "css");
            int lastVersion = GetVersionWeb(index[gameline]);
            int version = lastVersion + 1;
            index[gameline].Replace(lastVersion.ToString(), version.ToString());
            Debug.Log("检测style.css有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        if (OTHER_JS_CHANGE)
        {
            index = CheckVersionOther(ref index);
        }
        File.WriteAllLines("AutoBuild/web/index.html.template", index);
        SvnOperation.ExeSvnCommitAll("AutoBuild/web/index.html.template", "修改改动的js，css在index.html下的版本号");

    }

    private static string[] CheckVersionOther(ref string[] index)
    {
        if (!OTHER_JS_CHANGE)
        {
            return index;
        }
        for (int i = 0; i < jschange_index.Count; i++)
        {
            int gameline = GetLine(index, jschange_index[i]);
            if (gameline < 0)
            {
                continue;
            }
            int lastVersion = GetVersionWeb(index[gameline]);
            int version = lastVersion + 1;
            index[gameline] = index[gameline].Replace(lastVersion.ToString(), version.ToString());
            Debug.Log("检测" + jschange_index[i] + "有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        return index;
    }

    private static int GetVersionWeb(string line)
    {
        string version = "";
        for (int i = 0; i < line.Length; i++)
        {
            if (line[i] == '?')
            {
                i += 3;
                while (Char.IsDigit(line[i]))
                {
                    version += line[i];
                    i++;
                }
                return int.Parse(version);
            }
        }
        return 0;
    }

    private static int GetLine(string[] index, string name)
    {
        for (int i = 0; i < index.Length; i++)
        {
            if (index[i].Contains(name))
            {
                return i;
            }
        }
        return -1;
    }
}