﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

/// <summary>
/// 播放器构建工具
/// </summary>
public static class PlayerBuilder
{
    private static readonly string[] DEBUG_LEVELS = { "Assets/Driver.unity" };
    private const BuildOptions DEBUG_BUILD_OPTIONS = BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
    private const string PATH_FN_BAT_CLIENT = @"E:\FNClient\FNClient.bat";  //蜂鸟打包工具bat文件绝对路径
    private const string PATH_CONFIG_BUILD = "AutoBuild/ConfigBuild.csv";

    public static string result_playerFullName; //上一次打出包的输出路径

    /// <summary>
    /// 初始化操作，如创建输出目录
    /// </summary>
    public static void Init(string key)
    {
        var config = LoadConfig(PATH_CONFIG_BUILD, key);
        Directory.CreateDirectory(config.OutputPath);
        if (!string.IsNullOrEmpty(config.CopyPath))
            Directory.CreateDirectory(config.CopyPath);
    }

    /// <summary>
    /// 使用ab资源发布播放器
    /// </summary>
    /// <param name="key"></param>
    /// <param name="version"></param>
    /// <param name="bigVersion"></param>
    /// <param name="debug"></param>
    /// <returns></returns>
    public static bool BuildWithAb(string key, string version, string bigVersion, bool debug = false)
    {
        var config = LoadConfig(PATH_CONFIG_BUILD, key);
        PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsPath());

        if (config.Platform == BuildTarget.StandaloneWindows.ToString())
            CopyMiniBundleToStreamingAssets();
        else
            CopyBundleToStreamingAssets(config.Platform);

        RemoveResources();
        if (!debug && config.Platform != BuildTarget.iPhone.ToString() && config.Platform != BuildTarget.WebPlayer.ToString())
            RemoveScripts();

        try
        {
            if (!Build(key, version, bigVersion, debug))
                return false;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return false;
        }
        finally
        {
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            RecoverResources();
            if (!debug && config.Platform != BuildTarget.iPhone.ToString() && config.Platform != BuildTarget.WebPlayer.ToString())
                RecoverScripts();
            PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsPath());
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        }
        return true;
    }

    /// <summary>
    /// 根据配置来构建播放器
    /// </summary>
    public static bool Build(string key, string version, string bigVersion, bool debug = false, bool clean = true)
    {
        result_playerFullName = "";
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

        var config = LoadConfig(PATH_CONFIG_BUILD, key);
        var activeTarget = EditorUserBuildSettings.activeBuildTarget;
        var configTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), config.Platform);
        if (activeTarget != configTarget)
        {
            Debug.LogError(string.Format("平台不匹配，当前项目平台为{0}，配置目标平台为{1}", activeTarget, configTarget));
            return false;
        }

        if (!ExportGameDefine(config, version, bigVersion))
            return false;

        PlayerSettings.productName = config.ProductName;
        PlayerSettings.companyName = config.CompanyName;

        ReplaceIcon(config);
        if (clean)
            CleanOutputPath(config);

        try
        {
            string result;
            if (activeTarget == BuildTarget.Android)
                result = BuildForAndroid(config, version, debug);
            else if (activeTarget == BuildTarget.StandaloneWindows)
                result = BuildForPc(config, version, debug);
            else if (activeTarget == BuildTarget.iPhone)
                result = BuildForIos(config, version, debug);
            else if (activeTarget == BuildTarget.WebPlayer)
                result = BuildForWeb(config, version, debug);
            else
            {
                Debug.LogError("未处理的平台：" + activeTarget);
                return false;
            }

            if (string.IsNullOrEmpty(result))
                Debug.Log(string.Format("【{0}】发布成功", config.Name));
            else
            {
                Debug.LogError(string.Format("【{0}】发布失败\n{1}", config.Name, result));
                return false;
            }
        }
        finally 
        {
#if UNITY_EDITOR_WIN
            SvnOperation.ExeSvnRevert("ProjectSettings/ProjectSettings.asset", true);
            SvnOperation.ExeSvnRevert("ProjectSettings/EditorBuildSettings.asset", true);
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            AssetDatabase.SaveAssets();
#endif
        }
        return true;
    }

    private static string BuildForAndroid(ConfigBuildLine config, string version, bool debug)
    {
        var levels = debug ? DEBUG_LEVELS : new[] {"Assets/GameLoader.unity"};
        var buildOption = debug ? DEBUG_BUILD_OPTIONS : BuildOptions.None;
        var isActiveSdk = !string.IsNullOrEmpty(config.SDKName);
        var playerName = Parse(config.PackageName) + (debug ? "_Debug" : "");
        var playerFullName = Path.Combine(config.OutputPath, playerName) + ".apk";
        var playerCopyFullName = Path.Combine(config.CopyPath, playerName) + ".apk";

        PlayerSettings.bundleIdentifier = config.Identifer;
        PlayerSettings.bundleVersion = version;
        PlayerSettings.Android.bundleVersionCode = int.Parse(GetTime());
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel9;
        PlayerSettings.Android.splashScreenScale = AndroidSplashScreenScale.ScaleToFill;
        PlayerSettings.Android.keystoreName = "SY2Key.keystore";
        PlayerSettings.Android.keyaliasName = "4399";
        PlayerSettings.keyaliasPass = "ssj4399";
        PlayerSettings.keystorePass = "ssj4399";

        //查错模式不单独打dll，所以这里要重新设置符号
        if (debug)
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, GetDefines(true, isActiveSdk, true));

        DeleteSdk();
        if (isActiveSdk)
            CopyAndroidSdk(config.SDKName, config.Identifer);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();
        var result = BuildPipeline.BuildPlayer(levels, playerFullName, BuildTarget.Android, buildOption);
        DeleteSdk();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

        if (string.IsNullOrEmpty(result))
        {
            result_playerFullName = playerFullName;
            if (!string.IsNullOrEmpty(config.CopyPath))
                File.Copy(playerFullName, playerCopyFullName, true);
            Debug.Log(GetSummary(playerName));

            //蜂鸟
            BuildFnClientAndUpload(config, playerFullName);
        }
        return result;
    }

    private static string BuildForIos(ConfigBuildLine config, string version, bool debug = false)
    {
        var levels = debug ? DEBUG_LEVELS : new[] { "Assets/GameLoader.unity", "Assets/UpdateWorker.unity", "Assets/Driver.unity" };
        var buildOption = debug ? DEBUG_BUILD_OPTIONS : BuildOptions.None;
        var isActiveSdk = !string.IsNullOrEmpty(config.SDKName);
        var playerName = Parse(config.PackageName) + (debug ? "_Debug" : "");
        var playerFullName = Path.Combine(config.OutputPath, playerName);

        //ios的版本号(1.2.3.456)，Build号为1.2.3456，Version号为1.2.34
        var arr = version.Split('.');
        PlayerSettings.bundleVersion = arr[0] + "." + arr[1] + "." + arr[2] + arr[3];
        PlayerSettings.shortBundleVersion = arr[0] + "." + arr[1] + "." + arr[2] + arr[3][0];
        PlayerSettings.bundleIdentifier = config.Identifer;
        PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;
        PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_5_1;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iPhone, GetDefines(true, isActiveSdk, debug));

#if UNITY_EDITOR_OSX
        config.OutputPath = "/Users/fps/Desktop/fps_ipa";
#endif

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();
        var result = BuildPipeline.BuildPlayer(levels, playerFullName, BuildTarget.iPhone, buildOption);

        if (string.IsNullOrEmpty(result))
            Debug.Log(string.Format("【{0}】Xcode生成成功", config.Name));
        else
        {
            Debug.LogError(string.Format("【{0}】Xcode生成失败", config.Name));
            return result;
        }

        //编译xcode工程生成app，再导出ipa
        var ipaFullName = playerFullName + "/build/" + playerName + ".ipa";
        PluginUtil.Excute("/bin/bash", string.Format("{0} {1} {2}", "Assets/Editor/AutoBuildIOS/shell/buildios.sh", playerFullName, playerName), false, false, true);
        if (File.Exists(ipaFullName))
        {
            result_playerFullName = playerFullName;
            Debug.Log(GetSummary(playerName));
        }
        else
            result = "未找到生成的ipa文件，" + ipaFullName;
        return result;
    }

    private static string BuildForPc(ConfigBuildLine config, string version, bool debug = false)
    {
        var levels = debug ? DEBUG_LEVELS : new[] { "Assets/GameLoader.unity" };
        var buildOption = debug ? DEBUG_BUILD_OPTIONS : BuildOptions.None;
        var isActiveSdk = !string.IsNullOrEmpty(config.SDKName);
        var playerName = Parse(config.PackageName) + (debug ? "_Debug" : "");
        var playerFullName = Path.Combine(config.OutputPath, playerName) + ".exe";

        PlayerSettings.usePlayerLog = true;

        //查错模式不单独打dll，所以这里要重新设置符号
        if (debug)
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, GetDefines(true, isActiveSdk, true));

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();
        var result = BuildPipeline.BuildPlayer(levels, playerFullName, BuildTarget.StandaloneWindows, buildOption);

        if (string.IsNullOrEmpty(result))
        {
            result_playerFullName = playerFullName;
            Debug.Log(GetSummary(playerName));
        }
        return result;
    }

    private static string BuildForWeb(ConfigBuildLine config, string version, bool debug = false)
    {
        var levels = debug ? DEBUG_LEVELS : new[] { "Assets/GameLoader.unity", "Assets/UpdateWorker.unity", "Assets/Driver.unity" };
        var buildOption = debug ? DEBUG_BUILD_OPTIONS : BuildOptions.None;
        var isActiveSdk = !string.IsNullOrEmpty(config.SDKName);
        var playerName = Parse(config.PackageName);
        var playerFullName = Path.Combine(config.OutputPath, playerName);

        PlayerSettings.usePlayerLog = true;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebPlayer, GetDefines(true, isActiveSdk, debug));

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();
        var result = BuildPipeline.BuildPlayer(levels, playerFullName, BuildTarget.WebPlayer, buildOption);

        if (string.IsNullOrEmpty(result))
        {
            //增加自定义的index.html主页和资源、脚本
            FileUtil.CopyFileOrDirectory("AutoBuild/web/Assets", playerFullName + "/Assets");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/Scripts", playerFullName + "/Scripts");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/css", playerFullName + "/css");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/images", playerFullName + "/images");
            var mainStr = File.ReadAllText("AutoBuild/web/index.html.template");
            var jsStr = File.ReadAllText("AutoBuild/web/game.js");
            mainStr = mainStr.Replace("{0}", config.ProductName);
            jsStr = jsStr.Replace("{1}", playerName + ".unity3d");
            jsStr = jsStr.Replace("{2}", config.UpdateUrl);
            File.WriteAllText(playerFullName + "/index.html", mainStr);
            File.WriteAllText(playerFullName + "/Scripts/game.js", jsStr, Encoding.UTF8);

            result_playerFullName = playerFullName;
            Debug.Log(GetSummary(playerName));
        }
        return result;
    }

    /// <summary>
    /// 替换Icon
    /// </summary>
    private static void ReplaceIcon(ConfigBuildLine config)
    {
        if (!string.IsNullOrEmpty(config.Icon))
        {
            var btg = config.Platform;
            if (config.Platform == "StandaloneWindows")
                btg = "Standalone";
            var buildTargetGroup = (BuildTargetGroup)Enum.Parse(typeof(BuildTargetGroup), btg);
            var icon = AssetDatabase.LoadMainAssetAtPath("Assets/Editor/Project/" + config.Icon) as Texture2D;
            var arr = new Texture2D[PlayerSettings.GetIconSizesForTargetGroup(buildTargetGroup).Length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = icon;
            }
            PlayerSettings.SetIconsForTargetGroup(buildTargetGroup, arr);
        }
    }

    /// <summary>
    /// 清理输出目录
    /// </summary>
    private static void CleanOutputPath(ConfigBuildLine config)
    {
        Directory.CreateDirectory(config.OutputPath);
        if (!string.IsNullOrEmpty(config.CopyPath))
            Directory.CreateDirectory(config.CopyPath);

        var fileArr = Directory.GetFiles(config.OutputPath);
        for (int i = 0; i < fileArr.Length; i++)
        {
            FileUtil.DeleteFileOrDirectory(fileArr[i]);
        }
        var dirArr = Directory.GetDirectories(config.OutputPath);
        for (int i = 0; i < dirArr.Length; i++)
        {
            if (dirArr[i] == "zjqz_splash") //为了配合蜂鸟打包工具的替换闪屏功能
                continue;
            FileUtil.DeleteFileOrDirectory(dirArr[i]);
        }
    }

    public static void BuildFnClientAndUpload(ConfigBuildLine config, string playerFullName)
    {
        var pArr = config.ChannelNames;
        if (pArr == null || pArr.Length <= 0)
            return;

        var args = "-g " + config.FnGameName;
        args += " -p " + string.Join("#", pArr);
        args += " -i " + playerFullName;
        args += " -o " + config.OutputPath;
        args += " -q";
        if (!config.Corner) args += " -nc";
        if (!config.Splash) args += " -ns";
        var lines = File.ReadAllLines(PATH_FN_BAT_CLIENT);
        if (lines[lines.Length - 1].Trim() != "exit")
        {
            lines = lines.ArrayConcat("exit");
            File.WriteAllLines(PATH_FN_BAT_CLIENT, lines, Encoding.ASCII);
        }
        PluginUtil.Excute(@"AutoBuild\BuildPlatformApkAndUpload.bat", args);
    }

    private static bool ExportGameDefine(ConfigBuildLine config, string version, string bigVersion, bool inner = false)
    {
        try
        {
            var result = new StringBuilder();
            result.AppendLine("Version=" + version);
            result.AppendLine("BigVersion=" + bigVersion);

            //拼接相关选项
            var fields = typeof(ConfigBuildLine).GetFields();
            for (int i = 0; i < fields.Length; i++)
            {
                var name = fields[i].Name;
                var value = fields[i].GetValue(config).ToString();
                if (inner)
                    value = value.Replace("http://zjqz-cdnres.me4399.com", "file:////172.16.10.158/zjqz");
                result.AppendLine(name + "=" + value);
            }

            var path = PluginUtil.GetEditorStreamingAssetsPath() + "VersionInfo/GameDefine.txt";
            PluginUtil.CreateFileDirectory(path);
            File.WriteAllText(path, result.ToString(), new UTF8Encoding(false));
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            Debug.Log("导出PlayerBuilder GameDefine完毕\n" + result);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("导出PlayerBuilder GameDefine失败");
            Debug.LogException(e);
            return false;
        }
    }

    private static ConfigBuildLine LoadConfig(string path, string key)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe(path));
        var line = configFile.GetLine(key);
        Object.DestroyImmediate(configFile);
        return line;
    }

    private static void CopyMiniBundleToStreamingAssets()
    {
        var arr = new[]
        {
            "Managed",
            "VersionInfo",
            "Atlas/UpdateWorkerBg.assetbundle",
            "Atlas/UpdateWorker.assetbundle",
            "UI/Update/PanelUpdateBg.assetbundle",
            "UI/Update/PanelUpdate.assetbundle",
            "Depend.assetbundle",
            "Config.assetbundle",
            "LuaRoot.assetbundle",
            "Font/Normal.assetbundle"
        };

        for (int i = 0; i < arr.Length; i++)
        {
            FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath() + arr[i], PluginUtil.GetEditorStreamingAssetsPath() + arr[i]);
        }
    }

    private static void CopyBundleToStreamingAssets(string platform)
    {
        FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath().TrimEnd('/'), PluginUtil.GetEditorStreamingAssetsPath().TrimEnd('/'));

        if (platform == BuildTarget.iPhone.ToString() || platform == BuildTarget.Android.ToString())
        {
            var mapList = Resources.Load<TextAsset>("Config/ConfigMapList").text;
            var configMapList = ScriptableObject.CreateInstance<ConfigMapList>();
            configMapList.Load(mapList);
            var packageOutsideMapPathList = new List<string>();
            for (int i = 0; i < configMapList.m_dataArr.Length; i++)
            {
                if (configMapList.m_dataArr[i].PackageOutside)
                    packageOutsideMapPathList.Add("Scenes/map_" + configMapList.m_dataArr[i].MapId + "/");
            }
            var packageOutsideMapPathArr = packageOutsideMapPathList.ToArray();
            var scenesDir = Directory.GetDirectories(PluginUtil.GetEditorStreamingAssetsPath() + "Scenes/");
            for (int i = 0; i < scenesDir.Length; i++)
            {
                var path = PluginUtil.GetUnifyPath(scenesDir[i]) + "/";
                if (path.StartsWithAny(packageOutsideMapPathArr) && Directory.Exists(scenesDir[i]))
                    FileUtil.DeleteFileOrDirectory(scenesDir[i]);
            }
        }
    }

    private static void RemoveResources()
    {
        AssetDatabase.RenameAsset("Assets/Resources", "ResourcesTmp");
    }

    private static void RecoverResources()
    {
        AssetDatabase.RenameAsset("Assets/ResourcesTmp", "Resources");
    }

    private static void RemoveScripts()
    {
        AssetDatabase.MoveAsset("Assets/Scripts", "Assets/Editor/Scripts");
    }

    private static void RecoverScripts()
    {
        AssetDatabase.MoveAsset("Assets/Editor/Scripts", "Assets/Scripts");
    }

    private static void CopyAndroidSdk(string sdkName, string packageName)
    {
        const string pluginDir = "Android";
        var from = string.Format("../sdk/Output/{0}/{1}", sdkName, pluginDir);
        var to = string.Format("Assets/Plugins/{0}", pluginDir);
        FileUtil.CopyFileOrDirectory(from, to);

        var pathManifest = to + "/AndroidManifest.xml";
        var manifest = File.ReadAllText(pathManifest);
        manifest = manifest.Replace("com.fps.sdk", packageName);
        File.WriteAllText(pathManifest, manifest);
    }

    private static void DeleteSdk()
    {
        AssetDatabase.DeleteAsset("Assets/Plugins/Android");
    }

    /// <summary>
    /// 解析动态字符串
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    private static string Parse(string content)
    {
        var ios = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iPhone;
        content = content.Replace("{Time}", GetTime());
        content = content.Replace("{Version}", ios ? PlayerSettings.shortBundleVersion : PlayerSettings.bundleVersion);
        return content;
    }

    private static string GetTime()
    {
        return DateTime.Now.ToString("yyMMddHHmm");
    }

    private static string GetSummary(string packageName)
    {
        var ios = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iPhone;
        var result = new StringBuilder();
        result.AppendFormat("{0}：{1}\n", "包文件名", packageName);
        result.AppendFormat("{0}：{1}\n", "APP名", PlayerSettings.productName);
        result.AppendFormat("{0}：{1}\n", "包名", PlayerSettings.bundleIdentifier);
        result.AppendFormat("{0}：{1}\n", "版本号", ios ? PlayerSettings.shortBundleVersion : PlayerSettings.bundleVersion);
        if (!ios)
            result.AppendFormat("{0}：{1}\n", "版本Code", PlayerSettings.Android.bundleVersionCode);
        return result.ToString();
    }

    private static string GetDefines(bool isAb, bool isSdk, bool isDebug)
    {
        var list = new List<string>();
        if (isAb)
            list.Add("Assetbundle");
        if (isSdk)
            list.Add("ActiveSDK");
        if (isDebug)
            list.Add("DebugErrorMode");
        return string.Join(";", list.ToArray());
    }
}