﻿using System.IO;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 版本发布流程控制
/// </summary>
public static class Publisher
{
    private const string PATH_CONFIG_BUILD = "AutoBuild/ConfigBuild.csv";

    public static void PublishHotUpdate(string key = "10002", bool officialPublish = true, bool nextVersion = true)
    {
        var publishName = LoadConfig(PATH_CONFIG_BUILD, key).PublishName;
        var branchName = GetProjectName();
        var prevResDir = VersionManager.GetLastResDir(publishName, branchName);
        
        if (!VersionManager.CheckBranchExist(publishName, branchName))
            return;
        if (!ResourcePacker.Pack(key, prevResDir))
            return;

        string version;
        string bigVersion;
        var versionType = officialPublish && nextVersion ? NextVersionType.NextVersion : NextVersionType.Current;
        VersionManager.GetVersion(publishName, branchName, officialPublish, out version, out bigVersion, versionType);
        if (!HotUpdater.Update(key, version, bigVersion))
            return;

        VersionManager.Backup("热更");
        VersionManager.SetLastResDir(publishName, branchName, HotUpdater.GetOutPutPath(version));
        VersionManager.SetVersion(publishName, branchName, officialPublish, version, bigVersion);
        VersionManager.HotUpdate(publishName, branchName, officialPublish);
    }

    public static void PublishPlayer(string key = "10002", bool addAppStoreVer = false)
    {
        const string prevResDir = "F:/union/1.0.0.0/";
        const bool officialPublish = true;
        var publishName = LoadConfig(PATH_CONFIG_BUILD, key).PublishName;
        var branchName = GetProjectName();
        if (!VersionManager.CheckBranchExist(publishName, branchName))
            return;
        var serverlist = !VersionManager.IsOnlineBranch(publishName, branchName) ? "Check" : "";
        if (!ResourcePacker.Pack(key, prevResDir))
            return;

        string version;
        string bigVersion;
        var versionType = addAppStoreVer ? NextVersionType.NextAppStoreVersion : NextVersionType.Current;
        VersionManager.GetVersion(publishName, branchName, officialPublish, out version, out bigVersion, versionType);
        if (!PlayerBuilder.BuildWithAb(key, version, bigVersion))
            return;

        VersionManager.Backup("整包");
        VersionManager.SetVersion(publishName, branchName, officialPublish, version, bigVersion);
        VersionManager.PublishPlayer(publishName, branchName, serverlist);
    }

    public static void ChangeOnlineBranch(string key = "10002")
    {
        var publishName = LoadConfig(PATH_CONFIG_BUILD, key).PublishName;
        var branchName = GetProjectName();
        if (!VersionManager.CheckBranchExist(publishName, branchName))
            return;

        VersionManager.Backup("转正");
        VersionManager.ChangeOnlineBranch(publishName, branchName);
        VersionManager.HotUpdate(publishName, branchName, true);
    }

    public static void CreateNextBranch(string key = "10002")
    {
        
    }

    public static void ContinueFnClient(string key)
    {
        if (!string.IsNullOrEmpty(PlayerBuilder.result_playerFullName))
            PlayerBuilder.BuildFnClientAndUpload(LoadConfig(PATH_CONFIG_BUILD, key), PlayerBuilder.result_playerFullName);
    }

    private static ConfigBuildLine LoadConfig(string path, string key)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe(path));
        var line = configFile.GetLine(key);
        Object.DestroyImmediate(configFile);
        return line;
    }

    private static string GetProjectName()
    {
        return new DirectoryInfo(Application.dataPath).Parent.Parent.Name;
    }
}