﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// 热更新管理器
/// </summary>
public static class HotUpdater
{
    private const string PATH_RES_LIB = "F:/union/";

    public static bool Update(string key, string version, string bigVersion)
    {
        if (!CopyLocalToLib(key, version, bigVersion)) 
            return false;

        //拷贝到Upload目录

        //数传

        //创建蓝海工单
        return true;
    }

    public static string GetOutPutPath(string version)
    {
        return PATH_RES_LIB + version;
    }

    private static bool CopyLocalToLib(string key, string version, string bigVersion)
    {
        var config = LoadConfig("AutoBuild/ConfigBuild.csv", key);

        //拷贝本地文件到库目录
        if (config.Platform == BuildTarget.Android.ToString() || config.Platform == BuildTarget.iPhone.ToString() ||
            config.Platform == BuildTarget.StandaloneWindows.ToString())
        {
            if (!ExportGameDefine(version, bigVersion))
                return false;

            if (Directory.Exists(PATH_RES_LIB + version))
                FileUtil.DeleteFileOrDirectory(PATH_RES_LIB + version);

            FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath().TrimEnd(new[] {'/', '\\'}),
                PATH_RES_LIB + version);
        }
        else if (config.Platform == BuildTarget.WebPlayer.ToString())
        {
            //打整包
            if (!PlayerBuilder.BuildWithAb(key, version, bigVersion))
                return false;

            if (Directory.Exists(PATH_RES_LIB + config.PackageName))
                FileUtil.DeleteFileOrDirectory(PATH_RES_LIB + config.PackageName);

            FileUtil.CopyFileOrDirectory(config.OutputPath.TrimEnd(new[] {'/', '\\'}) + "\\" + config.PackageName, PATH_RES_LIB + config.PackageName);
        }
        else
        {
            Debug.LogError("未处理的平台：" + config.Platform);
            return false;
        }

        if (!ExportServerCrc(config.Platform, PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/FileList.txt",version))
            return false;
//        if (!SvnOperation.ExeSvnCommitAll("../server/cfg/crc/", string.Format("[发版] {0} {1}", version, EditorUserBuildSettings.activeBuildTarget)))
//            return false;
        return true;
    }

    private static bool ExportServerCrc(string platform, string fileListPath, string version)
    {
        try
        {
            var crcList = new StringBuilder();
            crcList.AppendLine("return {");
            var fileListArr = File.ReadAllLines(fileListPath);
            for (int i = 0; i < fileListArr.Length; i++)
            {
                var arr = fileListArr[i].Split('=');
                var path = arr[0];
                var crc = arr[1];
                crcList.AppendLine("[\"" + path + "\"] = " + "{ Path = \"" + path + "\", CRC = 0x" + crc + "},");
            }
            crcList.AppendLine("}");

            var plat = "xxxx";
            if (platform == BuildTarget.Android.ToString())
                plat = "android";
            else if (platform == BuildTarget.iPhone.ToString())
                plat = "ios";
            else if (platform == BuildTarget.StandaloneWindows.ToString())
                plat = "pc";
            else if (platform == BuildTarget.WebPlayer.ToString())
                plat = "web";

            var filelistNameNoVersion = string.Format("{0}_clientfilelist.config", plat);
            File.WriteAllText("../server/cfg/crc/" + filelistNameNoVersion, crcList.ToString());

            if (!string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(version))
            {
                if (version.Split('.').Length != 4)
                    version = BuildTools.GetNextGameVersion(version, BuildVersionType.Current);

                var filelistName = string.Format("{0}_{1}_clientfilelist.config", version.Replace('.', '-'), plat);
                File.WriteAllText("../server/cfg/crc/" + filelistName, crcList.ToString());
            }
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            Debug.Log("生成ServerCRC成功");
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("生成ServerCRC失败");
            Debug.LogException(e);
            return false;
        }
    }

    private static bool ExportGameDefine(string version, string bigVersion)
    {
        try
        {
            var result = new StringBuilder();
            result.AppendLine("Version=" + version);
            result.AppendLine("BigVersion=" + bigVersion);

            var path = PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/GameDefine.txt";
            PluginUtil.CreateFileDirectory(path);
            File.WriteAllText(path, result.ToString(), new UTF8Encoding(false));
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            Debug.Log("导出HotUpdater GameDefine完毕\n" + result);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("导出HotUpdater GameDefine失败");
            Debug.LogException(e);
            return false;
        }
    }

    private static ConfigBuildLine LoadConfig(string path, string key)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe(path));
        var line = configFile.GetLine(key);
        Object.DestroyImmediate(configFile);
        return line;
    }

    
}