using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.XCodeEditor;
using System.Xml;
#endif
using System.IO;

public static class XCodePostProcess
{
	#if UNITY_EDITOR
	[PostProcessBuild (100)]
	public static void OnPostProcessBuild (BuildTarget target, string pathToBuiltProject)
	{
		if (target != BuildTarget.iPhone) {
			Debug.LogWarning ("Target is not iPhone. XCodePostProcess will not run");
			return;
		}
		
		//得到xcode工程的路径
		//string path = Path.GetFullPath (pathToBuiltProject);
		
		// Create a new project object from build target
		XCProject project = new XCProject (pathToBuiltProject);
		
		// Find and run through all projmods files to patch the project.
		// Please pay attention that ALL projmods files in your project folder will be excuted!
		//在这里面把frameworks添加在你的xcode工程里面

		if (AutoBuild.m_lastBuildParams != null && AutoBuild.m_lastBuildParams.isActiveSdk) {
			string p_url;
			if (AutoBuild.m_lastBuildParams.SdkName == "4399iosRuSdk") 
				p_url = "Ru";
			else if (AutoBuild.m_lastBuildParams.SdkName == "4399iosEnSdk") 
				p_url = "En";
			else
				p_url = "Ch";
            var url = string.Concat(Application.dataPath, "/", "Editor", "/", "AutoBuildIOS", "/", "XUPorter", "/", "Mods", "/", p_url);
            Debug.Log("p_url:" + url);
			string[] files = Directory.GetFiles (/*Application.dataPath,*/url, "*.projmods", SearchOption.AllDirectories);
			foreach (string file in files) {
				Debug.Log("projmods:" + file);
				project.ApplyMod (file);
			}
		}
		
		//增加一个编译标记。。没有的话sharesdk会报错。。
		//project.AddOtherLinkerFlags("-licucore");
		
		//设置签名的证书， 第二个参数 你可以设置成你的证书
		//string codeSigning = "iPhone Distribution: Guangzhou Aiyou Information Technology Co., Ltd.";
		//project.overwriteBuildSetting ("CODE_SIGN_IDENTITY", codeSigning, "Release");
		//project.overwriteBuildSetting ("CODE_SIGN_IDENTITY", codeSigning, "Debug");
		
		//解决报错：Warning: --resource-rules has been deprecated in Mac OS X >= 10.10!
		project.overwriteBuildSetting ("CODE_SIGN_RESOURCE_RULES_PATH", "$(SDKROOT)/ResourceRules.plist", "Release");
		project.overwriteBuildSetting ("CODE_SIGN_RESOURCE_RULES_PATH", "$(SDKROOT)/ResourceRules.plist", "Debug");
		
		//关闭Bitcode功能
		project.overwriteBuildSetting("ENABLE_BITCODE", "NO", "Release");
		project.overwriteBuildSetting("ENABLE_BITCODE", "NO", "Debug");
		
		//设置证书
		project.overwriteBuildSetting ("PROVISIONING_PROFILE", "0ef5de73-6b66-4e75-8b1d-ee67d2d6e484", "Release");
		project.overwriteBuildSetting ("PROVISIONING_PROFILE", "0ef5de73-6b66-4e75-8b1d-ee67d2d6e484", "Debug");
		
		//添加编译标记-ObjC
		project.AddOtherLinkerFlags ("-ObjC");
		
		// 编辑plist 文件
		EditorPlist(pathToBuiltProject);
		
		//编辑代码文件
		EditorCode(pathToBuiltProject);
		
		// Finally save the xcode project
		project.Save ();
		
		
		//上面的介绍 我已经在上一篇文章里面讲过， 这里就不赘述 。。
		//那么当我们打完包以后 如果需要根据不同平台 替换 不同的framework plist oc 包名 等等。。。
		
		//这里输出的projectName 就是 91  
		//		Debug.Log(projectName);
		
		//        if(projectName== "91")
		//        {
		//             当我们在打91包的时候 这里面做一些 操作。
		//        }
		
	}
	
	public static string projectName
	{
		get
		{
			foreach(string arg in System.Environment.GetCommandLineArgs()) {
				if(arg.StartsWith("project"))
				{
					return arg.Split("-"[0])[1];
				}
			}
			return "test";
		}
	}
	
	private static void EditorPlist(string filePath)
	{
		
		XCPlist list =new XCPlist(filePath);
		string bundle;
		if (AutoBuild.m_lastBuildParams.SdkName == "4399iosRuSdk") 
			bundle = "com.qzyx.ssjj";
		else if (AutoBuild.m_lastBuildParams.SdkName == "4399iosEnSdk") 
			bundle = "com.qzyx.ios.en4399";
		else
			bundle = "com.4399sy.zjqw";

		string PlistAdd = @"  
            <key>CFBundleURLTypes</key>
            <array>
            <dict>
            <key>CFBundleTypeRole</key>
            <string>Editor</string>
            <key>CFBundleURLIconFile</key>
            <string>Icon@2x</string>
            <key>CFBundleURLName</key>
            <string>"+bundle+@"</string>
            <key>CFBundleURLSchemes</key>
            <array>
            <string>ww123456</string>
            </array>
            </dict>
            </array>";
		string PlistString = @"
            <key>NSAppTransportSecurity</key>
            <dict>
            <key>NSAllowsArbitraryLoads</key>
            <true/>
            </dict>
			<key>NSPhotoLibraryUsageDescription</key>
    		<string>访问相册</string>
    		<key>NSCameraUsageDescription</key>
    		<string>访问相机</string>
            <key>UIViewControllerBasedStatusBarAppearance</key>
            <false/>
			<key>CFBundleURLTypes</key>				
				<array>				
					<dict>				
						<key>CFBundleURLName</key>				
							<string>weixin</string>				
						<key>CFBundleURLSchemes</key>				
							<array>				
							<string>wx959c73bbc7076ad1</string>				
							</array>				
					</dict>				
				</array>
			<key>UIRequiresFullScreen</key>
    		<true />
    		<key>CFBundleAllowMixedLocalizations</key>
    		<true />				
			<key>LSApplicationQueriesSchemes</key>				
				<array>				
					<string>wechat</string>				
					<string>weixin</string>				
				</array>";
		string PlistStringEnRu = @"
			<key>CFBundleURLTypes</key>
			<array>
				<dict>
					<key>CFBundleURLName</key>
					<string></string>
					<key>CFBundleURLSchemes</key>
					<array>
						<string>vk5691691</string>
						<string>fb1322705247760247</string>
					</array>
				</dict>
			</array>
			<key>UIRequiresFullScreen</key>
			<true/>
			<key>CFBundleAllowMixedLocalizations</key>
			<true/>
			<key>FacebookAppID</key>
			<string>1322705247760247</string>";
		
		//在plist里面增加一行
		list.AddKey(PlistString);
		Debug.Log ("EditorPlist PlistString:---"+PlistString);
		if ((AutoBuild.m_lastBuildParams.SdkName == "4399iosRuSdk") || (AutoBuild.m_lastBuildParams.SdkName == "4399iosEnSdk"))
		{
			list.AddKey (PlistStringEnRu);
			Debug.Log ("EditorPlist PlistStringEnRu:---"+PlistStringEnRu);
		}
			
		//在plist里面替换一行
		//list.ReplaceKey("<string>com.ssjjsyinner.${PRODUCT_NAME}</string>","<string>"+bundle+"</string>");
		//保存
		list.Save();
		
	}
	
	private static void EditorCode(string filePath)
	{
		//读取UnityAppController.mm文件
		XClass UnityAppController = new XClass(filePath + "/Classes/UnityAppController.mm");
		XClass KeyboardGCRext = new XClass (filePath + "/Classes/UI/Keyboard.mm");
		string wrap = "\n";
		
		string unityAppControllerHeader = 
			"\r#import \"BaiduGameStat.h\""+ wrap + 
				"#import <AdSupport/AdSupport.h>"+ wrap + 
				"#import \"InMobi.h\""+ wrap + 
				"#import \"IMBanner.h\""+ wrap + 
				"#import \"IMBannerDelegate.h\""+ wrap + 
				"#import \"IMIncentivisedDelegate.h\""+ wrap + 
				"#import \"IMInterstitial.h\""+ wrap + 
				"#import \"IMInterstitialDelegate.h\""+ wrap + 
				"#import \"IMNative.h\""+ wrap + 
				"#import \"IMNativeDelegate.h\""+ wrap + 
				"#import \"IMConstants.h\""+ wrap + 
				"#import \"IMError.h\""+ wrap + 
				"#import \"InMobiAnalytics.h\""+ wrap + 
				"#import \"ACTReporter.h\""+ wrap + 
				"#import \"WXApi.h\""+ wrap + 
				"#import <Chartboost/Chartboost.h>"+ wrap + 
				"#import <CommonCrypto/CommonDigest.h>"+ wrap + 
				"#import \"SsjjSyiOSSDK.h\""+ wrap + 
				"#import \"GeTui.h\""+ wrap;
		
		string unityAppControllerMarket =
			"\t[InMobi initialize:@\"ffd6bcba5d134271a774219f9ffb2f11\"];\r\t" +
				"[ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@\"947011225\"];\r\t" +
				"[ACTConversionReporter reportWithConversionID:@\"947011225\" label:@\"2GM6CKSLkmEQmf3IwwM\" value:@\"0.00\" isRepeatable:NO];\r\t" +
				"[Chartboost startWithAppId:@\"563c230ba8b63c42d1f5197d\" appSignature:@\"f29fd7868599d626917f5ba63b780fbcbca14392\" delegate:(id)self];\r\t" +
				"NSString *baiduKey = [[NSString alloc]initWithString:@\"395697aa5c\"];\r\t" +
				
				"BaiduGameStat *statTracker = [BaiduGameStat defaultStat];\r\t" +
				
				"[statTracker setChannelId:@\"Baidu-Mobads\" appKey:baiduKey];\r\t" +
				"[statTracker setLogSendWifiOnly:YES appKey:baiduKey];\r\t" +
				"[statTracker setSessionResumeInterval:30 appKey:baiduKey];\r\t" +
				"[statTracker setGameVersion:@\"1.0\" appKey:baiduKey];\r\t" +
				"[statTracker setEnableDebugOn:YES appKey:baiduKey];\r\t" +
				"[statTracker setExceptionLogEnable:YES appKey:baiduKey];\r\t" +
				"NSString *adId = @\"\";\r\t" +
				"if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0f)\r\t{\r\t\t" +
				"adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];\r\t}\r\t" +
				"[statTracker setAdid:adId appKey:baiduKey];\r\t" +
				"[statTracker startWithAppKey:baiduKey];\r\t" +
				"return [WXApi registerApp:@\"wx959c73bbc7076ad1\"];\r";
		string unityAppControllerGetui = 
			"- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo\n{\n\t" +
				"AppController_SendNotificationWithArg(kUnityDidReceiveRemoteNotification, userInfo);\n\t" + 
				"UnitySendRemoteNotification(userInfo);\n}\n\n" + 
				"- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken\n{\n\t" +
				"AppController_SendNotificationWithArg(kUnityDidRegisterForRemoteNotificationsWithDeviceToken, deviceToken);\n\t" + 
				"UnitySendDeviceToken(deviceToken);\n}\n\n" +
				"- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error\n{\n\t" +
				"AppController_SendNotificationWithArg(kUnityDidFailToRegisterForRemoteNotificationsWithError, error);\n\t" + 
				"UnitySendRemoteNotificationError(error);\n}\n";
		string unityAppControllerGetuiReplace = 
			"- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {\n\t" +
				"[application registerForRemoteNotifications];\n}\n\n" +
				"- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {\n\t" +
				"NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@\"<>\"]];\n\t" +
				"token = [token stringByReplacingOccurrencesOfString:@\" \" withString:@\"\"];\n\t" +
				"NSLog(@\">>>[DeviceToken Success]:%@\", token);\n\t" +
				"[[SsjjSyiOSSDK sharedInstance]sendMessage:@\"IOS_4399_GetDeviceToken\" param:token];\n\t" +
				"[GeTuiSdk registerDeviceToken:token];\n}\n\n" +
				"- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {\n}\n\n" +
				"- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {\n\t" +
				"application.applicationIconBadgeNumber = 0;\n}\n\n" +
				"- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {\n\t" +
				"completionHandler(UIBackgroundFetchResultNewData);\n}\n";
		string keyboardGCRext = 
			"\tCGRect tempRect = textField.frame;\r\t" +
				"textField.frame = CGRectMake(tempRect.origin.x, tempRect.origin.y, tempRect.size.width, 30);\r";		
		
		
		//在指定代码后面增加一行代码
		//UnityAppController.WriteBelow("#include \"PluginBase/AppDelegateListener.h\"","#import <ShareSDK/ShareSDK.h>");
		
		//在指定代码中替换一行
		//UnityAppController.Replace("return YES;","return [ShareSDK handleOpenURL:url sourceApplication:sourceApplication annotation:annotation wxDelegate:nil];");
		
		//在指定代码后面增加一行
		//UnityAppController.WriteBelow("UnityCleanup();\n}","- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url\r{\r    return [ShareSDK handleOpenURL:url wxDelegate:nil];\r}");
		//Debug.Log ("EditorPlist unityAppControllerHeader:---"+unityAppControllerHeader);
		//Debug.Log ("EditorPlist unityAppControllerMarket:---"+unityAppControllerMarket);
		UnityAppController.WriteBelow ("#include \"PluginBase/AppDelegateListener.h\"", unityAppControllerHeader);
		UnityAppController.Replace ("\treturn   (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationPortraitUpsideDown)\n",                                             
		                            "\treturn   (0 << UIInterfaceOrientationPortrait) | (0 << UIInterfaceOrientationPortraitUpsideDown)\r");
		
		UnityAppController.WriteBelow ("[self preStartUnity];\n", unityAppControllerMarket);
		KeyboardGCRext.WriteBelow ("[self updateInputHidden];\n", keyboardGCRext);
		
		UnityAppController.Replace (unityAppControllerGetui, unityAppControllerGetuiReplace);
	}
	#endif
}














