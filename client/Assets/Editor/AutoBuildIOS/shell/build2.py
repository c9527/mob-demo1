#!/bin/sh
#参数判断
if [ $# != 4 ];then
    echo "需要四个参数。 参数是游戏包的名子，打包时间，存放ipa路径，备份路径"
    exit
fi
    
#UNITY程序的路径#
UNITY_PATH=/Applications/Unity/Unity.app/Contents/MacOS/Unity
    
#先关闭unity工程
UNITY_PID=`ps -ef | grep -i $UNITY_PATH | grep -v grep | perl -ane 'print $F[1]'`
echo $UNITY_PID
if [[ $UNITY_PID != '' ]];then
    kill -9 $UNITY_PID
fi
    
#游戏程序路径#
PROJECT_PATH=/Users/fps/Documents/fps_code/client
    
#IOS打包脚本路径#
BUILD_IOS_PATH=${PROJECT_PATH}/Assets/Editor/AutoBuildIOS/shell/buildios.sh
    
#生成的Xcode工程路径#
XCODE_PATH=${PROJECT_PATH}/$1
    
echo "开始生成XCODE工程..."
    
#将unity导出成xcode工程#
$UNITY_PATH -projectPath $PROJECT_PATH -executeMethod ProjectBuild.BuildForIPhone project-$1 -quit
    
echo "XCODE工程生成完毕"
    
#开始生成ipa#
$BUILD_IOS_PATH $PROJECT_PATH/$1 $1

echo "ipa生成完毕"
    
cp $PROJECT_PATH/$1/build/$1.ipa $3
cp $3/$1.ipa $4/$1_$2.ipa
    
echo "开始将ipa上传到中转发版"
    
#上传IPA脚本路径#
IPA_UPLOAD_PATH=/Users/fps/Desktop/upload.txt
    
#开始上传
$IPA_UPLOAD_PATH $PROJECT_PATH/$1/build/$1.ipa

