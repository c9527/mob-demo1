﻿using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CanEditMultipleObjects]
[CustomEditor(typeof (Font))]
public class FontInspector : Editor
{
    private TextAsset m_bmfontData;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        m_bmfontData = EditorGUILayout.ObjectField(new GUIContent("BMFont", "Drag .fnt file in, to generate Character Rects and Line Spacing"), m_bmfontData, typeof(TextAsset), false) as TextAsset;
        if (m_bmfontData != null)
        {
            var bmfont = new BMFont();
            var tmp = m_bmfontData;
            m_bmfontData = null;
            BMFontReader.Load(bmfont, tmp.name, tmp.bytes);
            var charInfo = new CharacterInfo[bmfont.glyphCount];
            for (int i = 0; i < charInfo.Length; i++)
            {
                var bmInfo = bmfont.Glyphs[i];
                var info = new CharacterInfo();
                info.index = bmInfo.index;
                info.uv.x = (float) bmInfo.x/(float) bmfont.texWidth;
                info.uv.y = (float) bmInfo.y/(float) bmfont.texHeight;
                info.uv.width = (float) bmInfo.width/(float) bmfont.texWidth;
                info.uv.height = -(float) bmInfo.height/(float) bmfont.texHeight;

                info.vert.x = (float)bmInfo.offsetX;
                info.vert.y = -(float)bmInfo.height;
                info.vert.width = (float)bmInfo.width;
                info.vert.height = (float)bmInfo.height;

                info.width = (float) bmInfo.advance;
                charInfo[i] = info;
            }
            var font = target as Font;
            if (font)
            {
                font.characterInfo = charInfo;
                var fontPath = AssetDatabase.GetAssetPath(font);
                var text = File.ReadAllText(fontPath);
                text = Regex.Replace(text, @"(  m_LineSpacing:) \d*", "$1 " + (bmfont.charSize + 1));
                File.WriteAllText(fontPath, text);
                AssetDatabase.Refresh();
            }

            EditorApplication.SaveScene();
        }
    }
}