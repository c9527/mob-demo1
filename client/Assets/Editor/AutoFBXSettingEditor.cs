﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;


public class AutoFBXSettingEditor : EditorWindow
{
    [Serializable]
    public enum AvatarDefinitionEnum
    {
        CreateFromThisModel = 0,
        CopyFromOtherAvatar = 1
    }

    [Serializable]
    public class AutoFBXConfig
    {
        public string AnimationName;
        public ModelImporterAnimationType AnimationType;
        public AvatarDefinitionEnum AvatarDefinition;
        public bool RotationBakeIntoPose;
        public bool RotationBasedUpon;
        public bool PositionYBakeIntoPose;
        public bool PositionYBasedUpon;
        public bool PositionXZBakeIntoPose;
        public bool PositionXZBasedUpon;
        public string AvatarPath;
        public string AniLoopStr;

        public AutoFBXConfig()
        {

        }
    }

    static string AvatarGuid;
    static Avatar SourceAvatar;
    static AutoFBXConfig config;

    string[] AniLoop;

    [MenuItem("Assets/模型批量修改导入器属性")]
    static void Init()
    {
        config = new AutoFBXConfig();
        AutoFBXSettingEditor window = EditorWindow.GetWindow<AutoFBXSettingEditor>();
        window.minSize = new Vector2(300f, 320f);
        window.Show();
    }

    void OnGUI()
    {
        if (config == null)
            EditorWindow.GetWindow<AutoFBXSettingEditor>().Close();

        EditorGUILayout.LabelField("FBX模型批量导入设置");
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("加载第一人称配置"))
        {
            LoadCommon();
            SourceAvatar = AssetDatabase.LoadAssetAtPath("Assets/Resources/Roles/Fbx/arm.FBX", typeof(Avatar)) as Avatar;
        }

        if (GUILayout.Button("加载亚洲女配置"))
        {
            LoadCommon();
            SourceAvatar = AssetDatabase.LoadAssetAtPath("Assets/Resources/Roles/Fbx/yazhounv1.FBX", typeof(Avatar)) as Avatar;
        }

        if (GUILayout.Button("加载亚洲男配置"))
        {
            LoadCommon();
            SourceAvatar = AssetDatabase.LoadAssetAtPath("Assets/Resources/Roles/Fbx/yazhounan1.FBX", typeof(Avatar)) as Avatar;
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("加载生化人配置"))
        {
            LoadCommon();
            SourceAvatar = AssetDatabase.LoadAssetAtPath("Assets/Resources/Roles/Fbx/shenghuayouling.FBX", typeof(Avatar)) as Avatar;
        }

        if (GUILayout.Button("加载空配置"))
        {
            LoadEmpty();
        }
        EditorGUILayout.EndHorizontal();

        config.AnimationType = (ModelImporterAnimationType)EditorGUILayout.EnumPopup("AnimationType", config.AnimationType);

        if (config.AnimationType == ModelImporterAnimationType.Generic || config.AnimationType == ModelImporterAnimationType.Human)
        {
            config.AvatarDefinition = (AvatarDefinitionEnum)EditorGUILayout.EnumPopup("AvatarDefinition", config.AvatarDefinition);
            if (config.AvatarDefinition == AvatarDefinitionEnum.CopyFromOtherAvatar)
            {
                SourceAvatar = EditorGUILayout.ObjectField("Source", SourceAvatar, typeof(Avatar), false) as Avatar;
            }
            else
            {
                SourceAvatar = null;
            }
        }
        else
        {
            SourceAvatar = null;
        }

        config.RotationBakeIntoPose = EditorGUILayout.Toggle("RotationBakeIntoPose", config.RotationBakeIntoPose);
        config.RotationBasedUpon = EditorGUILayout.Toggle("RotationBasedUpon", config.RotationBasedUpon);
        config.PositionYBakeIntoPose = EditorGUILayout.Toggle("PositionYBakeIntoPose", config.PositionYBakeIntoPose);
        config.PositionYBasedUpon = EditorGUILayout.Toggle("PositionYBasedUpon", config.PositionYBasedUpon);
        config.PositionXZBakeIntoPose = EditorGUILayout.Toggle("PositionXZBakeIntoPose", config.PositionXZBakeIntoPose);
        config.PositionXZBasedUpon = EditorGUILayout.Toggle("PositionXZBasedUpon", config.PositionXZBasedUpon);
        EditorGUILayout.LabelField("设置为循环播放的名称过滤器（以,分割，大小写不敏感）");
        config.AniLoopStr = EditorGUILayout.TextField(config.AniLoopStr);

        EditorGUILayout.Space();

        if (GUILayout.Button("导入配置"))
        {
            LoadConfig();
        }

        if (GUILayout.Button("导出配置"))
        {
            SaveConfig();
        }

        if (GUILayout.Button("提交修改"))
        {
            Apply();
        }
    }

    /// <summary>
    /// 初始化公用配置
    /// </summary>
    void LoadCommon()
    {
        config.AnimationType = ModelImporterAnimationType.Generic;
        config.AvatarDefinition = AvatarDefinitionEnum.CopyFromOtherAvatar;
        config.RotationBakeIntoPose = true;
        config.RotationBasedUpon = true;
        config.PositionYBakeIntoPose = true;
        config.PositionYBasedUpon = true;
        config.PositionXZBakeIntoPose = true;
        config.PositionXZBasedUpon = false;
        config.AniLoopStr = "walk,stand,idle,fire2";
    }

    void LoadEmpty()
    {
        config.AnimationType = ModelImporterAnimationType.None;
        config.AvatarDefinition = AvatarDefinitionEnum.CreateFromThisModel;
        config.RotationBakeIntoPose = false;
        config.RotationBasedUpon = false;
        config.PositionYBakeIntoPose = false;
        config.PositionYBasedUpon = false;
        config.PositionXZBakeIntoPose = false;
        config.PositionXZBasedUpon = false;
        config.AniLoopStr = "";
        SourceAvatar = null;
    }

    /// <summary>
    /// 保存配置（现阶段只是将对象序列化保存）
    /// </summary>
    void SaveConfig()
    {
        string saveFile = EditorUtility.SaveFilePanel("save", ".", "未命名", "conf");
        if (!string.IsNullOrEmpty(saveFile))
        {
            config.AvatarPath = AssetDatabase.GetAssetPath(SourceAvatar);
            FileStream fs = new FileStream(saveFile, FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, config);
            fs.Close();
            ShowNotification(new GUIContent("导出成功"));
        }
    }

    /// <summary>
    /// 读取配置（现阶段只是将对象反序列化到对象上）
    /// </summary>
    void LoadConfig()
    {
        string openFile = EditorUtility.OpenFilePanel("open", ".", "conf");
        if (!string.IsNullOrEmpty(openFile))
        {
            OpenConfig(openFile);
            ShowNotification(new GUIContent("导入成功"));
        }
    }

    void OpenConfig(string openFile)
    {
        if (File.Exists(openFile))
        {
            FileStream fs = new FileStream(openFile, FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();
            config = formatter.Deserialize(fs) as AutoFBXConfig;
            SourceAvatar = AssetDatabase.LoadAssetAtPath(config.AvatarPath, typeof(Avatar)) as Avatar;
        }
    }

    /// <summary>
    /// 提交修改
    /// </summary>
    void Apply()
    {
        if (Selection.activeObject == null || Selection.objects.Length == 0)
        {
            EditorUtility.DisplayDialog("提示", "未选中任何东西", "确定");
            return;
        }

        AvatarGuid = string.Empty;

        if (config.AvatarDefinition == AvatarDefinitionEnum.CopyFromOtherAvatar)
        {
            if (SourceAvatar != null)
            {
                AvatarGuid = GetAvatarGuidFromMetaFile(AssetDatabase.GetAssetPath(SourceAvatar) + ".meta");
            }
        }

        AniLoop = null;
        if (!string.IsNullOrEmpty(config.AniLoopStr))
            AniLoop = config.AniLoopStr.ToLower().Split(',');

        UnityEngine.Object[] arrOJ = Selection.objects;
        foreach (var obj in arrOJ)
        {
            if (obj.GetType() == typeof(UnityEngine.GameObject))
            {
                AutoFBXsetting(AssetDatabase.GetAssetPath(obj));
            }
            else
            {
                DirectoryInfo kDI = new DirectoryInfo(AssetDatabase.GetAssetPath(obj));
                FileInfo[] arrFI = kDI.GetFiles("*.FBX", SearchOption.AllDirectories);
                foreach (FileInfo fi in arrFI)
                {
                    string file = fi.FullName.Substring(fi.FullName.IndexOf("Assets"));
                    AutoFBXsetting(file);
                }
            }
        }

        ShowNotification(new GUIContent("设置成功"));
    }

    /// <summary>
    /// 修改Meta文件设置AvatarDefinition和Source
    /// </summary>
    void AutoFBXsetting(string asset)
    {
        string assetMeta = asset + ".meta";
        //把meta文件的animation部分导入到模型的meta文件中，如果已经存在animation，则不添加
        List<string> dest = new List<string>();
        int index = -1;
        string[] fbx = File.ReadAllLines(assetMeta);
        bool next = false;
        bool isClip = false;
        for (int k = 0; k < fbx.Length; ++k)
        {
            if (next)
            {
                next = false;
                continue;
            }
            string f = fbx[k];

            if (f.Contains("path:") && !f.Contains("Bip_Hips"))
            {
                int i = f.IndexOf(":");
                if (f.Substring(i + 1).Length > 3)
                {
                    next = true;
                    continue;
                }
            }

            index++;
            dest.Add(f);

            if (f.Contains("clipAnimations: []"))
            {
                isClip = true;
                int i = f.IndexOf(":");
                dest[index] = dest[index].Substring(0, i + 2);
            }
            else if (f.Contains("clipAnimations:"))
            {
                for (k = k + 1; k < fbx.Length; ++k)
                {
                    string a = fbx[k];
                    if (a.Contains("maskSource"))
                    {
                        k++;
                        break;
                    }
                }
                isClip = true;
            }

            if (isClip && SourceAvatar != null)
            {
                string[] animation = File.ReadAllLines(AssetDatabase.GetAssetPath(SourceAvatar) + ".meta");
                bool flag = false;
                next = false;
                foreach (string a in animation)
                {
                    if (next)
                    {
                        next = false;
                        continue;
                    }
                    if (flag)
                    {
                        index++;
                        dest.Add(a);
                    }
                    if (a.Contains("path:") && !a.Contains("Bip_Hips"))
                    {
                        int i = a.IndexOf(":");
                        if (a.Substring(i + 1).Length > 3)
                        {
                            next = true;
                        }
                    }
                    else if (a.Contains("clipAnimations"))
                        flag = true;
                    else if (a.Contains("maskSource"))
                        break;
                }
                isClip = false;
            }
        }

        // 获取模型的一些参数信息
        TakeInfo[] arrTI = null;
        ModelImporter kMI = ModelImporter.GetAtPath(asset) as ModelImporter;
        Type type = typeof(ModelImporter);
        PropertyInfo[] arrPI = type.GetProperties(~System.Reflection.BindingFlags.Public);
        PropertyInfo pi = null;
        foreach (PropertyInfo PI in arrPI)
        {
            if (PI.Name == "importedTakeInfos")
                pi = PI;
        } 
        if (pi != null)
        {
            MethodInfo mi = pi.GetGetMethod(true);
            if (mi != null)
            {
                arrTI = mi.Invoke(kMI, null) as TakeInfo[];
            }
        }

        if (arrTI == null)
        {
            EditorUtility.DisplayDialog("error", "arrTI is null", "ok");
            return;
        }

        // 修改模型参数信息
        int j = -1;
        foreach (string str in dest)
        {
            j++;
            if (j > index)
                break;

            if (str.Contains("name") && arrTI != null && arrTI.Length != 0)
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].defaultClipName;
                continue;
            }

            if (str.Contains("takeName") && arrTI != null && arrTI.Length != 0)
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].name;
                continue;
            }
            if (str.Contains("firstFrame") && arrTI != null && arrTI.Length != 0)
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (arrTI[0].startTime < 0 ? 0 : arrTI[0].startTime) * arrTI[0].sampleRate;
                continue;
            }
            if (str.Contains("lastFrame") && arrTI != null && arrTI.Length != 0)
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].stopTime * arrTI[0].sampleRate;
                continue;
            }
            // 获取avatar的guid

            if (str.Contains("lastHumanDescriptionAvatarSource"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                if (SourceAvatar != null)
                {
                    dest[j] += "{fileID: 9000000, guid: " + AvatarGuid + ", type: 3}";
                }
                else
                {
                    dest[j] += "{instanceID: 0}";
                }
                continue;
            }

            if (str.Contains("copyAvatar"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)config.AvatarDefinition;
                continue;
            }
            if (str.Contains("animationType"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)config.AnimationType;
                continue;
            }
            if (str.Contains("importMaterials"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += "0";
                continue;
            }
            if (str.Contains("materialName"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += "0";
                continue;
            }
            if (str.Contains("materialSearch"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += "1";
                continue;
            }

            // 判断是否循环

            if (str.Contains("loopTime"))
            {
                bool jud = false;
                string lowName = asset.ToLower();
                if (AniLoop != null)
                {
                    foreach (var s in AniLoop)
                    {
                        if (lowName.Contains(s))
                        {
                            jud = true;
                            break;
                        }
                    }
                }
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                if (jud)
                    dest[j] += 1;
                else
                    dest[j] += 0;
                continue;
            }
            if (str.Contains("loopBlend:"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += 0;
                continue;
            }
            if (str.Contains("loopBlendOrientation"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.RotationBakeIntoPose ? 1 : 0;
                continue;
            }
            if (str.Contains("loopBlendPositionY"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.PositionYBakeIntoPose ? 1 : 0;
                continue;
            }
            if (str.Contains("loopBlendPositionXZ"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.PositionXZBakeIntoPose ? 1 : 0;
                continue;
            }
            if (str.Contains("keepOriginalOrientation"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.RotationBasedUpon ? 1 : 0;
                continue;
            }
            if (str.Contains("keepOriginalPositionY"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.PositionYBasedUpon ? 1 : 0;
                continue;
            }
            if (str.Contains("keepOriginalPositionXZ"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += config.PositionXZBasedUpon ? 1 : 0;
                continue;
            }
            if (str.Contains("type: 3}") && !str.Contains("lastHumanDescriptionAvatarSource"))
            {
                dest[j] = "";
            }
        }
        File.WriteAllLines(new DirectoryInfo("./").FullName + "/" + assetMeta, dest.ToArray());// 修改完成后写入目标模型meta文件中
        AssetDatabase.Refresh();
        dest.Clear();
    }

    /// <summary>
    /// 根据Meta文件获取Avatar的guid
    /// </summary>
    /// <param name="path">meta文件路径</param>
    /// <returns>返回guid</returns>
    static string GetAvatarGuidFromMetaFile(string path)
    {
        string[] file = File.ReadAllLines(path);
        string strReturn = "";
        foreach (string str in file)
        {
            if (str.Contains("guid"))
            {
                int i1 = str.IndexOf(":");
                int i2 = str.IndexOf(":", i1 + 1);
                int i3 = str.IndexOf(":", i2 + 1);
                strReturn = str.Substring(i3 + 2);
                break;
            }
        }
        return strReturn;
    }
}
