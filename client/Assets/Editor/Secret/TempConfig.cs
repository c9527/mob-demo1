﻿using UnityEngine;
using System.Collections;
using System.IO;
using Object = UnityEngine.Object;
using System.Text;
using UnityEditor;
using System;

public class TempConfig
{

    public static Object[] GetTempCfgs()
    {
        Object[] result = null;
        string path = Application.dataPath + "//Resources//Config";
        string tempPath = Application.dataPath + "//Resources//TempConfig";
        if (!Directory.Exists(tempPath))
            Directory.CreateDirectory(tempPath);
        DirectoryInfo dir = new DirectoryInfo(path);
        FileInfo[] files = dir.GetFiles();
        DirectoryInfo dirTemp = new DirectoryInfo(tempPath);
        FileInfo[] delList = dirTemp.GetFiles();
        for (int i = 0; i < delList.Length; i++)
        {
            delList[i].Delete();
        }

        for (int i = 0; i < files.Length; i++)
        {
            if (files[i].Extension == ".meta")
                continue;
            float progress = (i + 1) * 1f / files.Length;
            EditorUtility.DisplayProgressBar("配置加密", files[i].Name, progress);
            StreamReader sr = files[i].OpenText();
            string content = sr.ReadToEnd();
            sr.Dispose();
            sr = null;
            string encryptyStr = AesSecret.EncryptString(content);
            string copyPath = tempPath + "//" + files[i].Name;
            FileStream fs = File.Create(copyPath);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
            sw.Write(encryptyStr);
            sw.Dispose();
            fs.Dispose();
            fs = null;
            AssetDatabase.WriteImportSettingsIfDirty(copyPath);
        }
        AssetDatabase.Refresh();
        EditorUtility.ClearProgressBar();
        result = Resources.LoadAll("TempConfig/");
        return result;
    }



    public static void Clear()
    {
        string tempPath = Application.dataPath + "//Resources//TempConfig";
        DirectoryInfo dir = new DirectoryInfo(tempPath);
        var files = dir.GetFiles();
        for (int i = 0; i < files.Length; i++)
        {
            files[i].Delete();
        }
    }

}
