﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Diagnostics;
using System.Threading;


public class Setting
{
    public static void UpdateConfig()
    {
        // Weapon
        string fileName = new DirectoryInfo("../tool/conf_export/CfgExportor.exe").FullName;
        string configPath = new DirectoryInfo("./Assets/Resources/Config").FullName + "/Config";
        string luaPath = " "+new DirectoryInfo("../server/cfg/").FullName + " ";
        string filePath = new DirectoryInfo("../config/").FullName + "ItemWeapon.xls";
        var p = new Process();
        PluginUtil.Excute(p, fileName, configPath + luaPath + filePath);
        Thread.Sleep(1000);
        p.Kill();
        string str = File.ReadAllText(configPath+"ItemWeapon.csv");
        ConfigItemWeapon cw = ConfigManager.GetConfig<ConfigItemWeapon>();
        cw.Load(str);
        GameDispatcher.Dispatch(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE);

        // GameSetting
        p = new Process();
        filePath = new DirectoryInfo("../config/").FullName + "GameSetting.xls";
        PluginUtil.Excute(p, fileName, configPath + luaPath + filePath);
        Thread.Sleep(1000);
        p.Kill();
        str = File.ReadAllText(configPath + "GameSetting.csv");
        ConfigGameSetting cg = ConfigManager.GetConfig<ConfigGameSetting>();
        cg.Load(str);
        GlobalConfig.gameSetting = cg.m_dataArr[0];
    }
    public static void CameraSetting()
    {
        GameObject sceneCamera = GameObject.Find("Slot_SceneCamera");
        GameObject mainPlayerCamera = GameObject.Find("Slot_MainPlayerCamera");
        ConfigGameSetting cgs = ConfigManager.GetConfig<ConfigGameSetting>();
        cgs.m_dataArr[0].MainPlaerCameraRot = mainPlayerCamera.transform.localEulerAngles;
        cgs.m_dataArr[0].MainPlayerCameraPos = mainPlayerCamera.transform.localPosition;
        cgs.m_dataArr[0].SceneCameraPos = sceneCamera.transform.localPosition;
        cgs.m_dataArr[0].SceneCameraRot = sceneCamera.transform.localEulerAngles;
    }
}
