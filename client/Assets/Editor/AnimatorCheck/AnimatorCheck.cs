﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
/// <summary>
/// 检查武器动作配置表配置的动作是否错误
/// </summary>
public class AnimatorCheck  {
    static Regex regex = new Regex(@"guid: ([\w]*)");
    static Regex regexMP = new Regex("/arm@([\\w]*).FBX");
    static Regex regexGirl = new Regex("/girl@([\\w]*).FBX");
    static Regex regexPlayer = new Regex("/player@([\\w]*).FBX");
    static Regex regexAll= new Regex(@"m_Name: ([\w]*)\n  m_Speed: [0-9]*\n  m_CycleOffset: [0-9]*");
    static ConfigItemWeapon config;
    static bool isIgnoreDefault = false;
    static List<string> ignoreStrs = new List<string>();
	[MenuItem("Plugins/动作配置检查/包括默认名")]
    public static void DoCheck()
    {
        if(config!=null)
        {
            GameObject.DestroyImmediate(config);
        }
        isIgnoreDefault = false;
        ignoreStrs.Clear();       
        config = ScriptableObject.CreateInstance<ConfigItemWeapon>();
        config.Load(File.ReadAllText("Assets/Resources/Config/ConfigItemWeapon.csv"));                
        CheckMP();
        CheckGirl();
        CheckPlayer();
        GameObject.DestroyImmediate(config);
    }

    [MenuItem("Plugins/动作配置检查/无视默认名")]
    public static void DoCheckIgnore()
    {
        if (config != null)
        {
            GameObject.DestroyImmediate(config);
        }
        isIgnoreDefault = true;
        ignoreStrs.Clear();
        ignoreStrs.Add("fire1");
        ignoreStrs.Add("fire3");
        ignoreStrs.Add("fire4");
        ignoreStrs.Add("fire2");
        ignoreStrs.Add("idle");
        config = ScriptableObject.CreateInstance<ConfigItemWeapon>();
        config.Load(File.ReadAllText("Assets/Resources/Config/ConfigItemWeapon.csv"));
        CheckMP();
        CheckGirl();
        CheckPlayer();
        GameObject.DestroyImmediate(config);
    }

    /// <summary>
    /// 获取主角动作
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private static List<string> GetActorName(string path,Regex regex2)
    {
        regex2 = new Regex(@"m_Name: ([^ \r\n]*)\n  m_Speed: [0-9]*\n  m_CycleOffset: [0-9]*");
        //regex2 = new Regex(@"!u!((?!!u!).)+?m_Name:)",RegexOptions.Singleline);
        List<string> list = new List<string>();
        FileInfo info = new FileInfo(path);
        StreamReader sr = info.OpenText();
        string content = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();
        Match match = regex2.Match(content);
        while(match.Success)
        {
            list.Add(match.Groups[1].Value);
            match = match.NextMatch();
        }
        //Match match = regex.Match(content);
        //Match match2=null;
        //while(match.Success)
        //{
        //    //string filePath = AssetDatabase.GUIDToAssetPath(match.Groups[1].Value);
        //    match2 = regex2.Match(filePath);
        //    if(match2.Success)
        //    {
        //        list.Add(match2.Groups[1].Value);
        //    }
        //    match = match.NextMatch();
        //}
        list.Sort(
            (a, b) => 
            {
                int length = a.Length;
                int length2 = b.Length;
                length=length<length2? length:length2;
                for(int i=0;i<length;i++)
                {
                    if (a[i] < b[i])
                        return -1;
                    else if (a[i] > b[i])
                        return 1;
                    else continue;
                }
                return 0;
            });
        return list;
    }

    /// <summary>
    /// 获取主角动作
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private static List<string> GetGirlActorName(string path)
    {
        List<string> list = new List<string>();
        FileInfo info = new FileInfo(path);
        StreamReader sr = info.OpenText();
        string content = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();
        Match match = regex.Match(content);
        Match match2 = null;
        while (match.Success)
        {
            string filePath = AssetDatabase.GUIDToAssetPath(match.Groups[1].Value);
            match2 = regexMP.Match(filePath);
            if (match2.Success)
            {
                list.Add(match2.Groups[1].Value);
            }
            match = match.NextMatch();
        }
        return list;
    }    

    /// <summary>
    /// 主角动作检查
    /// </summary>
    private static void CheckMP()
    {
        List<string> listMP = GetActorName("Assets/Resources/Roles/AnimatorController/arm_AniCtrl.controller",regexAll);
        string errStr = "";
        foreach (ConfigItemWeaponLine weaponLine in config.m_dataArr)
        {
            if (string.IsNullOrEmpty(weaponLine.DispName))
                continue;
            if (!Contain(listMP, weaponLine.MPIdleAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPIdleAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listMP, weaponLine.MPFireBeginAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPFireBeginAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listMP, weaponLine.MPFireEndAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPFireEndAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listMP, weaponLine.MPFireAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPFireAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listMP, weaponLine.MPReloadAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPReloadAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listMP, weaponLine.MPSwitchAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:MPSwitchAni,名字:{1},对应Animator:arm_AniCtrl", weaponLine.ID, errStr));
            }
        }
    }
    /// <summary>
    /// 其他玩家动作检测
    /// </summary>
    private static void CheckGirl()
    {
        List<string> listGirl = GetActorName("Assets/Resources/Roles/AnimatorController/girl_AniCtrl.controller", regexAll);
        string errStr = "";
        foreach (ConfigItemWeaponLine weaponLine in config.m_dataArr)
        {
            if (weaponLine.DispName.IsNullOrEmpty())
                continue;
            if (!Contain(listGirl, weaponLine.OPSwitchAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPSwitchAni,名字:{1},对应Animator:girl_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listGirl, weaponLine.OPIdleAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPIdleAni,名字:{1},对应Animator:girl_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listGirl, weaponLine.OPFireAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPFireAni,名字:{1},对应Animator:girl_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listGirl, weaponLine.OPReloadAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPReloadAni,名字:{1},对应Animator:girl_AniCtrl", weaponLine.ID, errStr));
            }
        }
    }
    private static void CheckPlayer()
    {
        List<string> listPlayer = GetActorName("Assets/Resources/Roles/AnimatorController/player_AniCtrl.controller", regexAll);
        string errStr = "";
        foreach (ConfigItemWeaponLine weaponLine in config.m_dataArr)
        {
            if (weaponLine.DispName.IsNullOrEmpty())
                continue;
            if (!Contain(listPlayer, weaponLine.OPSwitchAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPSwitchAni,名字:{1},对应Animator:player_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listPlayer, weaponLine.OPIdleAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPIdleAni,名字:{1},对应Animator:player_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listPlayer, weaponLine.OPFireAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPFireAni,名字:{1},对应Animator:player_AniCtrl", weaponLine.ID, errStr));
            }
            if (!Contain(listPlayer, weaponLine.OPReloadAni, ref errStr))
            {
                Debug.LogError(string.Format("配置错误! ID:{0},字段:OPReloadAni,名字:{1},对应Animator:player_AniCtrl", weaponLine.ID, errStr));
            }
        }
    }
    private static bool Contain(List<string> list,string[] strs,ref string errStr)
    {
        bool result = true;
        for (int i = 0; i < strs.Length ; i++)
        {
            try
            {
                float num = float.Parse(strs[i]);
                break;
            }
            catch
            {

            }
            if(isIgnoreDefault&&ignoreStrs.Contains(strs[i]))
            {
                continue;
            }
            if (!list.Contains(strs[i]))
            {
                result = false;
                errStr = strs[i];
                break;
            }
        }       
        return result;
    }
    private static bool Contain(List<string> list, string strs, ref string errStr)
    {
        bool result = true;
        if (isIgnoreDefault && ignoreStrs.Contains(strs))
            return result;
        if (strs.IsNullOrEmpty())
            return true;
            if (!list.Contains(strs))
            {
                result = false;
                errStr = strs;                
            }        
        return result;
    }
}
