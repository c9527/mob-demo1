﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

 class ColliderData //: ScriptableObject
{
    public Vector3 colliderCenter;
    public Vector3 colliderSize;
}


class RoleColliderData //: ScriptableObject
{
    public Dictionary<string, ColliderData> dic;

    public RoleColliderData()
        : base()
    {
        dic = new Dictionary<string, ColliderData>();
    }
}

 
public class RoleColliderCopier : ScriptableObject
{
    public const string TYPE_BOY = "Boy";
    public const string TYPE_GIRL = "Girl";
    public const string TYPE_SH = "ShengHua";
    public const string ASSET_PATH = "Assets/Editor/RoleColliderData.asset";


    private static Dictionary<string, RoleColliderData> dicRoleColliderData = new Dictionary<string, RoleColliderData>();


    //public RoleColliderCopier():base()
    //{
    //    dicRoleColliderData = new Dictionary<string, RoleColliderData>();
    //}

    public void UpdateGOCollider(GameObject go, string type)
    {
        if (dicRoleColliderData.ContainsKey(type) == false)
        {
            EditorUtility.DisplayDialog("错误" + type, "之前没有保存过Collider数据! Type = " + type, "OK");

            return;
        }

        RoleColliderData rcd = dicRoleColliderData[type];

        Transform transform = go.transform;
        for (int i = 0; i < transform.childCount; ++i)
        {
            ColliderData cd = null;
            Transform child = transform.GetChild(i);
            if (rcd.dic.TryGetValue(child.name, out cd))
            {
                BoxCollider bc = GameObjectHelper.GetComponent<BoxCollider>(child.gameObject);
                bc.center = cd.colliderCenter;
                bc.size = cd.colliderSize;
                bc.isTrigger = true;
            }
            UpdateGOCollider(child.gameObject, type);
        }
    }
    

    public void SaveColliderData(GameObject go , string type, bool isChild = false)
    {
        RoleColliderData rcd = null;
        if (dicRoleColliderData.ContainsKey(type) == false)
        {
            //rcd = ScriptableObject.CreateInstance(typeof(RoleColliderData)) as RoleColliderData;
            rcd = new RoleColliderData();
            dicRoleColliderData.Add(type, rcd);
        }
        else
            rcd = dicRoleColliderData[type];
        
        if (!isChild)
            rcd.dic.Clear();

        Transform transform = go.transform;

        for (int i = 0; i < transform.childCount; ++i)
        {
            Transform child = transform.GetChild(i);
            BoxCollider bc = child.GetComponent<BoxCollider>();
            if (bc != null)
            {
                ColliderData cd = null;
                if (rcd.dic.ContainsKey(child.name) == false)
                {
                    cd = new ColliderData();
                    rcd.dic.Add(child.name, cd);
                }

                cd.colliderCenter = bc.center;
                cd.colliderSize = bc.size;
            }
            SaveColliderData(child.gameObject, type, true);
        }
    }

    static public void SaveRoleColliderData()
    {
        if(Selection.gameObjects.Length < 0)
        {
            EditorUtility.DisplayDialog("错误", "请选择已经制作好Collider的人物预设", "OK");
            return;
        }

        GameObject go = Selection.gameObjects[0];
        if (SaveRoleColliderData(go))
        {
            EditorUtility.DisplayDialog("提示", "保存完成", "OK");
        }
        AssetDatabase.SaveAssets();
    }

    static public bool SaveRoleColliderData(GameObject go)
    {
        if (go == null)
        {
            return false;
        }

        string assetPath = AssetDatabase.GetAssetPath(go);
        if (assetPath.Contains("Assets/Resources/Roles/"))
        {
            string name = Path.GetFileName(assetPath);
            RoleColliderCopier rcc = AssetDatabase.LoadAssetAtPath(ASSET_PATH, typeof(RoleColliderCopier)) as RoleColliderCopier;
            if (rcc == null)
            {
                rcc = ScriptableObject.CreateInstance(typeof(RoleColliderCopier)) as RoleColliderCopier;
                AssetDatabase.CreateAsset(rcc, ASSET_PATH);
            }
            rcc.SaveColliderData(go, GetRoleTypeStr(name));
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return true;
        }
        return false;
    }

    static public void UpdateGOCollider()
    {
        if (Selection.gameObjects.Length < 0)
        {
            EditorUtility.DisplayDialog("错误", "请选择要更新Collider的角色预设", "OK");
            return;
        }

        int ret = UpdateGOCollider(Selection.gameObjects);
        if (ret == -1)
        {
            EditorUtility.DisplayDialog("错误", "找不到角色Collider数据", "OK");
        }
        else
        {
            EditorUtility.DisplayDialog("提示", "已完成 " + ret + " 个", "OK");
        }
    }

    public static int UpdateGOCollider(GameObject[] gameObjects)
    {
        var obj = Resources.LoadAssetAtPath(ASSET_PATH, typeof(RoleColliderCopier));
        RoleColliderCopier rcc = obj as RoleColliderCopier;
        if (rcc == null)
        {
            //
            return -1;
        }

        int i = 0;
        foreach (GameObject go in gameObjects)
        {
            string assetPath = AssetDatabase.GetAssetPath(go);
            if (assetPath.Contains("Assets/Resources/Roles/"))
            {
                string name = Path.GetFileName(assetPath);
                rcc.UpdateGOCollider(go, GetRoleTypeStr(name));
                i++;
            }
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        return i;
    }

    public static string GetRoleTypeStr(string name)
    {
        string type = TYPE_SH;
        //if (name.Contains("nan"))
        //{
        //    type = TYPE_BOY;
        //}
        //else if (name.Contains("nv"))
        //{
        //    type = TYPE_GIRL;
        //}

        return type;
    }
}
