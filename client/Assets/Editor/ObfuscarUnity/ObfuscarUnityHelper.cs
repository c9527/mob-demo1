﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class ObfuscarUnityHelper
{
    static bool sDoneOnPostProcessScene;

    [PostProcessScene]
    public static void OnPostProcessScene()
    {
        if (sDoneOnPostProcessScene == true)
        {
            Debug.Log("[OnPostProcessScene] sDoneOnPostProcessScene = true");
            return;
        }

        if (EditorApplication.isPlayingOrWillChangePlaymode)
        {
            Debug.Log("[OnPostProcessScene] EditorApplication.isPlayingOrWillChangePlaymode");
            return;
        }

        if (BuildPipeline.isBuildingPlayer == false)
        {
            Debug.Log("[OnPostProcessScene] EditorApplication.isPlayingOrWillChangePlaymode");
            return;
        }

        if (AutoBuild.m_lastBuildParams == null || AutoBuild.m_lastBuildParams.buildTarget != BuildTarget.WebPlayer || AutoBuild.m_lastBuildParams.isObfuscar == false)
        {
//            Debug.Log("[OnPostProcessScene] AutoBuild.m_lastBuildParams  == null or AutoBuild.m_lastBuildParams.isObfuscar == false");
            return;
        }

        Debug.Log("[OnPostProcessScene] web版混淆有问题暂时不混淆");
        return;

        Debug.Log("[OnPostProcessScene] OK, do obfuscate");
        sDoneOnPostProcessScene = true;
        ObfuscateScriptAssemblies();
    }

    public static void ObfuscateScriptAssemblies()
    {
        string libraryPath = Path.Combine(Path.GetDirectoryName(Application.dataPath), "Library");
        string scriptAssembliesPath = Path.Combine(libraryPath, "ScriptAssemblies");
        string scriptAssembliesDll = Path.Combine(scriptAssembliesPath, "Assembly-CSharp.dll");

        string scriptAssembliesFirstpassDll = Path.Combine(scriptAssembliesPath, "Assembly-CSharp-firstpass.dll");

        //string autoBuildPath = "AutoBuild/";
        //string ObfuscarPath = "AutoBuild/obfuscar/";
        //string obfuscarInputPath = "AutoBuild/obfuscar/Input/";
        //string obfuscarOutputPath = "AutoBuild/obfuscar/Input/Output";
        string obfuscarInputDll = "AutoBuild/obfuscar/Input/Client.dll.bytes";
        string obfuscarInputFirstpassDll = "AutoBuild/obfuscar/Input/Assembly-CSharp-firstpass.dll";
        string obfuscarOutputDll = "AutoBuild/obfuscar/Output/Client.dll.bytes";

        File.Copy(scriptAssembliesDll, obfuscarInputDll, true);
        File.Copy(scriptAssembliesFirstpassDll, obfuscarInputFirstpassDll, true);

        string[] dllPath = new string[] { scriptAssembliesDll };
        string[] dllName = new string[] { "Client.dll.bytes" };

        BuildAB.ObfuscateDll(dllPath, dllName, (AutoBuild.m_lastBuildParams != null ? AutoBuild.m_lastBuildParams.version : PlayerSettings.bundleVersion));

        File.Copy(obfuscarOutputDll, scriptAssembliesDll, true);

        Debug.Log("ObfuscateScriptAssemblies Done");
    }

    [PostProcessBuild]
    static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
    {
        sDoneOnPostProcessScene = false;
        Debug.Log("[OnPostProcessBuild] Set sDoneOnPostProcessScene = false");
    }
}
