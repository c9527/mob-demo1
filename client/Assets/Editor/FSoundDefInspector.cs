﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FSoundDef))]
public class FSoundDefInspector : Editor
{
    private static bool m_inited;

    public override void OnInspectorGUI()
    {
        var sound = target as FSoundDef;
        if (!m_inited && Application.isPlaying)
        {
            m_inited = true;
            if (!string.IsNullOrEmpty(sound.Name))
                LoadFSoundDef(sound);
            if (!string.IsNullOrEmpty(sound.AudioSourceTemplate))
                LoadAdudioSource(sound);
        }

        if (PrefabUtility.GetPrefabType(sound.gameObject) == PrefabType.None)
        {
            sound.AudioSourceTemplate = EditorGUILayout.TextField("AudioSource模板", sound.AudioSourceTemplate);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("保存AudioSource"))
            {
                if (!string.IsNullOrEmpty(sound.AudioSourceTemplate))
                    SaveAudioSource(sound);
                else
                    Debug.LogError("AudioSourceName不可为空");
            }
            if (GUILayout.Button("加载AudioSource"))
            {
                if (!string.IsNullOrEmpty(sound.AudioSourceTemplate))
                    LoadAdudioSource(sound);
                else
                    Debug.LogError("AudioSourceName不可为空");
            }
            GUILayout.EndHorizontal();
        }
        
        EditorGUILayout.Separator();
        DrawDefaultInspector();
        EditorGUILayout.Separator();

        if (PrefabUtility.GetPrefabType(sound.gameObject) == PrefabType.None)
        {
            sound.Name = EditorGUILayout.TextField("FSoundDef模板", sound.Name);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("保存FSoundDef"))
            {
                if (!string.IsNullOrEmpty(sound.Name))
                    SaveFSoundDef(sound);
                else
                    Debug.LogError("Name不可为空");
            }
            if (GUILayout.Button("加载FSoundDef"))
            {
                if (!string.IsNullOrEmpty(sound.Name))
                    LoadFSoundDef(sound);
                else
                    Debug.LogError("Name不可为空");
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("播放"))
        {
            sound.Sounds = new List<FSound>();
            for (int i = 0; i < sound.clips.Length; i++)
            {
                sound.Sounds.Add(new FSound() { clip = sound.clips[i], Type = sound.clips[i] == null ? FSoundType.DontPlay : FSoundType.Sound});
            }
            sound.Play();
        }
        if (GUILayout.Button("停止"))
        {
            sound.Stop();
        }
        GUILayout.EndHorizontal();
    }

    private static void LoadFSoundDef(FSoundDef sound)
    {
        var source = AssetDatabase.LoadAssetAtPath("Assets/Resources/FSoundDef/" + sound.Name + ".prefab",
            typeof (FSoundDef));
        if (source)
        {
            var dest = sound.gameObject.GetComponent<FSoundDef>();
            EditorUtility.CopySerialized(source, dest);
        }
        else
            Debug.LogError(sound.Name + "不存在");
    }

    private static void SaveFSoundDef(FSoundDef sound)
    {
        var go = new GameObject();
        var dest = go.AddComponent<FSoundDef>();
        var source = sound.gameObject.GetComponent<FSoundDef>();
        EditorUtility.CopySerialized(source, dest);
        PrefabUtility.CreatePrefab("Assets/Resources/FSoundDef/" + sound.Name + ".prefab", go);
        DestroyImmediate(go);

        AssetDatabase.SaveAssets();
    }

    private static void SaveAudioSource(FSoundDef sound)
    {
        var go = new GameObject();
        var dest = go.AddComponent<AudioSource>();
        var source = sound.gameObject.GetComponent<AudioSource>();
        EditorUtility.CopySerialized(source, dest);
        PrefabUtility.CreatePrefab("Assets/Resources/AudioSource/" + sound.AudioSourceTemplate + ".prefab", go);
        Destroy(go);

        AssetDatabase.SaveAssets();
    }

    private void LoadAdudioSource(FSoundDef sound)
    {
        var source = AssetDatabase.LoadAssetAtPath("Assets/Resources/AudioSource/" + sound.AudioSourceTemplate + ".prefab",
            typeof (AudioSource));
        if (source)
        {
            var dest = sound.gameObject.GetComponent<AudioSource>();
            EditorUtility.CopySerialized(source, dest);

            //刷新曲线
            var type = Assembly.Load(new AssemblyName("UnityEditor")).GetType("UnityEditor.AudioSourceInspector");
            var arr = Resources.FindObjectsOfTypeAll(type);
            var func = type.GetMethod("UpdateWrappersAndLegend", BindingFlags.Instance | BindingFlags.NonPublic);
            EditorApplication.delayCall += () =>
            {
                func.Invoke(arr[0], new object[] {true});
                Repaint();
            };
        }
        else
            Debug.LogError(sound.AudioSourceTemplate + "不存在");
    }
}