﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ComponentAce.Compression.Libs.zlib;

static class CDataHelper
{
    ////// CRC32
    // see: http://www.w3.org/TR/PNG-CRCAppendix.html
    //
    static uint[] crcTable = null;
    static uint[] getCrcTable()
    {
        if (crcTable == null)
        {
            crcTable = new uint[0x100];
            for (int n = 0; n < crcTable.Length; n++)
            {
                uint c = (uint)n;
                for (int k = 0; k < 8; k++)
                {
                    if ((c & 1) != 0)
                        c = 0xedb88320 ^ (c >> 1);
                    else
                        c >>= 1;
                }
                crcTable[n] = c;
            }
        }
        return crcTable;
    }
    static uint updateCrc(uint crc, Stream stm, int len)
    {
        uint[] ct = getCrcTable();
        for (int n = 0; n < len; n++)
            crc = ct[(crc ^ stm.ReadByte()) & 0xff] ^ (crc >> 8);
        return crc;
    }
    public static uint CRC32(Stream stm, int len)
    {
        return updateCrc(0xffffffff, stm, len) ^ 0xffffffff;
    }
    public static uint CRC32(byte[] buf)
    {
        return CRC32(new MemoryStream(buf), buf.Length);
    }

    ////// Adler32
    // see: http://en.wikipedia.org/wiki/Adler-32
    //
    public static uint Adler32(Stream stm, int len)
    {
        uint s1 = 1, s2 = 0;
        for (int n = 0; n < len; n++)
        {
            s1 = (s1 + (uint)stm.ReadByte()) % 65521;
            s2 = (s2 + s1) % 65521;
        }
        return (s2 << 16) + s1;
    }
    public static uint Adler32(byte[] buf, int offset, int length)
    {
        using (MemoryStream stm = new MemoryStream(buf))
        {
            stm.Position = offset;
            return Adler32(stm, length);
        }
    }

    public static byte[] ZLibDecompress(byte[] srcDat)
    {
        using (MemoryStream stmOut = new MemoryStream())
        {
            using (ZOutputStream zip = new ZOutputStream(stmOut))
                zip.Write(srcDat, 0, srcDat.Length);
            return stmOut.ToArray();
        }
    }

    public static byte[] ZLibCompress(byte[] srcDat)
    {
        using (MemoryStream stmOut = new MemoryStream())
        {
            using (ZOutputStream zip = new ZOutputStream(stmOut, zlibConst.Z_BEST_COMPRESSION))
                zip.Write(srcDat, 0, srcDat.Length);
            return stmOut.ToArray();
        }
    }

    ////// Big-Endian
    public static ushort BE_ReadUInt16(Stream stm)
    {
        int b1 = stm.ReadByte();
        int b2 = stm.ReadByte();
        return (ushort)((b1 << 8) + b2);
    }
    public static uint BE_ReadUInt32(Stream stm)
    {
        byte[] buf = new byte[4];
        stm.Read(buf, 0, buf.Length);
        return BitConverter.ToUInt32(buf.Reverse().ToArray(), 0);
    }
    public static ulong BE_ReadUInt64(Stream stm)
    {
        byte[] buf = new byte[8];
        stm.Read(buf, 0, buf.Length);
        return BitConverter.ToUInt64(buf.Reverse().ToArray(), 0);
    }
    public static float BE_ReadSingle(Stream stm)
    {
        byte[] buf = new byte[4];
        stm.Read(buf, 0, buf.Length);
        return BitConverter.ToSingle(buf.Reverse().ToArray(), 0);
    }
    public static double BE_ReadDouble(Stream stm)
    {
        byte[] buf = new byte[8];
        stm.Read(buf, 0, buf.Length);
        return BitConverter.ToDouble(buf.Reverse().ToArray(), 0);
    }
    public static void BE_WriteUInt16(Stream stm, ushort value)
    {
        stm.WriteByte((byte)(value >> 8));
        stm.WriteByte((byte)value);
    }
    public static void BE_WriteUInt32(Stream stm, uint value)
    {
        byte[] buf = BitConverter.GetBytes(value).Reverse().ToArray();
        stm.Write(buf, 0, buf.Length);
    }
    public static void BE_WriteDouble(Stream stm, double value)
    {
        byte[] buf = BitConverter.GetBytes(value).Reverse().ToArray();
        stm.Write(buf, 0, buf.Length);
    }
    public static string BE_ReadShortStr(Stream stm)
    {
        int len = BE_ReadUInt16(stm);
        return ReadUtfStr(stm, len);
    }
    public static string BE_ReadLongStr(Stream stm)
    {
        int len = (int)BE_ReadUInt32(stm);
        Trace.Assert(len >= 0);
        return ReadUtfStr(stm, len);
    }
    public static void BE_WriteShortStr(Stream stm, string str)
    {
        byte[] buf = GetUTF8Buf(str);
        Trace.Assert(buf.Length >= 0 && buf.Length < 0x10000);
        BE_WriteUInt16(stm, (ushort)buf.Length);
        stm.Write(buf, 0, buf.Length);
    }
    public static void BE_WriteLongStr(Stream stm, string str)
    {
        byte[] buf = GetUTF8Buf(str);
        Trace.Assert(buf.Length >= 0);
        BE_WriteUInt32(stm, (uint)buf.Length);
        stm.Write(buf, 0, buf.Length);
    }

    public static string ReadUtfStr(Stream stm, int len)
    {
        byte[] buf = new byte[len];
        stm.Read(buf, 0, len);
        string str = GetUTF8Str(buf);
        return str;
    }

    public static string FormatValue(object obj, string space = null)
    {
        if (obj == null)
            return "null";

        if (obj is string)
            return string.Format("\"{0}\"", obj.ToString().Replace("\"", "\\\""));

        string nextSpace = space == null ? null : space + "  ";

        if (obj is IDictionary)
        {
            List<string> list = new List<string>();
            foreach (DictionaryEntry de in obj as IDictionary)
                list.Add(string.Format("{0}={1}", de.Key == null ? "null" : de.Key.ToString(), FormatValue(de.Value, nextSpace)));
            if (list.Count <= 0)
                return "{}";
            if (space == null)
                return string.Format("{{{0}}}", string.Join(",", list.ToArray()));
            return string.Format("{{\r\n{0}  {1}\r\n{0}}}", space, string.Join("\r\n" + nextSpace, list.ToArray()));
        }

        if (obj is IEnumerable)
        {
            List<string> list = new List<string>();
            foreach (object o in obj as IEnumerable)
                list.Add(FormatValue(o, nextSpace));
            return string.Format("[{0}]", string.Join(space == null ? "," : ", ", list.ToArray()));
        }

        return obj.ToString();
    }

    public static string GetUTF8Str(byte[] buf)
    {
        return Encoding.UTF8.GetString(buf);
    }

    public static byte[] GetUTF8Buf(string str)
    {
        return Encoding.UTF8.GetBytes(str);
    }

    public static string Md5(string str)
    {
        MD5CryptoServiceProvider md5csp = new MD5CryptoServiceProvider();
        byte[] buf = md5csp.ComputeHash(GetUTF8Buf(str));
        string s = "";
        foreach (byte b in buf)
            s += b.ToString("X2");
        return s;
    }

    public static void CopyTo(this Stream stm, Stream outStm)
    {
        while (true)
        {
            int b = stm.ReadByte();
            if (b < 0)
                break;
            outStm.WriteByte((byte)b);
        }
    }

    public static void Assert(bool con, string msg = "")
    {
        if (!con)
            throw new Exception("assert failed " + msg);
    }
}
