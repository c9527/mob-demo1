﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class AnimationCompressWindow : EditorWindow {
    private float m_rotationError=0.5f;
    private float m_positionError = 0.5f;
    private float m_scaleError = 0.5f;
    private string[] m_paths;
    private string subStr;
    void Awake()
    {        
        subStr = Application.dataPath.Replace("Assets", "").Replace("/","\\");               
    }

    [MenuItem("Assets/ 【动画压缩】")]
	public static void ShowWindow()
    {
        var arr = Selection.objects;
        var path = new string[arr.Length];
        for (int i = 0; i < arr.Length;i++ )
        {
            path[i] = AssetDatabase.GetAssetPath(arr[i]);            
        }
        GetWindowWithRect<AnimationCompressWindow>(new Rect(0, 0, 400, 400));
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        m_rotationError= EditorGUILayout.FloatField("Rotation Error", m_rotationError);
        m_positionError = EditorGUILayout.FloatField("position Error", m_positionError);
        m_scaleError = EditorGUILayout.FloatField("position Error", m_scaleError);
        if(GUILayout.Button("更改选中文件夹下文件动作"))
        {
            Change();
        }    
        EditorGUILayout.EndVertical();
    }

    void Change()
    {
        var arr = Selection.objects;
        List<string> strs = new List<string>();
        for(int i=0;i<arr.Length;i++)
        {
            string path = AssetDatabase.GetAssetPath(arr[i]);                    
            GetPath(strs, path);            
        }
        m_paths = strs.ToArray();   

       
        UpdateAnimation();
    }    

    void GetPath(List<string> list,string path)
    {
        DirectoryInfo info = new DirectoryInfo(path);
        foreach (FileInfo file in info.GetFiles())
        {
            if (file.Extension == ".FBX" && file.Name.Contains("@"))
            {
                if(!list.Contains(file.FullName))
                    list.Add(file.FullName);
            }
        }
        foreach (DirectoryInfo dir in info.GetDirectories())
        {
            if (!File.Exists(path))   
                GetPath(list, dir.FullName);
        }
    }


    void UpdateAnimation()
    {
        if(m_paths==null||m_paths.Length==0)
        {
            EditorUtility.DisplayDialog("", "未找到动作", "Close");
            return;
        }
        for (int i = 0; i < m_paths.Length; i++)
        {
            float progress = (i + 1) * 1f / m_paths.Length;
            EditorUtility.DisplayProgressBar("更改动作压缩设置", m_paths[i], progress);
            string path = m_paths[i].Replace(subStr,"");            
            var assetImporter = AssetImporter.GetAtPath(path);
            var importer = assetImporter as ModelImporter;
            if (importer == null)
            {
                Debug.LogError("subStr:" + subStr);
                Debug.LogError("path:"+path);
                EditorUtility.DisplayDialog("Error", "请选中对应资源文件夹","关闭");
                EditorUtility.ClearProgressBar();
                break;
            }
            importer.animationPositionError = m_positionError;
            importer.animationRotationError = m_rotationError;
            importer.animationScaleError = m_scaleError;
            AssetDatabase.WriteImportSettingsIfDirty(path);
        }
        EditorUtility.ClearProgressBar();
    }
}
