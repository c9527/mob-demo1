﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

public class ABAnalyse : EditorWindow
{
    private WWW www;
    private List<ABInfo> m_result = new List<ABInfo>();
    private Vector2 m_scrollPos;

    public static void Open()
    {
        GetWindowWithRect<ABAnalyse>(new Rect(0, 0, 330, 350), true);
    }

    void OnGUI()
    {
        if (GUILayout.Button("打开"))
        {
            var path = EditorUtility.OpenFilePanel("打开", "Assets/StreamingAssetsLib/", "assetbundle");
            if (!string.IsNullOrEmpty(path))
                www = new WWW("file://" + path);
        }
        GUILayout.BeginHorizontal();
        GUILayout.Label("类型", GUILayout.Width(100));
        GUILayout.Label("名称", GUILayout.Width(100));
        GUILayout.EndHorizontal();
        m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false, GUILayout.Height(300));
        for (int i = 0; i < m_result.Count; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(m_result[i].typeName, GUILayout.Width(100));
            GUILayout.Label(m_result[i].objName, GUILayout.Width(100));
            GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
    }

    void Update()
    {
        if (www != null && www.progress == 1)
        {
            m_result.Clear();
            var arr = www.assetBundle.LoadAll();
            for (int i = 0; i < arr.Length; i++)
            {
                var typeNameArr = arr[i].GetType().ToString().Split('.');
                m_result.Add(new ABInfo()
                {
                    typeName = typeNameArr[typeNameArr.Length - 1],
                    objName = arr[i].name
                });
            }
            m_result.Sort(CompareABInfo);
            www.assetBundle.Unload(false);
            www.Dispose();
            www = null;
        }
    }

    int CompareABInfo(ABInfo a, ABInfo b)
    {
        var result = a.typeName.CompareTo(b.typeName);
        if (result == 0)
            result = a.objName.CompareTo(b.objName);
        return result;
    }

    class ABInfo
    {
        public string typeName;
        public string objName;
    }
}