﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;


/// <summary>
/// 资源预处理
/// </summary>
public class FPSAssetPostprocessor : AssetPostprocessor {


    public void OnPreprocessModel()
    {
        ModelImporter mi = (ModelImporter) assetImporter;
        if (mi == null)
            return;

        //string assetPath = mi.assetPath;

        mi.importMaterials = false;

        // Weapon Model
        if (assetPath.Contains("ResourcesLib/Weapons/Animation"))
        {
            mi.animationType = ModelImporterAnimationType.Legacy;
             
        }
        else if(assetPath.Contains("Resources/Weapons"))
        {
            string fbxName = Path.GetFileNameWithoutExtension(assetPath);
            if (fbxName.EndsWith("_low"))
                mi.importAnimation = false;
            mi.importMaterials = false;
            mi.animationType = ModelImporterAnimationType.Legacy;
        }
        
        // Arm,Player
        //if (assetPath.Contains("Resources/Roles"))
        //{
        //    mi.animationType = ModelImporterAnimationType.Generic;
        //    mi.importMaterials = false;
        //}   
    
        // Building FBX
        if(assetPath.Contains("ResourcesLib/Scenes"))
        {
            string assetName = EditorHelper.GetAssetNameByPath(assetPath);
            if (!assetName.ToLower().EndsWith("_ani"))
            {
                mi.animationType = ModelImporterAnimationType.None;
                mi.importAnimation = false;
            }
            else
            {
                mi.animationType = ModelImporterAnimationType.Legacy;
                mi.importAnimation = true;
            }
           
        }

        // SceneItem
        if(assetPath.Contains("Resources/SceneItem/Fbx"))
        {
            string fbxName = EditorHelper.GetAssetNameByPath(assetPath);
            string strID = fbxName.Substring(0, 5);
            int id;
            if (strID.Length == 5 && int.TryParse(strID, out id))
            {
                mi.animationType = ModelImporterAnimationType.None;
                mi.importAnimation = false;
            }
            else
            {
                mi.animationType = ModelImporterAnimationType.Legacy;
            }
        }
        
    }

    public void OnPreprocessTexture()
    {
        string name = assetPath.Substring(assetPath.LastIndexOf('\\') + 1);
        string nameWithoutExtension = name.Substring(0, name.IndexOf("."));

        if(assetPath.Contains(EditorConst.WEAPON_TEX_PATH))
        {
            TexImpArg argWeapon = TextureHelper.texArg_Weapon;
            argWeapon.needTransparent = assetPath.ToLower().Contains("crystal");
            TextureHelper.SetTexImp(assetPath, argWeapon, false);
        }
        else if(assetPath.Contains(EditorConst.PLAYER_TEX_PATH))
        {
            TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_Player, false);
        }
        else if(assetPath.EndsWith(EditorConst.SUFFIX_LIGHTMAP))
        {
            TextureHelper.SetLightMap(assetPath, false);
        }
        else if(assetPath.Contains(EditorConst.SCENE_LIB_PATH))
        {
            if (name.ToLower().Contains(EditorConst.SKYBOX) == true)
                TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_SkyBox, false);
            else
                TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_Scene, false);
        }
        else if(assetPath.Contains(EditorConst.SCENE_EFFECT_TEX_PATH))
        {
            if (assetPath.Contains(EditorConst.UI_EFFECT_TEX_PATH))
                TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_UIEffect, false);
            else
            {
                TexImpArg arg = TextureHelper.texArg_SceneEffect;
                if (nameWithoutExtension.EndsWith("_tc"))   // tc = TrueColor,以tc结尾的图就不压缩了
                    arg.tf = TextureImporterFormat.AutomaticTruecolor;
                TextureHelper.SetTexImp(assetPath, arg, false);
            }
        }
        else if(assetPath.Contains(EditorConst.UI_ATLAS_BATTLE_SMALL_MAP))
        {
            TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_AtlasBattleSmallMap, false, TextureImporterType.Sprite);
        }
        else if (assetPath.Contains(EditorConst.UI_ATLAS_ALPHA))
        {
            TextureHelper.SetTexImp(assetPath, TextureHelper.texArg_AtlasAlpha, false);
        }
    }
}
