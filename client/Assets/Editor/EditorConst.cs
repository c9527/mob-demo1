﻿using UnityEngine;
using System.Collections;

public class EditorConst {

    public const string WEAPON_TEX_PATH = "Assets/Resources/Weapons/Textures";

    public const string PLAYER_TEX_PATH = "Assets/Resources/Roles/Textures";

    public const string SCENE_LIB_PATH = "Assets/ResourcesLib/Scenes/";

    public const string SKYBOX = "skybox";

    public const string SUFFIX_LIGHTMAP = ".exr";

    public const string SCENE_EFFECT_TEX_PATH = "Assets/ResourcesLib/Effect/Textures";

    public const string UI_EFFECT_TEX_PATH = "Assets/ResourcesLib/Effect/Textures/UI";

    public const string UI_ATLAS_BATTLE_SMALL_MAP = "Assets/ResourcesLib/Atlas/BattleSmallMap";

    public const string UI_ATLAS_ALPHA = "Assets/ResourcesLib/AtlasAlpha";
}
