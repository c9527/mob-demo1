﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
public class AutoFBXSetting
{
    // avaatar列表
    // 参数配置列表
    enum AnimationType
    {
        None=0,
        Legacy=1,
        Generic=2,
        Humanoid=3
    }
    enum AvatarDefinition
    {
        CreateFromThisModel=0,
        CopyFromOtherAvatar=1
    }
    enum RotationBakeIntoPose
    {
        True=1,
        False=0
    }
    enum RotationBasedUpon
    {
        True = 1,
        False = 0
    }
    enum PositionYBakeIntoPose
    {
        True = 1,
        False = 0
    }
    enum PositionYBasedUpon
    {
        True = 1,
        False = 0
    }
    enum PositionXZBakeIntoPose
    {
        True = 1,
        False = 0
    }
    enum PositionXZBasedUpon
    {
        True = 1,
        False = 0
    }

    class DirectoryConfig
    {
        public string AnimationName;
        public AnimationType AnimationType;
        public AvatarDefinition AvatarDefinition;
        public string SourceAvatar;
        public RotationBakeIntoPose RotationBakeIntoPose;
        public RotationBasedUpon RotationBasedUpon;
        public PositionYBakeIntoPose PositionYBakeIntoPose;
        public PositionYBasedUpon PositionYBasedUpon;
        public PositionXZBakeIntoPose PositionXZBakeIntoPose;
        public PositionXZBasedUpon PositionXZBasedUpon;
        public DirectoryConfig(string animationName, AnimationType animationType, AvatarDefinition avatarDefinition, string sourceAvatar, RotationBakeIntoPose rotationBakeIntoPose, RotationBasedUpon rotationBasedUpon, PositionYBakeIntoPose positionYBakeIntoPose, PositionYBasedUpon positionYBasedUpon, PositionXZBakeIntoPose positionXZBakeIntoPose, PositionXZBasedUpon positionXZBasedUpon)
        {
            AnimationName = animationName;                                  // 动画名称
            AnimationType = animationType;                                  // 0-None 1-Legacy 2-Generic 3-Humanoid
            AvatarDefinition = avatarDefinition;                            // 0-CreateFromThisModel 1-CopyFromOtherAvatar
            SourceAvatar = sourceAvatar;                                    // 1,2
            RotationBakeIntoPose = rotationBakeIntoPose;                    // 0-否 1-是
            RotationBasedUpon = rotationBasedUpon;                          // 0-Root Node Rotation   1-Original
            PositionYBakeIntoPose = positionYBakeIntoPose;                  // 0-否 1-是
            PositionYBasedUpon = positionYBasedUpon;                        // 0-Root Node Rotation   1-Original
            PositionXZBakeIntoPose = positionXZBakeIntoPose;                // 0-否 1-是
            PositionXZBasedUpon = positionXZBasedUpon;                      // 0-Root Node Rotation   1-Original
        }
    }
    class SourceAvatar
    {
        public string AvatarPath;
        public string AvatarName;
        public SourceAvatar(string avatarPath,string avatarName)
        {
            AvatarPath = avatarPath;
            AvatarName = avatarName;
        }
    }

    // 此处创建循环动画列表
    private static string[] LoopListArm = {"Walk", };
    private static string[] LoopListPlayer = { "Walk", "stand" };
    // 此处创建模板meta文件路径
    static string[] arr_SourceMeta = { "Assets/Resources/Roles/Animation/Arm/arm@fire1_ak47.FBX.meta", "Assets/Resources/Roles/Animation/Player/@aim.FBX.meta" };

    static SourceAvatar[] arrSA=new SourceAvatar[10];
    static DirectoryConfig[] arrDC = new DirectoryConfig[10];
    static string[] arrName=new string[10];
    static string SourceMeta = "";
    static string AvatarGUID = "";                                                               // 当前avatarGUID
    //[MenuItem("Assets/自动设置FBX参数")]
    public static void AutoFBXsetting1()
    {
        //EditorSettings.externalVersionControl = "Visible Meta Files";
        
        // 此处创建配置信息,需创建配置名称以及具体配置参数arrName和arrDC要同时设置
        arrName[0] = "DefaultArm";
        arrName[1] = "DefaultPlayer";
        // 1,2表示的是avatar的名称，必须与所创建的avatar同名
        arrDC[0] = new DirectoryConfig("Arm", AnimationType.Generic, AvatarDefinition.CopyFromOtherAvatar, "1", RotationBakeIntoPose.True, RotationBasedUpon.True, PositionYBakeIntoPose.True, PositionYBasedUpon.True, PositionXZBakeIntoPose.True, PositionXZBasedUpon.False);
        arrDC[1] = new DirectoryConfig("Player", AnimationType.Humanoid, AvatarDefinition.CopyFromOtherAvatar, "2", RotationBakeIntoPose.True, RotationBasedUpon.True, PositionYBakeIntoPose.True, PositionYBasedUpon.True, PositionXZBakeIntoPose.True, PositionXZBasedUpon.True);

        // 此处创建avatar，需指定avatar路径以及avatar名称 
        arrSA[0] = new SourceAvatar("Assets/Resources/Roles/Fbx/arm.FBX.meta","1");
        arrSA[1] = new SourceAvatar("Assets/Resources/Roles/Fbx/terrorist.FBX.meta","2");

        //操作选中的文件夹或文件
        UnityEngine.Object[] arrOJ = Selection.objects;
        foreach (UnityEngine.Object obj in arrOJ)
        {
            if (obj.GetType() == typeof(UnityEngine.GameObject))
            {
                if (AssetDatabase.GetAssetPath(obj).Contains("@") && Path.GetExtension(AssetDatabase.GetAssetPath(obj)) != ".meta")
                { 
                    AutoFBXsetting(AssetDatabase.GetAssetPath(obj) + ".meta", AssetDatabase.GetAssetPath(obj)); 
                }
            }
            else
            {
                DirectoryInfo kDI = new DirectoryInfo(AssetDatabase.GetAssetPath(obj));
                FileInfo[] arrFI = kDI.GetFiles("*.FBX", SearchOption.AllDirectories);
                foreach (FileInfo fi in arrFI)
                {
                    string file = fi.FullName.Substring(fi.FullName.IndexOf("Assets"));
                    AutoFBXsetting(file + ".meta", file); 
                }
            }
        }
        //EditorSettings.externalVersionControl="Hidden Meta Files";
        EditorUtility.DisplayDialog("提示","设置成功","ok");
    }
    static void AutoFBXsetting(string DestMeta,string DestFBX)    
    {
        if (DestMeta.Contains("arm"))
            SourceMeta = arr_SourceMeta[0];
        else
            SourceMeta = arr_SourceMeta[1];
        //把meta文件的animation部分导入到模型的meta文件中，如果已经存在animation，则不添加
        string[] dest=new string[1000];
        int index = -1;
        string[] fbx = File.ReadAllLines(DestMeta);
        foreach (string f in fbx)
        {
            index++;
            dest[index] = f;
            if (f.Contains("clipAnimations"))
            {
                int i = f.IndexOf(":");
                if ((i + 1) == f.Length)
                    continue;
                dest[index] = dest[index].Substring(0,i+2);
                string[] animation = File.ReadAllLines(SourceMeta);
                bool flag=false;
                foreach (string a in animation)
                {
                    if(flag)
                    {
                        index++;
                        dest[index]=a;
                    }   
                    if (a.Contains("clipAnimations"))
                        flag = true;
                    if (a.Contains("maskSource"))
                        break;
                }
            }
        }

        // 获取模型的一些参数信息
        
        ModelImporter kMI = ModelImporter.GetAtPath(DestFBX) as ModelImporter;
        Type type = typeof(ModelImporter);
        PropertyInfo[] arrPI = type.GetProperties(~System.Reflection.BindingFlags.Public);
        PropertyInfo pi = null;
        foreach (PropertyInfo PI in arrPI)
        {
            if (PI.Name == "importedTakeInfos")
                pi = PI;
        }
        TakeInfo[] arrTI=null;
        if (pi != null)
        {
            MethodInfo mi = pi.GetGetMethod(true);
            if (mi != null)
            {
                arrTI = mi.Invoke(kMI, null) as TakeInfo[];
            }
        }

        if (arrTI == null)
        {
            EditorUtility.DisplayDialog("eror", "arrTI is null", "ok");
            return;
        }
        // 找对应的配置信息
        int index1=-1;
        bool la = false;
        foreach (string name in arrName)
        {
            if (name != null)
            {
                index1++;
                if (DestFBX.Contains(name))
                {
                    la = true;
                    break;
                }
            }
        }
        // 不存在 根据文件名使用默认的配置值
        if (!la)
        {
            if (DestFBX.Contains("Arm"))
                index1 = 0;
            else
                index1 = 1;
        }
        string[] arrListLoop = null;
        if (index1 == 0)
            arrListLoop = LoopListArm;
        else
            arrListLoop = LoopListPlayer;


        // 根据名字找对应的avatar
        foreach (SourceAvatar sa in arrSA)
        {
            if (sa != null)
            {
                if (sa.AvatarName == arrDC[index1].SourceAvatar)
                    AvatarGUID = GetAvatarGuidFromMetaFile(sa.AvatarPath);
            }
        }
        if (AvatarGUID == "")
        {
            EditorUtility.DisplayDialog("警告：","不存在avatar","ok");
        }

        // 修改模型参数信息
        int j=-1;
        foreach (string str in dest)
        {
            j++;
            if (j > index)
                break;
            
            if (str.Contains("name"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].defaultClipName;
                continue;
            }
             
            if (str.Contains("takeName"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].name;
                continue;
            }
            if (str.Contains("firstFrame"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].startTime * arrTI[0].sampleRate;
                continue;
            }
            if (str.Contains("lastFrame"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += arrTI[0].stopTime * arrTI[0].sampleRate;
                continue;
            }
            // 获取avatar的guid
            
            if (str.Contains("lastHumanDescriptionAvatarSource") && !str.Contains("guid"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += "{fileID: 9000000, guid: " + AvatarGUID + ",type: 3}";
                continue;
            }    
            if (str.Contains("copyAvatar"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].AvatarDefinition;
                continue;
            }
            if (str.Contains("animationType"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].AnimationType;
                continue;
            }

            // 判断是否循环
            bool jud = false;
            foreach (string str1 in arrListLoop)
            {
                if (DestFBX.Contains(str1))
                    jud = true;
            }

            if (str.Contains("loopTime"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                if (jud)
                    dest[j] += 1;
                else
                    dest[j] += 0;
                continue;
            }
            if (str.Contains("loopBlend:"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] +=0;
                continue;
            }
            if (str.Contains("loopBlendOrientation"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].RotationBakeIntoPose;
                continue;
            }
            if (str.Contains("loopBlendPositionY"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].PositionYBakeIntoPose;
                continue;
            }
            if (str.Contains("loopBlendPositionXZ"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].PositionXZBakeIntoPose;
                continue;
            }
            if (str.Contains("keepOriginalOrientation"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].RotationBasedUpon;
                continue;
            }
            if (str.Contains("keepOriginalPositionY"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].PositionYBasedUpon;
                continue;
            }
            if (str.Contains("keepOriginalPositionXZ"))
            {
                int i = dest[j].IndexOf(":");
                dest[j] = dest[j].Substring(0, i + 2);
                dest[j] += (int)arrDC[index1].PositionXZBasedUpon;
                continue;
            }
        }
        File.WriteAllLines(new DirectoryInfo("./").FullName+"/"+DestMeta, dest);// 修改完成后写入目标模型meta文件中
        AssetDatabase.Refresh();
    }
    static string GetAvatarGuidFromMetaFile(string path)
    {
        
        string[] file = File.ReadAllLines(path);
        string strReturn = "";
        foreach(string str in file)
        {
            if (str.Contains("guid"))
            {
                int i1 = str.IndexOf(":");
                int i2 = str.IndexOf(":",i1+1);
                int i3 = str.IndexOf(":",i2+1);
                strReturn = str.Substring(i3+2,32);
                break;
            }
        }
        return strReturn;
    }
}
