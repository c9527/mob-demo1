﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Sprites;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class PluginMenu
{

#region Plugins Menu

    [MenuItem("Plugins/SVN Update", false, 1)]
    public static void ExeSvnUpdate()
    {
        SvnOperation.ExeSvnUpdate();
    }

    [MenuItem("Plugins/SVN Commit", false, 2)]
    public static void ExeSvnCommit()
    {
        SvnOperation.ExeSvnCommit();
    }

    [MenuItem("Plugins/SVN Update All", false, 3)]
    public static void ExeSvnUpdateAll()
    {
        SvnOperation.ExeSvnUpdateAll();
    }

    [MenuItem("Plugins/SVN Commit All", false, 4)]
    public static void ExeSvnCommitAll()
    {
        SvnOperation.ExeSvnCommitAll();
    }

    [MenuItem("Plugins/发布工具", false, 5)]
    public static void OpenBuildTools()
    {
        BuildTools.Open();
    }

    [MenuItem("Plugins/AssetBundle打包/代码打包（测试）", false, 23)]
    public static void BuildDll()
    {
        //, new[] { @"Assets\Scripts\Lib\UpdateWorker.cs" }
        BuildAB.BuildDll("UnityVS.client.CSharp.csproj", "Client.dll.bytes", "Android");
        BuildAB.BuildDll("UnityVS.UpdateWorker.CSharp.xml", "UpdateWorker.dll.bytes", "Android");
        BuildAB.BuildFileList();

        //拷贝到StreamingAssets
        FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsPath() + "Managed");
        FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath() + "Managed", PluginUtil.GetEditorStreamingAssetsPath() + "Managed");
        AssetDatabase.Refresh();
    }

    [MenuItem("Plugins/AssetBundle打包/配置打包", false, 24)]
    public static void BuildConfig()
    {
        BuildAB.BuildConfigs(false);
        BuildAB.BuildFileList();

        //拷贝到StreamingAssets
        FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsPath() + "Config.assetbundle");
        FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath() + "Config.assetbundle", PluginUtil.GetEditorStreamingAssetsPath() + "Config.assetbundle");
        AssetDatabase.Refresh();
    }

    [MenuItem("Plugins/AssetBundle打包/资源打包", false, 25)]
    public static void BuildResources()
    {
        BuildAB.BuildResources();
    }

    [MenuItem("Plugins/AssetBundle打包/创建FileList", false, 26)]
    public static void BuildFileList()
    {
        BuildAB.BuildFileList();
    }

    [MenuItem("Plugins/UI/图集批处理", false, 51)]
    public static void AutoAtlas()
    {
        EditorUIHelper.UpdateAtlas();
    }

    [MenuItem("Plugins/UI/图集工具", false, 52)]
    public static void UpdateAtlasTag()
    {
        AtlasTools.Open();
    }
    
    [MenuItem("Plugins/UI/替换所有字体为Normal", false, 53)]
    public static void ReplaceAllFont()
    {
        PluginUtil.ReplaceAllFont();
    }

    [MenuItem("Plugins/UI/重新生成所有图集ETC")]
    public static void GenerateEtcTex()
    {
        var time = TimeUtil.GetNowTimeStamp();
        EditorUtility.DisplayProgressBar("构建图集中...", "", 0.5f);
        Packer.RebuildAtlasCacheIfNeeded(BuildTarget.Android, true, Packer.Execution.Normal);
        var dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("构建图集用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));
        EditorUtility.ClearProgressBar();

        time = TimeUtil.GetNowTimeStamp();
        var dirArr = Directory.GetDirectories("Assets/ResourcesLib/Atlas/");
        for (int i = 0; i < dirArr.Length; i++)
        {
            EditorUtility.DisplayProgressBar("分离图集透明通道", dirArr[i], i * 1f / dirArr.Length);
            AtlasBuild.GenerateEtcTex(dirArr[i]);
        }
        dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("分离通道用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));
        EditorUtility.ClearProgressBar();

        time = TimeUtil.GetNowTimeStamp();
        AssetDatabase.Refresh();
        dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("通道图导入用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));
    }

    [MenuItem("Plugins/音效/更新音效设置", false, 60)]
    public static void UpdateAudio()
    {
        AudioHelper.SetAudio();
    }

    [MenuItem("Plugins/分析AB", false, 61)]
    public static void OpenABAnalyse()
    {
        ABAnalyse.Open();

    }

    [MenuItem("Plugins/资源优化/UISprite优化", false, 70)]
    public static void PngSettin()
    {
        string path = "Assets/Resources/UISprites";                               // 目标文件夹

        CPngHelper.Spritesetting(path);
    }

    //[MenuItem("Plugins/资源优化/优化 贴图（场景，光照，角色，武器，特效）", false, 71)]
    //public static void CheckTex()
    //{
    //    TextureHelper.CheckTex();
    //}    

    [MenuItem("Plugins/资源优化/优化 角色（贴图）", false, 74)]
    public static void OptimizePlayer()
    {
        ResOptimizeHelper.OptimizePlayer();
    }

    [MenuItem("Plugins/资源优化/优化 武器（贴图)", false, 75)]
    public static void OptimizeWeapon()
    {
        ResOptimizeHelper.OptimizeWeapon();
    }

    [MenuItem("Plugins/资源优化/优化 特效 (层 + 材质 + 贴图）", false, 77)]
    public static void OptimizeEffect()
    {
        ResOptimizeHelper.OptimizeEffect();
    }

    [MenuItem("Plugins/资源优化/优化 场景 (材质 + 贴图 + LightMap）", false, 78)]
    public static void OptimizeScene()
    {
        ResOptimizeHelper.OptimizeScene();
    }

    [MenuItem("Plugins/资源优化/删除 多余特效资源", false, 79)]
    public static void DeleteUnusedEffectRes()
    {
        ResOptimizeHelper.DeleteUnusedEffectRes();
    }

    [MenuItem("Plugins/场景/一键处理场景（生成预设，生成空场景，导出后端数据，添加空场景到编辑器）", false, 80)]
    public static void OnkeyHandleScene()
    {
        SceneHelper.OneKeyHandleScene();
    }

    [MenuItem("Plugins/场景/检查当前场景规范", false, 81)]
    public static void CheckCurSceneCorrect()
    {
        SceneHelper.CheckCurSceneCorrect();
    }

    [MenuItem("Plugins/场景/生成当前场景预设", false, 82)]
    public static void CreateScenePrefab()
    {
        SceneHelper.CreateScenePrefab(true);
    }

    [MenuItem("Plugins/场景/导出场景后端数据", false, 83)]
    public static void ExportMapData()
    {
        MapExport.Export();
    }

    //[MenuItem("Plugins/场景/生成空场景", false, 84)]
    //public static void CreateEmptyScene()
    //{
    //    SceneHelper.CreateEmptyScene();
    //}

    [MenuItem("Plugins/场景/生成场景ColliderGroup模型", false, 85)]
    public static void CreateColliderGroupCube()
    {
        if (Selection.activeGameObject != null && Selection.activeGameObject.name == SceneConst.SCENE_CHILD_COLLIDERGROUP)
        {
            SceneHelper.CreateColliderGroupCube(Selection.activeGameObject.transform);
        }
    }

    [MenuItem("Plugins/场景/检查场景墙壁碰撞体是否旋转", false, 86)]
    public static void FindRotatedWall()
    {
        SceneHelper.FindRotatedWall();
    }


    [MenuItem("Plugins/场景/检查出生点的玩家是否会与场景碰撞", false, 87)]
    public static void CheckBornPointIntersect()
    {
        SceneHelper.CheckBornPointIntersect();
    }

    [MenuItem("Plugins/UI/导出PanelBattle自定义按钮信息", false, 88)]
    public static void BattleUIControlPosExport()
    {
        EditorUIHelper.BattleUIControlPosExport();
    }

    [MenuItem("Plugins/Test", false, 101)]
    public static void TestBtn()
    {
//        VersionManager.Backup();
//
//        //常规发正式热更
//        VersionManager.NextVersion("union", "release_0.4.7.000");
//        VersionManager.HotUpdate("union", "release_0.4.7.000", true);
//
//        //常规发测试版热更
//        if (VersionManager.IsOnlineBranch("union", "release_0.4.7.000"))
//            VersionManager.HotUpdate("union", "release_0.4.7.000", false);
//
//        //常规发整包
//        VersionManager.PublishPlayer("union", "release_0.4.8.000", "");
//        if (!VersionManager.IsOnlineBranch("union", "release_0.4.7.000"))
//            VersionManager.PublishPlayer("union", "release_0.4.7.000", "Check");
    }

    [MenuItem("Plugins/查看Unity调试端口", false, 102)]
    public static void GetUnityPort()
    {
        EditorUtility.DisplayDialog("Unity调试端口", PluginUtil.GetUnityPort(), "ok");
    }
    
    [MenuItem("Plugins/刷新Launcher的cfg.xml文件", false, 102)]
    public static void RefreshLauncherCfg()
    {
        const string cfgPath = @"D:\zjqz\pc\launcher\cfg.xml";
        if (!File.Exists(cfgPath))
        {
            EditorUtility.DisplayDialog("请在打包机上执行","", "确定");
            return;
        }

        var cfg = File.ReadAllText(cfgPath);
        var launcherCfg = Regex.Match(cfg, " *<LauncherCFG>.*?</LauncherCFG>", RegexOptions.Singleline).Value;
        var baseNode = Regex.Match(cfg, " *<Base>.*?</Base>", RegexOptions.Singleline).Value;
        var leastGameVersionNode = Regex.Match(cfg, " *<LeastGameVersion>.*?</LeastGameVersion>").Value;
        var gameVersionNode = Regex.Match(cfg, " *<GameVersion>.*?</GameVersion>").Value;

        var result = new StringBuilder();
        result.AppendLine("<Root version=\"" + DateTime.Now.ToString("yyyy-M-dd") + "\">");
        result.AppendLine(launcherCfg);
        var arr = Directory.GetFiles(@"D:\zjqz\pc\launcher\", "*.exe");
        Array.Sort(arr, (a, b) =>
        {
            var aName = Path.GetFileName(a);
            var aChannel = int.Parse(aName.Split('_')[1]);

            var bName = Path.GetFileName(b);
            var bChannel = int.Parse(bName.Split('_')[1]);

            return aChannel - bChannel;
        });
        for (int i = 0; i < arr.Length; i++)
        {
            var fileName = Path.GetFileName(arr[i]);
            var channel = fileName.Split('_')[1];
            result.AppendLine("   <Launcher channel=\"" + channel + "\">");
            result.AppendLine("      <URL>http://zjqz-cdnres.me4399.com/pc/launcher/" + fileName + "</URL>");
            result.AppendLine("      <MD5>" + EditorUIHelper.GenerateMd5(arr[i]) + "</MD5>");
            result.AppendLine("   </Launcher>");
        }
        result.AppendLine(baseNode);
        result.AppendLine(leastGameVersionNode);
        result.AppendLine(gameVersionNode);
        result.AppendLine("</Root>");
        File.WriteAllText(cfgPath, result.ToString(), new UTF8Encoding(false));
        Debug.Log("Launcher cfg.xml refresh done.");
    }


    [MenuItem("Plugins/场景打包测试", false, 103)]
    public static void TestChangeCamp()
    {
       

        EditorHelper.VisitAssetTraverse("Assets/Resources/Scenes", ".unity",(obj) =>
        {
             
            string name = obj.name;
            string assetPath = AssetDatabase.GetAssetPath((obj));
            string outputPath = "";
            if (name.StartsWith(("map_")))
            {
                outputPath = "Scenes/" + name + "/" + name;
            }
            else
            {
                outputPath = "Scenes/" + name;
            }

            Debug.Log(assetPath + " " + outputPath);


            BuildAB.BuildSceneAssetBundle(new string[] { assetPath }, outputPath);
        });
    }

    [MenuItem("GameObject/UI/复制UI路径",false,1)]
    public static void CopyPath()
    {
        List<String> strs = new List<string>();
        var ob = Selection.activeTransform;
        while (ob != null)
        {
            strs.Add(ob.name);
            ob = ob.parent;
        }
        string ret = "";

        for (int i = strs.Count - 3; i > -1; i--)
        {
            if (i != strs.Count - 3)
            {
                ret += "/";
            }
            ret += strs[i];
        }
        Clipboard.clipBoard = ret;
    }


    [MenuItem("Plugins/解析全民配置", false, 103)]
    public static void Test()
    {
        var path = "F:/Configs.assetbundle";
        if (!string.IsNullOrEmpty(path))
        {
            var ab = AssetBundle.CreateFromMemoryImmediate(File.ReadAllBytes(path));
            var stream = new MemoryStream(((TextAsset)ab.mainAsset).bytes);
            try
            {
                var formatter = new BinaryFormatter();
                var data = (Dictionary<Type, object>)formatter.Deserialize(stream);
                foreach (var config in data.Values)
                {
                    var result = new StringBuilder();
                    var mgrFields = config.GetType().GetFields();
                    var mgrFieldObject = mgrFields[0].GetValue(config);

                    var mgrFieldType = mgrFieldObject.GetType();
                    if (mgrFieldType.IsGenericType)
                    {
                        var itemType = mgrFieldType.GetGenericArguments()[0];
                        var itemFields = itemType.GetFields();
                        for (int i = 0; i < itemFields.Length; i++)
                        {
                            result.Append(itemFields[i].Name + "\t");
                        }
                        result.AppendLine("");
                        var count = Convert.ToInt32(mgrFieldType.GetProperty("Count").GetValue(mgrFieldObject, null));
                        for (int i = 0; i < count; i++)
                        {
                            var item = mgrFieldType.GetProperty("Item").GetValue(mgrFieldObject, new object[] { i });
                            for (int j = 0; j < itemFields.Length; j++)
                            {
                                var value = itemFields[j].GetValue(item);
                                var valueType = value.GetType();
                                if (valueType.IsGenericType)
                                {
                                    var count2 = Convert.ToInt32(valueType.GetProperty("Count").GetValue(value, null));
                                    for (int i2 = 0; i2 < count2; i2++)
                                    {
                                        var item2 = valueType.GetProperty("Item").GetValue(value, new object[] { i2 });
                                        if (i2 < count2-1)
                                            result.Append(item2 + ";");
                                        else
                                            result.Append(item2 + "\t");
                                    }
                                }
                                else
                                    result.Append(value + "\t");
                            }
                            result.AppendLine("");
                        }
                    }
                    else
                        Debug.LogError(mgrFieldType);
                    File.WriteAllText("F:/Configs/" + config.GetType().Name + ".csv", result.ToString(), Encoding.Unicode);
                }
            }
            finally
            {
                stream.Dispose();
                ab.Unload(false);
            }
        }
    }

    [MenuItem("Plugins/卸载资源")]
    public static void TestChangeCamp2()
    {
        ResourceManager.ClearAll();
    }

    [MenuItem("Plugins/导出选中对象的坐标信息")]
    public static void ExportTransformInfo()
    {
        MapExport.ExportTranformInfo();
    }

    [MenuItem("Plugins/------------------------------------------8")]
    public static void sperateGameRuning() { }

    [MenuItem("Plugins/更新配置文件")]
    public static void UpdateWeaponConfig()
    {
        Setting.UpdateConfig();
    }

    [MenuItem("Plugins/设定相机参数")]
    public static void Camerasetting()
    {
        Setting.CameraSetting();
    }

    [MenuItem("Plugins/------------------------------------------角色")]
    public static void sperateRole() { }

    [MenuItem("Assets/保存角色Collider数据")]
    [MenuItem("Plugins/保存角色Collider数据")]
    public static void SaveRoleColliderData()
    {
        RoleColliderCopier.SaveRoleColliderData();
    }

    [MenuItem("Assets/更新角色Collider数据")]
    [MenuItem("Plugins/更新角色Collider数据")]
    public static void InitRoleColliderData()
    {
        RoleColliderCopier.UpdateGOCollider();
    }

    [MenuItem("Plugins/修复UI.dll加载失败")]
    public static void ReimportUGUIDll()
    {
        ReimportUnityEngineUI.ReimportUI();
    }

    [MenuItem("Plugins/导出协议")]
    public static void ExportProto()
    {
        ProtoExport.Export();
        AssetDatabase.Refresh();
        Debug.Log("协议导出完成，正在刷新重编代码");
    }

    [MenuItem("Plugins/清空协议（除报错）")]
    public static void ClearProto()
    {
        ProtoExport.Clear();
        AssetDatabase.Refresh();
        Debug.Log("清空协议完成，请等待代码编译完成再重新导出");
    }

#endregion

#region Assets Menu

    [MenuItem("Assets/【依赖打包所选资源】", false, 0)]
    public static void BuildSelect()
    {
        BuildAB.BuildSelect();
    }

    [MenuItem("Assets/【处理所选图集文件夹】", false, 1)]
    public static void SetAtlasConfig()
    {
        var arr = Selection.objects;
        var path = new string[arr.Length];
        var check = true;
        for (int i = 0; i < arr.Length; i++)
        {
            path[i] = AssetDatabase.GetAssetPath(arr[i]);
            if (!path[i].StartsWith("Assets/ResourcesLib/Atlas/") || path[i].Split(new []{'/'}, StringSplitOptions.RemoveEmptyEntries).Length != 4)
            {
                check = false;
                break;
            }
        }
        if (check)
            EditorUIHelper.UpdateAtlas(path);
        else
            EditorUtility.DisplayDialog("请只选择图集文件夹，可以多选", "", "确定");
    }

    [MenuItem("Assets/生成 武器预设 不透明")]
    public static void CreateWeapon()
    {
        WeaponHelper.CreateWeaponPrefabSelected(false, false);
    }

    [MenuItem("Assets/生成 武器预设 流光（不透明）")]
    public static void CreateWeaponFlow()
    {
        WeaponHelper.CreateWeaponPrefabSelected(false, true);
    }

    [MenuItem("Assets/生成 武器预设 透明")]
    public static void CreateWeaponCrystal()
    {
        WeaponHelper.CreateWeaponPrefabSelected(true, false);
    }

    //[MenuItem("Assets/导出后端地图数据")]
    //public static void ExportMap()
    //{
    //    //var obj = Selection.activeObject as GameObject;
    //    //if (obj == null || !obj.name.StartsWith("map_"))
    //    //{
    //    //    EditorUtility.DisplayDialog("", "请选中地图perfab", "ok");
    //    //    return;
    //    //}
    //    //MapExport.Export(obj);
    //}

    [MenuItem("Assets/自动设置角色动画参数")]
    public static void SetRoleFbx()
    {
        AutoFBXSetting.AutoFBXsetting1();
    }

    [MenuItem("Assets/分别导出包")]
    public static void ExportPackage()
    {
        EditorHelper.ExportPackage();
    }

    [MenuItem("Assets/---------------------------其他")]
    public static void OtherAssetsFunc() { }

    [MenuItem("Assets/预览场景 (只提供预览，不要保存原场景！)")]
    public static void PreviewSceneTest()
    {
        Debug.Log("开始预览场景，先判断是否有选中某场景");
        string sceneName = Selection.activeObject.name;

        var strScenePath = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (strScenePath == null || !strScenePath.Contains(".unity"))
        {
            EditorUtility.DisplayDialog("Select Scene", "You Must Select a Scene!(*.unity)", "Ok");
            EditorApplication.Beep();
            return;
        }
        if (!strScenePath.Contains("map_"))
        {
            EditorUtility.DisplayDialog("Select Map File", "You Must Select a map_**** file!", "ok");
            return;
        }
        Debug.Log("Opening " + strScenePath);

        EditorApplication.OpenScene(strScenePath);

        //启动时去掉生出点检测，如果有出生点就选一个当默认点
        Vector3 defaultBornPoint = new Vector3(1, 5, 1);
        GameObject prefab = GameObject.Find(sceneName + "_prefab");
        if (null != prefab)
        {
            Transform transTempConfig = prefab.transform.Find("TempConfig");
            if (null != transTempConfig)
            {
                for (int i = 0; i < 3; i++)
                {
                    Transform bps = prefab.transform.FindChild("TempConfig/BornPoint" + i);
                    if (bps == null)
                        continue;
                    foreach (Transform bp in bps)
                    {
                        if (bp.position[1] > 5) //这里美术设置的地图有时会很高
                            defaultBornPoint = new Vector3(bp.position[0], bp.position[1], bp.position[2]);
                        else
                            defaultBornPoint = new Vector3(bp.position[0], 5, bp.position[2]);
                        break;
                    }
                    break;
                }
                transTempConfig.gameObject.TrySetActive(false);
            }
        }

        GameObject goScene = GameObject.Find("Main Camera");
        if (null != goScene)
            goScene.TrySetActive(false);
        GameObject secondCamera = GameObject.Find("Camera");
        if (null != secondCamera)
            secondCamera.TrySetActive(false);

        GameObject capsule = new GameObject("Capsule");
        Camera capsuleCamera = new GameObject("Main Camera").AddComponent<Camera>();
        capsuleCamera.transform.position = new Vector3(0, 0.7f, 0);
        capsuleCamera.fieldOfView = 43F;
        GameObject capsuleObjectCamera = GameObject.Find("Main Camera");
        //capsuleObjectCamera.AddMissingComponent<MouseLook>();
        capsuleObjectCamera.transform.parent = capsule.transform;
        capsule.AddMissingComponent<MouseLook>();
        capsule.AddMissingComponent<PreviewFPSController>().bornVector = defaultBornPoint;
        capsuleObjectCamera.AddMissingComponent<PreviewSceneUI>();

        EditorApplication.isPlaying = true;
    }

    #endregion

    #region GameObject Menu

    [MenuItem("GameObject/UI/Text/Test16")]
    public static void CreateText()
    {
        EditorUIHelper.CreateText(16);
    }
    [MenuItem("GameObject/UI/Text/Text18")]
    public static void CreateText18()
    {
        EditorUIHelper.CreateText(18);
    }
    [MenuItem("GameObject/UI/Text/Text20")]
    public static void CreateText20()
    {
        EditorUIHelper.CreateText(20);
    }

    [MenuItem("GameObject/UI/Button/默认按钮")]
    public static void CreateGreenButton()
    {
        EditorUIHelper.CreateNormalButton();
    }
    [MenuItem("GameObject/UI/Button/默认按钮（黑边）")]
    public static void CreateYellowButton()
    {
        EditorUIHelper.CreateNormalButtonOutline();
    }
    [MenuItem("GameObject/UI/Button/黄色按钮")]
    public static void CreateBlackButton()
    {
        EditorUIHelper.CreateYellowButton();
    }
    [MenuItem("GameObject/UI/Button/黄色圆角矩形按钮")]
    public static void CreateRoundedYellowButton()
    {
        EditorUIHelper.CreateRoundedYellowButton();
    }
    [MenuItem("GameObject/UI/Button/蓝色圆角矩形按钮")]
    public static void CreateRoundedBlueButton()
    {
        EditorUIHelper.CreateRoundedBlueButton();
    }

    [MenuItem("GameObject/UI/ScrollView/ScrollDataGrid（垂直）")]
    public static void CreateScrollDataGridV()
    {
        EditorUIHelper.CreateScrollDataGrid(null, Direction.Vertical);
    }
    [MenuItem("GameObject/UI/ScrollView/ScrollDataGrid（水平）")]
    public static void CreateScrollDataGridH()
    {
        EditorUIHelper.CreateScrollDataGrid(null, Direction.Horizontal);
    }
    [MenuItem("GameObject/UI/[复制全路径]")]
    public static void CopyFullPath()
    {
        Transform go = UnityEditor.Selection.activeTransform;
        string path = "";
        while (go != null)
        {
            path = "/" + go.name + path;
            go = go.parent;
        }
        Clipboard.clipBoard = path;
        Debug.Log(path);
    }

#endregion

   
}
