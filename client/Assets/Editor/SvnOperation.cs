﻿using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Debug = UnityEngine.Debug;

public class SvnOperation
{
    public static void ExeSvnUpdate()
    {
        PluginUtil.Excute("TortoiseProc.exe", "/command:update /path:" + new DirectoryInfo("./").FullName);
    }

    public static void ExeSvnCommit()
    {
        PluginUtil.Excute("TortoiseProc.exe", "/command:commit /path:" + new DirectoryInfo("./").FullName);
    }

    public static void ExeSvnUpdateAll()
    {
        PluginUtil.Excute("TortoiseProc.exe", "/command:update /path:" + new DirectoryInfo("./../").FullName);
    }

    public static void ExeSvnCommitAll()
    {
        PluginUtil.Excute("TortoiseProc.exe", "/command:commit /path:" + new DirectoryInfo("./../").FullName);
    }

    public static void ExeSvnRevert(string path, bool waitForExit = false)
    {
        PluginUtil.Excute("svn", "revert -R " + path, true, false, waitForExit);
    }

    public static void ExeSvnFullRevert(string path, bool waitForExit = true)
    {
        PluginUtil.Excute("TortoiseProc.exe", "/command:cleanup /noui /noprogressui /nodlg /revert /delunversioned /path:" + path, true, false, waitForExit);
    }

    /// <summary>
    /// 提交指定路径下的所有文件，无需再次确认
    /// </summary>
    /// <param name="path"></param>
    /// <param name="logMsg"></param>
    public static bool ExeSvnCommitAll(string path, string logMsg)
    {
        var result = PluginUtil.Excute("svn", "status " + path, true, true, true);
        var addMatch = Regex.Match(result, @"\? +(.+)", RegexOptions.Compiled);
        var addPathList = new List<string>();
        while (addMatch.Success)
        {
            var addPath = addMatch.Groups[1].Value.TrimEnd('\r', '\n');
            //解决文件名中含有@对svn造成的冲突
            if (addPath.IndexOf("@") != -1)
                addPath += "@";
            addPathList.Add('\"' + addPath + '\"');
            addMatch = addMatch.NextMatch();
        }
        if (addPathList.Count > 0)
        {
            var addResult = PluginUtil.Excute("svn", "add " + string.Join(" ",addPathList.ToArray()), true, true, true);
            if (IsFail(addResult))
            {
                Debug.LogError("svn添加失败\n" + addResult);
                return false;
            }
            Debug.Log("添加文件：\n" + addResult);
        }

        var delMatch = Regex.Match(result, @"\! +(.+)", RegexOptions.Compiled);
        var delPathList = new List<string>();
        while (delMatch.Success)
        {
            var delPath = delMatch.Groups[1].Value.TrimEnd('\r', '\n');
            //解决文件名中含有@对svn造成的冲突
            if (delPath.IndexOf("@") != -1)
                delPath += "@";
            delPathList.Add('\"' + delPath + '\"');
            delMatch = delMatch.NextMatch();
        }
        if (delPathList.Count > 0)
        {
            var delResult = PluginUtil.Excute("svn", "delete " + string.Join(" ", delPathList.ToArray()), true, true, true);
            if (IsFail(delResult))
            {
                Debug.LogError("svn删除失败\n" + delResult);
                return false;
            }
            Debug.Log("删除文件：\n" + delResult);
        }

        var commitResult = PluginUtil.Excute("svn", string.Format("commit {0} -m \"{1}\"", path, logMsg), true, true, true);
        Debug.Log("提交文件：\n" + commitResult);

        if (IsFail(commitResult))
        {
            Debug.LogError("svn提交失败\n" + commitResult);
            return false;
        }
        return true;
    }

    private static bool IsFail(string result)
    {
        return result.IndexOf("failed") != -1 || Regex.IsMatch(result, "svn: E");
    }

    public static List<string> GetCommitFileList(string path, string prefix)
    {
        var result = PluginUtil.Excute("svn", "status " + path, true, true, true);
        var match = Regex.Match(result, @"M +(.+)", RegexOptions.Compiled);
        var diffFileList = new List<string>();
        while (match.Success)
        {
            var addPath = match.Groups[1].Value.TrimEnd('\r', '\n');
            //解决文件名中含有@对svn造成的冲突
            if (addPath.IndexOf("@") != -1)
                addPath += "@";
            addPath = addPath.Replace("\\", "/");
            diffFileList.Add(addPath);
            match = match.NextMatch();
        }
        return diffFileList;
    }
}
