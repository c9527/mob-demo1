﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


class MapExport
{
    const float HEIGHT_DIF = 0.5f;
    const int NEAR_DIS = 4;
    const int SHORT_DIS = 45;

    class RoutePoint
    {
        public int id;
        public int z, x;
        public List<RoutePoint> reaches = new List<RoutePoint>();
    }

    static int msPointCount = 0;
    static List<RoutePoint>[] msZoneList = null;

    class PPInfo
    {
        public RoutePoint pt = null;
        public double dis = double.PositiveInfinity;
        public bool smooth = false;
        public float height;
        public bool valid = true;
    }

    static PPInfo[,] msPoints;
    static int msZN;
    static int msXN;

    struct Pos
    {
        public double z, x;
    }
    static Pos[] msCircleEdge;

    static AIZone[] msAIZones = null;

    public static void Export()
    {
        string sceneName = EditorHelper.GetCurBattleSceneName();
        if (sceneName.IsNullOrEmpty())
        {
            EditorUtility.DisplayDialog("", "请打开战斗场景", "ok");
            return;
        }

        GameObject scenePrefab = GameObject.Find(sceneName + "_prefab");
        if (scenePrefab == null)
        {
            EditorUtility.DisplayDialog("", "找不到场景节点：" + sceneName + "_prefab", "OK");
            return;
        }

        string id = sceneName.Substring(4);

        Debug.Log("处理地图：" + id);
        string path = "../server/cfg/map/" + id + ".config";
        StreamWriter sw = File.CreateText(path);
        sw.WriteLine("ID = \"{0}\"", id);

        Vector2 minVec;
        Vector2 maxVec;
        Vector2 sizeVec;
        float maxY;

        string msg = "";
        msg += Export_BornPoint(scenePrefab, sw) + "\n";
        msg += Exprot_MissionPoint(scenePrefab, sw) + "\n";
        msg += Export_BombPoint(scenePrefab, sw) + "\n";
        msg += Export_DaggerOw_Point(scenePrefab, sw) + "\n";
        msg += Export_DropPoint(scenePrefab, sw) + "\n";
        msg += Export_TeamHeroDropPoint(scenePrefab, sw) + "\n";
        msg += Export_AI(scenePrefab, sw) + "\n";
        msg += Export_DMMSceneItemRandom(scenePrefab, sw) + "\n";
        msg += Export_Data(scenePrefab, sw, "../server/cfg/map/" + id, out minVec, out maxVec, out sizeVec, out maxY) + "\n";
        Debug.Log("写入文件：" + path);
        sw.Close();

        SceneHelper.ExportDMMSceneItem(id, scenePrefab, minVec, maxVec, sizeVec, maxY);

        EditorUtility.DisplayDialog("", msg, "ok");
    }

    private static string Export_BornPoint(GameObject obj, StreamWriter sw)
    {
        int campCount = 0;
        int bpCount = 0;
        sw.WriteLine("BornPoint = {");

        for (int i = 0; i < 3000; i++)
        {
            Transform bps = obj.transform.FindChild("TempConfig/BornPoint" + i);
            if (bps == null)
                continue;
            campCount++;
            sw.WriteLine("    [{0}] = {{", i);
            foreach (Transform bp in bps)
            {
                bpCount++;
                sw.WriteLine("        {{{0}, {1}, {2}, {3}, {4}, {5} }},",
                    bp.position[0], bp.position[1], bp.position[2],
                    bp.eulerAngles[0], bp.eulerAngles[1], bp.eulerAngles[2]);
            }
            sw.WriteLine("    },");
        }

        sw.WriteLine("}");

        sw.WriteLine("MonsterBornPoint = {");

        for (int i = 0; i < 50; i++)
        {
            Transform bps = obj.transform.FindChild("TempConfig/MonsterBorn" + i);
            if (bps == null)
                continue;
            campCount++;
            sw.WriteLine("    [{0}] = {{", i);
            foreach (Transform bp in bps)
            {
                bpCount++;
                sw.WriteLine("        {{{0}, {1}, {2}, {3}, {4}, {5} }},",
                    bp.position[0], bp.position[1], bp.position[2],
                    bp.eulerAngles[0], bp.eulerAngles[1], bp.eulerAngles[2]);
            }
            sw.WriteLine("    },");
        }

        sw.WriteLine("}"); 
        sw.WriteLine();
        return string.Format("阵营：{0}个  总计出生点：{1}个", campCount, bpCount);
    }

    private static string Exprot_MissionPoint(GameObject obj, StreamWriter sw)
    {
        int roundCnt = 0;
        int msPoint = 0;
        sw.WriteLine("MissionPoint = {");

        for (int i = 0; i < 50; i++)
        {
            Transform bps = obj.transform.FindChild("Config/MissionPoint" + i);
            if (bps == null)
                continue;
            roundCnt++;
            sw.WriteLine("    [{0}] = {{", i);
            foreach (Transform bp in bps)
            {
                msPoint++;
                sw.WriteLine("        [\"{0}\"] = {{{1}, {2}, {3}, {4}}},",
                    bp.name, bp.position[0], bp.position[1], bp.position[2], bp.eulerAngles[1]);
            }
            sw.WriteLine("    },");
        }

        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("回合：{0}个  总计任务点：{1}个", roundCnt, msPoint);
    }

    // 导出随机物件
    private static string Export_DMMSceneItemRandom(GameObject obj, StreamWriter sw)
    {
        sw.WriteLine("DMMSceneItemRandom = {");

        Transform dmmSceneItem = obj.transform.FindChild("TempConfig/DMMSceneItemRandom");
        int cnt = 0;
        if (dmmSceneItem != null)
        {
            cnt = dmmSceneItem.childCount;
            for (int i = 0; i < dmmSceneItem.childCount; i++)
            {
                Transform sceneItem = dmmSceneItem.GetChild(i);
                int sceneItemID = -1;
                int.TryParse(sceneItem.name.Substring(0, sceneItem.name.IndexOf("_")), out sceneItemID);
                if (sceneItemID != -1)
                {
                    sw.WriteLine("  {");
                    sw.WriteLine("      id = {0},", sceneItemID);
                    sw.WriteLine("      pos = {{ {0}, {1}, {2}, {3}, {4}, {5} }},",
                        sceneItem.position[0], sceneItem.position[1], sceneItem.position[2],
                        sceneItem.eulerAngles[0], sceneItem.eulerAngles[1], sceneItem.eulerAngles[2]);
                    sw.WriteLine("      scale = {{ {0}, {1}, {2} }}", sceneItem.localScale[0], sceneItem.localScale[1], sceneItem.localScale[2]);

                    sw.WriteLine("  },");
                }
            }
        }

        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("共导出 {0} 个场景物件", cnt);
    }

    private static string Export_BombPoint(GameObject obj, StreamWriter sw)
    {
        int bpCount = 0;
        Transform bps = obj.transform.FindChild("Config/BombPoint");
        if (bps == null)
            return "总计爆破点 0";
        sw.WriteLine("BombPoint = {");

        for (int cnt = 1; cnt <= 10; ++cnt)
        {
            Transform childBp = bps.FindChild(cnt.ToString());
            if (childBp == null)
                continue;

            sw.Write("    [{0}] = {{", cnt);

            bpCount++;
            sw.Write(" {0}, {1}, {2}, ",
                childBp.position[0], childBp.position[1], childBp.position[2]);
            sw.WriteLine("},");

        }
        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("总计爆破点：{0}个", bpCount);
    }

    /// <summary>
    /// 刀锋据点
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="sw"></param>
    /// <returns></returns>
    private static string Export_DaggerOw_Point(GameObject obj, StreamWriter sw)
    {
        int bpCount = 0;
        Transform bps = obj.transform.FindChild("Config/JDPoint");
        if (bps == null)
            return "总计据点 0";
        sw.WriteLine("JDPoint = {");

        for (int cnt = 1; cnt <= 10; ++cnt)
        {
            Transform childBp = bps.FindChild(cnt.ToString());
            if (childBp == null)
                continue;

            sw.Write("    [{0}] = {{", cnt);

            bpCount++;
            sw.Write(" {0}, {1}, {2}",
                childBp.position[0], childBp.position[1], childBp.position[2]);
            sw.WriteLine("},");

        }
        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("总计据点：{0}个", bpCount);
    }

    private static string Export_DropPoint(GameObject obj, StreamWriter sw)
    {
        int bpGroup = 0;
        int bpCount = 0;
        sw.WriteLine("DropPoint = {");
        for (int i = 0; i < 10; i++)
        {
            Transform bps = obj.transform.FindChild("TempConfig/DropPoint" + i);
            if (bps == null)
                continue;
            bpGroup++;
            sw.WriteLine("    [{0}] = {{", i);
            foreach (Transform bp in bps)
            {
                bpCount++;
                sw.WriteLine("        {{{0}, {1}, {2}, {3}, {4}, {5} }},",
                    bp.position[0], bp.position[1], bp.position[2],
                    bp.eulerAngles[0], bp.eulerAngles[1], bp.eulerAngles[2]);
            }
            sw.WriteLine("    },");
        }
        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("掉落组：{0}个  总计掉落点：{1}个", bpGroup, bpCount);
    }

    private static string Export_TeamHeroDropPoint(GameObject obj, StreamWriter sw)
    {
        int dpGroup = 0;
        int dpCount = 0;
        sw.WriteLine("TeamHeroDropPoint = {");
        for (int i = 0; i < 10; i++)
        {
            Transform dps = obj.transform.FindChild("TempConfig/TeamHeroDropPoint" + i);
            if (dps == null)
                continue;
            dpGroup++;
            sw.WriteLine("    [{0}] = {{", i);
            foreach (Transform dp in dps)
            {
                dpCount++;
                sw.WriteLine("        {{{0}, {1}, {2}, {3}, {4}, {5} }},",
                    dp.position[0], dp.position[1], dp.position[2],
                    dp.eulerAngles[0], dp.eulerAngles[1], dp.eulerAngles[2]);
            }
            sw.WriteLine("    },");
        }
        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("团队英雄掉落阵营：{0}个  总计掉落点：{1}个", dpGroup, dpCount);
    }

    private static string Export_AI(GameObject obj, StreamWriter sw)
    {
        msAIZones = null;

        Transform aiGroup = obj.transform.FindChild("TempConfig/AI");
        if (aiGroup == null)
            return "总计AI点：0个";
        msAIZones = aiGroup.GetComponentsInChildren<AIZone>();
        sw.WriteLine("AI = {");
        for (int i = 0; i < msAIZones.Length; i++)
        {
            AIZone ai = msAIZones[i];
            ai.IndexID = i + 1;
            Transform tr = ai.transform;
            sw.WriteLine("    {");
            sw.WriteLine("        name = \"{0}\",", tr.name);
            sw.WriteLine("        pos = {{{0}, {1}}},", tr.position[0], tr.position[2]);
            sw.WriteLine("        size = {{{0}, {1}}},", tr.lossyScale[0], tr.lossyScale[2]);
            sw.WriteLine("        shape = \"{0}\",", ai._zoneShape);
            if (ai._lookAt)
            {
                float y = tr.eulerAngles.y % 360;
                if (y < 0)
                    y += 360;
                sw.WriteLine("        look = {0},", y);
            }
            if (ai._targetPos.Length > 0)
            {
                sw.WriteLine("        target = {");
                foreach (Transform target in ai._targetPos)
                {
                    if (target == null)
                        continue;
                    Vector3 p = target.position;
                    Vector3 s = target.lossyScale;
                    if (target.GetComponent<AIZone>() != null)
                        s /= 2;
                    sw.WriteLine("            {{{0}, {1}, {2}, {3}, \"{4}\"}},", p.x, p.z, s.x, s.z, target.name);
                }
                sw.WriteLine("        },");
            }
            sw.WriteLine("    },");
        }
        sw.WriteLine("}");
        sw.WriteLine();
        return string.Format("总计AI点：{0}个", msAIZones.Length);
    }

    private static string Export_Data(GameObject obj, StreamWriter sw, string path, out Vector2 minVec, out Vector2 maxVec, out Vector2 sizeVec, out float maxY)
    {
        BoxCollider[] boxs = obj.transform.FindChild("ColliderGroup").GetComponentsInChildren<BoxCollider>().Where(b => b.gameObject.name.ToLower() != "ceiling").ToArray();
        Debug.Log("boxs: " + boxs.Length);
        float[] mins = new float[] { float.MaxValue, float.MaxValue, float.MaxValue };
        float[] maxs = new float[] { float.MinValue, float.MinValue, float.MinValue };
        foreach (BoxCollider box in boxs)
            CheckMinMax(box, mins, maxs);
        msXN = (int)((maxs[0] - mins[0]) * 10) + 1;
        msZN = (int)((maxs[2] - mins[2]) * 10) + 1;
        float tall = maxs[1] - mins[1];
        sw.WriteLine("XMin = {0};  XMax = {1};", mins[0], maxs[0]);
        sw.WriteLine("YMin = {0};  YMax = {1};", mins[1], maxs[1]);
        sw.WriteLine("ZMin = {0};  ZMax = {1};", mins[2], maxs[2]);
        sw.WriteLine("Width = {0};  Height = {1};", msXN, msZN);
        sw.WriteLine();

        minVec = new Vector2(mins[0], mins[2]);
        maxVec = new Vector2(maxs[0], maxs[2]);
        sizeVec = new Vector2(msXN, msZN);
        maxY = maxs[1];

        Debug.Log(string.Format("地图大小：{0}x{1}x{2}", msXN, msZN, tall));

        msPoints = new PPInfo[msZN, msXN];
        for (int z = 0; z < msZN; z++)
        {
            for (int x = 0; x < msXN; x++)
                msPoints[z, x] = new PPInfo() { height = mins[1] };
        }
        foreach (BoxCollider box in boxs)
        {
            float[] _mins = new float[] { float.MaxValue, float.MaxValue, float.MaxValue };
            float[] _maxs = new float[] { float.MinValue, float.MinValue, float.MinValue };
            CheckMinMax(box, _mins, _maxs);
            int left = (int)((_mins[0] - mins[0]) * 10);
            int right = (int)((_maxs[0] - mins[0]) * 10);
            int bottom = (int)((_mins[2] - mins[2]) * 10);
            int top = (int)((_maxs[2] - mins[2]) * 10);
            Ray ray = new Ray();
            ray.direction = new Vector3(0, -1, 0);
            RaycastHit hit;
            for (int z = bottom; z <= top; z++)
            {
                float fz = z / 10f + mins[2];
                for (int x = left; x <= right; x++)
                {
                    float fx = x / 10f + mins[0];
                    ray.origin = new Vector3(fx, maxs[1] + 1, fz);
                    if (box.Raycast(ray, out hit, tall + 2))
                    {
                        if (hit.point.y > msPoints[z, x].height)
                            msPoints[z, x].height = hit.point.y;
                    }
                }
            }
        }

        int count = (int)(SHORT_DIS * Mathf.PI * 4);
        msCircleEdge = new Pos[count];
        for (int i = 0; i < count; i++)
        {
            float d = Mathf.PI * 2 * i / count;
            msCircleEdge[i].z = Mathf.Sin(d) * SHORT_DIS;
            msCircleEdge[i].x = Mathf.Cos(d) * SHORT_DIS;
        }
        msPointCount = 0;

        MarkAllUnreachable();
        FillAllPoint();
        FindAllRoute();

        sw.WriteLine("RoutePoint = {");
        for (int zid = 0; zid < msZoneList.Length; zid++)
        {
            foreach (RoutePoint pt in msZoneList[zid])
                sw.WriteLine("    {{ {0}, {1}, {2}, {3}, {{{4}}} }},", pt.id, zid + 1, pt.x / 10f + mins[0], pt.z / 10f + mins[2], string.Join(",", pt.reaches.Select(p => p.id.ToString()).ToArray()));
        }
        sw.WriteLine("}");
        sw.WriteLine();

        string str = SavePng(path + "h.png", (z, x) => 0xff - (byte)((maxs[1] - msPoints[z, x].height) * 0xff / tall));
        sw.WriteLine("Data = \"{0}\"", str);
        sw.WriteLine();

        str = SavePng(path + "p.png", (z, x) =>
        {
            RoutePoint pt = msPoints[z, x].pt;
            return pt == null ? 0 : pt.id;
        }, true);
        sw.WriteLine("PointData = \"{0}\"", str);

        byte[,] bmp = new byte[msZN, msXN];
        foreach (var list in msZoneList)
        {
            foreach (RoutePoint pt1 in list)
            {
                bmp[pt1.z, pt1.x] = 0xff;
                foreach (RoutePoint pt2 in pt1.reaches)
                {
                    int dz = pt2.z - pt1.z;
                    int dx = pt2.x - pt1.x;
                    int l = (int)Math.Sqrt(dz * dz + dx * dx);
                    for (int i = 1; i < l; i++)
                    {
                        int z = pt1.z + dz * i / l;
                        int x = pt1.x + dx * i / l;
                        if (bmp[z, x] == 0)
                            bmp[z, x] = 0x44;
                    }
                    pt2.reaches.Remove(pt1);
                }
            }
        }
        SavePng(path + "r.png", (z, x) =>
        {
            int b = bmp[z, x];
            if (b > 0)
                return b;
            return msPoints[z, x].valid ? 0x11 : 0;
        });

        sw.WriteLine();

        return string.Format("总计寻路点：{0}个，可走区域：{1}个", msPointCount, msZoneList.Length);
    }

    static bool CheckHeight(int z, int x, int dz, int dx)
    {
        return Math.Abs(msPoints[z - dz, x - dx].height - msPoints[z, x].height) <= HEIGHT_DIF;
    }

    static void MarkAllUnreachable()
    {
        for (int z = 1; z < msZN; z++)
        {
            for (int x = 1; x < msXN; x++)
            {
                if (!CheckHeight(z, x, 1, 1) || !CheckHeight(z, x, 1, 0) || !CheckHeight(z, x, 0, 1))
                    MarkUnreachable(z, x);
            }
        }
    }

    static void MarkUnreachable(int mz, int mx)
    {
        for (int dz = -NEAR_DIS; dz < NEAR_DIS; dz++)
        {
            int z = mz + dz;
            if (z < 0 || z >= msZN)
                continue;
            for (int dx = -NEAR_DIS; dx < NEAR_DIS; dx++)
            {
                int x = mx + dx;
                if (x < 0 || x >= msXN)
                    continue;
                if ((dz + .5f) * (dz + .5f) + (dx + .5f) * (dx + .5f) > NEAR_DIS * NEAR_DIS)
                    continue;
                msPoints[z, x].valid = false;
            }
        }
    }

    static void FillAllPoint()
    {
        List<List<RoutePoint>> zones = new List<List<RoutePoint>>();
        for (int z = 0; z < msZN; z++)
        {
            for (int x = 0; x < msXN; x++)
            {
                PPInfo pp = msPoints[z, x];
                if (!pp.valid || pp.pt != null)
                    continue;
                List<RoutePoint> list = new List<RoutePoint>();
                zones.Add(list);
                msZoneList = zones.ToArray();
                FillPoint(list, z, x);
                while (FillZonePoint(list)) ;
            }
        }
    }

    static bool FillZonePoint(List<RoutePoint> list)
    {
        bool filled = false;
        for (int z = 0; z < msZN; z++)
        {
            for (int x = 0; x < msXN; x++)
            {
                PPInfo pp = msPoints[z, x];
                if (!pp.valid || pp.smooth)
                    continue;
                if (TryFillPoint(list, z, x))
                    filled = true;
            }
        }
        return filled;
    }

    static bool TryFillPoint(List<RoutePoint> list, int pz, int px)
    {
        for (int z = pz - 1; z <= pz + 1; z++)
        {
            if (z < 0 || z >= msZN)
                continue;
            for (int x = px - 1; x <= px + 1; x++)
            {
                if (x < 0 || x >= msXN)
                    continue;
                PPInfo pp = msPoints[z, x];
                if (pp.smooth)
                {
                    RoutePoint oldpt = pp.pt;
                    RoutePoint pt = FillPoint(list, z, x);
                    oldpt.reaches.Add(pt);
                    pt.reaches.Add(oldpt);
                    return true;
                }
            }
        }
        return false;
    }

    static RoutePoint FillPoint(List<RoutePoint> list, int pz, int px)
    {
        RoutePoint pt = new RoutePoint()
        {
            id = ++msPointCount,
            z = pz,
            x = px
        };
        list.Add(pt);
        foreach (Pos pos in msCircleEdge)
        {
            bool smooth = true;
            float oh = msPoints[pz, px].height;
            for (double l = 0; l <= SHORT_DIS; l += 0.5)
            {
                int z = (int)(pos.z * l / SHORT_DIS + pz + .5);
                if (z < 0 || z >= msZN)
                    break;
                int x = (int)(pos.x * l / SHORT_DIS + px + .5);
                if (x < 0 || x >= msXN)
                    break;
                PPInfo pp = msPoints[z, x];
                float nh = pp.height;
                if (Math.Abs(nh - oh) > HEIGHT_DIF)
                    break;
                if (!pp.valid)
                    smooth = false;
                oh = nh;
                if (pp.smooth && !smooth)
                    continue;
                if (l < pp.dis || (smooth && !pp.smooth))
                {
                    pp.pt = pt;
                    pp.dis = l;
                    pp.smooth = smooth;
                }
            }
        }
        return pt;
    }

    static void FindAllRoute()
    {
        foreach (var list in msZoneList)
        {
            var ary = list.ToArray();
            for (int i = 0; i < ary.Length; i++)
            {
                RoutePoint pt1 = ary[i];
                for (int j = i + 1; j < ary.Length; j++)
                {
                    RoutePoint pt2 = ary[j];
                    if (pt1.reaches.Contains(pt2))
                        continue;
                    if (RouteReachable(pt1, pt2))
                    {
                        pt1.reaches.Add(pt2);
                        pt2.reaches.Add(pt1);
                    }
                }
            }
            foreach (var pt in list)
                ReduceReaches(pt);
        }
    }

    static bool RouteReachable(RoutePoint pt1, RoutePoint pt2)
    {
        int dz = pt2.z - pt1.z;
        int dx = pt2.x - pt1.x;
        int ls = dz * dz + dx * dx;
        if (ls > SHORT_DIS * SHORT_DIS * 4)
            return false;
        int l = (int)Math.Sqrt(ls);
        for (int i = 1; i < l; i++)
        {
            int z = pt1.z + dz * i / l;
            int x = pt1.x + dx * i / l;
            if (!msPoints[z, x].valid)
                return false;
        }
        return true;
    }

    static double PointDistance(RoutePoint pt1, RoutePoint pt2)
    {
        int dz = pt2.z - pt1.z;
        int dx = pt2.x - pt1.x;
        return Math.Sqrt(dz * dz + dx * dx);
    }

    static void ReduceReaches(RoutePoint pt1)
    {
        foreach (RoutePoint pt2 in pt1.reaches.ToArray())
        {
            double l = PointDistance(pt1, pt2);
            if ((int)l <= SHORT_DIS)
                continue;
            foreach (RoutePoint pt3 in pt1.reaches)
            {
                if (pt3 == pt2)
                    continue;
                if (!pt3.reaches.Contains(pt2))
                    continue;
                double l2 = PointDistance(pt1, pt3) + PointDistance(pt3, pt2);
                if (l2 < l * 1.4)
                {
                    pt1.reaches.Remove(pt2);
                    pt2.reaches.Remove(pt1);
                    break;
                }
            }
        }
    }

    private static string SavePng(string path, Func<int, int, int> func, bool bit16 = false)
    {
        MemoryStream pngData = new MemoryStream((msXN + 1) * msZN);
        MemoryStream outData = new MemoryStream();
        for (int z = 0; z < msZN; z++)
        {
            pngData.Position = (msXN + 1) * (msZN - z - 1);
            pngData.WriteByte(0);
            for (int x = 0; x < msXN; x++)
            {
                int b = func(z, x);
                pngData.WriteByte((byte)(0xff - (byte)b));
                outData.WriteByte((byte)b);
                if (bit16)
                    outData.WriteByte((byte)(b >> 8));
            }
        }

        CPngHelper.IHDR head = new CPngHelper.IHDR();
        head.width = msXN;
        head.height = msZN;
        head.bitDepth = 8;
        head.colourType = CPngHelper.ColorType.Greyscale;
        CPngHelper.Chunk data = new CPngHelper.Chunk();
        data.name = "IDAT";
        data.data = CDataHelper.ZLibCompress(pngData.ToArray());
        CPngHelper.Chunk end = new CPngHelper.Chunk();
        end.name = "IEND";
        end.data = new byte[0];
        FileStream png = File.Create(path);
        CPngHelper.WriteChunks(png, new CPngHelper.Chunk[] { head.ToChunk(), data, end });
        png.Close();

        byte[] buf = CDataHelper.ZLibCompress(outData.ToArray());
        StringBuilder sb = new StringBuilder();
        foreach (byte b in buf)
            sb.AppendFormat("\\x{0:x2}", b);
        return sb.ToString();
    }

    private static void CheckMinMax(BoxCollider box, float[] mins, float[] maxs)
    {
        Vector3 size = box.size / 2;
        for (int x = -1; x <= 1; x += 2)
        {
            for (int y = -1; y <= 1; y += 2)
            {
                for (int z = -1; z <= 1; z += 2)
                {
                    Vector3 pos = box.transform.TransformPoint(new Vector3(size.x * x, size.y * y, size.z * z));
                    for (int i = 0; i < 3; i++)
                    {
                        if (pos[i] < mins[i])
                            mins[i] = pos[i];
                        if (pos[i] > maxs[i])
                            maxs[i] = pos[i];
                    }
                }
            }
        }
    }

    public static string ExportTranformInfo()
    {
        if (Selection.activeGameObject == null)
            return "输出路径 null";
        var tranArr = Selection.GetTransforms(SelectionMode.Deep);
        var result = new StringBuilder(tranArr.Length);
        for (int i = 0; i < tranArr.Length; i++)
        {
            var tran = tranArr[i];
            var meshFilter = tran.GetComponent<MeshFilter>();
            if (meshFilter == null)
                continue;
            result.Append(tran.localPosition.x + " ");
            result.Append(tran.localPosition.y + " ");
            result.Append(tran.localPosition.z + " ");
            result.Append(tran.localScale.x + " ");
            result.Append(tran.localScale.y + " ");
            result.Append(tran.localScale.z + " ");
            var eular = tran.eulerAngles;
            result.Append(eular.x + " ");
            result.Append(eular.y + " ");
            result.Append(eular.z + " ");
            result.Append(tran.name + " ");
            result.AppendLine(meshFilter.sharedMesh.name);
        }
        File.WriteAllText("c:\\mapTranformInfo.txt", result.ToString());
        return "输出路径：c:\\mapTranformInfo.txt";
    }
}
