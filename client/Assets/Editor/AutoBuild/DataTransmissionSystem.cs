﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

class DataTransmissionSystem
{
    private const string URL_HOME_PAGE = "http://dts-lan.gz4399.com/";
    private const string URL_LOGIN_PAGE = "http://dts-lan.gz4399.com/login";
    private const string URL_ADMIN_RELEASE = "http://dts-lan.gz4399.com/admin/release";
    private const string URL_API_RELEASE = "http://dts-lan.gz4399.com/api/release/";
    private const string CONTENT_TYPE = "application/x-www-form-urlencoded; charset=UTF-8";
    private const string USERNAME = "mashang@4399.com";
    private const string PASSWORD = "84546241";

    private static HttpClient m_client = new HttpClient();

    public static void Reset()
    {
        m_client = new HttpClient();
    }

    public static void Login()
    {
        var xsrf = GetXsrf();
        if (string.IsNullOrEmpty(xsrf))
        {
            m_client.DownloadData(URL_HOME_PAGE);
            xsrf = GetXsrf();
        }

        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = m_client.UploadString(URL_LOGIN_PAGE, string.Format("{0}&redirect=/admin/tasks&email={1}&password={2}", xsrf, USERNAME, PASSWORD));
        result = Regex.Unescape(result);

        Debug.Log(IsSuccess(result) ? "数传系统登录成功" : result);
    }

    public static List<List<string>> GetTasks(int tryCount = 1)
    {
        if (tryCount < 0)
            return new List<List<string>>();
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = Encoding.UTF8.GetString(m_client.DownloadData(URL_ADMIN_RELEASE));

        if (IsNeedLogin(result))
        {
            Login();
            return GetTasks(tryCount - 1);
        }

        var taskList = new List<List<string>>();
        var match = Regex.Match(result, "<tr[^\n]*?data-id=\"(\\w+?)\"[^\n]*?>.*?<a[^\n]*?>([^\n]*?)</a>.*?<td>(\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d)[\\d-: >]*?</td>.*?<td>(\\w{3,4})</td>.*?</tr>", RegexOptions.Singleline | RegexOptions.Compiled);
        while (match.Success)
        {
            if (!Regex.IsMatch(match.Groups[2].Value, "-server-satble"))
                taskList.Add(new List<string>() {match.Groups[3].Value, match.Groups[2].Value, match.Groups[1].Value, match.Groups[4].Value });
            match = match.NextMatch();
        }

        return taskList.Count > 16 ? taskList.GetRange(0, 16) : taskList;
    }

    public static void PerformTask(string id)
    {
        if (string.IsNullOrEmpty(id))
            return;
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = m_client.UploadString(URL_API_RELEASE + "/" + id, "PUT", "");
        result = Regex.Unescape(result);
        Debug.Log(IsSuccess(result) ? string.Format("[{0}]任务开始发布", id) : result);
    }

    public static void CancelTask(string id)
    {
        if (string.IsNullOrEmpty(id))
            return;
        m_client.Headers.Remove(HttpRequestHeader.ContentType);
        var result = m_client.Delete(URL_API_RELEASE + "/" + id);
        result = Regex.Unescape(result);
        Debug.Log(IsSuccess(result) ? string.Format("[{0}]任务中止", id) : result);
    }

    private static bool IsNeedLogin(string result)
    {
        return result.IndexOf("登录已过期", StringComparison.Ordinal) != -1 || result.IndexOf("登陆", StringComparison.Ordinal) != -1;
    }

    private static bool IsSuccess(string result)
    {
        return result.IndexOf("\"error\": 0", StringComparison.Ordinal) != -1;
    }

    private static string GetXsrf()
    {
        var xsrf = m_client.CookieContainer.GetCookies(new Uri(URL_HOME_PAGE))["_xsrf"];
        if (xsrf != null)
            return xsrf.ToString();
        return "";
    }
}