﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

public static class AutoBuild
{
    private static readonly string[] DebugLevels = { "Assets/Driver.unity" };
    public static BuildParams m_lastBuildParams;

    public static void Build(string key, BuildParams buildParams)
    {
        var config = ReloadConfig(key);
        var buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), config.Platform);
        if (EditorUserBuildSettings.activeBuildTarget != buildTarget)
        {
            EditorUtility.DisplayDialog("提示", string.Format("平台不匹配，当前平台为{0}，目标平台为{1}，请手动切换平台", EditorUserBuildSettings.activeBuildTarget, buildTarget), "确定");
            return;
        }

        //创建GameDefine文件
        if (!BuildGameDefine(key, buildParams.buildVersionType))
            return;

        var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(BuildTools.RESOURCE_ONLINE_PATH + BuildTools.GetUpdateDirName(config.UpdateUrl) + Path.DirectorySeparatorChar + "Version.txt"));

        var btg = config.Platform;
        if (btg == "StandaloneWindows")
            btg = "Standalone";
        var buildTargetGroup = (BuildTargetGroup)Enum.Parse(typeof(BuildTargetGroup), btg);
        var isActiveSdk = !string.IsNullOrEmpty(config.SDKName);

        buildParams.version = BuildTools.GetNextGameVersion(dic.GetValue("PrepareVersion"), buildParams.buildVersionType);
        buildParams.buildTarget = buildTarget;
        buildParams.buildTargetGroup = buildTargetGroup;
        buildParams.isActiveSdk = isActiveSdk;
        buildParams.SdkName = config.SDKName;
        Debug.Log("isActiveSdk=" + isActiveSdk);

        Directory.CreateDirectory(config.OutputPath);
        //只删目录下的文件，不删下面的目录
        var fileArr = Directory.GetFiles(config.OutputPath);
        for (int i = 0; i < fileArr.Length; i++)
        {
            FileUtil.DeleteFileOrDirectory(fileArr[i]);
        }
        if (!string.IsNullOrEmpty(config.CopyPath))
            Directory.CreateDirectory(config.CopyPath);

        if (!string.IsNullOrEmpty(config.Icon))
        {
            var icon = AssetDatabase.LoadMainAssetAtPath("Assets/Editor/Project/" + config.Icon) as Texture2D;
            var arr = new Texture2D[PlayerSettings.GetIconSizesForTargetGroup(buildTargetGroup).Length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = icon;
            }
            PlayerSettings.SetIconsForTargetGroup(buildTargetGroup, arr);
        }

        PlayerSettings.productName = config.ProductName;
        PlayerSettings.companyName = config.CompanyName;

        m_lastBuildParams = buildParams;
        var target = EditorUserBuildSettings.activeBuildTarget;
        if (target == BuildTarget.iPhone)
            BuildForIos(config, buildParams);
        else if(target == BuildTarget.WebPlayer)
            BuildForWeb(config, buildParams);
        else if(target == BuildTarget.StandaloneWindows)
            BuildForPc(config, buildParams);
        else
            BuildForAndroid(config, buildParams);

#if UNITY_EDITOR_WIN
        SvnOperation.ExeSvnRevert("ProjectSettings/ProjectSettings.asset", true);
        SvnOperation.ExeSvnRevert("ProjectSettings/EditorBuildSettings.asset", true);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();
#endif
    }

    private static void BuildForWeb(ConfigBuildLine config, BuildParams buildParams)
    {
        string[] levels = { "Assets/GameLoader.unity", "Assets/UpdateWorker.unity", "Assets/Driver.unity" };
        PlayerSettings.usePlayerLog = true;
        PlayerSettings.bundleVersion = buildParams.version;

        var app_name = Parse(config.PackageName);
//        if (buildParams.debugErrorMode)
//            app_name += "_Debug";
//        else if (buildParams.developmentMode)
//        app_name += "_Development";
        var app_url = Path.Combine(config.OutputPath, app_name);
        var app_copy_url = Path.Combine(config.CopyPath, app_name);

        Caching.CleanCache();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();

//        if (Directory.Exists(app_url))//按时间备份同名目录
//        {
//            FileUtil.MoveFileOrDirectory(app_url, app_url + DateTime.Now.ToString("_yyMMddHHmm"));
//        }

        var buildStartTime = TimeUtil.GetNowTimeStamp();
        var user_defines = "Assetbundle";
        user_defines += buildParams.isActiveSdk ? ";ActiveSDK" : "";
        user_defines += ";LANG_" + buildParams.lang;
        var buildOption = BuildOptions.None;
        if (buildParams.developmentMode || buildParams.debugErrorMode)
        {
            buildOption |= BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
            user_defines += ";DebugErrorMode";
        }

        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildParams.buildTargetGroup, user_defines);
        string result = BuildPipeline.BuildPlayer(buildParams.debugErrorMode ? DebugLevels : levels, app_url, buildParams.buildTarget, buildOption);

        if (string.IsNullOrEmpty(result))
        {
            //增加自定义的index.html主页和资源、脚本
            FileUtil.CopyFileOrDirectory("AutoBuild/web/Assets", app_url + "/Assets");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/Scripts", app_url + "/Scripts");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/css",app_url+"/css");
            FileUtil.CopyFileOrDirectory("AutoBuild/web/images", app_url + "/images");
            var main_str = File.ReadAllText("AutoBuild/web/index.html.template");
            var js_str = File.ReadAllText("AutoBuild/web/game.js");
            main_str = main_str.Replace("{0}", config.ProductName);
            js_str = js_str.Replace("{1}", app_name + ".unity3d");
            js_str = js_str.Replace("{2}", config.UpdateUrl);
            File.WriteAllText(app_url + "/index.html", main_str);
            File.WriteAllText(app_url + "/Scripts/game.js", js_str,UTF8Encoding.UTF8);
            Debug.Log(string.Format("【{0}】发布成功\n{1}", config.Name, GetSummary(app_name, TimeUtil.GetPassTime(buildStartTime))));
            Process.Start("explorer", Path.GetFullPath(config.OutputPath));
        }
        else
        {
            Debug.LogError(string.Format("【{0}】发布失败\n", config.Name));
        }
    }

    private static void BuildForPc(ConfigBuildLine config, BuildParams buildParams)
    {
        string[] levels = { "Assets/GameLoader.unity" };

        PlayerSettings.usePlayerLog = true;

        //查错模式不单独打dll，所以这里要重新设置符号
        if (buildParams.debugErrorMode)
        {
            var defines = "Assetbundle;DebugErrorMode";
            defines += buildParams.isActiveSdk ? ";ActiveSDK" : "";
            defines += ";LANG_" + buildParams.lang;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildParams.buildTargetGroup, defines);
        }

        var exeName = Parse(config.PackageName);
        if (buildParams.debugErrorMode)
            exeName += "_Debug";
        else if (buildParams.developmentMode)
            exeName += "_Development";
        var exeFullName = Path.Combine(config.OutputPath, exeName);
        var exeCopyFullName = Path.Combine(config.CopyPath, exeName);
        exeFullName += ".exe";
        exeCopyFullName += ".exe";

        Caching.CleanCache();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();

        var buildStartTime = TimeUtil.GetNowTimeStamp();
        var buildOption = BuildOptions.None;
        if (buildParams.developmentMode || buildParams.debugErrorMode)
            buildOption = BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
        string result = BuildPipeline.BuildPlayer(buildParams.debugErrorMode ? DebugLevels : levels, exeFullName, buildParams.buildTarget, buildOption);
        var dtTime = TimeUtil.GetPassTime(buildStartTime);

        if (string.IsNullOrEmpty(result))
        {
            if (!string.IsNullOrEmpty(config.CopyPath))
            {
                if ((!File.Exists(exeCopyFullName) || EditorUtility.DisplayDialog("提示", "【备份目录】有重名包，覆盖旧包还是丢弃新包？" + "\n" + exeCopyFullName, "覆盖", "丢弃")))
                    File.Copy(exeFullName, exeCopyFullName, true);
            }

            Debug.Log(string.Format("【{0}】打包成功\n{1}", config.Name, GetSummary(exeName, dtTime)));
            Process.Start("explorer", Path.GetFullPath(config.OutputPath));
        }
        else
            Debug.LogError(string.Format("【{0}】打包失败\n", config.Name));
    }

    private static void BuildForAndroid(ConfigBuildLine config, BuildParams buildParams)
    {
        string[] levels = { "Assets/GameLoader.unity" };

        PlayerSettings.bundleIdentifier = config.Identifer;
        PlayerSettings.bundleVersion = buildParams.version;

        //查错模式不单独打dll，所以这里要重新设置符号
        if (buildParams.debugErrorMode)
        {
            var defines = "Assetbundle;DebugErrorMode";
            defines += buildParams.isActiveSdk ? ";ActiveSDK" : "";
            defines += ";LANG_" + buildParams.lang;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildParams.buildTargetGroup, defines);
        }

        PlayerSettings.Android.bundleVersionCode = int.Parse(GetTime());//1611151550;//int.Parse(GetTime());
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel9;
        PlayerSettings.Android.splashScreenScale = AndroidSplashScreenScale.ScaleToFill;
        PlayerSettings.Android.keystoreName = "SY2Key.keystore";
        PlayerSettings.Android.keyaliasName = "4399";

        PlayerSettings.keyaliasPass = "ssj4399";
        PlayerSettings.keystorePass = "ssj4399";

        var apkName = Parse(config.PackageName);
        if (buildParams.debugErrorMode)
            apkName += "_Debug";
        else if (buildParams.developmentMode)
            apkName += "_Development";
        var apkFullName = Path.Combine(config.OutputPath, apkName);
        var apkCopyFullName = Path.Combine(config.CopyPath, apkName);
        apkFullName += ".apk";
        apkCopyFullName += ".apk";

        Caching.CleanCache();
        DeleteSdk();
        if (buildParams.isActiveSdk)
            CopyAndroidSdk(config.SDKName, config.Identifer);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();

        var buildStartTime = TimeUtil.GetNowTimeStamp();
        var buildOption = BuildOptions.None;
        if (buildParams.developmentMode || buildParams.debugErrorMode)
            buildOption = BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
        string result = BuildPipeline.BuildPlayer(buildParams.debugErrorMode ? DebugLevels : levels, apkFullName, buildParams.buildTarget, buildOption);
        var dtTime = TimeUtil.GetPassTime(buildStartTime);
        DeleteSdk();

        if (string.IsNullOrEmpty(result))
        {
            if (!string.IsNullOrEmpty(config.CopyPath))
            {
                if ((!File.Exists(apkCopyFullName) || EditorUtility.DisplayDialog("提示", "【备份目录】有重名包，覆盖旧包还是丢弃新包？" + "\n" + apkCopyFullName, "覆盖", "丢弃")))
                    File.Copy(apkFullName, apkCopyFullName, true);
            }

            Debug.Log(string.Format("【{0}】打包成功\n{1}", config.Name, GetSummary(apkName, dtTime)));
            if (buildParams.platforms != null && buildParams.platforms.Length > 0)
            {
                var args = "-g "+ config.FnGameName;
                args += " -p " + string.Join("#", buildParams.platforms);
                args += " -i " + apkFullName;
                args += " -q";
                if (!buildParams.corner) args += " -nc";
                if (!buildParams.splash) args += " -ns";
                var lines = File.ReadAllLines(@"E:\FNClient\FNClient.bat");
                if (lines[lines.Length - 1].Trim() != "exit")
                {
                    lines = lines.ArrayConcat("exit");
                    File.WriteAllLines(@"E:\FNClient\FNClient.bat", lines, Encoding.ASCII);
                }
                PluginUtil.Excute(@"AutoBuild\BuildPlatformApkAndUpload.bat", args);
            }
            else
            {
                Process.Start("explorer", Path.GetFullPath(config.OutputPath));
            }
        }
        else
            Debug.LogError(string.Format("【{0}】打包失败\n", config.Name));
    }

    private static void BuildForIos(ConfigBuildLine config, BuildParams buildParams)
    {
        string[] levels = { "Assets/GameLoader.unity", "Assets/UpdateWorker.unity", "Assets/Driver.unity" };

        var arr = buildParams.version.Split('.');
        PlayerSettings.bundleVersion = arr[0] + "." + arr[1] + "." + arr[2]+arr[3];
        PlayerSettings.shortBundleVersion = arr[0] + "." + arr[1] + "." + arr[2];
        PlayerSettings.bundleIdentifier = config.Identifer;

        var defines = "Assetbundle";
        defines += buildParams.isActiveSdk ? ";ActiveSDK" : "";
        defines += buildParams.debugErrorMode ? ";DebugErrorMode" : "";
        defines += ";LANG_" + buildParams.lang;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildParams.buildTargetGroup, defines);

        PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;
        PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_5_1;

        var xcodeName = Parse(config.PackageName);
        if (buildParams.debugErrorMode)
            xcodeName += "_Debug";
        else if (buildParams.developmentMode)
            xcodeName += "_Development";
#if UNITY_EDITOR_OSX
        config.OutputPath = "/Users/fps/Desktop/fps_ipa";
#endif
        var xcodeFullName = Path.Combine(config.OutputPath, xcodeName);

        Caching.CleanCache();
        DeleteSdk();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        AssetDatabase.SaveAssets();

        var buildStartTime = TimeUtil.GetNowTimeStamp();
        var buildOption = BuildOptions.None;
		if (buildParams.developmentMode)
			buildOption = BuildOptions.Development;
        if (buildParams.debugErrorMode)
            buildOption = BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
        string result = BuildPipeline.BuildPlayer(buildParams.debugErrorMode ? DebugLevels : levels, xcodeFullName, buildParams.buildTarget, buildOption);
        var dtTime = TimeUtil.GetPassTime(buildStartTime);

        if (string.IsNullOrEmpty(result))
            Debug.Log(string.Format("【{0}】Xcode生成成功\n耗时{1}分{2}秒", config.Name, dtTime.Minutes, dtTime.Seconds));
        else
            Debug.LogError(string.Format("【{0}】Xcode生成失败\n", config.Name));

        //编译xcode工程生成app，再导出ipa
        var ipaFullName = xcodeFullName + "/build/" + xcodeName + ".ipa";
        var ipaCopyFullName = Path.Combine(config.CopyPath, xcodeName);
        buildStartTime = TimeUtil.GetNowTimeStamp();
        PluginUtil.Excute("/bin/bash", string.Format("{0} {1} {2}", "Assets/Editor/AutoBuildIOS/shell/buildios.sh", xcodeFullName, xcodeName), false, false, true);
        dtTime = TimeUtil.GetPassTime(buildStartTime);
        if (File.Exists(ipaFullName))
        {
            Debug.Log(string.Format("【{0}】IPA生成成功\n{1}", config.Name, GetSummary(xcodeName, dtTime)));
            if (!File.Exists(ipaCopyFullName) || EditorUtility.DisplayDialog("提示", "【备份目录】有重名包，覆盖旧包还是丢弃新包？" + "\n" + ipaCopyFullName, "覆盖", "丢弃"))
                File.Copy(ipaFullName, ipaCopyFullName, true);
        }
        else
            Debug.LogError(string.Format("【{0}】IPA生成失败\n", config.Name));
    }

    private static ConfigBuildLine ReloadConfig(string key)
    {
        var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
        configFile.Load(PluginUtil.ReadAllTextWriteSafe("AutoBuild/ConfigBuild.csv"));
        var line = configFile.GetLine(key);
        Object.DestroyImmediate(configFile);
        return line;
    }

    private static void CopyAndroidSdk(string sdkName, string packageName)
    {
        var pluginDir ="Android";
        var from = string.Format("../sdk/Output/{0}/{1}", sdkName, pluginDir);
        var to = string.Format("Assets/Plugins/{0}", pluginDir);
        FileUtil.CopyFileOrDirectory(from, to);

        var pathManifest = to + "/AndroidManifest.xml";
        var manifest = File.ReadAllText(pathManifest);
        manifest = manifest.Replace("com.fps.sdk", packageName);
        File.WriteAllText(pathManifest, manifest);
    }

    private static void DeleteSdk()
    {
        AssetDatabase.DeleteAsset("Assets/Plugins/Android");
    }

    /// <summary>
    /// 解析动态字符串
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    private static string Parse(string content)
    {
        var ios = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iPhone;
        content = content.Replace("{Time}", GetTime());
        content = content.Replace("{Version}", ios ? PlayerSettings.shortBundleVersion : PlayerSettings.bundleVersion);
        return content;
    }

    private static string GetTime()
    {
        return DateTime.Now.ToString("yyMMddHHmm");
    }

    private static string GetSummary(string packageName, TimeSpan dtTime)
    {
        var ios = EditorUserBuildSettings.activeBuildTarget == BuildTarget.iPhone;
        var result = new StringBuilder();
        result.AppendFormat("{0}：{1}\n", "包文件名", packageName);
        result.AppendFormat("{0}：{1}\n", "APP名", PlayerSettings.productName);
        result.AppendFormat("{0}：{1}\n", "包名", PlayerSettings.bundleIdentifier);
        result.AppendFormat("{0}：{1}\n", "版本号", ios ? PlayerSettings.shortBundleVersion : PlayerSettings.bundleVersion);
        if(!ios) result.AppendFormat("{0}：{1}\n", "版本Code", PlayerSettings.Android.bundleVersionCode);
        result.AppendFormat("{0}：{1}分{2}秒", "打包耗时", dtTime.Minutes, dtTime.Seconds);
        
        return result.ToString();
    }

    public static bool BuildGameDefine(string key, BuildVersionType useVersionType, bool inner = false)
    {
        try
        {
            var result = new StringBuilder();
            var configFile = ScriptableObject.CreateInstance<ConfigBuild>();
            configFile.Load(PluginUtil.ReadAllTextWriteSafe("AutoBuild/ConfigBuild.csv"));
            var config = configFile.GetLine(key);

            //拼接包内版本信息
            var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(BuildTools.RESOURCE_ONLINE_PATH + BuildTools.GetUpdateDirName(config.UpdateUrl) + Path.DirectorySeparatorChar + "Version.txt"));
            result.AppendLine("Version=" + BuildTools.GetNextGameVersion(dic.GetValue("PrepareVersion"), useVersionType));
            if (config.Platform == BuildTarget.iPhone.ToString())
                result.AppendLine("BigVersion=" + BuildTools.GetNextBigGameVersion(dic.GetValue("PrepareBigVersion"), useVersionType));
            else
                result.AppendLine("BigVersion=" + dic.GetValue("BigVersion"));
            result.AppendLine("HotUpdateVersion=" + dic.GetValue("HotUpdateVersion"));

            //拼接相关选项
            var fields = typeof(ConfigBuildLine).GetFields();
            for (int i = 0; i < fields.Length; i++)
            {
                var name = fields[i].Name;
                var value = fields[i].GetValue(config).ToString();
                if (inner)
                    value = value.Replace("http://zjqz-cdnres.me4399.com", "file:////172.16.10.158/zjqz");
                result.AppendLine(name + "=" + value);
            }

            var path = PluginUtil.GetEditorStreamingAssetsPath() + "VersionInfo/GameDefine.txt";
            PluginUtil.CreateFileDirectory(path);
            File.WriteAllText(path, result.ToString(), new UTF8Encoding(false));
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            Debug.Log("创建GameDefine完毕\n\n"+result);
            Object.DestroyImmediate(configFile);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("创建GameDefine失败");
            Debug.LogException(e);
            return false;
        }
    }

    /// <summary>
    /// 混淆web库（因为disunity打包有问题所以不用）
    /// </summary>
    /// <param name="url"></param>
    /// <param name="name"></param>
    /// <param name="version"></param>
    public static void ObfusecarWeb(string url, string name ,string version)
    {
        //复制unity包到disunity目录中
        string srcPath = url + "/" + name + ".unity3d";
        string packPath = "AutoBuild/disunity/Pack/" + name + ".unity3d";
        string unpackPath = "AutoBuild/disunity/Unpack/" + name;
        string unpackJsonPath = unpackPath + ".json";
        string obfusescarPackPath = "AutoBuild/disunity/ObfuscarPack/" + name + ".unity3d";
        

        File.Copy(srcPath, packPath, true);

        string exeName = "java";
        string exeParam = "-jar \"AutoBuild/disunity/disunity.jar\" bundle unpack -p -o \"" + unpackPath + "\" \"" + packPath + "\"";
        var log = PluginUtil.Excute(exeName, exeParam, true, true);

        Debug.Log("ObfusecarWeb Unpack:\n" + log);

        string sourceDllPath = "AutoBuild/disunity/Unpack/" + name + "/Assembly-CSharp.dll";
        string obfusescarDllPath = "AutoBuild/obfuscar/Output/Client.dll.bytes";

        string[] dllPath = new string[] { sourceDllPath };
        string[] dllName = new string[] { "Client.dll.bytes" };

        BuildAB.ObfuscateDll(dllPath, dllName, version);

        File.Copy(obfusescarDllPath, sourceDllPath, true);

        exeParam = "-jar \"AutoBuild/disunity/disunity.jar\" bundle pack -o \"" + obfusescarPackPath + "\" \"" + unpackJsonPath + "\"";
        log = PluginUtil.Excute(exeName, exeParam, true, true);

        File.Copy(obfusescarPackPath, srcPath, true);

        Debug.Log("ObfusecarWeb Pack:" + log);

        Debug.Log("ObfusecarWeb Done");
    }
}