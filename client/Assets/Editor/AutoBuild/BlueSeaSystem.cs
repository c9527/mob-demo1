﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

static class BlueSeaSystem
{
    private const string URL_HOME_PAGE = "https://bss.gz4399.com/";
    private const string URL_LOGIN_PAGE = "https://bss.gz4399.com/login";
    private const string URL_UPDATE_ORDER = "https://bss.gz4399.com/api/update_order";
    private const string URL_GET_ORDER = "https://bss.gz4399.com/update_order";
    private const string URL_PERFORM_ORDER = "https://bss.gz4399.com/api/update_order_perform";
    private const string URL_ORDER_LOGS = "https://bss.gz4399.com/update_logs";
    private const string URL_GET_ORDER_LOG = "https://bss.gz4399.com/update_log/";
    private const string ORDER_TYPE = "前端CDN更新";
    private const string CONTENT_TYPE = "application/x-www-form-urlencoded; charset=UTF-8";
    private const string USERNAME = "gz1227";
    private const string PASSWORD = "12123456";

    private static HttpClient m_client = new HttpClient();

    public static void Reset()
    {
        m_client = new HttpClient();
    }

    private static void Login()
    {
        var xsrf = GetXsrf();
        if (string.IsNullOrEmpty(xsrf))
        {
            m_client.DownloadData(URL_HOME_PAGE);
            xsrf = GetXsrf();
        }

        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = m_client.UploadString(URL_LOGIN_PAGE, string.Format("{0}&gonghao={1}&passwd={2}", xsrf, USERNAME, PASSWORD));
        result = Regex.Unescape(result);

        Debug.Log(IsSuccess(result) ? "蓝海系统登录成功" : result);
    }

    public static void CreateUpdateOrder(ConfigBuildLine data,string updateParam, string desc, int tryCount = 1)
    {
        if (tryCount < 0)
            return;
       
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var nowTimeData = WWW.EscapeURL(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        var postData = string.Format("task_name={0}&{1}&project_id=102&start_time={2}&stype_id=132&update_stype=0&include_test=0&customize=&updatesteps=1&1_input={3}&queue_stype=version&starttime=&endtime&version={4}&updatequeues={5}--%3E%E5%85%A8%E9%83%A8%0D%0A&notice_stype=&comment=", WWW.EscapeURL(desc), GetXsrf(), nowTimeData, updateParam, data.targetCDN, data.targetCDN);
        var result = m_client.UploadString(URL_UPDATE_ORDER, postData);
        result = Regex.Unescape(result);
        Debug.Log(IsSuccess(result) ? "工单创建成功" : result);

        if (IsNeedLogin(result))
        {
            Login();
            CreateUpdateOrder(data,updateParam, desc, tryCount - 1);
        }
    }

    public static List<List<string>> GetOrders(int tryCount = 1)
    {
        if (tryCount < 0)
            return new List<List<string>>();
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = Encoding.UTF8.GetString(m_client.DownloadData(URL_GET_ORDER));

        if (IsNeedLogin(result))
        {
            Login();
            return GetOrders(tryCount - 1);
        }

        //未执行工单
        var orderList = new List<List<string>>();
        var regexTd = new Regex("<td class=\"center\">(.*)</td>", RegexOptions.Compiled);
        var regexPerformHref = new Regex("href=\".*?(\\d+)\" title=\"编辑\"", RegexOptions.Compiled);
        var match = Regex.Match(result, "<tbody>.*?</tbody>", RegexOptions.Singleline);
        if (match.Success)
        {
            match = Regex.Match(match.Value, "<tr>.*?</tr>", RegexOptions.Singleline);
            while (match.Success)
            {
                var m = regexTd.Match(match.Value);
                if (m.Success)
                    orderList.Add(new List<string> { "0" });
                while (m.Success)
                {
                    orderList[orderList.Count - 1].Add(m.Groups[1].Value);
                    m = m.NextMatch();
                }

                m = regexPerformHref.Match(match.Value);
                if (m.Success)
                    orderList[orderList.Count - 1].Add(m.Groups[1].Value);
                match = match.NextMatch();
            }
        }

        //正在执行中的工单
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        result = Encoding.UTF8.GetString(m_client.DownloadData(URL_ORDER_LOGS));
        var regexLogHref = new Regex("href=\".*?(\\d+)\" title=\"日志查看\"", RegexOptions.Compiled);
        match = Regex.Match(result, "<tbody>.*?</tbody>", RegexOptions.Singleline);
        if (match.Success)
        {
            match = Regex.Match(match.Value, "<tr>.*?</tr>", RegexOptions.Singleline);
            while (match.Success)
            {
                var m = regexTd.Match(match.Value);
                if (m.Success)
                    orderList.Add(new List<string> { "1" });
                while (m.Success)
                {
                    orderList[orderList.Count - 1].Add(m.Groups[1].Value);
                    m = m.NextMatch();
                }

                m = regexLogHref.Match(match.Value);
                if (m.Success)
                    orderList[orderList.Count - 1].Add(m.Groups[1].Value);
                match = match.NextMatch();
            }
        }
        

        //删除掉不用的
        for (int i = orderList.Count - 1; i >= 0; i--)
        {
            var isNotPreformOrder = orderList[i][0] == "0";
            if (!isNotPreformOrder)
            {
                if (orderList[i][6].IndexOf("zhengque", StringComparison.Ordinal) != -1)
                    orderList[i][0] = "2";
                if (orderList[i][6].IndexOf("cuowu", StringComparison.Ordinal) != -1)
                    orderList[i][0] = "3";
            }
            for (int j = orderList[i].Count - 1; j >= 0; j--)
            {
                if (isNotPreformOrder && (j != 0 && j != 2 && j != 3 && j != 5 && j != 6))
                    orderList[i].RemoveAt(j);
                else if (!isNotPreformOrder && (j != 0 && j != 2 && j != 3 && j != 5 && j != 8))
                    orderList[i].RemoveAt(j);
            }
            //去掉不关心的类型、不是正在更新的单
            if (orderList[i][1].IndexOf(ORDER_TYPE, System.StringComparison.Ordinal) == -1)
                orderList.RemoveAt(i);

            if (i < orderList.Count)
            {
                var m = Regex.Match(orderList[i][1], "【.*?】");
                m.NextMatch();
                if (m.Success)
                    orderList[i][1] = m.Value;
            }
        }

        
        return orderList;
    }

    public static string GetOrderParams(string id)
    {
        var regexParam = new Regex("\"1_input\".*?value=\"(.*)?\"");
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = Encoding.UTF8.GetString(m_client.DownloadData(URL_GET_ORDER + "/" + id));
        var m = regexParam.Match(result);
        if (m.Success)
            return m.Groups[1].Value;
        return "";
    }

    public static void PerformOrder(string id)
    {
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        var result = m_client.UploadString(URL_PERFORM_ORDER, "id=" + id);
        result = Regex.Unescape(result);

        Debug.Log(IsSuccess(result) ? string.Format("[{0}]工单开始更新", id) : result);
    }

    public static void CancelOrder(string id)
    {
        m_client.Headers[HttpRequestHeader.ContentType] = CONTENT_TYPE;
        try
        {
            var result = m_client.UploadString(URL_PERFORM_ORDER + "/" + id, "PUT", "id=" + id);
            result = Regex.Unescape(result);
            Debug.Log(IsSuccess(result) ? string.Format("[{0}]工单取消", id) : result);
        }
        catch (WebException)
        {
            Debug.Log(string.Format("[{0}]该工单未在执行中，无法取消", id));
        }
    }

    private static bool IsNeedLogin(string result)
    {
        return result.IndexOf("登录已过期", StringComparison.Ordinal) != -1 || result.IndexOf("登录-蓝海系统", StringComparison.Ordinal) != -1;
    }

    private static bool IsSuccess(string result)
    {
        return result.IndexOf("\"error\": 0", StringComparison.Ordinal) != -1;
    }

    private static string GetXsrf()
    {
        var xsrf = m_client.CookieContainer.GetCookies(new Uri(URL_HOME_PAGE))["_xsrf"];
        return xsrf != null ? xsrf.ToString() : "";
    }
}