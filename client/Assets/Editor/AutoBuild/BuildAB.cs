﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using TreeEditor;
using UnityEditor;
using UnityEditor.Graphs;
using UnityEditor.Sprites;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;
using System.Security.Cryptography;

public static class BuildAB
{
    private static Regex m_regGuid = new Regex(@"guid: ([\w\d]*)");
    private static string[] m_leafAssetExt = new[] { ".fbx", ".wav", ".anim", ".shader", ".png", ".jpg", ".gif", ".tga", ".exr", ".ttf", ".mask", ".rendertexture" };
    //private static List<BundleNode> m_bundleGraphic = new List<BundleNode>();
    private static Dictionary<string, AssetNode> m_fileDic = new Dictionary<string, AssetNode>();//资源路径、资源
    private static Dictionary<string, BundleNode> m_bundleDic = new Dictionary<string, BundleNode>(); //bundle路径、bundle
    private static Dictionary<string, List<string>> m_unBundleDic = new Dictionary<string, List<string>>();//资源路径、bundle路径列表
    private static Dictionary<int, List<BundleNode>> m_bundleGroupDic = new Dictionary<int, List<BundleNode>>();//分组id、bundle列表
    private static Dictionary<string, string> m_bundleInfo = new Dictionary<string, string>(); //资源路径、bundle路径
    private static Dictionary<string, HashSet<string>> m_lastDependDic; //上一次的依赖关系字典
    /// <summary>
    /// 检查prefab命名的唯一性
    /// </summary>
    public static bool CheckPrefabNameUnique()
    {
        var result = true;        
        var bundleNameDic = new Dictionary<string, string>();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            var name = Path.GetFileName(bundleNode.AssetPath);
            if (name == null)
            {
                Debug.LogError(bundleNode.AssetPath);
                result = false;
                continue;
            }

            name = name.ToLower();
            if (name.IndexOf(' ') != -1)
            {
                Debug.LogError("Bundle名中有空格：\n" + bundleNode.AssetPath);
                result = false;
            }
            if (!bundleNameDic.ContainsKey(name))
                bundleNameDic.Add(name, bundleNode.AssetPath);
            else
            {
                Debug.LogError("Bundle名有重复：\n" + bundleNode.AssetPath + "\n" + bundleNameDic[name]);
                result = false;
            }
        }
        return result;
    }

    /// <summary>
    /// 独立打包所选资源
    /// </summary>
    public static void BuildSelectGameObjectsIndependent()
    {
        if (!CanRun())
            return;

        var objs = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
        for (int i = 0; i < objs.Length; i++)
        {
            var path = AssetDatabase.GetAssetPath(objs[i]);
            BuildAssetBundle(objs[i], PluginUtil.GetUnifyPath(path));
        }
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
    }

    /// <summary>
    /// 依赖打包所选资源
    /// </summary>
    public static void BuildSelect()
    {
        if (!CanRun())
            return;

        var pathList = new List<string>();
        var objs = Selection.objects;
        for (int i = 0; i < objs.Length; i++)
        {
            var path = AssetDatabase.GetAssetPath(objs[i]);
            if (!Path.HasExtension(path))
                pathList.AddRange(PluginUtil.GetFiles(path));
            else
                pathList.Add(path);
        }

        if (pathList.Count > 0)
        {
            var diffFileList = new StringBuilder();
            for (int i = 0; i < pathList.Count; i++)
            {
                diffFileList.AppendLine(pathList[i]);
            }
            BuildResources(diffFileList.ToString());
        }
    }

    /// <summary>
    /// 查找直接引用文件
    /// </summary>
    private static string[] FindDirectDenpendencies(string path)
    {
        string[] paths;
        var hash = new HashSet<string>();
        if (!Path.HasExtension(path))
        {
            if (!m_bundleDic.ContainsKey(path))
                Debug.Log(path);
            var bundleNode = m_bundleDic[path];
            var list = new List<string>();
            for (int i = 0; i < bundleNode.IncludeAssets.Count; i++)
            {
                AssetNode assetNode = bundleNode.IncludeAssets[i];
                list.Add(assetNode.FilePath);
            }
            paths = list.ToArray();
        }
        else
            paths = new[] {path};
        for (int i = 0; i < paths.Length; i++)
        {
            var subPath = paths[i];
            if (m_leafAssetExt.Contains(Path.GetExtension(subPath).ToLower()))
                continue;
            var prefab = File.ReadAllText(subPath);
            var match = m_regGuid.Match(prefab);
            while (match.Success)
            {
                var guid = match.Groups[1].ToString();
                var resPath = AssetDatabase.GUIDToAssetPath(guid);
                if (!string.IsNullOrEmpty(resPath) && !resPath.EndsWith(".dll") && !resPath.EndsWith(".cs") && resPath != subPath)
                {
                    if (resPath == "Library/unity default resources")
                    {
                        //Debug.LogWarning("引用了系统默认资源 guid:" + guid + "\n" + subPath);
                    }
                    else if (resPath == "Resources/unity_builtin_extra")
                    {
                        //Debug.LogWarning("引用了unity内置额外资源 guid:" + guid + "\n" + subPath);
                    }
                    else
                        hash.Add(resPath);
                }
                match = match.NextMatch();
            }
        }
        return hash.ToArray();
    }

    public static bool TestBuildResources()
    {
        return BuildResources();
    }

    /// <summary>
    /// 打包资源AB
    /// </summary>
    /// <param name="diffFileList">变更文件列表，若为null则不使用，注意区别空字符串表示没有变更</param>
    /// <returns></returns>
    public static bool BuildResources(string diffFileList = null, bool testMode = false)
    {
        var time = TimeUtil.GetNowTimeStamp();
        //m_bundleGraphic.Clear();
        m_fileDic.Clear();
        m_bundleDic.Clear();
        m_unBundleDic.Clear();
        m_bundleGroupDic.Clear();
        m_bundleInfo.Clear();
        Caching.CleanCache();
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

        //把资源文件划归到对应Bundle
        BuildAssetDic("Assets/Resources/LuaRoot/", true, null, true);
        BuildAssetDic("Assets/Resources/Music/", true, typeof(AudioClip));
        BuildAssetDic("Assets/Resources/Sounds/", true, typeof(AudioClip));
        BuildAssetDic("Assets/Resources/Font/", false, typeof(Font));
        BuildAssetDic("Assets/Resources/Effect/", false, null);
        BuildAssetDic("Assets/Resources/CameraAni/", false, typeof(AnimationClip));
        BuildAssetDic("Assets/Resources/Text/", false, null);
        BuildAssetDic("Assets/Resources/Shader/", true, typeof(Shader));
        BuildAssetDic("Assets/Resources/Materials/", false, typeof(Material));
        BuildAssetDic("Assets/Resources/Textures/", false, typeof(Texture2D));
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/Atlas/", typeof(Sprite));
#if UNITY_ANDROID
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/AtlasAlpha/Android/", typeof(Texture2D));
#elif UNITY_IPHONE
        BuildSecondaryCombineAssetDic("Assets/ResourcesLib/AtlasAlpha/iPhone/", typeof(Texture2D));
#endif
        BuildAssetDic("Assets/Resources/UI/", true, null);
//        BuildAssetDic("Assets/Resources/UI/PanelUpdate.prefab", false, null);
//      BuildAssetDic("Assets/Resources/Roles/Mask/", false, typeof(AvatarMask), true);   //AvatarMask只能包含到AnimatorController里，无法单独打成包
        BuildRoleAnimationAssetDic("Assets/Resources/Roles/Animation/");
        BuildAssetDic("Assets/Resources/Roles/AnimatorController/", false, typeof(AnimatorController));
        BuildAssetDic("Assets/Resources/Roles/Fbx/", false, null, false, true);
        BuildAssetDic("Assets/Resources/Roles/", false, null);
        BuildAssetDic("Assets/Resources/Roles/Textures", false, typeof(Texture2D));
        BuildAssetDic("Assets/Resources/Weapons/WeaponsAnimation/", false, typeof(AnimationClip));
        BuildAssetDic("Assets/Resources/Weapons/Fbx/", false, null, false, true);
        BuildAssetDic("Assets/Resources/Weapons/", false, null);
        BuildAssetDic("Assets/Resources/Weapons/Textures/", false, typeof(Texture2D));
        BuildAssetDic("Assets/Resources/Scenes/", true, null);
        BuildMapAssetDic("Assets/ResourcesLib/Scenes/");
        BuildAssetDic("Assets/Resources/SceneItem/", false, null);
        BuildAssetDic("Assets/Resources/SceneItem/Texture/", false, typeof(Texture2D));
        BuildAssetDic("Assets/Resources/SceneItem/Fbx/Materials/", false, typeof(Material));
        BuildAssetDic("Assets/Resources/Roles/Decoration/", false, null, false, false);
        //检查Bundle包名唯一性
        if (!CheckPrefabNameUnique())
            return false;

        //构建Bundle之间的依赖图
        var max = m_bundleDic.Count;
        var count = 0f;
        foreach (var bundleNode in m_bundleDic.Values)
        {
            count++;
            EditorUtility.DisplayProgressBar("构建Bundle间依赖关系",bundleNode.AssetPath, count/max);
            BuildBundleGraphic(bundleNode);
        }
        EditorUtility.ClearProgressBar();

        //构建Bundle分组，有相互引用关系的划分为同一组
        var groupId = 1;
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (BuildGroup(bundleNode, groupId))
                groupId++;
        }

        //对每个分组内从root节点分层
        foreach (var list in m_bundleGroupDic.Values)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var bundleNode = list[i];
                if (bundleNode.IsRoot)
                    BuildLayer(bundleNode, 1);
            }
        }

        //加载本地的依赖文件，也就是上一次的依赖关系
        SvnOperation.ExeSvnRevert(PluginUtil.GetEditorStreamingAssetsLibPath()+"Depend.assetbundle", true);
        m_lastDependDic = GetDepend();

        //输出Bundle间依赖关系
        ExportBundleDepend(testMode);

        //输出Bundle间依赖分组，和组内的层级
        ExportBundleGroupLayer();

        //根据文件变更情况来设置需要重新打包的Bundle
        CheckChangeBundle(diffFileList, testMode);

        //安卓平台下对etc格式图集进行重新通道分离
        if (!TryGenerateEtcTex())
            return false;

        //按文件变更情况打包
        if (BuildByLayer(testMode))
        {
            //打包成功后保存新的版本文件
            var dtTime = TimeUtil.GetPassTime(time);
            Debug.Log(string.Format("资源打包用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));
        }
        else
        {
            //打包有报错则提示还原
            return false;
        }

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        return true;
    }

    private static bool TryGenerateEtcTex()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        var list = new List<string>();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (bundleNode.IsEtcAtlas)
                list.Add(bundleNode.AssetPath);
        }

        if (list.Count == 0)
            return true;

        EditorUtility.DisplayProgressBar("构建图集中...", "", 0.5f);
        Packer.RebuildAtlasCacheIfNeeded(GetBuildTarget(), true, Packer.Execution.Normal);

        var time = TimeUtil.GetNowTimeStamp();
        for (int i = 0; i < list.Count; i++)
        {
            EditorUtility.DisplayProgressBar("分离图集透明通道", list[i], i * 1f / list.Count);
            Debug.Log("【图集分离通道】" + list[i]);
            if (!AtlasBuild.GenerateEtcTex(list[i]))
                return false;
        }
        
        var dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("分离图集Alpha通道用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));

        time = TimeUtil.GetNowTimeStamp();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("图集Alpha通道导入用时：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));

        EditorUtility.ClearProgressBar();   //向后放一点，它会触发系统的资源检查导入函数，让上面的Refresh
        time = TimeUtil.GetNowTimeStamp();
        for (int i = 0; i < list.Count; i++)
        {
            var dirSplit = list[i].Split('/');
            var dirName = dirSplit[dirSplit.Length - 1];
            var filePaths = PluginUtil.GetFiles(string.Format("Assets/ResourcesLib/AtlasAlpha/{0}/{1}_a", GetBuildTarget(), dirName));
            var buildList = new List<Object>();
            for (int j = 0; j < filePaths.Length; j++)
            {
                buildList.Add(AssetDatabase.LoadAssetAtPath(filePaths[j], typeof(Texture2D)));
            }
            if (buildList.Count > 0)
                BuildAssetBundle(buildList.ToArray(), string.Format("AtlasAlpha/{0}_a", dirName));
        }
        dtTime = TimeUtil.GetPassTime(time);
        Debug.Log(string.Format("图集Alpha通道打包：{0}分{1}秒", dtTime.Minutes, dtTime.Seconds));
#endif
        return true;
    }

    private static Dictionary<string, HashSet<string>> GetDepend()
    {
        var dic = new Dictionary<string, HashSet<string>>();
        var ab = PluginUtil.LoadAssetBundle(PluginUtil.GetEditorStreamingAssetsLibPath() + "Depend.assetbundle");
        var dependLines = ab.mainAsset.ToString().Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < dependLines.Length; i++)
        {
            var arr = dependLines[i].Split(':');
            var depends = arr[1].Split('|');
            dic.Add(arr[0], new HashSet<string>(depends));
        }

        ab.Unload(false);
        return dic;
    }

    private static void CheckChangeBundle(string diffFileList, bool testMode)
    {
        if (diffFileList != null)
        {
            //文件增、改导致Bundle变化，若Bundle内只有文件删除且没全部删除，则不会重新打
            var fileArr = diffFileList.Split(new char[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fileArr.Length; i++)
            {
                if (fileArr[i].IndexOf("Assets/", StringComparison.Ordinal) == -1)
                    continue;
                var path = PluginUtil.GetProjectPath(fileArr[i], false);

                if (m_bundleInfo.ContainsKey(path)) //已经划分到包的，加上标记(将其本身及所依赖的)
                {
                    TrySetChangeTag(m_bundleInfo[path]);

                    //处理因为Bundle名称变化的情况，这种情况需要额外的把依赖它的也重新打，因为名称是查找依赖的根据
                    var fileBundlePath = PluginUtil.GetUnifyPath(m_bundleInfo[path]);
                    var usagesNodes = m_bundleDic[m_fileDic[path].BundlePath].UsagesBundles;
                    if (usagesNodes == null)
                        continue;
                    for (int j = 0; j < usagesNodes.Count; j++)
                    {
                        var bundlePath = PluginUtil.GetUnifyPath(usagesNodes[j].AssetPath);
                        var hasDepend = m_lastDependDic.ContainsKey(bundlePath) && m_lastDependDic[bundlePath].Contains(fileBundlePath);
                        if (!hasDepend)
                            TrySetChangeTag(usagesNodes[j].AssetPath, true);
                    }
                }
                else if (m_unBundleDic.ContainsKey(path)) //若是没有划分包的文件，则将所有依赖它的包加上标记(将其本身及所依赖的)
                {
                    for (int j = 0; j < m_unBundleDic[path].Count; j++)
                    {
                        TrySetChangeTag(m_unBundleDic[path][j]);
                    }
                }
            }

            //文件删导致Bundle删除
            var result = new StringBuilder();
            fileArr = PluginUtil.GetFiles(PluginUtil.GetEditorStreamingAssetsLibPath());
            var bundleUnifyPathDic = new Dictionary<string, bool>();
            foreach (var bundleNode in m_bundleDic)
            {
                bundleUnifyPathDic.Add(PluginUtil.GetUnifyPath(bundleNode.Key), true);
            }
            for (int i = 0; i < fileArr.Length; i++)
            {
                var path = PluginUtil.GetUnifyPath(fileArr[i]);
                path = PluginUtil.ChangeAtlasAlphaPathWithPlatform(path);
                if (path.StartsWith("Managed/") || path.StartsWith("VersionInfo/") || path == "Config" || path == "Depend" || path == "UpdateLog") //跳过
                    continue;
                if (!bundleUnifyPathDic.ContainsKey(path))
                {
                    if (!testMode)
                        File.Delete(fileArr[i]);
                    result.AppendLine("【删除】" + fileArr[i]);

#if UNITY_ANDROID || UNITY_IPHONE
                    var pathAltasAlpha = UIHelper.TryGetAtlasAlphaPath(fileArr[i]);
                    if (!string.IsNullOrEmpty(pathAltasAlpha))
                    {
                        var libDir = "Assets/ResourcesLib/" + UIHelper.TryGetAtlasAlphaPath(path);
                        if (!testMode)
                        {
                            FileUtil.DeleteFileOrDirectory(libDir);
                            FileUtil.DeleteFileOrDirectory(libDir+".meta");
                            File.Delete(pathAltasAlpha);
                        }
                        result.AppendLine("【删除】" + pathAltasAlpha);
                        result.AppendLine("【删除】" + libDir);
                    }
#endif
                }
            }
            if (result.Length > 0)
                Debug.Log(result);
        }
        else
        {
            foreach (var bundleNode in m_bundleDic.Values)
            {
                bundleNode.Changed = true;
            }
        }
    }

    /// <summary>
    /// 设置依赖、变化标记
    /// </summary>
    /// <param name="bundlePath"></param>
    /// <param name="isPassivityChanged">是否是被动改变：所依赖的包更名造成的变更</param>
    /// <returns></returns>
    private static bool TrySetChangeTag(string bundlePath, bool isPassivityChanged = false)
    {
        if (bundlePath != null)
        {
            m_bundleDic[bundlePath].Changed = true;
            m_bundleDic[bundlePath].IsPassivityChanged = isPassivityChanged;

            var dependList = new List<string>();
            GetAllDepend(bundlePath, dependList);
            for (int j = 0; j < dependList.Count; j++)
            {
                m_bundleDic[dependList[j]].BeDepend = true;
            }

#if UNITY_ANDROID || UNITY_IPHONE
            var pathAtlasAlpha = UIHelper.TryGetAtlasAlphaPath(bundlePath);
            if (!string.IsNullOrEmpty(pathAtlasAlpha))
            {
                m_bundleDic[bundlePath].IsEtcAtlas = true;
                if (m_bundleDic.ContainsKey(pathAtlasAlpha))
                    m_bundleDic[pathAtlasAlpha].IsEtcAlphaAtlas = true;
            }
#endif
            return true;
        }
        return false;
    }

    private static void GetAllDepend(string path, List<string> dependList)
    {
        if (!m_bundleDic.ContainsKey(path) || m_bundleDic[path].IsLeaf)
            return;

        var bundleNode = m_bundleDic[path];
        foreach (var dependNode in bundleNode.DependBundles)
        {
            dependList.Add(dependNode.AssetPath);
            GetAllDepend(dependNode.AssetPath, dependList);
        }
    }

    private static bool BuildByLayer(bool testMode)
    {
        var result = true;
        var resultChanged = new StringBuilder();
        var resultDepend = new StringBuilder();
        foreach (var list in m_bundleGroupDic.Values)
        {
            list.Sort((a,b)=>b.Layer-a.Layer);
            var maxLayer = list[0].Layer;
            var curLayer = maxLayer;
            if (maxLayer > 1)
                BuildPipeline.PushAssetDependencies();
            
            for (int i = 0; i < list.Count; i++)
            {
                var bundleNode = list[i];
                if (bundleNode.Layer < curLayer)
                {
                    BuildPipeline.PushAssetDependencies();
                    curLayer = bundleNode.Layer;
                }
                if (!bundleNode.NeedRebuild || bundleNode.IsEtcAlphaAtlas)
                    continue;
                if (!bundleNode.IsScene)
                {
                    var assets = new List<Object>();
                    for (int j = 0; j < bundleNode.IncludeAssets.Count; j++)
                    {
                        var includeAsset = bundleNode.IncludeAssets[j];
                        if (includeAsset.AssetType == null)
                            assets.Add(AssetDatabase.LoadMainAssetAtPath(includeAsset.FilePath));
                        else
                            assets.Add(AssetDatabase.LoadAssetAtPath(includeAsset.FilePath, includeAsset.AssetType));
                    }
                    var outputPath = PluginUtil.GetUnifyPath(bundleNode.AssetPath);
#if UNITY_IPHONE
                    if (outputPath.IndexOf("/iPhone/") != -1)
                        outputPath = outputPath.Replace("/iPhone/", "/");
#endif
#if UNITY_ANDROID
                    if (outputPath.IndexOf("/Android/") != -1)
                        outputPath = outputPath.Replace("/Android/", "/");
#endif
                    if (bundleNode.Changed)
                    {
                        if (!bundleNode.IsPassivityChanged)
                            resultChanged.AppendLine("【变更】" + outputPath);
                        else
                            resultChanged.AppendLine("【被动变更】" + outputPath);
                    }
                    if (bundleNode.BeDepend)
                        resultDepend.AppendLine("【依赖】" + outputPath);
                    result &= BuildAssetBundle(assets.ToArray(), outputPath, curLayer == 1, testMode, !bundleNode.Changed);
                }
                else
                {
                    var paths = new List<string>();
                    for (int j = 0; j < bundleNode.IncludeAssets.Count; j++)
                    {
                        var includeAsset = bundleNode.IncludeAssets[j];
                        paths.Add(includeAsset.FilePath);
                    }
                    var outputPath = PluginUtil.GetUnifyPath(bundleNode.AssetPath);
                    if (bundleNode.Changed)
                    {
                        if (!bundleNode.IsPassivityChanged)
                            resultChanged.AppendLine("【场景变更】" + outputPath);
                        else
                            resultChanged.AppendLine("【被动场景变更】" + outputPath);
                    }
                    if (bundleNode.BeDepend)
                        resultDepend.AppendLine("【场景依赖】" + outputPath);
                    result &= BuildSceneAssetBundle(paths.ToArray(), outputPath, testMode);
                }

                if (!result)
                {
                    EditorUtility.DisplayDialog("打包中止", "", "确定");
                    return false;
                }
            }
            if (maxLayer > 1)
            {
                for (int i = 0; i < maxLayer; i++)
                {
                    BuildPipeline.PopAssetDependencies();
                }
            }    
            
        }
        if (resultDepend.Length > 0)
            Debug.Log(resultDepend);
        if (resultChanged.Length > 0)
            Debug.Log(resultChanged);

        return result;
    }

    /// <summary>
    /// 把资源文件划归到对应Bundle
    /// </summary>
    /// <param name="path"></param>
    /// <param name="recursive"></param>
    /// <param name="type"></param>
    /// <param name="combine"></param>
    /// <param name="uniqueName"></param>
    private static void BuildAssetDic(string path, bool recursive, Type type, bool combine = false, bool uniqueName = false)
    {
        var isFile = Path.HasExtension(path);
        var pathArr = !isFile ? PluginUtil.GetFiles(path, recursive) : new[] { path };
        for (int i = 0; i < pathArr.Length; i++)
        {
            var assetNode = new AssetNode();
            assetNode.FilePath = pathArr[i];
            assetNode.AssetType = type;
            var dotIndex = pathArr[i].LastIndexOf('.');
            assetNode.BundlePath = !combine ? pathArr[i].Substring(0, dotIndex > -1 ? dotIndex : pathArr[i].Length) : path.TrimEnd(new[] { '/', '\\' });
            if (uniqueName)
            {
                var suffix = Path.GetDirectoryName(PluginUtil.GetUnifyPath(!combine ? pathArr[i] : path)).Replace('/', '_');
                assetNode.BundlePath += "+" + suffix;
            }
            m_bundleInfo[assetNode.FilePath] = assetNode.BundlePath;
            m_fileDic[assetNode.FilePath] = assetNode;

            if (!m_bundleDic.ContainsKey(assetNode.BundlePath))
                m_bundleDic[assetNode.BundlePath] = new BundleNode();

            var bundleNode = m_bundleDic[assetNode.BundlePath];
            bundleNode.AssetPath = assetNode.BundlePath;
            if (bundleNode.IncludeAssets == null)
                bundleNode.IncludeAssets = new List<AssetNode>();
            if (!bundleNode.IncludeAssets.Contains(assetNode))
                bundleNode.IncludeAssets.Add(assetNode);
            if (pathArr[i].EndsWith(".unity"))
                bundleNode.IsScene = true;
        }
    }

    /// <summary>
    /// 对目录里的每个子目录进行合并打包
    /// </summary>
    /// <param name="dirPath"></param>
    /// <param name="type"></param>
    private static void BuildSecondaryCombineAssetDic(string dirPath, Type type)
    {
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }
        BuildAssetDic(dirPath, false, type, true);

        var dirPathArr = Directory.GetDirectories(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            BuildAssetDic(dirPathArr[i], false, type, true);
        }
    }

    private static void BuildRoleAnimationAssetDic(string dirPath)
    {
        var type = typeof (AnimationClip);
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }
        BuildAssetDic(dirPath, false, type, true);

        var dirPathArr = Directory.GetDirectories(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            var subDirPathArr = PluginUtil.GetDirectory(dirPathArr[i], false);
            for (int j = 0; j < subDirPathArr.Length; j++)
            {
                BuildAssetDic(subDirPathArr[j], true, type, true, true);
            }
        }
    }

    private static void BuildMapAssetDic(string dirPath)
    {
        if (Path.HasExtension(dirPath))
        {
            Debug.LogError("参数dirPath不是目录：" + dirPath);
            return;
        }

        var dirPathArr = PluginUtil.GetDirectory(dirPath);
        for (int i = 0; i < dirPathArr.Length; i++)
        {
            var subDirPathArr = PluginUtil.GetDirectory(dirPathArr[i]);
            for (int j = 0; j < subDirPathArr.Length; j++)
            {
                var combine = false;
                var directoryName = Path.GetFileName(subDirPathArr[j]);
                if (directoryName.Equals("Prefab", StringComparison.CurrentCultureIgnoreCase))
                    continue;
                if (directoryName.Equals("FBX", StringComparison.CurrentCultureIgnoreCase))
                    combine = true;
                if (directoryName.Equals("Materials", StringComparison.CurrentCultureIgnoreCase))
                    combine = true;
                BuildAssetDic(subDirPathArr[j], false, null, combine, true);
            }
            
        }
    }

    /// <summary>
    /// 构建Bundle之间的依赖图
    /// </summary>
    /// <param name="bundleNode"></param>
    private static void BuildBundleGraphic(BundleNode bundleNode)
    {
        var files = FindDirectDenpendencies(bundleNode.AssetPath);
        var bundleFile = new List<string>();
        FindDirectBundleDenpendencies(files, bundleFile);
        files = bundleFile.ToArray();
        for (int i = 0; i < files.Length; i++)
        {
            if (!m_fileDic.ContainsKey(files[i]))
            {
                if (!m_unBundleDic.ContainsKey(files[i]))
                    m_unBundleDic.Add(files[i], new List<string>());
                m_unBundleDic[files[i]].Add(bundleNode.AssetPath);
                //var arr = FindDirectDenpendencies(files[i]);
                continue;
            }
            var dependBundleNode = m_bundleDic[m_fileDic[files[i]].BundlePath];

            if (bundleNode.DependBundles == null)
                bundleNode.DependBundles = new List<BundleNode>();
            if (!bundleNode.DependBundles.Contains(dependBundleNode))
                bundleNode.DependBundles.Add(dependBundleNode);

            if (dependBundleNode.UsagesBundles == null)
                dependBundleNode.UsagesBundles = new List<BundleNode>();
            if (!dependBundleNode.UsagesBundles.Contains(bundleNode))
                dependBundleNode.UsagesBundles.Add(bundleNode);
        }
    }

    /// <summary>
    /// 根据文件列表，查找依赖的文件列表
    /// </summary>
    /// <param name="files"></param>
    /// <param name="bundleFile"></param>
    private static void FindDirectBundleDenpendencies(string[] files, List<string> bundleFile)
    {
        for (int i = 0; i < files.Length; i++)
        {
            if (!bundleFile.Contains(files[i]))
                bundleFile.Add(files[i]);
            if (!m_fileDic.ContainsKey(files[i]))
            {
                var arr = FindDirectDenpendencies(files[i]);
                FindDirectBundleDenpendencies(arr, bundleFile);
            }
        }
    }

    private static bool BuildGroup(BundleNode bundleNode, int groupId)
    {
        if (bundleNode.Group != 0)
            return false;

        bundleNode.Group = groupId;
        if (!m_bundleGroupDic.ContainsKey(groupId))
            m_bundleGroupDic.Add(groupId, new List<BundleNode>());
        m_bundleGroupDic[groupId].Add(bundleNode);

        if (!bundleNode.IsLeaf)
            for (int i = 0; i < bundleNode.DependBundles.Count; i++)
            {
                var dependBundle = bundleNode.DependBundles[i];
                BuildGroup(dependBundle, groupId);
            }

        if (!bundleNode.IsRoot)
            for (int i = 0; i < bundleNode.UsagesBundles.Count; i++)
            {
                var usagesBundle = bundleNode.UsagesBundles[i];
                BuildGroup(usagesBundle, groupId);
            }
        return true;
    }

    private static void BuildLayer(BundleNode bundleNode, int layer)
    {
        if (layer > bundleNode.Layer)
            bundleNode.Layer = layer;
        if (bundleNode.IsLeaf)
            return;
        
        foreach (var dependBundle in bundleNode.DependBundles)
        {
            BuildLayer(dependBundle, layer+1);
        }
    }

    /// <summary>
    /// 打包配置文件
    /// </summary>
    /// <param name="testMode">测试模式</param>
    /// <param name="delAssets">是否删除临时生成的Asset对象</param>
    public static bool BuildConfigs(bool testMode, bool delAssets = true, bool useCrypted = false)
    {
        if (!CanRun())
            return false;

        var assetsList = new List<Object>();
        //var configList = Resources.LoadAll("Config/");        
        var configList = useCrypted ? TempConfig.GetTempCfgs() : Resources.LoadAll("Config/");
        for (int i = 0; i < configList.Length; i++)
        {
            var configName = configList[i].name;
            
            if (!configName.StartsWith("Config"))
                continue;
            
            assetsList.Add(configList[i]);
        }
        var result = BuildAssetBundle(assetsList.ToArray(), "Config", false, testMode);
        if (result)
            Debug.Log("打包配置成功");
        else
            Debug.LogError("打包配置失败");
        TempConfig.Clear();
        return result;
    }

    public static bool BuildDll(string projectName, string dllName, string buildTarget, string[] defineConstants = null, string[] exceptCodePaths = null, bool testMode = false)
    {
        if (!CanRun())
            return false;

        var name = Path.GetFileNameWithoutExtension(projectName);
        var projNew = name + "-new.csproj";
        var content = File.ReadAllText(projectName);

        content = content.Replace("ENABLE_PROFILER", "");

        var platformArr = new[] { "UNITY_EDITOR", "UNITY_EDITOR_WIN", "UNITY_ANDROID", "UNITY_IPHONE", "UNITY_STANDALONE", "UNITY_STANDALONE_WIN" };
        var platformDefines = new [] { "UNITY_EDITOR", "UNITY_EDITOR_WIN" };
        if (buildTarget == BuildTarget.Android.ToString())
            platformDefines = new[] { "UNITY_ANDROID" };
        else if (buildTarget == BuildTarget.iPhone.ToString())
            platformDefines = new[] { "UNITY_IPHONE", "UNITY_IOS" };
        else if (buildTarget == BuildTarget.StandaloneWindows.ToString())
            platformDefines = new[] { "UNITY_STANDALONE", "UNITY_STANDALONE_WIN" };
        else if (buildTarget == BuildTarget.WebPlayer.ToString())
            platformDefines = new[] { "UNITY_WEBPLAYER" };

        var regex = new Regex("(<DefineConstants>)(.*?)(</DefineConstants>)");
        content = regex.Replace(content, match =>
        {
            var definesString = match.Groups[2].Value;
            for (int i = 0; i < platformArr.Length; i++)
            {
                definesString = definesString.Replace(platformArr[i], "");
            }
            var arr = definesString.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            arr = arr.ArrayConcat(platformDefines);
            if (defineConstants != null)
                arr = arr.ArrayConcat(defineConstants);

            return match.Groups[1].Value + string.Join(";", arr) + match.Groups[3].Value;

        });

        if (exceptCodePaths != null)
        {
            for (int i = 0; i < exceptCodePaths.Length; i++)
            {
                content = content.Replace(string.Format("    <Compile Include=\"{0}\" />\r\n", exceptCodePaths[i]), "");
            }
        }
        var dllPath = Regex.Match(content, @"<OutputPath>(.+?)</OutputPath>").Groups[1].Value + Regex.Match(content, @"<AssemblyName>(.+?)</AssemblyName>").Groups[1].Value + ".dll";

        if (File.Exists(dllPath))
            File.Delete(dllPath);

        File.WriteAllText(projNew, content);

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        var exeName = Environment.GetEnvironmentVariable("WinDir") + @"\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe";
        var log = PluginUtil.Excute(exeName, projNew+" /t:rebuild /p:Optimize=true", true, true);

        var destDir = PluginUtil.GetEditorStreamingAssetsLibPath() + "Managed/";
        Directory.CreateDirectory(destDir);
        
        var result = File.Exists(dllPath);
        if (result)
        {
            //if (compress)
            //{
            //    var projectTargetPath = "Assets/Editor/" + dllName;
            //    File.Copy(dllPath, projectTargetPath, true);
            //    AssetDatabase.Refresh();
            //    if (BuildAssetBundle(AssetDatabase.LoadMainAssetAtPath(projectTargetPath), "Managed/"+dllName, false, testMode, ""))
            //        Debug.Log(string.Format("生成、压缩DLL完毕\t{0}", dllName));
            //    else
            //        Debug.LogError(string.Format("\t压缩DLL失败\t{0}", dllName));
            //    if (File.Exists(projectTargetPath))
            //        AssetDatabase.DeleteAsset(projectTargetPath);
            //}
            //else
            //{
            //    if (!testMode)
            //        File.Copy(dllPath, Path.Combine(destDir, dllName), true);
            //    Debug.Log(string.Format("生成DLL完毕\t{0}", dllName));
            //}
            if (!testMode)
                File.Copy(dllPath, Path.Combine(destDir, dllName), true);
            Debug.Log(string.Format("生成DLL完毕\t{0}", dllName));
        }
        else
        {
            var startIndex = Mathf.Max(0, log.Length - 10000);
            var length = Mathf.Min(log.Length, startIndex + 10000);
            Debug.LogError("生成DLL失败\t" + dllName + "\n" + log.Substring(startIndex, length - startIndex));
        }

        //File.Delete(projNew);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        return result;
    }



    static public void CompressDll(string dllPath, string dllName, bool testMode = false)
    {
        var projectTargetPath = "Assets/Editor/" + dllName;
        File.Copy(dllPath, projectTargetPath, true);
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        if (BuildAssetBundle(AssetDatabase.LoadMainAssetAtPath(projectTargetPath), "Managed/" + dllName, false, testMode, ""))
            Debug.Log(string.Format("生成、压缩DLL完毕\t{0}", dllName));
        else
            Debug.LogError(string.Format("\t压缩DLL失败\t{0}", dllName));
        if (File.Exists(projectTargetPath))
            AssetDatabase.DeleteAsset(projectTargetPath);
    }

    static public void ObfuscateDll(string[] dllPath, string[] dllName, string curVersion)
    {
        for (int i = 0; i < dllPath.Length; ++i)
        {
            var inDll = "AutoBuild/obfuscar/Input/" + dllName[i];
            File.Copy(dllPath[i], inDll, true);
        }
        var exeName =  "AutoBuild/obfuscar/Obfuscar.Console.exe";
        var paramArg =  "AutoBuild/obfuscar/ObfuscarUnity.xml";
        var log = PluginUtil.Excute(exeName, paramArg, true, true);

        if (string.IsNullOrEmpty(curVersion))
        {
            var dirName = BuildTools.GetUpdateDirName();
            var path = BuildTools.RESOURCE_ONLINE_PATH + dirName + BuildTools.CHAR_SEP;
            var versionDic = PluginUtil.ReadSimpleConfig(File.ReadAllText(path + "Version.txt"));
            curVersion = versionDic["PrepareVersion"];
        }

        var targetFilePath = "AutoBuild/obfuscar/Output/Mapping_" + EditorUserBuildSettings.activeBuildTarget + "_" + curVersion + ".xml";
        if (File.Exists(targetFilePath))
            File.Delete(targetFilePath);
        File.Move("AutoBuild/obfuscar/Output/Mapping.xml", targetFilePath);

        var targetDllPath = "AutoBuild/obfuscar/Output/Client.dll.bytes_" + EditorUserBuildSettings.activeBuildTarget + "_" + curVersion;
        if (File.Exists(targetDllPath))
            File.Delete(targetDllPath);

        File.Copy("AutoBuild/obfuscar/Output/Client.dll.bytes", targetDllPath);

        Debug.Log("ObfuscateDll:" + log);

        for (int i = 0; i < dllPath.Length; ++i)
        {
            var outDll = "AutoBuild/obfuscar/Output/" + dllName[i];
            File.Copy(outDll, dllPath[i], true);
        }
    }

	public static void EncryptDll(string dllPath)
    {
        Debug.Log("EncryptDll:" + dllPath);
        var exeName = Application.dataPath + "/../AutoBuild/EncryptDll.exe";
        string param = "-e " + dllPath + " " + dllPath;
        //Debug.Log("exeName=" + exeName + "  param=" + param);
        var log = PluginUtil.Excute(exeName, param, true, true);
        Debug.Log("EncryptDll log=" + log);
    }

    public static bool BuildFileList(string platform = null, string version = null, bool isSmallBag = false)
    {
        try
        {
            var needIgnoreMap = platform == BuildTarget.Android.ToString() || platform == BuildTarget.iPhone.ToString();
            string[] packageOutsideMapPathArr = new string[0];
            if (needIgnoreMap)
            {
                var mapList = Resources.Load<TextAsset>("Config/ConfigMapList").text;
                var configMapList = ScriptableObject.CreateInstance<ConfigMapList>();
                configMapList.Load(mapList);
                var packageOutsideMapPathList = new List<string>();
                for (int i = 0; i < configMapList.m_dataArr.Length; i++)
                {
                    if (configMapList.m_dataArr[i].PackageOutside)
                        packageOutsideMapPathList.Add("Scenes/map_" + configMapList.m_dataArr[i].MapId + "/");
                }
                Object.DestroyImmediate(configMapList);
                packageOutsideMapPathArr = packageOutsideMapPathList.ToArray();
            }
            

            var files = PluginUtil.GetFiles(PluginUtil.GetEditorStreamingAssetsLibPath());
            var fileList = new StringBuilder();
            var fileSize = new StringBuilder();
            var crcList = new StringBuilder();
            crcList.AppendLine("return {");
            for (int i = 0; i < files.Length; i++)
            {
                var path = PluginUtil.GetUnifyPath(files[i], true);
                if (path.IndexOf("FileList.txt") != -1 || path.IndexOf("FileSize.txt") != -1)
                    continue;

                var bytes = File.ReadAllBytes(files[i]);
                string crc = Util.CRC32String(bytes);
                //根据配置过滤掉指定的地图资源化
                if (needIgnoreMap && path.StartsWithAny(packageOutsideMapPathArr))
                    fileList.AppendLine(path + "=" + crc + "=outside");
                else
                    fileList.AppendLine(path + "=" + crc);
                fileSize.AppendLine(path + "=" + bytes.Length);
                crcList.AppendLine("[\"" + path + "\"] = " + "{ Path = \"" + path + "\", CRC = 0x" + crc + "},");
            }
            crcList.AppendLine("}");
            var targetPath = PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/";
            PluginUtil.CreateFileDirectory(targetPath);
            File.WriteAllText(targetPath + "FileList.txt", fileList.ToString());
            File.WriteAllText(targetPath + "FileSize.txt", fileSize.ToString());

#if UNITY_ANDROID
            File.WriteAllText("../server/cfg/clientfilelist.config", crcList.ToString());
#endif
            var plat = "xxxx";
            if (platform == BuildTarget.Android.ToString())
                plat = "android";
            else if (platform == BuildTarget.iPhone.ToString())
                plat = "ios";
            else if (platform == BuildTarget.StandaloneWindows.ToString())
                plat = "pc";
            else if (platform == BuildTarget.WebPlayer.ToString())
                plat = "web";

            var filelistNameNoVersion = string.Format("{0}_clientfilelist.config", plat);
            File.WriteAllText("../server/cfg/crc/" + filelistNameNoVersion, crcList.ToString());

            
            if (!string.IsNullOrEmpty(platform) && !string.IsNullOrEmpty(version))
            {
                if (version.Split('.').Length != 4)
                    version = BuildTools.GetNextGameVersion(version, BuildVersionType.Current);

                var filelistName = string.Format("{0}_{1}_clientfilelist.config", version.Replace('.','-'), plat);
                File.WriteAllText("../server/cfg/crc/" + filelistName, crcList.ToString());
                SvnOperation.ExeSvnCommitAll("../server/cfg/crc/" + filelistName, filelistName);
            }
            

            //打包
//            const string projectTargetPath = "Assets/Editor/FileList.txt";
//            File.Copy(targetPath + "FileList.txt", projectTargetPath, true);
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
//            var obj = AssetDatabase.LoadMainAssetAtPath(projectTargetPath);
//            BuildAssetBundle(obj, "VersionInfo/FileList");
//            if (File.Exists(projectTargetPath))
//                AssetDatabase.DeleteAsset(projectTargetPath);
            Debug.Log("生成FileList完毕");
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError("生成FileList失败");
            Debug.LogException(e);
            return false;
        }
    }

    public static bool BuildUpdateLog()
    {
        File.Copy("Assets/Resources/UpdateLog.txt", PluginUtil.GetEditorStreamingAssetsLibPath() + "UpdateLog.txt", true);
        Debug.Log("复制更新日志文件");
        return true;
    }

    private static void ExportBundleDepend(bool testMode)
    {
        var result = new StringBuilder();
        foreach (var bundleNode in m_bundleDic.Values)
        {
            if (bundleNode.IsLeaf)
                continue;
            result.Append(PluginUtil.GetUnifyPath(bundleNode.AssetPath + ":"));
            var index = 0;
            foreach (var dependBundleNode in bundleNode.DependBundles)
            {
                if (index++ > 0)
                    result.Append("|");
                result.Append(PluginUtil.GetUnifyPath(dependBundleNode.AssetPath));
            }
            result.AppendLine();
        }

        if(!testMode)
            File.WriteAllText("Assets/Resources/Depend.txt", result.ToString());
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
        BuildAssetBundle(Resources.Load("Depend"), "Depend", false, testMode);
        AssetDatabase.DeleteAsset("Assets/Resources/Depend.txt");
        Debug.Log("bundle包间依赖\n"+result);

        var hasFile = false;
        result = new StringBuilder();
        result.AppendLine("有文件未被划分到Bundle且被引用不止一次，会造成容量浪费");
        foreach (var kv in m_unBundleDic)
        {
            if (kv.Value.Count > 1)
            {
                hasFile = true;
                result.AppendLine(string.Format("【{0}次】{1}\n\t{2}", kv.Value.Count, kv.Key, string.Join("\n\t", kv.Value.ToArray())));
            }
        }
        if (hasFile)
            Debug.Log(result);
    }

    private static void ExportBundleGroupLayer()
    {
        var result = new StringBuilder();
        foreach (var list in m_bundleGroupDic.Values)
        {
//            if (list.Count == 1)
//                continue;

            list.Sort((a,b)=>b.Layer-a.Layer);
            for (int i = 0; i < list.Count; i++)
            {
                var bundleNode = list[i];
                result.Append(bundleNode.Layer);
                result.Append("\t");
                result.AppendLine(bundleNode.AssetPath);
            }

            result.AppendLine();
        }

        Debug.Log("bundle包分组分层：\n"+result);
    }

    /// <summary>
    /// 打包AssetBundle
    /// </summary>
    /// <param name="mainAsset">要打包的资源</param>
    /// <param name="outputPath">相对于streamingAssetsPath的保存路径</param>
    /// <param name="seal">密封打包，即不可能再被其他包依赖</param>
    /// <param name="testMode">测试模式</param>
    /// <param name="suffix">打包后缀</param>
    /// <returns></returns>
    private static bool BuildAssetBundle(Object mainAsset, string outputPath, bool seal = false, bool testMode = false, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;

        return BuildAssetBundle(new[] {mainAsset}, outputPath, seal, testMode, false, suffix);
    }

    /// <summary>
    /// 打包AssetBundle
    /// </summary>
    /// <param name="assets">要打包到一起的资源列表</param>
    /// <param name="outputPath">相对于streamingAssetsPath的保存路径</param>
    /// <param name="seal">是否密封，如果密封，则会Push到高层打包，然后再Pop</param>
    /// <param name="testMode">测试模式</param>
    /// <param name="temp">输出到临时路径，打包后再删除</param>
    /// <param name="suffix">打包后缀</param>
    /// <returns></returns>
    public static bool BuildAssetBundle(Object[] assets, string outputPath, bool seal = false, bool testMode = false, bool temp = false, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;
        if (testMode)
            return true;
        var options = BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.DeterministicAssetBundle;
        outputPath = string.Format("{0}{1}{2}", PluginUtil.GetEditorStreamingAssetsLibPath(), outputPath, suffix);
        Object mainAsset = assets.Length == 1 ? assets[0] : null;
        assets = mainAsset == null ? assets : null;
        if (temp)
            outputPath = Path.Combine("Temp",outputPath);
        PluginUtil.CreateFileDirectory(outputPath);
        if (seal)
            BuildPipeline.PushAssetDependencies();
        var result = BuildPipeline.BuildAssetBundle(mainAsset, assets, outputPath, options, GetBuildTarget());
        if (seal)
            BuildPipeline.PopAssetDependencies();

        if (temp && File.Exists(outputPath))
            File.Delete(outputPath);

        //自己压缩
//        EditorUtility.DisplayProgressBar("AssetBundle打包", "压缩资源中...", 0.5f);
//        Util.CompressFileLZMA(outputPath+"_tmp", outputPath);
//        File.Delete(outputPath+"_tmp");
//        EditorUtility.ClearProgressBar();

        return result;
    }

    /// <summary>
    /// 打包场景
    /// </summary>
    /// <param name="paths"></param>
    /// <param name="outputPath"></param>
    /// <param name="testMode"></param>
    /// <param name="suffix"></param>
    /// <returns></returns>
    public static bool BuildSceneAssetBundle(string[] paths, string outputPath, bool testMode = false, string suffix = ".assetbundle")
    {
        if (!CanRun())
            return false;
        if (testMode)
            return true;
        outputPath = string.Format("{0}{1}{2}", PluginUtil.GetEditorStreamingAssetsLibPath(), outputPath, suffix);
        PluginUtil.CreateFileDirectory(outputPath);
        var result = BuildPipeline.BuildStreamedSceneAssetBundle(paths, outputPath, GetBuildTarget());
        if (!string.IsNullOrEmpty(result))
            Debug.LogError(result);
        return string.IsNullOrEmpty(result);
    }

    public static BuildTarget GetBuildTarget()
    {
        var buildTarget = BuildTarget.StandaloneWindows;
#if UNITY_ANDROID
        buildTarget = BuildTarget.Android;
#elif UNITY_IPHONE
        buildTarget = BuildTarget.iPhone;
#elif UNITY_WEBPLAYER
        buildTarget = BuildTarget.WebPlayer;
#elif UNITY_STANDALONE || UNITY_STANDALONE_WIN
        buildTarget = BuildTarget.StandaloneWindows;
#else
        throw new Exception("未支持的平台");
#endif
        return buildTarget;
    }

    private static bool CanRun()
    {
        if (Application.isPlaying)
        {
            Debug.LogError("不可在运行时执行");
            return false;
        }
        return true;
    }

    class BundleNode
    {
        public string AssetPath;        //Bundle的路径
        public bool Changed = false;    //已改变
        public bool IsPassivityChanged = false; //被动改变
        public bool BeDepend = false;   //被依赖标记
        public bool IsScene = false;    //是否是场景
        public bool IsEtcAtlas = false;    //是否是采用ETC格式压缩的图集，打包时要根据这个做进一步处理
        public bool IsEtcAlphaAtlas = false;
        public List<AssetNode> IncludeAssets;
        public List<BundleNode> DependBundles;//引用
        public List<BundleNode> UsagesBundles;//被引用

        public bool IsRoot { get { return UsagesBundles == null; } }
        public bool IsLeaf { get { return DependBundles == null; } }
        public int Group { get; set; }
        public int Layer { get; set; }
        public bool NeedRebuild { get { return Changed | BeDepend; }}
    }

    class AssetNode
    {
        public string FilePath;     //文件路径
        public Type AssetType;      //需要打包的资源类型
        public string BundlePath;   //归属Bundle的路径
    }
}