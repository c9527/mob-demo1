﻿using System;

[Serializable]
public class ConfigBuildLine
{
    public string Key;
    public string Name;
    public string Platform;
    public string ProductName;
    public string CompanyName;
    public string PackageName;
    public string Icon;
    public string Identifer;
    public string UnityVersion;
    public string SDKName;
    public string Lang;
    public bool Encrypt;
    public bool Obfuscate;
    public bool Corner;
    public bool Splash;
    public string FnGameName;
    public string[] ChannelNames;
    public string OutputPath;
    public string CopyPath;
    public string PublishName;
    public string ServerlistUrl;
    public string UpdateUrl;
    public string targetCDN;
}

public class ConfigBuild : ConfigBase<ConfigBuildLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigBuild() : base("Key")
    {
    }
}
