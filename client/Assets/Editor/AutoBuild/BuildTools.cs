﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using Debug = UnityEngine.Debug;

public class  BuildTools : EditorWindow
{
    private const int WINDOW_HEIGHT = 440;
    public static readonly char CHAR_SEP = Path.DirectorySeparatorChar;

    private string m_language = "zh";

#if UNITY_EDITOR_OSX
    private const string ROOT_PATH = @"/Volumes";
#else
    private const string ROOT_PATH = @"\\172.16.10.158";
#endif

    public static readonly string RESOURCE_ONLINE_PATH = ROOT_PATH + CHAR_SEP + "zjqz" + CHAR_SEP;        //在线资源根目录
    private static string m_buildVersionPath = "";     //保存当前库中ab资源包版本对应的svn版本号

    private const string TIP_BUILD_AB = "1.确定要增量打包的文件 [自动]\n" +
                                        "2.打包变更资源 [自动]\n" +
                                        "3.修改本地BuildVersion文件 [自动]\n";

    private const string TIP_TEST_BUILD = "1.确定要增量打包的文件 [自动]\n" +
                                          "2.打包变更资源 [自动]\n" +
                                          "3.修改本地BuildVersion文件 [自动]\n" +
                                          "4.提交所有更改文件 [自动]\n" +
                                          "5.删除旧的在线资源 [自动]\n" +
                                          "6.复制本地资源到在线目录 [自动]\n" +
                                          "7.修改在线版本号 [自动]\n" +
                                          "8.提交在线资源到数传平台 [自动]\n" +
                                          "9.内传外审核 【手动】\n" +
                                          "10.用蓝海系统发布资源 【手动】\n";

    private const string TIP_CHANGE_OFFICIAL = "1.获取在线PrepareVersion版本号 [自动]\n" +
                                               "2.修改在线资源目录名为正式的 [自动]\n" +
                                               "3.修改在线版本号 [自动]\n" +
                                               "4.提交在线资源到数传平台 [自动]\n" +
                                               "5.内传外审核 【手动】\n" +
                                               "6.用蓝海系统发布资源 【手动】";

    private const string TIP_BUILD_PLATFORM_APK = "2.生成Apk母包 [自动]\n" +
                                                  "3.生成各平台包 [自动]\n" +
                                                  "4.提交Apk包到数传平台 [自动]\n" +
                                                  "5.内传外审核 【手动】";

    private const string TIP_OPEN_PORT = "1.修改服务器列表状态为开启 [自动]\n" +
                                         "2.提交在线资源到数传平台 [自动]\n" +
                                         "3.内传外审核 【手动】\n" +
                                         "4.用蓝海系统发布资源 【手动】";

    private const string TIP_CLOSE_PORT = "1.修改服务器列表状态为关闭 [自动]\n" +
                                          "2.提交在线资源到数传平台 [自动]\n" +
                                          "3.内传外审核 【手动】\n" +
                                          "4.用蓝海系统发布资源 【手动】";
    private int m_pageIndex = 1;

    private static int m_selectIndex = -1;
    private const int DEFAULT_SELECT_INDEX = 1;

    private static string[] m_buildConfigKeyArr;
    private static string[] m_buildNameArr;
    private static ConfigBuild m_configFile;
    private static string m_version;
    private static string m_prepareVersion;
    private string m_diffFileList = "";

    private List<List<string>> m_taskList = new List<List<string>>();
    private const int VERSION_CHECK = 7;//versonchecklist
    private const int ALL_MENU_INDEX = 6;       //全部
    private const int OFFICIAL_MENU_INDEX = 5;  //发正式
    private const int TEST_MENU_INDEX = 4;      //发测试
    private const int VERSION_MENU_INDEX = 3;   //版本文件
    private const int SERVERLIST_INDEX = 2;     //服务器列表
    private const int CLOSE_SERVER_INDEX = 1;   //关入口
    private const int OPEN_SERVER_INDEX = 0;    //开入口

    private List<List<string>> m_orderList = new List<List<string>>(); 
    private int m_selectOrderType = 4;

    private bool m_buildAll;
    private bool m_buildCode;
    private bool m_buildConfig;
    private bool m_buildResource;
    private bool m_buildSmallPkg;

    private bool m_encryptDll;
    private bool m_obfuscateDll;

    private bool m_buildDiff;
    private bool m_buildDiffTest;
    private bool m_buildDevelopmentMode;    //开发模式
    private bool m_buildDebugErrorMode;     //查错模式，只能打独立apk，无法热更。代码内嵌，开启开发模式，可以看到行号
//    private bool m_buildLangMode;     //多语言增量打包

    private bool m_buildCorner;
    private bool m_buildSplash;
    private List<KeyValue<string, bool>> m_buildChannels; 

    private BuildTarget m_buildTarget;

    private Vector2 m_scrollVector;
    private string m_oldVersion = "";
    private string m_oldFileList = "";
    private string m_updateFileSize = "";
    private readonly List<KeyValue<string, int>> changeFileSizeList = new List<KeyValue<string, int>>();

    private bool m_ignoreUnityVersionLimit;
    private bool m_ignorePlatformLimit;
    private bool m_isBuildCryptedCfg = true;

    public const string WEB_STYLE_CSS_PATH = "/trunk/client/AutoBuild/web/css/styles.css";
    public const string WEB_GAME_JS_PATH = "/trunk/client/AutoBuild/web/game.js";
    public const string WEB_JS_ROOT_PATH = "/trunk/client/AutoBuild/web/Scripts";

    bool CSS_CHANGE;
    bool GAME_JS_CHANGE;
    bool OTHER_JS_CHANGE;
    List<string> jschange_index;

    bool m_updateAndroid = true;
    bool m_updatePc = true;
    bool m_updateWeb = true;
    bool m_updateIos = true;

    bool cdnready = false;

    public int GetLine(string[] index,string name)
    {
        for (int i = 0; i < index.Length; i++)
        {
            if (index[i].Contains(name))
            {
                return i;
            }
        }
        return -1;
    }

    int getVersionWeb(string line)
    {
        string version = "";
        for (int i = 0; i < line.Length; i++)
        {
            if (line[i] == '?')
            {
                i += 3;
                while (Char.IsDigit(line[i]))
                {
                    version += line[i];
                    i++;
                }
                return int.Parse(version);
            }
        }
        return 0;
    }



    public void CheckWebVersion()
    {
        string[] index = File.ReadAllLines("AutoBuild/web/index.html.template");
        if (GAME_JS_CHANGE)
        {
            int gameline = GetLine(index, "game.js");
            int lastVersion = getVersionWeb(index[gameline]);

            int version = lastVersion + 1;
            if (index[gameline].Contains(lastVersion.ToString()))
            {
               index[gameline]= index[gameline].Replace(lastVersion.ToString(), version.ToString());
            }
            Debug.Log("检测到game.js有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        if (CSS_CHANGE)
        {
            int gameline = GetLine(index, "css");
            int lastVersion = getVersionWeb(index[gameline]);
            int version = lastVersion + 1;
            index[gameline].Replace(lastVersion.ToString(), version.ToString());
            Debug.Log("检测style.css有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        if (OTHER_JS_CHANGE)
        {
           index = CheckVersionOther(ref index);
        }
        File.WriteAllLines("AutoBuild/web/index.html.template",index);
        SvnOperation.ExeSvnCommitAll("AutoBuild/web/index.html.template", "修改改动的js，css在index.html下的版本号");
        
    }

    string[] CheckVersionOther(ref string[] index)
    {
        if (!OTHER_JS_CHANGE)
        {
            return index;
        }
        for (int i = 0; i < jschange_index.Count; i++)
        {
            int gameline = GetLine(index, jschange_index[i]);
            if (gameline < 0)
            {
                continue;
            }
            int lastVersion = getVersionWeb(index[gameline]);
            int version = lastVersion + 1;
            index[gameline] = index[gameline].Replace(lastVersion.ToString(), version.ToString());
            Debug.Log("检测"+jschange_index[i]+"有变动，将校验号由" + lastVersion + "更新至" + version);
        }
        return index;
    }

    public static string KeySelectIndex
    {
        get { return "KEY_SELECT_INDEX_" + Directory.GetCurrentDirectory(); }
    }

    public static void Open()
    {
        if (!Directory.Exists(RESOURCE_ONLINE_PATH))
        {
            Debug.LogError("请检查网络");
            PluginUtil.ExcuteCmd(new[] { @"net use \\172.16.10.158" });//登录共享目录
            return;
        }

        GetWindowWithRect<BuildTools>(new Rect(0, 0, 450, WINDOW_HEIGHT), true, "[BuildTools] - "+Directory.GetCurrentDirectory());
    }

    void OnFocus()
    {
        ReloadConfig(true);
    }

    public static string GetUpdateDirName(string updateUrl)
    {
        var arr = updateUrl.Split(new[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
        return arr[arr.Length - 1];
    }

    public static string GetUpdateDirName()
    {
        var updateUrl = m_configFile.m_dataArr[m_selectIndex].UpdateUrl;
        return GetUpdateDirName(updateUrl);
    }

    private void ReloadConfig(bool onFocus = false)
    {
        if (m_selectIndex == -1)
        {
            if (!PluginUtil.HasKey(KeySelectIndex))
                m_selectIndex = DEFAULT_SELECT_INDEX;
            else
                m_selectIndex = int.Parse(PluginUtil.GetValue(KeySelectIndex));
            PluginUtil.SetValue(KeySelectIndex, m_selectIndex.ToString());
        }

        m_buildVersionPath = PluginUtil.GetEditorStreamingAssetsLibRootPath() + "BuildVersion_" + EditorUserBuildSettings.activeBuildTarget + ".txt";

        if (m_configFile != null)
            DestroyImmediate(m_configFile);
        m_configFile = CreateInstance<ConfigBuild>();
        m_configFile.Load(PluginUtil.ReadAllTextWriteSafe("AutoBuild/ConfigBuild.csv"));

        if (m_selectIndex >= m_configFile.m_dataArr.Length)
        {
            m_selectIndex = DEFAULT_SELECT_INDEX;
            PluginUtil.SetValue(KeySelectIndex, m_selectIndex.ToString());
        }
        var configLine = m_configFile.m_dataArr[m_selectIndex];
        var updateUrl = configLine.UpdateUrl;
        if (!string.IsNullOrEmpty(updateUrl))
        {
            var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(RESOURCE_ONLINE_PATH + GetUpdateDirName() + CHAR_SEP + "Version.txt"));
            m_version = dic.GetValue("Version", "-");
            m_prepareVersion = dic.GetValue("PrepareVersion", "-");
        }
        else
        {
            m_version = "-";
            m_prepareVersion = "-";
        }

        m_buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), configLine.Platform);
        m_buildConfigKeyArr = new string[m_configFile.m_dataArr.Length];
        m_buildNameArr = new string[m_configFile.m_dataArr.Length];
        for (int i = 0; i < m_buildNameArr.Length; i++)
        {
            m_buildConfigKeyArr[i] = m_configFile.m_dataArr[i].Key;
            m_buildNameArr[i] = m_configFile.m_dataArr[i].Name;
        }

        if (!onFocus || m_buildChannels == null)
        {
            m_buildCorner = configLine.Corner;
            m_buildSplash = configLine.Splash;
            m_encryptDll = configLine.Encrypt;
            m_obfuscateDll = configLine.Obfuscate;

            m_buildChannels = new List<KeyValue<string, bool>>();
            for (int i = 0; i < configLine.ChannelNames.Length; i++)
            {
                m_buildChannels.Add(new KeyValue<string, bool>(configLine.ChannelNames[i], false));
            }
            if (m_buildChannels.Count > 0)
                m_buildChannels[0].value = true;
        }
    }

    private void OnGUI()
    {
        GUI.backgroundColor = Color.green;
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Space(4);
        var selectIndexTmp = EditorGUILayout.Popup(m_selectIndex, m_buildNameArr);
        if (selectIndexTmp != m_selectIndex)
        {
            m_selectIndex = selectIndexTmp;
            m_ignorePlatformLimit = false;
            m_ignoreUnityVersionLimit = false;
            ReloadConfig();
            PluginUtil.SetValue(KeySelectIndex, m_selectIndex.ToString());
        }
        GUILayout.EndVertical();

        if (GUILayout.Button("打开修改", GUILayout.Width(80)))
        {
            EditorApplication.delayCall += () => Process.Start(Path.GetFullPath("AutoBuild/ConfigBuild.csv"));
            EditorApplication.update();
        }
        GUILayout.EndHorizontal();
        GUI.backgroundColor = Color.white;
        var pageIndexTmp = GUILayout.SelectionGrid(m_pageIndex, new[] { "自动打包", "手动打包", "数传系统", "蓝海系统", "热更大小" }, 5);
        PluginUtil.DrawSeparator();
        if (pageIndexTmp != m_pageIndex)
        {
            m_ignorePlatformLimit = false;
            m_ignoreUnityVersionLimit = false;
        }
        m_pageIndex = pageIndexTmp;

        //平台判断
        if (m_pageIndex == 0 || m_pageIndex == 1)
        {
            var config = m_configFile.m_dataArr[m_selectIndex];
            var buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), config.Platform);
            if (!m_ignorePlatformLimit && EditorUserBuildSettings.activeBuildTarget != buildTarget)
            {
                GUILayout.Label(string.Format("平台不匹配，当前平台为{0}，目标平台为{1}\n请选择正确的打包策略，或手动切换平台", EditorUserBuildSettings.activeBuildTarget, buildTarget));
                if (GUILayout.Button("忽略一次", GUILayout.Width(100)))
                    m_ignorePlatformLimit = true;
                return;
            }

            if (!m_ignoreUnityVersionLimit && !string.IsNullOrEmpty(config.UnityVersion) && config.UnityVersion != Application.unityVersion)
            {
                GUILayout.Label(string.Format("Unity版本不匹配，当前版本为{0}，目标版本为{1}\n请使用正确的Unity版本", Application.unityVersion, config.UnityVersion));
                if (GUILayout.Button("忽略一次", GUILayout.Width(100)))
                    m_ignoreUnityVersionLimit = true;
                return;
            }
        }

        if (m_pageIndex == 0)
        {
            DrawVersionInfo();
            OnAutoBuildGUI();
        }
        else if (m_pageIndex == 1)
        {
            DrawVersionInfo();
            OnBuildGUI();
        }
        else if (m_pageIndex == 2)
            OnDtsGUI();
        else if (m_pageIndex == 3)
            OnbssGUI();
        else if (m_pageIndex == 4)
            OnCalcSizeGUI();

        if (m_buildDiff && m_pageIndex == 1)
        {
            minSize = new Vector2(450, WINDOW_HEIGHT + 100);
            maxSize = minSize;
        }
        else
        {
            minSize = new Vector2(450, WINDOW_HEIGHT);
            maxSize = minSize;
        }
    }

    private void DrawVersionInfo()
    {
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Space(4);
        GUILayout.BeginHorizontal();
        GUILayout.Label("在线版本号：", GUILayout.Width(70));
        GUILayout.Label("Version " + m_version);
        GUILayout.Label("PrepareVersion " + m_prepareVersion);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        if (GUILayout.Button("修改", GUILayout.Width(80)))
        {
            EditorApplication.delayCall += () => Process.Start(RESOURCE_ONLINE_PATH + GetUpdateDirName() + CHAR_SEP + "Version.txt");
            EditorApplication.update();
        }
        GUILayout.EndHorizontal();
        PluginUtil.DrawSeparator();
    }


    private void OnSelectLang()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Toggle(m_language.Equals(LanguageConst.LANG_ZH),LanguageConst.LANG_ZH))
        {
            m_language = LanguageConst.LANG_ZH;
        }
        if (GUILayout.Toggle(m_language.Equals(LanguageConst.LANG_RU), LanguageConst.LANG_RU))
        {
            m_language = LanguageConst.LANG_RU;
        }
        if (GUILayout.Toggle(m_language.Equals(LanguageConst.LANG_EN), LanguageConst.LANG_EN))
        {
            m_language = LanguageConst.LANG_EN;
        }

        if (GUILayout.Toggle(m_language.Equals(LanguageConst.LANG_KR), LanguageConst.LANG_KR))
        {
            m_language = LanguageConst.LANG_KR;
        }
        if (GUILayout.Button("拷贝资源并翻译"))
        {
            HandleLangRes();
        }
        if (GUILayout.Button("拷贝语言资源"))
        {
            HandleLangRes(false);
        }
        if (GUILayout.Button("拷贝语言资源(不包括图集)"))
        {
            HandleLangRes(false, false);
        }
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// 自动打包 UI
    /// </summary>
    private void OnAutoBuildGUI()
    {
        var dirName = GetUpdateDirName();

        OnSelectLang();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("增量打包资源到最新版本 [本地]", GUILayout.Height(40)))
        {
            if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_BUILD_AB, "继续", "取消"))
                OneKeyBuildAB(true);
        }
        if (GUILayout.Button("增量打包资源到当前版本 [本地]", GUILayout.Height(40)))
        {
            if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_BUILD_AB, "继续", "取消"))
                OneKeyBuildAB(false);
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("发布《测试》用热更资源 [本地][在线]", GUILayout.Height(40)))
        {
            if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_TEST_BUILD, "继续", "取消"))
                OneKeyHotUpdateTestBuild();
        }
        if (GUILayout.Button("测试用热更资源转《正式》版 [在线]", GUILayout.Height(40)))
        {
            if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_CHANGE_OFFICIAL, "继续", "取消"))
                OneKeyChangeTestToOfficial();
        }
        if (m_buildTarget != BuildTarget.WebPlayer)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("开入口 [在线]", GUILayout.Height(40)))
            {
                if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_OPEN_PORT, "继续", "取消"))
                    OneKeySetPortState(true);
            }
            if (GUILayout.Button("关入口 [在线]", GUILayout.Height(40)))
            {
                if (EditorUtility.DisplayDialog("继续将执行以下步骤：", TIP_CLOSE_PORT, "继续", "取消"))
                    OneKeySetPortState(false);
            }
            GUILayout.EndHorizontal();
        }

//        if (m_buildTarget == BuildTarget.Android)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("发布整包 [当前版本]", GUILayout.Height(40)))
            {
                var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(RESOURCE_ONLINE_PATH + dirName + CHAR_SEP + "Version.txt"));
                var des = string.Format("1.使用{0}作为包内版本号\n", dic.GetValue("PrepareVersion"));
                if (m_buildTarget == BuildTarget.iPhone)
                    des += string.Format("2.使用{0}作为包内大版本号\n", dic.GetValue("PrepareBigVersion"));
                if (EditorUtility.DisplayDialog("继续将执行以下步骤：", des, "继续", "取消"))
                    OneKeyPlatformApkPublic(BuildVersionType.Current);
            }
            if (GUILayout.Button("发布整包 [下一版本]", GUILayout.Height(40)))
            {
                var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(RESOURCE_ONLINE_PATH + dirName + CHAR_SEP + "Version.txt"));
                var des = string.Format("1.使用{0}作为包内版本号\n", GetNextGameVersion(dic.GetValue("PrepareVersion"), BuildVersionType.Next));
                if (m_buildTarget == BuildTarget.iPhone)
                    des += string.Format("2.使用{0}作为包内大版本号\n", GetNextBigGameVersion(dic.GetValue("PrepareBigVersion"), BuildVersionType.Next));
                if (EditorUtility.DisplayDialog("继续将执行以下步骤：", des, "继续", "取消"))
                    OneKeyPlatformApkPublic(BuildVersionType.Next);
            }
            if (GUILayout.Button("发布整包 [下一大版本]", GUILayout.Height(40)))
            {
                var dic = PluginUtil.ReadSimpleConfig(File.ReadAllText(RESOURCE_ONLINE_PATH + dirName + CHAR_SEP + "Version.txt"));
                var des = string.Format("1.使用{0}作为包内版本号\n", GetNextGameVersion(dic.GetValue("PrepareVersion"), BuildVersionType.NextBig));
                if (m_buildTarget == BuildTarget.iPhone)
                    des += string.Format("2.使用{0}作为包内大版本号\n", GetNextBigGameVersion(dic.GetValue("PrepareBigVersion"), BuildVersionType.NextBig));
                if (EditorUtility.DisplayDialog("继续将执行以下步骤：", des, "继续", "取消"))
                    OneKeyPlatformApkPublic(BuildVersionType.NextBig);
            }
            GUILayout.EndHorizontal();
        }
        {
            if (GUILayout.Button("一键发布测试热更", GUILayout.Height(40)))
            {
                oneKeyPublishHotTestData();

            }
        }

        {
            GUILayout.BeginHorizontal();
            m_updateAndroid = GUILayout.Toggle(m_updateAndroid,"android", GUILayout.Height(20), GUILayout.Width(60));
            m_updateIos = GUILayout.Toggle(m_updateIos, "ios", GUILayout.Height(20), GUILayout.Width(40));
            m_updatePc = GUILayout.Toggle(m_updatePc, "pc", GUILayout.Height(20), GUILayout.Width(40));
            m_updateWeb = GUILayout.Toggle(m_updateWeb, "web", GUILayout.Height(20), GUILayout.Width(40));
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// 手动打包 UI
    /// </summary>
    private void OnBuildGUI()
    {
        var configLine = m_configFile.m_dataArr[m_selectIndex];

        GUILayout.Label("Name\t\t" + configLine.ProductName);
        GUILayout.Label("UpdateUrl\t\t" + configLine.UpdateUrl);
        GUILayout.Label("ServerlistUrl\t" + configLine.ServerlistUrl);
        GUILayout.Label("SDKName\t\t" + configLine.SDKName);
        GUILayout.Label("Identifer\t\t" + configLine.Identifer);

        GUILayout.Space(10);

        GUILayout.Label("重新打包：", GUILayout.Width(70));
        

        GUILayout.BeginHorizontal();
        

        GUI.enabled = !(m_buildDiff || m_buildDebugErrorMode || m_buildTarget == BuildTarget.iPhone );
        var buildAllTmp = GUILayout.Toggle(m_buildAll, "全部");
        if (buildAllTmp != m_buildAll)
            m_buildCode = m_buildConfig = m_buildResource = buildAllTmp;
        m_buildAll = buildAllTmp;

        GUILayout.BeginVertical(GUILayout.Width(100));
        m_buildCode = GUILayout.Toggle(m_buildCode, "代码");
        m_obfuscateDll = GUILayout.Toggle(m_obfuscateDll, "  ┣━━混淆");
        GUI.enabled = !(m_buildDiff || m_buildDebugErrorMode || m_buildTarget == BuildTarget.iPhone || m_buildTarget == BuildTarget.WebPlayer);
        m_encryptDll = GUILayout.Toggle(m_encryptDll, "  ┗━━加密");
        GUILayout.EndVertical();

        GUI.enabled = !(m_buildDiff || m_buildDebugErrorMode);
        GUILayout.BeginVertical();
        m_buildConfig = GUILayout.Toggle(m_buildConfig, "配置");
        m_isBuildCryptedCfg = GUILayout.Toggle(m_isBuildCryptedCfg, "配置加密");
        GUILayout.EndVertical();
        m_buildResource = GUILayout.Toggle(m_buildResource, "资源");
        m_buildSmallPkg = GUILayout.Toggle(m_buildSmallPkg, "大小包");
        m_buildAll = m_buildCode && m_buildConfig && m_buildResource;
        GUI.enabled = true;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("还原StreamingAssetsLib", GUILayout.Width(170)))
        {
            if (EditorUtility.DisplayDialog("确认", "确定要还原StreamingAssetsLib中修改的文件，并删除未加入版本管理的文件吗？", "确定", "取消"))
            {
                SvnOperation.ExeSvnFullRevert(PluginUtil.GetEditorStreamingAssetsLibPath(), true);
                SvnOperation.ExeSvnFullRevert("Assets/Resources/Depend.txt", true);
                SvnOperation.ExeSvnFullRevert("../server/cfg/clientfilelist.config", true);
            }
        }
        if (GUILayout.Button("打开StreamingAssetsLib", GUILayout.Width(170)))
        {
#if UNITY_EDITOR_OSX
            EditorUtility.RevealInFinder(Path.GetFullPath(PluginUtil.GetEditorStreamingAssetsLibPath()));
#else
            Process.Start("explorer", Path.GetFullPath(PluginUtil.GetEditorStreamingAssetsLibPath()));
#endif
        }
        if (GUILayout.Button("测试FileList", GUILayout.Width(90)))
        {
            BuildAB.BuildFileList(m_buildTarget.ToString());
        }
//        if (GUILayout.Button("测试小包复制", GUILayout.Width(90)))
//        {
//            FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsPath().TrimEnd('/'));
//            CopyMiniBundleToStreamingAssets();
//        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("复制StreamingAssetsLib", GUILayout.Width(170)))
        {
            if (EditorUtility.DisplayDialog("确认", "确定要把StreamingAssetsLib目录的资源复制到StreamingAssets目录中吗？", "确定", "取消"))
            {
                FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsPath().TrimEnd('/'));
                CopyBundleToStreamingAssets();
                AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            }
        }
        if (GUILayout.Button("清空StreamingAssets", GUILayout.Width(170)))
        {
            if (EditorUtility.DisplayDialog("确认", "确定要清空StreamingAssets目录吗？", "确定", "取消"))
            {
                PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsPath());
                AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            }
        }
        if (GUILayout.Button("内网GameDef", GUILayout.Width(90)))
        {
            AutoBuild.BuildGameDefine(configLine.Key, BuildVersionType.Current, true);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUI.enabled = !m_buildDebugErrorMode;
        var onlyBuildAb = GUILayout.Button("打AB包", GUILayout.Height(40), GUILayout.Width(170));
        GUI.enabled = true;

        var buildAbAndPublish = GUILayout.Button("打AB包并发布", GUILayout.Height(40), GUILayout.Width(170));

//        if (GUILayout.Button("打多语言AB包", GUILayout.Height(40), GUILayout.Width(170)))
//        {
//            EditLanguageDiffFileList();
//        }

        if (IsAndroidSdk())
        {
            GUILayout.BeginVertical();
            m_buildCorner = GUILayout.Toggle(m_buildCorner, "脚标", GUILayout.Height(20), GUILayout.Width(50));
            m_buildSplash = GUILayout.Toggle(m_buildSplash, "闪屏", GUILayout.Height(20), GUILayout.Width(50));
            GUILayout.EndVertical();
        }
        
        GUILayout.EndHorizontal();

        GUILayout.BeginVertical();
        if (IsAndroidSdk())
        {
            int count = 0;
            for (int i = 0; i < m_buildChannels.Count; i++)
            {
                if (count % 6 == 0)
                    GUILayout.BeginHorizontal();
                m_buildChannels[i].value = GUILayout.Toggle(m_buildChannels[i].value, m_buildChannels[i].key, GUILayout.Width(70));
                count++;
                if (count % 6 == 0)
                    GUILayout.EndHorizontal();
            }
        }
        GUILayout.EndVertical();

//        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUI.enabled = !m_buildDebugErrorMode;
        var tmpBuildDiff = GUILayout.Toggle(m_buildDiff, "增量打包", GUILayout.Height(20), GUILayout.Width(70));
        GUI.enabled = !m_buildDebugErrorMode;
//        m_buildDiffTest = GUILayout.Toggle(m_buildDiffTest, "跳过打包", GUILayout.Height(20), GUILayout.Width(130));
        m_buildDevelopmentMode = GUILayout.Toggle(m_buildDevelopmentMode, "开发模式", GUILayout.Height(20), GUILayout.Width(70));
        GUI.enabled = true;
        m_buildDebugErrorMode = GUILayout.Toggle(m_buildDebugErrorMode, "查错模式", GUILayout.Height(20), GUILayout.Width(70));


        GUILayout.EndHorizontal();
        if (tmpBuildDiff && !m_buildDiff)
            CheckChangeType();
        m_buildDiff = tmpBuildDiff;

        //查错模式不允许增量打包
        if (m_buildDebugErrorMode)
        {
            m_buildDiff = false;
            m_buildCode = false;
            m_buildConfig = false;
            m_buildResource = false;
        }

        //ios不允许增量打包代码
        if (m_buildTarget == BuildTarget.iPhone || m_buildTarget == BuildTarget.WebPlayer)
            m_buildCode = false;

        if (m_buildDiff)
        {
            var tmpDiffFileList = EditorGUILayout.TextArea(m_diffFileList, GUILayout.Height(100));
            if (tmpDiffFileList != m_diffFileList)
            {
                m_diffFileList = tmpDiffFileList;
                CheckChangeType();
            }
            else
                m_diffFileList = tmpDiffFileList;
        }

        if (onlyBuildAb || buildAbAndPublish)
        {
            if (!OnlyBuildAb())
                return;
        }

        if (buildAbAndPublish)
            OnlyPublish(BuildVersionType.Current);
    }

    void UpdateDtsAuto(int m_selectOrderType)
    {
        string inputParams;
        string log;
        GetParamsAndLog(m_selectOrderType, out inputParams, out log);
        inputParams = inputParams.Replace(',', ' ');
        if (!string.IsNullOrEmpty(inputParams))
        {
            if (true)
            {
                //PluginUtil.ClearDirectory(@"D:\upload\");
                var arr = inputParams.Split(' ');
                for (int i = 0; i < arr.Length; i++)
                {
                    FileUtil.DeleteFileOrDirectory(@"D:\upload\" + arr[i]);
                    PluginUtil.CreateFileDirectory(@"D:\upload\" + arr[i]);
                    FileUtil.CopyFileOrDirectory(@"D:\zjqz\" + arr[i], @"D:\upload\" + arr[i]);
                }

                //PluginUtil.Excute(@"D:\UploadToDts.bat",inputParams);
                //PluginUtil.Excute(@"AutoBuild\UploadToDts.bat", inputParams);
                bool canup = true;
                string[] paths = GetDirToCheck();
                for (int i = 0; i < paths.Length; i++)
                {
                    canup &= CheckDir(paths[i]);
                }

                if (canup)
                {
                    PluginUtil.Excute(@"D:\UploadToDts.bat", inputParams);
                    m_dtsStartTime = TimeUtil.GetNowTimeStamp();
                    CreateBlueSeaAuto();
                }
            }
        }
    }

    string[] GetDirToCheck()
    {
        List<string> lists = new List<string>();
        if (m_updateAndroid)
        {
            lists.Add("union");
        }
        if (m_updateIos)
        {
            lists.Add("ios");
        }
        if (m_updatePc)
        {
            lists.Add("pc");
        }
        if (m_updateWeb)
        {
            lists.Add("web");
        }

        return lists.ToArray();
    }


    bool CheckDir(string dir)
    {
        string path = @"D:\upload\"+dir;
        if(Directory.Exists(path))
        {
            if(File.Exists(path+"\\Version.txt"))
            {
                return true;
            }

        }
        else
        {
            Debug.Log("检测到有目录尚未准备好"+path);
            return false;
        }
        return false;
    }


    /// <summary>
    /// 数传系统 UI
    /// </summary>
    private void OnDtsGUI()
    {
        GUILayout.BeginHorizontal();
        m_selectOrderType = GUILayout.SelectionGrid(m_selectOrderType, new[] { "开入口", "关入口", "服务器列表", "版本文件", "发测试", "发正式", "全部","测试版本文件" }, 4, EditorStyles.radioButton, new[] { GUILayout.Width(355) });
        if (GUILayout.Button("同步数据", GUILayout.Width(80), GUILayout.Height(38)))
        {
            string inputParams;
            string log;
            GetParamsAndLog(m_selectOrderType, out inputParams, out log);
            inputParams = inputParams.Replace(',', ' ');
            if (!string.IsNullOrEmpty(inputParams))
            {
                if (EditorUtility.DisplayDialog("确定要同步本地数据到中转机吗？", string.Format("参数：{0}", inputParams), "确定", "取消"))
                {
                    //PluginUtil.ClearDirectory(@"D:\upload\");
                    var arr = inputParams.Split(' ');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        FileUtil.DeleteFileOrDirectory(@"D:\upload\" + arr[i]);
                        PluginUtil.CreateFileDirectory(@"D:\upload\" + arr[i]);
                        FileUtil.CopyFileOrDirectory(@"D:\zjqz\" + arr[i], @"D:\upload\" + arr[i]);
                    }
                    //PluginUtil.Excute(@"AutoBuild\UploadToDts.bat", inputParams);
                }
            }
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("刷新任务"))
            m_taskList = DataTransmissionSystem.GetTasks();

        for (int i = 0; i < m_taskList.Count; i++)
        {
            GUILayout.BeginHorizontal();
            var taskId = "";
            for (int j = 0; j < m_taskList[i].Count; j++)
            {
                var value = m_taskList[i][j];
                if (j == 1)
                    GUILayout.Label(value, GUILayout.Width(150));
                else if (j == 2)
                {
                    taskId = value;
                    GUILayout.Label(value);
                }
                else if (j == 3)
                {
                    if (value == "未发版")
                    {
                        if (GUILayout.Button("发布", GUILayout.Width(48)))
                        {
                            DataTransmissionSystem.PerformTask(taskId);
                            m_taskList = DataTransmissionSystem.GetTasks();
                            m_dtsStartTime = TimeUtil.GetNowTimeStamp();
                            m_dtsUpdatingId = taskId;
                        }
                        GUI.backgroundColor = Color.red;
                        if (GUILayout.Button("停止", GUILayout.Width(48)))
                        {
                            DataTransmissionSystem.CancelTask(taskId);
                            m_taskList = DataTransmissionSystem.GetTasks();
                        }
                        GUI.backgroundColor = Color.white;
                    }
                    else if (value == "正在发版")
                    {
                        GUI.backgroundColor = Color.red;
                        if (GUILayout.Button("停止", GUILayout.Width(100)))
                        {
                            DataTransmissionSystem.CancelTask(taskId);
                            m_taskList = DataTransmissionSystem.GetTasks();
                        }
                        GUI.backgroundColor = Color.white;
                    }
                    else if (value == "发版成功")
                    {
                        GUI.enabled = false;
                        GUI.backgroundColor = Color.green;
                        GUILayout.Button("发布成功", GUILayout.Width(100));
                        GUI.backgroundColor = Color.white;
                        GUI.enabled = true;
                    }
                    else
                    {
                        GUI.enabled = false;
                        GUILayout.Button(value, GUILayout.Width(100));
                        GUI.enabled = true;
                    }
                }
                else
                    GUILayout.Label(value);
            }
            
            GUILayout.EndHorizontal();
        }
    }

    private void CreateBlueSeaAuto()
    {
        string inputParams;
        string log;
        var configLine = m_configFile.m_dataArr[m_selectIndex];
        GetParamsAndLog(TEST_MENU_INDEX, out inputParams, out log);
        BlueSeaSystem.CreateUpdateOrder(configLine, inputParams, log);
        m_orderList = BlueSeaSystem.GetOrders();
    }



    /// <summary>
    /// 蓝海系统 UI
    /// </summary>
    private void OnbssGUI()
    {
        var configLine = m_configFile.m_dataArr[m_selectIndex];
        GUILayout.BeginHorizontal();
        m_selectOrderType = GUILayout.SelectionGrid(m_selectOrderType, new[] { "开入口", "关入口", "服务器列表", "版本文件", "发测试", "发正式", "全部","测试版本文件" }, 4, EditorStyles.radioButton, new[] { GUILayout.Width(355) });
        if (GUILayout.Button("创建工单", GUILayout.Width(80), GUILayout.Height(38)))
        {
            string inputParams;
            string log;
            GetParamsAndLog(m_selectOrderType, out inputParams, out log);
            if (!string.IsNullOrEmpty(inputParams))
            {
                if (EditorUtility.DisplayDialog("确定要创建工单吗？", string.Format("参数：{0}\n\n日志：{1}", inputParams, log), "确定", "取消"))
                {
                    BlueSeaSystem.CreateUpdateOrder(configLine, inputParams, log);
                    m_orderList = BlueSeaSystem.GetOrders();
                }
            }
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("刷新工单"))
        {
            m_orderList = BlueSeaSystem.GetOrders();
        }

        GUILayout.BeginVertical();
        for (int i = 0; i < m_orderList.Count; i++)
        {
            var stateType = m_orderList[i][0];
            GUILayout.BeginHorizontal();
            for (int j = 0; j < m_orderList[i].Count; j++)
            {
                var item = m_orderList[i][j];
                if (j == 0)
                {

                }
                else if (j == 1)
                    GUILayout.Label(m_orderList[i][2]);
                else if (j == 2)
                    GUILayout.Label(m_orderList[i][1], GUILayout.Width(150));
                else if (j == 3)
                {

                }
                else
                    GUILayout.Label(item);
                if (j == 4)
                {
                    if (stateType == "0" || stateType == "1")
                    {
                        if (GUILayout.Button("更新", GUILayout.Width(stateType == "0" ? 100 : 48)))
                        {

                            if (EditorUtility.DisplayDialog("确认", string.Format("确定要执行工单？{0}\n参数：{1}", item, BlueSeaSystem.GetOrderParams(item)), "确定", "取消"))
                            {
                                BlueSeaSystem.PerformOrder(item);
                                m_bssStartTime = TimeUtil.GetNowTimeStamp();
                                m_bssUpdatingId = item;
                                m_orderList = BlueSeaSystem.GetOrders();
                                return;
                            }
                        }
                        if (stateType != "0")
                        {
                            GUI.backgroundColor = Color.red;
                            if (GUILayout.Button("取消", GUILayout.Width(48)))
                            {
                                BlueSeaSystem.CancelOrder(item);
                                m_orderList = BlueSeaSystem.GetOrders();
                                return;
                            }
                            GUI.backgroundColor = Color.white;
                        }
                    }
                    else
                    {
                        GUI.enabled = false;
                        if (stateType == "2")
                        {
                            GUI.backgroundColor = Color.green;
                            GUILayout.Button("更新成功", GUILayout.Width(100));
                            GUI.backgroundColor = Color.white;
                        }
                        else if (stateType == "3")
                        {
                            GUI.backgroundColor = Color.red;
                            GUILayout.Button("更新失败", GUILayout.Width(100));
                            GUI.backgroundColor = Color.white;
                        }
                        else
                            GUILayout.Button(stateType, GUILayout.Width(100));
                        GUI.enabled = true;
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
    }

    /// <summary>
    /// 计算热更大小 UI
    /// </summary>
    private void OnCalcSizeGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("源版本号："+m_oldVersion+"       需更新大小:" + m_updateFileSize);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("升级到本地FileList所需大小"))
        {
            try
            {
                m_oldVersion = "";
                var lines = File.ReadAllLines(RESOURCE_ONLINE_PATH + GetUpdateDirName() + CHAR_SEP + "Version.txt");
                for (int i = 0; i < lines.Length; i++)
                {
                    var arr = lines[i].Split('=');
                    if (arr[0] == "Version")
                    {
                        m_oldVersion = arr[1];
                        var fileList = File.ReadAllText(RESOURCE_ONLINE_PATH + GetUpdateDirName() + CHAR_SEP + arr[1] + CHAR_SEP + "VersionInfo" + CHAR_SEP + "FileList.txt");
                        m_oldFileList = fileList;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            var oldFileListDic = PluginUtil.ReadSimpleConfig(m_oldFileList);
            var newFileListDic = PluginUtil.ReadSimpleConfig(File.ReadAllText(PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/FileList.txt"));
            var newFileSizeDic = PluginUtil.ReadSimpleConfig(File.ReadAllText(PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/FileSize.txt"));

            int fileSize = 0;
            changeFileSizeList.Clear();
            foreach (var fileName in newFileListDic.Keys)
            {
                if (m_oldFileList.Contains(fileName) && newFileListDic[fileName] == oldFileListDic[fileName])
                    continue;
                fileSize += int.Parse(newFileSizeDic[fileName]);
                changeFileSizeList.Add(new KeyValue<string, int>() { key = fileName, value = int.Parse(newFileSizeDic[fileName]) });
            }

            float size = fileSize / 1024f;
            var sizeUnit = "KB";
            if (size >= 1024)
            {
                size = size / 1024f;
                sizeUnit = "MB";
            }
            m_updateFileSize = size.ToString("0.##")+sizeUnit;

            changeFileSizeList.Sort((a, b) => b.value - a.value);
        }

        m_scrollVector = GUILayout.BeginScrollView(m_scrollVector, GUILayout.Width(450), GUILayout.Height(300));
        for (int i = 0; i < changeFileSizeList.Count; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(changeFileSizeList[i].key, GUILayout.Width(370));
            GUILayout.Label((changeFileSizeList[i].value / 1024) + " KB");
            GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
    }

    

    /// <summary>
    /// 获取上次打包到现在的版本的变更列表
    /// </summary>
    /// <param name="nowSvnVersion"></param>
    /// <param name="update"></param>
    /// <returns></returns>
    private string GetDiffFileList(out int nowSvnVersion, bool update = true)
    {
        jschange_index = new List<string>();
        GAME_JS_CHANGE = false;
        CSS_CHANGE = false;
        OTHER_JS_CHANGE = false;
        EditorUtility.DisplayProgressBar("svn更新", "", 0.5f);
        if (update)
            PluginUtil.Excute("svn", "update", true, true, true);
        var result = PluginUtil.Excute("svn", "info", true, true, true);
        nowSvnVersion = int.Parse(Regex.Match(result, @"Revision: (\d+)").Groups[1].Value);
        var lastSvnVersion = int.Parse(File.ReadAllText(m_buildVersionPath));
//        if (lastSvnVersion + 1 == nowSvnVersion)
//        {
//            Debug.Log("没有新资源");
//            return "";
//        }

        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

        EditorUtility.DisplayProgressBar("获取变更文件列表", string.Format("变更版本：{0} - {1}", lastSvnVersion, nowSvnVersion), 1f);
        result = PluginUtil.Excute("svn", string.Format("log -v -r {0}:{1}", lastSvnVersion, nowSvnVersion), true, true);
        var pathDic = new Dictionary<string, bool>();
        var regex = new Regex(@"   . (.+?)(?= \(from.+|\r|\n)", RegexOptions.Compiled);
        var match = regex.Match(result);
        while (match.Success)
        {
            var path = match.Groups[1].Value;
            if (!pathDic.ContainsKey(path))
                pathDic.Add(path, true);
            match = match.NextMatch();
        }

        var strBuild = new StringBuilder();
        foreach (var kv in pathDic)
        {
            strBuild.AppendLine(kv.Key);
        }
        EditorUtility.ClearProgressBar();
        if (m_buildTarget == BuildTarget.WebPlayer)
        {
            string paths = strBuild.ToString();
            if (paths.Contains(WEB_GAME_JS_PATH))
            {
                GAME_JS_CHANGE = true;
            }
            if (paths.Contains(WEB_STYLE_CSS_PATH))
            {
                CSS_CHANGE = true;
            }
            if (paths.Contains(WEB_JS_ROOT_PATH))
            {
                OTHER_JS_CHANGE = true;
                var list = paths.Split(new char[] { '\n' });
                for (int i = 0; i < list.Length; i++)
                {
                    if (list[i].Contains(WEB_JS_ROOT_PATH))
                    {
                        string path = list[i].Replace(WEB_JS_ROOT_PATH + "/", "");
                        path = path.Replace("\r", "");
                        jschange_index.Add(path);
                    }
                }
            }
            CheckWebVersion();
        }
        return strBuild.ToString();
    }

    /// <summary>
    /// 仅打包
    /// </summary>
    /// <returns></returns>
    private bool OnlyBuildAb(string curVersion = null)
    {
        if (m_buildCode && m_buildTarget != BuildTarget.WebPlayer)
        {
            var definesList = new List<string>();
            var config = m_configFile.m_dataArr[m_selectIndex];
            if (!string.IsNullOrEmpty(config.SDKName))
            {
                Debug.Log("Add ActiveSDK");
                definesList.Add("ActiveSDK");
            }
            if (!String.IsNullOrEmpty(config.Lang))
            {
                definesList.Add("LANG_" + config.Lang);
            }
            definesList.Add("Assetbundle");
            var definesArr = definesList.Count == 0 ? null : definesList.ToArray();
            if (!BuildAB.BuildDll("UnityVS.UpdateWorker.CSharp.xml", "UpdateWorker.dll.bytes", config.Platform, definesArr, null, m_buildDiffTest))
                return false;
            if (!BuildAB.BuildDll("UnityVS.client.CSharp.csproj", "Client.dll.bytes", config.Platform, definesArr, null, m_buildDiffTest))
                return false;

            //混淆和压缩
            var destDir = PluginUtil.GetEditorStreamingAssetsLibPath() + "Managed/";
            Directory.CreateDirectory(destDir);
            //string[] dllPath = new string[] { Path.Combine(destDir ,"UpdateWorker.dll.bytes"), Path.Combine(destDir ,"Client.dll.bytes") };
            //string[] dllName = new string[] { "UpdateWorker.dll.bytes", "Client.dll.bytes" };
            string[] dllPath = new string[] { Path.Combine(destDir ,"Client.dll.bytes") };
            string[] dllName = new string[] { "Client.dll.bytes" };

            if(m_obfuscateDll)
                BuildAB.ObfuscateDll(dllPath, dllName, curVersion);

            if (m_encryptDll)
                BuildAB.EncryptDll(dllPath[0]);

            BuildAB.CompressDll(dllPath[0], dllName[0], m_buildDiffTest);
        }
        
        if (m_buildConfig)
        {
            if (!BuildAB.BuildConfigs(m_buildDiffTest, true, m_isBuildCryptedCfg))
                return false;
        }
        if (m_buildResource)
        {
            //增量更新就不用删除原有ab包
            if (!m_buildDiff && !m_buildDiffTest)
            {
                var dirArr = Directory.GetDirectories(PluginUtil.GetEditorStreamingAssetsLibPath());
                for (int i = 0; i < dirArr.Length; i++)
                {
                    var path = PluginUtil.GetUnifyPath(dirArr[i]);
                    if (path == "Managed" || path == "VersionInfo") //跳过代码和FileList
                        continue;
                    FileUtil.DeleteFileOrDirectory(dirArr[i]);
                }

                var fileArr = Directory.GetFiles(PluginUtil.GetEditorStreamingAssetsLibPath());
                for (int i = 0; i < fileArr.Length; i++)
                {
                    var path = PluginUtil.GetUnifyPath(fileArr[i], true);
                    if (path == "Config.assetbundle") //跳过配置文件
                        continue;
                    FileUtil.DeleteFileOrDirectory(fileArr[i]);
                }
            }

            if (!BuildAB.BuildResources(m_buildDiff ? m_diffFileList : null, m_buildDiffTest))
                return false;
        }

        BuildAB.BuildUpdateLog();
        BuildAB.BuildFileList(m_buildTarget.ToString(), curVersion);

        return true;
    }

    /// <summary>
    /// 仅发布
    /// </summary>
    private void OnlyPublish(BuildVersionType useVersionType)
    {
        var configLine = m_configFile.m_dataArr[m_selectIndex];
        PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsPath());

        FileUtil.DeleteFileOrDirectory(PluginUtil.GetEditorStreamingAssetsPath().TrimEnd('/'));
        if (m_buildTarget == BuildTarget.StandaloneWindows)
        {
            PluginUtil.Excute(@"AutoBuild\pc\CopyRes.bat", "");
        }
        else
        {
            CopyBundleToStreamingAssets();
        }

        RemoveResources();
        if (!m_buildDebugErrorMode && m_buildTarget != BuildTarget.iPhone && m_buildTarget != BuildTarget.WebPlayer)
            RemoveScripts();
        AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

        try
        {
            var platList = new List<string>();
            if (IsAndroidSdk())
            {
                for (int i = 0; i < m_buildChannels.Count; i++)
                {
                    if (m_buildChannels[i].value)
                        platList.Add(m_buildChannels[i].key);
                }
            }
            var buildParams = new BuildParams()
            {
                buildVersionType = useVersionType,

                corner = m_buildCorner,
                splash = m_buildSplash,
                platforms = platList.ToArray(),

                developmentMode = m_buildDevelopmentMode,
                debugErrorMode = m_buildDebugErrorMode,

                isEncrypt = m_encryptDll,
                isObfuscar = m_obfuscateDll,
                lang = configLine.Lang
            };
            AutoBuild.Build(m_buildConfigKeyArr[m_selectIndex], buildParams);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            RecoverResources();
            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
            if (!m_buildDebugErrorMode && m_buildTarget != BuildTarget.iPhone && m_buildTarget != BuildTarget.WebPlayer)
                RecoverScripts();
            PluginUtil.ClearDirectory(PluginUtil.GetEditorStreamingAssetsPath());
            //SvnOperation.ExeSvnRevert("ProjectSettings/EditorBuildSettings.asset");
            //SvnOperation.ExeSvnRevert("Assets/Scripts.meta", true);
            //SvnOperation.ExeSvnRevert("Assets/StreamingAssets.meta", true);
        }
    }

    private void ClearChangeType()
    {
        m_buildAll = false;
        m_buildCode = false;
        m_buildConfig = false;
        m_buildResource = false;
        m_diffFileList = "";
        m_buildDiff = false;
        m_buildSmallPkg = false;
    }

    /// <summary>
    /// 检查哪些类型变更了
    /// </summary>
    private void CheckChangeType()
    {
        m_buildCode = false;
        m_buildConfig = false;
        m_buildResource = false;
        var fileArr = m_diffFileList.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < fileArr.Length; i++)
        {
            if (fileArr[i].IndexOf("Assets/", StringComparison.Ordinal) == -1)
                continue;
            var path = PluginUtil.GetProjectPath(fileArr[i]);
            if (path.StartsWith("Assets/Scripts/") || path.StartsWith("Assets/Plugins/"))
                m_buildCode = true;
            if (path.StartsWith("Assets/Resources/Config/"))
                m_buildConfig = true;
            else if (path.StartsWith("Assets/Resources/") || path.StartsWith("Assets/ResourcesLib/"))
                m_buildResource = true;
        }
    }

    private void OneKeyBuildAB(bool update)
    {
        int nowSvnVersion;
        m_diffFileList = GetDiffFileList(out nowSvnVersion, update);
        HandleLangRes();
        m_buildDiff = !string.IsNullOrEmpty(m_diffFileList);
        CheckChangeType();

        if (m_buildDiff)
        {
            
            if (OnlyBuildAb())
            {
                //读取并修改本地打包资源的svn版本号
                var lastSvnVersion = File.ReadAllText(m_buildVersionPath);
                File.WriteAllText(m_buildVersionPath, nowSvnVersion.ToString());

                Debug.Log(string.Format("[打包] {0} [{1}-{2}] {3}", "仅打包", lastSvnVersion, nowSvnVersion, EditorUserBuildSettings.activeBuildTarget));
            }
        }


        ClearChangeType();
    }


    private void HandleLangRes(bool translate = true,bool resourcelib = true)
    {
        SvnOperation.ExeSvnRevert(Application.dataPath + "/Resources", true);
        SvnOperation.ExeSvnRevert(Application.dataPath + "/ResourcesLib", true);

        if (!m_language.Equals(LanguageConst.LANG_ZH))
        {
            
            //复制海外基础资源
            CopyFile(Application.dataPath + "/../LangRes/oversea/Resources", Application.dataPath + "/Resources");

            //复制海外对应语种资源
            CopyFile(Application.dataPath + "/../LangRes/" + m_language + "/Resources", Application.dataPath + "/Resources");
            if (resourcelib)
            {
                CopyFile(Application.dataPath + "/../LangRes/oversea/ResourcesLib", Application.dataPath + "/ResourcesLib");

                //复制海外对应语种资源
                CopyFile(Application.dataPath + "/../LangRes/" + m_language + "/ResourcesLib", Application.dataPath + "/ResourcesLib");
            }

            //选择对应语种翻译文本
            CopyFile(Application.dataPath + "/../../tool/translate/csv/" + m_language,
                Application.dataPath + "/../../tool/translate");
            if (translate)
            {
                var p = new Process();
                var exeName = "../tool/translate/translator.exe";
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(Path.GetFullPath(exeName));
                PluginUtil.Excute(p, Path.GetFileName(exeName), "translate.csv", false, false, true);
                EditLanguageDiffFileList();
            }
        }

    }


    private void EditLanguageDiffFileList()
    {
        var commitFileList = SvnOperation.GetCommitFileList(".", "/trunk/client/");
        var fileArr = m_diffFileList.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
       
        for (int i = 0; i < fileArr.Length; i++)
        {
            if (commitFileList.Contains(fileArr[i]))
            {
                continue;
            }
            commitFileList.Add(fileArr[i]);
        }
        m_diffFileList = (String.Join("\n", commitFileList.ToArray()));
    }

    private void CopyFile(string sourceDir, string destDir, bool recursive = true)
    {
        if (!Directory.Exists(sourceDir))
        {
            return;
        }
        var searchOption = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
        foreach (var dir in Directory.GetDirectories(sourceDir, "*", searchOption))
        {
            var targetName = dir.Replace(sourceDir, destDir);
            if (!Directory.Exists(targetName))
            {
                Directory.CreateDirectory(targetName);
            }
        }
        foreach (var file in Directory.GetFiles(sourceDir, "*", searchOption))
        {
            var targetName = file.Replace(sourceDir, destDir);
            File.Copy(file, targetName, true);
        }
    }

    /// <summary>
    /// 一键准备测试热更资源
    /// </summary>
    private void OneKeyHotUpdateTestBuild()
    {
        var dirName = GetUpdateDirName();
        var path = RESOURCE_ONLINE_PATH + dirName + CHAR_SEP;
        EditorUtility.DisplayProgressBar("检查资源...", "", 0.1f);

        var result = PluginUtil.Excute("svn", "status ../", true, true, true);
        if (!string.IsNullOrEmpty(result))
        {
            Debug.LogError("本地工作目录中有修改");
            EditorUtility.ClearProgressBar();
            return;
        }

        var configLine = m_configFile.m_dataArr[m_selectIndex];
        int nowSvnVersion;
        m_diffFileList = GetDiffFileList(out nowSvnVersion);
        HandleLangRes();
        m_buildDiff = !string.IsNullOrEmpty(m_diffFileList);
        CheckChangeType();

        if (m_buildDiff)
        {
            //读取在线PrepareVersion版本号
            //读取并修改本地打包资源的svn版本号
            var lastSvnVersion = File.ReadAllText(m_buildVersionPath);
            var versionDic = PluginUtil.ReadSimpleConfig(File.ReadAllText(path + "Version.txt"));
            var lastGameVersion = versionDic["PrepareVersion"];
            var nowGameVersion = GetNextGameVersion(lastGameVersion, BuildVersionType.Next);

            EditorUtility.DisplayProgressBar("准备打包资源...", "", 0.2f);
            
            if (OnlyBuildAb(nowGameVersion))
            {
                ClearChangeType();
                File.WriteAllText(m_buildVersionPath, nowSvnVersion.ToString());

                //svn全部提交
                EditorUtility.DisplayProgressBar("提交svn...", "", 0.5f);
                if (!SvnOperation.ExeSvnCommitAll("../", string.Format("[发版] {0} [{1}-{2}] {3}", nowGameVersion, lastSvnVersion, nowSvnVersion, EditorUserBuildSettings.activeBuildTarget)))
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }

                var buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), configLine.Platform);
                if (buildTarget == BuildTarget.Android || buildTarget == BuildTarget.iPhone || buildTarget == BuildTarget.StandaloneWindows)
                {
                    //尝试删除旧的临时资源
                    EditorUtility.DisplayProgressBar("删除旧临时资源中，请稍后...", "", 0.6f);
                    if (lastGameVersion.Split('.').Length == 5 && Directory.Exists(path + lastGameVersion))
                        FileUtil.DeleteFileOrDirectory(path + lastGameVersion);

                    //复制资源到在线目录
                    EditorUtility.DisplayProgressBar("复制资源中，请稍后...", "", 0.7f);
                    FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath().TrimEnd(new[] { '/', '\\' }), path + nowGameVersion);
                }
                else if (buildTarget == BuildTarget.WebPlayer)
                {
                    //打整包
                    EditorUtility.DisplayProgressBar("Web版本发布...", "", 0.6f);
                    OnlyPublish(BuildVersionType.Current);

                    //尝试删除旧的资源
                    EditorUtility.DisplayProgressBar("删除旧临时资源中，请稍后...", "", 0.8f);
                    if (Directory.Exists(path + configLine.PackageName))
                        FileUtil.DeleteFileOrDirectory(path + configLine.PackageName);

                    //复制资源到在线目录
                    EditorUtility.DisplayProgressBar("复制资源中，请稍后...", "", 0.9f);
                    FileUtil.CopyFileOrDirectory(configLine.OutputPath.TrimEnd(new[] { '/', '\\' }) + "\\" + configLine.PackageName, path + configLine.PackageName);
                }
                else
                    Debug.LogError("未处理的平台："+buildTarget);

                //修改在线版本号
                var lines = File.ReadAllLines(path + "Version.txt");
                for (int i = 0; i < lines.Length; i++)
                {
                    var arr = lines[i].Split('=');
                    if (arr[0] == "PrepareVersion")
                    {
                        lines[i] = "PrepareVersion=" + nowGameVersion;
                        Debug.Log("已修改PrepareVersion为：" + nowGameVersion);
                        break;
                    }
                }
                File.WriteAllLines(path + "Version.txt", lines);

                //提交到数传平台
//                PluginUtil.Excute(@"AutoBuild\UploadToDts.bat",
//                    string.Format("{0}/{1} {0}/Version.txt", dirName,
//                        m_buildTarget == BuildTarget.WebPlayer ? configLine.PackageName : nowGameVersion));

                //蓝海发布
                Debug.Log("热更资源准备完毕！请手动上传到数传平台审核，再用蓝海系统发布");
            }
        }
        else
            Debug.Log("没有新资源需要打包");
        EditorUtility.ClearProgressBar();
    }

    public void oneKeyPublishHotTestData()
    {
        OneKeyHotUpdateTestBuild();
        UpdateDtsAuto(TEST_MENU_INDEX);
    }

    



    /// <summary>
    /// 一键将测试资源转为正式资源
    /// </summary>
    private void OneKeyChangeTestToOfficial()
    {
        var dirName = GetUpdateDirName();
        var path = RESOURCE_ONLINE_PATH + dirName + CHAR_SEP;

        //获取在线PrepareVersion版本号
        var versionDic = PluginUtil.ReadSimpleConfig(File.ReadAllText(path + "Version.txt"));
        var lastPrepareGameVersion = versionDic["PrepareVersion"];
        var lastGameVersion = versionDic["Version"];
        var nowGameVersion = lastPrepareGameVersion;    //现在测试版本号和正式版本号为同一套规则，减少发版次数

        var configLine = m_configFile.m_dataArr[m_selectIndex];
        var buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), configLine.Platform);
        if (buildTarget == BuildTarget.Android || buildTarget == BuildTarget.iPhone || buildTarget == BuildTarget.StandaloneWindows)
        {
            //修改在线资源目录名为正式的
//            try
//            {
//                FileUtil.MoveFileOrDirectory(path + lastPrepareGameVersion, path + nowGameVersion);
//            }
//            catch (IOException e)
//            {
//                Debug.LogException(e);
//                return;
//            }
        }
        else if (buildTarget == BuildTarget.WebPlayer)
        {
            //修改在线资源目录名为正式的
            try
            {
                if (Directory.Exists(path + configLine.PackageName))
                {
                    if (Directory.Exists(path + lastGameVersion))
                    {
                        EditorUtility.DisplayProgressBar("删除" + lastGameVersion+"...", "", 0.5f);
                        FileUtil.DeleteFileOrDirectory(path + lastGameVersion);
                        EditorUtility.ClearProgressBar();
                    }

                    EditorUtility.DisplayProgressBar("复制" + configLine.PackageName + "到game...", "", 0.5f);
                    Debug.Log("原game目录备份为" + lastGameVersion);
                    FileUtil.MoveFileOrDirectory(path + "game", path + lastGameVersion);
                    FileUtil.CopyFileOrDirectory(path + configLine.PackageName, path + "game");
                    EditorUtility.ClearProgressBar();
                }
                else
                    Debug.LogWarning("未发现要发布的资源，仅更改版本号：" + path + configLine.PackageName);
            }
            catch (IOException e)
            {
                Debug.LogException(e);
                return;
            }
        }
        else
            Debug.LogError("未处理的平台：" + buildTarget);
        

        //修改在线版本号
        var lines = File.ReadAllLines(path + "Version.txt");
        for (int i = 0; i < lines.Length; i++)
        {
            var arr = lines[i].Split('=');
            if (arr[0] == "PrepareVersion")
            {
                lines[i] = "PrepareVersion=" + nowGameVersion;
                Debug.Log("已修改在线版本号PrepareVersion为：" + nowGameVersion);
            }
            else if (arr[0] == "Version")
            {
                lines[i] = "Version=" + nowGameVersion;
                Debug.Log("已修改在线版本号Version为：" + nowGameVersion);
            }
        }
        File.WriteAllLines(path + "Version.txt", lines);

        //重新生成filelist文件
        BuildAB.BuildFileList(buildTarget.ToString(), nowGameVersion);

        //提交到数传平台
//        PluginUtil.Excute(@"AutoBuild\UploadToDts.bat",
//            string.Format("{0}/{1} {0}/Version.txt", dirName,
//                m_buildTarget == BuildTarget.WebPlayer ? "game" : nowGameVersion));

        //蓝海发布
        Debug.Log("热更资源准备完毕！请手动上传到数传平台审核，再用蓝海系统发布");
    }

    /// <summary>
    /// 一键发布apk
    /// </summary>
    private void OneKeyPlatformApkPublic(BuildVersionType useVersionType)
    {
        m_buildDiff = false;
        m_diffFileList = "";

        m_buildDiff = false;
        m_buildCode = false;
        m_buildConfig = false;
        m_buildResource = false;

        for (int i = 0; i < m_buildChannels.Count; i++)
        {
            m_buildChannels[i].value = true;
        }

        var configLine = m_configFile.m_dataArr[m_selectIndex];
        m_buildCorner = configLine.Corner;
        m_buildSplash = configLine.Splash;

        OnlyPublish(useVersionType);
    }

    private void OneKeySetPortState(bool open)
    {
        var dirName = GetUpdateDirName();
        var path = RESOURCE_ONLINE_PATH + dirName + CHAR_SEP;
        //修改所有服务器入口状态
        var config = CreateInstance<ConfigServerListData>();
        config.Load(File.ReadAllText(path + "serverlist"));
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            config.m_dataArr[i].Tag = open ? 0 : 1;
        }

        File.WriteAllText(path + "serverlist", config.Export());
        Debug.Log("所有服务器入口：" + (open ? "开" : "关"));

        //提交到数传平台
//        PluginUtil.Excute(@"AutoBuild\UploadToDts.bat", string.Format("{0}/serverlist", dirName));
        //蓝海发布
        Debug.Log("热更资源准备完毕！请手动上传到数传平台审核，再用蓝海系统发布");
    }

    /// <summary>
    /// 获取下一个版本的版本号
    /// </summary>
    /// <param name="gameVersion"></param>
    /// <param name="versionType"></param>
    /// <returns></returns>
    public static string GetNextGameVersion(string gameVersion, BuildVersionType versionType)
    {
        var arr = gameVersion.Split('.');
        var length = arr.Length;
        if (length != 4)
        {
            Debug.LogError("版本号位数非法：" + gameVersion);
            return "";
        }

        if (versionType == BuildVersionType.Next)
        {
            var buildCode = int.Parse(arr[3]) + 1;
            arr[3] = buildCode.ToString("000");
            return string.Join(".", arr);
        }
        if (versionType == BuildVersionType.NextBig)
        {
            var code = int.Parse(arr[0] + arr[1] + arr[2]) + 1;
            arr[0] = (code / 100 % 10).ToString();
            arr[1] = (code / 10 % 10).ToString();
            arr[2] = (code / 1 % 10).ToString();
            arr[3] = "000";
            return string.Join(".", arr);
        }
        return gameVersion;
    }

    public static string GetNextBigGameVersion(string bigVersion, BuildVersionType versionType)
    {
        if (versionType == BuildVersionType.NextBig)
        {
            var code = int.Parse(bigVersion) + 1;
            return code.ToString();
        }
        return bigVersion;
    }

    private void GetParamsAndLog(int modeIndex, out string inputParams, out string log)
    {
        var officialVersion = "";
        var testVersion = "";
        var configLine = m_configFile.m_dataArr[m_selectIndex];
        var rootDirName = GetUpdateDirName();
        if (modeIndex == OFFICIAL_MENU_INDEX || modeIndex == TEST_MENU_INDEX)
        {
            try
            {
                var lines = File.ReadAllLines(string.Format(RESOURCE_ONLINE_PATH + rootDirName + CHAR_SEP + "Version.txt"));
                for (int i = 0; i < lines.Length; i++)
                {
                    var arr = lines[i].Split('=');
                    if (arr[0] == "Version")
                        officialVersion = arr[1];
                    else if (arr[0] == "PrepareVersion")
                        testVersion = arr[1];
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        if (modeIndex == ALL_MENU_INDEX)
        {
            log = "全部";
            inputParams = rootDirName;
        }
        else if (modeIndex == OFFICIAL_MENU_INDEX)
        {
            log = "发版-" + officialVersion;
            inputParams = string.Format("{0}/{1},{0}/Version.txt", rootDirName, m_buildTarget == BuildTarget.WebPlayer ? "game" : officialVersion);
        }
        else if (modeIndex == TEST_MENU_INDEX)
        {
            log = "发版-" + testVersion;
            inputParams = string.Format("{0}/{1},{0}/Version.txt", rootDirName, m_buildTarget == BuildTarget.WebPlayer ? configLine.PackageName : testVersion);
        }
        else if (modeIndex == SERVERLIST_INDEX)
        {
            log = "服务器列表";
            if (m_buildTarget != BuildTarget.iPhone)
            {
                inputParams = string.Format("{0}/serverlist,{0}/serverlistTest,{0}/serverlistCheck", rootDirName);
            }
            else
            {
                inputParams = string.Format("{0}/serverlist,{0}/serverlistTest,{0}/serverlistAppStore", rootDirName);
            }
        }
        else if (modeIndex == VERSION_MENU_INDEX)
        {
            log = "版本文件";
            inputParams = string.Format("{0}/Version.txt", rootDirName);
        }
        else if (modeIndex == CLOSE_SERVER_INDEX)
        {
            log = "关入口";
            inputParams = rootDirName + "/serverlist";
        }
        else if (modeIndex == OPEN_SERVER_INDEX)
        {
            log = "开入口";
            inputParams = rootDirName + "/serverlist";
        }
        else if (modeIndex == VERSION_CHECK)
        {
            log = "测试版本文件";
            inputParams = rootDirName + "/VersionCheckList.txt";
        }
        else
        {
            log = "";
            inputParams = rootDirName;
        }

        log += "-" + rootDirName;
    }

    private void CopyBundleToStreamingAssets()
    {
        // 只有手机可以有大小包
        if ((m_buildTarget == BuildTarget.Android || m_buildTarget == BuildTarget.iPhone) && m_buildSmallPkg)
        {
            CopyMiniBundleToStreamingAssets();
        }
        else
        {
            FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath().TrimEnd('/'), PluginUtil.GetEditorStreamingAssetsPath().TrimEnd('/'));
        }
        
        if (m_buildTarget == BuildTarget.iPhone || m_buildTarget == BuildTarget.Android)
        {
            var mapList = Resources.Load<TextAsset>("Config/ConfigMapList").text;
            var configMapList = ScriptableObject.CreateInstance<ConfigMapList>();
            configMapList.Load(mapList);
            var packageOutsideMapPathList = new List<string>();
            for (int i = 0; i < configMapList.m_dataArr.Length; i++)
            {
                if (configMapList.m_dataArr[i].PackageOutside)
                    packageOutsideMapPathList.Add("Scenes/map_" + configMapList.m_dataArr[i].MapId + "/");
            }
            var packageOutsideMapPathArr = packageOutsideMapPathList.ToArray();
            var scenesDir = Directory.GetDirectories(PluginUtil.GetEditorStreamingAssetsPath() + "Scenes/");
            for (int i = 0; i < scenesDir.Length; i++)
            {
                var path = PluginUtil.GetUnifyPath(scenesDir[i]) + "/";
                if (path.StartsWithAny(packageOutsideMapPathArr) && Directory.Exists(scenesDir[i]))
                    FileUtil.DeleteFileOrDirectory(scenesDir[i]);
            }
        }

        
    }


    private void CopyMiniBundleToStreamingAssets()
    {
        if (!Directory.Exists(PluginUtil.GetEditorStreamingAssetsPath()))
        {
            Directory.CreateDirectory(PluginUtil.GetEditorStreamingAssetsPath());
        }
        var arr = new[]
        {
            "Managed",
//            "VersionInfo",
            "Atlas/UpdateWorkerBg.assetbundle",
            "Atlas/UpdateWorker.assetbundle",
            "AtlasAlpha/UpdateWorker_a.assetbundle",
            "Shader/SpriteETC.assetbundle",
            "UI/Update/PanelUpdateBg.assetbundle",
            "UI/Update/PanelUpdate.assetbundle",
            "Depend.assetbundle",
            "Config.assetbundle",
            "LuaRoot.assetbundle",
            "Font/Normal.assetbundle"
        };

       

        for (int i = 0; i < arr.Length; i++)
        {
            string directoryName = PluginUtil.GetEditorStreamingAssetsPath() + Path.GetDirectoryName(arr[i]);
            if (arr[i].IndexOf("/") != -1 && !Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            FileUtil.CopyFileOrDirectory(PluginUtil.GetEditorStreamingAssetsLibPath() + arr[i],
                PluginUtil.GetEditorStreamingAssetsPath() + arr[i]);
        }

        //小包需要缩减版的FileList和FileSize,置空的话会导致UpdateWorker报错
        Directory.CreateDirectory(PluginUtil.GetEditorStreamingAssetsPath() + "VersionInfo");
        var newFileLines = new string[3];

        var fileListLines = File.ReadAllLines(PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/FileList.txt");
        Array.Copy(fileListLines, newFileLines, 3);
        File.WriteAllLines(PluginUtil.GetEditorStreamingAssetsPath() + "VersionInfo/FileList.txt", newFileLines);

        var fileSizeLines = File.ReadAllLines(PluginUtil.GetEditorStreamingAssetsLibPath() + "VersionInfo/FileSize.txt");
        Array.Copy(fileSizeLines, newFileLines, 3);
        File.WriteAllLines(PluginUtil.GetEditorStreamingAssetsPath() + "VersionInfo/FileSize.txt", newFileLines);

        //还原下原来的目录结构
        foreach (var dir in Directory.GetDirectories((PluginUtil.GetEditorStreamingAssetsLibPath())))
        {
            var dirName = dir.Replace(PluginUtil.GetEditorStreamingAssetsLibPath(),
                PluginUtil.GetEditorStreamingAssetsPath());
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
        }
    }

    private void RemoveResources()
    {
        AssetDatabase.RenameAsset("Assets/Resources", "ResourcesTmp");
    }

    private void RecoverResources()
    {
        AssetDatabase.RenameAsset("Assets/ResourcesTmp", "Resources");
    }

    private void RemoveScripts()
    {
        AssetDatabase.MoveAsset("Assets/Scripts", "Assets/Editor/Scripts");
    }

    private void RecoverScripts()
    {
        AssetDatabase.MoveAsset("Assets/Editor/Scripts", "Assets/Scripts");
    }

    private int m_dtsStartTime;
    private string m_dtsUpdatingId = "";

    private int m_bssStartTime;
    private string m_bssUpdatingId = "";
    private void Update()
    {
        if (m_dtsStartTime > 0)
        {
            if (TimeUtil.GetNowTimeStamp() - m_dtsStartTime >= 5)
            {
                Debug.Log(string.Format("[{0}]任务状态查询中...", m_dtsUpdatingId));
                m_dtsStartTime = TimeUtil.GetNowTimeStamp();
                m_taskList = DataTransmissionSystem.GetTasks();
                Repaint();

                for (int i = 0; i < m_taskList.Count; i++)
                {
                    if (m_taskList[i][2] == m_dtsUpdatingId && m_taskList[i][3] == "发版成功")
                    {
                        m_dtsStartTime = 0;
                        Debug.Log("【数传发布完毕】\n========================================================");
                        break;
                    }
                }
                if (true)
                {
                    for (int i = 0; i < m_taskList.Count; i++)
                    {
                        if (m_taskList[i][3] == "未发版"&&m_taskList[i][1].Contains("cdn"))
                        {
                            DataTransmissionSystem.PerformTask(m_taskList[i][2]);
                        }
                    }
                    cdnready = true;
                    for (int i = 0; i < m_taskList.Count; i++)
                    {
                        if (m_taskList[i][3] != "发版成功" && m_taskList[i][1].Contains("cdn"))
                        {
                            cdnready = false;
                        }
                    }


                }
            } 
        }

        if (m_bssStartTime > 0)
        {
            if (TimeUtil.GetNowTimeStamp() - m_bssStartTime >= 5)
            {
                Debug.Log(string.Format("[{0}]订单状态查询中...", m_bssUpdatingId));
                m_bssStartTime = TimeUtil.GetNowTimeStamp();
                m_orderList = BlueSeaSystem.GetOrders();
                Repaint();

                for (int i = 0; i < m_orderList.Count; i++)
                {
                    if (m_orderList[i][4] == m_bssUpdatingId && m_orderList[i][0] != "1")
                    {
                        m_bssStartTime = 0;
                        Debug.Log("【蓝海热更完毕】\n========================================================");
                        break;
                    }
                }

                if (cdnready)
                {
                    for (int i = 0; i < m_orderList.Count; i++)
                    {
                        if (m_orderList[i][0] == "0")
                        {
                            BlueSeaSystem.PerformOrder(m_orderList[i][4]);
                        }
                    }
                }
            } 
        }
    }

    private bool IsAndroidSdk()
    {
        var config = m_configFile.m_dataArr[m_selectIndex];
        return !string.IsNullOrEmpty(config.SDKName) && config.Platform == "Android";
    }

    private void OnDestroy()
    {
        if (m_configFile != null)
            DestroyImmediate(m_configFile);

        m_bssStartTime = 0;
        m_bssUpdatingId = "";

        m_dtsStartTime = 0;
        m_dtsUpdatingId = "";
    }
}
