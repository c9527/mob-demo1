﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Sprites;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class AtlasBuild
{
    public static bool GenerateEtcTex(string dir)
    {
        var buildTarget = BuildAB.GetBuildTarget();
        var dirSplit = dir.Split('/');
        var dirName = dirSplit[dirSplit.Length - 1];
        var atlasAlphaPath = string.Format("{0}/ResourcesLib/AtlasAlpha/{1}/{2}_a", Application.dataPath, buildTarget, dirName);

        var atlasMap = new Dictionary<Texture2D, List<Sprite>>();
        var atlases = Packer.GetTexturesForAtlas(dirName);
        for (int i = 0; i < atlases.Length; ++i)
        {
            var atlas = atlases[i];
            atlas.name = dirName + "_" + i;
            atlasMap.Add(atlas, new List<Sprite>());
        }
        AssetDatabase.SaveAssets();

        var filePaths = PluginUtil.GetFiles(dir);
        for (int i = 0; i < filePaths.Length; ++i)
        {
            var filePathItem = filePaths[i];
            if (filePathItem.EndsWith(".meta"))
                continue;

            var shortPath = filePathItem.Replace(Application.dataPath, "");
            var sp = AssetDatabase.LoadAssetAtPath(shortPath, typeof(Sprite)) as Sprite;
            string atlasName;
            Texture2D atlasTexture;
            if (sp == null)
            {
                Debug.LogWarning("未找到指定的Sprite:"+ "\n" + filePathItem);
                continue;
            }
                
            Packer.GetAtlasDataForSprite(sp, out atlasName, out atlasTexture);
            if (null == atlasTexture)
            {
                Debug.LogWarning(string.Format("{0}: 找不到图集!", sp.name));
                continue;
            }

            var list = atlasMap[atlasTexture];
            list.Add(sp);
        }
        if (!Directory.Exists(atlasAlphaPath))
            Directory.CreateDirectory(atlasAlphaPath);
        else
        {
            var arr = PluginUtil.GetFiles(atlasAlphaPath);
            for (int i = 0; i < arr.Length; i++)
            {
                var dirSplit2 = arr[i].Split('/');
                var dirName2 = dirSplit2[dirSplit2.Length - 1];
                var localIndex = int.Parse(dirName2.Split(new char[] { '_', '.' })[1]);
                if (localIndex >= atlases.Length)
                    FileUtil.DeleteFileOrDirectory(arr[i]);
            }
        }

        for (int i = 0; i < atlases.Length; ++i)
        {
            var atlasTexture = atlases[i];
            var list = atlasMap[atlasTexture];
            var atlasWidth = atlasTexture.width;
            var atlasHeight = atlasTexture.height;
            var tex = new Texture2D(atlasWidth, atlasHeight, TextureFormat.RGB24, false);
            var colors = tex.GetPixels();
            for (int j = 0; j < colors.Length; ++j)
                colors[j] = Color.black;
            tex.SetPixels(colors);
            
            for (int j = 0; j < list.Count; ++j)
            {
                var sp = list[j];
                var uvs = DataUtility.GetSpriteUVs(sp, true);
                var spPath = AssetDatabase.GetAssetPath(sp);
                spPath = spPath.Remove(0, 6);
                spPath = spPath.Insert(0, Application.dataPath);

                float uvXMin = float.MaxValue;
                float uvYMin = float.MaxValue;
                float uvXMax = -float.MaxValue;
                float uvYMax = -float.MaxValue;
                for (int k = 0; k < uvs.Length; ++k)
                {
                    float uvx = uvs[k].x;
                    float uvy = uvs[k].y;

                    if (uvx < uvXMin) uvXMin = uvx;
                    if (uvy < uvYMin) uvYMin = uvy;
                    if (uvx > uvXMax) uvXMax = uvx;
                    if (uvy > uvYMax) uvYMax = uvy;
                }

                
                int x = Mathf.RoundToInt(uvXMin * (float)atlasWidth);
                int y = Mathf.RoundToInt(uvYMin * (float)atlasHeight);
                int blockWidth = Mathf.RoundToInt(sp.textureRect.width);
                int blockHeight = Mathf.RoundToInt(sp.textureRect.height);
                int blockOffsetX = Mathf.RoundToInt(sp.textureRectOffset.x);
                int blockOffsetY = Mathf.RoundToInt(sp.textureRectOffset.y);

                var spTex = DataUtility.GetSpriteTexture(sp, false);
                spTex = new Texture2D(spTex.width, spTex.height, TextureFormat.ARGB32, false);

//                Debug.Log(sp.name + ":"+sp.rect.width + "|" + sp.textureRect.width + "|" + blockWidth+"|"+spTex.width);
                
                var bs = File.ReadAllBytes(spPath);
                spTex.LoadImage(bs);

                if (!Mathf.Approximately(sp.rect.width, sp.textureRect.width) || !Mathf.Approximately(sp.rect.height, sp.textureRect.height))
                {
                    int scaleWidth = Mathf.RoundToInt(sp.rect.width);
                    int scaleHeight = Mathf.RoundToInt(sp.rect.height);
                    TextureScale.Bilinear(spTex, scaleWidth, scaleHeight);
                }
                
                var spPixels = GetPixels(spTex, blockOffsetX, blockOffsetY, blockWidth, blockHeight);
                for (int k = 0; k < spPixels.Length; ++k)
                    spPixels[k].r = spPixels[k].g = spPixels[k].b = spPixels[k].a;
                SetPixels(tex, x, y, blockWidth, blockHeight, spPixels);
                Object.DestroyImmediate(spTex);
            }
            
            var outputPath = string.Format("Assets/ResourcesLib/AtlasAlpha/{2}/{0}_a/{0}_{1}.jpg", dirName, i, buildTarget);
            File.WriteAllBytes(outputPath, tex.EncodeToJPG());
        }

        Resources.UnloadUnusedAssets();
        return true;
    }

    private static Color[] GetPixels(Texture2D tex, int x, int y, int width, int height)
    {
        //防止越界
        return tex.GetPixels(x, y, Mathf.Min(width, tex.width - x), Mathf.Min(height, tex.height - y));
    }

    private static void SetPixels(Texture2D tex, int x, int y, int width, int height, Color[] colors)
    {
        //补齐因四舍五入造成的误差
        if (width*height > colors.Length)
        {
            var addArr = new Color[width * height - colors.Length];
            colors = colors.ArrayConcat(addArr);
        }
        tex.SetPixels(x, y, width, height, colors);
    }

}