﻿using UnityEditor;

public class BuildParams
{
    public string fnGameName;
    public string[] platforms;
    public bool corner = true;
    public bool splash = true;

    public bool developmentMode;
    public bool debugErrorMode;

    public BuildVersionType buildVersionType;
    public string version = "";
    public BuildTarget buildTarget;
    public BuildTargetGroup buildTargetGroup;
    public bool isActiveSdk;
    public string lang;
    public string SdkName;

    public bool isObfuscar;  //是否混淆
    public bool isEncrypt;   //是否加密
}

public enum BuildVersionType
{
    Current,
    Next,
    NextBig
}