﻿using System;
using System.IO;
using System.Net;

public class HttpClient : WebClient
{
    private CookieContainer m_cookieContainer;
    public CookieContainer CookieContainer
    {
        get { return m_cookieContainer; }
        set { m_cookieContainer = value; }
    }

    public HttpClient()
    {
        m_cookieContainer = new CookieContainer();
        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
    }

    /// <summary>
    /// 重写GetWebRequest，使用指定的Cookie，使得WebClient具有保持会话的特性
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    protected override WebRequest GetWebRequest(Uri address)
    {
        var request = base.GetWebRequest(address);
        request.Timeout = 1000;
        if (request is HttpWebRequest)
        {
            (request as HttpWebRequest).CookieContainer = m_cookieContainer;
        }
        return request;
    }

    public string Delete(string address)
    {
        var request = GetWebRequest(new Uri(address));
        if (request != null)
        {
            request.Method = "DELETE";
            var response = (HttpWebResponse) request.GetResponse();
            var sr = new StreamReader(response.GetResponseStream());
            return sr.ReadToEnd();
        }
        return "";
    }
}