﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GunFireSound))]
public class GunFireSoundInspecter : Editor
{
    private const float LEFT_WIDTH = 150;
    private const float AUDIO_COUNT = 5;

    private void DrawItem(int index)
    {
        var sound = target as GunFireSound;
        if (sound == null)
            return;
        var clipList = sound.m_audioClipList;
        var propList = sound.m_probabilityList;

        EditorGUILayout.BeginHorizontal();
        var clip = index < clipList.Count ? clipList[index] : null;
        var tmpClip = EditorGUILayout.ObjectField(clip, typeof(AudioClip), true, GUILayout.Width(LEFT_WIDTH)) as AudioClip;

        if (clip != null && tmpClip != null)//改
            clipList[index] = tmpClip;
        if (clip == null && tmpClip != null) //增
        {
            clipList.Add(tmpClip);
            if (propList.Count == 0)
                propList.Add(100);
            else
                propList.Add(0);
            int totalProp = 0;
            for (int i = 0; i < propList.Count; i++)
                totalProp += propList[i];
        }
        if (clip != null && (tmpClip == null || GUILayout.Button("X", GUILayout.Width(20)))) //删
        {
            var removedProp = propList[index];
            clipList.RemoveAt(index);
            propList.RemoveAt(index);
            if (propList.Count>0)
                propList[propList.Count - 1] += removedProp;
        }

        if (tmpClip != null && index < propList.Count)
        {
            var prop = propList[index];
            var tmpProp = EditorGUILayout.IntSlider(prop, 0, 100);

            if (tmpProp != prop)
            {
                var dt = tmpProp - prop;
                if (index + 1 < propList.Count && propList[index + 1] - dt >= 0)
                {
                    propList[index] += dt;
                    propList[index + 1] -= dt;
                }
                else if (index - 1 >= 0 && propList[index - 1] - dt >= 0)
                {
                    propList[index] += dt;
                    propList[index - 1] -= dt;
                }
            }           
        }

        EditorGUILayout.EndHorizontal();
    }

    public override void OnInspectorGUI()
    {
        var sound = target as GunFireSound;
        if (sound == null)
            return;
        var clipList = sound.m_audioClipList;
        var propList = sound.m_probabilityList;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.Width(50));
        EditorGUILayout.LabelField("音频", GUILayout.Width(LEFT_WIDTH));
        EditorGUILayout.LabelField("概率(%)");
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < AUDIO_COUNT; i++)
        {
            DrawItem(i);
        }

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("最小开火间隔(秒)", GUILayout.Width(LEFT_WIDTH));
        var tmpMinSpawn = EditorGUILayout.IntSlider(sound.m_minSpawn, 0, 1000);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("最大开火间隔(秒)", GUILayout.Width(LEFT_WIDTH));
        var tmpMaxSpawn = EditorGUILayout.IntSlider(sound.m_maxSpawn, 0, 1000);
        EditorGUILayout.EndHorizontal();

        if (tmpMinSpawn != sound.m_minSpawn && tmpMinSpawn > tmpMaxSpawn)
            tmpMaxSpawn = tmpMinSpawn;
        if (tmpMaxSpawn != sound.m_maxSpawn && tmpMinSpawn > tmpMaxSpawn)
            tmpMinSpawn = tmpMaxSpawn;
        sound.m_minSpawn = tmpMinSpawn;
        sound.m_maxSpawn = tmpMaxSpawn;

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("fire"))
            sound.BeginGunFireSound();
        if (GUILayout.Button("stop"))
            sound.EndGunFireSound();
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("输出配置数据:");
        string soundNameConfig = "0";
        for (int i = 0; i < clipList.Count; i++)
        {
            soundNameConfig += ";" + clipList[i].name;
        }
        EditorGUILayout.TextField("fireSound", soundNameConfig);
        float prop = 0;
        string propConfig = "0";
        for (int i = 1; i < propList.Count; i++)
        {
            prop += propList[i-1]*0.01f;
            propConfig += ";" + prop.ToString("0.##");
        }
        propConfig += ";1";
        EditorGUILayout.TextField("fireSoundProbability", propConfig);
        EditorGUILayout.TextField("fireSoundSpawn", (sound.m_minSpawn*0.001f).ToString("0.###")+";"+(sound.m_maxSpawn*0.001f).ToString("0.###"));
    }
}
