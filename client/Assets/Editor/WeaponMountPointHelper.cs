﻿
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
class WeaponMountPointHelper : EditorWindow
{
    const string RightMountPointParent = "Bip_RightHandCenter";
    const string LeftMountPointParent = "Bip_LeftHandCenter";

    string mountInfo = "";

    [MenuItem("Assets/武器挂载点获取")]
    static void Init()
    {
        WeaponMountPointHelper window = EditorWindow.GetWindow<WeaponMountPointHelper>();
    }

    void OnGUI()
    {
        if (GUILayout.Button("生成挂载点"))
        {
            CreateMountPointInfo();
        }
        if (GUILayout.Button("导出"))
        {
            SaveConfig();
        }

        GUILayout.Label("名称,挂点Pos,挂点Rot");

        mountInfo = GUILayout.TextArea(mountInfo);

        if(GUILayout.Button("复制"))
        {
            Clipboard.clipBoard = mountInfo;
        }
    }


    void CreateMountPointInfo()
    {
        List<string> writeLines = new List<string>();

        mountInfo = "";
        foreach (GameObject go in Selection.gameObjects)
        {
            string fbxPath = AssetDatabase.GetAssetPath(go);

            if (fbxPath.ToLower().EndsWith(".fbx") == false)
                continue;

            string line = GetMountPointStr(go);
            mountInfo += line + "\n";
        }
    }

    float[] GetMountPoint(GameObject go, string parent)
    {
        Transform tranParent = GameObjectHelper.FindChildByTraverse(go.transform, parent);
        if (tranParent == null || tranParent.childCount == 0)
        {
            Debug.LogError("找不到挂点【" + parent + "】");
            return null;
        }
        float[] mountPoint = new float[6];
        Transform tran = tranParent.GetChild(0);
        mountPoint[0] = tran.localPosition.x;
        mountPoint[1] = tran.localPosition.y;
        mountPoint[2] = tran.localPosition.z;
        mountPoint[3] = tran.localEulerAngles.x;
        mountPoint[4] = tran.localEulerAngles.y;
        mountPoint[5] = tran.localEulerAngles.z;
        return mountPoint;
    }

    string GetMountPointStr(GameObject go)
    {
        float[] right = GetMountPoint(go, RightMountPointParent);
        float[] left = GetMountPoint(go, LeftMountPointParent);
        string pos = null, rot = null;
        if(right != null && left != null)
        {
            pos = string.Format("{0:N3}#{1:N3}#{2:N3};{3:N3}#{4:N3}#{5:N3}", right[0], right[1], right[2], left[0], left[1], left[2]);
            rot = string.Format("{0:N3}#{1:N3}#{2:N3};{3:N3}#{4:N3}#{5:N3}", right[3], right[4], right[5], left[3], left[4], left[5]);
        }
        else if(right != null)
        {
            pos = string.Format("{0:N3}#{1:N3}#{2:N3}", right[0], right[1], right[2]);
            rot = string.Format("{0:N3}#{1:N3}#{2:N3}", right[3], right[4], right[5]);
        }
        else
        {
            pos = string.Format("{0:N3}#{1:N3}#{2:N3}", left[0], left[1], left[2]);
            rot = string.Format("{0:N3}#{1:N3}#{2:N3}", left[3], left[4], left[5]);
        }

        return string.Format("{0},{1},{2}", go.name, pos, rot);
    }

    void SaveConfig()
    {
        string saveFile = EditorUtility.SaveFilePanel("save", ".", "未命名", "csv");
        if (!string.IsNullOrEmpty(saveFile))
        {
            string writeStr = "名称,挂点Pos,挂点Rot\n" + mountInfo;
            File.WriteAllText(saveFile, Encoding.GetEncoding("GBK").GetString(Encoding.Default.GetBytes(writeStr)), Encoding.GetEncoding("GBK"));// 修改完成后写入目标模型meta文件中
            ShowNotification(new GUIContent("导出成功"));
            AssetDatabase.Refresh();
        }
    }

    
}
