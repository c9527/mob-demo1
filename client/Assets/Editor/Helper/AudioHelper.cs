﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class AudioHelper
{
    public static void SetAudio()
    {
        SetSelectedAudio(PluginUtil.GetFiles("Assets/Resources/Sounds/Guide/", false), false, AudioImporterFormat.Compressed, 128000);
        SetSelectedAudio(PluginUtil.GetFiles("Assets/Resources/Sounds/2D/", false), false, AudioImporterFormat.Compressed, 128000);
        SetSelectedAudio(PluginUtil.GetFiles("Assets/Resources/Sounds/", false), true, AudioImporterFormat.Native);
    }

    private static void SetSelectedAudio(string[] paths, bool threeD, AudioImporterFormat format, int bitrate = 156000)
    {
        for (int i = 0; i < paths.Length; i++)
        {
            SetAudio(paths[i], threeD, format, bitrate);
        }
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }

    private static void SetAudio(string path, bool threeD = true, AudioImporterFormat format = AudioImporterFormat.Native, int bitrate = 156000)
    {
        var audio = AssetImporter.GetAtPath(path) as AudioImporter;
        if (audio == null)
            return;
        if (format != AudioImporterFormat.Compressed && path.EndsWith("_cp.wav"))
            format = AudioImporterFormat.Compressed;

        audio.threeD = threeD;
        audio.format = format;
        if (format == AudioImporterFormat.Compressed)
            audio.compressionBitrate = bitrate;

        AssetDatabase.WriteImportSettingsIfDirty(path);
    }
}
