﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System;
using Object = UnityEngine.Object;


public class EditorHelper
{
    public static void ExportPackage()
    {
        string outputDir = Application.dataPath;
        outputDir = outputDir.Substring(0, outputDir.IndexOf("Assets")) + "OutputPackage/";

        if (Directory.Exists(outputDir) == false)
            Directory.CreateDirectory(outputDir);

        Object[] arrObj = Selection.objects;

        foreach (Object obj in arrObj)
        {
            GameObject go = obj as GameObject;
            if (go == null)
                continue;

            string assetPath = AssetDatabase.GetAssetPath(obj);
            AssetDatabase.ExportPackage(assetPath, outputDir + obj.name + ".unitypackage", ExportPackageOptions.IncludeDependencies | ExportPackageOptions.Interactive);
        }
    }


    static public void VisitFileTraverse(string dir, List<string> listExtention, Action<string> callBack)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(dir);
        FileSystemInfo[] arrFSI = dirInfo.GetFileSystemInfos();

        foreach (FileSystemInfo kSI in arrFSI)
        {
            if (File.Exists(kSI.FullName) && kSI.FullName.EndsWith(".meta") == false)
            {
                if (listExtention == null || listExtention.IndexOf(kSI.Extension.ToString()) != -1)
                    callBack(kSI.FullName);
            }
            else if (Directory.Exists(kSI.FullName))
            {
                VisitFileTraverse(kSI.FullName, listExtention, callBack);
            }
        }
    }

    static public void VisitFileTraverse(string dir, string extention, Action<string> callBack)
    {
        List<string> listExtention = null;

        if (string.IsNullOrEmpty(extention) == false)
        {
            listExtention = new List<string>();
            listExtention.Add(extention);
        }

        VisitFileTraverse(dir, listExtention, callBack);
    }

    static public void VisitAssetTraverse(string dir, string extention, Action<Object> callBack)
    {
        List<string> listExtention = null;

        if (string.IsNullOrEmpty(extention) == false)
        {
            listExtention = new List<string>();
            listExtention.Add(extention);
        }

        VisitFileTraverse(dir, listExtention, (path) =>
        {
            string assetPath = path.Substring(path.IndexOf("Assets"));

            callBack(AssetDatabase.LoadMainAssetAtPath(assetPath)); ;
        });
    }

    static public void VisitFileNoTraverse(string dir, List<string> listExtention, Action<string> callBack)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(dir);
        FileSystemInfo[] arrFSI = dirInfo.GetFileSystemInfos();

        foreach (FileSystemInfo kSI in arrFSI)
        {
            if (File.Exists(kSI.FullName) && kSI.FullName.EndsWith(".meta") == false)
            {
                if (listExtention == null || listExtention.IndexOf(kSI.Extension.ToString()) != -1)
                {
                    callBack(kSI.FullName);
                }
            }
        }
    }

    static public void VisitFileNoTraverse(string dir, string extention, Action<string> callBack)
    {
        List<string> listExtention = null;

        if (string.IsNullOrEmpty(extention) == false)
        {
            listExtention = new List<string>();
            listExtention.Add(extention);
        }

        VisitFileNoTraverse(dir, listExtention, callBack);
    }

    static public void VisitAssetNoTraverse(string dir, string extention, Action<Object> callBack)
    {
        List<string> listExtention = null;

        if(string.IsNullOrEmpty(extention) == false)
        {
            listExtention = new List<string>();
            listExtention.Add(extention);
        }
            
        VisitFileNoTraverse(dir, listExtention, (path)=>{

            string assetPath = path.Substring(path.IndexOf("Assets"));

            callBack(AssetDatabase.LoadMainAssetAtPath(assetPath));;
        });;
    }

    static public string GetAssetNameByPath(string path)
    {
        int index1 = path.LastIndexOf("/") + 1;
        int index2 = path.LastIndexOf(".");
        return path.Substring(index1, index2 - index1);
    }

    static public string GetCurBattleSceneName()
    {
        string curScenePath = EditorApplication.currentScene;
        string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
        sceneName = sceneName.Substring(0, sceneName.Length - sceneName.LastIndexOf(".unity") + 2);

        if (sceneName.StartsWith("map_") == false)
            return "";

        return sceneName;
    }

    static public string GetCurSceneName()
    {
        string curScenePath = EditorApplication.currentScene;
        string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
        sceneName = sceneName.Substring(0, sceneName.LastIndexOf(".unity"));

        return sceneName;
    }

    static public string GetAssetFilePath(string assetPath)
    {
        return Application.dataPath + "/" + assetPath;
    }
}
