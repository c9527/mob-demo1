﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using System.Text;


public class SceneHelper 
{
    const string NAME_COLLIDER_GROUP = "ColliderGroup";
    const string NAME_COLLIDER_DEATHBOX = "ColliderGroup/deathbox";
    //const string NAME_CONFIG = "Config";
    const string NAME_TEMP_CONFIG = "TempConfig";   // 用来导出服务器数据，所以不会出现在正式场景预设中
    const string NAME_SCENE = "Scene";
    const string NAME_BUILDING = "Building";
    const string NAME_LIGHT_GROUP = "LightGroup";
    const string NAME_BORNPOINT = "BornPoint";
    const float GROUND_OFFSET = 0.05f;
    const float PLAYER_RADIUS = 0.4f;
    const float PLAYER_HEIGHT = 2.0f;

    static private List<string> PREFAB_NAME = new List<string> { "Scene.prefab", "ColliderGroup.prefab", "Config.prefab"};


    static public void OneKeyHandleScene()
    {
        if (CheckCurSceneCorrect(false) == false)
            return;

        if (CreateScenePrefab(false) == false)
            return;

        CreateEmptyScene();    

        MapExport.Export();

        EditorApplication.SaveScene();

        EditorUtility.DisplayDialog("", "一键处理场景完毕！", "ok");
    }

    static public bool CreateScenePrefab(bool checkSceneCorret)
    {
        if (checkSceneCorret && CheckCurSceneCorrect(false) == false)
            return false;

        string curScenePath = EditorApplication.currentScene;
        string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
        sceneName = sceneName.Substring(0, sceneName.Length - sceneName.LastIndexOf(".unity") + 2);

        bool notTestMap = IsNotTestMap(sceneName);

        if (sceneName.StartsWith("map_") == false)
        {
            EditorUtility.DisplayDialog("", "请打开场景文件重试", "ok");
            return false;
        }

        string scenePrefabName = sceneName + "_prefab";
        GameObject goScenePrefab = GameObject.Find(scenePrefabName);
        if (goScenePrefab == null)
        {
            EditorUtility.DisplayDialog("", "找不到场景根结点：" + scenePrefabName, "ok");
            return false;
        }

        bool hasOccluderGO = false;
        goScenePrefab.transform.VisitChild((go) =>
        {
            StaticEditorFlags sef = GameObjectUtility.GetStaticEditorFlags(go);
            if (sef.CompareTo(StaticEditorFlags.OccluderStatic) > 0)
                hasOccluderGO = true;
        }, false);
        if (hasOccluderGO == false && notTestMap)
            EditorUtility.DisplayDialog("警告", "当前场景中没有标记用于遮挡剔除的遮挡物Static.OccluderStatic！快找程序员", "ok");

        string childPrefabDir = curScenePath.Substring(0, curScenePath.LastIndexOf('/') + 1) + "Prefab/";
        if (Directory.Exists(childPrefabDir) == false)
            Directory.CreateDirectory(childPrefabDir);

        FileInfo[] arrFI = (new DirectoryInfo(childPrefabDir)).GetFiles("*.prefab");
        if (arrFI.Length <= 0)
        {
            EditorUtility.DisplayDialog("", "场景预设文件夹中找不到预设：" + childPrefabDir, "ok");
            return false;
        }
        
        GameObject goScene = new GameObject(sceneName + "_prefab");
        goScene.transform.position = goScenePrefab.transform.position;
        goScene.transform.localScale = goScenePrefab.transform.localScale;
        goScene.transform.eulerAngles = goScenePrefab.transform.eulerAngles;

        
        // TODO:如果SNV没变化，则不要生成

        foreach (FileInfo fi in arrFI)
        {
            if (PREFAB_NAME.Contains(fi.Name) == false)
            {
                //Debug.Log("排除的预设：" + fi.Name);
                continue;
            }

            string assetPath = fi.FullName.Substring(fi.FullName.IndexOf("Assets"));
            GameObject prefab = GameObject.Instantiate(AssetDatabase.LoadMainAssetAtPath(assetPath)) as GameObject;
            prefab.name = GetNameWithoutClone(prefab.name);

            Vector3 pos = prefab.transform.localPosition;
            Vector3 rot = prefab.transform.eulerAngles;
            Vector3 scale = prefab.transform.localScale;

            prefab.transform.VisitChild((go)=>
            {
                MonoBehaviour script = go.GetComponent<MonoBehaviour>();
                if(script != null && script.GetType() != typeof(MatInfo))
                {
                    Debug.LogError("场景中挂的有脚本 结点名：" + go.name + "  脚本名：" + script.GetType());
                    GameObject.DestroyImmediate(script);
                }
            }, true);

            prefab.transform.SetParent(goScene.transform);
            prefab.transform.localPosition = pos;
            prefab.transform.localEulerAngles = rot;
            prefab.transform.localScale = scale;
        }

        //Object oldPrefab = AssetDatabase.LoadMainAssetAtPath(scenePrefabPath);
        Object outputPrefab = null;
        //if (oldPrefab != null)
        //{
        //    outputPrefab = PrefabUtility.ReplacePrefab(goScene, oldPrefab, ReplacePrefabOptions.ConnectToPrefab);    // 没卵用，生成的文件还是会变
        //}
        //else
        //{
        //    outputPrefab = PrefabUtility.CreatePrefab(scenePrefabPath, goScene);
        //}

        string resSceneMapDir = "Assets/Resources/Scenes/" + sceneName;
        if (Directory.Exists(resSceneMapDir) == false)
            Directory.CreateDirectory(resSceneMapDir);

        string scenePrefabPath = resSceneMapDir + "/" + sceneName + "_prefab.prefab";
        outputPrefab = PrefabUtility.CreatePrefab(scenePrefabPath, goScene);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        GameObject.DestroyImmediate(goScene);

        EditorUtility.DisplayDialog("", "场景预设创建成功：" + scenePrefabPath, "ok");
        Selection.activeObject = outputPrefab;

        return true;
    }

    //static public void CreateSceneChildPrefab()
    //{
    //    string curScenePath = EditorApplication.currentScene;
    //    string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
    //    sceneName = sceneName.Substring(0, sceneName.Length - sceneName.LastIndexOf(".unity") + 2);

    //    if(sceneName.StartsWith("map_") == false)
    //    {
    //        EditorUtility.DisplayDialog("", "请打开场景文件重试", "ok");
    //        return;
    //    }

    //    string scenePrefabName = sceneName + "_Prefab";
    //    GameObject goScenePrefab = GameObject.Find(scenePrefabName);
    //    if(goScenePrefab == null)
    //    {
    //        EditorUtility.DisplayDialog("", "找不到场景根结点：" + scenePrefabName, "ok");
    //        return;
    //    }

    //    string childPrefabDir = curScenePath.Substring(0, curScenePath.LastIndexOf('/') + 1) + "Prefab/";
    //    if (Directory.Exists(childPrefabDir) == false)
    //        Directory.CreateDirectory(childPrefabDir);

    //    Debug.Log(childPrefabDir);

    //    for(int i=0; i<goScenePrefab.transform.childCount; i++)
    //    {
    //        GameObject goChild = goScenePrefab.transform.GetChild(i).gameObject;

    //        string childPath = childPrefabDir + goChild.name + ".prefab";
    //        //PrefabUtility.CreatePrefab(childPath, goChild);
    //        Object obj = PrefabUtility.CreateEmptyPrefab(childPath);
    //        PrefabUtility.ReplacePrefab(goChild, obj);
    //    }        
    //}

    //static public void CreateAllScenePrefab()
    //{
    //    string SceneDir = "Assets/Resources/Scenes";
    //    string SceneLibDir = "Assets/ResourcesLib/Scenes";

    //    DirectoryInfo di = new DirectoryInfo(SceneDir);
    //    foreach(DirectoryInfo kDI in di.GetDirectories())
    //    {
    //        string dirName = kDI.Name;
    //        if (dirName.StartsWith("map_") == false)
    //            continue;

    //        string path = SceneLibDir + "/" + dirName + "/" + "Prefab";
    //        DirectoryInfo libDI = new DirectoryInfo(path);
    //        if (libDI.Exists == false)
    //            continue;

    //        string scenePrefabPath = SceneDir + "/" + dirName + "/" + dirName + "_prefab.prefab";
    //        if (File.Exists(scenePrefabPath) == false)
    //        {
    //            Debug.LogWarning("找不到场景预设文件: " + scenePrefabPath);
    //            continue;
    //        }
            
    //        GameObject scenePrefab = GameObject.Instantiate(AssetDatabase.LoadMainAssetAtPath(scenePrefabPath)) as GameObject;

    //        FileInfo[] arrFI = libDI.GetFiles("*.prefab");
    //        if(arrFI.Length >= 1)
    //        {
    //            GameObject[] arrChild = new GameObject[scenePrefab.transform.childCount];

    //            for (int i = 0; i < scenePrefab.transform.childCount; i++ )
    //                arrChild[i] = scenePrefab.transform.GetChild(i).gameObject;

    //            for (int j = 0; j < arrChild.Length; j++ )
    //                GameObject.DestroyImmediate(arrChild[j], true);

    //            foreach (FileInfo fi in arrFI)
    //            {
    //                string assetPath = SceneLibDir + "/" + dirName + "/" + "Prefab/" + fi.Name;
    //                GameObject prefab = GameObject.Instantiate(AssetDatabase.LoadMainAssetAtPath(assetPath)) as GameObject;
    //                prefab.name = GetNameWithoutClone(prefab.name);

                    
    //                Vector3 pos = prefab.transform.position;
    //                Vector3 rot = prefab.transform.eulerAngles;
    //                Vector3 scale = prefab.transform.localScale;
    //                scale.x /= scenePrefab.transform.localScale.x;
    //                scale.y /= scenePrefab.transform.localScale.y;
    //                scale.y /= scenePrefab.transform.localScale.z;

    //                prefab.transform.SetParent(scenePrefab.transform);
    //                prefab.transform.localScale = scale;
    //                prefab.transform.eulerAngles = rot;
    //                prefab.transform.position = pos;                    
    //            }

    //            PrefabUtility.CreatePrefab(scenePrefabPath, scenePrefab);
    //            AssetDatabase.SaveAssets();

    //            GameObject.DestroyImmediate(scenePrefab, true);
    //        }
    //    }
    //}

    static public void CreateEmptyScene()
    {       
        string curSceneName = EditorHelper.GetCurBattleSceneName();
        if (curSceneName.StartsWith("map_") == false)
        {
            EditorUtility.DisplayDialog("", "打开的不是地图场景", "ok");
            return;
        }

        string oldPath = EditorApplication.currentScene;
        string copyPath = "Assets/Resources/Scenes/" + curSceneName + "/" + curSceneName + ".unity";
        //AssetDatabase.DeleteAsset(copyPath);
        //AssetDatabase.CopyAsset(oldPath, copyPath);
        //简单处理，如果一定要用Unity Editor Script来复制保存的话，可以把meta用AssetDatabase.MoveAsset处理
        File.Copy(oldPath, copyPath, true);

        EditorApplication.SaveScene();

        EditorApplication.OpenScene(copyPath);

        Object[] arrGO = GameObject.FindObjectsOfType<GameObject>();
        foreach(GameObject go in arrGO)
        {
            GameObject.DestroyImmediate(go);
        }
        EditorApplication.SaveScene();

        EditorApplication.OpenScene(oldPath);

        AssetDatabase.Refresh();

        AddEmptyMap2Editor();

        EditorUtility.DisplayDialog("", "空场景生成成功！ " + copyPath, "ok");
    }

    /// <summary>
    /// 导出躲猫猫固定物件
    /// </summary>
    static public void ExportDMMSceneItem(string sceneID, GameObject root, Vector2 minVec, Vector2 maxVec, Vector2 sizeVec, float maxY)
    {
        ConfigMapItem cmi = ScriptableObject.CreateInstance<ConfigMapItem>();

        string path = "Assets/Resources/Config/ConfigMapItem.csv";
        string content = File.ReadAllText(path);
        cmi.Load(content);
        cmi.ReBuildDic();

        ConfigMapItemLine cmil = cmi.GetLine(int.Parse(sceneID));
        if(cmil == null)
        {
            cmil = new ConfigMapItemLine();
            cmil.ID = int.Parse(sceneID);

            List<ConfigMapItemLine> list = new List<ConfigMapItemLine>(cmi.m_dataArr);
            list.Add(cmil);
            cmi.m_dataArr = list.ToArray();
        }

        Transform transDMMItem = root.transform.Find("TempConfig/DMMSceneItem");
        if(transDMMItem != null)
        {
            //Debug.LogWarning("没有找到躲猫猫必现物件节点 " + "TempConfig/DMMSceneItem");
            int num = transDMMItem.transform.childCount;

            cmil.DMMItemID = new int[num];
            cmil.DMMItemPos = new Vector3[num];
            cmil.DMMItemRot = new Vector3[num];
            cmil.DMMItemScale = new Vector3[num];
            cmil.GameRules = new string[num];

            for (int i = 0; i < transDMMItem.childCount; i++)
            {
                Transform child = transDMMItem.GetChild(i);
                string goName = child.gameObject.name;
                string id = goName.Substring(0, goName.IndexOf("_"));
                Debug.Log(id);

                cmil.DMMItemID[i] = int.Parse(id);
                cmil.DMMItemPos[i] = child.transform.position;
                cmil.DMMItemRot[i] = child.transform.localEulerAngles;
                cmil.DMMItemScale[i] = child.transform.localScale;

                SceneItemInfo sii = child.GetComponent<SceneItemInfo>();
                if(sii != null && sii.GameRules != null)
                {
                    cmil.GameRules[i] = string.Join("|", sii.GameRules).Trim();
                }
            }
        }

        cmil.posMin = minVec;
        cmil.posMax = maxVec;
        cmil.size = sizeVec;
        cmil.maxY = maxY;

        Debug.Log(cmi.Export());

        File.WriteAllText(Application.dataPath + "/Resources/Config/" + "ConfigMapItem.csv", cmi.Export(), Encoding.Unicode);
    }

    static private int GetSceneID(string sceneName)
    {
        int mapID = -1;
        int.TryParse(sceneName.Substring(4), out mapID);
        return mapID;
    }

    static private bool IsNotTestMap(string sceneName)
    {
        if (GetSceneID(sceneName) >= 9000)
            return false;
        return true;
    }

    static public bool CheckCurSceneCorrect(bool showSuccess = true)
    {
        string curScenePath = EditorApplication.currentScene;
        string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
        sceneName = sceneName.Substring(0, sceneName.Length - sceneName.LastIndexOf(".unity") + 2);

        if (sceneName.StartsWith("map_") == false)
        {
            EditorUtility.DisplayDialog("", "当前打开的不是战斗场景", "ok");
            return false;
        }

        bool notTestMap = IsNotTestMap(sceneName);

        string scenePrefabName = sceneName + "_prefab";
        GameObject goScenePrefab = GameObject.Find(scenePrefabName);
        if (goScenePrefab == null)
        {
            EditorUtility.DisplayDialog("", "找不到场景根结点：" + scenePrefabName + " 请检查大小写与命名", "ok");
            return false;
        }

        // 检查结点名字
        Transform transColliderGroup = goScenePrefab.transform.Find(NAME_COLLIDER_GROUP);
        if (transColliderGroup == null && notTestMap)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_COLLIDER_GROUP + " 请检查大小写与命名", "ok");
            return false;
        }

        // 碰撞体物体不能关闭，包括挂的组件与物体都不能关闭，避免反作弊误判
        bool retCollider = true;
        transColliderGroup.VisitChild(colliderGO =>
        {
            if(colliderGO.activeInHierarchy == false)
            {
                EditorUtility.DisplayDialog("失败", "有碰撞体被关闭!：" + colliderGO.name, "ok");
                Selection.activeGameObject = colliderGO;
                retCollider = false;
            }

            Collider collider = colliderGO.GetComponent<Collider>();
            if(collider != null && collider.enabled == false)
            {
                EditorUtility.DisplayDialog("失败", "有碰撞体组件被关闭!：" + colliderGO.name, "ok");
                Selection.activeGameObject = colliderGO;
                retCollider = false;           
            }

        }, false);
        if (retCollider == false)
            return false;


        Transform transColliderDeathBox = goScenePrefab.transform.Find(NAME_COLLIDER_DEATHBOX);
        if (transColliderDeathBox == null && notTestMap)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_COLLIDER_DEATHBOX + " 请检查大小写与命名", "ok");
            return false;
        }

        Transform transTempConfig = goScenePrefab.transform.Find(NAME_TEMP_CONFIG);
        if (transTempConfig == null && notTestMap)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_TEMP_CONFIG + " 请检查大小写与命名", "ok");
            return false;
        }

        Transform transScene = goScenePrefab.transform.Find(NAME_SCENE);
        if (transScene == null)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_SCENE + " 请检查大小写与命名", "ok");
            return false;
        }

        Transform transBuilding = transScene.Find(NAME_BUILDING);
        if (transBuilding == null)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_BUILDING + " 请检查大小写与命名", "ok");
            return false;
        }

        Transform transLightGroup = transScene.transform.Find(NAME_LIGHT_GROUP);
        if (transLightGroup == null && notTestMap)
        {
            EditorUtility.DisplayDialog("", "找不到结点：" + NAME_LIGHT_GROUP + " 请检查大小写与命名", "ok");
            return false;
        }

        // LightMap
        string lmFileName = sceneName + "_lightmap.exr";
        string lmPath = "Assets/Resources/Scenes/" + sceneName + "/" + lmFileName;
        string lmDir = "Assets/Resources/Scenes/" + sceneName;

        DirectoryInfo di = new DirectoryInfo(lmDir);
        FileInfo[] arrFI = di.GetFiles("*.exr");
        if (notTestMap && arrFI.Length > 1)
        {
            EditorUtility.DisplayDialog("", "发现多张光照贴图!" + lmDir, "ok");
            return false;
        }
        if (notTestMap && arrFI.Length == 0)
        {
            EditorUtility.DisplayDialog("", "目录中没有光照贴图文件!" + lmDir, "ok");
            return false;
        }

        if (notTestMap && arrFI[0].Name != lmFileName)
        {
            EditorUtility.DisplayDialog("", "找不到光照贴图：" + lmPath, "ok");
            return false;
        }

        // 遮挡剔除检查
        bool hasOA = false;
        Transform transOcclusion = goScenePrefab.transform.Find("Occlusion");
        if (transOcclusion != null)
        {
            transOcclusion.VisitChild((child) =>
            {
                OcclusionArea oa = child.GetComponent<OcclusionArea>();
                if (oa != null)
                {
                    hasOA = true;
                }
            }, true);
        }
        if (hasOA == false && notTestMap)
            EditorUtility.DisplayDialog("", "找不到遮挡剔除：Occlusion", "ok");

        if (notTestMap && StaticOcclusionCulling.umbraDataSize <= 0)
        {
            EditorUtility.DisplayDialog("", "当前场景没有 烘焙遮挡剔除", "ok");
            //return false;
        }

        //
        if (notTestMap && CheckBornPointPos(transTempConfig, sceneName) == false)
        {
            return false;
        }

        if (showSuccess)
            EditorUtility.DisplayDialog("", "看起来没撒问题", "ok");

        return true;
    }

    static public bool CheckBornPointPos(Transform transConfig, string sceneName)
    {
        bool error = false;
        bool changed = false;

        GameObject goConfig = transConfig.gameObject;
        string path = "Assets/ResourcesLib/Scenes/" + sceneName + "/Prefab/" + NAME_TEMP_CONFIG + ".prefab";

        Object configPrefab = AssetDatabase.LoadMainAssetAtPath(path);

        GameObjectHelper.VisitChildByTraverse(transConfig, (go) =>
        {
            if (error)
                return;

            if (go.name == NAME_BORNPOINT)
            {
                RaycastHit kRH;
                if (Physics.Raycast(go.transform.position, Vector3.down, out kRH, 1000f))
                {
                    Vector3 newPos = kRH.point + new Vector3(0, GROUND_OFFSET, 0);
                    if (go.transform.position != newPos)
                    {
                        go.transform.position = newPos;
                        changed = true;
                    }
                }
                else
                {
                    Selection.activeGameObject = go;
                    EditorUtility.DisplayDialog("错误", "当前出生点在地表下面", "ok");
                    error = true;
                    return;
                }

                //if(Physics.CheckCapsule(go.transform.position, go.transform.position + new Vector3(0, PLAYER_HEIGHT, 0), PLAYER_RADIUS))
                //{
                //    Selection.activeGameObject = go;
                //    EditorUtility.DisplayDialog("错误", "当前出生点的玩家，会与碰撞体相交", "ok");
                //    error = true;
                //    return;
                //}
            }
        });

        if (changed)
        {
            PrefabUtility.ReplacePrefab(goConfig, configPrefab, ReplacePrefabOptions.ConnectToPrefab);
            AssetDatabase.SaveAssets();
            EditorUtility.DisplayDialog("", "生出点已修改，并保存预设成功", "ok");
        }

        if (error)
            return false;

        return true;
    }

    static public void CheckBornPointIntersect()
    {
        string curScenePath = EditorApplication.currentScene;
        string sceneName = curScenePath.Substring(curScenePath.LastIndexOf("/") + 1);
        sceneName = sceneName.Substring(0, sceneName.Length - sceneName.LastIndexOf(".unity") + 2);

        GameObject goScene = GameObject.Find(sceneName + "_prefab");
        if (goScene == null)
        {
            EditorUtility.DisplayDialog("", "找不到场景结点", "ok");
            return;
        }

        GameObjectHelper.VisitChildByTraverse(goScene.transform, (go) =>
        {
            if (go.name != NAME_BORNPOINT)
                return;

            go.AddMissingComponent<CheckBornPointIntersect>();
            go.AddMissingComponent<Rigidbody>().isKinematic = true;
            CapsuleCollider cc = go.AddMissingComponent<CapsuleCollider>();
            cc.isTrigger = true;
            cc.height = PLAYER_HEIGHT;
            cc.radius = PLAYER_RADIUS;
            cc.center = new Vector3(0, PLAYER_HEIGHT * 0.5f, 0);
            go.TrySetActive(false);
            go.TrySetActive(true);
        });
    }

    static private string GetNameWithoutClone(string name)
    {
        return name.Replace("(Clone)", "");
    }

    static public void CreateColliderGroupCube(Transform trans)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.name = "Cube_Debug";
        cube.TrySetActive(false);
        cube.transform.SetParent(trans);
        Mesh meshCube = cube.GetComponent<MeshFilter>().mesh;
        Material matCube = cube.GetComponent<MeshRenderer>().material;

        GameObjectHelper.VisitChildByTraverse(trans, (go) =>
        {
            MeshRenderer mr = GameObjectHelper.GetComponent<MeshRenderer>(go);
            mr.material = matCube;
            MeshFilter mf = GameObjectHelper.GetComponent<MeshFilter>(go);
            mf.mesh = meshCube;
        });
    }

    static public void FindRotatedWall()
    {
        GameObject[] arrGO = GameObject.FindObjectsOfType<GameObject>();

        foreach (GameObject go in arrGO)
        {
            if (go.name.Contains("wall") && (go.transform.eulerAngles.x != 0 || go.transform.eulerAngles.z != 0))
            {
                Debug.LogWarning("场景中墙壁有旋转 " + go.name);
                Selection.activeGameObject = go;
                break;
            }
        }
    }

    static public void AddEmptyMap2Editor()
    {
        string curSceneName = EditorHelper.GetCurBattleSceneName();
        if (curSceneName.StartsWith("map_") == false)
        {
            EditorUtility.DisplayDialog("", "打开的不是地图场景", "ok");
            return;
        }

        string emptyScenePath = "Assets/Resources/Scenes/" + curSceneName + "/" + curSceneName + ".unity";
        if(AssetDatabase.LoadMainAssetAtPath(emptyScenePath) == null)
        {
            EditorUtility.DisplayDialog("添加空战斗场景到编辑器失败", "找不到空战斗场景 " + emptyScenePath, "ok");
            return;
        }
        
        bool ok = true;
        List<EditorBuildSettingsScene> listScene = new List<EditorBuildSettingsScene>();
        foreach(EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            listScene.Add(scene);
            if (scene.path == emptyScenePath)
            {
                ok = false;
                break;
            }
        }

        if (ok)
        {
            EditorBuildSettingsScene newScene = new EditorBuildSettingsScene(emptyScenePath, true);
            listScene.Add(newScene);
            EditorBuildSettings.scenes = listScene.ToArray();
            AssetDatabase.SaveAssets();
            EditorUtility.DisplayDialog("", "添加空战斗场景到编辑器成功", "ok");
        }
    }

#region Private

 

    static private string GetCurSceneAssetPath()
    {
        return EditorApplication.currentScene.Substring(0, EditorApplication.currentScene.LastIndexOf('/') + 1);
    }
#endregion

}

class CheckBornPointIntersect : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        gameObject.name += " error";
        EditorUtility.DisplayDialog(gameObject.name, "当前出生点的玩家会与场景Collider相交", "ok");
        Selection.activeGameObject = gameObject;
    }
}
