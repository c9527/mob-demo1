﻿using System.Globalization;
using System.IO;
using UnityEditorInternal;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;


public class FbxHelper 
{

    //static public void SetFbxHumanoid()
    //{
    //    // 设置为Humanoid

    //    GameObject[] arrGO = Selection.gameObjects;

    //    List<GameObject> listFbxGO = new List<GameObject>();

    //    foreach( GameObject goItem in arrGO)
    //    {
    //        string strName = goItem.name.ToLower();
    //        if(goItem.name.EndsWith(strName))
    //        {
    //            listFbxGO.Add(goItem);

    //            string strAssetPath = AssetDatabase.GetAssetPath(goItem);
    //            ModelImporter kMI = ModelImporter.GetAtPath(strAssetPath) as ModelImporter;
    //            kMI.animationType = ModelImporterAnimationType.Generic;
    //            kMI.importAnimation = true;

    //            ModelImporterClipAnimation[] arrAni = kMI.clipAnimations;
    //            Debug.Log(arrAni.Length);

    //            //kMI.sourceAvatar = null;
    //            //foreach(ModelImporterClipAnimation kCA in kMI.clipAnimations)
    //            //{
    //            //    kCA.lockRootRotation = true;
    //            //    kCA.keepOriginalOrientation = true;
    //            //    kCA.lockRootHeightY = true;
    //            //    kCA.heightFromFeet = true;
    //            //    kCA.lockRootPositionXZ = true;
    //            //    kCA.keepOriginalPositionXZ = false;
    //            //}
    //            AssetDatabase.Refresh();
    //        }
    //    }
    //}

    /// <summary>
    /// 规范化所有角色相关资源的命名、fbx设置、Shader、关联、prefab创建
    /// </summary>
    //public static void NormalizeAllRoleModel()
    //{
    //    NormalizeRoleModel("terrorist", false);
    //    NormalizeRoleModel("police", false);
    //    NormalizeRoleModel("arm", false);

    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //}

    /// <summary>
    /// 规范化所有角色动画的命名、fbx设置
    /// </summary>
    //public static void NormalizeAllRoleAnim()
    //{
    //    NormalizeRoleAnim("player", false);

    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //}

    /// <summary>
    /// 重新创建所有角色的prefab
    /// </summary>
    //public static void CreateAllRolePrefab()
    //{
    //    var avatar = AssetDatabase.LoadAssetAtPath("Assets/ResourcesLib/Roles/Fbx/Terrorist/terrorist.FBX", typeof(Avatar)) as Avatar;
    //    var aniCtrlPlayer = AssetDatabase.LoadMainAssetAtPath("Assets/ResourcesLib/Roles/AnimatorContoller/player_AniCtrl.controller") as AnimatorController;
    //    var aniCtrlArm = AssetDatabase.LoadMainAssetAtPath("Assets/ResourcesLib/Roles/AnimatorContoller/arm_AniCtrl.controller") as AnimatorController;

    //    CreateRolePrefab("terrorist", null, aniCtrlPlayer, new object[] { 45f, 0.3f, new Vector3(0.1f, 0.95f, 0.15f), 0.4f, 1.9f }, false);
    //    CreateRolePrefab("police", avatar, aniCtrlPlayer, new object[] { 45f, 0.3f, new Vector3(0.1f, 0.95f, 0.15f), 0.4f, 1.9f }, false);
    //    CreateRolePrefab("arm", null, aniCtrlArm, new object[] { 45f, 0.3f, new Vector3(0f, 0.9f, 0f), 0.3f, 1.8f }, false);

    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //}

    /// <summary>
    /// 在指定目录拥有FBX文件和贴图文件，且目录结构和文件夹命名正确的情况下，规范化角色相关资源的命名、fbx设置、关联、prefab创建
    /// </summary>
    /// <param name="fbxName">fbx名称</param>
    /// <param name="updateModify">是否立即更新所做的修改</param>
    private static void NormalizeRoleModel(string fbxName, bool updateModify = true)
    {
        var fbxDir = "Assets/ResourcesLib/Roles/Fbx/" + fbxName.ToUpTitle() + "/";

        //加载fbx，设置fbx设置
        var fbxPath = Directory.GetFiles(fbxDir, "*.fbx")[0];
        var fbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;
        if (fbx == Selection.activeGameObject)  //如果当前选中了自己，取消选择，否则GUI绘制会异常
            Selection.activeGameObject = null;
        var improter = AssetImporter.GetAtPath(fbxPath) as ModelImporter;

        if (improter.importAnimation || improter.animationType != ModelImporterAnimationType.Human)
        {
            improter.importAnimation = false;
            improter.animationType = ModelImporterAnimationType.Human;
            AssetDatabase.ImportAsset(fbxPath);
        }

        //此时，有可能没有Materials目录，有可能有目录且有零个或者多个mat
        //这里处理的目的是尽可能的避免mat对象的guid改变
        var fbxObj = GameObject.Instantiate(fbx) as GameObject;
        var fbxMaterial = fbxObj.GetComponentInChildren<Renderer>().sharedMaterial; //拿到fbx指向的mat对象引用
        if (!Directory.Exists(fbxDir + "Materials/"))
            AssetDatabase.ImportAsset(fbxPath);
        else
        {
            var matArr = Directory.GetFiles(fbxDir + "Materials/", "*.mat");
            for (int i = 0; i < matArr.Length; i++)
            {
                //不是fbx当前引用的mat统统删掉
                if (fbxMaterial != AssetDatabase.LoadMainAssetAtPath(matArr[i]))
                    AssetDatabase.DeleteAsset(matArr[i]);
            }

            //最后只可能剩余1个或0个，如果为0，则需要Reimport
            matArr = Directory.GetFiles(fbxDir + "Materials/", "*.mat");
            if (matArr.Length == 0)
                AssetDatabase.ImportAsset(fbxPath);
        }
        GameObject.DestroyImmediate(fbxObj);

        //加载贴图和材质对象
        var texPath = Directory.GetFiles(fbxDir + "Textures/", "*.tga")[0];
        var matPath = Directory.GetFiles(fbxDir + "Materials/", "*.mat")[0];
        var texture = AssetDatabase.LoadMainAssetAtPath(texPath) as Texture;
        var material = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
        
        //规范化资源名称、shader，建立材质和贴图的关联
        if (Path.GetFileNameWithoutExtension(fbxPath) != fbxName)
            AssetDatabase.RenameAsset(fbxPath, fbxName);
        if (Path.GetFileNameWithoutExtension(texPath) != "tex_" + fbxName)
            AssetDatabase.RenameAsset(texPath, "tex_" + fbxName);
        if (Path.GetFileNameWithoutExtension(matPath) != "mat_" + fbxName)
            AssetDatabase.RenameAsset(matPath, "mat_" + fbxName);
        material.shader = Shader.Find("Unlit/Texture");
        material.mainTexture = texture;

        if (updateModify)
        {
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    /// <summary>
    /// 规范化指定角色动作的命名、fbx设置
    /// </summary>
    /// <param name="animClassName"></param>
    /// <param name="updateModify"></param>
    //private static void NormalizeRoleAnim(string animClassName, bool updateModify = true)
    //{
    //    var animDir = "Assets/ResourcesLib/Roles/Animation/" + animClassName.ToUpTitle() + "/";
    //    //加载fbx，设置fbx设置
    //    var fbxPathArr = Directory.GetFiles(animDir, "*.fbx");
    //    for (int i = 0; i < fbxPathArr.Length; i++)
    //    {
    //        var fbxPath = fbxPathArr[i];
    //        var fbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;
    //        if (fbx == Selection.activeGameObject)  //如果当前选中了自己，取消选择，否则GUI绘制会异常
    //            Selection.activeGameObject = null;
    //        var improter = AssetImporter.GetAtPath(fbxPath) as ModelImporter;

    //        if (improter.animationType != ModelImporterAnimationType.Human)
    //        {
    //            improter.animationType = ModelImporterAnimationType.Human;
    //            AssetDatabase.ImportAsset(fbxPath);
    //        }
    //    }

    //    if (updateModify)
    //    {
    //        AssetDatabase.SaveAssets();
    //        AssetDatabase.Refresh();
    //    }
    //}

    /// <summary>
    /// 重新创建指定角色的prefab
    /// </summary>
    /// <param name="fbxName">fbx名称</param>
    /// <param name="avatar">Avatar，为空则用自身的</param>
    /// <param name="aniCtrl">AnimatorController</param>
    /// <param name="charCtrlParams">CharacterController的参数列表</param>
    /// <param name="updateModify">是否立即更新所做的修改</param>
    //private static void CreateRolePrefab(string fbxName, Avatar avatar, AnimatorController aniCtrl, object[] charCtrlParams, bool updateModify = true)
    //{
    //    var fbxDir = "Assets/ResourcesLib/Roles/Fbx/" + fbxName.ToUpTitle() + "/";
    //    var fbxPath = Directory.GetFiles(fbxDir, "*.fbx")[0];
    //    var fbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;

    //    //创建prefab，由于每次都是新增Animator和CharacterController组件操作，所以prefab的改变在所难免
    //    var prefab = PrefabUtility.CreatePrefab("Assets/Resources/Roles/" + fbxName + ".prefab", fbx);
    //    var animation = prefab.GetComponent<Animator>();
    //    if (animation == null)
    //        animation = prefab.AddComponent<Animator>();
    //    if (avatar != null)
    //        animation.avatar = avatar;
    //    animation.cullingMode = AnimatorCullingMode.BasedOnRenderers;
    //    AnimatorController.SetAnimatorController(animation, aniCtrl);

    //    //创建CharacterController
    //    var charCtrl = prefab.AddComponent<CharacterController>();
    //    charCtrl.slopeLimit = (float)charCtrlParams[0];
    //    charCtrl.stepOffset = (float)charCtrlParams[1];
    //    charCtrl.center = (Vector3)charCtrlParams[2];
    //    charCtrl.radius = (float)charCtrlParams[3];
    //    charCtrl.height = (float)charCtrlParams[4];

    //    if (updateModify)
    //    {
    //        AssetDatabase.SaveAssets();
    //        AssetDatabase.Refresh();
    //    }
    //}

    /// <summary>
    /// 从源目录的FBX文件中提取Animation到目标目录
    /// </summary>
    /// <param name="source_path"> 源路径</param>
    /// <param name="dest_path"> 目标路径</param>
    public static void GetAnimation(string source_path, string dest_path)
    {
        int iAniClipNum = 0;
        DirectoryInfo kDI = new DirectoryInfo(source_path);
        FileInfo[] arrFI = kDI.GetFiles("*.FBX", SearchOption.AllDirectories);
        AnimationClip kAC = null;
        string strFileName, strExtention, strFbxAssetPath;
        foreach (FileInfo kFI in arrFI)
        {
            strFileName = kFI.Name;
            strExtention = kFI.Extension;
            strFbxAssetPath = source_path + "/" + strFileName;
            ModelImporter kMI = ModelImporter.GetAtPath(strFbxAssetPath) as ModelImporter;
            kMI.animationType = ModelImporterAnimationType.Legacy;
            if (kFI.Name.IndexOf("@") == -1)
            {
                continue;
            }
            List<ModelImporterClipAnimation> listMCA = new List<ModelImporterClipAnimation>();
            foreach (ModelImporterClipAnimation kMCA in kMI.clipAnimations)
            {
                listMCA.Add(kMCA);
            }
            AssetDatabase.Refresh();

            UnityEngine.Object[] arrObj = AssetDatabase.LoadAllAssetRepresentationsAtPath(strFbxAssetPath);
            foreach (UnityEngine.Object obj in arrObj)
            {
                kAC = obj as AnimationClip;
                if (kAC != null)
                {
                    if (kAC.name.IndexOf("Take") != -1)
                    {
                        kAC.name = strFileName.Substring(strFileName.IndexOf("@") + 1, strFileName.IndexOf(strExtention) - strFileName.IndexOf("@") - 1);
                    }
                    CreateAniClipAsset(kAC, dest_path, kAC.name);
                    iAniClipNum++;
                }
            }
        }
        if (iAniClipNum > 0)
        {
            string strContext = String.Format("导出完成，共导出{0}个动画文件", iAniClipNum);
            EditorUtility.DisplayDialog("导出动画文件", strContext, "确定");
        }
    }
    static private void CreateAniClipAsset(AnimationClip kSrcAniClip, string strDir, string strName)
    {
        string strClipAssetPath = strDir + "/" + strName + ".anim";
        AnimationClip kDstAniClip = new AnimationClip();
        EditorUtility.CopySerialized(kSrcAniClip, kDstAniClip);
        AssetDatabase.CreateAsset(kDstAniClip, strClipAssetPath);
        AssetDatabase.Refresh();
    }

    //public static void SetFbxLegacy(string path)
    //{
    //    DirectoryInfo kDI = new DirectoryInfo(path);
    //    FileInfo[] arrFI = kDI.GetFiles("*.FBX", SearchOption.AllDirectories);
    //    int i = 0;
    //    foreach (var kFI in arrFI)
    //    {
    //        i++;
    //        string strFbxAssetPath = kFI.FullName.Substring(kFI.FullName.IndexOf("Assets"));
    //        ModelImporter kMI = ModelImporter.GetAtPath(strFbxAssetPath) as ModelImporter;
    //        kMI.animationType = ModelImporterAnimationType.Legacy;
    //    }

    //    string strContext = String.Format("共处理{0}个Fbx", i);
    //    EditorUtility.DisplayDialog("设置模型骨骼为Legacy", strContext, "确定");
    //}
}
