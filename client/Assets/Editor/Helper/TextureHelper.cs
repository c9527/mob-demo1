﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System;

internal class TexImpArg
{
    public bool needTransparent;

    public bool checkMipMap = true;
    public bool needMipMap;

    public bool needSetFormat = false;
    public TextureImporterFormat tf;

    public bool needSetFilerMode = false;
    public FilterMode fm;

    public bool needSetTexSize = false;
    public int maxTextureSize = 1024;

    public bool needSetAnisolevel = true;
    public int anisoLevel = -1;
}

public class TextureHelper 
{
    static private List<string> PIC_EXTENTION = new List<string> { ".tga", ".png", ".jpg", ".bmp"};
    static private List<string> LIGHTMAP_EXTENTION = new List<string> { ".exr" };
     


#region 贴图格式信息

    static internal TexImpArg texArg_Scene
    {
        get{
            TexImpArg arg = new TexImpArg();
             
            arg.needTransparent = true;
            arg.needMipMap = true;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetFilerMode = true;
            arg.fm = FilterMode.Trilinear;

             return arg;
        }
    }

    static internal TexImpArg texArg_SkyBox
    {
        get{

            TexImpArg arg = new TexImpArg();
            arg.needTransparent = false;
            arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetFilerMode = true;
            arg.fm = FilterMode.Trilinear;
            return arg;
        }
    }

    static internal TexImpArg texArg_Player
    {
        get{
            TexImpArg arg = new TexImpArg();

            arg.needTransparent = false;
            arg.checkMipMap = false;    // 手动设置是否开启角色MipMap，用于画质设置
            arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetFilerMode = true;
            arg.fm = FilterMode.Bilinear;

            return arg;
        }
    }

    static internal TexImpArg texArg_Weapon
    {
        get{
                TexImpArg arg = new TexImpArg();
                arg.needTransparent = false;
                arg.needMipMap = false;

                arg.needSetFormat = true;
                arg.tf = TextureImporterFormat.AutomaticCompressed;

                arg.needSetFilerMode = true;
                arg.fm = FilterMode.Bilinear;
                return arg;
           }
    }

    static internal TexImpArg texArg_SceneEffect
    {
        get{
            TexImpArg arg = new TexImpArg();
             
            arg.needTransparent = true;
            arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetFilerMode = true;
            arg.fm = FilterMode.Bilinear;

            return arg;
        }
    }

    static internal TexImpArg texArg_UIEffect
    {
        get{
            
            TexImpArg arg = new TexImpArg();
             
            arg.needTransparent = true;
            arg.needMipMap = false;

            arg.needSetFormat = false;
            //arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetFilerMode = false;
            //arg.fm = FilterMode.Bilinear;

            return arg;
        }
       
    }

    static internal TexImpArg texArg_AtlasBattleSmallMap
    {
       get{
           TexImpArg arg = new TexImpArg();
            
            arg.needTransparent = true;
            arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            arg.needSetTexSize = true;
            arg.maxTextureSize = 256;

            return arg;
       }
    }

    static internal TexImpArg texArg_AtlasAlpha
    {
        get
        {
            TexImpArg arg = new TexImpArg();

            arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticCompressed;

            return arg;
        }
    }

    static internal TexImpArg texArg_LightMap
    {
        get
        {
            TexImpArg arg = new TexImpArg();
            //arg.needMipMap = false;

            arg.needSetFormat = true;
            arg.tf = TextureImporterFormat.AutomaticTruecolor;

            arg.needSetTexSize = true;
            arg.maxTextureSize = 512;

            arg.needSetAnisolevel = false;
			
			arg.needSetFilerMode = false;

            return arg;
        }
    }
#endregion
   
    static public void CheckTex()
    {
        CheckSceneTex();
        CheckLightMap();
        CheckPlayerTex();
        CheckWeaponTex();
        CheckEffectTex();
    }

    static public void CheckSceneTex()
    {
        string sceneLibPath = "Assets/ResourcesLib/Scenes/";

        EditorHelper.VisitFileTraverse(sceneLibPath, PIC_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));
            string name = assetPath.Substring(assetPath.LastIndexOf('\\') + 1);

            if (name.ToLower().Contains(EditorConst.SKYBOX) == true)
                SetTexImp(assetPath, texArg_SkyBox);
            else
                SetTexImp(assetPath, texArg_Scene);
        });

        AssetDatabase.Refresh();         
    }

    static public void CheckLightMap()
    {
        string sceneLibPath = "Assets/Resources/Scenes/";

        EditorHelper.VisitFileTraverse(sceneLibPath, LIGHTMAP_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));
            SetLightMap(assetPath);
        });

        AssetDatabase.Refresh();    
    }

    static public void CheckPlayerTex()
    {
        string sceneLibPath = EditorConst.PLAYER_TEX_PATH;

        EditorHelper.VisitFileTraverse(sceneLibPath, PIC_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));
            SetTexImp(assetPath, texArg_Player);
        });

        AssetDatabase.Refresh();
    }

    static public void CheckWeaponTex()
    {
        string sceneLibPath = EditorConst.WEAPON_TEX_PATH;
        string Transparent_Cristal = "crystal";

        EditorHelper.VisitFileTraverse(sceneLibPath, PIC_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));

            string fileName = assetPath.Substring(assetPath.LastIndexOf('\\') + 1);
            texArg_Weapon.needTransparent = fileName.ToLower().Contains(Transparent_Cristal);

            SetTexImp(assetPath, texArg_Weapon);
        });
        
        AssetDatabase.Refresh();
    }

    /// <summary>
    /// 设置特效贴图 去除MipMap，是否压缩
    /// </summary>
    static public void CheckEffectTex()
    {
        string sceneEffectPath = "Assets/ResourcesLib/Effect/Textures";
        string uiEffectPath = "Assets/ResourcesLib/Effect/Textures/UI";

        EditorHelper.VisitFileNoTraverse(sceneEffectPath, PIC_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));
            SetTexImp(assetPath, texArg_SceneEffect);
        });

        EditorHelper.VisitFileNoTraverse(uiEffectPath, PIC_EXTENTION, (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));
            SetTexImp(assetPath, texArg_UIEffect);
        });

        AssetDatabase.Refresh();
    }

#region private

    static internal void SetLightMap(string assetPath, bool saveAsset = true)
    {		
        TextureImporter texImp = TextureImporter.GetAtPath(assetPath) as TextureImporter;
        if (texImp == null)
            return;

        texImp.ClearPlatformTextureSettings("Android");
        texImp.ClearPlatformTextureSettings("iPhone");
        //texImp.SetPlatformTextureSettings("Default", 512, TextureImporterFormat.AutomaticTruecolor);

        //AssetDatabase.WriteImportSettingsIfDirty(texImp.assetPath);

        SetTexImp(assetPath, texArg_LightMap, saveAsset, TextureImporterType.Lightmap);

        //if (saveAsset)
            //AssetDatabase.SaveAssets();

        AssetDatabase.Refresh();
    }

    static internal void SetTexImp(string assetPath, TexImpArg texArg, bool saveAsset = true, TextureImporterType texType = TextureImporterType.Advanced)
    {
        TextureImporter texImp = TextureImporter.GetAtPath(assetPath) as TextureImporter;
        if (texImp == null)
            return;

        texImp.textureType = texType;

		if(texType != TextureImporterType.Lightmap)
			texImp.isReadable = false;

        if (texArg.needSetAnisolevel && texImp.anisoLevel != 1)
            texImp.anisoLevel = texArg.anisoLevel;

        if (texArg.checkMipMap)
            texImp.mipmapEnabled = texArg.needMipMap;
	
		if(texType != TextureImporterType.Lightmap)
			texImp.alphaIsTransparency = texImp.DoesSourceTextureHaveAlpha() && texArg.needTransparent;

		if (texArg.needSetTexSize)
            texImp.maxTextureSize = texArg.maxTextureSize;

        if (texArg.needSetFormat)
        {
            texImp.textureFormat = texArg.tf;
            texImp.SetPlatformTextureSettings("Default", 1024, texArg.tf);
        }
		
        if (texArg.needSetFilerMode)
            texImp.filterMode = texArg.fm;

        texImp.ClearPlatformTextureSettings("Android");
        texImp.ClearPlatformTextureSettings("iPhone");		
		
        AssetDatabase.WriteImportSettingsIfDirty(texImp.assetPath);		
		
        if (saveAsset)
            AssetDatabase.SaveAssets();		
		
        AssetDatabase.Refresh();

        if (texType != TextureImporterType.Lightmap && texImp.DoesSourceTextureHaveAlpha() && texArg.needTransparent == false)
            Debug.LogError("贴图含有透明通道 " + texImp.assetPath);
    }

    
#endregion


}
