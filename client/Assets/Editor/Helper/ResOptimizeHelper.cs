﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


public class ResOptimizeHelper 
{

    static public void OptimizeEffect()
    {
        TextureHelper.CheckEffectTex();
        ShaderHelper.CheckEffectShader();
        PrefabHelper.CheckEffectPrefab();

        EditorUtility.DisplayDialog("", "完成", "OK");
    }

    static public void OptimizeScene()
    {
        TextureHelper.CheckSceneTex();
        TextureHelper.CheckLightMap();
        ShaderHelper.CheckSceneShader();
        EditorUtility.DisplayDialog("", "完成", "OK");
    }

    static public void OptimizeWeapon()
    {
        TextureHelper.CheckWeaponTex();
    }

    static public void OptimizePlayer()
    {
        TextureHelper.CheckPlayerTex();
    }

    static public void DeleteUnusedEffectRes()
    {
        // 有些特效是没有预设的，直接挂在武器上的，会导致误删除
        return;

        string effectPath = "Assets/Resources/Effect";
        string effectTexPath = "Assets/ResourcesLib/Effect/Textures";
        string effectMatPath = "Assets/ResourcesLib/Effect/Materials";
        string effectMatPath2 = "Assets/ResourcesLib/Effect/Textures/UI/Materials";

        HashSet<string> prefabMat = new HashSet<string>();
        HashSet<string> prefabTex = new HashSet<string>();

        HashSet<string> delAsset = new HashSet<string>();

        EditorHelper.VisitAssetNoTraverse(effectPath, ".prefab", (obj) =>
        {
            GameObject go = obj as GameObject;
            if (go == null)
                return;

            GameObjectHelper.VisitChildByTraverse(go.transform, (child) =>
            {
                Renderer r = child.GetComponent<Renderer>();
                if(r != null)
                {
                    Material mat = r.sharedMaterial;
                    if (mat != null)
                    {
                        prefabMat.Add(mat.name);
                        Texture tex = mat.mainTexture;
                        if (tex != null)
                            prefabTex.Add(tex.name);
                    }
                }

            }, true);
        });

        EditorHelper.VisitAssetTraverse(effectTexPath, "", (obj) =>
        {
            Texture tex = obj as Texture;
            if (tex != null)
            {
                if (prefabTex.Contains(tex.name) == false)
                {
                    delAsset.Add(AssetDatabase.GetAssetPath(obj));
                    Debug.Log(tex.name, obj);
                }
            }
        });
        
        EditorHelper.VisitAssetTraverse(effectMatPath, "", (obj) =>
        {
            Material mat = obj as Material;

            if (mat != null)
            {
                if (prefabMat.Contains(mat.name) == false)
                {
                    delAsset.Add(AssetDatabase.GetAssetPath(obj));
                    Debug.Log(mat.name, obj);
                }
            }
        });

        EditorHelper.VisitAssetTraverse(effectMatPath2, "", (obj) =>
        {
            Material mat = obj as Material;

            if (mat != null)
            {
                if (prefabMat.Contains(mat.name) == false)
                {
                    delAsset.Add(AssetDatabase.GetAssetPath(obj));
                    Debug.Log(mat.name, obj);
                }
            }
        });

        bool ok = EditorUtility.DisplayDialog("", "确认要删除多余的 " + delAsset.Count + "  个特效资源吗？", "删除", "取消");
        
        if(ok)
        {
            foreach(string str in delAsset)
            {
                AssetDatabase.DeleteAsset(str);
            }
        }
    }

	 
}
