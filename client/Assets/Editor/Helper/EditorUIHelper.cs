﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class EditorUIHelper
{
    private static readonly string[] COMPRESS_DIR_NAME = new[] { "Icon", "SmallMap", "ArmyIcon", "RoleIcon", "Background", "BattleSmallMap", "ActivityFrame", "Loading" };
    /// <summary>
    /// 重新按照目录名分配图片的图集名
    /// </summary>
    public static void UpdateAtlas(string[] atlasDirPaths = null)
    {
        var dirArr = Directory.GetDirectories("Assets/ResourcesLib/Atlas/");
        if (atlasDirPaths != null)
            dirArr = atlasDirPaths;
        for (int i = 0; i < dirArr.Length; i++)
        {
            var pathArr = Directory.GetFiles(dirArr[i]);
            var compress = false;
            var dirName = Path.GetFileName(dirArr[i]);
            for (int j = 0; j < COMPRESS_DIR_NAME.Length; j++)
            {
                if (COMPRESS_DIR_NAME[j] == dirName)
                    compress = true;
            }

            for (int j = 0; j < pathArr.Length; j++)
            {
                if (Path.GetExtension(pathArr[j]) == ".meta")
                    continue;
                var progress = i*1f/dirArr.Length + (j + 1f)/pathArr.Length/dirArr.Length;
                EditorUtility.DisplayProgressBar("更新图集Tag", pathArr[j], progress);
                var importer = AssetImporter.GetAtPath(pathArr[j]) as TextureImporter;
                if (importer != null)
                {
                    if (dirName == "ShareAvatar")
                    {
                        importer.textureType = TextureImporterType.Advanced;
                        importer.spriteImportMode = SpriteImportMode.Single;
                        importer.isReadable = true;
                    }
                    else
                    {
                        importer.textureType = TextureImporterType.Sprite;
                    }
                    importer.mipmapEnabled = false;
                    if (compress)
                        importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
                    else
                        importer.textureFormat = TextureImporterFormat.ARGB32;
                    importer.spritePackingTag = Path.GetFileName(dirArr[i]);
                    AssetDatabase.WriteImportSettingsIfDirty(pathArr[j]);
                }
            }

//            EditorUtility.DisplayProgressBar("创建图集Prefab", dirArr[i], (i + 1f) / dirArr.Length);
//            CreateAtlasPrefab(Path.GetFileName(dirArr[i]));
        }

        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
        EditorUtility.ClearProgressBar();

        for (int i = 0; i < dirArr.Length; i++)
        {
            EditorUtility.DisplayProgressBar("创建图集Prefab", dirArr[i], (i + 1f) / dirArr.Length);
            CreateAtlasPrefab(Path.GetFileName(dirArr[i]));
        }
        AssetDatabase.SaveAssets();
        EditorUtility.ClearProgressBar();
    }

    private static void CreateAtlasPrefab(string atlas)
    {
        var pathArr = Directory.GetFiles("Assets/ResourcesLib/Atlas/" + atlas + "/");
        var list = new List<Object>();
        for (int i = 0; i < pathArr.Length; i++)
        {
            if (Path.GetExtension(pathArr[i]) == ".meta")
                continue;
            var sp = AssetDatabase.LoadAssetAtPath(pathArr[i], typeof (Sprite));
            if (sp != null)
            {
                list.Add(sp);
            }
        }

        if(list.Count == 0)
            return;
        var prefabPath = "Assets/Resources/Atlas/" + atlas + ".prefab";
        var altasGo = AssetDatabase.LoadMainAssetAtPath(prefabPath) as GameObject;
        if (altasGo == null)
        {
            altasGo = new GameObject();
            var holder = altasGo.AddComponent<SpritesHolder>();
            holder.Sprites = new Sprite[list.Count];
            for (int i = 0; i < holder.Sprites.Length; i++)
            {
                holder.Sprites[i] = list[i] as Sprite;
            }
            PrefabUtility.CreatePrefab(prefabPath, altasGo);
            GameObject.DestroyImmediate(altasGo);
        }
        else
        {
            var holder = altasGo.GetComponent<SpritesHolder>();
            holder.Sprites = new Sprite[list.Count];
            for (int i = 0; i < holder.Sprites.Length; i++)
            {
                holder.Sprites[i] = list[i] as Sprite;
            }
            EditorUtility.SetDirty(altasGo);
            AssetDatabase.SaveAssets();
//            PrefabUtility.CreatePrefab(prefabPath, altasGo);
        }
    }

    public static Text CreateText(int size, Transform parent = null, string text = null, Color32 color = default(Color32), Color32 shadowColor = default(Color32), bool shadow = false, bool outline = false, float width = 160f, float height = 30f, TextAnchor alignment = TextAnchor.UpperLeft)
    {
        if (parent == null)
        {
            if (Selection.activeGameObject == null)
                return null;
            parent = Selection.activeGameObject.GetComponent<RectTransform>();
            if (parent == null)
                return null;
        }
        
        var textGo = new GameObject("Text");
        textGo.layer = LayerMask.NameToLayer("UI");

        var txt = textGo.AddComponent<Text>();
        txt.text = text ?? "New Text";
        txt.fontSize = size;
        txt.alignment = alignment;
        txt.horizontalOverflow = HorizontalWrapMode.Overflow;
        txt.verticalOverflow = VerticalWrapMode.Overflow;
        if (color != default(Color))
            txt.color = color;

        var tran = textGo.GetComponent<RectTransform>();
        tran.SetUILocation(parent, 0, 0);
        tran.sizeDelta = new Vector2(width, height);

        if (outline)
            textGo.AddComponent<Outline>();
        if (shadow)
        {
            var eff = textGo.AddComponent<Shadow>();
            eff.effectColor = shadowColor;
            eff.effectDistance = new Vector2(0.5f, -0.5f);
        }

        var font = AssetDatabase.LoadAssetAtPath("Assets/Resources/Font/Normal.ttf", typeof (Font)) as Font;
        txt.font = font;

        return txt;
    }

    public static void CreateNormalButton(Transform parent = null)
    {
        CreateButton(parent, AtlasName.Common, "WhiteBtn_upSkin", new Color32(62, 41, 24, 255), false, true, new Color32(62, 41, 24, 255), 24, 100f, 49f);
    }

    public static void CreateYellowButton(Transform parent = null)
    {
        CreateButton(parent, AtlasName.Common, "YellowBtn_upSkin", new Color32(62, 41, 24, 255), false, true, new Color32(62, 41, 24, 255), 24, 100f, 49f);
    }

    public static void CreateNormalButtonOutline(Transform parent = null)
    {
        CreateButton(parent, AtlasName.Common, "WhiteBtn_upSkin", new Color32(41, 44, 48, 255), true, true, new Color32(72, 171, 195, 162), 24, 100f, 49f);
    }

    public static void CreateRoundedBlueButton(Transform parent = null)
    {
        CreateButton(parent, AtlasName.Strengthen, "BlueBtn_upSkin", Color.white, false, false, Color.white, 18, 110f, 41f);
    }

    public static void CreateRoundedYellowButton(Transform parent = null)
    {
        CreateButton(parent, AtlasName.Strengthen, "YellowBtn_upSkin", new Color32(101, 45, 23, 255), false, false, Color.white, 18, 110f, 41f);
    }

   public static void CreateButton(Transform parent, string atlasName, string spriteName, Color32 fontColor, bool buttonOutline = false, bool fontShadow = false, Color32 fontShadowColor=default(Color32), int fontSize = 18, float width = 116f, float height = 59f)
    {
        if (parent == null)
        {
            if (Selection.activeGameObject == null)
                return;
            parent = Selection.activeGameObject.GetComponent<RectTransform>();
            if (parent == null)
                return;
        }
        var btnGo = new GameObject("Button");
        btnGo.layer = LayerMask.NameToLayer("UI");

        if (buttonOutline)
        {
            var eff = btnGo.AddComponent<Outline>();
            eff.effectColor = new Color32(55, 55, 55, 255);
            eff.effectDistance = new Vector2(3, -3);
        }

        var image = btnGo.AddComponent<Image>();
        var sp = AssetDatabase.LoadAssetAtPath("Assets/ResourcesLib/Atlas/" + atlasName + "/" + spriteName+".png", typeof(Sprite)) as Sprite;
        image.SetSprite( sp);
        if (sp.border != Vector4.zero)
            image.type = Image.Type.Sliced;

        var btn = btnGo.AddComponent<Button>();
        var txt = CreateText(fontSize, btn.transform, "按钮", fontColor, fontShadowColor, fontShadow, false, width, height, TextAnchor.MiddleCenter);
        var rectTran = txt.rectTransform;
        rectTran.anchorMin = Vector2.zero;
        rectTran.anchorMax = Vector2.one;
        rectTran.anchoredPosition = Vector2.zero;
        rectTran.sizeDelta = Vector2.zero;

        var tran = btnGo.GetComponent<RectTransform>();
        tran.SetUILocation(parent, 0, 0);
        tran.sizeDelta = new Vector2(width, height);
    }

    public static void CreateScrollDataGrid(Transform parent = null, Direction dir = Direction.Vertical, Vector2 visualSize = default(Vector2), Vector2 cellSize = default(Vector2), Vector2 spacing = default(Vector2))
    {
        if (parent == null)
        {
            if (Selection.activeGameObject == null)
                return;
            parent = Selection.activeGameObject.GetComponent<RectTransform>();
            if (parent == null)
                return;
        }

        var testItem = visualSize == Vector2.zero && cellSize == Vector2.zero && spacing == Vector2.zero;
        if (visualSize == Vector2.zero)
            visualSize = dir == Direction.Vertical ? new Vector2(500, 300) : new Vector2(500, 100);
        if (cellSize == Vector2.zero)
            cellSize = dir == Direction.Vertical ? new Vector2(visualSize.x, 100) : new Vector2(200, visualSize.y);
        if (spacing == Vector2.zero)
            spacing = new Vector2(5, 5);

        var scrollDataGridGo = new GameObject("ScrollDataGrid");
        var scrollDataGridTran = scrollDataGridGo.AddComponent<RectTransform>();
        var scrollRect = scrollDataGridGo.AddComponent<ScrollRect>();
        scrollDataGridGo.AddComponent<Image>(); //mask用Image
        var mask = scrollDataGridGo.AddComponent<Mask>();

        var contentGo = new GameObject("content");
        var contentTran = contentGo.AddComponent<RectTransform>();
        var gridLayout = contentGo.AddComponent<GridLayoutGroup>();
        var sizeFitter = contentGo.AddComponent<ContentSizeFitter>();
        contentGo.AddComponent<ToggleGroup>();

        scrollDataGridTran.SetParent(parent, false);
        scrollDataGridTran.sizeDelta = visualSize;
        scrollRect.content = contentTran;
        scrollRect.horizontal = dir == Direction.Horizontal;
        scrollRect.vertical = dir == Direction.Vertical;
        mask.showMaskGraphic = false;

        contentGo.transform.SetParent(scrollDataGridTran, false);
        contentTran.anchorMin = new Vector2(0, 1);
        contentTran.anchorMax = new Vector2(0, 1);
        contentTran.pivot = new Vector2(0, 1);
        contentTran.anchoredPosition = Vector2.zero;
        contentTran.sizeDelta = new Vector2(100, 100);
        gridLayout.cellSize = cellSize;
        gridLayout.spacing = spacing;
        gridLayout.constraint = dir == Direction.Vertical ? GridLayoutGroup.Constraint.FixedColumnCount : GridLayoutGroup.Constraint.FixedRowCount;
        gridLayout.constraintCount = 1;
        sizeFitter.horizontalFit = dir == Direction.Horizontal ? ContentSizeFitter.FitMode.PreferredSize : ContentSizeFitter.FitMode.Unconstrained;
        sizeFitter.verticalFit = dir == Direction.Vertical ? ContentSizeFitter.FitMode.PreferredSize : ContentSizeFitter.FitMode.Unconstrained;

        if (testItem)
        {
            for (int i = 0; i < 5; i++)
            {
                var testImage = new GameObject("test").AddComponent<Image>();
                testImage.transform.SetParent(contentTran, false);
            }
        }

        scrollDataGridGo.ApplyLayer(LayerMask.NameToLayer("UI"));
    }

    public static string GenerateMd5(string filePath)
    {
        var md5 = new MD5CryptoServiceProvider();
        using (var file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
            var hashBytes = md5.ComputeHash(file);
            var md5String = BitConverter.ToString(hashBytes).Replace("-", "");
            return md5String;
        }
    }

    public static Dictionary<string, List<string>> GenerateAtlasMd5()
    {
        var md5 = new MD5CryptoServiceProvider();
        var dic = new Dictionary<string, List<string>>();
        var result = new StringBuilder();

        var dirArr = Directory.GetDirectories("Assets/ResourcesLib/Atlas/");
        for (int i = 0; i < dirArr.Length; i++)
        {
            var pathArr = PluginUtil.GetFiles(dirArr[i]);
            for (int j = 0; j < pathArr.Length; j++)
            {
                var filePath = pathArr[j];
                var progress = i * 1f / dirArr.Length + (j + 1f) / pathArr.Length / dirArr.Length;
                EditorUtility.DisplayProgressBar("计算图集图片的MD5值...", filePath, progress);

                using (var file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var hashBytes = md5.ComputeHash(file);
                    var md5String = BitConverter.ToString(hashBytes).Replace("-", "");
                    result.AppendLine(md5String + " " + filePath);
                    if (!dic.ContainsKey(md5String))
                        dic.Add(md5String, new List<string>());
                    dic[md5String].Add(filePath);
                }
            }
        }
        EditorUtility.ClearProgressBar();
        return dic;
    }

    public static void BattleUIControlPosExport()
    {
        string sceneName = EditorHelper.GetCurSceneName();
        if (sceneName != "UIMakeScene")
        {
            EditorUtility.DisplayDialog("", "请打开UIMakeScene", "ok");
            return;
        }

        GameObject PanelBattlePrefab = GameObject.Find("PanelBattle");
        if (PanelBattlePrefab == null)
        {
            EditorUtility.DisplayDialog("", "找不到PanelBattle：", "OK");
            return;
        }

        //Debug.Log("读取配置表：ConfigBattleUIControlPos");
        ConfigBattleUIControlPos config = ScriptableObject.CreateInstance<ConfigBattleUIControlPos>();

        string path = "Assets/Resources/Config/ConfigBattleUIControlPos.csv";
        try
        {
            string content = File.ReadAllText(path);
            config.Load(content);
            config.ReBuildDic();
        }
        catch (Exception e)
        {
            EditorUtility.DisplayDialog("", "打开【ConfigBattleUIControlPos.csv】配置文件报错, 请先确保文件已经关闭：" + e.Message, "OK");
            return;
        }

        var trans = PanelBattlePrefab.transform;
        Transform child = null;
        int count = trans.childCount;
        for (int i = 0; i < count; i++)
        {
            child = trans.GetChild(i);
            var line = config.GetLine(child.name);
            if (line == null) continue;

            var rect = child.GetComponent<RectTransform>();
            if (rect == null) { Debug.LogError(child.name + " got no rectTransform component---"); continue; }
            line.anchorX = rect.anchoredPosition.x;
            line.anchorY = rect.anchoredPosition.y;
            line.fAnchorMinX = rect.anchorMin.x;
            line.fAnchorMinY = rect.anchorMin.y;
            line.fAnchorMaxX = rect.anchorMax.x;
            line.fAnchorMaxY = rect.anchorMax.y;
            line.fLocalScale = rect.localScale.x;
            line.fWidth = rect.sizeDelta.x;
            line.fHeight = rect.sizeDelta.y;
        }

        File.WriteAllText(path, config.Export(), Encoding.Unicode);
        EditorUtility.DisplayDialog("", "导出数据成功！", "ok");
    }
}
   

public enum Direction
{
    Horizontal,
    Vertical
}