﻿// PNG Format Helper
//
// see: http://www.w3.org/TR/PNG/
//

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using UnityEditor;

static class CPngHelper
{
    static byte[] sign = { 137, 80, 78, 71, 13, 10, 26, 10 };   // "\x89PNG\r\n\x1a\n"

    public class Chunk
    {
        public string name;
        public byte[] data;
    }

    public enum ColorType
    {
        Greyscale = 0,
        Truecolour = 2,
        IndexedColour = 3,
        GreyscaleWithAlpha = 4,
        TruecolourWithAlpha = 6
    }

    public class IHDR
    {
        public int width;
        public int height;
        public int bitDepth;
        public ColorType colourType;
        public int compressionMethod;
        public int filterMethod;
        public int interlaceMethod;

        public static IHDR FromChunk(Chunk chunk)
        {
            IHDR ihdr = new IHDR();
            if (chunk.name != "IHDR")
                return null;
            Trace.Assert(chunk.data.Length == 13);
            using (MemoryStream stm = new MemoryStream(chunk.data))
            {
                ihdr.width = (int)CDataHelper.BE_ReadUInt32(stm);
                Trace.Assert(ihdr.width > 0);
                ihdr.height = (int)CDataHelper.BE_ReadUInt32(stm);
                Trace.Assert(ihdr.height > 0);
                ihdr.bitDepth = stm.ReadByte();
                Trace.Assert(ihdr.bitDepth == 1 || ihdr.bitDepth == 2 || ihdr.bitDepth == 4 || ihdr.bitDepth == 8 || ihdr.bitDepth == 16);
                ihdr.colourType = (ColorType)stm.ReadByte();
                ihdr.compressionMethod = stm.ReadByte();
                Trace.Assert(ihdr.compressionMethod == 0);  // Only compression method 0 (deflate) is defined in this International Standard.
                ihdr.filterMethod = stm.ReadByte();
                Trace.Assert(ihdr.filterMethod == 0);   // Only filter method 0 is defined in this International Standard.
                ihdr.interlaceMethod = stm.ReadByte();
                Trace.Assert(ihdr.interlaceMethod == 0 || ihdr.interlaceMethod == 1); // Two values are defined in this International Standard: 0 (no interlace) or 1 (Adam7 interlace).
            }

            return ihdr;
        }

        public Chunk ToChunk()
        {
            Chunk chunk = new Chunk();
            chunk.name = "IHDR";
            using (MemoryStream stm = new MemoryStream(13))
            {
                CDataHelper.BE_WriteUInt32(stm, (uint)width);
                CDataHelper.BE_WriteUInt32(stm, (uint)height);
                stm.WriteByte((byte)bitDepth);
                stm.WriteByte((byte)colourType);
                stm.WriteByte((byte)compressionMethod);
                stm.WriteByte((byte)filterMethod);
                stm.WriteByte((byte)interlaceMethod);
                chunk.data = stm.ToArray();
            }
            return chunk;
        }
    }

    public static bool IsPngFile(string file)
    {
        using (Stream stm = File.Open(file, FileMode.Open))
        {
            for (int i = 0; i < sign.Length; i++)
                if (stm.ReadByte() != sign[i])
                    return false;
        }
        return true;
    }

    public static Chunk[] ReadChunks(Stream stm)
    {
        for (int i = 0; i < sign.Length; i++)
            Trace.Assert(stm.ReadByte() == sign[i]);
        List<Chunk> chunks = new List<Chunk>();
        while (true)
        {
            Chunk chunk = new Chunk();
            chunks.Add(chunk);

            int len = (int)CDataHelper.BE_ReadUInt32(stm);
            Trace.Assert(len >= 0);
            chunk.name = CDataHelper.ReadUtfStr(stm, 4);
            chunk.data = new byte[len];
            stm.Read(chunk.data, 0, len);
            stm.Position -= len + 4;
            uint crc1 = CDataHelper.CRC32(stm, len + 4);
            uint crc2 = CDataHelper.BE_ReadUInt32(stm);
            Trace.Assert(crc1 == crc2);

            if (chunk.name == "IEND")
                break;
        }
        return chunks.ToArray();
    }

    public static Chunk[] ReadChunks(string file)
    {
        Stream stm = File.Open(file, FileMode.Open);
        Chunk[] chunks = ReadChunks(stm);
        Trace.Assert(stm.Position == stm.Length);
        stm.Close();
        return chunks;
    }

    public static byte[] ReadChunk(string file, string name)
    {
        Chunk[] chunks = ReadChunks(file);
        foreach (Chunk chunk in chunks)
        {
            if (chunk.name == name)
                return chunk.data;
        }
        return null;
    }

    public static void WriteChunks(Stream stm, Chunk[] chunks)
    {
        foreach (byte b in sign)
            stm.WriteByte(b);

        foreach (Chunk chunk in chunks)
        {
            CDataHelper.BE_WriteUInt32(stm, (uint)chunk.data.Length);

            byte[] buf = Encoding.UTF8.GetBytes(chunk.name);
            Trace.Assert(buf.Length == 4);
            stm.Write(buf, 0, buf.Length);
            stm.Write(chunk.data, 0, chunk.data.Length);
                
            stm.Position -= chunk.data.Length + 4;
            uint crc = CDataHelper.CRC32(stm, chunk.data.Length + 4);

            CDataHelper.BE_WriteUInt32(stm, crc);
        }
    }

    public static void WriteChunks(string file, Chunk[] chunks)
    {
        Stream stm = File.Open(file, FileMode.Create);
        WriteChunks(stm, chunks);
        stm.Close();
    }

    public static void UpdateChunk(string file, string name, byte[] data)
    {
        Chunk[] chunks = ReadChunks(file);
        foreach (Chunk chunk in chunks)
        {
            if (chunk.name == name)
            {
                chunk.data = data;
                break;
            }
            else if (chunk.name == "IEND")
            {
                Chunk newChunk = new Chunk();
                newChunk.name = name;
                newChunk.data = data;

                List<Chunk> list = chunks.ToList();
                list.Insert(chunks.Length - 1, newChunk);
                chunks = list.ToArray();

                break;
            }
        }
        WriteChunks(file, chunks);
    }
    /// <summary>
    /// 设置PNG图片不使用多重纹理映射
    /// </summary>
    /// <param name="path"> 要操作的目标文件夹</param>
    public static void Spritesetting(string path)
    {
        DirectoryInfo kDI = new DirectoryInfo(path);
        FileInfo[] arrFI = kDI.GetFiles("*.png", SearchOption.AllDirectories);
        foreach (FileInfo fi in arrFI)
        {
            string file = fi.FullName.Substring(fi.FullName.IndexOf("Assets"));
            TextureImporter TI = TextureImporter.GetAtPath(file) as TextureImporter;
            TI.mipmapEnabled = false;
        }
        EditorUtility.DisplayDialog("提示", "设置成功 " + arrFI.Length, "ok");
    }
}
