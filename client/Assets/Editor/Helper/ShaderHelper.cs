﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;


public class ShaderHelper 
{

     [MenuItem("Assets/更新材质Shader")]
    static public void CheckMaterialShader()
    {
        Debug.developerConsoleVisible = true;
        Debug.ClearDeveloperConsole();

        Object[] arrObj = Selection.objects;

        foreach(Object obj in arrObj)
        {
            if (obj is Material)
            {
                Material mat = obj as Material;
                ReplaceShader(mat);
            }
            else if(obj is GameObject)
            {
                GameObject go = obj as GameObject;
                if (go == null)
                    continue;

                List<Renderer> listR = new List<Renderer>();
                GameObjectHelper.GetComponentsByTraverse<Renderer>(go.transform, ref listR);
                foreach (Renderer r in listR)
                {
                    ReplaceShader(r.sharedMaterial, go);
                }
            }

            AssetDatabase.SaveAssets();
        }

        EditorUtility.DisplayDialog("", "完成", "OK");
    }

    static public void CheckEffectShader()
    {
        string path = "Assets/ResourcesLib/Effect/Materials";

        EditorHelper.VisitFileNoTraverse(path, ".mat", (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));

            Material mat = AssetDatabase.LoadMainAssetAtPath(assetPath) as Material;

            ReplaceShader(mat);
        });

        AssetDatabase.Refresh();
    }

    static public void CheckSceneShader()
    {
        string path = "Assets/ResourcesLib/Scenes";

        EditorHelper.VisitFileTraverse(path, ".mat", (filePath) =>
        {
            string assetPath = filePath.Substring(filePath.IndexOf("Assets"));

            Material mat = AssetDatabase.LoadMainAssetAtPath(assetPath) as Material;

            ReplaceShader(mat);
        });

        AssetDatabase.Refresh();
    }

     static private void ReplaceShader(Material mat, GameObject go = null)
     {
         if (mat == null || mat.name == "Default-Particle")
             return;

         Shader shader = mat.shader;
         string newShaderName = "";

         switch(shader.name)
         {
             case "Particles/Alpha Blended":
                 newShaderName = "FPS/AlphaBlend_TintColor";               
                 break;
             case "Particles/Additive":
                 newShaderName = "FPS/Additive_TintColor";                 
                 break;
             case "Particles/Multiply":
                 newShaderName = "FPS/Multiply";
                 break;

             case "Mobile/Particles/Additive":             
                 newShaderName = "FPS/Additive";
                 break;
             case "Mobile/Particles/Alpha Blended":            
                 newShaderName = "FPS/AlphaBlend";
                 break;
             case "Mobile/Particles/Multiply":
                 newShaderName = "FPS/Multiply";
                 break;
            
             case "Mobile/Unlit (Supports Lightmap)":
                 newShaderName = GameConst.SHADER_LIGHTMAP;
                 break;

             case "Unlit/Texture":
                 newShaderName = "FPS/Texture";
                 break;
             case "Diffuse":
                 newShaderName = GameConst.SHADER_DIFFUSE;
                 break;
             case "Self-Illumin/Diffuse":
                 newShaderName = "FPS/Texture";
                 break;
         }

         if (string.IsNullOrEmpty(newShaderName))
         {
             if(shader.name.StartsWith("FPS") && shader.name.Contains("Unity") == true && shader.name != "FPS/Unity/Unity_Diffuse")
                Debug.LogError("找不到对应的Shader:\t" + shader.name + "\t" + mat.name, mat);
             return;
         }

         Shader newShader = Shader.Find(newShaderName);
         mat.shader = newShader;
         AssetDatabase.SaveAssets();

         Debug.Log("【已处理】\t" + shader.name + "\t=>\t" + newShaderName + "\t" + mat.name, mat);
     }
	 
}
