﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;


public class SkinWeaponHelper : EditorWindow
{
    private static string weaponTag;
    private static bool isCrystal;

    const string WeaponFbxPath = "Assets/Resources/Weapons/Fbx/";
    const string WeaponPrefabPath = "Assets/Resources/Weapons/";
    const string WeaponMatPath = "Assets/Resources/Weapons/Fbx/Materials/";
    const string WeaponTexPath = "Assets/Resources/Weapons/Textures/";

    static private string textField;

    [MenuItem("Assets/生成皮肤武器")]
    static void Init()
    {
        SkinWeaponHelper window = EditorWindow.GetWindow<SkinWeaponHelper>();
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("皮肤名称（后缀）：");
        weaponTag = EditorGUILayout.TextField(weaponTag);
        isCrystal = EditorGUILayout.Toggle("isCrystal（是否水晶）：", isCrystal);
        if (GUILayout.Button("查看已存在武器"))
        {
            ShowWeaponPrefabSelected();
        }
        if (GUILayout.Button("生成武器"))
        {
            if (string.IsNullOrEmpty(weaponTag))
            {
                EditorUtility.DisplayDialog("错误", "请输入Tag", "OK");
                return;
            }
            CreateWeaponPrefabSelected();
        }

        EditorGUILayout.Space();

        EditorGUILayout.TextArea(textField);
    }

    static public void ShowWeaponPrefabSelected()
    {
        textField = "";
        bool showGap = false;
        foreach (GameObject go in Selection.gameObjects)
        {
            if (!showGap)
                showGap = true;
            else
                textField += "-----------------------------------------\n";
            List<string> result = GetWeaponNameList(go);
            textField += string.Join("\n", result.ToArray()) + "\n";
        }
    }

    static private List<string> GetWeaponNameList(GameObject go)
    {
        List<string> result = new List<string>();

        //DirectoryInfo kDI = new DirectoryInfo(AssetDatabase.GetAssetPath(go));
        //FileInfo[] arrFI = kDI.Parent.Parent.GetFiles("*.prefab", SearchOption.TopDirectoryOnly);
        DirectoryInfo kDI = new DirectoryInfo(WeaponPrefabPath);
        FileInfo[] arrFI = kDI.GetFiles("*.prefab", SearchOption.TopDirectoryOnly);
        string weaponName = go.name;
        foreach (FileInfo fi in arrFI)
        {
            if (fi.Name.StartsWith(weaponName))
            {
                result.Add(Path.GetFileNameWithoutExtension(fi.Name));
            }
        }
        return result;
    }

    static public void CreateWeaponPrefabSelected()
    {
        int i = 0;
        foreach(GameObject go in Selection.gameObjects)
        {
            string fbxPath = AssetDatabase.GetAssetPath(go);

            if (fbxPath.Contains(WeaponFbxPath) == false || fbxPath.ToLower().EndsWith(".fbx") == false)
                continue;

            string fbxName = Path.GetFileNameWithoutExtension(fbxPath);
            bool succes = false;
            if(fbxName.EndsWith("_low"))
                succes = CreatePrefab(fbxPath, isCrystal ? GameConst.SHADER_TRANSPARENT : GameConst.SHADER_OP_WEAPON);          
            else
                succes = CreatePrefab(fbxPath, isCrystal ? GameConst.SHADER_MP_WEAPON_CRYSTAL : GameConst.SHADER_MP_WEAPON);

            if (succes)
                i++;
        }
        AssetDatabase.Refresh();
        EditorUtility.DisplayDialog("生成武器预设", "共生成 " + i + " 个", "OK");
    }

    static private bool CreatePrefab(string fbxPath, string shaderName)
    {
        string fbxName = Path.GetFileNameWithoutExtension(fbxPath);

        GameObject goFbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;



        string msg;
        if (WeaponHelper.CheckWeaponFbx(goFbx, out msg) == false)
        {
            EditorUtility.DisplayDialog("错误", msg, "OK");
            return false;
        }

        bool isLow = fbxName.EndsWith("_low");
        if (isLow)
        {
            fbxName = fbxName.Substring(0, fbxName.Length - 4) + "_" + weaponTag + "_low";
        }
        else
        {
            fbxName += "_" + weaponTag;
        }

        string prefabName = fbxName + ".prefab";

        GameObject prefab = PrefabUtility.CreatePrefab(WeaponPrefabPath + prefabName, goFbx);
        WeaponHelper.CopyCompoment(prefab);

        // set mat
        string matName = "mat_" + fbxName;
        string matPath = WeaponMatPath + matName + ".mat";
        Material mat = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
        if (mat == null)   
        {
            Shader shader = Shader.Find(shaderName);
            string texName = fbxName;
            if (fbxName.EndsWith("_low"))
                texName = fbxName.Replace("_low", "");

            string texPath = WeaponTexPath + "tex_" + texName + ".tga";
            Texture2D texture = AssetDatabase.LoadMainAssetAtPath(texPath) as Texture2D;
            if (texture == null)
            {
                Debug.LogError("找不到武器贴图：" + texPath);
            }
            else
            {
                mat = new Material(shader);
                mat.mainTexture = texture;
                AssetDatabase.CreateAsset(mat, matPath);
            }
        }

        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(prefab.transform, ref listRender);
        listRender[0].material = mat;

        if (isLow)
        {
            Animation ani = prefab.GetComponent<Animation>();
            if (ani != null)
                GameObject.DestroyImmediate(ani, true);
        }

        AssetDatabase.SaveAssets();
        return true;
    }

}
