﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class PrefabHelper 
{

    /// <summary>
    /// 设置特效预设Layer
    /// </summary>
	 static public void CheckEffectPrefab()
     {
         string effectPath = "Assets/Resources/Effect";

         List<string> listExtention = new List<string>() { ".prefab" };

         EditorHelper.VisitFileTraverse(effectPath, listExtention, (filePath) =>
         {
             string assetPath = filePath.Substring(filePath.IndexOf("Assets"));

             GameObject go = AssetDatabase.LoadMainAssetAtPath(assetPath) as GameObject;

             int layer = go.name.StartsWith("UI") ? LayerMask.NameToLayer(GameSetting.LAYER_NAME_UI) : 0;
             go.layer = layer;

             GameObjectHelper.VisitChildByTraverse(go.transform, (child) =>
             {
                 child.layer = layer;
             });

             AssetDatabase.SaveAssets();
         });

         AssetDatabase.Refresh();
     }




}
