﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;


public class WeaponHelper 
{
    const string WeaponFbxPath = "Assets/Resources/Weapons/Fbx/";
    const string WeaponPrefabPath = "Assets/Resources/Weapons/";
    const string WeaponMatPath = "Assets/Resources/Weapons/Fbx/Materials/";
    const string WeaponTexPath = "Assets/Resources/Weapons/Textures/";

    const string DefaultGrenade = "m26";
    static readonly string[] GrenadeNameBegin = new string[] { "yuetu", "m26", "m24shouliudan", "nangua", "shengdanshoulei", "heroliudan", "dahuoji" };
    static readonly string[] KnifeNameBegin = new string[] { "niboer", "knife", "junfu", "shitou", "jian", "zhen", "jiguangjian", "malaijian", "quantao", "junci" , "cidao" };
    static readonly string[] SpecialWeapons = new string[] { "zhen", "quantaoL", "quantaoR", "dahuoji" };
    static readonly string[] SpecialWeaponLows = new string[] {"zhen_low"};
    static readonly string[] SpecialWeaponTexs = new string[] { "tex_arm_meiying.tga", "tex_quantao.tga", "tex_quantao.tga", "tex_bianpao.tga" };
    static readonly string[] SpecialWeaponLowTexs = new string[] { "tex_meiying.tga", "tex_quantao.tga", "tex_quantao.tga" };
    static readonly string[] SpecialWeaponMat = new string[] { "meiying", "quantao", "quantao", "bianpao" };
    static readonly string[] SpecialWeaponLowMat = new string[] { "meiying_low"};

    static public void CreateWeaponPrefabSelected(bool isCrystal, bool isFlow)
    {
        int i = 0;
        foreach(GameObject go in Selection.gameObjects)
        {
            string fbxPath = AssetDatabase.GetAssetPath(go);

            if (fbxPath.Contains(WeaponFbxPath) == false || fbxPath.ToLower().EndsWith(".fbx") == false)
                continue;

            string fbxName = Path.GetFileNameWithoutExtension(fbxPath);
            bool succes = false;
            if(fbxName.EndsWith("_low"))
                succes = CreatePrefab(fbxPath, isCrystal ? GameConst.SHADER_TRANSPARENT : GameConst.SHADER_OP_WEAPON);          
            else
                succes = CreatePrefab(fbxPath, isCrystal ? GameConst.SHADER_MP_WEAPON_CRYSTAL :  (isFlow? GameConst.SHADER_MP_WEAPON_FLOW : GameConst.SHADER_MP_WEAPON));

            if (succes)
                i ++;
        }
        AssetDatabase.Refresh();
        EditorUtility.DisplayDialog("生成武器预设", "共生成 " + i + " 个", "OK");
    }

    static private bool CreatePrefab(string fbxPath, string shaderName)
    {
        // create weapon prefab
        string fbxName = Path.GetFileNameWithoutExtension(fbxPath);
        string prefabName = Path.GetFileNameWithoutExtension(fbxPath) + ".prefab";
        GameObject goFbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;

        string msg;
        if (CheckWeaponFbx(goFbx, out msg) == false)
        {
            EditorUtility.DisplayDialog("错误", msg, "OK");
            return false;
        }
        GameObject prefab = PrefabUtility.CreatePrefab(WeaponPrefabPath + prefabName, goFbx);
        CopyCompoment(prefab);

        // set mat
        string matName = "";

        int index = -1;
        index = GetSpecialIndex(fbxName);
        if(index != -1)
        {
            if (fbxName.EndsWith("_low"))
                matName = "mat_" + SpecialWeaponLowMat[index] + ".mat";
            else
                matName = "mat_" + SpecialWeaponMat[index] + ".mat";
        }
        else
        {
            matName = "mat_" + fbxName;
        }
        string matPath = WeaponMatPath + matName + ".mat";

        Material mat = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;

        if (mat == null)   
        {
            Shader shader = Shader.Find(shaderName);
            string texName = fbxName;

            
            string texPath;

            if (fbxName.EndsWith("_low"))
            {
                if (index != -1)
                    texPath = WeaponTexPath + SpecialWeaponLowTexs[index];
                else
                {
                    texName = fbxName.Replace("_low", "");
                    texPath = WeaponTexPath + "tex_" + texName + ".tga";
                }
            }
            else if (index != -1)
                texPath = WeaponTexPath + SpecialWeaponTexs[index];
            else
                texPath = WeaponTexPath + "tex_" + texName + ".tga";

            Texture2D texture = AssetDatabase.LoadMainAssetAtPath(texPath) as Texture2D;
            if (texture == null)
            {
                Debug.LogError("找不到武器贴图：" + texPath);
            }
            else
            {
                mat = new Material(shader);
                mat.mainTexture = texture;
                AssetDatabase.CreateAsset(mat, matPath);
            }
        }

        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(prefab.transform, ref listRender);
        listRender[0].material = mat;

        if(fbxName.EndsWith("_low"))
        {
            Animation ani = prefab.GetComponent<Animation>();
            if (ani != null)
                GameObject.DestroyImmediate(ani, true);
        }

        AssetDatabase.SaveAssets();
        return true;
    }

    static public void CopyCompoment(GameObject to)
    {
        if (IsGrenade(to.name))
        {
            GameObject from = AssetDatabase.LoadAssetAtPath(WeaponPrefabPath + to.name + ".prefab", typeof(GameObject)) as GameObject;
            if (from == null || from.GetComponent<CapsuleCollider>() == null)
            {
                from = AssetDatabase.LoadAssetAtPath(WeaponPrefabPath + DefaultGrenade + ".prefab", typeof(GameObject)) as GameObject;
                if (from == null || from.GetComponent<CapsuleCollider>() == null)
                {
                    EditorUtility.DisplayDialog("提示", "复制碰撞体出错：要复制的预设【" + to.name + "】和【" + DefaultGrenade + "】不存在或不存在CapsuleCollider，请自己生成", "OK");
                    return;
                }
            }
            CopyComponent<CapsuleCollider>(to, from);
        }
    }

    private static void CopyComponent<T>(GameObject to, GameObject from) where T : Component
    {
        T fromT = from.GetComponent<T>();
        if (fromT != null)
        {
            if (UnityEditorInternal.ComponentUtility.CopyComponent(fromT))
            {
                T t = to.AddMissingComponent<T>();
                UnityEditorInternal.ComponentUtility.PasteComponentValues(t);
            }
        }
    }

    static public bool CheckWeaponFbx(GameObject go, out string msg)
    {
        msg = "";
        string goName = go.name;

        //跳过刀类和手雷类武器检测
        if (IsKnife(goName) || IsGrenade(goName))
            return true;

        if (GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_FIREPOINT, false) == null)
            msg += goName + " 找不到火花特效挂接点 " + ModelConst.WEAPON_SLOT_FIREPOINT;

        if (GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_CARTRIDGE, false) == null)
            msg += "\n" + goName + " 找不到武器弹壳挂接点 " + ModelConst.WEAPON_SLOT_CARTRIDGE;

        return string.IsNullOrEmpty(msg);
    }

    static public bool IsKnife(string objName)
    {
        for (int i = 0; i < KnifeNameBegin.Length; ++i)
        {
            if (objName.StartsWith(KnifeNameBegin[i]))
                return true;
        }
        return false;
    }

    static public bool IsGrenade(string goName)
    {
        for (int i = 0; i < GrenadeNameBegin.Length; ++i)
        {
            if (goName.StartsWith(GrenadeNameBegin[i]))
                return true;
        }
        return false;
    }

    static public int GetSpecialIndex(string name)
    {
        for (int i = 0; i < SpecialWeapons.Length; ++i)
        {
            if (name.StartsWith(SpecialWeapons[i]))
                return i;
        }
        return -1;
    }

    static public int GetSpecialLowIndex(string name)
    {
        for (int i = 0; i < SpecialWeaponLows.Length; ++i)
        {
            if (name.StartsWith(SpecialWeaponLows[i]))
                return i;
        }
        return -1;
    }

}
