﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Threading;


public class DmmHelper
{
    const string DmmFbxPath = "Assets/Resources/Dmm/Roles/Fbx/";
    const string DmmPrefabPath = "Assets/Resources/Roles/Dmm/";
    const string DmmMatPath = "Assets/Resources/Dmm/Materials/";
    const string DmmTexPath = "Assets/Resources/Dmm/Textures/";
    const string DMM_SHADER_NAME = "FPS/Texture_Color";
    const string DmmMatNameHead = "mat_dmm_thing";

    [MenuItem("Assets/生成选中躲猫猫物件预设")]
    static public void CreatePrefabSelected()
    {
        int createCount = 0;
        foreach (GameObject go in Selection.gameObjects)
        {
            string fbxPath = AssetDatabase.GetAssetPath(go);

            if (fbxPath.Contains(DmmFbxPath) == false || fbxPath.ToLower().EndsWith(".fbx") == false)
                continue;
            
            string fbxName = Path.GetFileNameWithoutExtension(fbxPath);
            CreatePrefab(fbxName, fbxPath);
            //RenamePrefab(fbxName);
            ++createCount;
        }
        AssetDatabase.Refresh();
        EditorUtility.DisplayDialog("生成躲猫猫物件预设", "共生成 " + createCount + " 个", "OK");
    }

    //根据材质球改预设名字
    static private void RenamePrefab(string prefabName)
    {
        GameObject goPrefab = AssetDatabase.LoadAssetAtPath(DmmPrefabPath + prefabName + ".prefab", typeof(GameObject)) as GameObject;
        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(goPrefab.transform, ref listRender);
        string matName = listRender[0].sharedMaterial.name;

        string matNo = matName.Substring(matName.Length - 2);
        if (!prefabName.EndsWith("_" + matNo))
        {
            string msg = AssetDatabase.RenameAsset(DmmPrefabPath + prefabName + ".prefab", prefabName + "_" + matNo);
        }
    }

    //创建预设
    static private void CreatePrefab(string fbxName, string fbxPath)
    {
        string prefabName = fbxName + ".prefab";
        GameObject goFbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;
        GameObject goPrefab = PrefabUtility.CreatePrefab(DmmPrefabPath + prefabName, goFbx);

        int index = prefabName.LastIndexOf('_');
        if (index == -1)
            return;
        string matNo = prefabName.Substring(index + 1);

        if (matNo.Length != 2)
            return;

        string matName = DmmMatNameHead + matNo;

        //设置材质球
        SetRoleMat(goPrefab, fbxName, matName);
        goPrefab.transform.localPosition = Vector3.zero;
        goPrefab.transform.localEulerAngles = Vector3.zero;
        goPrefab.transform.localScale = Vector3.one;

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void SetRoleMat(GameObject go, string fbxName, string matName)
    {
        string matPath = DmmMatPath + matName + ".mat";
        Material mat = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
        if (mat == null)
        {
            Shader shader = Shader.Find(DMM_SHADER_NAME);
            string texName = fbxName;

            string texPath = DmmTexPath + "tex_" + texName + ".tga";
            Texture2D texture = AssetDatabase.LoadMainAssetAtPath(texPath) as Texture2D;
            if (texture == null)
            {
                Debug.LogError("找不到贴图：" + texPath);
            }
            else
            {
                mat = new Material(shader);
                mat.mainTexture = texture;
                AssetDatabase.CreateAsset(mat, matPath);
            }
        }

        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(go.transform, ref listRender);
        listRender[0].material = mat;
        AssetDatabase.SaveAssets();
    }
}
