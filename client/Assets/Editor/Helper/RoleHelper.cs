﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Threading;


public class RoleHelper
{
    const string RoleFbxPath = "Assets/Resources/Roles/Fbx/";
    const string RolePrefabPath = "Assets/Resources/Roles/";
    const string RoleMatPath = "Assets/Resources/Roles/Fbx/Materials/";
    const string RoleTexPath = "Assets/Resources/Roles/Textures/";
    const string RoleAnimatorControllerPath = "Assets/Resources/Roles/AnimatorController/";

    const string Defalut_Arm_Prefab = "arm";
    const string Defalut_Boy_Prefab = "yazhounan1";
    const string Defalut_Girl_Prefab = "yazhounv1";

    const string GirlAnimatorControllerPath = "girl_AniCtrl.controller";
    const string BoyAnimatorControllerPath = "player_AniCtrl.controller";
    const string ShylAnimatorControllerPath = "shenghuayouling_AniCtrl.controller";
    const string JushiAnimatorControllerPath = "jushi_AniCtrl.controller";
    const string YingxiongAnimatorControllerPath = "yingxiong_AniCtrl.controller";
    const string MeiyingAnimatorControllerPath = "meiying_AniCtrl.controller";
    const string JiangshinanAnimatorControllerPath = "Jiangshinan_AniCtrl.controller";
    const string FeichongControllerPath = "Feichong_AniCtrl.controller";
    const string ArmAnimatorControllerPath = "arm_AniCtrl.controller";
    const string ArmShylAnimatorControllerPath = "arm_shenghuayouling_AniCtrl.controller";
    const string ArmJushiAnimatorControllerPath = "arm_jushi_AniCtrl.controller";
    const string ArmYingxiongAnimatorControllerPath = "arm_yingxiong_AniCtrl.controller";
    const string ArmMeiyingAnimatorControllerPath = "arm_meiying_AniCtrl.controller";
    const string ArmJiangshinanAnimatorControllerPath = "arm_jiangshinan_AniCtrl.controller";

    const string ROLE_SHADER_NAME = "FPS/Texture_Color";
    const string ARM_SHADER_NAME = "FPS/LightProbe_Diff";

    const string MainPlayerExtraExposedTransformPathsFile = "Assets/Editor/MPExtraExposedTransformPathsFile.txt";
    const string OtherPlayerExtraExposedTransformPathsFile = "Assets/Editor/OPExtraExposedTransformPathsFile.txt";

    /// <summary>
    /// 角色类型
    /// </summary>
    public enum RoleType
    {
        Arm,
        ArmShyl,
        ArmJushi,
        ArmYingxiong,
        ArmMeiying,
        ArmJiangshinan,
        Boy,
        Girl,
        Shyl,
        Jushi,
        YingXiong,
        Scyl,
        Scjs,
        Meiying,
        Jiangshinan,
        Feichong,
        AI,
    }

    [MenuItem("Assets/生成选中人物预设")]
    static public void CreateRolePrefabSelected()
    {
        int createCount = 0;
        foreach (GameObject go in Selection.gameObjects)
        {
            string fbxPath = AssetDatabase.GetAssetPath(go);

            if (fbxPath.Contains(RoleFbxPath) == false || fbxPath.ToLower().EndsWith(".fbx") == false)
                continue;

            string fbxName = Path.GetFileNameWithoutExtension(fbxPath);
            string prefabName = fbxName;
            CreatePrefab(fbxName, prefabName, fbxPath);
            if (IsNeedHallPrefab(fbxName))
            {
                prefabName = fbxName + "Hall";
                CreatePrefab(fbxName, prefabName, fbxPath);
            }
            ++createCount;
        }
        AssetDatabase.Refresh();
        EditorUtility.DisplayDialog("生成人物预设", "共生成 " + createCount + " 个", "OK");
    }

    static private bool IsNeedHallPrefab(string name)
    {
        return name.StartsWith("yazhou") || name.StartsWith("oumei");
    }

    static private void CreatePrefab(string fbxName, string prefabName, string fbxPath)
    {
        bool isArm = fbxName.StartsWith("arm");
        bool isHall = prefabName.Contains("Hall");


        string prefabPath = prefabName + ".prefab";
        string tmpPrefabPath = fbxName + (isHall? "___halltmp" : "___tmp") + ".prefab";

        RoleType roleType = GetRoleType(fbxName);

        //设置优化节点
        SetExposeGameObj(fbxPath, roleType, isArm);

        GameObject goFbx = AssetDatabase.LoadMainAssetAtPath(fbxPath) as GameObject;
        GameObject tmpPrefab = PrefabUtility.CreatePrefab(RolePrefabPath + tmpPrefabPath, goFbx);

        string roleMatTexName = GetMatTexName(roleType);
        if (string.IsNullOrEmpty(roleMatTexName))
            roleMatTexName = fbxName;
        //设置材质球
        SetRoleMat(tmpPrefab, roleMatTexName, isArm);

        if (isArm)
            SetRendererBound(tmpPrefab);

        //设置碰撞盒
        bool ret = SetCollider(tmpPrefab, fbxName, fbxPath, prefabPath, isArm, isHall, roleType);

        //替换
        if (ret)
        {
            AssetDatabase.DeleteAsset(RolePrefabPath + prefabPath);
            string msg = AssetDatabase.RenameAsset(RolePrefabPath + tmpPrefabPath, prefabName);
            if (!string.IsNullOrEmpty(msg))
            {
                Debug.LogError(msg);
            }
        }
        AssetDatabase.SaveAssets();
    }

    private static void SetRoleMat(GameObject go, string fbxName, bool isArm)
    {
        string matName = "mat_" + fbxName;
        string matPath = RoleMatPath + matName + ".mat";
        Material mat = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
        if (mat == null)
        {
            string shaderName = isArm ? ARM_SHADER_NAME : ROLE_SHADER_NAME;
            Shader shader = Shader.Find(shaderName);
            string texName = fbxName;

            string texPath = RoleTexPath + "tex_" + texName + ".tga";
            Texture2D texture = AssetDatabase.LoadMainAssetAtPath(texPath) as Texture2D;
            if (texture == null)
            {
                Debug.LogError("找不到人物贴图：" + texPath);
                return;
            }
            else
            {
                mat = new Material(shader);
                mat.mainTexture = texture;
                AssetDatabase.CreateAsset(mat, matPath);
            }
        }

        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(go.transform, ref listRender);
        Renderer render = listRender[0];
        render.material = mat;
        AssetDatabase.SaveAssets();
    }

    private static void SetRendererBound(GameObject go)
    {
        List<SkinnedMeshRenderer> listRender = new List<SkinnedMeshRenderer>();
        GameObjectHelper.GetComponentsByTraverse<SkinnedMeshRenderer>(go.transform, ref listRender);
        if (listRender != null)
        {
            SkinnedMeshRenderer smr = listRender[0];
            if (smr != null)
            {
                Bounds b = smr.localBounds;
                b.extents = Vector3.one;
                smr.localBounds = b;
            }
        }
    }

    private static bool SetCollider(GameObject go, string fbxName, string fbxPath, string copyPrefabPath, bool isArm, bool isHall, RoleType roleType)
    {
        GameObject copyPrefab = AssetDatabase.LoadAssetAtPath(RolePrefabPath + copyPrefabPath, typeof(GameObject)) as GameObject;

        int updateRoleColliderRet = -1;

        if (copyPrefab == null)
        {
            string newCopyPrefabName = Defalut_Boy_Prefab;
            if (isArm)
                newCopyPrefabName = Defalut_Arm_Prefab;
            else if (roleType == RoleType.Girl)
                newCopyPrefabName = Defalut_Girl_Prefab;

            copyPrefab = AssetDatabase.LoadMainAssetAtPath(RolePrefabPath + newCopyPrefabName + ".prefab") as GameObject;
            if (copyPrefab == null)
            {
                Debug.LogError("预设【" + copyPrefabPath + "】不存在");
                return false;
            }
        }

        Animator animator = go.AddMissingComponent<Animator>();
        animator.avatar = AssetDatabase.LoadAssetAtPath(fbxPath, typeof(Avatar)) as Avatar;
        string aniControllerPath = GetAniControllerPath(roleType);
        if (!string.IsNullOrEmpty(aniControllerPath))
            animator.runtimeAnimatorController = AssetDatabase.LoadAssetAtPath(RoleAnimatorControllerPath + GetAniControllerPath(roleType), typeof(RuntimeAnimatorController)) as RuntimeAnimatorController;
        //CopyComponent<CharacterController>(copyPrefab, go);
        if (isArm || isHall)
            updateRoleColliderRet = 1;
        else if (RoleColliderCopier.SaveRoleColliderData(copyPrefab))
        {
            updateRoleColliderRet = RoleColliderCopier.UpdateGOCollider(new GameObject[] { go });
        }
        
        if(updateRoleColliderRet < 0)
        {
            Debug.LogError("更新【" + fbxName + "】碰撞信息错误!!");
            return false;
        }
        return true;
    }

    private static void SetExposeGameObj(string fbxPath, RoleType roleType, bool isArm)
    {
        ModelImporter kMI = ModelImporter.GetAtPath(fbxPath) as ModelImporter;
        kMI.optimizeGameObjects = isArm ? true : false;
        if (kMI.optimizeGameObjects)
            kMI.extraExposedTransformPaths = GetExtraExposedTransformPaths(roleType);
        kMI.importMaterials = false;
        AssetDatabase.ImportAsset(fbxPath);
        AssetDatabase.SaveAssets();
    }

    private static void CopyComponent<T>(GameObject to, GameObject from) where T : Component
    {
        T fromT = from.GetComponent<T>();
        if (fromT != null)
        {
            if (UnityEditorInternal.ComponentUtility.CopyComponent(fromT))
            {
                T t = to.AddMissingComponent<T>();
                UnityEditorInternal.ComponentUtility.PasteComponentValues(t);
            }
        }
    }

    private static RoleType GetRoleType(string name)
    {
        if (name.StartsWith("arm_jushi"))
            return RoleType.ArmJushi;
        if (name.StartsWith("arm_shenghuayouling"))
            return RoleType.ArmShyl;
        if (name.StartsWith("arm_yingxiong"))
            return RoleType.ArmYingxiong;
        if (name.StartsWith("arm_meiying"))
            return RoleType.ArmMeiying;
        if (name.StartsWith("arm_jiangshinan"))
            return RoleType.ArmJiangshinan;
        if (name.StartsWith("arm"))
            return RoleType.Arm;
        if (name.StartsWith("jushi"))
            return RoleType.Jushi;
        if (name.StartsWith("shengcunjiangshi"))
            return RoleType.Scyl;
        if (name.StartsWith("shengcunjushi"))
            return RoleType.Scjs;
        if (name.StartsWith("shengcunjiangshi"))
            return RoleType.Scyl;
        if (name.StartsWith("yingxiong"))
            return RoleType.YingXiong;
        if (name.StartsWith("meiying"))
            return RoleType.Meiying;
        if (name.StartsWith("jiangshinan"))
            return RoleType.Jiangshinan;
        if (name.StartsWith("feichong"))
            return RoleType.Feichong;
        if (name.Contains("nan"))
            return RoleType.Boy;
        if (name.Contains("nv"))
            return RoleType.Girl;

        return RoleType.AI;
    }

    private static string GetAniControllerPath(RoleType roleType)
    {
        switch(roleType)
        {
            case RoleType.Arm:
                return ArmAnimatorControllerPath;
            case RoleType.ArmJushi:
                return ArmJushiAnimatorControllerPath;
            case RoleType.ArmShyl:
                return ArmShylAnimatorControllerPath;
            case RoleType.ArmYingxiong:
                return ArmYingxiongAnimatorControllerPath;
            case RoleType.ArmMeiying:
                return ArmMeiyingAnimatorControllerPath;
            case RoleType.ArmJiangshinan:
                return ArmJiangshinanAnimatorControllerPath;
            case RoleType.Jushi:
                return JushiAnimatorControllerPath;
            case RoleType.Shyl:
                return ShylAnimatorControllerPath;
            case RoleType.Boy:
                return BoyAnimatorControllerPath;
            case RoleType.Girl:
                return GirlAnimatorControllerPath;
            case RoleType.YingXiong:
                return YingxiongAnimatorControllerPath;
            case RoleType.Scyl:
                return ShylAnimatorControllerPath;
            case RoleType.Scjs:
                return JushiAnimatorControllerPath;
            case RoleType.Meiying:
                return MeiyingAnimatorControllerPath;
            case RoleType.Jiangshinan:
                return JiangshinanAnimatorControllerPath;
            case RoleType.Feichong:
                return FeichongControllerPath;
            case RoleType.AI:
                return null;
        }
        return BoyAnimatorControllerPath;
    }

    private static string[] GetExtraExposedTransformPaths(RoleType roleType)
    {
        switch (roleType)
        {
            case RoleType.Arm:
            case RoleType.ArmJushi:
            case RoleType.ArmShyl:
            case RoleType.ArmYingxiong:
            case RoleType.ArmMeiying:
            case RoleType.ArmJiangshinan:
                return GetMpExtraExposedTransformPaths();
            case RoleType.Jushi:
            case RoleType.Shyl:
            case RoleType.Scyl:
            case RoleType.Boy:
            case RoleType.Girl:
            case RoleType.YingXiong:
            case RoleType.Meiying:
            case RoleType.Jiangshinan:
            case RoleType.Feichong:
                return GetOpExtraExposedTransformPaths();
        }
        return null;
    }

    private static string GetMatTexName(RoleType roleType)
    {
        switch (roleType)
        {
  
            case RoleType.Scyl:
                return "shenghuayouling";
            case RoleType.Scjs:
                return "jushi";
        }
        return null;
    }

    private static string[] GetMpExtraExposedTransformPaths()
    {
        if (File.Exists(MainPlayerExtraExposedTransformPathsFile))
        {
            string[] line = File.ReadAllLines(MainPlayerExtraExposedTransformPathsFile);
            return line;
        }
        return null;
    }

    private static string[] GetOpExtraExposedTransformPaths()
    {
        if (File.Exists(OtherPlayerExtraExposedTransformPathsFile))
        {
            string[] line = File.ReadAllLines(OtherPlayerExtraExposedTransformPathsFile);
            return line;
        }
        return null;
    }
}
