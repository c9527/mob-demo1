﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// 借用了CanvasRenderer类，因为他在Inspector里没有可见内容。。。不会被覆盖掉
/// 对UI增加键盘微调操作，方向键微调移动，加control键微调宽度
/// </summary>
[CanEditMultipleObjects]
[CustomEditor(typeof(CanvasRenderer), true)]
public class CanvasRendererInspector : Editor
{
    private void OnSceneGUI()
    {
        Event e = Event.current;
        int id = GUIUtility.GetControlID("RectTransform".GetHashCode(), FocusType.Passive);
        EventType type = e.GetTypeForControl(id);

        if (type == EventType.KeyDown)
        {
            if (e.keyCode == KeyCode.UpArrow)
                OperateRect(e, 0, 1);
            else if (e.keyCode == KeyCode.DownArrow)
                OperateRect(e, 0, -1);
            else if (e.keyCode == KeyCode.LeftArrow)
                OperateRect(e, -1, 0);
            else if (e.keyCode == KeyCode.RightArrow)
                OperateRect(e, 1, 0);
        }
    }

    private void OperateRect(Event e, float dtX, float dtY)
    {
        var selects = Selection.gameObjects;
        if (selects == null || selects.Length == 0)
            return;
        
        for (int i = 0; i < selects.Length; i++)
        {
            var rect = selects[i].GetComponent<RectTransform>();
            if (rect)
            {
                Undo.RecordObject(rect, "Modify RectTransform");
                if (e.control)
                    rect.sizeDelta = new Vector2(Mathf.Round(rect.sizeDelta.x + dtX), Mathf.Round(rect.sizeDelta.y + dtY));
                else
                    rect.anchoredPosition = new Vector2(Mathf.Round(rect.anchoredPosition.x + dtX), Mathf.Round(rect.anchoredPosition.y + dtY));
                EditorUtility.SetDirty(rect);
                e.Use();
            }
        }
    }
}
