﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class CheckResRef : EditorWindow
{
    float ThumbnailWidth = 18;
    float ThumbnailHeight = 18;
    private Vector2 scrolPos1;
    private Vector2 scrolPos2;
    private List<RefItem> mResultList = new List<RefItem>();
    private List<RefItem> mFilterList = new List<RefItem>();
    private Dictionary<string, bool> mTargetTypeDic = new Dictionary<string, bool>();
    private List<string> mTargetTypeKeySortedList = new List<string>();

    private List<RefItem> mResultListBackup; 
    private bool mCheckRefMode;
    private string mFilter = "";

    [MenuItem("Assets/浏览资源引用关系")]
    public static void Open()
    {
        if (Selection.activeObject == null)
        {
            Debug.LogWarning("请确保至少选择一个目录或文件");
            return;
        }
        Rect wr = new Rect(0, 0, 500, 800);
        var window = (CheckResRef)EditorWindow.GetWindowWithRect(typeof(CheckResRef), wr, true);
        window.title = "当前路径：" + AssetDatabase.GetAssetPath(Selection.activeObject);
        window.Show();
        window.CheckRef();
        
    }

    void OnGUI()
    {
        GUILayout.Label("选择需要查看的类型");
        scrolPos1 = GUILayout.BeginScrollView(scrolPos1, GUILayout.Height(300));

        for (int i = 0; i < mTargetTypeKeySortedList.Count; i++)
        {
            var key = mTargetTypeKeySortedList[i];
            mTargetTypeDic[key] = GUILayout.Toggle(mTargetTypeDic[key], key);
        }

        GUILayout.EndScrollView();

        GUILayout.Box("", GUILayout.Width(500-8), GUILayout.Height(5));
        if (mCheckRefMode)
        {
            GUI.color = Color.green;
            if (GUILayout.Button("<== Back"))
            {
                mCheckRefMode = false;
                mResultList = mResultListBackup;

                mFilterList = mResultList.FindAll(item =>
                {
                    if (item.Obj.name.IndexOf(mFilter, StringComparison.CurrentCultureIgnoreCase) != -1)
                        return true;
                    return false;
                });
            }
            GUI.color = Color.white;
        }
        else
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("过滤：", GUILayout.Width(70));
            var tmp = EditorGUILayout.TextField(mFilter);
            if (string.IsNullOrEmpty(mFilter))
                mFilterList = mResultList;
            if (tmp != mFilter)
            {
                mFilter = tmp;
                if (!string.IsNullOrEmpty(tmp))
                    mFilterList = mResultList.FindAll(item =>
                    {
                        if (item.Obj.name.IndexOf(mFilter, StringComparison.CurrentCultureIgnoreCase) != -1)
                            return true;
                        return false;
                    });
            }
            GUILayout.EndHorizontal();
        }

        
        scrolPos2 = GUILayout.BeginScrollView(scrolPos2);
        foreach (var item in mFilterList)
        {
            if (item.RefCount > 0 || item.IsEnd)
            {
                var type = item.Obj.GetType().Name;
                if (mTargetTypeDic.ContainsKey(type) && mTargetTypeDic[type] || item.IsEnd)
                {
                    GUILayout.BeginHorizontal();

                    if (GUILayout.Button(item.Obj.name, GUILayout.Width(170)))
                        Selection.activeObject = item.Obj;
                    if (!item.IsEnd && GUILayout.Button(item.RefCount + " 个引用", GUILayout.Width(70)))
                        ShowRef(item.RefList);

                    if (item.Obj is Texture)
                        GUILayout.Box(item.Obj as Texture, GUILayout.Width(ThumbnailWidth), GUILayout.Height(ThumbnailHeight));
                    else if (item.Obj is Material)
                        GUILayout.Box((item.Obj as Material).mainTexture, GUILayout.Width(ThumbnailWidth), GUILayout.Height(ThumbnailHeight));
                    GUILayout.Label(type);
                    
                    GUILayout.EndHorizontal();
                }
            }
        }
        GUILayout.EndScrollView();
    }

    public void CheckRef()
    {
        Object[] selected = Selection.GetFiltered(typeof(Object), SelectionMode.TopLevel | SelectionMode.DeepAssets | SelectionMode.Editable);
        if (selected.Length > 0)
            CheckRef(new List<Object>(selected));
    }

//    public void CheckRef(string path)
//    {
//        var rootPath = Application.dataPath;
//        var objectList = new List<Object>();
//        var effPrefabPath = new DirectoryInfo(Application.dataPath + path);
//        var fileList = effPrefabPath.GetFiles("*.*", SearchOption.AllDirectories);
//        for (int i = 0; i < fileList.Length; i++)
//        {
//            var file = fileList[i];
//            if (file.Extension == ".meta")
//                continue;
//
//            var prefabPath = file.FullName.Substring(rootPath.Length - 6, file.FullName.Length - rootPath.Length + 6);
//            var obj = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(Object));
//            objectList.Add(obj);
//        }
//
//        CheckRef(objectList);
//    }

    public void CheckRef(List<Object> roots)
    {
        //引用计数字典
        var refCountDic = new Dictionary<Object, RefItem>();
        mTargetTypeDic.Clear();

        for (int i = 0; i < roots.Count; i++)
        {
            var obj = roots[i];
            var depandArr = EditorUtility.CollectDependencies(new Object[] { obj });

            for (int j = 0; j < depandArr.Length; j++)
            {
                var depand = depandArr[j];
                if (depand == null)
                    continue;

                if (!mTargetTypeDic.ContainsKey(depand.GetType().Name))
                    mTargetTypeDic.Add(depand.GetType().Name, false);

                if (!refCountDic.ContainsKey(depand))
                    refCountDic.Add(depand, new RefItem());
                refCountDic[depand].Obj = depand;
                refCountDic[depand].RefCount++;
                refCountDic[depand].RefList.Add(obj);
            }
        }

        mResultList = refCountDic.Values.ToList();
        mResultList.Sort((a, b) =>
        {
            if (b.RefCount != a.RefCount)
                return b.RefCount - a.RefCount;
            return a.Obj.name.CompareTo(b.Obj.name);
        });
        mResultListBackup = mResultList;

        mTargetTypeKeySortedList = mTargetTypeDic.Keys.ToList();
        mTargetTypeKeySortedList.Sort();
    }

    public void ShowRef(List<Object> roots)
    {
        mCheckRefMode = true;
        var list = new List<RefItem>();
        for (int i = 0; i < roots.Count; i++)
        {
            list.Add(new RefItem() { Obj = roots[i], IsEnd = true });
        }
        mResultList = list;
        mFilterList = list;
    }
}

class RefItem
{
    public bool IsEnd;
    public Object Obj;
    public int RefCount;
    public List<Object> RefList = new List<Object>();
}
