﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ColorFade : MonoBehaviour
{

    public Color dstColor = new Color(1f, 0.1f, 0.1f);
    public float fadeTime = 0.5f;

    private List<Material> m_listMat = new List<Material>();
    //private Dictionary<int, Color> m_dicOldColor = new Dictionary<int, Color>();
    private Color m_colorSpeed;
    private Color m_colorTick;
    private bool m_bRun;
    private bool m_bReverse;
    private bool m_bInit;

    public void Init()
    {
        m_listMat.Clear();
        //m_dicOldColor.Clear();

        List<Renderer> listRender = new List<Renderer>();
        GameObjectHelper.GetComponentsByTraverse<Renderer>(transform, ref listRender);
        foreach (Renderer r in listRender)
        {
            Material mat = r.material;
            if (m_listMat.IndexOf(mat) == -1)
            {
                if (mat.HasProperty(GameConst.SHADER_PROPERTY_COLOR))
                {
                    //Color c = mat.GetColor(GameConst.SHADER_PROPERTY_COLOR);
                    m_listMat.Add(mat);
                    //m_dicOldColor.Add(mat.GetInstanceID(), c);
                }
            }
        }
        m_bInit = true;
    }

    public void Run(bool bValue, Color color = default(Color))
    {
        if (m_bRun == bValue && dstColor == color)
            return;

        dstColor = color;
        
        if(bValue)
        {
            if(!m_bInit)
                Init();
            m_colorSpeed = (Color.white - dstColor) / fadeTime;
            m_colorSpeed.a = 0;
            m_colorTick = Color.white;
            m_bRun = true;
        }
        else
        {
            m_bRun = false;
            dstColor = Color.white;
            foreach(Material mat in m_listMat)
            {
                mat.color = Color.white; //m_dicOldColor[mat.GetInstanceID()];
            }
        }
        
    }
	 
	void Update () {

        if (!m_bRun || !m_bInit)
            return;

        Color colorStep = m_colorSpeed * Time.deltaTime;

        if (m_bReverse)
            m_colorTick += colorStep;
        else
            m_colorTick -= colorStep;

        if (m_colorTick.r <= dstColor.r && m_colorTick.g <= dstColor.g && m_colorTick.b <= dstColor.b)
        {
            //foreach (Material mat in m_listMat)
            //{
            //    if (mat.HasProperty(ShaderConst.PROPERTY_NAME_COLOR))
            //        mat.SetColor(ShaderConst.PROPERTY_NAME_COLOR, m_dicOldColor[mat.GetInstanceID()]);
            //}
            m_bReverse = true;
            //m_colorTick = Color.white;
        }
        else if(m_colorTick.r >= 1 && m_colorTick.g >= 1 && m_colorTick.b >= 1)
        {
            m_bReverse = false;
        }

        foreach (Material mat in m_listMat)
        {
            mat.color = m_colorTick;
        }
	}

    public void Release()
    {
        if (m_bInit)
        {
            Run(false);
            m_listMat.Clear();
            //m_dicOldColor.Clear();
            m_bInit = false;
        }
    }

    private void OnEnable()
    {
        if (m_bRun)
            return;
        Release();
        m_bRun = false;
        m_bReverse = false;
        m_bInit = false;
    }
}
