﻿using System.Collections;
using System.ComponentModel;
using UniLua;
using UnityEngine;

public enum HookType
{
    Net_TcpRecv,
    Net_TcpSend,
    UI_Click_Before,
    UI_Click_After,
    UI_Open,
    UI_Goto_Before,
    UI_Goto_After,
    Guide_Start,
    Guide_DoNext,
    _Count,
}

static class LuaHook
{
    static ILuaState msLua = LuaAPI.NewState();
    static bool[] msHooks = new bool[(int)HookType._Count];
    static MonoBehaviour mMonoDriver;

    public static ILuaState Lua { get { return msLua; } }

    public static void Init(MonoBehaviour driver)
    {
        mMonoDriver = driver;

        msLua.L_OpenLibs();
        msLua.L_RequireF("hook.cs", OpenLib, true);
        msLua.L_RequireF("base.cs", LuaBase.OpenLib, true);

        var ret = msLua.L_DoFile("main.lua");
        if (ret != ThreadStatus.LUA_OK)
            LogError("init error[{0}] : {1}", ret, msLua.ToString(-1));
        LoadFixCode();
    }

    public static void LoadFixCode()
    {
        msLua.SetTop(0);
        var lr = msLua.L_DoString("load_fix_code()");
        if (lr != ThreadStatus.LUA_OK)
            LogError("LoadFixCode error[{0}] : {1}", lr, msLua.ToString(-1));
    }

    static int OpenLib(ILuaState lua)
    {
        var define = new NameFuncPair[]
			{
				new NameFuncPair( "set_hook", lua_set_hook ),
				new NameFuncPair( "clear_hook", lua_clear_hook ),
				new NameFuncPair( "start_coroutine", lua_start_coroutine ),
			};

        lua.L_NewLib(define);
        return 1;
    }

    static int lua_set_hook(ILuaState lua)
    {
        string tn = lua.ToString(1);
        bool open = true;
        if (lua.GetTop() >= 2)
            open = lua.ToBoolean(2);
        object to = null;
        try
        { to = System.Enum.Parse(typeof(HookType), tn); }
        catch (System.OverflowException) { }
        if (to == null)
        {
            LogError("error HookType:" + tn);
            return 0;
        }
        msHooks[(int)(HookType)to] = open;
        lua.PushBoolean(true);
        return 1;
    }

    static int lua_clear_hook(ILuaState lua)
    {
        msHooks = new bool[(int)HookType._Count];
        return 0;
    }

    public static bool CheckHook(HookType type, int ret = 0, params object[] args)
    {
        if (!msHooks[(int)type])
            return false;

        msLua.SetTop(0);
        msLua.GetGlobal("call_hook");

        msLua.PushString(type.ToString());

        for (int i = 0; i < args.Length; i++)
            LuaBase.PushLuaValue(msLua, args[i]);

        try
        {
            msLua.Call(args.Length + 1, ret + 1);
        }
        catch (LuaRuntimeException e)
        {
            LogError("call_hook({0}) error[{1}] : {2}", type, e.ErrCode, msLua.ToString(-1));
            return false;
        }

        return msLua.ToBoolean(-ret - 1);
    }

    static int lua_start_coroutine(ILuaState lua)
    {
        int id = lua.L_CheckInteger(1);
        mMonoDriver.StartCoroutine(LuaEnumerator(id));
        return 0;
    }

    static IEnumerator LuaEnumerator(int id)
    {
        int top;
        while (true)
        {
            top = msLua.GetTop();
            msLua.GetGlobal("call_coroutine");
            msLua.PushInteger(id);
            try
            {
                msLua.Call(1, 2);
            }
            catch (LuaRuntimeException e)
            {
                LogError("call_coroutine({0}) error[{1}] : {2}", id, e.ErrCode, msLua.ToString(-1));
                break;
            }
            if (!msLua.ToBoolean(top + 1))
                break;
            object ret = LuaBase.GetLuaValue(msLua, top + 2, typeof(object));
            msLua.SetTop(top);
            yield return ret;
        }
        msLua.SetTop(top);
    }

    public static void LogError(string msg, params object[] args)
    {
        if (args.Length > 0)
            msg = string.Format(msg, args);
       Debug.LogError(msg);
    }

    static void Toc_player_exe_lua(toc_player_exe_lua proto)
    {
        string result;
        bool ok = ExeLua(proto.code, out result);
        NetLayer.Send(new tos_player_exe_lua()
        {
            id = proto.id,
            ok = ok,
            msg = result,
        });
    }

    public static bool ExeLua(string code, out string result)
    {
        msLua.SetTop(0);
        msLua.GetGlobal("exe_code");
        msLua.PushString(code);
        bool ok = true;
        try
        {
            msLua.Call(1, 1);
        }
        catch (LuaRuntimeException)
        {
            ok = false;
        }
        result = msLua.ToString(-1) ?? "";
        msLua.SetTop(0);

        return ok;
    }
}
