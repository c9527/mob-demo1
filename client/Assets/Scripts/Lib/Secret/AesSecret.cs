﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System;
using Object = UnityEngine.Object;
using ComponentAce.Compression.Libs.zlib;


public class AesSecret
{
#if UNITY_ANDROID
    private const string m_key = "key@android";
    private const string m_vector = "vec@android";
#elif UNITY_IPHONE
        private const string m_key="key2@iOS";
        private const string m_vector="vec2@iOS";       
#elif UNITY_WEBPLAYER
        private const string m_key="key3@web";
        private const string m_vector="vec3@web";       
#elif UNITY_STANDALONE
        private const string m_key="key4@pc";
        private const string m_vector="vec4@pc";       
#else
        private const string m_key="key5@default";
        private const string m_vector="vec5@default";       
#endif

    private const string m_signal = "Too Yong,Too Simple";


    public static byte[] ZLibDecompress(byte[] srcDat)
    {
        using (MemoryStream stmOut = new MemoryStream())
        {
            using (ZOutputStream zip = new ZOutputStream(stmOut))
                zip.Write(srcDat, 0, srcDat.Length);
            return stmOut.ToArray();
        }
    }

    public static byte[] ZLibCompress(byte[] srcDat)
    {
        using (MemoryStream stmOut = new MemoryStream())
        {
            using (ZOutputStream zip = new ZOutputStream(stmOut, zlibConst.Z_BEST_COMPRESSION))
                zip.Write(srcDat, 0, srcDat.Length);
            return stmOut.ToArray();
        }
    }

    public static string EncryptString(string data)
    {
        Byte[] plainBytes = Encoding.UTF8.GetBytes(data);
        plainBytes = ZLibCompress(plainBytes);
        Byte[] bKey = new Byte[32];
        Array.Copy(Encoding.UTF8.GetBytes(m_key.PadRight(bKey.Length)), bKey, bKey.Length);
        Byte[] bVector = new Byte[16];
        Array.Copy(Encoding.UTF8.GetBytes(m_vector.PadRight(bVector.Length)), bVector, bVector.Length);

        Byte[] Cryptograph = null; // 加密后的密文  

        Rijndael Aes = Rijndael.Create();
        try
        {
            // 开辟一块内存流  
            using (MemoryStream Memory = new MemoryStream())
            {
                // 把内存流对象包装成加密流对象  
                using (CryptoStream Encryptor = new CryptoStream(Memory,
                 Aes.CreateEncryptor(bKey, bVector),
                 CryptoStreamMode.Write))
                {
                    // 明文数据写入加密流  
                    Encryptor.Write(plainBytes, 0, plainBytes.Length);
                    Encryptor.FlushFinalBlock();

                    Cryptograph = Memory.ToArray();
                }

            }
        }
        catch
        {
            Cryptograph = null;
        }
        return m_signal + Convert.ToBase64String(Cryptograph);
        //return Cryptograph;
    }

    public static string DecryptString(string strSource)
    {
        if (!strSource.StartsWith(m_signal))
            return strSource;
        else
        {
            strSource = strSource.Remove(0, m_signal.Length);
        }
        Byte[] encryptedBytes = Convert.FromBase64String(strSource);
        Byte[] bKey = new Byte[32];
        Array.Copy(Encoding.UTF8.GetBytes(m_key.PadRight(bKey.Length)), bKey, bKey.Length);
        Byte[] bVector = new Byte[16];
        Array.Copy(Encoding.UTF8.GetBytes(m_vector.PadRight(bVector.Length)), bVector, bVector.Length);

        Byte[] original = null; // 解密后的明文  
        try
        {
            Rijndael Aes = Rijndael.Create();
            // 开辟一块内存流，存储密文  
            using (MemoryStream Memory = new MemoryStream(encryptedBytes))
            {
                // 把内存流对象包装成加密流对象  
                using (CryptoStream Decryptor = new CryptoStream(Memory,
                Aes.CreateDecryptor(bKey, bVector),
                CryptoStreamMode.Read))
                {
                    // 明文存储区  
                    using (MemoryStream originalMemory = new MemoryStream())
                    {
                        Byte[] Buffer = new Byte[1024];
                        Int32 readBytes = 0;
                        while ((readBytes = Decryptor.Read(Buffer, 0, Buffer.Length)) > 0)
                        {
                            originalMemory.Write(Buffer, 0, readBytes);
                        }
                        original = originalMemory.ToArray();
                    }
                }
                original = ZLibDecompress(original);
            }
        }
        catch
        {
            Debug.LogError("配置解密出错");
        }
        return Encoding.UTF8.GetString(original);
    }

}
