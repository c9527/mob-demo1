//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2014 Tasharen Entertainment
//----------------------------------------------

using System;
using UnityEngine;

/// <summary>
/// Tween the object's rotation.
/// </summary>

[AddComponentMenu("NGUI/Tween/Tween Rotation")]
public class TweenRotation : UITweener
{
	public Vector3 from;
	public Vector3 to;

	Transform mTrans;
    Action m_callBack;
	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }

	[System.Obsolete("Use 'value' instead")]
	public Quaternion rotation { get { return this.value; } set { this.value = value; } }

	/// <summary>
	/// Tween's current value.
	/// </summary>

	public Quaternion value { get { return cachedTransform.localRotation; } set { cachedTransform.localRotation = value; } }

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished)
	{
		value = Quaternion.Euler(new Vector3(
			Mathf.Lerp(from.x, to.x, factor),
			Mathf.Lerp(from.y, to.y, factor),
			Mathf.Lerp(from.z, to.z, factor)));     
        if(isFinished&&m_callBack!=null)
        {
            m_callBack();
            m_callBack = null;
        }        
	}

    /// <summary>
    /// 避免360旋转时出现反转问题
    /// </summary>
    /// <param name="trans"></param>
    /// <param name="duration"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    static public TweenRotation Begin360(Transform trans, TweenRotation tr, float duration, Vector3 to)
    {
        var pre = tr;   // go.GetComponent<TweenRotation>();
        if (pre)
            pre.SetEndToCurrentValue();
        var from = trans.localEulerAngles; // go.transform.localEulerAngles;
        if (to.x > from.x + 180) to.x -= 360;
        if (to.y > from.y + 180) to.y -= 360;
        if (to.z > from.z + 180) to.z -= 360;
        if (to.x < from.x - 180) to.x += 360;
        if (to.y < from.y - 180) to.y += 360;
        if (to.z < from.z - 180) to.z += 360;

        TweenRotation comp = UITweener.Begin<TweenRotation>(trans.gameObject, duration);
        comp.from = from;
        comp.to = to;

        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	static public TweenRotation Begin (GameObject go, float duration, Quaternion rot,Action callBack=null)
	{        
		TweenRotation comp = UITweener.Begin<TweenRotation>(go, duration);
        comp.m_callBack = callBack;
		comp.from = comp.value.eulerAngles;
		comp.to = rot.eulerAngles;                
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	[ContextMenu("Set 'From' to current value")]
	public override void SetStartToCurrentValue () { from = value.eulerAngles; }

	[ContextMenu("Set 'To' to current value")]
	public override void SetEndToCurrentValue () { to = value.eulerAngles; }

	[ContextMenu("Assume value of 'From'")]
	void SetCurrentValueToStart () { value = Quaternion.Euler(from); }

	[ContextMenu("Assume value of 'To'")]
	void SetCurrentValueToEnd () { value = Quaternion.Euler(to); }
}
