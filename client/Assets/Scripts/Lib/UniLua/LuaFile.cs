﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace UniLua
{
    public class LuaFile : ILoadInfo, IDisposable
    {
        static Dictionary<string, byte[]> msAllFile = new Dictionary<string, byte[]>();

        byte[] mBuf;
        int mPos = 0;

        static LuaFile()
        {
            foreach (var obj in ResourceManager.LoadAssets("LuaRoot"))
                msAllFile[obj.name + ".lua"] = Encoding.UTF8.GetBytes(obj.ToString());
        }

        public static LuaFile OpenFile(string filename)
        {
            return new LuaFile(msAllFile[filename]);
        }

        public static bool Readable(string filename)
        {
            return msAllFile.ContainsKey(filename);
        }

        public LuaFile(byte[] buf)
        {
            mBuf = buf;
        }

        public int ReadByte()
        {
            if (mPos >= mBuf.Length)
                return -1;
            return mBuf[mPos++];
        }

        public int PeekByte()
        {
            if (mPos >= mBuf.Length)
                return -1;
            return mBuf[mPos];
        }

        public void Dispose()
        {
            // nothing
        }

        public void SkipComment()
        {
            // nothing
        }
    }

}

