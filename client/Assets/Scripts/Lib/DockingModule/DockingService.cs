﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
#if UNITY_WEBPLAYER
using DockingWeb;
#endif


namespace DockingModule
{
	public enum enumResponseID
	{
		ResponseID_Ready = 0,
		ResponseID_Request,
	}
	
	public enum enumRequestID
	{
		RequestID_None = 0,
		RequestID_GetData,
		RequestID_Login,
		RequestID_Charge,
		RequestID_Log,
		RequestID_PostData,
		RequestID_CheckPic,
		RequestID_MAX,
	}
	
	public enum enumResponseType
	{
		ResponseType_String = 0,
		ResponseType_Texture,
	}
	
	public class DockingService
	{
		public static bool isReady = false;
		protected static IntPtr m_pContext = IntPtr.Zero;
		private static MessageQueue sMessageQueue = new MessageQueue();
        private static CDict m_jsonValue = new CDict();
		private const string LogOpenGame = "OpenGame";
		private const string LogOpenLoginView = "OpenLoginView";
		private const string LogLogin = "Login";
		private const string LogCreateRole = "CreateRole";
		private const string LogSelectServer = "SelectServer";
		private const string LogLevelUp = "LevelUp";
		private const string LogOnline = "Online";
		
		public static bool Initialize()
		{
			if (IntPtr.Zero != m_pContext)
			{
				return true;
			}
			
			InitJsonValue();
		    try
		    {
		        if (Driver.m_platform == EnumPlatform.web)
		        {
		            var json = new CDict();
                    json["cid"] = SdkManager.m_cid;
                    json["oid"] = SdkManager.m_oid;
                    json["aid"] = SdkManager.m_aid;
                    json["fnpid"] = SdkManager.m_pid;
                    json["fnppid"] = SdkManager.m_fnppid;
                    json["uid"] = SdkManager.m_uid;
                    json["fngid"] = SdkManager.m_fngid;
                    json["fnpidraw"] = SdkManager.m_fnpidraw;
		            var strJson = CJsonHelper.Save(json);
                    Debug.Log("Docking初始化：" + strJson);
                    m_pContext = DockingPlugin.CreateDocking(OnResponse, strJson);
		        }
		        else
		            m_pContext = DockingPlugin.CreateDocking(OnResponse, "D:\\4399\\qmqs\\game");
		    }
		    catch (Exception)
		    {
                Debug.Log("Docking初始化失败");
		    }

			//Debug.Log("Intialize success!!!12111");
			return m_pContext != null;
		}
		
        public static bool Request(int nReqID, string szInfo1, string szInfo2, string szInfo3)
		{
			Debug.Log ("Docking Request : " + szInfo1 + "," + szInfo2);
			if (IntPtr.Zero == m_pContext)
			{
				Debug.Log("not init");
				return false;
			}

		    try
		    {
            return DockingPlugin.Request(m_pContext, nReqID, szInfo1, szInfo2, szInfo3);
		    }
		    catch (Exception)
		    {
		        Debug.Log("Docking Request失败");
		    }
		    return false;
		}
		
		#if UNITY_WEBPLAYER
		public static void OnResponse(int nRespID, int nRetCode, int nReqID, int nResponseType, string szInfo, uint len, string szInfo2)
			
			#else 
			public static void OnResponse(int nRespID, int nRetCode, int nReqID, int nResponseType, IntPtr szInfo, uint len, string szInfo2)
				#endif
		{
			CMessage pMsg = new CMessage();
			pMsg.nResponseID = nRespID;
			pMsg.nRequestID = nReqID;
			pMsg.nRetCode = nRetCode;
			pMsg.nResponseType = nResponseType;
			#if UNITY_WEBPLAYER
			string str = szInfo;
			#else
			string str = Marshal.PtrToStringAnsi(szInfo);
			
			#endif
			byte[] buffer;
			if (len > 0)
			{
				buffer = new byte[(int)len];
				#if UNITY_WEBPLAYER
				buffer = System.Text.Encoding.Default.GetBytes(szInfo);
				#else
				Marshal.Copy(szInfo, buffer, 0, (int)len);
				#endif
				
			}
			else
			{
				buffer = new byte[1];
			}
			pMsg.strInfo = buffer;
			pMsg.nLength = len;
			pMsg.strInfo2 = szInfo2;
			sMessageQueue.AppendMessage(pMsg);
		}
		
		public static void Update()
		{
			if (sMessageQueue.m_dqMessageQueue.Count > 0)
			{
				sMessageQueue.RunMessages();
			}
		}
		
		public static void ExecuteMessage(CMessage pMsg)
		{
			OnMessage(pMsg);
		}
		
		public static void OnMessage(CMessage pMsg)
		{
			if (pMsg.nResponseID == (int)enumResponseID.ResponseID_Ready)
			{
				isReady = true;
				Debug.Log("Docking初始化成功");
			    SendLoginedLog();
			}
			else if (pMsg.nResponseID == (int)enumResponseID.ResponseID_Request)
			{
				if (pMsg.nRetCode != 0)
				{
					Debug.Log("返回信息有误，错误码为：" + pMsg.nRetCode);
				}
                else if (pMsg.nRequestID == (int) enumRequestID.RequestID_GetData)
                {
                    string str = System.Text.Encoding.Default.GetString(pMsg.strInfo);
                    var json = CJsonHelper.Load(str) as CDict;
                    var imgBytes = Convert.FromBase64String(json.Dict("data").Str("qrdata"));
                    if (UIManager.IsOpen<PanelShare>())
                    {
                        UIManager.GetPanel<PanelShare>().SetQRCode(imgBytes);
                        UIManager.GetPanel<PanelShare>().SetShareLink(json.Dict("data").Str("url").Replace("\\/", "/"));
                    }
                }
				else
				{
					string str = System.Text.Encoding.Default.GetString(pMsg.strInfo);
					Debug.Log("ResponseID_Request callback:, id = " + pMsg.nRequestID + ", strInfo = " + str);
				}
				
			}
		}
		
		public static void InitJsonValue()
		{
			m_jsonValue["eventname"] = "";
			m_jsonValue["eventkey"] = "";
			m_jsonValue["eventval"] = "";
			m_jsonValue["uid"] = "";
			m_jsonValue["server"] = "";
			m_jsonValue["screen"] = "";
		}
		
		public static void ClearJson()
		{
			m_jsonValue.Clear();
		}
		
		private static void SetJsonValue(string key, string value)
		{
			m_jsonValue[key] = value;
		}
		
		public static void SendOpenGameLog()
		{
			SetJsonValue("eventname", "open_game");
            setStatic();
			SendLog();
		}
		
		public static void SendOpenLoginViewLog()
		{
			SetJsonValue("eventname", "open_login");
            setStatic();
			SendLog();
		}
		
		public static void SendLoginedLog()
		{
            ClearJson();
            Debug.Log("登录第三方平台");
			SetJsonValue("eventname", "logined");
            setStatic();
			SendLog();
		}
		
		public static void SendSelectServerLog()
		{
            ClearJson();
            Debug.Log("选择服务器" + SdkManager.m_serverId);
			SetJsonValue("eventname", "select_server");
            setStatic();
			SendLog();
		}
		
		public static void SendCreateRoleLog(string eventVal)
		{
            ClearJson();
            Debug.Log("创建角色");
			SetJsonValue("eventname", "create_role");
			SetJsonValue("eventkey", "role");
			SetJsonValue("eventval", eventVal);
            setStatic();
			SendLog();
		}
		
		public static void SendRoleLevelChangeLog(string eventVal)
		{
            ClearJson();
            Debug.Log("升级");
			SetJsonValue("eventname", "role_level_change");
			SetJsonValue("eventkey", "level");
			SetJsonValue("eventval", eventVal);
            setStatic();
			SendLog();
		}
		
		public static void SendOnlineLog(string eventVal)
		{
            ClearJson();
            Debug.Log("在线");
			SetJsonValue("eventname", "send_online");
			SetJsonValue("eventkey", "onlne_status");
			SetJsonValue("eventval", eventVal);
            setStatic();
            SendLog();
		}
		
		public static void GetShareData()
		{
			ClearJson();
			//必填
            SetJsonValue("roleId", PlayerSystem.roleId.ToString());
            SetJsonValue("roleName", PlayerSystem.roleData.name);
            SetJsonValue("uid", SdkManager.m_uid);
            SetJsonValue("serverName", SdkManager.m_serverName);
			SetJsonValue("callback_info", "");
            SetJsonValue("serverId", SdkManager.m_serverId);
            SetJsonValue("device_id", SystemInfo.deviceUniqueIdentifier);
			//选填
			SetJsonValue("appVersion", "");
			SetJsonValue("sdkVersion", "");
			SetJsonValue("device", "");
			SetJsonValue("osVersion", "");
			SetJsonValue("nm", "");
			SetJsonValue("mno", "");
			SetJsonValue("areaId", "");
			SetJsonValue("screen", "");
			SetJsonValue("client_id", SdkManager.m_fngid);
			SetJsonValue("deviceType", "");
			
			SendLog((int)enumRequestID.RequestID_GetData, "Share");
		}
		
		public static void SendShareEnterLog()
		{
            Debug.Log("玩家登陆【分享】");
			ClearJson();
			SetJsonValue("roleId", PlayerSystem.roleId.ToString());
			SetJsonValue("roleName", PlayerSystem.roleData.name);
			SetJsonValue("uid", SdkManager.m_uid);
			SetJsonValue("serverName",SdkManager.m_serverName);
			SetJsonValue("serverId", SdkManager.m_serverId);
			SetJsonValue("roleLevel", PlayerSystem.roleData.level.ToString());
			SetJsonValue("gameId", SdkManager.m_fngid);
            SetJsonValue("did", SystemInfo.deviceUniqueIdentifier);
			SetJsonValue("appVersion", "");
			SetJsonValue("sdkVersion", "");
			SetJsonValue("device", "");
			SetJsonValue("osVersion", "");
			SetJsonValue("nm", "");
			SetJsonValue("mno", "");
			SetJsonValue("areaId", "");
			SetJsonValue("screen", "");
			SetJsonValue("deviceType", "");
			SendLog("ShareEnterGameLog");
		}
		
		public static void PostPicData(string imageData)
		{
			ClearJson();
            SetJsonValue("client_id", SdkManager.m_fngid);
			SetJsonValue("use_type", "1");//1代表头像，2代表朋友圈图片，默认为1
			SetJsonValue("upload_type", "1");//1代表图片文件数据使用字符串形式， 2代表采用multipart/form-data 表单提交, 默认为1
			SetJsonValue("image_type", "1");//约定的图片格式，其中1代表原图， 2代表128x128		注意：原图不要超过1M， 原则上应该在客户端控制在600K以内；
			SetJsonValue("client_key", "615059b3e67a89d8e719fc67caf308ed");
			SendLog((int)enumRequestID.RequestID_PostData, "", imageData);
		}
		
		public static void CheckPicData()
		{
			ClearJson();
			SetJsonValue("url", @"http:\/\/f1.img4399.com\/syavatar~2016\/03\/10\/13_UKA_EP3EWu.248x248.png");
			SendLog((int)enumRequestID.RequestID_CheckPic, "");
		}
		
		
		private static void SendLog()
		{
			SendLog(m_jsonValue["eventname"].ToString());
		}
		
		private static void SendLog(string eventName)
		{
			SendLog((int)enumRequestID.RequestID_Log, eventName);
		}

        public static void setStatic()
        {
            m_jsonValue["uid"] = SdkManager.m_uid;
            m_jsonValue["server"] = SdkManager.m_platform_server_id; 
            m_jsonValue["screen"] = Screen.currentResolution.width + "X" + Screen.currentResolution.height;
        }

		private static void SendLog(int reqID, string eventname, string szInfo = "")
		{
			Debug.Log ("reqID..." + reqID + ",eventname..." + eventname);
            string strJson = CJsonHelper.Save(m_jsonValue);
			if (!isReady)
			{
				Debug.LogError("Docking 还没有准备好");
				return;
			}
			
            Request(reqID, eventname, strJson, szInfo);
		}

        public static void Destroy()
        {
            try
            {
                if (!Application.isEditor && SdkManager.Enable)
                    DockingPlugin.DestroyDocking(m_pContext);
            }
            catch (Exception)
            {
                Debug.Log("Docking Destroy失败");
            }
            
        }
	}
}
