using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using UnityEngine;
#if UNITY_WEBPLAYER
using DockingWeb;
#endif


namespace DockingModule
{
	public class DockingPlugin
	{
#if UNITY_WEBPLAYER
		public static IntPtr CreateDocking(ResponseFunc callback, string szInfo)
		{
			return Docking.CreateDocking(callback, szInfo);
		}
		
		public static bool Request(IntPtr pContext, int reqID, string szInfo1, string szInfo2, string szInfo3)
		{
			return Docking.Request(pContext, reqID, szInfo1, szInfo2, szInfo3);
		}
		
		public static void DestroyDocking(IntPtr pContext)
		{
			Docking.DestroyDocking(pContext);
		}
#else
        private const string PluginPath = "Docking";
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate void ResponseFunc(int nRespID, int retCode, int nReqID, int nResponseType, IntPtr szInfo, uint len, string szInfo2);
		
		[DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr CreateDocking(ResponseFunc callback, string szInfo);
		
		[DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Request(IntPtr pContext, int reqID, string szInfo1, string szInfo2, string szInfo3);
		
		[DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
		public static extern void DestroyDocking(IntPtr pContext);
#endif
	}
}
