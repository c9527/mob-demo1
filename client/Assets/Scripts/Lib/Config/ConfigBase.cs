﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// 可序列化的单行配置项基类
///     1.支持的字段类型即为int,bool,float,string,Vector2,Vector3,ItemInfo及其各自的一维数组
///     2.字段名和配置中相同字段名（大小写敏感）的自动赋值，找不到对应的则为默认值
///     3.可以在其派生类的构造函数传入字段名作为主键，来创建字典，主键值不可重复，不可留空
///     4.原则上，在正常使用配置的时候，是不可以修改配置对象里的数据的
///     5.m_dataArr和m_dataDic是两份不同的数据，如果要修改数据的话要修改m_dataArr，m_dataDic字典里的数据只用来查询，不会序列化
/// </summary>
/// 
public class ConfigBase<TConfigLine> : ScriptableObject, IConfig where TConfigLine : class
{
    protected const char SPLIT_FIELD = '\t';      //列分隔符
    protected const char SPLIT_ARRAY = ';';       //数组分隔符
    protected const char SPLIT_SUB_ARRAY = '#';   //子数组分隔符
    private int m_dataStartLine = 2;      //数据区起始行

    private static readonly Type TYPE_INT = typeof(int);
    private static readonly Type TYPE_EPT_INT = typeof(EptInt);
    private static readonly Type TYPE_BOOL = typeof(bool);
    private static readonly Type TYPE_EPT_FLOAT = typeof(EptFloat);
    private static readonly Type TYPE_FLOAT = typeof(float);
    private static readonly Type TYPE_STRING = typeof(string);
    private static readonly Type TYPE_VECTOR2 = typeof(Vector2);
    private static readonly Type TYPE_VECTOR3 = typeof(Vector3);
    private static readonly Type TYPE_ITEM = typeof(ItemInfo);
    private static readonly Type TYPE_ENUM = typeof(Enum);
    private static readonly Type TYPE_ROBOT = typeof(RobotInfo);
    private static readonly Type TYPE_ITEM_MALL_PRICE = typeof(ItemMallPrice);

    private static readonly string[] ZERO_STRINGS_ARRAY = new string[0];

    public const int ERROR_CODE_SUCCESS = 0;            //成功
    public const int ERROR_CODE_PARSE_FAILED = 1;       //数据格式转换失败
    public const int ERROR_CODE_NOT_SUPPORT_TYPE = 2;   //不支持的数据类型

    /// <summary>
    /// 数据数组
    /// </summary>
    public TConfigLine[] m_dataArr;
    public string[] m_fieldNames;           //列名
    public string[] m_fieldComments;        //列备注
    public string m_keyName;                //保存指定的keyName
    private Dictionary<object, TConfigLine> m_dataDic;     //可根据需要，在构造函数里指定了keyName，就可以用字典来加速读取

    /// <summary>
    /// 构造函数
    /// </summary>
    
    protected ConfigBase(string keyName = "")
    {
        m_keyName = keyName;
    }

    /// <summary>
    /// 创建配置对象
    /// </summary>
    /// <param name="content">配置内容</param>
    /// <returns></returns>
    public void Load(string content)
    {
        if (string.IsNullOrEmpty(content))
            throw new Exception(GetErrorMsg("配置加载内容为空"));
        var lines = Util.StrToLines(content);
        var lineCount = lines.Count;
        var hasCommon = false;
        if (lines.Count > 1)
            hasCommon = lines[1].StartsWith("//") || lines[1].StartsWith("\t") || lines[1].StartsWith("\r") || lines[1].StartsWith("\n");
        m_dataStartLine = hasCommon ?  2 : 1;
        var configLinesArr = new TConfigLine[lineCount - m_dataStartLine];
        var fields = typeof(TConfigLine).GetFields();
        var names = lines[0].Split(SPLIT_FIELD);
        var namesKeyIndex = -1;
        
        m_fieldNames = names;
        m_fieldComments = hasCommon ? lines[1].Split(SPLIT_FIELD) : null;

        //生成从配置文件到类的字段索引映射
        var anyMapSucess = false;
        var indexMap = new int[names.Length];
        for (int i = 0; i < names.Length; i++)
        {
            var hasMap = false;
            for (int j = 0; j < fields.Length; j++)
            {
                var field = fields[j];
                if (names[i] == field.Name)
                {
                    hasMap = true;
                    indexMap[i] = j;
                    if (names[i] == m_keyName)
                        namesKeyIndex = i;
                    break;
                }
            }
            if (!hasMap)
                indexMap[i] = -1;
            anyMapSucess |= hasMap;
        }

        if (names.Length == 0 || !anyMapSucess)
            throw new Exception(GetErrorMsg("配置格式错误"));

        if (!string.IsNullOrEmpty(m_keyName))
            m_dataDic = new Dictionary<object, TConfigLine>();

        for (int i = m_dataStartLine; i < lineCount; i++)
        {
            var line = lines[i];
            var datas = line.Split(SPLIT_FIELD);
            var configLine = Activator.CreateInstance<TConfigLine>();
            object key = null;

            if (datas.Length > indexMap.Length)
                throw new Exception(GetErrorMsg("数据列数超过标题列数", i+1));

            for (int j = 0; j < datas.Length; j++)
            {
                var data = datas[j];
                var index = indexMap[j];
                if (data == null || index == -1)
                    continue;

                object value;
                int resultCode; //结果代码，0表示正常
                var fieldType = fields[index].FieldType;
                if (fields[index].FieldType.IsArray)
                    resultCode = ParseArray(fieldType.GetElementType(), string.IsNullOrEmpty(data) ? ZERO_STRINGS_ARRAY : data.Split(SPLIT_ARRAY), out value);
                else
                    resultCode = ParseValue(fieldType, data, out value);

                if (resultCode == ERROR_CODE_SUCCESS)
                    fields[index].SetValue(configLine, value);
                else if (resultCode == ERROR_CODE_PARSE_FAILED)
                    throw new Exception(GetErrorMsg("数据格式转换失败", i + 1, names[j]));
                else if (resultCode == ERROR_CODE_NOT_SUPPORT_TYPE)
                    throw new Exception(GetErrorMsg("不支持的数据类型", i + 1, names[j]));

                if (j == namesKeyIndex)
                    key = value;
            }
            configLinesArr.SetValue(configLine, i - m_dataStartLine);
            if (!string.IsNullOrEmpty(m_keyName) && key != null)
            {
                if (m_dataDic.ContainsKey(key))
                    throw new Exception(GetErrorMsg("配置文件的主键重复", key, names[0]));
                m_dataDic.Add(key, configLine);
            }
        }

        m_dataArr = configLinesArr;
    }

    /// <summary>
    /// 克隆配置对象的内容
    /// </summary>
    /// <returns></returns>
    public void Load(ConfigBase<TConfigLine> config)
    {
        Load(config.Export());
    }

    /// <summary>
    /// 用字典的方式查询某一行数据
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public TConfigLine GetLine(object key)
    {
        if (m_dataDic == null)
            throw new Exception(GetErrorMsg("需要先构建字典"));
        return m_dataDic.ContainsKey(key) ? m_dataDic[key] : null;
    }

    public bool HasLine(object key)
    {
        if (m_dataDic == null)
            return false;
        return m_dataDic.ContainsKey(key);
    }

    /// <summary>
    /// 因为字典对象无法序列化，所以从AB中读取对象后，如果需要用字典的，重新构建字典对象
    /// </summary>
    public void ReBuildDic()
    {
        if (string.IsNullOrEmpty(m_keyName) || m_dataArr == null || m_dataArr.Length == 0)
            return;

        m_dataDic = new Dictionary<object, TConfigLine>();
        var fieldInfo = typeof(TConfigLine).GetField(m_keyName);
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            var key = fieldInfo.GetValue(m_dataArr[i]);
            if (m_dataDic.ContainsKey(key))
                throw new Exception(GetErrorMsg("配置文件的主键重复", key));
            m_dataDic.Add(key, m_dataArr[i]);
        }
    }

    /// <summary>
    /// 导出内存中的数据为字符串
    /// </summary>
    /// <returns></returns>
    public string Export()
    {
        var sb = new StringBuilder();
        var fields = typeof(TConfigLine).GetFields();
        var names  = m_fieldNames;
        if (names == null)
        {
            names = new string[fields.Length];
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = fields[i].Name;
            }
        }

        //生成从配置文件到类的字段索引映射
        var indexMap = new int[names.Length];
        for (int i = 0; i < names.Length; i++)
        {
            var hasMap = false;
            for (int j = 0; j < fields.Length; j++)
            {
                var field = fields[j];
                if (names[i] == field.Name)
                {
                    hasMap = true;
                    indexMap[i] = j;
                    break;
                }
            }
            if (!hasMap)
                indexMap[i] = -1;
        }

        AppendLine(sb, names);
        if (m_fieldComments != null)
            AppendLine(sb, m_fieldComments);

        for (int i = 0; i < m_dataArr.Length; i++)
        {
            int resultCode; //结果代码
            var lineDataArr = new string[names.Length];
            for (int j = 0; j < names.Length; j++)
            {
                var index = indexMap[j];
                if (index == -1)
                    continue;
                string result;
                var value = fields[index].GetValue(m_dataArr[i]);
                var fieldType = fields[index].FieldType;
                if (fieldType.IsArray)
                    resultCode = AntiParseArray(fieldType.GetElementType(), (Array)value, out result);
                else
                    resultCode = AntiParseValue(fieldType, value, out result);
                
                if (resultCode == ERROR_CODE_SUCCESS)
                    lineDataArr[j] = result;
                else if (resultCode == ERROR_CODE_PARSE_FAILED)
                    throw new Exception(GetErrorMsg("数据格式转换失败", i + 1, names[j]));
                else if (resultCode == ERROR_CODE_NOT_SUPPORT_TYPE)
                    throw new Exception(GetErrorMsg("不支持的数据类型", i + 1, names[j]));
            }

            AppendLine(sb, lineDataArr);
        }

        return sb.ToString();
    }

    /// <summary>
    /// 克隆对象，效率不高，不可经常调用
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
        var clone = CreateInstance(GetType()) as ConfigBase<TConfigLine>;
        clone.Load(Export());
        return clone;
    }

    /// <summary>
    /// 将字符串数组用\t分隔元素并在末尾添加换行符，追加到指定的StringBuilder对象上
    /// </summary>
    /// <param name="sb"></param>
    /// <param name="data"></param>
    private void AppendLine(StringBuilder sb, string[] data)
    {
        for (int i = 0; i < data.Length; i++)
        {
            if (i > 0)
                sb.Append('\t');
            sb.Append(data[i]);
        }
        sb.Append("\r\n");
    }

    /// <summary>
    /// 清空数据
    /// </summary>
    public void Clear()
    {
        m_fieldNames = null;
        m_fieldComments = null;
        m_keyName = null;

        if (m_dataArr != null)
        {
            for (int i = 0; i < m_dataArr.Length; i++)
                m_dataArr[i] = null;
        }

        if (m_dataDic != null)
        {
            m_dataDic.Clear();
        }

    }

    /// <summary>
    /// 解析值类型
    /// </summary>
    /// <param name="type">类型</param>
    /// <param name="value">待解析的字符串</param>
    /// <param name="result">解析后的结果</param>
    /// <returns>错误代码</returns>
    private static int ParseValue(Type type, string value, out object result)
    {
        var success = true;
        if (type == TYPE_STRING)
            result = value;
        else if (type == TYPE_INT)
        {
            int v = 0;
            if (!string.IsNullOrEmpty(value))
                success = Int32.TryParse(value, out v);
            result = v;
        }
        else if (type == TYPE_EPT_INT)
        {
            int v = 0;
            if (!string.IsNullOrEmpty(value))
                success = Int32.TryParse(value, out v);
            result = (EptInt)v;
        }
        else if (type == TYPE_FLOAT)
        {
            float v = 0f;
            if (!string.IsNullOrEmpty(value))
                success = Single.TryParse(value, out v);
            result = v;
        }
        else if(type == TYPE_EPT_FLOAT)
        {
            float v = 0f;
            if (!string.IsNullOrEmpty(value))
                success = Single.TryParse(value, out v);
            result = (EptFloat)v;
        }
        else if (type == TYPE_BOOL)
        {
            bool v = false;
            if (!string.IsNullOrEmpty(value))
                success = Boolean.TryParse(value, out v);
            result = v;
        }
        else if (type.BaseType == TYPE_ENUM)
        {
            Enum e;
            try
            {
                e = Enum.Parse(type, value, false) as Enum;
            }
            catch (Exception)
            {
                e = null;
                success = false;
            }
            result = e;
        }
        else if (type == TYPE_VECTOR2)
        {
            var v = new Vector2();
            if (!string.IsNullOrEmpty(value))
            {
                float p;
                char splitChar = SPLIT_SUB_ARRAY;
                if (value.IndexOf(SPLIT_SUB_ARRAY) == -1)
                    splitChar = SPLIT_ARRAY;
                var arr = value.Split(splitChar);
                if (arr.Length == 2)
                {
                    success &= Single.TryParse(arr[0], out p);
                    v.x = p;
                    success &= Single.TryParse(arr[1], out p);
                    v.y = p;
                }
                else
                    success = false;
            }
            result = v;
        }
        else if (type == TYPE_VECTOR3)
        {
            var v = new Vector3();
            if (!string.IsNullOrEmpty(value))
            {
                char splitChar = SPLIT_SUB_ARRAY;
                if (value.IndexOf(SPLIT_SUB_ARRAY) == -1)
                    splitChar = SPLIT_ARRAY;
                var arr = value.Split(splitChar);
                if (arr.Length == 3)
                {
                    success &= Single.TryParse(arr[0], out v.x);
                    success &= Single.TryParse(arr[1], out v.y);
                    success &= Single.TryParse(arr[2], out v.z);
                }
                else
                    success = false;
            }
            result = v;
        }
        else if (type == TYPE_ITEM)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var item = new ItemInfo();
                char splitChar = SPLIT_SUB_ARRAY;
                if (value.IndexOf(SPLIT_SUB_ARRAY) == -1)
                    splitChar = SPLIT_ARRAY;
                var arr = value.Split(splitChar);
                if (arr.Length >= 2)
                {
                    success &= int.TryParse(arr[0], out item.ID);
                    success &= int.TryParse(arr[1], out item.cnt);
                    if (arr.Length > 2)
                        success &= float.TryParse(arr[2], out item.day);
                }
                else
                    success = false;
                result = item;
            }
            else
            {
                result = null;
            }
        }
        else if(type == TYPE_ROBOT)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var info = new RobotInfo();
                char splitChar = SPLIT_SUB_ARRAY;
                if (value.IndexOf(SPLIT_SUB_ARRAY) == -1)
                    splitChar = SPLIT_ARRAY;
                var arr = value.Split(splitChar);
                if (arr.Length == 2)
                {
                    info.lvl = arr[0];
                    success &= int.TryParse(arr[1], out info.num);
                }
                else
                    success = false;
                result = info;
            }
            else
                result = null;
        }
        else if (type == TYPE_ITEM_MALL_PRICE)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var item = new ItemMallPrice();
                char splitChar = SPLIT_SUB_ARRAY;
                if (value.IndexOf(SPLIT_SUB_ARRAY) == -1)
                    splitChar = SPLIT_ARRAY;
                var arr = value.Split(splitChar);
                if (arr.Length >= 2)
                {
                    success &= float.TryParse(arr[0], out item.time);
                    success &= int.TryParse(arr[1], out item.price);
                    if (arr.Length > 2)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(arr[2]))
                                item.type = EnumMoneyType.NONE;
                            else
                                item.type = (EnumMoneyType)Enum.Parse(typeof(EnumMoneyType), arr[2], false);
                        }
                        catch (Exception)
                        {
                            success = false;
                            item.type = EnumMoneyType.NONE;
                        }
                    }
                }
                else
                    success = false;
                result = item;
            }
            else
                result = null;
        }
        else
        {
            result = null;
            return ERROR_CODE_NOT_SUPPORT_TYPE;
        }

        if (!success)
            return ERROR_CODE_PARSE_FAILED;

        return ERROR_CODE_SUCCESS;
    }

    /// <summary>
    /// 解析数组类型
    /// </summary>
    /// <param name="eleType">数组元素类型</param>
    /// <param name="values">待解析的字符串数组</param>
    /// <param name="result">返回的结果</param>
    /// <returns>错误代码</returns>
    private static int ParseArray(Type eleType, string[] values, out object result)
    {
        object obj;
        int resultCode = 0;
        var array = Array.CreateInstance(eleType, values.Length);
        for (int i = 0; i < array.Length; i++)
        {
            resultCode = ParseValue(eleType, values[i], out obj);
            if (resultCode != 0)
                break;
            array.SetValue(obj, i);
        }
        result = array;
        return resultCode;
    }

    /// <summary>
    /// 反解析对象，输出对象的字符串表示
    /// </summary>
    /// <param name="type">对象类型</param>
    /// <param name="value">对象数值</param>
    /// <param name="result">返回字符串</param>
    /// <returns>错误代码</returns>
    private static int AntiParseValue(Type type, object value, out string result)
    {
        if (type == TYPE_STRING)
            result = (string)value;
        else if (type == TYPE_INT)
        {
            result = value.ToString();
        }
        else if (type == TYPE_FLOAT)
        {
            result = value.ToString();
        }
        else if (type == TYPE_BOOL)
        {
            result = (bool)value ? "TRUE" : "FALSE";
        }
        else if (type.BaseType == TYPE_ENUM)
        {
            result = value.ToString().ToUpper();
        }
        else if (type == TYPE_VECTOR2)
        {
            if (value != null)
            {
                var v = (Vector2) value;
                result = string.Format("{0}#{1}", v.x, v.y);
            }
            else
                result = null;

        }
        else if (type == TYPE_VECTOR3)
        {
            if (value != null)
            {
                var v = (Vector3) value;
                result = string.Format("{0}#{1}#{2}", v.x, v.y, v.z);
            }
            else
                result = null;
        }
        else if (type == TYPE_ITEM)
        {
            if (value != null)
            {
                var item = (ItemInfo) value;
                if (item.day == 0)
                    result = string.Format("{0}#{1}", item.ID, item.cnt);
                else
                    result = string.Format("{0}#{1}#{2}", item.ID, item.cnt, item.day);
            }
            else
                result = null;
        }
        else if(type == TYPE_ROBOT)
        {
            if (value != null)
            {
                var info = (RobotInfo)value;
                result = string.Format("{0}#{1}", info.lvl, info.num);
               
            }
            else
                result = null;
        }
        else if (type == TYPE_ITEM_MALL_PRICE)
        {
            if (value != null)
            {
                var item = (ItemMallPrice)value;
                if (item.type == EnumMoneyType.NONE)
                    result = string.Format("{0}#{1}", item.time, item.price);
                else
                    result = string.Format("{0}#{1}#{2}", item.time, item.price, item.type.ToString().ToUpper());

            }
            else
                result = null;
        }
        else
        {
            result = null;
            return ERROR_CODE_NOT_SUPPORT_TYPE;
        }

        return ERROR_CODE_SUCCESS;
    }

    /// <summary>
    /// 反解析对象的数组
    /// </summary>
    /// <param name="eleType">数组的元素类型</param>
    /// <param name="values">数组对象</param>
    /// <param name="result">输出的字符串</param>
    /// <returns>错误代码</returns>
    private static int AntiParseArray(Type eleType, Array values, out string result)
    {
        var sb = new StringBuilder();
        int resultCode = ERROR_CODE_SUCCESS;
        if (values == null)
        {
            result = null;
            return resultCode;
        }
        for (int i = 0; i < values.Length; i++)
        {
            string strOut;
            resultCode = AntiParseValue(eleType, values.GetValue(i), out strOut);
            if (resultCode != ERROR_CODE_SUCCESS)
                break;
            
            if (i > 0)
                sb.Append(';');
            sb.Append(strOut);
        }
        result = sb.ToString();
        return resultCode;
    }

    /// <summary>
    /// 配置的统一错误字符串拼接函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="row"></param>
    /// <param name="field"></param>
    /// <returns></returns>
    private static string GetErrorMsg(string message, object row = null, object field = null)
    {
        return String.Format("【配置】{0}\n{1} 行:{2} 列:{3}", message, typeof(TConfigLine), row, field);
    }
}

public class ItemInfo
{
    public int ID;
    public int cnt;
    public float day = 0;

    public static float TimeLimitToTime(uint timeLimit)
    {
        DateTime now = TimeUtil.GetNowTime();
        DateTime limit = TimeUtil.GetTime(timeLimit);
        TimeSpan span=limit-now;
        float t = 0f;
        t = span.Days + span.Hours * 0.01f + span.Minutes * 0.0001f;
        return t;
    }

}

public class ItemMallPrice
{
    public float time;
    public int price;
    public EnumMoneyType type = EnumMoneyType.NONE;
}

public class RobotInfo
{
    public string lvl;
    public int num;
}