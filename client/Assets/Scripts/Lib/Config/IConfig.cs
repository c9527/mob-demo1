﻿public interface IConfig
{
    void Load(string content);
    string Export();
    void ReBuildDic();
    void Clear();
}