﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

/// <summary>
/// CSV格式配置文件管理类
/// 新增配置文件方法：
///     1.定义【Config前缀】的数据格式描述类
///     2.创建同名csv文件放在Resources/Config/目录下(可以复制模板文件再修改)
///     3.用GetConfig拿到配置文件对象，可以选择访问m_dataArr或m_dataDic来访问行数据
/// </summary>
public static class ConfigManager
{
    private static Dictionary<string, IConfig> m_configDic;

    /// <summary>
    /// 异步从AB中加载所有配置文件，并缓存
    /// </summary>
    /// <param name="callBack"></param>
    public static void LoadAllConfig(Action callBack)
    {
        var time = DateTime.Now;
        ResourceManager.LoadConfigs("Config", assets =>
        {
            m_configDic = new Dictionary<string, IConfig>();
            if (assets == null)
            {
                Debug.Log("asset为空111111111111111111");
            }
            for (int i = 0; i < assets.Length; i++)
            {
                var configName = assets[i].name;
                if (!configName.StartsWith("Config"))
                    continue;

                if (Type.GetType(configName) != null)
                {
                    var config = ScriptableObject.CreateInstance(Type.GetType(configName)) as IConfig;
                    string decryptedStr = AesSecret.DecryptString(assets[i].ToString());
                    config.Load(decryptedStr);
                    m_configDic.Add(configName, config);
                }
                else
                {
                    Logger.Warning("【配置】未找到配置对应的类：" + configName);
                }
            }
            Logger.Log("配置加载时间(ms)：" + (DateTime.Now - time).TotalMilliseconds);
            if (callBack != null)
                callBack();
        });
    }

    /// <summary>
    /// 【编辑器专用】保存内存中的[指定]配置对象数据到本地
    /// </summary>
    /// <typeparam name="TConfig"></typeparam>
    public static void SaveConfigToResources<TConfig>() where TConfig : class, IConfig
    {
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        var config = GetConfig<TConfig>();
        File.WriteAllText(Application.dataPath + "/Resources/Config/" + typeof (TConfig).Name + ".csv", config.Export(), Encoding.Unicode);
#endif
    }

    /// <summary>
    /// 【编辑器专用】保存内存中的[所有]配置对象数据到本地
    /// </summary>
    public static void SaveAllConfigToResources()
    {
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        foreach (var kv in m_configDic)
        {
            File.WriteAllText(Application.dataPath + "/Resources/Config/" + kv.Key + ".csv", kv.Value.Export(), Encoding.Unicode);
        }
#endif
    }

    /// <summary>
    /// 获取配置文件对象
    /// </summary>
    /// <typeparam name="TConfig"></typeparam>
    /// <returns></returns>
    public static TConfig GetConfig<TConfig>() where TConfig : class, IConfig
    {
        var key = typeof (TConfig).ToString();
        return m_configDic.ContainsKey(key) ? (TConfig)m_configDic[key] : null;
    }

    public static IConfig GetConfig(string configName)
    {
        return m_configDic.ContainsKey(configName) ? m_configDic[configName] : null;
    }

    /// <summary>
    /// 清除指定配置数据
    /// </summary>
    /// <typeparam name="TConfig"></typeparam>
    public static void Clear<TConfig>() where TConfig : class, IConfig
    {
        var key = typeof(TConfig).ToString();
        if (m_configDic.ContainsKey(key))
        {
            m_configDic[key].Clear();
            m_configDic.Remove(key);
        }
    }

    /// <summary>
    /// 清空配置数据
    /// </summary>
    public static void ClearAll()
    {
        if (m_configDic == null)
            return;
        foreach (var config in m_configDic)
            config.Value.Clear();
        m_configDic.Clear();
    }

    public static Dictionary<string, IConfig> ConfigDic { get { return m_configDic; } } 

}
