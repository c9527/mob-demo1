﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ModelStealth : MonoBehaviour
{
    public float stealthTime = 5f; //秒
    public float minPlayTime = 0;  // 至少要播放多久才会被停止, 秒
    private float m_fTick = 0f;
    private float alpha;
    private Material stealthMat;

    private Renderer[] renderers;
    private List<Material> m_OldMat = new List<Material>();
    private bool m_bRun = false;
    private bool m_bInit = false;

    public void Reset()
    {

    }

    public void SetAlpha(float alpha)
    {
       this.alpha = alpha;
    }

    public void SetTime(float time)
    {
        stealthTime = time;
    }

    public void Run(bool bValue)
    {
        if(bValue == true)
        {
            if (stealthMat == null)
            {
                ResourceManager.LoadAsset("Roles/Fbx/Materials/mat_stealth", (obj) => { 
                    stealthMat = obj as Material;
                    SetStealthMat(stealthMat);
                    m_bRun = bValue;
                    m_fTick = 0;
                });
            }
            else
            {
                SetStealthMat(stealthMat);
                m_bRun = bValue;
                m_fTick = 0;
            }
        }
        else
        {
            if (renderers != null)
            {
                int i = 0;
                foreach (Renderer renderer in renderers)
                {
                    renderer.material = m_OldMat[i++];
                }
            }
        }
    }
	 
	void Update ()
    {
        // 至少要播放这么久才会被结束
        if (m_bRun == true && stealthTime != 0)
        {
            m_fTick += Time.deltaTime;
            if (m_fTick >= stealthTime && m_fTick > minPlayTime)
            {
                //到点关闭
                m_bRun = false;
                Run(false);
            }
        }
	}

    void SetStealthMat(Material stealthMat)
    {
        if (!m_bInit)
        {
            Init();
        }

        if (renderers != null)
        {
            foreach (Renderer renderer in renderers)
            {
                Material mat = renderer.material;
                stealthMat.mainTexture = mat.mainTexture;
                stealthMat.SetFloat(GameConst.SHADER_PROPERTY_ALPHA, alpha);
                renderer.material = stealthMat;
            }
        }
    }

    void Init()
    {
        renderers = transform.gameObject.GetComponentsInChildren<Renderer>();
        if (renderers != null)
        {
            m_OldMat.Clear();
            foreach (Renderer renderer in renderers)
            {
                m_OldMat.Add(renderer.material);
            }
        }
        m_bInit = true;
    }
}
