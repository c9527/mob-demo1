﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using UniLua;

static class LuaBase
{
    static List<Assembly> msAssemblyList = new List<Assembly>();

    public static int OpenLib(ILuaState lua)
    {
        var define = new NameFuncPair[]
			{
				new NameFuncPair( "add_assembly", lua_add_assembly ),
				new NameFuncPair( "get_type", lua_get_type ),
				new NameFuncPair( "get_member", lua_get_member ),
				new NameFuncPair( "member_get", lua_member_get ),
				new NameFuncPair( "member_set", lua_member_set ),
				new NameFuncPair( "member_call", lua_member_call ),
				new NameFuncPair( "class_type", lua_class_type ),
				new NameFuncPair( "create_obj", lua_create_obj ),
			};

        lua.L_NewLib(define);

        msAssemblyList.Add(Assembly.GetCallingAssembly());

        return 1;
    }

    static int lua_add_assembly(ILuaState lua)
    {
        string name = lua.L_CheckString(1);
        Assembly assembly = Assembly.Load(name);
        lua.L_ArgCheck(assembly != null, 1, "assembly not found: " + name);
        foreach (Assembly ass in msAssemblyList)
        {
            if (assembly == ass)
                return 0;
        }
        msAssemblyList.Add(assembly);
        lua.PushBoolean(true);
        return 1;
    }

    static int lua_get_type(ILuaState lua)
    {
        string name = lua.L_CheckString(1);
        Type typ = null;
        foreach (Assembly ass in msAssemblyList)
        {
            typ = ass.GetType(name);
            if (typ != null)
                break;
        }
        if (typ != null)
            lua.PushLightUserData(typ);
        else
            lua.PushNil();
        return 1;
    }

    static int lua_get_member(ILuaState lua)
    {
        Type typ = lua.ToUserData(1) as Type;
        lua.L_ArgCheck(typ != null, 1, "need c# Type");
        string key = lua.L_CheckString(2);
        MemberInfo[] mems = typ.GetMember(key, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        if (mems.Length <= 0)
        {
            lua.PushNil();
            return 1;
        }
        lua.CreateTable(mems.Length * 2, 0);
        for (int i = 0; i < mems.Length; i++)
        {
            MemberInfo mi = mems[i];
            lua.PushLightUserData(mi);
            lua.RawSetI(-2, i * 2 + 1);
            lua.PushString(mi.MemberType.ToString());
            lua.RawSetI(-2, i * 2 + 2);
        }
        return 1;
    }

    static void CheckStaticMember(ILuaState lua, bool isStatic, object obj, int argn)
    {
        if (isStatic)
            lua.L_ArgCheck(obj == null, argn, "static member do not need obj");
        else
            lua.L_ArgCheck(obj != null, argn, "instance member need obj");
    }

    static int lua_member_get(ILuaState lua)
    {
        MemberInfo mi = lua.ToUserData(1) as MemberInfo;
        lua.L_ArgCheck(mi != null, 1, "need c# MemberInfo");
        object obj = lua.ToUserData(2);
        switch (mi.MemberType)
        {
            case MemberTypes.Field:
                FieldInfo fi = mi as FieldInfo;
                CheckStaticMember(lua, fi.IsStatic, obj, 2);
                PPushC(lua, () => fi.GetValue(obj));
                break;
            case MemberTypes.Property:
                MethodInfo pmi = (mi as PropertyInfo).GetGetMethod();
                CheckStaticMember(lua, pmi.IsStatic, obj, 2);
                PPushC(lua, () => pmi.Invoke(obj, null));
                break;
            default:
                return lua.L_ArgError(1, "need Field or Property");
        }
        return 1;
    }

    public static void PushLuaValue(ILuaState lua, object value)
    {
        if (value == null)
        {
            lua.PushNil();
            return;
        }

        if (value is string)
        {
            lua.PushString(value as string);
            return;
        }

        if (value is Array)
        {
            Array ary = value as Array;
            lua.CreateTable(ary.Length, 0);
            for (int i = 0; i < ary.Length; i++)
            {
                PushLuaValue(lua, ary.GetValue(i));
                lua.RawSetI(-2, i + 1);
            }
            return;
        }

        Type typ = value.GetType();
        if (typ.IsEnum)
        {
            lua.PushInteger((int)value);
            return;
        }

        if (!typ.IsPrimitive)
        {
            lua.PushLightUserData(value);
            return;
        }

        if (value is int)
            lua.PushNumber((int)value);
        else if (value is uint)
            lua.PushNumber((uint)value);
        else if (value is long)
            lua.PushNumber((long)value);
        else if (value is byte)
            lua.PushNumber((byte)value);
        else if (value is short)
            lua.PushNumber((short)value);
        else if (value is float)
            lua.PushNumber((float)value);
        else if (value is double)
            lua.PushNumber((double)value);
        else if (value is bool)
            lua.PushBoolean((bool)value);
    }

    static int lua_member_set(ILuaState lua)
    {
        MemberInfo mi = lua.ToUserData(1) as MemberInfo;
        lua.L_ArgCheck(mi != null, 1, "need c# MemberInfo");
        object obj = lua.ToUserData(2);
        switch (mi.MemberType)
        {
            case MemberTypes.Field:
                FieldInfo fi = mi as FieldInfo;
                CheckStaticMember(lua, fi.IsStatic, obj, 2);
                object fv = GetLuaValue(lua, 3, fi.FieldType);
                PCallC(lua, () => fi.SetValue(obj, fv));
                break;
            case MemberTypes.Property:
                PropertyInfo pi = mi as PropertyInfo;
                MethodInfo pmi = pi.GetSetMethod();
                CheckStaticMember(lua, pmi.IsStatic, obj, 2);
                object pv = GetLuaValue(lua, 3, pi.PropertyType);
                PCallC(lua, () => pmi.Invoke(obj, new object[] { pv }));
                break;
            default:
                return lua.L_ArgError(1, "need Field or Property");
        }
        return 0;
    }

    public static object GetLuaValue(ILuaState lua, int index, Type needType, int argn = 0)
    {
        if (argn == 0)
            argn = index;
        LuaType typ = lua.Type(index);
        switch (typ)
        {
            case LuaType.LUA_TNONE:
            case LuaType.LUA_TNIL:
                lua.L_ArgCheck(!needType.IsValueType, argn, "need type: " + needType.Name);
                return null;
            case LuaType.LUA_TBOOLEAN:
                lua.L_ArgCheck(needType.IsAssignableFrom(typeof(bool)), argn, "need type: " + needType.Name);
                return lua.ToBoolean(index);
            case LuaType.LUA_TLIGHTUSERDATA:
            case LuaType.LUA_TUSERDATA:
                lua.L_ArgCheck(!needType.IsPrimitive, argn, "need type: " + needType.Name);
                return lua.ToUserData(index);
            case LuaType.LUA_TNUMBER:
                double dv = lua.ToNumber(index);
                if (needType.IsAssignableFrom(typeof(double)))
                    return dv;
                if (needType == typeof(int))
                    return (int)dv;
                if (needType == typeof(uint))
                    return (uint)dv;
                if (needType == typeof(long))
                    return (long)dv;
                if (needType == typeof(byte))
                    return (byte)dv;
                if (needType == typeof(short))
                    return (short)dv;
                if (needType == typeof(float))
                    return (float)dv;
                if (needType == typeof(string))
                    return dv.ToString();
                if (needType.IsEnum)
                    return Enum.ToObject(needType, (int)dv);
                return lua.L_ArgError(argn, "not support c# type: " + needType.Name);
            case LuaType.LUA_TSTRING:
                string sv = lua.ToString(index);
                if (needType.IsEnum)
                    return Enum.Parse(needType, sv);
                lua.L_ArgCheck(needType.IsAssignableFrom(typeof(string)), argn, "need type: " + needType.Name);
                return sv;
            case LuaType.LUA_TTABLE:
                if (needType == typeof(object))
                    needType = typeof(object[]);
                lua.L_ArgCheck(needType.IsArray, argn, "need type: " + needType.Name);
                Type et = needType.GetElementType();
                int len = lua.L_Len(index);
                int ei = lua.GetTop() + 1;
                Array ary = Array.CreateInstance(et, len);
                for (int i = 0; i < len; i++)
                {
                    lua.RawGetI(index, i + 1);
                    ary.SetValue(GetLuaValue(lua, ei, et, argn), i);
                    lua.Pop(1);
                }
                return ary;
            default:
                return lua.L_ArgError(argn, "not support lua type: " + typ);
        }
    }

    static int lua_member_call(ILuaState lua)
    {
        MethodInfo mi = lua.ToUserData(1) as MethodInfo;
        lua.L_ArgCheck(mi != null, 1, "need c# MethodInfo");
        object obj = lua.ToUserData(2);
        CheckStaticMember(lua, mi.IsStatic, obj, 2);
        object[] args = GetArgs(lua, mi.GetParameters(), 3);
        PPushC(lua, () => mi.Invoke(obj, args));
        return mi.ReturnType == null ? 0 : 1;
    }

    static object[] GetArgs(ILuaState lua, ParameterInfo[] pis, int startIndex)
    {
        object[] args = new object[pis.Length];
        for (int i = 0; i < pis.Length; i++)
        {
            ParameterInfo pi = pis[i];
            int li = startIndex + i;
            if (!pi.IsOptional)
            {
                lua.L_CheckAny(li);
            }
            else if (lua.Type(li) == LuaType.LUA_TNONE)
            {
                args[i] = pi.DefaultValue;
                continue;
            }
            args[i] = GetLuaValue(lua, li, pi.ParameterType);
        }
        return args;
    }

    static int lua_class_type(ILuaState lua)
    {
        object obj = lua.ToUserData(1);
        lua.L_ArgCheck(obj != null, 1, "need c# obj");
        Type typ = obj.GetType();
        lua.PushLightUserData(typ);
        lua.PushString(typ.FullName);
        return 2;
    }

    static int lua_create_obj(ILuaState lua)
    {
        Type typ = lua.ToUserData(1) as Type;
        lua.L_ArgCheck(typ != null, 1, "need c# Type");
        ConstructorInfo[] cis = typ.GetConstructors();
        int idx = lua.L_CheckInteger(2);
        lua.L_ArgCheck(idx >= 1 && idx <= cis.Length, 2, "no such constructor: " + idx);
        ConstructorInfo ci = cis[idx - 1];
        object[] args = GetArgs(lua, ci.GetParameters(), 3);
        PPushC(lua, () => ci.Invoke(args));
        return 1;
    }

    static void ShowException(ILuaState lua, Exception e)
    {
        string msg = "";
        while (e != null)
        {
            msg = string.Format("error: {0}\n{1}\n", e.Message, e.StackTrace) + msg;
            e = e.InnerException;
        }
        lua.L_Error("{0}", msg.TrimEnd());
    }

    static void PCallC(ILuaState lua, Action func)
    {
        try
        { func(); }
        catch (Exception e)
        { ShowException(lua, e); }
    }

    static void PPushC(ILuaState lua, Func<object> func)
    {
        object value = null;
        try
        { value = func(); }
        catch (Exception e)
        { ShowException(lua, e); }
        PushLuaValue(lua, value);
    }
}
