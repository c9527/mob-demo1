﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldFix : MonoBehaviour
{
    private InputField m_input;
    private float m_lastValueChangeTime;
    private string m_lastNotEmptyValue = "";
    private string m_lastValue = "";
    public event Action<string> onEndEdit;
    //public event Action<string> onValueChanged;
    void Awake()
    {
        m_input = GetComponent<InputField>();
        m_input.onEndEdit.AddListener(OnEndEdit);
        m_input.onValueChange.AddListener(OnValueChange);
    }

    void OnEndEdit(string text)
    {
        var textValue = Time.time - m_lastValueChangeTime < 3 * Time.deltaTime ? m_lastNotEmptyValue : m_lastValue;
        m_input.text = m_input.textComponent.text = textValue;
        if (onEndEdit != null)
            onEndEdit(textValue);
    }

    void OnValueChange(string text)
    {
        if (text != "")
            m_lastNotEmptyValue = text;
        m_lastValue = text;
        m_input.text = m_input.textComponent.text = text;
        m_lastValueChangeTime = Time.time;
        //onValueChanged(m_input.text);
    }
}
