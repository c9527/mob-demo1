﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
#if UNITY_IPHONE
using System.Runtime.InteropServices;
#endif

/// <summary>
/// 启动脚本2/3  (GameLoader --> UpdateWorker --> Driver)
/// </summary>
class UpdateWorker : MonoBehaviour
{
    private GameObject m_eventSystemGo;
    private Camera m_uiCamera;
    private GameObject m_rootCanvasGo;
    private Image m_imgUpdateTip;
    private Image m_imgProgress;
    private Image m_imgCircle;
    private Text m_txtTotalDetailProgress;
    private Text m_txtTotalProgress;
    private Text m_txtProgress;
    private Text m_txtMsg;
    private Text m_txtVersion;
    private GameObject m_panelTip;
    private Button m_btnStopDownload;

    private DownloadState m_downloadState;  //用户是否选择下载更新资源文件
    private bool m_stopDownload;            //是否中止下载
    private WWW m_www;
    private string m_wwwError;
    private Dictionary<string, bool> m_failUrlDic = new Dictionary<string, bool>();  //下载失败的url记录，防止多次提交apm 
    private string m_msg;
    private int m_totalProgress;
    private bool m_isDownloading; //正在下载文件
    private int m_downloadFilesMax; //需要下载的文件总数
    private int m_downloadFiles;    //当前已经下载完的文件数
    private int m_downloadKBSizeMax; //需要下载KB数
    private int m_downloadKBSize; //已下载KB数
    private int m_downloadingKBSize; //正在下载的文件的字节数

    private bool m_debuging;       //测试模式中，会暂停流程
    private string m_password = "";       //测试模式密码
    private string m_passwordCRC = "";
    private bool m_debugSkipUpdate;       //跳过更新
    private bool m_useDebugUpdateVersion;   //使用调试更新版本

    private List<WWW> m_uiResList = new List<WWW>(); //保存ui的ab，后面统一清除
    private const string iOSUpdateAddress = "https://itunes.apple.com/cn/app/quan-min-qiang-shen-fps-zi/id1060919838?l=zh&ls=1&mt=8";

    enum DownloadState
    {
        WaitChoosen,
        AgreeDownload,
        DenyDownload
    }

    private void Awake()
    {
        m_stopDownload = false;
        m_downloadState = DownloadState.WaitChoosen;

        //屏幕常亮
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        m_eventSystemGo = new GameObject("EventSystem");
        m_eventSystemGo.AddComponent<StandaloneInputModule>();
        m_eventSystemGo.AddComponent<TouchInputModule>();

        m_uiCamera = new GameObject("UICamera").AddComponent<Camera>();
        m_uiCamera.clearFlags = CameraClearFlags.Depth;
        m_uiCamera.orthographic = true;
		m_uiCamera.clearFlags = CameraClearFlags.SolidColor;
		m_uiCamera.backgroundColor = Color.black;			
        m_uiCamera.nearClipPlane = -100;
        m_uiCamera.farClipPlane = 100;
        m_uiCamera.depth = 10;

		//分辨率适配
        var result = 1f * Screen.width / Screen.height;
		if (result < 1.5f) 
			m_uiCamera.orthographicSize = 15.0f / result;
		else 
			m_uiCamera.orthographicSize = 10;
        m_rootCanvasGo = new GameObject("UGUIRootCanvas");
        m_rootCanvasGo.layer = LayerMask.NameToLayer("UI");
        var canvas = m_rootCanvasGo.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = m_uiCamera;
        canvas.pixelPerfect = true;
        canvas.planeDistance = 0;
        var scaler = m_rootCanvasGo.AddComponent<CanvasScaler>();
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.referenceResolution = new Vector2(960, 640);
		scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        m_rootCanvasGo.AddComponent<GraphicRaycaster>();
    }

    private void Start()
    {
        StartCoroutine(Init());
    }

    // (1/3) 启动UI，读取信息，在显示完UI前，不允许访问网络资源，防止出现网络问题时无法弹窗提示
    private IEnumerator Init()
    {
        #region 显示一张背景图
        var www = BeginLoadResource("Atlas/UpdateWorkerBg.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1001, www.error);
        var ab = www.assetBundle;

        www = BeginLoadResource("UI/Update/PanelUpdateBg.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1001, www.error);
        var prefabBg = www.assetBundle.Load("PanelUpdateBg");
        if (prefabBg)
        {
            var panelBg = Instantiate(prefabBg) as GameObject;
            if (panelBg)
                panelBg.transform.SetParent(m_rootCanvasGo.transform, false);
            else
                SendLogServer(1001, "PanelUpdateBg 实例化失败");
        }
        yield return 0;
        #endregion

        #region 读取本地信息
        m_www = BeginLoadFromApp(InfoDir + "GameDefine.txt");
        yield return m_www;
        while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
        var gameDefineDic = ReadSimpleConfig();
        if (gameDefineDic == null)
            SendLogServer(1002, m_wwwError);
        Global.Dic["UpdateUrl"] = TryGetValue(gameDefineDic, "UpdateUrl");              //更新资源服务器域名
        Global.appVersion = TryGetValue(gameDefineDic, "Version");                      //打包时的版本号
        Global.Dic["LocalBigVersion"] = TryGetValue(gameDefineDic, "BigVersion");       //打包时的大版本号，只有相同大版本号的才执行热更
        Global.gameDefineServerlistUrl = TryGetValue(gameDefineDic, "ServerlistUrl");   //服务器列表地址
        Global.Dic["GameDefineIdentifer"] = TryGetValue(gameDefineDic, "Identifer");    //游戏配置的包名，和真实的可能不一样，比如蜂鸟会修改包名
        Global.Dic["GameDefineSDKName"] = TryGetValue(gameDefineDic, "SDKName");    //SDK名称
        Global.Dic["GameDefineLang"] = TryGetValue(gameDefineDic, "Lang");    //语言类型
        LogDebug("更新地址:" + Global.Dic["UpdateUrl"]);
        LogDebug("包内版本号:" + Global.appVersion);
        LogDebug("包内大包版本号:" + Global.Dic["LocalBigVersion"]);
        LogDebug("服务器列表地址:" + Global.gameDefineServerlistUrl);

        

        if (SdExists(InfoDir + "AppVersion.txt"))
        {
            m_www = BeginLoadFromSd(InfoDir + "AppVersion.txt");
            yield return m_www;
            while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
            var appVersionDic = ReadSimpleConfig();
            if (appVersionDic == null || appVersionDic.Count == 0)
            {
                SendLogServer(1003, m_wwwError);
                yield break;
            }
            var sdAppVersion = TryGetValue(appVersionDic, "Version"); //sd卡上的资源对应的版本号
            Global.Dic["SdAppVersion"] = sdAppVersion;
            LogDebug("资源版本版本号:" + sdAppVersion);

            //判断app包是否变更，如果有变更，有可能是覆盖安装了新版本，安全起见会清空sd卡的内容，重新走更新流程
            if ((string.IsNullOrEmpty(sdAppVersion) || sdAppVersion != Global.appVersion))
                SdClear();
        }
        else
        {
            LogDebug("资源版本版本号:首次打开");
            SdClear();
        }

        if (SdExists(InfoDir + "CurrentVersion.txt"))
        {
            m_www = BeginLoadFromSd(InfoDir + "CurrentVersion.txt");
            yield return m_www;
            while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
            var currentVersionDic = ReadSimpleConfig();
            if (currentVersionDic == null || currentVersionDic.Count == 0)
            {
                SendLogServer(1004, m_wwwError);
                yield break;
            }
            Global.currentVersion = TryGetValue(currentVersionDic, "Version"); //当前版本号
            LogDebug("当前版本号:" + Global.currentVersion);
        }
        else
        {
            Global.currentVersion = Global.appVersion;
            LogDebug("当前版本号:首次打开," + Global.currentVersion);
        }

        var outsideDic = new Dictionary<string, string>();
        m_www = SdExists(InfoDir + "FileList.txt") ? BeginLoadFromSd(InfoDir + "FileList.txt") : BeginLoadFromApp(InfoDir + "FileList.txt");
        yield return m_www;
        while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
        Global.Dic["LocalVersionDic"] = ReadSimpleConfig(outsideDic);   //本地FileList版本号字典
        Global.Dic["OutsideDic"] = outsideDic;    //本地包外资源字典
        if (Global.Dic["LocalVersionDic"] == null)
            SendLogServer(1005, m_wwwError);

        m_www = SdExists(InfoDir + "FileSize.txt") ? BeginLoadFromSd(InfoDir + "FileSize.txt") : BeginLoadFromApp(InfoDir + "FileSize.txt");
        yield return m_www;
        while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
        Global.Dic["LocalFileSizeDic"] = ReadSimpleConfig();   //本地FileSize字典
        if (Global.Dic["LocalFileSizeDic"] == null)
            SendLogServer(1006, m_wwwError);
        #endregion

        #region 显示其余的更新UI
        www = BeginLoadResource("Atlas/UpdateWorker.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1007, www.error);
        ab = www.assetBundle;

#if UNITY_ANDROID || UNITY_IPHONE
        www = BeginLoadResource("AtlasAlpha/UpdateWorker_a.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1007, www.error);
        var atlasAlpha = www.assetBundle.mainAsset as Texture2D;

        www = BeginLoadResource("Shader/SpriteETC.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1007, www.error);
        var etcShader = www.assetBundle.mainAsset as Shader;
#endif

        www = BeginLoadResource("Font/Normal.assetbundle",true);
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1007, www.error);
        ab = www.assetBundle;

        www = BeginLoadResource("UI/Update/PanelUpdate.assetbundle");
        m_uiResList.Add(www);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            SendLogServer(1007, www.error);
        var prefab = www.assetBundle.Load("PanelUpdate");
        if (prefab)
        {
            var panel = Instantiate(prefab) as GameObject;
            if (panel)
            {
                var tran = panel.transform;
                try
                {
#if UNITY_ANDROID || UNITY_IPHONE
                    DealImageEtc(panel, etcShader, atlasAlpha);
#endif
                    
                    tran.SetParent(m_rootCanvasGo.transform, false);
                    m_imgUpdateTip = tran.Find("imgUpdateTip").GetComponent<Image>();
                    m_imgProgress = tran.Find("imgPgBar/imgProgress").GetComponent<Image>();
                    m_imgCircle = tran.Find("imgCircle").GetComponent<Image>();
                    m_txtProgress = tran.Find("imgPgBar/txtProgress").GetComponent<Text>();
                    m_txtMsg = tran.Find("txtMsg").GetComponent<Text>();
                    m_txtTotalProgress = tran.Find("txtTotalProgress").GetComponent<Text>();
                    m_txtTotalDetailProgress = tran.Find("txtTotalDetailProgress").GetComponent<Text>();
                    m_txtVersion = tran.Find("txtVersion").GetComponent<Text>();
                    m_panelTip = tran.Find("PanelTip").gameObject;
                }
                catch(Exception e)
                {
                    SendLogServer(1007, e.Message);
                }

                m_btnStopDownload = tran.Find("btnStopDownload").GetComponent<Button>();
                if (m_btnStopDownload)
                {
                    m_btnStopDownload.gameObject.SetActive(false);
                    m_btnStopDownload.onClick.AddListener(() =>
                    {
                        m_btnStopDownload.gameObject.SetActive(false);
                        m_stopDownload = true;
                        LogUser("用户终止下载");
                        if (m_www != null)
                        {
                            m_www.Dispose();
                            m_www = null;
                        }
                    });
                }
            }
            else
                SendLogServer(1007, "PanelUpdate 实例化失败");
        }
        else
            SendLogServer(1007, "PanelUpdate 为空");

        if (m_txtVersion)
            m_txtVersion.text = "当前版本：" + Global.currentVersion;
        #endregion

        #region 卸载UI的AssetBundle
        for (int i = 0; i < m_uiResList.Count; i++)
        {
            m_uiResList[i].assetBundle.Unload(false);
            m_uiResList[i].Dispose();
        }
        m_uiResList.Clear();
        #endregion

        if (Input.touchCount >= 5 || (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.G)))
            m_debuging = true;
        else
            StartCoroutine(BeginCheckServerVersion());
    }

    // (2/3) 服务端资源版本检查，执行更新逻辑
    private IEnumerator BeginCheckServerVersion()
    {
        #region 读取在线版本信息
        var updateUrl = Global.Dic["UpdateUrl"].ToString();
        do
        {
            if (m_debugSkipUpdate || string.IsNullOrEmpty(updateUrl))
            {
                Global.Dic["ServerVersion"] = Global.currentVersion;
                Global.Dic["ServerBigVersion"] = Global.Dic["LocalBigVersion"];
                break;
            }

            Dictionary<string, string> versionDic = null;
            LogUser("检查版本更新...");
            var enumerator = BeginSafeDownload(updateUrl + "Version.txt?id=" + GetTimeStamp(), ReadSimpleConfig, 1008);
            while (versionDic == null)
            {
                enumerator.MoveNext();
                versionDic = enumerator.Current as Dictionary<string, string>;
                yield return null;
            }

            Global.Dic["ServerVersion"] = TryGetValue(versionDic, m_useDebugUpdateVersion ? "PrepareVersion" : "Version");
            Global.Dic["ServerBigVersion"] = TryGetValue(versionDic, m_useDebugUpdateVersion ? "PrepareBigVersion" : "BigVersion");

        } while (false);
        LogDebug("最新版本号：" + Global.Dic["ServerVersion"]);
        LogDebug("最新大包版本号：" + Global.Dic["ServerBigVersion"]);
        #endregion

        #region 判断是否需要更新，是否需要下载FileList
#if UNITY_WEBPLAYER
        Global.currentVersion = Global.Dic["ServerVersion"].ToString();
        if (m_txtVersion)
            m_txtVersion.text = "当前版本：" + Global.currentVersion;
        LogDebug("Web平台更改当前版本号：" + Global.currentVersion);

        m_www = BeginLoadFromApp(InfoDir + "FileList.txt");
        yield return m_www;
        while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
        Global.Dic["ServerVersionDic"] = ReadSimpleConfig();
#elif UNITY_STANDALONE
        Dictionary<string, string> svrFileListDic = null;
        var serverVersion = Global.Dic["ServerVersion"].ToString();
        {
            var enumerator = BeginSafeDownload(updateUrl + serverVersion + "/" + InfoDir + "FileList.txt?id=" + GetTimeStamp(), ReadSimpleConfig);
            while (svrFileListDic == null)
            {
                enumerator.MoveNext();
                svrFileListDic = enumerator.Current as Dictionary<string, string>;
                yield return null;
            }
        }
        //保存服务器上的文件列表字典
        Global.Dic["ServerVersionDic"] = svrFileListDic;
#else
        //检查安卓ios整包版本号，对于包内版本号大于最新版本号的，或包内版本号在VersionCheckList表中，则跳过更新（或指定版本号）
        {
            var serverlistDic = new Dictionary<string, string>();
            Dictionary<string, string> versionCheckDic = null;
            var enumerator = BeginSafeDownload(updateUrl + "VersionCheckList.txt?id=" + GetTimeStamp(), ReadSimpleConfig, 1009, null, serverlistDic);
            while (versionCheckDic == null)
            {
                enumerator.MoveNext();
                versionCheckDic = enumerator.Current as Dictionary<string, string>;
                yield return null;
            }
            Global.Dic["VersionCheckDic"] = versionCheckDic;
            Global.Dic["VersionCheckServerlistDic"] = serverlistDic;
            if (!(m_useDebugUpdateVersion || m_debugSkipUpdate))
            {
                if (versionCheckDic.ContainsKey(Global.appVersion) && !string.IsNullOrEmpty(versionCheckDic[Global.appVersion]))
                {
                    Global.Dic["ServerVersion"] = versionCheckDic[Global.appVersion];
                    Global.Dic["ServerBigVersion"] = Global.Dic["LocalBigVersion"];
                    LogDebug("VersionCheckList匹配：包内" + Global.appVersion+"-->"+versionCheckDic[Global.appVersion]);
                }
                else if (Global.appVersion.CompareTo(Global.Dic["ServerVersion"]) > 0)
                {
                    Global.Dic["ServerVersion"] = Global.currentVersion;
                    Global.Dic["ServerBigVersion"] = Global.Dic["LocalBigVersion"];
                    LogDebug("包内版本号大于最新版本号：ServerVersion改为" + Global.currentVersion);
                }
            }
            
        }

        if (NeedUpdate())
        {
            var localVersionDic = Global.Dic["LocalVersionDic"] as Dictionary<string, string>;
            var localOutSideDic = Global.Dic["OutsideDic"] as Dictionary<string, string>;
            var serverVersion = Global.Dic["ServerVersion"].ToString();
            var outsideDic = new Dictionary<string, string>();
            Dictionary<string, string> svrFileListDic = null;

            var enumerator = BeginSafeDownload(updateUrl + serverVersion + "/" + InfoDir + "FileList.txt?id=" + GetTimeStamp(), ReadSimpleConfig, 1010, null, outsideDic);
            while (svrFileListDic == null)
            {
                enumerator.MoveNext();
                svrFileListDic = enumerator.Current as Dictionary<string, string>;
                yield return null;
            }

#if !UNITY_WEBPLAYER
            //当更新时原本是包外的资源变为了包内资源，则检查sd卡上是否存在该资源，有则更新他的本地crc值，没有则删除
            foreach (var kv in localOutSideDic)
            {
                if (!outsideDic.ContainsKey(kv.Key))
                {
                    if (SdExists(ResDir + kv.Key))
                        localVersionDic[kv.Key] = CRC32String(File.ReadAllBytes(SdRootPath + ResDir + kv.Key));
                    else
                        localVersionDic.Remove(kv.Key);
                }
            }
#endif
            //缓存服务器上的文件列表字典
            Global.Dic["ServerVersionDic"] = svrFileListDic;
            Global.Dic["OutsideDic"] = outsideDic;

            Dictionary<string, string> fileSizeDic = null;
            enumerator = BeginSafeDownload(updateUrl + serverVersion + "/" + InfoDir + "FileSize.txt?id=" + GetTimeStamp(), ReadSimpleConfig, 1011);
            while (fileSizeDic == null)
            {
                enumerator.MoveNext();
                fileSizeDic = enumerator.Current as Dictionary<string, string>;
                yield return null;
            }
            //缓存服务器上的文件大小列表字典
            Global.Dic["ServerFileSizeDic"] = fileSizeDic;
        }
        else
        {
            //无需更新时，克隆一份本地的FileList作为服务器端的
            Global.Dic["ServerVersionDic"] = new Dictionary<string, string>(Global.Dic["LocalVersionDic"] as Dictionary<string, string>);
            Global.Dic["ServerFileSizeDic"] = new Dictionary<string, string>(Global.Dic["LocalFileSizeDic"] as Dictionary<string, string>);
        }
#endif
        #endregion

#if UNITY_ANDROID && ActiveSDK
        FnSdkCheckUpdate();
#elif UNITY_WEBPLAYER
        BeginClient();
#elif UNITY_STANDALONE
        PcCheckUpdate();
#else
        StartCoroutine(ResourceCheckUpdate());
#endif
    }

    // (3/3) 启动主程序
    private void BeginClient()
    {
#if UNITY_IPHONE || UNITY_WEBPLAYER
        SceneClientStart();
#else
        StartCoroutine(DllClientStart());
#endif
    }

    #region 游戏自身热更新逻辑
    // 比较版本号判断是否需要更新
    private bool NeedUpdate()
    {
        var serverVersion = Global.Dic["ServerVersion"].ToString();
        if (serverVersion.EndsWith("999"))  //.999结尾的版本号专门用来做测试版本，这样使得连续发测试版本时使用相同资源目录，提高发布速度
            return true;
        return !NeedPackageUpdate() && Global.currentVersion != serverVersion && !string.IsNullOrEmpty(serverVersion);
    }

    //是否需要整包更新
    private bool NeedPackageUpdate()
    {
        var appBigVersion = Global.Dic["LocalBigVersion"].ToString();
        var serverBigVersion = Global.Dic["ServerBigVersion"].ToString();
        return !string.IsNullOrEmpty(appBigVersion) && !string.IsNullOrEmpty(serverBigVersion) && appBigVersion != serverBigVersion;
    }

    // 资源更新逻辑
    private IEnumerator ResourceCheckUpdate(Dictionary<string, string> serverVersionDic = null)
    {


        m_totalProgress = 0;
        //获取变量
        var updateUrl = Global.Dic["UpdateUrl"].ToString();
        var localVersionDic = Global.Dic["LocalVersionDic"] as Dictionary<string, string>;
        var outsideDic = Global.Dic.ContainsKey("OutsideDic") ? Global.Dic["OutsideDic"]as Dictionary<string, string> : null;
        var serverVersion = Global.Dic["ServerVersion"].ToString();
        if (serverVersionDic == null)
            serverVersionDic = Global.Dic["ServerVersionDic"] as Dictionary<string, string>;
        
#if !UNITY_WEBPLAYER
        try
        {
            SdCreateDirectory(InfoDir);
            File.WriteAllText(SdRootPath + InfoDir + "AppVersion.txt", "Version=" + Global.appVersion);
        }
        catch (Exception e)
        {
            SendLogServerUnique(1012, e.Message);
        }
#endif
        if (NeedUpdate())
        {
            //服务器上的文件大小
            m_totalProgress = 8;
            Dictionary<string, string> fileSizeDic = null;
            if (!Global.Dic.ContainsKey("ServerFileSizeDic"))
            {
                var enumerator = BeginSafeDownload(updateUrl + serverVersion + "/" + InfoDir + "FileSize.txt?id=" + GetTimeStamp(), ReadSimpleConfig, 1011);
                while (fileSizeDic == null)
                {
                    enumerator.MoveNext();
                    fileSizeDic = enumerator.Current as Dictionary<string, string>;
                    yield return null;
                }
                Global.Dic["ServerFileSizeDic"] = fileSizeDic;
            }
            else
                fileSizeDic = Global.Dic["ServerFileSizeDic"] as Dictionary<string, string>;

            //获取变更文件列表，准备下载
            m_totalProgress = 9;
            var downloadFileList = new List<string>();
            var changedFileList = new List<string>();
            foreach (var kv in serverVersionDic)
            {
                //跳过指定为包外资源的下载更新
                if (outsideDic != null && outsideDic.ContainsKey(kv.Key))
                    continue;

                string localFileCrc = localVersionDic.ContainsKey(kv.Key) ? localVersionDic[kv.Key] : null;

                //本地文件和服务器上的文件crc相同，跳过下载
                if (localFileCrc == kv.Value)
                    continue;
                changedFileList.Add(kv.Key);

#if !UNITY_WEBPLAYER
                //如果下载目录已有同名文件且crc相同，跳过下载
                if (SdExists(DownloadDir + kv.Key) && CRC32String(File.ReadAllBytes(SdRootPath + DownloadDir + kv.Key)) == kv.Value)
                    continue;
#endif
                downloadFileList.Add(kv.Key);
            }

            //计算需要下载文件的总大小
            m_totalProgress = 10;
            var totalSize = 0;
            for (int i = 0; i < downloadFileList.Count; i++)
            {
                int size = 0;
                if (fileSizeDic.ContainsKey(downloadFileList[i]))
                    int.TryParse(fileSizeDic[downloadFileList[i]], out size);
                totalSize += size;
            }
            float downloadSize = Mathf.Max(1, totalSize/1024);
            var sizeUnit = "KB";
            if (downloadSize >= 1024)
            {
                downloadSize = downloadSize/1024f;
                sizeUnit = "MB";
            }

            //判断网络环境
            var networkTip = "";
            if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
                networkTip = "当前网络为本地网络（WiFi）"+"\n";
            if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
                networkTip = "当前网络为移动网络（2G/3G/4G），确定下载更新资源将产生流量消耗，土豪请无视"+"\n";

            var quiteUpdate = false;
            //大于2M的更新包询问玩家是否要下载
            if (totalSize > 2048*1024)
            {
                LogUser("获取更新日志");
                m_www = BeginDownload(updateUrl + serverVersion + "/" + "UpdateLog.txt?id=" + GetTimeStamp());
                yield return m_www;
                while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
                var updateLog = "";
                if (m_www.assetBundle != null && m_www.assetBundle.mainAsset is TextAsset)
                    updateLog = (m_www.assetBundle.mainAsset as TextAsset).text;
                else if (!string.IsNullOrEmpty(m_www.text))
                    updateLog = m_www.text;
                if (m_www.assetBundle != null)
                    m_www.assetBundle.Unload(false);
                m_www.Dispose();
                m_www = null;

                LogUser("确认下载");
                ShowTipPanel(string.Format(networkTip + "最新版本为" + serverVersion + "，需要下载约{0}{1}\n\n{2}", downloadSize.ToString("0.##"), sizeUnit, updateLog),
                    () => { m_downloadState = DownloadState.AgreeDownload; m_panelTip.SetActive(false); },
                    () => { m_downloadState = DownloadState.DenyDownload; m_panelTip.SetActive(false); });
                while (m_downloadState == DownloadState.WaitChoosen)
                    yield return null;
                if (m_downloadState == DownloadState.DenyDownload)
                {
                    LogUser("用户取消下载");
                    Application.Quit();
                    yield break;
                }
            }
            else
            {
                LogDebug("更新包小于1024K，开始静默更新");
                m_downloadState = DownloadState.AgreeDownload;
                quiteUpdate = true;
            }

            if (!quiteUpdate)
            {
                if (m_imgUpdateTip)
                    m_imgUpdateTip.gameObject.SetActive(true);
            }
            //单线程下载，后面再扩展同时下载和失败重试
            LogUser("开始下载...");
            m_isDownloading = true;
            m_downloadKBSizeMax = totalSize / 1024;
//            if (m_btnStopDownload != null)
//                m_btnStopDownload.gameObject.SetActive(true);
            m_totalProgress = 10;
            m_downloadKBSize = 0;
            m_downloadFiles = 0;
            m_downloadFilesMax = downloadFileList.Count;
            LogDebug("待下载数量:" + m_downloadFilesMax);
            for (int i = 0; i < downloadFileList.Count; i++)
            {
                if (fileSizeDic.ContainsKey(downloadFileList[i]))
                    int.TryParse(fileSizeDic[downloadFileList[i]], out m_downloadingKBSize);
                m_downloadingKBSize /= 1024;

                byte[] fileBytes = null;
                var fileEnumerator = BeginSafeDownload(updateUrl + serverVersion + "/" + downloadFileList[i], ReadBytes, 1013, serverVersionDic[downloadFileList[i]]);
                while (fileBytes == null)
                {
                    fileEnumerator.MoveNext();
                    fileBytes = fileEnumerator.Current as byte[];
                    yield return null;
                }

                //保存到下载目录
                SdSaveFile(DownloadDir + downloadFileList[i], fileBytes);
                m_downloadFiles++;

                int size = 0;
                if (fileSizeDic.ContainsKey(downloadFileList[i]))
                    int.TryParse(fileSizeDic[downloadFileList[i]], out size);
                m_downloadKBSize += size/1024; 
            }
            m_totalProgress = 98;
            if (m_btnStopDownload != null)
                m_btnStopDownload.gameObject.SetActive(false);
            m_downloadingKBSize = 0;
            m_isDownloading = false;
            LogUser("下载完毕");

            #region 资源下载完毕后，拷贝资源到正式目录，写入新的版本、文件列表、清理临时下载目录和旧资源等操作
            LogUser("整合资源");
            try
            {
                for (int i = 0; i < changedFileList.Count; i++)
                {
                    var sourcePath = SdRootPath + DownloadDir + changedFileList[i];
                    var targetPath = SdRootPath + ResDir + changedFileList[i];
                    SdCreateDirectory(ResDir + changedFileList[i]);

                    if (File.Exists(sourcePath))
                    {
                        File.Copy(sourcePath, targetPath, true);
                        LogDebug("资源整合", "拷贝" + sourcePath + "到" + targetPath);
                    }
                    else
                        LogDebug("资源不存在", sourcePath);
                }
            }
            catch (Exception e)
            {
                SendLogServerUnique(1015, e.Message);
            }

            //保存新的文件列表文件
            LogUser("保存文件列表", SdRootPath + InfoDir + "FileList.txt");
            var newFileList = new StringBuilder();
            foreach (var kv in serverVersionDic)
            {
                //为了支持地图下载，这里不能直接保存最新的FileList到本地
                if (outsideDic.ContainsKey(kv.Key))
                    newFileList.AppendLine(kv.Key + "=" + kv.Value + "=outside");
                else
                    newFileList.AppendLine(kv.Key + "=" + kv.Value);
            }

            var newFileSize = new StringBuilder();
            foreach (var kv in fileSizeDic)
            {
                newFileSize.AppendLine(kv.Key + "=" + kv.Value);
            }

            try
            {
#if !UNITY_WEBPLAYER && !UNITY_STANDALONE
                File.WriteAllText(SdRootPath + InfoDir + "FileList.txt", newFileList.ToString());
                File.WriteAllText(SdRootPath + InfoDir + "FileSize.txt", newFileSize.ToString());
#endif

                //保存当前版本号文件
                LogUser("保存版本号", serverVersion);
                Global.currentVersion = serverVersion;
#if !UNITY_WEBPLAYER
                File.WriteAllText(SdRootPath + InfoDir + "CurrentVersion.txt", "Version=" + serverVersion);
#endif
            }
            catch (Exception e)
            {
                SendLogServerUnique(1016, e.Message);
            }

            try
            {
                //删除临时下载目录
                LogUser("删除临时下载目录");
                SdDeleteDirectory(DownloadDir);

                //删除过期资源
                foreach (var oldFile in localVersionDic.Keys)
                {
                    if (!serverVersionDic.ContainsKey(oldFile) && SdExists(ResDir + oldFile))
                    {
                        File.Delete(SdRootPath + ResDir + oldFile);
                        LogDebug("删除过期资源", SdRootPath + ResDir + oldFile);
                    }
                }
            }
            catch (Exception e)
            {
                SendLogServerUnique(1017, e.Message);
            }
            

            //更新完毕后，本地的FileList就是最新的FileList
            Global.Dic["LocalVersionDic"] = new Dictionary<string, string>(serverVersionDic);
            Global.Dic["LocalFileSizeDic"] = new Dictionary<string, string>(fileSizeDic);
            #endregion
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer && NeedPackageUpdate())
        {
            LogUser("等待整包更新确认");
            m_www = BeginDownload(updateUrl + serverVersion + "/" + "UpdateLog.txt?id=" + GetTimeStamp());
            yield return m_www;
            while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
            var updateLog = "";
            if (m_www.assetBundle != null && m_www.assetBundle.mainAsset is TextAsset)
                updateLog = (m_www.assetBundle.mainAsset as TextAsset).text;
            else if (!string.IsNullOrEmpty(m_www.text))
                updateLog = m_www.text;
            if (m_www.assetBundle != null)
                m_www.assetBundle.Unload(false);
            m_www.Dispose();
            m_www = null;

            m_downloadState = DownloadState.WaitChoosen;
            ShowTipPanel(string.Format("最新版本为{0}，点击更新前往AppStore下载\n\n{1}", serverVersion, updateLog),
                () => { appGotoUpdate(iOSUpdateAddress); }, null);
            while (m_downloadState == DownloadState.WaitChoosen)
                yield return null;
        }
        else if (string.IsNullOrEmpty(serverVersion))
        {
            LogUser("获取最新版本号出错");
            yield break;
        }
        else
            LogUser("已经是最新版本");

        m_totalProgress = 99;

        //启动游戏
        BeginClient();
    }

    // 启动Client.dll，对应代码可热更平台
    private IEnumerator DllClientStart()
    {
        var www = BeginLoadResource("Managed/Client.dll.bytes");
        yield return www;
        while (!www.isDone && string.IsNullOrEmpty(www.error))
            yield return null;
        byte[] clientBytes;
        if (www.assetBundle != null && www.assetBundle.mainAsset is TextAsset)
            clientBytes = (www.assetBundle.mainAsset as TextAsset).bytes;
        else
            clientBytes = www.bytes;
        LogDebug("主程序集加载完毕：" + CRC32String(clientBytes));
        var assembly = Assembly.Load(clientBytes);
        if (assembly != null)
        {
            var driver = assembly.GetType("Driver", true);
            var go = new GameObject("Driver");
            go.AddComponent(driver);
            DestorySelf();
            m_totalProgress = 100;
            LogUser("游戏加载启动中,请稍后...");
            if (www.assetBundle != null)
                www.assetBundle.Unload(false);
        }
        else
        {
            LogError("主程序集解析失败", "");
            SendLogServer(1018, "主程序集解析失败 " + CRC32String(clientBytes));
        }
    }

    // 启动Driver场景，对应代码不可热更的平台
    private void SceneClientStart()
    {
#if UNITY_WEBPLAYER && DebugErrorMode
        Global.useDebugServerlistUrl = true;
#endif
        LogUser("游戏启动中,请稍后...");
        Application.LoadLevelAdditive("Driver");
        DestorySelf();
    }
    #endregion

    #region 蜂鸟更新逻辑
    // 蜂鸟更新逻辑
    private void FnSdkCheckUpdate()
    {
#if ActiveSDK && UNITY_ANDROID 
        var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        activity.Call("setAndroidCallbackUnityObjectName", gameObject.name);
        activity.Call("setDebug", true.ToString());
        activity.Call("checkUpdate");
#if LANG_RU||LANG_EN
        activity.Call("OpenLog");
        activity.Call("StartLoadingLog");
        StartCoroutine(ResourceCheckUpdate()); 
#endif
#endif
#if ActiveSDK && UNITY_ANDROID && !LANG_RU&&!LANG_EN
        var ajcFn = new AndroidJavaClass("com.ssjj.fnsdk.platform.FNConfig");
        Global.Dic["fn_pid"] = ajcFn.GetStatic<string>("fn_platformId");
        Global.Dic["fn_gameId"] = ajcFn.GetStatic<string>("fn_gameId");
#endif
    }

    // 蜂鸟检查更新回调
    private void AndroidCheckUpdate(string msg)
    {
        LogDebug("【SDK回调】AndroidCheckUpdate", msg);

        switch (msg)
        {
            case "onNotSDCard": break;
            case "onNotNewVersion": break;
            case "onNormalUpdateLoading": break;
            case "onNetWorkError": break;
            case "onForceUpdateLoading": break;
            case "onException": break;
            case "onCheckVersionFailure": break;
            case "onCancelNormalUpdate": break;
        }

        StartCoroutine(ResourceCheckUpdate());
    }
    #endregion

    #region PC更新逻辑
    // PC更新逻辑
    private void PcCheckUpdate()
    {
        var sdAppVersion = Global.Dic.ContainsKey("SdAppVersion") ? Global.Dic["SdAppVersion"].ToString() : null;
        //只更新代码
        var localVersionDic = Global.Dic["LocalVersionDic"] as Dictionary<string, string>;
        var serverVersionDic = Global.Dic["ServerVersionDic"] as Dictionary<string, string>;
        var dic = new Dictionary<string, string>(localVersionDic);
        dic["Managed/Client.dll.bytes"] = serverVersionDic["Managed/Client.dll.bytes"];
        dic["Managed/UpdateWorker.dll.bytes"] = serverVersionDic["Managed/UpdateWorker.dll.bytes"];

#if !UNITY_WEBPLAYER
        //覆盖安装了新的包时，pc版将额外进行资源校验操作
        if ((string.IsNullOrEmpty(sdAppVersion) || sdAppVersion != Global.appVersion))
        {
            LogUser("校验本地资源");
            try
            {
                var arr = new string[0];

                if (Directory.Exists(SdRootPath + ResDir))
                    arr = Directory.GetFiles(SdRootPath + ResDir, "*", SearchOption.AllDirectories);
                for (int i = 0; i < arr.Length; i++)
                {
                    m_msg = "校验本地资源(" + i + "/" + arr.Length + ")";
                    arr[i] = arr[i].Replace('\\', '/');
                    var pathName = arr[i].Replace(SdRootPath + ResDir, "");
                    if (serverVersionDic.ContainsKey(pathName) && serverVersionDic[pathName] == CRC32String(File.ReadAllBytes(arr[i])))
                        continue;
                    File.Delete(arr[i]);
                    LogDebug("校验删除：" + arr[i]);
                }
            }
            catch (Exception e)
            {
                SdClearDirectory(ResDir);
                LogError("本地校验出错，资源全部清除", "", e.ToString());
            }
        }
#endif

        StartCoroutine(ResourceCheckUpdate(dic));
    }
    #endregion

    // 调试模式GUI界面
    private void OnGUI()
    {
        if (!m_debuging)
            return;

        GUI.matrix = Matrix4x4.Scale(new Vector3(Screen.width / 960f, Screen.height / 640f, 1));
        GUILayout.Space(30);
        
        //开启密码
        if (m_passwordCRC != "927F9E58")
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(120);
            if (GUILayout.Button("7", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "7";
            if (GUILayout.Button("8", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "8";
            if (GUILayout.Button("9", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "9";
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(120);
            if (GUILayout.Button("4", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "4";
            if (GUILayout.Button("5", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "5";
            if (GUILayout.Button("6", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "6";
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(120);
            if (GUILayout.Button("1", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "1";
            if (GUILayout.Button("2", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "2";
            if (GUILayout.Button("3", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "3";
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(120);
            if (GUILayout.Button("0", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password += "0";
            if (GUILayout.Button("Clear", new[] { GUILayout.Height(80), GUILayout.Width(80) })) m_password = "";
            if (GUILayout.Button("Enter", new[] {GUILayout.Height(80), GUILayout.Width(80)}))
            {
                m_passwordCRC = CRC32String(Encoding.UTF8.GetBytes(m_password));
                m_password = "";
            }
            GUILayout.EndHorizontal();
            return;
        }

        //显示调试菜单
        GUILayout.BeginHorizontal();
        GUILayout.Space(120);
        GUILayout.BeginVertical();
        if (GUILayout.Button("退出调试模式", GUILayout.Height(80)))
        {
            m_debuging = false;
            StartCoroutine(BeginCheckServerVersion());
        }
        if (GUILayout.Button("跳过更新", GUILayout.Height(80)))
            m_debugSkipUpdate = true;
        if (GUILayout.Button("清空SD卡", GUILayout.Height(80)))
            SdClearDirectory();
        if (GUILayout.Button("使用测试更新地址", GUILayout.Height(80)))
            m_useDebugUpdateVersion = true;
        if (GUILayout.Button("使用测试服务器列表", GUILayout.Height(80)))
            Global.useDebugServerlistUrl = true;
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }

    private void Update()
    {
        var progress = m_www != null ? m_www.progress : 1;

        int totalDownloaded = m_downloadKBSize + (int)(m_downloadingKBSize * progress);
        int totalLoading = m_totalProgress + (m_isDownloading ? 88 * totalDownloaded / m_downloadKBSizeMax : 0);

        if (m_txtTotalDetailProgress != null)
            m_txtTotalDetailProgress.text = string.Format("下载进度：{0}KB/{1}KB  ({2}/{3})", totalDownloaded, m_downloadKBSizeMax, m_downloadFiles, m_downloadFilesMax);
        if (m_txtTotalProgress != null)
            m_txtTotalProgress.text = totalLoading + "%";
        if (m_imgProgress != null)
            m_imgProgress.fillAmount = progress;
        if (m_txtProgress != null)
            m_txtProgress.text = (int)(progress * 100) + "%";
        if (m_txtMsg != null)
            m_txtMsg.text = m_msg;

        if (m_imgCircle != null && m_downloadKBSize + m_downloadingKBSize > 0)
        {
            m_imgCircle.rectTransform.Rotate(0, 0, -360 * Time.deltaTime, Space.Self);
        }
    }

    // 手动清除场景
    private void DestorySelf()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        const int delayTime = 10;
#else
        const int delayTime = 1;
#endif
        if (m_eventSystemGo != null)
            Destroy(m_eventSystemGo);
        if (m_uiCamera.gameObject != null)
            Destroy(m_uiCamera.gameObject, delayTime);
        if (m_rootCanvasGo != null)
            Destroy(m_rootCanvasGo, delayTime);

        Destroy(gameObject, delayTime);
    }

    // 提示对话框
    private bool ShowTipPanel(string content, UnityAction okCallback, UnityAction cancelCallback, bool showCancel = false, string ok = "", string cancel = "")
    {
        if (m_panelTip == null)
            return false;

        m_panelTip.SetActive(true);
        var tran = m_panelTip.transform;
        var tranText = tran.Find("background/Scroll/Text");
        if (tranText != null)
        {
            var text = tranText.GetComponent<Text>();
            if (text != null)
                text.text = content;
        }

        var tranBtnOk = tran.Find("background/btnOk");
        if (tranBtnOk != null)
        {
            var btnOk = tranBtnOk.GetComponent<Button>();
            if (btnOk != null)
            {
                if (okCallback != null)
                    btnOk.onClick.AddListener(okCallback);
                if (!string.IsNullOrEmpty(ok))
                    tranBtnOk.Find("Text").GetComponent<Text>().text = ok;
            }
        }

        var tranBtnCancel = tran.Find("background/btnCancel");
        if (tranBtnCancel != null)
        {
            var btnCancel = tranBtnCancel.GetComponent<Button>();
            if (btnCancel != null)
            {
                if (cancelCallback != null)
                    btnCancel.onClick.AddListener(cancelCallback);
                if (!string.IsNullOrEmpty(cancel))
                    tranBtnCancel.Find("Text").GetComponent<Text>().text = cancel;
                btnCancel.gameObject.SetActive(showCancel);
            }
        }
        return true;
    }

    #region www加载和读取
    private WWW BeginDownload(string url)
    {
        LogDebug("开始下载：" + url);
        return new WWW(url);
    }

    private IEnumerator BeginSafeDownload(string url, Func<Dictionary<string, string>, object> readFunc, int errorCode = 0, string crc = null, Dictionary<string, string> dic2 = null)
    {
        LogDebug("开始安全下载：" + url);
        object readObject = null;
        for (int i = 0; i < 3; i++) //自动重试3次
        {
            m_www = new WWW(url);
            yield return m_www;
            while (!m_www.isDone && string.IsNullOrEmpty(m_www.error)) yield return null;
            readObject = readFunc(dic2);
            if (readObject != null)
            {
                if (crc != null && readObject is byte[])
                {
                    var newCrc = CRC32String((byte[]) readObject);
                    if (crc == newCrc)
                        break;
                    LogDebug(string.Format("校验失败{0} SerCRC:{1} fileCRC:{2} {3}", (i + 1), crc, newCrc, url));
//                    if (errorCode != 0)
//                        SendLogServer(errorCode + 3, string.Format("校验失败{0} SerCRC:{1} fileCRC:{2} {3}", (i + 1), crc, newCrc, url));
                }
                else
                    break;
            }
            else
            {
                LogDebug("下载失败" + (i + 1) + " " + url);
                try
                {
                    var host = new Uri(url).Host;
                    var ips = Dns.GetHostAddresses(host);
                    var ipArr = new string[ips.Length];
                    for (int j = 0; j < ipArr.Length; j++)
                    {
                        ipArr[j] = ips[j].ToString();
                    }
//                    SendLogServer(errorCode + 1, string.Format("下载失败，url:{0}，域名{1}解析为{2}", url, host, string.Join(";", ipArr)));
                }
                catch
                {
//                    SendLogServer(errorCode + 2, string.Format("解析地址{0}出错:\n{1}", url, StackTraceUtility.ExtractStringFromException(e)));
                }
            }

            if (i < 2)
                continue;
            m_downloadState = DownloadState.WaitChoosen;
            if (errorCode != 0 && !m_failUrlDic.ContainsKey(url))
            {
                SendLogServer(errorCode, "文件多次下载失败 " + url + " " + m_www.error);
                m_failUrlDic.Add(url, true);
            }
            ShowTipPanel("文件多次下载失败，请检查网络连接是否稳定，是否继续重试？",
                () => { m_downloadState = DownloadState.AgreeDownload; m_panelTip.SetActive(false); },
                () => { m_downloadState = DownloadState.DenyDownload; m_panelTip.SetActive(false); }, false, "重试", "退出");
            while (m_downloadState == DownloadState.WaitChoosen)
                yield return null;
            if (m_downloadState == DownloadState.AgreeDownload)
                i = -1;
            else if (m_downloadState == DownloadState.DenyDownload)
                Application.Quit();
        }

        yield return readObject;
    }

    /// <summary>
    /// 先读取sd卡上Resources目录下的资源，如果没有则读取app包内的
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private WWW BeginLoadResource(string path, bool no_version=false)
    {
        if (path.IndexOf('\\') != -1)
            path = path.Replace(@"\", "/");

        if (SdExists(ResDir + path))
            return BeginLoadFromSd(ResDir + path);
        else
            return BeginLoadFromApp(path, no_version);
    }

    /// <summary>
    /// 仅从app包内读取资源
    /// </summary>
    /// <param name="path"></param>
    /// <returns>如果出错则返回null</returns>
    private WWW BeginLoadFromApp(string path,bool no_version=false)
    {
        if (path.IndexOf('\\') != -1)
            path = path.Replace(@"\", "/");

        var wwwPath = AppRootPathWww + path + (Application.isWebPlayer && no_version==false ? "?v=" + GetTimeStamp() : "");
        LogDebug("开始包内加载", wwwPath);
        return new WWW(wwwPath);
    }

    /// <summary>
    /// 仅从sd卡上读取资源
    /// </summary>
    /// <param name="path"></param>
    /// <returns>如果出错则返回null</returns>
    private WWW BeginLoadFromSd(string path)
    {
        if (path.IndexOf('\\') != -1)
            path = path.Replace(@"\", "/");

        var wwwPath = SdRootPathWww + path;
        LogDebug("开始SD加载", wwwPath);
        return new WWW(wwwPath);
    }

    /// <summary>
    /// 读取bytes数组，并销毁www
    /// </summary>
    /// <returns>如果出错则返回null</returns>
    private byte[] ReadBytes(Dictionary<string, string> tmp)
    {
        if (m_www == null)
            return null;
        byte[] bytes;
        if (!string.IsNullOrEmpty(m_www.error) || !m_www.isDone)
        {
            LogError("加载失败", m_www.url, m_www.error);
            bytes = null;
        }
        else
        {
//            LogDebug("加载成功", m_www.url + "。文件字节 " + m_www.bytes.Length);
            bytes = m_www.bytes;
        }
        m_www.Dispose();
        m_www = null;

        return bytes;
    }

    /// <summary>
    /// 读取用等号分隔key value，一行一条配置的简易文本配置，返回字典，并销毁www
    /// </summary>
    /// <returns>如果出错则返回null</returns>
    private Dictionary<string, string> ReadSimpleConfig(Dictionary<string, string> dic2 = null)
    {
        var debug = false;
        if (m_www == null)
            return null;
        string text = "";
        m_wwwError = "";
        if (!string.IsNullOrEmpty(m_www.error) || !m_www.isDone)
        {
            LogError("加载失败", m_www.url, m_www.error);
            m_wwwError = m_www.error;
            if (m_www.assetBundle != null)
                m_www.assetBundle.Unload(true);
            m_www.Dispose();
            m_www = null;
            return null;
        }
        else
        {
//            LogDebug("加载成功", m_www.url + "。文件字节 " + m_www.bytes.Length);

            if (m_www.assetBundle != null && m_www.assetBundle.mainAsset is TextAsset)
                text = (m_www.assetBundle.mainAsset as TextAsset).text;
            else if (!string.IsNullOrEmpty(m_www.text))
                text = m_www.text;
            else
            {
                LogError("资源内容不是文本", m_www.url);
                m_wwwError = "资源内容不是文本" + m_www.url;
                if (m_www.assetBundle != null)
                    m_www.assetBundle.Unload(true);
                m_www.Dispose();
                m_www = null;
                return null;
            }

            if (debug)
                Debug.Log(text);
        }

        var dic = new Dictionary<string, string>();
        var lines = text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            var kv = lines[i].Split(new char[] {'='});
            if (kv.Length == 0)
                continue;
            if (dic2 != null && kv.Length >= 3)
            {
                if (kv[2] == "outside")
                    dic2[kv[0]] = kv[1];
                else
                    dic2[kv[0]] = kv[2];
            }

            if (!dic.ContainsKey(kv[0]))
                dic.Add(kv[0], kv.Length > 1 ? kv[1] : "");
            else
            {
                Debug.LogWarning(string.Format("[Warning]有重复键值配置项 url:{0} key:{1}", m_www.url, kv[0]));
                m_wwwError = string.Format("[Warning]有重复键值配置项 url:{0} key:{1}", m_www.url, kv[0]);
            }
        }
        if (m_www.assetBundle != null)
            m_www.assetBundle.Unload(false);
        m_www.Dispose();
        m_www = null;

        if (dic.Count == 0)
            return null;
        return dic;
    }

    /// <summary>
    /// 从字典中取值，如果没有，则返回空字符串
    /// </summary>
    /// <param name="source"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    private string TryGetValue(Dictionary<string, string> source, string key)
    {
        if (source.ContainsKey(key))
            return source[key];
        else
            return "";
    }
    #endregion

    #region SD卡IO操作
    private void SdSaveFile(string path, byte[] buf)
    {
#if UNITY_WEBPLAYER
        return;
#else
        try
        {
            SdCreateDirectory(path);
            File.WriteAllBytes(SdRootPath + path, buf);
        }
        catch (Exception e)
        {
            SendLogServerUnique(1014, e.Message);
        }
        
#endif
    }

    private static bool SdExists(string path)
    {
#if UNITY_WEBPLAYER
        return false;
#else
        return File.Exists(SdRootPath + path);
#endif

    }

    private void SdCreateDirectory(string filePath)
    {
#if !UNITY_WEBPLAYER
        var dir = Path.GetDirectoryName(SdRootPath + filePath);
        if (dir != null && !Directory.Exists(dir))
            Directory.CreateDirectory(dir);
#endif
    }

    private void SdDeleteDirectory(string path)
    {
#if !UNITY_WEBPLAYER
        try
        {
            path = SdRootPath + path;
            if (Directory.Exists(path))
                Directory.Delete(path, true);
        }
        catch (Exception e)
        {
            LogError("删除路径失败", path, e.ToString());
        }
#endif
    }

    private void SdClearDirectory(string path = "")
    {
#if !UNITY_WEBPLAYER
        path = SdRootPath + path;
        if (!Directory.Exists(path))
            return;
        var dirArr = Directory.GetDirectories(path);
        for (int i = 0; i < dirArr.Length; i++)
        {
            Directory.Delete(dirArr[i], true);
        }

        var fileArr = Directory.GetFiles(path);
        for (int i = 0; i < fileArr.Length; i++)
        {
            File.Delete(fileArr[i]);
        }
#endif
    }

    //清空、整理sd卡上资源
    private void SdClear()
    {
        LogUser("App包版本变更，清除版本信息和临时下载目录");
        SdClearDirectory(InfoDir);
        SdClearDirectory(DownloadDir);

#if !UNITY_STANDALONE
        LogUser("清除资源目录");
        SdClearDirectory(ResDir);
#endif
    }
    #endregion

    #region 日志打印
    private void LogDebug(string main, string debug = "")
    {
        Debug.Log(DateTime.Now.ToString("HH:mm:ss ") + "【UpdateWorker】" + main + " " + debug + "\n");
    }

    private void LogUser(string main, string debug = "")
    {
        Debug.Log(DateTime.Now.ToString("HH:mm:ss ") + "【UpdateWorker】" + main + " " + debug + "\n");
        if (string.IsNullOrEmpty(debug))
            m_msg = main;
        else
            m_msg = main + "  " + debug;
    }

    private void LogError(string main, string debug, string errorRaw = "")
    {
        Debug.LogError(DateTime.Now.ToString("HH:mm:ss ") + "【UpdateWorker】" + main + " " + debug + "\n" + errorRaw);
        //可以根据需要开启UI上显示报错的调试信息
//        m_msg = main;
        m_msg = main + "  " + debug;
    }
    #endregion

    #region CRC校验
    private static readonly uint[] CRC32_TABLE = MakeCRC32Table();
    private static uint[] MakeCRC32Table()
    {
        uint[] table = new uint[256];
        for (uint i = 0; i < 256; i++)
        {
            uint vCRC = i;
            for (int j = 0; j < 8; j++)
            {
                if (vCRC % 2 == 0)
                    vCRC = vCRC >> 1;
                else
                    vCRC = (vCRC >> 1) ^ 0xEDB88320;
                table[i] = vCRC;
            }
        }
        return table;
    }

    private static uint CRC32(byte[] bytes)
    {
        uint result = 0xFFFFFFFF;
        for (int i = 0; i < bytes.Length; i++)
            result = CRC32_TABLE[(result & 0xff) ^ bytes[i]] ^ (result >> 8);
        return ~result;
    }

    private static string CRC32String(byte[] bytes)
    {
        return CRC32(bytes).ToString("X");
    }
    #endregion

    #region 发送日志到APM服务器
    private static Dictionary<int, bool> m_apmCodeDic = new Dictionary<int, bool>(); 
#if UNITY_EDITOR
    private const string PLATFORM = "Unity";
    private const string APM_URL = "";
#elif UNITY_ANDROID
    private const string PLATFORM = "android";
    private const string APM_URL = "http://apiapm.gz4399.com/api/apm/8ff10eab76b5ac711";
#elif UNITY_IPHONE
    private const string PLATFORM = "ios";
    private const string APM_URL = "http://apiapm.gz4399.com/api/apm/cd3cdbdb76b5ac711";
#elif UNITY_WEBPLAYER
    private const string PLATFORM = "web";
    private const string APM_URL = "";
#elif UNITY_STANDALONE
    private const string PLATFORM = "pc";
    private const string APM_URL = "";
#else
    private const string PLATFORM = "";
    private const string APM_URL = "";
#endif

    public static void SendLogServer(int code, string msg = "", params object[] args)
    {
        if (args.Length >= 0)
            msg = string.Format(msg, args);
        Debug.LogError(string.Format("APM Log[{0}]: {1}", code, msg));
        if (string.IsNullOrEmpty(APM_URL))
            return;
        (new Thread(PostApm)).Start(GetPostApmData(code, msg));
    }

    public static void SendLogServerUnique(int code, string msg = "", params object[] args)
    {
        if (args.Length >= 0)
            msg = string.Format(msg, args);
        Debug.LogError(string.Format("APM Log[{0}]: {1}", code, msg));
        if (string.IsNullOrEmpty(APM_URL))
            return;
        if (m_apmCodeDic.ContainsKey(code))
            return;
        m_apmCodeDic.Add(code, true);
        (new Thread(PostApm)).Start(GetPostApmData(code, msg));
    }

    private static string GetPostApmData(int code, string msg)
    {
        var data = new StringBuilder();
        data.Append("&server_id=0");
        data.Append("&uid=0");
        data.Append("&log_time="); data.Append((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        data.Append("&code="); data.Append(code);
        data.Append("&code_msg="); data.Append(msg);
        data.Append("&did="); data.Append(SystemInfo.deviceUniqueIdentifier);
        data.Append("&device="); data.Append(PLATFORM);
        data.Append("&device_name="); data.Append(SystemInfo.deviceModel);
        data.Append("&game_version="); data.Append(Global.currentVersion);
        data.Append("&os="); data.Append(SystemInfo.operatingSystem);
        data.Append("&os_version=");
        data.Append("&Mno=");
        data.Append("&Nm="); data.Append((Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork ? "Wifi" : "Data"));
        return data.ToString();
    }

    private static void PostApm(object data)
    {
        try
        {
            HttpRequest(data.ToString());
        }
        catch (WebException ex)
        {
            Debug.Log("尝试DNS重连! 状态值: " + ex.Status);
            if (ex.Status.ToString().Equals("NameResolutionFailure"))
            {
                var ip = DnsRetryGet("apiapm.gz4399.com");
                if (!ip.Equals("0"))
                {
                    Debug.Log("获取到DNS重连IP: " + ip);
                    HttpRequest(data.ToString(), new WebProxy(ip, 80));
                }

                HttpRequest(GetPostApmData(1204, string.Format("ip:{0} status:{1} msg:{2}", ip, ex.Status, ex.Message)));
            }
        }
    }

    private static void HttpRequest(string content, WebProxy proxy = null)
    {
        var request = (HttpWebRequest)WebRequest.Create(APM_URL);
        request.Method = "POST";
        request.Timeout = 3000;
        if (proxy != null)
            request.Proxy = proxy;
        request.ContentType = "application/x-www-form-urlencoded";
        request.ServicePoint.Expect100Continue = false;
        byte[] byteArray = Encoding.UTF8.GetBytes(content);
        request.ContentLength = byteArray.Length;
        var dataStream = request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();
        var response = request.GetResponse();
        response.Close();
    }

    private static string DnsRetryGet(string doMain)
    {
        string ip;
        var dnsUrl = "http://119.29.29.29/d?dn=";
        const string pattern = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
        var ipMatch = new Regex(pattern);
        try
        {
            dnsUrl = (doMain.Length > 1) ? (dnsUrl + doMain) : "";
            var request = (HttpWebRequest)WebRequest.Create(dnsUrl);
            request.Method = "GET";
            request.Timeout = 5000;
            var response = request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            var reqText = reader.ReadToEnd();
            var ipArray = reqText.Split(';');
            reader.Close();
            response.Close();
            ip = (ipMatch.IsMatch(ipArray[0])) ? ipArray[0] : "0";
        }
        catch
        {
            return "0";
        }
        return ip;
    }
    #endregion

    #region ETC图集的处理
    private static void DealImageEtc(GameObject go, Shader etcShader, Texture2D alphaTex)
    {
        var tran = go.transform;
        var arr = tran.GetComponentsInChildren<Image>(true);
        for (int i = 0; i < arr.Length; i++)
        {
            var img = arr[i];
            if (!img.sprite || !img.sprite.texture)
                return;
            var atlasArgs = img.sprite.texture.name.Split('_');
            if (atlasArgs.Length < 2)
                return;
            var shaderName = img.materialForRendering.shader.name;
            if (shaderName == "UI/Default" || shaderName == "UI/SpriteETC")
            {
#if UNITY_EDITOR
                etcShader = Shader.Find(etcShader.name);
#endif
                var mat = new Material(etcShader);
                mat.SetTexture("_AlphaTex", alphaTex);
                img.material = mat;
            }
        }
    }

    #endregion

    #region 各种目录
    public static string AppRootPathWww { get { return GetStreamingAssetsPath(); } }
    public static string AppRootPath { get { return Application.dataPath + "/StreamingAssets/"; } }
    public static string SdRootPath { get { return Application.persistentDataPath + "/"; } }
    public static string SdRootPathWww { get { return "file:///" + Application.persistentDataPath + "/"; } }

    public static string DownloadDir { get { return "TempDownload/"; } }
    public static string ResDir { get { return "Resources/"; } }
    public static string InfoDir { get { return "VersionInfo/"; } }

    public static string GetStreamingAssetsPath()
    {
        string s = "";
#if UNITY_EDITOR
        s = "file://" + Application.dataPath + "/StreamingAssets/";
#elif UNITY_IPHONE
        s = "file://" + Application.dataPath + "/Raw/";          
#elif UNITY_ANDROID
        s="jar:file://" + Application.dataPath + "!/assets/";
#elif UNITY_STANDALONE
        s = "file://" + Application.dataPath + "/StreamingAssets/";
#elif UNITY_WEBPLAYER
        s = Application.dataPath + "/StreamingAssets/";
#endif
        return s;
    }

    private static long GetTimeStamp()
    {
        // DateTime.ToFileTime()以100毫秒为单位，此处只取秒数。这样cdn服务器理论上每小时最大只产生3600个文件缓存。
        return DateTime.Now.Ticks/10000000;
    }
    #endregion

    #region OC接口

#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void OCappGotoUpdate(string address);
#else
    private static void OCappGotoUpdate(string address) { }
#endif

    private static void appGotoUpdate(string address)
    {
        OCappGotoUpdate(address);
    }
    #endregion
}
