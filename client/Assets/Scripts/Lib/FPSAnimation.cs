﻿using UnityEngine;
using System.Collections;

public class FPSAnimation 
{
    private Animation m_kAni;
    private string m_curClipName;


    public void SetAnimation(Animation ani)
    {
        m_kAni = ani;
    }

    public void Release()
    {
        m_kAni = null;
        m_curClipName = null;
    }

    public void AddClip(AnimationClip clip, string name)
    {
        if (m_kAni == null)
            return;

        m_kAni.AddClip(clip, name);
    }

    public AnimationClip GetClip(string name)
    {
        if (m_kAni == null)
            return null;

        return m_kAni.GetClip(name);
    }

    public void Play(string name)
    {
        if (m_kAni == null)
            return;

        //if(m_kAni.isPlaying && m_kAni.clip != null)
        if (m_kAni.isPlaying)
        {
            Jump2End();
        }

        m_kAni.Play(name);
        m_kAni.clip = m_kAni.GetClip(name);
        m_curClipName = name;
    }

    public void CrossFade(string name, float transitionDuration = 0.2f )
    {
        if (m_kAni == null)
            return;

        //if(m_kAni.isPlaying && m_kAni.clip != null)
        if (m_kAni.isPlaying)
        {
            Jump2End();
        }

        m_kAni.CrossFade(name, transitionDuration);
        m_kAni.clip = m_kAni.GetClip(name);
        m_curClipName = name;
    }

    public void PlayQueued(string animation, QueueMode queue)
    {
        if (m_kAni == null)
            return;
        m_kAni.PlayQueued(animation, queue);
    }

    public void CrossFadeQueued(string animation, float fadeLength, QueueMode queue)
    {
        if (m_kAni == null)
            return;
        m_kAni.CrossFadeQueued(animation, fadeLength, queue);
    }

    public void Stop()
    {
        if (m_kAni != null)
            m_kAni.Stop();
    }

    public void Jump2End()
    {
        // 如果动画在播放之前被sample到最后一帧，则本次不会播放，除非正在播放则没问题
        //if (m_kAni != null && m_kAni.clip != null)
        if (m_kAni != null && string.IsNullOrEmpty(m_curClipName) == false)
        {
            AnimationState curState =m_kAni[m_curClipName];
            if (curState != null)
            {
                curState.normalizedTime = 1f;
                m_kAni.Sample();
                curState.normalizedTime = 0f;
                m_kAni.clip = null;
                m_kAni.Stop();
            }
            m_curClipName = null;
        }
    }

    //public string curAniName
    //{
    //    get
    //    {
    //        if (m_kAni != null && m_kAni.clip != null)
    //            return m_kAni.clip.name;
    //        return "";
    //    }
    //}
}
