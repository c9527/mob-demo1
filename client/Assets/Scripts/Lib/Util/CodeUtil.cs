﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CodeUtil
{
    public const float CODE_PRECISION = 1000F;
    public const int CODE_MASK_RANGE = 10000000;

    private int m_mask;

    public CodeUtil()
    {
        m_mask = UnityEngine.Random.Range(1, CODE_MASK_RANGE);
    }

    #region Code float
    //static public void CodeValue(ref float val, int mask)
    //{
    //    val = (int)(val * CODE_PRECISION + 0.5f) ^ mask;
    //}

    static public long CodeValue(float val, int mask)
    {
        return BitConverter.DoubleToInt64Bits(val) ^ mask;
    }

    public void CodeValue(ref float val)
    {
        val = (int)(val * CODE_PRECISION + 0.5f) ^ m_mask;
    }

    //static public float DecodeValue(float val, int mask)
    //{
    //    return ((int)val ^ mask) / CODE_PRECISION;
    //}

    static public float DecodeValue(long val, int mask)
    {
        return (float)BitConverter.Int64BitsToDouble(val ^ mask);
    }

    public float DecodeValue(float val)
    {
        return ((int)val ^ m_mask) / CODE_PRECISION;
    }
    #endregion float

    #region int
    static public void CodeValue(ref int val, int mask)
    {
        val = val ^ mask;
    }

    public void CodeValue(ref int val)
    {
        val = val ^ m_mask;
    }

    static public int DecodeValue(int val, int mask)
    {
        return val ^ mask;
    }

    public int DecodeValue(int val)
    {
        return val ^ m_mask;
    }

    #endregion

    #region Code Vector3
    //static public void CodeValue(ref Vector3 val, int mask)
    //{
    //    CodeValue(ref val.x, mask);
    //    CodeValue(ref val.y, mask);
    //    CodeValue(ref val.z, mask);
    //}

    //public void CodeValue(ref Vector3 val)
    //{
    //    CodeValue(ref val.x);
    //    CodeValue(ref val.y);
    //    CodeValue(ref val.z);
    //}

    //static public Vector3 DecodeValue(Vector3 val, int mask)
    //{
    //    val.x = DecodeValue(val.x, mask);
    //    val.y = DecodeValue(val.y, mask);
    //    val.z = DecodeValue(val.z, mask);
    //    return val;
    //}

    //public Vector3 DecodeValue(Vector3 val)
    //{
    //    val.x = DecodeValue(val.x, m_mask);
    //    val.y = DecodeValue(val.y, m_mask);
    //    val.z = DecodeValue(val.z, m_mask);
    //    return val;
    //}
    #endregion

}

#region Ept Struct

public interface IEpt
{
    long GetLong();    
}

public struct EptFloat : IEpt
{
    private long m_value;
    private int m_mask;
    private bool m_init;

#if UNITY_EDITOR
    private float m_oriValue;
#endif

    public static implicit operator float(EptFloat s)
    {
        return s.val;
    }

    public static implicit operator EptFloat(float v)
    {
        EptFloat ef = new EptFloat();
        ef.val = v;
        return ef;
    }

    internal float val
    {
        get
        {
            if (m_init == false)
                val = 0f;
            return CodeUtil.DecodeValue(m_value, m_mask);
        }
        set
        {
            m_init = true;

            if (m_mask == 0)
                m_mask = UnityEngine.Random.Range(1, CodeUtil.CODE_MASK_RANGE);

            m_value = CodeUtil.CodeValue(value, m_mask);
#if UNITY_EDITOR
            m_oriValue = value;
#endif
        }
    }

    public override string ToString()
    {
#if UNITY_EDITOR
        return "[dv: " + val + " ori: " + m_oriValue + " ev: " + m_value + " msk: " + m_mask + "]";
        //return "[dv: " + val + " ori: " + m_oriValue + "]";
#endif

        return val.ToString();
    }

    public long GetLong()
    {
        return m_value;
    }
}

public struct EptInt : IEpt
{
    private int m_value;
    private int m_mask;
    private bool m_init;

#if UNITY_EDITOR
    private int m_oriValue;
#endif

    public static implicit operator int(EptInt s)
    {
        return s.val;
    }

    public static implicit operator EptInt(int v)
    {
        EptInt ef = new EptInt();
        ef.val = v;
        return ef;
    }

    internal int val
    {
        get
        {
            if (m_init == false)
                val = 0;
            return CodeUtil.DecodeValue(m_value, m_mask);
        }
        set
        {
            m_init = true;
            m_value = value;

            if (m_mask == 0)
                m_mask = UnityEngine.Random.Range(1, CodeUtil.CODE_MASK_RANGE);

            CodeUtil.CodeValue(ref m_value, m_mask);
#if UNITY_EDITOR
            m_oriValue = value;
#endif
        }
    }

    public override string ToString()
    {
#if UNITY_EDITOR
        //return "[" + "dv: " + val + " ori: " + m_oriValue + " ev: " + m_value + " msk: " + m_mask + "]";
        return "[" + "dv: " + val + " ori: " + m_oriValue + "]";
#endif

        return val.ToString();
    }

    public long GetLong()
    {
        return val;
    }
}

public struct EptVector3 : IEpt
{
    //private Vector3 m_value;

    private long vx;
    private long vy;
    private long vz;

#if UNITY_EDITOR
    private Vector3 m_oriValue;
#endif
    private int m_mask;
    private bool m_init;


    public static implicit operator Vector3(EptVector3 ept)
    {
        return ept.val;
    }

    public static implicit operator EptVector3(Vector3 v)
    {
        EptVector3 ef = new EptVector3();
        ef.val = v;
        return ef;
    }

    internal Vector3 val
    {
        get
        {
            if (m_init == false)
                val = Vector3.zero;

            return new Vector3(CodeUtil.DecodeValue(vx, m_mask), CodeUtil.DecodeValue(vy, m_mask), CodeUtil.DecodeValue(vz, m_mask));
        }
        set
        {
            m_init = true;
            #if UNITY_EDITOR
            m_oriValue = value;
            #endif

            if(m_mask == 0)
                m_mask = UnityEngine.Random.Range(1, CodeUtil.CODE_MASK_RANGE);

            vx = CodeUtil.CodeValue(value.x, m_mask);
            vy = CodeUtil.CodeValue(value.y, m_mask);
            vz = CodeUtil.CodeValue(value.z, m_mask);
        }
    }

    public override string ToString()
    {
//#if UNITY_EDITOR
//        return "[dv: " + val.ToStrFloat() + " ori: " + m_oriValue.ToStrFloat() + " ev: " + "(" + vx + "," + vy + ","+ vz + ")" + " mask: " + m_mask + "]";
//#endif

        return val.ToStrFloat();
    }

    public float x
    {
        get
        {
            if (m_init == false)
                val = Vector3.zero;
            return val.x;
        }
    }

    public float y
    {
        get
        {
            if (m_init == false)
                val = Vector3.zero;
            return val.y;
        }
    }

    public float z
    {
        get
        {
            if (m_init == false)
                val = Vector3.zero;
            return val.z;
        }
    }

    public long GetLong()
    {
        return vx + vy + vz;
    }
}

#endregion

//#region EptCls

//public class EptIntCls
//{
//    private EptInt m_ept;

//    public EptInt ept
//    {
//        get { return m_ept; }
//        set { m_ept = value; }
//    }

//    public static implicit operator int(EptIntCls s)
//    {
//        return s.ept.value;
//    }

//    public static implicit operator EptIntCls(int v)
//    {
//        EptIntCls ef = new EptIntCls();
//        ef.ept = v;
//        return ef;
//    }

//    public override string ToString()
//    {
//        return "[" + ept.ToString() + " id: " + GetHashCode() + "]";
//    }
//}

//public class EptFloatCls
//{
//    private EptFloat m_ept;

//    public EptFloat ept
//    {
//        get { return m_ept; }
//        set { m_ept = value; }
//    }

//    public static implicit operator float(EptFloatCls s)
//    {
//        return s.ept.value;
//    }

//    public static implicit operator EptFloatCls(float v)
//    {
//        EptFloatCls ef = new EptFloatCls();
//        ef.ept = v;
//        return ef;
//    }

//    public override string ToString()
//    {
//        return "[" + ept.ToString() + " id: " + GetHashCode() + "]";
//    }
//}

//public class EptVector3Cls
//{
//    private EptVector3 m_ept;

//    public EptVector3 ept
//    {
//        get { return m_ept; }
//        set { m_ept = value; }
//    }

//    public static implicit operator Vector3(EptVector3Cls s)
//    {
//        return s.ept.value;
//    }

//    public static implicit operator EptVector3Cls(Vector3 v)
//    {
//        EptVector3Cls ev = new EptVector3Cls();
//        ev.ept = v;
//        return ev;
//    }

//    public override string ToString()
//    {
//        return "[" + ept.ToString() + " id: " + GetHashCode() + "]";
//    }

//}

//#endregion

#region VtfObject

public class VtfObject
{
    private string m_id;

    public VtfObject()
    {
        m_id = UnityEngine.Random.Range(0, 1000000).ToString() + GetHashCode();
    }

    protected string id
    {
        get { return m_id; }
    }
}

public class VtfFloatCls : VtfObject
{
    public float val
    {
        get { return VertificateUtil.GetValue<EptFloat>(id); }
        set { VertificateUtil.SetValue<EptFloat>(id, value); }
    }
}

public class VtfVector3Cls : VtfObject
{
    public Vector3 val
    {
        get { return VertificateUtil.GetValue<EptVector3>(id); }
        set { VertificateUtil.SetValue<EptVector3>(id, value); }
    }

    public void SetPos(Vector3 data)
    {
        VertificateUtil.SetValue<EptVector3>(id, data, true);
    }
}

public class VtfIntCls : VtfObject
{
    public int val
    {
        get { return VertificateUtil.GetValue<EptInt>(id); }
        set { VertificateUtil.SetValue<EptInt>(id, value); }
    }
}

public class VtfBoolCls : VtfObject
{
    public bool val
    {
        get { return VertificateUtil.GetValue<EptInt>(id) == 1; }
        set { VertificateUtil.SetValue<EptInt>(id, value ? 1 : 0); }
    }
}
#endregion

public class VertificateUtil
{
    static private int Mask = UnityEngine.Random.Range(1, CodeUtil.CODE_MASK_RANGE);

    static private Dictionary<string, IEpt> m_dic = new Dictionary<string, IEpt>();
    static private long m_code = 0 ^ Mask;

    static public void Reset()
    {
        m_dic.Clear();
        m_code = 0 ^ Mask;
    }

    static public T GetValue<T>(string id) where T : IEpt
    {
        T val = default(T);

        IEpt obj = null;

        if (m_dic.TryGetValue(id, out obj) == false)
            m_dic.Add(id, val);
            //SetValue(id, val);
        else
            val = (T)obj;

        return val;
    }

    static public void SetValue<T>(string id, T data, bool debug = false) where T: IEpt
    {
        if (debug == false)
            m_code = CaculateCode(id, data);
           
        if (m_dic.ContainsKey(id) == false)
        {
            m_dic.Add(id, data);
        }
        else
        {
            m_dic[id] = data;
        }
    }  
   
    static private long CaculateCode<T>(string id, T data) where T : IEpt
    {
        long code = m_code;

        code ^= Mask;
        code += data.GetLong() - GetValue<T>(id).GetLong();
        code ^= Mask;
        return code;
    }

    static private long Decode(long data)
    {
        return data ^ Mask;
    }

    static private long CaculateCode()
    {
        long sum = 0;
        foreach (KeyValuePair<string, IEpt> kvp in m_dic)
        {
            sum += kvp.Value.GetLong();
        }

        return sum ^ Mask;
    }

    static public void GetVtfCode()
    {
        long code = CaculateCode();
        List<long> list = new List<long>() { m_code, code };
        NetLayer.SendMsg("player", "vtfcode", list);
        //Logger.Log("VtfCode => " + "code = " + m_code + " / " + Decode(m_code) + "   " + " newcode = " + code + " / " + Decode(code));
    }

    static public void GMTest()
    {
        long code = CaculateCode();
        string msg = "code = " + m_code + " / " + Decode(m_code) + "   " + " newcode = " + code + " / " + Decode(code);
        if (code != m_code)
        {
            UIManager.ShowTipPanel("数据校验失败 Msg = " + msg);
        }

        Logger.Log("VtfCode Error: " + msg);
    }
}