﻿// ----------------------------------------
//
//  MathTool.cs
//
//  Author:
//       bavol
//
// ----------------------------------------
//
using UnityEngine;

/// <summary>
/// 常用数学工具
/// </summary>
public static class MathTool
{
    /// <summary>
    /// 点到直线的距离，其中直线是lineStart和lineEnd两点决定的
    /// </summary>
    /// <param name="point"></param>
    /// <param name="lineStart"></param>
    /// <param name="lineEnd"></param>
    /// <returns></returns>
    static public float DistancePointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
    {
        Ray ray = new Ray(lineStart, lineEnd);
        return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
    }

    static public bool InRange(float val, float standVal, float offset)
    {
        if (val > standVal + offset || val < standVal - offset)
            return false;
 
        return true;
    }

    static public bool InRange(Vector3 val, Vector3 standVal, Vector3 offset)
    {
        if (InRange(val.x, standVal.x, offset.x) &&
            InRange(val.y, standVal.y, offset.y) &&
            InRange(val.z, standVal.z, offset.z))
            return true;

        return false;
    }

    /// <summary>
    /// halfLength即长方形的长是跟朝向平行来定义的
    /// </summary>
    /// <param name="targetPos"></param>
    /// <param name="halfLength"></param>
    /// <param name="halfWidth"></param>
    /// <param name="judgePos"></param>
    /// <returns></returns>
    static public bool InRectangle(Vector3 targetPos, float halfLength, float halfWidth, Vector3 judgePos)
    {
        Rect rect = new Rect(targetPos.x - halfWidth, targetPos.y - halfLength, 2*halfWidth, 2*halfLength);

        return rect.Contains(judgePos, true);
    }

    static public bool InCircle(Vector3 targetPos, float radius, Vector3 judgePos)
    {
        return (judgePos - targetPos).sqrMagnitude <= radius * radius;
    }
}