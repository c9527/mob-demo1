﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using Object = UnityEngine.Object;


public static class ExtendFuncHelper
{
    /// <summary>
    /// 【扩展】设置UI组件的父对象、坐标
    /// </summary>
    /// <param name="source"></param>
    /// <param name="parent"></param>
    /// <param name="x">如果为float.NaN，则不更改</param>
    /// <param name="y">如果为float.NaN，则不更改</param>
    public static void SetUILocation(this RectTransform source, Transform parent, float x = float.NaN, float y = float.NaN)
    {
        if (source.parent != parent)
        {
            source.SetParent(parent, false);
        }
        var pos = source.anchoredPosition;
        if (!float.IsNaN(x))
            pos.x = x;
        if (!float.IsNaN(y))
            pos.y = y;
        source.anchoredPosition = pos;
    }

    /// <summary>
    /// 比较ActiveSelf过后再执行SetActive，减少性能消耗
    /// </summary>
    /// <param name="source"></param>
    /// <param name="active"></param>
    public static void TrySetActive(this GameObject source, bool active)
    {
        if (source != null && source.activeSelf != active)
            source.SetActive(active);
    }

    /// <summary>
    /// 【扩展】设置Tranform的父对象、本地坐标
    /// </summary>
    /// <param name="source"></param>
    /// <param name="parent"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public static void SetLocation(this Transform source, Transform parent, float x = float.NaN, float y = float.NaN, float z = float.NaN)
    {
        source.SetParent(parent, false);
        var pos = source.localPosition;
        if (!float.IsNaN(x))
            pos.x = x;
        if (!float.IsNaN(y))
            pos.y = y;
        if (!float.IsNaN(z))
            pos.z = z;
        source.localPosition = pos;
    }

    public static RectTransform GetRectTransform(this GameObject source)
    {
        return source.GetComponent<RectTransform>();
    }

    static public T AddMissingComponent<T>(this GameObject go) where T : Component
    {
        if (go == null)
            return null;

        T comp = go.GetComponent<T>();
        if (comp == null)
            comp = go.AddComponent<T>();
        return comp;
    }

    static public void DestroyComponent<T>(this GameObject go) where T : Component
    {
        if (go == null)
            return;

        T comp = go.GetComponent<T>();
        if (comp != null)
            UnityEngine.Object.Destroy(comp);
    }

    static public void ResetLocal(this Transform trans)
    {
        if (trans == null)
        {
            Logger.Error("ResetLocal trans is null");
            return;
        }

        trans.localPosition = Vector3.zero;
        trans.localEulerAngles = Vector3.zero;
        trans.localScale = Vector3.one;
    }

    /// <summary>
    /// 递归搜索
    /// </summary>
    static public Transform Find2(this Transform trans, string name, bool ignoreCase = true)
    {
        if (trans == null)
            return null;

        Transform child = null;
        Transform ret = null;
        for (int i = 0; i < trans.childCount; i++)
        {
            child = trans.GetChild(i);
            if (ignoreCase && child.name.ToLower() == name.ToLower() || !ignoreCase && child.name == name)
            {
                ret = child;
                break;
            }

            ret = child.Find2(name, ignoreCase);
            if (ret != null)
                break;
        }

        return ret;
    }

    static public void ApplyLayer(this GameObject goParent, int newLayer, int[] excludeLayers = null, string[] excludeName = null, bool traverse = true)
    {
        if (goParent == null)
            return;

        if (excludeLayers != null)
        {
            for (int i = 0; i < excludeLayers.Length; i++)
            {
                if (excludeLayers[i] == goParent.layer)
                    return;
            }
        }

        string name = goParent.name;
        if (excludeName != null)
        {
            for (int i = 0; i < excludeName.Length; i++)
            {
                if (excludeName[i] == name)
                    return;
            }
        }

        goParent.layer = newLayer;

        if (!traverse)
            return;

        Transform transParent = goParent.transform;
        Transform transChild;
        int iChildCnt = transParent.transform.childCount;

        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transParent.transform.GetChild(i);
            transChild.gameObject.ApplyLayer(newLayer, excludeLayers, excludeName, traverse);
        }
    }

    /// <summary>
    /// 只设置所有子节点的层，不设置本层
    /// </summary>
    static public void ApplyChildrenLayer(this GameObject goParent, int newLayer, int[] excludeLayers = null, string[] excludeName = null)
    {
        if (goParent == null)
            return;

        Transform transParent = goParent.transform;
        Transform transChild;
        int iChildCnt = transParent.transform.childCount;

        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transParent.transform.GetChild(i);
            transChild.gameObject.ApplyLayer(newLayer, excludeLayers, excludeName, true);
        }
    }

    /// <summary>
    /// 递归访问
    /// </summary>
    static public void VisitChild(this Transform transform, Action<GameObject> funCallBack, bool includeSelf)
    {
        if (includeSelf)
            funCallBack(transform.gameObject);

        transform.VisitChild(funCallBack);
    }

    static private void VisitChild(this Transform transform, Action<GameObject> funCallBack)
    {
        if (transform == null)
            return;

        Transform transChild = null;

        int iChildCnt = transform.transform.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transform.transform.GetChild(i);
            funCallBack(transChild.gameObject);
            transChild.VisitChild(funCallBack);
        }
    }

    public delegate bool CallBackBool<T>(T obj);

    static public void VisitChild(this Transform transform, CallBackBool<GameObject> funCallBack, bool includeSelf)
    {
        if (includeSelf)
        {
            if (funCallBack(transform.gameObject))
                transform.VisitChild(funCallBack);
        }
        else
        {
            transform.VisitChild(funCallBack);
        }
    }

    static private void VisitChild(this Transform transform, CallBackBool<GameObject> funCallBack)
    {
        if (transform == null)
            return;

        Transform transChild = null;

        int iChildCnt = transform.transform.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transform.transform.GetChild(i);
            if (funCallBack(transChild.gameObject) == true)
                transChild.VisitChild(funCallBack);
        }
    }

    static public bool IsInLayerMask(this GameObject go, LayerMask layerMask)
    {
        int goLayerMask = (1 << go.layer);
        return (goLayerMask & layerMask.value) > 0;
    }

    static public void ReplaceShader(this GameObject go, string oldShaderName, List<string> excludeShader, Shader newShader, bool useLightProbe, bool newMaterial = false, bool isTraversal = true)
    {
        if (go == null)
            return;

        //#if UNITY_EDITOR
        //        if (newMaterial == true)
        //            GameObjectHelper.RefreshMaterial(go.transform);
        //#endif

        go.transform.VisitChild((child) =>
        {
            Renderer render = child.GetComponent<Renderer>();
            if (render != null)
            {
                render.useLightProbes = useLightProbe;
                Material mat = null;
                if (newMaterial)
                    mat = render.material;
                else
                    mat = render.sharedMaterial;

                if (mat != null)
                {
                    if (oldShaderName.IsNullOrEmpty() || mat.shader.name == oldShaderName)
                    {
                        if (excludeShader == null || excludeShader.Contains(mat.shader.name) == false)
                        {
                            mat.shader = newShader;
#if UNITY_EDITOR
                            mat.shader = Shader.Find(newShader.name);
#endif
                        }
                    }
                }
            }
            return isTraversal;
        }, true);
    }

    static public void CopyPropFloatFrom(this Material toMat, Material fromMat, string propName)
    {
        if (toMat == null || fromMat == null || toMat.HasProperty(propName) == false || fromMat.HasProperty(propName) == false)
            return;

        toMat.SetFloat(propName, fromMat.GetFloat(propName));
    }

    static public void CopyPropTexFrom(this Material toMat, Material fromMat, string propName)
    {
        if (toMat == null || fromMat == null || toMat.HasProperty(propName) == false || fromMat.HasProperty(propName) == false)
            return;

        toMat.SetTexture(propName, fromMat.GetTexture(propName));
    }

    static public void CopyPropColorFrom(this Material toMat, Material fromMat, string propName)
    {
        if (toMat == null || fromMat == null || toMat.HasProperty(propName) == false || fromMat.HasProperty(propName) == false)
            return;

        toMat.SetColor(propName, fromMat.GetColor(propName));
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="img"></param>
    /// <param name="sprite"></param>
    /// <param name="iconType"></param>  道具配置表中的  SubType字段
    static public void SetSprite(this Image img, Sprite sprite, String iconType = "")
    {
        img.sprite = sprite;
        UIHelper.DealImageEtc(img);
        //处理碎片合成的图标
        string mergeIconName = "_mergeIcon";
        if (iconType == GameConst.ITEMS_SUBTYPE_MERGE && sprite != null)
        {
            img.SetNativeSize();
            //碎片需要特殊处理，挂载一个碎片的图标
            Image mergeImg;
            GameObject mergeIcon;
            if (img.gameObject.transform.Find(mergeIconName) == null)
            {
                mergeIcon = new GameObject(mergeIconName);
                mergeIcon.transform.SetParent(img.gameObject.transform);
                mergeImg = mergeIcon.AddComponent<Image>();
                mergeImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "partSubIcon"));
                mergeImg.SetNativeSize();
            }
            else
            {
                mergeIcon = img.gameObject.transform.Find(mergeIconName).gameObject;
                mergeImg = mergeIcon.GetComponent<Image>();
                img.gameObject.transform.Find(mergeIconName).gameObject.TrySetActive(true);
            }
            RectTransform rtform = mergeImg.rectTransform;
            Vector2 vec = img.rectTransform.sizeDelta;
            rtform.anchoredPosition3D = Vector3.zero;
            rtform.localScale = Vector3.one;
            //rtform.RotateAroundLocal()
            mergeIcon.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);

            rtform.anchoredPosition = new Vector2(vec.x / 2 - rtform.sizeDelta.x / 2, rtform.sizeDelta.y / 2 - vec.y / 2 + 5);
        }
        else
        {
            if (img.gameObject.transform.Find(mergeIconName))
                img.gameObject.transform.Find(mergeIconName).gameObject.TrySetActive(false);
        }
    }

    static public bool StartsWithAny(this string src, string[] values)
    {
        var result = false;
        for (int i = 0; i < values.Length; i++)
        {
            if (src.StartsWith(values[i]))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    # region C# Related Expend Method

    static public string ToStr<T>(this T[] arr, string splitChar = " ")
    {
        if (arr == null)
            return "null";

        string msg = "";
        foreach (T value in arr)
        {
            msg += splitChar + value.ToString();
        }

        return msg;
    }

    static public string ToStr<T>(this List<T> list, string splitChar = " ")
    {
        if (list == null)
            return "null";

        string msg = "";
        foreach (T value in list)
        {
            msg += splitChar + value.ToString();
        }

        return msg;
    }

    static public bool IsNullOrEmpty(this string str)
    {
        return string.IsNullOrEmpty(str);
    }

    /// <summary>
    /// 带正负号的角度
    /// </summary>
    static public float Angle2(this Vector3 vec, Vector3 dir)
    {
        float angle = Vector3.Angle(vec, dir);
        float crossValue = Vector3.Cross(vec, dir).y;
        angle = crossValue >= 0 ? angle : -angle;
        return angle;
    }

    static public string ToStrFloat(this Vector3 vec)
    {
        return "(" + vec.x + ", " + vec.y + ", " + vec.z + ")";
    }

    static public Color toColor(this int[] arr)
    {
        if (arr == null || arr.Length < 3)
        {
            Logger.Error("int[].toColor length < 3");
            return Color.white;
        }

        Color color = new Color(arr[0], arr[1], arr[2], 255);
        if (arr.Length == 4)
            color.a = arr[3];

        return color;
    }

    static public Color toColor(this string arr)
    {
        if (arr == null)
        {
            Logger.Error("string[].toColor err");
            return Color.white;
        }

        float _r = Convert.ToInt32("0x" + arr.Substring(0, 2), 16);
        float _g = Convert.ToInt32("0x" + arr.Substring(2, 2), 16);
        float _b = Convert.ToInt32("0x" + arr.Substring(4, 2), 16);

        Color color = new Color(_r, _g, _b, 255);

        return color;
    }

    static public TValue GetValue<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, TValue defalueValue = default(TValue))
    {
        return dic.ContainsKey(key) ? dic[key] : defalueValue;
    }

    static public int IndexOf(this string[] arr, string str)
    {
        for(int i=0; i<arr.Length; i++)
        {
            if (arr[i] == str)
                return i;
        }

        return -1;
    }

    static public T[] ToArray<T>(this HashSet<T> hs)
    {
        T[] arr = new T[hs.Count];
        int i = 0;
        foreach( T val in hs)
        {
            arr[i++] = val;
        }

        return arr;
    }

    #endregion
}
