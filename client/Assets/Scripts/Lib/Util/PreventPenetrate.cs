﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PreventPenetrate : MonoBehaviour {
    
    public float Speed = 0;
    public float SpeedDecay = 0.2f;
    private float SphereCastRadius = 0.2f;
    private float SphereCastDisOffset = 0.1f;
    private float StopRunSpeed = -1f;

    private Vector3 m_refDir;
    private Vector3 m_vec3PrePos;
    private Vector3 m_vec3TargetPos;
    private Vector3 m_hitPos = Vector3.zero;

    private int m_iTick;
    private bool m_bApplyForce = false;
    private bool m_bRun;
    private float m_halfSize = 0.5f;

	
	void Start () {

        Collider c = GetComponent<Collider>();
        if(c is CapsuleCollider)
        {
            CapsuleCollider cc = c as CapsuleCollider;
            m_halfSize = cc.radius > cc.height ? cc.radius : cc.height;
        }
        else if(c is BoxCollider)
        {
            BoxCollider bc = c as BoxCollider;
            m_halfSize = bc.size.x > bc.size.y ? bc.size.x : bc.size.y;
            m_halfSize = m_halfSize > bc.size.z ? m_halfSize : bc.size.z;
        }
        else if(c is SphereCollider)
        {
            SphereCollider sc = c as SphereCollider;
            m_halfSize = sc.radius;
        }
        m_halfSize += 0.1f;
	}

    public void Run()
    {
        m_vec3PrePos = transform.position;
        m_bRun = true;
        rigidbody.isKinematic = false;
    }
	
	void FixedUpdate () 
    {
        if (m_bRun == false)
            return;

        m_iTick++;

        if (m_bApplyForce && m_iTick == 1)
        {
            m_bApplyForce = false;

            rigidbody.AddForce(Speed * m_refDir, ForceMode.VelocityChange);
            //Logger.Log("AddForce, Speed = " + Speed);

            if (Speed <= StopRunSpeed)
            {
                m_bRun = false;
                //Logger.Log("StopRun");
            }
        }

        if (Speed >= 0.001f)
        {
            Vector3 curPos = transform.position;
            Vector3 moveDir = Vector3.Normalize(curPos - m_vec3PrePos);
            Vector3 oriPos = m_vec3PrePos - moveDir * SphereCastDisOffset;
            Vector3 endPos = curPos + moveDir * SphereCastDisOffset;
            float dis = Vector3.Distance(oriPos, endPos);

            int layer = GameSetting.LAYER_MASK_BUILDING;
            RaycastHit[] arrRH = Physics.SphereCastAll(oriPos, SphereCastRadius, moveDir, dis, layer);
            List<RaycastHit> listRH = new List<RaycastHit>(arrRH);
            listRH.Sort(delegate(RaycastHit kRH1, RaycastHit kRH2)
            {
                return kRH1.distance - kRH2.distance >= 0 ? 1 : -1;
            });

            if (listRH.Count > 0)
            {
                //Logger.Log(listRH.Count);

                RaycastHit kRH = listRH[0];
                Vector3 vec3Normal = listRH[0].normal;
                if (listRH.Count >= 2)
                {
                    vec3Normal = Vector3.Normalize(vec3Normal + listRH[1].normal);
                    //Logger.Log("normal1: " + vec3Normal + " normal2:" + listRH[1].normal);
                }

                if ((m_hitPos - kRH.point).sqrMagnitude > 0.01f && (m_vec3PrePos - curPos).sqrMagnitude > 0.01f)
                {
                    m_hitPos = kRH.point;
                    Speed *= SpeedDecay;

                    Vector3 dir1 = Vector3.Normalize(m_vec3PrePos - curPos);
                    Vector3 refDir = Vector3.Dot(2 * vec3Normal, dir1) * vec3Normal - dir1;
                    refDir.Normalize();
                    m_refDir = refDir;

                    m_vec3TargetPos = kRH.point + refDir * m_halfSize;

                    transform.position = m_vec3TargetPos;
                    rigidbody.velocity = Vector3.zero;

                    m_bApplyForce = true;
                    m_iTick = 0;

                    //Logger.Log("RayCastHit, HitPos = " + kRH.point + " m_vec3PrePos=" + m_vec3PrePos + "  curPos=" + curPos + "  m_refDir=" + m_refDir);
                }
            }
        }

        m_vec3PrePos = transform.position;
	}
}
