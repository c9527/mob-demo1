﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class Util
{
    public static string[] BIG_NUM_ARRAY = { "零","一","二","三","四","五","六","七","八","九" };
    public static bool AlwaysSmallMem; //测试用

    public static bool NeedClearAll()
    {
        if (IsSmallMem()) 
            return true;

        int freeMemory = GetFreeMemory();
        if (freeMemory != -1 && freeMemory <= GlobalConfig.gameSetting.MinFreeMemory)
            return true;

        int usedMemory = GetUsedMemory();
        if (usedMemory != -1 && usedMemory >= GlobalConfig.gameSetting.MaxUsedMemory)
            return true;

        return false;
    }

    public static bool IsSmallMem()
    {
        return AlwaysSmallMem || SystemInfo.systemMemorySize <= (GlobalConfig.gameSetting == null ? 550 : GlobalConfig.gameSetting.SmallMemory);
    }

    public static string[] GetAllProcessName()
    {
        if (SdkManager.IsSupportFunction("GetAllProcessName") == false)
            return null;

        return PlatformAPI.GetAllProcessName();
    }

    public static string GetMemoryInfo()
    {
        return "FreeMemory = " + GetFreeMemory() + " UsedMemory = " + GetUsedMemory() + " IsLowMemory = " + IsLowMemory() + " SysMemSize = " + SystemMemorySize() + " MemorySize = " + GetMemorySize();
    }

    public static int GetMemorySize()
    {
        return SystemInfo.systemMemorySize + SystemInfo.graphicsMemorySize;
    }

    public static int SystemMemorySize()
    {
        return SystemInfo.systemMemorySize;
    }

    public static int GetFreeMemory()
    {
        if (SdkManager.IsSupportFunction("GetFreeMemory") == false)
            return -1;

        return PlatformAPI.GetFreeMemory();
    }

    public static int GetUsedMemory()
    {
        if (SdkManager.IsSupportFunction("GetUsedMemory") == false)
            return -1;

        return PlatformAPI.GetUsedMemory();
    }

    public static bool IsLowMemory()
    {
        if (SdkManager.IsSupportFunction("IsLowMemory") == false)
            return false;

        return PlatformAPI.IsLowMemory();
    }
  
    /// <summary>
    /// 获取错误码信息,如果有错误码,则优先按错误码查表,否则返回错误信息字符串
    /// </summary>
    /// <param name="errCode">错误码</param>
    /// <param name="errMsg">错误信息</param>
    /// <returns>正确的错误信息</returns>
    public static string GetErrMsg(int errCode, string errMsg)
    {
        if (errCode != 0)
        {
            var errStr = ConfigManager.GetConfig<ConfigMsgStr>().GetMsgStr(errCode);
            if (!String.IsNullOrEmpty(errStr))
            {
                return errStr;
            }
        }
        return errMsg;
    }

    public static bool isLangSupported(string langStr)
    {
        if (String.IsNullOrEmpty(langStr))
        {
            return true;
        }
        var langStrArr = langStr.Split(new []{';'}, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < langStrArr.Length; i++)
        {
            if (langStrArr[i] == SdkManager.Lang)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 读取文件的所有文本内容
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string ReadAllText(string path)
    {
        using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (var sr = new StreamReader(fs, Encoding.Unicode))
            {
                return sr.ReadToEnd();
            }
        }
    }

    public static string ToThousandStr(int num)
    {
        if (num < 1000)
        {
            return num.ToString();
        }
        return ((decimal)num / 1000).ToString("#.#k");
    }

    public static string ToBigNum(int num)
    {
        string res = "";
        if (num >= BIG_NUM_ARRAY.Length || num < 0) return res;
        return BIG_NUM_ARRAY[num];
    }

    /// <summary>
    /// 字符串转行列表
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    public static List<string> StrToLines(string content)
    {
        var lines = new List<string>();
        using (var sr = new StringReader(content))
        {
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                lines.Add(line);
            }
        }
        return lines;
    }

    public static T[] ArrayConcat<T>(this T[] srcArr, T[] addArr)
    {
        var srcArrLength = srcArr.Length;
        var newArr = new T[srcArrLength + addArr.Length];
        for (int i = 0; i < srcArr.Length; i++)
        {
            newArr[i] = srcArr[i];
        }
        for (int i = 0; i < addArr.Length; i++)
        {
            newArr[i + srcArrLength] = addArr[i];
        }
        return newArr;
    }

    public static T[] ArrayConcat<T>(this T[] srcArr, T add)
    {
        return ArrayConcat(srcArr, new []{add});
    }

    public static T[] ArrayDelete<T>(this T[] srcArr, int index)
    {
        if (srcArr.Length == 0)
            return new T[0];
        var newArr = new T[srcArr.Length - 1];
        for (int i = 0; i < newArr.Length; i++)
        {
            if (i < index)
                newArr[i] = srcArr[i];
            else
                newArr[i] = srcArr[i + 1];
        }
        return newArr;
    }

    /// <summary>
    /// 随机打乱数组内容的顺序
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="srcArr"></param>
    /// <returns></returns>
    public static T[] Random<T>(this T[] srcArr)
    {
        for (int i = srcArr.Length - 1; i >= 0; i--)
        {
            for (int j = 0; j < i; j++)
            {
                var swapIndex = UnityEngine.Random.Range(0, srcArr.Length);
                var tmp = srcArr[swapIndex];
                srcArr[swapIndex] = srcArr[srcArr.Length - 1];
                srcArr[srcArr.Length - 1] = tmp;
            }
        }

        return srcArr;
    }
   
    public static Text CreateText(Transform parent, Vector2 pos = default(Vector2), int fontSize = 18, Color32 color = default(Color32), Font font = null,TextAnchor align=TextAnchor.UpperLeft)
    {
        var go = new GameObject("Text");
        var text = go.AddComponent<Text>();
        text.fontSize = fontSize;
        if (font == null)
            text.font = ResourceManager.LoadFont();
        else
            text.font = font;
        if (color != default(Color))
            text.color = color;
        if (parent != null)
            go.transform.SetParent(parent, false);
        text.rectTransform.anchoredPosition = pos;
        text.rectTransform.sizeDelta = new Vector2(160,30);
        text.alignment = align;
        return text;
    }

    /// <summary>
    /// 用bool变量来设置对象的灰化与否
    /// </summary>
    /// <param name="go"></param>
    /// <param name="isGray"></param>
    public static void SetGoGrayShader(GameObject go, bool isGray = true)
    {
        if (go == null) return;
        var compontList = go.GetComponentsInChildren<Component>();
        for (int i = 0; i < compontList.Length; i++)
        {
            if (isGray) SetGrayShader(compontList[i]);
            else SetNormalShader(compontList[i]);
        }
    }

    public static void SetGrayShader(Component graphic)
    {
        var gray = ResourceManager.LoadMaterial("mat_Gray");
        var grayFont = ResourceManager.LoadMaterial("mat_GrayFont");
        var arr = graphic.GetComponentsInChildren<Graphic>(true);
        for (int i = 0; i < arr.Length; i++)
        {
            if (UIHelper.IsEnableEtcAtlas())
            {
                var atlasArgs = arr[i].mainTexture.name.Split('_');
                if (atlasArgs.Length < 2)
                    continue;
                arr[i].material = ResourceManager.LoadAtlasAlphaMaterial(atlasArgs[0], atlasArgs[1], GameConst.SHADER_UI_SPRITE_GRAY_ETC);
            }
            else
                arr[i].material = gray;
            
            if (arr[i] is Text)
                arr[i].material = grayFont;
        }
    }

    public static void SetNormalShader(Component graphic)
    {
        //var gray = GrayMaterial();
        var arr = graphic.GetComponentsInChildren<Graphic>(true);
        for (int i = 0; i < arr.Length; i++)
        {
            if (UIHelper.IsEnableEtcAtlas())
            {
                var atlasArgs = arr[i].mainTexture.name.Split('_');
                if (atlasArgs.Length < 2)
                    continue;
                arr[i].material = ResourceManager.LoadAtlasAlphaMaterial(atlasArgs[0], atlasArgs[1], GameConst.SHADER_UI_SPRITE_ETC);
            }
            else
                arr[i].material = null;
            
            if (arr[i] is Text)
                arr[i].material = null;
        }
    }

    public static void DumpBytes(byte[] bdata, int len)
    {
        int i;
        int j = 0;
        char dchar;
        // 3 * 16 chars for hex display, 16 chars for text and 8 chars
        // for the 'gutter' int the middle.
        StringBuilder dumptext = new StringBuilder("        ", 16 * 4 + 8);
        for (i = 0; i < len; i++)
        {
            dumptext.Insert(j * 3, String.Format("{0:X2} ", (int)bdata[i]));
            dchar = (char)bdata[i];
            //' replace 'non-printable' chars with a '.'.
            if (Char.IsWhiteSpace(dchar) || Char.IsControl(dchar))
            {
                dchar = '.';
            }
            dumptext.Append(dchar);
            j++;
            if (j == 16)
            {
                Logger.Log(dumptext.ToString());
                dumptext.Length = 0;
                dumptext.Append("        ");
                j = 0;
            }
        }
        // display the remaining line
        if (j > 0)
        {
            for (i = j; i < 16; i++)
            {
                dumptext.Insert(j * 3, "   ");
            }
            Logger.Log(dumptext.ToString());
        }
    }

    private static readonly uint[] CRC32_TABLE = MakeCRC32Table();

    private static uint[] MakeCRC32Table()
    {
        uint[] table = new uint[256];
        for (uint i = 0; i < 256; i++)
        {
            uint vCRC = i;
            for (int j = 0; j < 8; j++)
            {
                if (vCRC % 2 == 0)
                    vCRC = vCRC >> 1;
                else
                    vCRC = (vCRC >> 1) ^ 0xEDB88320;
                table[i] = vCRC;
            }
        }
        return table;
    }

    public static uint CRC32(byte[] bytes)
    {
        uint result = 0xFFFFFFFF;
        for (int i = 0; i < bytes.Length; i++)
            result = CRC32_TABLE[(result & 0xff) ^ bytes[i]] ^ (result >> 8);
        return ~result;
    }

    public static string CRC32String(byte[] bytes)
    {
        return CRC32(bytes).ToString("X");
    }

    static public bool IsEqual(Vector3 val1, Vector3 val2, Vector3 offset)
    {
        Vector3 delta = val1 - val2;

        if (Mathf.Abs(delta.x) <= offset.x && Mathf.Abs(delta.y) <= offset.y && Mathf.Abs(delta.z) <= offset.z)
            return true;

        return false;
    }

    static public string TransStrInLine(string line)
    {
        string text = "";
        if (line != null)
        {
            text = line.Replace("\\n", "\n");
        }
        return text;
    }
}