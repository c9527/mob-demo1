﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class UIHelper
{
    public static Vector2 HALF_X_HALF_Y_VEC = new Vector2(0.5f, 0.5f);
    public static Vector2 HALF_X_ONE_Y_VEC = new Vector2(0.5f, 1f);
    public static Vector2 ONE_X_HALF_Y_VEC = new Vector2(1f, 0.5f);
    public static Vector2 ZERO_X_HALF_Y_VEC = new Vector2(0, 0.5f);
    public static Vector2 ZERO_X_ONE_Y_VEC = new Vector2(0, 1);

    #region ETC通道分离相关方法
    /// <summary>
    /// 实例化UI对象的专用方法，会对包含的图片进行替换shader
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static GameObject Instantiate(Object obj)
    {
        var tmp = Object.Instantiate(obj) as GameObject;
        DealImageEtc(tmp);
        return tmp;
    }

    /// <summary>
    /// 是否启用etc通道分离方式显示UI
    /// </summary>
    /// <returns></returns>
    public static bool IsEnableEtcAtlas()
    {
        return ResourceManager.m_loadMode == ELoadMode.AssetBundle && Driver.isMobilePlatform;
    }

    /// <summary>
    /// 尝试根据资源路径来获得对应的透明图集路径
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string TryGetAtlasAlphaPath(string path)
    {
        if (Regex.IsMatch(path, "(^|/)Atlas/"))
            return Regex.Replace(path, "(^|/)Atlas/(.+?)(\\.|$|/)", "$1AtlasAlpha/$2_a$3");
        return null;
    }

    private static void DealImageEtc(GameObject go)
    {
        if (!IsEnableEtcAtlas())
            return;
        var tran = go.transform;
        if (ResourceManager.m_loadMode == ELoadMode.Resources)
            return;
        var arr = tran.GetComponentsInChildren<Image>(true);
        for (int i = 0; i < arr.Length; i++)
        {
            DealImageEtc(arr[i]);
        }
    }

    public static void DealImageEtc(Image img)
    {
        if (!IsEnableEtcAtlas())
            return;
        if (!img.sprite || !img.sprite.texture)
            return;
        var atlasArgs = img.sprite.texture.name.Split('_');
        if (atlasArgs.Length < 2)
            return;
        var shaderName = img.materialForRendering.shader.name;
        if (shaderName == GameConst.SHADER_UI_DEFAULT || shaderName == GameConst.SHADER_UI_SPRITE_ETC)
        {
            img.material = ResourceManager.LoadAtlasAlphaMaterial(atlasArgs[0], atlasArgs[1], GameConst.SHADER_UI_SPRITE_ETC);
        }
        else if (shaderName == GameConst.SHADER_FPS_UI_SHANGUANG || shaderName == GameConst.SHADER_FPS_UI_SHANGUANG_ETC)
        {
            img.material = ResourceManager.LoadAtlasAlphaMaterial(atlasArgs[0], atlasArgs[1], GameConst.SHADER_FPS_UI_SHANGUANG_ETC);
        }
        else
        {
#if UNITY_EDITOR
            img.material.shader = Shader.Find(img.material.shader.name);
#endif
        }
    }
    #endregion

    #region UI辅助方法
    /// <summary>
    /// 显示图片的按钮
    /// </summary>
    public static void ShowBtn(ref GameObject go, string name, string atlas, string sprite, Vector2 pos, Delegate onClick, bool isShow, RectTransformProp prop = null, Transform parent = null)
    {
        if (!isShow && go == null) return;
        if (go == null && isShow)
        {
            ShowSprite(ref go, name, atlas, sprite, pos, isShow, prop, parent);
            go.AddMissingComponent<Button>();
            UGUIClickHandler.Get(go).onPointerClick += (UGUIClickHandler.PointerEvetCallBackFunc)onClick;
        }
        go.TrySetActive(isShow);
    }
    /// <summary>
    /// 没有显示图片的按钮
    /// </summary>
    public static void ShowBtn(ref GameObject go, string name, Vector3 pos, Delegate onClick, bool isShow, RectTransformProp prop = null, Transform parent = null)
    {
        if (!isShow && go == null) return;
        if (go == null && isShow)
        {
            go = new GameObject(name);
            UGUIClickHandler.Get(go).onPointerClick += (UGUIClickHandler.PointerEvetCallBackFunc)onClick;
            SetParent(go.transform, parent);
            SetRectTransform(go, pos, prop);
        }
        go.TrySetActive(isShow);
    }
    /// <summary>
    /// 只是显示图片，不接受点击事件
    /// </summary>
    public static void ShowSprite(ref GameObject go, string name, Sprite sprite, Vector2 pos, bool isShow, RectTransformProp prop = null, Transform parent = null, bool force = false,
        Image.Type spriteType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
    {
        if (!isShow && go == null) return;
        if (sprite == null)
        { 
            if (go == null && isShow && force)
            {
                go = new GameObject(name);
                SetParent(go.transform, parent);
            }
            if (go != null)
            {
                go.TrySetActive(false);
            }
            return; 
        }
        if (go == null && isShow)
        {
            go = new GameObject(name);
            SetParent(go.transform, parent);
            SetSprite(go, sprite, pos, prop, spriteType, fillMethod);
        }
        else if(go != null && isShow && force)
        {
            go.name = name;
            SetParent(go.transform, parent);
            SetSprite(go, sprite, pos, prop, spriteType, fillMethod);
        }
        go.TrySetActive(isShow);
    }
    /// <summary>
    /// 只是显示图片，不接受点击事件
    /// </summary>
    public static void ShowSprite(ref GameObject go, string name, string atlas, string sprite, Vector2 pos, bool isShow, RectTransformProp prop = null, Transform parent = null, bool force = false,
        Image.Type spriteType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
    {
        Sprite img = ResourceManager.LoadSprite(atlas, sprite);
        ShowSprite(ref go, name, img, pos, isShow, prop, parent, force, spriteType, fillMethod);
    }
    /// <summary>
    /// 显示文字
    /// </summary>
    public static Text ShowText(ref GameObject go, string msg, bool isShow = true, Vector2 pos = default(Vector2), TextProp textProp = null, Transform parent = null, bool force = false)
    {
        if (!isShow && go == null) return null;
        Text label_txt = null;
        if (go == null && isShow)
        {
            go = new GameObject("Label");
            label_txt = go.AddComponent<Text>();
            SetParent(label_txt.transform, parent);
            label_txt.font = ResourceManager.LoadFont();
            label_txt.fontSize = 20;
            label_txt.color = Color.white;
            label_txt.alignment = TextAnchor.MiddleCenter;
            //label_txt.horizontalOverflow = textProp.horizontalWrapMode;
            //label_txt.verticalOverflow = textProp.verticalWrapMode;
            SetText(go, label_txt, pos, textProp);
        }
        else if (go != null && isShow && force)
        {
            label_txt = go.AddMissingComponent<Text>();
            SetText(go, label_txt, pos, textProp);
        }
        else
        {
            label_txt = go.AddMissingComponent<Text>();
        }

        label_txt.text = msg;
        go.TrySetActive(isShow);

        return label_txt;
    }

    /// <summary>
    /// 对齐显示文本
    /// </summary>
    /// <param name="go"></param>
    /// <param name="msg"></param>
    /// <param name="isShow"></param>
    /// <param name="pos"></param>
    /// <param name="textProp"></param>
    /// <param name="parent"></param>
    /// <param name="force"></param>
    /// <param name="anchorAlign"></param>
    public static void ShowTextAlign(ref GameObject go, string msg, bool isShow = true, Vector2 pos = default(Vector2), TextProp textProp = null, Transform parent = null, bool force = false, TextAnchor anchorAlign = TextAnchor.MiddleCenter)
    {
        if (!isShow && go == null) return;
        if (go == null && isShow)
        {
            go = new GameObject("Label");
            Text label_txt = go.AddComponent<Text>();
            Vector2 vec = parent.gameObject.GetComponent<RectTransform>().sizeDelta;
            SetParent(label_txt.transform, parent);
            label_txt.font = ResourceManager.LoadFont();
            label_txt.fontSize = 20;
            label_txt.color = new Color(27 / 255f, 171 / 255f, 48 / 255f);
            //label_txt.color = Color.white;
            label_txt.alignment = anchorAlign;
            label_txt.horizontalOverflow = HorizontalWrapMode.Overflow;
            label_txt.verticalOverflow = VerticalWrapMode.Overflow;
            SetText(go, label_txt, pos, textProp);
        }
        else if (go != null && isShow && force)
        {
            var label_txt = go.AddMissingComponent<Text>();
            SetText(go, label_txt, pos, textProp);
        }
        go.GetComponent<Text>().text = msg;
        go.TrySetActive(isShow);
        go.GetComponent<RectTransform>().sizeDelta = parent.gameObject.GetComponent<RectTransform>().sizeDelta;
    }

    private static void SetText(GameObject go, Text label_txt, Vector2 pos = default(Vector2), TextProp textProp = null)
    {
        RectTransformProp prop = null;
        if (textProp != null)
        {
            label_txt.fontSize = textProp.fontSize;
            label_txt.alignment = textProp.anchor;
            label_txt.color = textProp.color;
            label_txt.horizontalOverflow = textProp.horizontalWrap;
            label_txt.verticalOverflow = textProp.verticalWrap;
            prop = textProp.rect;
        }
        SetRectTransform(go, pos, prop);
    }

    public static void SetParent(Transform trans, Transform parent = null)
    {
        if (trans == null || trans.parent == parent) return;
        trans.SetParent(parent);
        trans.localScale = Vector3.one;
    }

    public static void SetSprite(GameObject go, Sprite sprite, Vector2 pos = default(Vector2), RectTransformProp prop = null, Image.Type spriteType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
    {
        if (go == null) return;
        Image img = go.AddMissingComponent<Image>();
        img.SetSprite(sprite);
        if(img.type != spriteType)
            img.type = spriteType;
        if (img.fillMethod != fillMethod)
            img.fillMethod = fillMethod;
        SetRectTransform(go, pos, prop, img);
    }

    public static void SetRectTransform(GameObject go, Vector2 pos = default(Vector2), RectTransformProp prop = null, Image img = null)
    {
        if (go == null) return;
        RectTransform rectTransform = go.AddMissingComponent<RectTransform>();
        Vector2 pivot = HALF_X_HALF_Y_VEC;
        Vector2 min = HALF_X_HALF_Y_VEC;
        Vector2 max = HALF_X_HALF_Y_VEC;
        Vector2 size = Vector2.zero;
        Vector3 scale = Vector3.one;
        Vector3 rotation = Vector3.zero;
        int siblingIndex = -1;
        if (prop != null)
        {
            pivot = prop.pivot;
            min = prop.min;
            max = prop.max;
            size = prop.size;
            scale = prop.scale;
            rotation = prop.rotation;
            siblingIndex = prop.siblingIndex;
        }
        rectTransform.pivot = pivot;
        rectTransform.anchorMin = min;
        rectTransform.anchorMax = max;
        rectTransform.localScale = scale;
        rectTransform.eulerAngles = rotation;
        rectTransform.anchoredPosition = pos;
        if (siblingIndex != -1)
        {
            rectTransform.SetSiblingIndex(siblingIndex);
        }
        if (size == null || size == Vector2.zero)
        {
            if (img != null)
            {
                var rect = img.sprite.rect;
                var sizeDelta = rectTransform.sizeDelta;
                if(rect.width != sizeDelta.x || rect.height != sizeDelta.y)
                {
                    //Logger.Error("sizedelta: " + rectTransform.sizeDelta + ", img w: " + rect.width + ", h: " + rect.height);
                    img.SetNativeSize();
                }
            }
            else
                rectTransform.sizeDelta = new Vector2(100, 100);
        }
        else rectTransform.sizeDelta = size;
    }
    #endregion
}

#region UI辅助类
public class RectTransformProp
{
    public Vector2 min = UIHelper.HALF_X_HALF_Y_VEC;
    public Vector2 max = UIHelper.HALF_X_HALF_Y_VEC;
    public Vector2 pivot = UIHelper.HALF_X_HALF_Y_VEC;
    public Vector3 scale = Vector3.one;
    public Vector2 size = Vector2.zero;
    public Vector3 rotation = Vector3.zero;
    public int siblingIndex = -1;

    public void reset()
    {
        min = UIHelper.HALF_X_HALF_Y_VEC;
        max = UIHelper.HALF_X_HALF_Y_VEC;
        pivot = UIHelper.HALF_X_HALF_Y_VEC;
        scale = Vector3.one;
        size = Vector2.zero;
        rotation = Vector3.zero;
        siblingIndex = -1;
    }
}

public class TextProp
{
    public int fontSize = 20;
    public Color color = Color.white;
    public TextAnchor anchor = TextAnchor.MiddleCenter;
    public RectTransformProp rect = null;
    public VerticalWrapMode verticalWrap = VerticalWrapMode.Overflow;

    private HorizontalWrapMode _horizontalWrapMode = HorizontalWrapMode.Overflow;
    public HorizontalWrapMode horizontalWrap
    {

        get { return _horizontalWrapMode; }
        set {  _horizontalWrapMode = value; }
    }
    

}
#endregion
