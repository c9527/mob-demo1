﻿using UnityEngine;
using System.Collections;

public class Datou : MonoBehaviour {

    private Transform transHead;
    public float headSize = 1.0f;


	void Start () {

        transHead = transform.Find2("Bip_Head");	
	}
	
    void LateUpdate()
    {
        transHead.localScale *= headSize;
    }
}
