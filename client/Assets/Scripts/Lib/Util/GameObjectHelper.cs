﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameObjectHelper
{
    static public Transform FindChildByTraverse(Transform transFrom, string strName, bool ignoreCase = false)
    {
        if (transFrom == null)
            return null;

        Transform transRet = null;
        Transform transChild = null;

        int iChildCnt = transFrom.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transFrom.GetChild(i);

            if (ignoreCase == false)
            {
                if (transChild.name == strName)
                {
                    transRet = transChild;
                    break;
                }
            }
            else
            {
                if(transChild.name.ToLower() == strName.ToLower())
                {
                    transRet = transChild;
                    break;
                }
            }

            Transform goTemp = FindChildByTraverse(transChild, strName, ignoreCase);
            if (goTemp != null)
            {
                transRet = goTemp;
                break;
            }
        }

        return transRet;
    }

    static private void VisitChild(Transform transFrom, Action<GameObject> funCallBack)
    {
        if (transFrom == null)
            return ;

        Transform transChild = null;

        int iChildCnt = transFrom.transform.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transFrom.transform.GetChild(i);
            funCallBack(transChild.gameObject);
            VisitChild(transChild, funCallBack);            
        }
    }

    static public void VisitChildByTraverse(Transform transform, Action<GameObject> funCallBack, bool includeSelf = false)
    {
        if (includeSelf)
            funCallBack(transform.gameObject);

        VisitChild(transform, funCallBack);
    }

    static public void FindChildrenByComponent<T>(Transform transParent, out List<GameObject> listRet) where T : Component
    {
        listRet = new List<GameObject>();

        if (transParent == null)
            return;

        Transform transChild = null;

        int iChildCnt = transParent.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transParent.GetChild(i);

            T kCompoent = transChild.gameObject.GetComponent<T>();
            if (kCompoent != null)
            {
                listRet.Add(transChild.gameObject);
            }

            FindChildrenByComponent<T>(transChild, out listRet);
        }
    }

    static public void GetComponentsByTraverse<T>(Transform transParent, ref List<T> listRet) where T : Component
    {
        if (transParent == null)
            return;

        T kCompoent = null;
        kCompoent = transParent.gameObject.GetComponent<T>();
        if(kCompoent != null)
            listRet.Add(kCompoent);

        Transform transChild = null;
        int iChildCnt = transParent.childCount;
        for (int i = 0; i < iChildCnt; i++)
        {
            transChild = transParent.GetChild(i);

            //kCompoent = transChild.gameObject.GetComponent<T>();
            //if (kCompoent != null)
            //{
            //    listRet.Add(kCompoent);
            //}

            GetComponentsByTraverse<T>(transChild, ref listRet);
        }
    }

    static public T GetComponentsByTraverse<T>(GameObject go) where T : Component
    {
        if (go == null)
            return null;

        T kCompoent = null;
        kCompoent = go.GetComponent<T>();
        if (kCompoent != null)
            return kCompoent;

        Transform transParent = go.transform;
        int iChildCnt = transParent.childCount;

        for (int i = 0; i < iChildCnt; i++)
        {
            GameObject goChild = transParent.GetChild(i).gameObject;
            kCompoent = goChild.GetComponent<T>();
            if (kCompoent != null)
                return kCompoent;

            T ret = GetComponentsByTraverse<T>(goChild);
            if (ret != null)
                return ret;
        }

        return null;
    }

    /// <summary>
    /// 从GameObject上获取组件，如果没有就Add
    /// </summary>
    static public T GetComponent<T>(GameObject go) where T : Component
    {
        if (go == null)
            return null;

        T kComponent = go.GetComponent<T>();
        if (kComponent == null)
            kComponent = go.AddComponent<T>();

        return kComponent;
    }

    /// <summary>
    /// 找父结点下的子结点，如果没有就创建一个，这里不遍历，只找第一层下的结点
    /// </summary>
    static public GameObject GetChildGameObjectFirstLayer(Transform transParent, string strChildName, bool bCreateIfNotFound=false)
    {
        if (transParent == null)
            return null;

        Transform transChild = transParent.FindChild(strChildName);
        if (transChild == null && bCreateIfNotFound)
        {
            GameObject goChild = new GameObject();
            goChild.name = strChildName;
            goChild.transform.parent = transParent;
            transChild = goChild.transform;
        }

        transChild.localPosition = Vector3.zero;
        transChild.localEulerAngles = Vector3.zero;
        transChild.localScale = Vector3.one;

        return transChild.gameObject;
    }

    static public GameObject CreateChildIfNotExisted(Transform transParent, string strChildName)
    {
        if (transParent == null)
            return null;

        GameObject goChild;
        Transform kTrans = transParent.Find(strChildName);
        if(kTrans == null)
        {
            goChild = new GameObject(strChildName);
            goChild.transform.SetParent(transParent);
        }
        else
        {
            goChild = kTrans.gameObject;
        }

        return goChild;
    }
   
    public static string GetHierarchyPath(GameObject go)
    {
        if (go == null)
            return "";
        var path = go.name;
        var tran = go.transform.parent;
        while (tran != null)
        {
            path = string.Format("{0}/{1}",tran.name, path);
            tran = tran.parent;
        }
        return path;
    }

    static public string GetNameWithoutClone(string clonePrefabName)
    {
        if (string.IsNullOrEmpty(clonePrefabName))
            return "";

        int index = clonePrefabName.LastIndexOf("(Clone)");
        if(index >= 0)
            return clonePrefabName.Substring(0, index);

        return clonePrefabName;
    }

    class MeshData
    {
        public CombineInstance ci;
        public MeshFilter mf;
        public MeshRenderer mr;
    }

    /// <summary>
    /// active = false的物体，不会被Combine 
    /// </summary>
    static public List<GameObject> CombineMesh(Transform root)
    {
        if (root == null)
            return null;

        List<GameObject> listCombineMesh = new List<GameObject>();
        Dictionary<Material, List<MeshData>> dic = new Dictionary<Material, List<MeshData>>();

        root.VisitChild((child) =>
        {
            if (child.activeInHierarchy == false || child.name.StartsWith("CombineMesh_"))
                return;

            MeshFilter mf = child.GetComponent<MeshFilter>();
            MeshRenderer mr = child.GetComponent<MeshRenderer>();
            if(mf != null && mr != null)
            {
                MeshData md = new MeshData();
                md.mf = mf;
                md.mr = mr;
                md.ci = new CombineInstance();
                md.ci.mesh = mf.sharedMesh;
                md.ci.transform = mf.transform.localToWorldMatrix;

                List<MeshData> listCI = null;
                Material mat = mr.sharedMaterial;
                dic.TryGetValue(mat, out listCI);
                if(listCI == null)
                {
                    listCI = new List<MeshData>();
                    dic.Add(mat, listCI);
                }

                listCI.Add(md);
            }
        }, true);

        foreach (KeyValuePair<Material, List<MeshData>> kvp in dic)
        {
            List<MeshData> listMD = kvp.Value;

            if (listMD.Count <= 1)
                continue;

            CombineInstance[] arrCI = new CombineInstance[listMD.Count];
            
            for (int i = 0; i < listMD.Count; i++)
            {
                arrCI[i] = listMD[i].ci;
            }

            Mesh cm = new Mesh();
            cm.name = "CombineMesh_" + kvp.Key.name;
            cm.CombineMeshes(arrCI);

            for (int i = 0; i < listMD.Count; i++)
            {
                listMD[i].mr.enabled = false;
            }

            GameObject goCombine = new GameObject();
            goCombine.name = "CombineMesh_" + kvp.Key.name;
            MeshFilter mf = goCombine.AddComponent<MeshFilter>();
            MeshRenderer mr = goCombine.AddComponent<MeshRenderer>();
            mf.sharedMesh = cm;
            mr.sharedMaterial = kvp.Key;
            mr.useLightProbes = true;

            Transform trans = goCombine.transform;
            trans.SetParent(root);
            trans.position = Vector3.zero;
            trans.eulerAngles = Vector3.zero;

            listCombineMesh.Add(goCombine);
        }

        return listCombineMesh;
    }

    static public void SetSceneItemPos(GameObject go, float[] pos, float[] scale)
    {
        if (go == null)
            return;

        Transform trans = go.transform;
        trans.position = new Vector3(pos[0], pos[1], pos[2]);
        trans.eulerAngles = new Vector3(pos[3], pos[4], pos[5]);
        trans.localScale = new Vector3(scale[0], scale[1], scale[2]);
    }

    static public void RefreshMaterial(Transform root)
    {

#if UNITY_EDITOR

        if (root == null)
            return;

        Dictionary<Material, List<Renderer>> dic = new Dictionary<Material, List<Renderer>>();

        root.VisitChild((child) =>
        {
            Renderer render = child.GetComponent<Renderer>();
            if (render == null || render.sharedMaterial == null)
                return;

            List<Renderer> list;
            if (dic.TryGetValue(render.sharedMaterial, out list) == false)
            {
                list = new List<Renderer>();
                dic.Add(render.sharedMaterial, list);
            }

            list.Add(render);

        }, true);

        foreach (KeyValuePair<Material, List<Renderer>> kvp in dic)
        {
            Material newMat = new Material(kvp.Key);
            for (int i = 0; i < kvp.Value.Count; i++)
            {
                kvp.Value[i].sharedMaterial = newMat;
            }
        }
#endif
        
    }

    static public void UseLightProbe(Transform root)
    {
        if (root == null)
            return;

        Renderer[] arrR = root.GetComponentsInChildren<Renderer>();
        for(int i=0; i<arrR.Length; i++)
        {
            arrR[i].useLightProbes = true;
            arrR[i].castShadows = false;
            arrR[i].receiveShadows = false;
        }
    }

}