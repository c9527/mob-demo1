﻿using UnityEngine;


public static class VectorHelper
{
    public const string DIR_FORWARD = "forward";
    public const string DIR_BACK = "back";
    public const string DIR_LEFT = "left";
    public const string DIR_RIGHT = "right";



    static public float[] ToArray(this Vector3 vec3)
    {
        float[] arr = new float[] { vec3.x, vec3.y, vec3.z };

        return arr;
    }

    static public Vector3 ToVector3( this float[] arr)
    {
        if (arr.Length < 3)
        {
            //Logger.Warning("ArrayToVector3 arr.length < 3");
            return Vector3.zero;
        }

        Vector3 vec3 = new Vector3(arr[0], arr[1], arr[2]);
        return vec3;
    }

    static public float XZLength(this Vector3 vec3)
    {
        return Mathf.Sqrt(vec3.x * vec3.x + vec3.z * vec3.z);
    }

    /// <summary>
    /// 求目标方向在X象限中的方位
    /// </summary>
    /// <param name="face">朝向</param>
    /// <param name="dir2">从当前坐标到目标点的方向</param>
    /// <returns></returns>
    static public string GetDir(Vector3 face, Vector3 dir2)
    {
        string dirName = "";
        face.y = 0;
        dir2.y = 0;

        face.Normalize();
        dir2.Normalize();
        float angle = Vector3.Angle(face, dir2);

        if (angle <= 45)
            dirName = DIR_FORWARD;
        else if (angle >= 135)
            dirName = DIR_BACK;
        else
        {
            Vector3 corssDir = Vector3.Cross(face, dir2);
            if (corssDir.y > 0)
                dirName = DIR_RIGHT;
            else
                dirName = DIR_LEFT;
        }

        return dirName;
    }

    static public Vector3 NormalizeXZ(Vector3 vec)
    {
        vec.y = 0;
        vec.Normalize();
        return vec;
    }

    static public float AngleXZ(Vector3 dir1, Vector3 dir2)
    {
        dir1.y = 0;
        dir2.y = 0;
        return Vector3.Angle(dir1, dir2);
    }

    static public float Angle360(Vector3 dir1, Vector3 dir2)
    {
        Vector3 v = Vector3.Cross(dir1, dir2);
        float angle = Vector3.Angle(dir1, dir2);
        if (v.z > 0)
        {
            return angle;
        }
        else
        {
            return 360 - angle;
        }
    }

}