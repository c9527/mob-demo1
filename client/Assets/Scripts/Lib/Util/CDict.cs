﻿using System;
using System.Collections.Generic;
using System.IO;

public class CDict : Dictionary<string, object>
{
    public static CDict Single(string name, object value)
    {
        CDict set = new CDict();
        set.Add(name, value);
        return set;
    }

    public static CDict FromDict<T>(Dictionary<string, T> dict)
    {
        CDict set = new CDict();
        set.Merge(dict);
        return set;
    }

    public object Get(string key, object def)
    {
        object obj;
        if (TryGetValue(key, out obj))
            return obj;
        return def;
    }

    public string Str(string key, string def = "")
    {
        object value = Get(key, def);
        if (value == null)
            return null;
        return value.ToString();
    }

    public int Int(string key, int def = 0)
    {
        return Convert.ToInt32(Get(key, def));
    }

    public uint UInt(string key, uint def = 0)
    {
        return Convert.ToUInt32(Get(key, def));
    }

    public long Int64(string key, long def = 0)
    {
        return Convert.ToInt64(Get(key, def));
    }

    public float Float(string key, float def = 0)
    {
        return Convert.ToSingle(Get(key, def));
    }

    public bool Bool(string key, bool def = false)
    {
        object o = Get(key, def);
        if (!(o is Boolean))
            return def;
        return (bool)o;
    }

    public CDict Dict(string key)
    {
        var dic = Get(key, null) as Dictionary<string, object>;
        if (dic == null)
            return null;
        if (dic is CDict)
            return dic as CDict;
        return CDict.FromDict(dic);
    }

    public Array Ary(string key)
    {
        return Get(key, null) as Array;
    }

    public T[] Ary<T>(string key)
    {
        Array ary = Ary(key);
        if (ary == null)
            return new T[0];    // lua+json 会导致无法辨别空对象和空数组，这里直接返回空数组
        T[] ret = new T[ary.Length];
        for (int i = 0; i < ary.Length; i++)
        {
            object v = ary.GetValue(i);
            if (typeof(T) == typeof(CDict))
                v = CDict.FromDict(v as Dictionary<string, object>);
            else if (typeof(T) == typeof(int))
                v = Convert.ToInt32(v);
            else if (typeof(T) == typeof(long))
                v = Convert.ToInt64(v);
            else if (typeof(T) == typeof(float))
                v = Convert.ToSingle(v);
            else if (typeof(T) == typeof(string))
                v = v.ToString();
            else if (typeof(T) == typeof(bool))
                v = (bool)v;
            ret.SetValue(v, i);
        }
        return ret;
    }

    public void Merge<T>(Dictionary<string, T> dict)
    {
        foreach (string key in dict.Keys)
            this[key] = dict[key];
    }

    public override string ToString()
    {
        string data = "";
        foreach (string key in Keys)
            data += string.Format("  {0}={1}\r\n", key, CJsonHelper.Save(this[key]));
        return data;
    }
}
