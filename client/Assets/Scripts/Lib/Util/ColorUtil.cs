using System.Text;
using UnityEngine;

public class ColorUtil 
{
    public static readonly Color NormalTextColor = HexToColor(0x5e3310ff);
    public static readonly Color TitleTextColor = HexToColor(0xffdc7bff);

    public enum ColorEnum
    {
        None = 0,
        White,
        Green,
        DrakGreen,
        Blue,
        Purple,
        Orange,
        Darkgold,
        Red,
        Yellow,
        Gray,
        NormalText = 100,
        TitleText,
    }

    /// <summary>
    /// 转化对应的Color至RGB字符串形式，如“FFFFFF”
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static string ColorToHex(Color color)
    {
        int retVal = 0xFFFFFF & (ColorToInt(color) >> 8);
        return DecimalToHex(retVal);
    }

    public static string ColorToString(Color color)
    {
        StringBuilder kSB = new StringBuilder();
        kSB.Append("#");
        kSB.Append(((int)(color.r * 255)).ToString("X2"));
        kSB.Append(((int)(color.g * 255)).ToString("X2"));
        kSB.Append(((int)(color.b * 255)).ToString("X2"));
        kSB.Append(((int)(color.a * 255)).ToString("X2"));
        return kSB.ToString();
    }

    /// <summary>
    /// 转化对应的Color至RGB字符串形式，如“FFFFFF”
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static string ColorToHex(ColorEnum color, bool prefix = false)
    {
        string strColor = ColorToHex(GetColor(color));
        return string.Concat(prefix ? "#" : "", strColor);
    }

    /// <summary>
    /// 根据Color的Index获取常用的Color
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static Color GetColor(ColorEnum color)
    {
        Color c = NormalTextColor;
        switch (color)
        {
            case ColorEnum.White://白色
                c = HexToColor(0xFFFFFFFF);
                break;
            case ColorEnum.Green://绿色
                c = HexToColor(0x2CFF16FF);
                break;
            case ColorEnum.Blue://蓝色
                c = HexToColor(0x00BAFFFF);
                break; ;
            case ColorEnum.Purple://紫色
                c = HexToColor(0xFF0066FF);
                break;
            case ColorEnum.Orange://橙色
                c = HexToColor(0xFFD800FF);
                break;
            case ColorEnum.Darkgold://暗金
                c = HexToColor(0xB8860BFF);
                break;
            case ColorEnum.Red://红色
                c = Color.red;
                break;
            case ColorEnum.Yellow://黄色
                c = Color.yellow;
                break;
            case ColorEnum.NormalText:
                c = NormalTextColor;
                break;
            case ColorEnum.TitleText:
                c = TitleTextColor;
                break;
            case ColorEnum.DrakGreen:
                c = HexToColor(0xFF006400);
                break;
            case ColorEnum.Gray:
                c = HexToColor(0x808080FF);
                break;
        }
        return c;
    }

    /// <summary>
    /// 根据Color的Index获取常用的Color
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static Color GetColor(uint colorIndex)
    {
        Color c = NormalTextColor;
        switch (colorIndex)
        {
            case 1://白色
                c = HexToColor(0xFFFFFFFF);
                break;
            case 2://绿色
                c = HexToColor(0x2CFF16FF);
                break;
            case 3://蓝色
                c = HexToColor(0x00BAFFFF);
                break; ;
            case 4://紫色
                c = HexToColor(0xFF0066FF);
                break;
            case 5://橙色
                c = HexToColor(0xFFD800FF);
                break;
            case 6://暗金
                c = HexToColor(0xFFB886FF);
                break;
            case 7://红色
                c = Color.red;
                break;
            case 8://黄色
                c = Color.yellow;
                break;
        }
        return c;
    }

    /// <summary>
    /// 根据常用的颜色Index获取颜色名称
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static string GetColorName(ColorEnum color)
    {
        string retVal = string.Empty;
        switch (color)
        {
            case ColorEnum.White:
                retVal = "白色";
                break;
            case ColorEnum.Green:
                retVal = "绿色";
                break;
            case ColorEnum.Blue:
                retVal = "蓝色";
                break; ;
            case ColorEnum.Purple:
                retVal = "紫色";
                break;
            case ColorEnum.Orange:
                retVal = "橙色";
                break;
            case ColorEnum.Darkgold:
                retVal = "暗金";
                break;
            case ColorEnum.Red:
                retVal = "红色";
                break;
            case ColorEnum.Yellow:
                retVal = "黄色";
                break;
            case ColorEnum.DrakGreen:
                retVal = "暗绿";
                break;
            case ColorEnum.Gray:
                retVal = "灰色";
                break;
        }
        return retVal;
    }

    public static Color GetRareColor(int rare)
    {
        switch (rare)
        {
            case 1: return HexToColor(0xFFFFFFFF);
            case 2: return HexToColor(0x65FF65FF);
            case 3: return HexToColor(0x49C1FFFF);
            case 4: return HexToColor(0xC97DFFFF);
            case 5: return HexToColor(0xFFFE34FF);
            default: return HexToColor(0xFFFFFFFF);
        }
    }

    public static string GetRareColorName(int rare)
    {
        switch (rare)
        {
            case 1: return "white";
            case 2: return "green";
            case 3: return "blue";
            case 4: return "purple";
            case 5: return "gold";
            default: return "white";
        }
    }

    public static string GetStringWithColor(string name, ColorEnum color = ColorEnum.NormalText)
    {
        return "[" + ColorToHex(GetColor(color)) + "]" + name + "[-]";
    }

    public static string GetStringWithColorIndex(string name, uint index)
    {
        return "[" + ColorToHex(GetColor(index)) + "]" + name + "[-]";
    }

    public static string GetStringNormalColor(string name)
    {
        return "[" + ColorToHex(NormalTextColor) + "]" + name+"[-]";
    }

    public static string GetStringTitleColor(string name)
    {
        return "[" + ColorToHex(TitleTextColor) + "]" + name + "[-]";
    }

    public static int ColorToInt(Color c)
    {
        int retVal = 0;
        retVal |= Mathf.RoundToInt(c.r * 255f) << 24;
        retVal |= Mathf.RoundToInt(c.g * 255f) << 16;
        retVal |= Mathf.RoundToInt(c.b * 255f) << 8;
        retVal |= Mathf.RoundToInt(c.a * 255f) ;
        return retVal;             
    }

    public static Color HexToColor(uint val)
    {
        return IntToColor((int)val);
    }

    public static Color IntToColor(int val)
    {
        float inv = 1f / 255f;
        Color c = Color.black;
        c.r = inv * ((val >> 24) & 0xFF);
        c.g = inv * ((val >> 16) & 0xFF);
        c.b = inv * ((val >> 8) & 0xFF);
        c.a = inv * (val & 0xFF);
        return c;
    }

    public static string DecimalToHex(int num)
    {
        return num.ToString("X6");
    }
}
