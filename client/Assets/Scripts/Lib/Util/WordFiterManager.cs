﻿using System.Text.RegularExpressions;
using UnityEngine;
using System;
static class WordFiterManager
{
    private static string[] m_wordFiterArr;
    private static string[] m_wordCheatFiterArr;//聊天欺骗过滤
    private static Regex[] m_regHtmlSigns;    //需要去除的html标记
    private static string[] m_htmlSigns;    //需要去除的html标记

    //private static string url = "file://" + Application.dataPath + "/auto_reg.json";
    private const string JASON_LAST_LOAD_TIME = "jason_last_load_time";
    private const string JASON_REGEX_STRING = "jason_regex_string";
    private const string JASON_REGEX_CONFIG_SIGN = "jason_regex_config_sign";
    

    public static void Init()
    {
        ResourceManager.LoadTextLine("Text/WordFilter", strings => m_wordFiterArr = strings);
        //ResourceManager.LoadWebData(url, LoadWebDataBack);
        InitCheatFiter();
        m_regHtmlSigns = new Regex[]
        {
            new Regex("<color=.*?>", RegexOptions.IgnoreCase | RegexOptions.Singleline), 
            new Regex("<size=.*?>", RegexOptions.IgnoreCase | RegexOptions.Singleline), 
        };

        m_htmlSigns = new string[] { "</color>", "</size>", "<i>", "</i>", "<b>", "</b>" };
    }

    private static void LoadJasonData()
    {
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("jason_regex_data_url");
        if (cfg == null) return;
        string vstr =cfg.ValueStr+"?v=";
        cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("jason_regex_data");
        if (cfg != null)
            vstr += cfg.ValueInt + "s";
        vstr += TimeUtil.GetNowTimeStamp() + "";
        ResourceManager.LoadWebData(vstr, LoadWebDataBack);
    }

    /// <summary>
    /// 初始化监控字符串
    /// </summary>
    private static void InitCheatFiter()
    {
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("jason_regex_data");
        if (cfg != null && cfg.ValueInt == 0) return;//不启用聊天监控
        if (PlayerPrefs.HasKey(JASON_LAST_LOAD_TIME))
        {
            int loadTime = PlayerPrefs.GetInt(JASON_LAST_LOAD_TIME, -1);
            if (loadTime == -1)
            {
                LoadJasonData();
                return;
            }
            DateTime dateTime = TimeUtil.GetTime(loadTime);
            DateTime nowTime = TimeUtil.GetNowTime();
            if (dateTime.Day != nowTime.Day || dateTime.Year != nowTime.Year || dateTime.Month != nowTime.Month)
            {
                //
                LoadJasonData();
            }
            else
            {
                if (PlayerPrefs.HasKey(JASON_REGEX_CONFIG_SIGN))
                {

                    if (cfg != null)
                    {
                        if (cfg.ValueInt != PlayerPrefs.GetInt(JASON_REGEX_CONFIG_SIGN, -1)) { LoadJasonData(); return; }
                    }
                }
                else
                {
                    LoadJasonData(); return;
                }

                //同一天直接读取本来的缓存数据
                if (PlayerPrefs.HasKey(JASON_REGEX_STRING))
                {
                    string regexStr = PlayerPrefs.GetString(JASON_REGEX_STRING, "");
                    if (regexStr == "")
                    {
                        LoadJasonData();
                        return;
                    }
                    parseWebJasonRegexData(WWW.UnEscapeURL(regexStr));
                }
                else
                    LoadJasonData();
            }
        }
        else
            LoadJasonData();

    }

    /// <summary>
    /// 加载返回
    /// </summary>
    /// <param name="str"></param>
    private static void LoadWebDataBack(string str)
    {
        PlayerPrefs.SetInt(JASON_LAST_LOAD_TIME, TimeUtil.GetNowTimeStamp());
        PlayerPrefs.SetString(JASON_REGEX_STRING, WWW.EscapeURL(str));
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("jason_regex_data");
        if (cfg != null) PlayerPrefs.SetInt(JASON_REGEX_CONFIG_SIGN, cfg.ValueInt);
        PlayerPrefs.Save();
        parseWebJasonRegexData(str);
    }

    /// <summary>
    /// 解析PHP的正则字符串
    /// </summary>
    /// <param name="str"></param>
    private static void parseWebJasonRegexData(string str)
    {
        if (string.IsNullOrEmpty(str)) return;
        string tempStr = str.Replace("\r\n", "");
        tempStr = tempStr.Replace("[{\"reg", "{\"reg");
        string[] strAry = tempStr.Split(new string[] { "{\"reg\":\"" }, System.StringSplitOptions.RemoveEmptyEntries);
        int lngth = strAry.Length;
        m_wordCheatFiterArr = new string[lngth];
        for (int i = 0; i < lngth; i++)
        {
            if (i == lngth - 1)
                m_wordCheatFiterArr[i] = strAry[i].Replace("\"}],", "");
            else
                m_wordCheatFiterArr[i] = strAry[i].Replace("\"},", "");
            m_wordCheatFiterArr[i] = m_wordCheatFiterArr[i].Replace("\\/u", "");
            m_wordCheatFiterArr[i] = m_wordCheatFiterArr[i].Replace("\\/", "");
        }
    }
    /// <summary>
    /// 过滤欺诈字符串
    /// </summary>
    /// <returns></returns>
    public static bool isFiterCheat(string content)
    {
        bool isCheat = false;
        if (string.IsNullOrEmpty(content)) return isCheat;
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("jason_regex_data");
        if (cfg == null) return isCheat;
        if (cfg.ValueInt == 0) return isCheat;
        if (m_wordCheatFiterArr == null) return isCheat;
        Regex reg;
        foreach (string temp in m_wordCheatFiterArr)
        {
            reg = new Regex(temp);
            if (reg.IsMatch(content))
            {
                isCheat = true;
                break;
            }
        }
        return isCheat;
    }

    public static string Fiter(string content)
    {
        if (m_wordFiterArr == null)
            return content;

        for (int i = 0; i < m_htmlSigns.Length; i++)
        {
            var start = content.IndexOf(m_htmlSigns[i], StringComparison.OrdinalIgnoreCase);
            if (start != -1)
                content = content.Remove(start, m_htmlSigns[i].Length);
        }

        for (int i = 0; i < m_regHtmlSigns.Length; i++)
        {
            content = m_regHtmlSigns[i].Replace(content, "");
        }

        if (content.IndexOf("<color>") != -1)
            content = content.Replace("<color>", "");
        for (int i = 0; i < m_wordFiterArr.Length; i++)
        {
            var start = content.IndexOf(m_wordFiterArr[i], StringComparison.OrdinalIgnoreCase);
            if (start != -1)
            {
                content = content.Remove(start, m_wordFiterArr[i].Length);
                content = content.Insert(start, "*");
            }
        }
        return content;
    }

    public static void Destroy()
    {
        m_wordFiterArr = null;
    }
}