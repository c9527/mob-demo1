﻿using UnityEngine;
using System;


public class StepUpdate
{
    private Action<float> m_funStepUpdate;
    private Action m_funComplete;

    private EptFloat m_fTargetValue;
    private EptFloat m_fInitValue;
    private EptFloat m_fSpeed;

    private EptFloat m_fTick;
    private bool m_bRun;


    public void Release()
    {
        m_funComplete = null;
        m_funStepUpdate = null;
    }

    public void SetData1(float initValue, float targetValue, float speed)
    {
        m_fInitValue = initValue;
        m_fTargetValue = targetValue;
        m_fSpeed = speed;
    }

    public void SetData2(float initValue, float targetValue, float time)
    {
        m_fInitValue = initValue;
        m_fTargetValue = targetValue;
        if(time <= 0 )
            m_fSpeed = 999999999;
        else
            m_fSpeed = (m_fTargetValue - m_fInitValue) / time;
    }

    public void Start()
    {
        m_fTick = m_fInitValue;
        m_bRun = true;
    }

    public void SetCallBack(Action<float> funStepUpdate, Action funComplete)
    {
        m_funStepUpdate = funStepUpdate;
        m_funComplete = funComplete;
    }

    public void Update()
    {
        if (m_bRun == false)
            return;

        float step = Time.deltaTime * m_fSpeed;
        m_fTick += step;

        if (m_fSpeed > 0 && m_fTick >= m_fTargetValue)
        {
            if (m_funStepUpdate != null)
                m_funStepUpdate(m_fTargetValue);
            if (m_funComplete != null)
                m_funComplete();
            m_bRun = false;
            return;
        }
        if (m_fSpeed < 0 && m_fTick <= m_fTargetValue)
        {
            if (m_funStepUpdate != null)
                m_funStepUpdate(m_fTargetValue);
            if (m_funComplete != null)
                m_funComplete();
            m_bRun = false;
            return;
        }

        m_funStepUpdate(m_fTick);
    }
}
