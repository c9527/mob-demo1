﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;
using Debug = UnityEngine.Debug;
using System.Collections.Generic;


class Logger
{
#if UNITY_EDITOR
    static public bool IsSaveLog = false;    
    static public bool IsShowLog = true;   
#elif DebugErrorMode && UNITY_WEBPLAYER
    static public bool IsSaveLog = false;
    static public bool IsShowLog = true;
#elif DebugErrorMode && UNITY_IPHONE
	static public bool IsSaveLog = false;
	static public bool IsShowLog = true;
#elif DebugErrorMode
    static public bool IsSaveLog = true;
    static public bool IsShowLog = true;  
#else
    static public bool IsSaveLog = false;    
    static public bool IsShowLog = true;    
#endif

    static public bool IsShowFunCostTime = false;

    private static bool m_inited = false;


    public static void Init()
    {
        if (m_inited)
            return;
        m_inited = true;

        if (IsShowLog && IsSaveLog)
        {
            WriteLog.Init();
        }
    }

    static public void Log(string strMsg, params object[] args)
    {
        if (!IsShowLog) return;
        if (args.Length > 0)
            strMsg = string.Format(strMsg, args);

        Debug.Log(strMsg);
    }

    static public void Log(string strMsg, UnityEngine.Object context = null, params object[] args)
    {
        if (!IsShowLog) return;
        if (args.Length > 0)
            strMsg = string.Format(strMsg, args);

        if (context != null)
            Debug.Log(strMsg, context);
        else
            Debug.Log(strMsg);
    }

    static public void Warning(string strMsg, UnityEngine.Object context = null)
    {
        if (!IsShowLog) return;

        if (context != null)
            Debug.LogWarning(strMsg, context);
        else
            Debug.LogWarning(strMsg);
    }

    static public void Error(string strMsg, UnityEngine.Object context = null)
    {
        if (!IsShowLog) return;

        if (context != null)
            Debug.LogError(strMsg, context);
        else
            Debug.LogError(strMsg);

        //        var stack = new StackTrace(true).ToString();
        //        var reg = new Regex(@".*? in (.*?):line (\d+)");
        //        var match = reg.Match(stack);
        //        match = match.NextMatch();
        //        var filePath = "";
        //        var line = 0;
        //        if (match.Success)
        //        {
        //            filePath = match.Groups[1].Value;
        //            filePath = filePath.Replace('\\', '/');
        //            var index = filePath.IndexOf("Assets/", StringComparison.Ordinal);
        //            if (index > -1)
        //                filePath = filePath.Remove(0, index);
        //            line = int.Parse(match.Groups[2].Value);
        //        }
        //        typeof(Debug).GetMethod("LogPlayerBuildError", BindingFlags.Static | BindingFlags.NonPublic)
        //            .Invoke(null, new object[] { strMsg, filePath, line, 0 });
    }

    /// <summary>
    /// 断言
    /// </summary>
    /// <param name="condition"></param>
    /// <param name="message"></param>
    static public void Assert(bool condition, string message)
    {
        if (!IsShowLog) return;
        if (!condition)
        {
            Debug.LogError("[Assert]" + message);
        }
    }


    #region 方法运行时间打印
    static private Dictionary<string, DateTime> ms_dicTime = new Dictionary<string, DateTime>();

    static public void RecordTime(string name)
    {
        if (IsShowFunCostTime == false)
            return;

        if (ms_dicTime.ContainsKey(name) == false)
            ms_dicTime.Add(name, DateTime.Now);
        else
            ms_dicTime[name] = DateTime.Now;
    }

    static public void PrintTime(string name, string msg = "")
    {
        if (IsShowFunCostTime == false)
            return;

        DateTime time;
        ms_dicTime.TryGetValue(name, out time);
        if(time != null)
        {
            Logger.Log("[" + name + "] Времени потрачено (s):" + (DateTime.Now - time).TotalSeconds + " " + msg);
        }
    }

    static public void LogCostTime(Action fun)
    {
        if (fun != null)
        {
            if (IsShowFunCostTime)
                RecordTime(fun.Method.Name);

            fun();

            if (IsShowFunCostTime)
                PrintTime(fun.Method.Name);
        }
    }

    #endregion

}

