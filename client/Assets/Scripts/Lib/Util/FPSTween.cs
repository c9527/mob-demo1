﻿using UnityEngine;
using System.Collections;
using System;


public class FPSTween : MonoBehaviour {

    private Transform m_trans;
    //private CharacterController m_cc;
    private bool m_bUpdate;
    private Action m_completeCallBack;

    private float m_targetX;
    private float m_targetY;
    private float m_targetZ;

    private float m_speedX;
    private float m_speedY;
    private float m_speedZ;

    void Awake()
    {
        m_trans = transform;
        //m_cc = GetComponent<CharacterController>();
    }

    void OnDisable()
    {
        m_bUpdate = false;
    }

    static public FPSTween GetTweenPosition(GameObject go)
    {
        if (go == null)
            return null;

        FPSTween tween = go.GetComponent<FPSTween>();
        if(tween == null)
        {
            tween = go.AddComponent<FPSTween>();
        }

        return tween;
    }

    public void TweenPosition(float duration, float targetX, float targetY, float targetZ, Action callBack = null)
    {
        m_targetX = targetX;
        m_targetY = targetY;
        m_targetZ = targetZ;
        m_completeCallBack = callBack;

        Vector3 curPos = m_trans.position;
        if(duration <= 0)
        {
            m_speedX = targetX - curPos.x;
            m_speedY = targetY - curPos.y;
            m_speedZ = targetZ - curPos.z;
        }
        else
        {
            m_speedX = (targetX - curPos.x) / duration;
            m_speedY = (targetY - curPos.y) / duration;
            m_speedZ = (targetZ - curPos.z) / duration;
        }

        if (m_speedX == 0 && m_speedY == 0 && m_speedZ == 0)
            m_bUpdate = false;
        else
            m_bUpdate = true;        
    }

    public void TweenPositionXZ(float duration, float targetX, float targetZ, Action callBack = null)
    {
        m_targetX = targetX;
        m_targetY = float.MaxValue;
        m_targetZ = targetZ;
        m_completeCallBack = callBack;

        Vector3 curPos = m_trans.position;
        if (duration <= 0)
        {
            m_speedX = targetX - curPos.x;
            m_speedY = 0;
            m_speedZ = targetZ - curPos.z;
        }
        else
        {
            m_speedX = (targetX - curPos.x) / duration;
            m_speedY = 0;
            m_speedZ = (targetZ - curPos.z) / duration;
        }

        if(m_speedX == 0 && m_speedZ == 0)
            m_bUpdate = false;
        else
            m_bUpdate = true;
    }

    private float ClampSpeed(float speed, float value, float targetValue)
    {
        if (speed > 0)
        {
            if (value <= targetValue && value + speed >= targetValue)
            {
                speed = targetValue - value;
            }
        }
        else if(speed < 0)
        {
            if (value >= targetValue && value - speed <= targetValue)
            {
                speed = targetValue - value;
            }
        }

        return speed;
    }

    void Update()
    {
        if (m_bUpdate == false)
            return;

        Vector3 curPos = m_trans.position;
        if (m_speedX > 0 && curPos.x >= m_targetX || m_speedX < 0 && curPos.x <= m_targetX ||
            m_speedY > 0 && curPos.y >= m_targetY || m_speedY < 0 && curPos.y <= m_targetY ||
            m_speedZ > 0 && curPos.z >= m_targetZ || m_speedZ < 0 && curPos.z <= m_targetZ)
        {
            m_bUpdate = false;
            if (m_completeCallBack != null)
                m_completeCallBack();
            return;
        }

        float speedX = ClampSpeed(m_speedX * Time.smoothDeltaTime, curPos.x, m_targetX);
        float speedY = ClampSpeed(m_speedY * Time.smoothDeltaTime, curPos.y, m_targetY);
        float speedZ = ClampSpeed(m_speedZ * Time.smoothDeltaTime, curPos.z, m_targetZ);

        Vector3 moveDir = new Vector3(speedX, speedY, speedZ);
        //if (m_cc == null)
            m_trans.position = curPos + moveDir;
        //else
        //{
            //m_cc.Move(moveDir);
        //}
    }          

}
