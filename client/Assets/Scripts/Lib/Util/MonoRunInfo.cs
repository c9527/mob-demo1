﻿using UnityEngine;
using System.Collections;

public class MonoRunInfo : MonoBehaviour {
    static public MonoRunInfo singleton;

    private float m_fTick = 0.0f;
    private int m_iFPS = 0;
    private int m_iTemp;

    public int m_usedMemory;
    public int m_freeMemory;
    public bool m_isLowMemory;

    private GUIStyle guiStyle;


    void Start()
    {
        singleton = this;

        guiStyle = new GUIStyle();
        guiStyle.fontSize = 18;
        guiStyle.normal.background = null;
        guiStyle.normal.textColor = Color.red;
    }

	void Update () {

        m_fTick += Time.deltaTime;

        m_iTemp++;

        if(m_fTick >= 1.0f)
        {
            m_fTick = 0.0f;
            m_iFPS = m_iTemp;
            m_iTemp = 0;
        }
	}

    void OnGUI()
    {
        GUI.Label(new Rect(10, Screen.height * 0.5f + 80, 40, 100), "FPS: " + m_iFPS.ToString() + " / " + QualityManager.m_fpsAvg.ToString(), guiStyle);
        GUI.Label(new Rect(10, Screen.height * 0.5f + 100, 40, 100), "UsedMem: " + m_usedMemory.ToString(), guiStyle);
        GUI.Label(new Rect(10, Screen.height * 0.5f + 120, 40, 100), "FreeMem: " + m_freeMemory.ToString(), guiStyle);
        GUI.Label(new Rect(10, Screen.height * 0.5f + 140, 40, 100), "IsLowMem: " + m_isLowMemory.ToString(), guiStyle);
        GUI.Label(new Rect(10, Screen.height * 0.5f + 160, 40, 100), "SMR: " + WorldManager.VisibleSkinnedMeshNum + "/" + WorldManager.singleton.allPlayer.Count, guiStyle);
        GUI.Label(new Rect(10, Screen.height * 0.5f + 180, 40, 100), "Ping: " + (int)(NetLayer.PingTime * 1000), guiStyle);
    }
}
