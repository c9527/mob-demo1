﻿using UnityEngine;
using System.Collections;

public class CheckShader : MonoBehaviour {

    static Camera _camera;
    public Texture2D _tex2D;
    public RenderTexture _rt;
    public Color _color;

    private Material m_mat;

    private float m_tick;
    public static CheckShader singleton;

    void Awake()
    {
        singleton = this;
    }

	void Start () {
        _camera = camera;
        _rt = new RenderTexture(128, 128, 16);
        _tex2D = new Texture2D(_rt.width, _rt.height);
        _camera.targetTexture = _rt;
        m_mat = gameObject.GetComponentInChildren<MeshRenderer>().material;
        m_mat.SetInt("_FuckKC", 0);
        Debug.LogWarning("CheckShader SetInt " + m_mat.GetInt("_FuckKC"));
	}

    void OnDestroy()
    {
        GameObject.Destroy(_tex2D);
        GameObject.Destroy(_rt);
        singleton = null;
    }

    public void CheckShaderTest(bool isShow)
    {
        if (singleton.GetComponent<Camera>() != null)
        {
            if(isShow == true)
            {
                singleton.GetComponent<Camera>().targetTexture = null;
            }
            else
            {
                singleton.GetComponent<Camera>().targetTexture = _rt;
            }
        }
        Debug.LogWarning("CheckShaderTest " + m_mat.GetInt("_FuckKC"));    
    }
	
	void Update () {

        Check();
	}

    void Check()
    {
       if (Time.timeSinceLevelLoad - m_tick < 10)
            return;
        m_tick = Time.timeSinceLevelLoad;

        if (_rt.IsCreated() == false)
            return;

        RenderTexture _curRT = RenderTexture.active;
        RenderTexture.active = _rt;

        _tex2D.ReadPixels(new Rect(0, 0, _tex2D.width, _tex2D.height), 0, 0);
        Color _colorZW = _tex2D.GetPixel(_tex2D.width / 4, _tex2D.height / 2);
        Color _colorCull = _tex2D.GetPixel(_tex2D.width * 3 / 4, _tex2D.height / 2);

        RenderTexture.active = _curRT;

        if (_colorZW != Color.red)
        {
            Debug.LogError("ZO!!!");
            NetLayer.CloseUdp();
        }else if(_colorCull != Color.red)
        {
            Debug.LogError("Cull!!!");
            NetLayer.CloseUdp();
        }
    }
   
}
