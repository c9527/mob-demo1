﻿using UnityEngine;
using System.Collections;
using System;


public class EffectPlugin_Move : MonoBehaviour {
    
    public float timeOut;
    public float distance;
    public float speed;
    //public bool isRelativeSpeed;

    private bool m_bPlayinging;
    private Action m_funCallBack;
    private float m_fMoveDis;
    private float m_fTick;
    private Transform m_trans;
    //private Vector3 curSpeedVec=Vector3.zero;
    //private Vector3 oldSpeedVec = Vector3.zero;
    //private Vector3 targetPos = Vector3.zero;

	void Start ()
	{
	    m_trans = gameObject.transform;
        //oldSpeedVec = m_trans.forward * speed;
        //targetPos = m_trans.position;
    }

    public void Reset()
    {
        m_funCallBack = null;
    }

    public void Play()
    {
        m_bPlayinging = true;
        m_fMoveDis = 0;
        m_fTick = 0;
        enabled = true;
        //if (m_trans != null)
        //{
        //    oldSpeedVec = m_trans.forward * speed;
        //    targetPos = m_trans.position;
        //}
    }

    public void Stop()
    {
        m_bPlayinging = false;
        enabled = false;
    }

    public void SetMoveCompleteCallBack(Action funCallBack)
    {
        m_funCallBack = funCallBack;
    }
	
    //private float GetSpeed()
    //{
    //    if (!isRelativeSpeed || MainPlayer.singleton == null)
    //    {
    //        curSpeedVec = oldSpeedVec;
    //        return speed;
    //    }
    //    Vector3 addSpeedVec = MainPlayer.singleton.moveDir * MainPlayer.singleton.moveSpeed;
    //    addSpeedVec = Vector3.Project(addSpeedVec, MainPlayer.singleton.transform.forward);
    //    curSpeedVec = oldSpeedVec + addSpeedVec;
    //    //Logger.Error("cur: " + curSpeedVec + ", add: " + addSpeedVec + ", speed: " + MainPlayer.singleton.moveSpeed);
    //    return curSpeedVec.magnitude;
    //}

	void Update ()
	{
	    if (m_bPlayinging == false)
	        return;

	    m_fTick += Time.deltaTime;
        float step = Time.deltaTime * speed;
        //float step = Time.deltaTime * GetSpeed();
	    m_fMoveDis += step;

        //if (m_fTick > timeOut || (!isRelativeSpeed && m_fMoveDis >= distance))
        if (m_fTick > timeOut || m_fMoveDis >= distance)
	    {
	        m_bPlayinging = false;
            if (m_funCallBack != null)
            {
                enabled = false;
                m_funCallBack();
            }
                
	        return;
	    }

        m_trans.position += m_trans.forward * step;
        //m_trans.position += curSpeedVec.normalized * step;
	}
}
