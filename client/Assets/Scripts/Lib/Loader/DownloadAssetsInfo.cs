﻿using UnityEngine;

public class DownloadAssetsInfo
{
    public WWW www;
    public int loadedCount;
    public int maxCount;

    public float Progress
    {
        get
        {
            var pg = (float) loadedCount/maxCount;
            if (www != null)
                pg += 1f/maxCount*www.progress;
            return Mathf.Clamp01(pg);
        }
    }
}