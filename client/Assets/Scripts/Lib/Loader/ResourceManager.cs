﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;
using System.Text.RegularExpressions;

/// <summary>
/// 资源管理类
/// 设计目的是对外部隐藏Resources加载和Assetbundle加载的区别，提供资源的加载、缓存和释放，提供易于访问的使用接口
/// </summary>
public static class ResourceManager
{
    public static ELoadMode m_loadMode;          //加载模式，在游戏启动后不可再更改
    private static MonoBehaviour m_monoDriver;                          //驱动器，提供monobehavior的update等方法特性
    private static Dictionary<string, ResourceItem> m_resourceItemDic = new Dictionary<string, ResourceItem>();  //资源缓存字典

    private static Dictionary<string, string[]> m_dependDic = new Dictionary<string, string[]>();
    private static string m_fileList;
    private static int simulateNetSpeed = 0;    //对assetbundle加载方式根据网速(KB)模拟延迟，0关闭
    private static string m_update_url;
    private static Dictionary<string, string> m_serverFileVerion;
    private static Dictionary<string, string> m_serverFileSize;
    private static Dictionary<string, string> m_outsideFileVersion; //包外资源
    private static Dictionary<string, string> m_localFileVerion;
    private static Dictionary<string, Material> m_atlasMatDic;  //采用通道分离方式的图集，缓存每张图集对应的材质 
    private static Dictionary<string, DownloadAssetsInfo> m_downloadProgressDic;  //下载组总进度字典
    private static float m_save_filelist_time = float.MaxValue;
//    private static int m_flush_timer;
    private static List<tos_player_verify_crc.info> m_verifyCrcList = new List<tos_player_verify_crc.info>();    //待验证的资源
    private static float m_verifyDelayTime = float.MaxValue;     //为了实现延迟验证，这样可以将段时间内的多条协议合并为1次
    private static string m_externalCacheDir = Application.persistentDataPath + "/cache/";
    private static Regex _externalCacheJpgRegex = new Regex(@"_(.*?\.[a-zA-Z]+)");
    

    public static string ExternalCacheDir
    {
        get
        {
            if (!Directory.Exists(m_externalCacheDir))
            {
                Directory.CreateDirectory(m_externalCacheDir);
            }
            return m_externalCacheDir;
        }
    }

#if UNITY_STANDALONE
    private static bool m_save_loaded_file = true;
#else
    private static bool m_save_loaded_file = false;
#endif

    public static void Init(MonoBehaviour monoDriver, Action callBack)
    {
        m_monoDriver = monoDriver;
        m_loadMode = ELoadMode.Resources;
        if (Global.Dic.ContainsKey("ServerVersionDic"))
            m_serverFileVerion = Global.Dic["ServerVersionDic"] as Dictionary<string, string>;
        if (Global.Dic.ContainsKey("OutsideDic"))
            m_outsideFileVersion = Global.Dic["OutsideDic"] as Dictionary<string, string>;
        if (Global.Dic.ContainsKey("ServerFileSizeDic"))
            m_serverFileSize = Global.Dic["ServerFileSizeDic"] as Dictionary<string, string>;
        if (Global.Dic.ContainsKey("LocalVersionDic"))
            m_localFileVerion = Global.Dic["LocalVersionDic"] as Dictionary<string, string>;
        if (Global.Dic.ContainsKey("UpdateUrl"))
            m_update_url = Global.Dic["UpdateUrl"] as string;
        m_atlasMatDic = new Dictionary<string, Material>();
        m_downloadProgressDic = new Dictionary<string, DownloadAssetsInfo>();
#if UNITY_EDITOR
        if (File.Exists("Assets/StreamingAssets/Depend.assetbundle"))
            m_loadMode = ELoadMode.AssetBundle;
#elif Assetbundle
        m_loadMode = ELoadMode.AssetBundle;
#endif
        if (m_loadMode == ELoadMode.AssetBundle)
        {
            LoadTextLine("Depend", lines =>
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    var arr = lines[i].Split(':');
                    var depends = arr[1].Split('|');
                    m_dependDic.Add(arr[0], depends);
                }

//                LoadText("VersionInfo/FileList.txt", s =>
//                {
//                    m_fileList = s;
//                    Logger.Log(s);
//                });
                callBack();
            });
        }
        else
        {
            callBack();
        }
    }

    /// <summary>
    /// 手动添加添加ResourceItem对象
    /// </summary>
    /// <param name="items"></param>
    public static void AddResourceItems(ResourceItem[] items)
    {
        if (items == null)
            return;
        for (int i = 0; i < items.Length; i++)
        {
            if (!m_resourceItemDic.ContainsKey(items[i].Path))
                m_resourceItemDic.Add(items[i].Path, items[i]);
        }
    }

    /// <summary>
    /// 直接读取缓存资源，需要异步加载资源成功后才能使用，否则返回null
    /// </summary>
    /// <param name="path">相对于Asset/路径下的资源路径</param>
    /// <returns></returns>
    [Obsolete("暂不开放使用", true)]
    private static Object Load(string path)
    {
        if (!m_resourceItemDic.ContainsKey(path))
            m_resourceItemDic.Add(path, new ResourceItem(path));

        var item = m_resourceItemDic[path];
        return item.MainAsset;
    }


    public static void LoadCacheSprite(string url, Action<Sprite> callback)
    {
#if UNITY_ANDROID || UNITY_IPHONE
        GetCacheFileViaWWW(url, bytes =>
        {
            var texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            texture.LoadImage(bytes);
            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), 0.5f * Vector2.one);
            callback(sprite);
        });
#endif
    }

//    public static void CopyToCacheDir(string srcFile)
//    {
//        if (!Directory.Exists(m_externalCacheDir))
//        {
//            Directory.CreateDirectory(m_externalCacheDir);
//        }
//        File.Copy(srcFile, srcFile.Replace(Path.GetDirectoryName(srcFile), m_externalCacheDir));
//    }

    public static void GetCacheFileViaWWW(string url,  Action<byte[]> callback)
    {
#if UNITY_ANDROID || UNITY_IPHONE
        if (!url.StartsWith("http"))
        {
            url = PhotoDataManager.Instance.GetLongUrl(url);
        }
        var filePath = m_externalCacheDir + GetCacheFileName(url);
        if (File.Exists(filePath))
        {
            callback(File.ReadAllBytes(filePath));
        }
        else
        {
            Driver.singleton.StartCoroutine(DownLoadCacheFile(url, callback));
        }
#elif   UNITY_WEBPLAYER
        Driver.singleton.StartCoroutine(DownLoadCacheFile(url, callback, false));
#endif
    }

    public static string GetCacheFileName(string url)
    {
        var match = _externalCacheJpgRegex.Match(url);
        if (match.Success)
        {
            return match.Groups[1].Value;
        }
        var beginIndex = url.LastIndexOf('_');
        var endIndex = url.LastIndexOf('~');
        if (beginIndex + 1 < endIndex)
        {
            return url.Substring(beginIndex + 1, endIndex - (beginIndex + 1));
        }
        return url;
        
    }

    private static IEnumerator DownLoadCacheFile(string url, Action<byte[]> callback, bool isSaveFile = true)
    {
        var www = new WWW(url);
        yield return www;
        while (!www.isDone)
        {
            yield return null;
        }
        if (String.IsNullOrEmpty(www.error))
        {
            if (isSaveFile)
            {
                SaveFile(url, www.bytes);
            }
            callback(www.bytes);
        }
        www.Dispose();
    }

    private static void SaveFile(string url, byte[] bytes)
    {
        var filePath = ExternalCacheDir + GetCacheFileName(url);
#if !UNITY_WEBPLAYER
        File.WriteAllBytes(filePath, bytes);
#endif
    }


    /// <summary>
    /// 按行加载文本文件
    /// </summary>
    /// <param name="path"></param>
    /// <param name="callBack"></param>
    public static void LoadTextLine(string path, Action<string[]> callBack)
    {
        LoadCore(path, item =>
        {
            string[] arr = null;
            if (item.MainAsset != null)
                arr = item.MainAsset.ToString().Split(new[] {'\r','\n'}, StringSplitOptions.RemoveEmptyEntries);
            callBack(arr);
        }, EResType.Text, false, true);
    }

    /// <summary>
    /// 加载文本文件
    /// </summary>
    /// <param name="path"></param>
    /// <param name="callBack"></param>
    public static void LoadText(string path, Action<string> callBack)
    {
        LoadCore(path, item => callBack(item.MainAsset != null ? item.MainAsset.ToString() : ""), EResType.Text, false, true);
    }

    public static string LoadText(string path)
    {
        var item = LoadCore(path, EResType.Text);
        return item.MainAsset != null ? item.MainAsset.ToString() : "";
    }

    /// <summary>
    /// 加载角色模型Prefab并实例化
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
//    public static void LoadModel(string path, Action<GameObject> callBack, bool clearAfterLoaded = false)
//    {
//        LoadCore(path, item =>
//        {
//            var assetName = item.MainAsset.name;
//            InstanceAsync(item.MainAsset, go =>
//            {
//                go.name = assetName;
//#if UNITY_EDITOR
//                var arr = go.transform.GetComponentsInChildren<Renderer>();
//                for (int i = 0; i < arr.Length; i++)
//                {
//                    if (arr[i].sharedMaterial != null)
//                        arr[i].material.shader = Shader.Find(arr[i].sharedMaterial.shader.name);
//                }
//#endif
//                callBack(go);
//            });
//        }, EResType.Model, false, clearAfterLoaded);
//    }

    /// <summary>
    /// 加载战斗中用的Prefab并实例化
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    /// <param name="resType">类型</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    /// <param name="needCRC">是否需要计算CRC</param>
    public static void LoadBattlePrefab(string path, Action<GameObject, uint> callBack, EResType resType = EResType.Battle, bool clearDepend = false, bool needCRC = false)
    {
        LoadCore(path, item =>
        {
            if (item == null || item.MainAsset == null)
                return;
            var assetName = item.MainAsset.name;
            InstanceAsync(item.MainAsset, go =>
            {
				if (item.MainAsset == null)
				{
					callBack(null, 0);

					if (clearDepend)
						ClearDepend(path);
					return;
				}
				
                go.name = assetName;
#if UNITY_EDITOR
                var arr = go.transform.GetComponentsInChildren<Renderer>();
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].sharedMaterial != null)
                        arr[i].material.shader = Shader.Find(arr[i].sharedMaterial.shader.name);
                }
#endif
                callBack(go, item.CRC);

                if(clearDepend)
                    ClearDepend(path);
            });
        }, resType, false, false, true, needCRC);
    }

    //public static void LoadBattlePrefabRef(string path, Action<GameObject> callBack, bool clearAfterLoaded = false)
    //{
    //    LoadCore(path, item => callBack(item.MainAsset as GameObject), EResType.BattlePrefab, false, clearAfterLoaded);
    //}

    /// <summary>
    /// 加载角色模型Prefab
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack"></param>
    /// <param name="clearAfterLoaded"></param>
    public static void LoadModel(string path, Action<GameObject> callBack, bool clearDepend)
    {
        LoadCore(path, (item) =>
        {
            InstanceAsync(item.MainAsset, (go) =>
            {
                callBack(go);

                if (clearDepend)
                    ClearDepend(path);
            });

        }, EResType.Model, false, false);
    }

    public static void LoadModelRef(string path, Action<GameObject> callBack, bool clearDepend)
    {
        LoadCore(path, (item) =>
        {
            callBack(item.MainAsset as GameObject);

            if (clearDepend)
                ClearDepend(path);

        }, EResType.Model, false, false);
    }

    /// <summary>
    /// 加载字体资源
    /// </summary>
    /// <param name="name">字体名</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    public static void LoadFont(string name, Action<Font> callBack)
    {
        LoadCore("Font/"+name, item => callBack(item.MainAsset as Font), EResType.Font, false, false);
    }

    /// <summary>
    /// 加载字体资源
    /// </summary>
    /// <param name="name">字体名</param>
    public static Font LoadFont(string name = "Normal")
    {
        return LoadCore("Font/" + name, EResType.Font).MainAsset as Font;
    }

    public static void LoadShader(string shaderName, Action<Shader> callBack, bool clearAfterLoaded = false)
    {
        string shaderFileName = shaderName.Substring(shaderName.IndexOf("/") + 1);
        LoadCore("Shader/" + shaderFileName, item =>
        {
            var shader = item.MainAsset as Shader; 
#if UNITY_EDITOR
            shader = Shader.Find(shader.name);
#endif
            callBack(shader);
        }, EResType.Shader, false, clearAfterLoaded);
    }

    public static Shader LoadShader(string shaderName)
    {
        string shaderFileName = shaderName.Substring(shaderName.IndexOf("/") + 1);
        var shader = LoadCore("Shader/" + shaderFileName, EResType.Shader).MainAsset as Shader;
#if UNITY_EDITOR
        shader = Shader.Find(shader.name);
#endif
        return shader;
    }

    //public static void LoadMaterial(string name, Action<Material> callBack)
    //{
    //    LoadAsset("Materials/" + name, res => {

    //        if (m_loadMode == ELoadMode.Resources)
    //            callBack(Object.Instantiate(res) as Material);
    //        else
    //            callBack(res as Material);
    //    });
    //}

    public static Material LoadMaterial(string name)
    {
        ResourceItem ri = LoadCore("Materials/" + name, EResType.Material);
        if (ri == null)
            return null;

        Material retMat = ri.MainAsset as Material;

        if (m_loadMode == ELoadMode.Resources)
            retMat = Object.Instantiate(retMat) as Material;    // 避免本地文件被修改

#if UNITY_EDITOR
        retMat.shader = Shader.Find(retMat.shader.name);
#endif
        return retMat;
    }

    public static void LoadTexture(string name, Action<Texture2D> callBack, EResType restype = EResType.Texture)
    {
        LoadCore("Textures/" + name, res => callBack(res.MainAsset as Texture2D), restype);
    }

    /// <summary>
    /// 加载声音资源，由AudioManager自己管理缓存
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的AudioClip对象</param>
    public static void LoadAudio(string path, Action<AudioClip> callBack)
    {
        LoadCore(path, item => callBack(item.MainAsset as AudioClip), EResType.Audio, false, false, false);
    }

    /// <summary>
    /// 加载特效Prefab并实例化
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    
//    public static void LoadEffect(string path, Action<GameObject> callBack, bool clearAfterLoaded = false)
//    {
//        LoadCore(path, item => InstanceAsync(item.MainAsset, go =>
//        {
//#if UNITY_EDITOR
//            var arr = go.transform.GetComponentsInChildren<Renderer>();
//            for (int i = 0; i < arr.Length; i++)
//            {
//                if (arr[i].sharedMaterial != null)
//                    arr[i].material.shader = Shader.Find(arr[i].sharedMaterial.shader.name);
//            }
//#endif
//            callBack(go);
//        }), EResType.Effect, false, clearAfterLoaded);
//    }

    /// <summary>
    /// 加载特效Prefab
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为GameObject对象</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    public static void LoadUIEffect(string path, Action<GameObject> callBack, bool clearAfterLoaded = false)
    {
        LoadCore(path, item => callBack(item.MainAsset as GameObject), EResType.UIEffect, false, clearAfterLoaded);
    }

    public static void LoadAnimatorController(string path, Action<RuntimeAnimatorController> callBack, bool clearAfterLoaded = false)
    {
        LoadCore("Roles/AnimatorController/" + path, item => callBack(item.MainAsset as RuntimeAnimatorController), EResType.AnimatorController, false, clearAfterLoaded);
    }

    /// <summary>
    /// 加载AnimationClip动画资源
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    public static void LoadAnimation(string path, Action<AnimationClip> callBack, EResType resType = EResType.Battle, bool clearAfterLoaded = false)
    {
        LoadCore(path, item =>
        {
            if (item.MainAsset is AnimationClip)
                callBack(item.MainAsset as AnimationClip);
            else
            {
                var temp = item.MainAsset as GameObject;
                if (temp && temp.animation)
                    callBack(temp.animation.clip);
                else
                    callBack(null);
            }
        }, resType, false, clearAfterLoaded);
    } 

    /// <summary>
    /// 加载并切换场景
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack"></param>
    public static void LoadScene(string path, Action callBack, EResType resType = EResType.Scene)
    {
        var arr = path.Split('/');
        if (m_loadMode == ELoadMode.Resources)
        {
            Driver.singleton.ExecuteAsync(Application.LoadLevelAsync(arr[arr.Length - 1]), callBack);
        }
        else if (m_loadMode == ELoadMode.AssetBundle)
        {
            LoadCore("Scenes/" + path, item =>
            {
                Driver.singleton.ExecuteAsync(Application.LoadLevelAsync(arr[arr.Length - 1]), callBack);
            }, resType, true, true);
        }
    }

    /// <summary>
    /// 加载指定图集上的sprte
    /// </summary>
    /// <param name="atlas">图集名</param>
    /// <param name="sprite">sprite名</param>
    /// <param name="callBack">回调函数</param>
    public static void LoadSprite(string atlas, string sprite, Action<Sprite> callBack)
    {
        LoadCore("Atlas/" + atlas, item => callBack(LoadSprite(atlas, sprite)), EResType.Atlas, true);
    }

    /// <summary>
    /// 直接从缓存中读取，如果没有预加载，则返回空对象并报错
    /// </summary>
    /// <param name="atlas"></param>
    /// <param name="sprite"></param>
    /// <returns></returns>
    public static Sprite LoadSprite(string atlas, string sprite)
    {
        var path = "Atlas/" + atlas;
        if (m_resourceItemDic.ContainsKey(path))
        {            
            var item = m_resourceItemDic[path];
            item.SetType(EResType.Atlas);
            if (m_loadMode == ELoadMode.Resources)
            {
                if (item.Assets == null)
                    item.SetAssets((item.MainAsset as GameObject).GetComponent<SpritesHolder>().Sprites);
                return item.Load(sprite, typeof(Sprite)) as Sprite;
            }
            else if (m_loadMode == ELoadMode.AssetBundle)
                return item.Load(sprite, typeof(Sprite)) as Sprite;
        }
        Logger.Error("【Atlas】缓存中不存在该资源：\n" + "atlas:" + atlas + " sprite:" + sprite);
        return null;
    }

    /// <summary>
    /// 加载图集对应的材质
    /// </summary>
    /// <param name="atlas"></param>
    /// <param name="index"></param>
    /// <param name="shaderName"></param>
    /// <returns></returns>
    public static Material LoadAtlasAlphaMaterial(string atlas, string index, string shaderName)
    {
        if (!UIHelper.IsEnableEtcAtlas())
            return null;
        var path = "AtlasAlpha/" + atlas + "_a";
        var key = shaderName + "_" + atlas + "_" + index;

        //处理Asset被清除的情况（小内存状态时会被清掉）
        if (m_atlasMatDic.ContainsKey(key) && m_atlasMatDic[key].GetTexture("_AlphaTex") == null)
            m_atlasMatDic.Remove(key);

        if (!m_atlasMatDic.ContainsKey(key))
        {
            if (!m_resourceItemDic.ContainsKey(path))
            {
                Logger.Error("【AtlasAlpha】缓存中不存在该资源：\n" + "atlas:" + atlas + " texture:" + atlas + "_" + index);
                return null;
            }

            var item = m_resourceItemDic[path];
            item.SetType(EResType.Atlas);

            var atlasName = atlas + "_" + index;
            var tex = item.Load(atlasName, typeof(Texture2D)) as Texture2D;

            m_atlasMatDic.Add(key, new Material(LoadShader(shaderName)));
            m_atlasMatDic[key].SetTexture("_AlphaTex", tex);
        }

        return m_atlasMatDic[key];
    }

    /// <summary>
    ///  如果后面出现一个icon图集不够的情况，可以在次函数依次遍历查找，对外屏蔽这个细节。
    /// </summary>
    /// <param name="iconName"></param>
    /// <returns></returns>
    public static Sprite LoadIcon( string iconName )
    {
        Sprite icon = LoadSprite(AtlasName.ICON, iconName);
        return icon;
    }

    public static Sprite LoadIcon(int itemId)
    {
        var item = ItemDataManager.GetItem(itemId);
        if (item == null)
            return null;
        Sprite icon = LoadSprite(AtlasName.ICON, item.Icon);
        return icon;
    }

    public static Sprite LoadHideIcon(string name)
    {
        return LoadSprite(AtlasName.RoleIcon, name);
    }

    public static Sprite LoadWeaponIcon( int weaponId )
    {
        ConfigItemWeaponLine weaponLine = ItemDataManager.GetItemWeapon( weaponId );
        return LoadSprite( AtlasName.Weapon, weaponLine.Icon );
    }

    public static Sprite LoadRoleIcon(int id,int camp=0)
    {
        if (id <= 0)
        {
            id = camp == 2 ? 2 : 6;
        }
        var headIconLine = ConfigManager.GetConfig<ConfigRoleIcon>().GetLine(id);
        return LoadSprite(AtlasName.RoleIcon, headIconLine.Name);
    }

    public static Sprite LoadRoleIconBig(int id, int camp = 0)
    {
        if (id <= 0)
        {
            id = camp == 2 ? 2 : 6;
        }
        var headIconLine = ConfigManager.GetConfig<ConfigRoleIcon>().GetLine(id);
        if (headIconLine != null)
            return LoadSprite(AtlasName.RoleIcon, headIconLine.Name + "_b");
        else
            return null;
    }

    public static Sprite LoadArmyIcon(int level)
    {
        var configs = ConfigManager.GetConfig<ConfigRoleLevel>();
        var armyIconLine = configs.GetLine(level);
        string icon = null;
        if (armyIconLine != null)
            icon = armyIconLine.ArmyIcon;
        else
            icon = configs.m_dataArr[configs.m_dataArr.Length - 1].ArmyIcon;

        return LoadSprite(AtlasName.ArmyIcon, icon);
    }

    public static Sprite LoadMoneyIcon(EnumMoneyType type)
    {
        return LoadSprite(AtlasName.Common, GetMoneyIconName(type));
    }

    public static string GetMoneyIconName(EnumMoneyType type)
    {
        if (type == EnumMoneyType.DIAMOND)
            return "diamond";
        else if (type == EnumMoneyType.MEDAL)
            return "medal";
        else if (type == EnumMoneyType.CASH)
            return "cash";
        else if (type == EnumMoneyType.CORPS_COIN)
            return "corps_coin";
        else if (type == EnumMoneyType.EVENTCOIN_002)
            return "voucher";
        else if (type == EnumMoneyType.ACTIVE_COIN)
            return "active_coin";
        else
            return "gold";
    }

    public static Sprite LoadVipIcon(int vipLevel)
    {
        if (vipLevel == 1)
            return LoadSprite(AtlasName.Common, "member_icon");
        if (vipLevel == 2)
            return LoadSprite(AtlasName.Common, "high_member_icon");
        return null;
    }

    public static Sprite LoadVipIcon(MemberManager.MEMBER_TYPE enumVipLevel)
    {
        return LoadVipIcon((int)enumVipLevel);
    }

    /// <summary>
    /// 加载UI的prefab并实例化，未完成依赖加载部分，但可以先用
    /// </summary>
    /// <param name="path"></param>
    /// <param name="callBack"></param>
    /// <param name="clearAfterLoaded"></param>
    public static void LoadUI(string path, Action<GameObject> callBack, bool clearAfterLoaded = false)
    {
        //todo 查找并先加载依赖
        LoadCore(path, item =>
        {
            var go = UIHelper.Instantiate(item.MainAsset) as GameObject;
            go.name = item.MainAsset.name;
            callBack(go);
        }, EResType.UI, false, clearAfterLoaded);
    }

    /// <summary>
    /// 直接从缓存中读取并实例化，如果没有预加载，则返回空对象并报错
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static GameObject LoadUI(string path)
    {
        var item = LoadCore(path, EResType.UI);
        if (item != null)
        {
            var go = UIHelper.Instantiate(item.MainAsset) as GameObject;
            go.name = item.MainAsset.name;
            return go;
        }
        Logger.Error("【UI】缓存中不存在该资源：\n" + "path:" + path);
        return null;
    }

    /// <summary>
    /// 直接从缓存中读取资源引用，不实例化，如果没有预加载，则返回空对象并报错
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static GameObject LoadUIRef(string path)
    {
        var item = LoadCore(path, EResType.UI);
        if (item != null)
            return item.MainAsset as GameObject;
        Logger.Error("【UI】缓存中不存在该资源：\n" + "path:" + path);
        return null;
    }

    /// <summary>
    /// 直接从缓存中读取资源引用，不实例化，如果没有预加载，则返回空对象并报错
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static RenderTexture LoadRenderTexture(string path)
    {
        var item = LoadCore(path, EResType.RenderTexture);
        if (item != null)
            return item.MainAsset as RenderTexture;
        Logger.Error("【UI】缓存中不存在该资源：\n" + "path:" + path);
        return null;
    }

    /// <summary>
    /// 加载配置文件
    /// </summary>
    /// <param name="path">配置资源路径</param>
    /// <param name="callBack">加载完毕后的回调函数</param>
    public static void LoadConfigs(string path, Action<Object[]> callBack)
    {
        LoadCore(path, item => callBack(item.Assets), EResType.Config, false, true);
    }

    /// <summary>
    /// 批量加载多个资源，可以作为预加载等用途。最好一次加载同一类资源，否则设置type为EResType.None
    /// </summary>
    /// <param name="paths">资源地址数组</param>
    /// <param name="eachCallBack">每个资源加载完毕后的回调，可为null</param>
    /// <param name="completeCallBack">全部资源加载完毕的回调，可为null</param>
    public static void LoadMulti(string[] paths, Action<ResourceItem> eachCallBack, Action completeCallBack)
    {
        LoadMultiCore(paths, 0, eachCallBack, completeCallBack);
    }

    public static void LoadMulti(ResourceItem[] items, Action<ResourceItem> eachCallBack, Action completeCallBack)
    {
        LoadMultiCore(items, 0, eachCallBack, completeCallBack);
    }

    /// <summary>
    /// 在没有对应加载方式的情况下使用的，用来加载任意资源
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为实例化后的GameObject对象</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    public static void LoadAsset(string path, Action<Object> callBack, bool clearAfterLoaded = false)
    {
        LoadCore(path, item => callBack(item.MainAsset), EResType.None, false, clearAfterLoaded);
    }

    public static Object[] LoadAssets(string path)
    {
        return LoadCore(path).Assets;
    }

    /// <summary>
    /// 下载资源及其依赖的资源，保存到本地
    /// </summary>
    /// <param name="paths"></param>
    /// <param name="progressCallBack">下载进度回调</param>
    /// <param name="key">会在回调函数中作为参数，用以区别每次加载调用</param>
    public static void DownloadAsset(string[] paths, Action<string, DownloadAssetsInfo> progressCallBack, string key = "")
    {
        var queue = new Queue<string>();
        for (int i = 0; i < paths.Length; i++)
        {
            if (paths[i].IsNullOrEmpty())
                continue;

            if (m_dependDic.ContainsKey(paths[i]))
                GetAllDependResources(paths[i], queue);
            queue.Enqueue(paths[i]);
        }

        var list = new List<string>();
        foreach (string value in queue)
        {
            if (value.IsNullOrEmpty())
                continue;

            var abPath = value + ".assetbundle";
            var sdPath = UpdateWorker.SdRootPath + UpdateWorker.ResDir + abPath;
            var appPath = UpdateWorker.AppRootPath + abPath;
            var crc = m_serverFileVerion.GetValue(abPath, "");
#if !UNITY_WEBPLAYER
            if (File.Exists(sdPath) && Util.CRC32String(File.ReadAllBytes(sdPath)) == crc)
                continue;
            else if (File.Exists(appPath) && Util.CRC32String(File.ReadAllBytes(appPath)) == crc)
                continue;
#endif
            list.Add(value);
        }

        Driver.singleton.StartCoroutine(BeginDownloadAsset(list.ToArray(), progressCallBack, key));
    }

    private static IEnumerator BeginDownloadAsset(string[] pathArr, Action<string, DownloadAssetsInfo> progressCallBack, string key)
    {
        var info = new DownloadAssetsInfo();
        info.maxCount = pathArr.Length;
        info.loadedCount = 0;
        m_downloadProgressDic[key] = info;
        
        for (int i = 0; i < pathArr.Length; i++)
        {
            var path = pathArr[i] + ".assetbundle";
            var crc = m_serverFileVerion.GetValue(path, "");
            var url = m_update_url + Global.currentVersion + "/" + path + "?v=" + crc;
            byte[] bytes = null;
            for (int j = 0; j < 3; j++) //自动重试3次
            {
                var www = new WWW(url);
                info.www = www;
                yield return www;
                while (!www.isDone && string.IsNullOrEmpty(www.error)) yield return null;

                if (!string.IsNullOrEmpty(www.error) || !www.isDone)
                    bytes = null;
                else
                    bytes = www.bytes;
                www.Dispose();
                info.www = null;

                if (bytes != null)
                {
                    if (crc != null)
                    {
                        var newCrc = Util.CRC32String(bytes);
                        if (crc == newCrc)
                            break;
                        Logger.Log(string.Format("校验失败{0} SerCRC:{1} fileCRC:{2} {3}", (j + 1), crc, newCrc, url));
                    }
                    else
                        break;
                }
                else
                    Logger.Log("下载失败" + (j + 1) + " " + url);
            }

            if (bytes != null)
                m_monoDriver.StartCoroutine(SaveABToLoaclFile(path, url, bytes));

            info.loadedCount += 1;
            
            if (progressCallBack != null)
                progressCallBack(key, m_downloadProgressDic[key]);
        }

        if (pathArr.Length == 0 && progressCallBack != null)
            progressCallBack(key, m_downloadProgressDic[key]);

    }


    public static int GetAssetsSize(string[] paths)
    {
        if (m_serverFileSize == null)
            return 0;

        //获取依赖的所有资源
        var queue = new Queue<string>();
        for (int i = 0; i < paths.Length; i++)
        {
            queue.Enqueue(paths[i]);
            GetAllDependResources(paths[i], queue);
        }

        var list = new List<string>();
        foreach (string value in queue)
        {
            if (value.IsNullOrEmpty())
                continue;
            var abPath = value + ".assetbundle";
            var sdPath = UpdateWorker.SdRootPath + UpdateWorker.ResDir + abPath;
            var appPath = UpdateWorker.AppRootPath + abPath;
#if !UNITY_WEBPLAYER
            if (File.Exists(sdPath) && Util.CRC32String(File.ReadAllBytes(sdPath)) == m_serverFileVerion.GetValue(abPath))
                continue;
            else if (File.Exists(appPath) && Util.CRC32String(File.ReadAllBytes(appPath)) == m_serverFileVerion.GetValue(abPath))
                continue;
#endif
            list.Add(value);
        }

        var totalSize = 0;
        foreach (var path in list)
        {
            int size = 0;
            var abPath = path + ".assetbundle";
            if (m_serverFileSize.ContainsKey(abPath))
                int.TryParse(m_serverFileSize[abPath], out size);
            totalSize += size / 1024;
        }
        return Mathf.Max(1, totalSize);
    }

    public static bool CheckOutsideAssetsCrc(string[] paths)
    {
        if (m_outsideFileVersion == null)
            return true;

        //获取依赖的所有资源
        var queue = new Queue<string>();
        for (int i = 0; i < paths.Length; i++)
        {
            queue.Enqueue(paths[i]);
            GetAllDependResources(paths[i], queue);
        }

        foreach (var path in queue)
        {
            if (path.IsNullOrEmpty())
                continue;

            var abPath = path + ".assetbundle";
            if (!m_outsideFileVersion.ContainsKey(abPath))
                continue;

            //如果包外包内都没有，则直接认为不是最新
            var sdPath = UpdateWorker.SdRootPath + UpdateWorker.ResDir + abPath;
            if (!File.Exists(sdPath))
            {
                sdPath = UpdateWorker.AppRootPath + abPath;
                if (!File.Exists(sdPath))
                    return false;
            }

            //如果该资源CRC不等，则认为不是最新
#if !UNITY_WEBPLAYER
            var crc = Util.CRC32String(File.ReadAllBytes(sdPath));
            if (m_outsideFileVersion[abPath] != crc)
                return false;
#endif
        }

        return true;
    }

    /// <summary>
    /// 加载WEB数据
    /// </summary>
    /// <param name="pathURL"></param>
    /// <param name="loadedCallBack"></param>
    public static  void LoadWebData(string pathURL,Action<string> loadedCallBack){
        m_monoDriver.StartCoroutine(LoadWebDataByURL(pathURL,loadedCallBack));
    }
    /// <summary>
    /// 加载WEB数据
    /// </summary>
    /// <param name="pathURL"></param>
    /// <param name="loadedCallBack"></param>
    /// <returns></returns>
    private static IEnumerator LoadWebDataByURL(string pathURL,Action<string> loadedCallBack)
    {
       if(string.IsNullOrEmpty(pathURL)) yield break;
        WWW www=new WWW(pathURL);
        yield return www;
        if(www.error!=null) yield break;
        if(string.IsNullOrEmpty(www.text)) yield break;
        if(loadedCallBack!=null)
        loadedCallBack(www.text);
        www=null;
    }

    /// <summary>
    /// 底层加载函数，异步加载unity压缩过的AssetBundle，或者同步加载Resources类型
    /// </summary>
    /// <param name="path">相对于Resources路径下的资源路径</param>
    /// <param name="callBack">回调函数的参数为ResourceItem对象，可以详细拿到AB中的所有Asset</param>
    /// <param name="type">分类标签</param>
    /// <param name="saveAssetBundle">是否保存AssetBundle对象不卸载，可以保留依赖关系</param>
    /// <param name="clearAfterLoaded">加载完成后清除自己的缓存</param>
    private static void LoadCore(string path, Action<ResourceItem> callBack, EResType type = EResType.None, bool saveAssetBundle = false, bool clearAfterLoaded = false, bool dependLoad = true, bool needCRC = false, int tag = 0)
    {
        if (!m_resourceItemDic.ContainsKey(path))
        {
            var itemNew = new ResourceItem(path, saveAssetBundle);
            itemNew.NeedCRC = needCRC;
            itemNew.AddTags(tag);
            m_resourceItemDic.Add(path, itemNew);            
        }

        var item = m_resourceItemDic[path];
        item.SetType(type);
        item.IsClearAfterLoaded = clearAfterLoaded;
        if (!item.IsLoaded)
        {
            if (m_loadMode == ELoadMode.Resources)
                m_monoDriver.StartCoroutine(BeginResourceLoadAsync(item));
            else if (m_loadMode == ELoadMode.AssetBundle)
            {
                    if (dependLoad && m_dependDic.ContainsKey(item.Path))
                    {
                        var list = new Queue<string>();
                        GetAllDependResources(item.Path, list);
                        LoadMulti(list.ToArray(), null, () => m_monoDriver.StartCoroutine(BeginAssetBundleLoadAsync(item)));
                    }
                    else if (dependLoad && !m_dependDic.ContainsKey(item.Path) && item.Type == EResType.Atlas)
                    {
                        var list = new Queue<string>();
                        if (UIHelper.IsEnableEtcAtlas())
                        {
                            var pathAtlasAlpha = UIHelper.TryGetAtlasAlphaPath(item.Path);
                            if (!string.IsNullOrEmpty(pathAtlasAlpha))
                                list.Enqueue(pathAtlasAlpha);
                        }
                        LoadMulti(list.ToArray(), null, () => m_monoDriver.StartCoroutine(BeginAssetBundleLoadAsync(item)));
                    }
                    else
                        m_monoDriver.StartCoroutine(BeginAssetBundleLoadAsync(item));
            }
        }

        item.AddCallBackFunc(callBack);
        if (item.IsCompleted)
            item.LaunchCallBack();
    }

    private static void LoadCore(ResourceItem item, Action<ResourceItem> callBack, bool dependLoad = true)
    {
        LoadCore(item.Path, callBack, item.Type, item.IsSaveAssetBundle, item.IsClearAfterLoaded, dependLoad, item.NeedCRC, item.Tag);
    }

    private static ResourceItem LoadCore(string path, EResType type = EResType.None)
    {
        if (m_resourceItemDic.ContainsKey(path))
        {
            var item = m_resourceItemDic[path];
            item.SetType(type);
            return item;
        }
        return null;
    }

    private static void LoadMultiCore(string[] paths, int index, Action<ResourceItem> eachCallBack, Action completeCallBack)
    {
        List<ResourceItem> items = new List<ResourceItem>();
        for (int i = 0; i < paths.Length; i++)
        {
            items.Add(new ResourceItem(paths[i], true));
        }
        LoadMultiCore(items.ToArray(), index, eachCallBack, completeCallBack);
    }

    private static void LoadMultiCore(ResourceItem[] items, int index, Action<ResourceItem> eachCallBack, Action completeCallBack)
    {
        var multiLoadItem = new MultiLoadItem();
        for (int i = 0; i < items.Length; i++)
        {
            if (m_dependDic.ContainsKey(items[i].Path))
            {
                var dependList = new Queue<string>();
                GetAllDependResources(items[i].Path, dependList);
                while (dependList.Count > 0)
                {
                    var item = new ResourceItem(dependList.Dequeue(), true);
                    multiLoadItem.items.Enqueue(item);
                }
            }
            else if (UIHelper.IsEnableEtcAtlas())
            {
                var pathAtlasAlpha = UIHelper.TryGetAtlasAlphaPath(items[i].Path);
                if (!string.IsNullOrEmpty(pathAtlasAlpha))
                    multiLoadItem.items.Enqueue(new ResourceItem(pathAtlasAlpha, items[i].Type, items[i].Tag));
            }
            multiLoadItem.items.Enqueue(items[i]);
        }
        multiLoadItem.eachCallBack = eachCallBack;
        multiLoadItem.completeCallBack = completeCallBack;

        m_multiLoad.Enqueue(multiLoadItem);

        Update();
    }

    public static void GetAllDependResources(string path, Queue<string> dependList)
    {
        if (!m_dependDic.ContainsKey(path))
            return;
        var paths = m_dependDic[path];
        for (int i = 0; i < paths.Length; i++)
        {
            if (!dependList.Contains(paths[i]))
            {
                dependList.Enqueue(paths[i]);
                if (UIHelper.IsEnableEtcAtlas())
                {
                    var pathAtlasAlpha = UIHelper.TryGetAtlasAlphaPath(paths[i]);
                    if (!string.IsNullOrEmpty(pathAtlasAlpha))
                        dependList.Enqueue(pathAtlasAlpha);
                }
                
                GetAllDependResources(paths[i], dependList);
            }
        }
    }

    #region 全局的分帧实例化处理，在Update里实例化
    class InstanceItem
    {
        public Object go;
        public Action<GameObject> callBack;
    }

    private static readonly Queue<InstanceItem> m_instanceQueue = new Queue<InstanceItem>();
    private static void InstanceAsync(Object go, Action<GameObject> callBack)
    {
        m_instanceQueue.Enqueue(new InstanceItem { go = go, callBack = callBack });
    }
    #endregion

    private static bool m_isMultiLoading;
    private static string m_curLoadingPath;
    private static Queue<MultiLoadItem> m_multiLoad = new Queue<MultiLoadItem>();
    public static void Update()
    {
        if (Time.time > m_save_filelist_time)
        {
            m_save_filelist_time = float.MaxValue;
            SaveLocalFileList();
        }
        if (m_verifyCrcList.Count > 0 && Time.time > m_verifyDelayTime)
        {
            if (Logger.IsShowLog)
            {
                var result = "";
                for (int i = 0; i < m_verifyCrcList.Count; i++)
                {
                    result += "send crc verify:" + m_verifyCrcList[i].path + " " + m_verifyCrcList[i].crc + "\n";
                }
                Logger.Log(result);
            }
            m_verifyDelayTime = float.MaxValue;
            NetLayer.Send(new tos_player_verify_crc() {list = m_verifyCrcList.ToArray()});
            m_verifyCrcList.Clear();
        }

        if (m_instanceQueue.Count > 0)
        {
            var item = m_instanceQueue.Dequeue();
            if (item.callBack != null)
                item.callBack(Object.Instantiate(item.go) as GameObject);
        }

        if (m_multiLoad.Count == 0)
            return;
        if (m_isMultiLoading)
        {
            //加载中被清了。。没有回调了
            if (!string.IsNullOrEmpty(m_curLoadingPath) && !m_resourceItemDic.ContainsKey(m_curLoadingPath))
                m_isMultiLoading = false;
            else
                return;
        }
        var multiLoadItem = m_multiLoad.Peek();
        if (multiLoadItem.items.Count == 0)
        {
            var removeItem = m_multiLoad.Dequeue();
            if (removeItem.completeCallBack != null)
                removeItem.completeCallBack();

            return;
        }

        var loadItem = multiLoadItem.items.Dequeue();
        m_isMultiLoading = true;
        m_curLoadingPath = loadItem.Path;
        LoadCore(loadItem, item =>
        {
            m_curLoadingPath = null;
            if (multiLoadItem.eachCallBack != null)
                multiLoadItem.eachCallBack(item);
            m_isMultiLoading = false;
            Update();
        }, false);
    }

    //开始同步加载Resources类型的资源
    private static void BeginResourceLoad(ResourceItem item)
    {
        item.SetLoadState(EResItemLoadState.Loading);
        var asset = Resources.Load(item.Path);
        if (asset == null)
        {
            var assets = Resources.LoadAll(item.Path);
            if (assets == null || assets.Length == 0)
            {
                Logger.Error("资源不存在：\n路径：" + item.Path);
                return;
            }
            else
                item.SetAssets(assets);
        }
        else
            item.SetMainAsset(asset);
        item.SetLoadState(EResItemLoadState.Completed);

        //按需清除ResourceItem缓存
        if (item.IsClearAfterLoaded && m_resourceItemDic.ContainsKey(item.Path))
            m_resourceItemDic.Remove(item.Path);
    }

    //开始异步加载Resources类型的资源，用来模拟AssetBundle异步加载流程
    private static IEnumerator BeginResourceLoadAsync(ResourceItem item)
    {
        item.SetLoadState(EResItemLoadState.Loading);
        var request = Resources.LoadAsync(item.Path);
        yield return request;

        var asset = request.asset;
        if (asset == null)
        {
            var assets = Resources.LoadAll(item.Path);
            if (assets == null || assets.Length == 0)
            {
                Logger.Error("资源不存在：\n路径：" + item.Path);
                item.LaunchCallBack();
                yield break;
            }
            else
                item.SetAssets(assets);
        }
        else
            item.SetMainAsset(asset);

        //if (item.NeedCRC)
        //{
        //    item.CRC = FirstUtil.CRC32String(File.ReadAllBytes(string.Concat( Application.dataPath, "/Resources/", item.Path, ".prefab")));
        //}

        item.SetLoadState(EResItemLoadState.Completed);
        
        //按需清除ResourceItem缓存
        if (item.IsClearAfterLoaded && m_resourceItemDic.ContainsKey(item.Path))
            m_resourceItemDic.Remove(item.Path);

        item.LaunchCallBack();
    }

    /* 开始同步加载手动压缩后的AssetBundle类型的资源
    private static void BeginAssetBundleLoad(ResourceItem item)
    {
        var storePath = Application.persistentDataPath + "/" + item.Path;
        Util.CreateFileDirectory(storePath);            //创建目录
        Util.DecompressFileLZMA(item.Path, storePath);  //解压缩
        var assetBundle = AssetBundle.CreateFromFile(storePath);
        item.SetAsset(assetBundle.mainAsset);
        item.SetLoadState(EItemLoadState.Completed);
        File.Delete(storePath);     //删除解压的文件
    }
     */

    //开始异步加载AssetBundle类型的资源
    //远程下载文件到内存中成为AssetBundle镜像，再开辟内存从AssetBundle镜像中创建出指定Asset，最后卸载掉AB镜像内存，只保留Asset对象
    private static IEnumerator BeginAssetBundleLoadAsync(ResourceItem item)
    {

        if (item == null || string.IsNullOrEmpty(item.Path) || item.IsLoaded)
            yield break;

        var startTime = Time.realtimeSinceStartup;
        var path = GetResFullPathWww(item.Path + ".assetbundle");
        if (path == null)
        {
            item.SetLoadState(EResItemLoadState.Error);
           Logger.Error(string.Format("加载资源失败，资源地址不存在：{0}", item.Path + ".assetbundle"));
            item.LaunchCallBack();
            yield break;
        }
        item.SetLoadState(EResItemLoadState.Loading);
        var www = new WWW(path);
        yield return www;

        if (Driver.m_platform == EnumPlatform.web)
            while (!www.isDone && string.IsNullOrEmpty(www.error)) yield return null;

        if (!Driver.isMobilePlatform && Time.realtimeSinceStartup-startTime>1f)
            Logger.Log(string.Format("加载耗时：[{0}]\nUrl:{1}", Time.realtimeSinceStartup-startTime, path));

        //用来生成pc微端资源拷贝脚本
//        CreateCopyResBat(item);

#if UNITY_EDITOR
        if (simulateNetSpeed > 0)
        {
            var kb = www.bytes.Length/1024f;
            var time = kb/simulateNetSpeed;
            Logger.Log(string.Format("开始加载：({0}KB {1}秒){2}", kb, time.ToString("0.#"), item.Path));
            yield return new WaitForSeconds(time);
        }
#endif
        if (string.IsNullOrEmpty(www.error))
        {
            item.SetLoadState(EResItemLoadState.Completed);
            if (item.NeedCRC)
            {
                item.CRC = Util.CRC32(www.bytes);
                VerifyResCrc(item.Path + ".assetbundle", item.CRC);
                Logger.Log(string.Format("calc assetbundle crc of {0}:{1}", item.Path + ".assetbundle", item.CRC));
            }
            else
            {
                item.CRC = 0;
            }
            if (m_save_loaded_file)
            {//启动一个线程去做这个事情，避免阻塞
                m_monoDriver.StartCoroutine(SaveABToLoaclFile(item.Path + ".assetbundle", www.url, www.bytes));
            }
            if (!item.IsSaveAssetBundle)
            {
                item.SetMainAsset(www.assetBundle.mainAsset);
                item.SetAssets(www.assetBundle.LoadAll());
                item.LaunchCallBack();
                if (www.assetBundle != null)
                    www.assetBundle.Unload(false);
                www.Dispose();
            }
            else
            {
                item.SetAssetBundle(www.assetBundle);
                item.LaunchCallBack();
                www.Dispose();
            }
        }
        else
        {
            item.SetLoadState(EResItemLoadState.Error);
           Logger.Error(string.Format("加载资源失败：url-{0}, message-{1}", www.url, www.error));
            item.LaunchCallBack();
        }
        //按需清除ResourceItem缓存
        if (item.IsClearAfterLoaded)
        {
            Clear(item.Path);
            GarbageCollect();
        }
    }

    private static void CreateCopyResBat(ResourceItem item)
    {
#if !UNITY_WEBPLAYER
        const string batPath = "CopyRes.bat";
        if (!File.Exists(batPath))
        {
            File.AppendAllText(batPath, "@echo off\r\n");
            File.AppendAllText(batPath, "set WORK_PATH=StreamingAssetsLib\\StandaloneWindows\r\n");
            File.AppendAllText(batPath, "set OUTPUT_PATH=Assets\\StreamingAssets\r\n");

            var arr = new string[]
            {
                @"Managed\Client.dll.bytes",
                @"Managed\UpdateWorker.dll.bytes",
                @"VersionInfo\FileList.txt",
                @"VersionInfo\FileSize.txt",
                @"Atlas\UpdateWorkerBg.assetbundle",
                @"Atlas\UpdateWorker.assetbundle",
                @"UI\Update\PanelUpdateBg.assetbundle",
                @"UI\Update\PanelUpdate.assetbundle"
            };
            for (int i = 0; i < arr.Length; i++)
            {
                File.AppendAllText(batPath,
                    String.Format("echo f | xcopy /y \"%WORK_PATH%\\{0}\" \"%OUTPUT_PATH%\\{0}\"\r\n", arr[i]));
            }
        }
        File.AppendAllText(batPath,
            String.Format("echo f | xcopy /y \"%WORK_PATH%\\{0}.assetbundle\" \"%OUTPUT_PATH%\\{0}.assetbundle\"\r\n",
                item.Path.Replace('/', '\\')));
#endif
    }

    public static IEnumerator SaveABToLoaclFile(string path, string url, byte[] bytes)
    {
        if (url == null)
            yield break;

        if (!(Application.isEditor && url.StartsWith("file")) && !url.StartsWith("http"))
            yield break;

#if !UNITY_WEBPLAYER
        string fullPath = UpdateWorker.SdRootPath + UpdateWorker.ResDir + path;
        var crc = Util.CRC32String(bytes);
        if (crc != m_serverFileVerion.GetValue(path))
        {
            Logger.Error("文件保存校验失败：" + crc + " " + m_serverFileVerion.GetValue(path) + "\n" + url);
            yield break;
        }
        try
        {
            var dir = Path.GetDirectoryName(fullPath);
            if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            File.WriteAllBytes(fullPath, bytes);
            Logger.Log("保存资源文件：" + fullPath);
        }
        catch(Exception ex)
        {
            Logger.Error(ex.Message);
            yield break;
        }
        if (m_localFileVerion != null)
        {
            m_localFileVerion[path] = crc;
            m_save_filelist_time = Time.time + 3;
//            if (m_flush_timer == 0)
//                m_flush_timer = TimerManager.SetTimeOut(3, SaveLocalFileList);
        }
#endif
    }

    private static void SaveLocalFileList()
    {
#if !UNITY_WEBPLAYER
//        m_flush_timer = 0;
        if (m_localFileVerion != null && m_localFileVerion.Count > 0)
        {
            var time = Time.realtimeSinceStartup;
            var version_data = new StringBuilder();
            foreach (var info in m_localFileVerion)
            {
                if (m_outsideFileVersion.ContainsKey(info.Key))
                    version_data.AppendLine(info.Key + "=" + info.Value + "=outside");
                else
                    version_data.AppendLine(info.Key + "=" + info.Value);
            }
            string path = UpdateWorker.SdRootPath + UpdateWorker.InfoDir + "FileList.txt";
            try
            {
                var dir = Path.GetDirectoryName(path);
                if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                File.WriteAllText(path, version_data.ToString());
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            Logger.Log((Time.realtimeSinceStartup - time)+"s====================重写本地FileList.txt");
        }
#endif
    }

    public static string GetResFullPathWww(string path)
    {
#if UNITY_WEBPLAYER
        var vdata = "";
        if (m_serverFileVerion != null)
        {
            m_serverFileVerion.TryGetValue(path, out vdata);
        }
        if (string.IsNullOrEmpty(vdata))
            return null;
        return UpdateWorker.AppRootPathWww + path + "?v=" + (vdata ?? "");
#elif UNITY_STANDALONE
        var url = "";
        if (File.Exists(UpdateWorker.SdRootPath + UpdateWorker.ResDir + path))
        {
            url = UpdateWorker.SdRootPathWww + UpdateWorker.ResDir + path;
        }
        else if (File.Exists(UpdateWorker.AppRootPath + path))
        {
            url = UpdateWorker.AppRootPathWww + path;
        }
        if (!string.IsNullOrEmpty(m_update_url))
        {
            var vdata = "";
            var local_vdata = "";
            if (m_serverFileVerion != null)
            {
                m_serverFileVerion.TryGetValue(path, out vdata);
            }
            if (m_localFileVerion != null)
            {
                m_localFileVerion.TryGetValue(path, out local_vdata);
            }
            if (string.IsNullOrEmpty(url) || vdata != local_vdata)
            {
                url = m_update_url + Global.currentVersion + "/" + path + "?v=" + (vdata ?? "");
                if (string.IsNullOrEmpty(vdata))
                    return null;
            }
        }
        return !string.IsNullOrEmpty(url) ? url : UpdateWorker.AppRootPathWww + path;
#else
        if (File.Exists(UpdateWorker.SdRootPath + UpdateWorker.ResDir + path))
            return UpdateWorker.SdRootPathWww + UpdateWorker.ResDir + path;
        else
            return UpdateWorker.AppRootPathWww + path;
#endif
    }

    private static IEnumerator BeginSafeDownload(string url, string crc = null)
    {
        byte[] readObject = null;
        for (int i = 0; i < 3; i++) //自动重试3次
        {
            var www = new WWW(url);
            yield return www;
            while (!www.isDone && string.IsNullOrEmpty(www.error)) yield return null;

            if (!string.IsNullOrEmpty(www.error) || !www.isDone)
                readObject = null;
            else
                readObject = www.bytes;
            www.Dispose();

            if (readObject != null)
            {
                if (crc != null)
                {
                    var newCrc = Util.CRC32String(readObject);
                    if (crc == newCrc)
                        break;
                    Logger.Log(string.Format("校验失败{0} SerCRC:{1} fileCRC:{2} {3}", (i + 1), crc, newCrc, url));
                }
                else
                    break;
            }
            else
                Logger.Log("下载失败" + (i + 1) + " " + url);
        }

        yield return readObject;
    }

    private static void VerifyResCrc(string path, uint crc)
    {
        if (Driver.m_platform == EnumPlatform.web)
            return;
        m_verifyCrcList.Add( new tos_player_verify_crc.info() { path = path, crc = crc });
        m_verifyDelayTime = Time.time + 5;
    }

    /// <summary>
    /// 清除指定路径的资源缓存
    /// </summary>
    /// <param name="path"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    public static void Clear(string path, bool unloadAllLoadedObjects = false)
    {
        if (path != null && m_resourceItemDic.ContainsKey(path))
        {
            if (m_resourceItemDic[path].HasTags(ResTag.Forever))
                return;
            m_resourceItemDic[path].Clear(unloadAllLoadedObjects);
            m_resourceItemDic.Remove(path);
        }
    }

    /// <summary>
    /// 清除指定标记的资源缓存
    /// </summary>
    /// <param name="tags"></param>
    /// <param name="opposite"></param>
    /// <param name="unloadAllLoadedObjects"></param>
    public static void Clear(int tags, bool opposite = false, bool unloadAllLoadedObjects = false)
    {
        var delList = new List<string>();
        foreach (var resourceItem in m_resourceItemDic)
        {
            if (!opposite && resourceItem.Value.HasTags(tags) || opposite && !resourceItem.Value.HasTags(tags))
                delList.Add(resourceItem.Value.Path);
        }
        for (int i = 0; i < delList.Count; i++)
        {
            Clear(delList[i], unloadAllLoadedObjects);
        }
        delList.Clear();
    }

    /// <summary>
    /// 清除指定类型的资源缓存
    /// </summary>
    /// <param name="resType"></param>
    /// <param name="clearDepend"></param>
    public static void Clear(EResType resType, bool clearDepend = false)
    {
        var delList = new List<string>();
        var list = new Queue<string>();
        foreach (var resourceItem in m_resourceItemDic)
        {
            if (resourceItem.Value.Type == resType)
            {
                delList.Add(resourceItem.Value.Path);

                if (clearDepend)
                {
                    list.Clear();
                    GetAllDependResources(resourceItem.Value.Path, list);
                    delList.AddRange(list);
                }
            }
        }
        for (int i = 0; i < delList.Count; i++)
        {
            Clear(delList[i]);
        }
        delList.Clear();
    }

    public static void ClearAtlas(string name)
    {
        Clear("Atlas/" + name);
        Clear("AtlasAlpha/" + name + "_a");
    }

    //public static void Clear(List<EResType> resType, bool clearDepend = false)
    //{
    //    var delList = new List<string>();
    //    var list = new Queue<string>();
    //    foreach (var resourceItem in m_resourceItemDic)
    //    {
    //        if (resType.Contains(resourceItem.Value.Type))
    //        {
    //            delList.Add(resourceItem.Value.Path);

    //            if (clearDepend)
    //            {
    //                list.Clear();
    //                GetAllDependResources(resourceItem.Value.Path, list);
    //                delList.AddRange(list);
    //            }
    //        }
    //    }
    //    for (int i = 0; i < delList.Count; i++)
    //    {
    //        Clear(delList[i]);
    //    }
    //    delList.Clear();
    //}

    public static void ClearDepend(string path)
    {
        var delList = new List<string>();
        var list = new Queue<string>();
        if (m_resourceItemDic.ContainsKey(path))
        {
            delList.Add(path);

            list.Clear();
            GetAllDependResources(path, list);
            delList.AddRange(list);
        }

        for (int i = 0; i < delList.Count; i++)
        {
            Clear(delList[i]);
        }
        delList.Clear();
    }

    /// <summary>
    /// 清除所有资源缓存
    /// </summary>
    public static void ClearAll()
    {
        foreach (var resourceItem in m_resourceItemDic)
            resourceItem.Value.Clear();
        m_resourceItemDic.Clear();

        GarbageCollect();
    }

    /// <summary>
    /// 释放内存
    /// </summary>
    public static void GarbageCollect()
    {
        Resources.UnloadUnusedAssets();
        GC.Collect();
    }

    public static void Destroy(Object obj)
    {
        if (obj != null)
            GameObject.Destroy(obj);
    }

    //public static void Destroy(List<GameObject> list)
    //{
    //    if (list == null)
    //        return;

    //    for (int i = 0; i < list.Count; i++)
    //    {
    //        ResourceManager.Destroy(list[i]);
    //    }
    //}

    public static Dictionary<string, DownloadAssetsInfo> DownloadProgressDic { get { return m_downloadProgressDic; }}
    public static string UpdateUrl { get { return m_update_url; } }
}

public class MultiLoadItem
{
    public Queue<ResourceItem> items = new Queue<ResourceItem>();
    public Action<ResourceItem> eachCallBack;
    public Action completeCallBack;
}

public class ResourceItem
{
    private AssetBundle m_assetBundle;
    private Object m_mainAsset;
    private string m_mainAssetName;
    private Object[] m_assets;
    private string[] m_assetsName;
    private string m_path;
    private EResType m_type;
    private int m_tags;
    private bool m_saveAssetBundle;      //如果为true，则www加载完成后不进行LoadAll操作，也不会卸载AB。一般用在依赖打包的对象上
    private bool m_clearAfterLoaded;  //加载完成后移除对自己这个ResourceItem的引用，交由外部管理（比如配置对象）
    private EResItemLoadState m_loadState = EResItemLoadState.Wait;
    private Queue<Action<ResourceItem>> m_callBackFunc;

    private bool m_bNeedCRC = false;//是否需要计算资源的CRC
    private uint m_CRC;
    //private DateTime m_startLoadTime;

    public ResourceItem(string path, bool saveAssetBundle = false)
    {
        m_path = path;
        m_saveAssetBundle = saveAssetBundle;

        if (saveAssetBundle)
            AddTags(ResTag.AssetBundle);
    }

    /// <summary>
    /// 用来给预加载的资源方便用
    /// </summary>
    /// <param name="path"></param>
    /// <param name="type"></param>
    /// <param name="tag"></param>
    public ResourceItem(string path, EResType type, int tag = 0, bool checkCRC = false)
    {
        m_path = path;
        m_saveAssetBundle = type == EResType.Atlas || type == EResType.Font || type == EResType.Shader;
        SetType(type);
        m_clearAfterLoaded = false;
        //m_bNeedCRC = type == EResType.BattlePrefab;
        m_bNeedCRC = checkCRC;
        if (m_saveAssetBundle)
            AddTags(ResTag.AssetBundle);
        if (tag != 0)
            AddTags(tag);
    }

    /// <summary>
    /// 判断标签
    /// </summary>
    /// <param name="tags"></param>
    /// <returns></returns>
    public bool HasTags(int tags)
    {
        if ((m_tags & tags) == tags)
            return true;
        return false;
    }

    /// <summary>
    /// 增加标签
    /// </summary>
    /// <param name="tags"></param>
    public void AddTags(int tags)
    {
        m_tags |= tags;
    }

    /// <summary>
    /// 设置资源类型
    /// </summary>
    /// <param name="type"></param>
    public void SetType(EResType type)
    {
        //资源一旦被设置过非空类型，则不可再设置回空，除非清除
        if (type != EResType.None)
            m_type = type;
    }

    /// <summary>
    /// 执行回调函数，并清除已保存的回调函数
    /// </summary>
    public void LaunchCallBack()
    {
        //确保回调函数只会执行一次
        while (m_callBackFunc != null && m_callBackFunc.Count > 0)
        {
            var func = m_callBackFunc.Dequeue();
            func(this);
        }

        if (m_callBackFunc != null)
            m_callBackFunc.Clear();
    }

    /// <summary>
    /// 添加回调函数
    /// </summary>
    /// <param name="callBack"></param>
    public void AddCallBackFunc(Action<ResourceItem> callBack)
    {
        if (callBack != null)
        {
            if (m_callBackFunc == null)
                m_callBackFunc = new Queue<Action<ResourceItem>>();
            m_callBackFunc.Enqueue(callBack);
        }
    }

    /// <summary>
    /// 设置加载状态
    /// </summary>
    /// <param name="state"></param>
    public void SetLoadState(EResItemLoadState state)
    {
        if (m_loadState == EResItemLoadState.Wait && state == EResItemLoadState.Loading)
            //m_startLoadTime = DateTime.Now;
            Logger.RecordTime(m_path);
        else if (m_loadState == EResItemLoadState.Loading && state == EResItemLoadState.Completed)
        {
            Logger.PrintTime(m_path);
            //if(GameSetting.enableLogResLoadTime)
            //    Logger.Log("加载资源【" + m_path + "】花费时间(ms):" + (DateTime.Now - m_startLoadTime).TotalMilliseconds);
        }
        m_loadState = state;
    }

    /// <summary>
    /// 设置主资源对象
    /// </summary>
    /// <param name="mainAsset"></param>
    public void SetMainAsset(Object mainAsset)
    {
        m_mainAsset = mainAsset;
        if (mainAsset != null)
            m_mainAssetName = mainAsset.name;
    }

    /// <summary>
    /// 设置资源数组
    /// </summary>
    /// <param name="assets"></param>
    public void SetAssets(Object[] assets)
    {
        m_assets = assets;
        if (m_assets != null)
        {
            m_assetsName = new string[m_assets.Length];
            for (int i = 0; i < m_assets.Length; i++)
            {
                if (m_assets[i] != null)
                    m_assetsName[i] = m_assets[i].name;
                else
                    m_assetsName[i] = null;
            }
        }
    }

    /// <summary>
    /// 设置AssetBundle
    /// </summary>
    /// <param name="ab"></param>
    public void SetAssetBundle(AssetBundle ab)
    {
        m_assetBundle = ab;
    }

    public Object Load(string name, Type type)
    {
        if (m_assetBundle != null)
            return m_assetBundle.Load(name, type);
        if (m_mainAsset != null && m_mainAssetName == name && m_mainAsset.GetType() == type)
            return m_mainAsset;
        if (m_assets != null)
        {
            for (int i = 0; i < m_assets.Length; i++)
            {
                if (m_assetsName[i] == name && m_assets[i].GetType() == type)
                    return m_assets[i];
            }
        }
        return null;
    }

    /// <summary>
    /// 清除数据
    /// </summary>
    public void Clear(bool unloadAllLoadedObjects = false)
    {
        m_mainAsset = null;
        m_mainAssetName = null;
        m_assets = null;
        m_assetsName = null;
        if (m_assetBundle != null)
            m_assetBundle.Unload(unloadAllLoadedObjects);
        m_assetBundle = null;
        m_path = null;
        if (m_callBackFunc != null)
            m_callBackFunc.Clear();
        m_callBackFunc = null;
        m_type = EResType.None;
        m_tags = 0;
        m_bNeedCRC = false;
        m_CRC = 0;
    }

    /// <summary>
    /// 是否被加载过，即是否正在加载或已经加载完毕
    /// </summary>
    public bool IsLoaded{get { return m_loadState == EResItemLoadState.Loading || m_loadState == EResItemLoadState.Completed; }}
    public bool IsCompleted{get { return m_loadState == EResItemLoadState.Completed; }}
    public bool IsClearAfterLoaded{get { return m_clearAfterLoaded; } set { m_clearAfterLoaded = value; }}
    public bool IsSaveAssetBundle{get { return m_saveAssetBundle; }}
    public string Path{get { return m_path; }}
    public EResType Type{get { return m_type; }}
    public Object MainAsset { get { return m_assetBundle != null ? m_assetBundle.mainAsset : m_mainAsset; } }
    public Object[] Assets { get { return m_assetBundle != null ? m_assetBundle.LoadAll() : m_assets; } }
    public bool NeedCRC { get { return m_bNeedCRC; } set { m_bNeedCRC = value; } }
    public uint CRC { get { return m_CRC; } set { m_CRC = value; } }
    public int Tag { get { return m_tags; }}
}

public enum ELoadMode
{
    Resources,
    AssetBundle
}

public enum EResItemLoadState
{
    Wait,       //等待加载
    Loading,    //加载中
    Completed,  //加载完毕
    Error       //加载失败
}

public enum EResType
{
    None,
    Model,
    //Effect,
    UIEffect,
    AnimatorController,
    Animation,
    Audio,
    Atlas,
    UI,
    RenderTexture,
    Texture,
    Text,
    Scene,
    //BattlePrefab,
    //BattleWeapon,
    Config,
    Font,
    Material,
    Shader,
    Battle,
    UIModel
}

public static class ResTag
{
    public const int Forever = 0x00000001;
    public const int Battle =       0x00000002;
    public const int AssetBundle =  0x00000004;
}

public static class AtlasName
{
    public const string Common = "Common";
    public const string Nums = "Nums";
    public const string Battle = "Battle";
    public const string BattleNew = "BattleNew";
    public const string Settle = "Settle";
    public const string Weapon = "Weapon";
    public const string SmallMap = "SmallMap";
    public const string Hall = "Hall";
    public const string Room = "Room";
    public const string Activity = "Activity";
    public const string Setting = "Setting";
    public const string ICON = "Icon";
    public const string FRIEND = "Friend";
    public const string Guide = "Guide";
    public const string LimitGift = "LimitGift";
    public const string Package = "Package";
    public const string PVE = "PVE";
    public const string Strengthen = "Strengthen";
    public const string RankList = "RankList";
    public const string Role = "Role";
    public const string RoleIcon = "RoleIcon";
    public const string Chat = "Chat";
    public const string Corps = "Corps";
    public const string CorpsMatch = "CorpsMatch";
    public const string Background = "Background";
    public const string GunKing = "GunKing";
    public const string Legion = "Legion";
    public const string Survival = "Survival";
    public const string RechargeGift = "RechargeGift";
    public const string Member = "Member";
    public const string Event = "Event";
    public const string EventDynamic = "EventDynamic";
    public const string FightInTurn = "FightInTurn";
    public const string BattleSmallMap = "BattleSmallMap";
    public const string ShareAvatar = "ShareAvatar";
    public const string Master = "Master";
    public const string Achievement = "Achievement";
    public const string ArmyIcon = "ArmyIcon";
    public const string ActivityFrame = "ActivityFrame";
    public const string Login = "Login";  
    public const string SevenDay = "SevenDay";
    public const string Loading = "Loading";
    public const string BigHead = "BigHead";
    public const string ChooseHero = "ChooseHero";
    public const string QuickStart = "QuickStart";
    public const string Split = "Split";
    public const string LotteryNew = "LotteryNew";
	public const string LegionWar = "LegionWar";
    public const string WeiXinShare = "WeixinShare";
    public const string FlopCard = "FlopCard";
    public const string HeroCard = "HeroCard";
    public const string FightVideo = "FightVideo";
    public const string HeadIcon = "HeadIcon";
    public const string MicroBlog = "MicroBlog";
    public const string DaggerOW = "DaggerOW";
}