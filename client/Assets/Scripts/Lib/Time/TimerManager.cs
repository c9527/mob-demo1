﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 计时器管理类
/// 设计目的是提供O(1)时间复杂度的计时性能。为此，在添加计时器时做了链表的插入排序，其时间复杂度为O(1)到O(n)之间
/// 目前只提供延迟执行函数功能，不提供循环计时功能，该功能应该写在自身Update函数并统一由Driver调用，便于确定逻辑顺序
/// </summary>
public static class TimerManager
{
    private static int m_uidGlobal;                            //全局唯一的计时器id
    private static LinkedList<TimerVo> m_timerList;               //会被Time.timeScale影响的计时器列表
    private static LinkedList<TimerVo> m_timerIgnoreScaleList;    //忽略Time.timeScale影响的计时器列表
    private static readonly object m_queueLock = new object();

    public static void Init()
    {
        m_uidGlobal = 0;
        m_timerList = new LinkedList<TimerVo>();
        m_timerIgnoreScaleList = new LinkedList<TimerVo>();
    }

    /// <summary>
    /// 延迟执行函数
    /// </summary>
    /// <param name="action">函数</param>
    /// <param name="delay">延迟时间（秒）</param>
    /// <param name="ignoreTimeScale">是否忽略Time.timeScale的影响</param>
    /// <returns>计时器uid</returns>
    public static int SetTimeOut(float delay, Action action, bool ignoreTimeScale = false)
    {
        return SetTimeOutCore(action, null, null, delay, ignoreTimeScale);
    }

    /// <summary>
    /// 延迟执行函数
    /// </summary>
    /// <param name="action">函数</param>
    /// <param name="param">参数</param>
    /// <param name="delay">延迟时间（秒）</param>
    /// <param name="ignoreTimeScale">是否忽略Time.timeScale的影响</param>
    /// <returns>计时器uid</returns>
    public static int SetTimeOut(float delay, Action<object> action, object param, bool ignoreTimeScale = false)
    {
        return SetTimeOutCore(null, action, param, delay, ignoreTimeScale);
    }

    /// <summary>
    /// 移除指uid的计时器
    /// </summary>
    /// <param name="uid"></param>
    public static void RemoveTimeOut(int uid)
    {
        RemoveTimeOutCore(m_timerList, null, null, uid);
        RemoveTimeOutCore(m_timerIgnoreScaleList, null, null, uid);
    }

    /// <summary>
    /// 移除所有给定函数相关的计时器
    /// </summary>
    /// <param name="action">函数</param>
    public static void RemoveTimeOut(Action action)
    {
        RemoveTimeOutCore(m_timerList, action, null, 0);
        RemoveTimeOutCore(m_timerIgnoreScaleList, action, null, 0);
    }

    /// <summary>
    /// 移除所有给定函数相关的计时器
    /// </summary>
    /// <param name="action">函数</param>
    public static void RemoveTimeOut(Action<object> action)
    {
        RemoveTimeOutCore(m_timerList, null, action, 0);
        RemoveTimeOutCore(m_timerIgnoreScaleList, null, action, 0);
    }

    private static int SetTimeOutCore(Action action, Action<object> actionWithParams, object param, float delay, bool ignoreTimeScale = false)
    {
        //Logger.Error("setTimeOutCore, delay: " + delay + ", uid: " + (m_uidGlobal+1));
        if (delay > 0 && (action != null || actionWithParams != null))
        {
            m_uidGlobal++;
            LinkedList<TimerVo> list = ignoreTimeScale ? m_timerIgnoreScaleList : m_timerList;
            var vo = new TimerVo();
            vo.uid = m_uidGlobal;
            vo.action = action;
            vo.actionWithParams = actionWithParams;
            vo.param = param;
            vo.delay = delay;
            vo.ignoreTimeScale = ignoreTimeScale;
            vo.time = ignoreTimeScale ? Time.realtimeSinceStartup : Time.time;
            vo.endTime = vo.time + vo.delay;

            LinkedListNode<TimerVo> node = list.First;
            while (node != null)
            {
                if (vo.endTime < node.Value.endTime)
                {
                    lock(m_queueLock)
                        list.AddBefore(node, vo);
                    break;
                }
                node = node.Next;
            }
            if (node == null)
                lock (m_queueLock)
                    list.AddLast(vo);
        }
        else if(delay == 0)
        {
            if(action != null)
                action();

            if (actionWithParams != null)
                actionWithParams(param);
        }
        else
        {
            throw new Exception("注册时间管理函数参数有误！");
        }

        return m_uidGlobal;
    }

    /// <summary>
    /// 移除计时器Core，主要要确保action、actionWithParams、uid有且只有一个非空或非0
    /// </summary>
    /// <param name="list"></param>
    /// <param name="action"></param>
    /// <param name="actionWithParams"></param>
    /// <param name="uid"></param>
    private static void RemoveTimeOutCore(LinkedList<TimerVo> list, Action action, Action<object> actionWithParams, int uid)
    {
        var node = list.First;
        while (node != null)
        {
            if ((uid != 0 && node.Value.uid == uid) || 
                (action != null && node.Value.action == action) ||
                (actionWithParams != null && node.Value.actionWithParams == actionWithParams))
            {
                var delNode = node;
                node = node.Next;
                delNode.Value.action = null;
                delNode.Value.actionWithParams = null;
                delNode.Value.param = null;
                //Logger.Error("RemoveTimeOutCore, delay: " + delNode.Value.delay + ", uid: " + delNode.Value.uid);
                lock (m_queueLock)
                {
                    if (list.Contains(delNode.Value)) list.Remove(delNode);
                }
                //list.Remove(delNode);
            }
            else
                node = node.Next;
        }
    }

    public static void Update()
    {
        UpdateTimer(m_timerList, Time.time);
        UpdateTimer(m_timerIgnoreScaleList, Time.realtimeSinceStartup);
    }

    private static void UpdateTimer(LinkedList<TimerVo> list, float nowTime)
    {
        var node = list.First;
        while (node != null)
        {
            if (nowTime < node.Value.endTime)
                break;
            
            var delNode = node;
            node = node.Next;
            //Logger.Error("UpdateTimer, delay: " + delNode.Value.delay+", uid: "+delNode.Value.uid);
            lock (m_queueLock)
            {
                if (list.Contains(delNode.Value)) list.Remove(delNode);
            }

            if (delNode.Value.action != null)
                delNode.Value.action();
            if (delNode.Value.actionWithParams != null)
                delNode.Value.actionWithParams(delNode.Value.param);

            delNode.Value.action = null;
            delNode.Value.actionWithParams = null;
            delNode.Value.param = null;
        }
    }

    public static void Clear()
    {
        m_timerList.Clear();
        m_timerIgnoreScaleList.Clear();
    }

    private class TimerVo
    {
        public int uid;
        public float delay;
        public bool ignoreTimeScale;
        public float time;
        public float endTime;
        public Action action;                       //两个Action一定有且只有一个不为空
        public Action<object> actionWithParams;   //两个Action一定有且只有一个不为空
        public object param;
    }
}

