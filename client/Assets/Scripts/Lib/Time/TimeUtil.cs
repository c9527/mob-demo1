﻿using System;
using UnityEngine;

public static class TimeUtil
{
    private static DateTime m_startTime = new DateTime(1970, 1, 1).ToLocalTime();
    private static int _offset = 0;

    public static bool NowInRange(int minTimeStamp, int maxTimeStamp)
    {
        var nowTimeStamp = GetNowTimeStamp();
        return nowTimeStamp >= minTimeStamp && nowTimeStamp <= maxTimeStamp;
    }

    public static int GetNowTimeStamp()
    {
        return GetTimeStamp(GetNowTime());
    }

    public static DateTime GetNowTime()
    {
        return DateTime.Now.AddSeconds(_offset);
    }

    public static void updateTime(int offset)
    {
        _offset = offset;
    }

    public static DateTime GetTime(int timeStamp)
    {
        return m_startTime.AddSeconds(timeStamp);
    }

    public static DateTime GetTime(uint timeStamp)
    {
        return m_startTime.AddSeconds(timeStamp);
    }

    public static string GetRelativeTimeStr(int timeStamp)
    {
        int nowTimeStamp = GetNowTimeStamp();
        int diff = (nowTimeStamp - timeStamp);
        if (diff < 0 || diff > 86400)
        {
            return GetTime(timeStamp).ToString("yyyy-MM-dd HH:mm:ss");
        }
        if (diff > 3600)
        {
            return String.Format("{0}小时前", diff/3600);
        }
        return String.Format("{0}分钟前", diff / 60 + 1);
    }

    /// <summary>
    /// 获取从现在到截止时间的时间差
    /// </summary>
    /// <param name="deadlineStamp"></param>
    /// <returns></returns>
    public static TimeSpan GetRemainTime(uint deadlineStamp)
    {
        return GetTime(deadlineStamp) - GetNowTime();
    }

    /// <summary>
    /// 获取从指定时间到现在的时间差
    /// </summary>
    /// <param name="startStamp"></param>
    /// <returns></returns>
    public static TimeSpan GetPassTime(int startStamp)
    {
        return GetNowTime() - GetTime(startStamp);
    }

    public static int GetRemainTimeType(uint deadlineStamp)
    {
        if (deadlineStamp == 0)
            return 0;//永久

        var datetime = GetRemainTime(deadlineStamp);
        if (datetime.TotalDays > 0)
            return 1;//正常

        return -1;//过期
    }

    public static string GetRemainTimeString(uint deadlineStamp)
    {
        if (deadlineStamp == 0)
            return "永久";

        var datetime = GetRemainTime(deadlineStamp);
        if (datetime.TotalDays > 1)
            return string.Format("剩余{0}天", Mathf.CeilToInt((float)datetime.TotalDays));
        else if (datetime.TotalHours > 1)
            return string.Format("剩余{0}时", Mathf.CeilToInt((float)datetime.TotalHours));
        else if (datetime.TotalMinutes > 1)
            return string.Format("剩余{0}分", Mathf.CeilToInt((float)datetime.TotalMinutes));
        else if (datetime.TotalSeconds > 1)
            return string.Format("剩余{0}秒", Mathf.CeilToInt((float)datetime.TotalSeconds));

        return "已过期";
    }

    public static bool CheckItemRemainTime(uint deadlineStamp)
    {
        if (deadlineStamp == 0)
        {
            return false;
        }
        if (GetRemainTime(deadlineStamp).TotalDays <= 0)
        {
            if (deadlineStamp < PlayerSystem.roleData.last_logout_time)
            {
                return false;
            }
            else
            {
                if (PlayerSystem.last_intbattle_time != -1)
                {
                    if (deadlineStamp < PlayerSystem.last_intbattle_time)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static string GetRemainCountString(int count)
    {
        if (count == 0)
            return "已用完";
        return string.Format("剩余{0}个", count);
    }

    public static int GetTimeStamp(DateTime time)
    {
        return (int)(time - m_startTime).TotalSeconds;
    }

    /// <summary>
    /// 最近登录时间
    /// </summary>
    /// <param name="timeStamp"></param>
    /// <returns></returns>
    public static string LastLoginString(int timeStamp)
    {
        var passTime = GetPassTime(timeStamp);
        if (passTime.TotalDays >= 15)
            return "15天前";
        if (passTime.TotalDays >= 1)
            return passTime.Days + "天前";
        if (passTime.TotalHours >= 1)
            return passTime.Hours + "小时前";
        if (passTime.TotalMinutes >= 1)
            return passTime.Minutes + "分钟前";
        return "刚刚";
    }

    /// <summary>
    /// 将秒数格式化为00:00:00的字符串
    /// </summary>
    /// <param name="value"></param>
    /// <param name="mode">4-显示到天，2-显示到时，1-显示到分 0-自动适应</param>
    /// <returns></returns>
    public static string FormatTime(int value, uint mode = 3, bool isChineseFormat = false)
    {
        value = Mathf.Abs(value);
        string result = "";
        int time_vlaue = 0;
        bool need_split = false;
        if (mode == 0)
        {
            if (value >= (24 * 3600))
            {
                mode |= 7;
            }
            else if (value >= 3600)
            {
                mode |= 3;
            }
            else if (value >= 60)
            {
                mode |= 1;
            }
        }
        if ((mode & 4) > 0)
        {
            time_vlaue = value / (24 * 3600);
            value -= time_vlaue * 24 * 3600;
            result += (need_split ? ":" : "") + (time_vlaue < 10 ? "0" : "") + time_vlaue.ToString();
            mode |= 7;
            need_split = true;
        }
        if ((mode & 2) > 0)
        {
            time_vlaue = value / 3600;
            value -= time_vlaue * 3600;
            result += (need_split ? ":" : "") + (time_vlaue < 10 ? "0" : "") + time_vlaue.ToString();
            mode |= 3;
            need_split = true;
        }
        if ((mode & 1) > 0)
        {
            time_vlaue = value / 60;
            value -= time_vlaue * 60;
            result += (need_split ? ":" : "") + (time_vlaue < 10 ? "0" : "") + time_vlaue.ToString();
            mode |= 1;
            need_split = true;
        }
        result += (need_split ? ":" : "") + (value < 10 ? "0" : "") + value.ToString();
        if (isChineseFormat)
        {
            string[] ary = result.Split(new char[] { ':' });
            string[] aryName = { "秒", "分", "小时", "天" };
            string str = "";
            int lngth = ary.Length;
            for (int i = 0; i<lngth; i++)
            {
                str = ary[lngth - 1 - i] + aryName[i] + str;
            }
            result = str;
        }
        return result;
    }
}