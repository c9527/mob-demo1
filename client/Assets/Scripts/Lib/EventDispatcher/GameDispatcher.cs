﻿//负责游戏模块间事件通信

using System;

public class GameDispatcher
{
    private static EventDispatcher dispatcher = new EventDispatcher();

    private GameDispatcher() 
    {

    }

    #region 注册事件侦听器
    public static void AddEventListener(string type, EventCallback listener)
    {
        dispatcher.AddEventListener(type, listener);
    }

    public static void AddEventListener<T1>(string type, EventCallback<T1> listener)
    {
        dispatcher.AddEventListener<T1>(type, listener);
    }

    public static void AddEventListener<T1, T2>(string type, EventCallback<T1, T2> listener)
    {
        dispatcher.AddEventListener<T1,T2>(type, listener);
    }

    public static void AddEventListener<T1, T2, T3>(string type, EventCallback<T1, T2, T3> listener)
    {
        dispatcher.AddEventListener<T1, T2,T3>(type, listener);
    }

    public static void AddEventListener<T1, T2, T3, T4>(string type, EventCallback<T1, T2, T3, T4> listener)
    {
        dispatcher.AddEventListener<T1, T2, T3,T4>(type, listener);
    }

    public static void AddEventListener<T1, T2, T3, T4, T5>(string type, EventCallback<T1, T2, T3, T4, T5> listener)
    {
        dispatcher.AddEventListener<T1, T2, T3, T4,T5>(type, listener);
    }

    public static void AddEventListener(string type, Delegate listener)
    {
        dispatcher.RegisterEventListener(type, listener);
    }
    #endregion

    #region 删除事件侦听器
    public static void RemoveEventListener(string type, EventCallback listener)
    {
        dispatcher.RemoveEventListener(type, listener);
    }

    public static void RemoveEventListener<T1>(string type, EventCallback<T1> listener)
    {
        dispatcher.RemoveEventListener<T1>(type, listener);
    }

    public static void RemoveEventListener<T1, T2>(string type, EventCallback<T1, T2> listener)
    {
        dispatcher.RemoveEventListener<T1,T2>(type, listener);
    }

    public static void RemoveEventListener<T1, T2, T3>(string type, EventCallback<T1, T2, T3> listener)
    {
        dispatcher.RemoveEventListener<T1, T2,T3>(type, listener);
    }

    public static void RemoveEventListener<T1, T2, T3, T4>(string type, EventCallback<T1, T2, T3, T4> listener)
    {
        dispatcher.RemoveEventListener<T1, T2,T3, T4>(type, listener);
    }

    public static void RemoveEventListener<T1, T2, T3, T4, T5>(string type, EventCallback<T1, T2, T3, T4, T5> listener)
    {
        dispatcher.RemoveEventListener<T1, T2, T3, T4,T5>(type, listener);
    }

    public static void DeleteEventListener(string type, Delegate listener)
    {
        dispatcher.DeleteEventListener(type, listener);
    }
    #endregion

    #region 删除指定类型的所有关联事件侦听，如果参数为null则删除当前注册器中所有的函数侦听
    public static void RemoveEventListeners(string type = null)
    {
        dispatcher.RemoveEventListeners(type);
    }
    #endregion

    #region 派发事件
    public static void Dispatch(string type)
    {
        dispatcher.Dispatch(type);
    }
    public static void Dispatch<T1>(string type, T1 t1)
    {
        dispatcher.Dispatch<T1>(type, t1);
    }

    public static void Dispatch<T1, T2>(string type, T1 t1, T2 t2)
    {
        dispatcher.Dispatch<T1, T2>(type, t1, t2);
    }

    public static void Dispatch<T1, T2, T3>(string type, T1 t1, T2 t2, T3 t3)
    {
        dispatcher.Dispatch<T1, T2, T3>(type, t1, t2, t3);
    }

    public static void Dispatch<T1, T2, T3, T4>(string type, T1 t1, T2 t2, T3 t3, T4 t4)
    {
        dispatcher.Dispatch<T1, T2, T3, T4>(type, t1, t2, t3, t4);
    }

    public static void Dispatch<T1, T2, T3, T4, T5>(string type, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5)
    {
        dispatcher.Dispatch<T1, T2, T3, T4, T5>(type,t1,t2,t3,t4,t5);
    }

    #endregion

    #region 是否已经添加该类型的侦听事件
    public static bool HasEventListener(string type)
    {
        return dispatcher.HasEventListener(type);
    }
    #endregion

}