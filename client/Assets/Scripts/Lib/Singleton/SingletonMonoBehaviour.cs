﻿using UnityEngine;
using System;

/**
 * 该单例需要绑定某个对象，不确定该脚本是否存在的情况下，可用Exists 判断
 * */
public class SingletonMonoBehaviour<TClass> : MonoBehaviour where TClass : SingletonMonoBehaviour<TClass>
{
    private static TClass m_instance;
    private Transform m_kTrans;

    public static TClass Instance
    {
        get { return m_instance; }
    }

    protected virtual void Awake()
    {
        if (m_instance == null)
        {
            m_instance = (TClass)this;
            Exists = true;
        }
        else if (m_instance != this)
        {
            throw new InvalidOperationException("Cannot have two instances of a SingletonMonoBehaviour : " + typeof(TClass) + ".");
        }
    }

    public void DestroyImmediate()
    {
        if (m_instance == this)
        {
            Exists = false;
            m_instance = null;
        }

        Destroy(this.gameObject);
    }

    protected T GetComponent<T>(string path) where T : Component
    {
        var t = CachedTransform.FindChild(path);

        if (null == t)
            return default(T);

        var component = t.GetComponent<T>();

        return component;
    }

    protected virtual void OnDestroy()
    {
        if (m_instance == this)
        {
            Exists = false;
            m_instance = null;
        }
    }

    protected T AddComponent<T>() where T : Component
    {
		T component = GetComponent<T>();
        if (component == null)
			component = gameObject.AddComponent<T>();
		return component;
    }

    public static bool Exists
    {
        get;
        private set;
    }

    
    public Transform CachedTransform
    {
        get
        {
            if (null == m_kTrans)
                m_kTrans = transform;

            return m_kTrans;
        }
    }
}
