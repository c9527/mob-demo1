﻿using System.Collections.Generic;
using System.Diagnostics;

public class Singleton<T> where T: new()
{
    /// <summary>
    /// 保存单例列表，用于重置单例类的数据
    /// </summary>
    public static List<IResetData> singtonList = new List<IResetData>();

    private static readonly object _lock = new object();
    private static T instance;

    protected Singleton()
    {
        Debug.Assert(instance == null);
    }

	public static bool Exists
	{
	    get { return instance != null; }
	}
    
    public static T Instance
    {
        get
        {
            if (instance != null)
                return instance;
            lock (_lock)
            {
                if (instance != null)
                    return instance;
                instance = new T();

                if (instance is IResetData)
                    singtonList.Add(instance as IResetData);
            }
            return instance;
        }
    }
}
