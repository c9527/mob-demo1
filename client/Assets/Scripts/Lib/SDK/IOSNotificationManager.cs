﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;


public class iOSNotificationManager : MonoBehaviour
{
    public const int IS_ACTIVE = 1;
    //本地推送
#if UNITY_IPHONE
    public static void NotificationMessage(string message, int hour, int minute, bool isRepeatDay)
    {
        //Logger.Log("NotificationMessage1");
        int year = System.DateTime.Now.Year;
        int month = System.DateTime.Now.Month;
        int day = System.DateTime.Now.Day;
        System.DateTime newDate = new System.DateTime(year, month, day, hour, minute, 0);
        NotificationMessage(message, newDate, isRepeatDay);
    }

    public static void NotificationMessage(string message, System.DateTime newDate, bool isRepeatDay, CalendarUnit interval=CalendarUnit.Day)
    {
        //Logger.Log("NotificationMessage2");
        if(newDate > System.DateTime.Now)
        {
            LocalNotification localNotification = new LocalNotification();
            localNotification.fireDate = newDate;
            localNotification.alertBody = message;
            localNotification.applicationIconBadgeNumber = 1;
            localNotification.hasAction = true;
            if(isRepeatDay)
            {
                localNotification.repeatCalendar = CalendarIdentifier.ChineseCalendar;
                localNotification.repeatInterval = CalendarUnit.Day;
            }
            localNotification.soundName = LocalNotification.defaultSoundName;
            NotificationServices.ScheduleLocalNotification(localNotification);
            //Logger.Log("NotificationServices.ScheduleLocalNotification()");
        }
        else
        {
            //Logger.Log("newDate < System.DateTime.Now");
        }
    }

    void Awake()
    {
        //Logger.Log("Awake()");
		NotificationServices.RegisterForLocalNotificationTypes (LocalNotificationType.Alert|LocalNotificationType.Badge|LocalNotificationType.Sound);
        CleanNotification();
    }

     void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            //Logger.Log("pause");
            var notificationData = ConfigManager.GetConfig<ConfigLocalPush>().m_dataArr; 
            foreach(var data in notificationData)
            {
                int _isActive = int.Parse(data.isActive);
                int type = int.Parse(data.type);
                int day = int.Parse(data.day);
                int hour = int.Parse(data.hour);
                int minute = int.Parse(data.minute);
                if(_isActive == IS_ACTIVE)
                {
                    switch(type)
                    {
                        case 1://每日提醒
                            NotificationMessage(data.content, hour, minute, true);
                            //Logger.Log("everyday");
                            break;  
                        case 2://每周提醒
                            NotificationMessage(data.content, System.DateTime.Now.AddDays(7),true,CalendarUnit.Week);
                            //Logger.Log("everyweek");
                            break;
                        case 3://按条件设置提示时间
                            NotificationMessage(data.content, hour, minute, false);
                            break;
                        case 4://n日未登录提醒
                            NotificationMessage(data.content, DateTime.Now.AddDays(day), false);  
                            break;
                    }
                }
            }            
        }
        else
        {
            //Logger.Log("notPause");
            CleanNotification();
        }
    }

    void OnApplicationQuit()
    {
        //Logger.Log("OnApplicationQuit()");
        CleanNotification();
    }

    void CleanNotification()
     {
         //Logger.Log("CleanNotification");
         LocalNotification localNotification = new LocalNotification();
         localNotification.applicationIconBadgeNumber = -1;
         NotificationServices.PresentLocalNotificationNow(localNotification);
         NotificationServices.CancelAllLocalNotifications();
         NotificationServices.ClearLocalNotifications();
     }
#endif
}
