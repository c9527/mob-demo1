﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SdkFactory
{
    public SdkBase CreateSdk(GameObject gameObject,string sdkName)
    {
        switch (sdkName)
        {
            case "FnSdk":
                return gameObject.AddMissingComponent<AndroidSdkBase>();
#if UNITY_WEBPLAYER
            case "4399SDK":
                return gameObject.AddMissingComponent<WebPlayerSdkBase>();
#endif
            case "4399iossdk":
                return gameObject.AddMissingComponent<IosSdkBase>();
#if UNITY_STANDALONE
            case "4399SDK":
                return gameObject.AddMissingComponent<PcSdkBase>();
#endif
            case "RusSDK":
                return gameObject.AddMissingComponent<AndroidRusSdk>();
            case "EnSDK":
                return gameObject.AddMissingComponent<AndroidEnSdk>();
            case "4399iosRuSdk":
                return gameObject.AddMissingComponent<IosRuSdk>();
            case "4399iosEnSdk":
                return gameObject.AddMissingComponent<IosEnSdk>();
            default:
                return null;
        }
    }
    
}