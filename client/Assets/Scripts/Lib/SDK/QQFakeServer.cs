﻿using UnityEngine;
using System.Collections;

class QQFakeServer : MonoBehaviour
{
    const string SVR_URL = "http://houtai.zjqz.4343999.com/api/";
    Coroutine mRunning = null;

    public void Begin(string name)
    {
        if (mRunning != null)
        {
            StopCoroutine(mRunning);
            mRunning = null;
        }
        mRunning = StartCoroutine(WorkerFunc(name));
    }

    WWW SendMsg(string msg, params object[] args)
    {
        if (args.Length > 0)
            msg = string.Format(msg, args);
        return new WWW(SVR_URL + msg);
    }

    IEnumerator WorkerFunc(string name)
    {
        yield return SendMsg("login?pid={0}&plat=qq", SdkManager.m_pid);

        yield return new WaitForSeconds(5);

        yield return SendMsg("verify?name={0}&server={1}", name, SdkManager.m_serverId);

        while (Driver.isRunning)
        {
            yield return new WaitForSeconds(60);
            yield return SendMsg("refresh?name={0}&roleid={1}&time={2}", name, SdkManager.m_uid, Time.time);
        }
    }
}
