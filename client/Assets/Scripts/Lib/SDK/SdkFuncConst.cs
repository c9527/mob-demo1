﻿class SdkFuncConst
{
    public const string GET_SHARE_DATA = "GetShareData";
    public const string SHOW_PLATFORM_EXIT_DIALOG = "showPlatformExitDialog";   //内置退出对话框
    public const string SHOW_SPEAKER = "showSpeaker";   //语音
    public const string LOGOUT = "logout";
    public const string SHOW_LOCAL_PUSH = "GetLocalPush";
    public const string GET_SAVE_CAPTURE = "GetSaveCaptrue";
    public const string GET_SHARE = "GetShare";
    public const string GET_CHUSHOUTV = "GetChuShouTV";
}