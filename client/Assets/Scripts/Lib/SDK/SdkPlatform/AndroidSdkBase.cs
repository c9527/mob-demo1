﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class AndroidSdkBase : SdkBase
{
    private Dictionary<string, bool> m_supportFuncDic = new Dictionary<string, bool>();

    public override void Init()
    {
        SdkManager.m_pid = int.Parse(Global.Dic.GetValue("fn_pid", "0").ToString());
        SdkManager.m_fngid = Global.Dic.GetValue("fn_gameId", "").ToString();
        SetCallbackUnityObjectName(gameObject.name);
        SetDebug(true);
        InitSupportFuncCallBackUnity();
//        GetGetuiClientId();
    }

    /// <summary>
    /// 是否是测试模式，显示调试信息
    /// </summary>
    /// <param name="isDebug">Is debug.</param>
    public void SetDebug(bool isDebug)
    {
        PlatformAPI.Call("setDebug", isDebug.ToString());
    }

    /// <summary>
    /// 设置接收回调的gameobject的名字
    /// </summary>
    /// <param name="gameObjectName">Game object name.</param>
    public void SetCallbackUnityObjectName(string gameObjectName)
    {
        PlatformAPI.Call("setAndroidCallbackUnityObjectName", gameObjectName);
    }


    public void InitSupportFuncCallBackUnity()
    {
        PlatformAPI.Call("initSupportFuncCallBackUnity");
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowExitDialog();
        }
    }

    public override void Login()
    {
        PlatformAPI.Call("login");
    }

    public override void ValidateUploadPic(string url, Action<int> action)
    {
        Driver.singleton.StartCoroutine(AndroidValidateUploadPicViaWww(url, action));
    }

    private IEnumerator AndroidValidateUploadPicViaWww(string url, Action<int> action)
    {
        var www = new WWW(SdkManager.apiValidateUploadPicUrl + "?url=" + url + "&client_id=" + SdkManager.m_fngid);
        yield return www;
        while (!www.isDone)
        {
            yield return null;
        }
        if (String.IsNullOrEmpty(www.error))
        {
            CDict json = CJsonHelper.Load(www.text) as CDict;
            if (action != null)
            {
                action(json.Int("code"));
            }
        }
    }

//    public override void InitBugly(string channel, string version, string user)
//    {
//        BuglyAgent.ConfigDefault(channel, version, user, 10000);
//        BuglyAgent.InitWithAppId("900011471");
//        DebugLog(BuglyAgent.IsInitialized ? "[Bugly已启用]" : "[Bugly已关闭]");
//        if (BuglyAgent.IsInitialized)
//            BuglyAgent.EnableExceptionHandler();
//    }


    public override void Pay(PayMentInfo info)
    {
        //兼容旧包sdk的写法，两个方法都掉一下
        PlatformAPI.Call("pay", info.productName, info.productDesc, info.price,
                             info.productCount, info.rate, info.productId, info.coinName,
                             info.serverId, info.roleName, info.roleId, info.level, info.callbackInfo);
        PlatformAPI.Call("pay", SdkManager.m_uid, info.productName, info.productDesc, info.price,
                             info.productCount, info.rate, info.productId, info.coinName,
                             info.serverId, info.roleName, info.roleId, info.level, info.callbackInfo);
    }

    public override void SelectPic()
    {
        PlatformAPI.Call("selectPic");
    }

    public override void SelfiePic()
    {
        PlatformAPI.Call("selfiePic");
    }

    public override void Logout()
    {
        PlatformAPI.Call("logout");
    }

    public override void WeixinShare(string type, string imagePath)
    {
        if (type.Equals("wechat_friends"))
        {
            sendWeixinShareLog((int)EnumWeixinShare.FRIEND);
            SdkManager.m_logFrom = -1;
        }
        else if (type.Equals("wechat_moments"))
        {
            sendWeixinShareLog((int)EnumWeixinShare.FRIENDLINE);
            SdkManager.m_logFrom = -1;
        }
        PlatformAPI.Call("ShareCapture", type, imagePath, SdkManager.m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, PlayerSystem.roleData.level.ToString(), SdkManager.m_serverId, SdkManager.m_serverName);
    }


    public void sendAndroidLogToServer(int fromType, int shareBtn, int shareType, string chooseType)
    {
        string platformLog = string.Empty;
        if (SdkManager.m_pid == 166)
            platformLog = "yyb";
        else
            platformLog = EnumPlatform.android.ToString();
        Debug.Log(String.Format("sendAndroidLogToServer:fromType{0},shareBtn{1},shareType{2},chooseType{3}", fromType.ToString(), shareBtn, shareType, chooseType));
        NetLayer.Send(new tos_player_log_misc() { log_typ = fromType, log_n1 = shareBtn, log_n2 = shareType, log_s1 = platformLog, log_s2 = chooseType });
    }

    public override void SaveCapture(string imagePath)
    {
        DebugLog("SaveCapture--");
        TipsManager.Instance.showTips("已保存到相册");
        PlatformAPI.Call("saveCaptureToAlbum", imagePath);
    }

    public override void MultipleShare(string type, string imagePath)
    {
        Debug.Log(string.Format("MultipleShare:{0},{1},{2},{3},{4},{5},{6},{7}", type, imagePath, SdkManager.m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name,
            PlayerSystem.roleData.level.ToString(), SdkManager.m_serverId, SdkManager.m_serverName));
        PlatformAPI.Call("ShareCapture", type, imagePath, SdkManager.m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, 
            PlayerSystem.roleData.level.ToString(), SdkManager.m_serverId, SdkManager.m_serverName);
    }

    public override void GetShareData(string type, string info = null)
    {
        if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
        {
            UIManager.ShowNoThisFunc();
            return;
        }
        else
        {
            PlatformAPI.Call("GetShareData", type, SdkManager.m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, PlayerSystem.roleData.level.ToString(),
                SdkManager.m_serverId, SdkManager.m_serverName, !String.IsNullOrEmpty(info) ? info : "");
        }              
    }

    public override void SaveFileToPhotoLib(string url)
    {
        PlatformAPI.Call("SaveFileToPhotoLib", url);
    }

    public override void CopyToClipBoard(string value)
    {
        PlatformAPI.Call("CopyToClipBoard", value); 
    }


    public void UploadPic(string path)
    {
        PlatformAPI.Call("uploadPic", path);
    }

    public void CropPic(string inputPath, string outputPath, int aspectW = 1, int aspectH = 1, int width = 0, int height = 0)
    {
        PlatformAPI.Call("cropPic", inputPath, outputPath, aspectW, aspectH, width, height);
    }

    public void LocalPush()
    {
        ConfigLocalPushLine[] localPush = ConfigManager.GetConfig<ConfigLocalPush>().m_dataArr;
        if (localPush.Length == 0)
            return;
        string[] stringPush = new string[localPush.Length];
        for (int i = 0; i < localPush.Length; ++i)
        {
            var notice = localPush[i];
            stringPush[i] = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}", notice.ID, notice.type,
            notice.title, notice.content, notice.month, notice.day, notice.hour, notice.minute, notice.isActive);
            DebugLog("noticeInfo:" + notice.ID);
        }
        var param = string.Join("@", stringPush);
        DebugLog(param);
        PlatformAPI.Call("localPushStart", param);
    }

    /// <summary>
    /// 显示BBS
    /// </summary>
    public void ShowBBS()
    {
        PlatformAPI.Call("showBBS");
    }


    //=============================日志接口接口===============================
    /// <summary>
    /// 登录完成日志（登录完成时调用）.
    /// </summary>
    /// <param name="userId">User identifier.</param>
    public override void LogLoginFinish(string userId)
    {
        PlatformAPI.Call("logLoginFinish", userId);
    }

    /// <summary>
    /// 选服日志（选服完成时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="userName">User name.</param>
    /// <param name="serverId">Server identifier.</param>
    public override void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        PlatformAPI.Call("logSelectServer", roleLevel, userName, serverId);
    }

    /// <summary>
    /// 创建角色日志（创建角色完成时调用）
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public override void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
        PlatformAPI.Call("logCreateRole", roleId, roleName, serverId, serverName);
    }

    /// <summary>
    /// 进入游戏日志（用户选完服后，使用角色正式进入游戏时调用）：
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public override void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
        PlatformAPI.Call("logEnterGame", roleId, roleName, roleLevel, serverId, serverName);
    }


    /// <summary>
    /// 角色等级日志（角色升级时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.角色等级</param>
    /// <param name="serverId">Server identifier.服务器id</param>
    public override void LogRoleLevel(string roleLevel, string serverId)
    {
        PlatformAPI.Call("logRoleLevel", roleLevel, serverId);
    }

    public void TP2OnUserLogin(int accountType, int worldId, string openId, string roleId)
    {
        PlatformAPI.Call("TP2OnUserLogin", accountType, worldId, openId, roleId);
    }

    public void TP2SDKSetPopupOptions(int options)
    {
        PlatformAPI.Call("TP2SDKSetPopupOptions", options);
    }

    /// <summary>
    /// 显示悬浮窗
    /// </summary>
    public void ShowFloatBar()
    {
        PlatformAPI.Call("showFloatBar");
    }

    public void SetOauthInfo(string oauthInfo)
    {
        PlatformAPI.Call("setOauthInfo", oauthInfo);
    }

    /// <summary>
    /// 隐藏悬浮窗
    /// </summary>
    public void HideFloatBar()
    {
        PlatformAPI.Call("hideFloatBar");
    }

    /// <summary>
    /// 是否支持某方法
    /// </summary>
    /// <param name="functionName"></param>
    /// <returns></returns>
    public bool IsSupportFunction(string functionName)
    {
        return m_supportFuncDic.ContainsKey(functionName) && m_supportFuncDic[functionName];
    }

    /// <summary>
    /// 显示退出对话框
    /// </summary>
    public void ShowExitDialog()
    {
        PlatformAPI.Call("showExitDialog");
    }

//    public void GetGetuiClientId()
//    {
//        PlatformAPI.Call("getuiClientId");
//    }

    public string GetIconPath(string fileName = "icon.png")
    {
        string srcFilePath = Application.streamingAssetsPath + "/Icon/" + fileName;
        Logger.Error("src icon path:" + srcFilePath);
        Logger.Error("file exist" + (File.Exists(srcFilePath) ? "yes" : "no"));
        string destFilePath = ResourceManager.ExternalCacheDir + fileName;
        Logger.Error("target icon path:" + destFilePath);
        if (!File.Exists(destFilePath))
        {

            File.Copy(srcFilePath, destFilePath);
        }
        return destFilePath;
    }

    /// <summary>
    /// 切换用户
    /// </summary>
    public void SwitchUser()
    {
        PlatformAPI.Call("switchUser");
    }

    /// <summary>
    /// 释放SDK资源并退出游戏
    /// Releases the SDK and exit app.退出游戏前必须调用该接口
    /// </summary>
    public void ReleaseSDKAndExitApp()
    {
        PlatformAPI.Call("releaseSDKAndExitApp");
    }

    /// <summary>
    /// 检查更新
    /// </summary>
    public void CheckUpdate()
    {
        UIManager.ShowMask("检查更新中...");
        PlatformAPI.Call("checkUpdate");
    }


    public override void InitSpeaker(string config)
    {
        _speech_status = IsSupportFunction(SdkFuncConst.SHOW_SPEAKER) ? EnumSpeechStatus.INITIALIZED : EnumSpeechStatus.NONE;

    }

    public override void ShowSpeaker(bool builtin_ui = false, string url = null)
    {
        if (!IsSpeechInitialized())
        {
            return;
        }
        PlatformAPI.Call("showSpeaker", builtin_ui.ToString(), NetChatData.SPEECH_DATA_URL);
    }



    public override void CloseSpeaker(bool keep_recog)
    {
        if (!IsSpeechInitialized())
        {
            return;
        }
        PlatformAPI.Call("closeSpeaker");
    }

    public override void PlaySpeech(byte[] data)
    {
#if !UNITY_WEBPLAYER
        if (!IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            return;
        }
        FileStream file = File.Open(NetChatData.SPEECH_PLAY_URL, FileMode.Create);
        file.Write(data, 0, data.Length);
        file.Flush();
        file.Close();
        PlatformAPI.Call("playSpeech", NetChatData.SPEECH_PLAY_URL);
#endif
    }

    public override void StopSpeech()
    {
        if (!IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            return;
        }
        PlatformAPI.Call("stopSpeech");
    }

    public override void openCustomerServices(string serverId, string roleId)
    {
        //base.openCustomerServices(serverId, roleId);
    }

    public void ShowChuShouTV()
    {
        PlatformAPI.Call("showChuShouTV");

    }

    public void IsSupportChuShouTV()
    {
        PlatformAPI.Call("isSupportChuShouTV");
    }

    public override void ShowAssistant(string serverId)
    {
        //base.ShowAssistant(serverId);
    }

    public override void JoinVkOrFacebook()
    {
        base.JoinVkOrFacebook();
    }

    public override void ShareVkOrFacebook(string url, string title, string image, string desc)
    {
        base.ShareVkOrFacebook(url, title, image, desc);
    }

    public override void InviteFriendVkOrFacebool()
    {
        base.InviteFriendVkOrFacebool();
    }

    public override void ShareVkOrFacebookBattle(string url, string title, string image, string desc)
    {

    }



    #region Android-SDK回调
    public void AndroidSdkInitSuccess(string msg)
    {
        SdkLogger("AndroidSdkInitSuccess", msg);
    }

    public void AndroidSdkInitFailed(string msg)
    {
        SdkLogger("AndroidSdkInitFailed", msg);
        Driver.SendLogServer(1101, msg);
    }

    public void AndroidLoginComplete(string msg)
    {
        var json = CJsonHelper.Load(msg) as CDict;
        SdkManager.m_userName = json.Str("name");
        SdkManager.m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
        SdkManager.m_ext = json.Str("ext");

        var subJson = CJsonHelper.Load(SdkManager.m_ext) as CDict;
        SdkManager.m_pid = subJson.Int("fnpid");
        SdkManager.m_fnpidraw = subJson.Int("fnpidraw");
        SdkManager.m_fngid = subJson.Str("fngid");

        SdkLogger("AndroidLoginComplete", msg);
        ShowFloatBar();
        UIManager.HideMask();

        if (SdkManager.TokeyOverdue)
        {
            SdkManager.TokeyOverdue = false;
            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
        }

        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
        //        TestVerifyLogin(CJsonHelper.Save(json));
    }

    public void AndroidLoginError(string msg)
    {
        SdkManager.ClearLoginInfo();
        TipsManager.Instance.showTips(msg);
        SdkLogger("AndroidLoginError", msg);
        Driver.SendLogServer(1102, msg);
        UIManager.HideMask();
    }


    public void AndroidSdkSelectPicSuccess(string msg)
    {
        GameDispatcher.Dispatch(GameEvent.UI_SELECT_EXTERN_PIC, msg);
        SdkLogger("选择图片：", msg);
    }


    public void AndroidSdkSelectPicFail(string msg)
    {
        TipsManager.Instance.showTips("图片选择失败" + msg);
    }


    public void AndroidSdkUploadPicSuccess(string msg)
    {
        var json = CJsonHelper.Load(msg) as CDict;
        GameDispatcher.Dispatch(GameEvent.UI_UPLOAD_PIC, json.Str("inputPath"), json.Str("url"));
        SdkLogger("上传图片成功,返回url：", json.Str("url"));
    }

    public void AndroidSdkUploadPicFail(string msg)
    {
        TipsManager.Instance.showTips("上传图片失败:" + msg);
    }


    public void AndroidSdkSelfiePicSuccess(string msg)
    {
        GameDispatcher.Dispatch(GameEvent.UI_SELECT_EXTERN_PIC, msg);
        SdkLogger("自拍选择图片路径：", msg);
    }

    public void AndroidSdkCropPicSuccess(string msg)
    {
        var json = CJsonHelper.Load(msg) as CDict;

        GameDispatcher.Dispatch(GameEvent.UI_CROP_PIC, json.Str("inputPath"), json.Str("outputPath"));
        SdkLogger("裁剪图片返回路径：", json.Str("outputPath"));
    }

    //    public void AndroidVerifySuccess(string msg)
    //    {
    //        SdkLogger("AndroidVerifySuccess", msg);
    //
    //        var json = CJsonHelper.Load(msg) as CDict;
    //        Global.userName = json.Str("name");
    //        Global.uid = json.Str("uid");
    //        LogLoginFinish(Global.uid);
    //
    //        UIManager.HideMask();
    //    }

    public void AndroidSwitchUserSuccess(string msg)
    {
        var json = CJsonHelper.Load(msg) as CDict;
        SdkManager.m_userName = json.Str("name");
        SdkManager.m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
        SdkManager.m_ext = json.Str("ext");
        SdkManager.m_pid = (CJsonHelper.Load(SdkManager.m_ext) as CDict).Int("fnpid");

        SceneManager.ReturnToLogin();

        ShowFloatBar();
        SdkLogger("AndroidSwitchUserSuccess", msg);

        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
    }

    public void AndroidLoginOutSuccess(string msg)
    {
        SdkManager.ClearLoginInfo();
        UIManager.HideMask();

        SceneManager.ReturnToLogin();

        ShowFloatBar();
        SdkLogger("AndroidLoginOutSuccess", msg);
    }

    public void AndroidCheckUpdate(string msg)
    {
        SdkLogger("AndroidCheckUpdate", msg);

        switch (msg)
        {
            case "onNotSDCard": break;
            case "onNotNewVersion": break;
            case "onNormalUpdateLoading": break;
            case "onNetWorkError": break;
            case "onForceUpdateLoading": break;
            case "onException": break;
            case "onCheckVersionFailure": break;
            case "onCancelNormalUpdate": break;
        }

        UIManager.HideMask();
        GameDispatcher.Dispatch(GameEvent.SDK_CHECK_UPDATE_COMPLETE);
    }

    public void AndroidSdkPaySuccess(string msg)
    {
        SdkLogger("AndroidSdkPaySuccess", msg);
        PlayerSystem.TosGetRechargeData();
        UIManager.HideMask();

        NetLayer.Send(new tos_player_first_recharge_data());
        NetLayer.Send(new tos_player_recharge_welfare_list());

        var product_code = 0;
        var arr = msg.Split('|');
        if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
        {
            var info = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
            if (info != null)
            {
                if (info.Associator > 0)
                {
                    UIManager.PopPanel<PanelMemberEffect>(new object[] { info.Associator }, true, null);
                }
                TipsManager.Instance.showTips("充值获得" + info.Name);
            }
        }
    }

    public void AndroidSdkInitSupprotFunction(string msg)
    {
        SdkLogger("AndroidSdkInitSupprotFunction", msg);
        string[] msgs = msg.Split(Convert.ToChar(1));
        for (int i = 0; i < msgs.Length; i += 2)
        {
            string key = msgs[i];
            if (i + 1 >= msgs.Length)
            {
                DebugError("AndroidSdkInitSupprotFunction 数组越界");
                return;
            }
            string valueStr = msgs[i + 1];
            bool value = valueStr == "true";
            if (!m_supportFuncDic.ContainsKey(key))
            {
                m_supportFuncDic[key] = value;
            }
            else
            {
                m_supportFuncDic.Add(key, value);
            }
        }
    }

    public void AndroidSdkOnSpeechSuccess(string msg)
    {
#if !UNITY_WEBPLAYER
        byte[] audio_data = null;
        if (File.Exists(NetChatData.SPEECH_DATA_URL))
        {
            audio_data = File.ReadAllBytes(NetChatData.SPEECH_DATA_URL);
        }
        NetChatData.CacheSpeechData(audio_data);
        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, msg);
#endif
    }

    public void AndroidSdkOnSpeechError(string msg)
    {
        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, msg);
    }

    public void AndroidSdkOnSpeechBegin(string msg)
    {
        //开始说话
    }

    public void AndroidSdkOnSpeechVolumeChanged(string msg)
    {
        int volume = Int32.Parse(msg);
        PanelSpeechSpeaker.UpdateSpeaker(volume);
    }

    public void AndroidSdkOnSpeechEnd(string msg)
    {
        //说话结束
        PanelSpeechSpeaker.showSpeaker(1);
    }

    public void AndroidSdkOnSpeechPlayComplete(string msg)
    {
        //语音播放结束
    }

    public void AndroidSdkOnGetGetuiClientId(string msg)
    {
        //获取GetuiClientId
        SdkManager.m_getuiClientId = msg;

        SdkLogger("AndroidSdkOnGetGetuiClientId", msg);
    }

    public void AndroidSdkGetShareDataResult(string msg)
    {
        var data = msg.Split('|');
        if (data[0] == "1")
        {
            Singleton<EventModel>.Instance.share_url = data[1];
            Singleton<EventModel>.Instance.share_qcode_url = data[2];
            GameDispatcher.Dispatch(GameEvent.PROXY_EVENT_SHARE_DATA_UPDATED);
        }
        else
        {
            TipsManager.Instance.showTips(data.Length > 1 ? data[1] : "获取游戏分享地址失败");
        }
    }

    public void AndroidSdkSaveFileToPhotoLibResult(string msg)
    {
        var code = Int32.Parse(msg);
        if (code == 1)
        {
            UIManager.ShowTipPanel("保存失败，需要相册权限");
        }
        else if (code == 2)
        {
            UIManager.ShowTipPanel("文件数据错误");
        }
        else
        {
            TipsManager.Instance.showTips("已成功保存至相册");
        }
    }

    public void AndroidSdkIsSupportChuShouTV(string msg)
    {
        Debug.Log("fuckskkdka::" + msg);
        if (msg != null)
            PanelHallBattle.m_liveVideoGo.TrySetActive(Convert.ToBoolean(msg));
    }

    public void AndroidSdkShareLog(string msg)
    {
        if (msg != null)
        {
            int type = 0;
            int choose = 0;
            switch (msg)
            {
                case "wechat_friends":
                    type = (int)EnumSaveLog.WEIXIN_SHARE;
                    choose = 1;
                    break;
                case "wechat_moments":
                    type = (int)EnumSaveLog.WEIXIN_SHARE;
                    choose = 2;
                    break;
                case "qq_friends":
                    type = (int)EnumSaveLog.QQ_SHARE;
                    choose = 1;
                    break;
                case "qq_zone":
                    type = (int)EnumSaveLog.QQ_SHARE;
                    choose = 2;
                    break;
                case "sina_weibo":
                    type = (int)EnumSaveLog.WEIBO_SHARE;
                    choose = 1;
                    break;
            }
            //int fromType, int shareBtn, int shareType, string chooseType
            Debug.Log(String.Format("AndroidSdkShareLog:type{0},choose{1}", type, choose.ToString()));
            sendAndroidLogToServer(PanelWeixinShare.fromType, 2, type, choose.ToString());
        }
    }

    #endregion

}