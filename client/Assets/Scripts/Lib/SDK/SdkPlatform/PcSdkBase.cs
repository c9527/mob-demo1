﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using DockingModule;
using UnityEngine;

class PcSdkBase : SdkBase
{
    public override void Init()
    {
        PlatformAPI.ParseParams();
    }

    public override void Login()
    {
        PlatformAPI.Call("login");
    }


    public override void Pay(PayMentInfo info)
    {
        var param_data = new List<string>();
        param_data.Add("fnpid=" + SdkManager.m_fnpidraw);
        param_data.Add("fngid=" + SdkManager.m_fngid);
        param_data.Add("uid=" + SdkManager.m_uid);
        param_data.Add("name=" + WWW.EscapeURL(SdkManager.m_userName));
        param_data.Add("serverid=" + SdkManager.m_serverId);
        param_data.Add("ext=" + info.callbackInfo);
        param_data.Add("amount=" + info.price);
        param_data.Add("device_type=pc");
        Application.OpenURL(SdkManager.gamePayUrl + "?" + string.Join("&", param_data.ToArray()));
    }


    public override void Logout()
    {
        PlatformAPI.Call("logout");
    }


    public override void GetShareData(string type, string info = null)
    {
        DockingService.GetShareData();
    }

    public override void SaveFileToPhotoLib(string url)
    {
        PlatformAPI.Call("SaveFileToPhotoLib", url);
    }

    public override void CopyToClipBoard(string value)
    {
        var editor = new TextEditor();
        editor.content.text = value;
        editor.OnFocus();
        editor.Copy();
    }

    #region PC API

#if UNITY_STANDALONE
    [DllImport("Psapi.dll")]
    public static extern int GetModuleFileNameExW(IntPtr hProcess, IntPtr hModule, byte[] lpFilename, int nSize);
#endif

    public static void GetProcessList(string filter = "")
    {

#if UNITY_STANDALONE

        byte[] buf = new byte[1024];
        Encoding enc = Encoding.Unicode;        

        List<CDict> listProc = new List<CDict>();
        Regex reg = new Regex(filter);
        foreach (var ps in System.Diagnostics.Process.GetProcesses())
        {
            CDict dic = new CDict();
            try
            {
                string name = ps.ProcessName;
                if (string.IsNullOrEmpty(name))
                    continue;
                if (!reg.IsMatch(name))
                    continue;
                dic.Add("ProcessName", name);
                dic.Add("Id", ps.Id);
                dic.Add("StartTime", ps.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
                //var file = ps.MainModule.FileVersionInfo;
                //dic.Add("FileName", file.FileName);
                //dic.Add("Desc", file.FileDescription);
                //dic.Add("ProductName", file.ProductName);

                int len = GetModuleFileNameExW(ps.Handle, IntPtr.Zero, buf, buf.Length / 2);
                if (len > 0)
                {
                    string fileName = enc.GetString(buf, 0, len * 2);
                    dic.Add("FileName", fileName);
                }            
            }
            catch (Exception)
            { continue; }
            listProc.Add(dic);
        }

        NetLayer.SendMsg("player", "processlist", listProc);
#endif

    }



    #endregion
}