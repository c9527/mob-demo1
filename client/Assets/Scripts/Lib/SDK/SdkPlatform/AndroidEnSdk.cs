﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


class AndroidEnSdk : AndroidSdkBase
{
    public override void Logout()
    {
        SceneManager.ReturnToLogin();
        PlatformAPI.Call("switchUser");
    }

    public override void Pay(PayMentInfo info)
    {
        PlatformAPI.Call("Pay", info.serverId, info.price, info.roleId, info.level, info.Sku, "true", info.callbackInfo);
    }

    #region 英语版回调
    public void AndroidLoginCompleteEn(string msg)
    {
        //TipsManager.Instance.showTips("登录啦");


        var json = CJsonHelper.Load(msg) as CDict;
        SdkManager.m_userName = json.Str("name");
        SdkManager.m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
        string m_signStr = json.Str("signstr");
        SdkManager.timestamp = json.Int("timestamp");
        SdkManager.m_pid = 0;
        SdkManager.m_fnpidraw = 0;
        SdkManager.m_fngid = "0";
        SdkManager.m_ext = m_signStr;
        Debug.LogError(SdkManager.m_ext + "dddd");
        SdkLogger("AndroidLoginCompleteRus", msg);
        //ShowFloatBar();
        UIManager.HideMask();

        if (SdkManager.TokeyOverdue)
        {
            SdkManager.TokeyOverdue = false;
            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
        }

        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
        //        TestVerifyLogin(CJsonHelper.Save(json));
    }

    public void EnPaySuccess(string msg)
    {
        TipsManager.Instance.showTips("支付成功");
        SdkLogger("AndroidSdkPaySuccess", msg);
        PlayerSystem.TosGetRechargeData();
        UIManager.HideMask();

        NetLayer.Send(new tos_player_first_recharge_data());
        NetLayer.Send(new tos_player_recharge_welfare_list());

        var product_code = 0;
        var arr = msg.Split('|');
        if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
        {
            var info = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
            if (info != null)
            {
                if (info.Associator > 0)
                {
                    UIManager.PopPanel<PanelMemberEffect>(new object[] { info.Associator }, true, null);
                }
                TipsManager.Instance.showTips("充值获得" + info.Name);
            }
        }


    }

    public override void ShowAssistant(string serverId)
    {
        PlatformAPI.Call("showAssistant",serverId);
    }

    
    

    #endregion
    #region 英语版日志
    public override void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
        PlatformAPI.Call("CreateRoleLog", roleName);
    }

    public override void LogRoleLevel(string roleLevel, string serverId)
    {
        PlatformAPI.Call("RoleLevelUpLog", int.Parse(roleLevel));
    }

    public override void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        PlatformAPI.Call("SelectServerLog", serverId);
    }

    public override void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
    }

    public override void LogLoginFinish(string userId)
    {

    }

    public override void LoadFinishBeforeLoginLog()
    {

    }

    public override void ClickEnterGameLog()
    {

    }

    public override void RoleLoginLog(string name)
    {

    }

    public override void JoinVkOrFacebook()
    {
        base.JoinVkOrFacebook();
    }

    public override void ShareVkOrFacebook(string url, string title, string image, string desc)
    {
        //base.ShareVkOrFacebook(url, title, image, desc);
        PlatformAPI.Call("ShareToFaceBook", title, desc, image, PlayerSystem.roleId.ToString(), url);
    }
    #endregion
}

