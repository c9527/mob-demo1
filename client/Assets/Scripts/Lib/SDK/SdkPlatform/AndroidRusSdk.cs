﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class AndroidRusSdk : AndroidSdkBase
{

    public override void Logout()
    {
        SceneManager.ReturnToLogin();
        PlatformAPI.Call("switchUser");
    }

    public void ShowLogoutWin()
    {
        PlatformAPI.Call("logout");
    }


    public override void Pay(PayMentInfo info)
    {
        PlatformAPI.Call("ssjjPayment", info.serverId, info.price, info.roleId, info.level,info.Sku,"true",info.callbackInfo );
    }

    public override void ShowAssistant(string serverId)
    {
        //base.ShowAssistant(serverId);
    }
    

    public override void openCustomerServices(string serverId, string roleId)
    {
        PlatformAPI.Call("OpenCustomerServices", serverId, int.Parse(roleId));
    }


    #region 俄语版回调
    public void AndroidLoginCompleteRus(string msg)
    {
        //TipsManager.Instance.showTips("登录啦");


        var json = CJsonHelper.Load(msg) as CDict;
        SdkManager.m_userName = json.Str("name");
        SdkManager.m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
        string m_signStr = json.Str("signstr");
        SdkManager.timestamp = json.Int("timestamp");
        SdkManager.isNewTempUser = json.Bool("isNewTempUser");
        SdkManager.m_pid = 0;
        SdkManager.m_fnpidraw = 0;
        SdkManager.m_fngid = "0";
        SdkManager.m_ext = m_signStr;
        Debug.LogError(SdkManager.m_ext + "dddd");
        SdkLogger("AndroidLoginCompleteRus", msg);
        //ShowFloatBar();
        UIManager.HideMask();

        if (SdkManager.TokeyOverdue)
        {
            SdkManager.TokeyOverdue = false;
            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
        }

        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);

        //        TestVerifyLogin(CJsonHelper.Save(json));
    }

    public void RusPaySuccess(string msg)
    {
        TipsManager.Instance.showTips("支付成功");
        SdkLogger("AndroidSdkPaySuccess", msg);
        PlayerSystem.TosGetRechargeData();
        UIManager.HideMask();

        NetLayer.Send(new tos_player_first_recharge_data());
        NetLayer.Send(new tos_player_recharge_welfare_list());

        var product_code = 0;
        var arr = msg.Split('|');
        if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
        {
            var info = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
            if (info != null)
            {
                if (info.Associator > 0)
                {
                    UIManager.PopPanel<PanelMemberEffect>(new object[] { info.Associator }, true, null);
                }
                TipsManager.Instance.showTips("充值获得" + info.Name);
            }
        }


    }

    public void RusPayFailure(string msg)
    {
        TipsManager.Instance.showTips("支付失败");
    }

    


    public void AndroidLoginErrorRus(string msg)
    {
        TipsManager.Instance.showTips("登录错误");
    }

    public void AndroidLoginCancelRus(string msg)
    {
        TipsManager.Instance.showTips("登录取消");
    }


    public void AndroidJoinVk(string msg)
    {
        TipsManager.Instance.showTips("加入成功");
        NetLayer.Send(new tos_player_social_joingroup() { platform = "vk" });
        NetLayer.Send(new tos_player_social_join_media());
    }

    public void AndroidInviteFriend(string msg)
    {
        TipsManager.Instance.showTips("邀请成功");
        int id;
        if(int.TryParse(msg,out id)){
            NetLayer.Send(new tos_player_social_invite_friend() { friend_id = id });
        }
    }






    #endregion

    #region 俄语版日志

    public override void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
        PlatformAPI.Call("CreateRoleLog",roleName);
    }

    public override void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
        PlatformAPI.Call("EnterSuccessLog");
    }

    public override void LogRoleLevel(string roleLevel, string serverId)
    {
        PlatformAPI.Call("RoleLevelUpLog", int.Parse(roleLevel));
    }

    public override void LogLoginFinish(string userId)
    {
        
    }

    public override void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        PlatformAPI.Call("SelectServerLog", serverId);
    }

    public override void LoadFinishBeforeLoginLog()
    {
        PlatformAPI.Call("EndLoadingLog");
    }

    public override void ClickEnterGameLog()
    {
        PlatformAPI.Call("ClickEnterGameLog");
    }

    public override void RoleLoginLog(string name)
    {
        PlatformAPI.Call("RoleLoginLog", name);
    }

    public override void JoinVkOrFacebook()
    {
        base.JoinVkOrFacebook();
        PlatformAPI.Call("joinVKGroup", 133109711, "https://vk.com/ssjj.tatru");
    }

    public override void ShareVkOrFacebook(string url, string title, string image, string desc)
    {
        PlatformAPI.Call("shareToVk", url, title, image, desc);
        TimerManager.SetTimeOut(5, () => { shareSuccess(); });
    }


    void shareSuccess()
    {
        NetLayer.Send(new tos_player_social_share_game());
        TipsManager.Instance.showTips("分享完成");
    }

    public override void ShareVkOrFacebookBattle(string url, string title, string image, string desc)
    {
        PlatformAPI.Call("shareToVk", url, title, image, desc);
    }

    public override void InviteFriendVkOrFacebool()
    {
        base.InviteFriendVkOrFacebool();
        PlatformAPI.Call("GetVkFriendUninstall");
    }
    



    #endregion
}