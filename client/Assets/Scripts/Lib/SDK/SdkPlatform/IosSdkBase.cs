﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

class IosSdkBase : SdkBase
{
    private const string appkey = "1448259251130020";
    private const string appSecret = "16f8455ffee11d28c48dca19d52ab537";
    private const string channelId = "10";

    private const string title = "枪神小妹求好评";
    private const string alertMsg = "感谢您对《枪战英雄》的支持，如果觉得还不错，就打赏个五星好评吧！";
    private const string okLab = "1";
    private const string cancelLab = "1";
    private const string otherLab = "1";
    public const int THREEDAY = 259200;//3天秒数259200
    private static List<Action<int>> _iosValidatePicCallback = new List<Action<int>>();


    public override void Init()
    {
        SdkManager.m_fngid = appkey;
        init4399SDK(appkey, appSecret, channelId, gameObject.name);//初始化SDK
        appOpenLog(appkey, appSecret);//启动APP
        appBeforeLoginLog();
        initGeTuiSDK();
    }

    public override void Login()
    {
        appLogin();
    }

    public override void ValidateUploadPic(string url, Action<int> action)
    {
        appIOSValidatePhoto(url, action);
    }

    public override void Pay(PayMentInfo info)
    {
        var payId = Global.Dic.GetValue("GameDefineIdentifer") + "." + info.CfgRecharge.AppStoreId;
        appPay(SdkManager.m_uid, info.serverId, info.roleId, payId, info.productCount, info.callbackInfo);
        UIManager.ShowMask("正在充值，请稍后...", 100);
    }

    public override void SelectPic()
    {
        appIOSLocalPhoto(TimeUtil.GetNowTimeStamp() + ".png", ResourceManager.ExternalCacheDir);
    }

    public override void SelfiePic()
    {
        appIOSTakePhoto(TimeUtil.GetNowTimeStamp() + ".png", ResourceManager.ExternalCacheDir);
    }

    public override void Logout()
    {
        appLogOut();
    }

    public virtual void switchSdkAccount()
    {
    }

    public override void ShowAssistant(string serverId)
    {
        
    }

    public override void WeixinShare(string type, string imagePath)
    {
        var typeInt = Convert.ToInt32(type);
        if (typeInt == (int)EnumWeixinShare.FRIEND + 1)
        {
            sendWeixinShareLog((int)EnumWeixinShare.FRIEND);
            SdkManager.m_logFrom = -1;
        }
        else if (typeInt == (int)EnumWeixinShare.FRIENDLINE + 1)
        {
            sendWeixinShareLog((int)EnumWeixinShare.FRIENDLINE);
            SdkManager.m_logFrom = -1;
        }
        appWeixinShare(typeInt, imagePath);
    }

    public override void SaveCapture(string imagePath)
    {
        DebugLog("SaveCapture--");
        TipsManager.Instance.showTips("已保存到相册");
        appSaveCaptureToAlbum(imagePath);
    }

//    public override void MultipleShare(string type, string imagePath)
//    {
//        Debug.Log(string.Format("MultipleShare:{0},{1},{2},{3},{4},{5},{6},{7}", type, imagePath, m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, PlayerSystem.roleData.level.ToString(), m_serverId, m_serverName));
//        PlatformAPI.Call("ShareCapture", type, imagePath, m_uid, PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, PlayerSystem.roleData.level.ToString(), m_serverId, m_serverName);
//    }

    public override void GetShareData(string type, string info = null)
    {
        getIOSShareData(PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name,SdkManager.m_serverName, PlayerSystem.roleData.level.ToString());
    }

    public override void SaveFileToPhotoLib(string url)
    {
        appNSLog("SaveFileToPhotoLib-url:" + url);
        SaveCapture(url);
    }

    public override void CopyToClipBoard(string value)
    {
        appCopyToClipBoard(value);
    }


    public void ExchangeStatistic(string exchangeCode)
    {
        string roleName = PlayerSystem.roleData.name;
        appExchangeStatistic(exchangeCode, roleName);
    }

    // ios
    public void getStarComment()
    {
        appNSLog("getStarComment()");
        var address = getAddress(GameConst.FIVE_STARS_COMMENT_ADDRESS);
        if (address != null)
        {
            appStarComment(title, alertMsg, okLab, cancelLab, otherLab, address);
        }
    }

    //ios
    public string getAddress(string type)
    {
        ConfigXcodeParameterLine xpCfg = ConfigManager.GetConfig<ConfigXcodeParameter>().GetLine(type);
        if (xpCfg == null)
            return null;
        var address = xpCfg.Content;
        return address;
    }

    //ios
    public void getIOSShareData(string roleId, string roleName, string serverName, string roleLevel)
    {
        appGetIOSShareData(roleId, roleName, serverName, roleLevel);
    }

    public void IOSShareLogin()
    {
        appIOSShareLogin(PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, SdkManager.m_serverName, PlayerSystem.roleData.level.ToString());
    }

    public override void InitSpeaker(string config)
    {
        _speech_status = EnumSpeechStatus.INITIALIZED;
    }

    /// <summary>
    /// 打开语音
    /// </summary>
    public override void ShowSpeaker(bool builtin_ui = false, string url = null)
    {
        appShowSpeaker(builtin_ui.ToString(), !String.IsNullOrEmpty(url) ? url : NetChatData.SPEECH_DATA_URL);
    }

    /// <summary>
    /// 关闭语音
    /// keep_recog：是否关闭录音而继续识别（web版适用）
    /// </summary>
    public override void CloseSpeaker(bool keep_recog = false)
    {
        if (!IsSpeechInitialized())
        {
            return;
        }
        appCloseSpeaker();
    }

    /// <summary>
    /// 播放语音
    /// </summary>
    public override void PlaySpeech(byte[] data)
    {
#if !UNITY_WEBPLAYER
        FileStream file = File.Open(NetChatData.SPEECH_PLAY_URL, FileMode.Create);
        file.Write(data, 0, data.Length);
        file.Flush();
        file.Close();
        appPlaySpeech(NetChatData.SPEECH_PLAY_URL);
#endif

    }

    /// <summary>
    /// 停止播放语音
    /// </summary>
    public override void StopSpeech()
    {
        appStopSpeech();
    }


    #region OC接口
    private static void init4399SDK(string theAppKey, string appSecret, string chanelID, string reciever)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCinit4399SDK(theAppKey, appSecret, chanelID, reciever);
#endif
    }

    private static void appOpenLog(string theAppKey, string appSecret)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappOpenLog(appkey, appSecret);
#endif
    }

    private static void appBeforeLoginLog()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
         OCappBeforeLoginLog();
#endif
    }

    private static void appLogin()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappLogin();
#endif
    }
    private static void appLogOut()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappLogOut();
#endif
    }

    private static void ChoseServerLog(string serverId)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCChoseServerLog(serverId);
#endif
    }
    private static void createRoleLogWithRoleName(string roleName, string serverId)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCcreateRoleLogWithRoleName(roleName, serverId);
#endif
    }
    private static void roleLevelLogWithRoleLevel(string roleLevel)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCroleLevelLogWithRoleLevel(roleLevel);
#endif
    }
    private static void appReset()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappReset();
#endif
    }

    private static void appPay(string uid, string serverId, string roleId, string productId, string productCount, string callbackInfo)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappPay(uid, serverId, roleId, productId, productCount, callbackInfo);
#endif
    }

    private static void appExchangeStatistic(string exchangeCode, string roleName)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappExchangeStatistic(exchangeCode, roleName);
#endif
    }

    private static void appStarComment(string title, string alertMsg, string okLab, string cancelLab, string otherLab, string address)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappStarComment (title, alertMsg, okLab, cancelLab, otherLab, address);
#endif
    }

    private static void appWeixinShare(int type, string imagePath)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappWeixinShare (type, imagePath);
#endif
    }

    private static void appSaveCaptureToAlbum(string imagePath)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappSaveCaptureToAlbum (imagePath);
#endif
    }

    private static void appPlaySpeech(string content)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCappPlaySpeech (content);
#endif
    }

    private static void appStopSpeech()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCappStopSpeech ();
#endif
    }

    private static void appShowSpeaker(string isBuildingUI, string url)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCappShowSpeaker (isBuildingUI, url);
#endif
    }

    private static void appCloseSpeaker()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCappCloseSpeaker ();
#endif
    }

    private static void appGetIOSShareData(string roleId, string roleName, string serverName, string roleLevel)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappGetIOSShareData (roleId, roleName, serverName, roleLevel);
#endif
    }

    private static void appIOSShareLogin(string roleId, string roleName, string serverName, string roleLevel)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappIOSShareLogin (roleId, roleName, serverName, roleLevel);
#endif
    }

    private static void appCopyToClipBoard(string text)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappCopyToClipBoard (text);
#endif
    }

    private static void appIOSTakePhoto(string fileName, string filePath)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappTakePhoto(fileName, filePath);
#endif
    }

    private static void appIOSLocalPhoto(string fileName, string filePath)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappLocalPhoto(fileName, filePath);
#endif
    }

    private static void appIOSValidatePhoto(string url, Action<int> action)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
        _iosValidatePicCallback.Add(action);
		OCappIOSValidatePhoto(url);
#endif
    }

    public static void LogCallBack(string condition, string stackTrace, LogType type)
    {
#if ActiveSDK && UNITY_IPHONE
		OCappNSLog(string.Format("{0} - [{1}]: {2}\t[TRACE]: {3}", DateTime.Now.ToString("MM-dd HH:mm:ss"), type, condition.TrimEnd(), stackTrace));
#endif
    }

    public static void appNSLog(string msg)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappNSLog(msg);
#endif
    }

    private void initGeTuiSDK()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_ZH
		OCinitGeTuiSDK();
#endif
    }

    [DllImport("__Internal")]
    private static extern void OCinit4399SDK(string theAppKey, string appSecret, string chanelID, string reciever);

    [DllImport("__Internal")]
    private static extern void OCappOpenLog(string theAppKey, string appSecret);

    [DllImport("__Internal")]
    private static extern void OCappLogin();

    [DllImport("__Internal")]
    private static extern void OCappLogOut();

    [DllImport("__Internal")]
    private static extern void OCChoseServerLog(string serverId);

    [DllImport("__Internal")]
    private static extern void OCroleLevelLogWithRoleLevel(string roleLevel);

    [DllImport("__Internal")]
    private static extern void OCappReset();

	[DllImport("__Internal")]
	private static extern void OCappPay (string uid, string serverId, string roleId, string productId, string productCount, string callbackInfo);

	[DllImport("__Internal")]
	private static extern void OCappNSLog (string msg);

    [DllImport("__Internal")]
	private static extern void OCappExchangeStatistic (string exchangeCode, string roleName);

	[DllImport("__Internal")]
	private static extern void OCappStarComment (string title, string alertMsg, string okLab, string cancelLab, string otherLab, string address);

	[DllImport("__Internal")]
	private static extern void OCappWeixinShare (int type, string imagePath);

	[DllImport("__Internal")]
	private static extern void OCappSaveCaptureToAlbum (string imagePath);

	[DllImport("__Internal")]
	private static extern void OCappGetIOSShareData (string roleId, string roleName, string serverName, string roleLevel);

	[DllImport("__Internal")]
	private static extern void OCappIOSShareLogin (string roleId, string roleName, string serverName, string roleLevel);

	[DllImport("__Internal")]
	private static extern void OCappCopyToClipBoard (string text);

    [DllImport("__Internal")]
	private static extern void OCappTakePhoto (string fileName, string filePath);

    [DllImport("__Internal")]
	private static extern void OCappLocalPhoto (string fileName, string filePath);


	[DllImport("__Internal")]
	private static extern void OCappIOSValidatePhoto (string url);

#if LANG_ZH
	[DllImport("__Internal")]
	private static extern void OCcreateRoleLogWithRoleName(string roleName, string serverId);

	[DllImport("__Internal")]
	private static extern void OCappPlaySpeech (string content);
	
	[DllImport("__Internal")]
	private static extern void OCappStopSpeech ();
	
	[DllImport("__Internal")]
	private static extern void OCappShowSpeaker (string isBuildingUI, string url);
	
	[DllImport("__Internal")]
	private static extern void OCappCloseSpeaker ();

    [DllImport("__Internal")]
	private static extern void OCappBeforeLoginLog ();

	[DllImport("__Internal")]
	private static extern void OCinitGeTuiSDK ();
#endif

    #endregion


    #region IOS-SDK回调
    //登陆回调

    public void IOS_4399_Login(string param)
    {
        //username,suid,timestamp,signStr,targetServerId,verifyToken,comeFrom
        DebugLog("IOS_4399_Login=" + param);
       
        var json = CJsonHelper.Load(param) as CDict;
        //        var tempPid = json.Str("comeFrom");
        SdkManager.m_userName = json.Str("username");
        SdkManager.m_uid = json.Str("suid");  //平台返回的uid可能为空，要使用验证后的uid
        SdkManager.m_ext = json.Str("verifyToken");
        //        m_pid = Int32.Parse(tempPid);
        SdkManager.m_pid = 2;
        SdkManager.m_platform_id = 2;
        appNSLog("IOSengineCallBack" + param);
        //ShowFloatBar ();
        UIManager.HideMask();

        if (SdkManager.TokeyOverdue)
        {
            SdkManager.TokeyOverdue = false;
            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
        }

        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
    }

    public void IOS_4399_PurchaseCompelted(string param)
    {
        UIManager.HideMask();
    }

    public void IOS_4399_ValidatePicResult(string result)
    {
        appNSLog("校验图片成功,返回url：" + result);
        if (_iosValidatePicCallback.Count > 0)
        {
            if (result.Contains("ok"))
            {
                _iosValidatePicCallback[0](1);
            }
            else if (result.Contains("delete"))
            {
                _iosValidatePicCallback[0](-1);
            }
            else
            {
                _iosValidatePicCallback[0](0);
            }
            _iosValidatePicCallback.RemoveAt(0);
        }
    }

    public void IOS_4399_UploadPic(string msg)
    {
        appNSLog("上传图片成功,返回url：" + msg);
        GameDispatcher.Dispatch(GameEvent.UI_UPLOAD_PIC, "name", msg);
    }


    public void IOS_4399_PurchaseFailed(string param)
    {
        UIManager.HideMask();
    }

    //取消登陆
    public void IOS_4399_CancelLogin(string param)
    {
        DebugLog("#IOS_4399_CancelLogin");
    }

    public void IOS_4399_SdkPaySuccess(string msg)
    {
       
        PlayerSystem.TosGetRechargeData();
        SdkManager.payCallback = msg;
    }

    public void IOS_4399_SpeechSuccess(string msg)
    {
        appNSLog("IOS_4399_SpeechSuccess");
        byte[] audio_data = null;
#if !UNITY_WEBPLAYER
        if (File.Exists(NetChatData.SPEECH_DATA_URL))
        {
            appNSLog("IOS_4399_SpeechSuccess11");
            audio_data = File.ReadAllBytes(NetChatData.SPEECH_DATA_URL);
            string dataString = string.Empty;
            foreach (byte b in audio_data)
            {
                dataString += b.ToString();
            }
            appNSLog("dataString:" + dataString);
        }
#endif
        NetChatData.CacheSpeechData(audio_data);
        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, msg);
    }

    public void IOS_4399_SpeechError(string msg)
    {
        appNSLog("IOS_4399_SpeechError:" + msg);
        PanelSpeechSpeaker.closeSpeaker();
        TipsManager.Instance.showTips("您好像没有说话哦");
    }

    public void IOS_4399_SpeechVolumeChanged(string msg)
    {
        int volume = Int32.Parse(msg);
        PanelSpeechSpeaker.UpdateSpeaker(volume);
    }

    public void IOS_4399_SpeechEnd(string msg)
    {
        //说话结束
        PanelSpeechSpeaker.showSpeaker(1);
    }

    public void IOS_4399_SdkComment(string param)
    {
        
        appNSLog("IOS_4399_SdkComment");
        switch (param)
        {
            case "Comment":
                GlobalConfig.clientValue.isComment_thisVersion = true;
                GlobalConfig.SaveClientValue("isComment_thisVersion");
                sendLogToServer((int)EnumSaveLog.STAR_COMMENT, SdkManager.m_logFrom, (int)EnumStarComment.COMMENT);
                SdkManager.m_logFrom = -1;
                appNSLog("Comment");
                break;
            case "Complaint":
                UIManager.PopPanel<PanelAdvice>(null, true);
                GlobalConfig.clientValue.last_comment_stamp = TimeUtil.GetNowTimeStamp();
                GlobalConfig.SaveClientValue("last_comment_stamp");
                sendLogToServer((int)EnumSaveLog.STAR_COMMENT, SdkManager.m_logFrom, (int)EnumStarComment.COMPLAINT);
                SdkManager.m_logFrom = -1;
                break;
            case "NextTime":
                GlobalConfig.clientValue.last_comment_stamp = TimeUtil.GetNowTimeStamp();
                GlobalConfig.SaveClientValue("last_comment_stamp");
                sendLogToServer((int)EnumSaveLog.STAR_COMMENT, SdkManager.m_logFrom, (int)EnumStarComment.NEXTTIME);
                SdkManager.m_logFrom = -1;
                appNSLog("NextTime");
                break;
            default:
                break;
        }
    }


    public bool isGoComment()
    {
        var isCommentOpen = GlobalConfig.serverValue.fiveStarComment_open;
        var isThisVersion = GlobalConfig.clientValue.last_comment_stamp != 1;
        var isComment = GlobalConfig.clientValue.isComment_thisVersion;
        var isAfterFiveDay = TimeUtil.GetNowTimeStamp() - GlobalConfig.clientValue.last_comment_stamp > THREEDAY;
        return isCommentOpen && isThisVersion && isAfterFiveDay && !isComment;
    }

    //ios
    public void SdkPaySuccess(string msg)
    {
        
        //SdkLogger("AndroidSdkPaySuccess", msg);
        PlayerSystem.TosGetRechargeData();
        UIManager.HideMask();

        //NetLayer.Send(new tos_player_first_recharge_data());
        //NetLayer.Send(new tos_player_recharge_welfare_list());

        var product_code = 0;
        var arr = msg.Split('|');
        if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
        {
            var payInfo = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
            if (payInfo != null)
            {
                if (payInfo.Associator > 0)
                {
                    appNSLog("payInfo.Associator:" + payInfo.Associator);
                }
                string AdditionalTip = null;
                if (payInfo.AdditionalDiamond > 0)
                {
                    AdditionalTip = string.Format("\n(额外获得{0}钻石)", payInfo.AdditionalDiamond);
                }
                UIManager.ShowTipPanel("恭喜充值获得" + payInfo.Name + AdditionalTip, () =>
                {
                    if (payInfo.Associator > 0)
                    {
                        UIManager.PopPanel<PanelMemberEffect>(new object[] { payInfo.Associator }, true, null);
                    }
                    else
                    {
                        if (isGoComment())
                        {
                            getStarComment();
                            SdkManager.m_logFrom = 3;
                        }
                    }
                    SdkManager.payCallback = null;
                }, null, false, false, "确定");
            }
        }
    }

    public void IOS_4399_SdkShareDataResult(string param)
    {
        
        var json = CJsonHelper.Load(param) as CDict;
        var come = json.Str("come");
        var qrcode = json.Str("qrcode");
        //var qrdata = json.Str("qrdata");
        var url = json.Str("url");
        if (come.Equals("come"))
        {
            Singleton<EventModel>.Instance.share_qcode_url = qrcode;
            Singleton<EventModel>.Instance.share_url = url;
            GameDispatcher.Dispatch(GameEvent.PROXY_EVENT_SHARE_DATA_UPDATED);
        }
        else
        {
            TipsManager.Instance.showTips("获取游戏分享地址失败");
        }
    }

    public void IOS_4399_NotWeixinInstall()
    {
       
        TipsManager.Instance.showTips("您尚未安装微信");
    }

    public void IOS_4399_GetClientId(string msg)
    {

        SdkManager.m_getClientId = msg;
    }

    public void IOS_4399_GetDeviceToken(string msg)
    {

        SdkManager.m_getDeviceToken = msg;
    }

    public void IOS_4399_GeTuiPushPayBack(string msg)
    {
        
        appNSLog("IOS_4399_GeTuiPushPayBack:" + msg);
        UIManager.ShowTipPanel(msg);
    }

    #endregion

    //=============================日志接口接口===============================



    /// <summary>
    /// 选服日志（选服完成时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="userName">User name.</param>
    /// <param name="serverId">Server identifier.</param>
    public override void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        ChoseServerLog(serverId);
    }

    /// <summary>
    /// 创建角色日志（创建角色完成时调用）
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public override void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
        createRoleLogWithRoleName(roleName, serverId);
    }


    /// <summary>
    /// 角色等级日志（角色升级时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.角色等级</param>
    /// <param name="serverId">Server identifier.服务器id</param>
    public override void LogRoleLevel(string roleLevel, string serverId)
    {
        roleLevelLogWithRoleLevel(roleLevel);
    }
}
