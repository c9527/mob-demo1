﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

class IosRuSdk : IosSdkBase
{
    private const string appkey = "1448259251130020";
    private const string appSecret = "16f8455ffee11d28c48dca19d52ab537";
    private const string channelId = "10";

    public override void Init()
    {
        init4399SDK(appkey, appSecret, channelId, gameObject.name);//初始化SDK
        appOpenLog(appkey, appSecret);//启动APP
        appLoadStartBeforeLoginLog();
    }

    public override void LoadFinishBeforeLoginLog()
    {
        appLoadFinishBeforeLoginLog();
    }

    public override void ClickEnterGameLog()
    {
        appClickEnterGameLog();
    }

    public override void RoleLoginLog(string name)
    {
        appRoleLoginLog();
    }

    public override void ShowAssistant(string serverId)
    {
        
    }

    public override void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
        appLogEnterGameSuccess(roleId, roleName, roleLevel, serverId, serverName);
    }

    public override void Pay(PayMentInfo info)
    {
        var payId = Global.Dic.GetValue("GameDefineIdentifer") + "." + info.CfgRecharge.AppStoreId;
        Debug.Log("PayRu PayId:" + info.CfgRecharge.AppStoreId);
        Debug.Log("SdkManager.SdkName:" + SdkManager.SdkName);
        Debug.Log("PayRu PayId:" + info.CfgRecharge.AppStoreId);
        appPayRu(info.CfgRecharge.AppStoreId, info.callbackInfo);
        UIManager.ShowMask("正在充值，请稍后...", 100);
    }

    public override void Logout()
    {
        appLogOut();
    }

    public override void switchSdkAccount()
    {
        appSwitchSdkAccount();
    }

    public static void openCustomerService()
    {
        appOpenCustomerService(SdkManager.m_serverId, PlayerSystem.roleId.ToString());
    }

    #region IOS-SDK回调
    //登陆回调

	public void IOS_4399_Login(string param)
	{
		//username,suid,timestamp,signStr,targetServerId,verifyToken,comeFrom
		DebugLog("IOS_4399_Login=" + param);
		
		var json = CJsonHelper.Load(param) as CDict;
		//        var tempPid = json.Str("comeFrom");
		SdkManager.m_userName = json.Str("username");
		SdkManager.m_uid = json.Str("suid");  //平台返回的uid可能为空，要使用验证后的uid
		SdkManager.m_ext = json.Str("suidSignStr");
		SdkManager.timestamp = json.Int("timestamp");
		//        m_pid = Int32.Parse(tempPid);
		SdkManager.m_pid = 2;
		SdkManager.m_platform_id = 2;
		appNSLog("IOSengineCallBack" + param);
		//ShowFloatBar ();
		UIManager.HideMask();
		
		if (SdkManager.TokeyOverdue)
		{
			SdkManager.TokeyOverdue = false;
			GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
		}
		
		GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
	}

	public void IOS_4399_PurchaseFailedRu(string param)
	{
		UIManager.HideMask();
		TipsManager.Instance.showTips (param);
	}
	
	
	public void SdkPaySuccessRu(string msg)
	{
		UIManager.HideMask();
		TipsManager.Instance.showTips ("充值成功!");
		//SdkLogger("AndroidSdkPaySuccess", msg);
		PlayerSystem.TosGetRechargeData();
		UIManager.HideMask();
		
		//NetLayer.Send(new tos_player_first_recharge_data());
		//NetLayer.Send(new tos_player_recharge_welfare_list());
		
		var product_code = 0;
		var arr = msg.Split('|');
		if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
		{
			var payInfo = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
			if (payInfo != null)
			{
				if (payInfo.Associator > 0)
				{
					appNSLog("payInfo.Associator:" + payInfo.Associator);
				}
				string AdditionalTip = null;
				if (payInfo.AdditionalDiamond > 0)
				{
					AdditionalTip = string.Format("\n(额外获得{0}钻石)", payInfo.AdditionalDiamond);
				}
				UIManager.ShowTipPanel("恭喜充值获得" + payInfo.Name + AdditionalTip, () =>
				{
					if (payInfo.Associator > 0)
					{
						UIManager.PopPanel<PanelMemberEffect>(new object[] { payInfo.Associator }, true, null);
					}
					SdkManager.payCallback = null;
				}, null, false, false, "确定");
			}
		}
	}

    public void IOS_4399_SdkPaySuccess(string msg)
    {
        PlayerSystem.TosGetRechargeData();
        SdkManager.payCallback = msg;
    }    

    //=============================日志接口接口===============================
    /// <summary>
    /// 选服日志（选服完成时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="userName">User name.</param>
    /// <param name="serverId">Server identifier.</param>
    public override void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        ChoseServerLog(serverId);
    }

    /// <summary>
    /// 创建角色日志（创建角色完成时调用）
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
	public override void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
	{
		ruCreateRoleLogWithRoleName (roleName);
		ruRoleCreateRolePage(roleName);
		//		else
		//        	createRoleLogWithRoleName(roleName, serverId);
	}


    /// <summary>
    /// 角色等级日志（角色升级时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.角色等级</param>
    /// <param name="serverId">Server identifier.服务器id</param>
    public override void LogRoleLevel(string roleLevel, string serverId)
    {
        roleLevelLogWithRoleLevel(roleLevel);
    }


    #region OC接口
    private static void init4399SDK(string theAppKey, string appSecret, string chanelID, string reciever)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCinit4399SDK(theAppKey, appSecret, chanelID, reciever);
#endif
    }

    private static void appOpenLog(string theAppKey, string appSecret)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappOpenLog(appkey, appSecret);
#endif
    }

    private static void appLogin()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappLogin();
#endif
    }
    private static void appLogOut()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappLogOut();
#endif
    }

    private static void appLoadStartBeforeLoginLog()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappLoadStartBeforeLoginLog();
#endif
    }

    private static void appLoadFinishBeforeLoginLog()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappLoadFinishBeforeLoginLog();
#endif
    }
    private static void appClickEnterGameLog()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappClickEnterGameLog();
#endif
    }

    private static void appRoleLoginLog()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappRoleLoginLog();
#endif
    }

    private static void appLogEnterGameSuccess(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappLogEnterGameSuccess(roleId, roleName, roleLevel, serverId, serverName);
#endif
    }

    private static void appOpenCustomerService(string serverId, string roleId)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappOpenCustomerService(serverId, roleId);
#endif
    }
    
    private static void ChoseServerLog(string serverId)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCChoseServerLog(serverId);
#endif
    }
//    private static void createRoleLogWithRoleName(string roleName, string serverId)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCcreateRoleLogWithRoleName(roleName, serverId);
//#endif
//    }
	private static void ruCreateRoleLogWithRoleName(string roleName)
	{
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
		OCruCreateRoleLogWithRoleName(roleName);
#endif
	}
	
	private static void ruRoleCreateRolePage(string roleName)
	{
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
		OCruRoleCreateRolePage(roleName);
#endif
	}
    private static void roleLevelLogWithRoleLevel(string roleLevel)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCroleLevelLogWithRoleLevel(roleLevel);
#endif
    }
    private static void appReset()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
         OCappReset();
#endif
    }

    private static void appPayRu(string productId, string callbackInfo)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
		OCappPayRu(productId, callbackInfo);
#endif
    }

    public static void LogCallBack(string condition, string stackTrace, LogType type)
    {
#if ActiveSDK && UNITY_IPHONE
		OCappNSLog(string.Format("{0} - [{1}]: {2}\t[TRACE]: {3}", DateTime.Now.ToString("MM-dd HH:mm:ss"), type, condition.TrimEnd(), stackTrace));
#endif
    }

    public static void appNSLog(string msg)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		OCappNSLog(msg);
#endif
    }

    private static void appSwitchSdkAccount()
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR && LANG_RU
         OCappSwitchSdkAccount();
#endif
    }
    #endregion

    [DllImport("__Internal")]
    private static extern void OCinit4399SDK(string theAppKey, string appSecret, string chanelID, string reciever);

    [DllImport("__Internal")]
    private static extern void OCappOpenLog(string theAppKey, string appSecret);

    [DllImport("__Internal")]
    private static extern void OCappLogin();

    [DllImport("__Internal")]
    private static extern void OCappLogOut();

    [DllImport("__Internal")]
    private static extern void OCChoseServerLog(string serverId);

	//    [DllImport("__Internal")]
	//    private static extern void OCcreateRoleLogWithRoleName(string roleName, string serverId);

    [DllImport("__Internal")]
    private static extern void OCroleLevelLogWithRoleLevel(string roleLevel);

    [DllImport("__Internal")]
    private static extern void OCappReset();

    [DllImport("__Internal")]
    private static extern void OCappNSLog(string msg);

#if LANG_RU
	[DllImport("__Internal")]
	private static extern void OCruCreateRoleLogWithRoleName(string roleName);
	
	[DllImport("__Internal")]
	private static extern void OCruRoleCreateRolePage(string roleName);

	[DllImport("__Internal")]
	private static extern void OCappPayRu(string productId, string callbackInfo);

    [DllImport("__Internal")]
	private static extern void OCappOpenCustomerService(string serverId, string roleId);

    [DllImport("__Internal")]
	private static extern void OCappLoadStartBeforeLoginLog();

    [DllImport("__Internal")]
	private static extern void OCappLoadFinishBeforeLoginLog();

    [DllImport("__Internal")]
	private static extern void OCappClickEnterGameLog();

    [DllImport("__Internal")]
	private static extern void OCappRoleLoginLog(); 
   
    [DllImport("__Internal")]
    private static extern void OCappSwitchSdkAccount();
    
    [DllImport("__Internal")]
	private static extern void OCappLogEnterGameSuccess(string roleId, string roleName, string roleLevel, string serverId, string serverName);

#endif

    #endregion
}
