﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DockingModule;
using UnityEngine;

class WebPlayerSdkBase:SdkBase
{
    public override void Init()
    {
        PlatformAPI.ParseParams();
    }

    public override void Login()
    {
        PlatformAPI.Call("login");
    }


    public override void Pay(PayMentInfo info)
    {
        var param_data = new List<string>();
        param_data.Add("fnpid=" + SdkManager.m_fnpidraw);
        param_data.Add("fngid=" + SdkManager.m_fngid);
        param_data.Add("uid=" + SdkManager.m_uid);
        param_data.Add("name=" + WWW.EscapeURL(SdkManager.m_userName));
        param_data.Add("serverid=" + SdkManager.m_serverId);
        param_data.Add("ext=" + info.callbackInfo);
        param_data.Add("amount=" + info.price);
        param_data.Add("device_type=web");
        PlatformAPI.Call("openPayWindow", "qmqs", "S" + SdkManager.m_serverId, SdkManager.m_uid, 10, null, SdkManager.m_userName, PlayerSystem.roleId, PlayerSystem.roleData.name);
    }

   
    public override void Logout()
    {
        PlatformAPI.Call("logout");
    }


    public override void GetShareData(string type, string info = null)
    {
        DockingService.GetShareData();
    }

    public override void SaveFileToPhotoLib(string url)
    {
        PlatformAPI.Call("SaveFileToPhotoLib", url);
    }

    public override void CopyToClipBoard(string value)
    {
        var editor = new TextEditor();
        editor.content.text = value;
        editor.OnFocus();
        editor.Copy();
    }


    public override void InitSpeaker(string config)
    {
        if (_speech_status == EnumSpeechStatus.NONE)
        {
            _speech_status = EnumSpeechStatus.INITIALIZING;
            PlatformAPI.Call("UBAdapter.initSpeaker", config);
            _speech_run_time = Time.time;
            WakeupSpeechTimer();
        }
    }

    public override void ShowSpeaker(bool builtin_ui = false, string url = null)
    {
        if (!IsSpeechInitialized())
        {
            return;
        }
        PlatformAPI.Call("UBAdapter.startSpeaker");
        _speech_run_time = Time.time;
        WakeupSpeechTimer();
    }



    public override void CloseSpeaker(bool keep_recog)
    {
        if (!IsSpeechInitialized())
        {
            return;
        }
        _speech_run_time = keep_recog ? Time.time : 0;
        if (keep_recog)
        {
            PlatformAPI.Call("UBAdapter.stopSpeaker");
        }
        else
        {
            PlatformAPI.Call("UBAdapter.cancelSpeaker");
        }
    }

    public override void PlaySpeech(byte[] data)
    {
        StringBuilder value = new StringBuilder();
        foreach (byte b in data)
        {
            value.Append(b.ToString("X2"));
        }
        PlatformAPI.Call("UBAdapter.playSpeech", value.ToString());
    }

    public override void StopSpeech()
    {
        PlatformAPI.Call("UBAdapter.stopSpeech");
    }


    #region web JS-SDK回调

    /// <summary>
    /// msg：step|volume|(result[type|code|msg])
    /// step: NONE-0,INITIALIZED-1,RECORD-2,RECOGNIZE-3,COMPLETE-4
    /// volume: 音量大小
    /// result: type error,result
    ///         code error: 请参考Develop Manual for flash SDK_V3第六章错误码定义
    ///              result：大于0语音秒数，小于0参考rsltStatus值取负
    ///         msg 返回的解释结果或者错误消息
    /// </summary>
    /// <param name="step"></param>
    /// <param name="volume"></param>
    /// <param name="result"></param>
    private string speech_result_cache;
    private void JSUpdateSpeechData(string msg)
    {
        var data = !string.IsNullOrEmpty(msg) ? msg.Split('|') : null;
        //0-step,1-volume,2-type,3-code,4-msg
        if (data[0] == "1")
        {
            var last_step = _speech_status;
            _speech_status = EnumSpeechStatus.INITIALIZED;
            if (last_step == EnumSpeechStatus.INITIALIZING)
            {
                PanelSpeechSpeaker.showSpeaker();
            }
        }
        else if (data[0] == "2")
        {
            PanelSpeechSpeaker.UpdateSpeaker(int.Parse(data[1]));
        }
        else if (data[0] == "3")
        {
            PanelSpeechSpeaker.showSpeaker(1);
        }
        else if (data[0] == "4")
        {
            _speech_run_time = 0;
            if (data[2] == "result")
            {
                if (data[3] != "0")
                {
                    speech_result_cache = data[4] + "|" + data[3];
                    PlatformAPI.Call("UBAdapter.getSpeechAudioData");
                }
                else
                {
                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, "您好像没有说话哦");
                }
            }
            else
            {
                if (data[3] == "120103" || data[3] == "10105")
                {
                    _speech_status = EnumSpeechStatus.NONE;
                    if (data[3] == "10105")
                    {
                        PlatformAPI.Call("showSettingView", true);
                    }
                }
                if (data[3] != "10118")
                {
                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, data[4]);
                }
                else
                {
                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, "您好像没有说话哦");
                }
            }
        }
    }

    private void JSUpdateSpeechAudioData(string msg)
    {
        byte[] audio_data = null;
        if (!string.IsNullOrEmpty(msg))
        {
            audio_data = new byte[msg.Length / 2];
            for (var i = 0; i < audio_data.Length; i++)
            {
                audio_data[i] = Convert.ToByte(msg.Substring(i * 2, 2), 16);
            }
        }
        NetChatData.CacheSpeechData(audio_data);
        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, speech_result_cache);
    }
    #endregion

}