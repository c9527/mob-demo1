﻿using System;
using UnityEngine;
using System.Security.Cryptography;
using System.Text;

enum Tp2Opt
{
    SUSPICIOUS_PROCESS = 1,        // 可疑进程 
    SUSPICIOUS_MODULE = 2,        // 可疑模块 
    ILLEGAL_CERTIFICATE = 4,         // 非法签名 
    TAMPERED_INSTRUCTION = 8,    // 内存修改 
    MUTABLE_SPEED = 16,            // 加速减速 
    TAMPERED_HOST = 32,           // host 修改 
    UNWELCOME_APPS = 64,         // 软件卸载 
    ALL = 127   // 全选 
}

public static class PlatformAPI
{
    #region MTP

    public const int TP2_ENTRYID_OTHERS = 99;   // MTP 其它平台ID
    public const int TP2_ENTRYID_QQ = 1;        // MTP QQ平台ID

    public const int SUSPICIOUS_PROCESS = 1;        // 可疑进程 
    public const int SUSPICIOUS_MODULE = 2;        // 可疑模块 
    public const int ILLEGAL_CERTIFICATE = 4;         // 非法签名 
    public const int TAMPERED_INSTRUCTION = 8;    // 内存修改 
    public const int MUTABLE_SPEED = 16;            // 加速减速 
    public const int TAMPERED_HOST = 32;           // host 修改 
    public const int UNWELCOME_APPS = 64;         // 软件卸载 
    public const int ALL = 127;   // 全选 

    #endregion



#if UNITY_IPHONE
	public static void Call(string funcName, params object[] param)
	{
	}

    public static T Call<T>(string funcName, params object[] param)
	{
         return default(T);
	}

    public static string[] GetAllProcessName()
    {
        return null;
    }

    public static int GetFreeMemory()
    {
        return -1;
    }

    public static int GetUsedMemory()
    {
        return -1;
    }

    public static bool IsLowMemory()
    {
        return false;
    }
#elif UNITY_ANDROID
    private static AndroidJavaObject m_activity;

    public static void Call(string funcName, params object[] param)
    {
        if (Application.platform != RuntimePlatform.Android)
            return;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            m_activity.Call(funcName, param);
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
        }
        
    }

    public static T Call<T>(string funcName, params object[] param)
    {
        if (Application.platform != RuntimePlatform.Android)
            return default(T);

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            return m_activity.Call<T>(funcName, param);
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
        }
        return default(T);
    }

    public static void FixRechargeBug()
    {
        if (Application.platform != RuntimePlatform.Android)
            return;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            m_activity.Set("mUid", SdkManager.m_uid);
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
        }
    }

    public static string[] GetAllProcessName()
    {
        if (Application.platform != RuntimePlatform.Android)
            return null;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            string data = m_activity.Call<string>("GetAllProcessName");
            return data.Split('#');
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
            return null;
        }
    }

    public static int GetFreeMemory()
    {
        if (Application.platform != RuntimePlatform.Android)
            return -1;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            int ram = m_activity.Call<int>("GetFreeMemory");
            return ram;
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
            return -1;
        }
    }

    public static int GetUsedMemory()
    {
        if (Application.platform != RuntimePlatform.Android)
            return -1;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            return m_activity.Call<int>("GetUsedMemory");
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
            return -1;
        }
    }

    public static bool IsLowMemory()
    {
        if (Application.platform != RuntimePlatform.Android)
            return false;

        if (m_activity == null)
        {
            var ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_activity = ajc.GetStatic<AndroidJavaObject>("currentActivity");
        }
        try
        {
            return m_activity.Call<bool>("IsLowMemory");
        }
        catch (Exception e)
        {
            Logger.Error(e.Message + "\n" + e.StackTrace);
            return false;
        }
    }   
#elif UNITY_WEBPLAYER
    public static void Call(string funcName, params object[] param)
    {
        Application.ExternalCall(funcName, param);
    }

    public static T Call<T>(string funcName, params object[] param)
    {
         return default(T);
    }

    public static string[] GetAllProcessName()
    {
        return null;
    }

    public static int GetFreeMemory()
    {
        return -1;
    }

    public static int GetUsedMemory()
    {
        return -1;
    }

    public static bool IsLowMemory()
    {
        return false;
    }
#else
    public static void Call(string funcName, params object[] param)
    {

    }
    
    public static string[] GetAllProcessName()
    {
        return null;
    }

    public static int GetFreeMemory()
    {
        return -1;
    }

    public static int GetUsedMemory()
    {
        return -1;
    }

    public static bool IsLowMemory()
    {
        return false;
    }

#endif

    public static void ParseParams()
    {
        CDict json = null;
#if UNITY_WEBPLAYER
        var src_value = Application.srcValue;
        if (!string.IsNullOrEmpty(src_value))
        {
            Debug.Log("webplayer src value: " + src_value);
            var param_index = src_value.IndexOf('?');
            if (param_index!=-1)
            {
                var param_data = src_value.Substring(param_index + 1).Split('&');
                if (param_data != null && param_data.Length > 0)
                {
                    json = new CDict();
                    foreach (var value in param_data)
                    {
                        var data = value.Split('=');
                        if (data != null && data.Length > 1)
                        {
                            json.Add(data[0], data[1]);
                        }
                    }
                }
            }
        }
#elif UNITY_STANDALONE
        if (!string.IsNullOrEmpty(Environment.CommandLine))
        {
            Debug.Log("standalone cmd: " + Environment.CommandLine);
            var param_data = Environment.CommandLine.Split(' ');
            if (param_data != null && param_data.Length > 0)
            {
                try
                {
                    json = CJsonHelper.Load(param_data[param_data.Length - 1]) as CDict;
                }
                catch (Exception ex)
                {
                    json = null;
                }
            }
        }
#endif
        if (json != null)
        {
            SdkManager.m_pid = json.Int("fnpid");
            SdkManager.m_platform_id = json.Int("platform_id");
            SdkManager.m_fnpidraw = json.Int("fnpidraw");
            SdkManager.m_fngid = json.Str("fngid");
            SdkManager.m_uid = json.Str("username");
            SdkManager.m_userName = json.Str("name") ?? SdkManager.m_uid;
            SdkManager.time = json.Int("time");
            SdkManager.gameName = json.Str("game");
            SdkManager.m_platform_server_id = json.Str("server");
            SdkManager.cm = json.Int("cm");
            SdkManager.flag = json.Str("flag");
            SdkManager.m_fnppid = json.Str("fnppid");
            SdkManager.m_cid = json.Str("cid");
            SdkManager.m_oid = json.Str("oid");
            SdkManager.m_aid = json.Str("aid");
            SdkManager.m_ext = SdkManager.flag;
            Logger.Log("==========Platform login data: " + SdkManager.gameName + " " + SdkManager.m_platform_server_id + " " + SdkManager.m_uid + " " + SdkManager.m_userName + " " + SdkManager.time + " " + SdkManager.cm + " " + SdkManager.flag);

            GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
        }
        else
        {
            SdkManager.m_uid = null;
            SdkManager.m_userName = null;
            SdkManager.time = 0;
            SdkManager.gameName = null;
            SdkManager.m_platform_server_id = null;
            SdkManager.cm = 0;
            SdkManager.flag = null;
            SdkManager.m_ext = "";
            Logger.Error("==========error platform params format");
        }
    }
}
