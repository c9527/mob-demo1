﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;


public class PayMentInfo
{
    public int payCode;
    public int payCount;
    public uint paySessionId;
    public string productName;
    public string productDesc;
    public string price;
    public string productCount;
    public int rate;
    public string productId;
    public string coinName;
    public string serverId;
    public string roleName;
    public string roleId;
    public string level;
    public string callbackInfo;
    public TDRecharge CfgRecharge;
    public string Sku;

    public void Clear()
    {
        payCode = 0;
        payCount = 0;
        paySessionId = 0;
        productName = String.Empty;
        productDesc = String.Empty;
        price = String.Empty;
        productCount = String.Empty;
        rate = 0;
        productId = String.Empty;
        coinName = String.Empty;
        serverId = String.Empty;
        roleName = String.Empty;
        roleId = String.Empty;
        level = String.Empty;
        callbackInfo = String.Empty;
    }
}



public class SdkBase:MonoBehaviour
{
    #region 通用
    protected float _speech_run_time = 0;
    protected EnumSpeechStatus _speech_status = EnumSpeechStatus.NONE;
    private readonly Dictionary<string, int> m_gameIdMappingPlatformDic = new Dictionary<string, int>();
    #endregion


    public virtual void Init()
    {
    }

    public virtual void ValidateUploadPic(string url, Action<int> action)
    {
        
    }

    public virtual void BeforLogin()
    {
        
    }

    public virtual void Login()
    {
        
    }

    public virtual void Pay(PayMentInfo data)
    {
        
    }

    public virtual void SelectPic()
    {
        
    }

    public virtual void SelfiePic()
    {
        
    }

    public virtual void Logout()
    {
        
    }

    public virtual void WeixinShare(string type, string imagePath)
    {
        
    }

    public virtual void SaveCapture(string imagePath)
    {
        
    }

    public virtual void MultipleShare(string type, string imagePath)
    {
        
    }

    public virtual void GetShareData(string type, string info = null)
    {
       
    }

    public virtual void SaveFileToPhotoLib(string url)
    {
        
    }

    public virtual void CopyToClipBoard(string value)
    {
        
    }

    protected void sendLogToServer(int logTpye, int logFrom, int logChoose)
    {
        string platformLog = string.Empty;
        if (SdkManager.m_pid == 166)
            platformLog = "yyb";
        else if (Driver.m_platform == EnumPlatform.ios)
            platformLog = EnumPlatform.ios.ToString();
        else
            platformLog = EnumPlatform.android.ToString();
        NetLayer.Send(new tos_player_log_misc() { log_typ = logTpye, log_n1 = logFrom, log_n2 = logChoose, log_s1 = platformLog, log_s2 = "" });
    }

    public void sendWeixinShareLog(int type)
    {
        sendLogToServer((int)EnumSaveLog.WEIXIN_SHARE, SdkManager.m_logFrom, type);
    }

    protected void WakeupSpeechTimer()
    {
        if (_speech_run_time == 0 || Driver.m_platform != EnumPlatform.web)
        {
            CancelInvoke("WakeupSpeechTimer");
            return;
        }
        var run_time = Time.time - _speech_run_time;
        if (_speech_status == EnumSpeechStatus.INITIALIZING && run_time > 3)
        {
            _speech_status = EnumSpeechStatus.NONE;
            _speech_run_time = 0;
            UIManager.ShowTipPanel("语音组件初始化超时");
            return;
        }
        else if (_speech_status == EnumSpeechStatus.INITIALIZED && run_time > 20)
        {
            _speech_run_time = 0;
            UIManager.ShowTipPanel("语音组件响应超时");
            return;
        }
        if (_speech_run_time > 0 && !IsInvoking("WakeupSpeechTimer"))
        {
            InvokeRepeating("WakeupSpeechTimer", 0.25f, 0.25f);
        }
        PlatformAPI.Call("UBAdapter.getSpeechData");
    }

    #region LogInterface
    //=============================日志接口接口===============================
    /// <summary>
    /// 登录完成日志（登录完成时调用）.
    /// </summary>
    /// <param name="userId">User identifier.</param>
    public virtual void LogLoginFinish(string userId)
    {
    }

    /// <summary>
    /// 选服日志（选服完成时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="userName">User name.</param>
    /// <param name="serverId">Server identifier.</param>
    public virtual void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        
    }

    /// <summary>
    /// 创建角色日志（创建角色完成时调用）
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public virtual void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
       
    }

    /// <summary>
    /// 进入游戏日志（用户选完服后，使用角色正式进入游戏时调用）：
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public virtual void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
      
    }

   

    /// <summary>
    /// 角色等级日志（角色升级时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.角色等级</param>
    /// <param name="serverId">Server identifier.服务器id</param>
    public virtual void LogRoleLevel(string roleLevel, string serverId)
    {
        
    }
    #endregion


    /// <summary>
    /// 语音组件的识别功能的是否已经初始化
    /// </summary>
    public bool IsSpeechInitialized()
    {
        return _speech_status == EnumSpeechStatus.INITIALIZED;
    }


    public virtual void InitSpeaker(string config)
    {
    }

    public virtual void ShowSpeaker(bool builtin_ui = false, string url = null)
    {
    }



    public virtual void CloseSpeaker(bool keep_recog)
    {
    }

    public virtual void PlaySpeech(byte[] data)
    {
    }

    public virtual void StopSpeech()
    {
    }

    

    protected  void DebugLog(string s)
    {
#if DebugErrorMode
        Debug.Log(s);
#endif
    }

    protected  void DebugWarning(string s)
    {
#if DebugErrorMode
        Debug.LogWarning(s);
#endif
    }

    protected  void DebugError(string s)
    {
#if DebugErrorMode
        Debug.LogError(s);
#endif
    }

    protected void SdkLogger(string title, string content)
    {
        DebugLog("【SDK回调】" + title + "\n" + content);
    }

    public virtual void LoadFinishBeforeLoginLog()
    {

    }

    public virtual void ClickEnterGameLog()
    {

    }

    public virtual void RoleLoginLog(string name)
    {

    }

    public virtual void openCustomerServices(string serverId, string roleId)
    {

    }

    public virtual void joinVKGroup(int groupId,string groupLink)
    {

    }

    public virtual void getVKFriendsUnInstall()
    {

    }

    public virtual void inviteToVK(int friendId)
    {

    }

    public virtual void ShowAssistant(string serverId)
    {

    }

    public virtual void JoinVkOrFacebook()
    {

    }

    public virtual void ShareVkOrFacebook(string url,string title,string image,string desc)
    {

    }

    public virtual void InviteFriendVkOrFacebool()
    {

    }

    public virtual void ShareVkOrFacebookBattle(string url, string title, string image, string desc)
    {

    }


}

