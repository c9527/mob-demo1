﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text.RegularExpressions;
using DockingModule;
using UnityEngine;
using System.Text;
using System.IO;
using Debug = UnityEngine.Debug;


#if UNITY_IPHONE
using System.Runtime.InteropServices;
#endif

#if UNITY_STANDALONE
using System.Runtime.InteropServices;
#endif




public class SdkManager : MonoBehaviour
{
    #region 通用
#if ActiveSDK
    private static bool m_enable = true;   //是否启用SDK
#else
    private static bool m_enable = false;   //是否启用SDK
#endif
    private static bool m_inited = false;
    public static string SdkName = "";
    public static string Lang = "zh";
    public static bool Enable { get { return m_enable; } }
    public static bool TokeyOverdue { get; set; }
    public static string m_platform_server_id = "";//通过平台传入的服务器id，仅用于读取和校验，本地不修改

    public static string m_serverId = "";  //服务器id
    public static string m_serverName = ""; //服务器名称
    public static string m_userName = "";   //平台上的用户名
    public static string m_uid = "";        //各平台的唯一id，实际可能相同
    public static int m_platform_id;
    public static int timestamp;
    public static string m_oauthInfo = String.Empty;
    public static bool isNewTempUser = false;
    private static SdkFactory _factory;
    private static SdkBase _currentSdk;
    

    public static SdkBase CurrentSdk
    {
        get { return _currentSdk; }

    }




    private static Dictionary<string, int> m_gameIdMappingPlatformDic = new Dictionary<string, int>();

    //1:4399 166:应用宝 176:盒子(转为1) 270:9377
    public static int m_pid;                //平台id，相同平台id的账号是互通的

    public static int PlatformForLogin
    {
        get { return Driver.isMobilePlatform ? m_pid : m_platform_id; }
    }

    public static int PlatformForSeparate
    {
        get
        {
            if (m_pid == 166 && m_gameIdMappingPlatformDic.ContainsKey(m_fngid))
                return m_gameIdMappingPlatformDic[m_fngid];
            return Driver.isMobilePlatform ? m_pid : m_platform_id;
        }
    }

    public static bool IsSdkLoginSuccess()
    {
        if (!m_enable)
        {
            return true;
        }
        return !string.IsNullOrEmpty(SdkManager.m_ext);
    }
    //
    public static void ClearLoginInfo()
    {
        if (!m_enable)
        {
            return;
        }
        m_userName = "";
        m_uid = "";
        m_ext = "";
    }
    #endregion


    public static bool InterNationalVersion()
    {
        return SdkName == "RusSDK" || SdkName == "EnSDK";
    }
//    private static SdkManager _instance = null;
//    private static float _speech_run_time = 0;
//    private static EnumSpeechStatus _speech_status = EnumSpeechStatus.NONE;
//    private static List<Action<int>> _iosValidatePicCallback = new List<Action<int>>(); 
    /// <summary>
    /// 语音组件的识别功能的是否已经初始化
    /// </summary>
//    public static bool IsSpeechInitialized
//    {
//        get 
//        {
//            return _speech_status == EnumSpeechStatus.INITIALIZED;
//        }
//    }

    private static void DebugLog(string s)
    {
#if DebugErrorMode
        Debug.Log(s);
#endif
    }

    private static void DebugWarning(string s)
    {
#if DebugErrorMode
        Debug.LogWarning(s);
#endif
    }

    private static void DebugError(string s)
    {
#if DebugErrorMode
        Debug.LogError(s);
#endif
    }


    /// <summary>
    /// 是否IOS账号登陆其他设备平台
    /// </summary>
    public static bool IsIOSAccountLoginOther
    {
        get
        {
            return Driver.m_platform != EnumPlatform.ios && PlatformForLogin == 2;
        }
    }

    public static int ConvertPlatformServerID()
    {
        var match = !String.IsNullOrEmpty(m_platform_server_id) ? Regex.Match(m_platform_server_id, @"(\d+)$") : null;
        if (match != null && match.Success)
        {
            return Int32.Parse(match.Groups[1].Value);
        }
        return 0;
    }

    public static string ConvertGameServerID()
    {
        var result = m_serverId;
        var match = !String.IsNullOrEmpty(m_platform_server_id) ? Regex.Match(m_platform_server_id, @"^(\D+)\d+") : null;
        if (match != null && match.Success)
        {
            result = match.Groups[1].Value + result;
        }
        return result;
    }

    #region Android
    public static string m_ext = "";      //蜂鸟登录额外参数
    public static string m_getuiClientId = ""; //ClientId，后台消息推送用
//    private static Dictionary<string, bool> m_supportFuncDic = new Dictionary<string, bool>();
    #endregion

    #region IOS
#if UNITY_IPHONE
	private static bool m_ios = true;   //是否IOS
#else
    private static bool m_ios = false;  //是否IOS
#endif
    public static bool IsSDKIOS { get { return m_ios; } }
//    private const string appkey = "1448259251130020";
//    private const string appSecret = "16f8455ffee11d28c48dca19d52ab537";
//    private const string channelId = "10";
//
//	private const string title = "枪神小妹求好评";
//	private const string alertMsg = "感谢您对《枪战英雄》的支持，如果觉得还不错，就打赏个五星好评吧！";
//	private const string okLab = "1";
//	private const string cancelLab = "1";
//	private const string otherLab = "1";
	public const int THREEDAY = 259200;//3天秒数259200
	public static string payCallback=null;
	public static TDRecharge payInfo=null;

    public static int m_logFrom = -1;//0结算1军火库2充值3第二天签到4等级提升
	public static string m_getClientId = "";//ClientId，后台消息推送用
	public static string m_getDeviceToken = "";//DeviceToken，后台消息推送用

    #endregion

    #region WEB PC
    public static bool IsSDKWeb { get { return m_web; } }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
    private static bool m_web = true;
#else
    private static bool m_web = false;
#endif
    public static string gameName;
    public static int time;
    public static int cm = 2;
    public static string flag;
    public static string gameUrl = "http://web.4399.com/qmqs/";
    public static string gamePayUrl = "http://fnsdk.4399sy.com/pc/api/pay.php";
    public static string gameCMUrl = "http://web.4399.com/api/reg/fcm_api.php";
    public static string apiValidateUploadPicUrl = "http://api.4399sy.com/service/upload/check";
    public static int m_fnpidraw;              //原始平台id，更详细，区分设备平台
    public static string m_fngid = "";              //蜂鸟平台统一分配的游戏唯一ID 1429500985465820
    public static string m_cid = "";
    public static string m_oid = "";
    public static string m_aid = "";
    public static string m_fnppid = "";
    public static PayMentInfo _paymentInfo = new PayMentInfo();
    #endregion

    static SdkManager()
    {
        TokeyOverdue = false;
    }

    public static void Init()
    {
        DebugLog(m_enable ? "[SDK已启用]" : "[SDK已关闭]");
        if (!m_enable)
            return;
        if (m_inited)
            return;

        m_inited = true;
        if (Global.Dic.ContainsKey("GameDefineSDKName"))
            SdkName = Global.Dic.GetValue("GameDefineSDKName").ToString();

        if (Global.Dic.ContainsKey("GameDefineLang"))
            Lang = Global.Dic.GetValue("GameDefineLang").ToString().ToLower();
        var driver = new GameObject("SdkManager");
        DontDestroyOnLoad(driver);
//        if (driver.GetComponent<SdkManager>() == null)
//        {
//            _instance = driver.AddComponent<SdkManager>();
//        }
        CreateSdk(driver);
        _currentSdk.Init();

//        if (m_web)
//        {
//            PlatformAPI.ParseParams();
//        }
//        else if (m_ios)
//        {
//            m_fngid = appkey;
//            init4399SDK(appkey, appSecret, channelId, driver.name);//初始化SDK
//            appOpenLog(appkey, appSecret);//启动APP
//			initGeTuiSDK();
//        }
//        else
        {
//            if (SdkName != "RusSDK")
//            {
//                m_pid = int.Parse(Global.Dic.GetValue("fn_pid", "0").ToString());
//                m_fngid = Global.Dic.GetValue("fn_gameId", "").ToString();
//                SetCallbackUnityObjectName(driver.name);
//                SetDebug(true);
//                InitSupportFuncCallBackUnity();
//                GetGetuiClientId();
//            }
        }
    }


    private static void CreateSdk(GameObject go)
    {
        _factory = new SdkFactory();
        _currentSdk = _factory.CreateSdk(go, SdkName);
    }

    public static void InitGameIdMaping()
    {
        var arr1 = ConfigMisc.GetValue("gameIdMappingPlatform").Split(';');
        for (int i = 0; i < arr1.Length; i++)
        {
            var arr = arr1[i].Split('#');
            m_gameIdMappingPlatformDic.Add(arr[0], int.Parse(arr[1]));
        }
    }

    public static void ValidateUploadPic(string url, Action<int> action)
    {
        if (!m_enable)
        {
            return;
        }
        _currentSdk.ValidateUploadPic(url, action);
    }

//    private static IEnumerator AndroidValidateUploadPicViaWww(string url, Action<int> action)
//    {
//        var www = new WWW(apiValidateUploadPicUrl + "?url=" + url + "&client_id=" + m_fngid);
//        yield return www;
//        while (!www.isDone)
//        {
//            yield return null;
//        }
//        if (String.IsNullOrEmpty(www.error))
//        {
//            CDict json = CJsonHelper.Load(www.text) as CDict;
//            if (action != null)
//            {
//                action(json.Int("code"));
//            }
//        }
//        
//    }

    public static void InitBugly(string channel, string version, string user)
    {
        BuglyAgent.ConfigDefault(channel, version, user, 10000);
        if (Driver.m_platform == EnumPlatform.android)
            BuglyAgent.InitWithAppId("900011471");

        DebugLog(BuglyAgent.IsInitialized ? "[Bugly已启用]" : "[Bugly已关闭]");
        if (BuglyAgent.IsInitialized)
            BuglyAgent.EnableExceptionHandler();
    }

//    public void Update()
//    {
//        if(Driver.m_platform == EnumPlatform.android)
//        {
//            
//            if (Input.GetKeyDown(KeyCode.Escape))
//            {
//                if (SdkName != "RusSDK")
//                {
//                    ShowExitDialog();
//                }
//                else
//                {
//                    PlatformAPI.Call("LoginOut");
//                }
//            }
//        }
//    }

    /// <summary>
    /// 是否是测试模式，显示调试信息
    /// </summary>
    /// <param name="isDebug">Is debug.</param>
//    public static void SetDebug(bool isDebug)
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("setDebug", isDebug.ToString());
//    }

    /// <summary>
    /// 设置接收回调的gameobject的名字
    /// </summary>
    /// <param name="gameObjectName">Game object name.</param>
//    public static void SetCallbackUnityObjectName(string gameObjectName)
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("setAndroidCallbackUnityObjectName", gameObjectName);
//    }

    /// <summary>
    /// 是否支持某方法
    /// </summary>
    /// <param name="functionName"></param>
    /// <returns></returns>
    public static bool IsSupportFunction(string functionName)
    {
        if (functionName == SdkFuncConst.GET_SHARE_DATA)
        {
            if (Global.appVersion == "2.7.7" || Global.appVersion == "2.7.8")//关闭2.7.7与2.7.8分享功能
            {
                return false;
            }
            if (!Driver.isMobilePlatform)
                return true;
        }
        else if (functionName == SdkFuncConst.SHOW_SPEAKER && Driver.m_platform == EnumPlatform.web || Driver.m_platform == EnumPlatform.ios)
        {
            return true;
        }
        if (_currentSdk is AndroidSdkBase)
        {
            return (_currentSdk as AndroidSdkBase).IsSupportFunction(functionName);
        }
        return false;
    }

//    public static string GetIconPath(string fileName = "icon.png")
//    {
//        if (!m_enable)
//        {
//            return string.Empty;
//        }
//        if (_currentSdk is AndroidSdkBase)
//        {
//            return (_currentSdk as AndroidSdkBase).GetIconPath(fileName);
//        }
//        return String.Empty
//    }

    /// <summary>
    /// 登陆前日志
    /// </summary>
    public static void BeforLogin()
    {
        if (!m_enable)
            return;
        _currentSdk.BeforLogin();
    }

    /// <summary>
    /// 显示登陆界面
    /// </summary>
    public static void Login()
    {
        if (!m_enable)
            return;
        UIManager.ShowMask("登录中...");
       _currentSdk.Login();
    }

    /// <summary>
    /// 测试用的验证函数
    /// </summary>
//    public static void TestVerifyLogin(string json)
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("testVerifyLogin", json);
//    }

    /// <summary>
    /// 兑换礼包统计
    /// </summary>
    public static void ExchangeStatistic(string exchangeCode)
    {
        if (!m_enable)
            return;
        if (_currentSdk is IosSdkBase)
        {
            (_currentSdk as IosSdkBase).ExchangeStatistic(exchangeCode);
        }
    }

    /// <summary>
    /// 充值接口
    /// </summary>
    /// <param name="productName">Product name.商品名称。前面不要带数量，数量放在productCount里</param>
    /// <param name="productDesc">Product desc.商品描述</param>
    /// <param name="price">Price.实际充值金额，单位：元。不管productCount和rate是多少，这个值填多少就充多少元</param>
    /// <param name="productCount">Product count.商品数量。会和productName一起显示给用户看，不参与price计算。如显示：100元宝</param>
    /// <param name="rate">Rate.人民币和游戏币兑换比例（如 1元 = 10元宝）。仅显示给用户看，不参与price计算</param>
    /// <param name="productId">Product identifier.商品id。不同的商品要有不同id</param>
    /// <param name="coinName">Coin name.游戏币名称</param>
    /// <param name="serverId">Server identifier.服务器ID</param>
    /// <param name="roleName">Role name.角色名</param>
    /// <param name="roleId">Role identifier.角色id</param>
    /// <param name="level">Level.角色等级</param>
//    private static void Pay(string productName, string productDesc, string price,
//                    string productCount, int rate, string productId, string coinName,
//                    string serverId, string roleName, string roleId, string level, string callbackInfo)
//    {
//        if (!m_enable)
//            return;
//        if (m_ios)
//        {
//            appPay(m_uid, serverId, roleId, productId, productCount, callbackInfo);
//            UIManager.ShowMask("正在充值，请稍后...", 100);
//        }
//        else
//        {
//            //兼容旧包sdk的写法，两个方法都掉一下
//            PlatformAPI.Call("pay", productName, productDesc, price,
//                                 productCount, rate, productId, coinName,
//                                 serverId, roleName, roleId, level, callbackInfo);
//            PlatformAPI.Call("pay", m_uid, productName, productDesc, price,
//                                 productCount, rate, productId, coinName,
//                                 serverId, roleName, roleId, level, callbackInfo);
//        }
//    }

    public static void Pay(int code, int count = 1)
    {
        Logger.Log(String.Format("准备请求充值：{0}，{1}", code, count));
        _paymentInfo.Clear();

        _paymentInfo.payCode = code;
        _paymentInfo.payCount = count;
        NetLayer.Send(new tos_player_recharge_switch { item_id = code });
        _paymentInfo.paySessionId = NetLayer.SessionId;
    }

    private static void Toc_player_recharge_switch(toc_player_recharge_switch proto)
    {
        if (proto.sid != _paymentInfo.paySessionId)
            return;

        if (proto.is_open)
        {
            var info = ConfigManager.GetConfig<ConfigRecharge>().GetLine(_paymentInfo.payCode);
            if (info != null)
            {
                var payId = _paymentInfo.payCode.ToString();
                if (!m_enable)
                {
                    return;
                }
                _paymentInfo.productName = info.Name;
                _paymentInfo.productDesc = info.Name;
                _paymentInfo.price = info.PlatformCost.ToString();
                _paymentInfo.productCount = _paymentInfo.payCount.ToString();
                _paymentInfo.rate = 10;
                _paymentInfo.productId = payId;
                _paymentInfo.coinName = "钻石";
                _paymentInfo.serverId = m_serverId;
                _paymentInfo.roleName = PlayerSystem.roleData.name;
                _paymentInfo.roleId = PlayerSystem.roleId.ToString();
                _paymentInfo.level = PlayerSystem.roleData.level.ToString();
                _paymentInfo.callbackInfo = proto.callback_info;
                _paymentInfo.CfgRecharge = info;
                _paymentInfo.Sku = info.ProductID;
                DebugLog(String.Format("调用SDK充值接口：{0}，{1}", payId, _paymentInfo.payCount));
                _currentSdk.Pay(_paymentInfo);
                _paymentInfo.Clear();
            }
            else
                UIManager.ShowTipPanel("无效的充值");
        }
        else
            UIManager.ShowTipPanel("充值未开放");
    }

//    /// <summary>
//    /// 显示用户中心
//    /// </summary>
//    public static void ShowUserCenter()
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("showUserCenter");
//    }
//
//    /// <summary>
//    /// 显示游戏中心
//    /// </summary>
//    public static void ShowGameCenter()
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("showGameCenter");
//    }

    public static void SelectPic()
    {
        if (!m_enable)
            return ;
        _currentSdk.SelectPic();
    }
public static void UploadPic(string path)
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).UploadPic(path);
        }
    }

    public static void CropPic(string inputPath, string outputPath, int aspectW = 1, int aspectH = 1, int width = 0, int height = 0)
    {
        if (!m_enable)
            return;
         if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).CropPic(inputPath, outputPath, aspectW, aspectH, width, height);
        } 
    }

    public static void SelfiePic()
    {
        if (!m_enable)
            return;
        _currentSdk.SelfiePic();
    }

//    /// <summary>
//    /// 显示BBS
//    /// </summary>
//    public static void ShowBBS()
//    {
//        if (!m_enable)
//            return;
//        PlatformAPI.Call("showBBS");
//    }

    /// <summary>
    /// 显示退出对话框
    /// </summary>
    public static void ShowExitDialog()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).ShowExitDialog();
        }
    }

    /// <summary>
    /// 显示悬浮窗
    /// </summary>
    public static void ShowFloatBar()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).ShowFloatBar();
        } 
    }

    /// <summary>
    /// 隐藏悬浮窗
    /// </summary>
    public static void HideFloatBar()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).HideFloatBar();
        } 
    }

    /// <summary>
    /// 登出
    /// </summary>
    public static void Logout()
    {
        if (!m_enable)
            return;
        _currentSdk.Logout();
//        if (m_ios)
//        {
//            appLogOut();
//        }
//        else
//        {
//            if (SdkName != "RusSDK")
//            {
//                PlatformAPI.Call("logout");
//            }
//            else
//            {
//                PlatformAPI.Call("LoginOut");
//            }
//        }
    }

    /// <summary>
    /// 切换用户
    /// </summary>
    public static void SwitchUser()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).SwitchUser();
        }
    }

    public static void SwitchUserAccount()
    {
        if (!m_enable)
            return;
        if(_currentSdk is IosSdkBase)
        {
            (_currentSdk as IosSdkBase).switchSdkAccount();
        }         
    }

    /// <summary>
    /// 释放SDK资源并退出游戏
    /// Releases the SDK and exit app.退出游戏前必须调用该接口
    /// </summary>
    public static void ReleaseSDKAndExitApp()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).ReleaseSDKAndExitApp();
        } 
//        PlatformAPI.Call("releaseSDKAndExitApp");
    }

//    /// <summary>
//    /// 检查更新
//    /// </summary>
    public static void CheckUpdate()
    {
        if (!m_enable)
            return;
        UIManager.ShowMask("检查更新中...");
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).CheckUpdate();
        }
    }
//
    public static void InitSupportFuncCallBackUnity()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).InitSupportFuncCallBackUnity();
        }
    }

    /// <summary>
    /// 初始化语音组件
    /// </summary>
    /// <param name="config"></param>
    public static void InitSpeaker(string config)
    {
        if (!m_enable)
        {
//            _speech_status = EnumSpeechStatus.NONE;
            return;
        }
        else
        {
            _currentSdk.InitSpeaker(config);
        }

}

//    private void WakeupSpeechTimer()
//    {
//        if (_speech_run_time == 0 || Driver.m_platform != EnumPlatform.web)
//        {
//            CancelInvoke("WakeupSpeechTimer");
//            return;
//        }
//        var run_time = Time.time - _speech_run_time;
//        if (_speech_status == EnumSpeechStatus.INITIALIZING && run_time>3)
//        {
//            _speech_status = EnumSpeechStatus.NONE;
//            _speech_run_time = 0;
//            UIManager.ShowTipPanel("语音组件初始化超时");
//            return;
//        }
//        else if(_speech_status == EnumSpeechStatus.INITIALIZED && run_time > 20)
//        {
//            _speech_run_time = 0;
//            UIManager.ShowTipPanel("语音组件响应超时");
//            return;
//        }
//        if (_speech_run_time > 0 && !IsInvoking("WakeupSpeechTimer"))
//        {
//            InvokeRepeating("WakeupSpeechTimer", 0.25f, 0.25f);
//        }
//        PlatformAPI.Call("UBAdapter.getSpeechData");
//    }

    /// <summary>
    /// 打开语音
    /// </summary>
    public static void ShowSpeaker(bool builtin_ui = false, string url = null)
    {
        if (!m_enable)
        {
            return;
        }
        _currentSdk.ShowSpeaker(builtin_ui, url);
    }

    /// <summary>
    /// 关闭语音
    /// keep_recog：是否关闭录音而继续识别（web版适用）
    /// </summary>
    public static void CloseSpeaker(bool keep_recog=false)
    {
        if (!m_enable)
        {
            return;
        }
        _currentSdk.CloseSpeaker(keep_recog);
    }

    /// <summary>
    /// 播放语音
    /// </summary>
    public static void PlaySpeech(byte[] data)
    {
        if (!m_enable)
        {
            return;
        }
        _currentSdk.PlaySpeech(data);
    }

    /// <summary>
    /// 停止播放语音
    /// </summary>
    public static void StopSpeech()
    {
         if (!m_enable)
        {
            return;
        }
        _currentSdk.StopSpeech();
    }

    public static void GetShareData(string type, string info = null)
    {
        if (!m_enable)
            return;
        _currentSdk.GetShareData(type, info);
    }

    public static void ShowChuShouTV()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).ShowChuShouTV();
        }
    }

    public static void IsSupportChuShouTV()
    {
        if (!m_enable)
            return;
         if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).IsSupportChuShouTV();
        }
    }

	public static void IOSShareLogin()
	{
		if (!m_enable)
			return;
        if (_currentSdk is IosSdkBase)
        {
            (_currentSdk as IosSdkBase).IOSShareLogin();
        }
	}

    public static void SaveFileToPhotoLib(string url)
    {
        if (!m_enable)
        {
            return;
        }
        _currentSdk.SaveFileToPhotoLib(url);
    }

    public static void CopyToClipBoard(string value)
    {
         if (!m_enable)
                return;
        _currentSdk.CopyToClipBoard(value);
    }

    public static void SetOauthInfo(string oauthInfo)
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).SetOauthInfo(oauthInfo);
        }
    }

    public static void LocalPush()
    {
        if (!m_enable)
            return;
        if (_currentSdk is AndroidSdkBase)
        {
            (_currentSdk as AndroidSdkBase).LocalPush();
        }
    }

    public static void openCustomerServices()
    {
        if (m_enable)
        {
            _currentSdk.openCustomerServices(SdkManager.m_serverId,PlayerSystem.roleId.ToString());
        }
    }

    //=============================日志接口接口===============================
    /// <summary>
    /// 登录完成日志（登录完成时调用）.
    /// </summary>
    /// <param name="userId">User identifier.</param>
    public static void LogLoginFinish(string userId)
    {
        if (!m_enable)
            return;
        _currentSdk.LogLoginFinish(userId);
    }

   

    /// <summary>
    /// 选服日志（选服完成时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="userName">User name.</param>
    /// <param name="serverId">Server identifier.</param>
    public static void LogSelectServer(string roleLevel, string userName, string serverId)
    {
        if (m_pid == 166)
        {
            GameObject go = Driver.singleton.gameObject;
            QQFakeServer svr = go.GetComponent<QQFakeServer>();
            if (svr == null)
                svr = go.AddComponent<QQFakeServer>();
            svr.Begin(userName);
        }
        if (!m_enable)
            return;
        _currentSdk.LogSelectServer(roleLevel, userName, serverId);
    }

    /// <summary>
    /// 创建角色日志（创建角色完成时调用）
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public static void LogCreateRole(string roleId, string roleName, string serverId, string serverName)
    {
        if (!m_enable)
            return;
        _currentSdk.LogCreateRole(roleId, roleName, serverId, serverName);
    }

    /// <summary>
    /// 进入游戏日志（用户选完服后，使用角色正式进入游戏时调用）：
    /// </summary>
    /// <param name="roleId">Role identifier.</param>
    /// <param name="roleName">Role name.</param>
    /// <param name="roleLevel">Role level.</param>
    /// <param name="serverId">Server identifier.</param>
    /// <param name="serverName">Server name.</param>
    public static void LogEnterGame(string roleId, string roleName, string roleLevel, string serverId, string serverName)
    {
        if (!m_enable)
            return;
        _currentSdk.LogEnterGame(roleId, roleName, roleLevel, serverId, serverName);
    }

    public static void TP2OnUserLogin(int accountType, int worldId, string openId, string roleId)
    {
        if (_currentSdk is AndroidSdkBase)
        {
            var sdk = _currentSdk as AndroidSdkBase;

            if (sdk.IsSupportFunction("TP2OnUserLogin") == false)
            return;
            sdk.TP2OnUserLogin(accountType, worldId, openId, roleId);
        }
        
    }

    public static void TP2SDKSetPopupOptions(int options)
    {
        if (_currentSdk is AndroidSdkBase)
        {
            var sdk = _currentSdk as AndroidSdkBase;

            if (sdk.IsSupportFunction("TP2OnUserLogin") == false)
            return;
            sdk.TP2SDKSetPopupOptions(options);
        }
    }

    /// <summary>
    /// 角色等级日志（角色升级时调用）
    /// </summary>
    /// <param name="roleLevel">Role level.角色等级</param>
    /// <param name="serverId">Server identifier.服务器id</param>
    public static void LogRoleLevel(string roleLevel, string serverId)
    {
        if (!m_enable)
            return;
        _currentSdk.LogRoleLevel(roleLevel, serverId);
    }

//    public static void GetGetuiClientId()
//    {
//        if (!m_enable)
//            return;
//
//        PlatformAPI.Call("getuiClientId");
//    }

    public static void WeixinShare(string type, string imagePath)
    {
        if (!m_enable)
            return;
        DebugLog("WeixinShare--");
        if (type != null)
        {
            _currentSdk.WeixinShare(type, imagePath);
        }       
    }

    public static void SaveCapture(string imagePath)
    {
        if(!m_enable)
            return;
        DebugLog("SaveCapture--");
        TipsManager.Instance.showTips("已保存到相册");
        _currentSdk.SaveCapture(imagePath);
    }
     
    public static void MultipleShare(string type, string imagePath)
    {
        if (!m_enable)
            return;
        _currentSdk.MultipleShare(type, imagePath);
    }

    // ios
	public static void getStarComment ()
	{
		if (_currentSdk is IosSdkBase)
		{
		    (_currentSdk as IosSdkBase).getStarComment();
		}
	}

    public static void LoadEndBeforeLoginLog()
    {
        if (m_enable)
        {
            _currentSdk.LoadFinishBeforeLoginLog();
        }
    }

    public static void ClickEnterGameLog()
    {
        if (m_enable)
        {
            _currentSdk.ClickEnterGameLog();
        }
    }

    public static void RoleLoginlog(string name)
    {
        if (m_enable)
        {
            _currentSdk.RoleLoginLog(name);
        }
    }


    public static void ShowAssistant()
    {
        if (m_enable)
        {
            _currentSdk.ShowAssistant(SdkManager.m_serverId);
        }
    }

    public static void JoinVkOrFaceBook()
    {
        if(m_enable){
            _currentSdk.JoinVkOrFacebook();
        }
    }

    public static void ShareToVkOrFB(string url, string title, string image, string desc)
    {
        if(m_enable){
            _currentSdk.ShareVkOrFacebook(url, title, image, desc);
        }
    }

    public static void ShareToVkOrFBbattle(string url, string title, string image, string desc)
    {
        if (m_enable)
        {
            _currentSdk.ShareVkOrFacebookBattle(url, title, image, desc);
        }
    }

    public static void InviteFriend()
    {
        if (m_enable)
        {
            _currentSdk.InviteFriendVkOrFacebool();
        }
    }


//
//    //ios
//    public static string getAddress(string type)
//    {        
//        ConfigXcodeParameterLine xpCfg = ConfigManager.GetConfig<ConfigXcodeParameter>().GetLine(type);
//        if (xpCfg == null)
//            return null;
//        var address = xpCfg.Content;
//        return address;
//    }
//
//    //ios
//	public static void getIOSShareData(string roleId, string roleName, string serverName, string roleLevel)
//	{
//		if(!m_enable)
//			return;
//		if (m_ios) 
//		{
//			appGetIOSShareData(roleId, roleName, serverName, roleLevel);
//		}
//	}

    //===============SDK的回调函数======================================

//    #region Android-SDK回调
//    public void AndroidSdkInitSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        SdkLogger("AndroidSdkInitSuccess", msg);
//    }
//
//    public void AndroidSdkInitFailed(string msg)
//    {
//        if (!m_enable)
//            return;
//        SdkLogger("AndroidSdkInitFailed", msg);
//        Driver.SendLogServer(1101, msg);
//    }
//
//    public void AndroidLoginComplete(string msg)
//    {
//        if (!m_enable)
//            return;
//
//        var json = CJsonHelper.Load(msg) as CDict;
//        m_userName = json.Str("name");
//        m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
//        m_ext = json.Str("ext");
//
//        var subJson = CJsonHelper.Load(m_ext) as CDict;
//        m_pid = subJson.Int("fnpid");
//        m_fnpidraw = subJson.Int("fnpidraw");
//        m_fngid = subJson.Str("fngid");
//
//        SdkLogger("AndroidLoginComplete", msg);
//        ShowFloatBar();
//        UIManager.HideMask();
//
//        if (TokeyOverdue)
//        {
//            TokeyOverdue = false;
//            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
//        }
//
//        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
//        //        TestVerifyLogin(CJsonHelper.Save(json));
//    }
//
//    public void AndroidLoginError(string msg)
//    {
//        if (!m_enable)
//            return;
//
//        ClearLoginInfo();
//        TipsManager.Instance.showTips(msg);
//        SdkLogger("AndroidLoginError", msg);
//        Driver.SendLogServer(1102, msg);
//        UIManager.HideMask();
//    }
//
//
//    public void AndroidSdkSelectPicSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        GameDispatcher.Dispatch(GameEvent.UI_SELECT_EXTERN_PIC, msg);
//        SdkLogger("选择图片：", msg);
//    }
//
//
//    public void AndroidSdkSelectPicFail(string msg)
//    {
//        if (!m_enable)
//            return;
//        TipsManager.Instance.showTips("图片选择失败" + msg);
//    }
//
//
//    public void AndroidSdkUploadPicSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        var json = CJsonHelper.Load(msg) as CDict;
//        GameDispatcher.Dispatch(GameEvent.UI_UPLOAD_PIC, json.Str("inputPath"), json.Str("url"));
//        SdkLogger("上传图片成功,返回url：", json.Str("url"));
//    }
//
//    public void AndroidSdkUploadPicFail(string msg)
//    {
//        if (!m_enable)
//            return;
//        TipsManager.Instance.showTips("上传图片失败:"+ msg);
//    }
//
//
//    public void AndroidSdkSelfiePicSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        GameDispatcher.Dispatch(GameEvent.UI_SELECT_EXTERN_PIC, msg);
//        SdkLogger("自拍选择图片路径：", msg);
//    }
//
//    public void AndroidSdkCropPicSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        var json = CJsonHelper.Load(msg) as CDict;
//
//        GameDispatcher.Dispatch(GameEvent.UI_CROP_PIC, json.Str("inputPath"), json.Str("outputPath"));
//        SdkLogger("裁剪图片返回路径：", json.Str("outputPath"));
//    }
//
//    //    public void AndroidVerifySuccess(string msg)
//    //    {
//    //        SdkLogger("AndroidVerifySuccess", msg);
//    //
//    //        var json = CJsonHelper.Load(msg) as CDict;
//    //        Global.userName = json.Str("name");
//    //        Global.uid = json.Str("uid");
//    //        LogLoginFinish(Global.uid);
//    //
//    //        UIManager.HideMask();
//    //    }
//
//    public void AndroidSwitchUserSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//
//        var json = CJsonHelper.Load(msg) as CDict;
//        m_userName = json.Str("name");
//        m_uid = json.Str("uid");  //平台返回的uid可能为空，要使用验证后的uid
//        m_ext = json.Str("ext");
//        m_pid = (CJsonHelper.Load(m_ext) as CDict).Int("fnpid");
//
//        SceneManager.ReturnToLogin();
//
//        ShowFloatBar();
//        SdkLogger("AndroidSwitchUserSuccess", msg);
//
//        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
//    }
//
//    public void AndroidLoginOutSuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//
//        ClearLoginInfo();
//        UIManager.HideMask();
//
//        SceneManager.ReturnToLogin();
//
//        ShowFloatBar();
//        SdkLogger("AndroidLoginOutSuccess", msg);
//    }
//
//    public void AndroidCheckUpdate(string msg)
//    {
//        if (!m_enable)
//            return;
//        SdkLogger("AndroidCheckUpdate", msg);
//
//        switch (msg)
//        {
//            case "onNotSDCard": break;
//            case "onNotNewVersion": break;
//            case "onNormalUpdateLoading": break;
//            case "onNetWorkError": break;
//            case "onForceUpdateLoading": break;
//            case "onException": break;
//            case "onCheckVersionFailure": break;
//            case "onCancelNormalUpdate": break;
//        }
//
//        UIManager.HideMask();
//        GameDispatcher.Dispatch(GameEvent.SDK_CHECK_UPDATE_COMPLETE);
//    }
//
//    public void AndroidSdkPaySuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        SdkLogger("AndroidSdkPaySuccess", msg);
//        PlayerSystem.TosGetRechargeData();
//        UIManager.HideMask();
//
//        NetLayer.Send(new tos_player_first_recharge_data());
//        NetLayer.Send(new tos_player_recharge_welfare_list());
//
//        var product_code = 0;
//        var arr = msg.Split('|');
//        if (arr.Length > 0 && Int32.TryParse(arr[0], out product_code))
//        {
//            var info = product_code > 0 ? ConfigManager.GetConfig<ConfigRecharge>().GetLine(product_code) : null;
//            if (info != null)
//            {
//                if (info.Associator > 0)
//                {
//                    UIManager.PopPanel<PanelMemberEffect>(new object[] { info.Associator }, true, null);
//                }
//                TipsManager.Instance.showTips("充值获得" + info.Name);
//            }
//        }
//    }
//
//    public void AndroidSdkInitSupprotFunction(string msg)
//    {
//        if (!m_enable)
//            return;
//        SdkLogger("AndroidSdkInitSupprotFunction", msg);
//        string[] msgs = msg.Split(Convert.ToChar(1));
//        for (int i = 0; i < msgs.Length; i += 2)
//        {
//            string key = msgs[i];
//            if (i + 1 >= msgs.Length)
//            {
//                DebugError("AndroidSdkInitSupprotFunction 数组越界");
//                return;
//            }
//            string valueStr = msgs[i + 1];
//            bool value = valueStr == "true";
//            if (!m_supportFuncDic.ContainsKey(key))
//            {
//                m_supportFuncDic[key] = value;
//            }
//            else
//            {
//                m_supportFuncDic.Add(key, value);
//            }
//        }
//    }
//

//
//    private void SdkLogger(string title, string content)
//    {
//        DebugLog("【SDK回调】" + title + "\n" + content);
//    }
//
//    private void AndroidSdkOnSpeechSuccess(string msg)
//    {
//        byte[] audio_data = null;
//#if !UNITY_WEBPLAYER
//        if (File.Exists(NetChatData.SPEECH_DATA_URL))
//        {
//            audio_data = File.ReadAllBytes(NetChatData.SPEECH_DATA_URL);
//        }
//#endif
//        NetChatData.CacheSpeechData(audio_data);
//        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, msg);
//    }
//
//    private void AndroidSdkOnSpeechError(string msg)
//    {
//        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, msg);
//    }
//
//    private void AndroidSdkOnSpeechBegin(string msg)
//    {
//        //开始说话
//    }
//
//    private void AndroidSdkOnSpeechVolumeChanged(string msg)
//    {
//        int volume = Int32.Parse(msg);
//        PanelSpeechSpeaker.UpdateSpeaker(volume);
//    }
//
//    private void AndroidSdkOnSpeechEnd(string msg)
//    {
//        //说话结束
//        PanelSpeechSpeaker.showSpeaker(1);
//    }
//
//    private void AndroidSdkOnSpeechPlayComplete(string msg)
//    {
//        //语音播放结束
//    }
//
//    private void AndroidSdkOnGetGetuiClientId(string msg)
//    {
//        //获取GetuiClientId
//        m_getuiClientId = msg;
//
//        SdkLogger("AndroidSdkOnGetGetuiClientId", msg);
//    }
//
//    private void AndroidSdkGetShareDataResult(string msg)
//    {
//        var data = msg.Split('|');
//        if (data[0] == "1")
//        {
//            Singleton<EventModel>.Instance.share_url = data[1];
//            Singleton<EventModel>.Instance.share_qcode_url = data[2];
//            GameDispatcher.Dispatch(GameEvent.PROXY_EVENT_SHARE_DATA_UPDATED);
//        }
//        else
//        {
//            TipsManager.Instance.showTips(data.Length > 1 ? data[1] : "获取游戏分享地址失败");
//        }
//    }
//
//    private void AndroidSdkSaveFileToPhotoLibResult(string msg)
//    {
//        var code = Int32.Parse(msg);
//        if (code == 1)
//        {
//            UIManager.ShowTipPanel("保存失败，需要相册权限");
//        }
//        else if (code == 2)
//        {
//            UIManager.ShowTipPanel("文件数据错误");
//        }
//        else
//        {
//            TipsManager.Instance.showTips("已成功保存至相册");
//        }
//    }
//
//    private void AndroidSdkIsSupportChuShouTV(string msg)
//    {
//        if(msg != null)
//            PanelHallBattle.m_liveVideoGo.TrySetActive(Convert.ToBoolean(msg));
//    }
//
//    private void AndroidSdkShareLog(string msg)
//    {
//        if(msg != null)
//        {
//            int type = 0;
//            int choose = 0;
//            switch (msg)
//            {
//                case "wechat_friends":
//                    type = (int)EnumSaveLog.WEIXIN_SHARE;
//                    choose = 1;
//                    break;
//                case "wechat_moments":
//                    type = (int)EnumSaveLog.WEIXIN_SHARE;
//                    choose = 2;
//                    break;
//                case "qq_friends":
//                    type = (int)EnumSaveLog.QQ_SHARE;
//                    choose = 1;
//                    break;
//                case "qq_zone":
//                    type = (int)EnumSaveLog.QQ_SHARE;
//                    choose = 2;
//                    break;
//                case "sina_weibo":
//                    type = (int)EnumSaveLog.WEIBO_SHARE;
//                    choose = 1;
//                    break;
//            }
//            //int fromType, int shareBtn, int shareType, string chooseType
//            Debug.Log(String.Format("AndroidSdkShareLog:type{0},choose{1}",type, choose.ToString()));
//            sendAndroidLogToServer(PanelWeixinShare.fromType, 2, type, choose.ToString());            
//        }
//    }
//
//    #endregion

//    #region IOS-SDK回调
//    //登陆回调
//
//    public void IOS_4399_Login(string param)
//    {
//        //username,suid,timestamp,signStr,targetServerId,verifyToken,comeFrom
//        DebugLog("IOS_4399_Login=" + param);
//        if (!m_enable)
//            return;
//        var json = CJsonHelper.Load(param) as CDict;
////        var tempPid = json.Str("comeFrom");
//        m_userName = json.Str("username");
//        m_uid = json.Str("suid");  //平台返回的uid可能为空，要使用验证后的uid
//        m_ext = json.Str("verifyToken");
////        m_pid = Int32.Parse(tempPid);
//        m_pid = 2;
//        m_platform_id = 2;
//        appNSLog("IOSengineCallBack" +param);
//        //ShowFloatBar ();
//        UIManager.HideMask();
//
//        if (TokeyOverdue)
//        {
//            TokeyOverdue = false;
//            GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY);
//        }
//
//        GameDispatcher.Dispatch(GameEvent.LOGIN_PLATFORM_COMPLETE);
//    }
//
//    public void IOS_4399_PurchaseCompelted(string param)
//    {
//        UIManager.HideMask();
//    }
//
//    public void IOS_4399_ValidatePicResult(string result)
//    {
//		appNSLog("校验图片成功,返回url："+result);
//        if (_iosValidatePicCallback.Count > 0)
//        {
//            if (result.Contains("ok"))
//            {
//                _iosValidatePicCallback[0](1);
//            }
//            else if (result.Contains("delete"))
//            {
//                _iosValidatePicCallback[0](-1);
//            }
//            else
//            {
//                _iosValidatePicCallback[0](0);
//            }
//            _iosValidatePicCallback.RemoveAt(0);
//        }
//        
////        Logger.Error(result);
//    }
//
//    public void IOS_4399_UploadPic(string msg)
//    {
//		appNSLog("上传图片成功,返回url："+ msg);
//        GameDispatcher.Dispatch(GameEvent.UI_UPLOAD_PIC, "name", msg);
//    }
//
//
//    public void IOS_4399_PurchaseFailed(string param)
//    {
//        UIManager.HideMask();
//    }
//
//    //取消登陆
//    public void IOS_4399_CancelLogin(string param)
//    {
//        DebugLog("#IOS_4399_CancelLogin");
//    }
//
//    public void IOS_4399_SdkPaySuccess(string msg)
//    {
//        if (!m_enable)
//            return;
//        PlayerSystem.TosGetRechargeData();
//		payCallback = msg;
//    }
//
//	public void IOS_4399_SpeechSuccess(string msg)
//	{
//		appNSLog ("IOS_4399_SpeechSuccess");
//		byte[] audio_data = null;
//#if !UNITY_WEBPLAYER
//		if (File.Exists(NetChatData.SPEECH_DATA_URL))
//		{
//			appNSLog ("IOS_4399_SpeechSuccess11");
//			audio_data = File.ReadAllBytes(NetChatData.SPEECH_DATA_URL);
//			string dataString = string.Empty;
//			foreach(byte b in audio_data)
//			{
//				dataString += b.ToString();
//			}
//			appNSLog("dataString:"+dataString);
//		}
//#endif
//		NetChatData.CacheSpeechData(audio_data);
//		GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, msg);
//	}
//
//	public void IOS_4399_SpeechError(string msg)
//	{
//		appNSLog ("IOS_4399_SpeechError:"+msg);
//		PanelSpeechSpeaker.closeSpeaker();
//		TipsManager.Instance.showTips ("您好像没有说话哦");
//	}
//
//	public void IOS_4399_SpeechVolumeChanged(string msg)
//	{
//		int volume = Int32.Parse(msg);
//		PanelSpeechSpeaker.UpdateSpeaker(volume);
//	}
//
//	public void IOS_4399_SpeechEnd(string msg)
//	{
//		//说话结束
//		PanelSpeechSpeaker.showSpeaker(1);
//	}
//
//	public void IOS_4399_SdkComment(string param)
//	{
//		if (!m_enable)
//			return;
//		appNSLog ("IOS_4399_SdkComment");
//		switch (param) 
//		{
//			case "Comment":
//				GlobalConfig.clientValue.isComment_thisVersion = true;
//				GlobalConfig.SaveClientValue("isComment_thisVersion");
//                sendLogToServer((int)EnumSaveLog.STAR_COMMENT,m_logFrom,(int)EnumStarComment.COMMENT);
//                m_logFrom = -1;
//				appNSLog("Comment");
//				break;
//			case "Complaint":				
//				UIManager.PopPanel<PanelAdvice>(null, true);
//				GlobalConfig.clientValue.last_comment_stamp = TimeUtil.GetNowTimeStamp();
//				GlobalConfig.SaveClientValue("last_comment_stamp");
//                sendLogToServer((int)EnumSaveLog.STAR_COMMENT, m_logFrom, (int)EnumStarComment.COMPLAINT);
//                m_logFrom = -1;
//				break;
//			case "NextTime":
//				GlobalConfig.clientValue.last_comment_stamp = TimeUtil.GetNowTimeStamp();
//				GlobalConfig.SaveClientValue("last_comment_stamp");
//                sendLogToServer((int)EnumSaveLog.STAR_COMMENT, m_logFrom, (int)EnumStarComment.NEXTTIME);
//                m_logFrom = -1;
//                appNSLog("NextTime");
//				break;
//			default:
//				break;
//		}
//	}
//
//    private static void sendLogToServer(int logTpye, int logFrom, int logChoose)
//    {
//        string platformLog = string.Empty;
//        if (m_pid == 166)
//            platformLog = "yyb";
//        else if(Driver.m_platform == EnumPlatform.ios)
//            platformLog = EnumPlatform.ios.ToString();
//        else
//            platformLog = EnumPlatform.android.ToString();
//		NetLayer.Send(new tos_player_log_misc() { log_typ = logTpye, log_n1 = logFrom, log_n2 = logChoose, log_s1 = platformLog, log_s2 = "" });
//    }
//
//    public static void sendAndroidLogToServer(int fromType, int shareBtn, int shareType, string chooseType)
//    {
//        string platformLog = string.Empty;
//        if (m_pid == 166)
//            platformLog = "yyb";
//        else
//            platformLog = EnumPlatform.android.ToString();
//        Debug.Log(String.Format("sendAndroidLogToServer:fromType{0},shareBtn{1},shareType{2},chooseType{3}", fromType.ToString(),shareBtn,shareType,chooseType));
//        NetLayer.Send(new tos_player_log_misc() { log_typ = fromType, log_n1 = shareBtn, log_n2 = shareType, log_s1 = platformLog, log_s2 = chooseType });
//    }
//
//    public static void sendWeixinShareLog(int type)
//    {
//        sendLogToServer((int)EnumSaveLog.WEIXIN_SHARE, m_logFrom, type);
//    }

	public static bool isGoComment()
	{   
        var isCommentOpen = GlobalConfig.serverValue.fiveStarComment_open;
		var isThisVersion = GlobalConfig.clientValue.last_comment_stamp != 1;
		var isComment = GlobalConfig.clientValue.isComment_thisVersion;
		var isAfterFiveDay = TimeUtil.GetNowTimeStamp()-GlobalConfig.clientValue.last_comment_stamp>THREEDAY;
		return isCommentOpen && isThisVersion && isAfterFiveDay && !isComment;
	}
//
    //ios
	public static void SdkPaySuccess(string msg)
	{
		if (!m_enable)
			return;
	    if (_currentSdk is IosSdkBase)
	    {
	        (_currentSdk as IosSdkBase).SdkPaySuccess(msg);
	    }
	}

    
//
//	public void IOS_4399_SdkShareDataResult(string param)
//	{
//		if (!m_enable)
//			return;
//		var json = CJsonHelper.Load(param) as CDict;
//		var come = json.Str ("come");
//		var qrcode = json.Str("qrcode");
//		//var qrdata = json.Str("qrdata");
//		var url = json.Str("url");
//		if (come.Equals("come"))
//		{
//			Singleton<EventModel>.Instance.share_qcode_url = qrcode;
//			Singleton<EventModel>.Instance.share_url = url;
//			GameDispatcher.Dispatch(GameEvent.PROXY_EVENT_SHARE_DATA_UPDATED);
//		}
//		else
//		{
//			TipsManager.Instance.showTips("获取游戏分享地址失败");
//		}
//	}
//		
//	public void IOS_4399_NotWeixinInstall()
//	{
//		if (!m_enable)
//		{
//			return;
//		}
//		TipsManager.Instance.showTips ("您尚未安装微信");
//	}
//
//	public void IOS_4399_GetClientId(string msg)
//	{
//		if (!m_enable)
//		{
//			return;
//		}
//		m_getClientId = msg;
//	}
//
//	public void IOS_4399_GetDeviceToken(string msg)
//	{
//		if(!m_enable)
//		{
//			return;
//		}
//		m_getDeviceToken = msg;
//	}
//
//	public void IOS_4399_GeTuiPushPayBack(string msg)
//	{
//		if (!m_enable) 
//		{
//			return;
//		}
//		appNSLog ("IOS_4399_GeTuiPushPayBack:"+msg);
//		UIManager.ShowTipPanel (msg);	
//	}
//		
//	#endregion
//	
//	#region OC接口
//	private static void init4399SDK(string theAppKey, string appSecret, string chanelID, string reciever)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCinit4399SDK(theAppKey, appSecret, chanelID, reciever);
//#endif
//    }
//
//    private static void appOpenLog(string theAppKey, string appSecret)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCappOpenLog(appkey, appSecret);
//#endif
//    }
//
//    private static void appBeforeLoginLog()
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCappBeforeLoginLog();
//#endif
//    }
//    private static void appLogin()
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCappLogin();
//#endif
//    }
//    private static void appLogOut()
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCappLogOut();
//#endif
//    }
//    private static void ChoseServerLog(string serverId)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCChoseServerLog(serverId);
//#endif
//    }
//	private static void createRoleLogWithRoleName(string roleName, string serverId)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCcreateRoleLogWithRoleName(roleName, serverId);
//#endif
//    }
//    private static void roleLevelLogWithRoleLevel(string roleLevel)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCroleLevelLogWithRoleLevel(roleLevel);
//#endif
//    }
//    private static void appReset()
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//         OCappReset();
//#endif
//    }
//
//    private static void appPay(string uid, string serverId, string roleId, string productId, string productCount, string callbackInfo)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappPay(uid, serverId, roleId, productId, productCount, callbackInfo);
//#endif
//    }
//
//    private static void appExchangeStatistic(string exchangeCode, string roleName)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappExchangeStatistic(exchangeCode, roleName);
//#endif
//    }
//
//	private static void appStarComment(string title, string alertMsg, string okLab, string cancelLab, string otherLab, string address)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappStarComment (title, alertMsg, okLab, cancelLab, otherLab, address);
//#endif
//	} 
//
//	private static void appWeixinShare (int type,string imagePath)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappWeixinShare (type, imagePath);
//#endif
//	}    
//
//	private static void appSaveCaptureToAlbum (string imagePath)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappSaveCaptureToAlbum (imagePath);
//#endif
//	}
//
//	private static void appPlaySpeech (string content)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappPlaySpeech (content);
//#endif
//	}
//
//	private static void appStopSpeech()
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappStopSpeech ();
//#endif
//	}
//
//	private static void appShowSpeaker(string isBuildingUI, string url)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappShowSpeaker (isBuildingUI, url);
//#endif
//	}
//
//	private static void appCloseSpeaker()
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappCloseSpeaker ();
//#endif
//	}
//
//	private static void appGetIOSShareData(string roleId, string roleName, string serverName, string roleLevel)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappGetIOSShareData (roleId, roleName, serverName, roleLevel);
//#endif
//	}
//
//private static void appIOSShareLogin(string roleId, string roleName, string serverName, string roleLevel)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappIOSShareLogin (roleId, roleName, serverName, roleLevel);
//#endif
//	}
//
//	private static void appCopyToClipBoard(string text)
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappCopyToClipBoard (text);
//#endif
//	}
//
//    private static void appIOSTakePhoto(string fileName, string filePath)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappTakePhoto(fileName, filePath);
//#endif
//    }
//
//    private static void appIOSLocalPhoto(string fileName, string filePath)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCappLocalPhoto(fileName, filePath);
//#endif
//    }
//
//    private static void appIOSValidatePhoto(string url, Action<int> action)
//    {
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//        _iosValidatePicCallback.Add(action);
//		OCappIOSValidatePhoto(url);
//#endif
//    }
//
    public static void LogCallBack(string condition, string stackTrace, LogType type)
    {
#if ActiveSDK && UNITY_IPHONE
		IosSdkBase.LogCallBack(condition, stackTrace, type);
#endif
    }

	public static void appNSLog(string msg)
    {
#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
		IosSdkBase.appNSLog(msg);
#endif
    }
//
//	private static void initGeTuiSDK()
//	{
//#if ActiveSDK && UNITY_IPHONE && !UNITY_EDITOR
//		OCinitGeTuiSDK();
//#endif
//	}
//
//#if ActiveSDK && UNITY_IPHONE
//    [DllImport("__Internal")]
//    private static extern void OCinit4399SDK(string theAppKey, string appSecret, string chanelID, string reciever);
//
//    [DllImport("__Internal")]
//    private static extern void OCappOpenLog(string theAppKey, string appSecret);
//
//    [DllImport("__Internal")]
//    private static extern void OCappBeforeLoginLog();
//
//    [DllImport("__Internal")]
//    private static extern void OCappLogin();
//
//    [DllImport("__Internal")]
//    private static extern void OCappLogOut();
//
//    [DllImport("__Internal")]
//    private static extern void OCChoseServerLog(string serverId);
//
//    [DllImport("__Internal")]
//    private static extern void OCcreateRoleLogWithRoleName(string roleName, string serverId);
//
//    [DllImport("__Internal")]
//    private static extern void OCroleLevelLogWithRoleLevel(string roleLevel);
//
//    [DllImport("__Internal")]
//    private static extern void OCappReset();
//
//	[DllImport("__Internal")]
//	private static extern void OCappPay (string uid, string serverId, string roleId, string productId, string productCount, string callbackInfo);
//
//	[DllImport("__Internal")]
//	private static extern void OCappNSLog (string msg);
//
//    [DllImport("__Internal")]
//	private static extern void OCappExchangeStatistic (string exchangeCode, string roleName);
//
//	[DllImport("__Internal")]
//	private static extern void OCappStarComment (string title, string alertMsg, string okLab, string cancelLab, string otherLab, string address);
//
//	[DllImport("__Internal")]
//	private static extern void OCappWeixinShare (int type, string imagePath);
//
//	[DllImport("__Internal")]
//	private static extern void OCappSaveCaptureToAlbum (string imagePath);
//
//	[DllImport("__Internal")]
//	private static extern void OCappPlaySpeech (string content);
//
//	[DllImport("__Internal")]
//	private static extern void OCappStopSpeech ();
//
//	[DllImport("__Internal")]
//	private static extern void OCappShowSpeaker (string isBuildingUI, string url);
//
//	[DllImport("__Internal")]
//	private static extern void OCappCloseSpeaker ();
//	
//	[DllImport("__Internal")]
//	private static extern void OCappGetIOSShareData (string roleId, string roleName, string serverName, string roleLevel);
//
//	[DllImport("__Internal")]
//	private static extern void OCappIOSShareLogin (string roleId, string roleName, string serverName, string roleLevel);
//
//	[DllImport("__Internal")]
//	private static extern void OCappCopyToClipBoard (string text);
//
//	[DllImport("__Internal")]
//	private static extern void OCinitGeTuiSDK ();
//
//    [DllImport("__Internal")]
//	private static extern void OCappTakePhoto (string fileName, string filePath);
//
//    [DllImport("__Internal")]
//	private static extern void OCappLocalPhoto (string fileName, string filePath);
//
//
//	[DllImport("__Internal")]
//	private static extern void OCappIOSValidatePhoto (string url);
//    
//
//	
//#endif
//    #endregion
//
//    #region web JS-SDK回调
//
//    /// <summary>
//    /// msg：step|volume|(result[type|code|msg])
//    /// step: NONE-0,INITIALIZED-1,RECORD-2,RECOGNIZE-3,COMPLETE-4
//    /// volume: 音量大小
//    /// result: type error,result
//    ///         code error: 请参考Develop Manual for flash SDK_V3第六章错误码定义
//    ///              result：大于0语音秒数，小于0参考rsltStatus值取负
//    ///         msg 返回的解释结果或者错误消息
//    /// </summary>
//    /// <param name="step"></param>
//    /// <param name="volume"></param>
//    /// <param name="result"></param>
//    private string speech_result_cache;
//    private void JSUpdateSpeechData(string msg)
//    {
//        var data = !string.IsNullOrEmpty(msg) ? msg.Split('|') : null;
//        //0-step,1-volume,2-type,3-code,4-msg
//        if (data[0] == "1")
//        {
//            var last_step = _speech_status;
//            _speech_status = EnumSpeechStatus.INITIALIZED;
//            if (last_step == EnumSpeechStatus.INITIALIZING)
//            {
//                PanelSpeechSpeaker.showSpeaker();
//            }
//        }
//        else if (data[0] == "2")
//        {
//            PanelSpeechSpeaker.UpdateSpeaker(int.Parse(data[1]));
//        }
//        else if (data[0] == "3")
//        {
//            PanelSpeechSpeaker.showSpeaker(1);
//        }
//        else if (data[0] == "4")
//        {
//            _speech_run_time = 0;
//            if (data[2] == "result")
//            {
//                if (data[3]!="0")
//                {
//                    speech_result_cache = data[4] + "|" + data[3];
//                    PlatformAPI.Call("UBAdapter.getSpeechAudioData");
//                }
//                else
//                {
//                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, "您好像没有说话哦");
//                }
//            }
//            else
//            {
//                if (data[3] == "120103" || data[3]=="10105")
//                {
//                    _speech_status = EnumSpeechStatus.NONE;
//                    if (data[3] == "10105")
//                    {
//                        PlatformAPI.Call("showSettingView",true);
//                    }
//                }
//                if (data[3] != "10118")
//                {
//                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, data[4]);
//                }
//                else
//                {
//                    GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 0, "您好像没有说话哦");
//                }
//            }
//        }
//    }
//
//    private void JSUpdateSpeechAudioData(string msg)
//    {
//        byte[] audio_data = null;
//        if (!string.IsNullOrEmpty(msg))
//        {
//            audio_data = new byte[msg.Length/2];
//            for (var i = 0; i < audio_data.Length; i++)
//            {
//                audio_data[i] = Convert.ToByte(msg.Substring(i*2, 2), 16);
//            }
//        }
//        NetChatData.CacheSpeechData(audio_data);
//        GameDispatcher.Dispatch(GameEvent.SDK_SPEECH_COMPLETE, 1, speech_result_cache);
//    }
//    #endregion
//     
//    #region PC API
//
//#if UNITY_STANDALONE
//    [DllImport("Psapi.dll")]
//    public static extern int GetModuleFileNameExW(IntPtr hProcess, IntPtr hModule, byte[] lpFilename, int nSize);
//#endif

//    public static void GetProcessList(string filter = "")
//    {
//
//#if UNITY_STANDALONE
//
//        byte[] buf = new byte[1024];
//        Encoding enc = Encoding.Unicode;        
//
//        List<CDict> listProc = new List<CDict>();
//        Regex reg = new Regex(filter);
//        foreach (var ps in System.Diagnostics.Process.GetProcesses())
//        {
//            CDict dic = new CDict();
//            try
//            {
//                string name = ps.ProcessName;
//                if (string.IsNullOrEmpty(name))
//                    continue;
//                if (!reg.IsMatch(name))
//                    continue;
//                dic.Add("ProcessName", name);
//                dic.Add("Id", ps.Id);
//                dic.Add("StartTime", ps.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
//                //var file = ps.MainModule.FileVersionInfo;
//                //dic.Add("FileName", file.FileName);
//                //dic.Add("Desc", file.FileDescription);
//                //dic.Add("ProductName", file.ProductName);
//
//                int len = GetModuleFileNameExW(ps.Handle, IntPtr.Zero, buf, buf.Length / 2);
//                if (len > 0)
//                {
//                    string fileName = enc.GetString(buf, 0, len * 2);
//                    dic.Add("FileName", fileName);
//                }            
//            }
//            catch (Exception)
//            { continue; }
//            listProc.Add(dic);
//        }
//
//        NetLayer.SendMsg("player", "processlist", listProc);
//#endif
//        
//    }
//
//
//
//    #endregion
}