﻿using UnityEngine;

public class FSound
{
    public FSoundType Type = FSoundType.Sound;
    public string SoundName;
    public AudioClip clip;
    public int Weight = 100;
}