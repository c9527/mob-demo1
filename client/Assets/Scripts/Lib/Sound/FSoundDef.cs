﻿using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class FSoundDef : MonoBehaviour
{
    public AudioClip[] clips;
    public List<FSound> Sounds;
    [HideInInspector]
    public string Name;
    [HideInInspector]
    public string AudioSourceTemplate;
    public string Category;
    public bool SingleStepMode;
    public FSoundDefPlayMode PlayMode;
    public bool StopFirst;
    public float Volume = 1f;
    public float VolumeRandomization;
    public float Pitch = 1f;
    public float PitchRandomization;
    public FSoundDefRandomizationBehavior PitchRandomizationBehavior;
    public int PlayCount;
    public int MaximumPolyphony;
    public Vector2 SpawnTime;   //ms
    public Vector2 TriggerDelay;

    private AudioSource m_as;
    private List<FSound> m_playList = new List<FSound>();
    private Queue<Vector2> m_playLengthQueue = new Queue<Vector2>(); 
    private int m_playIndex;
    private bool m_isPlaying;
    private float m_nextPlayTime;
    private int m_playCount;
    private float m_playPitch;

    private bool IsPlayed { get { return m_playIndex >= 0; }}
    public bool IsPlaying { get { return m_isPlaying; } }

    void Awake()
    {
        m_as = GetComponent<AudioSource>();
        FSoundManager.AddSoundDef(this);
    }

    void OnDestroy()
    {
        FSoundManager.RemoveSoundDef(this);
    }

    public void Play()
    {
        if (TriggerDelay != Vector2.zero)
            m_nextPlayTime = Time.timeSinceLevelLoad + Random.Range(TriggerDelay.x, TriggerDelay.y) * 0.001f;
        else
            m_nextPlayTime = Time.timeSinceLevelLoad;

        if (!SingleStepMode || !m_isPlaying)
        {
            m_playCount = 0;
            m_playIndex = -1;
            m_playPitch = -1;
            ResetPlayList();
            m_playLengthQueue.Clear();
        }
        m_isPlaying = true;
    }

    public void Stop()
    {
        m_isPlaying = false;
    }

    public void Update()
    {
        while (m_playLengthQueue.Count > 0 && Time.timeSinceLevelLoad > m_playLengthQueue.Peek().y)
        {
            m_playLengthQueue.Dequeue();
        }
        
        if (!m_isPlaying)
            return;

        if (Time.timeSinceLevelLoad > m_nextPlayTime )
        {
            PlaySound();

            if (SingleStepMode)
                m_nextPlayTime = float.MaxValue;
            else
            {
                if (SpawnTime != Vector2.zero)
                    m_nextPlayTime = m_nextPlayTime + Random.Range(SpawnTime.x, SpawnTime.y) * 0.001f;
                else
                    Stop();
            }
        }
    }

    private void PlaySound()
    {
        if (m_playList.Count == 0)
        {
            Stop();
            return;
        }

        var volumeScale = 1f;
        if (!Mathf.Approximately(VolumeRandomization, 0))
            volumeScale = 1 + Random.Range(-VolumeRandomization, VolumeRandomization);

        var pitch = 1f;
        if (!Mathf.Approximately(PitchRandomization, 0))
        {
            if (PitchRandomizationBehavior == FSoundDefRandomizationBehavior.RandomizeWhenStarts)
            {
                if (m_playPitch < 0)
                    m_playPitch = Pitch * (1 + Random.Range(-PitchRandomization, PitchRandomization));
                pitch = m_playPitch;
            }
            else
                pitch = Pitch * (1 + Random.Range(-PitchRandomization, PitchRandomization));
        }

        if (m_as.isActiveAndEnabled && (MaximumPolyphony <= 0 || m_playLengthQueue.Count < MaximumPolyphony))
        {
            var sound = GetSound();
            if (sound.Type == FSoundType.Sound)
            {
                if (StopFirst)
                    m_as.Stop();
                m_as.volume = Volume;
                m_as.pitch = pitch;
                m_as.PlayOneShot(sound.clip, volumeScale);
                m_playCount++;

                if (MaximumPolyphony > 0 && sound.clip)
                {
                    var time = Time.timeSinceLevelLoad;
                    m_playLengthQueue.Enqueue(new Vector2(time, time + sound.clip.length));
                }
            }
        }

        if (PlayCount > 0 && m_playCount >= PlayCount)
        {
            Stop();
            return;
        }

        if (PlayMode == FSoundDefPlayMode.ShuffleWhenFinish && m_playCount % Sounds.Count == 0)
            ResetPlayList();
    }

    private FSound GetSound()
    {
        if (PlayMode == FSoundDefPlayMode.Random)
            m_playIndex = RandomWithWeight();
        else if (PlayMode == FSoundDefPlayMode.RandomNoRepeat)
            m_playIndex = RandomWithWeight(true);
        else
        {
            if (IsPlayed)
                m_playIndex++;
            else
                m_playIndex = 0;
            m_playIndex %= m_playList.Count;
        }
        
        return m_playList[m_playIndex];
    }

    private int RandomWithWeight(bool noRepeat = false)
    {
        var r = Random.value;
        var totalWeight = 0f;

        var percentList = new float[m_playList.Count];
        for (int i = 0; i < m_playList.Count; i++)
        {
            if (noRepeat && i == m_playIndex)
                continue;
            totalWeight += m_playList[i].Weight;
        }
        for (int i = 0; i < m_playList.Count; i++)
        {
            if (noRepeat && i == m_playIndex)
                continue;
            if (i == 0)
                percentList[i] = m_playList[i].Weight / totalWeight;
            else
                percentList[i] = percentList[i-1] + m_playList[i].Weight / totalWeight;
        }

        for (int i = 0; i < percentList.Length; i++)
        {
            if (noRepeat && i == m_playIndex)
                continue;
            if (percentList[i] <= 0)
                continue;
            if (r <= percentList[i])
                return i;
        }
        return 0;
    }

    private void ResetPlayList()
    {
        m_playList.Clear();
        m_playList.AddRange(Sounds);

        if (PlayMode == FSoundDefPlayMode.Shuffle || PlayMode == FSoundDefPlayMode.ShuffleWhenFinish)
        {
            for (int i = 0; i < m_playList.Count; i++)
            {
                var j = Random.Range(i, m_playList.Count);
                var tmp = m_playList[i];
                m_playList[i] = m_playList[j];
                m_playList[j] = tmp;
            }
        }
    }
}

public enum FSoundDefPlayMode
{
    Sequential,         //按列表顺序播放
    Random,             //每次随机挑选播放
    RandomNoRepeat,     //相邻两次随机不重复播放
    Shuffle,            //乱序播放
    ShuffleWhenFinish,  //每次列表播放完后再次乱序
}

public enum FSoundDefRandomizationBehavior
{
    RandomizeEverySpawn,
    RandomizeWhenStarts
}

public enum FSoundType
{
    Sound,
    DontPlay,
}