﻿using UnityEngine;
using System.Collections;
using System;


public class EffectPlugin_Timeout : MonoBehaviour {

    public float lifeTime = 2;     // 生命周期，ms 如果为0则不结束
    //public float minPlayTime = 0;     // 至少要播放多久才会被停止, ms

    private Action<GameObject, string> m_funCallBack;
    private string m_strEffectID;
    private float m_fTick;
    private bool m_bPlayinging = false;
 

    public void Reset()
    {
        m_bPlayinging = false;
    }

    public float tickTime
    {
        get { return m_fTick; }
    }

    public void Play()
    {
        m_bPlayinging = true;
        m_fTick = 0;
        enabled = true;
    }

    public void Stop()
    {
        m_bPlayinging = false;
        enabled = false;
    }
    
    public void SetTimeoutCallBack(Action<GameObject, string> callBack, string effectID = "")
    {
        m_funCallBack = callBack;
        m_strEffectID = effectID;
        enabled = false;
    }

    private void Complete()
    {
        if (m_funCallBack != null)
            m_funCallBack(gameObject, m_strEffectID);        
        
        m_bPlayinging = false;
    }
	
	void Update () 
    {
        // 至少要播放这么久才会被结束
        //if (m_bPlayinging == false && m_fTick < minPlayTime)
        //{
        //    m_fTick += Time.deltaTime;
        //    if(m_fTick >= minPlayTime)
        //    {
        //        Complete();
        //        return;
        //    }
        //}

        if (m_bPlayinging == true && lifeTime != 0)
        {
            m_fTick += Time.deltaTime;
            if (m_fTick >= lifeTime)
            {
                Complete();
            }
        }
	}
}
