﻿namespace fps.game
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;


    public class EffectPlugin_Fadeout : MonoBehaviour
    {
        public float fadeSpeed = 5f;
        public float initAlpha = 0.5f;
        public string colorPropertyName = "_TintColor";
        public bool autoDeactive = true;

        private List<Material> m_materials;
        private List<Color> m_colors;
        private GameObject m_go;
        private Renderer[] m_renderers;


        void Awake()
        {
            m_go = gameObject;
            m_materials = new List<Material>();
            m_colors = new List<Color>();
            m_renderers = m_go.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < m_renderers.Length; i++)
            {
                m_materials.Add(m_renderers[i].material);
                m_colors.Add(Color.white);
            }

            for (int i = 0; i < m_materials.Count; i++)
            {
                if (m_materials[i].HasProperty(colorPropertyName))
                {
                    var color = m_materials[i].GetColor(colorPropertyName);
                    color.a = 0;
                    m_colors[i] = color;
                    //_material.SetColor(colorPropertyName, _colorMaterial);
                }
                else
                {
                    //Logger.Warning("EffectPlugin_Fadeout Material has no propertyName " + colorPropertyName + " GameObject: " + gameObject.name);
                }
            }
            
        }

        public void Play()
        {
            if (m_colors != null)
            {
                for (int i = 0; i < m_colors.Count; i++)
                {
                    var clr = m_colors[i];
                    clr.a = initAlpha;
                    m_colors[i] = clr;
                }
                
            }
            if (transform != null)
                transform.Rotate(0, 0, Random.Range(0, 360));

            if (m_renderers != null)
            {
                for (int i = 0; i < m_renderers.Length; i++)
                {
                    if (m_renderers[i].enabled == false)
                        m_renderers[i].enabled = true;
                }
            }
        }

        public void Stop()
        {
            if (m_colors != null)
            {
                for (int i = 0; i < m_colors.Count; i++)
                {
                    var clr = m_colors[i];
                    clr.a = 0;
                    m_colors[i] = clr;
                }
            }
            if (m_renderers != null)
            {
                for (int i = 0; i < m_renderers.Length; i++)
                {
                    m_renderers[i].enabled = false;
                }
            }
        }

        void Update()
        {
            var allAlphaZero = true;
            for (int i = 0; i < m_colors.Count; i++)
            {
                var clr = m_colors[i];
                if (clr.a > 0f)
                {
                    clr.a -= fadeSpeed * Time.deltaTime;
                    clr.a = clr.a < 0 ? 0 : clr.a;
                    m_colors[i] = clr;
                    allAlphaZero = false;
                }
            }

            if (allAlphaZero && autoDeactive && m_go.activeSelf)
            {
                for (int i = 0; i < m_renderers.Length; i++)
                {
                    m_renderers[i].enabled = false;
                }
            }
        }
    }

}
