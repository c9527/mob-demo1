﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using UnityEngine;

namespace UDB
{

    class UDB
    {
        CNetPacker mNet = new CNetPacker();
        List<Transform> mGoList = new List<Transform>();
        Dictionary<Transform, int> mGoId = new Dictionary<Transform, int>();
        List<Type> mComList = new List<Type>();

        Dictionary<Type, int> mComId = new Dictionary<Type, int>();
        Queue<CObj> mLogList = new Queue<CObj>();

        public delegate bool ScriptExe(string code, out string result);

        int mMaxLogCount;

        ScriptExe mScriptExe;

        float mReportTime = 0;

        int mScreenshotQuality = -1;

        public UDB(int maxLogCount = 1000)
        {
            mMaxLogCount = maxLogCount;
            mScriptExe = delegate (string code, out string ret)
              {
                  ret = "NotImplemented: " + code;
                  return false;
              };
        }

        public void Start()
        {
            mNet.StartUdp();
        }

        public void Close()
        {
            mNet.Close();
        }

        public IEnumerator UpdateCallback()
        {
            while (true)
            {
                try
                {
                    if (Time.time > mReportTime)
                        ReportToServer();

                    NetPack pack;
                    while ((pack = mNet.PopPack()) != null)
                        ProcessPack(pack);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }

                if (mScreenshotQuality >= 0)
                {
                    yield return new WaitForEndOfFrame();
                    try
                    { ProcessScreenshot(mScreenshotQuality); }
                    catch (Exception e)
                    { Debug.LogException(e); }
                    mScreenshotQuality = -1;
                }

                yield return new WaitForSeconds(0.1f);
            }
        }

        public void ReportToServer()
        {
            mReportTime = Time.time + 60;
            CObj obj = GetReportData();
            mNet.BroadcastToServer(NetProto.Report, JsonHelper.Save(obj));
        }

        public void LogCallBack(string condition, string stackTrace, LogType type)
        {
            CObj obj = new CObj();
            obj["time"] = DateTime.Now.ToBinary();
            obj["cond"] = condition;
            obj["stack"] = stackTrace;
            obj["type"] = (int)type;
            SendTcp(NetProto.PushLog, obj);
            mLogList.Enqueue(obj);
            if (mLogList.Count > mMaxLogCount)
                mLogList.Dequeue();
        }

        public void SetScriptProcess(ScriptExe func)
        {
            mScriptExe = func;
        }

        CObj GetReportData()
        {
            CObj obj = new CObj();
            obj["name"] = SystemInfo.deviceName;
            obj["model"] = SystemInfo.deviceModel;
            obj["type"] = SystemInfo.deviceType.ToString();
            obj["duid"] = SystemInfo.deviceUniqueIdentifier;
            obj["os"] = SystemInfo.operatingSystem;
            obj["plat"] = Application.platform.ToString();
            try
            { obj["pid"] = System.Diagnostics.Process.GetCurrentProcess().Id; }
            catch
            { obj["pid"] = 0; }
            obj["status"] = StstusText;
            obj["ver"] = 3;
            return obj;
        }

        public bool SendUdp(EndPoint ep, NetProto id, CObj data = null)
        {
            return mNet.SendUdp(ep, id, data);
        }

        public bool SendTcp(NetProto id, CObj data = null)
        {
            return mNet.SendTcp(id, data);
        }

        void ProcessPack(NetPack pack)
        {
            Dictionary<string, object> obj = pack.Obj;

            switch (pack.id)
            {
                case NetProto.Connect:
                    string addr = obj.Str("addr");
                    if (string.IsNullOrEmpty(addr))
                        addr = (pack.ep as IPEndPoint).Address.ToString() + ":" + obj.Str("port");
                    ProcessConnect(addr);
                    break;
                case NetProto.GetChildren:
                    ProcessGetChildren(obj.Int("id"));
                    break;
                case NetProto.SetActive:
                    Transform go = GetGo(obj.Int("id"));
                    if (go != null)
                        go.gameObject.TrySetActive(obj.Bool("active"));
                    break;
                case NetProto.GetComponents:
                    ProcessGetComponents(obj.Int("id"));
                    break;
                case NetProto.SetComponentField:
                    ProcessSetComponentField(obj.Int("gid"), obj.Int("cid"), obj.Str("key"), obj["value"]);
                    break;
                case NetProto.Screenshot:
                    mScreenshotQuality = obj.Int("quality");
                    break;
                case NetProto.Script:
                    ProcessScript(obj.Int("sid"), obj.Str("code"));
                    break;
                case NetProto.BinFile:
#if !UNITY_WEBPLAYER
                    File.WriteAllBytes(obj.Str("path"), pack.buf);
#endif
                    break;
                case NetProto.OpenPath:
                    ProcessOpenPath(obj.Str("path"));
                    break;
                default:
                    break;
            }
        }

        void ProcessConnect(string addr)
        {
            mNet.ConnectTcp(addr);
            mGoList.Clear();
            mGoId.Clear();
            mComList.Clear();
            mComId.Clear();
            SendTcp(NetProto.Report, GetReportData());

            foreach (CObj obj in mLogList)
                SendTcp(NetProto.PushLog, obj);
            mLogList.Clear();
        }

        Transform GetGo(int id)
        {
            Transform go = mGoList[id - 1];
            if (go == null)
                return null;
            return go;
        }

        void ProcessGetChildren(int id)
        {
            IEnumerable<Transform> gos;
            if (id > 0)
            {
                Transform go = GetGo(id);
                if (go == null)
                    return;
                gos = EnumChildren(go);
            }
            else
            {
                List<Transform> roots = new List<Transform>();
                foreach (Transform go in Transform.FindObjectsOfType<Transform>())
                {
                    if (go.parent == null)
                        roots.Add(go);
                }
                gos = roots;
            }

            List<CObj> list = new List<CObj>();
            foreach (Transform go in gos)
            {
                CObj ro = new CObj();
                ro["name"] = go.name;
                ro["active"] = go.gameObject.activeSelf;
                ro["child"] = go.childCount;
                int cid;
                if (!mGoId.TryGetValue(go, out cid))
                {
                    mGoList.Add(go);
                    cid = mGoList.Count;
                    mGoId[go] = cid;
                }
                ro["id"] = cid;
                list.Add(ro);
            }

            CObj ret = new CObj();
            ret["id"] = id;
            ret["list"] = list;
            SendTcp(NetProto.GetChildren, ret);
        }

        static IEnumerable<Transform> EnumChildren(Transform go)
        {
            for (int i = 0; i < go.childCount; i++)
                yield return go.GetChild(i);
        }

        void ProcessGetComponents(int id)
        {
            Transform go = GetGo(id);
            if (go == null)
                return;
            List<CObj> list = new List<CObj>();
            foreach (Component com in go.GetComponents<Component>())
            {
                Type typ = com.GetType();
                CObj ro = new CObj();
                ro["name"] = typ.Name;
                int cid;
                if (!mComId.TryGetValue(typ, out cid))
                {
                    mComList.Add(typ);
                    cid = mComList.Count;
                    mComId[typ] = cid;
                    ro["fields"] = GetComponentFields(typ);
                }
                ro["id"] = cid;
                ro["values"] = GetComponentValues(com);
                list.Add(ro);
            }

            CObj ret = new CObj();
            ret["id"] = id;
            ret["list"] = list;
            SendTcp(NetProto.GetComponents, ret);
        }

        void ProcessSetComponentField(int gid, int cid, string key, object value)
        {
            Transform go = GetGo(gid);
            if (go == null)
                return;
            Type typ = mComList[cid - 1];
            Component com = go.GetComponent(typ);
            MemberInfo mi = typ.GetMember(key)[0];
            switch (mi.MemberType)
            {
                case MemberTypes.Field:
                    FieldInfo fi = mi as FieldInfo;
                    fi.SetValue(com, ConvertTo(fi.FieldType, value));
                    break;
                case MemberTypes.Property:
                    PropertyInfo pi = mi as PropertyInfo;
                    pi.SetValue(com, ConvertTo(pi.PropertyType, value), null);
                    break;
                default:
                    return;
            }
        }

        void ProcessScreenshot(int quality)
        {
            Texture2D img = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            Rect rect = new Rect(0, 0, Screen.width, Screen.height);
            img.ReadPixels(rect, 0, 0);
            img.Apply();
            byte[] buf;
            if (quality > 0)
                buf = img.EncodeToJPG(quality);
            else
                buf = img.EncodeToPNG();
            mNet.SendTcp(NetProto.Screenshot, "", buf);
        }

        void ProcessScript(int sid, string code)
        {
            bool ok;
            string ret;
            try
            { ok = mScriptExe(code, out ret); }
            catch (Exception e)
            {
                Debug.LogException(e);
                ret = e.Message;
                ok = false;
            }

            CObj obj = new CObj();
            obj["sid"] = sid;
            obj["ok"] = ok;
            obj["ret"] = ret;
            SendTcp(NetProto.Script, obj);
        }

        void ProcessOpenPath(string path)
        {
#if !UNITY_WEBPLAYER
            path = path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            if (File.Exists(path))
            {
                SendBinFile(path);
                return;
            }

            List<CObj> list = new List<CObj>();
            if (string.IsNullOrEmpty(path))
            {
                foreach (string driver in Directory.GetLogicalDrives())
                {
                    CObj file = new CObj();
                    file["name"] = driver;
                    list.Add(file);
                }
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    CObj file = new CObj();
                    file["name"] = di.Name;
                    file["time"] = di.LastWriteTime.ToBinary();
                    list.Add(file);
                }
                foreach (FileInfo fi in dir.GetFiles())
                {
                    CObj file = new CObj();
                    file["name"] = fi.Name;
                    file["time"] = fi.LastWriteTime.ToBinary();
                    file["size"] = fi.Length;
                    list.Add(file);
                }
                path = path.TrimEnd('/', '\\') + Path.DirectorySeparatorChar;
            }

            CObj obj = new CObj();
            obj["path"] = path;
            obj["list"] = list;
            SendTcp(NetProto.OpenPath, obj);
#endif
        }

#if !UNITY_WEBPLAYER
        public void SendBinFile(string path)
        {
            using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                SendBinFile(path, fs);
                fs.Close();
            }
        }
#endif

        public void SendBinFile(string path, byte[] buf)
        {
            CObj obj = new CObj();
            obj["path"] = path;
            mNet.SendTcp(NetProto.BinFile, JsonHelper.Save(obj), buf);
        }

        public void SendBinFile(string path, Stream stm)
        {
            using (MemoryStream mem = new MemoryStream((int)stm.Length))
            {
                stm.Position = 0;
                stm.Read(mem.GetBuffer(), 0, (int)stm.Length);
                SendBinFile(path, mem.GetBuffer());
            };
        }

        object GetComponentFields(Type typ)
        {
            List<CObj> list = new List<CObj>();
            Type baseType = typeof(Component);
            foreach (MemberInfo mi in typ.GetMembers())
            {
                if (mi.DeclaringType == baseType || baseType.IsSubclassOf(mi.DeclaringType))
                    continue;
                bool canset = true;
                Type mt;
                switch (mi.MemberType)
                {
                    case MemberTypes.Field:
                        mt = (mi as FieldInfo).FieldType;
                        break;
                    case MemberTypes.Property:
                        mt = (mi as PropertyInfo).PropertyType;
                        canset = (mi as PropertyInfo).CanWrite;
                        break;
                    default:
                        continue;
                }
                CObj info = new CObj();
                info["name"] = mi.Name;
                info["type"] = mt.Name;
                info["canset"] = canset;
                if (mt.IsEnum)
                    info["enum"] = Enum.GetNames(mt);
                list.Add(info);
            }
            return list;
        }

        object GetComponentValues(Component com)
        {
            List<object> list = new List<object>();
            Type baseType = typeof(Component);
            foreach (MemberInfo mi in com.GetType().GetMembers())
            {
                if (mi.DeclaringType == baseType || baseType.IsSubclassOf(mi.DeclaringType))
                    continue;
                object value;
                switch (mi.MemberType)
                {
                    case MemberTypes.Field:
                        value = (mi as FieldInfo).GetValue(com);
                        break;
                    case MemberTypes.Property:
                        if (!(mi as PropertyInfo).CanRead)
                            continue;
                        value = (mi as PropertyInfo).GetValue(com, null);
                        break;
                    default:
                        continue;
                }
                if (value != null && !(value is string) && !value.GetType().IsPrimitive)
                    value = value.ToString();
                list.Add(value);
            }
            return list;
        }

        object ConvertTo(Type typ, object value)
        {
            if (value == null)
                return null;
            if (typ == typeof(string))
                return value.ToString();
            if (typ.IsEnum)
                return Enum.Parse(typ, value.ToString());
            if (!typ.IsPrimitive)
                throw new Exception("unknown type: " + typ.Name);
            if (typ == typeof(int))
                return Convert.ToInt32(value);
            if (typ == typeof(uint))
                return Convert.ToUInt32(value);
            if (typ == typeof(long))
                return Convert.ToInt64(value);
            if (typ == typeof(byte))
                return Convert.ToByte(value);
            if (typ == typeof(short))
                return Convert.ToInt16(value);
            if (typ == typeof(float))
                return Convert.ToSingle(value);
            if (typ == typeof(double))
                return Convert.ToDouble(value);
            if (typ == typeof(bool))
                return Convert.ToBoolean(value);
            throw new Exception("unknown primitive type: " + typ.Name);
        }

        public string StstusText { get; set; }
    }

}
