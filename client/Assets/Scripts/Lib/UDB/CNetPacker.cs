﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace UDB
{

    class CObj : Dictionary<string, object>
    {
    }

    enum NetProto
    {
        Test,
        Report,
        ListClient,
        Connect,
        GetChildren,
        SetActive,
        GetComponents,
        SetComponentField,
        PushLog,
        Screenshot,
        Script,
        BinFile,
        OpenPath,
    }

    class NetPack
    {
        public NetProto id;
        public EndPoint ep;
        public string data;
        public byte[] buf = null;
        public Dictionary<string, object> Obj
        { get { return JsonHelper.Load(data) as Dictionary<string, object>; } }
    }

    class CNetPacker
    {
        public const int ServerPort = 10001;
        public const int TransferPort = 10002;

        Socket mUdpSk = null;
        Socket mTcpSk = null;
        Socket mListenSk = null;

        MemoryStream mSendMem = new MemoryStream();
        BinaryWriter mSendWriter;
        Queue<NetPack> mPacks = new Queue<NetPack>();

        static string[] msServerList = { "172.16.11.69", "42.62.106.219" };

        public CNetPacker()
        {
            mSendWriter = new BinaryWriter(mSendMem);

#if UNITY_EDITOR
            UnityEditor.EditorApplication.update += CheckCompiling;
#endif
        }

#if UNITY_EDITOR
        void CheckCompiling()
        {
            if (!UnityEditor.EditorApplication.isCompiling)
                return;
            Close();
            UnityEditor.EditorApplication.update -= CheckCompiling;
        }
#endif

        public void Close()
        {
            CloseUdp();
            CloseTcp(ref mListenSk);
            CloseTcp(ref mTcpSk);
        }

        public int StartUdp(int port = 0)
        {
            Socket sk = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sk.Bind(new IPEndPoint(IPAddress.Any, port));
            CloseUdp();
            mUdpSk = sk;
            (new System.Threading.Thread(RecvUdpWorker)).Start(sk);
            return (sk.LocalEndPoint as IPEndPoint).Port;
        }

        void CloseUdp()
        {
            Socket sk = mUdpSk;
            if (sk == null)
                return;
            mUdpSk = null;
            sk.Close();
        }

        public int ListenTcp(int port = 0)
        {
            Socket sk = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sk.Bind(new IPEndPoint(IPAddress.Any, port));
            sk.Listen(0);
            CloseTcp(ref mListenSk);
            mListenSk = sk;
            (new System.Threading.Thread(ListenWorker)).Start(sk);
            return (sk.LocalEndPoint as IPEndPoint).Port;
        }

        public void ConnectTcp(string addr)
        {
            Socket sk = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sk.Connect(PasreAddr(addr));
            CloseTcp(ref mTcpSk);
            mTcpSk = sk;
            (new System.Threading.Thread(RecvTcpWorker)).Start(sk);
        }

        static void CloseTcp(ref Socket refSk)
        {
            Socket sk = refSk;
            if (sk == null)
                return;
            refSk = null;
            try
            { sk.Shutdown(SocketShutdown.Both); }
            catch { }
            sk.Close();
        }

        public NetPack PopPack()
        {
            while (mPacks.Count <= 0)
                return null;
            lock (mPacks)
                return mPacks.Dequeue();
        }

        public bool SendUdp(EndPoint ep, NetProto id, CObj data = null)
        {
            return SendUdp(ep, id, JsonHelper.Save(data));
        }

        public bool SendUdp(EndPoint ep, NetProto id, string data)
        {
            Socket sk = mUdpSk;
            if (sk == null)
                return false;
            mSendMem.SetLength(0);
            mSendWriter.Write((int)id);
            mSendWriter.Write(data);
            try
            { sk.SendTo(mSendMem.GetBuffer(), (int)mSendMem.Length, SocketFlags.None, ep); }
            catch
            { return false; }
            return true;
        }

        public void BroadcastToServer(NetProto id, string data)
        {
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, ServerPort);
            List<string> newList = null;
            foreach (string host in msServerList)
            {
                ep.Address = IPAddress.Parse(host);
                if (SendUdp(ep, id, data))  // WEB版本这里会因为843端口验证错产生卡顿和失败
                    continue;
                if (newList == null)
                    newList = msServerList.ToList();
                newList.Remove(host);
            }
            if (newList != null)
                msServerList = newList.ToArray();
        }

        public bool SendTcp(NetProto id, CObj data = null)
        {
            if (mTcpSk == null)
                return false;
            return SendTcp(id, JsonHelper.Save(data));
        }

        public bool SendTcp(NetProto id, string data, byte[] buf = null)
        {
            Socket sk = mTcpSk;
            if (sk == null)
                return false;
            mSendMem.SetLength(0);
            mSendWriter.Write(0);
            mSendWriter.Write((int)id);
            mSendWriter.Write(data);
            if (buf != null)
                mSendWriter.Write(buf);
            mSendMem.Position = 0;
            mSendWriter.Write((int)mSendMem.Length - 4);
            try
            { sk.Send(mSendMem.GetBuffer(), (int)mSendMem.Length, SocketFlags.None); }
            catch
            { return false; }
            return true;
        }

        void RecvUdpWorker(object obj)
        {
            Socket sk = obj as Socket;
            using (MemoryStream mem = new MemoryStream(1500))
            using (BinaryReader br = new BinaryReader(mem))
            {
                mem.SetLength(mem.Capacity);
                while (sk == mUdpSk)
                {
                    EndPoint ep = new IPEndPoint(IPAddress.Any, 0);
                    int len = 0;
                    try
                    { len = sk.ReceiveFrom(mem.GetBuffer(), ref ep); }
                    catch (ObjectDisposedException)
                    { break; }
                    catch (SocketException)
                    { continue; }

                    mem.Position = 0;
                    try
                    { PushPack(ep, br, len); }
                    catch (IOException)
                    { }
                }
            }
        }

        void ListenWorker(object obj)
        {
            Socket listen = obj as Socket;
            while (listen == mListenSk)
            {
                try
                {
                    using (Socket sk = listen.Accept())
                    {
                        CloseTcp(ref mTcpSk);
                        mTcpSk = sk;
                        RecvTcpWorker(sk);  // 单线程处理，同时只允许一个连接
                    }
                }
                catch (ObjectDisposedException)
                { break; }
                catch (SocketException)
                { continue; }
            }
        }

        void RecvTcpWorker(object obj)
        {
            Socket sk = obj as Socket;
            EndPoint ep = sk.RemoteEndPoint;
            using (MemoryStream mem = new MemoryStream(0x10000))
            using (BinaryReader br = new BinaryReader(mem))
            {
                while (sk == mTcpSk && RecvTcpData(sk, mem))
                {
                    int size;
                    while (CanRecvTcpPack(mem, out size))
                    {
                        mem.Position += 4;
                        PushPack(ep, br, size);
                    }
                }
            }
            sk.Close();
        }

        bool RecvTcpData(Socket sk, MemoryStream mem)
        {
            byte[] buf = mem.GetBuffer();
            long pos = mem.Position;
            long len = mem.Length;
            if (pos > 0)
            {
                for (long i = pos; i < len; i++)
                    buf[i - pos] = buf[i];
                mem.SetLength(len -= pos);
                mem.Position = 0;
            }
            if (len >= mem.Capacity)
            {
                mem.Capacity <<= 1;
                buf = mem.GetBuffer();
            }
            mem.SetLength(mem.Capacity);
            int add = 0;
            try
            { add = sk.Receive(buf, (int)len, buf.Length - (int)len, SocketFlags.None); }
            catch { }
            mem.SetLength(len + add);
            return add > 0;
        }

        bool CanRecvTcpPack(MemoryStream mem, out int size)
        {
            size = 0;
            int pos = (int)mem.Position;
            int len = (int)mem.Length;
            if (len < pos + 4)
                return false;
            byte[] buf = mem.GetBuffer();
            size = buf[pos + 3] << 24 | buf[pos + 2] << 16 | buf[pos + 1] << 8 | buf[pos];
            return len >= pos + 4 + size;
        }

        void PushPack(EndPoint ep, BinaryReader br, int size)
        {
            long l = br.BaseStream.Position;
            NetPack pack = new NetPack();
            pack.ep = ep;
            pack.id = (NetProto)br.ReadInt32();
            pack.data = br.ReadString();
            l = l + size - br.BaseStream.Position;
            if (l > 0)
            {
                pack.buf = new byte[l];
                br.Read(pack.buf, 0, (int)l);
            }
            lock (mPacks)
                mPacks.Enqueue(pack);
        }

        static public IPEndPoint PasreAddr(string addr)
        {
            int i = addr.IndexOf(':');
            return new IPEndPoint(IPAddress.Parse(addr.Substring(0, i)), int.Parse(addr.Substring(i + 1)));
        }
    }

}
