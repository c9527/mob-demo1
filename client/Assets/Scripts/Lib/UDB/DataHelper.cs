﻿using System;
using System.Collections.Generic;

static class DataHelper
{
    public static object Get(this Dictionary<string, object> dict, string key, object def)
    {
        object obj;
        if (dict.TryGetValue(key, out obj))
            return obj;
        return def;
    }

    public static string Str(this Dictionary<string, object> dict, string key, string def = "")
    {
        object o = dict.Get(key, def);
        if (o == null)
            return null;
        return o.ToString();
    }

    public static int Int(this Dictionary<string, object> dict, string key, int def = 0)
    {
        return Convert.ToInt32(dict.Get(key, def));
    }

    public static long Long(this Dictionary<string, object> dict, string key, long def = 0)
    {
        return Convert.ToInt64(dict.Get(key, def));
    }

    public static bool Bool(this Dictionary<string, object> dict, string key, bool def = false)
    {
        object o = dict.Get(key, def);
        if (!(o is bool))
            return def;
        return (bool)o;
    }

    public static Dictionary<string, object> Dict(this Dictionary<string, object> dict, string key)
    {
        return dict.Get(key, null) as Dictionary<string, object>;
    }

    public static T[] Ary<T>(this Dictionary<string, object> dict, string key)
    {
        Array ary = dict.Get(key, null) as Array;
        if (ary == null)
            return null;
        Func<object, object> conv;
        Type typ = typeof(T);
        if (typ == typeof(int))
            conv = (o) => Convert.ToInt32(o);
        else if (typ == typeof(long))
            conv = (o) => Convert.ToInt64(o);
        else if (typ == typeof(bool))
            conv = (o) => o is bool ? o : false;
        else if (typ == typeof(string))
            conv = (o) => o.ToString();
        else
            conv = (o) => o is T ? o : null;
        T[] ret = new T[ary.Length];
        for (int i = 0; i < ret.Length; i++)
            ret[i] = (T)conv(ary.GetValue(i));
        return ret;
    }

    public static Dictionary<string, object>[] DictAry(this Dictionary<string, object> dict, string key)
    {
        return dict.Ary<Dictionary<string, object>>(key);
    }

    public static void Merge<T>(this Dictionary<string, object> dict, Dictionary<string, T> from)
    {
        foreach (string key in from.Keys)
            dict[key] = from[key];
    }

    public static List<T> ToList<T>(this IEnumerable<T> em)
    {
        List<T> list = new List<T>();
        foreach (T e in em)
            list.Add(e);
        return list;
    }
}
