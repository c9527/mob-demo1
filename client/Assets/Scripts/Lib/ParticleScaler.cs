﻿using UnityEngine;
using System.Collections;

public class ParticleScaler : MonoBehaviour
{
    private ScaleVo[] m_startList;
    private ParticleSystem[] m_psList;

    public bool m_update = false;
	void Awake ()
	{
        m_psList = GetComponentsInChildren<ParticleSystem>();

        m_startList = new ScaleVo[m_psList.Length];
        for (int i = 0; i < m_psList.Length; i++)
        {
            var vo = new ScaleVo();
            vo.startSize = m_psList[i].startSize;
            vo.startSpeed = m_psList[i].startSpeed;
            m_startList[i] = vo;
        }
	}

    void Start()
    {
        UpdateScale();
    }

    void Update()
    {
        if (m_update)
        {
            m_update = false;
            UpdateScale();
        }
    }

    public void UpdateScale()
    {
        if (m_psList == null)
            return;
        
        for (int i = 0; i < m_psList.Length; i++)
        {
            var scale = m_psList[i].transform.lossyScale.x;
            m_psList[i].startSize = m_startList[i].startSize * scale;
            m_psList[i].startSpeed = m_startList[i].startSpeed * scale;
        }
    }

    class ScaleVo
    {
        public float startSize;
        public float startSpeed;
    }
}
