﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class AudioManager
{
    const string SOUND_DIR = "Sounds/";
    const string HIT_POINT = "hitPoint";

    private static Transform m_audioTran;
    private static Transform m_listenerParent;
    private static AudioListener m_listener;
    private static Dictionary<string, AudioSource> m_audioSourceDic;
    private static List<FPSAudioSource> m_hitList;
    private static Dictionary<string, AudioClip> m_audioDic; 

    private static bool m_musicMute = false;    //音乐静音
    private static float m_musicVolume = 1f;    //背景音乐音量值

    private static bool m_soundMute = false;    //音效静音
    private static float m_soundVolumn = 1f;    //音效音量值

    private static float m_fightSoundVolumn = 1f;    //战斗音效音量值

    private static string m_curBgSourceName = AudioSourceConst.MUSIC_BACKGROUND1;
    private static bool m_isBgCrossFading;
    private static float m_fadeValue = 0f;

    public static void Init()
    {
        if (m_audioTran == null)
        {
            m_audioSourceDic = new Dictionary<string, AudioSource>();
            m_hitList = new List<FPSAudioSource>();
            m_audioDic = new Dictionary<string, AudioClip>();
            m_audioTran = new GameObject("AudioManager").transform;
            GameObject.DontDestroyOnLoad(m_audioTran);

            SetListener();
        }

        MusicVolume = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_MUSIC);
        SoundVolume = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_SOUND);
        FightSoundVolume = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_FIGHT_SOUND);
    }

    public static void Update()
    {
        if (m_isBgCrossFading)
        {
            var to = GetSource(m_curBgSourceName);
            var from = GetSource(m_curBgSourceName == AudioSourceConst.MUSIC_BACKGROUND1 ? AudioSourceConst.MUSIC_BACKGROUND2 : AudioSourceConst.MUSIC_BACKGROUND1);

            from.volume = (1 - m_fadeValue) * m_musicVolume;
            to.volume = m_fadeValue * m_musicVolume;

            m_fadeValue = m_fadeValue + Time.deltaTime * 0.8f;
            if (m_fadeValue > 1)
            {
                m_isBgCrossFading = false;
                from.Stop();
            }
        }
    }

    /// <summary>
    /// 设置Listener的位置
    /// </summary>
    /// <param name="parent"></param>
    public static void SetListener(Transform parent = null)
    {
        if (parent == null)
        {
            for (int i = 0; i < m_hitList.Count; i++)
            {
                GameObject.Destroy(m_hitList[i].gameObject);
            }
            m_hitList.Clear();
        }
        if (parent == null)
            parent = m_audioTran;
        if (m_listenerParent != null)
            GameObject.DestroyImmediate(m_listener);

        m_listenerParent = parent;
        m_listener = m_listenerParent.gameObject.AddComponent<AudioListener>();

    }

    public static void PlayGuideVoice(string clipName)
    {
        if (m_listener == null)
            return;

        var clipPath = "Sounds/Guide/" + clipName;
        var source = GetSource(AudioSourceConst.SOUND_GUIDE_UI);
        ResourceManager.LoadAudio(clipPath, clip => PlayAudioOneShot(source, clip, true, 1, m_soundVolumn, m_soundMute));
    }

    /// <summary>
    /// 播放UI音效，单次播放，可选是中断当前播放还是混音
    /// </summary>
    /// <param name="clipName">clip名称</param>
    /// <param name="stopFirst">是否先停止当前Source</param>
    public static void PlayUISound(string clipName, bool stopFirst = true)
    {
        if (m_listener == null)
            return;

        var clipPath = "Sounds/2D/" + clipName;
        var source = GetSource(AudioSourceConst.SOUND_UI);
        if (m_audioDic.ContainsKey(clipPath))
            PlayAudioOneShot(source, m_audioDic[clipPath], stopFirst, 1, m_soundVolumn, m_soundMute);
        else
            ResourceManager.LoadAudio(clipPath, clip =>
            {
                if (!m_audioDic.ContainsKey(clipPath))
                    m_audioDic.Add(clipPath, clip);
                PlayAudioOneShot(source, clip, stopFirst, 1, m_soundVolumn, m_soundMute);
            });
    }

    /// <summary>
    /// 循环播放UI音效
    /// </summary>
    /// <param name="clipName">clip名称</param>
    public static void PlayUISoundLoop(string clipName)
    {
        if (m_listener == null)
            return;

        if (string.IsNullOrEmpty(clipName))
            return;

        if (null != GetSource(AudioSourceConst.SOUND_UI_LOOP) && GetSource(AudioSourceConst.SOUND_UI_LOOP).isPlaying)
            return;

        var clipPath = "Sounds/2D/" + clipName;
        ResourceManager.LoadAudio(clipPath, clip =>
        {
            if (clip != null)
            {
                var source = GetSource(AudioSourceConst.SOUND_UI_LOOP);
                source.clip = clip;
                source.volume = m_soundVolumn;
                source.pitch = 1;
                source.loop = true;
                if (!source.isPlaying)
                    source.Play();
            }
        });
    }

    /// <summary>
    /// 停止UI循环音效
    /// </summary>
    public static void StopUISoundLoop()
    {
        if (m_listener == null)
            return;

        var source = GetSource(AudioSourceConst.SOUND_UI_LOOP);
        source.Stop();
    }

    private static void PlayAudioOneShot(AudioSource source, AudioClip clip, bool stopFirst, float volumeScale, float volume, bool mute, float pitch = 1)
    {
        if (source == null || clip == null || source.isActiveAndEnabled == false)
            return;

        if (stopFirst)
            source.Stop();
        source.volume = volume;
        source.mute = mute;
        source.pitch = pitch;
        source.PlayOneShot(clip, volumeScale);
    }

    private static void PlayAudio(AudioSource source, AudioClip clip, bool stopFirst, float volume, bool mute, float pitch = 1)
    {
        if (source == null || clip == null)
            return;
        if (stopFirst)
            source.Stop();
        source.volume = volume;
        source.pitch = pitch;
        source.mute = mute;
        source.clip = clip;
        source.Play();
    }

    /// <summary>
    /// 循环播放背景音乐
    /// </summary>
    /// <param name="clipName">clip名称，留空则根据场景配置随机</param>
    public static void PlayBgMusic(string clipName = null)
    {
        if (m_listener == null)
            return;

        if (string.IsNullOrEmpty(clipName))
        {
            var musics = WorldManager.singleton.backgroundMusics;
            if (musics != null && musics.Length > 0)
                clipName = musics[UnityEngine.Random.Range(0, musics.Length)];
            else
            {
                StopBgMusic();
                return;
            }
        }

        ResourceManager.LoadAudio("Music/"+clipName, clip =>
        {
            if (clip != null)
            {
                m_curBgSourceName = m_curBgSourceName == AudioSourceConst.MUSIC_BACKGROUND1 ? AudioSourceConst.MUSIC_BACKGROUND2 : AudioSourceConst.MUSIC_BACKGROUND1;
                var source = GetSource(m_curBgSourceName);
                source.clip = clip;
                source.volume = m_musicVolume;
                source.pitch = 1;
                source.loop = true;
                if (!source.isPlaying)
                    source.Play();

                m_fadeValue = 0;
                m_isBgCrossFading = true;
            }
        });

    }

    public static void StopBgMusic()
    {
        if (m_listener == null)
            return;

        m_curBgSourceName = m_curBgSourceName == AudioSourceConst.MUSIC_BACKGROUND1 ? AudioSourceConst.MUSIC_BACKGROUND2 : AudioSourceConst.MUSIC_BACKGROUND1;
        var source = GetSource(m_curBgSourceName);
        source.Stop();

        m_fadeValue = 0;
        m_isBgCrossFading = true;
    }


    public static bool IsBgMusicPlaying()
    {
        var source = GetSource(m_curBgSourceName);
        return source.isPlaying;
    }

    public static string GetBGMusicName()
    {
        var source = GetSource(m_curBgSourceName);
        if (source == null || source.clip == null)
        {
            Logger.Error("GetBGMusicName m_curBgSourceName=" + m_curBgSourceName + "   null!!");
            return string.Empty;
        }
        return source.clip.name;
    }


    public static void PlayFightUIVoice(string clipName, bool stopFirst = false, float volumeScale = 1, string sourceKey = "")
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.modelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
            clipName += "_girl";

        sourceKey = string.IsNullOrEmpty(sourceKey) ? AudioSourceConst.SOUND_FIGHT_VOICE : sourceKey;
        PlayFightUISound(clipName, stopFirst, volumeScale, sourceKey);
    }

    /// <summary>
    /// 战斗用
    /// </summary>
    /// <param name="clipName"></param>
    /// <param name="volumeScale"></param>
    /// <param name="stopFirst"></param>
    /// <param name="sourceKey"></param>
    public static void PlayFightUISound(string clipName, bool stopFirst = false, float volumeScale = 1, string sourceKey = "")
    {
        if (m_listener == null)
            return;

        var clipPath = "Sounds/2D/" + clipName;
        var source = GetSource(string.IsNullOrEmpty(sourceKey) ? AudioSourceConst.SOUND_FIGHT_2D : sourceKey);

        if (m_audioDic.ContainsKey(clipPath))
            PlayAudioOneShot(source, m_audioDic[clipPath], stopFirst, volumeScale, m_fightSoundVolumn, false);
        else
            ResourceManager.LoadAudio(clipPath, clip =>
            {
                if (!m_audioDic.ContainsKey(clipPath))
                    m_audioDic.Add(clipPath, clip);
                PlayAudioOneShot(source, clip, stopFirst, volumeScale, m_fightSoundVolumn, false);
            });
    }

    public static void PlayOneShot(AudioSource source, string clipName, float volumeScale = 1, bool stopFirst = false)
    {
        if (m_listener == null)
            return;

        var clipPath = string.Format("{0}{1}", SOUND_DIR, clipName);
        if (m_audioDic.ContainsKey(clipPath))
            PlayAudioOneShot(source, m_audioDic[clipPath], stopFirst, volumeScale, m_fightSoundVolumn, false);
        else
            ResourceManager.LoadAudio(clipPath, clip =>
            {
                if (!m_audioDic.ContainsKey(clipPath))
                    m_audioDic.Add(clipPath, clip);
                PlayAudioOneShot(source, clip, stopFirst, volumeScale, m_fightSoundVolumn, false);
            });
    }

    private const float GLOBAL_SOUND_CD = 0f;   //先取消cd
    private static Dictionary<string, float> m_hitSoundCdDic = new Dictionary<string, float>(); 
    private static Dictionary<string, float> m_hitSoundLastPlaySoundDic = new Dictionary<string, float>(); 
    /// <summary>
    /// 播放子弹命中音效
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="clipName"></param>
    /// <param name="distance"></param>
    /// <param name="volume"></param>
    /// <param name="cdNameKey"></param>
    public static bool PlayHitSound(Vector3 pos, string clipName, float distance, float volume = 1f, string cdNameKey = null)
    {
        if (m_listener == null)
            return false;

        //超过距离的就忽略了
        if (MainPlayer.singleton != null && (MainPlayer.singleton.position-pos).sqrMagnitude > distance*distance)
            return false;

        //cd中也忽略
        if (!string.IsNullOrEmpty(cdNameKey))
        {
            if (!m_hitSoundCdDic.ContainsKey(cdNameKey))
                m_hitSoundCdDic.Add(cdNameKey, GLOBAL_SOUND_CD);
            if (!m_hitSoundLastPlaySoundDic.ContainsKey(cdNameKey))
                m_hitSoundLastPlaySoundDic.Add(cdNameKey, 0);

            var cd = m_hitSoundCdDic[cdNameKey];
            var lastPlayTime = m_hitSoundLastPlaySoundDic[cdNameKey];

            if (Time.time - lastPlayTime < cd)
                return false;
            m_hitSoundLastPlaySoundDic[cdNameKey] = Time.time;                                      //开始cd
            m_hitSoundCdDic[cdNameKey] = GLOBAL_SOUND_CD * UnityEngine.Random.Range(0.6f, 1.4f);    //随机cd值
        }

        var full = true;
        FPSAudioSource source = null;
        for (int i = 0; i < m_hitList.Count; i++)
        {
            if (!m_hitList[i].isPlaying)
            {
                full = false;
                source = m_hitList[i];
                break;
            }
        }

        if (full)
        {
            var goAudio = new GameObject(HIT_POINT);
            var audioSource = goAudio.AddComponent<AudioSource>();
            audioSource.maxDistance = distance;
            audioSource.rolloffMode = AudioRolloffMode.Linear;

            var fas = new FPSAudioSource(audioSource, goAudio);
            fas.transform.SetParent(m_audioTran);
            m_hitList.Add(fas);

            source = fas;
        }

        source.position = pos;
        var clipPath = SOUND_DIR + clipName;
        if (m_audioDic.ContainsKey(clipPath))
            PlayAudio(source.audioSource, m_audioDic[clipPath], false, m_fightSoundVolumn * volume, false, UnityEngine.Random.Range(0.8f, 1.2f));
        else
            ResourceManager.LoadAudio(clipPath, clip =>
            {
                if (!m_audioDic.ContainsKey(clipPath))
                    m_audioDic.Add(clipPath, clip);
                PlayAudio(source.audioSource, clip, false, m_fightSoundVolumn * volume, false, UnityEngine.Random.Range(0.8f, 1.2f));
            });
        return true;
    }

    /// <summary>
    /// 获取或者创建指定key的AudioSource
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private static AudioSource GetSource(string key)
    {
        if (!m_audioSourceDic.ContainsKey(key))
        {
            var audioSource = m_audioTran.gameObject.AddComponent<AudioSource>();
            m_audioSourceDic.Add(key, audioSource);
        }

        return m_audioSourceDic[key];
    }

    public static void Clear()
    {
        m_hitSoundCdDic.Clear();
        m_hitSoundLastPlaySoundDic.Clear();
        m_audioDic.Clear();
    }

    public static bool MusicMute
    {
        set
        {
            m_musicMute = value;
            GetSource(AudioSourceConst.MUSIC_BACKGROUND1).mute = m_musicMute;
            GetSource(AudioSourceConst.MUSIC_BACKGROUND2).mute = m_musicMute;
        }
        get { return m_musicMute; }
    }
    public static float MusicVolume
    {
        set
        {
            m_musicVolume = value;
            GetSource(AudioSourceConst.MUSIC_BACKGROUND1).volume = m_musicVolume;
            GetSource(AudioSourceConst.MUSIC_BACKGROUND2).volume = m_musicVolume;
        }
        get { return m_musicVolume; }
    }

    public static bool SoundMute
    {
        set
        {
            m_soundMute = value;
            GetSource(AudioSourceConst.SOUND_UI).mute = m_soundMute;
            GetSource(AudioSourceConst.SOUND_UI_LOOP).mute = m_soundMute;
        }
        get { return m_soundMute; }
    }
    public static float SoundVolume
    {
        set
        {
            m_soundVolumn = value;
            GetSource(AudioSourceConst.SOUND_UI).volume = m_soundVolumn;
            GetSource(AudioSourceConst.SOUND_UI_LOOP).volume = m_soundVolumn;
        }
        get { return m_soundVolumn; }
    }

    public static float FightSoundVolume
    {
        set { m_fightSoundVolumn = value; }
        get { return m_fightSoundVolumn; }
    }
}

class FPSAudioSource
{
    public AudioSource audioSource;
    public GameObject gameObject;
    public Transform transform;

    public FPSAudioSource(AudioSource kAS, GameObject go)
    {
        audioSource = kAS;
        gameObject = go;
        transform = go.transform;
    }

    public void PlayOneShot(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public bool isPlaying
    {
        get { return audioSource.isPlaying; }
    }

    public float volume
    {
        set { audioSource.volume = value; }
    }

    public Vector3 position
    {
        set { transform.position = value; }
    }
}


