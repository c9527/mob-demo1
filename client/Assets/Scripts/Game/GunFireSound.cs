﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]
public class GunFireSound : MonoBehaviour
{
    public List<AudioClip> m_audioClipList = new List<AudioClip>();
    public List<int> m_probabilityList = new List<int>(); 
    public int m_minSpawn = 100;
    public int m_maxSpawn = 100;

    private AudioSource m_kAS;
    private bool m_playingGunFireSound;
    private float m_time;
    private float m_nextTime;

    private void Start()
    {
        m_kAS = gameObject.GetComponent<AudioSource>();
    }

    public void BeginGunFireSound()
    {
        if (m_playingGunFireSound || m_audioClipList.Count == 0)
            return;

        m_playingGunFireSound = true;
        m_kAS.maxDistance = 500;

        m_time = Time.time;
        m_nextTime = m_time + Random.Range(m_minSpawn, m_maxSpawn)*0.001f;

        PlayGunFireSound();
    }

    public void EndGunFireSound()
    {
        m_playingGunFireSound = false;
    }

    private void PlayGunFireSound()
    {
        if (m_audioClipList.Count == 0)
        {
            m_playingGunFireSound = false;
            return;
        }

        float r = Random.value;
        float prop = 0;
        AudioClip audioClip = null;
        for (int i = 0; i < m_probabilityList.Count; i++)
        {
            prop += m_probabilityList[i]*0.01f;
            if (r < prop)
            {
                audioClip = m_audioClipList[i];
                break;
            }
        }
        m_kAS.PlayOneShot(audioClip);
    }

    public void Update()
    {
        if (m_playingGunFireSound)
        {
            m_time += Time.deltaTime;
            if (m_time >= m_nextTime)
            {
                PlayGunFireSound();
                m_nextTime = m_nextTime + Random.Range(m_minSpawn, m_maxSpawn)*0.001f;
            }
        }
    }
}
