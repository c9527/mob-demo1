﻿using System.Collections.Generic;

/// <summary>
/// 权限管理类
/// </summary>
public static class RightManager
{
    public const string USER_HERO_CARD_SUPER = "user_hero_card_super";
    public const string USER_HERO_CARD_LEGEND = "user_hero_card_legend";
    public const string USER_SYSTEM = "user_system";

    public const string RIGHT_MOTION_DOU_WA = "right_motion_dou_wa";

    private class User
    {
        public string Name;
        public Dictionary<string, Right> RightDic = new Dictionary<string, Right>();
    }

    private class Right
    {
        public string Name;
        public int Level;
        public bool Enable { get { return Level > 0; }}
    }

    private static Dictionary<string, User> m_userDic = new Dictionary<string,User>();

    public static void Init()
    {
        m_userDic.Clear();
        Add(USER_HERO_CARD_SUPER, RIGHT_MOTION_DOU_WA);
        Add(USER_HERO_CARD_LEGEND, RIGHT_MOTION_DOU_WA);
    }

    public static bool Has(string userName, params string[] rights)
    {
        if (userName == USER_SYSTEM)
            return true;

        if (userName == null || !m_userDic.ContainsKey(userName))
            return false;

        var user = m_userDic[userName];
        var rightDic = user.RightDic;
        for (int i = 0; i < rights.Length; i++)
        {
            if (!rightDic.ContainsKey(rights[i]) || !rightDic[rights[i]].Enable)
                return false;
        }
        return true;
    }

    private static void Add(string userName, params string[] rights)
    {
        User user;
        if (!m_userDic.ContainsKey(userName))
        {
            user = new User() {Name = userName};
            m_userDic.Add(user.Name, user);
        }
        else
            user = m_userDic[userName];
        

        for (int i = 0; i < rights.Length; i++)
        {
            var rightName = rights[i];
            Right right;
            if (!user.RightDic.ContainsKey(rightName))
            {
                right = new Right() {Level = 1, Name = rightName};
                user.RightDic.Add(rightName, right);
            }
        }
    }
}