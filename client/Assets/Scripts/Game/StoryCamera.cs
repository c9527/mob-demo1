﻿using UnityEngine;
using System.Collections;
using System;

public class StoryCamera : MonoBehaviour 
{
    public const string STORYCAMERA_ANI_LOADED = "storycamera_ani_loaded";

    private GameObject m_go;
    private Camera m_camera;
    private Transform m_trans;
    private PosTweener m_posTweener;
    private RotTweener m_rotTweener;

    private CameraShake m_cameraShake;

    private FPSAnimation m_ani;

    void Awake()
    {
        m_go = gameObject;
        m_trans = m_go.transform;

        m_camera = m_go.AddComponent<Camera>();
        m_camera.orthographic = false;
        m_camera.fieldOfView = 45;
        m_camera.cullingMask = ~(GameSetting.LAYER_MASK_DEFAULT |GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_UI | GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND);
        m_camera.depth = GameSetting.CAMERA_DEEPTH_SCENECAMERA;
        //QualityManager.InitSceneCamera(m_camera);

        m_go.AddMissingComponent<SceneCameraController>();

        m_cameraShake = m_go.AddMissingComponent<CameraShake>();
        m_cameraShake.AddCamera(m_camera);
        if (MainPlayer.singleton != null && MainPlayer.singleton.cameraMainPlayer != null)
            m_cameraShake.AddCamera(MainPlayer.singleton.cameraMainPlayer, true, MainPlayer.singleton.cameraShaker.transform.parent);

        m_posTweener = m_go.AddMissingComponent<PosTweener>();
        m_rotTweener = m_go.AddMissingComponent<RotTweener>();

        Animation ani = null;
        if(m_trans.parent != null)
            ani = m_trans.parent.gameObject.AddMissingComponent<Animation>();
        else

        ani = m_go.AddMissingComponent<Animation>();
        m_ani = new FPSAnimation();
        m_ani.SetAnimation(ani);
    }

    public void SetActive(bool value)
    {
        m_go.TrySetActive(value);
    }

    public void SetParent(Transform parent)
    {
        m_trans.SetParent(parent);
        if(GameSetting.enableCameraLog)
        {
            Logger.Warning(StackTraceUtility.ExtractStackTrace() + "\n Parent = " + (parent != null ? parent.root.name : "null"));
        }
    }

    public void ResetLocalTransform()
    {
        m_trans.ResetLocal();
    }

    public void LookAt(Vector3 targetPos)
    {
        m_trans.LookAt(targetPos);
    }

    public void RotateAround(Vector3 tragetPos, Vector3 axis, float angle)
    {
        m_trans.RotateAround(tragetPos, axis, angle);
    }

    public void Rotate(float xAngle, float yAngle, float zAngle, Space relativeTo)
    {
        m_trans.Rotate(xAngle, yAngle, zAngle, relativeTo);
    }

    public Vector3 WorldToScreenPoint(Vector3 pos)
    {
        return m_camera.WorldToScreenPoint(pos);
    }

    public Vector3 ScreenToWorldPoint(Vector3 pos)
    {
        return m_camera.ScreenToWorldPoint(pos);
    }

    public void StartShake(ConfigShakeLine config)
    {
        if (config != null)
        {
            StartShake(config.Time, config.HorizontalLevel, config.HorizontalDecay, config.HorizontalRandom == 1, config.VerticalLevel, config.VerticalDecay, config.VerticalRandom == 1, config.Frequency, config.MainCameraLevelScale);
        }
    }

    public void StartShake(float shakeTime, float horizontalLevel, float horizontalDecay, bool horizontalRandom, float verticalLevel, float verticalDecay, bool verticalRandom, float frequency, float mainCameraLevelScale)
    {
        if(m_cameraShake != null)
        {
            //if (parent == null)
            //    m_cameraShake.AddCamera(m_camera);
            if (shakeTime > 0)
                m_cameraShake.StartShake(shakeTime, horizontalLevel, horizontalDecay, horizontalRandom, verticalLevel, verticalDecay, verticalRandom, frequency, mainCameraLevelScale, false);
        }
    }

    public void StopShake()
    {
        if (m_cameraShake != null)
        {
            m_cameraShake.StopShake();
        }
    }

    public void MoveTo(Vector3 pos, float duration)
    {
        if(m_posTweener != null)
        {
            m_posTweener.TweenPos(duration, pos);
        }
    }

    public void RotateTo(Vector3 rot, float duration)
    {
        if (m_rotTweener != null)
        {
            m_rotTweener.TweenRot(duration, rot);
        }
    }

    #region 播放动画
    public void PlayAni(string aniName)
    {
        if (m_ani != null)
        {
            //if (m_ani.GetClip(name) == null)
            //{
            //    Logger.Error("StoryCamera PlayAni err, Ani is null, aniName=" + aniName);
            //    return;
            //}

            m_ani.Play(aniName);
        }
    }

    public float GetAniTime(string aniName)
    {
        if(m_ani != null)
        {
            var clip = m_ani.GetClip(aniName);
            if (clip != null)
                return clip.length / GameSetting.FRAME_RATE;
        }

        return -1f;
    }

    public void AddAni(string aniName)
    {
        if (string.IsNullOrEmpty(aniName) == false && m_ani.GetClip(aniName) == null)
        {
            string strPath = PathHelper.GetCameraAni(aniName);
            ResourceManager.LoadAnimation(strPath, (kAniClip) =>
            {
                if (m_ani != null && kAniClip != null)
                {
                    m_ani.AddClip(kAniClip, aniName);
                    GameDispatcher.Dispatch<string>(STORYCAMERA_ANI_LOADED, aniName);
                }
            });
        }
        else
        {
            GameDispatcher.Dispatch<string>(STORYCAMERA_ANI_LOADED, aniName);
        }
    }

    #endregion

    #region Property

    public bool enableCamera
    {
        set 
        {
            m_camera.enabled = value;
        }
    }

    public Vector3 forward
    {
        get { return m_trans.forward; }
        set { m_trans.forward = value; }
    }

    public Vector3 right
    {
        get { return m_trans.right; }
    }

    public Vector3 position
    {
        get { return m_trans.position; }
        set
        {
            m_trans.position = value;
        }
    }

    public Vector3 localPosition
    {
        get { return m_trans.localPosition; }
    }

    public Vector3 eulerAngles
    {
        get { return m_trans.eulerAngles; }
        set
        {
            m_trans.eulerAngles = value;
        }
    }

    public Quaternion localRotation
    {
        set
        {
            m_trans.localRotation = value;
        }
    }

    public Transform parent
    {
        get { return m_trans == null ? null : m_trans.parent; }
    }    

    public Camera camera
    {
        get { return m_camera; }
    }

    public float fieldOfView
    {
        get { return m_camera.fieldOfView; }
        set
        {
            m_camera.fieldOfView = value;
        }
    }

    public bool useOcclusionCulling
    {
        get { return m_camera.useOcclusionCulling; }
        set {
            if (m_camera != null && m_camera.useOcclusionCulling != value)
                m_camera.useOcclusionCulling = value; 
        }
    }

    public CameraShake cameraShake
    {
        get { return m_cameraShake; }
    }

#endregion
  

   








	 
}
