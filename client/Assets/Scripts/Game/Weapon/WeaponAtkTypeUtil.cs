﻿using System;
using System.Collections.Generic;
using System.Reflection;


/// <summary>
/// 武器攻击类型
/// </summary>
class Ref_WeaponAtkType
{
    public const int WEAPON1 = 0;
    public const int WEAPON2 = 3;
    public const int DAGGER = 6;
    public const int GRENADE = 9;
    public const int FLASHBOMB = 12;
    public const int SKILLWEAPON = 15;
    public const int MAX_COUNT = SKILLWEAPON + 3;
}

/// <summary>
/// 武器攻击类型工具类
/// </summary>
class WeaponAtkTypeUtil
{
    private static Dictionary<string, int> m_dicWeaponAtkTypes;

    static WeaponAtkTypeUtil()
    {
        m_dicWeaponAtkTypes = new Dictionary<string,int>();
        Type t = typeof(Ref_WeaponAtkType);
        FieldInfo[] fis = t.GetFields(BindingFlags.Static| BindingFlags.Public);
        foreach(var fi in fis)
        {
            m_dicWeaponAtkTypes[fi.Name] = (int)fi.GetValue(null);
        }
    }

    static public int GetWeaponAtkType(string weaponSubType, bool useAdv = false, bool isSub = false)
    {
        int result = 0;
        m_dicWeaponAtkTypes.TryGetValue(weaponSubType, out result);
        result += useAdv ? 1 : 0;
        result += isSub ? 2 : 0;
        return result;
    }

    static public bool IsMainWeaponType(int atkType)
    {
        return atkType % 3 == 0;
    }

    static public bool IsSubWeaponType(int atkType)
    {
        return atkType % 3 == 2;
    }

    static public bool IsAdvWeaponType(int atkType)
    {
        return atkType % 3 == 1;
    }

}
