﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;



public class MonoBulletArrow : MonoBulletBase
{



    public override void Fire(Vector3 startPos, Vector3 startRot, Vector3 fireDir, ConfigItemBulletLine cb, BasePlayer player, float Aspeed = 0)
    {
        WorldManager.singleton.DisanbleTeamMateCollider(false);
        WorldManager.singleton.EnableRoleBodyPartCollider(true);
        WorldManager.singleton.EnableRoleBigCollider(false);
        WorldManager.singleton.EnableRoleBodyTrigger(false);
        base.Fire(startPos, startRot, fireDir, cb, player, Aspeed);
        //m_trans.eulerAngles += new Vector3(0, 90, 0);
        //WorldManager.singleton.EnableRoleBodyPartCollider(true);
        //m_trans.forward = fireDir;
    }

    
    

    override protected void DoDamage(BasePlayer bp = null)
    {
        if (m_isMainPlayer == false)    // 只有主角扔的，才告诉服务器我炸了哪些人，如果是第三人称扔的，就不处理，服务器会单独通知伤害
            return;

        List<p_bowhit_info_toc> list = new List<p_bowhit_info_toc>();
        //Dictionary<int, BasePlayer> dicAllPlayer = WorldManager.singleton.allPlayer;

        Vector3 vec3ExplodePos = gameObject.transform.position;
        //foreach (KeyValuePair<int, BasePlayer> kvp in dicAllPlayer)
        //{
        //    BasePlayer player = kvp.Value;

        //    if (WeaponBase.CanAttack(player) == false)
        //        continue;

        //    float distance = (player.position - vec3ExplodePs).sqrMagnitude;
        //    Logger.Error(distance.ToString());
        //    if (distance <= m_cb.DamageMaxR * m_cb.DamageMaxR &&
        //        distance >= m_cb.DamageMinR * m_cb.DamageMinR)
        //    {
        //        p_hitg_info_toc p = new p_hitg_info_toc
        //        {
        //            id = player.PlayerId,
        //            pos = player.position
        //        };
        //        list.Add(p);
        //    }
        //}
        if (bp == null)
        {
            return;
        }

        Vector3 headpos = bp.headPos;
        float distance = ((vec3ExplodePos - headpos)).magnitude;

        int partIndex = 0;
        Debug.LogError(distance.ToString());
        if (distance*distance <= m_cb.HeadShotRange * m_cb.HeadShotRange)
        {
            partIndex = 1;
            Logger.Error("爆头了");
        }

        p_bowhit_info_toc p = new p_bowhit_info_toc
        {
            id = bp.PlayerId,
            pos = bp.position,
            bodyPart = partIndex
        };
        list.Add(p);

        NetLayer.Send(new tos_fight_bowfire()
        {
            fromPos = vec3ExplodePos,
            hitList = list.ToArray(),
            index = m_bullet.bulletIndex,
            firePos = m_playerFirePos
        });
        WorldManager.singleton.DisanbleTeamMateCollider(true);
        WorldManager.singleton.EnableRoleBodyPartCollider(false);
        WorldManager.singleton.EnableRoleBigCollider(true);
        WorldManager.singleton.EnableRoleBodyTrigger(true);
        startFrame = 0;
        //WorldManager.singleton.EnableRoleBodyPartCollider(false);
    }


    public int startFrame = 0;
    override protected void FixedUpdate()
    {
        if (m_bStarted == false)
            return;

        base.FixedUpdate();

            Vector3 speed = Vector3.Normalize(m_rd.velocity);
            if (speed != Vector3.zero)
                m_trans.forward = speed;
            
            //m_trans.eulerAngles += new Vector3(0, 90, -90);
        //Debug.LogError(m_rd.velocity);
        //startFrame++;
    }
    
}

