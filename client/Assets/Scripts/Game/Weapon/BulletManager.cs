﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BulletManager
{
    private static Dictionary<string, List<BulletBase>> m_dicBullet = new Dictionary<string, List<BulletBase>>();

    static public BulletBase CreateBullet(string type, ConfigItemWeaponLine cw, bool isFpv)
    {
        if (cw == null)
            return null;

        string model = "";
        if(cw.Class == WeaponManager.WEAPON_TYPE_RIFLEGRENADE||cw.Class ==WeaponManager.WEAPOM_TYPE_BOW )
            model = isFpv ? cw.MPModel2 : cw.OPModel2;
        else
            model = isFpv ? cw.MPModel : cw.OPModel;

        List<BulletBase> listBB = null;
        m_dicBullet.TryGetValue(type, out listBB);

        if (listBB == null)
        {
            listBB = new List<BulletBase>();
            m_dicBullet.Add(type, listBB);
        }
        
        BulletBase bb = null;
        for(int i=0; i<listBB.Count; i++)
        {
            BulletBase tmp = listBB[i];
            if (tmp.reclaimed == true && tmp.model == model)
            {
                bb = tmp;
                break;
            }
        }

        if(bb == null)
        {
            switch(type)
            {
                case BulletBase.TYPE_RIFLEGRENADE:
                case BulletBase.TYPE_GRENADE:
                case BulletBase.TYPE_FLASHBOM:
                case BulletBase.TYPE_DEJECTILE:
                    bb = new BulletBase(type);
                    break;
                case BulletBase.TYPE_SMOKEBOM:
                    break;
                case BulletBase.TYPE_ARROW:
                    bb = new BulletBase(type);
                    break;
            }
        }

        if(bb != null)
        {
            listBB.Add(bb);
            bb.reclaimed = false;
        }
        return bb;
    }

    static public void Release()
    {
        foreach(KeyValuePair<string, List<BulletBase>> kvp in m_dicBullet)
        {
            List<BulletBase> listBB = kvp.Value;
            for(int i=0; i<listBB.Count; i++)
            {
                BulletBase bb = listBB[i];
                bb.Release();
            }

            listBB.Clear();
        }
    }
}
