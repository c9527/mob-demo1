﻿using UnityEngine;
using System.Collections;
using System;


public class BulletBase 
{
    public const string TYPE_RIFLEGRENADE = "riflegrenade";
    public const string TYPE_GRENADE = "grenade";
    public const string TYPE_FLASHBOM = "flashbom";
    public const string TYPE_SMOKEBOM = "smokebom";
    public const string TYPE_DEJECTILE = "dejectile"; //投射物（石头等）
    public const string TYPE_ARROW = "arrow";//箭矢

    private string m_type;
    private bool m_reclaimed;
    private string m_model; // 同是手雷，也会有不同模型，普通，高爆等。然后还会再分第一人称，第三人称

    protected GameObject m_go;
    protected Transform m_trans;
    protected MonoBulletBase m_monoBB;

    protected ConfigItemWeaponLine m_cw;
    protected ConfigItemBulletLine m_cb;
    protected Vector3 m_startPos;
    protected Vector3 m_fireDir;
    protected bool m_isMainPlayer;
    protected int m_bulletIndex;
    protected float m_scale;
    protected Vector3 m_srcScale;

    public BulletBase(string type)
    {
        m_type = type;
    }

    virtual protected void Reset()
    {

    }

    virtual public void Release()
    {
        ResourceManager.Destroy(m_go);
    }

    virtual public void Fire(Vector3 startPos, Vector3 startRot, Vector3 fireDir, ConfigItemWeaponLine wl, BasePlayer player, int bulletIndex,float Aspeed = 0)
    {
        Reset();

        m_cw = wl;
        m_startPos = startPos;
        m_fireDir = fireDir;
        m_isMainPlayer = player is MainPlayer;
        m_bulletIndex = bulletIndex;
        m_scale = player.scale;

        m_cb = ConfigManager.GetConfig<ConfigItemBullet>().GetLine(wl.AmmoID);
        if (m_cb == null)
        {
            Logger.Error("找不到ConfigBulletLine " + wl.ID + " " + wl.AmmoID);
            Reclaim();
            return;
        }

        LoadRes((ok) =>
        {
            if (ok == false)
                Reclaim();
            else
            {
                m_monoBB.Fire(startPos, startRot, fireDir, m_cb, player,Aspeed);
            }
        });
    }

    private void LoadRes(Action<bool> callBack)
    {
        if(m_go != null)
        {
            m_trans.localScale = m_srcScale * m_scale;
            callBack(true);
            return;
        }

        string strModelName = "";

        if (m_cw.Class == WeaponManager.WEAPON_TYPE_RIFLEGRENADE||m_cw.Class == WeaponManager.WEAPOM_TYPE_BOW)
            strModelName = m_isMainPlayer ? m_cw.MPModel2 : m_cw.OPModel2;
        else
            strModelName = m_isMainPlayer ? m_cw.MPModel : m_cw.OPModel;
        string strPath = PathHelper.GetWeaponPrefabPath(strModelName);
        m_model = strModelName;
        ResourceManager.LoadBattlePrefab(strPath, (go, crc) =>
        {
            if (go == null)
            {
                callBack(false);
                return;
            }
             
            SetGO(go);
            callBack(true);
        }, EResType.Battle, false, false);
    }

    private void SetGO(GameObject go)
    {
        m_go = go;
        m_trans = go.transform;

        m_srcScale = m_trans.localScale;
        m_trans.localScale = m_srcScale * m_scale;

        switch(m_type)
        {
            case TYPE_RIFLEGRENADE:
            case TYPE_GRENADE:
                m_monoBB = go.AddMissingComponent<MonoBulletGrenade>();
                break;
            case TYPE_FLASHBOM:
                m_monoBB = go.AddMissingComponent<MonoBulletFlashBomb>();
                break;
            case TYPE_SMOKEBOM:
                break;
            case TYPE_DEJECTILE:
                m_monoBB = go.AddMissingComponent<MonoBulletProjectile>();
                break;
            case TYPE_ARROW:
                m_monoBB = go.AddMissingComponent<MonoBulletArrow>();
                break;
        }

        if (m_monoBB != null)
            m_monoBB.Init(this);

        Reclaim();
    }

    public void Explode()
    {
        Reclaim();
    }

    private void Reclaim()
    {
        reclaimed = true;

        if(m_trans != null)
        {
            m_trans.SetParent(SceneManager.singleton.reclaimRoot);
            m_trans.localPosition = Vector3.zero;
            m_trans.localScale = m_srcScale;
        }
    }

    public bool reclaimed
    {
        get { return m_reclaimed; }
        set { m_reclaimed = value; }
    }

    public string model
    {
        get { return m_model; }
    }

    public int bulletIndex
    {
        get { return m_bulletIndex; }
    }

    public ConfigItemWeaponLine weaponConfig
    {
        get { return m_cw; }
    }

    public GameObject gameObject
    {
        get { return m_go; }
    }

    public Transform transform
    {
        get { return m_trans; }
    }
}
