﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Object = System.Object;


public class WeaponBase
{
    
#region Const

    public const string WEAPON_TEST_UNKNOW = "weapon_test_unknow";
    public const string WEAPON_TEST_SUCCESS = "weapon_test_succes";
    public const string WEAPON_TEST_NO_AMMO = "weapon_test_no_ammo";
    public const string WEAPON_TEST_NEED_RELOAD = "weapon_test_need_reload";
    public const string WEAPON_TEST_ZOOMINOUT = "weapon_test_zoominout";

    public const int WEAPON_FIRE_TYPE_UNKNOW = 1;//"weapon_fire_type_unknow";
    public const int WEAPON_FIRE_TYPE_ONESHOT = 2;//"weapon_fire_type_oneshot";
    public const int WEAPON_FIRE_TYPE_STRAFE = 3;//"weapon_fire_type_strafe";  

    public const int FIRE_MODE_SINGLETIME = 1;
    public const int FIRE_MODE_BURST = 2;
    public const int FIRE_MODE_RAPID = 3;

    public const int FIRE_COST_AMMO_NUM_BURST = 3;
    public const float FIRE_BUILDING_HIT_EFFECT_RANDOM_RANGE = 0.1f;

    public const float AIM_POINT_LENGTH = 3.0f;

    public const int WEAPON_SLOTTYPE_RIGHTHAND = 0;
    public const int WEAPON_SLOTTYPE_LEFTHAND = 1;
    public const int WEAPON_SLOTTYPE_BOTHHAND = 2;
    public const int WEAPON_SLOTTYPE_NOSLOT = 3;

    public const float FIRE_ONE_SHOT_INVERTAL_TIME = 0.15f;     // 间隔内为单射，否则为扫射
    public const float FIRE_ANI_DEFAULT_LENGTH = 0.2f;
    public const float FIRE_SHOT_BREAK_TIME = 2f;               // 连续播放攻击动画中断时间

    protected const int GUN_MAX_HIT_NUM = 10;                  // 射击一次，击中的最多目标数，防止发送消息过大
    protected const int SHOTGUN_MAX_HIT_NUM = 3;
#endregion

    protected BasePlayer m_kPlayer;
    protected WeaponManager m_weaponMgr;

    protected LocalWeaponData m_kLWD;
    protected ConfigItemWeaponLine m_kWL;
    protected ConfigItemBulletLine m_kBL;
    protected GameObject m_kGO;
    protected GameObject m_kGO2;
    protected Transform m_kTrans;
    protected Transform m_kTrans2;
    protected ModelComponentCache m_mcc1;
    protected ModelComponentCache m_mcc2;


    protected SoundPlayer m_kSP;
    protected string m_model;

    protected WeaponSpread m_kSpread;

    protected Transform m_transParentSetted;
    protected int m_iActiveSetted = 0;  // 1 activated == true; -1 activate == false;

    protected Ray m_aimRay;
    protected EptInt m_fireType;
    protected bool m_loadResFlg;
    protected bool m_released;
    protected bool m_isSubWeapon;
    protected bool m_noCacheGo;

    private GameObject m_goEffect1;
    private GameObject m_goEffectLeft1;
    private VisibilityManager m_kVM;

    protected VtfFloatCls m_lastFireTime = new VtfFloatCls();
    protected EptFloat m_fFireKeyPressTime;
    protected bool m_bFirePressed;

    protected EptFloat m_shareFireCd;

    protected bool m_bIsFireBefore;

    protected bool m_bIsSkillFire;
    protected int m_iFireSkillTimerId;
    protected Action m_fireCallBack;

    //武器觉醒
    protected bool m_bUseawakenFireLineColor;
    //protected Color m_awakenFireColor;
    protected Color m_awakenFireLineColor = Color.white;
    //protected string m_awakenFireLineEffect;  //觉醒后火线特效
    protected string m_awakenFireEffect;      //觉醒后枪火特效
    protected List<Renderer> m_lstRenderer;
    protected List<Renderer> m_lstRenderer2;

    protected bool m_bIsWalkWaggle;

    protected int m_kActiveSkillAwakenId = -1;                //当前主动技能的觉醒ID
    protected bool m_activeSkillIsRevive;
    protected float m_flastTime = 0;
    protected bool m_bIsEmptyModel;

    protected Dictionary<string,Transform> m_fireModelMountEffect;
    protected Dictionary<string, Transform> m_fireWeaponMountEffect;

    protected Behaviour[] m_arrBehaviour;
    protected Behaviour[] m_arrBehaviour2;

    protected Transform m_transBipRoot = null;
    protected Transform m_transBipRoot2 = null;
    protected Transform m_transSlot = null;

    protected Vector3 m_defaultScale = Vector3.one;
    protected Vector3 m_defaultScale2 = Vector3.one;

    protected WeaponBase m_relateWeapon;
    protected bool m_isHeal;

    static public bool CanAttack(BasePlayer target)
    {
        if (target == null || target.released || !target.alive || target.status.god)
            return false;

        return true;
    }

    public WeaponBase()
    {
        m_kSP = new SoundPlayer();
        m_kVM = new VisibilityManager();
        m_lstRenderer = new List<Renderer>();
        m_lstRenderer2 = new List<Renderer>();
        m_fireModelMountEffect = new Dictionary<string,Transform>();
        m_fireWeaponMountEffect = new Dictionary<string, Transform>();
        
    }

    virtual public void Reset()
    {
        m_bFirePressed = false;
        m_bIsFireBefore = false;
        m_kVM.SetVisable(m_kVM.visableDegree, 1f,false);
    }

    /// <summary>
    ///  ！！！ 注意，内部构造的对象，要在Release中Reset或clear，但不要置为null
    ///  因为WeaponObj是回收使用的,回收使用的时候，这些成员对象不会被实例化，否则会报空
    /// </summary>
    public virtual void Release()
    {
        m_released = true;

        if (gameObject != null && m_noCacheGo == false)
        {
            WeaponManager.CacheWeaponGO(gameObject.name, gameObject);
        }

        if (isBothHandSlot && gameObject2 != null && m_noCacheGo == false)
            WeaponManager.CacheWeaponGO(gameObject2.name, gameObject2);

        m_kPlayer = null;
        m_weaponMgr = null;
        m_kLWD = null;
        m_kWL = null;
        m_kBL = null;
        m_kGO = null;
        m_kGO2 = null;
        m_kTrans = null;
        m_kTrans2 = null;
        m_mcc1 = null;
        m_mcc2 = null;
    
        m_kSP.Release();
        m_transParentSetted = null;
        m_iActiveSetted = 0;
        m_loadResFlg = false;
        m_isSubWeapon = false;
        

        m_goEffect1 = null;
        m_goEffectLeft1 = null;

        m_kVM.Release();
        m_lastFireTime.val = 0;
        m_fFireKeyPressTime = 0;
        m_bFirePressed = false;
        m_shareFireCd.val = 0;
        m_bIsFireBefore = false;
        m_bIsSkillFire = false;
        m_iFireSkillTimerId = 0;
        m_fireCallBack = null;

        m_bUseawakenFireLineColor = false;
        m_awakenFireLineColor = Color.white;
        m_awakenFireEffect = null;
        //m_lstRenderer = null;
        m_lstRenderer.Clear();
        //m_lstRenderer2 = null;
        m_lstRenderer2.Clear();
        m_bIsWalkWaggle = false;
        m_kActiveSkillAwakenId = -1;
        m_activeSkillIsRevive = false;
        m_flastTime = 0;
        m_bIsEmptyModel = false;

        m_fireModelMountEffect.Clear();
        m_fireWeaponMountEffect.Clear();

        m_arrBehaviour = null;
        m_arrBehaviour2 = null;

        m_transBipRoot = null;
        m_transBipRoot2 = null;
        m_transSlot = null;

        m_defaultScale = Vector3.one;
        m_defaultScale2 = Vector3.one;

        m_noCacheGo = false;
        //m_hitCallBack = null;
        //m_atkCallBack = null;

        WeaponManager.CacheWeaponObj(this);
    }

    virtual public void Update()
    {
        m_kVM.Update();

        if (Time.time - m_flastTime >= 1f)
        {
            m_flastTime = Time.time;
            if (m_kPlayer != null && m_kPlayer.isAlive && m_bIsWalkWaggle && m_kPlayer.playerAniController.IsCurrentAni(m_kWL.OPIdleAni, GameConst.PLAYER_UPBODY_LAYER_INDEX))
            {
                m_kPlayer.playerAniController.EnableWalkWaggleLayer(true);
            }
        }

    } 

    virtual public void StopFire()
    {
        m_bFirePressed = false;
        m_bIsFireBefore = false;
        m_bIsSkillFire = false;
        m_kPlayer.StopFire();
        if (m_iFireSkillTimerId != -1)
        {
            TimerManager.RemoveTimeOut(m_iFireSkillTimerId);
            m_iFireSkillTimerId = -1;
        }
    }

    //public void SetCallBack(Action<WeaponBase, CDict> atkCallBack, Action<WeaponBase, CDict> hitCallBack)
    //{
    //    m_hitCallBack = hitCallBack;
    //    m_atkCallBack = atkCallBack;
    //}

    public void Aim(Ray ray)
    {
        m_aimRay = ray;
    }

    public void BindPlayer(BasePlayer kPlayer)
    {
        m_kPlayer = kPlayer;
        m_weaponMgr = kPlayer.weaponMgr;
        m_bIsWalkWaggle = m_kPlayer is OtherPlayer && m_kPlayer.modelSex == ModelConst.MODEL_PLAYER_BOY_SEX && m_kPlayer.playerAniController.animator != null
                && m_kPlayer.playerAniController.animator.GetLayerName(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX) == GameConst.OTHERPLAYER_WALKWAGGLE_LAYER_NAME;
    }

    virtual public void SetData(ConfigItemWeaponLine kWL)
    {
        m_kWL = kWL;
        m_kBL = ConfigManager.GetConfig<ConfigItemBullet>().GetLine(m_kWL.AmmoID);

    }

    public void UpdateLocalWeaponData(LocalWeaponData kLWD)
    {
        // 死亡后，当前的LocalWeaponData会被清除，在Revive后会重新创建，所以这里要重新引用下
        m_kLWD = kLWD;
    }

    public virtual void LoadRes(Action<WeaponBase, bool> funLoaded = null, string emptyGOName = null)
    {
        WeaponBase weapon = this;

        if (m_loadResFlg == false && gameObject == null)
        {
            m_loadResFlg = true;

            Action<GameObject> callback2 = (go2) =>
            {
                if (m_released)
                {
                    if (go2 != null)
                        GameObject.Destroy(go2);
                    return;
                }

                if (go2 == null)
                    Logger.Error("Weapon LoadRes, go2 is null, WeaponID: " + m_kWL.ID);
                else
                {
                    SetGO2(go2);
                }

                if (funLoaded != null)
                    funLoaded(weapon, go2 != null);
            };

            Action<GameObject> callback = (go) =>
            {
                if (m_released)
                {
                    if (go != null)
                        GameObject.Destroy(go);
                    return;
                }

                if (go == null)
                    Logger.Error("Weapon LoadRes, go is null, WeaponID: " + m_kWL.ID);
                else
                    SetGO(go);

                if (isBothHandSlot) //双手加载
                {
                    string model2 = isFPV ? m_kWL.MPModel2 : m_kWL.OPModel2;
                    LoadRes(model2, callback2);
                }
                else
                {
                    if (funLoaded != null)
                        funLoaded(weapon, go != null);
                }
            };

            if(string.IsNullOrEmpty(emptyGOName))
            {
                string model = isFPV ? m_kWL.MPModel : m_kWL.OPModel;
                m_model = model;
                LoadRes(model, callback);
            }
            else
            {
                LoadRes(emptyGOName, callback, true);
            }
        }
        else if (gameObject != null)
        {
            funLoaded(weapon, true);
        }
    }

    protected void LoadRes(string modelName, Action<GameObject> funLoaded, bool emptyGO = false)
    {
        string model = string.IsNullOrEmpty(modelName) ? "EmptyWeapon" : modelName;
        GameObject weaponGO = WeaponManager.GetWeaponGO(model);
        if (weaponGO == null)
        {
            if(emptyGO)
            {
                weaponGO = new GameObject(modelName);
                funLoaded(weaponGO);
            }
            else if (string.IsNullOrEmpty(modelName))
            {
                m_bIsEmptyModel = true;
                weaponGO = new GameObject("EmptyWeapon");
                funLoaded(weaponGO);
            }
            else
            {
                string strPath = PathHelper.GetWeaponPrefabPath(modelName);
                ResourceManager.LoadBattlePrefab(strPath, (go, crc) =>
                {
                    if (go == null)
                        Logger.Error("LoadRes strPath:[" + strPath + "] err");

                    funLoaded(go);
                });
            }
        }
        else
        {
            funLoaded(weaponGO);
        }
    }

    virtual public void SetGO(GameObject go)
    {
        m_kVM.RemoveAllRenderer();
        m_kVM.SetPlayer(m_kPlayer);
        InitGO(go, out m_kGO, out m_kTrans, out m_defaultScale, out m_mcc1, out m_lstRenderer, out m_arrBehaviour, out m_goEffect1);

        AudioSource kAS = m_mcc1.AddMissingComponent<AudioSource>("AudioSource", go);
        if (isFPV)
            kAS.panLevel = 0;
        else
            kAS.panLevel = 1;
        m_kSP.SetAudioSource(kAS);

        float soundRadius = 10;
        if(m_kWL.FireSound.Length > 0)
        {
            float.TryParse(m_kWL.FireSound[0], out soundRadius);
        }
        m_kSP.radius = soundRadius;

        if (m_transParentSetted != null)
        {
            go.transform.parent = m_transParentSetted;
            m_transParentSetted = null;
        }
        UIManager.GetPanel<PanelBattle>().CheckBowCross();
    }

    virtual public void SetGO2(GameObject go)
    {
        InitGO(go, out m_kGO2, out m_kTrans2, out m_defaultScale2, out m_mcc2, out m_lstRenderer2, out m_arrBehaviour2, out m_goEffectLeft1);
    }

    private void InitGO(GameObject go, out GameObject refGO, out Transform refTrans, out Vector3 refDefaultScale, out ModelComponentCache refmcc, out List<Renderer> refRenderers, out Behaviour[] refBehaviours, out GameObject refEffect)
    {
        refGO = go;
        refTrans = go.transform;
        refmcc = go.AddMissingComponent<ModelComponentCache>();

        Rigidbody rb = refmcc.FindComponent<Rigidbody>("Rigidbody", go, false);
        if (rb != null)
            rb.isKinematic = true;

        Collider collider = refmcc.FindComponent<Collider>("Collider", go, false);
        if (collider != null)
            collider.enabled = false;

        if (isSubWeapon == false)
            SetActive(m_iActiveSetted > 0);

        //m_arrBehaviour2 = null;
        //m_lstRenderer2.Clear();

        if (refmcc.renderers == null || refmcc.renderers.Count == 0)
        {
            List<Renderer> lr = new List<Renderer>();
            List<Behaviour> behaviours = new List<Behaviour>();
            GameObject goEffect1 = null;

            ExtendFuncHelper.CallBackBool<GameObject> funVisit = ((child) =>
            {
                if (child.name == ModelConst.WEAPON_EFFECT1)
                    goEffect1 = child;
                else if (child.name == ModelConst.WEAPON_EFFECT2)
                    child.SetActive(false);

                Behaviour bhr = child.GetComponent<Behaviour>();
                if (bhr != null)
                    behaviours.Add(bhr);

                if (child.name.StartsWith("effect"))
                    return false;

                Renderer r = child.GetComponent<Renderer>();
                if (r != null)
                {
                    lr.Add(r);
                    r.useLightProbes = isFPV;
                }
                return true;
            });

            refTrans.VisitChild(funVisit, true);
            refmcc.effect1 = goEffect1;
            refmcc.modelBehaviours = behaviours.ToArray();
            refmcc.renderers = lr;
            refmcc.defaultScale = refTrans.localScale;
        }

        refEffect = refmcc.effect1;
        refBehaviours = refmcc.modelBehaviours;
        refRenderers = refmcc.renderers;
        refDefaultScale = refmcc.defaultScale;

        m_kVM.AddRenderer(refRenderers);

        if (!string.IsNullOrEmpty(m_kWL.FlowTex))
        {
            foreach (var renderer in refRenderers)
                SetFlowEffect(renderer, m_kWL.FlowTex, m_kWL.FlowTexColor.toColor());
        }

        m_kVM.RefreshVisable();
    }

    protected void SetAwakenInfo()
    {
        bool hasWeaponSkill = false;
        m_activeSkillIsRevive = false;
        m_awakenFireEffect = "";
        //添加武器觉醒特效
        if (m_kLWD.awaken_props != null)
        {
            var awakenConfig = ConfigManager.GetConfig<ConfigWeaponAwakenProp>();
            if (awakenConfig == null)
            {
                Logger.Error("ConfigWeaponAwakenProp is null");
                return;
            }

            for (int i = 0; i < m_kLWD.awaken_props.Length; ++i)
            {
                var awakenProp = awakenConfig.GetLine(m_kLWD.awaken_props[i]);
                if (awakenProp != null)
                {
                    if (awakenProp.Effect == GameConst.AWAKEN_WEAPON_EFFECT_FLOW)
                    {
                        string[] arr = awakenProp.Param.Split(',');
                        if (arr.Length != 3)
                        {
                            Logger.Error("SetAwakenInfo config id:" + awakenProp.Id + " err!!");
                            continue;
                        }
                        string flowTex = arr[0];
                        Color flowColor = arr[1].toColor();
                        float flowSpeed = 5;
                        if (!float.TryParse(arr[2], out flowSpeed))
                            continue;
                        if (isFPV)
                        {
                            if (m_lstRenderer != null)
                            {
                                foreach (var renderer in m_lstRenderer)
                                    SetFlowEffect(renderer, flowTex, flowColor, flowSpeed);
                            }
                            if (m_lstRenderer2 != null)
                            {
                                foreach (var renderer in m_lstRenderer2)
                                    SetFlowEffect(renderer, flowTex, flowColor, flowSpeed);
                            }
                        }
                    }
                    else if (awakenProp.Effect == GameConst.AWAKEN_WEAPON_EFFECT_FIRE)
                    {
                        string[] arr = awakenProp.Param.Split(',');
                        m_awakenFireEffect = isFPV ? arr[0] : arr[1];
                        if (arr.Length == 3)
                        {
                            m_awakenFireLineColor = arr[2].toColor();
                            m_bUseawakenFireLineColor = true;
                        }
                    }
                    if (isMainPlayer && m_kPlayer.curWeapon == this && awakenProp.Type == GameConst.AWAKEN_SKILL_ACTIVE && awakenProp.Effect == GameConst.AWAKEN_ACTOR_SKILL)
                    {
                        hasWeaponSkill = true;
                        m_kActiveSkillAwakenId = awakenProp.Id;
                        int skillId = 0;
                        var param = awakenProp.Param.Split(',');
                        if (param.Length > 1)
                        {
                            if (!int.TryParse(param[0], out skillId))
                                continue;
                        }
                        else
                        {
                            if (!int.TryParse(awakenProp.Param, out skillId))
                                continue;
                        }
                        TDSKillInfo skill = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);
                        if (skill != null && skill.Effect == GameConst.SKILL_EFFECT_REVIVE)
                            m_activeSkillIsRevive = true;
                        GameDispatcher.Dispatch<int>(GameEvent.UI_UPDATE_WEAPON_SKILL, awakenProp.Id);
                    }
                }
            }
        }
        if (isMainPlayer && m_kPlayer.curWeapon == this && !hasWeaponSkill)
            GameDispatcher.Dispatch<int>(GameEvent.UI_UPDATE_WEAPON_SKILL, -1);
    }

    private void SetFlowEffect(Renderer renderer, string flowTex, Color flowColor, float flowSpeed = 5)
    {
        // 加载配置中的流光贴图
        if (renderer != null && !string.IsNullOrEmpty(flowTex))
        {
            //string shaderName = !isCrystal ? GameConst.SHADER_MP_WEAPON : GameConst.SHADER_MP_WEAPON_CRYSTAL;
            string curShaderName = renderer.material.shader.name;
            string shaderName = GetFlowUVShader(curShaderName);

            if (curShaderName != shaderName)
            {
                ResourceManager.LoadShader(shaderName, (shader) =>
                {
                    ResourceManager.LoadTexture(flowTex, (tex) =>
                    {
                        Material mat = new Material(shader);
                        mat.mainTexture = renderer.material.mainTexture;
                        mat.SetTexture(GameConst.SHADER_PROPERTY_FLOWTEX, tex);
                        mat.SetColor(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR, flowColor / 255);
                        mat.SetFloat(GameConst.SHADER_PROPERTY_FLOWSPEED, flowSpeed);
                        mat.SetFloat(GameConst.SHADER_PROPERTY_SPECBRIGHTNESS, renderer.material.GetFloat(GameConst.SHADER_PROPERTY_SPECBRIGHTNESS));
                        renderer.material = mat;
                        m_kVM.AddRenderer(renderer);
                    }, EResType.Battle);
                });
            }
            else
            {
                Texture texture = renderer.material.GetTexture(GameConst.SHADER_PROPERTY_FLOWTEX);
                if (texture == null || texture.name != flowTex)
                {
                    ResourceManager.LoadTexture(flowTex, (tex) =>
                    {
                        renderer.material.SetTexture(GameConst.SHADER_PROPERTY_FLOWTEX, tex);
                        renderer.material.SetColor(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR, flowColor / 255);
                    }, EResType.Battle);
                }
                else
                {
                    renderer.material.SetColor(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR, flowColor / 255);
                }
            }
        }
    }

    private string GetFlowUVShader(string curShaderName)
    {
        if (curShaderName.Contains("Transparent"))
            return GameConst.SHADER_MP_WEAPON_FLOW_TRANSPARENT;
        else
            return GameConst.SHADER_MP_WEAPON_FLOW;
    }
    virtual public void SetActive(bool bValue)
    {
        if (gameObject != null)
        {
            //gameObject.TrySetActive(bValue);
            //if (gameObject2 != null)
            //    gameObject2.TrySetActive(bValue);

            if (m_arrBehaviour != null)
            {
                for (int i = 0; i < m_arrBehaviour.Length; i++)
                {
                    if (m_arrBehaviour[i] != null)
                        m_arrBehaviour[i].enabled = bValue;
                }
            }

            if (m_arrBehaviour2 != null)
            {
                for (int i = 0; i < m_arrBehaviour2.Length; i++)
                {
                    if (m_arrBehaviour2[i] != null)
                        m_arrBehaviour2[i].enabled = bValue;
                }
            }


            if (!bValue)
            {
                m_kTrans.SetParent(SceneManager.singleton.reclaimRoot, false);
                m_kTrans.localPosition = Vector3.zero;
                if(m_kTrans2 != null)
                {
                    m_kTrans2.SetParent(SceneManager.singleton.reclaimRoot, false);
                    m_kTrans2.localPosition = Vector3.zero;
                }
            }
            else
            {
                WeaponBase.AttachToPlayer(isFPV, m_kPlayer, m_kWL, m_kTrans, m_kTrans2 != null ? m_kTrans2 : null, defaultScale, m_defaultScale2, this);
                //if (!isBothHandSlot)
                //{
                //    WeaponBase.SlotHand(isFPV, m_kPlayer, m_kWL, m_kTrans, m_transBipRoot, m_transSlot, 0);
                //}
                //else
                //{
                //    WeaponBase.SlotHand(isFPV, m_kPlayer, m_kWL, m_kTrans, m_transBipRoot, m_kPlayer.rightHandSlot, 0);
                //    WeaponBase.SlotHand(isFPV, m_kPlayer, m_kWL, m_kTrans, m_transBipRoot2, m_kPlayer.leftHandSlot, 1);
                //}
            }


            m_iActiveSetted = 0;

            //if (m_kPlayer is OtherPlayer)
                //Logger.Log("Weapon SetActive " + bValue.ToString() + " " + weaponId);
        }
        else
        {
            m_iActiveSetted = bValue == true ? 1 : -1;  // 如果模型还没加载完，这里要记录下状态，模型加载完后补上
        }
    }

    virtual public string TestWeapon()
    {
        return WEAPON_TEST_UNKNOW;
    }

    static public void AttachToPlayer(bool isFpv, BasePlayer player, ConfigItemWeaponLine wl, Transform tranWeapon, Transform tranWeapon2 = null, Vector3 rightScale = default(Vector3), Vector3 leftScale = default(Vector3), WeaponBase weapon = null)
    {
        if (tranWeapon == null)
            return;

        Transform transSlot = null;
        Transform transBipRoot = null;
        Transform transBipRoot2 = null;

        if(weapon == null || weapon.transBipRoot == null)
        {
            transBipRoot = GameObjectHelper.FindChildByTraverse(tranWeapon, ModelConst.WEAPON_BIP_ROOT);
            switch (wl.SlotType)
            {
                case WEAPON_SLOTTYPE_RIGHTHAND:
                    transSlot = player.rightHandSlot;
                    break;
                case WEAPON_SLOTTYPE_LEFTHAND:
                    transSlot = player.leftHandSlot;
                    break;
                case WEAPON_SLOTTYPE_BOTHHAND:
                    break;
            }
            if (tranWeapon2 != null)
                transBipRoot2 = GameObjectHelper.FindChildByTraverse(tranWeapon2, ModelConst.WEAPON_BIP_ROOT);

            if (weapon != null)
            {
                weapon.transBipRoot = transBipRoot;
                weapon.transBipRoot2 = transBipRoot2;
                weapon.transSlot = transSlot;
            }
        }
        else
        {
            transSlot = weapon.transSlot;
            transBipRoot = weapon.transBipRoot;
            transBipRoot2 = weapon.transBipRoot2;
        }

        if (tranWeapon2 == null) //单手挂载
        {
            SlotHand(isFpv, player, wl, tranWeapon, transBipRoot, transSlot, rightScale, 0);
        }
        else
        {
            SlotHand(isFpv, player, wl, tranWeapon, transBipRoot, player.rightHandSlot, rightScale, 0);
            SlotHand(isFpv, player, wl, tranWeapon2, transBipRoot2, player.leftHandSlot, leftScale, 1);
        }
    }

    private static void SlotHand(bool isFpv, BasePlayer player, ConfigItemWeaponLine wl, Transform tranWeapon, Transform transRoot , Transform transSlot, Vector3 scale, int index)
    {
        if (transSlot != null && tranWeapon.parent == transSlot)
            return;

        Vector3 vec3Pos = Vector3.zero;
        Vector3 vec3Rotation = Vector3.zero;

        if (isFpv)
        {
            if (wl.MPPos != null && wl.MPPos.Length > 0)
            {
                vec3Pos = wl.MPPos[index];
                vec3Rotation = wl.MPRot[index];
            }
        }
        else if (player.modelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
        {
            if (wl.OPGirlPos != null && wl.OPGirlPos.Length > 0)
            {
                vec3Pos = wl.OPGirlPos[index];
                vec3Rotation = wl.OPGirlRot[index];
            }
        }
        else if (wl.OPBoyPos != null && wl.OPBoyRot.Length > 0)
        {
            vec3Pos = wl.OPBoyPos[index];
            vec3Rotation = wl.OPBoyRot[index];
        }

        if (transSlot == null)
            tranWeapon.parent = player.transform;
        else
            tranWeapon.parent = transSlot;

        if (transRoot != null)
            transRoot.ResetLocal();

        if (!isFpv)
            tranWeapon.localScale = scale;

        tranWeapon.localPosition = vec3Pos;
        tranWeapon.localEulerAngles = vec3Rotation;
    }

    virtual public void UpdateConfigReference()
    {
        ConfigItemWeaponLine kCWL = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(m_kWL.ID);
        m_kWL = kCWL;
        m_kBL = ConfigManager.GetConfig<ConfigItemBullet>().GetLine(m_kWL.AmmoID);
        
        if (m_kSpread != null)
            m_kSpread.UpdateConfig(m_kWL);
    }

    private int m_hitSoundCount;
    private const float MISS_SOUND_DISTANCE = 5f;
    protected void PlayHitBuildingEffect(Vector3 hitPos, Vector3 hitNormal, int matType, string matEffectType, float rotZ = 0, Vector3 rayDir = default(Vector3), Vector3 rayOriginPos = default(Vector3), bool isOP = false)
    {
        if (GameSetting.enableDanheng == false || m_kPlayer is OtherPlayer && QualityManager.enableOPDanHeng == false)
            return;

        ConfigMaterialLine ml = ConfigManager.GetConfig<ConfigMaterial>().GetLine(matType);
        if (ml == null)
            return;

        string soundName = "";
        float soundDistance = 40f;
        string[] tmpArr = null;
        string effectName = "";
        float effectTime = -1;
        switch(matEffectType)
        {
            case Effect.MAT_EFFECT_GUN:
                effectName = isOP ? ml.HitEffectOP : ml.HitEffect;
                effectTime = ml.HitEffectTime;
                tmpArr = ml.HitSound.Split(';');
                soundName = tmpArr[1];
                soundDistance = float.Parse(tmpArr[0]);
                break;
            case Effect.MAT_EFFECT_KNIFE:
                effectName = ml.KnifeEffect;
                effectTime = ml.KnifeEffectTime;
                tmpArr = ml.KnifeHitSound.Split(';');
                soundName = tmpArr[1];
                soundDistance = float.Parse(tmpArr[0]);
                break;
            case Effect.MAT_EFFECT_EXP:
                effectName = ml.ExpEffect;
                effectTime = ml.ExpEffectTime;
                break;
        }

        if (GameSetting.enableSound)
        {
            m_hitSoundCount++;
            if (m_hitSoundCount >= m_kWL.BulletEffectFrequency)
            {
                m_hitSoundCount = 0;
                if (!string.IsNullOrEmpty(soundName))
                    AudioManager.PlayHitSound(hitPos, soundName, soundDistance, 1, "hit");
                if (MainPlayer.singleton != null && MainPlayer.singleton != m_kPlayer)
                {
                    //计算自己距离弹线的距离（点到射线的距离），呼啸声
                    var mainPlayerPos = MainPlayer.singleton.position;
                    var distance = Vector3.Cross(-rayDir, mainPlayerPos - hitPos).magnitude;
//                    Logger.Log(distance);
                    if (distance < MISS_SOUND_DISTANCE)
                    {
                        var s1 = Vector3.Project(rayOriginPos - mainPlayerPos, rayDir).magnitude;
                        var s2 = Vector3.Project(hitPos - mainPlayerPos, rayDir).magnitude;
                        var length = (hitPos - rayOriginPos).magnitude;   //弹线的长度

                        if (s1 < length + MISS_SOUND_DISTANCE && s2 < length)   //有效长度为从射击点到(着弹点+呼啸声半径)
                        {
                            AudioManager.PlayHitSound(mainPlayerPos, "oml_" + UnityEngine.Random.Range(1, 5), 10, UnityEngine.Random.Range(0.6f, 1f), "miss");
//                            Debug.DrawRay(hitPos, -rayDir * length, Color.red, 1);
                        }
                    }
                }
            }
        }

        if (string.IsNullOrEmpty(effectName))
            return;

        EffectManager.singleton.GetEffect<EffectTimeout>(effectName, (kEffect) =>
        {
            float zoffset = GameSetting.EffectPosZOffset;

            kEffect.Attach(hitPos, hitNormal, Space.World, SceneManager.singleton.effectRoot, zoffset, rotZ);
            kEffect.SetTimeOut(effectTime);
            kEffect.Play();
        });
    }

    protected void PlayHitPlayerEffect(BasePlayer player, Vector3 hitPos, Vector3 hitNormal, int iBodyPartID, string matEffectType)
    {
        if(player == null || player.isMaxBloodEffect || player.isRendererVisible == false)
        {
            return;
        }

        player.m_bloodEffectNum++;
        
        ConfigMaterialLine ml = EffectManager.GetPlayerHitEffectCfg(iBodyPartID);
        if (ml == null) return;
        string effectID = matEffectType == Effect.MAT_EFFECT_KNIFE ? ml.KnifeEffect : ml.HitEffect;

        if (string.IsNullOrEmpty(effectID) == false)
        {
            EffectManager.singleton.GetEffect<EffectTimeAndScale>(effectID, (kEffect) =>
            {
                kEffect.Attach(hitPos, hitNormal, 0);
                //这里根据离主角的距离缩放击中效果
                if (null != MainPlayer.singleton && 0 != ml.HitEffectRange && 1 != ml.HitEffectRange) //这里是粒子系统，修改localscale不影响动批和性能，也方便ParticleScaler作处理
                    kEffect.transform.localScale = getHitScale(Vector3.Distance(MainPlayer.singleton.headPos, kEffect.transform.position), ml.HitEffectMin, ml.HitEffectMax, ml.HitEffectRange);
                kEffect.SetTimeOut(-1);
                kEffect.Play();
                kEffect.SetCompleteCallBack(player, (target) =>
                {
                    BasePlayer p = target as BasePlayer;
                    if(p != null)
                    {
                        p.m_bloodEffectNum--;
                    }
                });
            });
        }

        //命中音效
        PlayerHitSound(hitPos, iBodyPartID, matEffectType);
    }

    public void PlayerHitSound(Vector3 hitPos, int iBodyPartID, string matEffectType, float volume = 1f)
    {
        int iMatID = (int)EMat.Body;
        switch (iBodyPartID)
        {
            case BasePlayer.BODY_PART_ID_HEAD:
                iMatID = (int)EMat.Head;
                break;
            case BasePlayer.BODY_PART_ID_BODY:
                iMatID = (int)EMat.Body;
                break;
            case BasePlayer.BODY_PART_ID_LIMBS:
                iMatID = (int)EMat.Limbs;
                break;
        }
        ConfigMaterialLine kML = ConfigManager.GetConfig<ConfigMaterial>().GetLine(iMatID);
        if (kML != null)
        {
            string soundName = "";
            float soundDistance = 40f;
            string[] tmpArr = null;
            switch (matEffectType)
            {
                case Effect.MAT_EFFECT_GUN:
                    tmpArr = kML.HitSound.Split(';');
                    soundName = tmpArr[1];
                    soundDistance = float.Parse(tmpArr[0]);
                    break;
                case Effect.MAT_EFFECT_KNIFE:
                    tmpArr = kML.KnifeHitSound.Split(';');
                    soundName = tmpArr[1];
                    soundDistance = float.Parse(tmpArr[0]);
                    break;
            }
            if (!string.IsNullOrEmpty(soundName))
                AudioManager.PlayHitSound(hitPos, soundName, soundDistance, volume);
        }
    }

    virtual public void StopWeaponAni()
    {
        if (relateWeapon != null)
            relateWeapon.JumpAni2End();
        JumpAni2End();
    }

    virtual public void JumpAni2End()
    {

    }

    protected void PlayFireEmptySound()
    {
        if(m_kWL.FireEmptySound.Length == 3)
        {
            float delay = 0;
            float.TryParse(m_kWL.FireEmptySound[1], out delay);

            TimerManager.SetTimeOut(delay, () =>
            {
                m_kSP.PlayOneShot(m_kWL.FireEmptySound[2], AudioManager.FightSoundVolume);
            });
        }
    }

    protected void DelayPlayOneShot(float delay, string clipName)
    {
        TimerManager.SetTimeOut(delay, () => { if (m_kSP != null) m_kSP.PlayOneShot(clipName, AudioManager.FightSoundVolume); });
    }

    protected void SendReloadMsg(bool onlyAddOne)
    {
        if (!isMainPlayer)
            return;

        NetLayer.Send(new tos_fight_reload_done()
        {
            weaponVer = m_kPlayer.serverData.weaponVer,
            onlyOne = onlyAddOne,
        });

        m_kPlayer.serverData.weaponVer++;
    }

    public void ApplyMainPlayerLayer()
    {
        if(gameObject != null && gameObject.layer != GameSetting.LAYER_VALUE_MAIN_PLAYER)
            gameObject.ApplyLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER, null, null, true);
        if(gameObject2 != null && gameObject2.layer != GameSetting.LAYER_VALUE_MAIN_PLAYER)
            gameObject2.ApplyLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER, null, null, true);
    }

    protected bool NeedLightProbe(Renderer r)
    {
        if (isFPV || r != null && r.sharedMaterial != null && r.sharedMaterial.shader.name.Contains(GameConst.SHADER_NAME_LIGHTPROBE))
            return true;

        return false;
    }

    public void InitCrossHair()
    {
        if (m_kWL == null || !this.ower.alive)
            return;
        if (m_kWL.Class == WeaponManager.WEAPOM_TYPE_BOW)
        {
            UIManager.GetPanel<PanelBattle>().CheckBowCross();
        }
        var crossHairs = PanelBattle.m_crossHairs;

        var m_serverData = ower.serverData;
        //m_serverData.UpdateTestWeaponConfig(m_kWL);
        float fov = SceneCamera.singleton.fieldOfView;


        var xx = 0.5f * UIManager.m_resolution.y * Mathf.Tan(1 * Mathf.PI / 180f) / Mathf.Tan(fov / 2 * Mathf.PI / 180f);

        if (crossHairs != null && crossHairs.Length == 5)
        {
            crossHairs[0].anchoredPosition = new Vector2(xx, 0);
            crossHairs[1].anchoredPosition = new Vector2(0, xx);
            crossHairs[2].anchoredPosition = new Vector2(-xx, 0);
            crossHairs[3].anchoredPosition = new Vector2(0, -xx);
        }
    }

    /// <summary>
    /// 反作弊
    /// </summary>
    //protected bool CheatDis(BasePlayer target)
    //{
    //    if (target == null || target.released)
    //        return false;

    //    if ((target.position - target.serverPos).sqrMagnitude >= 10 * 10)
    //        return true;

    //    return false;
    //}

    #region getMaterialHitScale
    private Vector3 getHitScale(float mainDis, float hitMin = 0, float hitMax = 0, float hitRange = 0)
    {
        if (0 == hitMin || 0 == hitMax || 0 == hitRange)
            return Vector3.one;

        float hitScaleFactor = 1;
        if (mainDis < hitMin)
            hitScaleFactor = 1;
        else if (mainDis > hitMax)
            hitScaleFactor = hitRange;
        else
            hitScaleFactor = (mainDis - hitMin) * ((hitRange - 1) / (hitMax - hitMin)) + 1;
        return new Vector3(hitScaleFactor, hitScaleFactor, 1);
    }
    #endregion

    #region Fire
    virtual public void SkillFire(Action callBack = null)
    {
        m_fireCallBack = callBack;
        m_bIsFireBefore = false;
        m_bIsSkillFire = true;
        m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
        TryToFire();
    }


    /// <summary>
    /// 子类一定要调用父类的此方法 base.TryToFire
    /// </summary>
    virtual protected void TryToFire()
    {
        if (lastFireTime < 0 || lastFireTime > Time.timeSinceLevelLoad + 0.1f)
            AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_LastFireTime, lastFireTime + " " + Time.timeSinceLevelLoad);

        if (GameSetting.fireRateMsg)
        {
            Logger.Log("FR = " + fireRate + "  serverData.FireRate = " + m_kPlayer.serverData.FireRate(atkType) + " it = " + (Time.timeSinceLevelLoad - lastFireTime) + " Time = " + Time.timeSinceLevelLoad + " lt = " + lastFireTime);
        }
        if(TestWeapon() == WEAPON_TEST_SUCCESS)
        {
            lastFireTime = Time.timeSinceLevelLoad;
        }
    }

    virtual protected void DoFireReally()
    {

    }

    virtual internal void Fire2(toc_fight_fire arg)
    {
        if (gameObject == null || m_kPlayer.alive == false)
            return;
    }

    virtual internal void Atk(toc_fight_atk arg)
    {
    }

    protected void ReduceAmmoNum()
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (m_isSubWeapon)
        {
            if(m_kLWD.subAmmoNum != -1)
            {
                m_kLWD.subAmmoNum -= 1;
                m_kLWD.subAmmoNum = m_kLWD.subAmmoNum < 0 ? 0 : (int)m_kLWD.subAmmoNum;
            }
        }
        else
        {
            if (m_kLWD.curClipAmmoNum != -1)
            {
                m_kLWD.curClipAmmoNum -= 1;
                m_kLWD.curClipAmmoNum = m_kLWD.curClipAmmoNum < 0 ? 0 : (int)m_kLWD.curClipAmmoNum;

                if (MainPlayer.singleton.useAdvAmmo)
                {
                    m_kLWD.advAmmoNum -= 1;
                    m_kLWD.advAmmoNum = m_kLWD.advAmmoNum < 0 ? 0 : (int)m_kLWD.advAmmoNum;
                }
            }
        }
    }
#endregion

#region Reload Ammo

    virtual public void ReloadAmmo()
    {

    }

    virtual public void ReloadAmmoComplete()
    {

    }

    virtual internal void ReloadAmmo2(toc_fight_reload arg)
    {
        if (gameObject == null || m_kPlayer.serverData.alive == false)
            return;
    }
    
    virtual public void StopReload()
    {

    }

#endregion

#region Switch

    virtual public void WeaponSwitchOff()
    {
        if (m_released)
            return;
        foreach (var effect in m_fireModelMountEffect.Values)
        {
            if (effect != null && effect.gameObject != null)
                effect.gameObject.TrySetActive(false);
        }
        StopWeaponAni();
        UIManager.GetPanel<PanelBattle>().CheckBowCross();
    }

    virtual public void WeaponSwitched()
    {
        if (m_released)
            return;
        lastFireTime = 0;
        SetAwakenInfo();
        UIManager.GetPanel<PanelBattle>().CheckBowCross();
    }
#endregion 

#region 武器操作

    virtual public void OnFireKeyDown()
    {

    }

    virtual public void OnFireKeyUp()
    {

    }

    virtual protected void OnFireUpdate()
    {

    }

    virtual public void ZoomInImmediate()
    {

    }

    virtual public void OnZoomIn()
    {

    }

    virtual public void OnZoomOut()
    {

    }

    virtual public void OnCancleFire()
    {

    }

    virtual protected void ZoomIn()
    {

    }

    virtual protected void ZoomOut()
    {

    }

    virtual public void ZoomOutImmediate()
    { 
    }

    virtual protected void ZoomInComplete()
    {

    }

    virtual protected void ZoomOutComplete()
    {

    }

    

#endregion

#region 可视度设置
    virtual public void SetVisableDegree(float visableDegree,float time)
    {
        m_kVM.SetVisable(visableDegree, time, true);
        if (m_goEffect1 != null)
        {
            m_goEffect1.TrySetActive(visableDegree == 1f);
        }
        if(m_goEffectLeft1 != null)
        {
            m_goEffectLeft1.TrySetActive(visableDegree == 1f);
        }
    }
#endregion

    #region 武器行为

    virtual public void WeaponMotion(int motion)
    {
        switch(motion)
        {
            case WeaponConst.Weapon_Motion_Scope_ZoomIn:
                m_kLWD.isZoomIn = true;
                if(isFPWatchPlayer)
                    ZoomIn();
                break;
            case WeaponConst.Weapon_Motion_Scope_ZoomOut:
                m_kLWD.isZoomIn = false;
                if (isFPWatchPlayer)
                    ZoomOut();
                break;
            case WeaponConst.Weapon_Motion_Scope_ZoomOutImmediate:
                m_kLWD.isZoomIn = false;
                if (isFPWatchPlayer)
                    ZoomOutImmediate();
                break;
        }
        //Logger.Log("WeaponMotion: Player=" + m_kPlayer.ColorName + "  Weapon=" + m_kWL.DispName + "  motion=" + motion + "  isZoomIn=" + m_kLWD.isZoomIn + "    " + m_kLWD.GetHashCode());
    }
    #endregion

    #region 特效
    virtual public void PlayMPFireMountEffect()
    {

    }

    virtual public void PlayOPFireMountEffect(int index)
    {
        if (m_kWL.OPFireMountEffect != null && m_kWL.OPFireMountEffect.Length != 0)
        {
            string fireMountEffect = "";
            if (m_kWL.OPFireMountEffect.Length >= index)
                fireMountEffect = m_kWL.OPFireMountEffect[index];
            else if (m_kWL.OPFireMountEffect.Length > 0)
                fireMountEffect = m_kWL.OPFireMountEffect[0];

            Transform fireMountEffectTran = null;
            if (!m_fireModelMountEffect.TryGetValue(fireMountEffect, out fireMountEffectTran))
            {
                fireMountEffectTran = m_kPlayer.transform.FindChild(fireMountEffect);
                m_fireModelMountEffect.Add(fireMountEffect, fireMountEffectTran);
            }
            if (fireMountEffectTran != null)
                EffectManager.PlayEffect(fireMountEffectTran, true);
        }
    }
    #endregion

    #region 其他
    public void SetLayer(int layer)
    {
        if (m_kGO != null)
        {
            m_kGO.ApplyChildrenLayer(layer);
            m_kGO.layer = layer;
        }
        if (m_kGO2 != null)
        {
            m_kGO2.ApplyChildrenLayer(layer);
            m_kGO2.layer = layer;
        }
    }
    #endregion

    #region get set

    public bool released
    {
        get { return m_released; }
        set { m_released = value; }
    }

    virtual public bool hasExtAmmo
    {
        get { return m_kLWD.extraAmmoNum > 0; }
    }

    virtual public bool hasAmmoCurClip
    {
        get
        {
            return m_isSubWeapon ? (m_kLWD.subAmmoNum == -1 || m_kLWD.subAmmoNum > 0): (m_kLWD.curClipAmmoNum == -1 || m_kLWD.curClipAmmoNum > 0);
        }
    }
    virtual public bool isAmmoCurClipNolimit
    {
        get
        {
            return m_kLWD.curClipAmmoNum == -1 ;
        }
    }

    virtual public bool IsCurClipFull()
    {
        return false;
    }    

    public ConfigItemWeaponLine weaponConfigLine
    {
        get { return m_kWL;}
    }

    public GameObject gameObject
    {
        get { return m_kGO; }
    }

    public GameObject gameObject2
    {
        get { return m_kGO2; }
    }

    public BasePlayer ower
    {
        get { return m_kPlayer; }
    }

    public WeaponSpread weaponSpread
    {
        get { return m_kSpread; }
    }

    public int ammoNumInClip
    {
        get { return m_kLWD.curClipAmmoNum;  }
        set { m_kLWD.curClipAmmoNum = value; }
    }

    public int subAmmoNum
    {
        get { return m_kLWD.subAmmoNum; }
        set { m_kLWD.subAmmoNum = value; }
    }

    public int advNum
    {
        get { return m_kLWD.advAmmoNum; }
        set { m_kLWD.advAmmoNum = value; }
    }

    public int minAmmoAdv
    {
        get { return ammoNumInClip <= advNum ? ammoNumInClip : advNum; }
    }

    public int protoBulletCnt
    {
        get
        {
            int bulletCnt = ammoNumInClip;
            if (MainPlayer.singleton.useAdvAmmo)
                bulletCnt = minAmmoAdv;
            return bulletCnt;
        }
    }

    public int ammoNumLeft
    {
        get { return m_kLWD.extraAmmoNum; }
    }

    public EptInt curFireType
    {
        get { return m_fireType; }
    }

    public string weaponId
    {
        get
        {
            return m_kLWD.id;
        }
    }

    virtual public int atkType
    {
        get { return WeaponAtkTypeUtil.GetWeaponAtkType(m_isSubWeapon ? relateWeapon.weaponConfigLine.SubType : m_kWL.SubType, (!m_isSubWeapon && m_kPlayer is MainPlayer && MainPlayer.singleton.useAdvAmmo), m_isSubWeapon); }
    }
   
    public float FireRate
    {
        get
        {
            return fireRate;
        }
    }

    virtual protected float fireRate
    {
        get
        {
            if (m_kPlayer == null)
                return 0;

            float ret = 60.0f / m_kPlayer.serverData.FireRate(atkType);
            return ret;
        }
    }

    protected bool isCrystal
    {
        get { return m_model.Contains(ModelConst.WEAPON_CRYSTAL); }
    }

    public bool isSubWeapon
    {
        get { return m_isSubWeapon; }
        set
        {
            m_isSubWeapon = value;
            if (value && !(m_kWL.Class == WeaponManager.WEAPON_TYPE_SHOT_GUN))
                m_kSpread = null;
        }
    }

    public bool noCacheGo
    {
        get { return m_noCacheGo; }
        set { m_noCacheGo = value; }
    }

    virtual protected int bulletCount
    {
        get { return (!m_isSubWeapon && m_kPlayer is MainPlayer && MainPlayer.singleton.useAdvAmmo) ? advNum : (m_isSubWeapon ? subAmmoNum : ammoNumInClip); }
    }
    
    /// <summary>
    /// 依赖子弹数
    /// </summary>
    virtual protected int relyBulletCount
    {
        get { return (!m_isSubWeapon && m_kPlayer is MainPlayer && MainPlayer.singleton.useAdvAmmo) ? ammoNumInClip : 0; }
    }

    public bool isFirePressed
    {
        get { return m_bFirePressed; }
    }
   
    public bool canFire
    {
        get 
        {
            if (m_released || m_kPlayer == null)
                return false;

            if (m_kWL.SubType == GameConst.WEAPON_SUBTYPE_SKILLWEAPON)
                return false;

            float minFireTime = Time.timeSinceLevelLoad - m_shareFireCd;
            if (m_isSubWeapon)
            {
                if (relateWeapon != null && !relateWeapon.isFirePressed && relateWeapon.lastFireTime <= minFireTime)
                    return true;
            }
            else if (relateWeapon == null)
                return true;
            else if (!relateWeapon.isFirePressed && relateWeapon.lastFireTime <= minFireTime)
                return true;

            return false;
        }
    }

    public float shareFireCd
    {
        set { m_shareFireCd = value; }
    }

     
    public float lastFireTime
    {
        get { return m_lastFireTime.val; }
        protected set{ m_lastFireTime.val = value; }
    }

    public bool isFPV
    {
        get { return isMainPlayer || isFPWatchPlayer; }
    }

    public bool isMainPlayer
    {
        get { return m_kPlayer is MainPlayer; }
    }

    public bool isFPWatchPlayer
    {
        get { return m_kPlayer is FPWatchPlayer; }
    }

    virtual public bool isBothHandSlot
    {
        get { return m_kWL.SlotType == WEAPON_SLOTTYPE_BOTHHAND; }
    }

    /// <summary>
    /// 武器觉醒属性
    /// </summary>
    public int[] awakenProps
    {
        get { return m_kLWD.awaken_props; }
    }

    public int activeSkillAwakenId
    {
        get { return m_kActiveSkillAwakenId; }
    }

    public bool activeSkillIsRevive
    {
        get 
        {
            return m_activeSkillIsRevive;
        }
    }

    public LocalWeaponData localWeaponData
    {
        get { return m_kLWD; }
    }

    public virtual bool isZoomOrOut
    {
        get { return false; }
    }

    public Transform transBipRoot { get { return m_transBipRoot; } set { m_transBipRoot = value; } }
    public Transform transBipRoot2 { get { return m_transBipRoot2; } set { m_transBipRoot2 = value; } }
    public Transform transSlot { get { return m_transSlot; } set { m_transSlot = value; } }

    public Vector3 defaultScale { get { return m_defaultScale; } }
    public Vector3 defaultScale2 { get { return m_defaultScale2; } }

    virtual public float ReloadTime
    {
        get { return m_kLWD.reloadTime; }
    }

    public WeaponBase relateWeapon
    {
        get { return m_relateWeapon; }
        set { m_relateWeapon = value; }
    }

    public bool IsHeal
    {
        get { return m_isHeal; }
        set { m_isHeal = value; }
    }

    public bool canAutoAim
    {
        get
        {
            if (weaponConfigLine == null || weaponConfigLine.AutoAimType == 0)
                return false;
            return weaponConfigLine.AutoAimType == 2 ||
                   (weaponConfigLine.AutoAimType == 1 && WorldManager.singleton.isZombieTypeModeOpen);
        }
    }

    // 判断是否安娜的治愈武器
    public void setIsHeal()
    {
        IsHeal = weaponConfigLine.TeamHealRate > 0;
    }


    #endregion
}