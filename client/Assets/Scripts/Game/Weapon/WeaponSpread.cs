﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponSpread
{
    private WeaponBase m_kWeapon;
    private ConfigItemWeaponLine m_kWL;
    private PlayerStatus m_kPlayerStatus;
    private PlayerServerData m_serverData;

    private Vector2 crossHairCenter;
    private RectTransform[] crossHairs;

    private VtfFloatCls m_lastFireTime = new VtfFloatCls();
    private VtfFloatCls m_endPitchTime = new VtfFloatCls();
    private VtfFloatCls m_endYawTime = new VtfFloatCls();
    private VtfFloatCls m_lastFirePitch = new VtfFloatCls();
    private VtfFloatCls m_lastFireYaw = new VtfFloatCls();
    private VtfIntCls m_fireIndex = new VtfIntCls();

    private VtfFloatCls m_recoilCount = new VtfFloatCls();
    private VtfFloatCls m_recoilPitchSpeed = new VtfFloatCls();
    private VtfFloatCls m_recoilYawSpeed = new VtfFloatCls();
    private VtfFloatCls m_recoilPitch = new VtfFloatCls();
    private VtfFloatCls m_recoilYaw = new VtfFloatCls();

    private VtfFloatCls m_spread = new VtfFloatCls();
    private VtfFloatCls m_spreadFinal = new VtfFloatCls();
    private VtfFloatCls m_spreadMovement = new VtfFloatCls();
    private VtfFloatCls m_spreadJump = new VtfFloatCls();


    public void Init(WeaponBase weapon)
    {
        m_kWeapon = weapon;
        m_kWL = weapon.weaponConfigLine;
        m_kPlayerStatus = weapon.ower.status;
        m_serverData = weapon.ower.serverData;
        m_serverData.UpdateTestWeaponConfig(m_kWL);
    }

    public void UpdateConfig(ConfigItemWeaponLine config)
    {
        m_kWL = config;
        m_serverData.UpdateTestWeaponConfig(config);
    }

    public void Update()
    {
        AutoAim.Update();
        UpdateRecoilAndSpread();
    }

    public bool IsRecoil()
    {
        return lastFireYaw != 0 || recoilYaw != 0 || lastFirePitch != 0 || recoilPitch != 0;
    }

    public void InitJumpSpread()
    {
        spreadJump = m_serverData.SpreadJumpMax(m_kWeapon.atkType);
    }

    public void ClearRecoilAndSpread()
    {
        recoilCount = 0;
        fireIndex = 0;
        if (m_kPlayerStatus.stand)
            spread = m_serverData.StandSpreadInitial(m_kWeapon.atkType);
        else if (m_kPlayerStatus.crouch)
            spread = m_serverData.CrouchSpreadInitial(m_kWeapon.atkType);
        spreadMovement = 0;
        spreadJump = 0;
    }

    public float ApplySpread()
    {
        FireRecoil();
        FireSpread();
        return spreadFinal;
    }

    private void FireRecoil()
    {
        fireIndex++;
        recoilCount = recoilCount + 1;
        var recoilCountMax = m_serverData.RecoilCountMax(m_kWeapon.atkType);
        if (recoilCount > recoilCountMax)
            recoilCount = recoilCountMax;

        var x = Mathf.CeilToInt(recoilCount);
        float minPitchKickSpeed = m_kWL.MinPitch[0] * x + m_kWL.MinPitch[1] * x * x + m_kWL.MinPitch[2] * x * x * x + m_kWL.MinPitch[3];
        float maxPitchKickSpeed = m_kWL.MaxPitch[0] * x + m_kWL.MaxPitch[1] * x * x + m_kWL.MaxPitch[2] * x * x * x + m_kWL.MaxPitch[3];
        float minYawKickSpeed = m_kWL.MinYaw[0] * x + m_kWL.MinYaw[1] * x * x + m_kWL.MinYaw[2] * x * x * x + m_kWL.MinYaw[3];
        float maxYawKickSpeed = m_kWL.MaxYaw[0] * x + m_kWL.MaxYaw[1] * x * x + m_kWL.MaxYaw[2] * x * x * x + m_kWL.MaxYaw[3];

        if (GlobalConfig.showRecoilDebugInfo)
        {
            var str = "";
            str += string.Format("recoilCount={0}\n", x);
            str += string.Format("\tMinPitch a={0} b={1} c={2} d={3} KickSpeed={4}\n", m_kWL.MinPitch[0], m_kWL.MinPitch[1], m_kWL.MinPitch[2], m_kWL.MinPitch[3], minPitchKickSpeed);
            str += string.Format("\tMaxPitch a={0} b={1} c={2} d={3} KickSpeed={4}\n", m_kWL.MaxPitch[0], m_kWL.MaxPitch[1], m_kWL.MaxPitch[2], m_kWL.MaxPitch[3], maxPitchKickSpeed);
            str += string.Format("\tMinYaw a={0} b={1} c={2} d={3} KickSpeed={4}\n", m_kWL.MinYaw[0], m_kWL.MinYaw[1], m_kWL.MinYaw[2], m_kWL.MinYaw[3], minYawKickSpeed);
            str += string.Format("\tMaxYaw a={0} b={1} c={2} d={3} KickSpeed={4}\n", m_kWL.MaxYaw[0], m_kWL.MaxYaw[1], m_kWL.MaxYaw[2], m_kWL.MaxYaw[3], maxYawKickSpeed);
            str += string.Format("\tRecoilPitch={0} RecoilYaw={1}\n", recoilPitch, recoilYaw);
            Logger.Log(str);
        }

        
        var p = m_serverData.serverPos + m_serverData.serverRot;
        Random.seed = m_serverData.id + m_kWeapon.localWeaponData.curClipAmmoNum + (int)p.x + (int)(p.y) * 1000 + (int)(p.z) * 1000000;

        recoilPitchSpeed = Random.Range(minPitchKickSpeed, maxPitchKickSpeed);
        recoilYawSpeed = Random.Range(minYawKickSpeed, maxYawKickSpeed);
        if (m_kWeapon is WeaponGun && ((WeaponGun)m_kWeapon).isZoomOrOut)
        {
            recoilPitchSpeed *= m_kWL.RecoilSightsFactor;
            recoilYawSpeed *= m_kWL.RecoilSightsFactor;
        }

        endPitchTime = TimeTime + CalcTime(recoilPitch, recoilPitchSpeed, m_serverData.RecoilPitchMin(m_kWeapon.atkType), m_serverData.RecoilPitchMax(m_kWeapon.atkType));
        endYawTime = TimeTime + CalcTime(recoilYaw, recoilYawSpeed, m_serverData.RecoilYawMin(m_kWeapon.atkType), m_serverData.RecoilYawMax(m_kWeapon.atkType));
        lastFireTime = TimeTime;
        lastFirePitch = recoilPitch;
        lastFireYaw = recoilYaw;
    }

    private void FireSpread()
    {
        if (m_kPlayerStatus.stand)
        {
            spreadFinal = spread;
            spread += m_serverData.StandSpreadFireAdd(m_kWeapon.atkType);
            spread = Mathf.Min(spread, m_serverData.StandSpreadMax(m_kWeapon.atkType));
        }
        else
        {
            spreadFinal = spread;
            spread += m_serverData.CrouchSpreadFireAdd(m_kWeapon.atkType);
            spread = Mathf.Min(spread, m_serverData.CrouchSpreadMax(m_kWeapon.atkType));
        }

        spreadFinal += spreadMovement + spreadJump;
    }

    private float CalcShift(float s0, float v0, float t, float minShift, float maxShift)
    {
        //每次开枪，产生一个初速度，先匀减速到速度为0，再匀加速到位移为0
        float a1 = m_kWL.RecoilKickAcc * -(int)Mathf.Sign(v0); //减速度
        float t1 = v0 / (-a1);    //减速到0的时间

        //若运动过程中碰到边界，t1的时间相当于变短为到边界的时间
        float sLimit = (v0 >= 0 ? maxShift : minShift) - s0;
        float dt = v0 * v0 + 2 * a1 * (sLimit);
        if (dt >= 0)
        {
            float tLimit1 = (-v0 + Mathf.Sqrt(dt)) / a1;
            float tLimit2 = (-v0 - Mathf.Sqrt(dt)) / a1;
            t1 = Mathf.Min(t1, tLimit1, tLimit2);
        }
        //        Logger.Log(string.Format("============s0={0}, v0={1}, a1={2}, maxShift={3}, t={4}, dt={5}, sLimit={6}", s0, v0, a1, maxShift, t, dt, sLimit));

        if (t <= t1)
        {

            return s0 + v0 * t + 0.5f * a1 * t * t;
        }
        else
        {
            float s1 = v0 * t1 + 0.5f * a1 * t1 * t1;   //减速到0的位移
            float a2 = m_kWL.RecoilKickAcc * (FPWatchPlayer.singleton!=null && FPWatchPlayer.singleton.isWatching ? 0.3f:1f) * m_kWL.RecoilReturnAccFactor * -(int)Mathf.Sign(s0 + s1);
            float s2 = 0.5f * a2 * (t - t1) * (t - t1);
            return s0 + s1 + s2;
        }
    }

    private float CalcTime(float s0, float v0, float minShift, float maxShift)
    {
        //每次开枪，产生一个初速度，先匀减速到速度为0，再匀加速到位移为0
        float a1 = m_kWL.RecoilKickAcc * -(int)Mathf.Sign(v0); //减速度
        float t1 = v0 / (-a1); //减速到0的时间

        //若运动过程中碰到边界，t1的时间相当于变短为到边界的时间
        float sLimit = (v0 >= 0 ? maxShift : minShift) - s0;
        float dt = v0 * v0 + 2 * a1 * (sLimit);
        if (dt >= 0)
        {
            float tLimit1 = (-v0 + Mathf.Sqrt(dt)) / a1;
            float tLimit2 = (-v0 - Mathf.Sqrt(dt)) / a1;
            t1 = Mathf.Min(t1, tLimit1, tLimit2);
        }

        float s1 = v0 * t1 + 0.5f * a1 * t1 * t1; //减速到0的位移
        float a2 = m_kWL.RecoilKickAcc * (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching ? 0.3f : 1f) * m_kWL.RecoilReturnAccFactor * -(int)Mathf.Sign(s0 + s1);
        float s2 = -(s0 + s1);
        float t2 = Mathf.Sqrt(2 * s2 / a2);
        return t1 + t2;
    }

    /// <summary>
    /// 数值转换
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    private float TimeToSpeed(float time)
    {
        return time;
        //        return time == 0 ? float.MaxValue : 1/time;
    }

    private void UpdateRecoilAndSpread()
    {
        if (m_kWL == null || !m_kWeapon.ower.alive)
            return;

        //简易准星
        if (crossHairs == null)
            crossHairs = PanelBattle.m_crossHairs;
        RectTransform circleTran = PanelBattle.m_CircleCrossImage;
        Image m_CircleImg = PanelBattle.m_CircleImg;
        var curSpread = spread + spreadJump + spreadMovement;
        if (m_kWeapon is WeaponGun && ((WeaponGun)m_kWeapon).isZoomOrOut)
            curSpread *= m_serverData.SpreadSightsFactor(m_kWeapon.atkType);
        var dispMin = m_serverData.SpreadDispMin(m_kWeapon.atkType);
        float fov = SceneCamera.singleton.fieldOfView;
        var spreadDispMin = Mathf.Approximately(dispMin, 0) ? 0.5f : dispMin;
        var xx = 0.5f * UIManager.m_resolution.y * Mathf.Tan(Mathf.Max(spreadDispMin, curSpread) * Mathf.PI / 180f) / Mathf.Tan(fov / 2 * Mathf.PI / 180f);
        if (crossHairs != null && crossHairs.Length == 5)
        {
            crossHairs[0].anchoredPosition = new Vector2(xx, 0);
            crossHairs[1].anchoredPosition = new Vector2(0, xx);
            crossHairs[2].anchoredPosition = new Vector2(-xx, 0);
            crossHairs[3].anchoredPosition = new Vector2(0, -xx);
        }
        if (circleTran != null)
        {
            if (xx < 200)
            {
                circleTran.sizeDelta = new Vector2(30 + xx, 30 + xx);
            }
            if( circleTran.sizeDelta.x < 0 )
            {
                m_CircleImg.enabled = false;
            }
            else
            {
                m_CircleImg.enabled = true;
            }
        }
        
        if (recoilCount > 0)
        {
            recoilCount -= Time.deltaTime * TimeToSpeed(m_kWL.RecoilCountDecaySpeed);
            if (recoilCount < 0)
                recoilCount = 0;
        }

        //pitch分量
        if (TimeTime < endPitchTime)
        {
            recoilPitch = CalcShift(lastFirePitch, recoilPitchSpeed, TimeTime - lastFireTime, m_serverData.RecoilPitchMin(m_kWeapon.atkType), m_serverData.RecoilPitchMax(m_kWeapon.atkType));
            //            recoilPitch = Mathf.Clamp(recoilPitch, m_serverData.RecoilPitchMin, m_serverData.RecoilPitchMax);
        }
        else
        {
            recoilPitch = 0;
            recoilPitchSpeed = 0;
            lastFirePitch = 0;
        }

        //yaw分量
        if (TimeTime < endYawTime)
        {
            recoilYaw = CalcShift(lastFireYaw, recoilYawSpeed, TimeTime - lastFireTime, m_serverData.RecoilYawMin(m_kWeapon.atkType), m_serverData.RecoilYawMax(m_kWeapon.atkType));
            //            recoilYaw = Mathf.Clamp(recoilYaw, m_serverData.RecoilYawMin, m_serverData.RecoilYawMax);
        }
        else
        {
            recoilYaw = 0;
            recoilYawSpeed = 0;
            lastFireYaw = 0;
        }

        SceneCamera.singleton.localRotation = Quaternion.Euler(-recoilPitch, recoilYaw, 0);

        if (m_kPlayerStatus.stand)
        {
            var standSpreadInitial = m_serverData.StandSpreadInitial(m_kWeapon.atkType);
            if (spread >= standSpreadInitial)
                spread -= Time.deltaTime * TimeToSpeed(m_kWL.StandSpreadDecaySpeed);
            if (spread < standSpreadInitial)
                spread = standSpreadInitial;
        }
        else if (m_kPlayerStatus.crouch)
        {
            var crouchSpreadInitial = m_serverData.CrouchSpreadInitial(m_kWeapon.atkType);
            if (spread >= crouchSpreadInitial)
                spread -= Time.deltaTime * TimeToSpeed(m_kWL.CrouchSpreadDecaySpeed);
            if (spread < crouchSpreadInitial)
                spread = crouchSpreadInitial;
        }

        if (m_kPlayerStatus.move)
        {
            spreadMovement = m_kWeapon.ower.clientMoveSpeed * m_kWL.SpreadMovementFactor;
            spreadMovement = Mathf.Min(spreadMovement, m_serverData.SpreadMovementMax(m_kWeapon.atkType));
        }
        else if (spreadMovement > 0)
        {
            spreadMovement -= Time.deltaTime * TimeToSpeed(m_kWL.SpreadMovementDecaySpeed);
            if (spreadMovement < 0)
                spreadMovement = 0;
        }

        if (spreadJump > 0 && !m_kPlayerStatus.jump)
        {
            spreadJump -= Time.deltaTime * TimeToSpeed(m_kWL.SpreadJumpDecaySpeed);
            if (spreadJump < 0)
                spreadJump = 0;
        }

    }

    #region get set
    private float TimeTime { get { return Time.timeSinceLevelLoad; } }

    private float lastFireTime
    {
        get { return m_lastFireTime.val; }
        set { m_lastFireTime.val = value; }
    }

    private float endPitchTime
    {
        get { return m_endPitchTime.val; }
        set { m_endPitchTime.val = value; }
    }

    private float endYawTime
    {
        get { return m_endYawTime.val; }
        set { m_endYawTime.val = value; }
    }

    private float lastFirePitch
    {
        get { return m_lastFirePitch.val; }
        set { m_lastFirePitch.val = value; }
    }

    private float lastFireYaw
    {
        get { return m_lastFireYaw.val; }
        set { m_lastFireYaw.val = value; }
    }

    private int fireIndex
    {
        get { return m_fireIndex.val; }
        set { m_fireIndex.val = value; }
    }


    private float recoilCount
    {
        get { return m_recoilCount.val; }
        set { m_recoilCount.val = value; }
    }

    private float recoilPitchSpeed
    {
        get { return m_recoilPitchSpeed.val; }
        set { m_recoilPitchSpeed.val = value; }
    }

    private float recoilYawSpeed
    {
        get { return m_recoilYawSpeed.val; }
        set { m_recoilYawSpeed.val = value; }
    }

    private float recoilPitch
    {
        get { return m_recoilPitch.val; }
        set { m_recoilPitch.val = value; }
    }

    private float recoilYaw
    {
        get { return m_recoilYaw.val; }
        set { m_recoilYaw.val = value; }
    }


    private float spread
    {
        get { return m_spread.val; }
        set { m_spread.val = value; }
    }

    private float spreadFinal
    {
        get { return m_spreadFinal.val; }
        set { m_spreadFinal.val = value; }
    }

    private float spreadMovement
    {
        get { return m_spreadMovement.val; }
        set { m_spreadMovement.val = value; }
    }

    private float spreadJump
    {
        get { return m_spreadJump.val; }
        set { m_spreadJump.val = value; }
    }

    #endregion

}
