﻿using UnityEngine;
using System.Collections;

public class MonoBulletFlashBomb : MonoBulletBase {


    protected override void DoDamage(BasePlayer bp = null)
    {
        if (MainPlayer.singleton == null)   // 主角扔的，别人扔的，都是一样的逻辑，闪光前端自己处理，后端是不知道闪了谁的
            return;

        ConfigItemWeaponLine weaponConfig = m_bullet.weaponConfig;
        float maxTime = weaponConfig.Damage;
        float minTime = weaponConfig.DamageDecayMax;

        float radius1 = weaponConfig.DamageDecayStartDis;
        float radius2 = weaponConfig.DamageDecayEndDis;
        float lastTime = 0;

        float dis = Vector3.Distance(MainPlayer.singleton.position, position);

        if (dis > radius2)
        {
            return;
        }

        if(dis <= radius1 && isPhysiclyVisible == true)
        {
            lastTime = isInSight ? maxTime : maxTime * 0.6f;
        }
        else if (dis <= radius2 && isInSight == true && isPhysiclyVisible == true)
        {
            lastTime = maxTime - (dis - radius1) / (radius2 - radius1) * (maxTime - minTime);
        }

        if (lastTime != 0)
        {
            if (ScreenEffectManager.singleton != null)
                ScreenEffectManager.singleton.GetEffect(ScreenEffectManager.EFFECT_FLASH).Show(lastTime, weaponConfig.FlashBombScreenPic);
        }
    }

    

 
}
