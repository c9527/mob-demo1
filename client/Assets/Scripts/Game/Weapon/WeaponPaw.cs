﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WeaponPaw : WeaponKnife
{
    public override void LoadRes(System.Action<WeaponBase, bool> funLoaded, string emptyGOName = null)
    {
        base.LoadRes(funLoaded, "Paw");
    }

    //public override void LoadRes(System.Action<WeaponBase, bool> funLoaded = null)
    //{
    //    m_bIsEmptyModel = true;
    //    if (gameObject == null)
    //        SetGO(new GameObject("Paw"));

    //    if (funLoaded != null)
    //        funLoaded(this, true);
    //}
}