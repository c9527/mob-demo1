﻿class WeaponConst
{
    //WeaponMotion
    public const int Weapon_Motion_Scope_ZoomIn = 0;
    public const int Weapon_Motion_Scope_ZoomOut = 1;
    public const int Weapon_Motion_Scope_ZoomOutImmediate = 2;
}