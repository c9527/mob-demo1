﻿using UnityEngine;
using System.Collections;

public class WeaponBom : WeaponBase
{

    private int m_iSwitchAniTimer = -1;


    public override void Release()
    {
        base.Release();

        if(m_iSwitchAniTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iSwitchAniTimer);
        }
    }

#region Fire

    protected override void TryToFire()
    {
       
    }

#endregion


#region Swtich
    public override void WeaponSwitchOff()
    {
        Logger.Warning("C4切走 WeaponSwitchOff");
        GameDispatcher.Dispatch<bool>(GameEvent.WEAPON_C4_SWITCH, false);
        if (m_iSwitchAniTimer != -1)
            TimerManager.RemoveTimeOut(m_iSwitchAniTimer);
        m_iSwitchAniTimer = -1;             
    }

    public override void WeaponSwitched()
    {
        base.WeaponSwitched();
        Logger.Warning("C4切出 WeaponSwitched");
        GameDispatcher.Dispatch<bool>(GameEvent.WEAPON_C4_SWITCH, true);
        // 这样写可能会有问题，因为武器加载需求时间，而此时主角动作已经开始播放了
        m_iSwitchAniTimer = TimerManager.SetTimeOut(m_kWL.SwitchTime, () =>
        {
            m_iSwitchAniTimer = -1;
            m_kPlayer.SwitchPreWeapon();
        });
    }


#endregion


}
