﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 散弹枪
/// </summary>
public class WeaponShotGun : WeaponGun
{
#region Fire

    //protected override void TryToFire()
    //{
    //    if (ammoNumInClip >= 0)
    //    {
    //        base.TryToFire();
    //    } 
    //}

    protected override void DoFireReally()
    {
        if (m_iReloadTimer != -1)
            TimerManager.RemoveTimeOut(m_iReloadTimer);
        m_iReloadTimer = -1;
        m_kPlayer.status.reload = false;
        //m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
        base.DoFireReally();   
    }

    override protected void DoWeaponFireLogic()
    {
        if (m_kEffectGunFire != null)
            m_kEffectGunFire.Play();

        if (m_kEffectGunFire2 != null)
            m_kEffectGunFire2.Play();

        AttachAndPlayCartridgeEffect();

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
        {
            m_kSP.BeginGunFireSound(new[] { m_kWL.FireSound[1] }, m_kWL.FireSoundProbability, m_kWL.FireSoundSpawn[0],
                m_kWL.FireSoundSpawn[1], AudioManager.FightSoundVolume);
        }

        int pelletNum = m_kWL.Pellets;
        pelletNum = pelletNum <= 0 ? 1 : pelletNum;

        ReduceAmmoNum();
        int bulletCnt = protoBulletCnt;
        bool isHitEnemy;
        bool hitOneEnemy;

        WorldManager.singleton.EnableRoleBodyPartCollider(true);

        var spread = m_kSpread.ApplySpread();
        tos_fight_fire fireArg = DoMainPlayerRayTest(spread, bulletCnt, true, out hitOneEnemy);
        List<p_hit_info[]> list = new List<p_hit_info[]>();
        list.AddRange(fireArg.hitList);
        for (int i = 1; i < pelletNum; i++)
        {
            tos_fight_fire hitData = DoMainPlayerRayTest(spread, bulletCnt, !hitOneEnemy, out isHitEnemy);
            if (!hitOneEnemy && isHitEnemy)
                hitOneEnemy = true;
            list.AddRange(hitData.hitList);
        }

        WorldManager.singleton.EnableRoleBodyPartCollider(false);

        if (m_fireCallBack != null)
        {
            m_fireCallBack();
            m_fireCallBack = null;
        }

        fireArg.hitList = list.ToArray();
        fireArg.timeStamp = Time.timeSinceLevelLoad;
        WorldManager.SendUdp(fireArg);

        if (!hitOneEnemy)
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_WRONG_ATK);

        if(m_isSubWeapon)
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subAmmoNum);
        else
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);    // 在发送完协议后，再让界面更新，界面更新会设置useAdv=false


        if (m_kWL.FireAni.Length >= 1 && m_kAni.GetClip(m_kWL.FireAni[0]) != null)
            m_kAni.Play(m_kWL.FireAni[0]);
    }


#endregion

#region OnKey
    override protected string GetMPWeaponFireAni(int fireType)
    {
        if (isBothHandSlot)
        {
            //if (!m_bIsFireBefore)
            //    m_iAniIndex = 0;
            //else
            m_iAniIndex = (m_iAniIndex == m_kWL.MPFireAni.Length - 1) ? 0 : m_iAniIndex + 1;
            return m_kWL.MPFireAni[m_iAniIndex];
        }

        m_iAniIndex = -1;

        if (m_kWL != null && m_kWL.MPFireAni.Length >= 1)
        {
            m_iAniIndex = 0;
        }

        return m_iAniIndex == -1 ? "" : m_kWL.MPFireAni[m_iAniIndex];
    }
#endregion
}
