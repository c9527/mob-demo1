﻿using UnityEngine;
using System.Collections;
using System;


public class GunScope
{
    protected const int STATUS_ZOOMIN = 1;      //"status_zoomin";
    protected const int STATUS_ZOOMOUT = 2;     //"status_zoomout";
    protected const int STATUS_ZOOM = 3;        //"status_zoom";
    protected const int STATUS_IDLE = 4;        //"status_idle";

    private EptInt m_status = STATUS_IDLE;

    private WeaponGun m_kGun;
    private StepUpdate m_suZoomInSceneFov = new StepUpdate();
    private StepUpdate m_suZoomInMainplayerFov = new StepUpdate();
    private StepUpdate m_suZoomOutSceneFov = new StepUpdate();
    private StepUpdate m_suZoomOutMainplayerFov = new StepUpdate();

    private Action m_funZoonInComplete;
    private Action m_funZoomOutComplete;

    private EptFloat m_targetFov;
    private EptFloat m_mainTargetFov;

    public GunScope(WeaponGun kGun)
    {
        m_kGun = kGun;
        Init();
    }

    private void Init()
    {
        Action<float> funZoomStepSceneFov = (fTickFov) =>
        {
            if(m_kGun.isMainPlayer)
                MainPlayer.singleton.SetSceneFov(fTickFov);
            else if(m_kGun.isFPWatchPlayer)
                FPWatchPlayer.singleton.SetSceneFov(fTickFov);
        };

        Action<float> funZoomStepMainPlayerFov = (fTickFov) =>
        {
            if (m_kGun.isMainPlayer)
                MainPlayer.singleton.SetMainPlayerFov(fTickFov);
            else if (m_kGun.isFPWatchPlayer)
                FPWatchPlayer.singleton.SetMainPlayerFov(fTickFov);
        };

        Action funZoomInComplete = () =>
        {
            m_status = STATUS_ZOOM;
            if (m_kGun.isFPV)
                GameDispatcher.Dispatch<int>(GameEvent.GAME_SHOW_GUNSCOPE, m_kGun.weaponConfigLine.ZoomMode);
            if (m_funZoonInComplete != null)
                m_funZoonInComplete();
        };

        m_suZoomInSceneFov.SetCallBack(funZoomStepSceneFov, funZoomInComplete);
        m_suZoomInMainplayerFov.SetCallBack(funZoomStepMainPlayerFov, null);
        
        Action funZoomOutComplete = () =>
        {
            m_status = STATUS_IDLE;
            if (m_funZoomOutComplete != null)
                m_funZoomOutComplete();
        };

        m_suZoomOutSceneFov.SetCallBack(funZoomStepSceneFov, funZoomOutComplete);
        m_suZoomOutMainplayerFov.SetCallBack(funZoomStepMainPlayerFov, null);

        UpdateConfig();
    }

    public void UpdateConfig()
    {
        //float initFov = GameSetting.FOV_SCENE_CAMERA_DEFAULT;
        //ConfigItemRoleLine rl = m_kGun.ower.configData;
        //if (rl != null)
        //    initFov = rl.Fov;

        ConfigItemWeaponLine kWL = m_kGun.weaponConfigLine;
        float factor = kWL.ZoomFactor <= 0 ? 1 : kWL.ZoomFactor;
        //float targetFov = initFov / factor;
        m_targetFov = 1 / factor;

        m_mainTargetFov = GameSetting.FOV_MAINPLAYER_CAMERA / GameSetting.FOV_SNIPER_MAINPLAYER_CAMERA;

        m_suZoomInSceneFov.SetData2(1, m_targetFov, kWL.ZoomInTime);
        m_suZoomInMainplayerFov.SetData2(1, m_mainTargetFov, kWL.ZoomInTime);
        m_suZoomOutSceneFov.SetData2(m_targetFov, 1, kWL.ZoomOutTime);
        m_suZoomOutMainplayerFov.SetData2(m_mainTargetFov, 1, kWL.ZoomOutTime);
    }

    public void Release()
    {
        m_suZoomInSceneFov.Release();
        m_suZoomOutSceneFov.Release();
        m_suZoomInMainplayerFov.Release();
        m_suZoomOutMainplayerFov.Release();

        m_suZoomInSceneFov = null;
        m_suZoomOutSceneFov = null;
        m_suZoomInMainplayerFov = null;
        m_suZoomOutMainplayerFov = null;
        m_kGun = null;

        m_funZoomOutComplete = null;
        m_funZoonInComplete = null;
    }

    public void SetCallBack(Action funZoomInComplete, Action funZoomOutComplete)
    {
        m_funZoonInComplete = funZoomInComplete;
        m_funZoomOutComplete = funZoomOutComplete;
    }

    public void ZoomIn(bool setMainPlayerCameraFov = false)
    {
        if (m_status != STATUS_IDLE)
            return;

        m_status = STATUS_ZOOMIN;
        m_suZoomInSceneFov.Start();
        m_suZoomInMainplayerFov.Start();
    }

    public void ZoomOut()
    {
        //if (m_strStatus != STATUS_ZOOMING)
            //return;

        m_status = STATUS_ZOOMOUT;
        m_suZoomOutSceneFov.Start();
        m_suZoomOutMainplayerFov.Start();

        if (m_kGun.isFPV)
            GameDispatcher.Dispatch(GameEvent.GAME_SHOW_GUNSCOPE, 0);
    }

    public void ZoomInImmediate()
    {
        if (m_status != STATUS_IDLE)
            return;

        if (m_kGun.isMainPlayer)
        {
            MainPlayer.singleton.SetSceneFov(m_targetFov);
            MainPlayer.singleton.SetMainPlayerFov(m_mainTargetFov);
        }
        else if (m_kGun.isFPWatchPlayer)
        {
            FPWatchPlayer.singleton.SetSceneFov(m_targetFov);
            FPWatchPlayer.singleton.SetMainPlayerFov(m_mainTargetFov);
        }

        m_status = STATUS_ZOOM;
        GameDispatcher.Dispatch<int>(GameEvent.GAME_SHOW_GUNSCOPE, m_kGun.weaponConfigLine.ZoomMode);
        if (m_funZoonInComplete != null)
            m_funZoonInComplete();
    }

    // 立马关镜，没有渐变过程
    public void ZoomOutImmediate()
    {
        m_status = STATUS_IDLE;
        if (m_kGun.isMainPlayer)
        {
            GameDispatcher.Dispatch(GameEvent.GAME_SHOW_GUNSCOPE, 0);
            MainPlayer.singleton.ResetCameraFov();
        }
        else if(m_kGun.isFPWatchPlayer)
        {
            GameDispatcher.Dispatch(GameEvent.GAME_SHOW_GUNSCOPE, 0);
            FPWatchPlayer.singleton.ResetCameraFov();
        }
    }
    public void Update()
    {
        if (m_status == STATUS_ZOOMIN)
        {
            m_suZoomInSceneFov.Update();
            m_suZoomInMainplayerFov.Update();
        }
        else if (m_status == STATUS_ZOOMOUT)
        {
            m_suZoomOutSceneFov.Update();
            m_suZoomOutMainplayerFov.Update();
        }
    }

    public bool isZoomIn
    {
        get { return m_status == STATUS_ZOOMIN; }
    }

    public bool isZoomOrOut
    {
        get { return m_status == STATUS_ZOOM || m_status == STATUS_ZOOMOUT; }
    }

    public bool isZoomInOrOut
    {
        get { return m_status == STATUS_ZOOMIN || m_status == STATUS_ZOOMOUT; }
    }

    public bool isZoom
    {
        get { return m_status == STATUS_ZOOM; }
    }

    public bool isIdle
    {
        get { return m_status == STATUS_IDLE; }
    }

   

}
