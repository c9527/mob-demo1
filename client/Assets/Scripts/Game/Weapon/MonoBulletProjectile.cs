﻿using UnityEngine;
using System.Collections;

public class MonoBulletProjectile : MonoBulletBase 
{
    /*
    override public void Fire(Vector3 startPos, Vector3 startRot, Vector3 fireDir, ConfigItemBulletLine cb, BasePlayer player)
    {
        Reset();

        ActiveComponent(true);

        m_cb = cb;
        m_player = player;
        m_isMainPlayer = player is MainPlayer;

        m_bStarted = true;
        m_strStatus = STATUS_FLY;

        m_trans.SetParent(null);
        m_startPos = startPos;
        m_trans.position = startPos;
        fireDir.Normalize();

        m_trans.eulerAngles = startRot;

        m_rd.AddForce(fireDir * m_cb.Speed, ForceMode.VelocityChange);

        m_pp = GameObjectHelper.GetComponent<PreventPenetrate>(gameObject);
        m_pp.Speed = m_cb.Speed;
        m_pp.Run();

        PlayTailEffect();

        if (m_cb.Gravity == 0 && m_cb.Resilient == 0)
        {
            Ray shootRay;
            RaycastHit hitInfo; ;
            if (m_isMainPlayer || player is FPWatchPlayer)
            {
                shootRay = AutoAim.CurAimRay;
            }
            else
            {
                shootRay = new Ray(startPos, fireDir);
            }

            WorldManager.singleton.EnableRoleBodyPartCollider(true);
            if (Physics.Raycast(shootRay, out hitInfo, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_AIR_WALL | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER))
            {
                m_explorePoint = hitInfo.point;
                m_fLineraflightExploreTime = Vector3.Distance(startPos, m_explorePoint) / m_cb.Speed;
                m_strStatus = STATUS_LINERAFLIGHT;
            }
            WorldManager.singleton.EnableRoleBodyPartCollider(false);
        }
    }
    */

}
