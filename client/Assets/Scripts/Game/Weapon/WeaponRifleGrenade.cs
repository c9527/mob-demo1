﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


/// <summary>
/// 枪榴弹
/// </summary>
public class WeaponRifleGrenade : WeaponGun
{
    // TODO: 不同模型高度不同
    protected const float THROW_HEIGHT_STAND = 2.0f;
    protected const float THROW_HEGITH_CROUCH = 1.0f;
    protected const float THROW_OFFSET_ANGLE = 5f;

    protected const float THROW_ANI_FRAME_TIME = 0.3f;
    protected const float THROW_ANI_TIME = 0.6f;

    protected const int FIRE_POSE_PULLPIN = 0;
    protected const int FIRE_POSE_THROW = 1;

    protected string m_bulletType = "";

    protected bool m_bFirePressed;
    protected Vector3 m_vec3HitPos;

    protected int m_iFireFrameTimer;
    protected int m_iFireEndTimer;

    protected int m_bulletIndex;

    private float m_switchSoundPlayTime;


    //弓箭专用
    public static double powerOnTime = 0;//蓄力时间
    public static DateTime powerOnStartTime;
    public static DateTime powerOnEndTime;
    public WeaponRifleGrenade()
    {
        m_bulletType = BulletBase.TYPE_RIFLEGRENADE;
        m_kAni = new FPSAnimation();
    }

    override public void Release()
    {
        base.Release();

        m_bFirePressed = false;

        if(m_iFireFrameTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iFireFrameTimer);
            m_iFireFrameTimer = -1;
        }

        if(m_iFireEndTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iFireEndTimer);
            m_iFireEndTimer = -1;
        }

        m_bulletIndex = 0;
        m_switchSoundPlayTime = 0;
    }
#region Fire
    virtual protected void Throw()
    {
        m_weaponMgr.usingThrowWeapon = true;

        m_bulletIndex = ProjectileManager.GetNextBulletIndex();

        m_kPlayer.playerAniController.CrossFade(m_kWL.MPFireAni[0], 0.2f);
        if (m_kWL.FireDelay < 0)
            OnFireFrame();
        else
            m_iFireFrameTimer = TimerManager.SetTimeOut(m_kWL.FireDelay == 0 ? THROW_ANI_FRAME_TIME : m_kWL.FireDelay, OnFireFrame);
        if (m_kWL.FireAfterDelay < 0)
            OnFireEndFrame();
        else
            m_iFireEndTimer = TimerManager.SetTimeOut(m_kWL.FireAfterDelay == 0 ? THROW_ANI_TIME : m_kWL.FireAfterDelay, OnFireEndFrame);

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 3)
            m_kSP.PlayOneShot(m_kWL.FireSound[2], AudioManager.FightSoundVolume);

        if (isFPV && m_kWL.FireSound.Length >= 4)
            AudioManager.PlayFightUIVoice(m_kWL.FireSound[3], true);
    }

    internal override void Atk(toc_fight_atk arg)
    {
        if (( !isSubWeapon && gameObject == null )|| m_kPlayer.serverData.alive == false)
            return;

        if (m_kPlayer is OtherPlayer)
        {
            if (m_bIsWalkWaggle && m_kPlayer.playerAniController.animator != null && m_kPlayer.playerAniController.animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                m_kPlayer.playerAniController.EnableWalkWaggleLayer(false);
            m_kPlayer.playerAniController.CrossFade(m_kWL.OPFireAni[0], 0.2f, 1);
            m_vec3HitPos = arg.fireDir;
            if (m_kWL.FireDelay < 0)
                OnFireFrame();
            else
                m_iFireFrameTimer = TimerManager.SetTimeOut(m_kWL.FireDelay == 0 ? THROW_ANI_FRAME_TIME : m_kWL.FireDelay, OnFireFrame);
            if (m_kWL.FireAfterDelay < 0)
                OnFireEndFrame();
            else
                m_iFireEndTimer = TimerManager.SetTimeOut(m_kWL.FireAfterDelay == 0 ? THROW_ANI_TIME : m_kWL.FireAfterDelay, OnFireEndFrame);
        }
        else
        {
            Throw();
        }
    }

    protected virtual void OnFireFrame()
    {
        if (m_released)
            return;
        float speed = 0;//基础速度
        Vector3 fireDir;
        Vector3 startPos = GetStartPos();
        if (isMainPlayer)
        {
            if (m_isSubWeapon)
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subAmmoNum);
            else
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);

            RaycastHit rh;
            Vector3 hitPos;

            if (Physics.Raycast(SceneCamera.singleton.ray, out rh, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER))
            {
                hitPos = rh.point;
                Vector3 dir = rh.point - startPos;
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * dir;
            }
            else
            {
                hitPos = SceneCamera.singleton.position + SceneCamera.singleton.forward * 50.0f;
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * SceneCamera.singleton.forward;
            }
            if (m_kWL.Class == WeaponManager.WEAPOM_TYPE_BOW)
            {
                NetLayer.Send(new tos_fight_poweroff()
                {
                    fireDir = hitPos,
                    index = m_bulletIndex,
                    atkType = atkType,
                    timeStamp = MainPlayer.singleton.powerOnEndTime
                });
            }
            else
            {
                NetLayer.Send(new tos_fight_atk()
                {
                    fireDir = hitPos,
                    index = m_bulletIndex,
                    atkType = atkType
                });
            }
            float poweronTime = MainPlayer.singleton.powerOnEndTime - MainPlayer.singleton.powerOnStartTime;
            if (m_kBL.MaxSpeed != 0)
            {
                if (poweronTime >= m_kWL.MaxPowerOnTime)
                {
                    speed = m_kBL.MaxSpeed;
                }
                else if (poweronTime <= m_kWL.MinPowerOnTime)
                {
                    speed = m_kBL.Speed;
                }
                else
                {
                    speed = (m_kBL.MaxSpeed - m_kBL.Speed) / (m_kWL.MaxPowerOnTime - m_kWL.MinPowerOnTime) * (float)(poweronTime) + m_kBL.Speed;
                }
            }
            MainPlayer.singleton.powerOnStartTime = 0;
            MainPlayer.singleton.powerOnEndTime = 0;
        }
        else
        {
            fireDir = Vector3.Normalize(m_vec3HitPos - startPos);
            if (m_kPlayer.transform != null)
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, m_kPlayer.transform.right) * fireDir;

            float poweronTime = m_kPlayer.powerOnEndTime - m_kPlayer.powerOnStartTime;
            if (m_kBL.MaxSpeed != 0)
            {
                if (poweronTime >= m_kWL.MaxPowerOnTime)
                {
                    speed = m_kBL.MaxSpeed;
                }
                else if (poweronTime <= m_kWL.MinPowerOnTime)
                {
                    speed = m_kBL.Speed;
                }
                else
                {
                    speed = (m_kBL.MaxSpeed - m_kBL.Speed) / (m_kWL.MaxPowerOnTime - m_kWL.MinPowerOnTime) * (float)(poweronTime) + m_kBL.Speed;
                }
            }
            m_kPlayer.powerOnStartTime = 0;
            m_kPlayer.powerOnEndTime = 0;

        }

        BulletBase bullet = BulletManager.CreateBullet(m_bulletType, m_kWL, isFPV);

        bullet.Fire(startPos, Vector3.zero, fireDir, m_kWL, m_kPlayer, m_bulletIndex,speed);


        //Logger.Error(fireDir.ToString());
        m_weaponMgr.usingThrowWeapon = false;
        PanelBattle.powerOn = false;
    }

    protected virtual void OnFireEndFrame()
    {
        if (m_released)
            return;

        m_iFireEndTimer = -1;
        if (!m_isSubWeapon && m_kPlayer is MainPlayer && m_bIsSkillFire)
        {
            if (m_fireCallBack != null)
            {
                m_fireCallBack();
                m_fireCallBack = null;
            }
        }
    }

    protected override void TryToFire()
    {
        base.TryToFire();
        
        if (TestWeapon() == WEAPON_TEST_SUCCESS)
        {

            Throw();
        }

    }

    protected override void DoFireReally()
    {
         
    }

#endregion  

#region Switch

    public override void WeaponSwitchOff()
    {
        base.WeaponSwitchOff();

        if (m_kPlayer.weaponMgr.curWeapon == this)
            return;

        if (m_iFireFrameTimer != -1)
            TimerManager.RemoveTimeOut(m_iFireFrameTimer);
        if (m_iFireEndTimer != -1)
            TimerManager.RemoveTimeOut(m_iFireEndTimer);
    }
#endregion

    virtual protected Vector3 GetStartPos()
    {

        if(m_kPlayer is MainPlayer)
        {
            if (isSubWeapon)//子武器
            {
                return (m_kPlayer.curWeapon is WeaponGun) ?
                    SceneCamera.singleton.camera.ScreenToWorldPoint(MainPlayer.singleton.cameraMainPlayer.WorldToScreenPoint((m_kPlayer.curWeapon as WeaponGun).rifleGrenadeFirePoint)) :
                    (m_kWL.SlotType == WEAPON_SLOTTYPE_RIGHTHAND ? m_kPlayer.rightHandSlot.position : m_kPlayer.leftHandSlot.position);
            }
            else if (m_kWL.Class == WeaponManager.WEAPOM_TYPE_BOW)
            {
                return firePoint;
            }
            else if (m_bIsEmptyModel)
            {
                return m_kWL.SlotType == WEAPON_SLOTTYPE_RIGHTHAND ? m_kPlayer.rightHandSlot.position : m_kPlayer.leftHandSlot.position;
            }
            return SceneCamera.singleton.camera.ScreenToWorldPoint(MainPlayer.singleton.cameraMainPlayer.WorldToScreenPoint(rifleGrenadeFirePoint));
            
        }
        else
        {
            if (isSubWeapon) //子武器
            {
                return (m_kPlayer.curWeapon is WeaponGun) ? (m_kPlayer.curWeapon as WeaponGun).rifleGrenadeFirePoint :
                     (m_kWL.SlotType == WEAPON_SLOTTYPE_RIGHTHAND ? m_kPlayer.rightHandSlot.position : m_kPlayer.leftHandSlot.position);
            }
            return rifleGrenadeFirePoint;
        }
    }

#region OnKey

    //override public void OnFireKeyDown()
    //{
    //    PlayerStatus status = m_kPlayer.status;
    //
    //    if (status.reload || !canFire)
    //        return;
    //
    //    if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
    //        return;
    //
    //    if (hasAmmoCurClip)
    //    {
    //        m_bFirePressed = true;
    //    }
    //}
    //
    //override public void OnFireKeyUp()
    //{
    //    if (m_bFirePressed == false)
    //        return;
    //
    //    PlayerStatus status = m_kPlayer.status;
    //    if (!m_kPlayer.alive || status.reload)
    //        return;
    //
    //    m_bFirePressed = false;        
    //    TryToFire();
    //}
#endregion

}
