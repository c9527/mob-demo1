﻿using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 武器的加载，缓存，管理
/// </summary>
public class WeaponManager
{
    public const int WEAPON_TYPE_PISTOL = 1;
    public const int WEAPON_TYPE_RIFLE = 2;
    public const int WEAPON_TYPE_SUBMACHINE_GUN = 3;
    public const int WEAPON_TYPE_SHOT_GUN = 4;
    public const int WEAPON_TYPE_MACHINE_GUN = 5;
    public const int WEAPON_TYPE_SNIPER_GUN = 6;
    public const int WEAPON_TYPE_KNIFE = 7;
    public const int WEAPON_TYPE_GRENADE = 8;
    public const int WEAPON_TYPE_BOM = 9;
    public const int WEAPON_TYPE_PAW = 10;
    public const int WEAPON_TYPE_RIFLEGRENADE = 11;
    public const int WEAPON_TYPE_PAWGUN = 12;
    public const int WEAPON_TYPE_FLASHBOMB = 13;
    public const int WEAPOM_TYPE_BOW = 14;


    private Dictionary<string, WeaponBase> m_dicWeapon;     
    private BasePlayer m_kPlayer;
    private WeaponBase m_kCurWeapon;
    private WeaponBase m_kSubWeapon;
    private string m_strPreWeapon;
    private bool m_usingThrowWeapon;


    public WeaponManager(BasePlayer kPlayer)
    {
        m_kPlayer = kPlayer;
        Init();
    }

    private void Init()
    {
        m_dicWeapon = new Dictionary<string, WeaponBase>();
        GameDispatcher.AddEventListener(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE, UpdateWeaponConfigReference);
    }

    public void Run()
    {
        Init();
    }

    public void Stop()
    {
        ReleaseWeapon();
        GameDispatcher.RemoveEventListener(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE, UpdateWeaponConfigReference);
    }

    /// <summary>
    ///  玩家Release时调用
    /// </summary>
    public void Release()
    {
        Stop();
    }

    /// <summary>
    ///  在人物模型变化的时候，会释放武器资源
    /// </summary>
    public void ReleaseWeapon()
    {
        foreach (KeyValuePair<string, WeaponBase> kvp in m_dicWeapon)
        {
            if (!kvp.Value.released)
                kvp.Value.Release();
        }

        m_dicWeapon.Clear();
        curWeapon = null;
        subWeapon = null;
    }

    public void PlayerDieOrLock()
    {
        if(m_kPlayer is MainPlayer|| m_kPlayer is FPWatchPlayer)
        {
            if (curWeapon != null)
            {
                curWeapon.Reset();
                curWeapon.StopFire();
                curWeapon.JumpAni2End();
            }
        }

        if(curWeapon != null)
            curWeapon.SetActive(false);

        usingThrowWeapon = false;
        curWeapon = null;
        preWeapon = "";
    }

    public bool SwitchWeapon(string iWeaponID)
    {
        WeaponBase kWeapon = null;
        m_dicWeapon.TryGetValue(iWeaponID, out kWeapon);

        if (kWeapon == null)
        {
            kWeapon = CreateWeapon(iWeaponID);
            if (kWeapon != null)
                m_dicWeapon.Add(iWeaponID, kWeapon);
            else
                return false;
        }

        kWeapon.Reset();

        LocalWeaponData kLWD = m_kPlayer.localPlayerData.GetLocalWeaponData(iWeaponID);
        kWeapon.UpdateLocalWeaponData(kLWD);
        kWeapon.isSubWeapon = false;
        kWeapon.setIsHeal();

        WeaponBase kPreWeapon = curWeapon;
        curWeapon = kWeapon;
        subWeapon = null;

        if (kPreWeapon != null && kPreWeapon != curWeapon)
        {
            //string subType = kPreWeapon.weaponConfigLine.SubType;
            //if (subType == GameConst.WEAPON_SUBTYPE_WEAPON1 || subType == GameConst.WEAPON_SUBTYPE_WEAPON2 || subType == GameConst.WEAPON_SUBTYPE_DAGGER)
            //    m_strPreWeapon = kPreWeapon.weaponId;
            
            if(m_kPlayer.serverData.hasWeapon(int.Parse(kPreWeapon.weaponId)))
                m_strPreWeapon = kPreWeapon.weaponId;
            else
                m_strPreWeapon = "";
        }

        //if(m_kPlayer is MainPlayer)
        //    Logger.Log("切枪 " + iWeaponID);

        if (kPreWeapon != null)
        {
            kPreWeapon.WeaponSwitchOff();
            kPreWeapon.SetActive(false);
            //Logger.Log("PreWeapon SetActive(false) " + kPreWeapon.weaponId);
        } 

        kWeapon.LoadRes((weapon, success) =>  
        {
            if (!success)
            {
                Logger.Error("加载武器:" + iWeaponID + " 失败");
                return;
            }

            if (m_kPlayer.released || m_kPlayer.serverData == null || m_kPlayer.serverData.roleData == null || m_kPlayer.alive == false)
            {
                weapon.SetActive(false);
                return;
            }
            
            if (curWeapon != null && weapon.weaponId == curWeapon.weaponId)
            {
                weapon.SetActive(true);
                weapon.WeaponSwitched();
                m_kPlayer.WeaponAttachComplete();
                weapon.shareFireCd = 60.0f / weapon.weaponConfigLine.FireRate;
                weapon.relateWeapon = null;

                //第二攻击武器ID大于，即设置副武器
                if (weapon.weaponConfigLine.SubWeapon > 0)
                {
                    string subWeaponID = weapon.weaponConfigLine.SubWeapon.ToString();
                    WeaponBase ksubWeapon = null;
                    m_dicWeapon.TryGetValue(subWeaponID, out ksubWeapon);

                    if (ksubWeapon == null)
                    {
                        ksubWeapon = CreateWeapon(subWeaponID);
                        if (kWeapon != null)
                            m_dicWeapon.Add(subWeaponID, ksubWeapon);
                        else
                            return;
                    }

                    ksubWeapon.Reset();
                    ksubWeapon.UpdateLocalWeaponData(kLWD);
                    ksubWeapon.isSubWeapon = true;
                    kWeapon.setIsHeal();
                    subWeapon = ksubWeapon;
                    weapon.relateWeapon = ksubWeapon;
                    ksubWeapon.relateWeapon = weapon;
                    weapon.shareFireCd = subWeapon.shareFireCd = 60.0f / ( weapon.weaponConfigLine.FireRate > subWeapon.weaponConfigLine.FireRate ? weapon.weaponConfigLine.FireRate : subWeapon.weaponConfigLine.FireRate);

                    if (subWeapon is WeaponGrenade || subWeapon is WeaponRifleGrenade)
                    {
                        if (m_kPlayer is MainPlayer)
                            GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, true);
                    }
                    else if (weapon.gameObject != null)
                    {
                        subWeapon.noCacheGo = true;
                        subWeapon.SetGO(weapon.gameObject);
                        if (subWeapon.isBothHandSlot && weapon.gameObject2 != null)
                            subWeapon.SetGO2(weapon.gameObject2);
                        if (m_kPlayer is MainPlayer)
                            GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, true);
                    }
                }
                else if (m_kPlayer is MainPlayer)
                {
                    GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, false);
                    GameDispatcher.Dispatch<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, true);
                }
                //if (m_kPlayer is MainPlayer)
                //    Logger.Log("Weapon LoadRes SetActive(true) " + weapon.weaponId);
            }
            else
            {
                weapon.SetActive(false);
            }
        });

        return true;
    }

    public WeaponBase TryGetWeapon(string weaponID)
    {
        WeaponBase kWeapon = null;
        m_dicWeapon.TryGetValue(weaponID, out kWeapon);
        return kWeapon;
    }

    
    private WeaponBase CreateWeapon(string weaponID)
    {
        ConfigItemWeapon kCW = ConfigManager.GetConfig<ConfigItemWeapon>();
        ConfigItemWeaponLine kWL = null;
        if (string.IsNullOrEmpty(weaponID) == false)
            kWL = kCW.GetLine(int.Parse(weaponID));
        if (kWL == null)
        {
            Logger.Error("Can't get ConfigWeaponLine, id " + weaponID);
            return null;
        }

        WeaponBase kWeapon = null;
        switch (kWL.Class)
        {
            case WEAPON_TYPE_PISTOL:
            case WEAPON_TYPE_RIFLE:
            case WEAPON_TYPE_SUBMACHINE_GUN:
            case WEAPON_TYPE_MACHINE_GUN:
                kWeapon = NewWeaponObj<WeaponGun>();
                break;
            case WEAPON_TYPE_SHOT_GUN:
                kWeapon = NewWeaponObj<WeaponShotGun>();
                break;
            case WEAPON_TYPE_SNIPER_GUN:
                kWeapon = NewWeaponObj<WeaponSniper>();
                break;
            case WEAPON_TYPE_KNIFE:
                kWeapon = NewWeaponObj<WeaponKnife>();
                break;
            case WEAPON_TYPE_GRENADE:
                kWeapon = NewWeaponObj<WeaponGrenade>();
                break;
            case WEAPON_TYPE_FLASHBOMB:
                kWeapon = NewWeaponObj<WeaponFlashbomb>();
                break;
            case WEAPON_TYPE_BOM:
                kWeapon = NewWeaponObj<WeaponBom>();
                break;
            case WEAPON_TYPE_PAW:
                kWeapon = NewWeaponObj<WeaponPaw>();
                break;
            case WEAPON_TYPE_RIFLEGRENADE:
                kWeapon = NewWeaponObj<WeaponRifleGrenade>();
                break;
            case WEAPON_TYPE_PAWGUN:
                kWeapon = NewWeaponObj<WeaponPawGun>();
                break;
            case WEAPOM_TYPE_BOW:
                kWeapon = NewWeaponObj<WeaponBow>();
                break;
        }

        if (kWeapon == null)
        {
            Logger.Error("CreateWeapon null, Can't match weapontype " + kWL.Type + " name: " + weaponID);
            return null;
        }

        //LocalWeaponData lwd = m_kPlayer.localPlayerData.GetLocalWeaponData(weaponID);     
        kWeapon.BindPlayer(m_kPlayer);
        kWeapon.SetData(kWL);

        return kWeapon;
    }

    private T NewWeaponObj<T>() where T : WeaponBase
    {
        T weapon = GetWeaponObj<T>();
        if (weapon == null)
            weapon = System.Activator.CreateInstance<T>();
        else
        {
            weapon.released = false;
        }

        return weapon;
    }

    public void UpdateWeaponConfigReference()
    {
        foreach (KeyValuePair<string, WeaponBase> kvp in m_dicWeapon)
        {
            kvp.Value.UpdateConfigReference();
        }

        if (m_kPlayer is MainPlayer)
        {
            MainPlayer.singleton.UpdateMoveSpeed();
        }
    }

    public WeaponBase curWeapon
    {
        get { return m_kCurWeapon; }
        set { m_kCurWeapon = value; }
    }

    public WeaponBase subWeapon
    {
        get { return m_kSubWeapon; }
        set { m_kSubWeapon = value; }
    }

    public string preWeapon
    {
        get { return m_strPreWeapon; }
        set { m_strPreWeapon = value; }
    }

    public bool usingThrowWeapon
    {
        get { return m_usingThrowWeapon; }
        set { m_usingThrowWeapon = value; }
    }

    static public void ReleaseCacheRes()
    {
        ReleaseCacheGO();
        ReleaseWeaponObj();
    }

    #region Cache Weapon GameObject
    static private Dictionary<string, List<GameObject>> m_dicWeaponGO = new Dictionary<string, List<GameObject>>();

    static private void ReleaseCacheGO()
    {
        foreach(KeyValuePair<string, List<GameObject>> kvp in m_dicWeaponGO)
        {
            for(int i=0; i<kvp.Value.Count; i++)
            {
                ResourceManager.Destroy(kvp.Value[i]);
            }
        }

        m_dicWeaponGO.Clear();
    }

    static public void CacheWeaponGO(string name, GameObject go)
    {
        if (go == null)
            return;

        List<GameObject> list = null;
        m_dicWeaponGO.TryGetValue(name, out list);
        if(list == null)
        {
            list = new List<GameObject>();
            m_dicWeaponGO.Add(name, list);
        }

        Transform trans = go.transform;
        trans.SetParent(SceneManager.singleton.reclaimRoot);
        trans.ResetLocal();

        list.Add(go);
    }

    static public GameObject GetWeaponGO(string name)
    {
        List<GameObject> list = null;
        m_dicWeaponGO.TryGetValue(name, out list);
        if (list == null || list.Count == 0)
            return null;
        int iRearIndex = list.Count - 1;
        GameObject ret = list[iRearIndex];
        list.RemoveAt(iRearIndex);

        return ret;
    }

    #endregion


    #region Cache Weapon Object
    static private Dictionary<string, List<WeaponBase>> m_dicWeaponObj = new Dictionary<string, List<WeaponBase>>();

    static private void ReleaseWeaponObj()
    {
        m_dicWeaponObj.Clear();
    }

    static public void CacheWeaponObj(WeaponBase weapon)
    {
        if (weapon == null)
            return;

        string className = weapon.GetType().Name;

        List<WeaponBase> lst = null;
        m_dicWeaponObj.TryGetValue(className, out lst);
        if(lst == null)
        {
            lst = new List<WeaponBase>();
            m_dicWeaponObj.Add(className, lst);
        }
        lst.Add(weapon);
    }

    static public T GetWeaponObj<T>() where T : WeaponBase
    {
        List<WeaponBase> lst = null;
        string className = typeof(T).Name;
        m_dicWeaponObj.TryGetValue(className, out lst);

        if (lst == null || lst.Count == 0)
            return null;

        int iRearIndex = lst.Count - 1;
        WeaponBase ret = lst[iRearIndex];
        lst.RemoveAt(iRearIndex);

        //Logger.Warning("GetCache WeaponObj");

        return ret as T;
    }

    #endregion

}

