﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WeaponKnife : WeaponBase
{
    protected FPSAnimation m_kAni;

    private const float RAY_INTERVAL = 0.1f;

    protected Dictionary<int, float> m_dicDamagePlayerInfo = new Dictionary<int, float>();

    protected int m_iDamageTimer;
    protected int m_iAtkLineTick;
    protected Transform m_transAtkPoint;

    private int m_iAniIndex = -1;

    public WeaponKnife()
    {
        m_kAni = new FPSAnimation();
    }

    override public void Reset()
    {
        base.Reset();
        m_bFirePressed = false;
        m_iDamageTimer = -1;
        m_iAtkLineTick = 0;
    }

    override public void Update()
    {
        //m_fFireTick += Time.deltaTime;
        base.Update();

        if (m_bFirePressed)
            OnFireUpdate();
    }

    override public void Release()
    {
        base.Release();

        if (m_kAni != null)
            m_kAni.Release();
        m_dicDamagePlayerInfo.Clear();
        m_iAtkLineTick = 0;
        m_transAtkPoint = null;
        m_iAniIndex = -1;
    }

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        m_transAtkPoint = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
        //if (m_transAtkPoint == null)
        //    Logger.Warning("找不到近战武器特效挂接点!");
        Animation ani = GameObjectHelper.GetComponent<Animation>(go);
        ani.playAutomatically = false;
        m_kAni.SetAnimation(ani);
        LoadWeaponAni();
        base.InitCrossHair();
    }
    //狙击枪准心会将十字放置到距离很远的地方，在切刀时把它移动回来
    
  
#region Fire

    public override string TestWeapon()
    {
        return WEAPON_TEST_SUCCESS;
    }    

    protected override void TryToFire()
    {
        base.TryToFire();

        if (fireRate < (60.0f / 120)) // 刀的最大攻速为100，超过就是内存被改了
        {
            //AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_KnifeFireRate, fireRate.ToString() + "," + atkType + "," + m_isSubWeapon + "," + m_kWL.ID);
            Logger.Error(AntiCheatManager.ErrorCode_KnifeFireRate + " " + fireRate.ToString() + "," + atkType + "," + m_isSubWeapon + "," + m_kWL.ID);
            return;
        }

        DoFireReally();
    }

    protected override void DoFireReally()
    {
        m_kPlayer.status.fire = true;

        if (isMainPlayer)
        {
            if (!m_bIsFireBefore)
                m_iAniIndex = 0;
            else
                m_iAniIndex = (m_iAniIndex == m_kWL.MPFireAni.Length - 1) ? 0 : m_iAniIndex + 1;
        }
        m_kPlayer.playerAniController.Play(m_kWL.MPFireAni[m_iAniIndex]);

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
        {
            m_kSP.PlayOneShot(m_kWL.FireSound[m_iAniIndex + 1], AudioManager.FightSoundVolume);
        }

        string fireAni = "";
        if (m_kWL.FireAni != null && m_kWL.FireAni.Length != 0)
        {
            if (m_kWL.FireAni.Length == 1)
                fireAni = m_kWL.FireAni[0];
            else if (m_kWL.FireAni.Length == 2)
                fireAni = m_kWL.FireAni[m_iAniIndex];
        }

        StopWeaponAni();

        if (m_kAni.GetClip(fireAni) != null)
            m_kAni.Play(fireAni);

        PlayAtkLineEffect();

        //播放人物身上的特效
        PlayMPFireMountEffect();

        m_iDamageTimer = TimerManager.SetTimeOut(m_kWL.DamageDelay, DoRayTest, m_iAniIndex);

        //CDict fireArg = new CDict();
        //fireArg[ServerMsgKey.firePose] = aniIndex;
        //MainPlayer.SendMsg("atk", fireArg);

        if (isMainPlayer)
        {
            NetLayer.Send(new tos_fight_atk()
            {
                firePose = m_iAniIndex,
                atkType = atkType,
                index = -1,
            });
        }

        //StopFire();
    }

    internal override void Atk(toc_fight_atk arg)
    {
        if (gameObject == null || m_kPlayer.alive == false)
            return;

        m_iAniIndex = arg.firePose;
        if (m_kPlayer is OtherPlayer)
        {
            if (m_bIsWalkWaggle && m_kPlayer.playerAniController.animator != null && m_kPlayer.playerAniController.animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                m_kPlayer.playerAniController.EnableWalkWaggleLayer(false);
            m_kPlayer.playerAniController.Play(m_kWL.OPFireAni[m_iAniIndex < m_kWL.OPFireAni.Length ? m_iAniIndex : 0]);
            //m_kPlayer.playerAniController.Play(m_kWL.OPFireAni[arg.firePose]);
            //播放人物身上的特效
            PlayOPFireMountEffect(m_iAniIndex);
            PlayAtkLineEffect();
        }
        else if(isFPWatchPlayer)
        {
            TryToFire();   
        }
    }

    protected void PlayAtkLineEffect()
    {
        if (GameSetting.enableGunFire == false)
            return;

        m_iAtkLineTick++;
        if (m_iAtkLineTick >= m_kWL.BulletEffectFrequency)
        {
            m_iAtkLineTick = 0;

            string strBulletEffect = isFPV ? m_kWL.BulletEffect : m_kWL.BulletEffect2;
            if (string.IsNullOrEmpty(strBulletEffect) == false)
            {
                Vector3 vec3Dir = Vector3.zero;
                Vector3 vecStartPos = m_kPlayer.transform.position;
                float distance = 0;
                if (m_kWL.isAttachPlayer == 0)
                {
                    if (isFPV)
                    {
                        Vector3 vec3RayHitPos = Vector3.zero;
                        distance = m_kWL.BulletEffectSpeed * m_kWL.BulletEffectTime1;
                        int screenHigth = Screen.height;
                        int screenWidth = Screen.width;
                        vec3RayHitPos = SceneCamera.singleton.ScreenToWorldPoint(new Vector3(screenWidth / 2, screenHigth / 2, distance));
                        vec3Dir = vec3RayHitPos - m_transAtkPoint.position;
                        distance = Vector3.Distance(m_transAtkPoint.position, vec3RayHitPos);
                    }
                    else
                    {
                        vec3Dir = m_kPlayer.transform.forward;
                        distance = m_kWL.BulletEffectSpeed2 * m_kWL.BulletEffectTime2;
                    }
                    vecStartPos = m_transAtkPoint.position;

                    PlayAtkLineEffect(vec3Dir, vecStartPos, distance, strBulletEffect);
                }
                else if (m_kWL.isAttachPlayer == 1)
                {
                    PlayAtkLineEffect(strBulletEffect);
                }
            }
        }
    }

    //不跟随直接飞出去
    protected void PlayAtkLineEffect(Vector3 vec3Dir, Vector3 vec3StartPos, float fDis, string strBulletEffect)
    {
        //Logger.Error("forward: " + vec3Dir + ", dis: " + fDis);
        EffectManager.singleton.GetEffect<EffectMove>(strBulletEffect, (kEffect) =>
        {
            float angle = VectorHelper.Angle360(m_transAtkPoint.up, kEffect.transform.up);
            kEffect.Attach(vec3StartPos, vec3Dir, Space.World, null, 0f, angle);
            kEffect.distance = fDis;
            kEffect.speed = isFPV ? m_kWL.BulletEffectSpeed : m_kWL.BulletEffectSpeed2;
            //kEffect.isRelativeSpeed = m_kPlayer is MainPlayer;
            kEffect.timeOut = kEffect.speed > 0 ? fDis / kEffect.speed + 0.1f : 10;
            kEffect.Play();
        });
    }

    //跟随自身直接播放
    protected void PlayAtkLineEffect(string strBulletEffect)
    {
        EffectManager.singleton.GetEffect<EffectTimeout>(strBulletEffect, (kEffect) =>
        {
            if (m_released)
            {
                kEffect.Release();
                return;
            }
                
            Transform trans = isFPV ? SceneCamera.singleton.camera.transform : m_kPlayer.transform;
            float angle = VectorHelper.Angle360(m_transAtkPoint.up, kEffect.transform.up);
            kEffect.Attach(m_transAtkPoint.position, trans.forward, Space.World, trans, 0, angle);
            kEffect.Play();
        });
    }

    override public void StopFire()
    {
        base.StopFire();

        // 有的武器扫射动画时间很长，所以停止扫射时，跳到最后一帧
        //if (m_strFireType == WEAPON_FIRE_TYPE_STRAFE)
        //{
        //    JumpAni2End();
        //    m_kAni.Stop();
        //}

    }

    private void DoRayTest(object arg)
    {
        m_iDamageTimer = -1;
        if (m_kPlayer == null)
            return;

        if (!m_kPlayer.alive || 
            (!m_isSubWeapon && m_kPlayer.weaponMgr.curWeapon != this) ||
            (m_isSubWeapon && m_kPlayer.weaponMgr.subWeapon != this))
            return;

        if (m_kWL.DamageRange.Length != 2)
        {
            Logger.Error("ConfigWeapon.damageRange.Legnth != 2, " + m_kWL.ID);
            return;
        }

        float width = m_kWL.DamageRange[0];
        float height = m_kWL.DamageRange[1];

        //Transform transCamera = SceneManager.singleton.transSceneCamera;
        Vector3 cameraPos = SceneCamera.singleton.position;
        Vector3 vec3CameraForward = SceneCamera.singleton.forward;
        Vector3 vec3CameraRight = SceneCamera.singleton.right;
        Vector3 vec3Origin = cameraPos - vec3CameraRight * width * 0.5f;
        Ray ray = new Ray();
        float dis = 0;
        Dictionary<int, RaycastHit> dictRH = new Dictionary<int, RaycastHit>();
        int dictRHCount = 0;
        RaycastHit kRH;

        int layer = GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND;

        if (isMainPlayer)
            WorldManager.singleton.EnableRoleBodyPartCollider(true);

        while (true)
        {
            vec3Origin += vec3CameraRight * dis;
            ray.origin = vec3Origin;
            ray.direction = vec3CameraForward;

            if (dis == width)
                break;
            dis = dis + RAY_INTERVAL > width ? width : dis + RAY_INTERVAL;

            // RayCast
            if (Physics.Raycast(ray, out kRH, height, layer))
            {
                if (kRH.collider.gameObject.layer == GameSetting.LAYER_VALUE_BUILDING)
                    continue;

                if (kRH.collider.gameObject.layer == GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND && !WorldManager.singleton.isBigHeadModeOpen)
                    continue;

                if (kRH.collider.gameObject.layer == GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND && !WorldManager.singleton.isDotaModeOpen)
                    continue;

                Transform root = kRH.transform.root;

                if (!dictRH.ContainsKey(root.GetInstanceID()))
                {
                    dictRH.Add(root.GetInstanceID(), kRH);  // 同一个对象被射线击中后，返回的RaycastHit对象是同一个，所以这里通过集合来去重
                    var maxHitCount = m_kPlayer.serverData.MaxHitCount(atkType);
                    if (++dictRHCount >= maxHitCount && maxHitCount != 0) //超过击中目标后就不再检测
                    {
                        break;
                    }
                }
            }
        }

        if (dictRHCount <= 0)  // 如果没伤到玩家，试试有没有戳到建筑
        {
            ray.origin = cameraPos;
            ray.direction = vec3CameraForward;
            if (Physics.Raycast(ray, out kRH, height, GameSetting.LAYER_MASK_BUILDING))
            {
                int matType = SceneManager.GetMaterialType(kRH.collider.gameObject);
                int aniIndex = (int)arg;
                float rotZ = 0f;
                if (m_kWL.HitEffectRot.Length >= aniIndex + 1)
                    rotZ = m_kWL.HitEffectRot[aniIndex];

                PlayHitBuildingEffect(kRH.point, kRH.normal, matType, Effect.MAT_EFFECT_KNIFE, rotZ);

                if(isMainPlayer)
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_WRONG_ATK);
            }
        }
        else if (isMainPlayer)
        {
            DoDamage(dictRH);
        }

        if (isMainPlayer)
            WorldManager.singleton.EnableRoleBodyPartCollider(false);

        if (m_fireCallBack != null)
        {
            m_fireCallBack();
            m_fireCallBack = null;
        }
    }

    protected void DoDamage(Dictionary<int, RaycastHit> dictRH)
    {
        float nowTime = Time.timeSinceLevelLoad;
        List<p_hit_info> list = new List<p_hit_info>();
        List<int> listPlayer = new List<int>();
        Vector3 khitPos = Vector3.zero;

        foreach (RaycastHit kRH in dictRH.Values)
        {
            khitPos = kRH.point;

            GameObject goTarget = null;
            Transform transRoot = kRH.collider.transform.root;   // 玩家GameObject一定要放在场景根结点下
            if (transRoot != null)
                goTarget = transRoot.gameObject;
            else
                goTarget = kRH.collider.gameObject;

            GameObject goCollider = kRH.collider.gameObject;

            BasePlayer kBP = WorldManager.singleton.GetPlayerWithGOHash(goTarget.GetHashCode());
            if(kBP == null)
                continue;

            //外挂检测
            if (AntiCheatManager.CheatCollider(kBP, kRH.collider, kRH.point))
                continue;

            int playerID = kBP.PlayerId;
            if (CanAttack(kBP) && listPlayer.IndexOf(playerID) == -1)
            {
                listPlayer.Add(playerID);

                float lastDamageTime = 0;
                
                if (m_dicDamagePlayerInfo.ContainsKey(playerID) == false)
                    m_dicDamagePlayerInfo.Add(playerID, 0);

                lastDamageTime = m_dicDamagePlayerInfo[playerID];
                if (nowTime - lastDamageTime > m_kWL.DamageDuration)
                {
                    // Do Hurt
                    m_dicDamagePlayerInfo[playerID] = nowTime;

                    int iBodyPartID = BasePlayer.GetBodyPartID(goCollider.name);
                    if (kRH.collider.gameObject.layer != GameSetting.LAYER_VALUE_OTHER_PLAYER && goCollider.layer != GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND)
                        PlayHitPlayerEffect(kBP, kRH.point, kRH.normal, iBodyPartID, Effect.MAT_EFFECT_KNIFE);

                    //Vector3 vec3LocalHitPos = goTarget.transform.worldToLocalMatrix.MultiplyPoint(kRH.point);

                    p_hit_info hitData = new p_hit_info()
                    {
                        actorId = kBP.serverData.id,
                        bodyPart = iBodyPartID,
                        pos = kBP.beShotPos
                    };
                    list.Add(hitData);
                }
            }
        }

        if (list.Count > 0)
        {
            tos_fight_fire fireArg = new tos_fight_fire()
            {
                hitPos = SceneCamera.singleton.position + (khitPos - SceneCamera.singleton.position).normalized * GameSetting.GUN_RAYHITPOS_DISTANCE,
                hitList = new p_hit_info[][] { list.ToArray() },
                bulletCount = -1,
                relyBulletCount = 0,
                weaponVer = m_kPlayer.serverData.weaponVer,
                atkType = WeaponAtkTypeUtil.GetWeaponAtkType(m_isSubWeapon ? m_kPlayer.curWeapon.weaponConfigLine.SubType : m_kWL.SubType, MainPlayer.singleton.useAdvAmmo, m_isSubWeapon),
                firePos = m_kPlayer.position,
                cameraRot = new short[] { (short)(50 * SceneCamera.singleton.localEulerAngles.x), (short)(50 * SceneCamera.singleton.localEulerAngles.y) },
                timeStamp = Time.timeSinceLevelLoad
            };
            WorldManager.SendUdp(fireArg);
        }
    }
#endregion

#region Switch

    public override void WeaponSwitched()
    {
        base.WeaponSwitched();
        if (isMainPlayer|| (isFPWatchPlayer && (m_kPlayer as FPWatchPlayer).isSwitchViewDone))
        {
            if (m_kWL.SwitchAni.Length != 0)
            {
                if (m_kAni.GetClip(m_kWL.SwitchAni[0]) != null)
                    m_kAni.Play(m_kWL.SwitchAni[0]);
                if (isBothHandSlot && m_kWL.SwitchAni.Length != 1)
                {
                    if (m_kAni.GetClip(m_kWL.SwitchAni[1]) != null)
                        m_kAni.Play(m_kWL.SwitchAni[1]);
                }
            }

            if (m_kWL.SwitchSound != null && m_kWL.SwitchSound.Length >= 3)
            {
                var index = 1;
                while (index < m_kWL.SwitchSound.Length)
                {
                    DelayPlayOneShot(float.Parse(m_kWL.SwitchSound[index]), m_kWL.SwitchSound[index + 1]);
                    index += 2;
                }
            }

        }
    }

    public override void StopWeaponAni()
    {
        base.StopWeaponAni();
        m_kAni.Stop();
    }

    override public void JumpAni2End()
    {
        base.JumpAni2End();
        m_kAni.Jump2End();
    }

    override public void WeaponSwitchOff()
    {
        base.WeaponSwitchOff();
        m_kAni.Jump2End();
        m_kSP.EndGunFireSound();
        if (m_iDamageTimer != -1)
            TimerManager.RemoveTimeOut(m_iDamageTimer);
    }
#endregion

#region OnKey

    override public void OnFireKeyDown()
    {
        PlayerStatus status = m_kPlayer.status;
        ConfigItemWeaponLine kWL = m_kWL;

        if (status.reload || status.switchWeapon)
            return;

        m_bFirePressed = true;
        m_fFireKeyPressTime = Time.timeSinceLevelLoad;

        if (!canFire)
            return;

        if (Time.timeSinceLevelLoad - lastFireTime > FIRE_SHOT_BREAK_TIME)
        {
            m_bIsFireBefore = false;
        }

        if (kWL.FireMode == WeaponBase.FIRE_MODE_SINGLETIME || kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
        {
            if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
            {
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
                m_bIsFireBefore = true;
            }
        }
    }

    override public void OnFireKeyUp()
    {
        if (m_bFirePressed == false)
            return;

        m_bFirePressed = false;

        PlayerStatus status = m_kPlayer.status;
        if (!m_kPlayer.alive || status.reload || status.switchWeapon || !canFire)
            return;

        ConfigItemWeaponLine kWL = m_kWL;

        // 扫射枪,点击时间小于指定时间，则为单射
        if (kWL.FireMode == WeaponBase.FIRE_MODE_RAPID)
        {
            float pressTime = Time.timeSinceLevelLoad - m_fFireKeyPressTime;
            if ( Time.timeSinceLevelLoad - lastFireTime >= fireRate && pressTime < FIRE_ONE_SHOT_INVERTAL_TIME)
            {
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
                m_bIsFireBefore = true;
                //Logger.Log("单射");
            }
            else
            {
                //Logger.Log("单射失败， FireTick = " + m_fFireTick + " cd = " + fFireCD + " pressTime = " + pressTime);
            }
        }

        if (status.fire == true)
        {
            status.fire = false;
            StopFire();
        }

        m_fFireKeyPressTime = 0;
    }

    protected override void OnFireUpdate()
    {
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon || !canFire)
            return;

        ConfigItemWeaponLine kWL = m_kWL;

        // 扫射枪
        if (kWL.FireMode == WeaponBase.FIRE_MODE_RAPID)
        {
            float pressTime = Time.timeSinceLevelLoad - m_fFireKeyPressTime;
            if (Time.timeSinceLevelLoad - lastFireTime >= fireRate && pressTime > FIRE_ONE_SHOT_INVERTAL_TIME)
            {
                m_fireType = WEAPON_FIRE_TYPE_STRAFE;
                TryToFire();
                m_bIsFireBefore = true;
            }
        }
    }
#endregion

    #region 挂载武器动画
    virtual protected void LoadWeaponAni()
    {
        LoadWeaponAni(m_kWL.SwitchAni);
        LoadWeaponAni(m_kWL.ReloadAni);
        LoadWeaponAni(m_kWL.FireBeginAni);
        LoadWeaponAni(m_kWL.FireEndAni);
        if (m_kWL.FireAni != null)
        {
            foreach (string ani in m_kWL.FireAni)
                LoadWeaponAni(ani);
        }
    }

    private void LoadWeaponAni(string aniName)
    {
        if (string.IsNullOrEmpty(aniName) == false && m_kAni.GetClip(aniName) == null)
        {
            string strPath = PathHelper.GetWeaponAni(aniName);
            ResourceManager.LoadAnimation(strPath, (kAniClip) =>
            {
                if (m_kAni != null && kAniClip != null)
                {
                    m_kAni.AddClip(kAniClip, aniName);
                }
            });
        }
    }

    private void LoadWeaponAni(string[] arrName)
    {
        if (arrName == null)
            return;

        foreach (string str in arrName)
        {
            LoadWeaponAni(str);
        }
    }

    #endregion

    #region effect
    override public void PlayMPFireMountEffect()
    {
        if (m_kWL.MPFireMountEffect != null && m_kWL.MPFireMountEffect.Length != 0)
        {
            string fireMountEffect = "";
            
            if (m_kWL.MPFireMountEffect.Length > m_iAniIndex)
                fireMountEffect = m_kWL.MPFireMountEffect[m_iAniIndex];
            else if (m_kWL.MPFireMountEffect.Length > 0)
                    fireMountEffect = m_kWL.MPFireMountEffect[0];

            Transform fireMountEffectTran = null;
            if (!m_fireModelMountEffect.TryGetValue(fireMountEffect, out fireMountEffectTran))
            {
                fireMountEffectTran = m_kPlayer.transform.FindChild(fireMountEffect);
                if (fireMountEffectTran == null)
                    fireMountEffectTran = m_kPlayer.transform.FindChild(m_kPlayer.transform.name + "/" + fireMountEffect);
                m_fireModelMountEffect.Add(fireMountEffect, fireMountEffectTran);
            }
            if (fireMountEffectTran != null)
                EffectManager.PlayEffect(fireMountEffectTran, true, true);
        }
    }
    #endregion

}