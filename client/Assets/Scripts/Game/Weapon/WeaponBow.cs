﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WeaponBow:WeaponRifleGrenade
{
    public WeaponBow()
    {
        m_bulletType = BulletBase.TYPE_ARROW;
    }
    bool iskeyfireDown = false;

    

    public float firestartTime = 0;
    public void OnPowerOn()
    {
        if (m_kPlayer is MainPlayer)
        {
            if (m_kWL.PowerOnAni.Length!= 0)
            {
                if (m_kAni.GetClip(m_kWL.PowerOnAni[0])!=null)
                {
                    m_kAni.Play(m_kWL.PowerOnAni[0]);
                }
            }
        }
    }


    protected override void OnFireFrame()
    {
        if (m_kPlayer is MainPlayer)
        {
            if (m_kWL.FireAni.Length != 0)
            {
                if (m_kAni.GetClip(m_kWL.FireAni[0]) != null)
                {
                    m_kAni.Play(m_kWL.FireAni[0]);
                }

                if (m_kLWD.extraAmmoNum>0)
                {
                    if (m_kWL.IdleAni.Length != 0)
                    {
                        m_kAni.PlayQueued(m_kWL.IdleAni[0], QueueMode.CompleteOthers);
                    }
                }
                else
                {
                    if (m_kWL.IdleAni.Length >= 2)
                    {
                        if (m_kAni.GetClip(m_kWL.IdleAni[1]) != null)
                        {
                            m_kAni.CrossFadeQueued(m_kWL.IdleAni[1], 0.5f, QueueMode.CompleteOthers);
                        }
                    }
                }

            }
            
        }
        
        base.OnFireFrame();

    }

    public void OnFireEnd()
    {
        if (m_kPlayer is MainPlayer)
        {
            if (m_kWL.IdleAni.Length !=0)
            {
                m_kAni.Play(m_kWL.IdleAni[0]);
            }
        }
    }

    internal override void ReloadAmmoWholly()
    {
        if (isMainPlayer)
        {
            NetLayer.Send(new tos_fight_reload()
            {
                reloadTime = m_kPlayer.serverData.reloadTime(atkType)
            });
            GameDispatcher.Dispatch<float, bool>(GameEvent.MAINPLAYER_RELOADBULLET_CD_MASK, Time.realtimeSinceStartup, true);
            SendReloadMsg(false);
        }
    }

    public override void ReloadAmmo()
    {
        ReloadAmmoWholly();
    }
    protected override void TryToFire()
    {
        //m_kLWD.curClipAmmoNum = m_kLWD.curClipAmmoNum - 1;
        base.TryToFire();
        
        
        ReloadAmmo();
    }

    protected override void DoFireReally()
    {
        base.DoFireReally();
        
        ReloadAmmo();
    }

    #region OnKey

    override public void OnFireKeyDown()
    {
        powerOnTime = 0;
        powerOnEndTime = new DateTime();
        iskeyfireDown = true;
        
        PlayerStatus status = m_kPlayer.status;

        if (status.reload || !canFire)
            return;

        if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
            return;

        if (hasAmmoCurClip)
        {
            m_bFirePressed = true;
        }
        else
        {
            return;
        }

        MainPlayer.singleton.powerOnStartTime = Time.timeSinceLevelLoad;
        tos_fight_poweron firearg = new tos_fight_poweron() { timeStamp = MainPlayer.singleton.powerOnStartTime };
        NetLayer.Send(firearg);
        //Debug.LogError("开始蓄力");
        MainPlayer.singleton.PowerOn();
        PanelBattle.powerOn = true;
        OnPowerOn();
        firestartTime = 0;
        iskeyfireDown = false;
    }


    public override void Update()
    {
        base.Update();
        if (iskeyfireDown)
        {
            PlayerStatus status = m_kPlayer.status;
            if (status.reload || !canFire)
                return;
            if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
                return;
            if (hasAmmoCurClip)
            {
                m_bFirePressed = true;
            }
            else
            {
                return;
            }
            powerOnTime = 0;
            powerOnEndTime = new DateTime();

            MainPlayer.singleton.powerOnStartTime = Time.timeSinceLevelLoad;
            tos_fight_poweron firearg = new tos_fight_poweron() { timeStamp = MainPlayer.singleton.powerOnStartTime };
            NetLayer.Send(firearg);

            MainPlayer.singleton.PowerOn();
            PanelBattle.powerOn = true;
            OnPowerOn();
            firestartTime = 0;
            iskeyfireDown = false;
        }

    }
    override public void OnFireKeyUp()
    {
        iskeyfireDown = false;
        if (m_bFirePressed == false)
            return;

        PlayerStatus status = m_kPlayer.status;
        if (!m_kPlayer.alive || status.reload)
            return;

        m_bFirePressed = false;
       
        //Debug.LogError("结束蓄力");
        MainPlayer.singleton.powerOnEndTime = Time.timeSinceLevelLoad;
        TryToFire();
        //Debug.LogError("蓄力时间" + (MainPlayer.singleton.powerOnEndTime - MainPlayer.singleton.powerOnStartTime).ToString());
       
        firestartTime = Time.timeSinceLevelLoad;
        //powerOnTime = (powerOnEndTime - powerOnStartTime).TotalMilliseconds;
        //if (powerOnTime >= m_kWL.MaxPowerOnTime * 1000)
        //{
        //    powerOnTime = m_kWL.MaxPowerOnTime * 1000;
        //}
        //tos_fight_poweroff firearg = new tos_fight_poweroff() { timeStamp = Time.timeSinceLevelLoad };
        //NetLayer.Send(firearg);

        //Debug.LogError("蓄力时间" + powerOnTime.ToString());
       
    }


    #endregion
}

