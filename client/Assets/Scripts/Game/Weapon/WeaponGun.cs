﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using Object = System.Object;


public class WeaponGun : WeaponBase
{
    protected FPSAnimation m_kAni;
    protected FPSAnimation m_kAni2;

    protected EffectGunFire m_kEffectGunFire;
    protected EffectGunFire m_kEffectGunFire2;
    protected Transform m_transFirePoint;
    protected Transform m_transFirePoint2;
    protected Transform m_transCartridge;
    protected Transform m_transCartridge2;
    protected Transform m_transRifleGrenadePoint;
    protected Transform m_transRifleGrenadePoint2;

    protected GunScope m_kGunScope;

    protected int m_iFireLineTick;
    private float m_switchSoundPlayTime;

    protected int m_iReloadTimer = -1;
    protected bool m_scopeButtonPressed;
    protected int m_hitBuildingTick;

    protected bool m_bIsPlayFireBeginAni;

    protected int m_iAniIndex = -1;

    protected EptInt m_reloadAmmoCount;

    public WeaponGun()
    {
        m_kSpread = new WeaponSpread();
        m_kAni = new FPSAnimation();
        m_kAni2 = new FPSAnimation();
    }

    override public void SetData(ConfigItemWeaponLine kWL)
    {
        base.SetData(kWL);

        if(m_kSpread != null) m_kSpread.Init(this);

        if ((m_kWL.ZoomMode != 0 || this is WeaponSniper) && m_kGunScope == null)
        {
            m_kGunScope = new GunScope(this);
            m_kGunScope.SetCallBack(ZoomInComplete, ZoomOutComplete);
        }
    }

    override public void Reset()
    {
        base.Reset();
        m_iFireLineTick = 0;
        m_bIsPlayFireBeginAni = false;
        m_iReloadTimer = -1;
        ZoomOutImmediate();
    }

    override public void UpdateConfigReference()
    {
        base.UpdateConfigReference();
        if(m_kGunScope != null)
            m_kGunScope.UpdateConfig();
    }

    override public void Update()
    {
        base.Update();

        if (m_bFirePressed)
            OnFireUpdate();

        if(m_kGunScope != null)
            m_kGunScope.Update();

        if (m_kSP != null)
            m_kSP.Update();

        if (m_kSpread != null && isFPV) m_kSpread.Update();
    }

    override public void Release()
    {
        base.Release();

        // Release Component
        if (m_kAni != null)
            m_kAni.Release();
        if (m_kAni2 != null)
            m_kAni2.Release();

        if (m_kEffectGunFire != null)
        {
            EffectManager.singleton.Reclaim(m_kEffectGunFire);
            m_kEffectGunFire = null;
        }

        if (m_kEffectGunFire2 != null)
        {
            EffectManager.singleton.Reclaim(m_kEffectGunFire2);
            m_kEffectGunFire2 = null;
        }
            
        m_transFirePoint = null;
        m_transFirePoint2 = null;
        m_transCartridge = null;
        m_transCartridge2 = null;
        m_transRifleGrenadePoint = null;
        m_transRifleGrenadePoint2 = null;

        if (m_kGunScope != null)
        {
            m_kGunScope.Release();
            m_kGunScope = null;
        }

        m_iFireLineTick = 0;
        m_switchSoundPlayTime = 0;

        if (m_iReloadTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iReloadTimer);
            m_iReloadTimer = -1;
        }

        m_scopeButtonPressed = false;
        m_hitBuildingTick = 0;
        m_bIsPlayFireBeginAni = false;
        m_iAniIndex = -1;
    }

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        if (!(this is WeaponPawGun))
        {
            Animation ani = GameObjectHelper.GetComponent<Animation>(go);
            ani.playAutomatically = false;
            m_kAni.SetAnimation(ani);

            go.transform.VisitChild((child) =>
            {
                switch(child.name)
                {
                    case ModelConst.WEAPON_SLOT_FIREPOINT:
                        m_transFirePoint = child.transform;
                        break;
                    case ModelConst.WEAPON_SLOT_CARTRIDGE:
                        m_transCartridge = child.transform;
                        break;
                    case ModelConst.WEAPON_SLOT_RIFLEGRENADE:
                        m_transRifleGrenadePoint = child.transform;
                        break;
                }
            }, false);

            if (m_transFirePoint == null)
                Logger.Warning("找不到枪口火花挂接点!");
            if (m_transCartridge == null)
                Logger.Warning("找不到弹壳挂接点!");

            LoadWeaponAni(m_kAni);
        }
    }

    public override void SetGO2(GameObject go)
    {
        base.SetGO2(go);

        if (!(this is WeaponPawGun))
        {
            Animation ani = GameObjectHelper.GetComponent<Animation>(go);
            ani.playAutomatically = false;
            m_kAni2.SetAnimation(ani);

            m_transFirePoint2 = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
            if (m_transFirePoint2 == null)
                Logger.Error("找不到枪口火花挂接点!");

            m_transCartridge2 = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_CARTRIDGE);
            if (m_transCartridge2 == null)
                Logger.Error("找不到弹壳挂接点!");

            m_transRifleGrenadePoint2 = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_RIFLEGRENADE);

            LoadWeaponAni(m_kAni2);
        }
    }


    override public string TestWeapon()
    {
        string strRet = WEAPON_TEST_UNKNOW;
        if(m_kWL.InitClipSize == -1)
        {
            strRet = WEAPON_TEST_SUCCESS;
        }
        else if(isSubWeapon)
        {
            if (m_kLWD.subAmmoNum > 0 || m_kLWD.subAmmoNum == -1)
                strRet = WEAPON_TEST_SUCCESS;
            else
                strRet = WEAPON_TEST_NO_AMMO;
        }
        else if(m_kLWD.curClipAmmoNum > 0 || m_kLWD.curClipAmmoNum == -1)
        {
            strRet = WEAPON_TEST_SUCCESS;

            if (m_kGunScope != null && m_kGunScope.isZoomInOrOut)
                strRet = WEAPON_TEST_ZOOMINOUT;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum > 0)
        {
            strRet = WEAPON_TEST_NEED_RELOAD;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum <= 0)
        {
            strRet = WEAPON_TEST_NO_AMMO;
        }

        return strRet;
    }

    override public void StopFire()
    {
        // 有的武器扫射动画时间很长，所以停止扫射时，跳到最后一帧
        if (m_fireType == WEAPON_FIRE_TYPE_STRAFE)
        {
            StopWeaponAni();
        }

        //如果之前是开火状态，则显示结束开火动画
        if (m_kPlayer.status.fire && !string.IsNullOrEmpty(m_kWL.MPFireEndAni))
        {
            PlayerAniBaseController kPA = m_kPlayer.playerAniController;
            kPA.Play(m_kWL.MPFireEndAni);

            if (m_kWL.FireEndAni.Length != 0)
            {
                if (m_kAni.GetClip(m_kWL.FireEndAni[0]) != null)
                {
                    m_kAni.Play(m_kWL.FireEndAni[0]);
                }
                if (isBothHandSlot)
                {
                    var index = m_kWL.FireEndAni.Length == 1 ? 0 : 1;

                    if (m_kAni2.GetClip(m_kWL.FireEndAni[index]) != null)
                    {
                        m_kAni2.Play(m_kWL.FireEndAni[index]);
                    }
                }
            }
        }

        base.StopFire();

        m_bIsPlayFireBeginAni = false;

        m_kSP.EndGunFireSound();
//      m_kSpread.ClearRecoilAndSpread();
    }

    public override void StopWeaponAni()
    {
        base.StopWeaponAni();
        m_kAni.Stop();
        if (isBothHandSlot)
            m_kAni2.Stop();
    }

    override public void JumpAni2End()
    {
        base.JumpAni2End();
        m_kAni.Jump2End();
        m_kAni2.Jump2End();

    }

    override public bool IsCurClipFull()
    {
        return m_kLWD.curClipAmmoNum == m_kLWD.maxAmmoNum;
    }

    override public bool isZoomOrOut
    {
        get
        {
            if (m_kGunScope == null)
                return false;
            return m_kGunScope.isZoomOrOut;
        }
    }

    public bool isZoom
    {
        get
        {
            if (m_kGunScope == null)
                return false;
            return m_kGunScope.isZoom;
        }
    }

    public bool isIdle
    {
        get
        {
            if (m_kGunScope == null)
                return false;
            return m_kGunScope.isIdle;
        }
    }

    public Vector3 rifleGrenadeFirePoint
    {
        get 
        {
            return m_transRifleGrenadePoint != null ? m_transRifleGrenadePoint.position : firePoint;
        }
    }

    virtual protected void AttachFireEffect()
    {
        if (m_kEffectGunFire != null)
            return;

        string fireEffect = "";

        if (!string.IsNullOrEmpty(m_awakenFireEffect))
            fireEffect = m_awakenFireEffect;
        else
        {
            fireEffect = isFPV ? (m_kWL.FireEffect.Length > 0 ? m_kWL.FireEffect[0] : "") : (m_kWL.FireEffect2.Length > 0 ? m_kWL.FireEffect2[0] : "");
        }

        if (string.IsNullOrEmpty(fireEffect))
            return;

        EffectManager.singleton.GetEffect<EffectGunFire>(fireEffect, (kEffect) =>
        {
            if(m_released)
            {
                kEffect.Release();
                return;
            }

            if (m_kEffectGunFire != null)
                EffectManager.singleton.Reclaim(m_kEffectGunFire);

            m_kEffectGunFire = kEffect;
            m_kEffectGunFire.gameObject.TrySetActive(false);
            kEffect.Attach(m_transFirePoint);

            if (isFPV)
            {
                kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            }
            else
            {
                kEffect.SetFadeOut(5f, 0.5f);
            }

            //foreach (var renderer in kEffect.rendererList)
            //    SetRendererTintColor(renderer, m_awakenFireColor);

        });
    }

    virtual protected void AttachFireEffect2()
    {
        if (m_kEffectGunFire2 != null)
            return;

        string fireEffect = "";

        if (!string.IsNullOrEmpty(m_awakenFireEffect))
            fireEffect = m_awakenFireEffect;
        else
        {
            fireEffect = isFPV ? (m_kWL.FireEffect.Length > 1 ? m_kWL.FireEffect[1] : (m_kWL.FireEffect.Length > 0 ? m_kWL.FireEffect[0] : "")) : (m_kWL.FireEffect2.Length > 1 ? m_kWL.FireEffect2[1] : (m_kWL.FireEffect2.Length > 0 ? m_kWL.FireEffect2[0] : ""));
        }

        if (string.IsNullOrEmpty(fireEffect))
            return;

        EffectManager.singleton.GetEffect<EffectGunFire>(fireEffect, (kEffect) =>
        {
            if (m_released)
            {
                kEffect.Release();
                return;
            }

            if (m_kEffectGunFire2 != null)
                EffectManager.singleton.Reclaim(m_kEffectGunFire2);

            m_kEffectGunFire2 = kEffect;
            m_kEffectGunFire2.gameObject.TrySetActive(false);
            kEffect.Attach(m_transFirePoint2);

            if (isFPV)
            {
                kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            }
            else
            {
                kEffect.SetFadeOut(5f, 0.5f);
            }

            //foreach (var renderer in kEffect.rendererList)
            //    SetRendererTintColor(renderer, m_awakenFireColor);

        });
    }

    private void SetRendererTintColor(Renderer renderer, Color color)
    {
        if (renderer != null)
        {
            renderer.material.SetColor(GameConst.SHADER_PROPERTY_TintColor, color / 255);
        }
    }

    virtual protected void LoadWeaponAni(FPSAnimation fpsAni)
    {
        LoadWeaponAni(fpsAni, m_kWL.SwitchAni);
        LoadWeaponAni(fpsAni, m_kWL.IdleAni);
        LoadWeaponAni(fpsAni, m_kWL.ReloadAni);
        LoadWeaponAni(fpsAni, m_kWL.FireBeginAni);
        LoadWeaponAni(fpsAni, m_kWL.FireEndAni);
        LoadWeaponAni(fpsAni, m_kWL.PowerOnAni);
        LoadWeaponAni(fpsAni, m_kWL.OpWeaponFire);
        LoadWeaponAni(fpsAni, m_kWL.OpWeaponIdle);
        LoadWeaponAni(fpsAni, m_kWL.OpWeaponSwitch);
        
        if (m_kWL.FireAni != null)
        {
            foreach (string ani in m_kWL.FireAni)
                LoadWeaponAni(fpsAni, ani);
        }
    }


    public void PlayOpWeaponFire()
    {
        if (m_kWL.OpWeaponFire.Length != 0)
        {
            if (m_kAni.GetClip(m_kWL.OpWeaponFire[0]) != null)
            {
                m_kAni.Play(m_kWL.OpWeaponFire[0]);
            }
        }
        PlayOpWeaponIdle();
    }
    public void PlayOpWeaponSwitch()
    {
        if (m_kWL.OpWeaponSwitch.Length != 0)
        {
            if (m_kAni.GetClip(m_kWL.OpWeaponSwitch[0]) != null)
            {
                m_kAni.Play(m_kWL.OpWeaponSwitch[0]);
            }
        }
        PlayOpWeaponIdle();
    }
    public void PlayOpWeaponIdle()
    {
        if (m_kWL.OpWeaponIdle.Length != 0)
        {
            if (m_kAni.GetClip(m_kWL.OpWeaponIdle[0]) != null)
            {
                m_kAni.PlayQueued(m_kWL.OpWeaponIdle[0],QueueMode.CompleteOthers);
            }
        }
    }

    

    private void LoadWeaponAni(FPSAnimation fpsAni, string aniName)
    {
        if (string.IsNullOrEmpty(aniName) == false && m_kAni.GetClip(aniName) == null)
        {
            string strPath = PathHelper.GetWeaponAni(aniName);
            ResourceManager.LoadAnimation(strPath, (kAniClip) =>
            {
                if (fpsAni != null && kAniClip != null)
                {
                    fpsAni.AddClip(kAniClip, aniName);
                }
            });
        }
    }

    private void LoadWeaponAni(FPSAnimation fpsAni, string[] arrName)
    {
        if (arrName == null)
            return;

        foreach(string str in arrName)
        {
            LoadWeaponAni(fpsAni, str);
        }
    }

    protected void AttachAndPlayCartridgeEffect()
    {
        if (string.IsNullOrEmpty(m_kWL.CartridgeEffect) || GameSetting.enableGunShell == false)
            return;

        EffectManager.singleton.GetEffect<EffectTimeout>(m_kWL.CartridgeEffect, (kEffect) =>
        {
            kEffect.Attach(m_transCartridge);
            kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            kEffect.SetTimeOut(-1);
            kEffect.Play();
        });
    }

    protected void AttachAndPlayCartridgeEffect2()
    {
        if (string.IsNullOrEmpty(m_kWL.CartridgeEffect) || GameSetting.enableGunShell == false)
            return;

        EffectManager.singleton.GetEffect<EffectTimeout>(m_kWL.CartridgeEffect, (kEffect) =>
        {
            kEffect.Attach(m_transCartridge2);
            kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            kEffect.SetTimeOut(-1);
            kEffect.Play();
        });
    }

    protected void PlayOPFireLineEffect(Vector3 vec3RayHitPos)
    {
        if (GameSetting.enableGunFire == false || QualityManager.enableOPFileLine == false)
            return;

        if (string.IsNullOrEmpty(m_kWL.BulletEffect2) == true)
            return;

        m_iFireLineTick++;
        if (m_iFireLineTick < m_kWL.BulletEffectFrequency)
            return;
        m_iFireLineTick = 0;

        Vector3 dir;
        float dis = 50;
        var firePoint = (isBothHandSlot == false || m_iAniIndex % 2 == 0) ? m_transFirePoint.position : m_transFirePoint2.position;
        if (vec3RayHitPos == Vector3.zero)
        {
            dir = (isBothHandSlot == false || m_iAniIndex % 2 == 0) ? m_transFirePoint.forward : m_transFirePoint2.forward;
        }
        else
        {
            dir = vec3RayHitPos - firePoint;
            dis = dir.sqrMagnitude;
        }

        PlayFireLineEffect(Vector3.Normalize(dir), firePoint, dis, m_kWL.BulletEffect2);
    }

    protected void PlayMPFireLineEffect(Ray ray, Vector3 targetPos)
    {
        return; // 先关掉主角火线

        if (string.IsNullOrEmpty(m_kWL.BulletEffect) == true)
            return;

         //m_iFireLineTick++;
         //if (m_iFireLineTick < m_kWL.BulletEffectFrequency)
         //    return;
         //m_iFireLineTick = 0;

        var tran = (isBothHandSlot == false || m_iAniIndex % 2 == 0) ? m_transFirePoint : m_transFirePoint2;

        var pos = tran.position;
        var rotation = tran.rotation;

        Vector3 vec3Dir = Vector3.zero;

        tran.RotateAround(MainPlayer.singleton.sceneCameraSlot.position, MainPlayer.singleton.transform.right, MainPlayer.singleton.sceneCameraSlot.localEulerAngles.x);
        vec3Dir = targetPos - tran.position;

        PlayFireLineEffect(vec3Dir, tran.position, vec3Dir.sqrMagnitude, m_kWL.BulletEffect);

        tran.position = pos;
        tran.rotation = rotation;
    }

    private void PlayFireLineEffect(Vector3 vec3Dir, Vector3 vec3StartPos, float fDis, string strBulletEffect)
    {
        EffectManager.singleton.GetEffect<EffectMove>(strBulletEffect, (kEffect) =>
        {
            if (kEffect == null)
                return;

            if (m_released)
            {
                kEffect.Release();  
                return;
            }

            float time = isFPV ? m_kWL.BulletEffectTime1 : m_kWL.BulletEffectTime2;
            time = time > 0 ? time : kEffect.timeOut;
            kEffect.Attach(vec3StartPos, vec3Dir, Space.World, SceneManager.singleton.effectRoot);
            kEffect.distance = fDis;
            kEffect.speed = isFPV ? m_kWL.BulletEffectSpeed : m_kWL.BulletEffectSpeed2;
            kEffect.timeOut = time;

            if (m_bUseawakenFireLineColor)
            {
                foreach (var renderer in kEffect.rendererList)
                    SetRendererTintColor(renderer, m_awakenFireLineColor);
            }

            kEffect.Play();
        });
    }

    //protected float GetFireCD()
    //{
    //    float fireCD = 0;
    //    if (isZoomOrOut && m_kWL.ZoomFireRate != 0)
    //        fireCD = 60.0f / m_kWL.ZoomFireRate;
    //    else
    //        fireCD = 60.0f / m_kWL.FireRate;

    //    return fireCD;
    //}

    protected EptInt GetMPWeaponFireType()
    {
        //if (m_kWL.FireMode == WeaponBase.FIRE_MODE_RAPID)
        //    return WEAPON_FIRE_TYPE_STRAFE;
        return WEAPON_FIRE_TYPE_ONESHOT;
    }

    virtual protected string GetMPWeaponFireAni(int fireType)
    {
        if (isBothHandSlot)
        {
            //if (!m_bIsFireBefore)
            //    m_iAniIndex = 0;
            //else
                m_iAniIndex = (m_iAniIndex == m_kWL.MPFireAni.Length - 1) ? 0 : m_iAniIndex + 1;
            return m_kWL.MPFireAni[m_iAniIndex];
        }

        m_iAniIndex = -1;

        if (fireType == WEAPON_FIRE_TYPE_ONESHOT)
        {
            if (m_kWL != null && m_kWL.MPFireAni.Length >= 1)
            {
                m_iAniIndex = 0;
            }
        }
        else if (fireType == WEAPON_FIRE_TYPE_STRAFE)
        {
            if (m_kWL != null && m_kWL.MPFireAni.Length >= 2)
            {
                m_iAniIndex = 1;
            }
        }
        return m_iAniIndex == -1 ? "" : m_kWL.MPFireAni[m_iAniIndex];
    }

    protected Vector3 firePoint
    {
        get
        {
            if (m_transFirePoint == null)
                return m_kWL.SlotType == WEAPON_SLOTTYPE_RIGHTHAND ? m_kPlayer.rightHandSlot.position : m_kPlayer.leftHandSlot.position;
            else
                return m_transFirePoint.position;
        }
    }

    public bool hasScopeButton
    {
        get
        {
            if (this is WeaponSniper && m_kWL.ContinuousSniper > 0 ||      // 是连狙 或者 普通枪但有镜
                !(this is WeaponSniper) && m_kWL.ZoomMode != 0)
                return true;
            return false;
        }
    }

    protected override float fireRate
    {
        get
        {
            if (m_kPlayer == null)
                return float.MaxValue;

            float fireCD = 0;
            if (isZoomOrOut && m_kPlayer.serverData.ZoomFireRate(atkType) > 1f)
                fireCD = 60.0f / m_kPlayer.serverData.ZoomFireRate(atkType);
            else
                fireCD = 60.0f / m_kPlayer.serverData.FireRate(atkType);

#if UNITY_EDITOR
            return fireCD * GameSetting.MPFireFactor;
#else
            return fireCD;
#endif
        }
    }

#region Fire

    protected override void TryToFire()
    {
        base.TryToFire();
        string strRet = TestWeapon();

        if (strRet != WEAPON_TEST_SUCCESS)
        {
            StopFire();
        }

        switch(strRet)
        {
            case WEAPON_TEST_SUCCESS:
                DoFireReally();
                break;
            case WEAPON_TEST_NO_AMMO:
                PlayFireEmptySound();
                break;
            case WEAPON_TEST_NEED_RELOAD:
                if (m_iReloadTimer == -1)
                    ReloadAmmo();
                break;
        }
    }

    // 最后一颗子弹打完后，要自动装弹

    protected override void DoFireReally()
    {
        m_kPlayer.status.fire = true;
        m_aimRay = AutoAim.CurAimRay;

        
        
        PlayerAniBaseController kPA = m_kPlayer.playerAniController;
        string fireAni = GetMPWeaponFireAni(m_fireType);

        StopWeaponAni();

        if (m_fireType == WEAPON_FIRE_TYPE_ONESHOT || weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SHOT_GUN)
        {
            kPA.Play(fireAni, 0, 0);
        }
        else if(m_fireType == WEAPON_FIRE_TYPE_STRAFE)
        {
            if (kPA.IsCurrentAni(fireAni) == false)
            {
                kPA.CrossFade(fireAni, 0.2f);                
            }
            
            kPA.SetBool(AniConst.TriggerStrafeFire, true);
            m_kPlayer.cameraShaker.apply = true;
        }

        if (m_bIsSkillFire && m_kWL.FireDelay > 0)
        {
            m_iFireSkillTimerId = TimerManager.SetTimeOut(m_kWL.FireDelay, () =>
            {
                m_iFireSkillTimerId = -1;
                if(m_kPlayer.curWeapon == this)
                    DoWeaponFireLogic();
            });
        }
        else
        {
            DoWeaponFireLogic();
            if (m_fireType == WEAPON_FIRE_TYPE_ONESHOT)
            {
                m_kSP.EndGunFireSound();
                if (!m_bIsFireBefore)
                    StopFire();
            }
        }

        // 最后一发打完，要装弹
        if(ammoNumInClip == 0 && ammoNumLeft > 0)
        {
            //float fireAniTime = 0.3f;
            //if (m_kWL.FireAni != null && m_kWL.FireAni.Length > 0)
            //{
            //    if (float.TryParse(m_kWL.FireAni[m_kWL.FireAni.Length - 1], out fireAniTime) == false)
            //        fireAniTime = FIRE_ANI_DEFAULT_LENGTH;
            //}
            //TimerManager.SetTimeOut(fireAniTime, () =>
            //{
            //    if (m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
            //        return;
            //    ReloadAmmo();
            //});
            if (!(m_kWL.Class == WeaponManager.WEAPOM_TYPE_BOW))
            {
                TimerManager.SetTimeOut(fireRate, () =>
                {
                    if (m_kPlayer == null || m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                        return;
                    ReloadAmmo();
                });
            }
        }
    }

    int shotTime = 1;
    Action<object> FireLogicAct = null;
    virtual protected void DoWeaponFireLogic()
    {

        float burstCD = 0;
        if (m_kWL.FireMode == FIRE_MODE_BURST)
        {
            int clipAmmoNum = isSubWeapon ? m_kLWD.subAmmoNum : m_kLWD.curClipAmmoNum;
            int Pellets = m_kWL.BurstNum > 0 ? m_kWL.BurstNum : 1;
            shotTime = clipAmmoNum >= Pellets ? Pellets : clipAmmoNum;
            if (m_kWL.BurstRate <= 0)
                Logger.Warning("BurstRate <= 0 " + m_kWL.ID);
            else
                burstCD = 60f / m_kWL.FireRate;
        }

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
        {
            m_kSP.BeginGunFireSound(new[] { m_kWL.FireSound[1] }, m_kWL.FireSoundProbability, m_kWL.FireSoundSpawn[0],
                m_kWL.FireSoundSpawn[1], AudioManager.FightSoundVolume);
        }

        string gunFireAni = "";
        if (m_kWL.FireAni != null)
        {
            if (m_kWL.FireAni.Length == 1)
                gunFireAni = m_kWL.FireAni[0];
            else if (m_kWL.FireAni.Length == 2)
                gunFireAni = m_fireType == WEAPON_FIRE_TYPE_ONESHOT ? m_kWL.FireAni[0] : m_kWL.FireAni[1];
        }

        if (m_kAni.GetClip(gunFireAni) != null)
        {
            m_kAni.Stop();
            m_kAni.Play(gunFireAni);
        }

        //播放人物身上的特效
        PlayMPFireMountEffect();

        FireLogicAct = (arg) =>
            {
                if (m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                    return;
                WorldManager.singleton.EnableRoleBodyPartCollider(true);
                m_aimRay = AutoAim.CurAimRay;
                int index = (int)arg;

                if (GameSetting.enableGunFire)
                {

                    if (isBothHandSlot)
                    {
                        if (m_iAniIndex % 2 == 0)
                        {
                            if (m_kEffectGunFire != null)
                                m_kEffectGunFire.Play();
                            AttachAndPlayCartridgeEffect();
                        }
                        else
                        {
                            if (m_kEffectGunFire2 != null)
                                m_kEffectGunFire2.Play();
                            AttachAndPlayCartridgeEffect2();
                        }
                    }
                    else
                    {
                        if (m_kEffectGunFire != null)
                            m_kEffectGunFire.Play();
                        AttachAndPlayCartridgeEffect();
                    }
                }

                ReduceAmmoNum();

                int pelletNum = m_kWL.Pellets <= 0 ? 1 : m_kWL.Pellets;
                int bulletCnt = protoBulletCnt;
                bool isHitEnemy;
                bool hitOneEnemy = false;

                if (m_kSpread != null)
                {
                    tos_fight_fire fireArg = DoMainPlayerRayTest(m_kSpread.ApplySpread(), bulletCnt, true, out hitOneEnemy);
                    List<p_hit_info[]> list = new List<p_hit_info[]>();
                    list.AddRange(fireArg.hitList);
                    for (int j = 1; j < pelletNum; j++)
                    {
                        tos_fight_fire hitData = DoMainPlayerRayTest(m_kSpread.ApplySpread(), bulletCnt, !hitOneEnemy, out isHitEnemy);
                        if (!hitOneEnemy && isHitEnemy)
                            hitOneEnemy = true;
                        list.AddRange(hitData.hitList);
                    }

                    fireArg.hitList = list.ToArray();

                    fireArg.notPlayAni = 0x00;
                    fireArg.notPlayAni += (byte)((m_iAniIndex << 1) & 0x06);
                    fireArg.notPlayAni += (byte)((localWeaponData.curClipAmmoNum % 10 << 4) & 0xF0);
                    fireArg.timeStamp = Time.timeSinceLevelLoad;
                    //Debug.LogError("bulletCount = " + fireArg.bulletCount + "  relyBulletCount = " + fireArg.relyBulletCount + " fireArg.timeStamp =  " + fireArg.timeStamp);
                    WorldManager.SendUdp(fireArg);
                }

                if (!hitOneEnemy)
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_WRONG_ATK);
                if (m_isSubWeapon)
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subAmmoNum);
                else
                {
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);    // 在发送完协议后，再让界面更新，界面更新会设置useAdv=false
                    if (m_kWL.FireMode == FIRE_MODE_BURST && ammoNumInClip == 0 && ammoNumLeft > 0)
                    {
                        TimerManager.SetTimeOut(fireRate, () =>
                        {
                            if (m_kPlayer == null || m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                                return;
                            ReloadAmmo();
                        });
                    }
                }
                shotTime--;
                if (index > 1)
                    TimerManager.SetTimeOut(burstCD, FireLogicAct, shotTime);
                WorldManager.singleton.EnableRoleBodyPartCollider(false);
            };

        TimerManager.SetTimeOut(burstCD, FireLogicAct, shotTime);

        if (m_fireCallBack != null)
        {
            Action callback = () =>
            {
                m_fireCallBack();
                m_fireCallBack = null;
                m_iFireSkillTimerId = -1;
            };

            if (m_kWL.FireAfterDelay > 0)
                m_iFireSkillTimerId = TimerManager.SetTimeOut(m_kWL.FireAfterDelay, callback);
            else
                callback();
        }
    }


    virtual internal tos_fight_fire DoMainPlayerRayTest(float spread, int bulletCnt, bool isPlayHit, out bool isHitEnemy)
    {
        isHitEnemy = false;

        // 子弹可穿透性的武器，需要检测多个碰撞对象，这些对象按射线长度排序后，可以看出受击顺序。
        int iLayerMaskBuildingEnemy = GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND;

        if (isZoomOrOut)
            spread *= m_kPlayer.serverData.SpreadSightsFactor(atkType);
        //计算弹道的散布角度
        Ray shootRay = m_aimRay;
        var spreadRandom = Random.Range(spread * m_kWL.StandSpreadMinFactor, spread);
        var spreadYaw = spreadRandom * m_kWL.SpreadYawFactor;

        if (GlobalConfig.showSpreadDebugInfo)
            Logger.Log("散射值：" + spread + " 最终散射值：" + spreadYaw + "\n");

        var dir = Quaternion.AngleAxis(spreadYaw, Vector3.up) * Vector3.forward;
        dir = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward) * dir;
        if (spreadYaw == 0)
            dir.y = 0;
        else
            dir.y *= m_kWL.SpreadPitchFactor / m_kWL.SpreadYawFactor;
        shootRay.direction = Quaternion.LookRotation(m_aimRay.direction) * dir;

        RaycastHit[] arrRH = Physics.RaycastAll(shootRay, GameSetting.FIRE_RAYTEST_LENGTH, iLayerMaskBuildingEnemy);
        List<RaycastHit> listRH = new List<RaycastHit>(arrRH);
        listRH.Sort(delegate(RaycastHit kRH1, RaycastHit kRH2)
        {
            return kRH1.distance - kRH2.distance >= 0 ? 1 : -1;
        });

        List<p_hit_info> listFightHitData = new List<p_hit_info>();

        bool isPlayHit2 = true;
        int cnt = listRH.Count;

        var maxHitCount = m_kPlayer.serverData.MaxHitCount(atkType);
        if (maxHitCount != 0)
            cnt = listRH.Count > maxHitCount ? maxHitCount : cnt;

        if (weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SHOT_GUN)
            cnt = cnt > SHOTGUN_MAX_HIT_NUM ? SHOTGUN_MAX_HIT_NUM : cnt;      // 限制最大数据，否则消息会很大
        else
            cnt = cnt > GUN_MAX_HIT_NUM ? GUN_MAX_HIT_NUM : cnt;      // 限制最大数据，否则消息会很大

        RaycastHit rh;
        List<BasePlayer> listHitPlayer = new List<BasePlayer>();
        List<BasePlayer> listExcludePlayer = new List<BasePlayer>();
        bool buildingEffectPlayed = false;
        Vector3 bulletHitPos = Vector3.zero;
        for (int i = 0; i < cnt; i++)
        {
            rh = listRH[i];
            GameObject goCollider = rh.collider.gameObject;
            Transform transCollider = goCollider.transform;

            if (i== 0 && buildingEffectPlayed == false && goCollider.layer == GameSetting.LAYER_VALUE_BUILDING)
            {
                int matType = SceneManager.GetMaterialType(goCollider);
                if(matType != 0)
                {
                    buildingEffectPlayed = true;
                    PlayHitBuildingEffect(rh.point, rh.normal, matType, Effect.MAT_EFFECT_GUN);
                }
            }

            if(bulletHitPos == Vector3.zero)
                bulletHitPos = rh.point;

            p_hit_info hitData = null;

            if(goCollider.layer == GameSetting.LAYER_VALUE_BUILDING)
            {
                int mt = SceneManager.GetMaterialType(goCollider);
                if(mt != 0)
                    hitData = new p_hit_info() { matType = mt };
            }
            else if (goCollider.layer == GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY || goCollider.layer == GameSetting.LAYER_VALUE_DMM_PLAYER
                || (goCollider.layer == GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND))
            {
                GameObject goTarget = null;
                Transform transRoot = transCollider.root;   // 玩家GameObject一定要放在场景根结点下
                if (transRoot != null)
                    goTarget = transRoot.gameObject;
                else
                    goTarget = goCollider;

                BasePlayer kBP = WorldManager.singleton.GetPlayerWithGOHash(goTarget.GetHashCode());

                // 玩家有多个碰撞体，射线穿过会有多个碰撞点,这里要排除玩家重复受击; 如果Boss有护盾，会被排除
                if (kBP == null || listHitPlayer.Contains(kBP) || !CanAttack(kBP) || listExcludePlayer.IndexOf(kBP) != -1)  
                    continue;
                
                //外挂检测
                if (AntiCheatManager.CheatCollider(kBP, rh.collider, rh.point))
                    continue;

                // 先打到的是护盾，则本次攻击无效(如果先打到其它部位，逻辑不会走到这里，上一行会返回)
                int bodyPartID = BasePlayer.GetBodyPartID(goCollider.name);
                if(bodyPartID == BasePlayer.BODY_PART_ID_SHIELD)
                {
                    listExcludePlayer.Add(kBP);
                    // 播放打到护盾特效
                    //PlayHitPlayerEffect(kBP, rh.point, rh.normal, bodyPartID, Effect.MAT_EFFECT_GUN);
                    continue;
                }



                listHitPlayer.Add(kBP);

                hitData = new p_hit_info() { 
                    matType = SceneManager.GetMaterialType(goCollider),
                    bodyPart = bodyPartID,
                    actorId = kBP.serverData.id,
                    pos = kBP.beShotPos
                };

                if (i == 0 && isPlayHit && isPlayHit2 && goCollider.layer != GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND)
                {
                    PlayHitPlayerEffect(kBP, rh.point, rh.normal, hitData.bodyPart, Effect.MAT_EFFECT_GUN);
                    isPlayHit2 = false;
                }

                //命中玩家时，在自己身上也播放命中音效，增强玩家的命中反馈
                PlayerHitSound(m_kPlayer.position, hitData.bodyPart, Effect.MAT_EFFECT_GUN);

                isHitEnemy = true;
            }

            if (hitData != null)
                listFightHitData.Add(hitData);
        }

        // 第一人称先不播放火线特效
        if (listRH.Count > 0)
        {
            // TODO：每隔几次播放一次
            RaycastHit kRH1 = listRH[0];           
            PlayMPFireLineEffect(shootRay, kRH1.point);
        }

        if (GameSetting.openFireHitInfo)
        {
            string msg = "";
            for (int i = 0; i < listFightHitData.Count; i++)
            {
                if (listFightHitData[i].actorId != 0)
                {

                    msg += "id=" + listFightHitData[i].actorId + " hitPos=" + listFightHitData[i].pos;
                    BasePlayer p = WorldManager.singleton.GetPlayerById(listFightHitData[i].actorId);
                    if (p != null)
                        msg += " serverPos=" + p.serverPos;
                }
            }
            if (!msg.IsNullOrEmpty())
                Logger.Log("HitInfo: " + msg + " firePos=" + m_kPlayer.position);
        }

        bulletHitPos = bulletHitPos == Vector3.zero ? SceneCamera.singleton.position + shootRay.direction * GameSetting.GUN_RAYHITPOS_DISTANCE : bulletHitPos;
        tos_fight_fire protoFire = new tos_fight_fire()
        {
            hitPos = bulletHitPos,
            hitList = new p_hit_info[][] { listFightHitData.ToArray() },
            bulletCount = bulletCount,
            weaponVer = m_kPlayer.serverData.weaponVer,
            isZoom = isZoomOrOut,
            relyBulletCount = relyBulletCount,
            atkType = WeaponAtkTypeUtil.GetWeaponAtkType(m_isSubWeapon ? m_kPlayer.curWeapon.weaponConfigLine.SubType : m_kWL.SubType, MainPlayer.singleton.useAdvAmmo, m_isSubWeapon),
            firePos = m_kPlayer.position,
            cameraRot = new short[] { (short)(50 * SceneCamera.singleton.localEulerAngles.x), (short)(50 * SceneCamera.singleton.localEulerAngles.y) }
        };

        //Logger.Log("SendFireRot = " + protoFire.cameraRot[0] / 50f + " " + protoFire.cameraRot[1] / 50f);

        return protoFire;
    }

    internal override void Fire2(toc_fight_fire arg)
    {
        if (gameObject == null || m_kPlayer.alive == false)
            return;

        if (m_kPlayer is OtherPlayer)
        {
            bool notPlayAni = (arg.notPlayAni & 0x01) == 1;
            int fireIndex = (arg.notPlayAni >> 1) & 0x03;
            if (!notPlayAni)
            {
                if (m_bIsWalkWaggle && m_kPlayer.playerAniController.animator != null && m_kPlayer.playerAniController.animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                    m_kPlayer.playerAniController.EnableWalkWaggleLayer(false);
                var fireAniIndex = (fireIndex < m_kWL.OPFireAni.Length) ? fireIndex : 0;
                m_kPlayer.playerAniController.Play(m_kWL.OPFireAni[fireAniIndex]);
                if (GameSetting.enableGunFire && m_kPlayer.isRendererVisible)
                {
                    PlayOPFireMountEffect(fireAniIndex);

                    if (isBothHandSlot == false || fireIndex % 2 == 0)
                    {
                        if (m_kEffectGunFire != null)
                            m_kEffectGunFire.Play();
                    }
                    else
                    {
                        if (m_kEffectGunFire2 != null)
                            m_kEffectGunFire2.Play();
                    }
                }

                if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2 && GameSetting.enableSound)
                {
                    m_kSP.PlayOneShot(m_kWL.FireSound[1], AudioManager.FightSoundVolume);
                }
            }
            PlayOPFireLineEffect(arg.hitPos);
        }
        else
        {
            m_kPlayer.status.fire = true;
            m_kPlayer.status.reload = false;

            m_aimRay = AutoAim.CurAimRay;
            PlayerAniBaseController kPA = m_kPlayer.playerAniController;

            //string fireType = GetMPWeaponFireType();
            int fireType = WEAPON_FIRE_TYPE_ONESHOT;
            string fireAni = GetMPWeaponFireAni(fireType);

            StopWeaponAni();

            //if (fireType == WEAPON_FIRE_TYPE_ONESHOT)
            //{
            kPA.Play(fireAni, 0, 0);
            //}
            //else if (fireType == WEAPON_FIRE_TYPE_STRAFE)
            //{
            //    if (kPA.IsCurrentAni(fireAni) == false)
            //    {
            //        kPA.CrossFade(fireAni, 0.2f);
            //    }
            //
            //    kPA.SetBool(AniConst.TriggerStrafeFire, true);
            //    m_kPlayer.cameraShaker.apply = true;
            //}

            if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
            {
                m_kSP.PlayOneShot(m_kWL.FireSound[1], AudioManager.FightSoundVolume);
            }

            //播放人物身上的特效
            PlayMPFireMountEffect();

            if (m_kWL.FireAni != null && m_kWL.FireAni.Length != 0)
            {
                string gunFireAni = "";
                if (!isBothHandSlot)
                {
                    if (m_kWL.FireAni.Length == 1)
                        gunFireAni = m_kWL.FireAni[0];
                    else if (m_kWL.FireAni.Length == 2)
                        gunFireAni = m_fireType == WEAPON_FIRE_TYPE_ONESHOT ? m_kWL.FireAni[0] : m_kWL.FireAni[1];
                    if (m_kAni.GetClip(gunFireAni) != null)
                    {
                        m_kAni.Stop();
                        m_kAni.Play(gunFireAni);
                    }
                }
                else
                {
                    if (m_iAniIndex % 2 == 0)
                    {
                        gunFireAni = m_kWL.FireAni[0];
                        if (m_kAni.GetClip(gunFireAni) != null)
                        {
                            m_kAni.Stop();
                            m_kAni.Play(gunFireAni);
                        }
                    }
                    else
                    {
                        gunFireAni = m_kWL.FireAni.Length == 1 ? m_kWL.FireAni[0] : m_kWL.FireAni[1];
                        if (m_kAni2.GetClip(gunFireAni) != null)
                        {
                            m_kAni2.Stop();
                            m_kAni2.Play(gunFireAni);
                        }
                    }
                }
            }

            

            float burstCD = 0;
            if (m_kWL.FireMode == FIRE_MODE_BURST)
            {
                if (m_kWL.BurstRate <= 0)
                    Logger.Warning("BurstRate <= 0 " + m_kWL.ID);
                else
                    burstCD = 60f / m_kWL.BurstRate;
            }

            TimerManager.SetTimeOut(burstCD, () =>
            {
                if (m_released || m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                    return;

                if (GameSetting.enableGunFire)
                {
                    if (m_kEffectGunFire != null)
                        m_kEffectGunFire.Play();
                    if (m_kEffectGunFire2 != null)
                        m_kEffectGunFire2.Play();
                    AttachAndPlayCartridgeEffect();
                }
            });
        }

        DoOtherPlayerRayTest(firePoint, Vector3.Normalize(arg.hitPos - firePoint), Effect.MAT_EFFECT_GUN);
    }

    virtual protected void DoOtherPlayerRayTest(Vector3 rayPos, Vector3 rayDir, string matEffectType)
    {
        if (GameSetting.enableOPRayTest == false)
            return;

        m_hitBuildingTick++;

        if (isFPWatchPlayer || m_hitBuildingTick >= GlobalConfig.gameSetting.OPHitBuildingInterval)
        {
            m_hitBuildingTick = 0;
            RaycastHit kRH;
            if (Physics.Raycast(rayPos, rayDir, out kRH, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_BUILDING))
            {
                if (isFPWatchPlayer || WorldManager.singleton.isViewBattleModel || (MainPlayer.singleton != null && (!MainPlayer.singleton.isAlive || Vector3.SqrMagnitude(kRH.point - MainPlayer.singleton.position) <= Mathf.Pow(GlobalConfig.gameSetting.HitBuildingEffectDis, 2))))
                {
                    int matType = SceneManager.GetMaterialType(kRH.collider.gameObject);
                    PlayHitBuildingEffect(kRH.point, kRH.normal, matType, matEffectType, 0, rayDir, rayPos, true);
                }
            }
        }
    }

#endregion

#region Reload Ammo 

    public override void ReloadAmmo()
    {
        if(m_kWL.ReloadType == GameConst.WeaponReloadTypeWholly)
        {
            ReloadAmmoWholly();
        }
        else if (m_kWL.ReloadType == GameConst.WeaponReloadTypeOneByOne)
        {
            ReloadAmmoOneByOne();
        }
        else
        {
            Logger.Error("ReloadAmmo ReloadType Err! Use Wholly Reload!");
            ReloadAmmoWholly();
        }
    }

    override internal void ReloadAmmo2(toc_fight_reload arg)
    {
        if (m_kWL.ReloadType == GameConst.WeaponReloadTypeWholly)
        {
            ReloadAmmo2Wholly(arg);
        }
        else if (m_kWL.ReloadType == GameConst.WeaponReloadTypeOneByOne)
        {
            ReloadAmmo2OneByOne(arg);
        }
        else
        {
            Logger.Error("ReloadAmmo2 ReloadType Err! Use Wholly Reload!");
            ReloadAmmo2Wholly(arg);
        }
    }

    override public void ReloadAmmoComplete()
    {
        if (m_released || m_kPlayer == null || m_kPlayer.alive == false)
            return;

        int iAddAmmoNum = m_kLWD.clipSize - m_kLWD.curClipAmmoNum;
        if (m_kLWD.extraAmmoNum <= iAddAmmoNum)
        {
            iAddAmmoNum = m_kLWD.extraAmmoNum;
        }

        m_kLWD.extraAmmoNum -= iAddAmmoNum;
        m_kLWD.curClipAmmoNum += iAddAmmoNum;

        m_iReloadTimer = -1;
        m_iAniIndex = -1;
        m_kSpread.ClearRecoilAndSpread();
        if (m_kPlayer is MainPlayer)
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);
        m_kPlayer.OnReloadAmmoComplete();

        SendReloadMsg(false);
    }

    public override void StopReload()
    {
        if (m_iReloadTimer != -1)
            TimerManager.RemoveTimeOut(m_iReloadTimer);
        m_iReloadTimer = -1;

        if (m_kPlayer != null && m_kPlayer.status != null && m_kPlayer.status.reload)
        {
            m_kPlayer.OnReloadAmmoComplete();
            if (m_kWL.ReloadType == GameConst.WeaponReloadTypeOneByOne)
            {
                StopWeaponAni();
                m_kPlayer.playerAniController.CrossFade(m_kWL.MPIdleAni, 0.2f);
                SendReloadMsg(true);
            }
        }
    }

    #region 单段式上弹

    virtual internal void ReloadAmmoWholly()
    {
        if (isMainPlayer)
        {
            ZoomOutImmediate();
            if (m_scopeButtonPressed)
                GameDispatcher.Dispatch(GameEvent.UI_SET_GUNSCOPE_BUTTON_STATUS, 0);

            if (m_kPlayer.status.reload)
                return;

            if (hasExtAmmo == false)
            {
                m_kPlayer.OnReload_NoAmmo();
                return;
            }
            else if (IsCurClipFull() == true)
            {
                m_kPlayer.OnReload_AmmoFull();
                return;
            }
        }

        StopFire();
        StopWeaponAni();

        m_kPlayer.status.reload = true;
        PlayerAniBaseController kPA = m_kPlayer.playerAniController;
        if (m_kWL.MPReloadAni.Length != 0)
        {
            kPA.Play(m_kWL.MPReloadAni[0]);
        }
        m_kPlayer.OnReload_Begin();

        if (m_kWL.ReloadAni.Length != 0)
        {
            if (m_kAni.GetClip(m_kWL.ReloadAni[0]) != null)
                m_kAni.Play(m_kWL.ReloadAni[0]);
            if(isBothHandSlot)
            {
                var index = m_kWL.ReloadAni.Length == 1 ? 0 : 1;
                if (m_kAni2.GetClip(m_kWL.ReloadAni[index]) != null)
                    m_kAni2.Play(m_kWL.ReloadAni[index]);
            }
        }

        if (m_kWL.ReloadSound != null && m_kWL.ReloadSound.Length >= 3)
        {
            var index = 1;
            while (index < m_kWL.ReloadSound.Length)
            {
                DelayPlayOneShot(float.Parse(m_kWL.ReloadSound[index]), m_kWL.ReloadSound[index + 1]);
                index += 2;
            }
        }

        m_iReloadTimer = TimerManager.SetTimeOut(m_kLWD.reloadTime, ReloadAmmoComplete);

        //CDict arg = new CDict();
        //arg[ServerMsgKey.reloadTime] = m_kLWD.reloadTime;
        //MainPlayer.SendMsg("reload", arg);

        if (isMainPlayer)
        {
            NetLayer.Send(new tos_fight_reload()
            {
                reloadTime = m_kPlayer.serverData.reloadTime(atkType)
            });
            GameDispatcher.Dispatch<float, bool>(GameEvent.MAINPLAYER_RELOADBULLET_CD_MASK, Time.realtimeSinceStartup, true);
        }
    }


    virtual internal void ReloadAmmo2Wholly(toc_fight_reload arg)
    {
        if (m_released || m_kPlayer == null || m_kPlayer.playerAniController == null)
            return;

        if (gameObject == null || m_kPlayer.serverData.alive == false)
            return;

        if (m_kPlayer is OtherPlayer)
        {
            float reloadTime = arg.reloadTime;
            if (reloadTime == 0)
                reloadTime = m_kWL.ReloadTime;

            m_kPlayer.playerAniController.Play(m_kWL.OPReloadAni);
            m_iReloadTimer = TimerManager.SetTimeOut(reloadTime, () =>
            {
                m_iReloadTimer = -1;
                if (m_kPlayer == null || m_kPlayer.playerAniController == null)
                {
                    return;
                }
                m_kPlayer.playerAniController.CrossFade(m_kWL.OPIdleAni);
            });
        }
        else if (m_kPlayer is FPWatchPlayer)
            ReloadAmmo();
    }
#endregion 

    #region 多段式上弹
    virtual internal void ReloadAmmo2OneByOne(toc_fight_reload arg)
    {
        m_reloadAmmoCount = arg.reloadConut;
        ReloadAmmo2Wholly(arg);
    }

    virtual internal void ReloadAmmoOneByOne()
    {
        if (isMainPlayer)
        {
            if (hasExtAmmo == false)
            {
                m_kPlayer.OnReload_NoAmmo();
                return;
            }
            else if (IsCurClipFull() == true)
            {
                m_kPlayer.OnReload_AmmoFull();
                return;
            }
        }
        else if (isFPWatchPlayer)
        {
            if (m_reloadAmmoCount == 0)
            {
                m_kPlayer.OnReload_NoAmmo();
                return;
            }
        }

        if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
        {
            Logger.Warning("冷却没完成，不能装弹");
            return;
        }

        StopFire();
        m_kPlayer.status.reload = true;
        
        float reloadAni1Time = float.Parse(m_kWL.MPReloadAni[1]);
        float reloadAni3Time = float.Parse(m_kWL.MPReloadAni[5]);
        float singleBulletReloadTime = (m_kLWD.reloadTime - reloadAni1Time - reloadAni3Time) / m_kLWD.maxAmmoNum;

        m_kPlayer.OnReload_Begin();
        m_kPlayer.playerAniController.Play(m_kWL.MPReloadAni[0]);
        if (m_kWL.ReloadAni.Length == 3 && !string.IsNullOrEmpty(m_kWL.ReloadAni[0]))
        {
            m_kAni.Play(m_kWL.ReloadAni[0]);
        }

        if (m_iReloadTimer != -1)
            TimerManager.RemoveTimeOut(m_iReloadTimer);

        m_iReloadTimer = TimerManager.SetTimeOut(reloadAni1Time, () =>
        {
            PlayReloadAni2();
        });

        int iAddAmmoNum = 0;
        if (!isSubWeapon)
        {
            iAddAmmoNum = m_kLWD.maxAmmoNum - m_kLWD.curClipAmmoNum;
            if (m_kLWD.extraAmmoNum <= iAddAmmoNum)
            {
                iAddAmmoNum = m_kLWD.extraAmmoNum;
            }
        }

        //CDict arg = new CDict();
        //arg[ServerMsgKey.reloadTime] = reloadAni1Time + reloadAni3Time + singleBulletReloadTime * iAddAmmoNum;
        //MainPlayer.SendMsg("reload", arg);

        if (isMainPlayer)
        {
            NetLayer.Send(new tos_fight_reload()
            {
                reloadTime = reloadAni1Time + reloadAni3Time + singleBulletReloadTime * iAddAmmoNum,
                reloadConut = iAddAmmoNum,
            });
        }

        if (m_kWL.ReloadSound != null && m_kWL.ReloadSound.Length >= 3)
            DelayPlayOneShot(float.Parse(m_kWL.ReloadSound[1]), m_kWL.ReloadSound[2]);
    }

    internal void PlayReloadAni2()
    {
        float reloadAni1Time = float.Parse(m_kWL.MPReloadAni[1]);
        float reloadAni3Time = float.Parse(m_kWL.MPReloadAni[5]);
        float singleBulletReloadTime = (m_kLWD.reloadTime - reloadAni1Time - reloadAni3Time) / m_kLWD.maxAmmoNum;

        if (m_kWL.ReloadAni.Length >= 2 && !string.IsNullOrEmpty(m_kWL.ReloadAni[m_kWL.ReloadAni.Length - 2]))
        {
            m_kAni.Play(m_kWL.ReloadAni[m_kWL.ReloadAni.Length - 2]);
        }
        m_kPlayer.playerAniController.Play(m_kWL.MPReloadAni[2]);

        m_iReloadTimer = TimerManager.SetTimeOut(singleBulletReloadTime, OnReloadAni2End);

        // TODO: 播放装子弹声音
        if (m_kWL.ReloadSound != null && m_kWL.ReloadSound.Length >= 5)
            DelayPlayOneShot(float.Parse(m_kWL.ReloadSound[3]), m_kWL.ReloadSound[4]);
    }

    internal void PlayReloadAni3()
    {
        if (m_kWL.ReloadAni.Length >= 2 && !string.IsNullOrEmpty(m_kWL.ReloadAni[m_kWL.ReloadAni.Length - 1]))
        {
            m_kAni.Play(m_kWL.ReloadAni[m_kWL.ReloadAni.Length - 1]);
        }
        m_kPlayer.playerAniController.CrossFade(m_kWL.MPReloadAni[4], 0.2f);

            // TODO: 播放上膛声音
        if (m_kWL.ReloadSound != null && m_kWL.ReloadSound.Length >= 7)
            DelayPlayOneShot(float.Parse(m_kWL.ReloadSound[5]), m_kWL.ReloadSound[6]);

        float reloadAni3Time = float.Parse(m_kWL.MPReloadAni[5]);
        m_iReloadTimer = TimerManager.SetTimeOut(reloadAni3Time, () =>
        {
            ReloadAmmoComplete();
        });
    }

    internal void OnReloadAni2End()
    {
        if (m_kPlayer.status.reload == false)
            return;

        m_iReloadTimer = -1;

        if (isMainPlayer)
        {
            m_kLWD.extraAmmoNum -= 1;
            m_kLWD.curClipAmmoNum += 1;

            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);
            SendReloadMsg(true);

            if (IsCurClipFull() == false && m_kLWD.extraAmmoNum > 0)
            {
                PlayReloadAni2();
            }
            else
            {
                PlayReloadAni3();
            }
        }
        else
        {
            m_reloadAmmoCount -= 1;
            if (m_reloadAmmoCount > 0)
                PlayReloadAni2();
            else
                PlayReloadAni3();
        }
    }
    #endregion

#endregion

#region Switch

    public override void WeaponSwitchOff()
    {
        base.WeaponSwitchOff();

        ZoomOutImmediate();

        if(m_kSpread != null) m_kSpread.ClearRecoilAndSpread();

        m_kPlayer.status.reload = false;
        m_kPlayer.status.fire = false;
        m_iAniIndex = -1;

        if (m_iReloadTimer != -1)
            TimerManager.RemoveTimeOut(m_iReloadTimer);

        // 当前枪的Switch动画可能还没播放完，就被打断了，于是要跳到最后一帧
        m_kAni.Jump2End();
        if(isBothHandSlot)
            m_kAni2.Jump2End();

        // 关闭声音
        m_kSP.EndGunFireSound();

        if(m_kPlayer is MainPlayer)
        {
            if (hasScopeButton)
            {
                GameDispatcher.Dispatch(GameEvent.UI_SHOW_GUNSCOPE_BUTTON, false);
            }
        }
    }

    public override void WeaponSwitched()
    {
        base.WeaponSwitched();

        if (m_released)
            return;

        m_iAniIndex = -1;

        AttachFireEffect();

        if (m_kGO2 != null)
            AttachFireEffect2();

        if (isMainPlayer || (isFPWatchPlayer && (m_kPlayer as FPWatchPlayer).isSwitchViewDone))
        {
            if (m_kWL.SwitchAni.Length != 0)
            {
                if (m_kAni.GetClip(m_kWL.SwitchAni[0]) != null)
                    m_kAni.Play(m_kWL.SwitchAni[0]);
                if (isBothHandSlot )
                {
                    var index = m_kWL.SwitchAni.Length > 1 ? 1 : 0;
                    if (m_kAni2.GetClip(m_kWL.SwitchAni[index]) != null)
                        m_kAni2.Play(m_kWL.SwitchAni[index]);
                }
            }

            if (m_kWL.IdleAni.Length != 0)
            {
                if (m_kLWD.extraAmmoNum > 0)
                {
                    if (m_kAni.GetClip(m_kWL.IdleAni[0]) != null)
                    {
                        m_kAni.PlayQueued(m_kWL.IdleAni[0], QueueMode.CompleteOthers);
                    }
                }
                else
                {
                    if (m_kWL.IdleAni.Length >= 2)
                    {
                        if (m_kAni.GetClip(m_kWL.IdleAni[1]) != null)
                        {
                            m_kAni.CrossFadeQueued(m_kWL.IdleAni[1], 0.5f, QueueMode.CompleteOthers);
                        }
                    }
                }
            }

            if (Time.time - m_switchSoundPlayTime >= GameSetting.SWITCH_WEAPON_SOUND_PLAY_MIN_INTERVAL)
            {
                m_switchSoundPlayTime = Time.time;

                if (m_kWL.SwitchSound != null && m_kWL.SwitchSound.Length >= 3)
                {
                    var index = 1;
                    while (index < m_kWL.SwitchSound.Length)
                    {
                        DelayPlayOneShot(float.Parse(m_kWL.SwitchSound[index]), m_kWL.SwitchSound[index + 1]);
                        index += 2;
                    }
                }
            }
           

            if(m_kPlayer is MainPlayer && hasScopeButton)
            {
                GameDispatcher.Dispatch(GameEvent.UI_SHOW_GUNSCOPE_BUTTON, true);
            }
        }

        if (m_kPlayer is MainPlayer && ammoNumInClip == 0 && ammoNumLeft > 0)
        {
            TimerManager.SetTimeOut(fireRate, () =>
            {
                if (m_released || m_kPlayer == null || m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                    return;
                ReloadAmmo();
            });
        }

        if (m_kPlayer is OtherPlayer)
        {
            PlayOpWeaponSwitch();
        }
    }
#endregion

#region OnKey

    override public void OnFireKeyDown()
    {
        PlayerStatus status = m_kPlayer.status;

        if (status.switchWeapon)
            return;

        if(status.reload)
        {
            if (m_kWL.ReloadType == GameConst.WeaponReloadTypeOneByOne)
                StopReload();
            else
                return;
        }

        m_bFirePressed = true;
        m_fFireKeyPressTime = Time.timeSinceLevelLoad;
        m_bIsFireBefore = true;

        if (!canFire)
            return;

        if (m_kWL.FireDelay > 0)
        {
            if (Time.timeSinceLevelLoad - lastFireTime > m_kWL.FireAfterDelay)
                PlayFireBeginAni();
            else
            {
                m_bIsPlayFireBeginAni = true;
                m_fFireKeyPressTime = 0;
            }
        }
        else
        {
            float tempFireRate = fireRate;
            if (m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
            {
                tempFireRate = 60f / m_kWL.BurstRate; 
            }
            if (Time.timeSinceLevelLoad - lastFireTime >= tempFireRate)
            {
                if (m_kWL.FireMode == WeaponBase.FIRE_MODE_SINGLETIME || m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
                    m_bIsFireBefore = false;
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT; 
                TryToFire();
            }
        }
    }

    override public void OnFireKeyUp()
    {
        if (m_bFirePressed == false)
            return;

        m_bFirePressed = false;
        
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon || !canFire)
            return;

        float tempFireRate = fireRate;
        if (m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
        {
            tempFireRate = 60f / m_kWL.BurstRate;
        }

        if (m_kWL.FireDelay > 0 && !m_bIsPlayFireBeginAni)
            m_fFireKeyPressTime = Time.timeSinceLevelLoad;

        float pressTime = Time.timeSinceLevelLoad - m_fFireKeyPressTime;

        if (m_kWL.FireMode == WeaponBase.FIRE_MODE_SINGLETIME || m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
        {
            if (m_bIsFireBefore && pressTime >= m_kWL.FireDelay && Time.timeSinceLevelLoad - lastFireTime >= tempFireRate)
            {
                m_bIsFireBefore = false;
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
            }
        }
        else if (m_kWL.FireMode == WeaponBase.FIRE_MODE_RAPID)
        {
            float interval = Time.timeSinceLevelLoad - lastFireTime;
            if (pressTime >= m_kWL.FireDelay && interval >= tempFireRate && pressTime > FIRE_ONE_SHOT_INVERTAL_TIME)
            {
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
            }
            //else
            //{
            //   Logger.Error("单射失败， 0=" + (pressTime >= m_kWL.FireDelay) + " 1=" + (interval >= fFireCD) + " 2=" + (pressTime <= FIRE_ONE_SHOT_INVERTAL_TIME) + "  cd=" + fFireCD + " pressTime=" + pressTime
            //        + " interval = " + interval + " delay=" + m_kWL.FireDelay);
            //}
        }

        if (status.fire == true)
        {
            StopFire();
            status.fire = false;
        }

        m_fFireKeyPressTime = 0;
    }

    override protected void OnFireUpdate()
    {
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon || !canFire)
            return;

        if (m_kWL.FireDelay > 0 && !m_bIsPlayFireBeginAni)
        {
            if (Time.timeSinceLevelLoad - lastFireTime > m_kWL.FireAfterDelay)
            {
                m_fFireKeyPressTime = Time.timeSinceLevelLoad;
                PlayFireBeginAni();
            }
            else
            {
                m_fFireKeyPressTime = 0;
                m_bIsPlayFireBeginAni = true;
            }
        }

        float pressTime = Time.timeSinceLevelLoad - m_fFireKeyPressTime;
        float tempFireRate = fireRate;
        if (m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
        {
            tempFireRate = 60f / m_kWL.BurstRate;
        }
        //非扫射枪的延迟模式
        if (m_kWL.FireMode == WeaponBase.FIRE_MODE_SINGLETIME || m_kWL.FireMode == WeaponBase.FIRE_MODE_BURST)
        {
            if (m_bIsFireBefore && pressTime >= m_kWL.FireDelay && Time.timeSinceLevelLoad - lastFireTime >= tempFireRate)
            {
                m_bIsFireBefore = false;
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
            }
        }
        // 扫射枪
        else if (m_kWL.FireMode == WeaponBase.FIRE_MODE_RAPID)
        {
            float interval = Time.timeSinceLevelLoad - lastFireTime;

            if (pressTime >= m_kWL.FireDelay && interval >= fireRate && pressTime > FIRE_ONE_SHOT_INVERTAL_TIME)
            {
                m_fireType = WEAPON_FIRE_TYPE_STRAFE;
                TryToFire();
                //Logger.Log("扫射");
            }
        }
    }

    public override void OnZoomIn()
    {
        m_scopeButtonPressed = true;
        ZoomIn();
    }

    public override void OnZoomOut()
    {
        m_scopeButtonPressed = false;
        ZoomOut();
    }

    protected override void ZoomIn()
    {
        if (m_kGunScope != null)
        {
            if (isMainPlayer)
            {
                NetLayer.Send(new tos_fight_weapon_motion()
                {
                    weapon_id = m_kWL.ID,
                    motion = WeaponConst.Weapon_Motion_Scope_ZoomIn
                });
            }
            m_kGunScope.ZoomIn(true);
        }
    }

    // 关镜按钮
    protected override void ZoomOut()
    {
        if (m_kGunScope != null)
        {
            if (isMainPlayer)
            {
                NetLayer.Send(new tos_fight_weapon_motion()
                {
                    weapon_id = m_kWL.ID,
                    motion = WeaponConst.Weapon_Motion_Scope_ZoomOut
                });
            }
            m_kGunScope.ZoomOut();
        }
        m_scopeButtonPressed = false;
    }

    public override  void ZoomInImmediate()
    {
        if (!isFPV)
            return;

        if (m_kGunScope != null)
        {
            m_kGunScope.ZoomInImmediate();
        }
    }

    public override void ZoomOutImmediate()
    {
        if (!isFPV)
            return;

        if (m_kGunScope != null)
        {
            if (isMainPlayer)
            {
                NetLayer.Send(new tos_fight_weapon_motion()
                {
                    weapon_id = m_kWL.ID,
                    motion = WeaponConst.Weapon_Motion_Scope_ZoomOutImmediate
                });
            }
            m_kGunScope.ZoomOutImmediate();
        }

        if(isMainPlayer)
        { 
            MainPlayer.singleton.UpdateMoveSpeed();
            m_scopeButtonPressed = false;

            if (hasScopeButton)
            GameDispatcher.Dispatch(GameEvent.UI_SET_GUNSCOPE_BUTTON_STATUS, 0);
        }
    }

    protected override void ZoomInComplete()
    {
        if (m_released)
            return;

        if (m_kPlayer is MainPlayer)
        {
            MainPlayer.singleton.UpdateMoveSpeed();
        }
    }

    protected override void ZoomOutComplete()
    {
        if (m_released)
            return;

        if (m_kPlayer is MainPlayer)
        {
            MainPlayer.singleton.UpdateMoveSpeed();
        }
    }

    protected void PlayFireBeginAni()
    {
        if (!string.IsNullOrEmpty(m_kWL.MPFireBeginAni))
        {
            PlayerAniBaseController kPA = m_kPlayer.playerAniController;
            kPA.Play(m_kWL.MPFireBeginAni);
        }

        if (m_kWL.FireBeginAni.Length != 0)
        {
            if (m_kAni.GetClip(m_kWL.FireBeginAni[0]) != null)
            {
                m_kAni.Play(m_kWL.FireBeginAni[0]);
            }
            if (isBothHandSlot)
            {
                var index = m_kWL.FireBeginAni.Length == 1 ? 0 : 1;

                if (m_kAni2.GetClip(m_kWL.FireBeginAni[index]) != null)
                {
                    m_kAni2.Play(m_kWL.FireBeginAni[index]);
                }
            }
        }
        m_bIsPlayFireBeginAni = true;
    }

#endregion

#region effect
    override public void PlayMPFireMountEffect()
    {
        if (m_kWL.MPFireMountEffect != null && m_kWL.MPFireMountEffect.Length != 0)
        {
            string fireMountEffect = "";
            if (m_kWL.MPFireMountEffect.Length == 1)
                fireMountEffect = m_kWL.MPFireMountEffect[0];
            else if (m_kWL.MPFireMountEffect.Length == 2)
                fireMountEffect = m_fireType == WEAPON_FIRE_TYPE_ONESHOT ? m_kWL.MPFireMountEffect[0] : m_kWL.MPFireMountEffect[1];

            Transform fireMountEffectTran = null;
            if (!m_fireModelMountEffect.TryGetValue(fireMountEffect, out fireMountEffectTran))
            {
                fireMountEffectTran = m_kPlayer.transform.FindChild(fireMountEffect);
                if (fireMountEffectTran == null)
                    fireMountEffectTran = m_kPlayer.transform.FindChild(m_kPlayer.transform.name + "/" + fireMountEffect);
                m_fireModelMountEffect.Add(fireMountEffect, fireMountEffectTran);
            }
            if (fireMountEffectTran != null)
                EffectManager.PlayEffect(fireMountEffectTran, true, true);
        }
    }
    #endregion

}

