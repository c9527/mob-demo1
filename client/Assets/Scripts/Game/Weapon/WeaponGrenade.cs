﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WeaponGrenade : WeaponBase
{
    // TODO: 不同模型高度不同
    protected const float THROW_HEIGHT_STAND = 2.0f;
    protected const float THROW_HEGITH_CROUCH = 1.0f;
    protected const float THROW_OFFSET_ANGLE = 5f;

    protected const float THROW_ANI_FRAME_TIME = 0.3f;
    protected const float THROW_ANI_TIME = 0.6f;

    protected const int FIRE_POSE_PULLPIN = 0;
    protected const int FIRE_POSE_THROW = 1;

    protected string m_bulletType = "";

    protected Vector3 m_vec3HitPos;

    protected int m_iFireFrameTimer;
    protected int m_iFireEndTimer;

    protected int m_bulletIndex;

    //protected Vector3 m_firePoint;

    protected EffectGunFire m_kEffectGunFire;
    protected EffectGunFire m_kEffectGunFire2;
    protected Transform m_transFirePoint;
    protected Transform m_transFirePoint2;

    public WeaponGrenade()
    {
        m_bulletType = BulletBase.TYPE_GRENADE;
    }

    override public void Reset()
    {
        base.Reset();
    }

    override public void Release()
    {
        base.Release();

        if (m_iFireFrameTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iFireFrameTimer);
            m_iFireFrameTimer = -1;
        }
           
        if (m_iFireEndTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iFireEndTimer);
            m_iFireEndTimer = -1;
        }

        m_bulletIndex = 0;

        if (m_kEffectGunFire != null)
        {
            EffectManager.singleton.Reclaim(m_kEffectGunFire);
            m_kEffectGunFire = null;
        }
            
        if (m_kEffectGunFire2 != null)
        {
            EffectManager.singleton.Reclaim(m_kEffectGunFire2);
            m_kEffectGunFire2 = null;
        }

        m_transFirePoint = null;
        m_transFirePoint2 = null;
    }

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        m_transFirePoint = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
        if (m_transFirePoint == null)
            m_transFirePoint = m_kTrans;
        base.InitCrossHair();

    }

    public override void SetGO2(GameObject go)
    {
        base.SetGO2(go);

        m_transFirePoint2 = GameObjectHelper.FindChildByTraverse(go.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
        if (m_transFirePoint2 == null)
            m_transFirePoint2 = m_kTrans2;
    }


#region Fire

    public override string TestWeapon()
    {
        string strRet = WEAPON_TEST_UNKNOW;
        if (isSubWeapon)
        {
            if (m_kLWD.subAmmoNum > 0)
                strRet = WEAPON_TEST_SUCCESS;
            else
                strRet = WEAPON_TEST_NO_AMMO;
        }
        else if (m_kLWD.curClipAmmoNum > 0 || m_kLWD.curClipAmmoNum == -1)
        {
            strRet = WEAPON_TEST_SUCCESS;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum > 0)
        {
            strRet = WEAPON_TEST_NEED_RELOAD;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum <= 0)
        {
            strRet = WEAPON_TEST_NO_AMMO;
        }

        return strRet;
    }

    virtual protected void PullPin()
    {
        if (m_kEffectGunFire != null)
            m_kEffectGunFire.Play();
        if (m_kEffectGunFire2 != null)
            m_kEffectGunFire2.Play();

        m_kPlayer.playerAniController.Play(m_kWL.MPFireAni[0]);

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
            m_kSP.PlayOneShot(m_kWL.FireSound[1], AudioManager.FightSoundVolume);
    }

    virtual protected void Throw()
    {
        if (m_kEffectGunFire != null)
            m_kEffectGunFire.Stop();
        if (m_kEffectGunFire2 != null)
            m_kEffectGunFire2.Stop();

        m_weaponMgr.usingThrowWeapon = true;

        lastFireTime = Time.timeSinceLevelLoad;
        m_bulletIndex = ProjectileManager.GetNextBulletIndex();

        m_kPlayer.playerAniController.CrossFade(m_kWL.MPFireAni[1], 0.2f);
        if (m_kWL.FireDelay < 0)
            OnFireFrame();
        else
            m_iFireFrameTimer = TimerManager.SetTimeOut(m_kWL.FireDelay == 0 ? THROW_ANI_FRAME_TIME : m_kWL.FireDelay, OnFireFrame);
        if (m_kWL.FireAfterDelay < 0)
            OnFireEndFrame();
        else
            m_iFireEndTimer = TimerManager.SetTimeOut(m_kWL.FireAfterDelay == 0 ? THROW_ANI_TIME : m_kWL.FireAfterDelay, OnFireEndFrame);

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 3)
            m_kSP.PlayOneShot(m_kWL.FireSound[2], AudioManager.FightSoundVolume);

        //RaycastHit rh;
        //if (Physics.Raycast(AutoAim.CurAimRay, out rh, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER))
        //{
        //    Vector3 dir = rh.point - GetStartPos();
        //    m_vec3ThrowDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * dir;
        //}
        //else
        //    m_vec3ThrowDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * SceneCamera.singleton.forward;


        if (isFPV && m_kWL.FireSound.Length >= 4)
            AudioManager.PlayFightUIVoice(m_kWL.FireSound[3], true);

        
    }

    internal override void Atk(toc_fight_atk arg)
    {
        //if (gameObject == null || m_kPlayer.serverData.alive == false)    // 第三人称手雷第一次扔的时候gameObject会为空正常，否则后面逻辑执行不了
        if (m_kPlayer.serverData.alive == false)
            return;

        m_vec3HitPos = arg.fireDir;
        if (m_kPlayer is OtherPlayer)
        {
            if (m_bIsWalkWaggle && m_kPlayer.playerAniController.animator != null && m_kPlayer.playerAniController.animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                m_kPlayer.playerAniController.EnableWalkWaggleLayer(false);
            m_kPlayer.playerAniController.CrossFade(m_kWL.OPFireAni[0], 0.2f, 1);
           
            if (m_kWL.FireDelay < 0)
                OnFireFrame();
            else
                m_iFireFrameTimer = TimerManager.SetTimeOut(m_kWL.FireDelay == 0 ? THROW_ANI_FRAME_TIME : m_kWL.FireDelay, OnFireFrame);
            if (m_kWL.FireAfterDelay < 0)
                OnFireEndFrame();
            else
                m_iFireEndTimer = TimerManager.SetTimeOut(m_kWL.FireAfterDelay == 0 ? THROW_ANI_TIME : m_kWL.FireAfterDelay, OnFireEndFrame);
        }
        else
        {
            Throw();
        }
    }

    protected virtual void OnFireFrame()
    {
        if (m_released)
            return;

        m_iFireFrameTimer = -1;

        SetActive(false);

        ReduceAmmoNum();

        Vector3 fireDir;
        Vector3 startPos = GetStartPos();
        if (isMainPlayer)
        {
            if(m_isSubWeapon)
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subAmmoNum);
            else
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);

            RaycastHit rh;
            Vector3 hitPos;
            var mask =  GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY |
                GameSetting.LAYER_MASK_DMM_PLAYER;
            //治疗手雷
//            mask |= IsHeal ? GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND : 0;
            if (Physics.Raycast(SceneCamera.singleton.ray, out rh, GameSetting.FIRE_RAYTEST_LENGTH, mask))
            {
                hitPos = rh.point;
                Vector3 dir = rh.point - startPos;
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * dir;
            }
            else
            {
                hitPos = SceneCamera.singleton.position + SceneCamera.singleton.forward * 50.0f;
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, SceneCamera.singleton.right) * SceneCamera.singleton.forward;
            }

            NetLayer.Send(new tos_fight_atk()
            {
                fireDir = hitPos,
                index = m_bulletIndex,
                atkType = atkType
            });
        }
        else
        {
            fireDir = Vector3.Normalize(m_vec3HitPos - startPos);
            if(m_kPlayer.transform != null)
                fireDir = Quaternion.AngleAxis((m_kBL == null ? THROW_OFFSET_ANGLE : m_kBL.ThrowAngle) * -1f, m_kPlayer.transform.right) * fireDir;
        }

        BulletBase bullet = BulletManager.CreateBullet(m_bulletType, m_kWL, isFPV);
        bullet.Fire(startPos, Vector3.zero, fireDir, m_kWL, m_kPlayer, m_bulletIndex);

        m_weaponMgr.usingThrowWeapon = false;
    }

    protected virtual void OnFireEndFrame()
    {
        if (m_released)
            return;

        m_iFireEndTimer = -1;

        if (!m_isSubWeapon)
        {
            // TODO: OtherPlayer暂时没有同步弹药数据
            //if (!(m_kPlayer is MainPlayer)) return;
            if (m_kPlayer is MainPlayer)
            {
                if (m_bIsSkillFire)
                {
                    if (m_fireCallBack != null)
                    {
                        m_fireCallBack();
                        m_fireCallBack = null;
                    }
                }
                else
                {
                    if (hasAmmoCurClip || hasExtAmmo)
                    {
                        m_kPlayer.SwitchWeapon(m_kWL.ID.ToString(), false, true);    // 这里要告诉第三人称再切手雷
                    }
                    else
                    {
                        m_kPlayer.weaponMgr.preWeapon = m_kPlayer.GetNextWeaponID();
                        m_kPlayer.SwitchWeapon(m_kPlayer.weaponMgr.preWeapon, true, false);
                        //m_kPlayer.SwitchPreWeapon();
                    }
                }
            }
            else
            {
                m_kPlayer.SwitchWeapon(m_kWL.ID.ToString(), false, true);
            }
        }
        else
        {
            m_kPlayer.SwitchWeapon(m_kPlayer.weaponMgr.curWeapon.weaponId, true, true);
        }
    }

    protected virtual Vector3 GetStartPos()
    {
        Vector3 pos = m_kPlayer.position;

        if(m_kPlayer.status.stand)
        {
            pos.y += THROW_HEIGHT_STAND * m_kPlayer.scale;
        }
        else if(m_kPlayer.status.crouch)
        {
            pos.y += THROW_HEGITH_CROUCH * m_kPlayer.scale;
        }

        return pos;
    }

    protected override void TryToFire()
    {
        base.TryToFire();

        if(TestWeapon() == WEAPON_TEST_SUCCESS)
            Throw();
    }

    protected override void DoFireReally()
    {
         
    }

    override public void StopFire()
    {
        base.StopFire();
    }

    //protected override void ReduceAmmoNum()
    //{
    //    if (WorldManager.singleton.BattleType != GameConst.GAME_RULE_GRENADE )
    //    {
    //        base.ReduceAmmoNum();
    //    }
        
    //} 
#endregion  

#region Switch

    public override void WeaponSwitched()
    {
        //base.WeaponSwitched();
        if (m_released)
            return;
        SetAwakenInfo();

        AttachFireEffect();
        if (m_kGO2 != null)
            AttachFireEffect2();

        if (m_kEffectGunFire != null)
            m_kEffectGunFire.Stop();
        if (m_kEffectGunFire2 != null)
            m_kEffectGunFire2.Stop();

        //-1表示无限手雷,无需reload
        if(m_kLWD.curClipAmmoNum <= 0 && m_kLWD.curClipAmmoNum != -1)
        {
            // 手雷扔完后，会再切到手雷，这个时候更新弹药数
            int iAddAmmoNum = m_kLWD.clipSize - m_kLWD.curClipAmmoNum;
            if (m_kLWD.extraAmmoNum <= iAddAmmoNum)
            {
                iAddAmmoNum = m_kLWD.extraAmmoNum;
            }

            m_kLWD.extraAmmoNum -= iAddAmmoNum;
            m_kLWD.curClipAmmoNum += iAddAmmoNum;

            SendReloadMsg(false);
        }

        if (m_kPlayer is MainPlayer)
        {
            if (m_kWL.SwitchSound != null && m_kWL.SwitchSound.Length >= 3)
            {
                var index = 1;
                while (index < m_kWL.SwitchSound.Length)
                {
                    DelayPlayOneShot(float.Parse(m_kWL.SwitchSound[index]), m_kWL.SwitchSound[index + 1]);
                    index += 2;
                }
            }
        }        
    }

    public override void WeaponSwitchOff()
    {
        base.WeaponSwitchOff();

        if (m_kPlayer.weaponMgr.curWeapon == this)
            return;

        if (m_iFireFrameTimer != -1)
            TimerManager.RemoveTimeOut(m_iFireFrameTimer);
        if (m_iFireEndTimer != -1)
            TimerManager.RemoveTimeOut(m_iFireEndTimer);
    }
#endregion

#region OnKey

    override public void OnFireKeyDown()
    {
        PlayerStatus status = m_kPlayer.status;

        if (status.reload || !canFire) //  || status.switchWeapon
            return;

        if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
            return;

        if (hasAmmoCurClip)
        {
            m_bFirePressed = true;   
            if(!m_isSubWeapon)
                PullPin();
        }
    }

    override public void OnFireKeyUp()
    {
        if (m_bFirePressed == false)
            return;

        PlayerStatus status = m_kPlayer.status;
        //if (!m_kPlayer.alive || status.reload || status.switchWeapon)
        if (!m_kPlayer.alive || status.reload)
            return;

        m_bFirePressed = false;
        TryToFire();        
    }   
#endregion


    #region 其他

    protected void AttachFireEffect()
    {
        if (m_kEffectGunFire != null)
            return;

        string fireEffect = "";

        if (!string.IsNullOrEmpty(m_awakenFireEffect))
            fireEffect = m_awakenFireEffect;
        else
        {
            fireEffect = isFPV ? (m_kWL.FireEffect.Length > 0 ? m_kWL.FireEffect[0] : "") : (m_kWL.FireEffect2.Length > 0 ? m_kWL.FireEffect2[0] : "");
        }

        if (string.IsNullOrEmpty(fireEffect))
            return;

        EffectManager.singleton.GetEffect<EffectGunFire>(fireEffect, (kEffect) =>
        {
            if (m_released)
            {
                kEffect.Release();
                return;
            }

            if (m_kEffectGunFire != null)
                EffectManager.singleton.Reclaim(m_kEffectGunFire);

            m_kEffectGunFire = kEffect;
            m_kEffectGunFire.gameObject.TrySetActive(false);
            kEffect.Attach(m_transFirePoint);

            if (isFPV)
            {
                kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            }
            else
            {
                kEffect.SetFadeOut(5f, 0.5f);
            }

            //foreach (var renderer in kEffect.rendererList)
            //    SetRendererTintColor(renderer, m_awakenFireColor);

        });
    }

    protected void AttachFireEffect2()
    {
        if (m_kEffectGunFire2 != null)
            return;

        string fireEffect = "";

        if (!string.IsNullOrEmpty(m_awakenFireEffect))
            fireEffect = m_awakenFireEffect;
        else
        {
            fireEffect = isFPV ? (m_kWL.FireEffect.Length > 1 ? m_kWL.FireEffect[1] : (m_kWL.FireEffect.Length > 0 ? m_kWL.FireEffect[0] : "")) : (m_kWL.FireEffect2.Length > 1 ? m_kWL.FireEffect2[1] : (m_kWL.FireEffect2.Length > 0 ? m_kWL.FireEffect2[0] : ""));
        }

        if (string.IsNullOrEmpty(fireEffect))
            return;

        EffectManager.singleton.GetEffect<EffectGunFire>(fireEffect, (kEffect) =>
        {
            if (m_released)
            {
                kEffect.Release();
                return;
            }

            if (m_kEffectGunFire2 != null)
                EffectManager.singleton.Reclaim(m_kEffectGunFire2);

            m_kEffectGunFire2 = kEffect;
            m_kEffectGunFire2.gameObject.TrySetActive(false);
            kEffect.Attach(m_transFirePoint2);

            if (isFPV)
            {
                kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            }
            else
            {
                kEffect.SetFadeOut(5f, 0.5f);
            }

            //foreach (var renderer in kEffect.rendererList)
            //    SetRendererTintColor(renderer, m_awakenFireColor);

        });
    }
    #endregion

}
