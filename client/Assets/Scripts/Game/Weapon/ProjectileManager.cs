﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class Projectile
{
    public ConfigItemWeaponLine wl;
    public GameObject go;
    public bool attached;
    public Vector3 defalutScale = Vector3.one;
    public Action callback;
}

/// <summary>
/// 投射物管理器（榴弹，石头，箭等等）
/// </summary>
public class ProjectileManager
{
    static public ProjectileManager singleton;
    static Dictionary<int, List<Projectile>> m_dicItemCache = new Dictionary<int, List<Projectile>>();
    static List<GameObject> m_listAllItem = new List<GameObject>();

    private static int s_ProjectileIndex = -1;

    static public int GetNextBulletIndex()
    {
        return ++s_ProjectileIndex % GameSetting.MAX_EXIST_PROJECTILE_SIZE;
    }

    static public Projectile AttachProjectile(BasePlayer player, int weaponId)
    {
        ConfigItemWeaponLine wl = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weaponId);
        if(wl == null)
            return null;
        Action<GameObject> callback = (go)=>
        {
            WeaponBase.AttachToPlayer(false, player, wl, go.transform, null, Vector3.one);
            go.TrySetActive(true);
        };

        Projectile projectile = GetItem(wl, callback);
        return projectile;
    }

    static private Projectile GetItem(ConfigItemWeaponLine wl, Action<GameObject> funLoaded)
    {
        List<Projectile> listItem = null;
        m_dicItemCache.TryGetValue(wl.ID, out listItem);
        if (listItem == null)
        {
            listItem = new List<Projectile>();
            m_dicItemCache.Add(wl.ID, listItem);
        }

        Projectile item;
        int iRearIndex = listItem.Count - 1;
        if (iRearIndex != -1)
        {
            item = listItem[iRearIndex];
            listItem.RemoveAt(iRearIndex);
            if (!item.attached)
            {
                item.attached = true;
                if (funLoaded != null)
                    funLoaded(item.go);
            }
        }
        else
        {
            item = new Projectile();
            item.wl = wl;
            string strPath = PathHelper.GetWeaponPrefabPath(wl.OPModel);
            ResourceManager.LoadBattlePrefab(strPath, (go, crc) =>
            {
                if (go == null)
                {
                    Logger.Error("LoadRes strPath:[" + strPath + "] err");
                    return;
                }
                m_listAllItem.Add(go);
                item.go = go;
                item.defalutScale = go.transform.localScale;
                if (!item.attached)
                {
                    item.attached = true;
                    if (funLoaded != null)
                        funLoaded(item.go);
                    if (item.callback != null)
                        item.callback();
                }
            });
        }
        return item;
    }

    static public void DeactiveItem(Projectile item)
    {
        if (item == null)
            return;

        if (item.go != null)
        {
            item.go.transform.SetParent(SceneManager.singleton.reclaimRoot);
            item.go.transform.localScale = item.defalutScale;
            item.go.TrySetActive(false);
        }

        item.attached = false;
        List<Projectile> listItem = null;
        m_dicItemCache.TryGetValue(item.wl.ID, out listItem);
        if (listItem == null)
        {
            listItem = new List<Projectile>();
            m_dicItemCache.Add(item.wl.ID, listItem);
        }
        listItem.Add(item);
    }

    static public void Release()
    {
        for (int index = 0; index < m_listAllItem.Count; ++index)
        {
            GameObject item = m_listAllItem[index];
            GameObject.Destroy(item);
        }

        m_dicItemCache.Clear();
        m_listAllItem.Clear();
    }
}
