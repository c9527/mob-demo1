﻿using UnityEngine;
using System.Collections;

public class MonoBulletBase : MonoBehaviour {

    protected const string STATUS_FLY = "status_fly";
    protected const string STATUS_GROUNDED = "status_grounded";
    protected const string STATUS_NONE = "status_none";
    protected const string STATUS_LINERAFLIGHT = "status_lineraflight"; //直线飞行

    protected const int EXPLODE_TYPE_ALL = 0;
    protected const int EXPLODE_TYPE_BUILDING = 1;
    protected const int EXPLODE_TYPE_ROLE = 2;
    protected const int EXPLODE_TYPE_ENEMY = 3;
    protected const int EXPLODE_TYPE_FRIEND = 4;

    protected GameObject m_go;
    protected Transform m_trans;
    protected Rigidbody m_rd;
    protected Collider m_collider;
    protected Renderer m_render;
    protected PreventPenetrate m_pp; 

    protected string m_strStatus;
    protected EptFloat m_fTick;
    protected bool m_bStarted;
    protected bool m_bCollided;
    protected Collider m_lastHitCollider;
    protected bool m_isMainPlayer;
    protected BasePlayer m_player;
    
    protected BulletBase m_bullet;
    protected Effect m_tailEffect;
    protected ConfigItemBulletLine m_cb;
    protected ConfigShakeLine m_cs;
    protected Vector3 m_startPos;               //起始位置
    protected float m_fLineraflightExploreTime; //直线飞行爆炸时间
    protected Vector3 m_explorePoint;

    protected Vector3 m_playerFirePos;

    private void Awake()
    {
        if (m_go == null)
        {
            m_go = gameObject;
            m_trans = transform;
            m_render = m_go.GetComponentInChildren<Renderer>();
            m_collider = m_go.GetComponentInChildren<Collider>();
            if (m_collider == null)
            {
                Logger.Error(m_go.name + " Collider is null!!");
            }
            m_rd = m_go.AddMissingComponent<Rigidbody>();
            m_pp = m_go.AddMissingComponent<PreventPenetrate>();
             
            m_rd.mass = 1;
            m_rd.drag = 0f;
            m_rd.angularDrag = 0.4f;
            m_rd.useGravity = false;
            m_rd.isKinematic = false;
            m_rd.collisionDetectionMode = CollisionDetectionMode.Continuous;

            m_go.ApplyLayer(GameSetting.LAYER_VALUE_BULLET, null, null, true);

            ResourceManager.LoadShader(GameConst.SHADER_TEXTURE, (newShader)=>
            {
                if (newShader == null)
                    return;

                 m_go.ReplaceShader("", GameConst.SHADER_LIST_PARTICLE,  newShader, false, true);
            });

            // Collider & Physic Material
            //PhysicMaterial pm = new PhysicMaterial("weaponPhysicMat");
            //pm.bounciness = 0.1f;
            //pm.staticFriction = 0f;
            //pm.dynamicFriction = 0.2f;
            //Collider collider = GameObjectHelper.GetComponent<Collider>(go);
            //collider.material = pm;
        }
    }

    public void Init(BulletBase bullet)
    {
        m_bullet = bullet;
    }

    virtual protected void Reset()
    {
        m_fTick = 0;
        m_bStarted = false;
        m_bCollided = false;
        m_lastHitCollider = null;
        m_isMainPlayer = false;
        m_strStatus = STATUS_NONE;
    }

    virtual protected void ActiveComponent(bool value)
    {
        if (m_collider != null)
            m_collider.enabled = value;

        if (m_rd != null)
            m_rd.isKinematic = !value;

        m_pp.enabled = value;

        enabled = value;
    }

    virtual public void Fire(Vector3 startPos, Vector3 startRot, Vector3 fireDir, ConfigItemBulletLine cb, BasePlayer player,float Aspeed = 0)
    {
        Reset();
       
        ActiveComponent(true);

        m_cb = cb;
        m_player = player;
        m_isMainPlayer = player is MainPlayer;
        if (player != null)
            m_playerFirePos = player.position;

        m_bStarted = true;
        m_strStatus = STATUS_FLY;

        m_trans.SetParent(null);
        m_startPos = startPos;
        m_trans.position = startPos;
        fireDir.Normalize();

        if (startRot != Vector3.zero)
            m_trans.eulerAngles = startRot;
        else if (m_cb.SubType != "ARROW")
        {
            m_trans.eulerAngles = Vector3.zero;
            m_trans.forward = Vector3.Normalize(new Vector3(fireDir.x, 0f, fireDir.z));
            m_trans.eulerAngles = new Vector3(m_trans.eulerAngles.x, m_trans.eulerAngles.y, -90); //转-90度跟模式有关系，后面这个地方需要改成配置
        }
        else
            m_trans.forward = fireDir;

        float speed = m_cb.Speed;
        if (Aspeed != 0)
        {
            speed = Aspeed;
        }

        m_rd.velocity = Vector3.zero;
        m_rd.AddForce(fireDir * speed, ForceMode.VelocityChange);

        //Logger.Error(fireDir.ToString() + "    m_rd:" + m_rd.velocity + "    m_trans:" + m_trans.forward);


        m_pp = GameObjectHelper.GetComponent<PreventPenetrate>(gameObject);
        m_pp.Speed = speed;
        m_pp.SpeedDecay = m_cb.SpeedDecay;
        m_pp.Run();

        PlayTailEffect();

        if (m_cb.Gravity == 0 && m_cb.Resilient == 0)
        {
            Ray shootRay;
            RaycastHit hitInfo;;
            if (m_isMainPlayer || player is FPWatchPlayer)
            {
                shootRay = AutoAim.CurAimRay;
            }
            else
            {
                shootRay = new Ray(startPos, fireDir);
            }

            WorldManager.singleton.EnableRoleBodyPartCollider(true);
            if (Physics.Raycast(shootRay, out hitInfo, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER))
            {
                m_explorePoint = hitInfo.point;
                m_fLineraflightExploreTime = Vector3.Distance(startPos, m_explorePoint) / speed;
                m_strStatus = STATUS_LINERAFLIGHT;
            }
            WorldManager.singleton.EnableRoleBodyPartCollider(false);
        }
    }

    protected void PlayTailEffect()
    {
        if (string.IsNullOrEmpty(m_cb.TailEffect))
            return;

        if (m_tailEffect != null)
        {
            TimerManager.SetTimeOut(m_cb.TailEffectShowTime, () =>
            {
                if(m_tailEffect != null)
                    m_tailEffect.Play();
            });
        }
        else
        {
            EffectManager.singleton.GetEffect<Effect>(m_cb.TailEffect, (kEffect) =>
            {
                TimerManager.SetTimeOut(m_cb.TailEffectShowTime, () =>
                {
                    m_tailEffect = kEffect;
                    if (kEffect != null)
                    {
                        kEffect.Attach(m_trans);
                        kEffect.Play();
                    }
                });
            });
        }
    }

    protected void Explode(BasePlayer bp = null)
    {
        m_bStarted = false;
        Vector3 pos = m_trans.position;

        //if(m_isMainPlayer)    //   在DoDamage中判断，不要在这里
            DoDamage(bp);

        PlayExplodeEffect(pos);

        PlayExplodeSound(pos);

        PlayShake(pos);

        m_bullet.Explode();

        ActiveComponent(false);

        EffectManager.singleton.Reclaim(m_tailEffect);
        m_tailEffect = null;
    }

    protected void PlayExplodeEffect(Vector3 pos)
    {
        //播放爆炸火焰
        if (string.IsNullOrEmpty(m_cb.ExplodeEffect) == false)
        {
            EffectManager.singleton.GetEffect<EffectTimeout>(m_cb.ExplodeEffect, (effect) =>
            {
                effect.Attach(pos);
                effect.SetTimeOut(-1);
                effect.Play();
            });
        }

        if (m_strStatus == STATUS_GROUNDED && m_lastHitCollider != null)
        {
            int matType = SceneManager.GetMaterialType(m_lastHitCollider.gameObject);
            ConfigMaterialLine cml = ConfigManager.GetConfig<ConfigMaterial>().GetLine(matType);
            if (cml != null)
            {
                //播放地表特效
                if (string.IsNullOrEmpty(cml.ExpEffect) == false)
                {
                    EffectManager.singleton.GetEffect<EffectTimeout>(cml.ExpEffect, (effect) =>
                    {
                        effect.Attach(pos);
                        effect.SetTimeOut(cml.ExpEffectTime > 0 ? cml.ExpEffectTime : -1);
                        effect.Play();
                    });
                }
            }

            m_lastHitCollider = null;
        }
    }

    protected void PlayExplodeSound(Vector3 pos)
    {
        if (m_cb.Explodesound.Length == 2)
        {
            float radius = 10f;
            float.TryParse(m_cb.Explodesound[0], out radius);

            AudioManager.PlayHitSound(pos, m_cb.Explodesound[1], 100, 1);
        }
    }

    protected void PlayShake(Vector3 pos)
    {
        if (m_cb.ShakeID != 0 && MainPlayer.singleton != null && MainPlayer.singleton.isAlive)
        {
            if(m_cs == null)
                m_cs = ConfigManager.GetConfig<ConfigShake>().GetLine(m_cb.ShakeID);

            float dis = Vector3.Distance(MainPlayer.singleton.position, pos);
            if (dis <= m_cs.IncidenceRadius)
            {
                if (SceneCamera.singleton != null)
                    SceneCamera.singleton.StartShake(m_cs);
            }
        }
    }

    virtual protected void DoDamage(BasePlayer bp=null)
    {

    }

    protected bool CheckExplode(Collision collision)
    {
        bool canExplode = true;

        if (m_pp.enabled && m_cb.SafeDistance > 0)
            canExplode = false;
        else if (m_cb.Resilient != 0 && m_strStatus != STATUS_LINERAFLIGHT && collision.gameObject != m_player.gameObject)
        {
            canExplode = false;
            switch(m_cb.ExplodeType)
            {
                //case EXPLODE_TYPE_ALL:
                //    canExplode = false;
                //    break;
                case EXPLODE_TYPE_BUILDING:
                    canExplode = collision.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER);
                    break;
                case EXPLODE_TYPE_ROLE:
                    canExplode = collision.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND);
                    break;
                case EXPLODE_TYPE_ENEMY:
                    canExplode = collision.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY);
                    break;
                case EXPLODE_TYPE_FRIEND:
                    canExplode = collision.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND);
                    break;
            }
        }
        return canExplode;
    }

    protected Vector3 position
    {
        get { return m_trans.position; }
    }

    protected bool isInSight
    {
        get {
            return m_render.isVisible;
        }
    }

    protected bool isPhysiclyVisible
    {
        get
        {
            if (MainPlayer.singleton == null) 
                return false;

            RaycastHit rayHit;
            Vector3 dir = MainPlayer.singleton.headPos - position;
            return !Physics.Raycast(position, dir, out rayHit, dir.magnitude, GameSetting.LAYER_MASK_BUILDING);
        }
    }
    

#region Mono Function
    private void OnCollisionEnter(Collision collision)
    {
        m_bCollided = true;
        m_lastHitCollider = collision.collider;
        //Logger.Warning(m_lastHitCollider.gameObject.name);
        var bp = collision.gameObject.GetComponent<OtherPlayerController>();
        //不反弹爆炸
        if (CheckExplode(collision))
        {
            if (bp != null)
            {
                Explode(bp.otherplayer);
            }
            else
            {
                Explode();
            }
        }
    }

    virtual protected void FixedUpdate()
    {
        if (m_bStarted == false)
            return;

        if (m_pp.enabled && m_cb.Resilient == 0 && Vector3.Distance(m_startPos, new Vector3(m_trans.position.x, m_startPos.y ,m_trans.position.z)) > m_cb.SafeDistance)
        {
            m_pp.enabled = false;
        }
        if (m_cb.Gravity != 0)
        {
            m_rd.AddForce(new Vector3(0, -m_cb.Gravity, 0), ForceMode.Force);
        }
    }

    private void Update()
    {
        if (m_bStarted == false)
            return;

        m_fTick += Time.deltaTime;

        if (m_strStatus == STATUS_FLY)
        {
            if (m_fTick < m_cb.FlyTime && m_bCollided)
            {
                m_fTick = 0;
                m_strStatus = STATUS_GROUNDED;
            }
            else if (m_fTick >= m_cb.FlyTime)
            {
                Explode();
            }
        }
        else if (m_strStatus == STATUS_GROUNDED)
        {
            if (m_fTick >= m_cb.GroundTime)
            {
                Explode();
            }
        }
        else if(m_strStatus == STATUS_LINERAFLIGHT)
        {
            if (m_fTick >= m_fLineraflightExploreTime)
            {
                m_trans.position = m_explorePoint;
                Explode();
            }
        }
    }

#endregion
  
	 
}
