﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class MonoBulletGrenade : MonoBulletBase {


    override protected void DoDamage(BasePlayer bp = null)
    {
        if (m_isMainPlayer == false)    // 只有主角扔的，才告诉服务器我炸了哪些人，如果是第三人称扔的，就不处理，服务器会单独通知伤害
            return;

        List<p_hitg_info_toc> list = new List<p_hitg_info_toc>();
        Dictionary<int, BasePlayer> dicAllPlayer = WorldManager.singleton.allPlayer;

        Vector3 vec3ExplodePos = gameObject.transform.position;
        foreach (KeyValuePair<int, BasePlayer> kvp in dicAllPlayer)
        {
            BasePlayer player = kvp.Value;

            if (WeaponBase.CanAttack(player) == false)
                continue;

            float distance = (player.position - vec3ExplodePos).sqrMagnitude;
            if (distance <= m_cb.DamageMaxR * m_cb.DamageMaxR &&
                distance >= m_cb.DamageMinR * m_cb.DamageMinR)
            {
                p_hitg_info_toc p = new p_hitg_info_toc
                {
                    id = player.PlayerId,
                    pos = player.position
                };
                list.Add(p);
            }
        }

        NetLayer.Send(new tos_fight_fireg()
        {
            fromPos = vec3ExplodePos,
            hitList = list.ToArray(),
            index = m_bullet.bulletIndex,
            firePos = m_playerFirePos
        });

        
    }
   
}
