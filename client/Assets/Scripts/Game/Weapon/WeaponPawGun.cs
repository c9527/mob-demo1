﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WeaponPawGun : WeaponGun
{
    public override void LoadRes(System.Action<WeaponBase, bool> funLoaded, string emptyGOName = null)
    {
        base.LoadRes(funLoaded, "PawGun");
    }

    //public override void LoadRes(System.Action<WeaponBase, bool> funLoaded = null)
    //{
    //    if (gameObject == null)
    //        SetGO(new GameObject("PawGun"));

    //    if (funLoaded != null)
    //        funLoaded(this, true);
    //}

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        //m_transFirePoint = GameObjectHelper.FindChildByTraverse(m_kPlayer.transform, ModelConst.PLAYER_SLOT_RIGHT_HAND);
        m_transFirePoint = m_kPlayer.rightHandSlot;
        if (m_transFirePoint == null)
            Logger.Error("找不到枪口火花挂接点!");

        //m_transCartridge = GameObjectHelper.FindChildByTraverse(m_kPlayer.transform, ModelConst.PLAYER_SLOT_RIGHT_HAND);
        m_transCartridge = m_kPlayer.leftHandSlot;
        if (m_transCartridge == null)
            Logger.Error("找不到弹壳挂接点!");
    }


    #region Fire

    protected override void TryToFire()
    {
        base.TryToFire();

        string strRet = TestWeapon();

        if (strRet != WEAPON_TEST_SUCCESS)
        {
            StopFire();
        }

        switch (strRet)
        {
            case WEAPON_TEST_SUCCESS:
                DoFireReally();
                break;
            case WEAPON_TEST_NO_AMMO:
                PlayFireEmptySound();
                break;
            case WEAPON_TEST_NEED_RELOAD:
                ReloadAmmo();
                break;
        }
    }

    // 最后一颗子弹打完后，要自动装弹

    protected override void DoFireReally()
    {
        m_kPlayer.status.fire = true;

        m_aimRay = AutoAim.CurAimRay;

        PlayerAniBaseController kPA = m_kPlayer.playerAniController;
        string fireAni = GetMPWeaponFireAni(m_fireType);

        StopWeaponAni();

        if (m_fireType == WEAPON_FIRE_TYPE_ONESHOT)
        {
            kPA.Play(fireAni, 0, 0);
        }
        else if (m_fireType == WEAPON_FIRE_TYPE_STRAFE)
        {
            if (kPA.IsCurrentAni(fireAni) == false)
            {
                kPA.CrossFade(fireAni, 0.2f);
            }

            kPA.SetBool(AniConst.TriggerStrafeFire, true);
            m_kPlayer.cameraShaker.apply = true;
        }

        if (isMainPlayer)
        {
            NetLayer.Send(new tos_fight_atk()
            {
                atkType = atkType,
                index = -1,
            });
        }

        m_iFireSkillTimerId = TimerManager.SetTimeOut(m_kWL.FireDelay, () =>
        {
            m_iFireSkillTimerId = -1;
            if (m_kPlayer.curWeapon == this)
                DoWeaponFireLogic();
        });
    }

    virtual protected void DoWeaponFireLogic()
    {
        int shotTime = 1;
        float burstCD = 0;
        if (m_kWL.FireMode == FIRE_MODE_BURST)
        {
            int clipAmmoNum = isSubWeapon ? m_kLWD.subAmmoNum : m_kLWD.curClipAmmoNum;

            shotTime = clipAmmoNum >= FIRE_COST_AMMO_NUM_BURST
                ? FIRE_COST_AMMO_NUM_BURST
                : clipAmmoNum;

            if (m_kWL.BurstRate <= 0)
                Logger.Warning("BurstRate <= 0 " + m_kWL.ID);
            else
                burstCD = 60f / m_kWL.BurstRate;
        }

        if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
        {
            m_kSP.BeginGunFireSound(new[] { m_kWL.FireSound[1] }, m_kWL.FireSoundProbability, m_kWL.FireSoundSpawn[0],
                m_kWL.FireSoundSpawn[1], AudioManager.FightSoundVolume);
        }

        string gunFireAni = "";
        if (m_kWL.FireAni != null && m_kWL.FireAni.Length != 0)
        {
            if (m_kWL.FireAni.Length == 1)
                gunFireAni = m_kWL.FireAni[0];
            else if (m_kWL.FireAni.Length == 2)
                gunFireAni = m_fireType == WEAPON_FIRE_TYPE_ONESHOT ? m_kWL.FireAni[0] : m_kWL.FireAni[1];
        }

        if (m_kAni.GetClip(gunFireAni) != null)
        {
            m_kAni.Stop();
            m_kAni.Play(gunFireAni);
        }

        WorldManager.singleton.EnableRoleBodyPartCollider(true);

        for (int i = 0; i < shotTime; i++)
        {
            float delayTime = i * burstCD;
            TimerManager.SetTimeOut(delayTime, (arg) =>
            {
                if (m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                    return;

                int index = (int)arg;

                if (GameSetting.enableGunFire)
                {
                    if (m_kEffectGunFire != null)
                        m_kEffectGunFire.Play();
                    if (m_kEffectGunFire2 != null)
                        m_kEffectGunFire2.Play();

                    AttachAndPlayCartridgeEffect();
                }

                ReduceAmmoNum();

                int pelletNum = m_kWL.Pellets <= 0 ? 1 : m_kWL.Pellets;
                int bulletCnt = protoBulletCnt;
                bool isHitEnemy;
                bool hitOneEnemy = false;

                if (m_kSpread != null)
                {
                    tos_fight_fire fireArg = DoMainPlayerRayTest(m_kSpread.ApplySpread(), bulletCnt, true, out hitOneEnemy);
                    List<p_hit_info[]> list = new List<p_hit_info[]>();
                    list.AddRange(fireArg.hitList);
                    for (int j = 1; j < pelletNum; j++)
                    {
                        tos_fight_fire hitData = DoMainPlayerRayTest(m_kSpread.ApplySpread(), bulletCnt, !hitOneEnemy, out isHitEnemy);
                        if (!hitOneEnemy && isHitEnemy)
                            hitOneEnemy = true;
                        list.AddRange(hitData.hitList);
                    }
                }

                if (!hitOneEnemy)
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_WRONG_ATK);
                if (m_isSubWeapon)
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subAmmoNum);
                else
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, ammoNumInClip, ammoNumLeft, advNum);    // 在发送完协议后，再让界面更新，界面更新会设置useAdv=false

            }, i, false);
        }

        WorldManager.singleton.EnableRoleBodyPartCollider(false);



        if (m_fireCallBack != null)
        {
            System.Action callback = () =>
            {
                m_fireCallBack();
                m_fireCallBack = null;
                m_iFireSkillTimerId = -1;
            };

            if (m_kWL.FireAfterDelay > 0)
                m_iFireSkillTimerId = TimerManager.SetTimeOut(m_kWL.FireAfterDelay, callback);
            else
                callback();
        }
    }

    virtual internal tos_fight_fire DoMainPlayerRayTest(float spread, int bulletCnt, bool isPlayHit, out bool isHitEnemy)
    {
        isHitEnemy = false;

        // 子弹可穿透性的武器，需要检测多个碰撞对象，这些对象按射线长度排序后，可以看出受击顺序。
        int iLayerMaskBuildingEnemy = GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_DMM_PLAYER;

        if (isZoomOrOut)
            spread *= m_kPlayer.serverData.SpreadSightsFactor(atkType);
        //计算弹道的散布角度
        Ray shootRay = m_aimRay;
        var spreadRandom = Random.Range(spread * m_kWL.StandSpreadMinFactor, spread);
        var spreadYaw = spreadRandom * m_kWL.SpreadYawFactor;
        var dir = Quaternion.AngleAxis(spreadYaw, Vector3.up) * Vector3.forward;
        dir = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward) * dir;
        if (spreadYaw == 0)
            dir.y = 0;
        else
            dir.y *= m_kWL.SpreadPitchFactor / m_kWL.SpreadYawFactor;
        shootRay.direction = Quaternion.LookRotation(m_aimRay.direction) * dir;

        RaycastHit[] arrRH = Physics.RaycastAll(shootRay, GameSetting.FIRE_RAYTEST_LENGTH, iLayerMaskBuildingEnemy);
        List<RaycastHit> listRH = new List<RaycastHit>(arrRH);
        listRH.Sort(delegate(RaycastHit kRH1, RaycastHit kRH2)
        {
            return kRH1.distance - kRH2.distance >= 0 ? 1 : -1;
        });

        List<p_hit_info> listFightHitData = new List<p_hit_info>();

        bool isPlayHit2 = true;
        int cnt = listRH.Count;
        if (weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SHOT_GUN)
            cnt = cnt > SHOTGUN_MAX_HIT_NUM ? SHOTGUN_MAX_HIT_NUM : cnt;      // 限制最大数据，否则消息会很大
        else
            cnt = cnt > GUN_MAX_HIT_NUM ? GUN_MAX_HIT_NUM : cnt;      // 限制最大数据，否则消息会很大

        RaycastHit rh;
        List<BasePlayer> listHitPlayer = new List<BasePlayer>();
        bool buildingEffectPlayed = false;
        for (int i = 0; i < cnt; i++)
        {
            rh = listRH[i];
            GameObject goCollider = rh.collider.gameObject;
            Transform transCollider = goCollider.transform;

            if (i == 0 && buildingEffectPlayed == false && goCollider.layer == GameSetting.LAYER_VALUE_BUILDING)
            {
                int matType = SceneManager.GetMaterialType(goCollider);
                if (matType != 0)
                {
                    buildingEffectPlayed = true;
                    PlayHitBuildingEffect(rh.point, rh.normal, matType, Effect.MAT_EFFECT_GUN);
                }
            }

            p_hit_info hitData = null;

            if (goCollider.layer == GameSetting.LAYER_VALUE_BUILDING)
            {
                int mt = SceneManager.GetMaterialType(goCollider);
                if (mt != 0)
                    hitData = new p_hit_info() { matType = mt };
            }
            else if (goCollider.layer == GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY || goCollider.layer == GameSetting.LAYER_VALUE_DMM_PLAYER)
            {
                GameObject goTarget = null;
                Transform transRoot = transCollider.root;   // 玩家GameObject一定要放在场景根结点下
                if (transRoot != null)
                    goTarget = transRoot.gameObject;
                else
                    goTarget = goCollider;

                BasePlayer kBP = WorldManager.singleton.GetPlayerWithGOHash(goTarget.GetHashCode());

                if (kBP == null || listHitPlayer.Contains(kBP) || !CanAttack(kBP))  // 玩家有多个碰撞体，射线穿过会有多个碰撞点,这里要排除玩家重复受击
                    continue;

                //外挂检测
                if (AntiCheatManager.CheatCollider(kBP, rh.collider, rh.point))
                    continue;

                listHitPlayer.Add(kBP);

                hitData = new p_hit_info()
                {
                    matType = SceneManager.GetMaterialType(goCollider),
                    bodyPart = BasePlayer.GetBodyPartID(goCollider.name),
                    actorId = kBP.serverData.id,
                    pos = kBP.beShotPos
                };

                if (i == 0 && isPlayHit && isPlayHit2)
                {
                    PlayHitPlayerEffect(kBP, rh.point, rh.normal, hitData.bodyPart, Effect.MAT_EFFECT_GUN);
                    isPlayHit2 = false;
                }

                //命中玩家时，在自己身上也播放命中音效，增强玩家的命中反馈
                PlayerHitSound(m_kPlayer.position, hitData.bodyPart, Effect.MAT_EFFECT_GUN);

                isHitEnemy = true;
            }

            if (hitData != null)
                listFightHitData.Add(hitData);
        }

        // 第一人称先不播放火线特效
        //if (listRH.Count > 0)
        //{
        //    // TODO：每隔几次播放一次
        //    RaycastHit kRH1 = listRH[0];
        //    PlayFireLineEffect(kRH1.point);
        //}

        return new tos_fight_fire()
        {
            hitPos = SceneCamera.singleton.position + shootRay.direction * GameSetting.GUN_RAYHITPOS_DISTANCE,
            hitList = new p_hit_info[][] { listFightHitData.ToArray() },
            bulletCount = bulletCount,
            weaponVer = m_kPlayer.serverData.weaponVer,
            isZoom = isZoomOrOut,
            relyBulletCount = relyBulletCount,
            atkType = WeaponAtkTypeUtil.GetWeaponAtkType(m_isSubWeapon ? m_kPlayer.curWeapon.weaponConfigLine.SubType : m_kWL.SubType, MainPlayer.singleton.useAdvAmmo, m_isSubWeapon),
            firePos = m_kPlayer.position,
            cameraRot = new short[] { (short)(50 * SceneCamera.singleton.localEulerAngles.x), (short)(50 * SceneCamera.singleton.localEulerAngles.y) }
        };
    }

    internal override void Fire2(toc_fight_fire arg)
    {
        if (gameObject == null || m_kPlayer.alive == false)
            return;

        if (m_kPlayer is OtherPlayer)
        {
            bool notPlayAni = (arg.notPlayAni & 0x01) == 1;
            int fireIndex = (arg.notPlayAni >> 1) & 0x06;
            if (!notPlayAni)
            {
                if (m_bIsWalkWaggle && m_kPlayer.playerAniController.animator != null && m_kPlayer.playerAniController.animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                    m_kPlayer.playerAniController.EnableWalkWaggleLayer(false);
                fireIndex = (fireIndex < m_kWL.OPFireAni.Length) ? fireIndex : 0;
                m_kPlayer.playerAniController.Play(m_kWL.OPFireAni[fireIndex]);
                if (GameSetting.enableGunFire && m_kPlayer.isRendererVisible)
                {
                    if (isBothHandSlot == false || fireIndex % 2 == 0)
                    {
                        if (m_kEffectGunFire != null)
                            m_kEffectGunFire.Play();
                    }
                    else
                    {
                        if (m_kEffectGunFire2 != null)
                            m_kEffectGunFire2.Play();
                    }
                }

                if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2 && GameSetting.enableSound)
                {
                    m_kSP.PlayOneShot(m_kWL.FireSound[1], AudioManager.FightSoundVolume);
                }
            }
            PlayOPFireLineEffect(arg.hitPos);
        }
        else
        {
            m_kPlayer.status.fire = true;
            m_kPlayer.status.reload = false;

            m_aimRay = AutoAim.CurAimRay;
            PlayerAniBaseController kPA = m_kPlayer.playerAniController;

            //string fireType = GetMPWeaponFireType();
            int fireType = WEAPON_FIRE_TYPE_ONESHOT;
            string fireAni = GetMPWeaponFireAni(fireType);

            StopWeaponAni();

            //if (fireType == WEAPON_FIRE_TYPE_ONESHOT)
            //{
            kPA.Play(fireAni, 0, 0);
            //}
            //else if (fireType == WEAPON_FIRE_TYPE_STRAFE)
            //{
            //    if (kPA.IsCurrentAni(fireAni) == false)
            //    {
            //        kPA.CrossFade(fireAni, 0.2f);
            //    }
            //
            //    kPA.SetBool(AniConst.TriggerStrafeFire, true);
            //    m_kPlayer.cameraShaker.apply = true;
            //}

            if (m_kWL.FireSound != null && m_kWL.FireSound.Length >= 2)
            {
                m_kSP.BeginGunFireSound(new[] { m_kWL.FireSound[1] }, m_kWL.FireSoundProbability, m_kWL.FireSoundSpawn[0],
                    m_kWL.FireSoundSpawn[1], AudioManager.FightSoundVolume);
            }

            string gunFireAni = "";
            if (m_kWL.FireAni != null)
            {
                if (m_kWL.FireAni.Length == 1)
                    gunFireAni = m_kWL.FireAni[0];
                else if (m_kWL.FireAni.Length == 2)
                    gunFireAni = m_fireType == WEAPON_FIRE_TYPE_ONESHOT ? m_kWL.FireAni[0] : m_kWL.FireAni[1];
            }

            if (m_kAni.GetClip(gunFireAni) != null)
            {
                m_kAni.Stop();
                m_kAni.Play(gunFireAni);
            }

            float burstCD = 0;
            if (m_kWL.FireMode == FIRE_MODE_BURST)
            {
                if (m_kWL.BurstRate <= 0)
                    Logger.Warning("BurstRate <= 0 " + m_kWL.ID);
                else
                    burstCD = 60f / m_kWL.BurstRate;
            }

            TimerManager.SetTimeOut(burstCD, () =>
            {
                if (m_kPlayer.serverData.alive == false || this != m_kPlayer.curWeapon)
                    return;

                if (GameSetting.enableGunFire)
                {
                    if (m_kEffectGunFire != null)
                        m_kEffectGunFire.Play();
                    if (m_kEffectGunFire2 != null)
                        m_kEffectGunFire2.Play();

                    AttachAndPlayCartridgeEffect();
                }
            });
        }

        DoOtherPlayerRayTest(firePoint, Vector3.Normalize(arg.hitPos - firePoint), Effect.MAT_EFFECT_GUN);
    }

    protected override void DoOtherPlayerRayTest(Vector3 rayPos, Vector3 rayDir, string matEffectType)
    {
        m_hitBuildingTick++;

        if (isFPWatchPlayer || m_hitBuildingTick >= GlobalConfig.gameSetting.OPHitBuildingInterval)
        {
            m_hitBuildingTick = 0;

            //if (GameSetting.enableGunRaytest == false)
            //{
            //    PlayHitBuildingEffect(m_kPlayer.position, m_kPlayer.transform.forward, 1, matEffectType);
            //    return;
            //}
            RaycastHit kRH;
            if (Physics.Raycast(rayPos, rayDir, out kRH, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_BUILDING))
            {
                if (isFPWatchPlayer || WorldManager.singleton.isViewBattleModel || (MainPlayer.singleton != null && (!MainPlayer.singleton.isAlive || Vector3.SqrMagnitude(kRH.point - MainPlayer.singleton.position) <= Mathf.Pow(GlobalConfig.gameSetting.HitBuildingEffectDis, 2))))
                {
                    int matType = SceneManager.GetMaterialType(kRH.collider.gameObject);
                    PlayHitBuildingEffect(kRH.point, kRH.normal, matType, matEffectType, 0, rayDir, rayPos);
                }
            }
        }
    }

    #endregion


}