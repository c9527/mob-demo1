﻿using UnityEngine;

public class WeaponSniper : WeaponGun
{
    protected bool m_bFireBeforeZooming;

    public override void Release()
    {
        base.Release();

        m_bFireBeforeZooming = false;
    }

    override public void OnFireKeyDown()
    {


#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (!canFire)
            return;
        if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
            return;
        m_bIsFireBefore = true;
        m_bFirePressed = true;
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon)
            return;

        if (m_kGunScope.isZoomIn)
        {
            m_bFireBeforeZooming = true;
        }
        else
        {
            m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
            if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
            {
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
                if (m_kGunScope.isZoom && !hasScopeButton)
                    ZoomOut();
            }
        }

#else
        if (!canFire)
            return;

        if (Time.timeSinceLevelLoad - lastFireTime < fireRate)
            return;

        if (m_scopeButtonPressed)
        {
            if (m_kGunScope.isZoom)
            {
                m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                TryToFire();
            }
        }
        else
        {
            if (m_kGunScope.isIdle == true)
            {
                m_bFirePressed = true;
                m_fFireKeyPressTime = Time.timeSinceLevelLoad;
            }
        }
#endif
    }

    override public void OnFireKeyUp()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_bFirePressed = false;
        m_bIsFireBefore = false;
        PlayerStatus status = m_kPlayer.status;
        if (status.fire == true)
        {
            status.fire = false;
            StopFire();
        }
#else
        if (m_bFirePressed == false)
            return;

        m_bFirePressed = false;

        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon)
            return;

        if (m_scopeButtonPressed == false)
        {
            float pressTime = Time.timeSinceLevelLoad - m_fFireKeyPressTime;
            if (pressTime < FIRE_ONE_SHOT_INVERTAL_TIME)
            {
                if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
                {
                    m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                    TryToFire();
                }
            }
            else
            {
                if (m_kGunScope.isZoomIn)
                {
                    m_bFireBeforeZooming = true;
                }
                else if (m_kGunScope.isZoom)
                {
                    m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
                    TryToFire();

                    OnZoomOut();
                }
            }
        }
#endif
    }

    override public void Update()
    {
        base.Update();
        //开枪结束后  自动开镜
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (m_scopeButtonPressed && !m_bFirePressed)
        {
            if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
            {
                if (!m_kGunScope.isZoomIn)
                ZoomIn();
            }
        }
#endif
    }

    override protected void OnFireUpdate()
    {
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon)
            return;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
        {
            //狙击枪开完一枪后 继续触发开枪行为
            OnFireKeyDown();
        }
#else
        if (Time.timeSinceLevelLoad - m_fFireKeyPressTime >= FIRE_ONE_SHOT_INVERTAL_TIME)
        {
            ZoomIn();
        }
#endif
    }

    public override void OnZoomIn()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        PlayerStatus status = m_kPlayer.status;
        if (status.reload || status.switchWeapon)
            return;
        if (Time.timeSinceLevelLoad - lastFireTime >= fireRate)
        {
            m_fFireKeyPressTime = Time.timeSinceLevelLoad;
            base.OnZoomIn();
        }
#else
        base.OnZoomIn();
#endif
    }

    public override void OnCancleFire()
    {
        m_bFirePressed = false;
        ZoomOutImmediate();
    }

    override protected void ZoomIn()
    {
        if (m_kGunScope.isIdle == false)
            return;

        base.ZoomIn();
    }

    /// <summary>
    /// pc版的狙击枪 内部关镜处理 不设置 m_scopeButtonPressed  且m_scopeButtonPressed的值在OnZoomIn 和OnZoonOut里面处理
    /// </summary>
    override protected void ZoomOut()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (m_kGunScope != null)
        {
            if (isMainPlayer)
            {
                NetLayer.Send(new tos_fight_weapon_motion()
                {
                    motion = WeaponConst.Weapon_Motion_Scope_ZoomOut
                });
            }
            m_kGunScope.ZoomOut();
        }
#else
        base.ZoomOut();
#endif
        //m_bFirePressed = false;
        if (isMainPlayer)
        {
            MainPlayer.singleton.EnableMainPlayerCamera(true);
        }
        else if (isFPWatchPlayer)
        {
            FPWatchPlayer.singleton.EnableMainPlayerCamera(true);
        }
    }

    public override void ZoomOutImmediate()
    {
        base.ZoomOutImmediate();
        if (isMainPlayer)
        {
            MainPlayer.singleton.EnableMainPlayerCamera(true);
            GameDispatcher.Dispatch(GameEvent.UI_SHOW_CANCLE_FIRE_BTN, false);
        }
        else if (isFPWatchPlayer)
        {
            FPWatchPlayer.singleton.EnableMainPlayerCamera(true);
        }
    }

    override protected void ZoomInComplete()
    {
        base.ZoomInComplete();

        if (m_bFireBeforeZooming == true && Time.timeSinceLevelLoad - lastFireTime >= fireRate)
        {
            m_bFireBeforeZooming = false;
            m_fireType = WEAPON_FIRE_TYPE_ONESHOT;
            OnZoomOut();
            TryToFire();
        }
        else
        {
            if (isMainPlayer)
            {
                MainPlayer.singleton.EnableMainPlayerCamera(false);
                GameDispatcher.Dispatch(GameEvent.UI_SHOW_CANCLE_FIRE_BTN, true);
            }
            else if (isFPWatchPlayer)
            {
                FPWatchPlayer.singleton.EnableMainPlayerCamera(false);
            }
        }
    }

    public override void WeaponSwitchOff()
    {
        base.WeaponSwitchOff();
        if (isMainPlayer)
        {
            MainPlayer.singleton.EnableMainPlayerCamera(true);
        }
        else if (isFPWatchPlayer)
        {
            FPWatchPlayer.singleton.EnableMainPlayerCamera(true);
        }
    }

    public override string TestWeapon()
    {
        string strRet = WEAPON_TEST_UNKNOW;

        if (isSubWeapon)
        {
            if (m_kLWD.subAmmoNum > 0)
                strRet = WEAPON_TEST_SUCCESS;
            else
                strRet = WEAPON_TEST_NO_AMMO;
        }
        else if (m_kLWD.curClipAmmoNum > 0)
        {
            strRet = WEAPON_TEST_SUCCESS;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum > 0)
        {
            strRet = WEAPON_TEST_NEED_RELOAD;
        }
        else if (m_kLWD.curClipAmmoNum <= 0 && m_kLWD.extraAmmoNum <= 0)
        {
            strRet = WEAPON_TEST_NO_AMMO;
        }

        return strRet;
    }

}
