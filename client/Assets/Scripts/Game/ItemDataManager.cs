﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

/// <summary>
/// 更新维护仓库和背包中的数据
/// </summary>
public static class ItemDataManager
{
    public static int UPDATE_REASON_LOTTERY = 1010;//军火库抽奖
    public static int UPDATE_REASON_NEWLOTTERY = 1011;//新军火库抽奖
    public static int UPDATE_REASON_EQUIP_BOX = 1001;//武器箱子
    public static List<p_item> renewlist = new List<p_item>();
    public static List<p_item> hasrenewedlist = new List<p_item>();
    public static p_item[] m_items = new p_item[0];
    public static p_bag[] m_bags = new p_bag[0];
    private static Dictionary<int, ConfigItemWeaponLine> m_materialToWeaponDic; //材料碎片到武器的映射字典
    public static KeyValue<int, List<ItemInfo>> item_added_temp_record = new KeyValue<int, List<ItemInfo>>() { key = 0, value = new List<ItemInfo>() };
    public const int MAX_DURABILITY = 200;  //武器最大耐久值
    public static ConfigMall cfm = ConfigManager.GetConfig<ConfigMall>();
    /// <summary>
    /// 物品兑换记录表,Key=to,Value=from
    /// </summary>
    public static KeyValuePair<ItemInfo, ItemInfo> itemConvertPair = new KeyValuePair<ItemInfo, ItemInfo>();
    public static List<p_phased_purchase_item> phasedItemList = new List<p_phased_purchase_item>();//分期付款的道具数据
    public static int bagselect = 0;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="reason">ItemDataManager.UPDATE_REASON_???</param>
    public static void ResetTempAddedRecord(int reason = 0)
    {
        item_added_temp_record.key = reason;
        item_added_temp_record.value.Clear();
        //Logger.Log("Clear");
    }

    /// <summary>
    /// 更新仓库或全部背包的数据
    /// </summary>
    /// <param name="depot"></param>
    /// <param name="bags"></param>
    public static void UpdateItemData(p_item[] depot = null, p_bag[] bags = null)
    {
        if (depot != null)
        {
            FakeItem.EVENTCOIN_002.item_cnt = PlayerSystem.roleData.eventcoin_002;
            m_items = depot.ArrayConcat(FakeItem.EVENTCOIN_002);
        }
        if (bags != null)
            m_bags = bags;
        for (int i = 0; i < m_items.Length; i++)
        {
            ChangeOverdueItem(m_items[i]);
        }
    }


    /// <summary>
    /// 更新单个背包数据
    /// </summary>
    /// <param name="bag"></param>
    public static void UpdateItemData(p_bag bag)
    {
        if (bag == null)
            return;
        for (int i = 0; i < m_bags.Length; i++)
        {
            if (m_bags[i].group_id == bag.group_id)
            {
                m_bags[i] = bag;
                return;
            }
        }

        m_bags = m_bags.ArrayConcat(bag);
    }

    /// <summary>
    /// 更新单个道具
    /// </summary>
    /// <param name="item"></param>
    public static void UpdateItemData(p_item item, int reason = 0)
    {
        if (item == null)
            return;
        p_item old_data = null;
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].uid == item.uid)
            {
                old_data = m_items[i];
                m_items[i] = item;
                break;
            }
        }
        if (old_data == null)
        {
            m_items = m_items.ArrayConcat(item);
            var config = ItemDataManager.GetItem(item.item_id);
            var config2 = config as ConfigItemOtherLine;
            if (config2 != null && config2.AutoUseGift == 1 && (config2.UseType == "use_giftbag" || config2.UseType == "animal_weapons"))
            {
                for (int i = 0; i < item.item_cnt; i++)
                {
                    TimerManager.SetTimeOut(0.5f, () => { PanelRewardItemTipsWithEffect.AutoUseGiftBag(item.item_id); });
                }
            }
        }
        else if (item.item_cnt > old_data.item_cnt || (item.time_limit > old_data.time_limit))
        {
            var config = ItemDataManager.GetItem(item.item_id);
            var config2 = config as ConfigItemOtherLine;
            if (config2 != null && config2.AutoUseGift == 1 && (config2.UseType == "use_giftbag" || config2.UseType == "animal_weapons"))
            {
                for (int i = 0; i < (item.item_cnt - old_data.item_cnt); i++)
                {
                    TimerManager.SetTimeOut(0.5f, () => { PanelRewardItemTipsWithEffect.AutoUseGiftBag(item.item_id); });
                }
            }
        }
        if (reason > 0 && item_added_temp_record.key == reason)
        {//新增物品记录（个数增加或者时效增加）
            if (old_data == null)
            {
                item_added_temp_record.value.Add(new ItemInfo() { ID = item.item_id, cnt = item.item_cnt, day = item.time_limit == 0 ? 0 : (int)Mathf.Ceil((item.time_limit - item.create_time) / (3600f * 24)) });
            }
            else if (item.item_cnt > old_data.item_cnt//数量变化 
                || (old_data.time_limit != 0 && item.time_limit > old_data.time_limit)//时效变化
                || item.time_limit == 0//变成永久
                )
            {
                item_added_temp_record.value.Add(new ItemInfo() { ID = item.item_id, cnt = item.item_cnt - old_data.item_cnt, day = item.time_limit == 0 ? 0 : (int)Mathf.Ceil((item.time_limit - old_data.create_time) / (3600f * 24)) });
            }
        }
        ChangeOverdueItem(item);
        //物品特殊逻辑处理
        var titem = ItemDataManager.GetItem(item.item_id);
        if ((titem.Type == GameConst.ITEM_TYPE_EQUIP
            && titem.SubType != GameConst.WEAPON_SUBTYPE_BULLET
            && titem.SubType != GameConst.WEAPON_SUBTYPE_ACCESSORY) || ItemIsHeroCard(titem as ConfigItemLine))
        {
            //英雄卡特殊处理
            if (old_data == null || (ItemIsHeroCard(titem as ConfigItemLine) && item.item_cnt!=0))//新增的物品
            {
                if (UIManager.IsOpen<PanelLottery>())
                {
                    if (PlayerPrefs.HasKey("PanelLottery_PanelChooseGroup_" + PlayerSystem.roleId))
                        return;

                    PlayerPrefs.SetInt("PanelLottery_PanelChooseGroup_" + PlayerSystem.roleId, 1);
                    PlayerPrefs.Save();
                    //TimerManager.SetTimeOut(1f, () => PanelChooseGroup.DoShow(new object[] { item.uid }, true));
                }
                if (UIManager.IsOpen<PanelRoleSelect>() || UIManager.IsOpen<PanelEvent>() ||
                    UIManager.IsOpen<PanelTaskNewHand>() || UIManager.IsOpen<PanelLottery>() || UIManager.IsOpen<PanelRechargeMember>() ||
                    PanelSettle.isShowSerttle || UIManager.IsOpen<PanelLotteryNew>()||UIManager.IsOpen<PanelSevenDayReward>()||UIManager.IsOpen<PanelSubWelfare>()||
                    UIManager.IsOpen<PanelCorpsTask>())
                    return;
                if (UIManager.IsOpen<PanelDepot>() && reason != 1032)
                {//如果是合成的武器，不会做过滤，会弹出获得物品
                    return;
                }
                //PanelChooseGroup.DoShow(new object[] { item.uid }, true);                           
                ItemInfo iteminfo = new ItemInfo();
                iteminfo.cnt = item.item_cnt;
                iteminfo.ID = item.item_id;
                iteminfo.day = ItemInfo.TimeLimitToTime(item.time_limit);
                PanelRewardItemTipsWithEffect.DoShow(new ItemInfo[] { iteminfo }, true);
            }
        }
        else if (titem.Type == GameConst.ITEM_TYPE_MATERIAL)
        {
            var upgrade_bubble = StrengthenModel.Instance.canUpgradeNow();
            if (upgrade_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade))
            {
                BubbleManager.SetBubble(BubbleConst.StrengthenUpgrade, upgrade_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
            }                                                                                                                                                                                                                                                                                                                                                       
        }
    }

    /// <summary>
    /// 判断是否为英雄卡
    /// </summary>
    /// <param name="cfg"></param>
    /// <returns></returns>
    public static bool ItemIsHeroCard(ConfigItemLine cfg)
    {
        if (cfg == null) return false;
        return cfg.Type == GameConst.ITEM_TYPE_ITEMS && cfg.SubType == "HEROCARD";
    }

    public static bool ItemIsHeroCard(int itemId)
    {
        var cfg = GetItem(itemId);
         if (cfg == null) return false;
         return cfg.Type == GameConst.ITEM_TYPE_ITEMS && cfg.SubType == "HEROCARD";
    }

    public static void UpdateRenewList(p_item item)
    {
        for (int i = 0; i < renewlist.Count; i++)
        {
            if (renewlist[i].item_id == item.item_id)
            {
                renewlist.RemoveAt(i);
            }
        }
        for (int i = 0; i < hasrenewedlist.Count; i++)
        {
            if (hasrenewedlist[i].item_id == item.item_id)
            {
                hasrenewedlist.RemoveAt(i);
            }
        }
        GameDispatcher.Dispatch("buy_item_update");
    }

    public static void SendDelItem(int id)
    {
        NetLayer.Send(new tos_player_del_item() { uid = id });
    }

    /// <summary>
    /// 如果装备中的道具过期，会换上、武器
    /// </summary>
    /// <param name="pitem"></param>
    private static void ChangeOverdueItem(p_item pitem)
    {
        if (TimeUtil.GetRemainTimeType(pitem.time_limit) == -1)
        {
            var bagId = GetBagGroupId(pitem.uid);
            if (bagId == 0)
                return;

            var item = GetItem(pitem.item_id);
            if (item.SubType == "WEAPON1")  //主武器
            {
                var defalutWeaponUid = GetDepotItem(1003).uid;
                if (!IsBagHas(defalutWeaponUid))
                    NetLayer.Send(new tos_player_equip_item() { group = bagId, uid = defalutWeaponUid });
            }
            else if (item.SubType == "WEAPON2")  //副武器
            {
                NetLayer.Send(new tos_player_equip_item() { group = bagId, uid = GetDepotItem(1601).uid });
            }
            else if (item.SubType == "DAGGER")  //匕首
            {
                NetLayer.Send(new tos_player_equip_item() { group = bagId, uid = GetDepotItem(1701).uid });
            }
            else if (item.SubType == "GRENADE") //手雷
            {
                NetLayer.Send(new tos_player_equip_item() { group = bagId, uid = GetDepotItem(1801).uid });
            }
        }
    }

    /// <summary>
    /// 删除指定道具
    /// </summary>
    /// <param name="uid"></param>
    public static void DeleteItemData(int uid)
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].uid == uid)
            {
                m_items = m_items.ArrayDelete(i);
                break;
            }
        }
    }

    /// <summary>
    /// 按类型获取仓库中的道具组
    /// </summary>
    /// <param name="type"></param>
    /// <param name="subtype"></param>
    /// <returns></returns>
    public static p_item[] GetDepotItemsByType(string type, string subtype)
    {
        var list = new List<p_item>();
        for (int i = 0; i < m_items.Length; i++)
        {
            var item = GetItem(m_items[i].item_id);
            if (item.Type == type && (subtype == "ALL" || item.SubType == subtype))
                list.Add(m_items[i]);
        }
        return list.ToArray();
    }

    /// <summary>
    /// 按商城分类类型获取仓库中的道具组
    /// </summary>
    /// <param name="type"></param>
    /// <param name="subtype"></param>
    /// <returns></returns>
    public static p_item[] GetDepotItemsByMallType(string type, string subtype)
    {
        if (type == "FRAGMENT")
            return GetDepotItemsPartsByMallType(subtype);

        var list = new List<p_item>();
        var mallConfigs = ConfigManager.GetConfig<ConfigMall>();
        if (type == "DECORATION")
        {
            for (int i = 0; i < m_items.Length; i++)
            {
                var item = mallConfigs.GetMallLine(m_items[i].item_id, -1);
                if (item != null && item.Type == "ROLE" && item.SubType=="DECORATION")
                {
                    if (subtype != "ALL")
                    {
                        var config = ConfigManager.GetConfig<ConfigItemDecoration>().GetLine(m_items[i].item_id);
                        if (config.Part == subtype)
                        {
                            list.Add(m_items[i]);
                        }
                    }
                    else
                    {
                        list.Add(m_items[i]);
                    }
                }
            }
            return list.ToArray();
        }
        
        for (int i = 0; i < m_items.Length; i++)
        {
            var item = mallConfigs.GetMallLine(m_items[i].item_id, -1);
            if (item != null && item.Type == type && (subtype == "ALL" || item.SubType == subtype))
            {
                if (item.SubType != "DECORATION")
                {
                    list.Add(m_items[i]);
                }
            }
        }
        return list.ToArray();
    }
    /// <summary>
    /// 按商城分类类型获取武器零件的道具组
    /// </summary>
    /// <returns></returns>
    public static p_item[] GetDepotItemsPartsByMallType(string subtype)
    {
        var list = new List<p_item>();
        ConfigItemLine itemCfg;
        var mallConfigs = ConfigManager.GetConfig<ConfigMall>();
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].item_id == 21407)
            {
                int a;
            }
            itemCfg = GetItem(m_items[i].item_id);
            if (itemCfg is ConfigItemPartsLine)
            {
                var item = mallConfigs.GetMallLine((itemCfg as ConfigItemPartsLine).MergeItem, -1);
                if (item != null && item.Type == "WEAPON" && (subtype == "ALL" || item.SubType == subtype))
                    list.Add(m_items[i]);
            }
        }
        return list.ToArray();


    }

    /// <summary>
    /// 判断武器零件是否可以合成
    /// </summary>
    /// <param name="pItem"></param>
    /// <returns></returns>
    public static int isItemPartsCanMerge(p_item pItem)
    {
        int canMerge = -1;
        ConfigItemPartsLine partsCfg = ConfigManager.GetConfig<ConfigItemParts>().GetLine(pItem.item_id) as ConfigItemPartsLine;
        p_item p = ItemDataManager.GetDepotItem(partsCfg.MergeItem);
        if (p == null)
            canMerge = 0;
        else
        {
            var mergeItemCfg = ItemDataManager.GetItem((p.item_id));
            canMerge = (mergeItemCfg.AddRule != 4) && (p.time_limit == 0) ? -2 : 0;
            
        }
        if (partsCfg != null && pItem.item_cnt >= partsCfg.MergeCost)
        {
            if (canMerge != -2)
            {
                canMerge = 0;
            }
        }
        else if (canMerge == 0)
        {
            canMerge = -1;
        }

        return canMerge;
    }

    public static bool CheckReNewList()
    {
        bool has = false;
        for (int i = 0; i < m_items.Length; i++)
        {
            if (TimeUtil.CheckItemRemainTime(m_items[i].time_limit))
            {
                if (!renewlist.Contains(m_items[i]) && !checkhasRenewed(m_items[i]))
                {
                    var cfml = cfm.GetMallLine(m_items[i].item_id);
                    if ( cfml!= null)
                    {
                        if (IsSale(cfml))
                        {
                            renewlist.Add(m_items[i]);
                            has = true;
                        }
                    }
                }
            }
        }
        return has;
    }

    public static bool checkhasRenewed(p_item item)
    {
        for (int i = 0; i < hasrenewedlist.Count; i++)
        {
            if (hasrenewedlist[i].item_id == item.item_id)
            {
                return true;
            }
        }
        return false;
    }
    public static p_item[] GetDepotItemsByTags(params EnumItemTag[] tags)
    {
        var list = new List<p_item>();
        for (int i = 0; i < m_items.Length; i++)
        {
            var item = GetItem(m_items[i].item_id);
            if (item != null && item.Tags.Length > 0 && item.CheckTags(tags))
            {
                list.Add(m_items[i]);
            }
        }

        return list.ToArray();
    }

    /// <summary>
    /// 师徒界面枪类型筛选
    /// </summary>
    /// <param name="type"></param> 0:主武器和副武器  1:主武器  2：副武器
    /// <returns></returns>
    public static p_item[] GetMasterGunItemByType(int type = 0)
    {
        List<p_item> list = new List<p_item>();
        ConfigItemWeaponLine tempCfg;
        p_item tempPItem;
        int lngth = m_items.Length;
        for (int i = 0; i < lngth; i++)
        {

            tempPItem = m_items[i];
            tempCfg = GetItem(tempPItem.item_id) as ConfigItemWeaponLine;
            if (tempCfg == null)
            {
                continue;
            }
            bool isFit = false;
            switch (type)
            {
                case 0://主副武器
                    if (tempCfg.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || tempCfg.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2)
                    {
                        isFit = true;

                    }
                    break;
                case 1://主武器
                    if (tempCfg.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1)
                    {
                        isFit = true;
                    }
                    break;
                case 2://副武器
                    if (tempCfg.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2)
                    {
                        isFit = true;
                    }
                    break;
                default:
                    break;
            }

            if (isFit && checkMasterPutGun(tempPItem))
            {
                list.Add(tempPItem);
            }
        }
        return list.ToArray();
    }

    /// <summary>
    /// 判断枪械是否满足放入师徒枪械库的条件
    /// </summary>
    /// <param name="pItem"></param>
    /// <returns></returns>
    public static bool checkMasterPutGun(p_item pItem)
    {
        if (pItem.item_id == 1003 || pItem.item_id == 1601)
        {
            return false;
        }
        if (IsBagHas(pItem.uid))//已装备的武器不能借出
        {
            return false;
        }
        if (pItem.time_limit == 0)//永久武器可以借出
        {
            return true;
        }
        else if (pItem.time_limit > 0)//剩余时间必须满足  24个小时
        {
            TimeSpan timeSpan = TimeUtil.GetRemainTime(pItem.time_limit);
            if (timeSpan.TotalHours >= 24)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 根据id获取道具配置项
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static ConfigItemLine GetItem(int itemId)
    {
        ConfigItemLine item = null;
        item = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemBullet>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemArmor>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemRole>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemOther>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemAccessories>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemParts>().GetLine(itemId);
        if (item == null)
            item = ConfigManager.GetConfig<ConfigItemDecoration>().GetLine(itemId);
        if (item == null && itemId != 0)
            Logger.Error("道具不存在：" + itemId);
        return item;
    }

    /// <summary>
    /// 根据武器的商城subtype来过滤碎片道具列表
    /// </summary>
    /// <param name="subtype"></param>
    /// <returns></returns>
    public static ConfigItemOtherLine[] GetMaterialItemsByWeaponMallSubType(string subtype)
    {
        var list = new List<ConfigItemOtherLine>();
        var mallConfigs = ConfigManager.GetConfig<ConfigMall>();
        var otherLines = ConfigManager.GetConfig<ConfigItemOther>().m_dataArr;
        for (int i = 0; i < otherLines.Length; i++)
        {
            if (otherLines[i].Type == GameConst.ITEM_TYPE_MATERIAL)
            {
                var weaponMallItem = mallConfigs.GetMallLine(GetWeaponItemByMaterial(otherLines[i].ID).ID);
                if (weaponMallItem != null && (weaponMallItem.SubType == subtype || subtype == "ALL"))
                    list.Add(otherLines[i]);
            }
        }
        return list.ToArray();
    }

    /// <summary>
    /// 根据碎片itemid获得对应的武器道具
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static ConfigItemWeaponLine GetWeaponItemByMaterial(int itemId)
    {
        if (m_materialToWeaponDic == null)
        {
            m_materialToWeaponDic = new Dictionary<int, ConfigItemWeaponLine>();
            var weaponConfig = ConfigManager.GetConfig<ConfigItemWeapon>();
            var arr = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().m_dataArr;
            for (int i = 0; i < arr.Length; i++)
            {
                m_materialToWeaponDic[arr[i].DisplayStuff] = weaponConfig.GetLine(arr[i].Weapon);
            }
        }

        return m_materialToWeaponDic.ContainsKey(itemId) ? m_materialToWeaponDic[itemId] : null;
    }

    /// <summary>
    /// 根据uid获取道具配置项
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    public static ConfigItemLine GetItemByUid(int uid)
    {
        var pitem = GetDepotItemByUid(uid);
        return pitem != null ? GetItem(pitem.item_id) : null;
    }

    /// <summary>
    /// 方便获取武器的道具配置项
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static ConfigItemWeaponLine GetItemWeapon(int itemId)
    {
        return GetItem(itemId) as ConfigItemWeaponLine;
    }

    /// <summary>
    /// 根据碎片的稀有度获得所有碎片
    /// </summary>
    /// <param name="rareType">-1为全部</param>
    /// <returns></returns>
    public static ConfigItemOtherLine[] GetMaterialsByRareType(int rareType)
    {
        var otherConfigs = ConfigManager.GetConfig<ConfigItemOther>().m_dataArr;
        var list = new List<ConfigItemOtherLine>();
        for (int i = 0; i < otherConfigs.Length; i++)
        {
            var item = otherConfigs[i];
            if (item.Type == GameConst.ITEM_TYPE_MATERIAL && item.ConvertType != 0 && (rareType == -1 || rareType == item.RareType))
                list.Add(otherConfigs[i]);
        }
        return list.ToArray();
    }

    /// <summary>
    /// 根据碎片的兑换类型获得所有碎片
    /// </summary>
    /// <param name="convertType">从1开始</param>
    /// <returns></returns>
    public static ConfigItemOtherLine[] GetMaterialsByConvertType(int convertType)
    {
        var otherConfigs = ConfigManager.GetConfig<ConfigItemOther>().m_dataArr;
        var list = new List<ConfigItemOtherLine>();
        for (int i = 0; i < otherConfigs.Length; i++)
        {
            var item = otherConfigs[i];
            if (item.Type == GameConst.ITEM_TYPE_MATERIAL && item.ConvertType != 0 && item.ConvertType == convertType)
                list.Add(otherConfigs[i]);
        }
        list.Sort((a, b) => b.RareType - a.RareType);
        return list.ToArray();
    }

    /// <summary>
    /// 根据id获取仓库中的第一个数据项
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static p_item GetDepotItem(int itemId)
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].item_id == itemId)
                return m_items[i];
        }
        return null;
    }


    /// <summary>
    /// 根据id获取仓库中的最后一个数据项
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static p_item GetDepotLastItem(int itemId)
    {
        for (int i = m_items.Length-1; i >= 0; i--)
        {
            if (m_items[i].item_id == itemId)
                return m_items[i];
        }
        return null;
    }


    public static p_item GetDepoUnlimitItem(int itemId)
    {
        List<p_item> list = new List<p_item>();
        p_item temp = new p_item();
        temp = null;
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].item_id == itemId)
            {
                list.Add(m_items[i]);
                //if (TimeUtil.GetRemainTimeType(m_items[i].time_limit) != -1)
                //{
                    temp = m_items[i];
                //}
                if (temp != null)
                {
                    if (TimeUtil.GetRemainTimeType(temp.time_limit) == 0)
                    {
                        return temp;
                    }
                }
            }
        }
        return temp;
    }

   


    /// <summary>
    /// 根据uid获取仓库中数据项
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    public static p_item GetDepotItemByUid(int uid)
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].uid == uid)
                return m_items[i];
        }
        return null;
    }


    /// <summary>
    /// 仓库中是否存在相同itemId的道具
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static bool IsDepotHas(int itemId)
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].item_id == itemId)
                return true;
        }
        return false;
    }

    public static bool IsDepotHasAndInTime(int itemId)
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            if (m_items[i].item_id == itemId)
            {
                if (m_items[i].time_limit >= TimeUtil.GetNowTimeStamp()||m_items[i].time_limit<=0)
                {
                    return true;
                }
            }

        }
        return false;
    }

    /// <summary>
    /// 背包中是否存在相同itemId的道具（是否装备）
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    public static bool IsBagHas(int uid)
    {
        return GetBagGroupId(uid) != 0;
    }


    /// <summary>
    /// 获取玩家背包ID数组
    /// </summary>
    public static object[] GetGroupsID()
    {
        object[] arr = new object[m_bags.Length];
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = (object)m_bags[i].group_id;
        }
        return arr;
    }

    /// <summary>
    /// 获取道具装备的背包id
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    public static int GetBagGroupId(int uid)
    {
        for (int i = 0; i < m_bags.Length; i++)
        {
            var equipList = m_bags[i].equip_list;
            if (uid == equipList.WEAPON1 || uid == equipList.HELMET || uid == equipList.ARMOR ||
                uid == equipList.BOOTS || uid == equipList.WEAPON2 || uid == equipList.DAGGER ||
                uid == equipList.GRENADE || uid == equipList.FLASHBOMB)
            {
                return m_bags[i].group_id;
            }
        }
        return 0;
    }

    public static PropRenderInfo[] GetPropRenderInfos(p_item data)
    {
        var info = data != null ? ItemDataManager.GetItem(data.item_id) as ConfigItemAccessoriesLine : null;
        var add_props = info != null ? info.GetAddProp() : null;
        var result = new List<PropRenderInfo>();
        if (add_props != null && add_props.Length > 0)
        {
            var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
            for (int i = 0; add_props != null && i < add_props.Length; i++)
            {
                if (data.accessory_values != null && i < data.accessory_values.Length)
                {
                    if (add_props[i] != null)
                    {
                        result.Add(new PropRenderInfo() { hidePgBar = true, name = prop_config.GetDispPropName(info.DispProps[i]), prop_type = info.DispProps[i], value = Mathf.Abs(data.accessory_values[i]), base_value = add_props[i].AddType == 1 ? 1000 : 0 });
                    }
                }
            }
        }
        return result.ToArray();
    }

    /// <summary>
    /// 获取武器的属性信息,value为0则表示改项不存在
    /// </summary>
    /// <param name="itemWeapon"></param>
    /// <param name="withStrengthen"></param>
    /// <param name="withPart"></param>
    /// <param name="withSuit"></param>
    /// <returns></returns>
    public static PropRenderInfo[] GetPropRenderInfos(ConfigItemWeaponLine itemWeapon, bool withStrengthen = false, bool withPart = false)
    {
        TDStrengthenLevel levelInfo = null;
        if (withStrengthen)
        {
            var strengthenInfo = StrengthenModel.Instance.getSStrengthenDataByCode(itemWeapon.ID);
            if (strengthenInfo != null)
                levelInfo = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(strengthenInfo.level, strengthenInfo.state, itemWeapon.StrengthenGroup);
        }
        if (levelInfo == null)
            levelInfo = new TDStrengthenLevel();
        var arr = new PropRenderInfo[]
        {
            new PropRenderInfo() {name = "伤害", prop_type="DispDamage", value = itemWeapon.DispDamage,valueAdd = levelInfo.DispDamage},
            new PropRenderInfo() {name = "伤害", prop_type="DispAttackForce", value = itemWeapon.DispAttackForce,valueAdd = levelInfo.DispDamage},
            new PropRenderInfo() {name = "伤害", prop_type="DispNearDamage", value = itemWeapon.DispNearDamage,valueAdd = levelInfo.DispDamage},

            new PropRenderInfo() {name = "射速", prop_type="DispShootSpeed", value = itemWeapon.DispShootSpeed,valueAdd = levelInfo.DispShootSpeed},
            new PropRenderInfo() {name = "攻速", prop_type="DispAttackSpeed", value = itemWeapon.DispAttackSpeed,valueAdd = levelInfo.DispShootSpeed},

            new PropRenderInfo() {name = "射程", prop_type="DispFireRange", value = itemWeapon.DispFireRange,valueAdd = levelInfo.DispFireRange},
            new PropRenderInfo() {name = "范围", prop_type="DispAttackArea", value = itemWeapon.DispAttackArea,valueAdd = levelInfo.DispFireRange},
            new PropRenderInfo() {name = "范围", prop_type="DispDamageArea", value = itemWeapon.DispDamageArea,valueAdd = levelInfo.DispFireRange},
            
            new PropRenderInfo() {name = "精准", prop_type="DispAccuracy", value = itemWeapon.DispAccuracy,valueAdd = levelInfo.DispAccuracy},
            new PropRenderInfo() {name = "稳定", prop_type="DispRecoil", value = itemWeapon.DispRecoil,valueAdd = levelInfo.DispRecoil},
            new PropRenderInfo() {name = "穿甲", prop_type="DispPierce", value = itemWeapon.DispPierce,valueAdd = levelInfo.DispPierce},
            new PropRenderInfo() {name = "便携", prop_type="DispWeight", value = itemWeapon.DispWeight,valueAdd = levelInfo.DispWeight},
            
            new PropRenderInfo() {name = "防弹", prop_type="DispBulletproof", value = itemWeapon.DispBulletproof},
            new PropRenderInfo() {name = "防爆", prop_type="DispBlastproof", value = itemWeapon.DispBlastproof},
            new PropRenderInfo() {name = "移速", prop_type="DispMoveSpeed", value = itemWeapon.DispMoveSpeed},
            new PropRenderInfo() {name = "载弹", prop_type="DispClipCap", value = itemWeapon.DispClipCap,valueAdd = levelInfo.DispClipCap, hidePgBar = true},
            new PropRenderInfo() {name = "耐久", prop_type="DispDurability", value = itemWeapon.DispDurability},
        };
//        List<PropRenderInfo> list = new List<PropRenderInfo>(arr);
//        if (itemWeapon.SpecialAtt.Length != 0)
//        {
//            if (itemWeapon.SpecialAtt.Length != 2)
//            {
//               Logger.Error("配置错误");
//            }
//            else
//            {
//                PropRenderInfo info = new PropRenderInfo() { name = "特殊属性", prop_type = itemWeapon.SpecialAtt[0].ToString(), value = itemWeapon.SpecialAtt[1] };
//                list.Add(info);
//            }
//        }
//        arr = list.ToArray();
        if (withPart == true)
        {
            p_item sweapon = ItemDataManager.GetDepotItem(itemWeapon.ID);
            var part_add_map = StrengthenModel.Instance.GenPartPropAddMap(sweapon);
            var suit_add_map = StrengthenModel.Instance.GenSuitPropAddMap(sweapon);
            Func<int, KeyValue<int, int>, float> Formula = (v, p) => p.key == 1 ? v * p.value / 1000f : p.value;
            KeyValue<int, int> vo = null;
            foreach (var info in arr)
            {
                if (part_add_map.TryGetValue(info.prop_type, out vo))
                {
                    info.partAdd = Formula(info.value, vo);
                }
                if (suit_add_map.TryGetValue(info.prop_type, out vo))
                {
                    info.suitAdd = Formula(info.value, vo);
                }
            }
        }
        return arr;
    }

    /// <summary>
    /// 根据商城中道具对应的道具类型(不是商城里的类型)获取商城道具配置数组
    /// </summary>
    /// <param name="type"></param>
    /// <param name="subtype"></param>
    /// <returns></returns>
    public static CDMallItem[] GetMallItemsByItemType(string type, string subtype)
    {
        var list = new List<CDMallItem>();
        //        var list = new List<ConfigMallLine>();
        var arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i].ShopType != 0 || !IsSale(arr[i]))//忽略不属于普通商城或者不在销售状态的物品
                continue;
            var item = GetItem(arr[i].ItemId);
            if (item != null && item.Type == type && (subtype == "ALL" || item.SubType == subtype))
                list.Add(new CDMallItem() { info = arr[i] });
        }
        list.Sort((x, y) => x.info.NormalIndex - y.info.NormalIndex);
        return list.ToArray();
    }

    /// <summary>
    /// shop_type: -1-全部，0-普通，1-分享商店
    /// </summary>
    /// <param name="shop_type"></param>
    /// <returns></returns>
    public static ConfigMallLine[] GetMallItemsByShopType(int shop_type = -1)
    {
        var list = new List<ConfigMallLine>();
        var arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
        for (int i = 0; i < arr.Length; i++)
        {
            if (!IsSale(arr[i]))
                continue;
            if (shop_type == -1 || arr[i].ShopType == shop_type)
            {
                list.Add(arr[i]);
            }
        }
        list.Sort((x, y) => x.NormalIndex - y.NormalIndex);
        return list.ToArray();
    }

    public static ConfigMallLine[] GetInstallmentItem()
    {
        var mall =  ConfigManager.GetConfig<ConfigMall>();
        var config = ConfigManager.GetConfig<ConfigPhasedPurchase>().m_dataArr;
        var list = new List<ConfigMallLine>();
        for (int i = 0; i < config.Length; i++)
        {
            var item = mall.GetLine(config[i].ID);
            if (item != null)
            {
                list.Add(item);
            }
        }

        return null;
    }

    /// <summary>
    /// 是否售卖
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static bool IsSale(ConfigMallLine item)
    {
        if (item.SaleStartTime == 0 && item.SaleEndTime == 0)
            return false;
        return TimeUtil.NowInRange(item.SaleStartTime, item.SaleEndTime);
    }

    /// <summary>
    /// 是否热卖
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static bool IsHot(ConfigMallLine item)
    {
        if (item.HotStartTime == 0 && item.HotEndTime == 0 || item.TopIndex <= 0)
            return false;
        return TimeUtil.NowInRange(item.HotStartTime, item.HotEndTime);
    }

    /// <summary>
    /// 是否打折
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static bool IsDiscount(ConfigMallLine item)
    {
        if (item.DiscountStartTime == 0 && item.DiscountEndTime == 0 || item.Discount >= 1)
            return false;
        return TimeUtil.NowInRange(item.DiscountStartTime, item.DiscountEndTime);
    }

    /// <summary>
    /// 武器是否破损
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    public static bool IsBroken(int itemId)
    {
        var item = GetItem(itemId);
        if (item == null || item.SubType != "WEAPON1" && item.SubType != "WEAPON2")
            return false;

        var pitem = GetDepotItem(itemId);
        if (pitem == null)
            return false;

        if (pitem.durability == 0)
            return true;
        return false;
    }

    public static bool IsBrokenByUid(int uid)
    {
        var item = GetItemByUid(uid);
        if (item == null || item.SubType != "WEAPON1" && item.SubType != "WEAPON2")
            return false;
        var pitem = GetDepotItemByUid(uid);
        if (pitem == null)
            return false;

        if (pitem.durability == 0)
            return true;
        return false;
    }

    /// <summary>
    /// 获取真实的购买价格，考虑打折和打折期限
    /// </summary>
    /// <param name="item"></param>
    /// <param name="price"></param>
    /// <returns></returns>
    public static int GetPrice(ConfigMallLine item, int price)
    {
        var isDiscount = IsDiscount(item);
        return isDiscount ? Mathf.FloorToInt(price * item.Discount) : price;
    }

    public static string GetMoneyName(EnumMoneyType type)
    {
        if (type == EnumMoneyType.DIAMOND)
            return "钻石";
        else if (type == EnumMoneyType.COIN)
            return "金币";
        else if (type == EnumMoneyType.MEDAL)
            return "勋章";
        else
            return "";
    }

    /// <summary>
    /// 新增道具通知
    /// </summary>
    /// <param name="data"></param>
    static void Toc_item_new(toc_item_new data)
    {
        UpdateItemData(data.item, data.reason);
    }
    /// <summary>
    /// 更新道具通知
    /// </summary>
    /// <param name="data"></param>
    static void Toc_item_update(toc_item_update data)
    {
        UpdateItemData(data.item, data.reason);
        UpdateRenewList(data.item);
        if (data.item.item_id == 5016)
        {
            StrengthenModel.UpdateAwakeState();
        }
        StrengthenModel.UpdateBubble();
    }
    /// <summary>
    /// 删除道具通知
    /// </summary>
    /// <param name="data"></param>
    static void Toc_item_del(toc_item_del data)
    {
        DeleteItemData(data.uid);
        if (UIManager.IsOpen<PanelDepot>())
        {
            UIManager.GetPanel<PanelDepot>().UpdateView();
        }
    }

    /// <summary>
    /// 更新背包和仓库数据
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_item_equip_data(toc_player_item_equip_data data)
    {
        UpdateItemData(data.item_bag, data.equip_groups);
    }

    /// <summary>
    /// 更新背包
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_equip_bag(toc_player_equip_bag data)
    {
        UpdateItemData(data.data);
    }

    /// <summary>
    /// 装备返回
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_equip_item(toc_player_equip_item data)
    {
        TipsManager.Instance.showTips("装备成功");
        //UIManager.ShowTipPanel("装备成功");
        UpdateItemData(data.val);

        ModelDisplay.ConfigDecoration = null;
        ModelDisplay.UpdateModel();
    }


    static void Toc_player_unequip_decoration(toc_player_unequip_decoration data)
    {
        var list = new List<p_group_decoration>();
        for (int i = 0; i < m_bags[data.group - 1].decorations.Length; i++)
        {
            if (m_bags[data.group - 1].decorations[i].part != data.part)
            {
                list.Add(m_bags[data.group - 1].decorations[i]);
            }
        }



        m_bags[data.group - 1].decorations = list.ToArray();
    }
    /// <summary>
    /// 开背包
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_buy_equip_bag(toc_player_buy_equip_bag data)
    {
        TipsManager.Instance.showTips("开启成功");
        //UIManager.ShowTipPanel("开启成功");
        UpdateItemData(data.bag);
    }

    /// <summary>
    /// 购买
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_buy_item(toc_player_buy_item data)
    {
        //UIManager.ShowTipPanel("购买成功");
        TipsManager.Instance.showTips("购买成功");
    }

    /// <summary>
    /// 通用道具使用返回
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_use_item(toc_player_use_item data)
    {
        var info = ItemDataManager.GetItem(data.item_id);
        if (info != null)
        {
            if (info.Type == GameConst.ITEM_TYPE_ITEMS && info.SubType == GameConst.ITEMS_SUBTYPE_BUFFCARD)
            {
                TipsManager.Instance.showTips("双倍掉落开始");
            }
            else if (info.UseType == "animal_weapons")
            {
                UIManager.PopPanel<PanelEquipBox>(new object[] { info.ID, item_added_temp_record.value.ToArray() }, true, null);
            }
            else if (info.UseType == "use_giftbag")
            {
                ConfigItemOtherLine itemOther = info as ConfigItemOtherLine;
                if (itemOther != null && itemOther.EffectType.Length == 2 && itemOther.VirtualItemList.Length > 0)
                {
                    return;
                }
                else
                {
                    var reward_info = info is ConfigItemOtherLine ? ConfigManager.GetConfig<ConfigReward>().GetLine((info as ConfigItemOtherLine).UseParam) : null;
                    if (reward_info != null && reward_info.ItemList != null && reward_info.ItemList.Length > 0 && reward_info.Probability.Length == 0)
                    {
                        //UIManager.PopPanel<PanelRewardItemTip>(new object[]{false,"恭喜你获得："},true).SetRewardItemList(reward_info.ItemList);
                        if (!PanelRewardItemTipsWithEffect.isShowing)
                        {
                            PanelRewardItemTipsWithEffect.DoShow(reward_info.ItemList);
                        }
                    }
                }
            }
        }
    }



    static void Toc_player_blackmarket_notice(toc_player_blackmarket_notice data)
    {
//        BubbleManager.SetBubble(BubbleConst.MallSPNotice, true);
    }

    static void Toc_player_use_giftbag(toc_player_use_giftbag giftbag)
    {
        var itemOther = ConfigManager.GetConfig<ConfigItemOther>().GetLine(giftbag.giftbag_id);
        if (itemOther != null && itemOther.EffectType.Length == 2 && itemOther.VirtualItemList.Length > 0)
        {
            List<ItemInfo> list = new List<ItemInfo>();
            for (int i = 0; i < giftbag.item_list.Length; i++)
            {
                ItemInfo info = new ItemInfo();
                info.ID = giftbag.item_list[i].item_id;
                info.cnt = giftbag.item_list[i].item_cnt;
                info.day = giftbag.item_list[i].days;
                list.Add(info);
            }
            UIManager.PopPanel<PanelEquipBox>(new object[] { itemOther.ID, list.ToArray() }, true, null);
            return;
        }
        var itemReward = ConfigManager.GetConfig<ConfigReward>().GetLine(itemOther.UseParam);
        if (itemReward.Probability == null || itemReward.Probability.Length == 0)
        {
            if (!PanelRewardItemTipsWithEffect.isShowing)
                PanelRewardItemTipsWithEffect.DoShow(itemReward.ItemList);
        }
        else
        {
            List<ItemInfo> list = new List<ItemInfo>();
            for (int i = 0; i < giftbag.item_list.Length; i++)
            {
                ItemInfo info = new ItemInfo();
                info.ID = giftbag.item_list[i].item_id;
                info.cnt = giftbag.item_list[i].item_cnt;
                info.day = giftbag.item_list[i].days;
                list.Add(info);
            }
            if (!PanelRewardItemTipsWithEffect.isShowing)
                PanelRewardItemTipsWithEffect.DoShow(list.ToArray());
        }
    }

    static void Toc_player_item_transform(toc_player_item_transform data)
    {
        itemConvertPair = new KeyValuePair<ItemInfo, ItemInfo>(data.to, data.from);
        Logger.Log(string.Format("from:{0},to:{1}", GetItem(data.from.ID).DispName, GetItem(data.to.ID).DispName));
    }

    static void Toc_player_phased_purchase_data(toc_player_phased_purchase_data data)
    {
        phasedItemList = new List<p_phased_purchase_item>(data.phased_purchase_list);
    }


    static void Toc_player_phased_item_new(toc_player_phased_item_new data)
    {
        phasedItemList.Add(data.item);
    }

    static void Toc_player_phased_item_update(toc_player_phased_item_update data)
    {
        for (int i = 0; i < phasedItemList.Count; i++)
        {
            if (phasedItemList[i].item_uid == data.item.item_uid)
            {
                phasedItemList[i] = data.item;
                return;
            }
        }
        phasedItemList.Add(data.item);
    }

    static void Toc_player_phased_item_del(toc_player_phased_item_del data)
    {
        for (int i = 0; i < phasedItemList.Count; i++)
        {
            if (phasedItemList[i].item_uid == data.item_uid)
            {
                phasedItemList.RemoveAt(i);
            }
        }
    }

    public static p_phased_purchase_item GetPhasedInfoByMallId(int mallid)
    {
        for (int i = 0; i < phasedItemList.Count; i++)
        {
            if (phasedItemList[i].mall_id== mallid)
            {
                    return phasedItemList[i];
            }
        }
        return null;
    }


    public static p_phased_purchase_item GetPhasedInfoByItemId(int itemid,int phasedTime)
    {
        for (int i = 0; i < phasedItemList.Count; i++)
        {
            if (phasedItemList[i].item_id == itemid)
            {
                if (phasedItemList[i].total_phased_number == phasedTime)
                {
                    return phasedItemList[i];
                }
            }
        }
        return null;
    }

    public static p_phased_purchase_item GetPhasedInfoByUid(int uid)
    {
        for (int i = 0; i < phasedItemList.Count; i++)
        {
            if (phasedItemList[i].item_uid == uid)
            {
                return phasedItemList[i];
            }
        }
        return null;
    }


    public static ItemInfo GetFromItem(int id)
    {
        return itemConvertPair.Value;
    }

    public static ItemTime FloatToDayHourMinute(float time)
    {
        ItemTime itemTime = new ItemTime();
        if (time >= 1f)
        {
            itemTime.day = Mathf.CeilToInt(time);
            return itemTime;
        }
        time = time * 100;
        if (time >= 1f)
        {
            itemTime.hour = Mathf.CeilToInt(time);
            return itemTime;
        }
        time = time * 100;
        if (time >= 1f)
        {
            itemTime.min = Mathf.CeilToInt(time);
            return itemTime;
        }
        return itemTime;
    }


    public static bool CanWeaponUpgrade(int weaponId)
    {

        var weaponcfg = GetItemWeapon(weaponId);
        var strdata = StrengthenModel.Instance.getSStrengthenDataByCode(weaponId);
        var config = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>();
        var next_info = config.getStrengthenLevelInfo(strdata.level + 1, strdata.state, weaponcfg.StrengthenGroup);
        var level_info = config.getStrengthenLevelInfo(strdata.level, strdata.state, weaponcfg.StrengthenGroup);
        var can_upgrade = level_info != null && next_info != null && next_info.State > level_info.State;

        if (can_upgrade == true)
        {
            var state_info = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(strdata.weapon_id, next_info.State);
            can_upgrade = state_info != null;
            if (can_upgrade == true)
            {
                foreach (ItemInfo item_info in state_info.Consume)
                {
                    var sitem = ItemDataManager.GetDepotItem(item_info.ID);
                    if (sitem == null || sitem.item_cnt < item_info.cnt)
                    {
                        can_upgrade = false;
                        break;
                    }
                }
            }
        }

        return can_upgrade;
    }


    public static bool CanWeaponAwaken(int weaponId)
    {
        var weaponcfg = GetItemWeapon(weaponId);
        var strData = StrengthenModel.Instance.getSStrengthenDataByCode(weaponId);
        var level = strData != null ? strData.level : 1;
        var state = strData != null ? strData.state : 0;
        var currentLevels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(state, weaponcfg.StrengthenGroup);
        var maxLevel = currentLevels.Length > 0 ? currentLevels[currentLevels.Length - 1].Level : 0;

        if (level == maxLevel && maxLevel == 9)
        {
            var info = ConfigManager.GetConfig<ConfigWeaponAwaken>().GetWakeupLevelData(weaponcfg.ID, 1);
            if (info != null && info.Open == 1)
            {
                var cost = ConfigManager.GetConfig<ConfigWeaponAwakenGroup>().GetLine(weaponId).ItemNum;
                var _materail_data = ItemDataManager.GetItem(ConfigManager.GetConfig<ConfigMisc>().GetLine("weapon_awaken_item").ValueInt);
                var sitem = ItemDataManager.GetDepotItem(_materail_data.ID);
                if ((strData.awaken_data == null || strData.awaken_data.level != 10) && sitem != null)
                {
                    if (sitem.item_cnt >= cost)
                    {
                        return ItemDataManager.IsDepotHasAndInTime(weaponId);
                    }
                }
            }
        }

        return false;
    }

}

public static class ItemColor
{
    public static Color White = Color.white;
    public static Color Green = Color.green;
    public static Color Blue = Color.blue;
    public static Color Purple = new Color(1, 0, 1);
    public static Color Orange = new Color(1, 0.6f, 0);
}

public class ItemTime
{
    public int day;
    public int hour;
    public int min;
    public string ToString(bool withKuoHao = true)
    {
        string str = "";
        if ((day+hour+min) == 0)
            str += "永久";
        if (day > 0)
            str += string.Format("{0}天", day);
        else if (hour > 0)
            str += string.Format("{0}小时", hour);
        else if (min > 0)
            str += string.Format("{0}分", hour);
        if (!withKuoHao)
            return str;
        str = string.Format("({0})", str);
        return str;
    }


}