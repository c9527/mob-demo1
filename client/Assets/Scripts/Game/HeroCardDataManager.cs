﻿using System;
using System.Collections.Generic;
using UnityEngine;
public enum EnumHeroCardType
{
    none = 0,
    normal = 1,
    super = 2,
    legend = 3
}
static class HeroCardDataManager
{
    public static int[] MallCardArray = { 391, 392, 393 };
    public static string[] HeroNameArray = { "英雄","超级英雄","传奇英雄"};
    public static toc_player_hero_card_data heroCardData;

    public static void Init()
    {
       GameDispatcher.AddEventListener<toc_login_select>(GameEvent.MAIN_PLAYER_JOIN, OnMainPlayerJoin);
    }

    public static void OnMainPlayerJoin(toc_login_select data)
    {
        NetLayer.Send(new tos_player_hero_card_data());
    }

    /// <summary>
    /// 获取当前英雄卡的最高等级
    /// </summary>
    /// <returns></returns>
    public static EnumHeroCardType getHeroMaxCardType()
    {
        EnumHeroCardType type = EnumHeroCardType.none;
        if (heroCardData != null)
        {
            int max = 0;
            if (heroCardData.cards != null)
            {
                foreach (var temp in heroCardData.cards)
                {
                    if (TimeUtil.GetRemainTimeType((uint)temp.end_time)!=-1&& temp.hero_type > max)
                    {
                        max = temp.hero_type;
                    }
                    type = (EnumHeroCardType)max;
                }
            }
        }
        return type;
    }


    /// <summary>
    /// 获取英雄卡等级
    /// </summary>
    /// <param name="cards"></param>
    /// <returns></returns>
    public static EnumHeroCardType getHeroCardTypeByCardInfo(p_hero_card_info[] cards)
    {
        EnumHeroCardType type = EnumHeroCardType.none;
      
            int max = 0;
            if (cards != null)
            {
                foreach (var temp in cards)
                {
                    if (TimeUtil.GetRemainTimeType((uint)temp.end_time) != -1 && temp.hero_type > max)
                    {
                        max = temp.hero_type;
                    }
                    type = (EnumHeroCardType)max;
                }
            }
       
        return type;
    }

    /// <summary>
    /// 判断能否领取奖励
    /// </summary>
    /// <returns></returns>
   public static bool isCanDrawGif()
   {
       //if(heroCardData.get)
       if (getHeroMaxCardType() == EnumHeroCardType.none) return false;
       DateTime time = TimeUtil.GetTime(HeroCardDataManager.heroCardData.reward_time);
       DateTime nowTime = TimeUtil.GetNowTime();
       if (time.Day != nowTime.Day || time.Month != nowTime.Month)
           return true;
       return false;
   }
   public static void setRewardTime(int rewardTime)
   {
       heroCardData.reward_time = rewardTime;
       BubbleManager.SetBubble(BubbleConst.HeroCardReward, isCanDrawGif(), true);
   }

    /// <summary>
    /// 英雄卡数据处理
    /// </summary>
    /// <param name="data"></param>
   static void Toc_player_hero_card_data(toc_player_hero_card_data data)
   {
       heroCardData = data;
       BubbleManager.SetBubble(BubbleConst.HeroCardReward, isCanDrawGif(), true);
       GameDispatcher.Dispatch(GameEvent.MAINPLAYER_HEROCARD_UPDATE);
   }

    /// <summary>
    /// 获取当前英雄卡等级升级下一级所需要的花费
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static int getNextUpCost()
    {
        EnumHeroCardType type = getHeroMaxCardType();
        if(type==EnumHeroCardType.legend)
        return 0;
        int cost = 0;
        int itemId = MallCardArray[(int)type];
        ConfigMallLine mallLine=ConfigManager.GetConfig<ConfigMall>().GetLine(itemId) as ConfigMallLine;
        if(mallLine!=null&&mallLine.Price!=null)
            cost = getUpCost((EnumHeroCardType)((int)type + 1), mallLine.Price[0].price, (int)mallLine.Price[0].time);
        return cost;
    }

    /// <summary>
    /// 获得升级需要的价格
    /// </summary>
    /// <param name="type"></param>
    /// <param name="mallPrice"></param>
    /// <param name="day"></param>
    /// <returns></returns>
    public static int getUpCost(EnumHeroCardType type, int mallPrice,int day)
    {
      
        int timeNormal=0;
        int timeSuper=0;
        int timeLegend=0;
        int daySec=86400;
        if (heroCardData != null && heroCardData.cards != null)
        {
            foreach (var temp in heroCardData.cards)
            {
                switch (temp.hero_type)
                {
                    case (int)EnumHeroCardType.normal:
                        timeNormal = temp.end_time;
                        break;
                    case (int)EnumHeroCardType.super:
                        timeSuper = temp.end_time;
                        break;
                    case (int)EnumHeroCardType.legend:
                        timeLegend = temp.end_time;
                        break;
                }
            }
        }
        List<int> timeList = new List<int>() { timeNormal, timeSuper, timeLegend };
        string[] strArray = ConfigMisc.GetValue("hero_card_prices").Split(';');
        List<int> prizeArray = new List<int>();
        int lngth = strArray.Length;
        for (int i = 0; i < lngth; i++)
        {
            prizeArray.Add(int.Parse(strArray[i]));
        }
        ConfigMallLine tempCfg = ConfigManager.GetConfig<ConfigMall>().GetLine(MallCardArray[1]) as ConfigMallLine;
        int remainTime = 0;
        int fullPrize = prizeArray[(int)type-1] * day;
        float allPrize=fullPrize;
        int lastRemainTime = (int)TimeUtil.GetRemainTime((uint)timeList[(int)type - 1]).TotalSeconds;
        if (lastRemainTime < 0) lastRemainTime = 0;
        for (int i = (int)type - 1; i > 0; i--)
        {
            remainTime = (int)TimeUtil.GetRemainTime((uint)timeList[i - 1]).TotalSeconds -lastRemainTime;
            if(remainTime<0) remainTime=0;
            if(remainTime>0)
            lastRemainTime = remainTime;
            if (remainTime > daySec * day)
                remainTime = daySec * day;
            allPrize -= remainTime * prizeArray[i-1] / (float)daySec;
        }
        return (int)Math.Ceiling((double )(allPrize * mallPrice / fullPrize));
    }
}

