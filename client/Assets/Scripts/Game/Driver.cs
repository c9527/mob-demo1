﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using DockingModule;
using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_WEBPLAYER
using DockingModule;
using DockingWeb;
#endif
#if UNITY_STANDALONE
using DockingModule;
#endif

/// <summary>
/// 启动脚本3/3  (GameLoader --> UpdateWorker --> Driver)
/// </summary>
public class Driver : MonoBehaviour
{
    public const string CONTROL_MODE_TOUCH = "touch";
    public const string CONTROL_MODE_INPUT = "input";
    public static EnumPlatform m_platform;
    public static bool isMobilePlatform;
    private string m_strCurControlMode;
    private bool m_inited;
    static public Driver singleton;
    static public bool isRunning = true;

    public DateTime now;

    [HideInInspector]
    public bool m_showMask = false;

    private bool m_showMask2 = false;
    [HideInInspector]
    public bool m_showQueueMask = false;

    [HideInInspector]
    private List<AsyncItem> m_asyncList = new List<AsyncItem>();

    private bool m_lastCanReconnect = false;
    private float m_lastReconnectTime;  //上一次重连时刻
    private int m_reConnectCount = 0;   //重连次数计数器
    private int m_OnlineTime;
    private float m_startTime;

    private UIEffect m_effectMouseLoop;
    private UIEffect m_effectMouseClick;
    private Transform m_tranEffectMouseLoop;
    private ParticleSystem m_psEffectMouseLoop;

    public static bool m_hasInnerServerlist;

    private static event Application.LogCallback m_logCallback;

    private UDB.UDB mUdb = null;

    private float mMaxDelayTime = 0;
    private float mFrameTime = 0;
    private int mFrameCount = 0;
    private int mLastFps = 0;

    void Awake()
    {
#if UNITY_ANDROID
        m_platform = EnumPlatform.android;
        isMobilePlatform = true;
#elif UNITY_IPHONE
        m_platform = EnumPlatform.ios;
        isMobilePlatform = true;
#elif UNITY_WEBPLAYER
        m_platform = EnumPlatform.web;
        isMobilePlatform = false;
#elif UNITY_STANDALONE
        m_platform = EnumPlatform.pc;
        isMobilePlatform = false;
#else
        m_platform = EnumPlatform.other;
        isMobilePlatform = false;
#endif

        // 尽早注册，可以看到更多日志
        Application.RegisterLogCallback(OnLogCallbackHandler);
        BuglyAgent.RegisterLogCallback(OnLogCallbackHandler);
        RegisterLogCallback(SdkManager.LogCallBack);
        
        if (m_platform == EnumPlatform.pc)
            QualitySettings.SetQualityLevel(3, true);

        if (singleton != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        singleton = this;

        DontDestroyOnLoad(gameObject);
        StartCoroutine(CheckInnerServerlist());
        SdkManager.Init();
        Logger.Init();

#if UNITY_WEBPLAYER
        this.gameObject.AddComponent<DockingWeb.Docking>();
#endif

#if DebugErrorMode
        StartCoroutine(TryGetServerlistUrl());
#endif
    }
        
    void Start()
    {
        if (m_platform == EnumPlatform.web || m_platform == EnumPlatform.pc)
            DockingService.Initialize();

        TimerManager.Init();
        ResourceManager.Init(this, () => ConfigManager.LoadAllConfig(LoadConfigComplete));
        Logger.Warning("UnityVersion = " + Application.unityVersion);
        m_OnlineTime = 0;
        m_startTime = Time.time;

        mUdb = new UDB.UDB(100);
        RegisterLogCallback(mUdb.LogCallBack);
        mUdb.SetScriptProcess(LuaHook.ExeLua);
        mUdb.StstusText = "ver:" + Global.currentVersion;
        mUdb.Start();
        StartCoroutine(mUdb.UpdateCallback());
    }

    private IEnumerator TryGetServerlistUrl()
    {
        if (!string.IsNullOrEmpty(Global.gameDefineServerlistUrl)) 
            yield break;

        Global.useDebugServerlistUrl = true;
        string updateUrl = null;

        //读取服务器列表地址
        var pathGameDefine = UpdateWorker.AppRootPathWww + UpdateWorker.InfoDir + "GameDefine.txt";
        var www = new WWW(pathGameDefine);
        yield return www;
        var lines = www.text.Split(new char[]{'\r','\n'}, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].StartsWith("ServerlistUrl"))
                Global.gameDefineServerlistUrl = lines[i].Split('=')[1];
            if (lines[i].StartsWith("UpdateUrl"))
                updateUrl = lines[i].Split('=')[1];
        }
        www.Dispose();

        //获取最新版本号，防止被后端踢
        var pathVersion = updateUrl + "Version.txt?id=" + (DateTime.Now.Ticks / 10000000);
        www = new WWW(pathVersion);
        yield return www;
        lines = www.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].StartsWith("Version"))
                Global.currentVersion = lines[i].Split('=')[1];
        }
        www.Dispose();
    }

    private IEnumerator CheckInnerServerlist()
    {
        if (m_platform != EnumPlatform.android)
            yield break;

        var www = new WWW(UpdateWorker.AppRootPathWww + "serverlist");
        yield return www;
        m_hasInnerServerlist = string.IsNullOrEmpty(www.error);

        Logger.Log("m_hasInnerServerlist:" + m_hasInnerServerlist + "\n"+ www.url + "\n" + www.error);
    }

    private void LoadConfigComplete()
    {
        ConfigMisc.Load();
        SdkManager.InitGameIdMaping();
        ResourceManager.AddResourceItems(new []
        {
            new ResourceItem("Shader/Additive", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/Additive_TintColor", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/AlphaBlend", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/AlphaBlend_TintColor", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/DiffSpec", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/DiffSpec_Metal", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/DiffSpec_TwoLit", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/DiffSpec_TwoLit_Transparent", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/GrayFilter", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/GrayFontFilter", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/LightProbe_Diff", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/LightProbe_Spec", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/LightProbe_Spec_Flow", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/LightProbe_Spec_Transparent", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/LightProbe_Texture", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/Multiply", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/PlayerHeadFlag", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/RichText", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/SkyBox", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/Texture", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/Texture_Brightness", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/Texture_Color", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/Texture_Lightmap", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/Transparent", EResType.Shader, ResTag.Forever, true),
            new ResourceItem("Shader/Transparent_AlphaAdd", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/Transparent_AlphaSet_OneSide", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/Unity/Unity_Diffuse", EResType.Shader, ResTag.Forever),
            new ResourceItem("Atlas/Guide", EResType.Atlas, ResTag.Forever),
        });

        if (isMobilePlatform)
            ResourceManager.AddResourceItems(new[] { new ResourceItem("AtlasAlpha/Guide_a", EResType.Atlas, ResTag.Forever)});

        var preloadArr = new[]
        {
            new ResourceItem("LuaRoot", EResType.Text, ResTag.Forever),
            new ResourceItem("Font/Normal", EResType.Font, ResTag.Forever),
            new ResourceItem("Atlas/Common", EResType.Atlas, ResTag.Forever),
            new ResourceItem("Atlas/Loading", EResType.Atlas, ResTag.Forever),
            new ResourceItem("Atlas/Icon", EResType.Atlas, ResTag.Forever),
            new ResourceItem("Atlas/RoleIcon", EResType.Atlas, ResTag.Forever),
            new ResourceItem("Atlas/ArmyIcon", EResType.Atlas, ResTag.Forever),
            new ResourceItem("Shader/SpriteETC", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/SpriteGrayETC", EResType.Shader, ResTag.Forever),
            new ResourceItem("Shader/UI_ShanGuangETC", EResType.Shader, ResTag.Forever),
            new ResourceItem("Materials/mat_Gray", EResType.Material, ResTag.Forever),
            new ResourceItem("Materials/mat_GrayFont", EResType.Material, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/DropList", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/DropListEx", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/PopList", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/ItemPop", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/ItemDropListRender", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/DropList/ItemDropListRenderEx", EResType.UI, ResTag.Forever),
            new ResourceItem("UI/Common/PanelMiniTip", EResType.UI, ResTag.Forever)
        };
        ResourceManager.LoadMulti(preloadArr, null, OnPreloadComplete);
    }

    private void OnPreloadComplete()
    {
        Init();
    }

    public string controlMode
    {
        get{
            return m_strCurControlMode;
        }
        set{
            m_strCurControlMode = value;
        }
    }

    private void Init()
    {
        NetLayer.AddAllHandler();
        GlobalConfig.Init();
        GameSetting.Init();
        QualityManager.Init();
        RightManager.Init();
		Time.timeScale = 1;
        Application.targetFrameRate = GameSetting.FRAME_RATE;
        new GlobalBattleParams();
        MainPlayerControlInputReceiver.singleton = gameObject.AddComponent<MainPlayerControlInputReceiver>();

#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
       
#else
        MainPlayerControlInputReceiver.singleton.enabled = false;
#endif           
#if UNITY_EDITOR
            controlMode = Driver.CONTROL_MODE_INPUT;
#elif UNITY_STANDALONE
            controlMode = Driver.CONTROL_MODE_INPUT;
#elif UNITY_WEBPLAYER
            controlMode = Driver.CONTROL_MODE_INPUT;
#else
            controlMode = Driver.CONTROL_MODE_TOUCH;
#endif

        if (GameSetting.DEBUG || Application.isEditor)
        {
//            gameObject.AddComponent<MonoRunInfo>();
            gameObject.AddComponent<UIGmPanel>();
        }

        gameObject.AddComponent<SceneManager>();

        
        UIManager.Init();
        AudioManager.Init();
        WordFiterManager.Init();
        GuideManager.Init();
        HeroCardDataManager.Init();
        new WorldManager();
        new EffectManager();
        new PlayerMissionData();
        //new PlayerAchieveData();
        new PlayerFriendData();
        new DropItemManager();
        new MasterGiftDataManager();
        //SwitchTempUI<UIServerTmpUI>();
        if (m_platform==EnumPlatform.web //平台
            && !string.IsNullOrEmpty(SdkManager.m_uid) && !string.IsNullOrEmpty(SdkManager.m_platform_server_id)//账号登陆数据
            && Global.useDebugServerlistUrl == false //非调试服务器数据
            )
        {
            StartCoroutine(AutoLoginGame(3));
        }
        else
        {
            UIManager.ShowPanel<PanelLogin>();
        }

        m_inited = true;
        AntiCheatManager.Init();
        Logger.Log("sdkmanager.localpush()");
        if(SdkManager.IsSupportFunction(SdkFuncConst.SHOW_LOCAL_PUSH))
        {
            SdkManager.LocalPush();
        }
        LuaHook.Init(this);

#if UNITY_EDITOR
        UnityEditor.EditorApplication.update += () =>
        {
            if (UnityEditor.EditorApplication.isCompiling)
            {
                NetLayer.Close();
                NetLayer.CloseUdp();
            }
        };
#endif
    }

    private IEnumerator AutoLoginGame(int tryCount = 0)
    {
        var www = new WWW(PanelLogin.GetServerListUrl());
        yield return www;
        while (!www.isDone && string.IsNullOrEmpty(www.error))
        {
            yield return null;
        }
        var error = "";
        if (string.IsNullOrEmpty(www.error))
        {
            var config = ScriptableObject.CreateInstance<ConfigServerListData>();
            config.Load(www.text);
            GlobalConfig.serverList = config.m_dataArr;
            TDServerListData server_info = null;
            if (GlobalConfig.serverList != null && GlobalConfig.serverList.Length > 0)
            {
                var server_id = SdkManager.ConvertPlatformServerID();
                foreach (TDServerListData info in GlobalConfig.serverList)
                {
                    if (info != null && info.ServerId == server_id)
                    {
                        server_info = info;
                        break;
                    }
                }
            }
            if (server_info != null)
            {
                var result = ConnectToServer.ConnectServer(server_info);
                if (result == 0)
                {
                    SdkManager.m_serverId = server_info.ServerId.ToString();
                    SdkManager.m_serverName = server_info.ServerName;
                    ConnectToServer.LoginVerify();
                }
                else if (result == 1)
                {
                    Driver.singleton.ShowQueueMask();
                }
                else
                {
                    error = "服务器连接失败";
                }
            }
            else
            {
                error = "加载服务器数据失败";
            }
        }
        else
        {
            error = "加载服务器数据失败";
            Logger.Error(www.error);
        }
        if (!string.IsNullOrEmpty(error))
        {
            if (tryCount > 0)
            {
                Debug.Log("连接服务器失败，重试...");
                TimerManager.SetTimeOut(1, () => StartCoroutine(AutoLoginGame(tryCount - 1)));
            }
            else
                UIManager.ShowTipPanel(error, () => StartCoroutine(AutoLoginGame()), null, false, false, "重试", null, 5, 1);
        }
    }

    public void ShowQueueMask()
    {
        NetLayer.ClearConnectId();
        TimerManager.SetTimeOut(UnityEngine.Random.Range(5f, 10f), () => GameDispatcher.Dispatch(GameEvent.LOGIN_VERIFY));

        if (!m_showQueueMask)
        {
            m_showQueueMask = true;
            UIManager.ShowMask("登录服务器人数过多，排队中...", float.PositiveInfinity);
        }
    }

    private void UpdateMouseEffect()
    {
        if (!m_inited) 
            return;

        if (m_effectMouseClick == null)
            m_effectMouseClick = UIEffect.ShowEffect(UIManager.GetLayer(LayerType.Loading), EffectConst.UI_SHUBIAO01, new Vector3(1000, 0, 0));
        if (m_effectMouseClick == null || m_effectMouseClick.m_goEffect == null)
            return;

        if (m_effectMouseLoop == null)
            m_effectMouseLoop = UIEffect.ShowEffect(UIManager.GetLayer(LayerType.Loading), EffectConst.UI_SHUBIAO02, new Vector3(1000, 0, 0));
        if (m_effectMouseLoop == null || m_effectMouseLoop.m_goEffect == null)
            return;

        if (m_tranEffectMouseLoop == null)
            m_tranEffectMouseLoop = m_effectMouseLoop.m_goEffect.transform;
        if (m_tranEffectMouseLoop == null) 
            return;

        if (m_psEffectMouseLoop == null)
            m_psEffectMouseLoop = m_tranEffectMouseLoop.Find("liubianxing").GetComponent<ParticleSystem>();
        if (m_psEffectMouseLoop == null)
            return;

        if (UIManager.IsBattle())
        {
            if (m_psEffectMouseLoop.isPlaying)
                m_psEffectMouseLoop.Stop();
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            m_effectMouseLoop.m_goEffect.GetComponent<ParticleSystem>().Play(true);
            m_psEffectMouseLoop.loop = true;
            m_tranEffectMouseLoop.SetAsLastSibling();

            m_effectMouseClick.m_goEffect.transform.position = UIManager.m_uiCamera.ScreenToWorldPoint(Input.mousePosition);
            m_effectMouseClick.m_goEffect.transform.SetAsLastSibling();
            m_effectMouseClick.Play();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_psEffectMouseLoop.loop = false;
        }
        if (Input.GetMouseButton(0))
        {
            m_tranEffectMouseLoop.position = UIManager.m_uiCamera.ScreenToWorldPoint(Input.mousePosition);
        }
    }

    void Update()
    {
        ResourceManager.Update();

        UpdateMouseEffect();

        for (int i = m_asyncList.Count - 1; i >= 0; i--)
        {
            if (m_asyncList[i] == null || m_asyncList[i].async == null)
                m_asyncList.RemoveAt(i);
            else if (m_asyncList[i].async.isDone)
            {
                var func = m_asyncList[i].callBack;
                m_asyncList.RemoveAt(i);
                if (func != null)
                    func();
            }
        }
        if (!m_inited)
            return;

        if (Time.deltaTime > mMaxDelayTime)
            mMaxDelayTime = Time.deltaTime;

        if (Time.time >= mFrameTime + 1)
        {
            mLastFps = mFrameCount;
            mFrameCount = 0;
            mFrameTime = Time.time;
        }
        else
        {
            mFrameCount++;
        }

        if (m_platform == EnumPlatform.web || m_platform == EnumPlatform.pc)
        {
            DockingService.Update();
            m_OnlineTime = (int)(Time.time - m_startTime);
            if (m_OnlineTime % 300 == 0 && m_OnlineTime / 300 != 0)
            {
                DockingService.SendOnlineLog(UIManager.fullScreen ? "1" : "0");
                m_startTime = Time.time;
            }
        }

        TimerManager.Update();
        UIManager.Update();
        GuideManager.Update();
        AudioManager.Update();

        DropItemManager.singleton.Update();

        if (WorldManager.singleton != null)
            WorldManager.singleton.UpdatePlayer();

        if (PlayerBattleModel.Instance != null)
            PlayerBattleModel.Instance.Update();

        if (UIManager.m_rootCanvas != null && UIManager.m_rootCanvas.gameObject.activeSelf != GameSetting.enableBattleUI)
            UIManager.m_rootCanvas.gameObject.TrySetActive(GameSetting.enableBattleUI);

        NetLayer.Update();
        ActivityFrameDataManager.Update();
        if (NetLayer.CanReconnect)
        {
            //在Socket连接后，未验证成功前，Socket被断开
            if (!ConnectToServer.LoginVerifySuccess)
            {
                if (!NetLayer.Connected)
                    ShowQueueMask();
            }
        }

        if (NetLayer.CanReconnect && !NetLayer.Connected)//连接断开，可以重连
        {
            if (!m_showMask)
            {
                m_showMask2 = true;
                m_showMask = true;
                m_lastReconnectTime = Time.time;
            }

            if (m_showMask2 && Time.time > m_lastReconnectTime + 3)
            {
                m_reConnectCount++;
                m_lastReconnectTime = Time.time;
                NetLayer.Reconnect();

                if (!NetLayer.Connected && m_reConnectCount == 1)
                    UIManager.ShowMask("网络不给力啊~ 努力重连中...", float.PositiveInfinity);
                if (m_reConnectCount >= 2 &&
                    (NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.ResponsePacketEmpty ||
                     NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.ResponsePacketTimeout ||
                     NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.SocketConnectFail ||
                     NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.SocketTimeout))
                {
                    NetLayer.EnableForwarding = true;
                }
                Logger.Warning(NetLayer.LastConnectErrorType.ToString());
            }
        }
        else if (m_showMask && NetLayer.Connected)//之前在重连，现在已连接成功
        {
            UIManager.HideMask();
            m_showMask2 = false;
            m_showMask = false;
            m_reConnectCount = 0;
        }

        if (m_lastCanReconnect && !NetLayer.CanReconnect)//连接由可重连变为不可重连
        {
            if (m_showMask)
            {
                UIManager.HideMask();
                m_showMask = false;
                m_showMask2 = false;
            }

            m_reConnectCount = 0;
            
            var msg = "";
            if (m_platform == EnumPlatform.web)
                msg = "请刷新游戏(F5)";
            else if (m_platform == EnumPlatform.pc)
                msg = "请重新启动游戏";
            else
                msg = "请重新登录";
            msg = "无法重连服务器，" + msg;
            if (!string.IsNullOrEmpty(PlayerSystem.m_closeMsg))
                msg = PlayerSystem.m_closeMsg;
            var panel = UIManager.ShowTipPanel(msg, () =>
            {
                SdkManager.ClearLoginInfo();
                AudioManager.SetListener();
                if (UIManager.IsBattle())
                {
                    if (WorldManager.singleton != null)
                        WorldManager.singleton.RunBattleModeScript(false);
                    SceneManager.singleton.ReleaseBattleSceneRes();
                }
                if (SceneManager.singleton != null)
                    SceneManager.singleton.ClearBattleSceneLoadedTag();

                PlayerSystem.m_closeCode = 0;
                PlayerSystem.m_closeMsg = "";
                SceneManager.ReturnToLogin();
            }, null, false);
            if (panel != null)
                panel.DontDestroyOnLoad = true;
        }
        m_lastCanReconnect = NetLayer.CanReconnect;

        QualityManager.Update();

        AntiCheatManager.Update();
    }

    public void ExecuteAsync(AsyncOperation async, Action callBack)
    {
        m_asyncList.Add(new AsyncItem(async, callBack));
    }

    //mashang 朱文说这里先临时处理一下新服，不限制进入等级
    public static bool IsNewServer()
    {
//        if (SdkManager.m_serverId == "1105")
//            return true;
        return GlobalConfig.serverValue.new_server_open;
    }

    //当前是否是跨服频道
    public static bool IsCrossServerChannel()
    {
        var config = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.m_channel);
        if (config == null || !config.ByCenter)
            return false;
        return true;
    }

    //当前是否显示跨服玩家的服名
    public static bool IsRoleCrossServerName()
    {
        var config = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.m_channel);
        if (config == null || !config.ByCenter || !config.IsRoleServerName)
            return false;
        return true;
    }

    //当前是否显示跨服AI假服名
    public static bool IsAiFakeCrossServerName()
    {
        var config = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.m_channel);
        if (config == null || !config.ByCenter || !config.IsAIFakeServerName)
            return false;
        return true;
    }

    public void OnApplicationQuit()
    {
        isRunning = false;
        mUdb.Close();
        NetLayer.Close();
        NetLayer.CloseUdp();
        ResourceManager.ClearAll();

        if (m_platform == EnumPlatform.web || m_platform == EnumPlatform.pc)
            DockingService.Destroy();
    }

    void OnApplicationPause(bool pause)
    {
        if (!ConnectToServer.LoginVerifySuccess)
            return;
        if (pause)
        {
            NetLayer.Close();
            mUdb.Close();
        }
        else
        {
            NetLayer.Reconnect();
            mUdb.Start();
        }
    }

    void OnApplicationFocus(bool focus)
    {
        GameDispatcher.Dispatch(GameEvent.UI_FOCUS_CHANGE, focus);
    }

    public void SetStatus(string txt)
    {
        mUdb.StstusText = txt;
        mUdb.ReportToServer();
    }

    #region 发送日志到APM服务器
    private static Dictionary<int, bool> m_apmCodeDic = new Dictionary<int, bool>(); 
#if UNITY_EDITOR
    private const string PLATFORM = "Unity";
    private const string APM_URL = "";
#elif UNITY_ANDROID
    private const string PLATFORM = "android";
    private const string APM_URL = "http://apiapm.gz4399.com/api/apm/8ff10eab76b5ac711";
#elif UNITY_IPHONE
    private const string PLATFORM = "ios";
    private const string APM_URL = "http://apiapm.gz4399.com/api/apm/cd3cdbdb76b5ac711";
#elif UNITY_WEBPLAYER
    private const string PLATFORM = "web";
    private const string APM_URL = "";
#elif UNITY_STANDALONE
    private const string PLATFORM = "pc";
    private const string APM_URL = "";
#else
    private const string PLATFORM = "";
    private const string APM_URL = "";
#endif

    public static void SendLogServer(int code, string msg = "", params object[] args)
    {
        if (args.Length >= 0)
            msg = string.Format(msg, args);
        Logger.Error(string.Format("APM Log[{0}]: {1}", code, msg));
        if (string.IsNullOrEmpty(APM_URL))
            return;
        (new Thread(PostApm)).Start(GetPostApmData(code, msg));
    }

    public static void SendLogServerUnique(int code, string msg = "", params object[] args)
    {
        if (args.Length >= 0)
            msg = string.Format(msg, args);
        Logger.Error(string.Format("APM Log[{0}]: {1}", code, msg));
        if (string.IsNullOrEmpty(APM_URL))
            return;
        if (m_apmCodeDic.ContainsKey(code))
            return;
        m_apmCodeDic.Add(code, true);
        (new Thread(PostApm)).Start(GetPostApmData(code, msg));
    }

    private static string GetPostApmData(int code, string msg)
    {
        var data = new StringBuilder();
        data.Append("&server_id="); data.Append(SdkManager.m_serverId == "" ? "0" : SdkManager.m_serverId);
        data.Append("&uid="); data.Append(PlayerSystem.roleId);
        data.Append("&log_time="); data.Append((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        data.Append("&code="); data.Append(code);
        data.Append("&code_msg="); data.Append(msg);
        data.Append("&did="); data.Append(SystemInfo.deviceUniqueIdentifier);
        data.Append("&device="); data.Append(PLATFORM);
        data.Append("&device_name="); data.Append(SystemInfo.deviceModel);
        data.Append("&game_version="); data.Append(Global.currentVersion);
        data.Append("&os="); data.Append(SystemInfo.operatingSystem);
        data.Append("&os_version=");
        data.Append("&Mno=");
        data.Append("&Nm="); data.Append((Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork ? "Wifi" : "Data"));
        return data.ToString();
    }

    private static void PostApm(object data)
    {
        try
        {
            HttpRequest(data.ToString());
        }
        catch (WebException ex)
        {
            Debug.Log("尝试DNS重连! 状态值: " + ex.Status);
            if (ex.Status.ToString().Equals("NameResolutionFailure"))
            {
                var ip = DnsRetryGet("apiapm.gz4399.com");
                if (!ip.Equals("0"))
                {
                    Debug.Log("获取到DNS重连IP: " + ip);
                    HttpRequest(data.ToString(), new WebProxy(ip, 80));
                }

                HttpRequest(GetPostApmData(1204, string.Format("ip:{0} status:{1} msg:{2}", ip, ex.Status, ex.Message)));
            }
        }
    }

    private static void HttpRequest(string content, WebProxy proxy = null)
    {
        var request = (HttpWebRequest)WebRequest.Create(APM_URL);
        request.Method = "POST";
        request.Timeout = 3000;
        if (proxy != null)
            request.Proxy = proxy;
        request.ContentType = "application/x-www-form-urlencoded";
        request.ServicePoint.Expect100Continue = false;
        byte[] byteArray = Encoding.UTF8.GetBytes(content);
        request.ContentLength = byteArray.Length;
        var dataStream = request.GetRequestStream();
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();
        var response = request.GetResponse();
        response.Close();
    }

    private static string DnsRetryGet(string doMain)
    {
        string ip;
        var dnsUrl = "http://119.29.29.29/d?dn=";
        const string pattern = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
        var ipMatch = new Regex(pattern);
        try
        {
            dnsUrl = (doMain.Length > 1) ? (dnsUrl + doMain) : "";
            var request = (HttpWebRequest)WebRequest.Create(dnsUrl);
            request.Method = "GET";
            request.Timeout = 5000;
            var response = request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            var reqText = reader.ReadToEnd();
            var ipArray = reqText.Split(';');
            reader.Close();
            response.Close();
            ip = (ipMatch.IsMatch(ipArray[0])) ? ipArray[0] : "0";
        }
        catch
        {
            return "0";
        }
        return ip;
    }
    #endregion

    // 游戏内的日志监听，统一用这个，不要直接用Application.RegisterLogCallback
    public static void RegisterLogCallback(Application.LogCallback handler)
    {
        m_logCallback += handler;
    }
    public static void UnregisterLogCallback(Application.LogCallback handler)
    {
        m_logCallback -= handler;
    }
    private static void OnLogCallbackHandler(string condition, string stackTrace, LogType type)
    {
        if (m_logCallback != null)
            m_logCallback(condition, stackTrace, type);
    }

    public static float MaxDelayTime
    {
        get { return singleton.mMaxDelayTime; }
        set { singleton.mMaxDelayTime = value; }
    }

    public static int LastFps { get { return singleton.mLastFps; } }
}

class AsyncItem
{
    public AsyncOperation async;
    public Action callBack;

    public AsyncItem(AsyncOperation async, Action callBack)
    {
        this.async = async;
        this.callBack = callBack;
    }
}
