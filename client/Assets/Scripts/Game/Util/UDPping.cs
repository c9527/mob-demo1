﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UDPping : Singleton<UDPping> {

    private const int UDPNum = 10;
    private const float UpdateInterval = 1f;

    private float m_tick;
    private uint[] m_udpID;
    private float[] m_udpSendTime;
    private float[] m_udpRecvTime;
    private int m_index;

    private float m_ping;


    public void Init()
    {
        m_udpID = new uint[UDPNum];
        m_udpSendTime = new float[UDPNum];
        m_udpRecvTime = new float[UDPNum];

        for (int i = 0; i < UDPNum; i++)
        {
            m_udpSendTime[i] = 0;
            m_udpRecvTime[i] = 0.03f;
        }

        m_tick = 0;
        m_index = 0;
        m_ping = 0;
    }

    public float ping
    {
        get { return m_ping; }
    }

    public void SendUDP(uint id)
    {
        if(PlayerBattleModel.Instance.battle_state.state == GameConst.Fight_State_RoundEnd)
        {
            //Logger.Log("SendUDP, RoundEnd No Record");
            return;
        }

        m_udpID[m_index] = id;
        m_udpSendTime[m_index] = Time.timeSinceLevelLoad;
        m_udpRecvTime[m_index] = -1;

        m_index = (m_index + 1) % UDPNum;

        //Logger.Log("Send: id=" + id + " " + Time.timeSinceLevelLoad);
    }

    public void Receive(uint id)
    {
        for(int i=0; i<m_udpID.Length; i++)
        {
            if(m_udpID[i] == id)
            {
                m_udpRecvTime[i] = Time.timeSinceLevelLoad;
                //Logger.Log("Rev: id=" + id + " " + m_udpRecvTime[i]);
                break;
            }
        }
    }


    public void Update()
    {
        if(Time.time - m_tick >= UpdateInterval)
        {
            m_tick = Time.time;

            float sum = 0;
            int num = 0;
            for (int i = 0; i < m_udpSendTime.Length; i++)
            {
                if(m_udpRecvTime[i] > 0)
                {
                    num++;
                    sum += m_udpRecvTime[i] - m_udpSendTime[i];
                }
            }
            
            m_ping = num > 0 ? sum / num : 9999.999f;
            //Logger.Warning("UDPPing: ping=" + m_ping + " num=" + num);
        }
       
    }

	 
}
