﻿using UnityEngine;
using UnityEngine.EventSystems;

class UGUIClickHandler: MonoBehaviour, IPointerClickHandler
{
    public delegate void PointerEvetCallBackFunc(GameObject target, PointerEventData eventData);
    public string m_sound = AudioConst.btnClick;
    public event PointerEvetCallBackFunc onPointerClick;
    public event PointerEvetCallBackFunc onDoublePointerClick;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (gameObject == null)
            return;
        if (!string.IsNullOrEmpty(m_sound))
            AudioManager.PlayUISound(m_sound, false);

        if (LuaHook.CheckHook(HookType.UI_Click_Before, 0, gameObject, eventData))
            return;

        if (onDoublePointerClick != null && eventData.clickCount == 2)
            onDoublePointerClick(gameObject, eventData);
        else if (onPointerClick != null) 
            onPointerClick(gameObject, eventData);
        else
            UIManager.ShowTipPanel("系统暂未开放");

        if (LuaHook.CheckHook(HookType.UI_Click_After, 0, gameObject, eventData))
            return;
    }

    public bool IsEmpty()
    {
        return onPointerClick == null;
    }

    public void RemoveAllHandler()
    {
        onPointerClick = null;
        onDoublePointerClick = null;
        DestroyImmediate(this);
    }

    public static UGUIClickHandler Get(GameObject go)
    {
        if (go == null)
        {
            Logger.Error("UGUI Click Handler Get Obj null");
            return null;
        }
        UGUIClickHandler listener = go.GetComponent<UGUIClickHandler>();
        if (listener == null) 
            listener = go.AddComponent<UGUIClickHandler>();
        return listener;
    }

    public static UGUIClickHandler Get(GameObject go, string sound)
    {
        UGUIClickHandler listener = Get(go);
        listener.m_sound = sound;
        return listener;
    }

    public static UGUIClickHandler Get(Transform tran, string sound)
    {
        UGUIClickHandler listener = Get(tran);
        listener.m_sound = sound;
        return listener;
    }

    public static UGUIClickHandler Get(Transform tran)
    {
        if (tran == null)
        {
            Logger.Error("UGUI Click Handler Get Obj null");
            return null;
        }
        return Get(tran.gameObject);
    }
}
