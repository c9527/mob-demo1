﻿public class PathHelper
{
    static public string GetBattleSceneName(int iSceneID)
    {
        return "map_" + iSceneID;
    }

    static public string GetBattleScenePath(int iSceneID)
    {
        return "map_" + iSceneID + "/map_" + iSceneID; 
    }

    static public string GetEmptyScenePath()
    {
        return SceneConst.SCENE_EMPTY;
    }

    static public string GetScenePrefabPath(int iSceneID)
    {
        string strMap = string.Concat("map_", iSceneID);

        return string.Concat("Scenes/", strMap, "/", strMap, "_prefab");
    }

    static public string GetLightMapPath(int iSceneID)
    {
        string strMap = string.Concat("map_", iSceneID);

        return string.Concat("Scenes/", strMap, "/", strMap, "_lightmap");
    }

    static public string GetLightProbePath(int iSceneID, string lightProbe)
    {
        string strMap = string.Concat("map_", iSceneID);

        return string.Concat("Scenes/", strMap, "/", lightProbe);
    }

    static public string GetRolePrefabPath(string strRoleName)
    {
        return "Roles/" + strRoleName;
    }

    static public string GetDmmPrefabPath(string strDmmName)
    {
        return "SceneItem/" + strDmmName;
    }

    static public string GetRoleAnimatorControllerPath(string strAniCtrl)
    {
        return "Roles/AnimatorController/" + strAniCtrl;
    }

    static public string GetWeaponPrefabPath(string strPrefab)
    {
        return "Weapons/" + strPrefab;
    }

    static public string GetWeaponAni(string strAni)
    {
        return "Weapons/WeaponsAnimation/weapon@" + strAni;
    }

    static public string GetEffectPrefabPath(string id)
    {
        return "Effect/" + id;
    }

    static public string GetAudioClipPath(string strName)
    {
        return "Sounds/" + strName;
    }

    static public string GetCameraAni(string strAniName)
    {
        return "CameraAni/" + strAniName;
    }

    static public string GetWidgetPath(string strPrefab)
    {
        return "Roles/Decoration/" + strPrefab;
    }
}
