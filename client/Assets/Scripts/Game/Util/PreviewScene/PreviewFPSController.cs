﻿using UnityEngine;

/// <summary>
/// FPS场景预览rigidbody控制器
/// </summary>
/// 
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PreviewFPSController : MonoBehaviour
{
    static private float GRAVITY_RAY_DISTANCE = 50;
    static private float PLAYER_GROUNDED_OFFSET = 0.04f;

    private CapsuleCollider m_collider;
    public float personHeight = 1.57F;
    public float speed = 5.0f;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 1.0f;
    public Vector3 bornVector = new Vector3(1, 5, 1);

    private float distToGround;
    private bool grounded = false;
    private bool isOnTerrain = true;

    private Vector3 origiPos;
    private Vector3 origiUp;
    private Vector3 origiRight;

    void OnGUI()
    {
        if (GUILayout.Button("重置", GUILayout.Height(40)))
        {
            transform.position = origiPos;
            transform.up = origiUp;
            transform.right = origiRight;
        }
    }

    void Awake()
    {
        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;
    }

    void FixedUpdate()
    {
        if (grounded)
        {
            Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            targetVelocity = transform.TransformDirection(targetVelocity);
            targetVelocity *= speed;

            Vector3 velocity = rigidbody.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);

            if (canJump && Input.GetButton("Jump"))
            {
                rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }
        }

        rigidbody.AddForce(new Vector3(0, -gravity * rigidbody.mass, 0));

        //if (!isTerrainDetect())
        grounded = false;
    }

    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Screen.lockCursor = false;

        //if (Input.GetMouseButton(0))
        //    Screen.lockCursor = true;

        //操作体验优化
        if (Input.GetKeyDown(KeyCode.F1))
            Screen.lockCursor = !Screen.lockCursor;

        //if (transform.position.y < 0)
        //    transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    void Start()
    {
        m_collider = GetComponent<CapsuleCollider>();
        m_collider.center = new Vector3(0, 0, 0);
        m_collider.height = personHeight;
        transform.position = bornVector; //默认点

        origiPos = transform.position;
        origiUp = transform.up;
        origiRight = transform.right;
        Screen.lockCursor = true;
    }
    private bool IsGrounded()
    {
        //if(GameSetting.enableGravityRay == false)
        //{
        //    return true;
        //}

        RaycastHit kRH;
        //float radius = this is MainPlayer ? GameSetting.MPColliderRadius : GameSetting.OPColliderRadius;
        float radius = m_collider.radius;
        Vector3 oriPos = transform.position + Vector3.up * radius;

        if (Physics.SphereCast(oriPos, radius, Vector3.down, out kRH, GRAVITY_RAY_DISTANCE, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {

            if (kRH.distance <= PLAYER_GROUNDED_OFFSET)
                return true;
        }
        return false;
    }
    private bool isTerrainDetect()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit rayHit;
        if (Physics.Raycast(ray, out rayHit, 50))
        {
            //Vector3 oldVec = currentCamera.transform.position;
            //currentCamera.transform.position = new Vector3(oldVec.x, rayHit.point.y + personHeight, oldVec.z);
            if (rayHit.distance <= PLAYER_GROUNDED_OFFSET)
                return true;
        }
        return false;
    }
    //private bool isTerrainDetect()
    //{
    //    Ray ray = new Ray(transform.position, Vector3.down);
    //    RaycastHit rayHit;
    //    if (Physics.Raycast(ray, out rayHit, 10))
    //    {
    //        Vector3 oldVec = currentCamera.transform.position;
    //        currentCamera.transform.position = new Vector3(oldVec.x, rayHit.point.y + personHeight, oldVec.z);

    //    }
    //}
}