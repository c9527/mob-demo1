﻿using System.IO;
using UnityEngine;

/// <summary>
/// 预览场景需要的模型UI相关
/// author: bavol
/// </summary>
public class PreviewSceneUI : MonoBehaviour
{
    static public PreviewSceneUI singleton;

    public string[] allRoles;

    private GameObject m_go;
    private string m_roleModel;
    private Animator m_animator;
    private int m_kWeaponId;
    private GameObject m_kWeapon;
    private GameObject m_kWeaponLeft;
    private Transform m_transLeftHandSlot;
    private Transform m_transRightHandSlot;

    private FPSAnimation m_kAni;

    private Transform m_transBipRoot;

    /// <summary>
    /// 简单显示准星
    /// </summary>
    void OnGUI()
    {
        var centerStyle = GUI.skin.GetStyle("Label");
        centerStyle.alignment = TextAnchor.UpperCenter;
        centerStyle.fontSize = 30;
        centerStyle.normal.textColor = Color.green;
        Rect rec = new Rect(Screen.width / 2 - 15, Screen.height / 2 - 15, 30, 30);
        GUI.Label(rec, "十", centerStyle);
    }

    void Awake()
    {
        if (singleton != null)
           Logger.Error("PreviewScene is singleton!");

        singleton = this;

        m_go = null;
        m_animator = null;
        m_kWeapon = null;
        m_roleModel = "";
        m_kWeaponId = -1;
        m_kAni = new FPSAnimation();

        Init();
    }

    public void Init()
    {
        //Logger.Log("PreviewScene, Init");

        //InitAllRole();
        //LoadRoleRes(allRoles[0]);
        LoadRoleRes("arm");
        SwitchWeapon("m4a1");
    }
    void InitAllRole()
    {
#if !UNITY_WEBPLAYER
        DirectoryInfo kDI = new DirectoryInfo("Assets/Resources/Roles/");
        FileInfo[] arrFI = kDI.GetFiles("*.prefab", SearchOption.TopDirectoryOnly);
        allRoles = new string[arrFI.Length];
        for (int i = 0; i < arrFI.Length; ++i)
            allRoles[i] = arrFI[i].Name.Substring(0, arrFI[i].Name.IndexOf('.'));
#endif
    }

    void LoadRoleRes(string roleModel)
    {
        if (string.IsNullOrEmpty(roleModel) || m_roleModel == roleModel)
            return;

        m_roleModel = roleModel;

        Object go = Resources.Load(PathHelper.GetRolePrefabPath(roleModel)) as GameObject;
        if (go == null)
        {
           Logger.Error("Player model load failed, path: " + roleModel);
            return;
        }

        if (m_go != null)
        {
            DestroyImmediate(m_go);
        }

        m_go = GameObject.Instantiate(go) as GameObject;

        //查找左右手挂载点
        Transform transBipRightHandCenter = GameObjectHelper.FindChildByTraverse(m_go.transform, ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
        if (transBipRightHandCenter == null)
           Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
        else
        {
            m_transRightHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipRightHandCenter, ModelConst.PLAYER_SLOT_RIGHT_HAND).transform;
            m_transRightHandSlot.ResetLocal();
        }

        Transform transBipLeftHandCenter = GameObjectHelper.FindChildByTraverse(m_go.transform, ModelConst.BIP_NAME_LEFT_HAND_CENTER);
        if (transBipLeftHandCenter == null)
           Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_LEFT_HAND_CENTER);
        else
        {
            m_transLeftHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipLeftHandCenter, ModelConst.PLAYER_SLOT_LEFT_HAND).transform;
            m_transLeftHandSlot.ResetLocal();
        }

        m_go.transform.localPosition = Vector3.zero;
        m_go.transform.localEulerAngles = Vector3.zero;
        //m_go.transform.parent = camera.transform;
        //m_go.transform.rotation = camera.transform.rotation;
        Vector3 mgoVec = new Vector3(500, -650, (camera.nearClipPlane + 0.094f));
        m_go.transform.position = camera.ScreenToWorldPoint(mgoVec);

        m_go.transform.parent = camera.transform;
        //Vector3 mgoLocalPostion = m_go.transform.localPosition;
        Vector3 mgoLocalPostion = new Vector3(0.08f, -1.67f, 0.5f);
        Vector3 mgoRotation = new Vector3(354.250f, 354.01f, 359.398f);

        m_go.transform.localPosition = mgoLocalPostion;
        m_go.transform.rotation = Quaternion.Euler(mgoRotation);

        //m_go.transform.position = camera.ScreenToWorldPoint(new Vector3(0, 0, 0));
        m_animator = m_go.GetComponent<Animator>();
    }

    void SwitchWeapon(string weaponStr)
    {
        Object go = Resources.Load(PathHelper.GetWeaponPrefabPath(weaponStr)) as GameObject;
        if (go == null)
            return;

        m_kWeapon = GameObject.Instantiate(go) as GameObject;

        m_transBipRoot = GameObjectHelper.FindChildByTraverse(m_kWeapon.transform, ModelConst.WEAPON_BIP_ROOT);

        Animation ani = GameObjectHelper.GetComponent<Animation>(m_kWeapon);
        m_kAni.SetAnimation(ani);

        if (m_kWeaponLeft != null)
            DestroyImmediate(m_kWeaponLeft);

        AttachToPlayer();
    }

    virtual public void AttachToPlayer()
    {
        Transform transWeapon = m_kWeapon.transform;
        Transform transSlot = null;
        transSlot = m_transRightHandSlot;
        SlotHand(transWeapon, m_transBipRoot, transSlot, 0);
    }

    private void SlotHand(Transform tranWeapon, Transform transRoot, Transform transSlot, int index)
    {
        if (tranWeapon.parent == transSlot)
            return;

        Vector3 vec3Pos = Vector3.zero;
        Vector3 vec3Rotation = Vector3.zero;

        vec3Pos = new Vector3(-0.001f, -0.019f, 0.010f);
        vec3Rotation = new Vector3(356.645f, 0.109f, 266.279f);

        tranWeapon.parent = transSlot;

        if (transRoot != null)
            transRoot.ResetLocal();

        tranWeapon.localPosition = vec3Pos;
        tranWeapon.localEulerAngles = vec3Rotation;
        //m_go.transform.localPosition = Vector3.zero;
        //m_go.transform.localEulerAngles = Vector3.zero;
        //m_go.transform.position = camera.ScreenToWorldPoint(new Vector3(400, -600, (camera.nearClipPlane + 0.07f)));
    }

    void Start()
    {
        //Logger.Log("PreviewSceneUI, start");
    }

    void Update()
    {

    }
}
