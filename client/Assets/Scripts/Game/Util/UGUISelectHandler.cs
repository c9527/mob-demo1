﻿using UnityEngine;
using UnityEngine.EventSystems;

class UGUISelectHandler: MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public delegate void BaseEvtCallBackFunc(GameObject target, BaseEventData eventData);

    public event BaseEvtCallBackFunc onSelect;
    public event BaseEvtCallBackFunc onDeselect;

    public void OnSelect(BaseEventData eventData)
    {
        if (onSelect != null)
            onSelect(gameObject, eventData);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if (onDeselect != null)
            onDeselect(gameObject, eventData);
    }

    public static UGUISelectHandler Get(GameObject go)
    {
        UGUISelectHandler listener = go.GetComponent<UGUISelectHandler>();
        if (listener == null)
            listener = go.AddComponent<UGUISelectHandler>();
        return listener;
    }

    public static UGUISelectHandler Get(Transform tran)
    {
        return Get(tran.gameObject);
    }


    
}
