﻿using UnityEngine;
using System.Collections;

public class SpringEvent : MonoBehaviour
{
    public float jumpHeight;
    public bool forceMove;
    public Vector3 moveDir;
    public float forceSpeed;

    public void Init(string effect, float diameter, float effectOffSet, float jumpHeight, bool forceMove, Vector3 moveDir, float forceSpeed)
    {
        this.jumpHeight = jumpHeight;
        this.forceMove = forceMove;
        this.moveDir = moveDir;
        this.forceSpeed = forceSpeed;
        string strPrefabPath = PathHelper.GetEffectPrefabPath(effect);
        ResourceManager.LoadBattlePrefab(strPrefabPath, (go, crc) =>
        {
            if (go == null)
            {
                Logger.Error("Effect gameObject is null! name:" + effect);
                return;
            }
            Transform trans = go.transform;
            trans.SetParent(transform.childCount == 0 ? transform : transform.GetChild(0));
            trans.localEulerAngles = new Vector3(90f, 0 , 0);
            trans.localPosition = new Vector3(0, effectOffSet, 0);
            trans.localScale = new Vector3(diameter / 5, trans.localScale.y, diameter / 5);
        });
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.IsInLayerMask( GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER ) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<bool, float, bool, Vector3, float>(GameEvent.MAINPLAYER_BOUNCE, true, jumpHeight, forceMove, moveDir, forceSpeed);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<bool, float, bool, Vector3, float>(GameEvent.MAINPLAYER_BOUNCE, false, 0, false, Vector3.zero, 0);
        }
    }
}
