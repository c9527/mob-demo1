﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerPosTweener : MonoBehaviour
{

    private Transform m_trans;

    private Action m_onFinished;
    private Vector3 m_targetPos;

    private Vector3 m_moveDir;
    private float m_moveSpeed;
    private float m_targetDis;

    private float m_moveDis;
    private bool m_finished = true;

    private bool m_isPauseMove = false;
    private int m_pauseStopTime = -1;

    public void TweenPos(float tweenTime, Vector3 targetPos)
    {
        if (tweenTime < 0)
            Logger.Error("tweenTime < 0");

        if (tweenTime <= 0 || m_trans.position == targetPos)
        {
            m_finished = true;
            m_trans.position = targetPos;
            if (m_onFinished != null)
                m_onFinished();

            return;
        }

        m_finished = false;
        m_moveDis = 0;

        m_targetPos = targetPos;
        m_targetDis = Vector3.Distance(m_trans.position, targetPos);
        m_moveSpeed = m_targetDis / tweenTime;
        m_moveDir = Vector3.Normalize(targetPos - m_trans.position);
    }

    public void AddOnFinished(Action onFinish)
    {
        m_onFinished = onFinish;
    }

    public void ClientStop(float time)
    {
        m_isPauseMove = true;
        if (-1 != m_pauseStopTime)
            TimerManager.RemoveTimeOut(m_pauseStopTime);
        m_pauseStopTime = TimerManager.SetTimeOut(time, () =>
        {
            TimerManager.RemoveTimeOut(m_pauseStopTime);
            m_pauseStopTime = -1;
            m_isPauseMove = false;
        });
    }

    void Awake()
    {
        m_trans = transform;
    }

    private void OnEnable()
    {
        m_trans = transform;
        m_finished = true;
        m_isPauseMove = false;
    }

    private void OnDisable()
    {
        m_targetPos = Vector3.zero;
        m_moveDir = Vector3.zero;
        m_moveSpeed = 0;
        m_targetDis = 0;
        m_moveDis = 0;

        m_finished = true;
        m_isPauseMove = false;
        if (-1 != m_pauseStopTime)
            TimerManager.RemoveTimeOut(m_pauseStopTime);
    }

    void Update()
    {
        if (m_isPauseMove)
            return;

        if (m_finished)
            return;

        float deltaMove = Time.smoothDeltaTime * m_moveSpeed;
        m_moveDis += deltaMove;

        if (m_moveDis >= m_targetDis)
        {
            m_finished = true;
            m_trans.position = m_targetPos;
            if (m_onFinished != null)
                m_onFinished();
            return;
        }

        m_trans.position += m_moveDir * deltaMove;
    }

}
