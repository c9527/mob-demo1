﻿using UnityEngine;
using System.Collections;
using System;

public class RotTweener : MonoBehaviour
{

    private Transform m_trans;
    private Transform m_target;

    private Action m_onFinished;

    private float m_moveSpeedX;
    private float m_moveSpeedY;
    private float m_moveSpeedZ;
    private float m_targetEulerX;
    private float m_targetEulerY;
    private float m_targetEulerZ;

    private float m_moveAngleX;
    private float m_moveAngleY;
    private float m_moveAngleZ;

    private bool m_finishedX = true;
    private bool m_finishedY = true;
    private bool m_finishedZ = true;

    private float m_tweenTime;
    private float m_tick;
    private const float m_interval = 1f;


    public void TweenRot(float tweenTime, Transform target)
    {
        if (tweenTime == 0)
        {
            m_trans.eulerAngles = target.eulerAngles;

            m_target = null;
            m_tweenTime = 0;

            m_finishedX = true;
            m_finishedY = true;
            m_finishedZ = true;

            if (m_onFinished != null)
                m_onFinished();

            return;
        }
        m_tweenTime = tweenTime;
        m_target = target;
        TweenRot(tweenTime, m_target.eulerAngles);
    }


    public void TweenRot(float tweenTime, Vector3 targetEuler)
    {
        if (tweenTime == 0)
        {
            m_trans.eulerAngles = targetEuler;

            m_finishedX = true;
            m_finishedY = true;
            m_finishedZ = true;

            if (m_onFinished != null)
                m_onFinished();

            return;
        }

        m_finishedX = false;
        m_finishedY = false;
        m_finishedZ = false; 
        
        m_moveAngleX = 0;
        m_moveAngleY = 0;
        m_moveAngleZ = 0;

        m_targetEulerX = CaculateAngle(targetEuler.x, m_trans.eulerAngles.x);
        m_targetEulerY = CaculateAngle(targetEuler.y, m_trans.eulerAngles.y);
        m_targetEulerZ = CaculateAngle(targetEuler.z, m_trans.eulerAngles.z);

        m_moveSpeedX = m_targetEulerX / tweenTime;
        m_moveSpeedY = m_targetEulerY / tweenTime;
        m_moveSpeedZ = m_targetEulerZ / tweenTime;
    }

    private float CaculateAngle(float targetEuler, float orginEuler)
    {
        float delta = targetEuler - orginEuler;

        if (delta > 180) delta -= 360;
        if (delta < -180) delta += 360; //如果上一步又超过180，则不进行反转
        return delta;
        //return delta < 180 ? delta : -(360 - delta);
    }

    public void AddOnFinished(Action onFinish)
    {
        m_onFinished = onFinish;
    }

    void Awake()
    {
        m_trans = transform;
    }

    private void OnEnable()
    {
        m_trans = transform;
        m_finishedX = false;
        m_finishedY = false;
        m_finishedZ = false;
    }

    private void OnDisable()
    {
        m_moveSpeedX = 0;
        m_moveSpeedY = 0;
        m_moveSpeedZ = 0;

        m_targetEulerX = 0;
        m_targetEulerY = 0;
        m_targetEulerZ = 0;

        m_moveAngleX = 0;
        m_moveAngleY = 0;
        m_moveAngleZ = 0;

        m_finishedX = true;
        m_finishedY = true;
        m_finishedZ = true;
    }

    void Update()
    {
        if(m_target != null && (m_tick += Time.smoothDeltaTime) >= m_interval)
        {
            m_tweenTime -= m_tick;
            m_tick = 0;
            if (m_tweenTime < 0)
                m_tweenTime = 0;
            TweenRot(m_tweenTime, m_target);
            return;
        }

        if (m_finishedX && m_finishedY && m_finishedZ)
            return;

        float deltaMoveX = 0;
        float deltaMoveY = 0;
        float deltaMoveZ = 0;

        if (!m_finishedX)
        {
            deltaMoveX = Time.smoothDeltaTime * m_moveSpeedX;
            m_moveAngleX += deltaMoveX;

            if (Mathf.Abs(m_moveAngleX) >= Mathf.Abs(m_targetEulerX))
            {
                deltaMoveX = deltaMoveX - (m_moveAngleX - m_targetEulerX);
                m_finishedX = true;
            }
        }

        if (!m_finishedY)
        {
            deltaMoveY = Time.smoothDeltaTime * m_moveSpeedY;
            m_moveAngleY += deltaMoveY;

            if (Mathf.Abs(m_moveAngleY) >= Mathf.Abs(m_targetEulerY))
            {
                deltaMoveY = deltaMoveY - (m_moveAngleY - m_targetEulerY);
                m_finishedY = true;
            }
        }

        if (!m_finishedZ)
        {
            deltaMoveZ = Time.smoothDeltaTime * m_moveSpeedZ;
            m_moveAngleZ += deltaMoveZ;

            if (Mathf.Abs(m_moveAngleZ) >= Mathf.Abs(m_targetEulerZ))
            {
                deltaMoveZ = deltaMoveZ - (m_moveAngleZ - m_targetEulerZ);
                m_finishedZ = true;
            }
        }

        Vector3 eulerAngle = m_trans.eulerAngles + new Vector3(deltaMoveX, deltaMoveY, deltaMoveZ);
        m_trans.eulerAngles = eulerAngle;

        if (m_finishedX && m_finishedY && m_finishedZ && m_onFinished != null)
        {
            m_onFinished();
        }
    }

    public Action onFinished
    {
        get
        {
            return m_onFinished;
        }
    }
}
