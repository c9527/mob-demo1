﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

[Obsolete("请使用更细分的事件监听类", true)]
class UGUIAllHandler: MonoBehaviour, 
    IPointerEnterHandler, 
    IPointerExitHandler, 
    IPointerDownHandler, 
    IPointerUpHandler, 
    IPointerClickHandler,
    IBeginDragHandler,
    IDragHandler,
    IEndDragHandler,
    IDropHandler,
    IScrollHandler,
    IUpdateSelectedHandler,
    IDeselectHandler,
    IMoveHandler,
    ISubmitHandler,
    ICancelHandler,
    ISelectHandler
{
    public delegate void PointerEvetCallBackFunc(GameObject target, PointerEventData eventData);
    public delegate void BaseEvtCallBackFunc(GameObject target, BaseEventData eventData);
    public delegate void AxisEvtCallBackFunc(GameObject target, AxisEventData eventData);

    public event PointerEvetCallBackFunc onPointerEnter;
    public event PointerEvetCallBackFunc onPointerExit;
    public event PointerEvetCallBackFunc onPointerDown;
    public event PointerEvetCallBackFunc onPointerUp;
    public event PointerEvetCallBackFunc onPointerClick;
    public event PointerEvetCallBackFunc onBeginDrag;
    public event PointerEvetCallBackFunc onDrag;
    public event PointerEvetCallBackFunc onEndDrag;
    public event PointerEvetCallBackFunc onDrop;
    public event PointerEvetCallBackFunc onScroll;
    public event BaseEvtCallBackFunc onUpdateSelected;
    public event BaseEvtCallBackFunc onDeselect;
    public event AxisEvtCallBackFunc onMove;
    public event BaseEvtCallBackFunc onSubmit;
    public event BaseEvtCallBackFunc onCancel;
    public event BaseEvtCallBackFunc onSelect;

    public void OnPointerEnter(PointerEventData eventData)  { if (onPointerEnter != null) onPointerEnter(gameObject, eventData); }
    public void OnPointerExit(PointerEventData eventData)   { if (onPointerExit != null) onPointerExit(gameObject, eventData); }
    public void OnPointerDown(PointerEventData eventData)   { if (onPointerDown != null) onPointerDown(gameObject, eventData); }
    public void OnPointerUp(PointerEventData eventData)     { if (onPointerUp != null) onPointerUp(gameObject, eventData);}
    public void OnPointerClick(PointerEventData eventData)  { if (onPointerClick != null) onPointerClick(gameObject, eventData); else if (onPointerDown == null && onPointerUp == null) DefaultClickAction(); }
    public void OnBeginDrag(PointerEventData eventData)     { if (onBeginDrag != null) onBeginDrag(gameObject, eventData); }
    public void OnDrag(PointerEventData eventData)          { if (onDrag != null) onDrag(gameObject, eventData); }
    public void OnEndDrag(PointerEventData eventData)       { if (onEndDrag != null) onEndDrag(gameObject, eventData); }
    public void OnDrop(PointerEventData eventData)          { if (onDrop != null) onDrop(gameObject, eventData); }
    public void OnScroll(PointerEventData eventData)        { if (onScroll != null) onScroll(gameObject, eventData); }
    public void OnUpdateSelected(BaseEventData eventData)   { if (onUpdateSelected != null) onUpdateSelected(gameObject, eventData); }
    public void OnDeselect(BaseEventData eventData)         { if (onDeselect != null) onDeselect(gameObject, eventData); }
    public void OnMove(AxisEventData eventData)             { if (onMove != null) onMove(gameObject, eventData); }
    public void OnSubmit(BaseEventData eventData)           { if (onSubmit != null) onSubmit(gameObject, eventData); }
    public void OnCancel(BaseEventData eventData)           { if (onCancel != null) onCancel(gameObject, eventData); }
    public void OnSelect(BaseEventData eventData)           { if (onSelect != null) onSelect(gameObject, eventData); }

    public static UGUIAllHandler Get(GameObject go)
    {
        UGUIAllHandler listener = go.GetComponent<UGUIAllHandler>();
        if (listener == null) listener = go.AddComponent<UGUIAllHandler>();
        return listener;
    }

    public static UGUIAllHandler Get(Transform tran)
    {
        return Get(tran.gameObject);
    }

    private void DefaultClickAction()
    {
        UIManager.ShowTipPanel("系统暂未开放");
    }

    
}
