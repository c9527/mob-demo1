﻿using UnityEngine;
using System.Collections;

public class LadderEvent : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.IsInLayerMask( GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER ) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<bool, GameObject>(GameEvent.MAINPLAYER_CLIMB, true, gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.IsInLayerMask(GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<bool, GameObject>(GameEvent.MAINPLAYER_CLIMB, false, gameObject);
        }
    }
}
