﻿using System;
using System.Collections.Generic;

class EaseMethod
{
    static public float UpdateByEaseOut(float t, float d, float c, float b, float s = 3.14f)
    {
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t">已缓动时间</param>
    /// <param name="d">缓动总时间</param>
    /// <param name="c">缓动变化值</param>
    /// <param name="b">缓动初始值</param>
    /// <returns></returns>
    static public float UpdateByEase(float t, float d, float c, float b)
    {
        return c * t / d + b;
    }
}
