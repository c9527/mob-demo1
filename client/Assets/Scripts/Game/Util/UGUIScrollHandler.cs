﻿using UnityEngine;
using UnityEngine.EventSystems;

class UGUIScrollHandler : MonoBehaviour, IScrollHandler
{
    public delegate void PointerEvetCallBackFunc(GameObject target, PointerEventData eventData);
    public event PointerEvetCallBackFunc onScroll;

    public void OnScroll(PointerEventData eventData)
    {
        if (onScroll != null)
            onScroll(gameObject, eventData);
    }


    public void RemoveAllHandler()
    {
        onScroll = null;
        DestroyImmediate(this);
    }

    public static UGUIScrollHandler Get(GameObject go)
    {
        UGUIScrollHandler listener = go.GetComponent<UGUIScrollHandler>();
        if (listener == null)
            listener = go.AddComponent<UGUIScrollHandler>();
        return listener;
    }

    public static UGUIScrollHandler Get(Transform tran)
    {
        return Get(tran.gameObject);
    }


}
