﻿
using System.Collections.Generic;

public class BaseMsgProxy 
{

    public delegate void MsgHandler(CDict data);

    protected Dictionary<string, List<MsgHandler>> m_msgHandlerList;
    public BaseMsgProxy()
    {
        
    }

    public void Init()
    {
        
    }

    public void AddMsgHandler( string key,MsgHandler handler )
    {
        if( m_msgHandlerList == null )
        {
            m_msgHandlerList = new Dictionary<string, List<MsgHandler>>();
        }

        if( !m_msgHandlerList.ContainsKey( key ) )
        {
            List<MsgHandler> handlerList = new List<MsgHandler>();
            m_msgHandlerList[key] = handlerList;
        }

        List<MsgHandler> list = m_msgHandlerList[key];

        if ( !list.Contains( handler ) )
            list.Add(handler);
        
    }

    public void RemoveMsgHandler( string key,MsgHandler handler )
    {
        if( m_msgHandlerList == null )
        {
            return;
        }

        if( m_msgHandlerList.ContainsKey( key ) )
        {
            List<MsgHandler> handlerList = m_msgHandlerList[key];
            if( handlerList.Contains( handler ) )
            {
                handlerList.Remove(handler);
            }
        }

    }

    public void Dispatch( string key,CDict data )
    {
        if (m_msgHandlerList == null)
            return;

        if( m_msgHandlerList.ContainsKey( key ) )
        {
            List<MsgHandler> handlerList = m_msgHandlerList[key];

            for( int index = 0; index < handlerList.Count; ++ index )
            {
                handlerList[index](data);
            }
        }
    }
}
