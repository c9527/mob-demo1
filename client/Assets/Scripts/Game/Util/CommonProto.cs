﻿using System;
using System.Collections;
using System.Reflection;

static class CommonProto
{
    static Assembly[] msAllAssembly = new Assembly[]
    {
        typeof(UnityEngine.GameObject).Assembly,
        typeof(UnityEngine.UI.Text).Assembly,
        typeof(Driver).Assembly,
    };

    static object GetValue(object obj)
    {
        if (obj is string)
            return obj as string;
        Type typ = obj.GetType();
        if (typ.IsValueType)
            return obj;
        object[] ary = obj as object[];
        if (ary == null)
            throw new Exception("error value type: " + typ.Name);
        object value = null;
        object parent = null;
        foreach (object item in ary)
        {
            if (item is string)
                value = GetSubValue(value, item as string, out parent);
            else
                value = CallFunc(parent, value, item);
        }
        return value;
    }

    static object GetSubValue(object value, string name, out object parent)
    {
        if (value == null)
        {
            parent = null;
            foreach (Assembly ass in msAllAssembly)
            {
                value = ass.GetType(name);
                if (value != null)
                    break;
            }
            if (value == null)
                throw new Exception("type not found: " + name);
            return value;
        }

        Type typ = value as Type;
        BindingFlags bf = BindingFlags.Public | BindingFlags.NonPublic;
        if (typ == null)
        {
            typ = value.GetType();
            bf |= BindingFlags.Instance;
            parent = value;
        }
        else
        {
            bf |= BindingFlags.Static;
            parent = null;
        }

        string[] ns = name.Split(':');
        int i = 0;
        if (ns.Length == 2)
        {
            i = int.Parse(ns[1]);
            name = ns[0];
        }
        MemberInfo[] mis = typ.GetMember(name, bf);
        if (mis.Length <= 0)
            throw new Exception(typ.Name + "." + name + " not found!");
        if ((mis.Length > 1 && i <= 0) || i > mis.Length)
            throw new Exception(typ.Name + "." + name + " count: " + mis.Length);
        if (i > 0)
            i--;
        MemberInfo mi = mis[i];
        value = typ.GetMethod(name, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        if (mi is MethodInfo)
            return mi as MethodInfo;
        if (mi is PropertyInfo)
            return (mi as PropertyInfo).GetValue(parent, null);
        if (mi is FieldInfo)
            return (mi as FieldInfo).GetValue(parent);
        throw new Exception("unsupport member type: " + mi.GetType().Name);
    }

    static object CallFunc(object parent, object value, object item)
    {
        MethodInfo mi = value as MethodInfo;
        if (mi == null)
            throw new Exception("error method type: " + value.GetType().Name);
        object[] args = item as object[];
        if (args == null)
            throw new Exception("error args type: " + item.GetType().Name);
        object[] ps = new object[args.Length];
        for (int i = 0; i < ps.Length; i++)
            ps[i] = GetValue(args[i]);
        return mi.Invoke(parent, ps);
    }

    static void Toc_player_getdata(toc_player_getdata proto)
    {
        CDict ret = new CDict();
        try
        {
            object value = GetValue(proto.req["cmd"]);
            CJsonHelper.Save(value);    // 测试是否能够序列化
            ret["ok"] = true;
            ret["val"] = value;
        }
        catch (Exception e)
        {
            ret["ok"] = false;
            ret["err"] = e.Message;
        }
        ret["sid"] = proto.req["sid"];
        ret["ver"] = Global.currentVersion;

        NetLayer.Send(new tos_player_getdata() { ret = ret });
    }
}
