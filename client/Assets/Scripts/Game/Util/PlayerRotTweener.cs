﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerRotTweener : MonoBehaviour
{

    private Transform m_trans;

    private Action m_onFinished;

    private float m_moveSpeed;
    private float m_targetEulerY;

    private float m_moveAngle;
    private bool m_finished = true;


    public void TweenRot(float tweenTime, float targetEulerY)
    {
        if (tweenTime == 0)
        {
            Vector3 eulerAngle = m_trans.eulerAngles;
            eulerAngle.y = targetEulerY;
            m_trans.eulerAngles = eulerAngle;

            m_finished = true;

            if (m_onFinished != null)
                m_onFinished();

            return;
        }

        m_finished = false;
        m_moveAngle = 0;

        m_targetEulerY = CaculateAngle(targetEulerY);
        m_moveSpeed = m_targetEulerY / tweenTime;
    }

    private float CaculateAngle(float targetEulerY)
    {
        float delta = targetEulerY - m_trans.eulerAngles.y;

        if (delta > 180) delta -= 360;
        if (delta < -180) delta += 360; //如果上一步又超过180，则不进行反转
        return delta;
        //return delta < 180 ? delta : -(360 - delta);
    }

    public void AddOnFinished(Action onFinish)
    {
        m_onFinished = onFinish;
    }

    void Awake()
    {
        m_trans = transform;
    }

    private void OnEnable()
    {
        m_trans = transform;
        m_finished = false;
    }

    private void OnDisable()
    {
        m_moveSpeed = 0;
        m_targetEulerY = 0;
        m_moveAngle = 0;

        m_finished = true;
    }

    void Update()
    {
        if (m_finished)
            return;

        float deltaMove = Time.smoothDeltaTime * m_moveSpeed;
        m_moveAngle += deltaMove;

        if(Mathf.Abs(m_moveAngle) >= Mathf.Abs(m_targetEulerY))
        {
            deltaMove = deltaMove - (m_moveAngle - m_targetEulerY);
            m_finished = true;
        }

        Vector3 eulerAngle = m_trans.eulerAngles;
        eulerAngle.y += deltaMove;
        m_trans.eulerAngles = eulerAngle;

        if (m_finished && m_onFinished != null)
            m_onFinished();
            
    }

}
