﻿using UnityEngine;
using UnityEngine.EventSystems;

class UGUIUpDownHandler: MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public delegate void PointerEvetCallBackFunc(GameObject target, PointerEventData eventData);

    public event PointerEvetCallBackFunc onPointerDown;
    public event PointerEvetCallBackFunc onPointerUp;

    public void OnPointerDown(PointerEventData eventData)   { if (onPointerDown != null) onPointerDown(gameObject, eventData); }
    public void OnPointerUp(PointerEventData eventData)     { if (onPointerUp != null) onPointerUp(gameObject, eventData); }

    public static UGUIUpDownHandler Get(GameObject go)
    {
        var listener = go.GetComponent<UGUIUpDownHandler>();
        if (listener == null) 
            listener = go.AddComponent<UGUIUpDownHandler>();
        return listener;
    }

    public static UGUIUpDownHandler Get(Transform tran)
    {
        return Get(tran.gameObject);
    }
}
