﻿using UnityEngine;
using System.Collections;
using System;

public class PosTweener : MonoBehaviour
{

    private Transform m_trans;

    private Transform m_target;
    private Vector3 m_offset;

    private Action m_onFinished;
    private Vector3 m_targetPos;

    private Vector3 m_moveDir;
    private float m_moveSpeed;
    private float m_targetDis;

    private float m_moveDis;
    private bool m_finished = true;

    private bool m_isPauseMove = false;
    private int m_pauseStopTime = -1;

    private float m_tweenTime;
    private float m_tick;
    private const float m_interval = 0.1f;

    public void TweenPos(float tweenTime, Transform target, Vector3 offset = default(Vector3))
    {
        if (tweenTime == 0)
        {
            m_finished = true;
            m_target = null;
            m_trans.position = target.position + offset;
            m_tweenTime = 0;
            if (m_onFinished != null)
                m_onFinished();

            return;
        }
        m_target = target;
        m_tweenTime = tweenTime;
        m_offset = offset;
        TweenPos(m_tweenTime, m_target.position + offset);
    }

    public void TweenPos(float tweenTime, Vector3 targetPos)
    {
        if (tweenTime == 0)
        {
            m_finished = true;
            m_trans.position = targetPos;
            if (m_onFinished != null)
                m_onFinished();

            return;
        }

        m_finished = false;
        m_moveDis = 0;

        m_targetPos = targetPos;
        m_targetDis = Vector3.Distance(m_trans.position, targetPos);
        m_moveSpeed = m_targetDis / tweenTime;
        m_moveDir = Vector3.Normalize(targetPos - m_trans.position);
    }

    public void AddOnFinished(Action onFinish)
    {
        m_onFinished = onFinish;
    }

    public void ClientStop(float time)
    {
        m_isPauseMove = true;
        if (-1 != m_pauseStopTime)
            TimerManager.RemoveTimeOut(m_pauseStopTime);
        m_pauseStopTime = TimerManager.SetTimeOut(time, () =>
        {
            TimerManager.RemoveTimeOut(m_pauseStopTime);
            m_pauseStopTime = -1;
            m_isPauseMove = false;
        });
    }

    void Awake()
    {
        m_trans = transform;
    }

    private void OnEnable()
    {
        m_trans = transform;
        m_finished = true;
        m_isPauseMove = false;
    }

    private void OnDisable()
    {
        m_targetPos = Vector3.zero;
        m_moveDir = Vector3.zero;
        m_moveSpeed = 0;
        m_targetDis = 0;
        m_moveDis = 0;

        m_target = null;
        m_tweenTime = 0;

        m_finished = true;
        m_isPauseMove = false;
        if (-1 != m_pauseStopTime)
            TimerManager.RemoveTimeOut(m_pauseStopTime);
    }

    void Update()
    {
        if (m_isPauseMove)
            return;

        if (m_finished)
            return;

        if (m_target != null && (m_tick += Time.smoothDeltaTime) >= m_interval)
        {
            m_tweenTime -= m_tick;
            m_tick = 0;
            if (m_tweenTime < 0)
                m_tweenTime = 0;
            TweenPos(m_tweenTime, m_target, m_offset);
            return;
        }

        float deltaMove = Time.smoothDeltaTime * m_moveSpeed;
        m_moveDis += deltaMove;

        if (m_moveDis >= m_targetDis)
        {
            m_finished = true;
            m_trans.position = m_targetPos;
            if (m_onFinished != null)
                m_onFinished();
            return;
        }

        m_trans.position += m_moveDir * deltaMove;
    }

    public Action onFinished
    {
        get
        {
            return m_onFinished;
        }
    }

}
