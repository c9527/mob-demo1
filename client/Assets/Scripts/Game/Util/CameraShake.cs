﻿using System.Collections.Generic;
using UnityEngine;

public class CameraShakeItem
{
    public Camera camera;
    public Vector3 camSrcPos;
    public Transform tranCam;
    public bool isMainCam;
    public bool hasParent;

    public Vector3 localPosition
    {
        get { return tranCam.localPosition; }
        set
        {
            if(SceneCamera.singleton != null && SceneCamera.singleton.camera == camera)
            {
                SceneCamera.singleton.localPosition = value;
            }
            else
            {
                tranCam.localPosition = value;
            }
        }
    }
}

public class CameraShake : MonoBehaviour
{
    
    private float m_shakeTick = 0f;
    private float m_frameTime = 0f;

    public float m_shakeTime = 0f;
    public float m_horizontalLevel = 0;
    public float m_horizontalDecay = 0f;
    public bool m_horizontalRandom;
    public float m_verticalLevel = 0;
    public float m_verticalDecay = 0f;
    public bool m_verticalRandom;
    public float m_shakeFrequency = 0;

    public VtfBoolCls m_isShake = new VtfBoolCls();
    private Dictionary<int, CameraShakeItem> m_cameraShakeDict = new Dictionary<int, CameraShakeItem>();
    private float m_trueHorizontalLevel = 0;
    private float m_trueVerticalLevel = 0;

    private float m_shakeValue = 1;

    public float m_mainCameraLevelScale;
    private bool m_isNeedSrcPos;


    void LateUpdate()
    {
        if (m_isShake.val)
        {
            if(m_shakeTick <= m_shakeTime)
            {
                m_shakeTick += Time.deltaTime;
                if (m_shakeTick > m_shakeTime)
                {
                    StopShake();
                }
                else
                {
                    m_frameTime += Time.deltaTime;
                    if (m_frameTime > m_shakeFrequency)
                    {
                        m_frameTime = 0;
                        Shake();
                    }
                }
            }
        }
    }

    public void Shake()
    {
        m_trueHorizontalLevel = m_horizontalLevel * (1 - m_shakeTick * m_horizontalDecay);
        m_trueHorizontalLevel = m_trueHorizontalLevel < 0 ? 0 : m_trueHorizontalLevel;
        m_trueVerticalLevel = m_verticalLevel * (1 - m_shakeTick * m_verticalDecay);
        m_trueVerticalLevel = m_trueVerticalLevel < 0 ? 0 : m_trueVerticalLevel;
        Vector3 shakeVec = new Vector3((-1f + 2f * (m_horizontalRandom ? Random.value : m_shakeValue)) * m_trueHorizontalLevel, (-1f + 2f * (m_verticalRandom ? Random.value : m_shakeValue)) * m_trueVerticalLevel, 0f);
        m_shakeValue *= -1;
        bool hasParent = false;

        foreach (var item in m_cameraShakeDict.Values)
        {
            hasParent = item.tranCam.parent != null;
            if (item.hasParent ==hasParent)
            {
                if (hasParent && m_isNeedSrcPos)
                {
                    item.localPosition = item.camSrcPos + shakeVec * (item.isMainCam ? m_mainCameraLevelScale : 1f);

                }
                else
                    item.localPosition += shakeVec * (item.isMainCam ? m_mainCameraLevelScale : 1f);
            }
        }
    }

    public void AddCamera(Camera camera, bool isMainCam = false, Transform shakeTrans = null)
    {
        CameraShakeItem shakeItem;
        int id = camera.GetInstanceID();
        if (!m_cameraShakeDict.TryGetValue(id, out shakeItem))
        {
            shakeItem = new CameraShakeItem();
            shakeItem.camera = camera;
            m_cameraShakeDict.Add(id, shakeItem);
        }
        shakeItem.tranCam = shakeTrans != null ? shakeTrans : camera.transform;
        //shakeItem.camSrcPos = shakeItem.localPosition;
        shakeItem.camSrcPos = new Vector3(4399,4399,4399);
        shakeItem.isMainCam = isMainCam;
    }

    public void RemoveCamera(Camera camera)
    {
        if (camera == null)
            return;

        int id = camera.GetInstanceID();
        if (m_cameraShakeDict.ContainsKey(id))
        {
            m_cameraShakeDict.Remove(id);
        }
    }

    /// <summary>
    /// 开始震动
    /// </summary>
    /// <param name="shakeTime"></param>
    /// <param name="shakeLevel"></param>
    /// <param name="isFade"></param>
    /// <param name="decaySpeed">衰减速度百分比</param>
    public void StartShake(float shakeTime, float horizontalLevel, float horizontalDecay, bool horizontalRandom, float verticalLevel, float verticalDecay, bool verticalRandom, float frequency, float mainCameraLevelScale, bool isNeedSrcPos)
    {
        StopShake();

        foreach(CameraShakeItem v in m_cameraShakeDict.Values)
        {
            v.camSrcPos = v.localPosition;
            v.hasParent = (v.tranCam.parent != null);
        }

        m_shakeTime = shakeTime;
        m_horizontalLevel = horizontalLevel;
        m_horizontalDecay = horizontalDecay;
        m_horizontalRandom = horizontalRandom;
        m_verticalLevel = verticalLevel;
        m_verticalDecay = verticalDecay;
        m_verticalRandom = verticalRandom;
        m_shakeFrequency = 1f / frequency;
        m_mainCameraLevelScale = mainCameraLevelScale;
        m_frameTime = float.MaxValue;
        m_isShake.val = true;
        m_isNeedSrcPos = isNeedSrcPos;
    }

    public void StopShake()
    {
        foreach (var item in m_cameraShakeDict.Values)
        {
            if (item.tranCam.parent != null && m_isNeedSrcPos)
            {
                if (item.camSrcPos != new Vector3(4399, 4399, 4399) && item.hasParent == (item.tranCam.parent != null))
                    item.localPosition = item.camSrcPos;
            }
        }
        m_shakeTick = 0f;
        m_isShake.val = false;
    }

    public void ReShake()
    {
        m_shakeTick = 0f;
        m_frameTime = float.MaxValue;
        m_isShake.val = true;
    }

    public bool isShake
    {
        get { return m_isShake.val; }
    }

}