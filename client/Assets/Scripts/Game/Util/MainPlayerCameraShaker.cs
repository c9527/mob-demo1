﻿using UnityEngine;
using System.Collections;

public class MainPlayerCameraShaker : MonoBehaviour
{
    public float _RecoilValue = 0.2f;
    public float _RecoilRecoverSpeed = 3f;

    private Transform _transMainPlayerCamera;
    public bool _bApply = false;


    void Start()
    {

    }

    void OnDisable()
    {
        apply = false;
    }

    public void BindMainPalyerCamera(Transform transCamera)
    {
        _transMainPlayerCamera = transCamera;
        if (_transMainPlayerCamera == null)
        {
            Logger.Log("MainPlayerCameraShaker _transMainPlayerCamera is null");
        }
    }

    public bool apply
    {
        get { return _bApply; }
        set
        {
            _bApply = value;

            if (_bApply == true)
            {
                if (_transMainPlayerCamera != null)
                    _transMainPlayerCamera.localPosition = new Vector3(0f, 0f, _RecoilValue);
            }
            else
            {
                if (_transMainPlayerCamera != null)
                    _transMainPlayerCamera.localPosition = Vector3.zero;
            }
        }
    }

    void Update()
    {
        if (_bApply == true)
        {
            Vector3 vec3LocalPos = _transMainPlayerCamera.localPosition;

            if (vec3LocalPos.z <= 0)
            {
                _transMainPlayerCamera.localPosition = new Vector3(0f, 0f, _RecoilValue);
            }
            else
            {
                _transMainPlayerCamera.localPosition -= new Vector3(0f, 0f, _RecoilRecoverSpeed * Time.deltaTime);
            }

        }

    }
}