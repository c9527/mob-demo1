﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

class EventRegisterHelper
{
    private static EventTrigger.TriggerEvent FindEventDelegate( GameObject go,EventTriggerType eventType )
    {
        EventTrigger evTrigger = go.GetComponent<EventTrigger>();
        if (evTrigger == null)
        {
            evTrigger = go.AddComponent<EventTrigger>();
        }

        if (evTrigger.delegates == null)
        {
            evTrigger.delegates = new List<EventTrigger.Entry>();
        }

        /*
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;

        evTrigger.delegates.Add(entry);
        */


        EventTrigger.Entry entry = null;
        foreach( EventTrigger.Entry evEntry in evTrigger.delegates )
        {
            if ( evEntry.eventID == eventType )
            {
                entry = evEntry;
                break;
            }
        }
             

        if (entry == null)
        {
            entry = new EventTrigger.Entry();
            entry.eventID = eventType;

            evTrigger.delegates.Add(entry);
        }
             

        return entry.callback;
    }
    public static void RegisterEventHandlerToObj(GameObject go, EventTriggerType eventType, UnityAction<BaseEventData> handler)
    {

        EventTrigger.TriggerEvent callback = FindEventDelegate(go, eventType);
        callback.AddListener( handler  );
           
    }

    public static void RemoveEventHandlerFromObj(  GameObject go,EventTriggerType eventType, UnityAction<BaseEventData> handler )
    {
        EventTrigger.TriggerEvent callback = FindEventDelegate(go, eventType);
        callback.RemoveListener(handler);
        return;
    }

}
