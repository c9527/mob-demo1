﻿using System;
using UnityEngine;

public class BinaryRotateAssistor : MonoBehaviour
{
    private readonly Rect AssistorRect = new Rect(5f, 40f, (float) (Screen.width), (float) Screen.height);
    private static float handspeed;
    private float HighRatio;
    private float HighSpeed;
    private float HighTime;
    private float LowRatio;
    private float LowSpeed;
    private string sHighRatio;
    private string sHighSpeed;
    private string sHighTime;
    private string sLowRatio;
    private string sLowSpeed;
    private float speedX;
    private float speedY;
    private string sSpeedX;
    private string sSpeedY;
    private string sThreshold;
    private float threshold;
    public static float time = float.MaxValue;

    public void Awake()
    {
        this.speedX = GlobalConfigs.RotateSpeedX;
        this.speedY = GlobalConfigs.RotateSpeedY;
        this.threshold = GlobalConfigs.RotateThreshold;
        this.LowSpeed = GlobalConfigs.RotateLowSpeed;
        this.LowRatio = GlobalConfigs.RotateLowRatio;
        this.HighSpeed = GlobalConfigs.RotateHighSpeed;
        this.HighRatio = GlobalConfigs.RotateHighRatio;
        this.HighTime = GlobalConfigs.RotateHighTime;
        this.sThreshold = this.threshold.ToString();
        this.sSpeedX = this.speedX.ToString();
        this.sSpeedY = this.speedY.ToString();
        this.sLowSpeed = this.LowSpeed.ToString();
        this.sLowRatio = this.LowRatio.ToString();
        this.sHighSpeed = this.HighSpeed.ToString();
        this.sHighRatio = this.HighRatio.ToString();
        this.sHighTime = this.HighTime.ToString();
    }

    public float CalcSensitivity(float speed, float fixspeed)
    {
        handspeed = speed;
        if ((speed >= this.threshold) && (Time.time <= (time + this.HighTime)))
        {
            return (this.HighSpeed + (fixspeed * this.HighRatio));
        }
        return (this.LowSpeed + (fixspeed * this.LowRatio));
    }

    private void OnGUI()
    {
        GUI.matrix = Matrix4x4.Scale(new Vector3(Screen.width / 800f, Screen.height / 480f, 1));

//        GUILayout.BeginArea(this.AssistorRect);
        GUIControls.HorizontalLabel("旋转度V: 若S<T, V=LS+敏感度*LR, 否则, V=HS+敏感度*HR.");
        GUILayout.Space(10f);
        GUIControls.HorizontalLabel("滑动速度S:" + handspeed);
        if (GUIControls.HorizontalGroup("分割值T:", ref this.threshold, ref this.sThreshold))
        {
            GlobalConfigs.RotateThreshold = this.threshold;
        }
        GUILayout.Space(10f * GUIControls.HeightRatio);
        if (GUIControls.HorizontalGroup("敏感度X:", ref this.speedX, ref this.sSpeedX))
        {
            GlobalConfigs.RotateSpeedX = this.speedX;
        }
        if (GUIControls.HorizontalGroup("敏感度Y:", ref this.speedY, ref this.sSpeedY))
        {
            GlobalConfigs.RotateSpeedY = this.speedY;
        }
        GUILayout.Space(10f * GUIControls.HeightRatio);
        if (GUIControls.HorizontalGroup("低速值LS:", ref this.LowSpeed, ref this.sLowSpeed))
        {
            GlobalConfigs.RotateLowSpeed = this.LowSpeed;
        }
        if (GUIControls.HorizontalGroup("低倍数LR:", ref this.LowRatio, ref this.sLowRatio))
        {
            GlobalConfigs.RotateLowRatio = this.LowRatio;
        }
        GUILayout.Space(10f * GUIControls.HeightRatio);
        if (GUIControls.HorizontalGroup("高速值HS:", ref this.HighSpeed, ref this.sHighSpeed))
        {
            GlobalConfigs.RotateHighSpeed = this.HighSpeed;
        }
        if (GUIControls.HorizontalGroup("高倍数HR:", ref this.HighRatio, ref this.sHighRatio))
        {
            GlobalConfigs.RotateHighRatio = this.HighRatio;
        }
        if (GUIControls.HorizontalGroup("有效时间:", ref this.HighTime, ref this.sHighTime))
        {
            GlobalConfigs.RotateHighTime = this.HighTime;
        }
//        GUILayout.EndArea();
    }

    private void Validate()
    {
        this.speedX = GlobalConfigs.RotateSpeedX;
        this.speedY = GlobalConfigs.RotateSpeedY;
        this.sSpeedX = this.speedX.ToString();
        this.sSpeedY = this.speedY.ToString();
    }
}

