﻿using UnityEngine;

public class TouchRotationSmooth
{
    private bool m_firstDelta;
    private Vector2 m_lastTouchPosition;
    private JoystickViewInterpolation m_interpolator;

    private float SensitivityModifier = 0.7f;
    private float PolarizedModifier = 0.2f;
    public float PitchAdd;
    public float YawAdd;
    private static float m_dpi = -1f;

    public static float factor = 1f;


    public TouchRotationSmooth()
    {
        m_interpolator = new JoystickViewInterpolation();
    }

    public void TouchBegan()
    {
        m_firstDelta = true;
        ZeroInput();
    }

    public void TouchMove(MyTouch touch, float sensitivity)
    {
        var deltaPosition = new Vector2();
        if (m_firstDelta)
        {
            deltaPosition = touch.deltaPosition;
            m_firstDelta = false;
        }
        else
            deltaPosition = touch.position - m_lastTouchPosition;
        m_lastTouchPosition = touch.position;

        m_interpolator.TouchChange(deltaPosition);
        deltaPosition = m_interpolator.ActualSmoothedTouch();
        if (deltaPosition != Vector2.zero)
        {
            float num;
            float num2;
            if (Dpi == 0f)
            {
                float num4 = 45f / (Screen.width * 0.25f);
                float num5 = 30f / (Screen.height * 0.25f);
                num = deltaPosition.x * num4;
                num2 = deltaPosition.y * num5;
                num *= (sensitivity + 1f) * 10f;
                num2 *= (sensitivity + 1f) * 10f;
            }
            else
            {
                var fx = (1136f / 326f) / (Screen.width / Dpi);
                var fy = (960f / 326f) / (Screen.height / Dpi);
                float num7 = 0.8f;
                num = (deltaPosition.x / Dpi) * fx * factor;
                num2 = (deltaPosition.y / Dpi) * num7 * fy * factor;
                bool flag = num < 0f;
                bool flag2 = num2 < 0f;
                num = Mathf.Clamp((float)(Mathf.Abs(num) * (0.5f + (1.3f * sensitivity))), (float)0f, (float)1f);
                num2 = Mathf.Clamp((float)(Mathf.Abs(num2) * (0.5f + (1.3f * sensitivity))), (float)0f, (float)1f);
                num = this.EasyIn(num);
                num2 = this.EasyIn(num2);
                num = 220f * (!flag ? num : -num);
                num2 = 220f * (!flag2 ? num2 : -num2);
                num = Mathf.Clamp(num, -180f, 180f);
                num2 = Mathf.Clamp(num2, -40f, 40f);
            }
            SetNewRotation(num, -num2);
            //transform.localEulerAngles += new Vector3(View.PitchAdd, View.YawAdd);
        }
    }

    private float EasyIn(float t)
    {
        float num = 1f - (0.6f);
        float num2 = 1f;
        float num3 = (t * num2) + ((1f - t) * num);
        return (((t * t) * (1f - num3)) + (t * num3));
    }

    private void SetNewRotation(float yaw, float pitch)
    {
        float sightSensitivity = 1;
        if (MainPlayer.singleton != null && FreeViewPlayer.singleton != null && !FreeViewPlayer.singleton.isRuning)
        {
            var gun = MainPlayer.singleton.curWeapon as WeaponGun;
            if (gun != null)
                sightSensitivity = gun.isZoomOrOut ? 1 / gun.weaponConfigLine.ZoomFactor : 1;
        }
        yaw *= SensitivityModifier * sightSensitivity;
        pitch *= SensitivityModifier * sightSensitivity;
        StickToAxis(ref yaw, ref pitch);
        YawAdd = yaw;
        PitchAdd = pitch;
    }

    private void StickToAxis(ref float yaw, ref float pitch)
    {
        var absYaw = Mathf.Abs(yaw);
        var absPitch = Mathf.Abs(pitch);

        if (absPitch < absYaw * PolarizedModifier)
            pitch = 0;
        else if (absYaw < absPitch * PolarizedModifier)
            yaw = 0;
    }

    private void ZeroInput()
    {
        YawAdd = 0;
        PitchAdd = 0;
    }

    public static float Dpi
    {
        get
        {
            if (m_dpi < 0)
                m_dpi = Screen.dpi;
            return m_dpi;
        }
    }
}