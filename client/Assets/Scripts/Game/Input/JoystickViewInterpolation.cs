﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class JoystickViewInterpolation
{
    private const float INTERPOLATION_TIME = 0.1f;
    private Queue<TouchInfo> m_Buffer = new Queue<TouchInfo>();
    private List<TouchInfo> m_TouchInfo = new List<TouchInfo>();
    private const int MAX_CAPACITY = 15;

    public JoystickViewInterpolation()
    {
        this.m_TouchInfo.Capacity = MAX_CAPACITY;
        for (int i = 0; i < MAX_CAPACITY; i++)
        {
            this.m_Buffer.Enqueue(new TouchInfo());
        }
    }

    public Vector2 ActualSmoothedTouch()
    {
        Vector2 totleDelta = Vector2.zero;
        float num2 = 0.1f;
        float totalDeltaTime = 0f;
        float num4 = 0f;
        int length = this.m_TouchInfo.Count - 1;
        for (int i = length; i >= 0; i--)
        {
            var deltaTime = this.m_TouchInfo[i].m_DeltaTime;
            float num7 = num2 * deltaTime;
            totleDelta += this.m_TouchInfo[i].m_Delta * num7;
            totalDeltaTime += num7;
            num2 -= deltaTime;
            num4 += deltaTime;
            if ((num4 >= 0.1f) || (num2 < 0.001f))
            {
                break;
            }
        }
        if (totalDeltaTime <= float.Epsilon)
        {
            return Vector2.zero;
        }
        return (Vector2) (totleDelta / totalDeltaTime);
    }

    public void NoTouch()
    {
        this.TouchChange(Vector2.zero);
    }

    public void TouchChange(Vector2 delta)
    {
        if (((delta.sqrMagnitude <= float.Epsilon) && (this.m_TouchInfo.Count > 0)) && (this.m_TouchInfo[this.m_TouchInfo.Count - 1].m_Delta.sqrMagnitude <= float.Epsilon))
        {
            TouchInfo local1 = this.m_TouchInfo[this.m_TouchInfo.Count - 1];
            local1.m_DeltaTime += Time.deltaTime;
        }
        else
        {
            if (this.m_Buffer.Count == 0)
            {
                this.m_Buffer.Enqueue(this.m_TouchInfo[0]);
                this.m_TouchInfo.RemoveAt(0);
            }
            TouchInfo item = this.m_Buffer.Dequeue();
            item.m_Delta = delta;
            item.m_DeltaTime = Time.deltaTime;
            this.m_TouchInfo.Add(item);
        }
    }

    private class TouchInfo
    {
        public Vector2 m_Delta;
        public float m_DeltaTime;
    }
}

