﻿using System;
using UnityEngine;

public class GlobalConfigs
{

    public static float RotateHighRatio
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateHighRatio", 3f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateHighRatio", value);
        }
    }

    public static float RotateHighSpeed
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateHighSpeed", 0f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateHighSpeed", value);
        }
    }

    public static float RotateHighTime
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateHighTime", 1f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateHighTime", value);
        }
    }

    public static float RotateLowRatio
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateLowRatio", 1f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateLowRatio", value);
        }
    }

    public static float RotateLowSpeed
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateLowSpeed", 0f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateLowSpeed", value);
        }
    }

    public static float RotateSpeedX
    {
        get
        {
            float defaultValue = 40f;
            return PlayerPrefs.GetFloat("RotateSpeedX", defaultValue);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateSpeedX", value);
        }
    }

    public static float RotateSpeedY
    {
        get
        {
            float defaultValue = 35f;
            return PlayerPrefs.GetFloat("RotateSpeedY", defaultValue);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateSpeedY", value);
        }
    }

    public static float RotateThreshold
    {
        get
        {
            return PlayerPrefs.GetFloat("RotateThreshold", 5f);
        }
        set
        {
            PlayerPrefs.SetFloat("RotateThreshold", value);
        }
    }
}

