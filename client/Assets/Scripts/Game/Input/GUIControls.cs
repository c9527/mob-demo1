﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public sealed class GUIControls
{
    public static bool HorizontalGroup(string label, ref float value, ref string sValue)
    {
        bool flag = false;
        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
        HorizontalLabel(label, 80f * WidthRatio, 20f, 0x12);
        GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.MaxWidth(160f * WidthRatio), GUILayout.MaxHeight(25f * HeightRatio) };
        sValue = GUILayout.TextField(sValue, options);
        bool flag2 = !sValue.Equals(((float) value).ToString());
        GUILayoutOption[] optionArray2 = new GUILayoutOption[] { GUILayout.MaxWidth(60f * WidthRatio), GUILayout.MaxHeight(25f * HeightRatio) };
        if (GUILayout.Button("Apply" + (!flag2 ? string.Empty : "*"), optionArray2))
        {
            if (!float.TryParse(sValue, out value))
            {
                sValue = "数据错误";
            }
            else
            {
                flag = true;
            }
        }
        GUILayout.EndHorizontal();
        return flag;
    }

    public static void HorizontalLabel(string label, float width = 0x7fffffff, float height = 20, int fontSize = 0x12)
    {
        GUIStyle style = new GUIStyle {
            normal = { textColor = Color.white },
            fontSize = fontSize
        };
        GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.MaxWidth(width), GUILayout.MaxHeight(height) };
        GUILayout.Label(label, style, options);
    }

    public static float HorizontalSlider(string label, float value, float leftValue, float rightValue)
    {
        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
        HorizontalLabel(label, 80f * WidthRatio, 40f * HeightRatio, 0x12);
        value = GUILayout.HorizontalSlider(value, leftValue, rightValue, new GUILayoutOption[0]);
        HorizontalLabel(value.ToString("0.000"), 40f * WidthRatio, 40f * HeightRatio, 0x12);
        GUILayout.EndHorizontal();
        return value;
    }

    public static float HeightRatio
    {
        get
        {
            return 1.2f;
        }
    }

    public static float WidthRatio
    {
        get
        {
            return 1f;
        }
    }
}

