﻿using UnityEngine;

class TouchRotationRough
{
    private float m_dpi = -1;
    private float m_touchBeginTime = float.MaxValue;
    public float PitchAdd;
    public float YawAdd;

    public void TouchBegan()
    {
        m_touchBeginTime = Time.time;
    }

    public void TouchEnd()
    {
        m_touchBeginTime = float.MaxValue;
    }

    public void TouchMove(MyTouch touch)
    {
        var deltaMove = touch.deltaPosition;
        var num = Time.deltaTime*Dpi;
        var slideX = Mathf.Abs(deltaMove.x/num);
        var slideY = Mathf.Abs(deltaMove.y/num);
        YawAdd = (CalcSensitivity(slideX, GlobalConfigs.RotateSpeedX, m_touchBeginTime) * touch.deltaPosition.x) / Dpi;
        PitchAdd = -(CalcSensitivity(slideY, GlobalConfigs.RotateSpeedY, m_touchBeginTime) * touch.deltaPosition.y) / Dpi;
    }

    private static float CalcSensitivity(float slide, float axisspeed, float startTime)
    {
        if ((slide >= GlobalConfigs.RotateThreshold) && (Time.time <= (startTime + GlobalConfigs.RotateHighTime)))
        {
            return (GlobalConfigs.RotateHighSpeed + (axisspeed * GlobalConfigs.RotateHighRatio));
        }
        return (GlobalConfigs.RotateLowSpeed + (axisspeed * GlobalConfigs.RotateLowRatio));
    }

    private float Dpi
    {
        get
        {
            if (m_dpi < 0f)
                m_dpi = Screen.dpi;
            return m_dpi;
        }
    }
}