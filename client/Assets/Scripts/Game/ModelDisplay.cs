﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

static class ModelDisplay
{
    private static Camera m_modelCamera;
    private static Vector3[] m_cameraPosArr;
    private static int m_cameraPosIndex;
    private static RenderTexture m_renderTexture;
    private static Transform m_tranRole;
    private static Transform m_tranWeapon;
    private static Transform[] m_tranDecoration = new Transform[5];
    private static ConfigItemRoleLine m_configRole;
    private static ConfigItemWeaponLine m_configWeapon;
    private static ConfigItemDecorationLine[] m_configDecoration;
    private static bool m_visible;
    private static WorldManager.CAMP_DEFINE m_modelDefaultCamp = WorldManager.CAMP_DEFINE.UNION;
    private static readonly Vector3 ORIGIN_POS = new Vector3(999.35f, 1.64f, 3.67f);
    private static Vector3 m_lastPos = ORIGIN_POS;
    private static Transform temp;
    static ResourceItem[] trans;

    static ConfigItemDecorationLine m_configHead;
    static Transform m_tranHead;

    static ConfigItemDecorationLine m_configFace;
    static Transform m_tranFace;

    static ConfigItemDecorationLine m_configShoulder;
    static Transform m_tranShoulder;

    static ConfigItemDecorationLine m_configWaist;
    static Transform m_tranWaist;


    static FPSAnimation weaponhallAni;
    public static void GetGuaJianModel(ConfigItemDecorationLine nowconfig,int part,ConfigItemRoleLine role,bool isrole,bool roleChange = false)
    {
        if (isrole)
        {
            setConfig(null, part);
            return;
        }
        bool ischange = !CheckConfigChange(nowconfig, part);
        if (ischange||isrole||roleChange)
        {
            setConfig(nowconfig,part);
            DestroyTran(part);
        }
        else
        {
            return;
        }
        if (nowconfig == null||isrole)
        {
            return;
        }
        else
        {
            var path = role.ModelSex == 1 ? nowconfig.BoyModel : nowconfig.GirlModel;
            path = "Roles/Decoration/" + path;
            ResourceManager.LoadModelRef(path, res => 
            {
                DestroyTran(part);
                if (res != null)
                {
                    var tranDeco = (Object.Instantiate(res) as GameObject).transform;
                    tranDeco.name = res.name;
                    Transform transbip = null;
                    var config = nowconfig;
                    if (config.SlotPath != "")
                    {
                        transbip = GameObjectHelper.FindChildByTraverse(m_tranRole, config.SlotPath);
                    }
                    else
                    {
                        transbip = GameObjectHelper.FindChildByTraverse(m_tranRole, ModelConst.BIP_HEAD);
                    }
                    var transSlot = GameObjectHelper.CreateChildIfNotExisted(transbip, "Slot" + config.Part.ToString()).transform;
                    transSlot.ResetLocal();

                    tranDeco.SetParent(transSlot, false);
                    tranDeco.localPosition = role.ModelSex == 1 ? config.BoyPos : config.GirlPos;
                    tranDeco.localRotation = Quaternion.Euler(role.ModelSex == 1 ? config.BoyRot : config.GirlRot);
                    tranDeco.localScale *= 1;
                    SetTran(part, tranDeco);
                }
            }, false);
            

        }



    }

    public static void SetTran(int part,Transform tran)
    {
        if (part == 1)
        {
            m_tranHead = tran;
        }
        if (part == 2)
        {
            m_tranFace = tran;

        }
        if (part == 3)
        {
            m_tranShoulder = tran;
        }
        if (part == 4)
        {
            m_tranWaist = tran;
        }
    }


    public static void DestroyTran(int part)
    {
        if (part == 1)
        {
            if (m_tranHead != null)
            {
                Object.Destroy(m_tranHead.gameObject);
            }
        }
        if (part == 2)
        {
            if (m_tranFace != null)
            {
                Object.Destroy(m_tranFace.gameObject);
            }
        }
        if (part == 3)
        {
            if (m_tranShoulder != null)
            {
                Object.Destroy(m_tranShoulder.gameObject);
            }
        }
        if (part == 4)
        {
            if (m_tranWaist != null)
            {
                Object.Destroy(m_tranWaist.gameObject);
            }
        }
    }

    public static void setConfig(ConfigItemDecorationLine nowconfig, int part)
    {
        if (part == 1)
        {
            m_configHead = nowconfig;
        }
        if (part == 2)
        {
            m_configFace = nowconfig;
        }
        if (part == 3)
        {
            m_configShoulder = nowconfig;
        }
        if (part == 4)
        {
            m_configWaist = nowconfig;
        }
    }

    public static bool CheckConfigChange(ConfigItemDecorationLine nowconfig, int part)
    {
        if (part == 1)
        {
            return nowconfig == m_configHead;
        }
        if (part == 2)
        {
            return nowconfig == m_configFace;
        }
        if (part == 3)
        {
            return nowconfig == m_configShoulder;
        }
        if (part == 4)
        {
            return nowconfig == m_configWaist;
        }
        return true;
    }



    


    public static RenderTexture RenderTexture
    {
        get
        {
            Init();
            if (m_renderTexture == null)
                m_renderTexture = new RenderTexture(Screen.width, Screen.height, 16);
            m_modelCamera.targetTexture = m_renderTexture;
            if ((!Driver.isMobilePlatform))
            {
                m_modelCamera.targetTexture.antiAliasing = 8;
            }
            return m_renderTexture;
        }
    }

    public static WorldManager.CAMP_DEFINE ModelDefaultCamp { set { m_modelDefaultCamp = value; } get { return m_modelDefaultCamp;} }
    public static ConfigItemRoleLine ConfigRole { set { m_configRole = value; } }
    public static ConfigItemWeaponLine ConfigWeapon { set { m_configWeapon = value; } }

    public static ConfigItemDecorationLine[] ConfigDecoration { set { m_configDecoration = value; } get { return m_configDecoration; } }
    public static bool Visible
    {
        set
        {
            Init();
            m_visible = value;
            m_modelCamera.transform.localPosition = value ? m_lastPos : new Vector3(4399, 0, 0);
        }
        get{return m_visible;}
    }

    public static void Init()
    {
        if (m_modelCamera)
            return;
        m_modelCamera = new GameObject("RoleCamera").AddComponent<Camera>();
        m_modelCamera.orthographic = false;
        m_modelCamera.orthographicSize = 0.51f;
        m_modelCamera.fieldOfView = 20;
        m_modelCamera.farClipPlane = 10;
        m_modelCamera.transform.localPosition = ORIGIN_POS;
        m_modelCamera.transform.localRotation = Quaternion.Euler(5f, 175.57f, 0);

        m_cameraPosArr = new Vector3[] {ORIGIN_POS};
        m_cameraPosIndex = 0;
        m_tranDecoration = new Transform[5];
    }

    public static void Destroy()
    {
        if (m_modelCamera)
            Object.Destroy(m_modelCamera.gameObject);
        if (m_tranRole)
            Object.Destroy(m_tranRole.gameObject);
        if (m_tranWeapon)
            Object.Destroy(m_tranWeapon.gameObject);
        for (int i = 0; i < 5; i++)
        {
            
            if (m_tranDecoration[i])
            {
                Object.Destroy(m_tranDecoration[i].gameObject);

            }
        }
        DestroyTran(1);
        DestroyTran(2);
        DestroyTran(3);
        DestroyTran(4);
    }

    public static void UpdateModel()
    {
        ConfigItemRoleLine configRole = m_configRole;
        if (m_configRole == null)
        {
            configRole = PlayerSystem.getEquipRoleInfo((int)m_modelDefaultCamp);
            if (configRole == null)
                configRole = ConfigManager.GetConfig<ConfigItemRole>().GetLine(4002);
        }

        var isZombie = configRole != null && configRole.RoleCamp == (int)WorldManager.CAMP_DEFINE.ZOMBIE;
        var isHero = configRole != null && configRole.SubType == GameConst.ITEMS_SUBTYPE_HERO;
        var roleName = configRole.OPModel + "Hall";
        if (isZombie || isHero)
            roleName = configRole.OPModel;
        var roleModelChanged = m_tranRole == null || m_tranRole.name != roleName;
        
        ResourceManager.LoadModelRef("Roles/" + roleName, res =>
        {
            if (WorldManager.singleton != null && WorldManager.singleton.fightEntered)
                return;

            if (roleModelChanged)
            {
                if (m_tranRole)
                    Object.Destroy(m_tranRole.gameObject);
                m_tranRole = (Object.Instantiate(res) as GameObject).transform;
                m_tranRole.name = res.name;
                var arr = m_tranRole.GetComponentsInChildren<Renderer>();
                for (int i = 0; i < arr.Length; i++)
                {
                    var texture = arr[i].sharedMaterial.mainTexture;
                    arr[i].material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_PLAYER);
                    arr[i].material.mainTexture = texture;
#if UNITY_EDITOR
                    if (ResourceManager.m_loadMode == ELoadMode.AssetBundle)
                        arr[i].material.shader = Shader.Find(arr[i].sharedMaterial.shader.name);
#endif
                }

                m_tranRole.localPosition = new Vector3(1000, 0, 0);
                m_tranRole.localRotation = Quaternion.Euler(0, 345, 0);
            }

            //默认武器
            ConfigItemWeaponLine configWeapon = m_configWeapon;

            if (isHero)
            {
                configWeapon = ItemDataManager.GetItemWeapon(configRole.Weapon[0]);
            }
            else if (m_configWeapon == null && ItemDataManager.m_bags.Length != 0)
            {
                configWeapon = ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON1) as ConfigItemWeaponLine;
                if (configWeapon == null)
                    configWeapon = ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON2) as ConfigItemWeaponLine;
            }
            if (configWeapon == null)
                return;

            //根据枪来选择播放的动作
            var animator = m_tranRole.GetComponent<Animator>();
            string roleAnim = "";
            if (configWeapon.HallAnimation != null && configWeapon.HallAnimation.Length > 0)
            {
                roleAnim = configWeapon.HallAnimation[0];
                if (configRole.ModelSex - 1 < configWeapon.HallAnimation.Length)
                    roleAnim = configWeapon.HallAnimation[configRole.ModelSex - 1];
            }

            if (string.IsNullOrEmpty(roleAnim))
                animator.Play(isZombie ? "stand" : "hallStand1");
            else
                animator.Play(isZombie ? "stand" : roleAnim);
//            animator.SetLayerWeight(1, 0);
//            animator.SetLayerWeight(2, 0);

            //不显示武器
            if (isZombie)
            {
                if (m_tranWeapon != null)
                    Object.Destroy(m_tranWeapon.gameObject);
            }
            else
            {
                var weaponModel = (configWeapon.HallModelCheck == 1) ? configWeapon.MPModel : configWeapon.OPModel;
                var weaponModelChanged = m_tranWeapon == null || m_tranWeapon.name != weaponModel;

                if (weaponModelChanged || roleModelChanged)
                {
                    if (m_tranWeapon != null)
                        Object.Destroy(m_tranWeapon.gameObject);
                    ResourceManager.LoadModelRef("Weapons/" + weaponModel, resWeapon =>
                    {
                        if (WorldManager.singleton != null && WorldManager.singleton.fightEntered)
                            return;

                        if (m_tranWeapon)
                            Object.Destroy(m_tranWeapon.gameObject);
                        m_tranWeapon = (Object.Instantiate(resWeapon) as GameObject).transform;
                        m_tranWeapon.name = resWeapon.name;

                        Transform transBipRightHandCenter = GameObjectHelper.FindChildByTraverse(m_tranRole, ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
                        var transRightHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipRightHandCenter, ModelConst.PLAYER_SLOT_RIGHT_HAND).transform;
                        transRightHandSlot.ResetLocal();

                        bool isRightHand = configWeapon.SlotType != WeaponBase.WEAPON_SLOTTYPE_LEFTHAND;

                        Transform transBipHandCenter = GameObjectHelper.FindChildByTraverse(m_tranRole, isRightHand ? ModelConst.BIP_NAME_RIGHT_HAND_CENTER : ModelConst.BIP_NAME_LEFT_HAND_CENTER);
                        var transHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipHandCenter, isRightHand ? ModelConst.PLAYER_SLOT_RIGHT_HAND : ModelConst.PLAYER_SLOT_LEFT_HAND).transform;
                        transHandSlot.ResetLocal();

                        m_tranWeapon.SetParent(transHandSlot, false);
                        m_tranWeapon.localPosition = configRole.ModelSex == 1 ? configWeapon.HallPos[0] : configWeapon.HallGrilPos[0];
                        m_tranWeapon.localRotation = Quaternion.Euler(configRole.ModelSex == 1 ? configWeapon.HallRot[0] : configWeapon.HallGrilRot[0]);
                        m_tranWeapon.localScale *= configWeapon.HallModelScale;
                        m_tranWeapon.VisitChild((childWeapon) =>
                        {
                            if (childWeapon.name.ToLower().StartsWith(ModelConst.WEAPON_EFFECT))
                                return false;

                            var r = childWeapon.GetComponent<Renderer>();
                            if (r != null)
                            {
                                var texture = r.sharedMaterial.mainTexture;
                                if (m_tranWeapon.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
                                    r.material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_WEAPON_CRYSTAL);
                                else
                                    r.material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_WEAPON);
                                r.material.mainTexture = texture;

#if UNITY_EDITOR
                                r.sharedMaterial.shader = Shader.Find(r.sharedMaterial.shader.name);
#endif
                            }
                            return true;

                        }, true);
                       
                        weaponhallAni = new FPSAnimation();
                        if (configWeapon.HallWeaponAnimation.Length != 0)
                        {
                            //Debug.LogError("gogogog");
                            weaponhallAni.SetAnimation(m_tranWeapon.gameObject.AddMissingComponent<Animation>());
                            LoadWeaponAni(weaponhallAni, configWeapon.HallWeaponAnimation);
                           
                        }
                    }, false);
                }
            }




            if (m_configDecoration == null)
            {
                m_configDecoration = new ConfigItemDecorationLine[5];
                for (int i = 0; i < ItemDataManager.m_bags[0].decorations.Length; i++)
                {
                    if (m_configDecoration[ItemDataManager.m_bags[0].decorations[i].part - 1] == null)
                    {
                        var item = ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].decorations[i].decoration);
                        m_configDecoration[ItemDataManager.m_bags[0].decorations[i].part - 1] = item as ConfigItemDecorationLine;
                    }
                }
            }

            for (int i = 0; i < 4; i++)
            {
                GetGuaJianModel(ConfigDecoration[i], i + 1, configRole, isZombie || isHero,roleModelChanged);
            }




            ////显示挂件模型
            //for (int i = 0; i < 4; i++)
            //{
            //    if (m_configDecoration[i] == null)
            //    {
            //        if (m_tranDecoration[i] != null)
            //        {
            //            Object.Destroy(m_tranDecoration[i].gameObject);
            //        }
            //        continue;
            //    }
            //    else
            //    {
            //        if (isZombie || isHero)
            //        {
            //            if (m_tranDecoration[i] != null)
            //            {
            //                Object.Destroy(m_tranDecoration[i].gameObject);
            //            }
            //        }
            //        else
            //        {
            //            var decorationModel = configRole.ModelSex == 1 ? m_configDecoration[i].BoyModel : m_configDecoration[i].GirlModel;
            //            var config = m_configDecoration[i];
            //            if (config.Part == "5")
            //            {
            //                if (m_tranDecoration[i] != null)
            //                {
            //                    Object.Destroy(m_tranDecoration[i].gameObject);
            //                    continue;
            //                }
            //            }
            //        }
            //    }
            //}


            //List<ConfigItemDecorationLine> list = new List<ConfigItemDecorationLine>();
            //for (int i = 0; i < 4; i++)
            //{
            //    if(m_configDecoration[i]!= null)
            //    {
                    
            //        list.Add(m_configDecoration[i]);
            //    }
            //}



            //var paths = new List<string>();
            //for (int i = 0; i < list.Count; i++)
            //{
            //    if (list[i] != null)
            //    {
            //        var path = configRole.ModelSex == 1 ? list[i].BoyModel : list[i].GirlModel;
            //        paths.Add("Roles/Decoration/" + path);
            //    }
            //}
            
            //int j = 0;
            //trans = new ResourceItem[paths.Count];
            //if (!isZombie && !isHero)
            //{


            //    ResourceManager.LoadMulti(paths.ToArray(), resi =>
            //    {
            //        Debug.LogError(j);
            //        Debug.LogError(trans.Length);
            //        trans[j] = resi;
            //        j++;
            //    }, () =>
            //    {
            //        j = 0;
            //        for (j = 0; j < paths.Count; j++)
            //        {
            //            if (m_tranDecoration[int.Parse(list[j].Part)-1] != null)
            //            {
            //                Object.Destroy(m_tranDecoration[int.Parse(list[j].Part) - 1].gameObject);
            //            }
            //            if (WorldManager.singleton != null && WorldManager.singleton.fightEntered)
            //                return;
            //            if (trans[j] == null)
            //            {
            //                continue;
            //            }
            //            if (trans[j].MainAsset == null)
            //            {
            //                continue;
            //            }
            //            var tranDeco = (Object.Instantiate(trans[j].MainAsset) as GameObject).transform;
            //            tranDeco.name = trans[j].MainAsset.name;
            //            Transform transbip = null;
            //            var config = list[j];
            //            if (config.SlotPath != "")
            //            {
            //                transbip = GameObjectHelper.FindChildByTraverse(m_tranRole, config.SlotPath);
            //            }
            //            else
            //            {
            //                transbip = GameObjectHelper.FindChildByTraverse(m_tranRole, ModelConst.BIP_HEAD);
            //            }
            //            var transSlot = GameObjectHelper.CreateChildIfNotExisted(transbip, "Slot" + config.Part.ToString()).transform;
            //            transSlot.ResetLocal();

            //            tranDeco.SetParent(transSlot, false);
            //            tranDeco.localPosition = configRole.ModelSex == 1 ? config.BoyPos : config.GirlPos;
            //            tranDeco.localRotation = Quaternion.Euler(configRole.ModelSex == 1 ? config.BoyRot : config.GirlRot);
            //            tranDeco.localScale *= 1;
            //            temp = tranDeco;

            //            m_tranDecoration[int.Parse(list[j].Part) - 1] = temp;
            //        }
            //    });
            //}

            



        }, false);


        var ar = trans;

    }

    public static void Rotation(float dx)
    {
        if (m_tranRole)
            m_tranRole.localEulerAngles += new Vector3(0, -dx * 0.3f, 0);
    }

    /// <summary>
    /// 循环切换镜头位置
    /// </summary>
    public static void ChangeCameraPosition()
    {
        if (m_cameraPosArr == null || m_cameraPosArr.Length <= 1)
            return;

        m_cameraPosIndex = (m_cameraPosIndex + 1) % m_cameraPosArr.Length;
        TweenPosition.Begin(m_modelCamera.gameObject, 0.2f, m_cameraPosArr[m_cameraPosIndex]).method = UITweener.Method.EaseOut;
        m_lastPos = m_cameraPosArr[m_cameraPosIndex];
    }

    public static void SetCameraPosArr(Vector3[] cameraPosArr)
    {
        m_cameraPosIndex = 0;
        m_cameraPosArr = cameraPosArr;
        Visible = true;

        if (!m_modelCamera)
            return;
        if (m_cameraPosArr != null && m_cameraPosArr.Length > 0)
        {
            if (Mathf.Abs(m_cameraPosArr[0].x - m_modelCamera.transform.localPosition.x) < 0.5f)
                TweenPosition.Begin(m_modelCamera.gameObject, 0.1f, m_cameraPosArr[0]).method = UITweener.Method.EaseOut;
            else
                m_modelCamera.transform.localPosition = m_cameraPosArr[0];
            m_lastPos = m_cameraPosArr[0];
        }
    }

    public static void MoveOutCamera()
    {
        if (!m_modelCamera)
            return;
        Visible = false;
//        SetCameraPosArr(new [] { new Vector3(996.5f, 1.64f, 3.67f) });
    }

    static void Toc_player_equip_item(toc_player_equip_item data)
    {
        //TimerManager.SetTimeOut(0.1f, UpdateModel);
    }

    static private void LoadWeaponAni(FPSAnimation fpsAni, string aniName)
    {
        if (string.IsNullOrEmpty(aniName) == false && weaponhallAni.GetClip(aniName)==null)
        {
            string strPath = PathHelper.GetWeaponAni(aniName);
            ResourceManager.LoadAnimation(strPath, (kAniClip) =>
            {
                if (fpsAni != null && kAniClip != null)
                {
                    fpsAni.AddClip(kAniClip, aniName);
                    fpsAni.Play(aniName);
                }
            });
        }
    }

    static private void LoadWeaponAni(FPSAnimation fpsAni, string[] arrName)
    {
        if (arrName == null)
            return;

        foreach (string str in arrName)
        {
            LoadWeaponAni(fpsAni, str);
        }
    }
}