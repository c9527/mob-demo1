﻿using UnityEngine;
using System.Collections;
using System;


public class SceneCamera 
{
    static public SceneCamera singleton;

    private GameObject m_go;
    private Camera m_camera;
    private Transform m_trans;
    private Ray m_ray = new Ray();

    private bool m_run;

    private float m_screenHalfWidth;
    private float m_screenHalfHeight;

    private CameraShake m_cameraShake;

    public SceneCamera()
    {
        singleton = this;

        Init();
    }

    private void Init()
    {
        m_go = new GameObject("SceneCamera");
        m_go.tag = "MainCamera";

        m_trans = m_go.transform;

        m_camera = m_go.AddComponent<Camera>();
        m_camera.orthographic = false;
        m_camera.fieldOfView = 45;        
        m_camera.cullingMask = ~(GameSetting.LAYER_MASK_MAIN_PLAYER | GameSetting.LAYER_MASK_UI);
        m_camera.depth = GameSetting.CAMERA_DEEPTH_SCENECAMERA;
        m_camera.nearClipPlane = 0.09f;
        m_camera.useOcclusionCulling = true;

        QualityManager.InitSceneCamera(m_camera);

        m_go.AddMissingComponent<SceneCameraController>();

        m_cameraShake = m_go.AddMissingComponent<CameraShake>();
        m_cameraShake.AddCamera(m_camera);
        if(MainPlayer.singleton != null && MainPlayer.singleton.cameraMainPlayer != null)
            m_cameraShake.AddCamera(MainPlayer.singleton.cameraMainPlayer, true, MainPlayer.singleton.cameraShaker.transform.parent);

        m_screenHalfWidth = Screen.width * 0.5f;
        m_screenHalfHeight = Screen.height * 0.5f;
    }

    public void Release()
    {
        ResourceManager.Destroy(m_go);
        singleton = null;
    }

    public void SetActive(bool value)
    {
        m_go.TrySetActive(value);
    }

    public void SetParent(Transform parent, bool resetLocal = false)
    {
        m_trans.SetParent(parent);

        if (resetLocal)
        {
            m_trans.localPosition = Vector3.zero;
            m_trans.localEulerAngles = Vector3.zero;
        }

        if(GameSetting.enableCameraLog)
        {
            Logger.Warning(StackTraceUtility.ExtractStackTrace() + "\n Parent = " + (parent != null ? parent.root.name : "null"));
        }
    }

    //public void ResetLocalTransform()
    //{
    //    m_trans.ResetLocal();
    //    Logger.Warning("ResetLocal " + position);
    //}

    public void LookAt(Vector3 targetPos)
    {
        m_trans.LookAt(targetPos);
    }

    public void RotateAround(Vector3 tragetPos, Vector3 axis, float angle)
    {
        m_trans.RotateAround(tragetPos, axis, angle);
        CheckPosError("RotateAround");
    }

    public void Rotate(float xAngle, float yAngle, float zAngle, Space relativeTo)
    {
        m_trans.Rotate(xAngle, yAngle, zAngle, relativeTo);
        CheckPosError("Rotate");
    }

    public Vector3 WorldToScreenPoint(Vector3 pos)
    {
        return m_camera.WorldToScreenPoint(pos);
    }

    public Vector3 ScreenToWorldPoint(Vector3 pos)
    {
        return m_camera.ScreenToWorldPoint(pos);
    }

    public Vector3 ScreenCenterToWorldPoint(float worldZ)
    {

    #if UNITY_EDITOR
        m_screenHalfWidth = Screen.width * 0.5f;
        m_screenHalfHeight = Screen.height * 0.5f;
    #endif

        return m_camera.ScreenToWorldPoint(new Vector3(m_screenHalfWidth, m_screenHalfHeight, worldZ));
    }

    public void SetListener()
    {
        AudioManager.SetListener(m_trans);
    }

    public void StartShake(ConfigShakeLine config)
    {
        if (config != null)
        {
            StartShake(config.Time, config.HorizontalLevel, config.HorizontalDecay, config.HorizontalRandom == 1, config.VerticalLevel, config.VerticalDecay, config.VerticalRandom == 1, config.Frequency, config.MainCameraLevelScale);
        }
    }

    public void StartShake(float shakeTime, float horizontalLevel, float horizontalDecay, bool horizontalRandom, float verticalLevel, float verticalDecay, bool verticalRandom, float frequency, float mainCameraLevelScale)
    {
        if(m_cameraShake != null)
        {
            //if (parent == null)
            //    m_cameraShake.AddCamera(m_camera);
            if (shakeTime > 0)
                m_cameraShake.StartShake(shakeTime, horizontalLevel, horizontalDecay, horizontalRandom, verticalLevel, verticalDecay, verticalRandom, frequency, mainCameraLevelScale, true);
        }
    }

    public void StopShake()
    {
        if (m_cameraShake != null)
        {
            m_cameraShake.StopShake();
        }
    }

#region Property

    public bool enableCamera
    {
        set 
        {
            m_camera.enabled = value;
            UIManager.m_uiCamera.clearFlags = value ? CameraClearFlags.Depth : CameraClearFlags.SolidColor;
        }
    }

    public Vector3 forward
    {
        get { return m_trans.forward; }
        set { m_trans.forward = value; }
    }

    public Vector3 right
    {
        get { return m_trans.right; }
    }

    public Ray ray
    {
        get
        {
            m_ray.origin = position;
            m_ray.direction = forward;
            return m_ray;
        }
    }

    public Vector3 position
    {
        get { return m_trans.position; }
        set
        {
            m_trans.position = value;
            CheckPosError("position");
        }
    }

    public Vector3 localPosition
    {
        get { return m_trans.localPosition; }
        set
        {
            m_trans.localPosition = value;
            CheckPosError("localPos");
        }
    }

    public Vector3 eulerAngles
    {
        get { return m_trans.eulerAngles; }
        set
        {
            m_trans.eulerAngles = value;
        }
    }

    public Vector3 localEulerAngles
    {
        get { return m_trans.localEulerAngles; }
        set { m_trans.localEulerAngles = value; }
    }

    public Quaternion localRotation
    {
        set
        {
            m_trans.localRotation = value;
        }
    }

    public Transform parent
    {
        get { return m_trans == null ? null : m_trans.parent; }
    }
    
    private string rootName
    {
        get{
            if (m_trans == null || m_trans.root == null)
                return "null";

            return m_trans.root.name; 
        }
    }

    /// <summary>
    ///  不要通过此接口来设置transform
    /// </summary>
    public GameObject gameObject
    {
        get { return m_go; }
    }

    public Camera camera
    {
        get { return m_camera; }
    }

    public float fieldOfView
    {
        get { return m_camera.fieldOfView; }
        set
        {
            m_camera.fieldOfView = value;
        }
    }

    public bool useOcclusionCulling
    {
        get { return m_camera.useOcclusionCulling; }
        //set {
        //    if (m_camera != null && m_camera.useOcclusionCulling != value)
        //        m_camera.useOcclusionCulling = value; 
        //}
    }

    public CameraShake cameraShake
    {
        get { return m_cameraShake; }
    }

    private void CheckPosError(string msg)
    {
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE) == false
            && parent != null
            && localPosition.magnitude >= 10
            || position.y >= 2000)
        {
            Logger.Error("SceneCamera " + msg + " Error!" + " rootName=" + rootName + " localPos=" + localPosition + " rootPos=" + m_trans.root.position + " alive=" + MainPlayer.singleton.alive);
        }
    }

#endregion
  

   








	 
}
