﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class SBattlePlayer
{
    private p_fight_role m_pRole;
    private int m_fight_energy;

    internal SBattlePlayer(p_fight_role pRole)
    {
        m_pRole = pRole;
    }

    public int actor_id
    {
        get { return m_pRole.id; }
    }

    public long player_id
    {
        get { return m_pRole.pid; }
    }

    public string name
    {
        get { return m_pRole.name; }
    }

    public bool alive
    {
        get { return m_pRole.state == GameConst.PLAYER_STATUS_ALIVE; }
    }

    public int state
    {
        get { return m_pRole.state; }
    }
    
    public int kill
    {
        set { m_pRole.kill = value; }
        get { return m_pRole.kill; }
    }

    public int round_kill
    {
        get { return m_pRole.round_kill; }
        set { m_pRole.round_kill = value; }
    }

    public int death_num
    {
        set { m_pRole.death = value; }
        get { return m_pRole.death; }
    }

    public int score
    {
        get { return m_pRole.score; }
        set { m_pRole.score = value; }
    }

    public int fight_energy
    {
        get { return m_fight_energy; }
        set { m_fight_energy = value; }
    }

    public int king_weapon_seq
    {
        get { return m_pRole.king_cur_seq; }
        set { m_pRole.king_cur_seq = value; }
    }

    [Obsolete]
    public int king_weapon_kill
    {
        get { return m_pRole.king_cur_weapon_kill; }
        set { m_pRole.king_cur_weapon_kill = value; }
    }

    public int level
    {
        get	{return m_pRole.level; }
    }

    public int vip_level
    {
        get { return m_pRole.vip_level; }
    }

    public int icon
    {
        get { return m_pRole.icon; }
    }

    public int camp
    {
        get { return m_pRole.camp; }
    }

    public int actor_type
    {
        get { return m_pRole.actor_type; }
    }
    public int honor
    {
        get { return m_pRole.honor; }
    }
}

public class SBattleSpectator
{
    public long id;
    public int icon;
    public string name;
    public int level;
    public int campId;
    public string campName;
}

public class SBattleCamp
{
    public int camp;
    public int win;   
    public int round_kill;
    public int king_camp_score;
    public int king_camp_cur_kill;
    public List<SBattlePlayer> players = new List<SBattlePlayer>();

    internal SBattleCamp(p_camp_info pCampInfo)
    {
        if (pCampInfo == null)
            return;

        camp = pCampInfo.camp;
        win = pCampInfo.win;
        round_kill = pCampInfo.kill - pCampInfo.round_begin_kill;
        king_camp_score = pCampInfo.king_camp_score;
        king_camp_cur_kill = pCampInfo.king_camp_cur_kill;
    }

    internal SBattlePlayer AddPlayer(p_fight_role pRole)
    {
        for(int i=0; i<players.Count; i++)
        {
            if(players[i].actor_id == pRole.id)
            {
                Logger.Error("SBattleCamp AddPlayer Duplicated! id = " + pRole.id);
                return null;
            }
        }

        SBattlePlayer sbp = new SBattlePlayer(pRole);
        players.Add(sbp);

        return sbp;
    }

    internal void UpdateKingCampData(p_king_camp_info data)
    {
        king_camp_score = data.king_camp_score;
        king_camp_cur_kill = 0;

        Logger.Log("UpdateKingCampData " + data.king_camp_score);

        // Event

    }
}

public class SBattleState
{
    public int round;
    //0-战斗初始化,1-战斗回合开始,2-战斗回合正式开始(准备时间后),3-战斗回合结束,4-游戏结束
    public int state;
    public int last_win;
    public int final_win;


    internal void updateData(p_fight_state data = null)
    {
        if (data == null)
            return;

        if (round != data.round)
            GameDispatcher.Dispatch(GameEvent.GAME_BATTLE_ROUND_UPDATE, data.round, true);

        round = data.round;
        state = data.state;
        last_win = data.last_win;
        final_win = data.final_win;
    }
}

public class SBattleInfo
{
    public string channel_type;
    public string channel_sub_type;
    public int win_param;
    private float m_round_time;
    private float round_time_local;  // 本地回合开始时的时间戳
    private int m_round_total_time;

    //ESG比赛观战
    private bool m_spectator_switch;

    public List<SBattleCamp> camps = new List<SBattleCamp>();
    public Dictionary<int, SBattlePlayer> m_allPlayers = new Dictionary<int, SBattlePlayer>();
    public int king_camp_last_win;

    public List<SBattleSpectator> spectators = new List<SBattleSpectator>();
    public int bomb_holder;
    public float[] bomb_point;
    public bool is_set_once;


    //internal void updateData(toc_fight_stat_info data)
    //{
    //    if (data == null)
    //        return;
       
    //    //bomb_point = data.bomb_point;
    //    //is_set_once = true;
    //}

    internal void SetBombInfo(toc_fight_bomb_info proto)
    {
        bool bombPointChanged = false;
        if(proto.bomb_point.Length != bomb_point.Length)
        {
            bombPointChanged = true;
        }
        else
        {
            for(int i=0; i<proto.bomb_point.Length; i++)
            {
                if(proto.bomb_point[i] != bomb_point[i])
                {
                    bombPointChanged = true;
                    break;
                }
            }
        }

        bomb_point = proto.bomb_point;
        bomb_holder = proto.bomb_holder;
        Logger.Log("SetBombInfo BombHolder = " + bomb_holder + " BombPoint = " + bomb_point.ToVector3());

        if (bombPointChanged)
        {
            Logger.Log("BombPoointChanged");
            GameDispatcher.Dispatch(GameEvent.GAME_BOMB_POINT);
        }
            
    }

    public SBattlePlayer GetPlayer(int id)
    {
        SBattlePlayer player = null;
        m_allPlayers.TryGetValue(id, out player);
        return player;
    }

    public SBattleCamp GetCamp(int camp)
    {
        for(int i=0; i<camps.Count; i++)
        {
            if (camps[i].camp == camp)
                return camps[i];
        }

        return null;
    }

    internal void AddPlayer(p_fight_role pRole)
    {
        int camp = pRole.camp;

        // Add To Camp
        SBattleCamp sbc = null;
        for(int i=0; i<camps.Count; i++)
        {
            if (camps[i].camp == camp)
            {
                sbc = camps[i];
                break;
            }
        }
        
        if(sbc == null)
        {
            sbc = new SBattleCamp(null);
            sbc.camp = camp;
            camps.Add(sbc);
            Logger.Log("AddPlayer Camp = " + camp);
        }

        SBattlePlayer player = sbc.AddPlayer(pRole);

        // Add To Player Map
        if (player != null && m_allPlayers.ContainsKey(pRole.id) == false)
        {
            m_allPlayers.Add(pRole.id, player);
        }

        GameDispatcher.Dispatch(GameEvent.BATTLE_MODEL_CAMP_UPDATE);
    }

    public void RemovePlayer(int playerID)
    {
        SBattlePlayer player = null;

        for (int i = 0; i < camps.Count; i++)
        {
            for (int j = 0; j < camps[i].players.Count; j++)
            {
                if (camps[i].players[j].actor_id == playerID)
                {
                    player = camps[i].players[j];
                    camps[i].players.RemoveAt(j);
                    break;
                }
            }

            if (player != null)
                break;
        }

        m_allPlayers.Remove(playerID);

        GameDispatcher.Dispatch(GameEvent.BATTLE_MODEL_CAMP_UPDATE);
    }

    internal void UpdatePlayerState(toc_fight_actorstate proto)
    {
        // 如果阵营有变化，则放到新的阵营列表中
        bool campChanged = false;
        SBattlePlayer player = null;
        for (int i = 0; i < proto.list.Length; i++)
        {
            p_actor_state_toc state = proto.list[i];
            player = null;

            for(int j=0; j<camps.Count; j++)
            {
                for (int n=0; n<camps[j].players.Count; n++)
                {
                    if(camps[j].players[n].actor_id == state.id)
                    {
                        if(camps[j].camp != state.camp)
                        {
                            player = camps[j].players[n];
                            camps[j].players.RemoveAt(n);
                        }
                        break;
                    }
                }

                if (player != null)
                    break;
            }

            if(player != null)
            {
                for(int m=0; m<camps.Count; m++)
                {
                    if(camps[m].camp == state.camp)
                    {
                        camps[m].players.Add(player);
                        break;
                    }
                }

                campChanged = true;
            }
        }

        // Event
        if (campChanged)
            ;

        GameDispatcher.Dispatch(GameEvent.BATTLE_MODEL_CAMP_UPDATE);
    }

    internal void AddSpecator(p_fight_spectator pSpecator, bool init)
    {
        for(int i=0; i<spectators.Count; i++)
        {
            if(spectators[i].id == pSpecator.player_id)
            {
                Logger.Warning("Duplicated Spector, ID = " + pSpecator.player_id);
                return;
            }
        }

        SBattleSpectator specator = new SBattleSpectator()
        {
            id = pSpecator.player_id,
            name = pSpecator.name,
            level = pSpecator.level,
            icon = pSpecator.icon,
            campName = pSpecator.corps_name,
            campId = pSpecator.corps_id,
        };
        spectators.Add(specator);

        Logger.Log("AddSpecator id=" + specator.id + "   name=" + specator.name);

        // Event
        if (init == false)
            ;

        GameDispatcher.Dispatch<long>(GameEvent.SPECTATOR_JOIN, pSpecator.player_id);
    }

    public void RemoveSpecator(long playerID)
    {
        for (int i = 0; i < spectators.Count; i++)
        {
            if (spectators[i].id == playerID)
            {
                spectators.RemoveAt(i);
                break;
            }
        }

        Logger.Log("RemoveSpecator id=" + playerID);

        // Event
        GameDispatcher.Dispatch<long>(GameEvent.SPECTATOR_LEAVE, playerID);
    }

    internal void InitCamp(toc_fight_game_info proto)
    {
        p_camp_info[] arrPCI = proto.camps;

        string msg = "Camp ";

        camps.Clear();
        SBattleCamp sbc = null;
        for(int i=0; i<arrPCI.Length; i++)
        {
            sbc = new SBattleCamp(arrPCI[i]);
            camps.Add(sbc);
            msg += sbc.camp;
        }
        king_camp_last_win = proto.king_camp_last_win;

        //Logger.Log("Camp " + msg);
    }

    internal void UpdateBattleState(p_fight_state pState)
    {
        if(pState.state == GameConst.Fight_State_RoundStart)
        {
            for (int i = 0; i < camps.Count; i++)
            {
                camps[i].round_kill = 0;
                for(int j=0; j<camps[i].players.Count; j++)
                {
                    camps[i].players[j].round_kill = 0;
                }
            }
        }


        if(pState.state == GameConst.Fight_State_RoundEnd)
        {
            for (int i = 0; i < camps.Count; i++)
            {
                if (camps[i].camp == pState.last_win)
                {
                    camps[i].win++;
                    break;
                }
                else if(WorldManager.singleton.gameRuleLine.IsDeuceWin == 1 && pState.last_win == 0)
                    camps[i].win++;
            }
        }
    }

    internal void UpdateKingCampInfo(toc_fight_king_camp proto)
    {
        for(int i=0; i<proto.camps.Length; i++)
        {
            for(int j=0; j<camps.Count; j++)
            {
                if(camps[j].camp == proto.camps[i].camp)
                {
                    camps[j].UpdateKingCampData(proto.camps[i]);
                    break;
                }
            }
        }

        king_camp_last_win = proto.king_camp_last_win;
    }

    internal void PlayerKilled(toc_fight_actor_die proto)
    {
        //if (proto.killer == proto.actor_id)
        //    return;
        killInfo(proto);
        
    }

    private void killInfo(object obj)
    {
        var proto = obj as toc_fight_actor_die;
        SBattlePlayer target = GetPlayer(proto.actor_id);
        // 死亡协议会推两次udp,tcp各推一次，因为tcp延时太大,所以这里要排除重复的那一次
        //Debug.LogError("target.alive = " + target.alive + " target.state = " + target.state + " targetname = " + target.name);
        if (target == null || target.state == GameConst.SERVER_ACTOR_STATUS_DEAD)
            return;

        if (target != null)
            target.death_num++;

        SBattlePlayer attacker = GetPlayer(proto.killer);

        if (attacker != null)
        {
            if (proto.killer != proto.actor_id)
            {
                attacker.kill++;
                attacker.round_kill++;

                SBattleCamp attackerCamp = GetCamp(attacker.camp);
                if (attackerCamp != null)
                {
                    if (WorldManager.singleton.isDotaModeOpen)
                    {
                        if (target != null && target.player_id > 0 && attacker.player_id > 0)
                            attackerCamp.
                                round_kill++;
                    }
                    else
                        attackerCamp.round_kill++;
                    attackerCamp.king_camp_cur_kill++;
                    //Logger.Log("PlayerKilled Camp.roundKill++ " + attackerCamp.camp + " " + attackerCamp.round_kill);
                }
            }
            else
            {
                int camp = attacker.camp;

                for (int i = 0; i < camps.Count; i++)
                {
                    if (camps[i].camp != camp)
                    {
                        SBattleCamp enemyCamp = camps[i];
                        enemyCamp.round_kill++;
                        enemyCamp.king_camp_cur_kill++;
                    }
                }
            }
        }
    }

    internal void UpdateKingSoloInfo(toc_fight_king_solo proto)
    {
        SBattlePlayer player = null;
        bool find;
        for(int i=0; i<proto.list.Length; i++)
        {
            find = false;
            for(int j=0; j<camps.Count; j++)
            {
                for(int n=0; n<camps[j].players.Count; n++)
                {
                    player = camps[j].players[n];
                    if(proto.list[i].actor_id == player.actor_id)
                    {
                        find = true;
                        player.king_weapon_seq = proto.list[i].king_cur_seq;
                        break;
                    }
                }

                if (find)
                    break;
            }
        }
    }

    internal void UpdateScore(toc_fight_score proto)
    {
        SBattlePlayer player = null;
        for (int i = 0; i < proto.list.Length; i++)
        {
            player = GetPlayer(proto.list[i].actor_id);
            if (player != null)
            {
                player.score = proto.list[i].score;                
            }
        }

        GameDispatcher.Dispatch<toc_fight_score>(GameEvent.PLAYER_SCORE_UPDATE, proto);
    }

    internal void UpdateEnergy(toc_fight_energy proto)
    {
        SBattlePlayer player = null;
        for (int i = 0; i < proto.list.Length; i++)
        {
            player = GetPlayer(proto.list[i].actor_id);
            if (player != null)
            {
                player.fight_energy = proto.list[i].score;
            }
        }
        
        GameDispatcher.Dispatch<toc_fight_energy>(GameEvent.PLAYER_ENERGY_UPDATE, proto);
    }

    #region get set
    public bool spectator_switch
    {
        get { return m_spectator_switch; }
        set
        {
            m_spectator_switch = value;
        }
    }

    public float round_time
    {
        get { return Time.realtimeSinceStartup - round_time_local; }
        set { 
            m_round_time = value;
            round_time_local = Time.realtimeSinceStartup - value;
        }
    }

    public float round_time_left
    {
        get { return m_round_total_time - round_time; }
    }

    public int round_total_time
    {
        set { m_round_total_time = value; }
    }
    #endregion
}

class MvpAndAceData
{
    public int ID { get; set; }
    public int Score { get; set; }
    public int KillNum { get; set; }
    public int DeathNum { get; set; }
    public string Name { get; set; }
}

public class KillNumRankData
{
    public int id { get; set; }
    public int rank { get; set; }
    public int killNum { get; set; }
    public float curHpPercent { get; set; }
    public string name { get; set; }

    public int score { get; set; }

    public void reset()
    {
        id = 0;
        rank = 0;
        killNum = 0;
        curHpPercent = 0;
        name = "";
        score = 0;
    }
}

public class PlayerBattleModel : Singleton<PlayerBattleModel>
{
    //战斗回合的状态数据
    private SBattleState _battle_state;
    //战局数据
    private SBattleInfo _battle_info;
    public int fight_role;
    public bool bag_selected;
    public bool pre_bag_selected;
 
    //生化模式的数据
    private int[] zombie_data;

    //MVP、ACE
    private int m_mvpId;
    private int m_gAceId;
    private int m_sAceId;
    private List<MvpAndAceData> m_mvpAndAceData;

    //投票踢人
    private bool m_canVoteKick = false;
    public bool hasBlockVotePanel = false;
    private toc_fight_begin_vote_kick m_proto;

    //击杀排行
    private Dictionary<int, List<KillNumRankData>> m_killNumRankDataDic;

    public PlayerBattleModel()
    {
        //initData();
    }

    public void Release()
    {
        _battle_info = null;
        _battle_state = null;
        fight_role = -1;
        zombie_data = null;
        m_mvpAndAceData = null;
        m_proto = null;
        m_killNumRankDataDic = null;
    }

    /// <summary>
    /// 清楚击杀、死亡、分数
    /// </summary>
    public void ClearBattleRecord()
    {
        var arr = battle_info.camps.ToArray();
        for (int i = 0; i < arr.Length; i++)
        {
            var players = arr[i].players.ToArray();
            for (int j = 0; j < players.Length; j++)
            {
                players[j].kill = 0;
                players[j].death_num = 0;
                players[j].score = 0;
                players[j].round_kill = 0;                
            }
        }        
        m_gAceId = -1;
        m_sAceId = -1;
        m_mvpId = -1;        
    }

    public bool  canStart
    {
        get
        {
            bool result = true;

            if (battle_info == null)
                return result;

            foreach(var item in battle_info.camps )
            {
                if(item.players.Count<WorldManager.singleton.gameRuleLine.CampPlayersMin)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }
    }


    internal void initData(toc_fight_game_info proto)
    {
        // Clear

        p_fight_state data = proto.state;

        bag_selected = false;

        _battle_state = new SBattleState();
        _battle_state.updateData(data);

        _battle_info = new SBattleInfo();
        _battle_info.bomb_point = new float[0];
        _battle_info.is_set_once = true;
        _battle_info.round_time = proto.round_time;
        _battle_info.spectator_switch = proto.spectator_switch;
        _battle_info.round_total_time = WorldManager.singleton.gameRuleLine.Time_play;
        _battle_info.win_param = WorldManager.singleton.gameRuleLine.WinParam;
        string type = WorldManager.singleton.fightType;
        var channel_data = !string.IsNullOrEmpty(type) ? type.Split('#') : null;
        _battle_info.channel_type = channel_data != null && channel_data.Length > 0 ? channel_data[0] : "";
        _battle_info.channel_sub_type = channel_data != null && channel_data.Length > 0 ? channel_data[1] : "";

        m_mvpId = 0;
        m_gAceId = 0;
        m_sAceId = 0;
        m_mvpAndAceData = new List<MvpAndAceData>();
        m_killNumRankDataDic = new Dictionary<int, List<KillNumRankData>>();

        // init camp
        _battle_info.InitCamp(proto);

        // init player
        for(int i=0; i<proto.list.Length; i++)
        {
            AddPlayer(proto.list[i]);
        }    

        // init specator
        for(int i=0; i<proto.spectator_list.Length; i++)
        {
            _battle_info.AddSpecator(proto.spectator_list[i], true);
        }

        // Event
        GameDispatcher.Dispatch<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, Instance.battle_state);
    }

    public void Update()
    {
        if (UIManager.IsBattle() && _battle_info != null)
            UpdateMvpAceData();
    }

    internal void UpdateBattleState(p_fight_state pState)
    {
        AudioManager.StopUISoundLoop(); //关闭预警音效

        if (_battle_state != null)
            _battle_state.updateData(pState);

        if (_battle_info != null)
            _battle_info.UpdateBattleState(pState);
        
        if(pState.state == GameConst.Fight_State_RoundEnd)
        {
            foreach(KeyValuePair<int, BasePlayer> kvp in WorldManager.singleton.allPlayer)
            {
                kvp.Value.GameRoundEnd();
            }
        }
    }   

    public int double_cost
    {
        get { return ConfigManager.GetConfig <ConfigDoubleRewardCost>().getCostData(PlayerSystem.roleData.double_reward_times).Diamond; }
    }

    public SBattleState battle_state
    {
        get { return _battle_state; }
    }

    public SBattleInfo battle_info
    {
        get { return _battle_info; }
    }

    //private void genCampKillNum()
    //{
    //    //Logger.Error("genCampKillNum");
    //    if (_battle_info == null) return;
    //    for (int i = 0; i < _battle_info.camps.Count; i++)
    //    {
    //        CampKillNum(_battle_info.camps[i]);
    //    }
    //}

    //private void CampKillNum(SBattleCamp camp)
    //{
    //    if (camp == null) return;
    //    if (!m_killNumRankDataDic.ContainsKey(camp.camp)) m_killNumRankDataDic.Add(camp.camp, new List<KillNumRankData>());
    //    List<KillNumRankData> dataList = m_killNumRankDataDic[camp.camp];
    //    int len = camp.players.Count;
    //    for (int i = 0; i < len; i++)
    //    {
    //        if (dataList.Count <= i) dataList.Add(new KillNumRankData());
    //        KillNumRankData data = dataList[i];
    //        data.id = camp.players[i].actor_id;
    //        data.name = camp.players[i].name;
    //        data.killNum = camp.players[i].kill;
    //        BasePlayer player = WorldManager.singleton.GetPlayerById(data.id);
    //        data.curHpPercent = player != null && player.serverData != null ? player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f : 0f;
    //        //Logger.Error("CampKillNum player: " + camp.players[i].actor_id + ", id: " + data.id);
    //    }
    //    for (int i = len; i < dataList.Count; i++)
    //    {
    //        KillNumRankData data = dataList[i];
    //        data.reset();
    //    }
    //}

    // 取m_killNumRankDataDic数据前调用
    private void UpdateKillNumRankData()
    {
        SBattleCamp camp = null;
        for (int j = 0; j < _battle_info.camps.Count; j++ )
        {
            camp = _battle_info.camps[j];
            if (!m_killNumRankDataDic.ContainsKey(camp.camp)) m_killNumRankDataDic.Add(camp.camp, new List<KillNumRankData>());
            List<KillNumRankData> dataList = m_killNumRankDataDic[camp.camp];
            int len = camp.players.Count;
            for (int i = 0; i < len; i++)
            {
                if (dataList.Count <= i) dataList.Add(new KillNumRankData());
                KillNumRankData data = dataList[i];
                data.id = camp.players[i].actor_id;
                data.name = camp.players[i].name;
                data.killNum = camp.players[i].kill;
                data.score = camp.players[i].score;
                BasePlayer player = WorldManager.singleton.GetPlayerById(data.id);
                data.curHpPercent = player != null && player.serverData != null ? player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f : 0f;
                //Logger.Error("CampKillNum player: " + camp.players[i].actor_id + ", id: " + data.id);
            }
            for (int i = len; i < dataList.Count; i++)
            {
                KillNumRankData data = dataList[i];
                data.reset();
            }
        }
    }

    //public void UpdatePlayerInfo(BasePlayer player)
    //{
    //    //Logger.Error("UpdatePlayerInfo");
    //    if (player == null || player.serverData == null) return;
    //    int camp = player.Camp;
    //    if (!m_killNumRankDataDic.ContainsKey(camp)) return;
    //    List<KillNumRankData> dataList = m_killNumRankDataDic[camp];
    //    for (int i = 0; i < dataList.Count; i++)
    //    {
    //        //Logger.Error("UpdatePlayerInfo player: " + player.PlayerId + ", id: " + dataList[i].id);
    //        if(player.PlayerId == dataList[i].id)
    //        {
    //            dataList[i].curHpPercent = player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f;
    //            break;
    //        }
    //    }

    //    if (SurvivalBehavior.singleton != null)
    //    {
    //        SurvivalBehavior.singleton.UpdateSurvivalPanel();
    //    }
    //}

    public List<KillNumRankData> GetKillNumRankDataList(int camp)
    {
        UpdateKillNumRankData();

        if (!m_killNumRankDataDic.ContainsKey(camp)) 
            return null;

        return m_killNumRankDataDic[camp];
    }

    public bool CheckRoundWin(int playerID)
    {
        if (string.IsNullOrEmpty(WorldManager.singleton.gameRule))
        {
            return false;
        }
        ConfigGameRuleLine rule = WorldManager.singleton.gameRuleLine;
        if (rule.WinType != "playerkill")
        {
            if (WorldManager.singleton.isViewBattleModel)
            {
                BasePlayer player = playerID > 0 ? WorldManager.singleton.GetPlayerById(playerID) : WorldManager.singleton.curPlayer;
                if (player != null)
                {
                    return player != null && player.Camp == _battle_state.last_win;
                }
                else
                {
                    Logger.Error("CheckRoundWin, selected player: "+playerID);
                }
            }
            return MainPlayer.singleton!=null && MainPlayer.singleton.serverData!=null && _battle_state.last_win == MainPlayer.singleton.Camp;
        }
        // 单人击杀按照个人击杀数最高的算胜利
        int myKill = 0;
        int maxKill = 0;
        int myID = 0;
        if (WorldManager.singleton.isViewBattleModel)
        {
            if (playerID > 0)
            {
                myID = playerID;
            }
            else
            {
                Logger.Error("CheckRoundWin, selected player: " + playerID);
            }
        }
        else
        {
            myID = WorldManager.singleton.myid;
        }
        foreach (var camp in _battle_info.camps)
        {
            foreach (var player in camp.players)
            {
                if (player.actor_id == myID)
                    myKill = player.kill;
                if (player.kill > maxKill)
                    maxKill = player.kill;
            }
        }
        return myKill >= maxKill;
    }

    public bool CheckGameWin(int playerID)
    {
        if (string.IsNullOrEmpty(WorldManager.singleton.gameRule))
        {
            return false;
        }
        ConfigGameRuleLine rule = WorldManager.singleton.gameRuleLine;
        if (rule.WinType != "playerkill")
        {
            if (WorldManager.singleton.isViewBattleModel)
            {
                BasePlayer player = playerID > 0 ?  WorldManager.singleton.GetPlayerById(playerID) : WorldManager.singleton.curPlayer;
                if (player != null)
                {
                    return player != null && (player.Camp == _battle_state.final_win || (WorldManager.singleton.gameRuleLine.IsDeuceWin == 1 && _battle_state.final_win == 0));
                }
                else
                {
                    Logger.Error("CheckGameWin, selected player: " + playerID);
                }
            }
            return MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && (_battle_state.final_win == MainPlayer.singleton.Camp || (WorldManager.singleton.gameRuleLine.IsDeuceWin == 1 && _battle_state.final_win == 0));
        }
        // 单人击杀应该只有一局定胜负的设定，直接返回回合胜负结果
        return CheckRoundWin(playerID);
    }

    public int getPlayerZombieData(int id)
    {
        if (zombie_data != null)
        {
            for (int i = 0; i < zombie_data.Length; i++)
            {
                if (zombie_data[i] == id)
                {
                    return i + 1;
                }
            }
        }
        return 0;
    }

    public SBattlePlayer GetKingSolo()
    {
        int kingKill = 0;
        foreach (var cfg in ConfigManager.GetConfig<ConfigKing>().m_dataArr)
            kingKill += cfg.SoloNeedKill;
        foreach (var camp in _battle_info.camps)
        { 
            foreach (var player in camp.players)
            {
                if (player.kill >= kingKill)
                    return player;
            }
        }
        return null;
    }

    public void ShowKickOutVotePanel(int pro, int con, int sec)
    {
        toc_fight_begin_vote_kick kickProto = Instance.m_proto;
        if (kickProto == null) return;

        bool showBtn = true;
        if (kickProto.kicked_player_id == MainPlayer.singleton.PlayerId || kickProto.sponsor_id == MainPlayer.singleton.PlayerId)
        {
            showBtn = false;
        }
        object[] param = new object[] { kickProto.kicked_player_name, kickProto.reason, kickProto.sponsor_name, pro, con, sec, showBtn };
        if (UIManager.IsOpen<PanelBattleKickInfo>())
        {
            UIManager.GetPanel<PanelBattleKickInfo>().refresh(param);
        }
        else
        {
            UIManager.PopPanel<PanelBattleKickInfo>(param);
        }
    }

    public bool isShowMVPAndACE
    {
        get { return !WorldManager.singleton.isHideModeOpen && !WorldManager.singleton.isSurvivalTypeModeOpen; }
    }

    private float m_tick;
    private void UpdateMvpAceData()
    {
        if (!isShowMVPAndACE) 
            return;

        m_tick += Time.deltaTime;
        if (m_tick >= 1)
            m_tick = 0;
        else
            return;

        List<SBattleCamp> tCamp = _battle_info.camps;
        for (int i = 0; i < tCamp.Count; ++i)
        {
            int num = tCamp[i].players.Count;
            for (int j = 0; j < num; j++ )
            {
                SBattlePlayer sbattlePlayer = tCamp[i].players[j];
                    m_mvpAndAceData.Add(
                                    new MvpAndAceData { ID = sbattlePlayer.actor_id, Score = sbattlePlayer.score, KillNum = sbattlePlayer.kill, DeathNum = sbattlePlayer.death_num, Name = sbattlePlayer.name });
            }
        }        

        m_mvpAndAceData.Sort((a, b) => { return b.Score != a.Score ? b.Score - a.Score : b.KillNum != a.KillNum ? b.KillNum - a.KillNum : a.DeathNum - b.DeathNum; });

        var sortForMvp = m_mvpAndAceData.FirstOrDefault();

        if (sortForMvp != null && sortForMvp.Score != 0)
        {           
            m_mvpId = sortForMvp.ID;
        }           
        m_mvpAndAceData.Sort((a, b) => { return b.KillNum != a.KillNum ? b.KillNum - a.KillNum : a.DeathNum - b.DeathNum; });

        MvpAndAceData temp;
        int length = m_mvpAndAceData.Count;
        for (int i = 0; i < length; i++)
        {
            temp = m_mvpAndAceData[i];
            if (i > 1)
            {
                break;
            }
            else if (i == 0 && temp.KillNum != 0)
            {
                m_gAceId = temp.ID;
            }
            else if (i == 1 && temp.KillNum != 0)
            {
                m_sAceId = temp.ID;
            }
        }
           
        m_mvpAndAceData.Clear();
    }

    public string GetSpecatorName(long id)
    {
        for(int i=0; i<_battle_info.spectators.Count; i++)
        {
            if (_battle_info.spectators[i].id == id)
                return _battle_info.spectators[i].name;
        }

        return "";
    }

    // MVP
    public int MVP
    {
        get { return m_mvpId; }
    }
    // 金色ACE
    public int GoldACE
    {
        get { return m_gAceId; }
    }
    // 银色ACE
    public int SilverACE
    {
        get { return m_sAceId; }
    }

    public bool CanVoteKick
    {
        get { return m_canVoteKick; }
    }

    public int bombHolder
    {
        set { battle_info.bomb_holder = value; }
        get { return battle_info.bomb_holder; }
    }

    public void clear()
    {
        AudioManager.StopUISoundLoop(); //关闭预警音效
        if (m_killNumRankDataDic != null)
        {
            foreach (var item in m_killNumRankDataDic)
            {
                item.Value.Clear();
            }
            m_killNumRankDataDic.Clear();
        }
    }
    //========================================= c to s
    public void TosSelectZombie(int idx)
    {
        NetLayer.Send(new tos_fight_select_zombie() { index = idx });
    }

    public void TosUseSkill(int code)
    {
        AudioManager.PlayFightUISound(AudioConst.bioUseSkill);
        var skillEffectParam = (MainPlayer.singleton != null && MainPlayer.singleton.skillMgr != null) ? MainPlayer.singleton.skillMgr.GetSkillEffectParam(code) : null;
        if (skillEffectParam == null)
            NetLayer.Send(new tos_fight_use_skill() { skill_id = code, pos = new float[0], target_ids = new int[0] });
        else
            NetLayer.Send(new tos_fight_use_skill() { skill_id = code, pos = skillEffectParam.pos, target_ids = skillEffectParam.target_ids });
    }

    public void TosUseWeaponSkill(int code)
    {
        var skillEffectParam = (MainPlayer.singleton != null && MainPlayer.singleton.skillMgr != null) ? MainPlayer.singleton.skillMgr.GetWeaponAwakenSkillEffectParam(code) : null;
        if (skillEffectParam == null)
            NetLayer.Send(new tos_fight_weapon_awaken_skill() { awaken_id = code, pos = new float[0], target_ids = new int[0] });
        else
            NetLayer.Send(new tos_fight_weapon_awaken_skill() { awaken_id = code, pos = skillEffectParam.pos, target_ids = skillEffectParam.target_ids });
    }

    public void TosDoubleGameReward()
    {
        NetLayer.Send(new tos_player_double_game_reward() { });
    }

    internal void AddPlayer(p_fight_role pRole)
    {
        if (_battle_info != null)
            _battle_info.AddPlayer(pRole);
    }

    internal void RemovePlayer(int playerID)
    {
        if (_battle_info != null)
            _battle_info.RemovePlayer(playerID);
    }

    //========================================= s to c
    static void Toc_fight_achieve(toc_fight_achieve proto)
    {
        //var player = WorldManager.singleton.GetPlayerById(proto.from);
        //if (player is MainPlayer)
        //{
        //    var achieve_str = "";
        //    foreach (var type in proto.achieve_list)
        //    {
        //        achieve_str += " " + type;
        //    }
        //    Logger.Log("============================player achieve: " + achieve_str);
        //}
        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_FIGHT_ACHIEVE, proto);
    }

    static void Toc_fight_state(toc_fight_state proto)
    {        
        Instance.UpdateBattleState(proto.state);

        if (Instance.battle_state.state==0 || Instance.battle_state.state == 1)
        {

            //fjs:2015.12.03 wenle's task
            //生存模式和战队副本战斗回合开始 如果已经选择过武器 则 bag_selected 状态保持不变  即这两个模式只能有一次机会选择武器
            if ((WorldManager.singleton.isSurvivalModeOpen || WorldManager.singleton.isDefendModeOpen || WorldManager.singleton.isChallengeModeOpen) && Instance.bag_selected)
            {

            }
            else
            {
                Instance.bag_selected = false;
            }
            Instance.battle_info.round_time = 0;

        }
        if (WorldManager.singleton.isZombieModeOpen && Instance.battle_state.state == 2)
        {
            Instance.battle_info.round_time = 0;
        }

        GameDispatcher.Dispatch<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, Instance.battle_state);
        if (WorldManager.singleton.isZombieTypeModeOpen)
        {
            if (Instance.battle_state.state == GameConst.Fight_State_GameStart && UIManager.IsOpen<PanelZombieSelect>())
            {
                var panel = UIManager.GetPanel<PanelZombieSelect>();
                panel.HidePanel();
            }
        }
        if(MainPlayer.singleton != null)
        {
            MainPlayer.singleton.m_BeAttckHarmList.Clear();
        }
    }

    static void Toc_fight_king_camp(toc_fight_king_camp proto)
    {
        Instance._battle_info.UpdateKingCampInfo(proto);
    }

    static void Toc_fight_bomb_info(toc_fight_bomb_info proto)
    {
        Instance._battle_info.SetBombInfo(proto);
    }

    //internal void TocUpdateBattleInfo(toc_fight_stat_info data)
    //{
    //    //var need_sync = _battle_state.round != _battle_info.round;
    //    //_battle_info.updateData(data);
    //    if (WorldManager.singleton.isSurvivalModeOpen)
    //    {
    //        //genCampKillNum();
    //        //if (SurvivalBehavior.singleton != null)
    //        //{
    //        //    //SurvivalBehavior.singleton.UpdateRound(data.round);   // 用消息替代
    //        //    SurvivalBehavior.singleton.UpdateSurvivalPanel();
    //        //}
    //    }
    //    //if (need_sync==true && _battle_state.round == _battle_info.round)//补发一次消息用以同步
    //    //{
    //    //    GameDispatcher.Dispatch<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, _battle_state);
    //    //}
    //    //OnFightStatInfoUpdate(_battle_info);
    //    //GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, _battle_info);
    //}
  
    static void Toc_player_fight_stat( CDict data )
    {
        PlayerSystem.fightStat = data.Dict("val");
        GameDispatcher.Dispatch<CDict>(GameEvent.PROXY_PLAYER_STAT_OF_FIGHT, data.Dict("val"));
    }

    static void Toc_fight_select_zombie(toc_fight_select_zombie data)
    {
        Instance.zombie_data = data.zombies;
        if (SceneManager.singleton.battleSceneLoaded && Instance.battle_state.state==GameConst.Fight_State_RoundStart)
        {
            if (UIManager.IsOpen<PanelZombieSelect>())
            {
                UIManager.GetPanel<PanelZombieSelect>().OnShow();
            }
            else
            {
                UIManager.PopPanel<PanelZombieSelect>(null, true);
            }
        }
    }

    static void Toc_fight_select_equip(toc_fight_select_equip data)
    {
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.SetParent(null);

        WorldManager.singleton.m_bagdata = data.list;
    }

    //static void Toc_fight_actor_die(toc_fight_actor_die proto)
    //{
    //    Instance.battle_info.PlayerKilled(proto);
    //}

    static void Toc_fight_join_spectator(toc_fight_join_spectator proto)
    {
        PlayerBattleModel.Instance._battle_info.AddSpecator(proto.spectator, false);
    }

    static void Toc_fight_leave_spectator(toc_fight_leave_spectator proto)
    {
        PlayerBattleModel.Instance._battle_info.RemoveSpecator(proto.player_id);
    }

    static void Toc_fight_king_solo(toc_fight_king_solo proto)
    {
        Instance.battle_info.UpdateKingSoloInfo(proto);
    }

    static void Toc_fight_score(toc_fight_score proto)
    {
        Instance.battle_info.UpdateScore(proto);
    }

    static void Toc_fight_energy(toc_fight_energy proto)
    {
        Instance.battle_info.UpdateEnergy(proto);
    }

    #region 投票踢人
    static void Toc_fight_can_vote_kick(toc_fight_can_vote_kick proto)
    {
        Instance.m_canVoteKick = proto.can_vote;
        if (proto.can_vote && UIManager.IsOpen<PanelBattleRecord>())
        {
            UIManager.GetPanel<PanelBattleRecord>().ShowVoteBtn(true);
        }

        if (!string.IsNullOrEmpty(proto.msg)) TipsManager.Instance.showTips(proto.msg);
    }

    static void Toc_fight_begin_vote_kick(toc_fight_begin_vote_kick proto)
    {
        //Logger.Error("s: " + proto.sponsor_id + ", k: " + proto.kicked_player_id + ", r: " + WorldManager.singleton.myid);
        Instance.m_proto = proto;
    }

    static void Toc_fight_vote_kick(toc_fight_vote_kick proto)
    {
        //Logger.Error("no: "+proto.con+", yes: "+proto.pro);
        toc_fight_begin_vote_kick kickProto = Instance.m_proto;
        if (kickProto == null) return;
        if (!UIManager.IsOpen<PanelBattle>())
        {
            Instance.hasBlockVotePanel = true;
            return;
        }
        Instance.ShowKickOutVotePanel(proto.pro, proto.con, proto.remainder_sec);
        //}
    }

    static void Toc_fight_vote_kick_result(toc_fight_vote_kick_result proto)
    {
        //Logger.Error("iskick: " + proto.is_kicked);
        if (UIManager.IsOpen<PanelBattleKickInfo>())
        {
            UIManager.GetPanel<PanelBattleKickInfo>().HidePanel();
        }
        Instance.m_proto = null;
        string msg = string.Concat("房间成员以", proto.pro, "票赞成 ", proto.con, "票反对 ，通过了将您请出房间的决议。");
        string reason = string.Concat("( 原因： ", proto.reason, " )");
        object[] param = new object[] { 2, msg, reason };
        if (proto.is_kicked && proto.kicked_player_id == WorldManager.singleton.myid)
        {
            TimerManager.SetTimeOut(2f, ()=> UIManager.PopPanel<PanelTransparentTip>(param, true));
            return;
        }
        if (proto.is_kicked)
        {
            msg = string.Concat(proto.kicked_player_name, " 已被踢出游戏");
        }
        else
        {
            //msg = string.Concat("未通过将 ", proto.kicked_player_name, " 踢出游戏的请求");            
            if (proto.reason != null)
            {
                var reasonType = Convert.ToInt32(proto.reason);
                var heroType = HeroCardDataManager.HeroNameArray[reasonType - 1];
                msg = string.Concat(proto.kicked_player_name + "拥有", heroType, "卡，可防踢一次");
            }
        }
        TipsManager.Instance.showTips(msg);
    }
    #endregion
}
