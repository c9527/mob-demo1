﻿using System;
using System.Collections.Generic;
using UnityEngine;

class EventModel : Singleton<EventModel>
{
    public string share_url;
    public string share_qcode_url;

    //========================================= c to s
    public void TosGetShareData()
    {
        NetLayer.Send(new tos_player_cash_gain_info());
    }

    public void TosLottery(int action=0)
    {
        if (action > 0)
        {
            ItemDataManager.ResetTempAddedRecord(ItemDataManager.UPDATE_REASON_LOTTERY);
        }
        NetLayer.Send(new tos_player_lottery() { lottery_type = action });
    }

    public void TosGetLotteryRank()
    {
        NetLayer.Send(new tos_player_get_lottery_rank());
    }

    public void TosExchangePoints(int op_code)
    {
        NetLayer.Send(new tos_player_exchange_points() { op_code = op_code });
    }

    //========================================= s to c
    static private void Toc_player_lottery(toc_player_lottery data)
    {
        if (BubbleManager.GetBubble(BubbleConst.LotteryFree) != data.free_times)
        {
            BubbleManager.SetBubble(BubbleConst.LotteryFree, data.free_times);
        }
    }
}

