﻿using System;
using System.Collections.Generic;
using UnityEngine;

class StrengthenModel : Singleton<StrengthenModel>
{
    public List<p_weapon_strength_info_toc> strengthen_data = new List<p_weapon_strength_info_toc>();
    public toc_player_weapon_train_info training_data;

    public List<int> _upgrade_list;

    private List<KeyValue<int,int>> _day_2_material_map;

    public Dictionary<int, accessory[]> AccessoryDic = new Dictionary<int, accessory[]>();

    public StrengthenModel()
    {
        _upgrade_list = new List<int>();
        _day_2_material_map = new List<KeyValue<int, int>>();
        var tdata = ConfigManager.GetConfig<ConfigMisc>().GetLine("weapon_days2material_cnt");
        if (tdata != null && !string.IsNullOrEmpty(tdata.ValueStr))
        {
            var temp = tdata.ValueStr.Split(';');
            for (int i = 0; temp != null && i < temp.Length;i++ )
            {
                var strs = temp[i].Split('#');
                _day_2_material_map.Add(new KeyValue<int,int>(){ key=int.Parse(strs[0]), value=int.Parse(strs[1])});
            }
        }
    }

    public List<int> upgrade_list
    {
        get { return _upgrade_list;}
    }

    public bool IsFullStrengthen(int code)
    {
        return IsFullStrengthen(getSStrengthenDataByCode(code));
    }

    public bool IsFullStrengthen(p_weapon_strength_info_toc data)
    {
        return data != null && data.state >= 3 && data.level >= 9;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="day">day小于或者等于0都被识别为无限期</param>
    /// <returns></returns>
    public int ConvertDayToMaterialNum(float day)
    {
        day = Mathf.Max(0, day);
        var result = 0;
        if (_day_2_material_map != null && _day_2_material_map.Count>0)
        {
            foreach (var vo in _day_2_material_map)
            {
                if (vo.key == day)
                {
                    result = vo.value;
                    break;
                }
            }
        }
        return result;
    }

    public int ConvertMaterialNumToDay(int num)
    {
        var result = 0;
        if (_day_2_material_map != null && _day_2_material_map.Count > 0)
        {
            foreach (var vo in _day_2_material_map)
            {
                if (vo.value == num)
                {
                    result = vo.key;
                    break;
                }
            }
        }
        return result;
    }

    public bool canUpgradeNow()
    {
        var result = false;
        foreach (int code in _upgrade_list)
        {
            var info = getSStrengthenDataByCode(code);
            var state_info = info!=null ? ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(info.weapon_id, info.state + 1) : null;
            if (state_info != null)
            {
                var is_enough = true;
                foreach (ItemInfo item_info in state_info.Consume)
                {
                    var sitem = ItemDataManager.GetDepotItem(item_info.ID);
                    if(sitem==null || sitem.item_cnt<item_info.cnt)
                    {
                        is_enough = false;
                        break;
                    }
                }
                var level = info != null ? info.level : 1;
                var _data = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(code);
                var state = info != null ? info.state : 0;
                var currentLevels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(state, _data.StrengthenGroup);
                var maxLevel = currentLevels.Length > 0 ? currentLevels[currentLevels.Length - 1].Level : 0;
                if (is_enough && ItemDataManager.IsDepotHasAndInTime(code) && ((level == maxLevel) && maxLevel < 9))
                {
                    result = is_enough;
                    break;
                }
            }
        }
        return result;
    }



    public bool isAccordUpgrade(int itemId)
    {
        return isAccordUpgrade(getSStrengthenDataByCode(itemId));
    }

    public bool isAccordUpgrade(p_weapon_strength_info_toc value)
    {
        var result = false;
        if (value != null)
        {
            var next_info = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(value.level + 1, value.state, ItemDataManager.GetItemWeapon(value.weapon_id).StrengthenGroup);
            if (next_info != null && next_info.State > value.state)
            {
                result = true;
            }
        }
        return result;
    }

    public p_weapon_strength_info_toc getSStrengthenDataByCode(int code)
    {
        p_weapon_strength_info_toc result = null;
        if(strengthen_data!=null)
        {
            for (int i = 0; i < strengthen_data.Count; i++)
            {
                if (strengthen_data[i].weapon_id == code)
                {
                    result = strengthen_data[i];
                    break;
                }
            }
            if (result == null)//查找为空时，判断如果是枪械就先初始化一个保存起来
            {
                var titem = code>0 ? ItemDataManager.GetItemWeapon(code) : null;
                if (titem != null)
                {
                    result = new p_weapon_strength_info_toc() { weapon_id = code, state = 0, level = 1, cur_exp = 0 };
                    strengthen_data.Add(result);
                }
            }
        }
        return result!=null ? result : new p_weapon_strength_info_toc() { weapon_id = code, state = 0, level = 1, cur_exp = 0 };
    }

    public CDItemData[] GenWeaponPartCDData(p_item data,int state=0)
    {
        if (data!=null)
        {
            return GenWeaponPartCDData(data.item_id, GetAccessoryListByWeaponId(data.item_id), state);
        }
        return null;
    }

    public CDItemData[] GenWeaponPartCDData(int itemId, accessory[] accessories, int state = 0)
    {
        if (itemId > 0)
        {
            CDItemData[] result = new CDItemData[4];
            for (int i = 0; accessories != null && i < accessories.Length; i++)
            {
                var index = accessories[i].pos - 1;
                if (index >= 0 && index < result.Length)
                {
                    var vo = result[index] = new CDItemData();
                    vo.UpdateData(accessories[i].item_id);
                    vo.sdata.accessory_values = accessories[i].accessory_values;
                    vo.extend = new KeyValue<int, int>(itemId, accessories[i].pos);
                }
            }
            for (int j = 0; j < result.Length; j++)
            {
                if (result[j] == null)
                {
                    var pos = j + 1;
                    result[j] = new CDItemData();
                    result[j].UpdateData(pos <= (state + 1) ? 0 : -1);
                    result[j].extend = new KeyValue<int, int>(itemId, pos);
                }
            }
            return result;
        }
        return null;
    }

    public Dictionary<string, KeyValue<int, int>> GenPartPropAddMap(p_item data)
    {
        var result = new Dictionary<string, KeyValue<int, int>>();
        var accessoryList = data != null?GetAccessoryListByWeaponId(data.item_id):null;
        if (accessoryList != null && accessoryList.Length > 0)
        {
            foreach (var spart in accessoryList)
            {
                var tpart = ItemDataManager.GetItem(spart.item_id) as ConfigItemAccessoriesLine;
                //部件属性
                var tprop_ary = tpart != null ? tpart.GetAddProp() : null;
                for (int i = 0; tprop_ary != null && i < tprop_ary.Length; i++)
                {
                    if (spart.accessory_values != null && i < spart.accessory_values.Length)
                    {
                        KeyValue<int, int> prop_value = null;
                        if (result.TryGetValue(tpart.DispProps[i], out prop_value) == false)
                        {
                            prop_value = new KeyValue<int, int>(tprop_ary[i].AddType, 0);
                            result.Add(tpart.DispProps[i], prop_value);
                        }
                        prop_value.value += Mathf.Abs(spart.accessory_values[i]);
                        result[tpart.DispProps[i]] = prop_value;
                    }
                }
            }
        }
        return result;
    }

    public accessory[] GetAccessoryListByWeaponId(int weaponId)
    {
        if (AccessoryDic.ContainsKey(weaponId))
        {
            return AccessoryDic[weaponId];
        }
        return null;
    }

    public Dictionary<string, KeyValue<int, int>> GenSuitPropAddMap(p_item data)
    {
        var result = new Dictionary<string, KeyValue<int, int>>();
        var accessoryList = data != null?GetAccessoryListByWeaponId(data.item_id):null;
        if (accessoryList != null && accessoryList.Length > 0)
        {
            var suit_part_map = new Dictionary<int, int>();
            foreach (var spart in accessoryList)
            {
                var tpart = ItemDataManager.GetItem(spart.item_id) as ConfigItemAccessoriesLine;
                //套装部件
                if (tpart != null && tpart.Suit > 0)
                {
                    var suit_part_count = 0;
                    if (suit_part_map.TryGetValue(tpart.Suit, out suit_part_count) == false)
                    {
                        suit_part_map.Add(tpart.Suit, 0);
                    }
                    suit_part_count += 1;
                    suit_part_map[tpart.Suit] = suit_part_count;
                }
            }
            //计算套装属性
            if (suit_part_map.Count > 0)
            {
                var suit_config = ConfigManager.GetConfig<ConfigAccessorySuit>();
                foreach (var obj in suit_part_map)
                {
                    var tsuit = suit_config.GetSuitAddPropData(obj.Key, obj.Value);
                    for (int i = 0; tsuit != null && tsuit.AddProps != null && i < tsuit.AddProps.Length; i++)
                    {
                        var prop_strs = tsuit.AddProps[i].Split('#');
                        KeyValue<int, int> suit_value = null;
                        if (result.TryGetValue(prop_strs[3], out suit_value) == false)
                        {
                            suit_value = new KeyValue<int, int>(int.Parse(prop_strs[1]), 0);
                            result.Add(prop_strs[3], suit_value);
                        }
                        suit_value.value += Mathf.Abs(int.Parse(prop_strs[2]));
                        result[prop_strs[3]] = suit_value;
                    }
                }
            }
        }
        return result;
    }

    private void WakeupTrainTimer()
    {
        var mini_cd = int.MaxValue;
        if (Instance.training_data.train_list!=null && Instance.training_data.train_list.Length > 0)
        {
            foreach (var sdata in Instance.training_data.train_list)
            {
                if (sdata.weapon > 0)
                {
                    var cd = (int)(TimeUtil.GetRemainTime((uint)(sdata.start_time + sdata.need_time)).TotalSeconds);
                    if (cd < mini_cd)
                    {
                        mini_cd = cd;
                        //break;
                    }
                }
            }
        }
        var train_bubble = mini_cd <= 0;
        if (train_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenTrain))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenTrain, train_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
        TimerManager.RemoveTimeOut(WakeupTrainTimer);
        if (mini_cd != int.MaxValue && mini_cd > 0)
        {//增加1.5秒误差
            TimerManager.SetTimeOut(mini_cd+1.5f, WakeupTrainTimer);
        }
    }

    //=====================c to s
    public void TosWakeupWeapon(long uid)
    {
        NetLayer.Send(new tos_player_weapon_awaken() { weapon_uid = uid });
    }

    public void TosSellWeaponPart(int[] uids)
    {
        NetLayer.Send(new tos_player_buyback_weapon_accessory() { accessorys = uids!=null ? uids : new int[0] });
    }

    public void TosUnEquipWeaponPart(int weapon_id,int pos )
    {
        NetLayer.Send(new tos_player_unequip_weapon_accessory() { weapon_id = weapon_id, pos = pos });
    }

    public void TosEquipWeaponPart(int weapon_id, int part_uid,int pos)
    {
        NetLayer.Send(new tos_player_equip_weapon_accessory() { weapon_id = weapon_id, accessory_uid = part_uid, pos=pos });
    }

    public void TosTestPartMeterial(int[] uids)
    {
        NetLayer.Send(new tos_player_test_accessory_meterial() { meterial_list = uids != null ? uids : new int[0] });
    }

    public void TosUpgradeWeapon(int uid)
    {
        NetLayer.Send(new tos_player_upgrade_weapon_strengthen_state() { weapon_uid=uid });
        //调试代码//Toc_player_upgrade_weapon_strengthen_state(new toc_player_upgrade_weapon_strengthen_state() { state = 1, weapon_id = ItemDataManager.GetItemByUid(uid).ID });
        //调试代码//Toc_player_weapon_strengthen_info(new toc_player_weapon_strengthen_info() { info = new p_weapon_strength_info_toc() {weapon_id=ItemDataManager.GetItemByUid(uid).ID, state=1, level=3} });
    }

    public void Tos_player_weapon_strengthen_data()
    {
        NetLayer.Send(new tos_player_weapon_strengthen_data());
    }

    public void Tos_player_weapon_train_info()
    {
        NetLayer.Send(new tos_player_weapon_train_info());
    }

    public void Tos_player_weapon_accessory_data()
    {
        NetLayer.Send(new tos_player_weapon_accessory_data());
    }

    public void Tos_player_start_weapon_train(int pos,int uid,int time_code,int value_code)
    {
        NetLayer.Send(new tos_player_start_weapon_train() { index=pos, weapon_uid=uid, time_opt=time_code, ratio_opt=value_code});
    }

    public void Tos_player_stop_weapon_train(int pos,int speedup=0)
    {
        NetLayer.Send(new tos_player_stop_weapon_train() { index=pos, quicken=speedup });
    }

    //=====================s to c
    static void Toc_player_weapon_awaken(toc_player_weapon_awaken rmo)
    {
        var strengthen_data = Instance.getSStrengthenDataByCode(rmo.weapon_id);
        if (strengthen_data != null)
        {
            if (strengthen_data.awaken_data == null)
            {
                strengthen_data.awaken_data = new p_awaken_data();
            }
            strengthen_data.awaken_data.level = rmo.level;
            strengthen_data.awaken_data.props = rmo.weapon_prop;
            if (rmo.ret_code)
            {
                strengthen_data.awaken_data.times = 0;
            }
            else
            {
                strengthen_data.awaken_data.times++;
            }
        }
        UpdateAwakeState();
    }

    static void Toc_player_weapon_accessory_data(toc_player_weapon_accessory_data data)
    {
        var dic = Instance.AccessoryDic;
        dic.Clear();
        var accessoryInfoList = data.list;
        for (int i = 0; i < accessoryInfoList.Length; i++)
        {
            var info = accessoryInfoList[i];
            dic.Add(info.weapon, info.accessories);
        }
    }

    static void Toc_player_weapon_accessories(toc_player_weapon_accessories data)
    {
        Instance.AccessoryDic[data.weapon] = data.accessories;
    }

    static void Toc_player_weapon_strengthen_data(toc_player_weapon_strengthen_data data)
    {
        Instance.strengthen_data = new List<p_weapon_strength_info_toc>(data.strengthen_list);
        Instance._upgrade_list.Clear();
        
        foreach(var sdata in Instance.strengthen_data)
        {

            if (Instance.isAccordUpgrade(sdata))
            {
                Instance._upgrade_list.Add(sdata.weapon_id);
            }
        }
        var upgrade_bubble = Instance.canUpgradeNow();

        if (upgrade_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenUpgrade, upgrade_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
        var awake_bubble = false;
        foreach (var sdata in Instance.strengthen_data)
        {
            if (ItemDataManager.CanWeaponAwaken(sdata.weapon_id))
            {
                awake_bubble = true;
//                Debug.LogError(sdata.weapon_id);
                break;
            }
        }
        if (awake_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenAwaken))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenAwaken, awake_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
    }

    static public void UpdateAwakeState()
    {
        var awake_bubble = false;
        foreach (var sdata in Instance.strengthen_data)
        {
            if (ItemDataManager.CanWeaponAwaken(sdata.weapon_id))
            {
                awake_bubble = true;
                Debug.LogError(sdata.weapon_id);
                break;
            }
        }
        if (awake_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenAwaken))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenAwaken, awake_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
    }

    static public void UpdateBubble()
    {
        var upgrade_bubble = Instance.canUpgradeNow();
        if (upgrade_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenUpgrade, upgrade_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
    }

    static void Toc_player_weapon_strengthen_info(toc_player_weapon_strengthen_info data)
    {
        var strengthen_data = Instance.getSStrengthenDataByCode(data.info.weapon_id);
        var old_level = 1;
        if (strengthen_data == null)
        {
            strengthen_data = data.info;
            Instance.strengthen_data.Add(strengthen_data);
        }
        else
        {
            old_level = strengthen_data.level;
        }
        strengthen_data.level = data.info.level;
        strengthen_data.state = data.info.state;
        strengthen_data.cur_exp = data.info.cur_exp;
        if (Instance.isAccordUpgrade(strengthen_data) && Instance.upgrade_list.IndexOf(strengthen_data.weapon_id)==-1)
        {
            Instance.upgrade_list.Add(strengthen_data.weapon_id);
        }
        var upgrade_bubble = Instance.canUpgradeNow();
        if (upgrade_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenUpgrade, upgrade_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
        //装备升级特效表现
        if (old_level < strengthen_data.level)
        {
            object[] args = new object[] { strengthen_data, old_level };
            if (UIManager.IsBattle())
            {
                PanelSettle.PushWeaponLevelup(args);
            }
            else
            {
                //不延时执行会造成层次显示混乱
                TimerManager.SetTimeOut(2f, () => UIManager.PopPanel<PanelWeaponLevelup>(args));
            }
        }
    }

    static void Toc_player_weapon_train_info(toc_player_weapon_train_info data)
    {
        Instance.training_data = data;
        Instance.WakeupTrainTimer();
    }

    static void Toc_player_upgrade_weapon_strengthen_state(toc_player_upgrade_weapon_strengthen_state data)
    {
        var strengthen_data = Instance.getSStrengthenDataByCode(data.weapon_id);
        if (strengthen_data == null)
        {
            UIManager.ShowTipPanel("无此装备强化数据，进阶失败");
            return;
        }
        strengthen_data.level = 1;
        strengthen_data.cur_exp = 0;
        strengthen_data.state = data.state;
        if (Instance.upgrade_list.IndexOf(strengthen_data.weapon_id) != -1)
        {
            Instance.upgrade_list.Remove(strengthen_data.weapon_id);
        }
        var upgrade_bubble = Instance.canUpgradeNow();
        if (upgrade_bubble != BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade))
        {
            BubbleManager.SetBubble(BubbleConst.StrengthenUpgrade, upgrade_bubble && PanelStrengthenMain.Lev >= PanelStrengthenMain.OpenLv);
        }
    }
}
