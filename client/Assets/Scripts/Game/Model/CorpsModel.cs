﻿using System;
using System.Collections.Generic;
using UnityEngine;

class CorpsModel : Singleton<CorpsModel>
{
    public float last_recruit_broadcast_time;
    public readonly float recruit_broadcast_min_interval = 100;

    public toc_corps_getlotterycounts lottery_data;
    public toc_corps_enter_mall shop_data;
    public toc_corps_mission_data task_data;

    public static int corpsVoucher;
    public static int corpsVoucherGet;

    //================================c to s
    public void TosEnterGuardTask()
    {
        NetLayer.Send(new tos_corps_start_instance());
        //Toc_corps_notify_mission(new toc_corps_notify_mission() { mission = new p_corps_mission() { id = 1, finisher = "OOXX", status = 2 } });
    }

    //public void TosGuardTaskAppointment()
    //{
    //    NetLayer.Send(new tos_corps_open_instance());
    //}

    public void TosGetActivenessReward(int code)
    {
        NetLayer.Send(new tos_corps_get_activeness_reward() { reward_id = code });   
    }

    public void TosGetTaskData()
    {
        NetLayer.Send(new tos_corps_mission_data());
    }

    public void TosGetShopData()
    {
        NetLayer.Send(new tos_corps_enter_mall());
    }

    public void TosUnlockShopItem(int box_id)
    {
        NetLayer.Send(new tos_corps_buymallbox() { box_id = box_id });
    }

    public void TosShopBidItem(int box_id,int mall_id,int price)
    {
        NetLayer.Send(new tos_corps_mallbid() { box_id = box_id, mall_id=mall_id, bid_price = price });
    }

    public void TosGetLotteryData(bool full=false)
    {
        NetLayer.Send(new tos_corps_getlotterycounts());
        if (full)
        {
            NetLayer.Send(new tos_corps_openlottery());
        }
    }

    public void DoLottery()
    {
        NetLayer.Send(new tos_corps_uselottery());
    }

    public void UnlockLotteryItem(int code)
    {
        NetLayer.Send(new tos_corps_buylotterybox() { box_id = code });
    }

    private void UpdateTaskRewardBubble()
    {
        if (task_data == null)
        {
            return;
        }
        var reward_num = 0;
        if (CorpsDataManager.IsJoinCorps())
        {
            var source_data = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().m_dataArr;
            if (source_data != null && source_data.Length > 0)
            {
                for (int i = 0; source_data != null && i < source_data.Length; i++)
                {
                    var tdata = source_data[i];
                    if (task_data.activeness >= tdata.Activeness)
                    {
                        var got_reward = false;
                        foreach (var code in task_data.rewarded_list)
                        {
                            if (code == tdata.ID)
                            {
                                got_reward = true;
                                break;
                            }
                        }
                        if (got_reward == false)
                        {
                            reward_num = 1;
                            break;
                        }
                    }
                }
            }
        }
        if (reward_num != BubbleManager.GetBubble(BubbleConst.CorpsTaskReward))
        {
            BubbleManager.SetBubble(BubbleConst.CorpsTaskReward, reward_num);
        }
    }

    //================================s to c
    static private void Toc_corps_getlotterycounts(toc_corps_getlotterycounts rmo)
    {
        Instance.lottery_data = rmo;
    }

    static private void Toc_corps_enter_mall(toc_corps_enter_mall rmo)
    {
        Instance.shop_data = rmo;
    }

    static private void Toc_corps_mission_data(toc_corps_mission_data rmo)
    {
        Instance.task_data = rmo;
        Instance.UpdateTaskRewardBubble();
        corpsVoucher = 0;
        corpsVoucherGet = 0;
        var source_data = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().m_dataArr;
        for (int i = 0; i < source_data.Length; i++)
        {
            var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(source_data[i].Reward);
            if (reward != null)
            {
                for (int j = 0; j < reward.ItemList.Length; j++)
                {
                    if (reward.ItemList[j].ID == 5052)
                    {
                        corpsVoucher += reward.ItemList[j].cnt;
                        //for (int e = 0; e < rmo.rewarded_list.Length; e++)
                        //{
                        //    if (rmo.rewarded_list[e] == source_data[i].ID)
                        //    {
                        //        corpsVoucherGet += reward.ItemList[j].cnt;
                        //    }
                        //}
                        if (rmo.mission_list[j].status == 2)
                        {
                            corpsVoucherGet += reward.ItemList[j].cnt;
                        }
                    }
                    
                }
                
            }
        }

        


    }

    static private void Toc_corps_notify_mission(toc_corps_notify_mission rmo)
    {
        var data = rmo.mission;
        if(Instance.task_data!=null)
        {
            Instance.task_data.activeness = rmo.activeness;
            for(var i=0;i<Instance.task_data.mission_list.Length;i++)
            {
                if (Instance.task_data.mission_list[i].id==data.id)
                {
                    Instance.task_data.mission_list[i] = data;
                    break;
                }
            }
        }
        if (data.status == 2)
        {
            var info = ConfigManager.GetConfig<ConfigCorpsMission>().GetLine(data.id);
            var str = "";
            if (data.type==1)
            {
                str = string.Format("{0}达成了“{1}”任务", data.finisher, info.Desc);
            }
            else
            {
                str = string.Format("本战队达成了“{0}”任务", info.Desc);
            }
            NetChatData.MockChatMsg(str,CHAT_CHANNEL.CC_CORPS);
            if (data.type == 1 && data.finisher == PlayerSystem.roleData.name)
            {
                //UIManager.PopPanel<PanelRewardItemTip>(new object[] { false, "恭喜你完成战队单人任务获得：" }, true).SetRewardItemList(new ItemInfo[] { new ItemInfo() { ID = 5014, cnt = info.CorpsCoin, day = 0 } });
                PanelRewardItemTipsWithEffect.DoShow(new ItemInfo[] { new ItemInfo() { ID = 5014, cnt = info.CorpsCoin, day = 0 } });
            }
            Instance.UpdateTaskRewardBubble();
        }
    }

    static private void Toc_corps_get_activeness_reward(toc_corps_get_activeness_reward data)
    {
        if (Instance.task_data != null)
        {
            var list = new List<int>(Instance.task_data.rewarded_list);
            var new_data = true;
            if(list.Count>0)
            {
                foreach (var id in list)
                {
                    if (id==data.reward_id)
                    {
                        new_data = false;
                        break;
                    }
                }
            }
            if (new_data)
            {
                list.Add(data.reward_id);
            }
            Instance.task_data.rewarded_list = list.ToArray();
            Instance.UpdateTaskRewardBubble();
        }
    }

    static private void Toc_corps_apply_enter(toc_corps_apply_enter rmo)
    {
        TipsManager.Instance.showTips("申请已发送！");
    }
}
