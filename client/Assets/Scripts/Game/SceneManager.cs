﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour
{
    //public class MatInfo2 : MonoBehaviour
    //{
    //    public EMat matValue = EMat.None;
    //    public ESceneItemType itemType = ESceneItemType.None;
    //}

    static public SceneManager singleton;

    private GameObject m_goMapPrefab;
    private Transform m_transSceneItem;
    private Transform m_transBuilding;

    private Transform m_transEffectRoot;
    private Transform m_transReclaimRoot;

    private int m_battleSceneID;
    private bool m_bBattleSceneLoaded;
    private bool m_clientSceneItemLoaded;
    private bool m_bSceneItemPreloaded;
    private List<GameObject> m_listCombineMesh;
    private Dictionary<int, List<GameObject>> m_dicSceneItem = new Dictionary<int, List<GameObject>>();
    private Dictionary<int, EMat> m_dicSceneItemMatType = new Dictionary<int, EMat>();

    private Dictionary<string, List<GameObject>> m_dicSceneEffect = new Dictionary<string, List<GameObject>>();

    private ConfigMapRuleLine m_mrl;
    private ConfigMapItemLine m_mapItemLine;

    private p_scene_item_toc[] m_arrPSceneItemToc;
    private int[] m_arrTotalSceneItem;

    private List<GameObject> m_listColliderGO;
    private List<Collider> m_listCollider;
    private Renderer[] m_arrSceneRenderer;
    private VtfIntCls m_lightmapSceneObjCnt = new VtfIntCls();

    private Dictionary<int, GameObject> m_dicDirection = new Dictionary<int, GameObject>();
    public List<Effect> m_daggerEffectList = new List<Effect>();

    void Awake()
    {
        if (singleton != null)
            Logger.Error("SceneManager is singleton!");

        singleton = this;

        Init();
    }

    public void Init()
    {
        // Layer Mask       
        if (GameSetting.LAYER_MASK_MAIN_PLAYER < 0)
            Debug.LogError("找不到 MainPlayer Layer!");
        if (GameSetting.LAYER_MASK_UI < 0)
            Debug.LogError("找不到 UI Layer!");

        // 忽略指定层间的，碰撞，Trigger
        for (int layerValue = 0; layerValue <= 31; layerValue++)
        {
            Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_AUTOTARGET, layerValue, true);
            //Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY, layerValue, true);
            Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND, layerValue, true);

            bool ignore = layerValue != GameSetting.LAYER_VALUE_MAIN_PLAYER && layerValue != GameSetting.LAYER_VALUE_BULLET;
            Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_OTHER_PLAYER, layerValue, ignore);
        }
        
        ConfigGameSettingLine cg = ConfigManager.GetConfig<ConfigGameSetting>().m_dataArr[0];

        // 物理碰撞层设置
        Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_BULLET, GameSetting.LAYER_VALUE_AIR_WALL, true);               // 忽略手雷等投掷武器与空气墙的碰撞
        Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_BULLET, GameSetting.LAYER_VALUE_MAIN_PLAYER, true);
        Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_BULLET, GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND, true);
        //Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_BULLET, GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY, true);
        Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_BULLET, GameSetting.LAYER_VALUE_AUTOTARGET, true);
    }

    public void ReleaseBattleSceneRes()
    {
        UISystem.isMinimizeRoom = false;        
        PanelBattleSurvivalScore.TryDeleteBossHPBar(null, true);
        PanelBattleDotaScore.TryDeleteBossHPBar(null, true);
        m_bBattleSceneLoaded = false;
        m_clientSceneItemLoaded = false;
        m_bSceneItemPreloaded = false;

        m_listCombineMesh = null;
        m_dicSceneItem.Clear();
        m_dicSceneItemMatType.Clear();
        m_transSceneItem = null;

        m_arrTotalSceneItem = null;
        m_arrPSceneItemToc = null;

        ResourceManager.Destroy(m_goMapPrefab);
        LightmapSettings.lightmaps = null;       
        LightmapSettings.lightProbes = null;

        try
        {
            BulletManager.Release();    // 释放扔出的手雷等

            EffectManager.singleton.ReleaseAllRes();

            WeaponManager.ReleaseCacheRes();

            WorldManager.singleton.ReleaseAllPlayerRes();

            if (SceneCamera.singleton != null)
                SceneCamera.singleton.Release();

            AudioManager.SetListener();

            ProjectileManager.Release();

            //ResourceManager.Clear(EResType.Battle, true);
        }
        catch(Exception exp)
        {
            Debug.LogError("ReleaseBattleSceneRes Exception : Msg = " + exp.Message + " Source = " + exp.Source + "\nStack = " + exp.StackTrace);
        }

        ResourceManager.GarbageCollect();
    }

#region Load Battle Scene Res

    internal void Toc_TotalSceneItems(toc_fight_total_scene_items proto)
    {
        List<int> list = new List<int>();
        for (int i = 0; i < proto.list.Length; i++)
        {
            if (list.Contains(proto.list[i]) == false)
                list.Add(proto.list[i]);
        }

        //Logger.Log("Toc_TotalSceneItems " + proto.list.Length + " NoRepeatNum = " + list.Count);

        m_arrTotalSceneItem = list.ToArray();

        LoadTotalSceneItems();
    }

    internal void Toc_ShowSceneItems(toc_fight_scene_items proto)
    {
        //Logger.Log("Toc_ShowSceneItems " + proto.list.Length);

        m_arrPSceneItemToc = proto.list;
        InitializeSceneItems();

#if UNITY_EDITOR

        Dictionary<int, List<p_scene_item_toc>> dic = new Dictionary<int, List<p_scene_item_toc>>();
        foreach (p_scene_item_toc p in proto.list)
        {
            List<p_scene_item_toc> list;
            if (dic.TryGetValue(p.id, out list) == false)
            {
                list = new List<p_scene_item_toc>();
                dic.Add(p.id, list);
            }
            list.Add(p);
        }

        string msg = "ShowItems \n";
        foreach (KeyValuePair<int, List<p_scene_item_toc>> kvp in dic)
        {
            List<string> listPos = new List<string>();

            foreach (p_scene_item_toc p in kvp.Value)
            {
                string strPos = p.pos.ToStr<float>();
                if (listPos.IndexOf(strPos) != -1)
                {
                    Logger.Error("Fuck Repeat Item id = " + p.id + " pos = " + strPos + " scale = " + p.scale.ToStr<float>());
                }
                else
                {
                    listPos.Add(strPos);
                }

                msg += "ID = " + p.id + " pos = " + strPos + " scale = " + p.scale.ToStr<float>() + "\n";

            }
        }
        //Logger.Log(msg);
#endif

    }

    public void LoadHllScene(Action callback = null)
    {
        ResourceManager.LoadScene(PathHelper.GetEmptyScenePath(), callback);
    }

    //返回重登
    static public void ReturnToLogin()
    {
        if (Driver.m_platform == EnumPlatform.web)
        {
            PlatformAPI.Call("reloadGame", SdkManager.ConvertGameServerID());
        }
        else if (Driver.m_platform == EnumPlatform.pc)
        {
            Application.Quit();
        }
        else
        {
            if (UIManager.IsBattle())
                singleton.ReleaseBattleSceneRes();

            singleton.LoadHllScene(() =>
            {
                UIManager.DestroyAllPanel();
                GuideManager.DestroyGuide(false);
                ResourceManager.GarbageCollect();
                UIManager.ShowPanel<PanelLogin>();
            });
        }
        clearData();
    }

    /// <summary>
    /// 清理数据
    /// </summary>
    private static void clearData()
    {
        PanelHallBattle.ClearLimitGift();
        AntiAddictionManager.Instance.isFirstLoading = true;
    }

    /// <summary>
    /// 打开充值
    /// </summary>
    /// <param name="subView"></param>
    public void OpenRecharge(string subView = null)
    {
        var rechargeOpen = true;
        if (Driver.m_platform == EnumPlatform.android && !GlobalConfig.serverValue.recharge_open_android)
            rechargeOpen = false;
        else if (Driver.m_platform == EnumPlatform.web && !GlobalConfig.serverValue.recharge_open_web)
            rechargeOpen = false;
        else if (Driver.m_platform == EnumPlatform.pc && !GlobalConfig.serverValue.recharge_open_pc)
            rechargeOpen = false;
        else if (Driver.m_platform == EnumPlatform.ios && !GlobalConfig.serverValue.recharge_open_ios)
            rechargeOpen = false;

        if (!rechargeOpen)
        {
            UIManager.ShowTipPanel("充值暂未开放", () =>
            {
                if (Driver.m_platform == EnumPlatform.web)
                    PlatformAPI.Call("openURL", "http://web.4399.com/qzyx/xwgg/gg/a784110.html", "_blank");
                else
                    Application.OpenURL("http://web.4399.com/qzyx/xwgg/gg/a784110.html");
            }, null, true, false, "查看说明");
            return;
        }

        if (Driver.isMobilePlatform)
            UIManager.ShowPanel<PanelRechargeMember>(subView == null ? null : new object[] { subView });
        else
        {
            if (SdkManager.IsIOSAccountLoginOther)
            {
                UIManager.PopPanel<PanelRechargeiOS>(null, true, null, LayerType.POP, 0.75f);
            }
            else
            {
                SdkManager.Pay(6);  //30元
            }
        }            
    }

    public void LoadBattleScene()
    {
        ResourceManager.Clear(EResType.Model, true);    // 清楚战斗外的UI加载的模型
        ResourceManager.LoadScene(PathHelper.GetBattleScenePath(m_battleSceneID), null);         
    }

    private void OnLevelWasLoaded(int level)
    {
        string sceneName = Application.loadedLevelName;

        EnableLightProbeKeyWord(false);

        if (sceneName.StartsWith("map_"))
        {
            Screen.lockCursor = true;
            LoadBattleSceneRes(m_battleSceneID);
        }
        else
        {
            Screen.lockCursor = false;
            if (sceneName == SceneConst.SCENE_EMPTY)
            {
                ResourceManager.Clear(EResType.Battle, true);    

                if (Util.NeedClearAll())
                {
                    ResourceManager.Clear(ResTag.Forever, true, true);
                    Logger.Warning("卸载所有资源 OnLevelWasLoaded");
                }

                ResourceManager.GarbageCollect();
                GameDispatcher.Dispatch(GameEvent.GAME_HALL_SCENE_LOADED);

                Logger.Warning("ExitBattle RamInfo = " + Util.GetMemoryInfo());
            }
        }
    }

    private void LoadBattleSceneRes(int iSceneID)
    {
        m_battleSceneID = iSceneID;

        string mapPath = PathHelper.GetScenePrefabPath(iSceneID);
        ResourceManager.LoadBattlePrefab(mapPath, (go, CRC) =>
        {
            if (go == null)
            {
                Debug.LogError("场景预设加载失败, id " + iSceneID);
                return;
            }

            // 返回的是上一次加载的场景则销毁
            if (go.name.Contains(m_battleSceneID.ToString()) == false)
            {
                ResourceManager.Destroy(go);
                return;
            }

            SceneCamera sc = new SceneCamera();
            SceneCamera.singleton.SetListener();

            ConfigMapListLine ml = ConfigManager.GetConfig<ConfigMapList>().GetLine(m_battleSceneID.ToString());
            if (ml != null)
            {
                if (MainPlayer.singleton == null || MainPlayer.singleton.attachedSceneCamera == false)
                {
                    SceneCamera.singleton.position = ml.SightPos;
                    SceneCamera.singleton.eulerAngles = ml.SightRot;
                }
            }

            m_goMapPrefab = go;
            m_transBuilding = go.transform.Find(SceneConst.SCENE_CHILD_BUILDING_PATH);

            
            m_transSceneItem = go.transform.Find(SceneConst.SCENE_CHILD_SCENEITEM_PATH);
            if (m_transSceneItem == null)
            {
                m_transSceneItem = new GameObject("SceneItem").transform;
                m_transSceneItem.SetParent(m_goMapPrefab.transform);
            }

            GameObject goEffectRoot = new GameObject("EffectRoot");
            m_transEffectRoot = goEffectRoot.transform;

            GameObject goReclaimRoot = new GameObject("ReclaimRoot");
            m_transReclaimRoot = goReclaimRoot.transform;
            m_transReclaimRoot.position = new Vector3(4399, 4399, 4399);

            LoadLightMap();    
            LoadLightProbe(ml != null ? ml.LightProbe : "");

            InitializeClientSceneItem();

//            Logger.Log(string.Format("send crc of map {0}:{1}", iSceneID, CRC));
//            if (Driver.m_platform == EnumPlatform.android || Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.ios)
//                NetLayer.Send(new tos_player_verify_crc()
//                {
//                    list = new tos_player_verify_crc.info[] {
//                        new tos_player_verify_crc.info(){ path = string.Concat(mapPath, ".assetbundle"), crc = CRC }
//                    }
//                });
            //ResourceManager.ClearDepend(mapPath);

            DeadPlayer.Instance.PreInstantiate();         

        }, EResType.Battle, true, true);

        //
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            ResourceManager.LoadBattlePrefab("SceneItem/" + "CheckShader", (go, crc) =>
            {
                if (go == null)
                    return;
                go.transform.position = new Vector3(0, -4399, 0);
                go.AddComponent<CheckShader>();
            });
        }
    }

    private void LoadLightMap()
    {
        Logger.RecordTime("LoadLightMap");

        string strLightMapPath = PathHelper.GetLightMapPath(m_battleSceneID);
        ResourceManager.LoadAsset(strLightMapPath, obj =>
        {
            if (obj == null)
            {
                Debug.LogWarning("加载LightMap失败 SceneID " + m_battleSceneID);
                return;
            }

            Texture2D tex2D = obj as Texture2D;
            LightmapData kLD = new LightmapData();
            kLD.lightmapFar = tex2D;
            LightmapSettings.lightmaps = new LightmapData[1] { kLD };

            Logger.PrintTime("LoadLightMap");

        }, true);
    }

    private void LoadLightProbe(string lightProbe)
    {
        if (lightProbe.IsNullOrEmpty())
            return;

        Logger.RecordTime("LoadLightProbe");

        string strLightProbePath = PathHelper.GetLightProbePath(m_battleSceneID, lightProbe);
        ResourceManager.LoadAsset(strLightProbePath, obj =>
        {
            EnableLightProbeKeyWord(obj != null);

            if (obj == null)
            {
                Debug.LogWarning("加载LightProbe失败 SceneID " + m_battleSceneID);
                return;
            }

            LightmapSettings.lightProbes = obj as LightProbes;
            Logger.PrintTime("LoadLightProbe");

            //Logger.Warning("LightProbe加载完成！");
        }, true);
    }

    private void InitializeClientSceneItem()
    {
        //if(WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            ConfigSceneItem csi = GlobalConfig.configSceneItem;
            ConfigMapItemLine cmil = ConfigManager.GetConfig<ConfigMapItem>().GetLine(m_battleSceneID);

            if (cmil == null)
            {
                Debug.LogWarning("躲猫猫没有配置酌定物件 ConfigMapItemLine null, id = " + m_battleSceneID);
                m_clientSceneItemLoaded = true;
                LoadTotalSceneItems();
            }
            else
            {
                Logger.RecordTime("InitializeClientSceneItem");

                HashSet<ResourceItem> setRI = new HashSet<ResourceItem>();
                for (int i = 0; i < cmil.DMMItemID.Length; i++)
                {
                    if(i < cmil.GameRules.Length)
                    {
                        string[] arrGR = cmil.GameRules[i].Split('|');
                        if (arrGR.IndexOf(WorldManager.singleton.gameRule) != -1)
                        {
                            ResourceItem ri = new ResourceItem("SceneItem/" + csi.GetLine(cmil.DMMItemID[i]).Model);
                            ri.SetType(EResType.Battle);
                            setRI.Add(ri);
                        }
                    }
                }

                var panel = UIManager.PopPanel<PanelLoadingTemp>(null, false, null, LayerType.Loading);
                ResourceManager.LoadMulti(setRI.ToArray(), null, () =>
                {
                    for (int i = 0; i < cmil.DMMItemID.Length; i++)
                    {
                        if (i < cmil.GameRules.Length)
                        {
                            string[] arrGR = cmil.GameRules[i].Split('|');
                            if (arrGR.IndexOf(WorldManager.singleton.gameRule) != -1)
                            {
                                int j = i;
                                ConfigSceneItemLine csil = csi.GetLine(cmil.DMMItemID[j]);
                                ResourceManager.LoadBattlePrefab("SceneItem/" + csil.Model, (go, crc) =>
                                {
                                    Transform trans = go.transform;
                                    trans.SetParent(m_transSceneItem);
                                    trans.position = cmil.DMMItemPos[j];
                                    trans.eulerAngles = cmil.DMMItemRot[j];
                                    trans.localScale = cmil.DMMItemScale[j];
                                    ProcessSceneItem(go, csil);
                                });
                            }
                        }
                    }

                    panel.HidePanel();

                    //Logger.Log("LoadClientSceneItem Complete! Num = " + cmil.DMMItemID.Length);
                    Logger.PrintTime("InitializeClientSceneItem", cmil.DMMItemID.Length.ToString());
                    m_clientSceneItemLoaded = true;
                    LoadTotalSceneItems();
                });
            }
        }
        //else
        //{
        //    //Logger.Log("LoadClientSceneItem Complete! Num = 00");
        //    m_clientSceneItemLoaded = true;
        //    LoadTotalSceneItems();
        //}
    }

    private void LoadTotalSceneItems()
    {
        if (m_clientSceneItemLoaded == false || m_arrTotalSceneItem == null)
        {
            //Logger.Log("LoadTotalSceneItems Return, m_clientSceneItemLoaded " + m_clientSceneItemLoaded + "m_arrTotalSceneItem = null is " + (m_arrTotalSceneItem == null));
            return;
        }

        if(m_arrTotalSceneItem.Length > 0)
        {
            Logger.RecordTime("LoadTotalSceneItems");

            ConfigSceneItem csi = ConfigManager.GetConfig<ConfigSceneItem>();

            List<ResourceItem> listRI = new List<ResourceItem>();
            for (int i = 0; i < m_arrTotalSceneItem.Length; i++)
            {
                ConfigSceneItemLine csil = csi.GetLine(m_arrTotalSceneItem[i]);
                if (csil == null)
                {
                    Logger.Error("策划配表出错 ConfigSceneItem 找不到 ID = " + m_arrTotalSceneItem[i]);
                    continue;
                }

                ResourceItem ri = new ResourceItem("SceneItem/" + csil.Model);
                ri.SetType(EResType.Battle);
                listRI.Add(ri);
            }

            var panel = UIManager.PopPanel<PanelLoadingTemp>(null, false, null, LayerType.Loading);
            ResourceManager.LoadMulti(listRI.ToArray(), null, () =>
            {
                Logger.PrintTime("LoadTotalSceneItems", listRI.Count.ToString());
                m_bSceneItemPreloaded = true;
                InitializeSceneItems();
                panel.HidePanel();
                OnBattleSceneLoaded();
            });
        }
        else
        {
            m_bSceneItemPreloaded = true;
            InitializeSceneItems();
            OnBattleSceneLoaded();
        }
    }

    private void InitializeSceneItems()
    {
        if (m_bSceneItemPreloaded == false || m_arrPSceneItemToc == null)
        {
            //Logger.Log("InitializeSceneItems Return, m_bSceneItemPreloaded = " + m_bSceneItemPreloaded + "m_bSceneItemPreloaded == null is " + (m_bSceneItemPreloaded == null));
            return;
        }

        Logger.Log("InitializeSceneItems");

        // 清除上一次物件
        foreach (KeyValuePair<int, List<GameObject>> kvp in m_dicSceneItem)
        {
            for (int i = 0; i < kvp.Value.Count; i++)
            {
                kvp.Value[i].TrySetActive(false);  // 暂时通过SetActive
            }
        }

        // 显示这次物件
        CreateSceneItem();

        // 替换所有物件Shader
        //if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        //{
        //    ResourceManager.LoadShader(GameConst.SHADER_LIGHTPROBE_TEX, (newShader) =>
        //    {
        //        m_transSceneItem.gameObject.ReplaceShader("", GameConst.SHADER_LIST_PARTICLE, newShader, true, true);
        //    });                               
        //}

        
    }

    private void CreateSceneItem()
    {
        Logger.RecordTime("CreateSceneItem");

        ConfigSceneItem csi = ConfigManager.GetConfig<ConfigSceneItem>();

        int createNum = 0;
        string createInfo = "\n";
        for (int i = 0; i < m_arrPSceneItemToc.Length; i++)
        {
            p_scene_item_toc p = m_arrPSceneItemToc[i];

            GameObject go = GetCacheSceneItem(p.id);
            if (go != null)
            {
                go.TrySetActive(true);
                GameObjectHelper.SetSceneItemPos(go, p.pos, p.scale);
            }
            else
            {
                ConfigSceneItemLine csil = GlobalConfig.configSceneItem.GetLine(p.id);
                if (csil == null)
                {
                    Logger.Error("找不到配置基 ConfigSceneItem, ID = " + p.id);
                    return;
                }

                ResourceManager.LoadBattlePrefab("SceneItem/" + csil.Model, (goSceneItem, crc) =>
                {                 
                    Transform transSceneItem = goSceneItem.transform;
                    transSceneItem.SetParent(m_transSceneItem);
                    GameObjectHelper.SetSceneItemPos(goSceneItem, p.pos, p.scale);

                    List<GameObject> listGO;
                    if (m_dicSceneItem.TryGetValue(p.id, out listGO) == false)
                    {
                        listGO = new List<GameObject>();
                        m_dicSceneItem.Add(p.id, listGO);
                    }
                    listGO.Add(goSceneItem);

                    ProcessSceneItem(goSceneItem, csil);

                    createNum++;
                    createInfo += "ID = " + p.id + " pos = " + p.pos.ToStr<float>() + " scale = " + p.scale.ToStr<float>() + "\n";
                });
            }
        }
        Logger.PrintTime("CreateSceneItem", "NeedNum:" + m_arrPSceneItemToc.Length + "CreanteNum = " + createNum + createInfo);
        //Logger.Log("CreateSceneItem Complete, NeedNum = " + m_arrPSceneItemToc.Length + " CreanteNum = " + createNum + createInfo);
    }

    private void ProcessSceneItem(GameObject goSceneItem, ConfigSceneItemLine csil)
    {
        Transform transSceneItem = goSceneItem.transform;

        // Layer
        goSceneItem.ApplyLayer(GameSetting.LAYER_VALUE_BUILDING);

        // 材质
        AddMatInfo(transSceneItem, (EMat)csil.Material, (ESceneItemType)csil.Type);

        // 使用LightProbe
        GameObjectHelper.UseLightProbe(transSceneItem);
    }

    private void OnBattleSceneLoaded()
    {
        Logger.Log("OnBattleSceneLoaded");

        m_bBattleSceneLoaded = true;

        PreprocessScenePrefab(m_goMapPrefab);

        GameDispatcher.Dispatch(GameEvent.GAME_BATTLE_SCENE_LOADED);
    }

    private void PreprocessScenePrefab(GameObject go)
    {
        Transform trans = go.transform;

        // Set Building Layer
        if (m_transBuilding != null)
            m_transBuilding.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_BUILDING, null, null, true);
        else
            Logger.Error("策划场景问题 Can't find Scene Child: " + SceneConst.SCENE_CHILD_BUILDING_PATH + ", SceneName: " + go.name);

        Transform transColliderBuilding = trans.Find(SceneConst.SCENE_CHILD_COLLIDER_BUILDING_PATH);
        if (transColliderBuilding != null)
            transColliderBuilding.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_BUILDING, null, null, true);
        else
            Logger.Error("策划场景问题 Can't find Scene Child: " + SceneConst.SCENE_CHILD_COLLIDER_BUILDING_PATH + ", SceneName: " + go.name);

        // Set SkyBox Layer
        if(m_transBuilding != null)
        {
            Transform transSkyBox = m_transBuilding.Find2("skybox", true);
            if (transSkyBox != null)
                transSkyBox.gameObject.layer = GameSetting.LAYER_VALUE_SKYBOX;
        }

        // Set AirWall Layer
        Transform transColliderAirWall = trans.Find(SceneConst.SCENE_CHILD_COLLIDER_AIRWALL_PATH);
        if (transColliderAirWall != null)
            transColliderAirWall.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_AIR_WALL, null, null, true);

        //Set Ladder Layer
        Transform transColliderLadder = trans.Find(SceneConst.SCENE_CHILD_COLLIDER_LADDER_PATH);
        if (transColliderLadder != null)
        {
            GameObjectHelper.VisitChildByTraverse(transColliderLadder, (g) =>
            {
                go.layer = GameSetting.LAYER_VALUE_DEFAULT;
                BoxCollider bc = GameObjectHelper.GetComponent<BoxCollider>(g);
                bc.isTrigger = true;
                g.AddMissingComponent<LadderEvent>();
            });
        }
        //set strongPoint layer
        var mapRule = ConfigManager.GetConfig<ConfigMapRule>().GetRule(SceneManager.singleton.curMapId, WorldManager.singleton.gameRule);
        if (mapRule != null && mapRule.UseStrongPoint == 1)
        {
            Transform bpp = trans.FindChild(SceneConst.SCENE_CHILD_OWPOINT_PATH);
            if (bpp != null)
            {
                ConfigMapListLine mapLine = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString());
                string[] radiusList = mapLine.StrongPointDistance.Split(';');
                string[] effectList = mapLine.StrongPointEffect.Split(';');
                if (radiusList == null)
                {
                    Logger.Log("刀锋据点地图 据点半径配置错误");
                }
                m_daggerEffectList.Clear();
                for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
                {
                    GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                    Transform tranBombTip = goChildBp.transform.FindChild("JDPointTip");
                    float radius = float.Parse(radiusList[childIndex]);
                    if (tranBombTip != null)
                    {
                       // m_bombPointPosList.Add(new Vector3(tranBombTip.position.x, tranBombTip.position.z, int.Parse(goChildBp.name)));
                        ThroughTheWallProp prop = new ThroughTheWallProp()
                        {
                            imgProp = new RectTransformProp() { scale = new Vector3(2, 2, 1) },
                            isShowDistance = true,
                            distancePos = new Vector2(0, 60),
                            targetRadius = radius,
                            distance = WorldManager.singleton.gameRuleLine.MapIconVisibleRange <= 0 ? float.MaxValue : WorldManager.singleton.gameRuleLine.MapIconVisibleRange,
                        };
                        string pointTipName = "";
                        if( goChildBp.name == "1" )
                            pointTipName = "a";
                        else if( goChildBp.name == "2" )
                            pointTipName = "b";
                        else if (goChildBp.name == "3")
                            pointTipName = "c";
                        else if (goChildBp.name == "4")
                            pointTipName = "d";
                        else if (goChildBp.name == "5")
                            pointTipName = "e";
                        UIThroughTheWall.createUI().play(AtlasName.Battle, pointTipName, tranBombTip, Vector3.zero, prop);
                    }

                    if (mapLine != null)
                    {
                        SphereCollider collider = goChildBp.GetComponent<SphereCollider>();
                        if (collider == null)
                        {
                            collider = goChildBp.AddComponent<SphereCollider>();
                            collider.radius = radius;
                        }
                        collider.isTrigger = true;
                        var pointEvent = goChildBp.AddMissingComponent<PointTriggerEvent>();
                        pointEvent.type = "Dagger_ow";
                        pointEvent.id = childIndex+1;
                        goChildBp.TrySetActive(false);
                        var effectName = getDaggerPointEffectName(effectList[childIndex]);
                        EffectManager.singleton.GetEffect<Effect>(effectName, (g) =>
                        {
                            if (g == null || goChildBp == null) return;
                            g.Attach(goChildBp.transform);
                            float r = radius / 2;
                            g.transform.localScale = new Vector3(r, r, r);
                            g.transform.localPosition = new Vector3(0f, -0.3f, 0f);
                            m_daggerEffectList.Add(g);
                        });

                    }
                }
            }
        }

        //Set Spring Layer
        mapRule = ConfigManager.GetConfig<ConfigMapRule>().GetRule(SceneManager.singleton.curMapId, WorldManager.singleton.gameRule);
        if (mapRule != null && mapRule.UseSpring == 1)
        {
            Transform transColliderSpring = trans.Find(SceneConst.SCENE_CHILD_COLLIDER_SPRING_PATH);
            if (transColliderSpring != null)
            {
                GameObjectHelper.VisitChildByTraverse(transColliderSpring, (g) =>
                {
                    g.layer = GameSetting.LAYER_VALUE_DEFAULT;

                    if (g.name.StartsWith("slot"))
                        return;

                    BoxCollider bc = GameObjectHelper.GetComponent<BoxCollider>(g);
                    bc.isTrigger = true;
                    SpringEvent spring = g.AddMissingComponent<SpringEvent>();
                    //弹簧格式   spring-特效名-直径-跳跃高度
                    string[] springParams = g.name.Split('-');
                    if (springParams == null || (springParams.Length != 4 && springParams.Length != 6))
                    {
                        Logger.Error("弹簧名称【" + g.name + "】格式错误，名称格式：特效名-直径-特效高度位移-跳跃高度[-强制方向-速度]");
                    }
                    else
                    {
                        string effect = springParams[0];
                        float diameter = 0;
                        float.TryParse(springParams[1], out diameter);
                        float offset = 0;
                        float.TryParse(springParams[2], out offset);
                        float jumpHeight = 0;
                        float.TryParse(springParams[3], out jumpHeight);
                        if (string.IsNullOrEmpty(effect) || diameter <= 0 || jumpHeight <= 0)
                        {
                            Logger.Error("弹簧名称【" + g.name + "】输入有误，名称格式：特效名-直径-特效高度位移-跳跃高度[-强制方向-速度]");
                            return;
                        }
                        int forceMove = 0;
                        float forceSpeed = 0;
                        if (springParams.Length == 6)
                        {
                            int.TryParse(springParams[4], out forceMove);
                            float.TryParse(springParams[5], out forceSpeed);
                        }
                        spring.Init(effect, diameter, offset, jumpHeight, forceMove == 1, g.transform.forward, forceSpeed);
                    }
                });
            }
        }

        // Set DeathBox Layer
        Transform transColliderDeathBox = trans.Find(SceneConst.SCENE_CHILD_COLLIDER_DEATHBOX_PATH);
        if(transColliderDeathBox != null)
        {
            transColliderDeathBox.VisitChild((db) =>
            {
                Collider collider = db.GetComponent<Collider>();
                if (collider != null)
                    collider.isTrigger = true;
                db.layer = GameSetting.LAYER_VALUE_DEATH_BOX;
            }, false);
            //transColliderDeathBox.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_DEATH_BOX, null, null, true);
        }

        // Disable Light
        Transform transLightGroup = trans.Find(SceneConst.SCENE_CHILD_LIGHTGROUP_PATH);
        if (transLightGroup != null)
        {
            if(!(curMapId >= 9000))
                transLightGroup.gameObject.TrySetActive(false);
        }
            
        else
            Logger.Error("策划场景问题 Can't find Scene Child: " + SceneConst.SCENE_CHILD_LIGHTGROUP_PATH + ", SceneName: " + go.name);

        // Set Scene Effect Layer
        Transform transSceneEffect = trans.Find(SceneConst.SCENE_CHILD_EFFECT_PATH);
        if (transSceneEffect != null)
        {
            transSceneEffect.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_SCENE_EFFECT);

            //添加场景特效
            for (int i = 0, imax = transSceneEffect.childCount; i < imax; ++i)
            {
                List<GameObject> goList = null;
                Transform child = transSceneEffect.GetChild(i);
                if (!m_dicSceneEffect.TryGetValue(child.name, out goList))
                {
                    goList = new List<GameObject>();
                    m_dicSceneEffect.Add(child.name, goList);
                }
                goList.Add(child.gameObject);
            }
        }

        // Replace Building Shader
        ResourceManager.LoadShader(GameConst.SHADER_LIGHTMAP, (newShader) =>
        {
            if (m_transBuilding != null)
            {
                List<string> excludeShader = new List<string>(GameConst.SHADER_LIST_PARTICLE);
                excludeShader.Add(GameConst.SHADER_SKYBOX);
                excludeShader.Add(GameConst.SHADER_TEXTURE);
                m_transBuilding.gameObject.ReplaceShader("", excludeShader, newShader, false, true);
                //Logger.Warning("Replace Building Shader, LightMapShader = " + newShader.name);

                // Test
                //Renderer[] arrRender = m_transBuilding.GetComponentsInChildren<Renderer>();
                //string msg = "";
                //foreach(Renderer r in arrRender)
                //{
                //    msg += r.sharedMaterial.name + " " + r.sharedMaterial.shader.name + "\n";
                //}
                //Logger.Warning("Replaced Building Shader, " + msg);

                // 记录场景renderer,用来检测shader是否被外挂修改
                m_arrSceneRenderer = m_transBuilding.gameObject.GetComponentsInChildren<Renderer>();
                m_lightmapSceneObjCnt.val = 0;
                for (int i = 0; i < m_arrSceneRenderer.Length; i++)
                {
                    if (m_arrSceneRenderer[i].sharedMaterial.shader.name == GameConst.SHADER_LIGHTMAP)
                    {
                        m_lightmapSceneObjCnt.val += 1;
                    }
                }
            }
        });

       

        // 记录场景碰撞体，用来检测是否被外挂关闭
        Transform transColliderGroup = trans.Find(SceneConst.SCENE_CHILD_COLLIDERGROUP);
        m_listColliderGO = new List<GameObject>();
        m_listCollider = new List<Collider>();
        if(transColliderGroup != null)
        {
            Collider collider = null;
            transColliderGroup.VisitChild((goCollider) =>
            {
                collider = goCollider.GetComponent<Collider>();
                if(collider != null)
                {
                    m_listColliderGO.Add(goCollider);
                    m_listCollider.Add(collider);
                }

            }, false);
        }

        //场景里的目标点
        m_dicDirection.Clear();
        Transform transDirectionGroup = trans.Find(SceneConst.SCENE_CHILD_DIRCTION_PATH);

        if (transDirectionGroup != null)
        {
            transDirectionGroup.VisitChild((goDirection) =>
            {
                if (goDirection != null)
                {
                    int id = -1;
                    int.TryParse(goDirection.name, out id);
                    if(!m_dicDirection.ContainsKey(id))
                    {
                        m_dicDirection.Add(id, goDirection);
                    }
                }
            }, false);
        }
    }

    private string getDaggerPointEffectName(string value)
    {
        switch(value)
        {
            case "0":
                return EffectConst.DAGGEROW_GRAY;
            case "1":
                return EffectConst.DAGGEROW_BLUE;
            case "2":
                return EffectConst.DAGGEROW_RED;
        }
        return EffectConst.DAGGEROW_GRAY;
    }

    private void AddMatInfo(Transform root, EMat mat, ESceneItemType itemType)
    {
        root.VisitChild((child) =>
        {
            Collider collider = child.GetComponent<Collider>();
            if (collider != null)
            {
                var mi = child.AddMissingComponent<MatInfo>();
                mi.matValue = mat;
                m_dicSceneItemMatType[mi.gameObject.GetInstanceID()] = mat;
                //mi.itemType = itemType;
            }
        }, true);
    }

    public GameObject GetDireciton(int id)
    {
        GameObject go = null;
        m_dicDirection.TryGetValue(id, out go);
        return go;
    }

    private GameObject GetCacheSceneItem(int id)
    {
        GameObject ret = null;

        List<GameObject> listGO;
        if (m_dicSceneItem.TryGetValue(id, out listGO))
        {
            for (int i = 0; i < listGO.Count; i++)
            {
                if (listGO[i].activeSelf == false)
                {
                    ret = listGO[i];
                    break;
                }
            }
        }

        return ret;
    }

#endregion
  
    public void ExitBattleScene()
    {
        if (MainPlayerControlInputReceiver.singleton != null)
        {
#if UNITY_WEBPLAYER || UNITY_STANDALONE

#else
        MainPlayerControlInputReceiver.singleton.enabled = false;
#endif
        }

        m_listCollider = null;
        m_listColliderGO = null;
        m_dicSceneEffect.Clear();
        m_dicDirection.Clear();
        m_arrSceneRenderer = null;
        m_lightmapSceneObjCnt.val = 0;

        ReleaseBattleSceneRes();

        LoadHllScene();

        GameDispatcher.Dispatch(GameEvent.GAME_EXIT_BATTLE_SCENE);
    }

    static public int GetMaterialType(GameObject go)
    {
        if (go == null)
        {
            Logger.Error("GetMaterialEffect arg 'go' is null");
            return 0;
        }

        EMat mt;
        if (singleton.m_dicSceneItemMatType.TryGetValue(go.GetInstanceID(), out mt))
        {
            return (int)mt;
        }

        MatInfo mi;
        if ((mi = go.GetComponent<MatInfo>()) != null)
            mt = mi.matValue;
        else
        {
            string colliderName = go.name.ToLower();

            if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_HEAD))
            {
                mt = EMat.Head;
            }
            else if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_ARM)
                  || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_HAND)
                  || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_LEG)
                  || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_FOOT))
            {
                mt = EMat.Limbs;
            }
            else if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_CHEST))
            {
                mt = EMat.Body;
            }
        }

        singleton.m_dicSceneItemMatType[go.GetInstanceID()] = mt;
        return (int)mt;
    }

    static public Vector3 GetGroundPoint(Vector3 pos)
    {
        // 求某点的垂直落地点
        RaycastHit kRH;
        if (Physics.Raycast(pos, Vector3.down, out kRH, 100f, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER))
        {
            return kRH.point + new Vector3(0, 0.01f, 0);
        }

        return pos;
    }

    public void ClearBattleSceneLoadedTag()
    {
        m_bBattleSceneLoaded = false;
    }

    public void ShowSceneEffect(string[] effects, bool isShow)
    {
        if (effects == null || m_dicSceneEffect.Count == 0)
            return;

        List<GameObject> goList = null;
        for(int i = 0; i < effects.Length; ++i)
        {
            if (m_dicSceneEffect.TryGetValue(effects[i], out goList))
           {
               if (goList != null)
               {
                   for (int j = 0, jmax = goList.Count; j < jmax; ++j )
                       goList[j].TrySetActive(isShow);
               }
           }
        }

    }

    #region Get Set

    public bool battleSceneLoaded
    {
        get { return m_bBattleSceneLoaded; }
    }

    public Transform effectRoot
    {
        get { return m_transEffectRoot; }
    }

    public Transform reclaimRoot
    {
        get { return m_transReclaimRoot;}
    }

    public int curMapId
    {
        get
        {
            return m_battleSceneID;
        }
        set
        {
            m_battleSceneID = value;
            m_mapItemLine = ConfigManager.GetConfig<ConfigMapItem>().GetLine(m_battleSceneID);
        }
    }

    public GameObject curSceneGo
    {
        get
        {
            return m_goMapPrefab;
        }
    }

    public List<Collider> sceneCollider
    {
        get { return m_listCollider; }
    }

    public Renderer[] sceneRenderer
    {
        get { return m_arrSceneRenderer; }
    }

    public int lightmapSceneObjCnt
    {
        get { return m_lightmapSceneObjCnt.val; }
    }

    public List<GameObject> sceneColliderGO
    {
        get { return m_listColliderGO; }
    }

    public ConfigMapItemLine curMapItemLine
    {
        get { return m_mapItemLine; }
    }

#endregion

    #region Debug

    public void EnableUseLightProbe(bool value)
    {
        Renderer[] arrRender = m_transSceneItem.GetComponentsInChildren<Renderer>();
        Logger.Warning("CheckDMMItem Renderer Cnt = " + arrRender.Length);

        for (int i = 0; i < arrRender.Length; i++)
        {
            Renderer r = arrRender[i];

            if (r.sharedMaterial.shader.name != GameConst.SHADER_LIGHTPROBE_TEX)
                Logger.Error("DMMItem Shader is " + r.sharedMaterial.shader.name);

            if (r.useLightProbes == false)
                Logger.Error("DMMItem useLightProbe is false! " + r.name);
        }

        for (int i = 0; i < arrRender.Length; i++)
        {
            Renderer r = arrRender[i];
            r.useLightProbes = value;
        }
    }

    public void RefreshItemMat()
    {
        Logger.Warning("RefreshItemMat");

        Renderer[] arrRender = m_transSceneItem.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < arrRender.Length; i++)
        {
            Renderer r = arrRender[i];
            r.material = new Material(r.sharedMaterial);
            r.useLightProbes = true;
        }
    }

    public void ShowLPMsg()
    {
        Logger.Warning("LightProbe = " + (LightmapSettings.lightProbes == null ? "null" : LightmapSettings.lightProbes.name));

        Renderer[] arrRender = m_transSceneItem.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < arrRender.Length; i++)
        {
            Renderer r = arrRender[i];
            string msg = "";

            MeshFilter mi = r.gameObject.GetComponent<MeshFilter>();
            if (mi != null && mi.mesh != null)
                msg += "MeshName = " + mi.mesh.name;

            msg += " MatName = " + r.sharedMaterial.name + " Shader = " + r.sharedMaterial.shader.name;

            Logger.Log(msg);
        }
    }

    public void EnableLightProbeKeyWord(bool value)
    {
        if (value)
        {
            Shader.EnableKeyword(GameConst.SHADER_KEYWORD_LIGHTPROBE_ON);
            Shader.DisableKeyword(GameConst.SHADER_KEYWORD_LIGHTPROBE_OFF);
        }
        else
        {
            Shader.EnableKeyword(GameConst.SHADER_KEYWORD_LIGHTPROBE_OFF);
            Shader.DisableKeyword(GameConst.SHADER_KEYWORD_LIGHTPROBE_ON);
        }
    }

    #endregion
   
}
