﻿using System.Collections.Generic;
using System;
using DockingModule;
using UnityEngine;
using Debug = UnityEngine.Debug;

/// <summary>
/// 存放非战斗玩家数据
/// </summary>
static class PlayerSystem
{
    public static long roleId;
    public static RoleData roleData;
    public static toc_player_survival_data survival_data;
    static toc_player_default_fight_roles.role[] _equip_roles;
    static public toc_player_buff_list.buff[] buff_data;
    public static CDict fightStat;
    static private Dictionary<int, int> _recharge_data = new Dictionary<int, int>();
    static int _hero_sex;

    public static int serverId
    {
        get {
            var bit = (int)(roleId & 0x0F) + 1;
            var _serverId = (roleId >> 4) & ((1 << bit) - 1);
            return (int)_serverId;
        }
    }
    public static int delay_levelup_effect_value;

    private static int _cm_timer;
    public static List<toc_player_tag_list.p_tag> m_tagList;
    public static int this_login_time;
    public static int last_intbattle_time;
    public static int last_outbattle_time;

    public static string GetRoleNameFullByActorId(string name, int actorId)
    {
        var player = WorldManager.singleton.GetPlayerById(actorId);
        return GetRoleNameFull(name, player != null ? player.Pid : 0);
    }

    public static int GetRealServerId(long id)
    {
        var bit = (int)(id & 0x0F) + 1;
        var serverId = (id >> 4) & ((1 << bit) - 1);
        return (int)serverId;
    }

    public static bool IsCrossServerRole(long id)
    {
        var serverId1 = GetRealServerId(id);
        var serverId2 = GetRealServerId(roleId);
        return serverId1 != serverId2;
    }

    public static string GetRoleSeverName(long id)
    {
        var bit = (int)(id & 0x0F) + 1;
        var serverId = (id >> 4) & ((1 << bit) - 1);

        var serverName = "";
        var serverList = GlobalConfig.serverList;
        for (int i = 0; i < serverList.Length; i++)
        {
            if (serverList[i].ServerId == serverId)
            {
                if (!string.IsNullOrEmpty(serverList[i].ShortServerName))
                    serverName = serverList[i].ShortServerName;
                else
                    serverName = serverId.ToString();
                break;
            }
        }
//        if (string.IsNullOrEmpty(serverName))
//            serverName = (serverId % 4 + 1) + "服";
        return serverName;

    }

    public static string GetRoleSeverCNName(long id)
    {
        var bit = (int)(id & 0x0F) + 1;
        var serverId = (id >> 4) & ((1 << bit) - 1);

        var serverName = "";
        var serverList = GlobalConfig.serverList;
        for (int i = 0; i < serverList.Length; i++)
        {
            if (serverList[i].ServerId == serverId)
            {
                if (!string.IsNullOrEmpty(serverList[i].ServerName))
                    serverName = serverList[i].ServerName;
                else
                    serverName = serverId.ToString();
                break;
            }
        }
        //        if (string.IsNullOrEmpty(serverName))
        //            serverName = (serverId % 4 + 1) + "服";
        return serverName;

    }

    public static string GetRoleNameFull(string name, long id)
    {
        if (!Driver.IsCrossServerChannel())
            return name;
        if (id < 0 && !Driver.IsAiFakeCrossServerName() || id >= 0 && !Driver.IsRoleCrossServerName())
            return name;
        var roleServerName = GetRoleSeverName(id);
        if (string.IsNullOrEmpty(roleServerName))
            return name;
        return string.Format("[{0}]{1}", roleServerName, name);
    }

    private static void Toc_login_select(toc_login_select proto)
    {
        roleId = proto.id;
        roleData = proto.data;
        if (roleData.level >= 10)
        {
            SdkManager.ShowAssistant();
        }
        ActivityFrameDataManager.newPlayerInfo_isNew = roleData.create_time >= int.Parse(ConfigMisc.GetValue("new_player_create_time"));
        this_login_time = TimeUtil.GetNowTimeStamp();
        last_intbattle_time = -1;
        last_outbattle_time = -1;
        
        SendMsg("fight_stat");

#if UNITY_ANDROID
        // DBA临时需求
        string path = Application.persistentDataPath + "/dba-info";
        System.IO.File.WriteAllText(path, roleId.ToString(), System.Text.Encoding.ASCII);
#endif
    }

    static public bool CheckMoney(int value, EnumMoneyType cur_type)
    {
        var result = false;
        if (roleData != null)
        {
            if ((cur_type == EnumMoneyType.COIN && roleData.coin >= value)
                || (cur_type == EnumMoneyType.MEDAL && roleData.medal >= value)
                || (cur_type == EnumMoneyType.CORPS_COIN && roleData.corps_coin >= value)
                || (cur_type == EnumMoneyType.DIAMOND && roleData.diamond >= value)
                || (cur_type == EnumMoneyType.EVENTCOIN_002 && roleData.eventcoin_002 >= value)
                || (cur_type == EnumMoneyType.CASH && roleData.cash >= value)
                || (cur_type == EnumMoneyType.ACTIVE_COIN && roleData.active_coin >= value)
                )
            {
                result = true;
            }
        }
        return result;
    }

    static public bool CheckMoney(int value, EnumMoneyType cur_type, string msg, Action onOk = null, Action onCancel = null, bool showCloseBtn = true, bool showCancel = false, string okLabel = null, string cancelLabel = null)
    {
        var result = CheckMoney(value, cur_type);
        if (result == false)
        {
            if (string.IsNullOrEmpty(msg))
            {
                if (cur_type == EnumMoneyType.COIN)
                {
                    msg = "金币不足，参与战斗快速获得金币";
                }
                else if (cur_type == EnumMoneyType.MEDAL)
                {
                    msg = "荣誉不足，参与战斗快速获得荣誉";
                }
                else if (cur_type == EnumMoneyType.CORPS_COIN)
                {
                    msg = "战队币不足，战队捐献可以获得战队币";
                }
                else
                {
                    msg = "钻石不足，是否充值？";
                }
            }
            if (onOk == null)
            {
                if (cur_type == EnumMoneyType.COIN)
                {
                    okLabel = "前往战斗";
                    onOk = () => UIManager.ShowPanel<PanelHallBattle>();
                }
                else if (cur_type == EnumMoneyType.MEDAL)
                {
                    okLabel = "前往战斗";
                    onOk = () => UIManager.ShowPanel<PanelHallBattle>();
                }
                else if (cur_type == EnumMoneyType.CORPS_COIN)
                {
                    okLabel = "前往捐献";
                    onOk = () => UIManager.PopPanel<PanelCorpsDonate>(null, true, null);
                }
                else
                {
                    okLabel = "充值";
                    onOk = () => SceneManager.singleton.OpenRecharge();
                }
            }
            UIManager.ShowTipPanel(msg, onOk, onCancel, showCloseBtn, showCancel, okLabel, cancelLabel);
        }
        return result;
    }

    static public bool isEquipRole(int uid)
    {
        var result = false;
        if (_equip_roles != null && _equip_roles.Length > 0)
        {
            foreach (var sdata in _equip_roles)
            {
                if (sdata.uid == uid)
                {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    static public ConfigItemRoleLine getEquipRoleInfo(int camp)
    {
        ConfigItemRoleLine info = null;
        if (camp > 0 && _equip_roles != null)
        {
            foreach (var sdata in _equip_roles)
            {
                if (sdata.camp == camp)
                {
                    info = ItemDataManager.GetItemByUid(sdata.uid) as ConfigItemRoleLine;
                    break;
                }
            }
        }
        return info;
    }

    static public int heroSex
    {
        get { return _hero_sex; }
    }

    static public bool IsFirstBuy(int code)
    {
        var result = 0;
        _recharge_data.TryGetValue(code, out result);
        return result == 0;
    }

    public static void WakeupCMSystem(object running)
    {
        if ((bool)running == false)
        {
            TimerManager.RemoveTimeOut(WakeupCMSystem);
        }
        WakeupCMTimer(running);
        var online_time = GlobalConfig.clientValue.today_online_time;
        var cd = 0;
        var second = 60;
        if (online_time >= 180 * second)//在线时长超过3小时
        {
            UIManager.ShowTipPanel("您的游戏时间已超过3小时，收益减半，超过5小时候，将不再获得任何收益。");
            //UIManager.ShowTipPanel("您已进入不健康游戏时间，请您暂离游戏进行适当休息和运动，合理安排您的游戏时间。",()=>Application.OpenURL(SdkManager.gameUrl),null,false);
            //TimerManager.SetTimeOut(10, () => Application.OpenURL(SdkManager.gameUrl));
            return;
        }
        else if (online_time >= 175 * second)//在线时长超过2小时55分
        {
            cd = 180 * second - online_time;
            UIManager.ShowTipPanel("你累计在线时间将满3小时");
        }
        else if (online_time >= 120 * second)//在线时长超过2小时
        {
            cd = 175 * second - online_time;
            UIManager.ShowTipPanel("你累计在线时间已满2小时");
        }
        else if (online_time >= 60 * second)//在线时长超过1小时
        {
            cd = 120 * second - online_time;
            UIManager.ShowTipPanel("你累计在线时间已满1小时");
        }
        else//在线时长不足1小时
        {
            cd = 60 * second - online_time;
        }
        TimerManager.SetTimeOut(cd, WakeupCMSystem, true);
    }

    static private void WakeupCMTimer(object running)
    {
        if (_cm_timer != 0 && (bool)running == false)
        {
            _cm_timer = 0;
            TimerManager.RemoveTimeOut(WakeupCMTimer);
        }
        var now_time = TimeUtil.GetNowTime();
        var last_time = TimeUtil.GetTime(GlobalConfig.clientValue.last_online_stamp);
        var total_online_time = GlobalConfig.clientValue.today_online_time;
        if (last_time.Year != now_time.Year || last_time.DayOfYear != now_time.DayOfYear)//数据记录跨天
        {
            last_time = (bool)running ? new DateTime(now_time.Year, now_time.Month, now_time.Day) : now_time;
            total_online_time = 0;
        }
        if ((bool)running == true)
        {
            total_online_time += (int)(now_time - last_time).TotalSeconds;
        }
        GlobalConfig.clientValue.last_online_stamp = TimeUtil.GetTimeStamp(now_time);
        GlobalConfig.clientValue.today_online_time = total_online_time;
        //if (total_online_time >= 180 * 60)//一天内在线已超过3小时
        //{
        //    if ((now_time - last_time).TotalSeconds >= 120 * 60)
        //    {
        //        Logger.Log("==========防沉迷系统数据更新：上次在线时间-" + last_time + "，超时游戏后离线超过2小时，重置在线游戏时间");
        //        GlobalConfig.clientValue.last_online_stamp = TimeUtil.GetTimeStamp(now_time);
        //        GlobalConfig.clientValue.today_online_time = 0;
        //    }
        //}
        //else
        //{
        //    GlobalConfig.clientValue.last_online_stamp = TimeUtil.GetTimeStamp(now_time);
        //    GlobalConfig.clientValue.today_online_time = total_online_time;
        //}
        Logger.Log("==========防沉迷系统数据更新：最后在线时间-" + TimeUtil.GetTime(GlobalConfig.clientValue.last_online_stamp) + "  当天在线总时长-" + GlobalConfig.clientValue.today_online_time);
        GlobalConfig.SaveClientValues(new string[] { "last_online_stamp", "today_online_time" });
        _cm_timer = TimerManager.SetTimeOut(10 * 60, WakeupCMTimer, true);
    }

    public static bool IsRankMatchOpen(int level = -1)
    {
        var lv = roleData.level;
        if (level != -1)
            lv = level;
        return lv >= 15;
    }

    public static bool IsCorpsMatchOpen(int level = -1)
    {
        var lv = roleData.level;
        if (level != -1)
        {
            lv = level;
        }
        var misc = ConfigManager.GetConfig<ConfigMisc>().GetLine("join_corps_limit_level");
        int lvl = misc.ValueInt;
        return lv >= lvl;
    }

    //=====================c to s
    static public void TosGetRechargeData()
    {
        NetLayer.Send(new tos_player_recharge_info());
    }

    static public void TosGetBuffData()
    {
        NetLayer.Send(new tos_player_buff_list());
    }

    static public void TosGetDefaultRoles()
    {
        NetLayer.Send(new tos_player_default_fight_roles());
    }

    static public void TosSetDefaultRole(int uid, int camp = 0)
    {
        NetLayer.Send(new tos_player_set_default_role() { role_uid = uid, camp = camp });
    }

    static public void TosRenameRoleName(string name)
    {
        NetLayer.Send(new tos_player_rename() { name = name });
    }

    static public void ToRenameCorpsName(int panel, int icon, int color, string corps_name)
    {
        NetLayer.Send(new tos_corps_change_name() { pannel = panel, icon = icon, color= color, corps_name = corps_name});
    }

    static public void TosSetHeroSex(int sex)
    {
        NetLayer.Send(new tos_player_set_hero_sex { sex = sex });
    }

    //=====================s to c
    static private void Toc_player_recharge_info(toc_player_recharge_info data)
    {
        _recharge_data.Clear();
        if (data.recharge_id_list != null && data.recharge_id_list.Length > 0)
        {
            foreach (var code in data.recharge_id_list)
            {
                _recharge_data[code] = 1;
            }
        }
    }

    private static void Toc_player_attribute(CDict data)
    {
        data = data.Get("val", null) as CDict;
        string key = data.Str("key");
        object value = data.Get("val", null);
        switch (key)
        {
            case "honor_joke":
                roleData.honor_joke = Convert.ToInt32(value);
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
                break;
            case "honor":
                roleData.honor = Convert.ToInt32(value);
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
                break;
            case "add_exp":
                roleData.exp = Convert.ToInt32(value);
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
                break;
            case "add_money":
                var type = data.Get("typ", "").ToString();
                if (type == "coin")
                    roleData.coin = Convert.ToInt32(value);
                else if (type == "diamond")
                    roleData.diamond = Convert.ToInt32(value);
                else if (type == "medal")
                    roleData.medal = Convert.ToInt32(value);
                else if (type == "cash")
                    roleData.cash = Convert.ToInt32(value);
                else if (type == "corps_coin")
                    roleData.corps_coin = Convert.ToInt32(value);
                else if (type == "blue_stone")
                    roleData.blue_stone = Convert.ToInt32(value);
                else if (type == "purple_stone")
                    roleData.purple_stone = Convert.ToInt32(value);
                else if (type == "white_stone")
                    roleData.white_stone = Convert.ToInt32(value);
                else if (type == "eventcoin_002")
                {
                    roleData.eventcoin_002 = Convert.ToInt32(value);
                    FakeItem.EVENTCOIN_002.item_cnt = roleData.eventcoin_002;
                    ItemDataManager.UpdateItemData(FakeItem.EVENTCOIN_002);
                }
                else if (type == "active_coin")
                {
                    roleData.active_coin = Convert.ToInt32(value);
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
                }
                else
                    Logger.Warning("未处理的货币变更  type:" + type + ", value:" + value);
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
                break;
            case "level_up":
                SdkManager.LogRoleLevel((roleData.level + 1).ToString(), SdkManager.m_serverId);
                Logger.Error("等级" + (roleData.level + 1));
                var old_level = roleData.level;
                roleData.level = Convert.ToInt32(value);
                if (roleData.level == 10)
                {
                    SdkManager.ShowAssistant();
                }

                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE); 
                GuideManager.TryContinueGuide();
                if (roleData.level > old_level)
                {
                    if (Driver.m_platform == EnumPlatform.web || Driver.m_platform == EnumPlatform.pc)
                        DockingService.SendRoleLevelChangeLog(roleData.level.ToString());
                    if (Driver.m_platform == EnumPlatform.ios)
                    {
                        if (roleData.level > 10 && roleData.level < 101 && roleData.level % 5 == 0)
                        {
                            if (SdkManager.isGoComment())
                            {
                                SdkManager.m_logFrom = 4;
                                SdkManager.getStarComment();
                            }
                        }
                    }
                    GameDispatcher.Dispatch(GameEvent.MAINPLAYER_LEVEL_UP, old_level, roleData.level);
                    if (WorldManager.singleton.fightEntered || UIManager.IsOpen<PanelSettle>())
                    {
                        delay_levelup_effect_value = old_level;
                    }
                    else
                    {
                        delay_levelup_effect_value = 0;
                        UIManager.PopPanel<PanelRoleLevelup>(new object[] { old_level, roleData.level }, false);
                    }
                }


                break;
            default:
                Logger.Warning("未处理的player属性变更  key:" + key + ", value:" + value);
                break;
        }
    }

    private static void Toc_player_rename(toc_player_rename data)
    {
        roleData.name = data.name;
        TipsManager.Instance.showTips("改名成功");
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INFO_CHANGE);
    }

    private static void Toc_player_set_icon(toc_player_set_icon data)
    {
        roleData.icon = data.icon;
        GameDispatcher.Dispatch(GameEvent.UI_ROLE_ICON_CHANGED);
    }

    static private void Toc_player_default_fight_roles(toc_player_default_fight_roles data)
    {
        _equip_roles = data.roles;
        _hero_sex = data.hero_sex;
        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_DEFAULT_ROLE_UPDATED);
    }

    static private void Toc_player_set_hero_sex(toc_player_set_hero_sex data)
    {
        _hero_sex = data.sex;
        if (UIManager.IsOpen<PanelDepot>())
        {
            UIManager.GetPanel<PanelDepot>().UpdateView();
        }
    }

    static private void Toc_player_set_default_role(toc_player_set_default_role data)
    {
        foreach (var sdata in _equip_roles)
        {
            if (sdata.camp == data.camp)
            {
                sdata.uid = data.role_uid;
                break;
            }
        }
        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_DEFAULT_ROLE_UPDATED);
    }

    static public void MockTocPlayerBuff()
    {
        Toc_player_buff_list(new toc_player_buff_list() { list = PlayerSystem.buff_data });
    }

    static private void Toc_player_buff_list(toc_player_buff_list data)
    {
        var temp = new List<toc_player_buff_list.buff>();
        for (int i = 0; i < data.list.Length; i++)
        {
            if (TimeUtil.GetNowTimeStamp() < data.list[i].time_limit)
            {
                temp.Add(data.list[i]);
            }
        }
        buff_data = temp.ToArray();
    }

    static private void Toc_player_survival_data(toc_player_survival_data data)
    {
        survival_data = data;
    }

    public static void SendMsg(string key, object value = null)
    {
        CDict proto = new CDict();
        proto["typ"] = "player";
        proto["key"] = key;
        if (value != null)
            proto["val"] = value;
        NetLayer.SendPack(proto);
    }

    static private void Toc_player_tag_list(toc_player_tag_list data)
    {
        m_tagList = new List<toc_player_tag_list.p_tag>();
        for (int i = 0; i < data.tag_list.Length; i++)
        {
            m_tagList.Add(data.tag_list[i]);
        }
        return;
    }

    static private void Toc_player_update_tag(toc_player_update_tag data)
    {
        bool has = false;
        for (int i = 0; i < m_tagList.Count; i++)
        {
            if (m_tagList[i].tag_id == data.tag_id)
            {
                m_tagList[i].value = data.value;
                has = true;
            }
        }
        if (!has)
        {
            m_tagList.Add(new toc_player_tag_list.p_tag() { tag_id = data.tag_id, value = data.value });
        }
    }

    static internal int m_closeCode = 0;
    static internal string m_closeMsg = "";
    static private void Toc_player_close_agent(toc_player_close_agent data)
    {
        m_closeCode = data.reason;
        m_closeMsg = data.msg;
    }
}
