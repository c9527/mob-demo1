﻿using UnityEngine;

public class AutoAimSpace : MonoBehaviour
{
    private CapsuleCollider m_collider;
    private float m_radius;
    private float m_height;
    private float m_minRadius;
    private float m_minHeight;
    private Transform m_tran;
    //private Transform m_tranCamera;
    private float m_fovTan;
    private float m_f1;

    private const float UPDATE_INTERVAL = 1.0f;
    private BasePlayer m_player;
    private float m_tick;

    void Start()
    {
        var autoTargetHeight = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0].autoAimTargetHeight;
        m_minRadius = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0].autoAimColliderRadius;
        m_minHeight = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0].autoAimColliderHeight;

        m_fovTan = Mathf.Tan(SceneCamera.singleton.camera.fieldOfView * 0.5f * Mathf.PI / 180f) * autoTargetHeight;
        m_f1 = (UIManager.m_resolution.y*0.5f);
        m_tran = transform.parent;
        //m_tranCamera = SceneCamera.singleton.camera.transform;
        m_collider = GetComponent<CapsuleCollider>();
        m_collider.center = new Vector3(0,0.8f,0);
        m_radius = 0.39f;
        m_height = 1f;
        
    }

    public void Init(BasePlayer player)
    {
        m_player = player;
    }

    private void Update()
    {
        if (Time.timeSinceLevelLoad - m_tick < UPDATE_INTERVAL)
            return;

        m_tick = Time.timeSinceLevelLoad;

        if (m_tran == null || m_collider == null || SceneCamera.singleton == null 
            || m_player == null || m_player.alive == false || m_player.isRendererVisible == false
            || MainPlayer.singleton == null || MainPlayer.singleton.alive == false)
            return;

        var scale = m_fovTan * (m_tran.position - SceneCamera.singleton.position).magnitude / m_f1;
        if (scale > 1000)
            return;

        m_collider.radius = Mathf.Max(m_minRadius, m_radius * scale);
        m_collider.height = Mathf.Max(m_minHeight, m_height * scale);

        //m_collider.radius = Mathf.Max(m_minRadius, m_radius * scale);
        //m_collider.height = Mathf.Max(m_minHeight, m_height * scale);
    }
}
