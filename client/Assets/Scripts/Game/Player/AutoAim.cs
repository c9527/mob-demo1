﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class AutoAim
{
    private static bool m_enable = true;

    public static bool IsCanAutoAim
    {
        get
        {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            return false;
#else
            return m_enable && !IsSpecialWeapon && !IsSpecialGameRule && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
#endif
        }
    }

    public static bool Enable
    {
        get { return m_enable; }
        set { m_enable = value; }
    }

    private static Transform m_autoBattleTarget;
    private static Transform m_autoBattleTargetHead;
    private static Transform m_autoBattleTargetPelvis;
    private static Transform m_autoAimTarget;
    private static Transform m_autoAimTargetHead;
    private static Transform m_autoAimTargetPelvis;
    private static Camera m_cameraScene;
    private static Vector2 m_curAimPos;     //当前准星的位置
    private static PanelBattle m_panelBattle;

    private static bool m_lastIsAutoAiming; //保存上一次IsAutoAiming的值，用来判断变化点
    private static bool m_operated;     //从吸附结束开始，是否操作过移动和转向

    /// <summary>
    /// 已锁定目标
    /// </summary>
    public static bool IsLockTarget
    {
        get { return AutoAimTarget != null; }
    }
    /// <summary>
    /// 枪口正在偏移归位中
    /// </summary>
    public static bool IsAutoAiming
    {
        get { return m_fAimBackCenterDelay > 0; }
    }

    /// <summary>
    /// 当cd值小于等于0，或者大于0但没有移动过时，认为不再cd中，允许吸附
    /// </summary>
    private static bool IsNotCding
    {
        get { return m_cdTime <= 0 || (m_cdTime > 0 && !m_operated); }
    }

    private static bool IsSpecialWeapon
    {
        get
        {
            if (MainPlayer.singleton == null)
                return false;
            var weapon = MainPlayer.singleton.curWeapon;
            //return weapon is WeaponGrenade || weapon is WeaponKnife || weapon is WeaponPaw;
            return (weapon is WeaponGrenade) || 
                (
                    (weapon is WeaponKnife || weapon is WeaponPaw) &&
                    (
                        weapon.canAutoAim == false ||
                        (/*(MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.ZOMBIE && GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_KNIFE_AUTOAIM) == GameConst.KNIFE_AUTOAIM_CLOSE) ||*/
                        (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_ZOMBIE_AUTOAIM) == GameConst.ZOMBIE_AUTOAIM_CLOSE))
                    )
                );
        }
    }

    private static bool IsSpecialGameRule
    {
        get { return WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE); }
    }



    /// <summary>
    /// 自己是否在后座力影响时期
    /// </summary>
    private static bool IsRecoil
    {
        get
        {
            if (MainPlayer.singleton == null || MainPlayer.singleton.curWeapon == null || MainPlayer.singleton.curWeapon.weaponSpread == null)
                return false;
            return MainPlayer.singleton.curWeapon.weaponSpread.IsRecoil();
        }
    }

    private static bool IsOperation
    {
        get
        {
            if (m_panelBattle == null)
                m_panelBattle = UIManager.GetPanel<PanelBattle>();
            return m_panelBattle != null && m_panelBattle.IsOperation();
        }
    }

    private static float m_fAimBackCenterDelay;

    private static float m_aimingTime;   //吸到位后的持续时间
    private static float m_cdTime;       //吸到位后并持续完后的cd时间

    public static void Init()
    {
        m_cameraScene = SceneCamera.singleton.camera;
        m_panelBattle = null;
        SetCurAimPos(Vector2.zero);
    }

    public static void Clear()
    {
        m_panelBattle = null;
        m_cameraScene = null;
        AutoAimTarget = null;
    }

    private static void ResetAimingTime()
    {
        m_aimingTime = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0].autoAimTraceTime;
        if (m_aimingTime == 0)
            ResetCdTime();
    }

    private static void ResetCdTime()
    {
        m_cdTime = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0].autoAimCoolDownTime;
        if (m_cdTime > 0)
        {
            m_operated = false;
            //            AutoAimTarget = null;
        }
    }

    /// <summary>
    /// 设置当前准星位置
    /// </summary>
    /// <param name="aimUIPos">准星的UI坐标值</param>
    private static void SetCurAimPos(Vector2 aimUIPos)
    {
        m_curAimPos = aimUIPos + new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
    }

    /// <summary>
    /// 当前准星位置
    /// </summary>
    public static Vector2 GetCurAimPos()
    {
        return m_curAimPos - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
    }

    /// <summary>
    /// 当前准星指向的射线
    /// </summary>
    public static Ray CurAimRay
    {
        get { return SceneCamera.singleton.camera.ScreenPointToRay(m_curAimPos); }
    }

    /// <summary>
    /// 自动瞄准的目标方向的射线
    /// </summary>
    public static Ray AutoAimTargetRay
    {
        get { return m_cameraScene.ScreenPointToRay(TargetScreenPos); }
    }

    /// <summary>
    /// 屏幕中心指向的射线
    /// </summary>
    public static Ray ScreenCenterRay
    {
        get { return m_cameraScene.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f)); }
    }

    /// <summary>
    /// 准星在UI上的目标坐标位置
    /// </summary>
    public static Vector2 CrossHairUIPos
    {
        get { return (Vector2)TargetScreenPos - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f); }
    }

    public static Transform AutoBattleTarget
    {
        set
        {
            if (value != m_autoBattleTarget)
            {
                if (value == null)
                {
                    m_autoBattleTargetHead = null;
                    m_autoBattleTargetPelvis = null;
                }
                else
                {
                    m_autoBattleTargetHead = GameObjectHelper.FindChildByTraverse(value, ModelConst.BIP_NAME_HEAD);
                    m_autoBattleTargetPelvis = GameObjectHelper.FindChildByTraverse(value, ModelConst.BIP_NAME_PEVIS);
                }
            }
            m_autoBattleTarget = value;
        }
        get { return m_autoBattleTarget; }
    }

    /// <summary>
    /// 自动瞄准的对象
    /// </summary>
    public static Transform AutoAimTarget
    {
        set
        {
            if (value != m_autoAimTarget)
            {
                if (value == null)
                {
                    m_autoAimTargetHead = null;
                    m_autoAimTargetPelvis = null;
                }
                else
                {
                    m_autoAimTargetHead = GameObjectHelper.FindChildByTraverse(value, ModelConst.BIP_NAME_HEAD);
                    m_autoAimTargetPelvis = GameObjectHelper.FindChildByTraverse(value, ModelConst.BIP_NAME_PEVIS);
                }
            }
            m_autoAimTarget = value;

        }
        get { return m_autoAimTarget; }
    }

    private static Vector3 TargetHeadPosition
    {
        get { return m_autoAimTargetHead.position + new Vector3(0f, -0.12f, 0f); }
    }

    private static Vector3 TargetPelvisPosition
    {
        get { return m_autoAimTargetPelvis.position + new Vector3(0f, -0.08f, 0f); }
    }

    /// <summary>
    /// 自动瞄准点的屏幕坐标
    /// </summary>
    public static Vector3 TargetScreenPos
    {
        get
        {
            if (m_autoAimTarget != null)
            {
                //选取最近的部位作为自动瞄准点
                var centerPos = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);

                var headPos = m_cameraScene.WorldToScreenPoint(TargetHeadPosition);
                var bodyPos = m_cameraScene.WorldToScreenPoint(TargetPelvisPosition);

                var targetPos = new Vector3();
                if (m_enable && IsRecoil)
                {
                    targetPos.y = centerPos.y;
                    targetPos.x = headPos.x;
                    targetPos.z = headPos.z;
                }
                else
                {
                    targetPos.y = Mathf.Clamp(centerPos.y, bodyPos.y, headPos.y);
                    targetPos.x = Mathf.Lerp(bodyPos.x, headPos.x, (targetPos.y - bodyPos.y) / (headPos.y - bodyPos.y));
                    targetPos.z = Mathf.Lerp(bodyPos.z, headPos.z, (targetPos.y - bodyPos.y) / (headPos.y - bodyPos.y));
                }
                if (targetPos.z < 0)
                {
                    targetPos.x = -targetPos.x;
                }
                return targetPos;
            }
            return new Vector3(Screen.width * 0.5f, Screen.height * 0.5f);
        }
    }

    /// <summary>
    /// 判断目标是否存活、不无敌、地方阵营、无遮挡
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public static bool IsAutoAimTarget(Transform target)
    {
        GameObject goPlayer = target.gameObject;
        BasePlayer bpTarget = WorldManager.singleton.GetPlayerWithGOHash(goPlayer.GetHashCode());
        return IsAutoAimTarget(bpTarget);
    }

    public static bool IsAutoAimTarget(BasePlayer bpTarget)
    {
        var startPos = MainPlayer.singleton.sceneCameraSlot.position;
        //var goPlayerPos = bpTarget.position;

        if (bpTarget != null && bpTarget.serverData != null && bpTarget.alive && !bpTarget.status.god && bpTarget.isVisible && MainPlayer.singleton.IsEnemy(bpTarget) && MainPlayer.singleton.IsAutoAimTarget(bpTarget))
        {
            var isPass = !Physics.Linecast(startPos, bpTarget.headPos, GameSetting.LAYER_MASK_BUILDING)
                   || !Physics.Linecast(startPos, bpTarget.centerPos, GameSetting.LAYER_MASK_BUILDING);
            return isPass;
        }
        return false;
    }

    private static List<Transform> m_autoTargetList = new List<Transform>(10);
    public static void AutoAimTargetRayTest()
    {
        RaycastHit[] raycastHitArr = null;

        if (IsCanAutoAim)
            raycastHitArr = Physics.RaycastAll(ScreenCenterRay, GameSetting.FIRE_RAYTEST_LENGTH, GameSetting.LAYER_MASK_AUTOTARGET);

        if (raycastHitArr != null && raycastHitArr.Length > 0)
        {
            //清理引用
            for (int i = 0; i < m_autoTargetList.Count; i++)
            {
                m_autoTargetList[i] = null;
            }
            //获取Tranform引用
            var hitCount = raycastHitArr.Length;
            for (int i = 0; i < raycastHitArr.Length; i++)
            {
                if (i >= m_autoTargetList.Count)
                    m_autoTargetList.Add(raycastHitArr[i].transform.root);
                else
                    m_autoTargetList[i] = raycastHitArr[i].transform.root;
            }

            var nearestIndex = 0;
            var nearestPos = m_cameraScene.WorldToScreenPoint(m_autoTargetList[0].position);
            for (int i = 1; i < hitCount; i++)
            {
                var target = m_autoTargetList[i].root;
                var targetPos = m_cameraScene.WorldToScreenPoint(target.position);
                if (Mathf.Abs(targetPos.x - Screen.width * 0.5f) < Mathf.Abs(nearestPos.x - Screen.width * 0.5f) && IsAutoAimTarget(target))
                {
                    nearestIndex = i;
                    nearestPos = targetPos;
                }
            }

            if (nearestIndex > 0 || IsAutoAimTarget(m_autoTargetList[nearestIndex]))
            {
                if (m_autoTargetList[nearestIndex] != AutoAimTarget)
                {
                    m_cdTime = 0;
                    m_operated = false;
                }
                AutoAimTarget = m_autoTargetList[nearestIndex];
                return;
            }
        }

        AutoAimTarget = null;
    }

    public static void Update()
    {
        if (!UIManager.IsBattle() || MainPlayer.singleton == null)
            return;

        //没找到屏幕变化的事件，那就再Update里执行好了。。
        SetCurAimPos(Vector2.zero);

        if (m_aimingTime > 0)
        {
            m_aimingTime -= Time.deltaTime;
            if (m_aimingTime <= 0)
            {
                m_aimingTime = 0;
                ResetCdTime();
            }

        }
        if (m_cdTime > 0)
        {
            if (m_operated)
            {
                m_cdTime -= Time.deltaTime;
                if (m_cdTime <= 0)
                    m_cdTime = 0;
            }
            else
            {
                if (IsOperation)
                    m_operated = true;
            }
        }

        if (!IsAutoAiming && m_lastIsAutoAiming)
        {
            ResetAimingTime();
        }

        m_lastIsAutoAiming = IsAutoAiming;
    }
    
    public static void DoAutoAim(Transform target, bool isAutoBattle)
    {
        if (target != null)
        {
            var m_kBP = MainPlayer.singleton;
            var angleTarget = Quaternion.LookRotation(isAutoBattle ? (m_autoBattleTargetPelvis.position - m_kBP.sceneCameraSlot.position).normalized : AutoAimTargetRay.direction).eulerAngles;

            var angleH = m_kBP.transform.localEulerAngles;
            var angleV = m_kBP.sceneCameraSlot.localEulerAngles;

            var absY = Mathf.Abs(angleH.y - angleTarget.y);
            var absX = Mathf.Abs(angleV.x - angleTarget.x);

            if (absY > 0.1f || absX > 0.1f)
            {
                m_fAimBackCenterDelay += Time.deltaTime;
                if (m_fAimBackCenterDelay >= GameSetting.PLAYER_AIM_BACK_CENTER_DELAY)
                {
                    var backSpeed = (m_enable ? GameSetting.PLAYER_AIM_BACK_CENTER_SPEED : GameSetting.PLAYER_AIM_ROTATE_SPEED) * Time.deltaTime;
                    if (MainPlayer.singleton != null)
                    {
                        var curWeapon = MainPlayer.singleton.curWeapon as WeaponGun;
                        if (curWeapon != null && curWeapon.isZoom)
                            backSpeed /= curWeapon.weaponConfigLine.ZoomFactor;
                    }
                    if (absY > 0.1f)
                    {
                        angleH.y = angleTarget.y;
                        var targetRotation = Quaternion.Euler(angleH);
                        m_kBP.transform.localRotation = Quaternion.RotateTowards(m_kBP.transform.localRotation, targetRotation, backSpeed);
                    }

                    if (absX > 0.1f)
                    {
                        //有后座力的时候垂直方向不吸
                        if (isAutoBattle || (!IsRecoil && IsNotCding))
                        {
                            angleV.x = angleTarget.x;
                            m_kBP.sceneCameraSlot.localRotation = Quaternion.RotateTowards(m_kBP.sceneCameraSlot.localRotation, Quaternion.Euler(angleV), backSpeed);
                        }
                    }
                }
            }
            else
                m_fAimBackCenterDelay = 0;
        }
        else
            m_fAimBackCenterDelay = 0;
    }

    public static void AutoBattleAim()
    {
        if (AutoBattleTarget != null)
            DoAutoAim(AutoBattleTarget, true);
        else if(AutoAimTarget == null)
            m_fAimBackCenterDelay = 0;
    }

    /// <summary>
    /// 枪口归正
    /// </summary>
    public static void LookFollow()
    {
        if (AutoBattleTarget == null)
        {
            if (AutoAimTarget != null)
                DoAutoAim(AutoAimTarget, false);
            else
                m_fAimBackCenterDelay = 0;
        }
    }
}