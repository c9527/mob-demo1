﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StoryManager : MonoBehaviour
{
    public static StoryManager singleton;

    private bool m_released;

    private Dictionary<int, OtherPlayer> m_otherPlayerDict;
    private Dictionary<int, GameObject> m_objDict;

    private Queue<ConfigStoryLine> m_commandQueue;

    private int m_storyId;

    private bool m_isPlaying = false;

    private Dictionary<int, EffectTimeout> m_effectDict;

    private Dictionary<string, Action<ConfigStoryLine>> m_commandHandle;

    private bool m_waitModel = false;
    private HashSet<int> m_waitModelSet;

    private bool m_waitEffect = false;
    private HashSet<int> m_waitEffectSet;

    private bool m_waitCameraAni = false;
    private HashSet<string> m_waitCameraAniSet;

    private SoundPlayer m_kSP;

    private short m_pdIndex = 0;
    private int m_storyVer = 0;

    private StoryCamera m_storyCamera;
    private AudioSource m_kAS;

    private Transform m_storyCameraSlot;
    private PosTweener m_storyCameraSlotPosT;
    private RotTweener m_storyCameraSlotRotT;

    private Vector3 m_offsetPos = Vector3.zero;
    private Vector3 m_offsetRot = Vector3.zero;

    private static readonly Vector3 defaultVector3 = new Vector3(4399, 4399, 4399);

    private bool m_breakCommand = false;
    private int m_curCommandId = -1;

    public StoryManager()
    {
        singleton = this;
        m_otherPlayerDict = new Dictionary<int, OtherPlayer>();
        m_effectDict = new Dictionary<int, EffectTimeout>();
        m_commandHandle = new Dictionary<string, Action<ConfigStoryLine>>();
        m_objDict = new Dictionary<int, GameObject>();
        m_kSP = new SoundPlayer();
        m_waitModelSet = new HashSet<int>();
        m_waitEffectSet = new HashSet<int>();
        m_waitCameraAniSet = new HashSet<string>();
        InitCommandHandle();
    }

    void InitCommandHandle()
    {
        m_commandHandle.Clear();
        m_commandHandle.Add("CreateModel", CreateModel);
        m_commandHandle.Add("PlayEffect", PlayEffect);
        m_commandHandle.Add("PlayAction", PlayAction);
        m_commandHandle.Add("SetTrigger", SetTrigger);
        m_commandHandle.Add("SetBool", SetBool);
        m_commandHandle.Add("DestoryModel", DestoryModel);
        m_commandHandle.Add("SetPos", SetPos);
        m_commandHandle.Add("MoveAndRotate", MoveAndRotate);
        m_commandHandle.Add("ShakeCamera", ShakeCamera);
        m_commandHandle.Add("EndStory", EndStory);
        m_commandHandle.Add("Sleep", Sleep);
        m_commandHandle.Add("WaitStart", WaitStart);
        m_commandHandle.Add("WaitEnd", WaitEnd);
        m_commandHandle.Add("PlayModelEffect", PlayModelEffect);
        m_commandHandle.Add("Preload", Preload);
        m_commandHandle.Add("AddCameraAni", AddCameraAni);
        m_commandHandle.Add("PlayCameraAni", PlayCameraAni);
        m_commandHandle.Add("SetOffset", SetOffset);
    }

    void Awake()
    {
        GameObject go = gameObject;
        m_kAS = GameObjectHelper.GetComponent<AudioSource>(go);
        m_kAS.panLevel = 0;
        m_kSP.SetAudioSource(m_kAS);
        float soundRadius = 10;
        m_kSP.radius = soundRadius;
        InitCamera();
        GameDispatcher.AddEventListener<int>(GameEvent.STORY_SLEEP_END, HandleWaitingEvent);
    }

    void InitCamera()
    {
        GameObject storyCameraSlot = new GameObject("StoryCameraSlot");
        m_storyCameraSlotPosT = storyCameraSlot.AddMissingComponent<PosTweener>();
        m_storyCameraSlotRotT = storyCameraSlot.AddMissingComponent<RotTweener>();
        m_storyCameraSlot = storyCameraSlot.transform;
        m_storyCameraSlot.SetParent(transform);
        m_storyCameraSlot.ResetLocal();
        
        GameObject go = new GameObject("StoryCamera");
        Transform trans = go.transform;
        trans.SetParent(m_storyCameraSlot);
        trans.ResetLocal();
        m_storyCamera = go.AddComponent<StoryCamera>();
        m_storyCamera.enableCamera = false;
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<int>(GameEvent.STORY_SLEEP_END, HandleWaitingEvent);
        foreach (var op in m_otherPlayerDict.Values)
        {
            op.Release(true);
        }
        m_otherPlayerDict.Clear();
        m_effectDict.Clear();
        m_commandHandle.Clear();
        m_otherPlayerDict = null;
        m_effectDict = null;
        m_commandHandle = null;
        m_commandQueue = null;
        m_released = true;
        singleton = null;
        GameObject.Destroy(m_storyCamera);
        m_storyCamera = null;
    }

    /// <summary>
    /// 添加玩家
    /// </summary>
    /// <param name="player"></param>
    public void AddPlayer(int id, OtherPlayer player)
    {
        m_otherPlayerDict.Add(id, player);
    }

    /// <summary>
    /// 移除玩家
    /// </summary>
    /// <param name="player"></param>
    public void RemovePlayer(int id)
    {
        m_otherPlayerDict.Remove(id);
    }

    #region 命令函数

    /// <summary>
    /// 创建模型
    /// </summary>
    /// <param name="roleId"></param>
    public void CreateModel(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 4)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        int id;
        int index = 0;
        if (int.TryParse(ap[index++], out id))
        {
            OtherPlayer op = new OtherPlayer();

            AddPlayer(id, op);
            if (m_waitModel)
            {
                m_waitModelSet.Add(id);
            }

            var roleData = new p_fight_role();
            roleData.id = id;
            roleData.actor_type = GameConst.ACTOR_TYPE_STORY;
            roleData.state = 1;
            roleData.prop = new p_fight_prop();
            roleData.prop.DispPart = new string[] { ap[index++] };
            Vector3 vPos = StoV(ap[index++], m_offsetPos);
            Vector3 vRot = StoV(ap[index++], m_offsetRot);
            roleData.pos = new float[] { vPos[0], vPos[1], vPos[2], vRot[0], vRot[1], vRot[2] };
            op.serverData.roleData = roleData;
            op.OnData_ActorData(roleData);
            op.SetState(roleData.state, true);
            
        }
    }

    public void PlayEffect(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');

        if (ap.Length != 6)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        int index = 0;
        int id = -1;
        int.TryParse(ap[index++], out id);
        string effect = ap[index++];
        Vector3 vPos = StoV(ap[index++], m_offsetPos);
        Vector3 vDir = StoV(ap[index++]);
        float duration;
        Single.TryParse(ap[index++], out duration);
        int attachId = 0;
        int.TryParse(ap[index++], out attachId);

        Transform parent = null;

        OtherPlayer op;
        if (m_otherPlayerDict.TryGetValue(attachId, out op))
        {
            parent = op.transform;
        }

        if(m_waitEffect)
        {
            m_waitEffectSet.Add(id);
        }

        EffectManager.singleton.GetEffect<EffectTimeout>(effect, (kEffect) =>
        {
            if (kEffect == null)
                return;

            m_effectDict.Add(id, kEffect);
            kEffect.Attach(vPos, vDir, Space.Self, parent);
            kEffect.SetTimeOut(duration);
            kEffect.SetCompleteCallBack(id, (obj) =>
            {
                int _id = (int)obj;
                EffectTimeout effectItem = null;
                if (m_effectDict.TryGetValue(_id, out effectItem))
                {
                    var rott = effectItem.gameObject.GetComponent<RotTweener>();
                    if (rott != null)
                        rott.enabled = false;
                    var post = effectItem.gameObject.GetComponent<PosTweener>();
                    if (post != null)
                        post.enabled = false;
                    effectItem.transform.ResetLocal();
                    m_effectDict.Remove(_id);
                }
            });
            kEffect.Play();

            OnEffectCreate(id);
        });
    }

    public void PlayModelEffect(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');

        if (ap.Length != 3)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        int index = 0;
        int id = -1;
        int.TryParse(ap[index++], out id);
        string effect = ap[index++];
        float time;
        float.TryParse(ap[index++], out time);

        OtherPlayer op;
        if (m_otherPlayerDict.TryGetValue(id, out op))
        {
            Transform trans = op.transform.FindChild(effect);
            if(trans == null)
            {
                return;
            }
            EffectManager.PlayEffect(trans, true);
            trans.gameObject.TrySetActive(true);
            TimerManager.SetTimeOut(time, () => { EffectManager.PlayEffect(trans, false); });
        }
    }

    public void PlayAction(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 2)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        int id;
        if (int.TryParse(ap[0], out id))
        {
            OtherPlayer op;
            if (m_otherPlayerDict.TryGetValue(id, out op))
            {
                string name = ap[1];
                op.playerAniController.Play(name);
            }
        }
    }

    public void SetTrigger(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 2)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        int id;
        if (int.TryParse(ap[0], out id))
        {
            OtherPlayer op;
            if (m_otherPlayerDict.TryGetValue(id, out op))
            {
                string name = ap[1];
                op.playerAniController.SetTrigger(name);
            }
        }
    }

    public void SetBool(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 3)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        int id;
        if (int.TryParse(ap[0], out id))
        {
            OtherPlayer op;
            if (m_otherPlayerDict.TryGetValue(id, out op))
            {
                string name = ap[1];
                int v = 0;
                int.TryParse(ap[2], out v);
                op.playerAniController.SetBool(name, v == 1);
            }
        }
    }

    public void DestoryModel(ConfigStoryLine command)
    {
        int id;
        if (int.TryParse(command.ActionParam, out id))
        {
            OtherPlayer op;
            if (m_otherPlayerDict.TryGetValue(id, out op))
            {
                op.Release(false);
                RemovePlayer(id);
            }
        }
    }

    private void SetPos(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 4)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        int id;
        int index = 0;
        string type = ap[index++];
        int.TryParse(ap[index++], out id);
        Vector3 vPos = StoV(ap[index++], m_offsetPos);
        Vector3 vRot = StoV(ap[index++], m_offsetRot);
        switch(type)
        {
            case "model":
                {
                    OtherPlayer op;
                    if (m_otherPlayerDict.TryGetValue(id, out op))
                    {
                        if (vPos != defaultVector3)
                            op.position = vPos;
                        if (vRot != defaultVector3)
                            op.eulerAngle = vRot;
                    }
                    break;
                }
            case "effect":
                {
                    EffectTimeout et;
                    if (m_effectDict.TryGetValue(id, out et))
                    {
                        var go = et.gameObject;
                        if (vPos != defaultVector3)
                            go.transform.position = vPos;
                        if (vRot != defaultVector3)
                            go.transform.eulerAngles = vRot;
                        
                    }
                    break;
                }
            case "camera":
                {
                    if (m_storyCamera != null)
                    {
                        if (vPos != defaultVector3)
                            m_storyCamera.position = vPos;
                        if (vRot != defaultVector3)
                            m_storyCamera.eulerAngles = vRot;
                    }
                    break;
                }
            case "camera_slot":
                {
                    if (m_storyCameraSlot != null)
                    {
                        if (vPos != defaultVector3)
                            m_storyCameraSlot.position = vPos;
                        if (vRot != defaultVector3)
                            m_storyCameraSlot.eulerAngles = vRot;
                    }
                    break;
                }
        }
    }

    private void MoveAndRotate(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 5)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        int id;
        int index = 0;
        string type = ap[index++];
        int.TryParse(ap[index++], out id);
        Vector3 vPos = StoV(ap[index++], m_offsetPos);
        Vector3 vRot = StoV(ap[index++], m_offsetRot);
        float duration;
        Single.TryParse(ap[index++], out duration);
        switch (type)
        {
            case "model":
                {
                    OtherPlayer op;
                    if (m_otherPlayerDict.TryGetValue(id, out op))
                    {
                        if (vPos == defaultVector3)
                            vPos = op.position;
                        if (vRot == defaultVector3)
                        {
                            vRot.x = op.lastAnglePitch;
                            vRot.y = op.eulerAngle.y;
                            vRot.z = 0;
                        }

                        toc_fight_move proto = new toc_fight_move()
                        {                      
                           
                            moveState = GameConst.MOVE_STATE_WALK,
                            clientSpeed = (byte)(Vector3.Distance(vPos, op.position) / duration * 10),
                            time = m_pdIndex++,
                        };

                        TweenData td = new TweenData()
                        {
                            type = GameConst.ACT_TYPE_MOVE,
                            pos = new float[] { vPos.x, vPos.y, vPos.z },
                            angleY = 4399,
                            anglePitch = 4399,
                            clientSpeed = -1,
                            maxSpeed = -1,
                            index = -1,
                            canBreak = false,
                            moveState = GameConst.MOVE_STATE_WALK,
                            fakeJumpGroundData = false,
                            tweenTime = duration
                        };
                        op.ActWithTween(td);
                    }
                    break;
                }
            case "effect":
                {
                    EffectTimeout et;
                    if (m_effectDict.TryGetValue(id, out et))
                    {
                        var go = et.gameObject;
                        if (vPos != Vector3.zero)
                        {
                            var posT = go.AddMissingComponent<PosTweener>();
                            if (posT.onFinished == null)
                                posT.AddOnFinished(() => { posT.enabled = false; });
                            posT.enabled = true;
                            posT.TweenPos(duration, vPos);
                        }

                        if (vRot != Vector3.zero)
                        {
                            var rotT = go.AddMissingComponent<RotTweener>();
                            if (rotT.onFinished == null)
                                rotT.AddOnFinished(() => { rotT.enabled = false; });
                            rotT.enabled = true;
                            rotT.TweenRot(duration, vRot);
                        }
                    }
                    break;
                }
            case "camera":
                {
                    if (m_storyCamera != null)
                    {
                        if (vPos != defaultVector3)
                        {
                            m_storyCamera.MoveTo(vPos, duration);
                        }
                        if (vRot != defaultVector3)
                        {
                            m_storyCamera.RotateTo(vRot, duration);
                        }
                    }
                    break;
                }
            case "camera_slot":
                {
                    if (m_storyCameraSlot != null)
                    {
                        if (vPos != defaultVector3)
                        {
                            m_storyCameraSlotPosT.TweenPos(duration, vPos);
                        }
                        if (vRot != defaultVector3)
                        {
                            m_storyCameraSlotRotT.TweenRot(duration, vRot);
                        }
                    }
                    break;
                }
        }
    }

    private void ShakeCamera(ConfigStoryLine command)
    {
        int id = int.Parse(command.ActionParam);
        ConfigShakeLine config = ConfigManager.GetConfig<ConfigShake>().GetLine(id);

        if (m_storyCamera != null)
            m_storyCamera.StartShake(config);
    }

    private void EndStory(ConfigStoryLine command)
    {
        EndCommand();
    }

    private void Sleep(ConfigStoryLine command)
    {
        m_breakCommand = true;
        var curVersion = m_storyVer;
        float time;
        float.TryParse(command.ActionParam, out time);
        TimerManager.SetTimeOut(time, () => 
        {
            GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, curVersion); 
        });
    }

    private void WaitStart(ConfigStoryLine command)
    {
        string waitType = command.ActionParam;
        switch(waitType)
        {
            case "model":
                {
                    m_waitModel = true;
                    GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
                }
                break;
            case "effect":
                {
                    m_waitEffect = true;
                }
                break;
            case "camera_ani":
                {
                    m_waitCameraAni = true;
                    GameDispatcher.AddEventListener<string>(StoryCamera.STORYCAMERA_ANI_LOADED, OnCameraAniLoaded);
                }
                break;
        }
        
    }

    private void WaitEnd(ConfigStoryLine command)
    {
        string waitType = command.ActionParam;
        switch (waitType)
        {
            case "model":
                {
                    m_waitModel = false;
                    if (m_waitModelSet.Count == 0)
                        return;
                }
                break;
            case "effect":
                {
                    m_waitEffect = false;
                    if (m_waitEffectSet.Count == 0)
                        return;
                }
                break;
            case "camera_ani":
                {
                    m_waitCameraAni = false;
                    if (m_waitCameraAniSet.Count == 0)
                        return;
                }
                break;
            default :
                return;
        }
        m_breakCommand = true;
    }

    public void Preload(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 2)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }
        string[] models = ap[0].Split('#');
        string[] effects = ap[1].Split('#');
        int modelLength = models.Length;
        int effectLength = effects.Length;
        string[] paths = new string[modelLength + effectLength];

        int roleId = 0;
        ConfigItemRoleLine role = null;
        var itemRoleConfig = ConfigManager.GetConfig<ConfigItemRole>();
        
        for (int i = 0; i < modelLength; ++i)
        {
            if (int.TryParse(models[i], out roleId))
            {
                role = itemRoleConfig.GetLine(roleId);
                if(role != null)
                    paths[i] = PathHelper.GetRolePrefabPath(role.OPModel);
            }
            else
            {
                Logger.Error("Preload Role Err index=" + i + "  models=" + models[i]);
            }
        }


        for (int i = 0; i < effectLength; ++i)
        {
            paths[i + modelLength] = PathHelper.GetEffectPrefabPath(effects[i]);
        }

        var curVersion = m_storyVer;
        m_breakCommand = true;

        ResourceManager.LoadMulti(paths, null, () =>
        {
            GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, curVersion); 
        });
    }

    public void AddCameraAni(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length == 0)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        if (m_storyCamera == null)
        {
            Logger.Error("StoryCamera is Null! id=" + command.ID);
            return;
        }

        string[] anis = ap;

        for (int i = 0, imax = anis.Length; i < imax; ++i)
        {
            string ani = anis[i];
            if(m_waitCameraAni)
            {
                m_waitCameraAniSet.Add(ani);
            }
             m_storyCamera.AddAni(ani);
        }
    }

    public void PlayCameraAni(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 2)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        if (m_storyCamera == null)
        {
            Logger.Error("StoryCamera is Null! id=" + command.ID);
            return;
        }

        int index = 0;
        string aniName = ap[index++];

        int waitEnd = 0;
        int.TryParse(ap[index++], out waitEnd);
        
        m_storyCamera.PlayAni(aniName);

        if (waitEnd == 1)
        {
            m_breakCommand = true;
            var curVersion = m_storyVer;
            float sleepTime = m_storyCamera.GetAniTime(aniName);
            TimerManager.SetTimeOut(sleepTime, () =>
            {
                GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, curVersion);
            });
        }
    }

    public void SetOffset(ConfigStoryLine command)
    {
        string[] ap = command.ActionParam.Split(',');
        if (ap.Length != 2)
        {
            Logger.Error("StoryManager Command Length err! id=" + command.ID);
            return;
        }

        if (m_storyCamera == null)
        {
            Logger.Error("StoryCamera is Null! id=" + command.ID);
            return;
        }

        int index = 0;
        m_offsetPos = StoV(ap[index++], m_offsetPos);
        m_offsetRot = StoV(ap[index++], m_offsetRot);
    }

    #endregion

    /// <summary>
    /// 运行剧情
    /// </summary>
    public void PlayStory(int storyID)
    {
        CleanCommand();
        m_storyId = storyID;
        
        m_commandQueue = ConfigManager.GetConfig<ConfigStory>().GetStoryCommand(m_storyId);
        if (m_commandQueue == null || m_commandQueue.Count == 0)
            return;

        m_storyCamera.enableCamera = true;
        if (SceneCamera.singleton != null)
        {
            m_storyCameraSlot.ResetLocal();
            m_storyCamera.position = SceneCamera.singleton.position;
            m_storyCamera.eulerAngles = SceneCamera.singleton.eulerAngles;
            SceneCamera.singleton.enableCamera = false;
        }
        if (MainPlayer.singleton != null && MainPlayer.singleton.isAlive)
        {
            MainPlayer.singleton.ForbidMainCam = true;
            MainPlayer.singleton.ForbidSceneCam = true;
        }
        else if(FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
        {
            FPWatchPlayer.singleton.ForbidMainCam = true;
        }
        if (MainPlayerControlInputReceiver.singleton != null)
            MainPlayerControlInputReceiver.singleton.enabled = false;
        UIManager.m_uiCamera.enabled = false;
        m_isPlaying = true;
        Execute();
    }

    public void EndStoryForce()
    {
        EndCommand();
    }

    private void HandleWaitingEvent(int version)
    {
        if (m_released || m_storyVer != version || !m_breakCommand)
            return;
        m_breakCommand = false;
        if (m_commandQueue.Count > 0)
        {
            Execute();
        }
    }

    private void OnPlayerJoin(BasePlayer bp)
    {
        int id = bp.PlayerId; 
        if(m_waitModelSet.Contains(id))
        {
            m_waitModelSet.Remove(id);
        }
        if (m_waitModelSet.Count == 0)
        {
            GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
            GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, m_storyVer);
        }
    }

    private void OnEffectCreate(int id)
    {
        if (m_waitEffectSet.Contains(id))
        {
            m_waitEffectSet.Remove(id);
        }
        if (m_waitEffectSet.Count == 0)
        {
            GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, m_storyVer);
        }
    }

    private void OnCameraAniLoaded(string aniName)
    {
        if (m_waitCameraAniSet.Contains(aniName))
        {
            m_waitCameraAniSet.Remove(aniName);
        }
        if (m_waitCameraAniSet.Count == 0)
        {
            GameDispatcher.RemoveEventListener<string>(StoryCamera.STORYCAMERA_ANI_LOADED, OnCameraAniLoaded);
            GameDispatcher.Dispatch<int>(GameEvent.STORY_SLEEP_END, m_storyVer);
        }
    }

    public void Execute()
    {
        while (m_commandQueue.Count != 0)
        {
            ConfigStoryLine command = m_commandQueue.Peek();

            m_commandQueue.Dequeue();
            try
            {
                ExcuteCommand(command);
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                EndCommand();
                return;
            }

            //if (command.Action == "Sleep" || command.Action == "WaitEnd" || command.Action == "Preload")
            if (m_breakCommand || m_curCommandId > command.ID)
            {
                break;
            }
        }
    }

    /// <summary>
    /// 执行命令
    /// </summary>
    private void ExcuteCommand(ConfigStoryLine command)
    {
        PrintCommand(command);

        m_curCommandId = command.ID;

        string sound = command.Sound;
        if(!string.IsNullOrEmpty(sound))
            m_kSP.PlayOneShot(sound);

        string action = command.Action;
        Action<ConfigStoryLine> handle;
        if(m_commandHandle.TryGetValue(action, out handle))
        {
            if (handle != null)
                handle(command);
        }
        else
        {
            Logger.Error("ExcuteCommand err!! command id=" + command.ID + "  command action=" + action);
        }

    }

    private void CleanCommand()
    {
        if (m_commandQueue != null)
            m_commandQueue.Clear();
        foreach (var op in m_otherPlayerDict.Values)
        {
            op.Release(false);
        }
        foreach (var e in m_effectDict.Values)
        {
            e.Release();
        }
        m_otherPlayerDict.Clear();
        m_effectDict.Clear();
        m_storyId = -1;
        ++m_storyVer;
        m_isPlaying = false;
        m_waitModel = false;
        m_waitModelSet.Clear();
        m_waitEffect = false;
        m_waitEffectSet.Clear();
        m_waitCameraAni = false;
        m_waitCameraAniSet.Clear();
    }

    private void EndCommand()
    {
        CleanCommand();
        m_curCommandId = 0;
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        m_storyCamera.enableCamera = false;
        UIManager.m_uiCamera.enabled = true;
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.enableCamera = true;  
        if(MainPlayer.singleton != null)
        {
            MainPlayer.singleton.ForbidMainCam = false;
            MainPlayer.singleton.ForbidSceneCam = false;
        }
        else if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
        {
            FPWatchPlayer.singleton.ForbidMainCam = false;
        }
        if (MainPlayerControlInputReceiver.singleton != null)
            MainPlayerControlInputReceiver.singleton.enabled = true;
        //m_storyCamera.SetParent(transform);
    }

    private void PrintCommand(ConfigStoryLine command)
    {
        Logger.Log("PrintCommand id=" + command.ID + " story=" + command.StoryID + " action=" + command.Action + "  param=" + command.ActionParam + "  sound=" + command.Sound);
    }

    public bool isPlaying
    {
        get { return m_isPlaying; }
    }

    #region Util
    private static Vector3 StoV(string s, Vector3 offect = default(Vector3))
    {
        Vector3 v = Vector3.zero;
        if (!string.IsNullOrEmpty(s))
        {
            string[] ss = s.Split('#');
            if (ss.Length == 3)
            {
                Single.TryParse(ss[0], out v.x);
                Single.TryParse(ss[1], out v.y);
                Single.TryParse(ss[2], out v.z);
            }
        }
        return v + offect;
    }

    #endregion
}
