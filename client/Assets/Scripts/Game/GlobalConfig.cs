﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

public class GlobalConfig
{
    static public ConfigItemWeapon configWeapon;
    static public ConfigItemRole configRole;
    static public ConfigGameSettingLine gameSetting;
    static public ConfigSceneItem configSceneItem;
    static public ConfigItemDecoration configDecoration;

    static public ServerValue serverValue;
    static public ClientValue clientValue;

    public static bool showRecoilDebugInfo;
    public static bool showSpreadDebugInfo;
    public static bool showHitDebugInfo;

    public static TDServerListData[] serverList = new TDServerListData[0];          //服务器列表数据
    public static string[] recentServerIdList = new string[0];     //最近登录服务器id数组
    public static ConfigAddressMapping addressMapping = ScriptableObject.CreateInstance<ConfigAddressMapping>();          //地址映射表

    public class ServerValue
    {
        public bool share_open = true;
        public bool share_open_pc = true;
        public bool share_open_web = true;
        public bool share_open_ios = true;
        public bool recharge_open_android = true;
        public bool recharge_open_pc = true;
        public bool recharge_open_web = true;
        public bool recharge_open_ios = true;
        public bool giftCode_open = true;
        public bool bindPhone_open = true;
        public bool cm_open = false;
        public bool new_server_open = false;
        //public bool cheatAppQB = true;  // quit battle if find any cheat app.
        public bool cheatAppRelogin = true;
        public bool weixinShare_open = true;
        public bool fiveStarComment_open = true;
        public bool localNotification_open = true;
		public bool legionWar_open = false;
        public bool activity_open_ios = true;
        public string fix_code = "";
        public string checkPCProcess = "ProcessName = KC";
        public bool checkMPPosY = true;
        public bool checkOPColliderInfo = true;
        public bool checkFireDir = true;
        public bool shareCash_open = true;
        public bool nation_channel_default = false;
        public bool open_purple_lottery = true;
    }

    public class ClientValue
    {
        public string guideChapterName = "";    //值为finish则表示指引完成
        public int today_online_time = 0; //今日在线总时长
        public int last_online_stamp = 0; //最后在线时间戳
		public int last_comment_stamp = 0;//最后未评价时间戳
		public bool isComment_thisVersion = false;//本版本是否给过好评
        public int[] xxx_ary = new int[] { 1, 2, 3 };
        public CDict xxx_dic = new CDict();
        public bool newHandWordBoss_enter = false;
    }

    static public void Init()
    {
        configWeapon = ConfigManager.GetConfig<ConfigItemWeapon>();
        configRole = ConfigManager.GetConfig<ConfigItemRole>();
        configDecoration = ConfigManager.GetConfig<ConfigItemDecoration>();
        gameSetting = ConfigManager.GetConfig<ConfigGameSetting>().m_dataArr[0];
        configSceneItem = ConfigManager.GetConfig<ConfigSceneItem>();
        ResetConfig();
        if (Global.Dic.ContainsKey("ServerBigVersion"))
		    SdkManager.appNSLog("Global.Dic[ServerBigVersion]:"+Convert.ToInt32(Global.Dic["ServerBigVersion"]));
        if (Global.Dic.ContainsKey("LocalBigVersion"))
		    SdkManager.appNSLog("Global.Dic[LocalBigVersion]:"+Convert.ToInt32(Global.Dic["LocalBigVersion"]));
        if (Global.Dic.ContainsKey("ServerBigVersion") && Convert.ToInt32(Global.Dic["ServerBigVersion"]) > Convert.ToInt32(Global.Dic["LocalBigVersion"])) 
		{
			clientValue.last_comment_stamp = 1;
			SaveClientValue("last_comment_stamp");
		}
    }

    static public void ResetConfig()
    {
        serverValue = new ServerValue();
        clientValue = new ClientValue();
    }

    static void Toc_player_server_value(toc_player_server_value proto)
    {
        Type typ = serverValue.GetType();
        string old_fix = serverValue.fix_code;
        foreach (string key in proto.dict.Keys)
        {
            FieldInfo fi = typ.GetField(key);
            if (fi == null)
                continue;
            fi.SetValue(serverValue, proto.dict[key]);
        }
        if (serverValue.fix_code != old_fix)
            LuaHook.LoadFixCode();
    }

    static object _convert_value(object value, Type type)
    {
        if (type.IsArray)
        {
            object[] ary = value as object[];
            int len = ary == null ? 0 : ary.Length;
            Type et = type.GetElementType();
            Array ret = Array.CreateInstance(et, len);
            for (int i = 0; i < len; i++)
                ret.SetValue(_convert_value(ary[i], et), i);
            return ret;
        }

        if (type.IsSubclassOf(typeof(IDictionary)))
        {
            CDict dic = value as CDict;
            IDictionary ret = type.GetConstructor(new Type[0]).Invoke(new object[0]) as IDictionary;
            Type kt = typeof(string);
            Type vt = typeof(object);
            if (type.IsGenericType)
            {
                Type[] ta = type.GetGenericArguments();
                if (ta.Length == 2)
                {
                    kt = ta[0];
                    vt = ta[1];
                }
            }
            foreach (var pair in dic)
                ret.Add(_convert_value(pair.Key, kt), _convert_value(pair.Value, vt));
            return ret;
        }

        MethodInfo mi = type.GetMethod("Parse", new Type[] { typeof(string) });
        if (mi != null)
            return mi.Invoke(null, new object[] { value.ToString() });

        return value;
    }

    static void Toc_player_client_value(toc_player_client_value proto)
    {
        Type typ = clientValue.GetType();
        foreach (string key in proto.dict.Keys)
        {
            FieldInfo fi = typ.GetField(key);
            if (fi == null)
                continue;
            fi.SetValue(clientValue, _convert_value(proto.dict[key], fi.FieldType));
        }
    }

    public static void SaveClientValue(string key)
    {
        SaveClientValues(new string[] { key });
    }

    public static void SaveClientValues(string[] keys)
    {
        CDict dic = new CDict();
        Type typ = clientValue.GetType();
        foreach (string key in keys)
        {
            FieldInfo fi = typ.GetField(key);
            if (fi == null)
                continue;
            dic[key] = fi.GetValue(clientValue);
        }
        NetLayer.Send(new tos_player_client_value_update() { dict = dic });
    }
}
