﻿using UnityEngine;
using System.Collections.Generic;


public class PlayerAnimatorController : PlayerAniBaseController
{
    protected Animator m_kAnimator;
    private List<float> m_listLayerWeight;

    public PlayerAnimatorController(BasePlayer player)
    {
        m_player = player;
        m_listLayerWeight = new List<float>();
    }


    override public void SetAnimator(Animator kAni)
    {
        m_kAnimator = kAni;
        if (m_kAnimator != null)
        {
            m_go = kAni.gameObject;
            m_listLayerWeight.Clear();
            for (int i = GameConst.PLAYER_UPBODY_LAYER_INDEX; i < m_kAnimator.layerCount; i++)
            {
                m_listLayerWeight.Add(kAni.GetLayerWeight(i));
            }   
        }
        else
        {
            m_go = null;
        }
    }

    override public void Play(string strAniName, int iLayer = -1, float normalizedTime = 0f)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        if(string.IsNullOrEmpty(strAniName))
        {
            Logger.Warning("PlayerAnimator strAniName is Empty");
            return;
        }

        m_strCurAniName = strAniName;
        m_kAnimator.Play(strAniName, iLayer, normalizedTime);
    }

    override public void CrossFade(string strAniName, float transitionDuration = 0.2f, int iLayer = -1)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        if (string.IsNullOrEmpty(strAniName))
        {
            Logger.Warning("PlayerAnimator strAniName is Empty");
            return;
        }

        // 避免不能播放正在播放的动画问题
        if (strAniName == m_strCurAniName)
            m_kAnimator.Play("");
        m_strCurAniName = strAniName;

        m_kAnimator.CrossFade(strAniName, transitionDuration, iLayer);
    }

    override public void SetInteger(string strPropertyName, int iValue)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        m_kAnimator.SetInteger(strPropertyName, iValue);
    }

    override public void SetTrigger(string strName)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        m_kAnimator.SetTrigger(strName);
    }

    override public void SetBool(string strName, bool bValue)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        m_kAnimator.SetBool(strName, bValue);
    }

    override public void SetFloat(string strName, float bValue)
    {
        if (m_kAnimator == null || m_kAnimator.runtimeAnimatorController == null || m_go.activeSelf == false)
            return;

        m_kAnimator.SetFloat(strName, bValue);
    }

    override public void EnableUpBodyLayer(bool val)
    {
        if (m_kAnimator == null)
            return;

        for (int i = 0; i < m_listLayerWeight.Count; i++)
        {
            if (val)
                m_kAnimator.SetLayerWeight(i + GameConst.PLAYER_UPBODY_LAYER_INDEX, m_listLayerWeight[i]);
            else
                m_kAnimator.SetLayerWeight(i + GameConst.PLAYER_UPBODY_LAYER_INDEX, 0);
        }
    }

    /// <summary>
    /// 是否打开WalkWaggle
    /// </summary>
    /// <param name="val"></param>
    override public void EnableWalkWaggleLayer(bool val)
    {
        if (m_kAnimator == null)
            return;

        if (val)
            m_kAnimator.SetLayerWeight(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX, m_listLayerWeight[GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX - GameConst.PLAYER_UPBODY_LAYER_INDEX]);
        else
            m_kAnimator.SetLayerWeight(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX, 0);
    }

    override public void SetSpeed(float speed)
    {
        if (m_kAnimator == null)
            return;

        m_kAnimator.speed = speed;
    }

    override public bool IsCurrentAni(string aniName, int layer = 0)
    {
        if (m_kAnimator == null)
            return false;

        return m_kAnimator.GetCurrentAnimatorStateInfo(layer).IsName(aniName);
    }

    override public bool enable
    {
        set
        {
            if (animator != null)
                animator.enabled = value;
        }
    }

    override public Animator animator
    {
        get { return m_kAnimator; }
    }

}

