﻿using UnityEngine;

public class OtherPlayerController : BasePlayerController
{
    protected OtherPlayer m_kPlayer;
    protected float m_fLastJumpY;
    private float m_updateBoneWeightTime;
    private float m_targetPitchAngle;
    private float m_pitchSpeed;
    private float m_curPitchAngle;

    private EptFloat m_headScale;

    // Debug
    public int _posCount;
    public bool _tweening;
    public int _state;


    public override void Release()
    {
        m_released = true;
        m_kPlayer = null;
    }

    public override void BindPlayer(BasePlayer kBP)
    {
        base.BindPlayer(kBP);
        m_kPlayer = kBP as OtherPlayer;
    }

    protected override void Update()
    {
        if (m_released || m_kPlayer == null || m_kPlayer.serverData == null)
            return;

        if (m_kPlayer.serverData.alive == true)
        {
            UpdateJump();
        }

        UpdateBoneWeight();

        // Debug
        _posCount = m_kPlayer.posCount;
        _tweening = m_kPlayer.tweening;
        _state = m_kPlayer.serverData.state;
    }

    protected void LateUpdate()
    {
        if (this == null || m_released || m_kPlayer == null || m_kPlayer.released ||
            m_go == null || m_trans == null || m_kPlayer.isRendererVisible == false)
            return;

        if (QualityManager.enableRigRot && GameSetting.enableRigRot && m_kPlayer.alive)
        {
            if (m_kPlayer.transSpine != null && m_kPlayer.transChest != null)
            {
                if (m_targetPitchAngle != m_curPitchAngle)
                {
                    float plusMinus = m_targetPitchAngle > m_curPitchAngle ? 1 : -1;
                    m_curPitchAngle += Time.deltaTime * m_pitchSpeed * plusMinus;
                    if (plusMinus == 1 && m_curPitchAngle >= m_targetPitchAngle || plusMinus == -1 && m_curPitchAngle <= m_targetPitchAngle)
                    {
                        m_curPitchAngle = m_targetPitchAngle;
                    }
                }

                m_kPlayer.transSpine.Rotate(m_trans.right, m_curPitchAngle * (1 - GlobalConfig.gameSetting.OPPitchChestFactor), Space.World);
                m_kPlayer.transChest.Rotate(m_trans.right, m_curPitchAngle * GlobalConfig.gameSetting.OPPitchChestFactor, Space.World);
            }
        }

        if (m_kPlayer.transBipHead != null && m_headScale > 1.1f)
        {
            m_kPlayer.transBipHead.localScale = new Vector3(m_headScale, m_headScale, m_headScale);
        }
    }

    override protected void UpdateJump()
    {
        if (m_released || m_kPlayer.status.jump == false)
            return;

        Vector3 curPos = m_kPlayer.position;
        float curY = curPos.y / Mathf.Abs(curPos.y) * (float)((int)(Mathf.Abs(curPos.y) * 100) * 0.01f);    // 不四舍五入，且只取小数点后2位。所有位都取的话，小数位波动很大。

        if (m_fLastJumpY == 0)
        {
            m_fLastJumpY = curY;
        }
        else
        {
            if (m_fLastJumpY != 8888 && curY < m_fLastJumpY)  // 跳到最大高度时，播放jump_end动画
            {
                m_fLastJumpY = 8888;    //  8888表示已经到了最大高度
                JumpTopPoint();
            }
        }

        if (m_fLastJumpY != 8888)
            m_fLastJumpY = curY;
    }

    override public void StartJump()
    {
        m_fLastJumpY = 0;
        jumpStatus = JUMP_STATUS_UP;
        m_kPlayer.playerAniController.Play(AniConst.jumpStart);
    }

    private void JumpTopPoint()
    {
        m_kPlayer.playerAniController.Play(AniConst.jumpEnd);
        //Logger.Warning("JumpTopPoint");
    }

    public override void JumpComplete()
    {
        // GameRoundEnd 时会主动调用一下，但有些怪就没有跳跃动画，报错动画不存在，这里处理一下
        if (m_kPlayer.status.jump)
            m_kPlayer.playerAniController.Play(AniConst.jumpHuanchong);

        //Logger.Warning("跳跃 完成");
        m_kPlayer.JumpComplete();
    }

    //override protected void UpdateGravity()
    //{
    //    float rayDis;
    //    if (m_kPlayer.IsOnTheGround(out rayDis) == false)
    //    {
    //        if (rayDis == float.MaxValue)
    //            return;

    //        float deltaTime = Time.smoothDeltaTime;

    //        m_fGravityTick += deltaTime;
    //        float fallSpeed = -gravity * m_fGravityTick * deltaTime;

    //        m_kPlayer.characterControler.Move(new Vector3(0, fallSpeed, 0));
    //    }
    //    else
    //    {
    //        m_fGravityTick = 0;
    //    }
    //}

    private void UpdateBoneWeight()
    {
        if (QualityManager.lowBoneWeightDis == -1 || SceneCamera.singleton == null)
            return;

        if(Time.time - m_updateBoneWeightTime >= 2)
        {
            m_updateBoneWeightTime = Time.time;

            float dis = Vector3.SqrMagnitude(m_kPlayer.position - SceneCamera.singleton.position);
            if (dis >= Mathf.Pow(QualityManager.lowBoneWeightDis, 2))
                m_kPlayer.SetBoneWeight(SkinQuality.Bone1);
            else
                m_kPlayer.SetBoneWeight(SkinQuality.Bone2);
        }
    }

    override public void SetPitchAngle(float angle, float tweenTime)
    {
        angle /= 0.5f;

        angle = angle > 180 ? angle - 360 : angle;
        angle = Mathf.Clamp(angle, -GlobalConfig.gameSetting.OPPitchUpMaxAngle, GlobalConfig.gameSetting.OPPitchDownMaxAngle);

        float curAngle = m_curPitchAngle > 180 ? m_curPitchAngle - 360 : m_curPitchAngle;

        m_pitchSpeed = tweenTime <= 0 ? float.MaxValue : Mathf.Abs(angle - curAngle) / tweenTime;
        m_targetPitchAngle = angle;
    }

    public override void SetHeadScale(float scale)
    {
        m_headScale = scale;
        if (m_kPlayer != null && m_kPlayer.transBipHead != null)
        {
            m_kPlayer.transBipHead.localScale = new Vector3(m_headScale, m_headScale, m_headScale);
        }
    }
    public OtherPlayer otherplayer
    {
        get { return m_kPlayer; }
    }
}
