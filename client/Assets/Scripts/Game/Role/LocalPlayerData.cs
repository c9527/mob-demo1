﻿using System.Collections.Generic;

//延迟伤害数据
public class DelayDamageData
{
    public float decelerate;
    public float decelerateTime;
}

// 从属性里，缓存武器相关数据
public class LocalWeaponData
{
    public string id;
    public EptInt clipSize;
    public EptInt subClibSize;
    public EptInt curClipAmmoNum;
    public EptInt extraAmmoNum;
    public EptInt advAmmoNum;
    public EptInt subAmmoNum;      //子武器子弹数
    public EptInt maxAmmoNum;      //最大弹夹数量
    public EptFloat reloadTime;
    public EptFloat decelerate;
    public EptFloat decelerateTime;
    public int[] awaken_props;
    public bool isZoomIn;
    //public bool use_adv;    // 使用穿甲弹
    //public int minAmmoAdv
    //{
    //    get { return curClipAmmoNum <= advAmmoNum ? curClipAmmoNum : advAmmoNum; }
    //}

    public bool hasAmmo
    {
        get
        {
            if (curClipAmmoNum == -1)   // 无限弹药
                return true;

            return curClipAmmoNum + extraAmmoNum > 0;
        }
    }

    public int weaponId
    {
        get { return int.Parse(id); }
    }
};

// 存放玩家所有的需要同步的类
public class LocalPlayerData 
{
    private BasePlayer m_kPlayer;

    // Weapon Data
    private Dictionary<string, LocalWeaponData> m_dicLocalWeaponData;

    private int m_itemRoleID;

    private DelayDamageData[] m_delayDamageData;

        
    public LocalPlayerData(BasePlayer kPlayer)
    {
        m_kPlayer = kPlayer;
        //m_kBPD = new BasePlayerData();
        m_delayDamageData = new DelayDamageData[GameSetting.MAX_EXIST_PROJECTILE_SIZE];
        m_dicLocalWeaponData = new Dictionary<string, LocalWeaponData>();
    }

    public BasePlayer basePlayer
    {
        get { return m_kPlayer; }
    }

    public void UpdateLocalWeaponData_EquipInfo()
    {
        PlayerServerData sd = m_kPlayer.serverData;
        foreach(int weaponID in sd.weaponList)
        {
            string id = weaponID.ToString();
            LocalWeaponData lwd = null;
            lwd = GetLocalWeaponData(id);

            toc_fight_equip_info.EquipInfo ei = sd.GetEquipInfo(weaponID);

            lwd.curClipAmmoNum = ei.cur_count;
            lwd.extraAmmoNum = ei.ext_count;
            lwd.advAmmoNum = ei.adv_count;
            lwd.subAmmoNum = ei.sub_count;
            lwd.maxAmmoNum = ei.max_count;
            lwd.awaken_props = ei.awaken_props;
            //lwd.atkType = ei.atk_type;
            //if (m_kPlayer != null)
            //{
            //    if (m_kPlayer.curWeapon != null && id == m_kPlayer.curWeapon.weaponId)
            //        m_kPlayer.curWeapon.UpdateLocalWeaponData(lwd);
            //    else if (m_kPlayer.subWeapon != null && id == m_kPlayer.subWeapon.weaponId)
            //        m_kPlayer.subWeapon.UpdateLocalWeaponData(lwd);
            //}
            //lwd.use_adv = ei.atk_type == 2; // TODO
            //Logger.Error("id: "+id+", cur: "+ei.cur_count+", ext: "+ei.ext_count+", adv: "+ei.adv_count);
        }
    }

    public void UpdateLocalWeaponData_ActorProp()
    {
        // 人物属性同步时，更新对应的武器数据 
        PlayerServerData sd = m_kPlayer.serverData;
        if (string.IsNullOrEmpty(sd.propWeapon()) == true)
            return;

        LocalWeaponData lwd = null;
        lwd = GetLocalWeaponData(sd.propWeapon());

        // 当前弹药数同步
        lwd.clipSize = sd.clipSize();
        lwd.reloadTime = sd.reloadTime();
        lwd.decelerate = sd.decelerate();
        lwd.decelerateTime = sd.decelerateTime();
        var equipInfo = sd.GetEquipInfo(lwd.weaponId);
        if (equipInfo != null)
            lwd.awaken_props = equipInfo.awaken_props;
    }

    public void UpdateLocalWeaponAwakenProps(int[] awaken_props)
    {
        PlayerServerData sd = m_kPlayer.serverData;
        if (string.IsNullOrEmpty(sd.propWeapon()) == true)
            return;

        LocalWeaponData lwd = null;
        lwd = GetLocalWeaponData(sd.propWeapon());
        lwd.awaken_props = awaken_props;
    }

    public void RestoreAmmo(int weaponID)
    {
        LocalWeaponData lwd;
        if(m_dicLocalWeaponData.TryGetValue(weaponID.ToString(), out lwd))
        {
            ConfigItemWeaponLine kWL = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weaponID);

            //lwd.clipSize = kWL.ClipSize;
            lwd.clipSize = lwd.maxAmmoNum;
            lwd.extraAmmoNum = kWL.ExtraAmmoNum;
            lwd.curClipAmmoNum = kWL.InitClipSize;
            lwd.reloadTime = kWL.ReloadTime;

            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, lwd.curClipAmmoNum, lwd.extraAmmoNum, lwd.advAmmoNum);
        }
    }

    /// <summary>
    /// Get LocalWeaponData and create if not existed 
    /// </summary>
    public LocalWeaponData GetLocalWeaponData(string weaponID)
    {
        // 第三人称会调用
        if(m_dicLocalWeaponData.ContainsKey(weaponID))
        {
            return m_dicLocalWeaponData[weaponID];
        }

        
        ConfigItemWeaponLine kWL = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(int.Parse(weaponID));
        if(kWL == null)
        {
            Logger.Error("找不到武器配置数据， 武器ID = " + weaponID);
            return null;
        }

        LocalWeaponData lwd = new LocalWeaponData();
        m_dicLocalWeaponData.Add(weaponID, lwd);
        lwd.id = weaponID;
        lwd.clipSize = kWL.ClipSize;
        lwd.curClipAmmoNum = kWL.InitClipSize;
        lwd.extraAmmoNum = kWL.ExtraAmmoNum;
        lwd.advAmmoNum = 0;
        lwd.maxAmmoNum = kWL.ClipSize;
        lwd.reloadTime = kWL.ReloadTime;
        //lwd.use_adv = false;

        if (kWL.SubWeapon > 0)
        {
            ConfigItemWeaponLine skWL = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(kWL.SubWeapon);
            if (skWL != null)
            {
                lwd.subClibSize = skWL.ClipSize;
                lwd.subAmmoNum = skWL.InitClipSize;
            }
            else
                Logger.Error("找不到子武器配置数据， 武器ID = " + kWL.SubWeapon);
        }

        return lwd;
    }

    public void ClearData()
    {
        m_dicLocalWeaponData.Clear();
        m_itemRoleID = -1;
    }

    public LocalWeaponData GetData(int id)
    {
        string str = id.ToString();
        return GetData(str);
    }

    public LocalWeaponData GetData(string id)
    {
        LocalWeaponData kWD = null;
        m_dicLocalWeaponData.TryGetValue(id, out kWD);
        return kWD;
    }


    public void SetDelayDamageData(int index, int atkType)
    {
        if (index >= GameSetting.MAX_EXIST_PROJECTILE_SIZE)
            return;
        if (m_delayDamageData[index] == null)
            m_delayDamageData[index] = new DelayDamageData();
        m_delayDamageData[index].decelerate = basePlayer.serverData.decelerate(atkType);
        m_delayDamageData[index].decelerateTime = basePlayer.serverData.decelerateTime(atkType);
    }

    public DelayDamageData GetDelayDamageData(int index)
    {
        if (index >= GameSetting.MAX_EXIST_PROJECTILE_SIZE)
            return null;
        return m_delayDamageData[index];
    }

#region Get Set

    public int itemRoleID
    {
        get { return m_itemRoleID; }
        set { m_itemRoleID = value; }
    }
#endregion

  

}

