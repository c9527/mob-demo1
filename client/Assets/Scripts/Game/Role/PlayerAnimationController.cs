﻿using UnityEngine;
using System.Collections.Generic;


public class PlayerAnimationController : PlayerAniBaseController
{
    protected const string DefaultAni = "stand";
    protected const string WalkAni = "standWalkForward";
    protected const string BehitAni = "behit";
    protected const string DeadAni = "dead";

    protected Animation m_kAnimation;
    protected FPSAnimation m_kAni;
    protected bool hasBehitAni = false;
    protected bool hasWalkAni = false;
    protected bool hasDefaultAni = false;
    protected bool hasDeadAni = false;

    public PlayerAnimationController(BasePlayer player)
    {
        m_player = player;
        m_kAni = new FPSAnimation();
    }

    override public void SetAnimation(Animation kAni)
    {
        m_kAnimation = kAni;
        if (m_kAnimation != null)
        {
            m_kAni.SetAnimation(m_kAnimation);
            m_go = kAni.gameObject;
            hasBehitAni = m_kAnimation.GetClip(BehitAni) != null;
            hasWalkAni = m_kAnimation.GetClip(WalkAni) != null;
            hasDefaultAni = m_kAnimation.GetClip(DefaultAni) != null;
            hasDeadAni = m_kAnimation.GetClip(DeadAni) != null;
        }
        else
        {
            m_go = null;
        }
    }

    override public void Play(string strAniName, int iLayer = -1, float normalizedTime = 0f)
    {

        CrossFade(strAniName);

        //if (m_kAnimation == null || m_go.activeSelf == false)
        //    return;
        //
        //if (string.IsNullOrEmpty(strAniName))
        //{
        //    Logger.Warning("PlayerAnimator strAniName is Empty");
        //    return;
        //}
        //
        //if (!CheckAni(ref strAniName))
        //    return;
        //
        //m_strCurAniName = strAniName;
        //m_kAnimation.Play(strAniName);
        //
        //if (!strAniName.StartsWith("dead") && strAniName != WalkAni && hasDefaultAni)
        //    m_kAnimation.PlayQueued(DefaultAni, QueueMode.CompleteOthers);
    }

    override public void CrossFade(string strAniName, float transitionDuration = 0.2f, int iLayer = -1)
    {
        bool isDead = false;

        if (m_kAnimation == null || m_go.activeSelf == false || string.IsNullOrEmpty(strAniName) || ((isDead = strAniName.StartsWith("dead")) == false && m_isLooping))
            return;

        if (isDead)
            m_isLooping = false;

        m_strCurAniName = strAniName;

        if (!CheckAni(ref strAniName))
            return;

        m_kAni.CrossFade(strAniName, transitionDuration);
        if (!strAniName.StartsWith("dead") && !IsCurrentAni(WalkAni) && hasDefaultAni)
            m_kAni.PlayQueued(DefaultAni, QueueMode.CompleteOthers);
    }

    public void CrossFadeQueued(string strAniName, float transitionDuration = 0.2f, int iLayer = -1)
    {
        if (m_kAnimation == null || m_go.activeSelf == false)
            return;

        if (string.IsNullOrEmpty(strAniName))
        {
            Logger.Warning("PlayerAnimator strAniName is Empty");
            return;
        }
        m_strCurAniName = strAniName;

        if (!CheckAni(ref strAniName))
            return;

        m_kAni.CrossFadeQueued(strAniName, transitionDuration, QueueMode.PlayNow);
        if (!strAniName.StartsWith("dead") && !IsCurrentAni(WalkAni) && hasDefaultAni)
            m_kAni.PlayQueued(DefaultAni, QueueMode.CompleteOthers);
    }

    override public void SetInteger(string strPropertyName, int iValue)
    {
        //Play(strPropertyName);
    }

    override public void SetTrigger(string strName)
    {
        if (strName == BehitAni && hasBehitAni)
        {
            CrossFade(strName);
        }
    }

    override public void SetBool(string strName, bool bValue)
    {
        if (strName == "move" && bValue && hasWalkAni)
        {
            if (!IsCurrentAni(WalkAni))
                CrossFade(WalkAni);
        }
        else if (!IsCurrentAni(DefaultAni))
            CrossFade(DefaultAni);
        //Play(strName);
    }

    override public void SetFloat(string strName, float bValue)
    {
        //Play(strName);
    }


    override public bool IsCurrentAni(string aniName, int layer = 0)
    {
        if (m_kAnimation == null)
            return false;
        return m_kAnimation.IsPlaying(aniName);
    }
    override public bool enable
    {
        set
        {
            if (animation != null)
                animation.enabled = value;
        }
    }

    override public Animation animation
    {
        get { return m_kAnimation; }
    }

    public void Jump2End()
    {
        // 如果动画在播放之前被sample到最后一帧，则本次不会播放，除非正在播放则没问题
        if (m_kAnimation != null && m_kAnimation.clip != null)
        {
            m_kAnimation[m_kAnimation.clip.name].normalizedTime = 1;
            m_kAnimation.Sample();
            m_kAnimation[m_kAnimation.clip.name].normalizedTime = 0;
            m_kAnimation.clip = null;
        }
    }

    /// <summary>
    /// 检查动画
    /// </summary>
    /// <param name="strAniName">动画名称</param>
    /// <returns></returns>
    private bool CheckAni(ref string strAniName)
    {
        if (m_kAnimation.GetClip(strAniName) == null)
        {
            if (strAniName.StartsWith(DeadAni))
            {
                strAniName = DeadAni;
                if (m_kAnimation.GetClip(DeadAni) == null)
                {
                    return false;
                }
            }
            else
                return false;
        }
        return true;
    }

}

