﻿using UnityEngine;
public class SkillRangeTest : MonoBehaviour
{
    public Transform player;

    public float param1 = 0f;
    public float param2 = 0f;
    public float param3 = 0;

    public float theta = 0.1f; //圆滑度

    public int type = -1;

    void OnDrawGizmos()
    {
        if (player == null || type == -1)
            return;

        Matrix4x4 defaultMatrix = Gizmos.matrix;
        Gizmos.matrix = player.localToWorldMatrix;

        Color defaultColor = Gizmos.color;
        Gizmos.color = Color.red;

        if(type == 0) //圆形或者扇形
        {
            Vector3 beginPoint = Vector3.zero;
            Vector3 firstPoint = Vector3.zero;
            Vector3 endPoint = Vector3.zero;

            if (param2 > 360)
                param2 = param2 % 360;

            float angle = param2 / 360;

            float angleOffset = (180 - param2) / 2 / 360;

            for (float t = 2 * Mathf.PI * angleOffset; t < 2 * Mathf.PI * (angle + angleOffset); t += theta)
            {
                float x = param1 * Mathf.Cos(t);
                float z = param1 * Mathf.Sin(t);
                endPoint = new Vector3(x, 0, z);
                if (t == 2 * Mathf.PI * angleOffset)
                {
                    firstPoint = endPoint;
                }
                else
                {
                    Gizmos.DrawLine(beginPoint, endPoint);
                }
                beginPoint = endPoint;
            }

            if(param2 < 360)
            {
                endPoint = new Vector3(param1 * Mathf.Cos(2 * Mathf.PI * (angle + angleOffset)), 0, param1 * Mathf.Sin(2 * Mathf.PI * (angle + angleOffset)));
                Gizmos.DrawLine(beginPoint, endPoint);

                Gizmos.DrawLine(Vector3.zero, firstPoint);
                Gizmos.DrawLine(Vector3.zero, endPoint);
            }
            else
            {
                Gizmos.DrawLine(beginPoint, firstPoint);
            }

        }
        else if(type == 1) //矩形
        {
            Vector3 left = Vector3.left * param1 * 0.5f;
            Vector3 right = Vector3.right * param1 * 0.5f;

            Vector3 leftEnd = left + Vector3.forward * param2;
            Vector3 rightEnd = right + Vector3.forward * param2;

            Gizmos.DrawLine(left, leftEnd);
            Gizmos.DrawLine(right, rightEnd);
            Gizmos.DrawLine(leftEnd, rightEnd);
            Gizmos.DrawLine(Vector3.zero, left);
            Gizmos.DrawLine(Vector3.zero, right);
        }

        Gizmos.color = Color.green;
        Gizmos.DrawLine(Vector3.zero, Vector3.forward * 20f);

        Gizmos.color = defaultColor;
        Gizmos.matrix = defaultMatrix;
    }

    public void Draw(Transform player, int type, float param1, float param2)
    {
        this.player = player;
        this.type = type;
        this.param1 = param1;
        this.param2 = param2;
    }
}
