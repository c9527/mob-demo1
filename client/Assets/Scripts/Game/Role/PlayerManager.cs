﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerManager
{
    #region Cache GameObject
    static private Dictionary<string, List<GameObject>> m_dicPlayerGO = new Dictionary<string, List<GameObject>>();

    static public void ReleaseGO()
    {
        foreach (KeyValuePair<string, List<GameObject>> kvp in m_dicPlayerGO)
        {
            for (int i = 0; i < kvp.Value.Count; i++)
            {
                ResourceManager.Destroy(kvp.Value[i]);
            }
        }

        m_dicPlayerGO.Clear();
    }

    static public void CacheGO(string name, GameObject go)
    {
        if (go == null)
            return;

        List<GameObject> list = null;
        m_dicPlayerGO.TryGetValue(name, out list);
        if (list == null)
        {
            list = new List<GameObject>();
            m_dicPlayerGO.Add(name, list);
        }

        Transform trans = go.transform;
        trans.SetParent(SceneManager.singleton.reclaimRoot);
        trans.localPosition = Vector3.zero;

        list.Add(go);

        //Logger.Warning("CacheGO name:" + name + "    Count:" + list.Count);
    }

    static public GameObject GetGO(string name)
    {
        List<GameObject> list = null;
        m_dicPlayerGO.TryGetValue(name, out list);
        if (list == null || list.Count == 0)
            return null;

        int rearIndex = list.Count - 1;
        GameObject ret = list[rearIndex];
        list.RemoveAt(rearIndex);

        //Logger.Warning("GetGO name:" + name + "    Count:" + list.Count);

        return ret;
    }

    #endregion


    #region Cache Player
    static private Dictionary<string, List<BasePlayer>> m_dicWeaponObj = new Dictionary<string, List<BasePlayer>>();

    static public T NewObj<T>() where T : BasePlayer, new()
    {
        T player = GetObj<T>();
        if (player == null)
            player = new T();
        else
        {
            player.ReActive();
        }

        return player;
    }

    static public void ReleaseObj()
    {
        foreach (var players in m_dicWeaponObj.Values)
        {
            for (int i = 0; i < players.Count; ++i)
            {
                if(players[i] != null)
                    players[i].Release(true);
            }
        }
        m_dicWeaponObj.Clear();
    }

    static public void CacheObj(BasePlayer player)
    {
        if (player == null)
            return;

        string className = player.GetType().Name;

        List<BasePlayer> list = null;
        m_dicWeaponObj.TryGetValue(className, out list);
        if (list == null)
        {
            list = new List<BasePlayer>();
            m_dicWeaponObj.Add(className, list);
        }
        list.Add(player);
        //Logger.Log("CacheObj className:" + className + "    Count:" + list.Count);
    }

    static public T GetObj<T>() where T : BasePlayer
    {
        List<BasePlayer> list = null;
        string className = typeof(T).Name;
        m_dicWeaponObj.TryGetValue(className, out list);

        if (list == null || list.Count == 0)
            return null;

        int rearIndex = list.Count - 1;
        BasePlayer ret = list[rearIndex];
        list.RemoveAt(rearIndex);
        //Logger.Log("GetObj className:" + className + "    Count:" + list.Count);
        return ret as T;
    }

    #endregion
}