﻿using System.Collections.Generic;
using UnityEngine;

public class VisibilityManager
{
    private float m_fcurVisableDegree = 1f;              //当前可视度
    private float m_fFinalVisableDegree = 1f;           //目标可视度
    private float m_fTargetVisableDegree = 1f;
    private float m_fShadeSpeed = 0f;      //可视度变化速度
    private float m_fShadeLeftTime = -1; //设置透明度剩余时间
    private bool m_bIsStealth = false;                      //是否隐身
    private BasePlayer m_player;

    private Dictionary<int, Renderer> m_dicRenderer;
    private Dictionary<int, Shader> m_dicOrgShader;

    public VisibilityManager()
    {
        m_dicRenderer = new Dictionary<int, Renderer>();
        m_dicOrgShader = new Dictionary<int, Shader>();
        m_fcurVisableDegree = 1f;
        m_fFinalVisableDegree = 1f;
        m_fTargetVisableDegree = 1f;
        m_fShadeSpeed = 0f;
        m_fShadeLeftTime = -1;
        m_bIsStealth = false;
    }

    public void Stop()
    {
        SetVisable(1f, 1f, false);
        if (m_dicRenderer != null)
        {
            m_dicRenderer.Clear();
        }
        if (m_dicOrgShader != null)
        {
            m_dicOrgShader.Clear();
        }

        m_fcurVisableDegree = 1f;
        m_fFinalVisableDegree = 1f;
        m_fShadeSpeed = 0f;
        m_fShadeLeftTime = -1;
        m_bIsStealth = false;
    }

    public void Release()
    {
        Stop();
    }

    public void SetPlayer(BasePlayer player)
    {
        m_player = player;
    }

    public void AddRenderer(List<Renderer> rendererList)
    {
        if (rendererList == null)
            return;

        for (int i = 0, imax = rendererList.Count; i < imax; ++i )
        {
            Renderer renderer = rendererList[i];
            AddRenderer(renderer);
        }
    }

    public void AddRenderer(Renderer[] renderers)
    {
        if (renderers == null)
            return;

        for (int i = 0; i < renderers.Length; ++i)
        {
            AddRenderer(renderers[i]);
        }
    }

    public void AddRenderer(Renderer renderer)
    {
        if (renderer == null)
            return;

        int key = renderer.GetInstanceID();
        if (!m_dicRenderer.ContainsKey(key))
        {
            m_dicRenderer.Add(key, renderer);
            //m_dicOrgShader.Add(key, renderer.sharedMaterial.shader);
            m_dicOrgShader.Add(key, renderer.material.shader);
        }
        else
        {
            m_dicRenderer[key] = renderer;
            m_dicOrgShader[key] = renderer.material.shader;
        }
    }

    public void RemoveAllRenderer()
    {
        //SetVisable(1f, false);
        //m_fcurVisableDegree = 1f;              //当前可视度
        //m_fFinalVisableDegree = 1f;           //目标可视度
        //m_fTargetVisableDegree = 1f;
        m_fShadeLeftTime = -1;
        if(m_bIsStealth)
            RecoverShader();
        m_dicRenderer.Clear();
        m_dicOrgShader.Clear();
    }

    public void RemoveRenderer(List<Renderer> rendererList)
    {
        if (rendererList == null)
            return;

        for (int i = 0, imax = rendererList.Count; i < imax; ++i)
        {
            Renderer renderer = rendererList[i];
            RemoveRenderer(renderer);
        }
    }

    public void RemoveRenderer(Renderer[] renderers)
    {
        if (renderers == null)
            return;

        for (int i = 0; i < renderers.Length; ++i)
        {
            RemoveRenderer(renderers[i]);
        }
    }

    public void RemoveRenderer(Renderer renderer)
    {
        if (renderer == null)
            return;

        int key = renderer.GetInstanceID();
        if (m_dicRenderer.ContainsKey(key))
        {
            m_dicRenderer.Remove(key);
            m_dicOrgShader.Remove(key);
        }
    }

    public void RefreshVisable()
    {
        SetVisable(m_fTargetVisableDegree,1f, false, true);
    }

    public void SetVisable(float visableDegree = 1f,float time = 1f, bool isFade = true, bool force = false)
    {
        m_fTargetVisableDegree = visableDegree;

        if (m_dicRenderer.Count == 0)
        {
            m_fFinalVisableDegree = visableDegree;
            return;
        }

        bool isStealth = visableDegree < 1f;
        if (force || m_bIsStealth != isStealth)
        {
            m_bIsStealth = isStealth; 
            if (isStealth)
            {
                int loadShadeCount = 0;
                int rendererCount = m_dicRenderer.Count;
                foreach (Renderer renderer in m_dicRenderer.Values)
                {
                    if (renderer == null)
                        continue;

                    Renderer r = renderer;
                    ResourceManager.LoadShader(GameConst.SHADER_ALPHASET, (newShader) =>
                    {
                        if (newShader == null)
                        {
                            Logger.Error(GameConst.SHADER_ALPHASET + " load null");
                            return;
                        }
                        ++loadShadeCount;
                        int key = r.GetInstanceID();
                        r.gameObject.ReplaceShader("", null, newShader, renderer.useLightProbes, true, false);
                        if (loadShadeCount == rendererCount)
                            SetVisableDegree(visableDegree, isFade, time);
                    });
                }
            }
            else
            {
                m_fFinalVisableDegree = visableDegree;
                RecoverShader();
            }
        }
        else
        {
            if (isStealth && visableDegree != m_fFinalVisableDegree)
            {
                SetVisableDegree(visableDegree, isFade, time);
            }
        }
    }

    public void Update()
    {
        if (m_bIsStealth && m_fShadeLeftTime >= 0)
        {
            m_fShadeLeftTime -= Time.deltaTime;
            if (m_fShadeLeftTime <= 0)
            {
                m_fcurVisableDegree = m_fFinalVisableDegree;
                m_fShadeLeftTime = -1;
            }
            else
            {
                m_fcurVisableDegree += m_fShadeSpeed * Time.deltaTime;
            }
            SetAlpha(m_fcurVisableDegree);
        }
    }

    private void SetVisableDegree(float visableDegree, bool isFade, float time)
    {
        if( time == 0 )
        {
            time = 1f;
            Logger.Log("可视度时间变化为time  = 0");
        }
        m_fFinalVisableDegree = visableDegree;
        if (!isFade || (m_fShadeSpeed = (m_fFinalVisableDegree - m_fcurVisableDegree) / time) >= 0)
        {
            m_fcurVisableDegree = m_fFinalVisableDegree;
            m_fShadeLeftTime = -1;
            SetAlpha(m_fcurVisableDegree);
        }
        else
            m_fShadeLeftTime = time;
    }

    private void SetAlpha(float visableDegree)
    {
        if (m_bIsStealth)
        {
            foreach (Renderer renderer in m_dicRenderer.Values)
            {
                //Material mat = renderer.sharedMaterial;
                if(renderer != null)
                {
                    Material mat = renderer.material;
                    if(mat != null)
                        mat.SetFloat(GameConst.SHADER_PROPERTY_ALPHA, visableDegree);
                }
               
            }
        }
    }

    private void RecoverShader()
    {
        foreach (var rendererItem in m_dicRenderer)
        {
            Renderer renderer = rendererItem.Value;
            if (renderer == null)
                continue;

            Shader orgShader = m_dicOrgShader[rendererItem.Key];
            //Logger.Log("Player:" + m_player.PlayerName + "   gameObject:" + renderer.name + "  instanceId:" + renderer.GetInstanceID() + "  Revert Shader to orgShader:" + orgShader.name);
            renderer.gameObject.ReplaceShader("", null, orgShader, renderer.useLightProbes, true, false);
        }
    }

    public float visableDegree
    {
        get { return m_fTargetVisableDegree; }
    }

    public int rendererCount
    {
        get { return m_dicRenderer.Count; }
    }
}
