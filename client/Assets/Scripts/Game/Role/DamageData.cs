﻿using UnityEngine;

public struct DamageData
{
    public BasePlayer victim;
    public BasePlayer attacker;
    public int bodyPart;
    public int damage;
    public Vector3 hitPos;
    public byte hitType;

    public bool IsCrit()
    {
        return (hitType & 1) == 1;
    }
}