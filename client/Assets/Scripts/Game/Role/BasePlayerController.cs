﻿using UnityEngine;
using System.Collections;

public class BasePlayerController : MonoBehaviour
{
    protected const int JUMP_STATUS_UP = 1;//"jump_status_up";
    protected const int JUMP_STATUS_DOWN = 2;//"jump_status_down";
    protected const int JUMP_STATUS_COMPLETE = 3;//"jump_status_complete";

    protected bool m_released;
    private BasePlayer m_kPlayer;
    protected GameObject m_go;
    protected Transform m_trans;

    protected VtfIntCls m_jumpStatus = new VtfIntCls();

    virtual public void Awake()
    {
        m_go = gameObject;
        m_trans = transform;

        m_jumpStatus.val = JUMP_STATUS_COMPLETE;
    }

    virtual public void OnEnable()
    {
        m_go = gameObject;
        m_trans = transform;
    }

    virtual public void Release()
    {
        m_released = true;
        m_go = null;
        m_trans = null;
    }

    virtual public void BindPlayer(BasePlayer kBP)
    {
        m_kPlayer = kBP;
    }

    virtual protected void Update()
    {

    }   

    virtual protected void UpdateGravity()
    {
         
    }
   
    virtual protected void UpdateJump()
    {
       
    }

    virtual public void StartJump()
    {

    }

    virtual public void JumpComplete()
    {

    }

    virtual public void SetPitchAngle(float angle, float tweenTime)
    {
        
    }

    virtual public void SetHeadScale(float scale)
    {

    }

    virtual public void ReActive()
    {
        m_released = false;
    }

    public int jumpStatus
    {
        get { return m_jumpStatus.val; }
        set { m_jumpStatus.val = value; }
    }
   
}

