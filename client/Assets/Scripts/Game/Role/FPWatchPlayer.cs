﻿using System;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 观点第一人称玩家
/// </summary>
public class FPWatchPlayer : ServerPlayer
{
    //static private Dictionary<string, List<GameObject>> ms_dicGOCache = new Dictionary<string, List<GameObject>>();

    public static FPWatchPlayer singleton;

    private bool m_bIsChangeModel;
    private bool m_bIsReplaySwitchWeapon;

    private int m_iSwitchTimer = -1;

    private Animation m_kMainPlayerCameraAnimation;
    private float m_fSceneCameraSlotLocalY;

    private bool m_bCameraMoveAniPlaying;
    private bool m_bCameraIdleAniPlaying;

    // Component
    private Transform m_transMainPlayerCamera;
    private Camera m_cameraMainPlayer;
    private float m_kGunHoldHand = 1f;
    private Transform m_transMainPlayerCameraSlot;
    private Transform m_transMainPlayerCameraShakeTarget;
    private Animation m_kMainPlayer;

    private CharacterController m_kCharacterCtrler;
    private CapsuleCollider m_cc;
    private MainPlayerCameraShaker m_kMPCS;

    private Vector3 m_bornPos;

    private BasePlayer m_relatePlayer;  //第一人称视角关联的角色
    private bool m_firstLoad;

    protected bool m_isSwitchViewDone;  //视角切换是否完成

    private float m_lastReceivePosTime;
    private float m_lastTweenPosTime;          // 刚刚缓动的那个包的时间
    private bool m_isWatching;                 // 是否正在观战
    private int m_iEnableSwitchTimerId = -1;

    private float m_sceneFov;

    private bool m_bForbidMainCam;

    private PlayerServerData m_orginServerData;
    private LocalPlayerData m_orginLocalData;

    public bool ForbidMainCam
    {
        get { return m_bForbidMainCam; }
        set
        {
            m_bForbidMainCam = value;
            if (gameObject != null)
            {
                EnableMainPlayerCamera(!m_bForbidMainCam && isWatching);
            }
        }
    }

    private UIEffect m_addLifeEffect;

    public FPWatchPlayer()
    {
        singleton = this;
    }

    #region 开始和结束第一人称视角观战
    public void StartWatch(BasePlayer player)
    {
        if (player == m_relatePlayer || player == this)
            return;

        m_actQueue.Clear();
        if (m_rotTweener != null)
            m_rotTweener.enabled = false;
        if (m_posTweener != null)
            m_posTweener.enabled = false;

        GameDispatcher.Dispatch<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, false);
        m_isWatching = true;

        if (curWeapon != null)
            curWeapon.WeaponSwitchOff();

        if (m_relatePlayer != null)
            m_relatePlayer.RecoverView(this);
        m_relatePlayer = player;
        m_orginServerData = m_kServerData;
        m_orginLocalData = m_localPlayerData;
        m_kServerData = player.serverData;  //直接使用第三人称的服务端数据
        m_localPlayerData = player.localPlayerData;
        m_relatePlayer.isSwitchFPV = true;
        WorldManager.singleton.SetPlayerById(player.PlayerId, this);

        m_isSwitchViewDone = false;
        OnData_Actor();
        m_kVM.SetVisable(player.visableDegree);
        Revive(true);
        if (m_iEnableSwitchTimerId != -1)
        {
            TimerManager.RemoveTimeOut(m_iEnableSwitchTimerId);
            m_iEnableSwitchTimerId = -1;
        }
        if (m_iSwitchTimer != -1)
            m_iEnableSwitchTimerId = TimerManager.SetTimeOut(5, () => { GameDispatcher.Dispatch<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, true); });
    }

    public void EndWatch()
    {
        m_isWatching = false;

        ResetCameraFov();
        EnableMainPlayerCamera(false);
        TryToDetachSceneCamera();
        if (m_relatePlayer != null)
        {
            if (curWeapon != null)
                curWeapon.Reset();
            //WorldManager.singleton.SetPlayerById(m_relatePlayer.PlayerId, m_relatePlayer);
            m_relatePlayer.RecoverView(this);
            m_relatePlayer = null;
        }
        m_kServerData = m_orginServerData;
        m_localPlayerData = m_orginLocalData;
        SetActive(false);

        if (m_iSwitchTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iSwitchTimer);
            m_iSwitchTimer = -1;
        }
        if (m_iEnableSwitchTimerId != -1)
        {
            TimerManager.RemoveTimeOut(m_iEnableSwitchTimerId);
            m_iEnableSwitchTimerId = -1;
            GameDispatcher.Dispatch<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, true);
        }

    }

    public BasePlayer Leave()
    {
        BasePlayer bp = m_relatePlayer;
        if (bp != null)
            bp.Release(false);
        m_relatePlayer = null;
        EndWatch();
        return bp;
    }
    #endregion

    #region 加载相关

    override public void LoadRes()
    {
        if (SceneManager.singleton.battleSceneLoaded == false)
        {
            return;
        }
        if (m_strModel == null || m_strModelPre == m_strModel && m_loadModel)    // 模型只加载一次
        {
            if(m_relatePlayer != null)
                m_relatePlayer.SetActive(false);
            return;
        }

        if (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX ||
            serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX)
            AudioManager.PlayFightUISound(AudioConst.bioMotherShow);

        if (m_loadModel == false)
        {
            m_strModelPre = m_strModel;
            m_iModelTypePre = m_iModelType;
            m_loadModel = true;
        }

        if (m_strModelPre != m_strModel && gameObject != null)
        {
            m_bIsChangeModel = true;
            WorldManager.singleton.UnRegisterPlayerGOHash(m_kGo.GetHashCode());
            DestroyPlayerOldModel();
        }

        GameObject goCC = null;
        #region CallBack
        Action<GameObject, uint> loadCallBack = (go, crc) =>
        {
            if (go == null)
            {
                Logger.Error("FPVPlayer model load failed, " + m_strModel);
                return;
            }

            string modelName = GameObjectHelper.GetNameWithoutClone(go.name);

            if (goCC != null && !go.transform.FindChild(modelName))
            {
                goCC.name = modelName;
                go.transform.SetParent(goCC.transform);
                go.transform.ResetLocal();
                go = goCC;
            }

            if (released)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                CacheGO(go);
                return;
            }

            if (modelName != m_strModel)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                CacheGO(go);
                Logger.Warning("FPVPlayer loaded model is not the current model,  " + modelName + " " + m_strModel);
                return;
            }

            if (m_isWatching == false)
            {
                CacheGO(go);
                return;
            }

            if (m_relatePlayer != null)
                m_relatePlayer.SetActive(false);

            SetGO(go);
            if(serverData!=null)
            {
                SetPosNow(serverData.pos);
                SetState(serverData.state, false);
                if (isAlive)
                {
                    if (serverData.actorState != null)
                        SetBuff(serverData.actorState.buff_list);
                    else if (serverData.actorState == null && serverData.roleData.buff_list != null)
                        SetBuff(serverData.roleData.buff_list);
                }
            }                       
            if (m_bIsReplaySwitchWeapon)
            {
                if(m_kPlayerAniController!=null&&curWeapon != null)
                    m_kPlayerAniController.Play(m_isSwitchViewDone ? curWeapon.weaponConfigLine.MPSwitchAni : curWeapon.weaponConfigLine.MPIdleAni);
                m_bIsReplaySwitchWeapon = false;
                //m_isSwitchViewDone = true;
            }
            m_bIsChangeModel = false;

            WorldManager.singleton.RegisterPlayerGOHash(go.GetHashCode(), this);

            BasePlayer bp = this;
            GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_JOIN, bp);
        };
        #endregion
        //string path = m_iModelType == GameConst.MODEL_TYPE_DMM ? PathHelper.GetDmmPrefabPath(m_strModel) : PathHelper.GetRolePrefabPath(m_strModel);
        string path = GetRolePrefablPath(m_iModelType, m_strModel);

        GameObject goCached = GetCacheGO(m_strModel);
        if (goCached != null)
        {
            Logger.Log("获得缓存模型:" + m_strModel);
            loadCallBack(goCached, 0);
        }
        else
        {
            ResourceManager.LoadAsset(PathHelper.GetRolePrefabPath(ModelConst.MODEL_MAINPLAYER_CC_GO), (obj) =>
            {
                goCC = GameObject.Instantiate(obj) as GameObject;
                ResourceManager.LoadBattlePrefab(path, loadCallBack, EResType.Battle, true);
            });
        }
    }

    override public void Release(bool releaseGO)
    {
        BasePlayer bp = m_relatePlayer;
        if (bp != null)
            bp.Release(releaseGO);
        m_relatePlayer = null;
        EndWatch();
        base.Release(releaseGO);
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.SetParent(null);

        if (m_iEnableSwitchTimerId != -1)
        {
            TimerManager.RemoveTimeOut(m_iEnableSwitchTimerId);
            m_iEnableSwitchTimerId = -1;
        }

        if (releaseGO)
        {
            if (m_kPlayerController != null)
                m_kPlayerController.Release();
            m_kPlayerController = null;
            m_transMainPlayerCamera = null;
            m_cameraMainPlayer = null;
            m_transSceneCameraSlot = null;
            m_transMainPlayerCameraSlot = null;
            m_transMainPlayerCameraShakeTarget = null;
            m_kMainPlayerCameraAnimation = null;
            m_kMPCS = null;
        }
    }

    protected override void DestroyPlayerOldModel()
    {
        base.DestroyPlayerOldModel();
        m_cameraMainPlayer = null;
    }

    override protected void SetGO(GameObject go)
    {
        base.SetGO(go);

        GameObjectHelper.GetComponentsByTraverse<Renderer>(go).useLightProbes = true;

        if (m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            m_transSceneCameraSlot.localPosition = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
            m_transSceneCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT;
            m_fSceneCameraSlotLocalY = m_transSceneCameraSlot.localPosition.y;

            m_transMainPlayerCameraSlot = GameObjectHelper.CreateChildIfNotExisted(go.transform, ModelConst.SLOT_MAINPLAYER_CAMERA).transform;
            m_transMainPlayerCameraSlot.localPosition = GameSetting.MAIN_PLAYER_CAMERA_LOCALPOS;
            m_transMainPlayerCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_CAMERA_LOCALROT;
            InitMainPlayerCamera();
        }
        else
        {
            GameObjectHelper.VisitChildByTraverse(go.transform, (obj) => { obj.gameObject.DestroyComponent<Collider>(); });
        }

        height = GetPlayerHeightStand();

        //添加CharacterController
        m_kCharacterCtrler = go.AddMissingComponent<CharacterController>();
        m_kCharacterCtrler.radius = GetPlayerRadius();
        m_kCharacterCtrler.height = height;
        //m_kCharacterCtrler.center = new Vector3(0, m_kCharacterCtrler.height * 0.5f + GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        m_kCharacterCtrler.center = GetPlayerCenter() + new Vector3(0, GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        m_kCharacterCtrler.slopeLimit = GameSetting.PlayerColliderSlope;
        m_kCharacterCtrler.stepOffset = GameSetting.MPStepOffset;

        CapsuleCollider cc = go.AddMissingComponent<CapsuleCollider>();
        m_cc = cc;
        cc.isTrigger = true;
        cc.height = m_kCharacterCtrler.height;
        cc.radius = m_kCharacterCtrler.radius;
        cc.center = m_kCharacterCtrler.center;

        m_kPlayerController = GameObjectHelper.GetComponent<FPWatchPlayerController>(go);
        m_kPlayerController.BindPlayer(this);

        m_kMPCS = GameObjectHelper.GetComponent<MainPlayerCameraShaker>(go);
        m_kMPCS.BindMainPalyerCamera(m_transMainPlayerCameraShakeTarget);
        if (m_iModelType == GameConst.MODEL_TYPE_ROLE && m_kPlayerAniController is PlayerAnimatorController)
        {
            if (m_kPlayerAniController.animator != null)
            {
                m_kPlayerAniController.animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            }
        }

        m_kStepSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());
        m_kHeartSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());
        m_kVoiceSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());

        m_arrBehaviour = m_kGo.GetComponentsInChildren<Behaviour>();
        SetActive(false);
    }

    private void InitMainPlayerCamera()
    {
        m_transMainPlayerCameraShakeTarget = m_transMainPlayerCameraSlot.FindChild("MainPlayerCameraShakeTarget");
        if (m_transMainPlayerCameraShakeTarget == null)
        {
            GameObject goMainPlayerCameraShakeTarget = new GameObject("MainPlayerCameraShakeTarget");
            m_transMainPlayerCameraShakeTarget = goMainPlayerCameraShakeTarget.transform;
            m_transMainPlayerCameraShakeTarget.SetParent(m_transMainPlayerCameraSlot);
            m_transMainPlayerCameraShakeTarget.ResetLocal();
        }

        m_transMainPlayerCamera = m_transMainPlayerCameraShakeTarget.FindChild("MainPlayerCamera");
        if (m_transMainPlayerCamera == null)
        {
            GameObject goMainPlayerCamera = new GameObject("MainPlayerCamera");
            m_transMainPlayerCamera = goMainPlayerCamera.transform;
            m_transMainPlayerCamera.SetParent(m_transMainPlayerCameraShakeTarget);
            m_transMainPlayerCamera.ResetLocal();
        }
        m_cameraMainPlayer = GameObjectHelper.GetComponent<Camera>(m_transMainPlayerCamera.gameObject);
        

        m_cameraMainPlayer.orthographic = false; 
        m_cameraMainPlayer.fieldOfView = GameSetting.FOV_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.nearClipPlane = GameSetting.MAINPLAYER_CAMERA_NEARPLANE;
        m_cameraMainPlayer.farClipPlane = GameSetting.MAINPLAYER_CAMERA_FARPLANE;
        m_cameraMainPlayer.cullingMask = GameSetting.LAYER_MASK_MAIN_PLAYER;
        m_cameraMainPlayer.depth = GameSetting.CAMERA_DEEPTH_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.clearFlags = CameraClearFlags.Depth;
        m_cameraMainPlayer.useOcclusionCulling = false;

        m_kMainPlayerCameraAnimation = GameObjectHelper.GetComponent<Animation>(m_transMainPlayerCamera.gameObject);
        m_kMainPlayerCameraAnimation.playAutomatically = false;

        FlipMainCamera();
    }

    public override void UpdateGameObjectLayer()
    {
        if (gameObject != null)
        {
            if (m_iModelType == GameConst.MODEL_TYPE_ROLE)
                gameObject.ApplyLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER, null, null, true);   // 主角相机要用
            else
                gameObject.ApplyLayer(GameSetting.LAYER_VALUE_DMM_PLAYER, null, null, true);   // 躲猫猫用
        }
    }

    override public void WeaponAttachComplete()
    {
        base.WeaponAttachComplete();

        ConfigItemWeaponLine kWL = m_kWeaponMgr.curWeapon.weaponConfigLine;

        m_kMPCS.apply = false;
        m_kMPCS._RecoilValue = kWL.RecoilDis;
        m_kMPCS._RecoilRecoverSpeed = kWL.RecoilRecoverSpeed;

        m_bCameraIdleAniPlaying = false;
        m_bCameraMoveAniPlaying = false;

        // 主角相机也能看到主角的枪
        if (curWeapon != null)
            curWeapon.ApplyMainPlayerLayer();
    }

    override public float GetPlayerRadius()
    {
        if (m_configData != null && m_configData.OPRadius != 0)
        {
            return m_configData.OPRadius;
        }
        return base.GetPlayerRadius();
    }
    override public Vector3 GetPlayerCenter()
    {
        if (m_configData != null && m_configData.MPCenter != Vector3.zero)
        {
            return m_configData.MPCenter;
        }
        return base.GetPlayerCenter();
    }

    #endregion

    #region 同步（跟第三人称方法基本一致）
 
    override public void Update()
    {
        base.Update();

        WorldManager.singleton.CameraSwitcher.Update();
    }

    #endregion

    #region 其他动作同步

    override internal void Behit(p_hit_info_toc hitInfo, BasePlayer kAttacker = null, Vector3 hitPos = default(Vector3), int skillId = 0, System.Object arg = null)
    {
        if (m_kServerData == null || !isWatching)
            return;

        int hitValue = m_kServerData.life - hitInfo.life;

        base.Behit(hitInfo, kAttacker, hitPos);

        if (serverData.alive == false)
            return;

        if (hitValue > 0)
        {
            if (kAttacker != null && transform != null)
            {
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BEHIT, kAttacker.position);
            }

            //播2d版的受击音效
            if (curWeapon != null)
            {
                if (kAttacker != null && (kAttacker.curWeapon is WeaponKnife || kAttacker.curWeapon is WeaponPaw))
                    curWeapon.PlayerHitSound(position, hitInfo.bodyPart, Effect.MAT_EFFECT_KNIFE, 0.5f);
                else
                    curWeapon.PlayerHitSound(position, hitInfo.bodyPart, Effect.MAT_EFFECT_GUN, 0.5f);
            }
        }
        else if (hitValue < 0 && kAttacker.Camp == Camp)
        {
            if (m_addLifeEffect != null)
                m_addLifeEffect.Play();
            else
                m_addLifeEffect = UIEffect.ShowEffect(UIManager.GetPanel<PanelBattle>().transform, EffectConst.UI_PLAYER_ADD_LIFE);
        }
    }

    override public void Die(BasePlayer killer, int weapon, int hitPart = BODY_PART_ID_BODY, Vector3 bulletDir = default(Vector3), Vector3 diePos = default(Vector3), Vector3 killerPos = default(Vector3))
    {
        base.Die(killer, weapon, hitPart, bulletDir, diePos, killerPos);

        if (ScreenEffectManager.singleton != null)
            ScreenEffectManager.singleton.HideAllEffect();

        Vector3 effectPos = centerPos;

        FallOnTheGround();

        if (gameObject != null)
        {
            m_cc.enabled = false;

            if (m_kMainPlayerCameraAnimation != null)
                m_kMainPlayerCameraAnimation.Stop();

            SceneCamera.singleton.SetParent(null);
            SceneCameraController.singleton.StopControl();

            if (curWeapon != null)
                curWeapon.Reset();
           
            ConfigItemWeaponLine wc = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weapon);
            int dieAniType = wc != null ? wc.DieAniType : 1;
            Vector3 fromPos = killer != null ? killer.position : Vector3.zero;

            bool _isSimpleDeadAni = GetOpModelIsSimpleDeadAni();
            PlayerDeadCameraSwitchData kData = new PlayerDeadCameraSwitchData()
            {
                pos = position,
                face = transform.forward,
                killerPos = fromPos,
                killer = killer,
                model = m_configData.OPModel,
                isShowModel = string.IsNullOrEmpty(m_configData.DieEffect),
                isSimpleDeadAni = _isSimpleDeadAni,
                deadAni = _isSimpleDeadAni ?
                    AniConst.GetSimpleDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, status.stand) :
                    AniConst.GetPlayerDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, dieAniType, status.stand, hitPart == BODY_PART_ID_HEAD)
            };
            WorldManager.singleton.CameraSwitcher.PlayMainPlayerDeadSwitch(kData);
            //if (m_iModelType == GameConst.MODEL_TYPE_DMM)
            if (!string.IsNullOrEmpty(m_configData.DieEffect))
            {
                //EffectManager.singleton.GetEffect<EffectTimeout>(EffectConst.FIGHT_HIDE_ITEM_DIE, (effect) =>
                EffectManager.singleton.GetEffect<EffectTimeout>(m_configData.DieEffect, (effect) =>
                {
                    SetActive(false);
                    effect.Attach(effectPos);
                    effect.SetTimeOut(-1);
                    effect.Play();
                });
            }
            else
            {
                SetActive(false);
            }
        }
        ClearState();
    }

    //internal override void Jump(toc_fight_jump data)
    //{
    //    if (m_kPlayerController == null || alive == false)
    //    {
    //        return;
    //    }

    //    TweenPos(data.pos, -1, -1, -1, data.type);
    //    //Logger.Log("Jump State = " + data.type);
    //}

    override public void Crouch()
    {
        if (alive == false)
            return;

        base.Crouch();

        if (m_iModelType != GameConst.MODEL_TYPE_DMM && m_transSceneCameraSlot != null)
        {
            float fR = GetPlayerHeightCrouch() / GetPlayerHeightStand();
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY * fR;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        }
    }

    override public void Stand(bool bSend2Server = false, bool bNeedGrounded = true)
    {
        if (alive == false || released)
            return;

        base.Stand(bSend2Server, bNeedGrounded);

        if (m_iModelType != GameConst.MODEL_TYPE_DMM && m_transSceneCameraSlot != null)
        {
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        }
    }

    #endregion

    #region 状态变化

    protected override void Lock()
    {
        // 移除场景相机
        if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
        {
            m_transSceneCameraSlot.GetChild(0).SetParent(null);
            Logger.Warning("MainPlayer Lock, Remove SceneCamera");
        }

        base.Lock();
    }

    protected override void Revive(bool dataRevive)
    {
        m_bornPos = PlayerServerData.GetPos(serverData.pos);

        DeadPlayer.Instance.HideRescue();

        base.Revive(dataRevive);

        if (SceneCameraController.singleton != null)
            SceneCameraController.singleton.StopControl();

        if(gameObject != null)
        {
            m_cc.enabled = true;

            m_bCameraIdleAniPlaying = false;
            m_bCameraMoveAniPlaying = false;

            if (curWeapon != null)
                curWeapon.Reset();          

            Stand(false, false);
            AttachSceneCamera();

            SwitchDefaultWeapon(false);

            if (curWeapon != null)
            {
                if (curWeapon.localWeaponData.isZoomIn)
                    curWeapon.ZoomInImmediate();
                else
                {
                    curWeapon.ZoomOutImmediate();
                    EnableMainPlayerCamera(true);
                }
            }
            else
            {
                EnableMainPlayerCamera(true);
            }

            SceneCamera.singleton.enableCamera = true;
            
            if (Driver.singleton != null && Driver.singleton.controlMode == Driver.CONTROL_MODE_INPUT)
                MainPlayerControlInputReceiver.singleton.enabled = true;
            //GameDispatcher.Dispatch(GameEvent.UI_SHOW_FIRE_AIM_POINT, true);
        }
    }

    public override void SetActive(bool value)
    {
        base.SetActive(value);

        if (m_cc != null)
            m_cc.enabled = value;

        if (m_kCharacterCtrler != null)
            m_kCharacterCtrler.enabled = value;

        if (value)
            Physics.IgnoreCollision(m_cc, m_kCharacterCtrler, true);

        if (m_cameraMainPlayer != null)
            m_cameraMainPlayer.enabled = value;
    }

    #endregion

    #region 相机相关
    public void SetSceneFov(float value)
    {
        SceneCamera.singleton.fieldOfView = SceneFov * value;
    }

    public void SetMainPlayerFov(float value)
    {
        m_cameraMainPlayer.fieldOfView = MainFov * value;
    }

    public void EnableMainPlayerCamera(bool value)
    {
        if (m_cameraMainPlayer != null)
        {
            m_cameraMainPlayer.enabled = value;
        }
        if (value && MainPlayer.singleton != null)
        {
            MainPlayer.singleton.EnableMainPlayerCamera(false);
        }

    }

    public void FlipMainCamera()
    {
        if (m_cameraMainPlayer != null && GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_HOLD_HAND) != m_kGunHoldHand)
        {
            m_kGunHoldHand = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_HOLD_HAND);
            Matrix4x4 mat = m_cameraMainPlayer.projectionMatrix;
            mat *= Matrix4x4.Scale(new Vector3(-1, 1, 1));
            m_cameraMainPlayer.projectionMatrix = mat;
        }
    }

    public override void AttachSceneCamera()
    {
        SceneCamera.singleton.SetParent(m_transSceneCameraSlot, true);
        //SceneCamera.singleton.ResetLocalTransform();

        ResetCameraFov();

        if (isDmmModel)
        {
            SceneCameraController.singleton.FollowPlayer(this, FollowHeight, FollowDistance, isDmmModel, true);
        }
        else
        {
            m_transSceneCameraSlot.localPosition = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
            m_transSceneCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT;
        }
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, true);
    }

    public void ResetCameraFov()
    {
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.fieldOfView = SceneFov;
        if (m_cameraMainPlayer != null)
            m_cameraMainPlayer.fieldOfView = MainFov;
    }

    protected override void FireCameraRot(Vector3 rot)
    {
        if(attachedSceneCamera && SceneCamera.singleton != null)
        {
            SceneCamera.singleton.localEulerAngles = rot;
            //Logger.Log("ReceiveFireRot " + rot.x + " " + rot.y + " " + rot.z);
            if (weaponMgr != null && weaponMgr.curWeapon != null && weaponMgr.curWeapon.weaponSpread != null)
                weaponMgr.curWeapon.weaponSpread.ApplySpread();
        }
    }

    #endregion

    #region 模型缓存
    override protected void CacheGO(GameObject go)
    {
        if (go == null)
            return;

        base.CacheGO(go);

        if (m_bReleaseGO)
            return;

        EnableMainPlayerCamera(false);

        //string name = go.name;
        //
        //List<GameObject> listGO = null;
        //if (ms_dicGOCache.TryGetValue(name, out listGO) == false)
        //{
        //    listGO = new List<GameObject>();
        //    ms_dicGOCache.Add(name, listGO);
        //}
        //
        ////Logger.Log(this + " CacheGO " + go.name + " " + go.GetInstanceID());
        //
        //listGO.Add(go);
        PlayerManager.CacheGO(go.name + "FP", go);
    }

    override protected GameObject GetCacheGO(string name)
    {
        //List<GameObject> listGO = null;
        //ms_dicGOCache.TryGetValue(name, out listGO);
        //if (listGO == null || listGO.Count <= 0)
        //    return null;
        //
        //GameObject go = listGO[listGO.Count - 1];
        //listGO.RemoveAt(listGO.Count - 1);
        //
        ////Logger.Log(this + " GetCacheGO " + go.name + " " + go.GetInstanceID());
        //
        //return go;
        return PlayerManager.GetGO(name + "FP");
    }

    static public void ReleaseCacheGO()
    {
        //foreach (KeyValuePair<string, List<GameObject>> kvp in ms_dicGOCache)
        //{
        //    for (int i = 0; i < kvp.Value.Count; i++)
        //    {
        //        ResourceManager.Destroy(kvp.Value[i]);
        //    }
        //}
        //ms_dicGOCache.Clear();
    }
    #endregion

    #region OnData
    internal override void OnData_ActorProp(toc_fight_actorprop proto)
    {
        if (serverData == null)
            return;

        base.OnData_ActorProp(proto);

        if (gameObject != null)
        {
            string newWeaponID = m_kServerData.propWeapon();
            if (string.IsNullOrEmpty(newWeaponID) == false)
                SwitchWeapon(newWeaponID, false, false);
        }
    }

    internal override void OnData_ActorData(p_fight_role proto)
    {
        base.OnData_ActorData(proto);
        m_sceneFov = m_configData == null ? GameSetting.FOV_SCENE_CAMERA_DEFAULT : m_configData.RealFov;
    }

    internal override void OnData_ActorState(p_actor_state_toc state)
    {
        //if (serverData == null || serverData.camp != state.camp || serverData.state != state.state)
        //{
        //    //_last_time = 0;
        //    //_data_updated = true;
        //}
        if (serverData == null)
            return;

        base.OnData_ActorState(state);

        UpdateGameObjectLayer();
    }

    #endregion

    #region Switch Weapon

    public void SwitchDefaultWeapon(bool sameSwitch)
    {
        // 装备更新消息可能在Revive前收到，也可能在Revive后收到，这俩个地方都会调用SwitchDefaultWeapon，作兼容
        SwitchWeapon(m_kServerData.propWeapon(), false, sameSwitch);
        GameDispatcher.Dispatch<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, m_iSwitchTimer == -1);
    }

    public override bool SwitchWeapon(string strWeaponID, bool bSendMsg, bool sameSwitch, bool delaySendMsg = true)
    {
        if (alive == false || m_kGo == null || m_kGo.activeSelf == false)
            return false;

        //sameSwitch-相同的枪能不能显示切枪
        if (!sameSwitch && curWeapon != null && curWeapon.weaponId == strWeaponID)
            return true;

        if (base.SwitchWeapon(strWeaponID, bSendMsg, sameSwitch) == false)
            return false;

        status.switchWeapon = true;
        AddMainPlayerCameraWalkAni();

        if (m_iSwitchTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iSwitchTimer);
            m_iSwitchTimer = -1;
        }

        if (m_kWeaponMgr.curWeapon.weaponConfigLine.SwitchTime > 0)       
            m_iSwitchTimer = TimerManager.SetTimeOut(m_kWeaponMgr.curWeapon.weaponConfigLine.SwitchTime, SwitchWeaponComplete);
        else
            SwitchWeaponComplete();

        if (!m_bIsChangeModel)
        {
            m_kPlayerAniController.Play((m_isSwitchViewDone || curWeapon is WeaponBom) ? curWeapon.weaponConfigLine.MPSwitchAni : curWeapon.weaponConfigLine.MPIdleAni);
            m_bIsReplaySwitchWeapon = false;
        }
        else
        {
            m_bIsReplaySwitchWeapon = true;
        }

        //WeaponBase weapon = m_kWeaponMgr.curWeapon;
        //GameDispatcher.Dispatch(GameEvent.UI_UPDATE_GUNLIST, weapon.weaponId);

        return true;
    }

    virtual public void SwitchWeaponComplete()
    {
        GameDispatcher.Dispatch<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, true);
        m_iSwitchTimer = -1;

        if (released | m_isWatching == false || alive == false || curWeapon == null)
            return;

        status.switchWeapon = false;
        m_isSwitchViewDone = true;
    }
    #endregion

    #region Reload Ammo

    override public void OnReloadAmmoComplete()
    {
        base.OnReloadAmmoComplete();
    }

    override internal void ReloadAmmo2(toc_fight_reload arg)
    {
        if (released)
            return;

        if (curWeapon != null)
        {
            curWeapon.ReloadAmmo2(arg);
        }
    }
    #endregion

    #region Fire

    override internal void Fire2(toc_fight_fire arg)
    {
        if (curWeapon != null)
        {
            curWeapon.Fire2(arg);
        }
    }

    override internal void Atk(toc_fight_atk proto)
    {
        if (WeaponAtkTypeUtil.IsSubWeaponType(proto.atkType))
        {
            if (subWeapon != null)
            {
                subWeapon.Atk(proto);
                GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ATK, this);
            }
        }
        else
        {
            if (curWeapon != null)
            {
                curWeapon.Atk(proto);
                GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ATK, this);
            }
        }
    }
    #endregion

    #region 晃动相机
    public bool cameraMoveAniPlaing
    {
        get { return m_bCameraMoveAniPlaying; }
    }

    public bool cameraIdleAniPlaying
    {
        get { return m_bCameraIdleAniPlaying; }
    }

    private void AddMainPlayerCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation == null)
            return;

        string strCameraWalkAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
        if (string.IsNullOrEmpty(strCameraWalkAni) == false)
        {
            string strPath = PathHelper.GetCameraAni(strCameraWalkAni);
            if (m_kMainPlayerCameraAnimation.GetClip(strCameraWalkAni) == null)
            {
                ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                {
                    if (kAniClip != null && m_kMainPlayerCameraAnimation != null)
                    {
                        kAniClip.wrapMode = WrapMode.Loop;
                        m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraWalkAni);
                    }
                });
            }
        }

        string strCameraIdleAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
        if (string.IsNullOrEmpty(strCameraIdleAni) == false)
        {
            string strPath = PathHelper.GetCameraAni(strCameraIdleAni);
            if (m_kMainPlayerCameraAnimation.GetClip(strCameraIdleAni) == null)
            {
                ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                {
                    if (m_kMainPlayerCameraAnimation != null && kAniClip != null)
                    {
                        kAniClip.wrapMode = WrapMode.Loop;
                        m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraIdleAni);
                    }
                });
            }
        }

    }

    public void PlayCameraWalkAni()
    {
        m_bCameraIdleAniPlaying = false;
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            m_bCameraMoveAniPlaying = true;
            if (m_kWeaponMgr.curWeapon != null)
            {
                string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
                AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
                if (state != null)
                {
                    state.speed = 1;
                    m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                    m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
                }
            }
        }
    }

    public void PlayCameraIdleAni()
    {
        if (m_kWeaponMgr.curWeapon == null)
            return;

        m_bCameraMoveAniPlaying = false;
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            m_bCameraIdleAniPlaying = true;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
            {
                state.speed = 1;
                m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
            }
        }
    }

    public void StopCameraIdleAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_bCameraIdleAniPlaying = false;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    public void StopCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_bCameraMoveAniPlaying = false;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    #endregion

    #region 模型相关
    override public void SetModelName()
    {
        m_strModel = m_configData == null ? "" : m_configData.MPModel;
    }

    override protected void SetModelSex()
    {
        if (string.IsNullOrEmpty(m_strModel))
            m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        else
        {
            string lowerModelName = m_strModel.ToLower();
            if (lowerModelName.Contains(ModelConst.MODEL_MAIN_PLAYER_GIRL_CHAR))
                m_iModelSex = ModelConst.MODEL_PLAYER_GIRL_SEX;
            else
                m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        }
    }

    #endregion

    #region 技能相关
    internal override void UseWeaponAwakenSkill(toc_fight_weapon_awaken_skill proto)
    {
        if (skillMgr != null)
            m_skillMgr.OPUseWeaponAwakenSkill(proto.awaken_id, proto.skill_id, proto.pos, proto.target_ids, "");
    }

    public override void SetScale(float scale)
    {
        if (gameObject != null && m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY * scale;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        }
    }

    public override void SetFov(float fov)
    {
        m_sceneFov = fov;
        SetSceneFov(1f);
    }

    public float SceneFov
    {
        get { return m_sceneFov; }
    }

    public float MainFov
    {
        get { return GameSetting.FOV_MAINPLAYER_CAMERA; }
    }
    #endregion

    #region get set
    override public MainPlayerCameraShaker cameraShaker
    {
        get { return m_kMPCS; }
    }

    public BasePlayer relatePlayer
    {
        get { return m_relatePlayer; }
    }

    public bool isSwitchViewDone
    {
        get 
        {
            return m_isSwitchViewDone; 
        }
    }

    public bool isWatching
    {
        get { return m_isWatching; }
    }

    #endregion

}