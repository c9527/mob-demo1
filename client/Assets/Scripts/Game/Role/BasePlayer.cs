﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class BasePlayer : EventDispatcher
{
    public const int BODY_PART_ID_HEAD = 1;
    public const int BODY_PART_ID_BODY = 2;
    public const int BODY_PART_ID_LIMBS = 3;
    public const int BODY_PART_ID_SHIELD = 4;     //  怪手中有盾牌，打到盾牌不算击中

    private const string HEART_SOUND = "heartBeat";

    // 使用静态记录角色死亡时间戳，否则角色离开再进入后，则记录的时间丢失，导致上一次死亡会被重执行一次（一次死亡推2次协议）
    static private Dictionary<int, float> m_dicPlayerDieStamp = new Dictionary<int, float>();

    // Component
    protected GameObject m_kGo;
    protected Transform m_kTrans;
    private Rigidbody m_rb;
    protected Transform m_transRightHandSlot;
    protected Transform m_transLeftHandSlot;
    protected Transform m_transHips;

    protected Transform m_transSceneCameraSlot;
    protected Behaviour[] m_arrBehaviour;
    protected ModelComponentCache m_mcc;

    public bool update_ESG_View = true;

    // data
    protected LocalPlayerData m_localPlayerData;
    protected PlayerServerData m_kServerData;
    protected float m_clientMoveSpeed;

    //
    protected PlayerStatus m_kStatus;
    protected PlayerAniBaseController m_kPlayerAniController;
    protected WeaponManager m_kWeaponMgr;
    protected SoundPlayer m_kVoiceSoundPlayer;  //声音音源
    protected SoundPlayer m_kStepSoundPlayer;   //脚步音源
    protected SoundPlayer m_kHeartSoundPlayer;  //心跳音源
    private const float SOUND_MAX_DISTANCE = 30;

    protected int m_iOldState;
    protected string m_strModel;
    protected string m_strModelPre;
    protected int m_iModelTypePre;
    protected bool m_loadModel;
    protected int m_iModelSex;   // 1:男 2:女
    protected int m_iItemRole;
    protected int m_iModelType;  // 模型类型
    protected float m_fGodTime = 0;   //无敌时长
    private bool m_bReleased;
    protected bool m_bReleaseGO;
    protected float m_height;   // 当前高度，站立，下蹲时不同

    private float m_moveTime;
    private float m_heartTime;
    private int m_moveSoundType;
    private string[] m_stepSounds;

    //protected TweenPosition m_tweenPos;
    //protected TweenRotation m_tweenRot;

    protected Renderer m_renderer;    // 玩家的Renderer
    public int m_bloodEffectNum;

    protected bool m_bIsIgnoreTransparent = false;
    protected bool m_bIsBeingRescued = false;

    protected bool m_bCanPlayHeartBeat = false;

    protected bool m_bIsCancelHitBack = false;  //取消受击

    protected BuffManager m_kBM;    //buff管理器
    protected VisibilityManager m_kVM; //可度视管理器

    protected Vector3 m_vecSceneCamSlotDir;

    private KeyValue<int, int> _armor_recovery_data;
    private KeyValue<int, int> _life_recovery_data;

    protected ConfigItemRoleLine m_configData;

    protected bool m_isSwitchFPV;

    protected WeaponHitEffectManager m_hitEffectMgr;

    protected PlayerEffectManager m_playerEffectMgr;

    //技能相关
    protected SkillManager m_skillMgr;
    protected EptInt m_scoutSkillState = 0;    //寻敌状态 0：未定义 1：开启 2：关闭
    protected bool m_reuseWeaponSkill;            //再次使用技能    
    protected GhostJumpRecord m_ghostJumpRecord;

    protected bool m_bIsActive = false;
    protected bool m_bIsSuicide = false;    //是否自杀
    protected bool m_bIsHPAlert = false;
    protected bool m_bIsGodMode = false;

    public float powerOnStartTime;
    public float powerOnEndTime;

    public bool IsFlying = false;//是否是飞行状态
    /// <summary>
    /// 死亡协议会推两次udp,tcp各推一次，因为tcp延时太大,所以这里要排除重复的那一次
    /// </summary>

    static private void SetDieStamp(int id, float stamp)
    {
        if (m_dicPlayerDieStamp.ContainsKey(id) == false)
            m_dicPlayerDieStamp.Add(id, stamp);
        else
            m_dicPlayerDieStamp[id] = stamp;
    }

    static private float GetDieStamp(int id)
    {
        if (m_dicPlayerDieStamp.ContainsKey(id) == false)
            return 0;

        return m_dicPlayerDieStamp[id];
    }

    static public void ClearDieStamp()
    {
        m_dicPlayerDieStamp.Clear();
    }

    public BasePlayer()
    {
        m_kServerData = new PlayerServerData(this);

        Init();
    }

    static public int GetBodyPartID(string colliderName)
    {
        //前端自定义受击部位
        ConfigCustomHitPart customCfg = ConfigManager.GetConfig<ConfigCustomHitPart>();
        if (null != customCfg)
        {
            ConfigCustomHitPartLine cfgLine = customCfg.GetLine(colliderName);
            if (null != cfgLine)
                return cfgLine.Part;
        }

        int bodyPartID = BODY_PART_ID_BODY;
        colliderName = colliderName.ToLower();

        if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_HEAD))
        {
            bodyPartID = BODY_PART_ID_HEAD;
        }
        else if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_SHIELD))
        {
            bodyPartID = BODY_PART_ID_SHIELD;
        }
        else if (colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_ARM)
              || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_HAND)
              || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_LEG)
              || colliderName.Contains(ModelConst.PLAYER_BODY_PART_NAME_FOOT))
        {
            bodyPartID = BODY_PART_ID_LIMBS;
        }

        return bodyPartID;
    }

    virtual protected void Init()
    {
        _armor_recovery_data = new KeyValue<int, int>();
        _life_recovery_data = new KeyValue<int, int>();

        _armor_recovery_data.key = int.MinValue;
        _life_recovery_data.key = int.MinValue;

        m_kStatus = new PlayerStatus(this);
        m_kStatus.stand = true;
        m_kPlayerAniController = new PlayerAniBaseController();

        m_localPlayerData = new LocalPlayerData(this);
        m_kWeaponMgr = new WeaponManager(this);
        m_kVM = new VisibilityManager();
        m_kBM = new BuffManager(this);
        m_kVoiceSoundPlayer = new SoundPlayer();
        m_kStepSoundPlayer = new SoundPlayer();
        m_kHeartSoundPlayer = new SoundPlayer();

        m_hitEffectMgr = new WeaponHitEffectManager(this);
        m_playerEffectMgr = new PlayerEffectManager(this);

        m_skillMgr = new SkillManager(this);

        var type1 = UnityEngine.Random.Range(1, 5);
        var type2 = type1;
        while (type2 == type1)
        {
            type2 = UnityEngine.Random.Range(1, 5);
        }
        m_stepSounds = new[] { "step" + type1, "step" + type2 };

        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, LoadRes);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
    }

    /// <summary>
    /// 重新激活
    /// </summary>
    virtual public void ReActive()
    {
        m_bReleased = false;
    }

    virtual public void Release(bool releaseGO)
    {
        TryToDetachSceneCamera();

        m_bReleased = true;
        m_bReleaseGO = releaseGO;

        if (m_kGo != null)
        {
            WorldManager.singleton.UnRegisterPlayerGOHash(m_kGo.GetHashCode());

            if (releaseGO)
                GameObject.Destroy(m_kGo);
            else
                CacheGO(m_kGo);

            m_kGo = null;
        }

        if(!releaseGO)
        {
            if (m_kBM != null)
                m_kBM.RemoveAllBuff();
            if (m_kWeaponMgr != null)
                m_kWeaponMgr.Stop();
            if (m_kVM != null)
                m_kVM.Stop();
            if (m_skillMgr != null)
                m_skillMgr.Stop();
        }
        else 
        {
            if (m_skillMgr != null)
                m_skillMgr.Release();
            m_skillMgr = null;

            // Release Member Obj
            if (m_kBM != null)
                m_kBM.RemoveAllBuff();
            m_kBM = null;

            if (m_kWeaponMgr != null)
                m_kWeaponMgr.Release();
            m_kWeaponMgr = null;

            if (m_playerEffectMgr != null)
                m_playerEffectMgr.Release();
            m_playerEffectMgr = null;

            if (m_kVoiceSoundPlayer != null)
                m_kVoiceSoundPlayer.Release();
            m_kVoiceSoundPlayer = null;

            if (m_kStepSoundPlayer != null)
                m_kStepSoundPlayer.Release();
            m_kStepSoundPlayer = null;

            if (m_kHeartSoundPlayer != null)
                m_kHeartSoundPlayer.Release();
            m_kHeartSoundPlayer = null;

            if (m_kStatus != null)
                m_kStatus.Release();
            m_kStatus = null;

            if (m_kVM != null)
                m_kVM.Release();
            m_kVM = null;

            m_hitEffectMgr.Release();

            if (m_kPlayerAniController != null)
                m_kPlayerAniController.Release();
            m_kPlayerAniController = null;

            m_localPlayerData = null;
            m_kServerData = null;
            _armor_recovery_data = null;
            _life_recovery_data = null;
        }

        if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
        {
            if (SceneCameraController.singleton != null)
                SceneCameraController.singleton.StopControl();
        }
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, LoadRes);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        m_strModel = "";
    }

    /// <summary>
    /// 玩家模型变化时调用
    /// </summary>
    virtual protected void DestroyPlayerOldModel()
    {
        TryToDetachSceneCamera();
        m_kWeaponMgr.ReleaseWeapon();
        m_kBM.RemoveAllBuff();
        m_kVM.RemoveAllRenderer();
        CacheGO(m_kGo);
        //GameObject.Destroy(m_kGo);
        //m_kGo = null;
        //m_kPlayerAniController.SetAnimator(null);
    }

    virtual public void Update()
    {
        if (m_bIsActive == false || alive == false)
            return;

        UpdateStepSound();

        if (canPlayHeartBeat) UpdateHeartSound();

        if (m_fGodTime > 0)
        {
            m_fGodTime -= Time.deltaTime;

            if (m_fGodTime <= 0)
            {
                m_kStatus.god = false;
                GodMode(false);
            }
        }
        //血量，护甲自动恢复
        if (serverData != null && WorldManager.singleton != null)
        {
            var now_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
            var armor_wait_time = now_time - serverData.last_damage_time;
            if (serverData.last_armor < serverData.maxArmor() && armor_wait_time > serverData.armorRecoverWait() && serverData.armorRecoverSpeed() > 0)
            {
                if (serverData.last_damage_time >= _armor_recovery_data.key)
                {
                    _armor_recovery_data.key = now_time;
                    _armor_recovery_data.value = serverData.last_armor;
                }
                var armor_rec_time = Mathf.Max(armor_wait_time - serverData.armorRecoverWait(), 0);
                var armor = _armor_recovery_data.value + armor_rec_time * serverData.armorRecoverSpeed() / 1000;
                serverData.last_armor = Mathf.Min(Mathf.Max(armor, 0), serverData.maxArmor());
            }
            var life_wait_time = now_time - Mathf.Max(serverData.last_damage_time, serverData.last_act_time);
            if (serverData.life < serverData.maxhp() && life_wait_time > serverData.hPRecoverWait() && serverData.hPRecoverSpeed() > 0)
            {
                if (Mathf.Max(serverData.last_damage_time, serverData.last_act_time) >= _life_recovery_data.key)
                {
                    _life_recovery_data.key = now_time;
                    _life_recovery_data.value = serverData.life;
                }
                var hp_rec_time = Mathf.Max(life_wait_time - serverData.hPRecoverWait(), 0);
                var life = _life_recovery_data.value + hp_rec_time * serverData.hPRecoverSpeed() / 1000;
                serverData.life = Mathf.Min(Mathf.Max(life, 0), serverData.maxhp());

                if (configData != null && configData.HPAlert != 0 && serverData.life > configData.HPAlert)
                    HPAlert(false);
            }
        }

        //可视度修改
        m_kVM.Update();

        //武器Update
        if (m_kWeaponMgr.curWeapon != null)
        {
            m_kWeaponMgr.curWeapon.Update();
        }

        if (m_kWeaponMgr.subWeapon != null)
        {
            m_kWeaponMgr.subWeapon.Update();
        }

        m_hitEffectMgr.Update();
        m_playerEffectMgr.Update();
    }

    virtual public void LoadRes()
    {

    }

    virtual protected void ReplaceShader(string newShaderName)
    {
        ResourceManager.LoadShader(newShaderName, (newShader) =>
        {
            if (gameObject == null)
                return;

            GameObjectHelper.VisitChildByTraverse(transform, (child) =>
            {
                Renderer r = child.renderer;
                if (r == null || r.sharedMaterial == null)
                {
                    return;
                }

                if (r.sharedMaterial.shader.name != newShaderName)
                {
                    r.sharedMaterial.shader = newShader;
                }
                r.useLightProbes = true;
            }, true);
        });
    }

    virtual protected void SetGO(GameObject go)
    {
        m_mcc = go.AddMissingComponent<ModelComponentCache>();

        // 同一个模型GameObject只会调用一次，除非是不同的模型
        m_kGo = go;
        m_kTrans = go.transform;
        UpdateGameObjectLayer();

        if (m_iModelType == GameConst.MODEL_TYPE_ROLE || m_iModelType == GameConst.MODEL_TYPE_STORY)
        {
            m_renderer = null;
            Transform transBipRightHandCenter = null;
            Transform transBipLeftHandCenter = null;
            Animator kAnimator = null;
            Animation kAnimation = null;

            if (m_mcc.HasInited("Renderer"))
            {
                m_renderer = m_mcc.FindComponent<Renderer>("Renderer", go);
                transBipRightHandCenter = m_mcc.FindComponent<Transform>("transBipRightHandCenter", go);
                transBipLeftHandCenter = m_mcc.FindComponent<Transform>("transBipLeftHandCenter", go);
                kAnimator = m_mcc.FindComponent<Animator>("Animator", go);
                kAnimation = m_mcc.FindComponent<Animation>("Animation", go);
            }
            else
            {
                m_kTrans.VisitChild((child) =>
                {
                    if (transBipRightHandCenter == null && child.name == ModelConst.BIP_NAME_RIGHT_HAND_CENTER)
                    {
                        transBipRightHandCenter = child.transform;
                    }
                        
                    if (transBipRightHandCenter == null && child.name == ModelConst.BIP_NAME_LEFT_HAND_CENTER)
                    {
                        transBipLeftHandCenter = child.transform;
                    }
                        
                    if (m_renderer == null)
                    {
                        m_renderer = child.GetComponent<SkinnedMeshRenderer>();
                    }

                    if (kAnimator == null && kAnimation == null)
                    {
                        kAnimator = child.GetComponent<Animator>();
                        kAnimation = child.GetComponent<Animation>();
                    }
                }, true);

                m_mcc.CacheComponent("transBipRightHandCenter", transBipRightHandCenter);
                m_mcc.CacheComponent("transBipLeftHandCenter", transBipLeftHandCenter);
                m_mcc.CacheComponent("Renderer", m_renderer);
                m_mcc.CacheComponent("Animator", kAnimator);
                m_mcc.CacheComponent("Animation", kAnimation);
            }

            if (m_kPlayerAniController != null)
                m_kPlayerAniController.Release();
            
            if (kAnimator != null)
            {
                m_kPlayerAniController = new PlayerAnimatorController(this);
                kAnimator.applyRootMotion = false;
                kAnimator.cullingMode = AnimatorCullingMode.BasedOnRenderers;
                m_kPlayerAniController.SetAnimator(kAnimator);
            }
            else if(kAnimation != null)
            {
                m_kPlayerAniController = new PlayerAnimationController(this);
                m_kPlayerAniController.SetAnimation(kAnimation);
            }

            m_kVM.SetPlayer(this);
            m_kVM.RemoveAllRenderer();
            m_kVM.AddRenderer(m_renderer);
            float time = GameSetting.PLAYER_SET_ALPHA_TIME * configData.HideDisappearTimePercent;
            SetVisableDegree(m_kVM.visableDegree, time);

            if (transBipRightHandCenter == null)
            {
                //Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
            }
            else
            {
                m_transRightHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipRightHandCenter, ModelConst.PLAYER_SLOT_RIGHT_HAND).transform;
                m_transRightHandSlot.ResetLocal();
            }

            if (transBipLeftHandCenter == null)
            {
                //Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_LEFT_HAND_CENTER);
            }
            else
            {
                m_transLeftHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipLeftHandCenter, ModelConst.PLAYER_SLOT_LEFT_HAND).transform;
                m_transLeftHandSlot.ResetLocal();
            }
        }
        else if (m_iModelType == GameConst.MODEL_TYPE_DMM)
        {
            m_renderer = m_kTrans.GetComponent<Renderer>();
            if (m_renderer == null)
            {
                m_kTrans.VisitChild((child) =>
                {
                    m_renderer = child.GetComponent<Renderer>();
                    return m_renderer == null;
                }, false);
            }
            //替换材质
            ReplaceShader(GameConst.SHADER_LIGHTPROBE_TEX);
        }

        m_rb = m_mcc.AddMissingComponent<Rigidbody>("Rigidbody", go);
        m_rb.isKinematic = true;
        m_rb.useGravity = false;

        // Create Node
        m_transSceneCameraSlot = GameObjectHelper.CreateChildIfNotExisted(go.transform, ModelConst.SLOT_SCENE_CAMERA).transform;

        m_playerEffectMgr.lastScale = m_kTrans.localScale.x;
        m_playerEffectMgr.SetScale(m_playerEffectMgr.lastScale, false);
        m_playerEffectMgr.SetFov(m_configData.RealFov, false);
    }

    virtual public void AttachSceneCamera()
    {

    }

    virtual public void TryToDetachSceneCamera()
    {
        //子接点等于1表示有挂载相机
        if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
        {
            m_vecSceneCamSlotDir = Vector3.Normalize(m_transSceneCameraSlot.position - headPos);
            SceneCamera.singleton.SetParent(null);
        }
    }

    public void ResetAnimatorTriggerValue(string strTriggerName)
    {
        m_kPlayerAniController.SetInteger(strTriggerName, 0);
    }

    /// <summary>
    /// 玩家GO加载完，或者更换阵营的时候调用，用于射击时射线检测
    /// </summary> 
    virtual public void UpdateGameObjectLayer()
    {

    }

    virtual public void SetActive(bool value)
    {
        m_bIsActive = value;

        if (m_kGo == null)
            return;

        for (int i = 0; i < m_arrBehaviour.Length; i++)
        {
            if (m_arrBehaviour[i] != null)
                m_arrBehaviour[i].enabled = value;
        }

        if (value == false)
            EnableBodyPartCollider(false, true);

        //if(this is MainPlayer && WorldManager.singleton.isFightInTurnModeOpen) Logger.Error("setactive: "+value);
        if (!value)
        {
            m_kTrans.SetParent(SceneManager.singleton.reclaimRoot, false);
            m_kTrans.localPosition = Vector3.zero;
        }
        else
        {
            m_kTrans.SetParent(null);
        }
    }

    virtual public void WeaponAttachComplete()
    {
        if (released)
            return;

        if (curWeapon != null)
        {
            float time = GameSetting.PLAYER_SET_ALPHA_TIME * (configData != null ?  configData.HideDisappearTimePercent : 1f);
            curWeapon.SetVisableDegree(visableDegree, time);
        }
            
    }

    public string CurWeaponDisName
    {
        get
        {
            ConfigItemWeaponLine curWeaponLine = null;
            if (!string.IsNullOrEmpty(serverData.propWeapon()))
            {
                curWeaponLine = ItemDataManager.GetItemWeapon(int.Parse(serverData.propWeapon()));
            }
            return curWeaponLine != null ? curWeaponLine.DispName : "";
        }
    }

    virtual public string GetNextWeaponID(bool force = false, bool isRight = true)
    {
        if (m_kServerData.weaponList.Count <= 0)
            return "";

        int iIndex = m_kServerData.weaponList.IndexOf(m_kServerData.curWeapon) % m_kServerData.weaponList.Count;
        string weaponId = force ? "" : weaponMgr.preWeapon;
        if (string.IsNullOrEmpty(weaponId))
        {
            iIndex = (isRight ? (iIndex + 1) : (iIndex - 1)) % m_kServerData.weaponList.Count;
            if (iIndex < 0) iIndex += m_kServerData.weaponList.Count;
            weaponId = m_kServerData.weaponList[iIndex].ToString();
        }

        WeaponBase preWeapon = weaponMgr.TryGetWeapon(weaponId);
        if (preWeapon != null && preWeapon is WeaponGrenade)
        {
            if (!preWeapon.hasAmmoCurClip)
                return GetNextWeaponID(iIndex, force, isRight);
        }
        else if (preWeapon == null)
        {
            var item = ItemDataManager.GetItem(int.Parse(weaponId));

            if (item == null || item.SubType == GameConst.WEAPON_SUBTYPE_GRENADE)
            {
                return GetNextWeaponID(iIndex, force, isRight);
            }
        }
        return weaponId;
    }

    private string GetNextWeaponID(int index, bool force, bool isRight)
    {
        int iIndex = (isRight ? (index + 1) : (index - 1)) % m_kServerData.weaponList.Count;
        if (iIndex < 0) iIndex += m_kServerData.weaponList.Count;
        string id = m_kServerData.weaponList[iIndex].ToString();
        WeaponBase newWeapon = weaponMgr.TryGetWeapon(id);
        if (newWeapon != null && newWeapon is WeaponGrenade)
        {
            if (!newWeapon.hasAmmoCurClip)
                return GetNextWeaponID(iIndex, force, isRight);
        }
        else if (newWeapon == null)
        {
            var item = ItemDataManager.GetItem(int.Parse(id));

            if (item == null || item.SubType == GameConst.WEAPON_SUBTYPE_GRENADE)
            {
                return GetNextWeaponID(iIndex, force, isRight);
            }
        }
        return id;
    }

    public void SetItemRoleLine()
    {
        m_configData = GlobalConfig.configRole.GetLine(serverData.itemRoleID);
    }

    public float GetPlayerHeightStand()
    {
        float heightStand = (m_configData == null || m_configData.HeightStand == 0) ? GameSetting.PLAYER_COLLIDER_HEIGHT_STAND : m_configData.HeightStand;
        return heightStand;
    }

    public float GetPlayerHeightCrouch()
    {
        float heightCrouch = (m_configData == null || m_configData.HeightCrouch == 0) ? GameSetting.PLAYER_COLLIDER_HEIGHT_CROUCH : m_configData.HeightCrouch;
        return heightCrouch;
    }

    public float FollowHeight
    {
        get
        {
            if (m_configData != null)
                return m_configData.FollowHeight == 0 ? GameSetting.CAMERA_MODE_FOLLOW_DELTA_HEIGHT : m_configData.FollowHeight;
            return GameSetting.CAMERA_MODE_FOLLOW_DELTA_HEIGHT;
        }
    }

    public float FollowDistance
    {
        get
        {
            if (m_configData != null)
                return m_configData.FollowDistance == 0 ? GameSetting.CAMERA_MODE_FOLLOW_DISTANCE : m_configData.FollowDistance;
            return GameSetting.CAMERA_MODE_FOLLOW_DISTANCE;
        }
    }

    virtual public float GetPlayerRadius()
    {
        return GameSetting.MPColliderRadius;
    }

    virtual public Vector3 GetPlayerCenter()
    {
        return new Vector3(0, m_height * 0.5f, 0);
    }

    virtual protected void SetModelSex()
    {
        m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
    }

    virtual public void SetModelName()
    {
        m_strModel = "";
    }

    protected void SetModelType()
    {
        if (serverData.actor_type == GameConst.ACTOR_TYPE_STORY)
            m_iModelType = GameConst.MODEL_TYPE_STORY;
        else if (m_configData != null)
        {
            m_iModelType = (m_configData.RoleCamp == (int)WorldManager.CAMP_DEFINE.CAT ? GameConst.MODEL_TYPE_DMM : GameConst.MODEL_TYPE_ROLE);
        }
        else
            m_iModelType = GameConst.MODEL_TYPE_ROLE;
    }

    public bool GetOpModelIsSimpleDeadAni()
    {
        if (m_configData == null)
            return false;
        string lowerModelName = m_configData.OPModel.ToLower();
        return !lowerModelName.StartsWith(ModelConst.MODEL_YAZHUO_CHAR) && !lowerModelName.StartsWith(ModelConst.MODEL_OUMEI_CHAR);
    }


    virtual public void EnableBodyPartCollider(bool value, bool force)
    {

    }

    #region 缓存
    virtual protected void CacheGO(GameObject go)
    {
        if (m_bReleaseGO)
        {
            ResourceManager.Destroy(go);
            return;
        }

        MonoBehaviour[] arrMB = go.GetComponentsInChildren<MonoBehaviour>();
        for (int i = 0; i < arrMB.Length; i++)
        {
            //GameObject.DestroyImmediate(arrMB[i]);  // 这里一定要立即销毁，否则后续使用AddMissing去添加脚本失败，因为下一帧脚本才被Destroy
            arrMB[i].enabled = false;
        }

        if (m_kGo == go)
        {
            SetActive(false);
            m_kGo = null;
            m_kTrans = null;
            m_renderer = null;
            m_transSceneCameraSlot = null;
            if (m_kPlayerAniController != null)
                m_kPlayerAniController.SetAnimator(null);
        }

        go.transform.SetParent(SceneManager.singleton.reclaimRoot);
        go.transform.localPosition = Vector3.zero;

    }

    virtual protected GameObject GetCacheGO(string name)
    {
        return null;
    }

    #endregion

    virtual public void GameRoundEnd()
    {

    }

    static public string GetRolePrefablPath(int modeType, string prefabName)
    {
        if (prefabName.IsNullOrEmpty())
            return "";

        string path = modeType == GameConst.MODEL_TYPE_DMM ? PathHelper.GetDmmPrefabPath(prefabName) : PathHelper.GetRolePrefabPath(prefabName);

        return path;
    }

    public bool IsEnemy(BasePlayer comingPlayer)
    {
        //DOTA模式添加的
        if (null != WorldManager.singleton && WorldManager.singleton.isDotaModeOpen)
        {
            if (null != comingPlayer.configData && comingPlayer.configData.SubType == GameConst.ROLE_SUBTYPE_BASE)
            {
                return false;
            }
        }

        return serverData.camp != comingPlayer.Camp || comingPlayer.Camp == 0 || serverData.camp == 0;
    }

    public bool IsAutoAimTarget(BasePlayer comingPlayer)
    {
        if (curWeapon == null || curWeapon.weaponConfigLine == null || curWeapon.weaponConfigLine.AutoAimDistance < 0)
            return false;

        if (curWeapon.weaponConfigLine.AutoAimDistance == 0)
            return true;

        float distance = Vector3.Distance(position, comingPlayer.position);
        return distance <= curWeapon.weaponConfigLine.AutoAimDistance;
    }

    #region Sound
    protected void UpdateHeartSound()
    {
        if (m_kGo == null)
            return;

        if (justMoveNotJump)
        {
            m_heartTime = -0.6f;
            m_kHeartSoundPlayer.Stop();
            return;
        }

        if (!isVisible && !status.move)
        {
            m_heartTime += Time.deltaTime;
            if (m_heartTime > 1.2f)
            {
                m_heartTime = 0;
                m_kHeartSoundPlayer.PlayOneShot(HEART_SOUND, 0.7f);
                //Logger.Error("UpdateHeartSound");
            }
        }
    }

    protected void UpdateStepSound()
    {
        if (m_kGo == null || m_configData== null || m_configData.IsCancelPlayStepSound == 1)
            return;

        if (!status.move || status.crouch)
            return;

        //锁定状态去除脚步声
        if (this is MainPlayer && (this as MainPlayer).IsFrezee)
            return;

        if (status.jump)
        {
            m_moveTime = -0.3f;
            m_kStepSoundPlayer.Stop();
            return;
        }
        m_moveTime += Time.deltaTime;
        if (m_moveTime > 0.3f)
        {
            m_moveTime = 0;
            m_moveSoundType = (m_moveSoundType + 1) % 2;
            if (m_stepSounds != null && m_moveSoundType < m_stepSounds.Length)
            {
                Vector3 curPos = Vector3.zero;
                if (WorldManager.singleton.isViewBattleModel)
                {
                    BasePlayer curPlayer = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
                    if (curPlayer == null) return;
                    curPos = curPlayer.position;
                }
                else if (MainPlayer.singleton != null)
                    curPos = MainPlayer.singleton.position;
                else
                    return;

                if ((curPos - position).sqrMagnitude <= SOUND_MAX_DISTANCE * SOUND_MAX_DISTANCE)
                    m_kStepSoundPlayer.PlayOneShot(m_stepSounds[m_moveSoundType]);
            }
        }
    }

    protected void PlayJumpBeginSound()
    {
        if (m_stepSounds == null || m_stepSounds.Length < 2 || m_iModelType == GameConst.MODEL_TYPE_DMM)
            return;

        Vector3 curPos = Vector3.zero;
        if (WorldManager.singleton.isViewBattleModel)
        {
            BasePlayer curPlayer = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            if (curPlayer == null) return;
            curPos = curPlayer.position;
        }
        else if (MainPlayer.singleton != null)
            curPos = MainPlayer.singleton.position;
        else
            return;

        if ((curPos - position).sqrMagnitude <= SOUND_MAX_DISTANCE * SOUND_MAX_DISTANCE)
            m_kStepSoundPlayer.PlayOneShot(m_stepSounds[0]);
        //        TimerManager.SetTimeOut(0.05f, () => m_kStepSoundPlayer.PlayOneShot(m_stepSounds[1]));
    }

    protected void PlayJumpEndSound()
    {
        if (m_stepSounds == null || m_stepSounds.Length < 2 || m_iModelType == GameConst.MODEL_TYPE_DMM)
            return;

        Vector3 curPos = Vector3.zero;
        if (WorldManager.singleton.isViewBattleModel)
        {
            BasePlayer curPlayer = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            if (curPlayer == null) return;
            curPos = curPlayer.position;
        }
        else if (MainPlayer.singleton != null)
            curPos = MainPlayer.singleton.position;
        else
            return;

        if ((curPos - position).sqrMagnitude <= SOUND_MAX_DISTANCE * SOUND_MAX_DISTANCE)
            m_kStepSoundPlayer.PlayOneShot(m_stepSounds[1]);
    }
    #endregion

    #region OnData

    virtual internal void OnData_ActorProp(toc_fight_actorprop proto)
    {
        m_kServerData.actorprops = proto.props;
        OnData_Actor();
    }

    /// <summary>
    /// 角色创建时调用，后续属性变化不会使用
    /// </summary>
    /// <param name="proto"></param>
    virtual internal void OnData_ActorData(p_fight_role proto)
    {
        m_kServerData.actorprop = proto.prop;
        OnData_Actor();
    }

    virtual protected void OnData_Actor()
    {
        if (released)
            return;

        if (m_localPlayerData != null)
            m_localPlayerData.UpdateLocalWeaponData_ActorProp();

        SetItemRoleLine();
        if (m_configData != null)
        {
            m_strModelPre = m_strModel;
            m_iModelTypePre = m_iModelType;

            SetModelName();
            SetModelType();
            SetModelSex();

            LoadRes();
        }
    }

    virtual internal void OnData_ActorState(p_actor_state_toc state)
    {
        serverData.actorState = state;
        serverData.camp = state.camp;
        serverData.immune = state.immune;
        serverData.actor_type = state.actor_type;
        serverData.life = state.life;
        serverData.SetServerPos(state.pos, false);;
        serverData.fight_level = state.fight_level;

        if (state.state == GameConst.SERVER_ACTOR_STATUS_LIVE)
        {
            SetPosNow(state.pos);
        }
        SetState(state.state, true);

        SetBuff(state.buff_list);

        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_ACTOR_STATE, this);
    }

    virtual internal void SetLife(int life)
    {
        if (serverData != null && serverData.roleData != null)
            serverData.life = life;
    }

    #endregion

    #region Fire

    virtual public void StopFire()
    {
        if (status.fire == false)
            return;

        status.fire = false;
    }

    virtual public void PlayFireAni()
    {

    }
    #endregion

    #region ReloadAmmo

    virtual public void ReloadAmmo()
    {

    }

    virtual protected void ReloadAmmoComplete()
    {
        status.reload = false;
        m_kWeaponMgr.curWeapon.ReloadAmmoComplete();
    }

    virtual public void OnReload_Begin()
    {

    }

    virtual public void OnReload_NoAmmo()
    {

    }

    virtual public void OnReload_AmmoFull()
    {


    }

    virtual public void OnReloadAmmoComplete()
    {
        if (released)
            return;

        status.reload = false;
    }
    #endregion

    #region Switch

    virtual public void SwitchPreWeapon()
    {
        SwitchWeapon(m_kWeaponMgr.preWeapon, true, false);
    }

    virtual public void SwitchBomb()
    {
        ConfigGameRuleLine rl = ConfigManager.GetConfig<ConfigGameRule>().GetLine(GameConst.GAME_RULE_BURST);
        if (rl != null && PlayerId == PlayerBattleModel.Instance.bombHolder)
        {
            string bombID = rl.Special_Item;
            SwitchWeapon(bombID, false, false);
        }
    }

    virtual public bool SwitchWeapon(string strWeaponID, bool bSendMsg, bool sameSwitch, bool delaySendMsg = true)
    {
        if (string.IsNullOrEmpty(strWeaponID) == true)
            return false;

        if (ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(int.Parse(strWeaponID)) == null)
            return false;

        if (m_kWeaponMgr.SwitchWeapon(strWeaponID) == false)
            return false;

        return true;
    }


    #endregion

    #region 角色控制

    //virtual internal void TweenPos(toc_fight_move proto)
    //{
    //    float[] pos = new float[6];
    //    pos[0] = proto.pos[0] * 0.01f;
    //    pos[1] = proto.pos[1] * 0.01f;
    //    pos[2] = proto.pos[2] * 0.01f;

    //    pos[4] = proto.angleY * 2;
        
    //    serverData.SetServerPos(pos, true);
    //}

    virtual internal void Act(PlayerActParam param)
    {
        if (param.pos == null || param.pos.Length < 3)
            return;

        // TODO: 是否要加Pitch进来
        float[] p = new float[6];
        p[0] = param.pos[0];
        p[1] = param.pos[1];
        p[2] = param.pos[2];
        p[4] = param.angleY;

        serverData.SetServerPos(p, true);
    }

    //virtual public void ActNoTween(TweenData param)
    //{

    //}

    virtual public void SetPosNow(float[] pos)
    {
        if (pos == null)
            return;

        serverData.SetServerPos(pos, false);

        if (pos.Length >= 3)
            position = new Vector3(pos[0], pos[1], pos[2]);
        if (pos.Length == 6)
            eulerAngle = new Vector3(0, pos[4], 0);
    }

    protected void SetPosNow(float[] pos, float angleY)
    {
        if (pos == null)
            return;

        float[] p = new float[6];
        p[0] = pos[0];
        p[1] = pos[1];
        p[2] = pos[2];
        p[4] = angleY;

        SetPosNow(p);
    }

    virtual public void ClearPosQueue()
    {

    }

    virtual public void Jump()
    {
        status.jump = true;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }


    virtual public void Fly()
    {
        status.fly = true;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }


    virtual public void Jump2()
    {
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
        PlayJumpBeginSound();
    }

    virtual public void JumpComplete()
    {
        status.jump = false;
        status.jump2 = false;
        PlayJumpEndSound();
    }

    virtual internal void Jump(toc_fight_jump data)
    {
        //m_kServerData.last_act_time = (int)((Time.time - WorldManager.singleton.StartTime) * 1000);
    }

    virtual public void Crouch()
    {
        status.stand = false;
        status.crouch = true;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }

    virtual public void Stand(bool bSend2Server = false, bool bNeedGrounded = true)
    {
        status.stand = true;
        status.crouch = false;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }


    virtual public void PowerOn()
    {
        status.poweron = true;
        status.fire = false;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }

    virtual public void RushDone()
    {

    }

    virtual public void FallOnTheGround()
    {
        if (gameObject == null)
            return;

        //tweenPosEnable = false;

        position = SceneManager.GetGroundPoint(position + new Vector3(0, 0.3f, 0));
    }

    virtual internal void Behit(p_hit_info_toc hitInfo, BasePlayer kAttacker = null, Vector3 hitPos = default(Vector3), int skillId = 0, System.Object arg = null)
    {
        if (serverData.alive == false)
            return;

        if (kAttacker != null && kAttacker is MainPlayer && GlobalConfig.showHitDebugInfo)
        {
            float dis = Vector3.Distance(position, kAttacker.position);
            string msg = string.Format("子弹击中[{0}] Part:{1} Life:{3}(-{4}) Armor:{5}(-{6}) Distance:{2}",
                PlayerId, hitInfo.bodyPart, dis, hitInfo.life, serverData.life - hitInfo.life, hitInfo.armor, serverData.last_armor - hitInfo.armor);
            Logger.Log(msg);
        }

        int hitValue = m_kServerData.life - hitInfo.life;
        int harm = m_kServerData.life - hitInfo.life;
        if (hitValue >= 0)
            hitValue += m_kServerData.last_armor - hitInfo.armor;

        if(!(kAttacker != null && kAttacker.Camp != Camp && hitInfo.life > serverData.life))
        {
            m_kServerData.life = hitInfo.life;
            m_kServerData.last_armor = hitInfo.armor;
            m_kServerData.last_damage_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
        }
       
        //if(WorldManager.singleton.isSurvivalModeOpen) PlayerBattleModel.Instance.UpdatePlayerInfo(this);

        //武器减速
        if (kAttacker != null && ((kAttacker.Camp != (int)WorldManager.CAMP_DEFINE.UNION && kAttacker.Camp != (int)WorldManager.CAMP_DEFINE.REBEL) || WorldManager.singleton.gameRuleLine.UseWeaponDecelerate == 1))
        {
            float deceValue = 0;
            float deceTime = 0;

            if (arg != null)
            {
                int atkIndex = (int)arg;
                var delayDamageData = kAttacker.localPlayerData.GetDelayDamageData(atkIndex);
                if (delayDamageData != null)
                {
                    if (delayDamageData.decelerateTime > 0)
                    {
                        deceValue = delayDamageData.decelerate - serverData.AntiDecelerate();
                        deceTime = delayDamageData.decelerateTime;
                    }
                }
            }
            else if (kAttacker.serverData != null && kAttacker.serverData.decelerateTime() > 0)
            {
                deceValue = kAttacker.serverData.decelerate();
                deceTime = kAttacker.serverData.decelerateTime();
            }

            if (deceTime > 0 && deceValue != 0)
                OnDecelerate(kAttacker, deceValue, deceTime);
        }
        if (RoomModel.IsPvp() == true)
        {
            //计算伤害玩家血量
            if (kAttacker != null && kAttacker is MainPlayer)
            {
                HarmInfo info = needCreatInfo(hitInfo.actorId);
                if (info == null)
                {
                    info = new HarmInfo();
                    MainPlayer.singleton.m_BeAttckHarmList.Add(info);
                }
                info.beActorId = hitInfo.actorId;
                info.harm += harm;
                info.beActorName = this.PlayerName;
                info.pid = m_kServerData.pid;
            }
        }

        if (configData != null && configData.HPAlert != 0)
        {
            if (serverData.life <= configData.HPAlert)
            {
                HPAlert(true);
            }
            else
                HPAlert(false);
        }

        var damageData = new DamageData()
        {
            victim = this,
            attacker = kAttacker,
            bodyPart = hitInfo.bodyPart,
            damage = hitValue,
            hitPos = hitPos,
            hitType = hitInfo.hurtType
        };

        GameDispatcher.Dispatch<DamageData>(GameEvent.PLAYER_BE_DAMAGE, damageData);
    }

    /// <summary>
    /// 检查列表里是否有数据，检查是否死亡
    /// </summary>
    /// <param name="actorId"></param>
    /// <param name="die"></param>
    /// <returns></returns>
    private HarmInfo needCreatInfo(int actorId)
    {
        for (int i = 0; i < MainPlayer.singleton.m_BeAttckHarmList.Count; i++)
        {
            if (MainPlayer.singleton.m_BeAttckHarmList[i].beActorId == actorId)
            {
                if (MainPlayer.singleton.m_BeAttckHarmList[i].die == false)
                {
                    return MainPlayer.singleton.m_BeAttckHarmList[i];
                }
            }
        }
        return null;
    }

    virtual protected void OnDecelerate(BasePlayer kAttacker, float value, float time)
    {
        if (kAttacker.curWeapon != null)
            m_hitEffectMgr.BeHit(kAttacker.curWeapon.weaponId);
    }

    virtual internal void AtkWrong(toc_fight_atk_wrong proto)
    {
        serverData.life = proto.life;
        serverData.last_damage_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
        var damageData = new DamageData()
        {
            victim = this,
            attacker = this,
            bodyPart = 0,
            damage = 0,
            hitPos = Vector3.zero,
            hitType = 0
        };
        GameDispatcher.Dispatch<DamageData>(GameEvent.PLAYER_BE_DAMAGE, damageData);
    }

    virtual public void Die(BasePlayer killer, int weapon, int hitPart = BODY_PART_ID_BODY, Vector3 bulletDir = default(Vector3), Vector3 diePos = default(Vector3), Vector3 killerPos = default(Vector3))
    {
        HPAlert(false);
        //serverData.state = GameConst.SERVER_ACTOR_STATUS_DEAD;
        m_bIsSuicide = killer == this;

        Vector3 curPos = Vector3.zero;
        if (WorldManager.singleton.isViewBattleModel)
        {
            BasePlayer curPlayer = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            if (curPlayer == null) return;
            curPos = curPlayer.position;
        }
        else if (MainPlayer.singleton != null)
            curPos = MainPlayer.singleton.position;
        else
            return;

        //如果被杀者是正在观战的人
        if (WorldManager.singleton.curPlayer != null && this == WorldManager.singleton.curPlayer)
        {
            GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_KILL_CURPLAYER, killer);
        }

        var config = m_configData;
        if (config == null) { Logger.Error("no itemrole line, id: " + serverData.itemRoleID); return; }

        if (SceneManager.singleton.battleSceneLoaded)
        {
            if (hitPart == BODY_PART_ID_HEAD)
            {
                if (config.DeadHeadSound.Length > 0)
                    AudioManager.PlayHitSound(position, config.DeadHeadSound[UnityEngine.Random.Range(0, config.DeadHeadSound.Length)], config.DeadSoundRange);
            }
            else
            {
                if (config.DeadSound.Length > 0)
                    AudioManager.PlayHitSound(position, config.DeadSound[UnityEngine.Random.Range(0, config.DeadSound.Length)], config.DeadSoundRange);
            }
        }

        //死亡时把buff清掉
        m_kBM.RemoveAllBuff();


        // 防止下次复活时，使用旧数据，新数据可能更新不及时 
        //m_kServerData.ClearData();

        weaponMgr.PlayerDieOrLock();

        m_hitEffectMgr.StopEffect();

        if (m_skillMgr != null)
            m_skillMgr.RemoveAll();
    }

    virtual public void SetState(int state, bool dataRevive)
    {
        m_iOldState = serverData.state;
        serverData.state = state;

        //if (dataRevive && m_iOldState == state)
        //    return;

        if (state == GameConst.SERVER_ACTOR_STATUS_DEAD) // dead
        {
            if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
            {
                if (WorldManager.singleton.CameraSwitcher != null)
                    WorldManager.singleton.CameraSwitcher.StopSwitching();
                if (SceneCameraController.singleton != null)
                    SceneCameraController.singleton.StopControl();
                if (SceneCamera.singleton != null)
                    SceneCamera.singleton.SetParent(null);
            }
        }

        if (state == GameConst.SERVER_ACTOR_STATUS_LOCK) // lock
        {
            Lock();
        }
        else if (state == GameConst.SERVER_ACTOR_STATUS_LIVE)  // live
        {
            Revive(dataRevive);
        }
        else if (m_iOldState == GameConst.SERVER_ACTOR_STATUS_DYING && state == GameConst.SERVER_ACTOR_STATUS_DEAD)
        {
            if (this is MainPlayer || this is FPWatchPlayer)
            {
                if (RescueBehavior.singleton != null) RescueBehavior.singleton.isRescueRevive = false;
                if (DeadPlayer.Instance != null) DeadPlayer.Instance.HideRescue();
            }
            else if (this is OtherPlayer)
            {
                SetActive(false);
            }
        }
        else if (m_iOldState == GameConst.SERVER_ACTOR_STATUS_LIVE && state == GameConst.SERVER_ACTOR_STATUS_DEAD)
        {
            if (m_bIsSuicide && isAI)
            {
                if (gameObject != null && (serverData == null || serverData.alive == false))
                {
                    SetActive(false);
                }
            }
            else if (this is MainPlayer || this is FPWatchPlayer)
            {
                if (m_configData != null && string.IsNullOrEmpty(m_configData.DieEffect))
                {
                    TimerManager.SetTimeOut(deadDisappearTime, () =>
                    {
                        if (DeadPlayer.Instance != null) DeadPlayer.Instance.Hide();
                    });

                    if (WorldManager.singleton.isFightInTurnModeOpen) SetActive(false);
                }
            }
            else if (this is OtherPlayer)
            {
                if (m_configData != null && string.IsNullOrEmpty(m_configData.DieEffect))
                {
                    TimerManager.SetTimeOut(deadDisappearTime, () =>
                    {
                        if (gameObject != null && (serverData == null || serverData.alive == false))
                        {
                            SetActive(false);
                        }
                    });
                }
                else
                {
                    if (gameObject != null && (serverData == null || serverData.alive == false))
                    {
                        SetActive(false);
                    }
                }
            }
        }
        else if (state == GameConst.SERVER_ACTOR_STATUS_DYING)
        {
            if (this is MainPlayer || this is FPWatchPlayer)
            {
                if (DeadPlayer.Instance != null) DeadPlayer.Instance.doDying();
            }
            else if (this is OtherPlayer)
            {
                (this as OtherPlayer).doDying();
            }
        }
        GameDispatcher.Dispatch(GameEvent.PLAYER_SET_STATE, this);
    }

    virtual public void SetActorType(int actorType)
    {
        if (serverData != null)
            serverData.actor_type = actorType;
    }

    virtual protected void ClearState()
    {
        status.ClearAllStatus();
    }

    virtual protected void Lock()
    {
        weaponMgr.PlayerDieOrLock();

        SetActive(false);
    }

    /// <summary>
    /// 当资源加载完，补调Revive时 dataRevive = false，不操作数据
    /// </summary>
    virtual protected void Revive(bool dataRevive)
    {
        // 服务端保证，在由 Lock, Dead 变为 Live之前先推Prop再推SetState
        // 所以这里使用的maxhp, maxArmor已经是最新的数据了        
        ClearState();

        if (dataRevive)
        {
            //serverData.life = serverData.maxhp();
            serverData.last_armor = serverData.maxArmor();
            SetPosNow(serverData.pos);
        }        //复活时把所有buff清掉

        //buffManager.RemoveAllBuff();

        if (gameObject != null)
        {
            SetActive(true);
            if (!isSwitchFPV)
                GameDispatcher.Dispatch(GameEvent.PLAYER_REVIVE, GameEvent.PLAYER_REVIVE, this);
        }
        //if (WorldManager.singleton.isSurvivalModeOpen) PlayerBattleModel.Instance.UpdatePlayerInfo(this);

        m_bloodEffectNum = 0;
    }

    virtual protected void GodMode(bool value)
    {
        m_bIsGodMode = value;
    }

    virtual protected void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        m_iOldState = -1;
        if (data.state == GameConst.Fight_State_GameStart)
        {
            buffManager.RemoveAllBuff();
        }
    }

    #endregion

    #region 可视度设置
    /// <summary>
    /// 设置可视度
    /// </summary>
    /// <param name="visableDegree">可视度</param>
    virtual public void SetVisableDegree(float visableDegree,float time)
    {
        if (m_kVM != null)
            m_kVM.SetVisable(visableDegree, time, true);
        if (curWeapon != null)
            curWeapon.SetVisableDegree(visableDegree,time);
    }
    #endregion

    #region 观战视角
    virtual public void RecoverView(BasePlayer fpBasePlayer)
    {
        WorldManager.singleton.SetPlayerById(PlayerId, this);
        m_strModel = "";
        OnData_Actor();
        m_kVM.SetVisable(fpBasePlayer.visableDegree);
        if (serverData.state == GameConst.SERVER_ACTOR_STATUS_DEAD)
            SetActive(false);
        else
        {
            SetPosNow(serverData.pos);
            SetState(serverData.state, false);
            if (serverData.state == GameConst.SERVER_ACTOR_STATUS_DYING)
            {
                SetActive(true);
                Die(null, -1);
            }
        }
        isSwitchFPV = false;
    }
    #endregion

    #region Fire Reload
    virtual internal void Fire2(toc_fight_fire arg)
    {

    }

    virtual internal void Atk(toc_fight_atk proto)
    {

    }

    virtual internal void ReloadAmmo2(toc_fight_reload arg)
    {

    }

    virtual internal void WeaponMotion(toc_fight_weapon_motion proto)
    {
        if (m_kWeaponMgr != null)
        {
            WeaponBase weapon = m_kWeaponMgr.TryGetWeapon(proto.weapon_id.ToString());
            if (weapon != null)
            {

                weapon.WeaponMotion(proto.motion);
            }
        }
    }

    #endregion

    #region 使用技能
    virtual internal void UseSkill(toc_fight_use_skill proto)
    {

    }

    virtual internal void UseWeaponAwakenSkill(toc_fight_weapon_awaken_skill proto)
    {

    }

    virtual internal void DoAIAction(toc_fight_ai_action proto)
    {

    }

    virtual internal void DoAISkill(toc_fight_ai_skill proto)
    {

    }

    //virtual internal void DoAISkill(int skill, float[][] targets)
    //{

    //}

    virtual internal void Rush(float distance, float time)
    {

    }

    virtual internal void ClientMove(Vector3 targetPos, float time)
    {

    }

    virtual internal void ClientStop(float time)
    {

    }

    #endregion

    #region buff
    public virtual void SetBuff(p_actor_buff_toc[] buffList)
    {
        if (buffList != null)
        {
            m_kBM.AddBuff(buffList);
        }
    }

    public virtual void SetScale(float scale)
    {

    }

    public virtual void SetFov(float fov)
    {

    }

    #endregion

    #region 角色设置
    virtual public void SetHeadScale(float scale)
    {

    }

    virtual public void HPAlert(bool isAlert)
    {
        m_bIsHPAlert = isAlert;
    }

    #endregion

    #region Get Set

    public GameObject gameObject
    {
        get { return m_kGo; }
    }

    public Transform transform
    {
        get { return m_kTrans; }
    }

    public Transform sceneCameraSlot
    {
        get { return m_transSceneCameraSlot; }
    }

    public bool attachedSceneCamera
    {
        get
        {
            if (sceneCameraSlot != null && sceneCameraSlot.transform.childCount == 1)
                return true;
            return false;
        }
    }

    public SoundPlayer voiceSoundPlayer
    {
        get { return m_kVoiceSoundPlayer; }
    }

    public SoundPlayer stepSoundPlayer
    {
        get { return m_kStepSoundPlayer; }
    }

    public PlayerAniBaseController playerAniController
    {
        get { return m_kPlayerAniController; }
    }

    public Transform rightHandSlot
    {
        get { return m_transRightHandSlot; }
    }

    public Transform leftHandSlot
    {
        get { return m_transLeftHandSlot; }
    }

    virtual public Transform headSlot
    {
        get { return null; }
    }

    virtual public Vector3 position
    {
        get
        {
            if (m_kTrans == null)
            {
                return serverData.serverPos;
            }
            return m_kTrans.position;
        }
        set
        {
            if (m_kTrans != null)
                m_kTrans.position = value;
        }
    }

    public Vector3 serverPos
    {
        get { return serverData.serverPos; }
    }

    public Vector3 eulerAngle
    {
        get
        {
            if (m_kTrans == null)
            {
                return serverData.serverRot;
            }
            return m_kTrans.eulerAngles;
        }
        set
        {
            if (m_kTrans != null)
                m_kTrans.eulerAngles = value;
        }
    }

    public PlayerStatus status
    {
        get { return m_kStatus; }
    }

    public bool justMoveNotJump
    {
        get { return m_kStatus == null ? false : m_kStatus.move && !m_kStatus.jump; }
    }

    public PlayerServerData serverData
    {
        get { return m_kServerData; }
    }

    public bool isHoldMainWeapon
    {
        get { return m_kServerData != null && m_kServerData.curWeaponSubType == GameConst.WEAPON_SUBTYPE_WEAPON1; }
    }

    public bool hasMainWeapon
    {
        get
        {
            return mainWeaponConfig != null;
        }
    }

    public ConfigItemWeaponLine mainWeaponConfig
    {
        get
        {
            if (m_kServerData == null || m_kServerData.weaponList == null) return null;
            int length = m_kServerData.weaponList.Count;
            for (int i = 0; i < length; i++)
            {
                var line = ItemDataManager.GetItemWeapon(m_kServerData.weaponList[i]);
                if (line == null) continue;
                if (line.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1) return line;
            }
            return null;
        }
    }

    public bool canDropWeapon
    {
        get
        {
            if (curWeapon == null || curWeapon.weaponConfigLine == null) return false;
            return curWeapon.weaponConfigLine.CanDrop == 1;
        }
    }

    public bool isHero
    {
        get
        {
            if (m_kServerData == null) return false;
            return m_kServerData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_HERO || m_kServerData.actor_type == GameConst.ACTOR_TYPE_HERO;
        }
    }

    public LocalPlayerData localPlayerData
    {
        get { return m_localPlayerData; }
    }

    public WeaponManager weaponMgr
    {
        get { return m_kWeaponMgr; }
    }

    public virtual WeaponBase curWeapon
    {
        get { return m_kWeaponMgr.curWeapon; }
    }

    public virtual WeaponBase subWeapon
    {
        get { return m_kWeaponMgr.subWeapon; }
    }

    virtual public MainPlayerCameraShaker cameraShaker
    {
        get { return null; }
    }

    //public bool tweenPosEnable
    //{
    //    set
    //    {
    //        if (m_tweenPos != null) m_tweenPos.enabled = value;
    //    }
    //}

    public string ColorName
    {
        get
        {
            int index = serverData.camp == 0 && serverData.id == WorldManager.singleton.myid ? 1 : serverData.camp;
            index = index < WorldManager.CAMP_COLOR.Length ? index : 0;
            return string.Format("<color={0}>{1}</color>", WorldManager.CAMP_COLOR[index], serverData.name);
        }
    }

    public string PlayerName { get { return serverData == null ? "null" : serverData.name; } }

    public int Camp { get { return serverData == null ? -1 : serverData.camp; } }

    public long Pid { get { return serverData == null ? -1 : serverData.pid; } }

    public int PlayerId { get { return serverData == null ? -1 : serverData.id; } }

    public int RoleId { get { return serverData == null ? -1 : serverData.itemRoleID; } }

    public int MaxHp { get { return serverData == null ? 0 : serverData.maxhp(); } }

    public int CurHp { get { return serverData == null ? 0 : serverData.life; } }

    public bool alive { get { return serverData == null ? false : serverData.alive; } }

    public bool released
    {
        get { return m_bReleased; }
        set { m_bReleased = value; }
    }

    public float GodTime
    {
        get
        {
            return m_fGodTime;
        }
        set
        {
            m_fGodTime = value / 1000;
            if (value > 0)
            {
                m_kStatus.god = true;
                GodMode(true);
            }
            else
            {
                m_kStatus.god = false;
                GodMode(false);
            }
        }
    }

    public bool isBombHolder
    {
        get { return PlayerId == PlayerBattleModel.Instance.bombHolder; }
    }

    //public int bombHolderId
    //{
    //    get
    //    {
    //        return WorldManager.singleton.bomHolder;
    //    }
    //}

    public int modelSex
    {
        get
        {
            return m_iModelSex;
        }
    }

    // 是否为MVP
    public bool isMVP
    {
        get { return PlayerId == PlayerBattleModel.Instance.MVP; }
    }
    // 是否为金色ACE
    public bool isGoldACE
    {
        get { return PlayerId == PlayerBattleModel.Instance.GoldACE; }
    }
    // 是否为银色ACE
    public bool isSilverACE
    {
        get { return PlayerId == PlayerBattleModel.Instance.SilverACE; }
    }

    public float visableDegree
    {
        get { return m_kVM.visableDegree; }
    }

    public bool isVisible
    {
        get { return m_kVM.visableDegree > 0f; }
    }

    public bool isIgnoreTransparent
    {
        get { return m_bIsIgnoreTransparent; }
        set { m_bIsIgnoreTransparent = value; }
    }

    public bool canPlayHeartBeat
    {
        get { return m_bCanPlayHeartBeat; }
        set { m_bCanPlayHeartBeat = value; }
    }

    public bool isBeingRescued
    {
        get { return m_bIsBeingRescued; }
    }

    public bool isDead
    {
        get { return serverData != null ? serverData.state == GameConst.SERVER_ACTOR_STATUS_DEAD : false; }
    }

    public bool isDying
    {
        get { return serverData != null ? serverData.state == GameConst.SERVER_ACTOR_STATUS_DYING : false; }
    }
    
    public float dieTimeStamp
    {
        get { return GetDieStamp(PlayerId); }
        set { SetDieStamp(PlayerId, value); }
    }
    public bool isAlive
    {
        get { return serverData != null ? serverData.state == GameConst.SERVER_ACTOR_STATUS_LIVE : false; }
    }

    public bool isLock
    {
        get { return serverData != null ? serverData.state == GameConst.SERVER_ACTOR_STATUS_LOCK : false; }
    }

    public int preState
    {
        get { return m_iOldState; }
    }

    public float height
    {
        get { return m_height; }
        set { m_height = value; }
    }

    public Vector3 headPos
    {
        get { return position + new Vector3(0, m_height, 0); }
    }

    public bool isCancelHitBack
    {
        get { return m_bIsCancelHitBack; }
        set { m_bIsCancelHitBack = value; }
    }

    public BuffManager buffManager
    {
        get { return m_kBM; }
    }

    public bool isRendererVisible
    {
        get
        {
            if (m_renderer == null)
                return false;

            return m_renderer.isVisible;
        }
    }

    public Renderer renderer
    {
        get
        {
            return m_renderer;
        }
    }

    //角色类型
    public int modelType
    {
        get { return m_iModelType; }
    }

    //是不是躲猫猫模型
    public bool isDmmModel
    {
        get { return m_iModelType == GameConst.MODEL_TYPE_DMM; }
    }

    public bool isDmmModelPre
    {
        get { return m_iModelTypePre == GameConst.MODEL_TYPE_DMM; }
    }

    public bool isZombie
    {
        get
        {
            if (serverData == null)
                return false;

            return (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX
                || serverData.actor_type == GameConst.ACTOR_TYPE_ELITE
                || serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX);
        }
    }

    public bool isHuman
    {
        get { return serverData == null ? false : (serverData.actor_type == GameConst.ACTOR_TYPE_PERSON); }
    }

    public bool canFirstPersonWatch
    {
        get { return (m_configData != null) ? m_configData.CanFirstPersonWatch == 1 : false; }
    }

    public bool isCancelDisplayWatchInfo
    {
        get { return (m_configData != null) ? m_configData.IsCancelDisplayWatchInfo == 1 : false; }
    }

    public Vector3 centerPos
    {
        get
        {
            return transHips == null ? position + new Vector3(0, height * 0.5f, 0) : transHips.position;
        }
    }

    public bool isMaxBloodEffect
    {
        get { return m_bloodEffectNum >= GlobalConfig.gameSetting.MaxBloodEffectNum; }
    }

    public ConfigItemRoleLine configData
    {
        get { return m_configData; }
    }

    public Transform transHips
    {
        get { return m_transHips; }
    }

    public float scale
    {
        get { return m_playerEffectMgr.scale; }
    }

    public float gameObjectScale
    {
        get { return Math.Max(transform.localScale.x,transform.localScale.y); }
    }

    public float lastScale
    {
        get { return m_playerEffectMgr.lastScale; }
    }

    public EptFloat lastBigHeadScale
    {
        get { return m_playerEffectMgr.lastBigHeadScale; }
        set { m_playerEffectMgr.lastBigHeadScale = value; }
    }

    public PlayerEffectManager playerEffectMgr
    {
        get { return m_playerEffectMgr; }
    }

    #region 第一人称视角
    public bool isSwitchFPV
    {
        get { return m_isSwitchFPV; }
        set { m_isSwitchFPV = value; }
    }
    #endregion

    public string modelName
    {
        get { return m_strModel; }
    }

    public int ScoutSkillState
    {
        get { return m_scoutSkillState; }
        set { m_scoutSkillState = value; }
    }

    public bool reuseWeaponSkill
    {
        get { return m_reuseWeaponSkill; }
        set { m_reuseWeaponSkill = value; }
    }


    public GhostJumpRecord ghostJumpRecord
    {
        get
        {
            if (m_ghostJumpRecord == null)
                m_ghostJumpRecord = new GhostJumpRecord();
            return m_ghostJumpRecord;
        }

        set
        {
            m_ghostJumpRecord = value;
        }
    }

    public Vector3 beShotPos
    {
        get
        {
            return position;
        }
    }

    public SkillManager skillMgr
    {
        get
        {
            return m_skillMgr;
        }
    }

    public bool isAI
    {
        get
        {
            return Pid <= 0;
        }
    }

    virtual public float clientMoveSpeed
    {
        get { return m_clientMoveSpeed; }
        set { m_clientMoveSpeed = value; }
    }

    public bool isStoryPlayer
    {
        get { return m_iModelType == GameConst.MODEL_TYPE_STORY; }
    }

    virtual public float deadDisappearTime
    {
        get { return (m_configData != null && m_configData.DeadDisappearTime > 0) ? m_configData.DeadDisappearTime : GameSetting.OTHER_PLAYER_DEAD_DISAPPEAR_TIME; }
    }

    #endregion


}