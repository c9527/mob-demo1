﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class SkillEffectParam
{
    public float[] pos;
    public int[] target_ids;
}

class SkillHitPlayerParam
{
    public int playerId;
    public int configId;
}

/// <summary>
/// 技能管理器
/// </summary>
public class SkillManager
{
    private BasePlayer m_kPlayer;
    private List<Effect> m_listAllEffect = new List<Effect>();

    private Vector3 m_alertPos; //要冲刺或跳跃的目标，多目标的话只取一个。现在用来计算BOSS到目标点的距离，客户端移动用
    private List<Vector3> m_calcTargetsList;
    private int m_attackTimer = -1; //不用了
    private List<int> m_attackTimeList = new List<int>();
    private float m_radius = 0f;
    private float m_dis = 0f;
    private bool isAlertShowing;
    private int m_alertTimeout = -1;
    private ConfigAISkillLine m_AISkillconfig;

    private int m_loopTimeout = -1;
    private string m_preActionType;
    private bool m_released;
    private Dictionary<string, string> m_aniEffectDic = new Dictionary<string, string>();
    public FlyManager flm = new FlyManager();

    public SkillManager(BasePlayer player)
    {
        m_calcTargetsList = new List<Vector3>();
        BindPlayer(player);
        //flm.init(ConfigManager.GetConfig<ConfigSkill>().GetLine(620103));
    }

    /// <summary>
    /// 绑定玩家
    /// </summary>
    /// <param name="player">玩家</param>
    public void BindPlayer(BasePlayer player)
    {
        m_kPlayer = player;
    }

    public void MPUseWeaponAwakenSkill(int awakenId, int skillId, float[] pos, int[] target_ids, string skillParam)
    {
        var awakenProp = ConfigManager.GetConfig<ConfigWeaponAwakenProp>().GetLine(awakenId);
        if (awakenProp != null)
        {
            MPUseSkill(skillId, pos, target_ids, skillParam);
        }
    }

    /// <summary>
    /// 用使用技能
    /// </summary>
    public void MPUseSkill(int skillId, float[] pos, int[] target_ids, string skillParam, long tauntedPlayerId=0)
    {
        TDSKillInfo skill = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);
        if (skill.Effect == GameConst.SKILL_EFFECT_USEWEAPON)    //武器技能
        {
            UseWeaponSkill(skill.Param);
        }
        else if (skill.Effect == GameConst.SKILL_EFFECT_SCOUT)   //侦查
        {
            StartScoutSkill(skill.Param);
        }
        else if (skill.Effect == GameConst.SKILL_EFFECT_RUSH)     //冲刺
        {
            RushSkill(skill.ClientParam);
        }
        else if (skill.Effect == GameConst.SKILL_EFFECT_ACTIVE_SKILL)     //主动技能
        {
            ActiveSkill(skill.Param, pos, target_ids);
        }
        else if (skill.Effect == GameConst.SKILL_EFFECT_TRANSFORM)     //变身
        {
            TransformSkill(skill.Param);
        }

        GameDispatcher.Dispatch(GameEvent.UI_REFRESH_SKILL, skillId);
        if (!string.IsNullOrEmpty(skillParam))
        {
            GameDispatcher.Dispatch<int, string, int, long>(GameEvent.UI_REFRESH_SKILL_WITHPARAM, skillId, skillParam, m_kPlayer.PlayerId, tauntedPlayerId);
        }
    }

    public void OPUseWeaponAwakenSkill(int awakenId, int skillId, float[] pos, int[] target_ids, string skillParam)
    {
        var awakenProp = ConfigManager.GetConfig<ConfigWeaponAwakenProp>().GetLine(awakenId);
        if (awakenProp != null)
        {
            OPUseSkill(skillId, pos, target_ids, skillParam);
        }
    }

    public void OPUseSkill(int skillId, float[] pos, int[] target_ids, string skillParam, long tauntedPlayerId = 0)
    {
        TDSKillInfo skill = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);
        if (skill.Effect == GameConst.SKILL_EFFECT_RUSH)     //冲刺
        {
            RushSkill(skill.ClientParam);
        }
        else if (skill.Effect == GameConst.SKILL_EFFECT_ACTIVE_SKILL)     //主动技能
        {
            ActiveSkill(skill.Param, pos, target_ids);
        }
        if (!string.IsNullOrEmpty(skillParam))
        {
            GameDispatcher.Dispatch<int, string, int, long>(GameEvent.UI_REFRESH_SKILL_WITHPARAM, skillId, skillParam, m_kPlayer.PlayerId, tauntedPlayerId);
        }
    }

    private void UseWeaponSkill(string weaponId)
    {
        m_kPlayer.reuseWeaponSkill = false;
        WeaponBase curWeapon = m_kPlayer.curWeapon;
        if (curWeapon.weaponId == weaponId && curWeapon.gameObject != null)
            curWeapon.SkillFire(m_kPlayer.SwitchPreWeapon);
        else
            m_kPlayer.reuseWeaponSkill = true;
    }

    private void StartScoutSkill(string param)
    {
        m_kPlayer.ScoutSkillState = GameConst.SCOUT_SKILL_STATE_START;
        int scoutTime = 0;
        int.TryParse(param, out scoutTime);
        TimerManager.SetTimeOut(scoutTime, StopScoutSkill);
    }

    private void StopScoutSkill()
    {
        if (m_kPlayer.ScoutSkillState != GameConst.SCOUT_SKILL_STATE_START)
            return;
        m_kPlayer.ScoutSkillState = GameConst.SCOUT_SKILL_STATE_END;
        TimerManager.SetTimeOut(2, () => { m_kPlayer.ScoutSkillState = GameConst.SCOUT_SKILL_STATE_NODEFINE; });
    }

    private void RushSkill(string param)
    {
        string[] paramArr = param.Split(',', ';');
        if (paramArr != null && paramArr.Length == 2)
        {
            float distance = 0;
            float time = 0;
            float.TryParse(paramArr[0], out distance);
            float.TryParse(paramArr[1], out time);
            m_kPlayer.Rush(distance, time);
        }
    }

    public SkillEffectParam GetWeaponAwakenSkillEffectParam(int awakenId)
    {
        var awakenProp = ConfigManager.GetConfig<ConfigWeaponAwakenProp>().GetLine(awakenId);
        if (awakenProp != null)
        {
            int skillId = 0;
            if( int.TryParse(awakenProp.Param, out skillId))
                return GetSkillEffectParam(skillId);
        }
        return null;
    }

    public SkillEffectParam GetSkillEffectParam(int skillId)
    {
        SkillEffectParam asParam = new SkillEffectParam();

        var config = GetSkillEffectConfig(skillId);

        if (config == null)
        {
            asParam.pos = new float[0];
            asParam.target_ids = new int[0];
        }
        else
        {
            Vector3 pos = m_kPlayer.position;
            Vector3 ang = m_kPlayer.eulerAngle;
            float[] sendPos = new float[6] { pos.x, pos.y, pos.z, ang.x, ang.y, ang.z };
            asParam.pos = sendPos;
            asParam.target_ids = GetSkillHitPlayers(config);
        }
        return asParam;
    }

    public static ConfigSkillEffectLine GetSkillEffectConfig(int skillId)
    {
        ConfigSkillEffectLine config = null;

        TDSKillInfo skill = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);
        if (skill.Effect == GameConst.SKILL_EFFECT_ACTIVE_SKILL)
        {
            int skillEffectId = 0;
            if (int.TryParse(skill.Param, out skillEffectId))
                config = ConfigManager.GetConfig<ConfigSkillEffect>().GetLine(skillEffectId);
        }

        return config;
    }

    private int[] GetSkillHitPlayers(ConfigSkillEffectLine config)
    {
        List<int> targetList = new List<int>();

        Dictionary<int, BasePlayer> dicAllPlayer = WorldManager.singleton.allPlayer;
        foreach (BasePlayer player in dicAllPlayer.Values)
        {
            if (!CheckHitPlayer(player, config))
                continue;
            targetList.Add(player.PlayerId);
        }
        return targetList.ToArray();
    }

    #region 变身

    private void TransformSkill(string param)
    {
        var skillParams  = param.Split(';');
        var modelId = int.Parse(skillParams[0]);
        var prepareTime = float.Parse(skillParams[1]);
        if (UIManager.IsOpen<PanelBattle>())
        {
            Vector2 pos = new Vector2(0, -115);
            var prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = new Vector2(0, -85), textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } } };
            UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "变身准备时间", prepareTime, prop, "TransfromPrepare");
        }
    }
    
    #endregion


    #region 主动技能

    private void ActiveSkill(string param, float[] pos, int[] target_ids)
    {
        int id = -1;
        int.TryParse(param, out id);
        ConfigSkillEffectLine config = ConfigManager.GetConfig<ConfigSkillEffect>().GetLine(id);
        if (config == null)
            return;

        string action = "";
        if (m_kPlayer is MainPlayer || m_kPlayer is FPWatchPlayer)
        {
            action = config.Action[0];
        }
        else
        {
            action = config.Action[1];
        }

        m_kPlayer.playerAniController.Play(action);

        Action callback = () =>
        {
            if (m_released || !m_kPlayer.alive || m_kPlayer.released)
                return;

            if (!(m_kPlayer is MainPlayer) && pos != null && pos.Length > 0)
                m_kPlayer.SetPosNow(pos);

            if (config.Directional == 0) //非指向型
                PlayAreaSkill(config, pos, target_ids);
            else if (config.Directional == 1) //指向型
                PlayDirectionalSkill(config, pos, target_ids);
        };

        if (config.EffectDelay > 0)
        {
            TimerManager.SetTimeOut(config.EffectDelay, callback);
        }
        else
            callback();
    }

    private void PlayAreaSkill(ConfigSkillEffectLine config, float[] pos, int[] target_ids)
    {
        EffectManager.singleton.GetEffect<EffectTimeout>(config.Effect, (effect) =>
        {
            if (m_released)
                return;

            Vector3 effectPos = Vector3.zero;
            switch (config.EffectSlotType)
            {
                case 1:
                    effectPos = m_kPlayer.centerPos;
                    break;
                default:
                    switch (config.Type)
                    {
                        case 0:
                        case 2:
                            effectPos = m_kPlayer.position + m_kPlayer.transform.forward * config.Range[0] * 0.5f;
                            break;
                        case 1:
                            effectPos = m_kPlayer.position;
                            break;
                    }
                    break;
            }

            effect.Attach(effectPos, m_kPlayer.transform.forward);
            effect.SetTimeOut(config.EffectTime);
            effect.Play();
        });
    }
    
    /// <summary>
    /// 播放指向型技能
    /// </summary>
    /// <param name="config"></param>
    /// <param name="pos"></param>
    /// <param name="target_ids"></param>
    private void PlayDirectionalSkill(ConfigSkillEffectLine config, float[] pos, int[] target_ids)
    {
        List<BasePlayer> hitPlayerList = null;
        hitPlayerList = new List<BasePlayer>();
        for (int i = 0, imax = target_ids.Length; i < imax; ++i)
        {
            BasePlayer bp = WorldManager.singleton.GetPlayerById(target_ids[i]);
            hitPlayerList.Add(bp);
        }

        for (int i = 0, imax = hitPlayerList.Count; i < imax; ++i)
        {
            BasePlayer bp = hitPlayerList[i];

            EffectManager.singleton.GetEffect<EffectTimeout>(config.Effect, (effect) =>
            {
                if (m_released)
                    return;

                Vector3 effectPos = Vector3.zero;
                switch (config.EffectSlotType)
                {
                    case 1:
                        effectPos = m_kPlayer.centerPos;
                        break;
                    default:
                        switch (config.Type)
                        {
                            case 0:
                            case 2:
                                effectPos = m_kPlayer.position + m_kPlayer.transform.forward * config.Range[0] * 0.5f;
                                break;
                            case 1:
                                effectPos = m_kPlayer.position;
                                break;
                        }
                        break;
                }

                effect.Attach(effectPos, m_kPlayer.transform.forward);
                effect.SetTimeOut(config.EffectTime);
                if (m_kPlayer is MainPlayer)
                {
                    SkillHitPlayerParam p = new SkillHitPlayerParam();
                    p.playerId = bp.PlayerId;
                    p.configId = config.ID;
                    effect.SetCompleteCallBack(p, HitPlayer);
                }
                else
                {
                    effect.SetCompleteCallBack(null, null);
                }

                //设置飞行
                PosTweener posTweener = effect.gameObject.AddMissingComponent<PosTweener>();
                if (bp.gameObject != null)
                    posTweener.TweenPos(config.EffectTime, bp.transHips != null ? bp.transHips : bp.transform);
                else
                    posTweener.TweenPos(config.EffectTime, bp.position);

                effect.Play();
            });
        }
    }

    private void HitPlayer(object data)
    {
        SkillHitPlayerParam _data = data as SkillHitPlayerParam;
        if (_data == null)
            return;
        Logger.Log("HitPlayer:playerId=" + _data.playerId + "   configId=" + _data.configId);
    }

    private bool CheckHitPlayer(BasePlayer player, ConfigSkillEffectLine config)
    {
        switch(config.Camp)
        {
            case 0: //全阵营生效
                break;
            case 1: //我方生效
                if(m_kPlayer.IsEnemy(player))
                    return false;
                break;
            case 2:
                if (!m_kPlayer.IsEnemy(player))
                    return false;
                break;
            default:
                return false;
        }

        if (WeaponBase.CanAttack(player) == false)
            return false;

        if (config.PenetrateWall == 0 && IsOcclusionCulling(player)) //穿墙判定
            return false;

        switch (config.Type)
        {
            case 0: //矩形
                return IsInRect(m_kPlayer.transform, player.transform, config.Range[0], config.Range[1]);
            case 1: //圆形
                return IsInCircle(m_kPlayer.transform, player.transform, config.Range[0]);
            case 2: //扇形
                return IsInSector(m_kPlayer.transform, player.transform, config.Range[0], config.Range[1]);
        }
        return false;
    }

    /// <summary>
    /// 是否在圆形中
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    private bool IsInCircle(Transform source, Transform target, float radius)
    {
        return (source.position - target.position).sqrMagnitude <= radius * radius;
    }

    /// <summary>
    /// 是否在扇形中
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="radius"></param>
    /// <param name="angle"></param>
    /// <returns></returns>
    private bool IsInSector(Transform source, Transform target, float radius, float angle)
    {
        float distance = Vector3.Distance(source.position, target.position);
        if (distance > radius)
            return false;

        Vector3 tVec = new Vector3(target.position.x, source.position.y, target.position.z) - source.position;
        Vector3 sVec = source.forward;//source.position + (source.rotation * Vector3.forward) * radius;

        float targetAngle = Vector3.Angle(tVec.normalized, sVec.normalized);
        if (targetAngle <= angle * 0.5f && targetAngle >= -angle * 0.5f)
            return true;
        return false;
    }


    /// <summary>
    /// 是否在矩形中
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <param name="length"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    private bool IsInRect(Transform source, Transform target, float length, float width)
    {
        Quaternion r = source.rotation;
        Vector3 left = source.position + (r * Vector3.left) * length * 0.5f;
        Vector3 right = source.position + (r * Vector3.right) * length * 0.5f;

        Vector3 leftEnd = left + (r * Vector3.forward) * width;
        Vector3 rightEnd = right + (r * Vector3.forward) * width;

        return IsInRect(target.position, leftEnd, rightEnd, right, left);
    }

    private bool IsInRect(Vector3 point, Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
    {
        float x = point.x;
        float y = point.z;

        float v0x = v0.x;
        float v0y = v0.z;

        float v1x = v1.x;
        float v1y = v1.z;

        float v2x = v2.x;
        float v2y = v2.z;

        float v3x = v3.x;
        float v3y = v3.z;

        if (Multiply(x, y, v0x, v0y, v1x, v1y) * Multiply(x, y, v3x, v3y, v2x, v2y) <= 0 && Multiply(x, y, v3x, v3y, v0x, v0y) * Multiply(x, y, v2x, v2y, v1x, v1y) <= 0)
            return true;

        return false;
    }

    private float Multiply(float p1x, float p1y, float p2x, float p2y, float p0x, float p0y)
    {
        return ((p1x - p0x) * (p2y - p0y) - (p2x - p0x) * (p1y - p0y));
    }

    private bool IsOcclusionCulling(BasePlayer player)
    {
        return !AutoAim.IsAutoAimTarget(player);
    }

    #endregion

    private void PlayLoopAni(bool isLoop)
    {
        if (m_kPlayer.isAlive && m_kPlayer.playerAniController.animator)
        {
            m_kPlayer.playerAniController.SetBool("loop", isLoop);
        }
    }

    private void PlayAniEffect(string aniName, bool isActive)
    {
        if(m_kPlayer.released || m_kPlayer.gameObject == null)
            return;

        if (null != m_kPlayer.gameObject.transform.Find(aniName))
        {
            m_kPlayer.gameObject.transform.Find(aniName).gameObject.TrySetActive(isActive);
        }
        else
        {
            if (!string.IsNullOrEmpty(aniName))
                Logger.Error("配置了不存在的绑定在怪物中的技能特效：" + aniName);
        }
    }

    public void DoAIAction(int action, Vector3 pos)
    {
        ConfigAIActionLine config = ConfigManager.GetConfig<ConfigAIAction>().GetLine(action);
        if (config == null)
        {
            Logger.Error("StartUseSkill get ConfigAIAction error, id=" + action);
            return;
        }

        if (config.Type == GameConst.AIACTION_JUMP)
        {
            m_kPlayer.ClientMove(m_alertPos, config.ClientTime / 1000f);
        }

        if (config.Type == GameConst.AIACTION_MOVE_FORWARD) //冲刺
        {
            PlayLoopAni(true);
            m_kPlayer.ClientMove(pos, config.ClientTime / 1000f);
        }
        else
        {
            PlayLoopAni(false);
        }

        if (!string.IsNullOrEmpty(config.Effect))
        {
            Action<Vector3> doOneShowEffect = (target) =>
                {
                    EffectManager.singleton.GetEffect<EffectTimeout>(config.Effect, (effect) =>
                    {
                        if (m_released)
                            return;

                        if (config.Type == GameConst.AIACTION_DAMAGE_AREA)
                            effect.Attach(target, (target - m_kPlayer.position));
                        else
                            effect.Attach(target);

                        effect.SetTimeOut(-1);
                        effect.Play();
                    });
                };

            switch (config.EffectAttach)
            {
                case GameConst.AIACTION_EFFECT_ATTACH_TARGET_PLAYER:
                    doOneShowEffect(m_kPlayer.centerPos);
                    break;
                case GameConst.AIACTION_EFFECT_ATTACH_TARGET_POINT:
                    if (m_calcTargetsList != null)
                    {
                        for (int i = 0; i < m_calcTargetsList.Count; i++)
                            doOneShowEffect(m_calcTargetsList[i]);
                    }
                    break;
                default:
                    doOneShowEffect(m_kPlayer.position);
                    break;
            }
        }

        //Animator中的特效开启和关闭
        if (!string.IsNullOrEmpty(config.AniEffect) && null != m_kPlayer.gameObject)
        {
            //双重大括号中，配置表会解析成';'和'#'分隔
            string[] aniStrArr = config.AniEffect.Split(new Char[] { ';', '#' }, StringSplitOptions.RemoveEmptyEntries);
            if (aniStrArr.Length > 2) //保证有一个值
            {
                for (int i = 0; i < aniStrArr.Length; i += 3)
                {
                    int delay;
                    if (Int32.TryParse(aniStrArr[i + 1], out delay))
                    {
                        if (!m_aniEffectDic.ContainsKey(aniStrArr[i]))
                            m_aniEffectDic.Add(aniStrArr[i], aniStrArr[i + 2]);
                        PlayAniEffect(aniStrArr[i + 2], true);
                        TimerManager.SetTimeOut(delay / 1000f, RestoreAniEffect, aniStrArr[i + 2]);
                    }
                }
            }
        }

        if (null != m_kPlayer.gameObject)
        {
            if (!string.IsNullOrEmpty(config.Animation))
                m_kPlayer.playerAniController.Play(config.Animation);
            //有武器的攻击
            if (config.Type == GameConst.AIACTION_ATTACK)
                SkillAttack(config.Params, config.ClientTime / 1000f);
        }

        //播放音效
        if (config.ActionSound.Length > 0)
            AudioManager.PlayHitSound(m_kPlayer.position, config.ActionSound[UnityEngine.Random.Range(0, config.ActionSound.Length)], config.ActionSoundRange);

        //处理震屏，针对主角
        if (0 != config.ShakeID && MainPlayer.singleton != null && MainPlayer.singleton.isAlive)
        {
            ConfigShakeLine cfgcs = ConfigManager.GetConfig<ConfigShake>().GetLine(config.ShakeID);
            if (null != cfgcs)
            {
                if ((MainPlayer.singleton.position - m_kPlayer.position).sqrMagnitude <= cfgcs.IncidenceRadius * cfgcs.IncidenceRadius)
                {
                    if (SceneCamera.singleton != null)
                        SceneCamera.singleton.StartShake(cfgcs);
                }
            }
        }

        m_preActionType = config.Type;
    }

    public void DoAISkill(int skill, float[][] targets)
    {
        //Logger.Error("--DoAISkill, skillID: " + skill + ",  targets.Length: " + targets.Length);
        isAlertShowing = false;
        if (null == targets || 0 == targets.Length)
        {
            //Logger.Error("targets is null or Length == 0, skill:" + skill);
            return;
        }
        m_calcTargetsList.Clear();
        m_alertPos = new Vector3(targets[0][0], targets[0][1], targets[0][2]);
        //Debug.LogError(targets[0][0] + "   " + targets[0][1] + "   " + targets[0][2]);
        m_radius = targets[0][3];
        m_alertPos = SceneManager.GetGroundPoint(m_alertPos);
        //Debug.LogError( " m_alertPos   " + targets[0][0] + "   " + targets[0][1] + "   " + targets[0][2]);
        Vector2 a = new Vector2(targets[0][0], targets[0][2]);
        Vector2 b = new Vector2(m_kPlayer.position.x, m_kPlayer.position.z);
        m_dis = Vector2.Distance(a, b);
        
        //施放技能时，朝向以服务端为准，根据目标直接设朝向
        if (null != m_kPlayer.gameObject && m_alertPos != m_kPlayer.position && 0 != m_kPlayer.serverData.standMoveSpeed())
        {
            Vector3 targetEuler = Quaternion.LookRotation((m_alertPos - m_kPlayer.position)).eulerAngles;
            m_kPlayer.eulerAngle = new Vector3(0, targetEuler.y, 0);
        }

        ConfigAISkillLine config = ConfigManager.GetConfig<ConfigAISkill>().GetLine(skill);
        if (config == null)
        {
            Logger.Error("StartUseSkill get ConfigAISkill error, skill_id=" + skill);
            return;
        }

        if (null == m_kPlayer.gameObject)
            return;

        //这里直接清除移动包，据说开始发招都不要动，蓄力
        m_kPlayer.ClearPosQueue();

        m_AISkillconfig = config;

        if (m_kPlayer.isAlive && m_kPlayer.playerAniController.animator)
        {
            if (config.CullingMode > 0)
                m_kPlayer.playerAniController.animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            else
                m_kPlayer.playerAniController.animator.cullingMode = AnimatorCullingMode.BasedOnRenderers;
        }

        SkillAlertCircle(config.AlertType, config.AlertHeightType, config.ClientTime / 1000f, config.Effect, m_dis, targets, config.AlertEulerAngles);

        //这里作计时测试预警跟主角的位置，发现警告
        if (config.ClientTime > 0)
        {
            isAlertShowing = true;
            m_alertTimeout = TimerManager.SetTimeOut(config.ClientTime / 1000f, () =>
            {
                TimerManager.RemoveTimeOut(m_alertTimeout);
                m_alertTimeout = -1;
                isAlertShowing = false;
                //Logger.Log("-----isAlertShowing = false");
            });
        }
    }

    private void SkillAlertCircle(string type, int heightType, float time, string effect, float dis, float[][] targets, Vector3 eulerAngles)
    {
        if (null == targets || 0 == targets.Length)
            return;

        //m_alertPos = SceneManager.GetGroundPoint(new Vector3(targets[0][0], m_kPlayer.gameObject != null ? m_kPlayer.transform.position.y : targets[0][1], targets[0][2]));
        if (!string.IsNullOrEmpty(effect))
        {
            /*if (null == param || param.Length < 2)
            {
                Logger.Error("AISkill, AlertParam is null or Length < 2");
                return;
            }*/
            float radius = targets[0][3];

            Action<Vector3> doOneAlert = (targetsAction) =>
            {
                EffectManager.singleton.GetEffect<EffectTimeout>(effect, (effectObj) =>
                {
                    if (m_released || !m_kPlayer.isAlive || effectObj == null || effectObj.gameObject == null || effectObj.transform == null)
                        return;

                    Vector3 forward = m_kPlayer.gameObject != null ? m_kPlayer.transform.forward : (Quaternion.Euler(m_kPlayer.eulerAngle) * Vector3.forward);
                    m_listAllEffect.Add(effectObj);
                    
                    if (type == GameConst.AIACTION_ALERT_RECTANGLE)
                        effectObj.transform.localScale = new Vector3(radius, 1, (dis * 0.5f));
                    else
                        effectObj.transform.localScale = new Vector3(radius, targetsAction.y, radius);
                    effectObj.Attach(targetsAction);
                    effectObj.transform.forward = forward;
                    effectObj.transform.localEulerAngles += eulerAngles;
                    effectObj.SetTimeOut(time);
                    effectObj.Play();
                    if(m_AISkillconfig.UseLaser == 1)
                    {
                        EffectManager.singleton.GetEffect<EffectTimeout>(EffectConst.LASER, (laserEffectObj) =>
                        {
                            Transform target = m_kPlayer.transform.Find("slot_laser");
                            laserEffectObj.Attach(target);
                            laserEffectObj.transform.forward = forward;
                            laserEffectObj.transform.LookAt(new Vector3(targetsAction.x, targetsAction.y, targetsAction.z));
                            laserEffectObj.SetTimeOut(time);
                            laserEffectObj.Play();
                        });
                    }
                    
                });
            };

            for (int i = 0; i < targets.Length; i++)
                doOneAlert(CalcTargetPos(type, heightType, targets[i], dis));
        }
    }

    private Vector3 CalcTargetPos(string type, int heightType, float[] tocTarget, float dis)
    {
        Vector3 calcPos = new Vector3(tocTarget[0], tocTarget[1], tocTarget[2]);
        switch(heightType)
        {
            case GameConst.AISKILL_ALERT_HEIGHT_TYPE_TARGET_GROUND:
                calcPos = SceneManager.GetGroundPoint(calcPos + new Vector3(0, GameConst.AISKILL_ALERT_TEST_GROUND_POINT_FIXED_HEIGHT, 0));
                break;
            case GameConst.AISKILL_ALERT_HEIGHT_TYPE_PLAYER_CENTER:
                calcPos.y = m_kPlayer.centerPos.y;
                break;
            case GameConst.AISKILL_ALERT_HEIGHT_TYPE_PLAYER_POS:
                calcPos.y = m_kPlayer.position.y;
                break;
            default:
                Vector3 fixPos = m_kPlayer.centerPos;
                if(m_kPlayer.centerPos.y < 0.1f)
                {
                    fixPos = new Vector3(m_kPlayer.centerPos.x, 0.3f, m_kPlayer.centerPos.z);
                }
                float y = SceneManager.GetGroundPoint(fixPos).y;
                calcPos.y = y;
                break;
        }

        m_calcTargetsList.Add(calcPos);
        return calcPos;
    }

    private void SkillAttack(float[] param, float duringTime)
    {
        if (null == param || 0 == param.Length)
        {
            //Logger.Error("Action param is null or empty");
            return;
        }

        int weaponId = (int)param[0];
        if (0 == weaponId)
        {
            //Logger.Error("Action weaponID为 0");
            return;
        }

        Action<Vector3> doOneAttack = (targetsAction) =>
        {
            Projectile projectile = ProjectileManager.AttachProjectile(m_kPlayer, weaponId);
            int tempTimer = TimerManager.SetTimeOut(duringTime, () =>
            {
                Action callback = () =>
                    {
                        if (m_released || projectile == null)
                            return;

                        Vector3 firePos = projectile.go.transform.position;
                        Vector3 fireRot = projectile.go.transform.eulerAngles;

                        ProjectileManager.DeactiveItem(projectile);
                        if (!m_kPlayer.isAlive)
                            return;

                        ConfigItemWeaponLine wl = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weaponId);
                        if (wl == null)
                        {
                            Logger.Error("获取武器配置失败，武器ID=" + weaponId);
                            return;
                        }

                        Vector3 vec3ThrowDir = targetsAction - firePos;
                        BulletBase bullet = BulletManager.CreateBullet(BulletBase.TYPE_DEJECTILE, wl, false);
                        bullet.Fire(firePos, fireRot, vec3ThrowDir, wl, m_kPlayer, ProjectileManager.GetNextBulletIndex());
                        projectile.callback = null;
                    };

                if (projectile != null && projectile.go == null)
                {
                    projectile.callback = callback;
                }
                else
                    callback();

            });
            m_attackTimeList.Add(tempTimer);
        };

        for (int i = 0; i < m_calcTargetsList.Count; i++)
            doOneAttack(m_calcTargetsList[i]);
    }

    public void RemoveAll()
    {
        if (m_kPlayer == null)
            return;

        StopScoutSkill();

        //如果有特效配置，则还原状态
        if (m_aniEffectDic.Count > 0)
        {
            foreach (KeyValuePair<string, string> pair in m_aniEffectDic)
                PlayAniEffect(pair.Value, false);
        }
        TimerManager.RemoveTimeOut(RestoreAniEffect);

        foreach (Effect effect in m_listAllEffect)
        {
            if (effect != null)
                effect.Release();
        }

        foreach (int timer in m_attackTimeList)
        {
            if (-1 != timer)
                TimerManager.RemoveTimeOut(timer);
        }

        if (-1 != m_alertTimeout)
            TimerManager.RemoveTimeOut(m_alertTimeout);
        if (-1 != m_loopTimeout)
            TimerManager.RemoveTimeOut(m_loopTimeout);

        m_attackTimeList.Clear();
        isAlertShowing = false;
    }

    public void Stop()
    {
        RemoveAll();
        m_listAllEffect.Clear();
    }

    public void Release()
    {
        Stop();
        m_released = true;
    }

    /// <summary>
    /// 重置动画特效的状态
    /// </summary>
    /// <param name="running"></param>
    private void RestoreAniEffect(object aniName)
    {
        PlayAniEffect((string)aniName, false);
    }

    /// <summary>
    /// 这里检测技能释放时，主角如果在预警范围内发出警告
    /// </summary>
    public void UpdateCheckAlert()
    {
        if (!isAlertShowing)
        {
            if (null != UIManager.GetPanel<PanelBattle>())
                UIManager.GetPanel<PanelBattle>().showAlertWarning(false, m_kPlayer.PlayerId);
            return;
        }

        if (null == m_calcTargetsList || 0 == m_calcTargetsList.Count)
            return;

        if (null == m_AISkillconfig || m_AISkillconfig.ClientTime < 1 )
            return;

        if (null == MainPlayer.singleton)
            return;

        bool isInAlert = false;
        //只检测主角坐标的x和z与目标点的距离是否在半径就OK了。只地面2D维度检测。
        for (int i = 0; i < m_calcTargetsList.Count; i++)
        {
            if (m_AISkillconfig.AlertType == GameConst.AIACTION_ALERT_RECTANGLE) //矩形检测
            {
                if (MathTool.InRectangle(MainPlayer.singleton.position, m_dis, m_radius, m_calcTargetsList[i]))
                {
                    isInAlert = true;
                    break;
                }
               /* if ((m_calcTargetsList[i] - MainPlayer.singleton.position).sqrMagnitude <= m_AISkillconfig.AlertParam[2] * m_AISkillconfig.AlertParam[2])
                {
                    isInAlert = true;
                    break;
                }
                else
                {
                    //if (Vector3.Distance(m_calcTargetsList[i], MainPlayer.singleton.position) < Vector3.Distance(m_calcTargetsList[i], m_kPlayer.position))
                    if ((m_calcTargetsList[i] - MainPlayer.singleton.position).sqrMagnitude <= (m_calcTargetsList[i] - m_kPlayer.position).sqrMagnitude)
                    {
                        isInAlert = true;
                        break;
                    }
                }*/
            }
            else
            {
                if (MathTool.InCircle(MainPlayer.singleton.position, m_radius, m_calcTargetsList[i]))
                {
                    isInAlert = true;
                    break;
                }

                /*if ((m_calcTargetsList[i] - MainPlayer.singleton.position).sqrMagnitude <= m_AISkillconfig.AlertParam[0] * m_AISkillconfig.AlertParam[0])
                {
                    isInAlert = true;
                    break;
                }*/
            }
        }

        var battle = UIManager.GetPanel<PanelBattle>();
        if (battle != null && battle.IsInited()) battle.showAlertWarning(isInAlert, m_kPlayer.PlayerId);
    }


    public void HitBackDiatance(BasePlayer kAttacker, int skillId)
    {
        if (m_kPlayer == null || !(m_kPlayer is MainPlayer) || kAttacker == null)
            return;

        MainPlayer mainPlayer = m_kPlayer as MainPlayer;

        if(mainPlayer.mainplayerController == null)
            return;

        var config = GetSkillEffectConfig(skillId);
        if (config != null && config.HitBackDiatance > 0 && config.HitBackSpeed > 0)
        {
            Vector3 moveDir = m_kPlayer.position - kAttacker.position;
            mainPlayer.mainplayerController.HitBackDistance(config.HitBackDiatance, moveDir, config.HitBackSpeed);
        }
    }

}
