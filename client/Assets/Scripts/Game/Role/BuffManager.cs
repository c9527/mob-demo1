﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffEffect
{
    private string m_name;
    private bool m_bIsRemove;
    private EffectTimeout m_opEffect;
    private UIEffect m_mpEffect;
    private bool m_isCancelHitBack;

   


    public bool isCancelHitBack
    {
        get { return m_isCancelHitBack; }
    }

    public void AttachEffect(int buffId, string effect, bool isCancelHitBack, Transform attachParent, Vector3 attachPos, bool isMainPlayer, float time, Action<object> callBack = null, object param = null)
    {
        m_name = effect + buffId;
        m_bIsRemove = false;
        this.m_isCancelHitBack = isCancelHitBack;
        if(isMainPlayer)
        {
            AttachMpEffect(effect, attachParent, attachPos, time, callBack, param);
        }
        else
        {
            AttachOpEffect(effect, attachParent, attachPos, time, callBack, param);
        }
    }

    private void AttachMpEffect(string effect, Transform attachParent, Vector3 attachPos, float time, Action<object> callBack = null, object param = null)
    {
        m_mpEffect = UIEffect.ShowEffect(UIManager.GetPanel<PanelBattle>().transform, effect, time, callBack, param);
    }

    private void AttachOpEffect(string effect, Transform attachParent, Vector3 attachPos, float time, Action<object> callBack = null, object param = null)
    {
        EffectManager.singleton.GetEffect<EffectTimeout>(effect, (kEffect) =>
        {
            if (m_bIsRemove)
            {
                kEffect.Release();
                return;
            }

            if (kEffect == null)
                return;

            m_opEffect = kEffect;
            kEffect.Attach(attachPos, Vector3.zero, Space.Self, attachParent);
            kEffect.SetTimeOut(time);
            kEffect.SetCompleteCallBack(param, callBack);
            kEffect.Play();
        });
    }

    public void RemoveEffect()
    {
        if (m_opEffect != null)
            m_opEffect.Release();
        if (m_mpEffect != null)
            m_mpEffect.Destroy();
        m_bIsRemove = true;
    }

    public string name
    {
        get { return m_name; }
    }
}

public class BuffData
{
    public int id;
    public int startTime;
    public int endTime;
    public float buffTime;
    public BuffType buffType;
    public int timer;
    public string effectName;
    public BuffEffect effect;
    public bool active;
    public float fov;
    public float scale;
    public float bigHead;
    public float visableDegree;
    public bool isCancelHitBack;
    public int limitState;
}

public enum BuffType
{
    None = 0,
    Effect = 1,
    Stealth = 2,
    Scale = 3,
    BigHead = 4,
}

public class BuffComparer: IComparer
{
    public int Compare(object x, object y)
    {
        return ((p_actor_buff_toc)x).start_time - ((p_actor_buff_toc)y).start_time;
    }
}

public class BuffManager
{
    private BasePlayer m_player;
    private Dictionary<string, BuffEffect> m_dicEffectBuff;
    private bool m_bIsMainPlayer;
    private bool m_bIsFPV;

    private Dictionary<int, BuffData> m_dicBuffData;

    private const int LIMIT_STATE_ATTACK = 0x1;//1：攻击
    private const int LIMIT_STATE_USE_SKILL = 0x2;//2：技能
    private const int LIMIT_STATE_MOVE = 0x4;//4：移动
    private const int LIMIT_STATE_HURT = 0x8;//8：受伤

    public BuffManager(BasePlayer player)
    {
        this.m_player = player;
        m_bIsFPV = player is MainPlayer || player is FPWatchPlayer;
        m_bIsMainPlayer = player is MainPlayer ;

        m_dicEffectBuff = new Dictionary<string, BuffEffect>();
        m_dicBuffData = new Dictionary<int, BuffData>();
    }

    public void RemoveBuff(int id, bool removeData)
    {
        BuffData bi = null;
        if (!m_dicBuffData.TryGetValue(id, out bi))
            return;

        if (removeData)
            bi.active = false;

        if (bi.timer != -1)
        {
            TimerManager.RemoveTimeOut(bi.timer);
            bi.timer = -1;
        }

        if (removeData && m_bIsMainPlayer)
        {
            m_player.isCancelHitBack = ExistCancelHitBackEffect();
            if (bi.fov != -1)
                SetLastFov();
        }

        switch (bi.buffType)
        {
            case BuffType.Effect:
                if (bi.effect != null)
                {
                    m_dicEffectBuff.Remove(bi.effect.name + id);
                }
                break;
            case BuffType.Stealth:
                SetLastVisableDegree();
                break;
            case BuffType.Scale:
                SetLastScale();
                break;
            case BuffType.BigHead:
                SetLastBigHead();
                break;
        }

        if (m_bIsMainPlayer)
        {
            PassiveSkillManager.singleton.RemovePassiveSkill(id);
            PassiveSkillTextManager.singleton.RemovePassiveSkill(id.ToString());

        }

    }

    public void AddBuff(p_actor_buff_toc[] buffList)
    {
        if (buffList == null || buffList.Length == 0)
        {
            RemoveAllBuff();
            return;
        }

        HashSet<int> buffSet = new HashSet<int>();

        Array.Sort(buffList, 0, buffList.Length, new BuffComparer());

        for (int i = 0; i < buffList.Length; ++i )
        {
            buffSet.Add(buffList[i].buff_id);
            AddBuff(buffList[i]);
        }

        List<int> removeBuffList = new List<int>();

        foreach(var k in m_dicBuffData.Keys)
        {
            if (!buffSet.Contains(k))
            {
                RemoveBuff(k, true);
                removeBuffList.Add(k);
            }
        }

        HashSet<string> effectSet = GetBuffEffectSet();
        BuffData buff = null;
        for (int i = 0, imax = removeBuffList.Count; i < imax; ++i)
        {
            if (m_dicBuffData.TryGetValue(removeBuffList[i], out buff))
            {
                switch (buff.buffType)
                {
                    case BuffType.Effect:
                        if (effectSet.Contains(buff.effectName) && buff.effect != null)
                        {
                            buff.effect.RemoveEffect();
                        }
                        break;
                }
            }
        }
    }

    public void AddBuff(p_actor_buff_toc buff)
    {
        if (m_player == null || m_player.gameObject == null)
            return;

        int id = buff.buff_id;

        TDBuffInfo buffConfig = null;
        buffConfig = ConfigManager.GetConfig<ConfigBuff>().GetLine(id);

        if(buffConfig == null)
        {
            Logger.Error("Buff Config id = " + id + " is null!!");
            return;
        }

        BuffType buffType = (BuffType)buffConfig.EffectType;

        if (m_dicBuffData.ContainsKey(id))
        {
            if (m_dicBuffData[id].active && m_dicBuffData[id].startTime == buff.start_time)
                return;
            m_dicBuffData[id].active = true;
            m_dicBuffData[id].startTime = buff.start_time;
            m_dicBuffData[id].endTime = buff.end_time;
            RemoveBuff(id, false);
        }
        else
        {
            BuffData buffData = new BuffData();
            buffData.id = id;
            buffData.buffType = buffType;
            buffData.startTime = buff.start_time;
            buffData.endTime = buff.end_time;
            buffData.timer = -1;
            buffData.effect = null;
            buffData.active = true;
            buffData.scale = -1f;
            buffData.visableDegree = -1f;
            buffData.fov = -1f;
            buffData.bigHead = -1f;
            buffData.effectName = "";
            buffData.isCancelHitBack = false;
            buffData.limitState = buffConfig.LimitState;;
            m_dicBuffData.Add(id, buffData);
        }

        float buffTime = buff.end_time / 1000f - (Time.realtimeSinceStartup - WorldManager.singleton.StartTime);

        if (buffTime <= 0)
            return;

        BuffData bi = m_dicBuffData[id];
        bi.buffTime = buffTime;

        if (m_bIsMainPlayer)
        {
            if (!string.IsNullOrEmpty(buffConfig.BuffIcon))
                PassiveSkillManager.singleton.AddPassiveSkill(buffConfig, bi.buffTime);
            if (!string.IsNullOrEmpty(buffConfig.BuffText))
                PassiveSkillTextManager.singleton.AddPassiveSkill(buffConfig, bi.buffTime);
        }

        string effectArg = m_bIsFPV ? buffConfig.MPEffectArg : buffConfig.OPEffectArg;
        bool isCancelHitBack = buffConfig.IsCancelHitBack != 0;
        if (buffConfig.Fov > 0)
        {
            bi.fov = buffConfig.Fov;
            m_player.playerEffectMgr.SetFov(buffConfig.Fov, true);
        }
            
        switch (buffType)
        {
            case BuffType.Effect:
                bi.effectName = effectArg;
                bi.isCancelHitBack = isCancelHitBack;
                AddEffectBuff(bi);
                break;
            case BuffType.Stealth:
                float visableDegree = float.Parse(effectArg);
                bi.visableDegree = visableDegree;
                AddStealthBuff(bi);
                break;
            case BuffType.Scale:
                float scale = float.Parse(effectArg);
                bi.scale = scale;
                AddScaleBuff(bi);
                break;
            case BuffType.BigHead:
                float bigHead = float.Parse(effectArg);
                bi.bigHead = bigHead;
                AddBigHeadBuff(bi);
                break;
        }
        UpdateMainPlayerStatus();
        HandleTransformBuff(bi);
    }


    private void UpdateMainPlayerStatus()
    {
        if (m_bIsMainPlayer)
        {
            var limitState = 0;
            foreach (var item in m_dicBuffData)
            {
                if (item.Value.active)
                {
                    limitState |= item.Value.limitState;
                }
            }
            var player = m_player as MainPlayer;
            player.ForbidFire = ((limitState & LIMIT_STATE_ATTACK) > 0);
            player.ForbidMove = ((limitState & LIMIT_STATE_MOVE) > 0);
            player.ForbidUseSkill = ((limitState & LIMIT_STATE_USE_SKILL) > 0);
            if ((limitState & LIMIT_STATE_HURT) > 0)
            {
                if (UIManager.IsOpen<PanelBattle>())
                {
                    if (prop == null)
                    {
                        Vector2 pos = new Vector2(0, -100);
                        prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, textProp = new TextProp() { fontSize = 18, rect = new RectTransformProp() { size = new Vector2(200, 30) } } };
                    }
                    UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "无敌时间", player.GodTime, prop, "GodTime");
                }
            }
            
        }

    }
    private ProgressBarProp prop;
    private void HandleTransformBuff(BuffData buffData)
    {
        if (m_bIsMainPlayer && buffData.id == GameConst.TRANSFORM_BUFF_ID && buffData.active)
        {
            if (UIManager.IsOpen<PanelBattle>())
            {
                Vector2 pos = new Vector2(0, -115);
                prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = new Vector2(0, -85), textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } } };
                UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "变身时间", buffData.buffTime, prop, "Transform");
            }
        }
    }

    private void AddEffectBuff(BuffData bi)
    {
        string effect = bi.effectName;
        float time = bi.buffTime;
        bool isCancelHitBack = bi.isCancelHitBack;

        Logger.Log("AddEffectBuff effect:" + effect + "  time:" + time);
        if (isCancelHitBack)
            m_player.isCancelHitBack = isCancelHitBack;

        if (!string.IsNullOrEmpty(effect))
        {
            BuffEffect effectItem = null;
            if (!m_dicEffectBuff.TryGetValue(effect + bi.id, out effectItem))
            {
                effectItem = new BuffEffect();
                m_dicEffectBuff.Add(effect + bi.id, effectItem);
            }

            Transform attachParent = null;
            Vector3 attachPos = Vector3.zero;
            if (m_bIsFPV)
            {
                attachParent = UIManager.GetPanel<PanelBattle>().transform;
                attachPos = Vector3.zero;
            }
            else
            {
                attachParent = m_player.transform;
                attachPos = new Vector3(0, m_player.height * 0.5f, 0);
            }

            int id = bi.id;
            effectItem.AttachEffect(id, effect, isCancelHitBack, attachParent, attachPos, m_bIsFPV, time,
               (obj) =>
               {
                   int _id = (int)obj;
                   if (!m_dicBuffData.ContainsKey(_id))
                       return;
                   m_dicEffectBuff.Remove(effect);
                   m_dicBuffData[_id].active = false;
                   if (m_bIsMainPlayer)
                   {

                       if (m_dicBuffData[_id].fov != -1)
                       {
                           SetLastFov();
                       }
                       m_player.isCancelHitBack = ExistCancelHitBackEffect();
                       UpdateMainPlayerStatus();
                   }
               }, id);
            bi.effect = effectItem;
        }
        else
        {
            bi.timer = TimerManager.SetTimeOut(time,
                () =>
                {
                    bi.timer = -1;
                    bi.active = false;
                    if (m_bIsMainPlayer && bi.fov != -1)
                    {
                        SetLastFov();
                    }
                    UpdateMainPlayerStatus();
                }
            );
        }
    }

    private void AddStealthBuff(BuffData bi)
    {
        if (m_player == null || m_player.gameObject == null)
            return;

        float visableDegree = bi.visableDegree;
        float time = bi.buffTime;

        Logger.Log("AddStealthBuff visableDegree:" + visableDegree + "  time:" + time);
        m_player.SetVisableDegree(visableDegree,GameSetting.PLAYER_SET_ALPHA_TIME);
        bi.timer = TimerManager.SetTimeOut(time,
            () =>
            {
                bi.timer = -1;
                bi.active = false;
                if (bi.fov != -1 && m_bIsMainPlayer)
                    MainPlayer.singleton.playerEffectMgr.SetFov(MainPlayer.singleton.configData.RealFov, true);
                SetLastVisableDegree();

                UpdateMainPlayerStatus();
            }
        );
    }

    private void AddScaleBuff(BuffData bi)
    {
        if (m_player == null || m_player.gameObject == null)
            return;

        float scale = bi.scale;
        float time = bi.buffTime;

        Logger.Log("AddScaleBuff scale:" + scale + "  time:" + time);
        m_player.playerEffectMgr.SetScale(scale, true);
        bi.timer = TimerManager.SetTimeOut(time,
            () =>
            {
                bi.timer = -1;
                bi.active = false;
                if (bi.fov != -1 && m_bIsMainPlayer)
                    MainPlayer.singleton.playerEffectMgr.SetFov(MainPlayer.singleton.configData.RealFov, true);
                SetLastScale();
            }
        );
    }

    private void AddBigHeadBuff(BuffData bi)
    {
        if (m_player == null || m_player.gameObject == null)
            return;

        float bigHead = bi.bigHead;
        float time = bi.buffTime;

        Logger.Log("AddBigHeadBuff bigHead:" + bigHead + "  time:" + time);
        m_player.playerEffectMgr.SetBigHead(bigHead);
        bi.timer = TimerManager.SetTimeOut(time,
            () =>
            {
                bi.timer = -1;
                bi.active = false;
                if (bi.fov != -1 && m_bIsMainPlayer)
                    MainPlayer.singleton.playerEffectMgr.SetFov(MainPlayer.singleton.configData.RealFov, true);
                SetLastBigHead();
            }
        );
    }

    private bool ExistCancelHitBackEffect()
    {
        foreach (var effectItem in m_dicEffectBuff.Values)
        {
            if (effectItem.isCancelHitBack)
                return true;
        }
        return false;
    }

    private HashSet<string> GetBuffEffectSet()
    {
        HashSet<string> result = new HashSet<string>();
        foreach (var buff in m_dicBuffData.Values)
        {
            if (buff.active)
            {
                switch (buff.buffType)
                {
                    case BuffType.Effect:
                        result.Add(buff.effectName);
                        break;
                    //case BuffType.Stealth:
                    //    result.Add(BuffType.Stealth.ToString());
                    //    break;
                    //case BuffType.Scale:
                    //    result.Add(BuffType.Scale.ToString());
                    //    break;
                }
            }
        }
        return result;
    }

    private void SetLastFov()
    {
        if (m_player == null)
            return;

        float lastVisableDegree = GetLastFov();
        if (lastVisableDegree != -1)
            MainPlayer.singleton.playerEffectMgr.SetFov(lastVisableDegree, true);
        else
            MainPlayer.singleton.playerEffectMgr.SetFov(MainPlayer.singleton.configData.RealFov, true);
    }

    private void SetLastVisableDegree()
    {
        if (m_player == null)
            return;

        float lastVisableDegree = GetLastVisableDegree();

        if (lastVisableDegree != -1)
            m_player.SetVisableDegree(lastVisableDegree, GameSetting.PLAYER_SET_ALPHA_TIME);
        else
            m_player.SetVisableDegree(1f, GameSetting.PLAYER_SET_ALPHA_TIME);
    }

    private void SetLastScale()
    {
        if (m_player == null)
            return;

        float lastScale = GetLastScale();

        if (lastScale != -1)
            m_player.playerEffectMgr.SetScale(lastScale, true);
        else
            m_player.playerEffectMgr.SetScale(m_player.lastScale, true);
    }

    private void SetLastBigHead()
    {
        if (m_player == null)
            return;

        float lastBigHeadScale = GetLastBigHead();

        if (lastBigHeadScale != -1)
            m_player.playerEffectMgr.SetBigHead(lastBigHeadScale);
        else
            m_player.playerEffectMgr.SetBigHead(m_player.lastBigHeadScale);
    }

    private float GetLastFov()
    {
        float lastFov = -1;
        float lastStartTime = -1;
        foreach (var buff in m_dicBuffData.Values)
        {
            if (buff.active && buff.fov != -1 && buff.startTime > lastStartTime)
            {
                lastStartTime = buff.startTime;
                lastFov = buff.fov;
            }
        }
        return lastFov;
    }

    private float GetLastScale()
    {
        float lastScale = -1;
        float lastStartTime = -1;
        foreach (var buff in m_dicBuffData.Values)
        {
            if (buff.active && buff.scale != -1 && buff.startTime > lastStartTime)
            {
                lastStartTime = buff.startTime;
                lastScale = buff.scale;
            }
        }
        return lastScale;
    }

    private float GetLastVisableDegree()
    {
        float lastVisableDegree = -1;
        float lastStartTime = -1;
        foreach (var buff in m_dicBuffData.Values)
        {
            if (buff.active && buff.visableDegree != -1 && buff.startTime > lastStartTime)
            {
                lastStartTime = buff.startTime;
                lastVisableDegree = buff.visableDegree;
            }
        }
        return lastVisableDegree;
    }
    
    private float GetLastBigHead()
    {
        float lastBigHead = -1;
        float lastStartTime = -1;
        foreach (var buff in m_dicBuffData.Values)
        {
            if (buff.active && buff.bigHead != -1 && buff.startTime > lastStartTime)
            {
                lastStartTime = buff.startTime;
                lastBigHead = buff.bigHead;
            }
        }
        return lastBigHead;
    }

    public void RemoveAllBuff()
    {
        //清除特效
        foreach (var effectItem in m_dicEffectBuff.Values)
        {
            effectItem.RemoveEffect();
        }
        m_dicEffectBuff.Clear();

        m_player.isCancelHitBack = false;

        bool existStealthBuff = false;
        bool existScaleBuff = false;
        bool existBigHeadBuff = false;

        foreach(var buffInfo in m_dicBuffData.Values)
        {
            switch(buffInfo.buffType)
            {
                case BuffType.Stealth:
                    existStealthBuff = true;
                    break;
                case BuffType.Scale:
                    existScaleBuff = true;
                    break;
                case BuffType.BigHead:
                    existBigHeadBuff = true;
                    break;
            }

            if(buffInfo.timer != -1)
                TimerManager.RemoveTimeOut(buffInfo.timer);
        }
        m_dicBuffData.Clear();

        if (existStealthBuff)
            m_player.SetVisableDegree(1f, GameSetting.PLAYER_SET_ALPHA_TIME);
        if (existScaleBuff)
            m_player.SetScale(m_player.lastScale);
        if (existBigHeadBuff)
            m_player.SetHeadScale(m_player.lastBigHeadScale);

        if (m_bIsMainPlayer)
        {
            if (MainPlayer.singleton.configData != null)
                MainPlayer.singleton.playerEffectMgr.SetFov(MainPlayer.singleton.configData.RealFov, false);
            if (PassiveSkillManager.singleton != null)
                PassiveSkillManager.singleton.RemoveAllPassiveSkill();
            if(PassiveSkillTextManager.singleton != null)
                PassiveSkillTextManager.singleton.RemoveAllPassiveSkill();

            UpdateMainPlayerStatus();
        }
    }
}