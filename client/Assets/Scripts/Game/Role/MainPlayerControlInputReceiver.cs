﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MainPlayerControlInputReceiver : MonoBehaviour
{
    public static float MOUSE_MOVE_SENSITIVITY = 5.0f;
    public static float MOUSE_MOVE_SENSITIVITY_SNIPER = 1.0f;
    public const float MOUSE_MOVE_SENSITIVITY_INIT = 2.5f;
    public const float MOUSE_MOVE_SENSITIVITY_STEP = 7.5f; //PC版将鼠标的灵敏度范围调大
    public const float MOUSE_MOVE_SENSITIVITY_SNIPER_STEP = 2.0f;
    public const float MOUSE_MOVE_SENSITIVITY_SNIPER_INIT = 0.75f;

    public KeyCode[] KEYPAD = new KeyCode[]
    {
         KeyCode.Keypad0,
         KeyCode.Keypad1,
         KeyCode.Keypad2,
         KeyCode.Keypad3,
         KeyCode.Keypad4,
         KeyCode.Keypad5,
         KeyCode.Keypad6,
         KeyCode.Keypad7,
         KeyCode.Keypad8,
         KeyCode.Keypad9,
    };

    public KeyCode[] ALPHA = new KeyCode[]
    {
        KeyCode.Alpha0,
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9,
    };

    static public MainPlayerControlInputReceiver singleton;

    private MainPlayer m_kMP;

    //
    private bool m_bLastMoveFlg;
    private bool m_bMoveForward;

    public bool moveForward
    {
        get { return m_bMoveForward; }
        set { m_bMoveForward = value; }
    }
    private bool m_bMoveBack;

    public bool moveBack
    {
        get { return m_bMoveBack; }
        set { m_bMoveBack = value; }
    }
    private bool m_bMoveLeft;

    public bool moveLeft
    {
        get { return m_bMoveLeft; }
        set { m_bMoveLeft = value; }
    }
    private bool m_bMoveRight;

    public bool moveRight
    {
        get { return m_bMoveRight; }
        set { m_bMoveRight = value; }
    }

    public static List<String> KeyList;

    void Start()
    {
        KeyList = new List<string>();
        ConfigBattlePcKeySetting keySetting = ConfigManager.GetConfig<ConfigBattlePcKeySetting>();
        if (keySetting != null && keySetting.m_dataArr != null)
            foreach (var item in keySetting.m_dataArr)
            {
                //这三个键的操作特殊处理
                if (item.Key != GameEvent.INPUT_KEY_TAB_DOWN && item.Key != GameEvent.INPUT_KEY_SHIFT && item.Key != GameEvent.INPUT_KEY_V_DOWN && item.Key != GameEvent.INPUT_KEY_ALT_DOWN)
                    KeyList.Add(item.Key);
            }
        singleton = this;
        MOUSE_MOVE_SENSITIVITY = MOUSE_MOVE_SENSITIVITY_INIT + (MOUSE_MOVE_SENSITIVITY_STEP * (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AIM) - 0.2f));
        MOUSE_MOVE_SENSITIVITY_SNIPER = MOUSE_MOVE_SENSITIVITY_SNIPER_INIT + MOUSE_MOVE_SENSITIVITY_SNIPER_STEP * (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AIMACC) - 0.3f);
        GlobalBattleParams.singleton.NotifyEvent += OnPlayerChangeBattleParamSetting;


    }

    private void onPcKeyChangeHandler()
    {

    }

    void OnPlayerChangeBattleParamSetting(string paramName, float newValue)
    {
        if (paramName == GlobalBattleParams.KEY_AIM)
            MOUSE_MOVE_SENSITIVITY = MOUSE_MOVE_SENSITIVITY_INIT + (MOUSE_MOVE_SENSITIVITY_STEP * (newValue - 0.2f));
        else if (paramName == GlobalBattleParams.KEY_AIMACC)
            MOUSE_MOVE_SENSITIVITY_SNIPER = MOUSE_MOVE_SENSITIVITY_SNIPER_INIT + MOUSE_MOVE_SENSITIVITY_SNIPER_STEP * (newValue - 0.3f);
    }

    void OnDestroy()
    {
        GlobalBattleParams.singleton.NotifyEvent -= OnPlayerChangeBattleParamSetting;
    }

    public Vector3 GetMoveDir()
    {
        Vector3 vec3Dir = Vector3.zero;

        vec3Dir += m_bMoveForward ? Vector3.forward : m_bMoveBack ? Vector3.back : Vector3.zero;
        vec3Dir += m_bMoveLeft ? Vector3.left : m_bMoveRight ? Vector3.right : Vector3.zero;

        return vec3Dir;
    }

    void Update()
    {
        m_kMP = MainPlayer.singleton;

#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
        //战斗中常用功能快捷键
        /* if (Input.GetKeyDown(KeyCode.V))
         {
             GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_DOWN);
         }

         if (Input.GetKeyUp(KeyCode.V))
         {
             GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_UP);
         }*/

        if (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_ENTER_UP);

        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_ESC_DOWN);
        }

        string ckstr = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_TAB_DOWN);
        if (ckstr != "")
        {
            KeyCode tabKey = (KeyCode)Enum.Parse(typeof(KeyCode), GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_TAB_DOWN));
            if (Input.GetKeyDown(tabKey))
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_KEY_TAB_DOWN, true);
            }
            if (Input.GetKeyUp(tabKey))
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_KEY_TAB_DOWN, false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_SPACE_DOWN, true);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_SPACE_UP);
        }
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_BACKQUOTE_DOWN, true);
        }
        if (Input.GetKeyDown(KeyCode.F8))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_F8_DOWN);
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_BATTLESPECTATOR_DOWN, true);
        }
        if (Input.GetKeyUp(KeyCode.F11))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_BATTLESPECTATOR_DOWN, false);
        }
        
#endif
        if ((m_kMP == null || m_kMP.gameObject == null) && (FreeViewPlayer.singleton == null || !FreeViewPlayer.singleton.isRuning))
        {
            //观战模式下特殊处理
            if (Input.GetMouseButtonDown(0))
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_MOUSELEFT_ACTION);
            }
            if (Input.GetMouseButtonDown(1))
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_MOUSERIGHT_ACTION, true);
            }
            //观战处理
            if (Driver.singleton != null && Driver.singleton.controlMode == Driver.CONTROL_MODE_INPUT)
            {
                if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
                {
                    //第一人称观看不处理
                }
                else if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
                {
                    float xx = Input.GetAxis("Mouse X");
                    float yy = Input.GetAxis("Mouse Y");

                    float xDelta = xx * MOUSE_MOVE_SENSITIVITY;
                    float yDelta = yy * MOUSE_MOVE_SENSITIVITY;
                    FreeViewPlayer.singleton.playerController.LookRotate(xDelta, yDelta);
                }
                else
                {
                    if (SceneCameraController.singleton != null)
                    {
                        float xx = Input.GetAxis("Mouse X");
                        float yy = Input.GetAxis("Mouse Y");
                        float xDelta = xx * MOUSE_MOVE_SENSITIVITY;
                        float yDelta = yy * MOUSE_MOVE_SENSITIVITY;

                        SceneCameraController.singleton.OnDrag(xDelta, yDelta);
                    }
                }
            }
            return;
        }
        bool bMoveFlg = false;
        if (Input.anyKey)
        {
            if (MainPlayer.singleton != null)
                MainPlayer.singleton.SetLastAction(ActionType.INPUT_KEY);

            string keycode = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_W_DOWN);
            if (keycode != "")
                m_bMoveForward = Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), keycode)) || Input.GetKey(KeyCode.UpArrow);
            else
                m_bMoveForward = false;
            keycode = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_S_DOWN);
            if (keycode != "")
                m_bMoveBack = Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), keycode)) || Input.GetKey(KeyCode.DownArrow);
            else
                m_bMoveBack = false;
            keycode = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_A_DOWN);
            if (keycode != "")
                m_bMoveLeft = Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), keycode)) || Input.GetKey(KeyCode.LeftArrow);
            else
                m_bMoveLeft = false;

            keycode = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_D_DOWN);
            if (keycode != "")
                m_bMoveRight = Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), keycode)) || Input.GetKey(KeyCode.RightArrow);
            else
                m_bMoveRight = false;
            bMoveFlg = m_bMoveForward || m_bMoveBack || m_bMoveLeft || m_bMoveRight;
        }

        if (bMoveFlg != m_bLastMoveFlg)
        {
            m_bLastMoveFlg = bMoveFlg;
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_MOVE, m_bLastMoveFlg);

            /* if (m_bLastMoveFlg)
                 GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, true);
             else
                 GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, false);*/
        }
        //fjs PC版接入
#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
        ckstr = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_SHIFT);
        if (ckstr != "")
        {
            KeyCode shiftKey = (KeyCode)Enum.Parse(typeof(KeyCode), ckstr);
            if (Input.GetKeyDown(shiftKey))
            {
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_SHIFT, true);
            }
            else if (Input.GetKeyUp(shiftKey))
            {
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_SHIFT, false);
            }
        }

        ckstr = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_ALT_DOWN);
        if (ckstr != "")
        {
            KeyCode shiftKey = (KeyCode)Enum.Parse(typeof(KeyCode), ckstr);
            if (Input.GetKeyDown(shiftKey))
            {
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_MOVE_SLOW, true);
            }
            else if (Input.GetKeyUp(shiftKey))
            {
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_MOVE_SLOW, false);
            }
        }


        ckstr = GlobalBattleParams.singleton.GetPcKeyParam(GameEvent.INPUT_KEY_V_DOWN);
        if (ckstr != "")
        {
            KeyCode kcv = (KeyCode)Enum.Parse(typeof(KeyCode), ckstr);

            if (Input.GetKeyDown(kcv))
            {
                // GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_DOWN);
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_V_DOWN, true);
            }
            else if (Input.GetKeyUp(kcv))
            {
                // GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_UP);
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_V_DOWN, false);
            }
        }

        /*if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            // GameDispatcher.Dispatch(GameEvent.INPUT_CROUCH);
            GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_SHIFT, true);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            GameDispatcher.Dispatch<bool>(GameEvent.INPUT_KEY_SHIFT, false);
        }*/
        //跟进自定义按键 派发对应处理事件
        if (Input.anyKeyDown || Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            System.Reflection.FieldInfo info;
            string keycode;
            KeyCode kc;
            foreach (string str in KeyList)
            {
                keycode = GlobalBattleParams.singleton.GetPcKeyParam(str);
                if (keycode != "")
                {
                    kc = (KeyCode)Enum.Parse(typeof(KeyCode), keycode);
                    if (kc != null)
                    {
                        if (kc == KeyCode.Mouse2)
                        {
                            if (Input.GetAxis("Mouse ScrollWheel") != 0)
                            {
                                info = typeof(GameEvent).GetField(str);
                                if (info != null)
                                    GameDispatcher.Dispatch(info.GetValue(null).ToString());
                            }
                        }
                        else if (Input.GetKeyDown(kc))
                        {
                            info = typeof(GameEvent).GetField(str);
                            if (info != null)
                                GameDispatcher.Dispatch(info.GetValue(null).ToString());
                        }
                    }
                }
            }
        }


        //通用快捷键

        if (Input.GetKeyDown(KeyCode.LeftParen))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_LEFTPAREN_DOWN);
        }

        if (Input.GetKeyDown(KeyCode.RightParen))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_RIGHTPAREN_DOWN);
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_Y_DOWN);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_N_DOWN);
        }

        /* if (Input.GetKeyDown(KeyCode.LeftAlt))
         {
             Screen.lockCursor = !Screen.lockCursor;
             //GameDispatcher.Dispatch(GameEvent.INPUT_KEY_ALT_DOWN);
         }*/

        for (int i = 0; i <= 9; i++)
        {
            if (Input.GetKeyDown(KEYPAD[i]))
            {
                GameDispatcher.Dispatch<int>(GameEvent.INPUT_KEY_PAD_NUM_DOWN, i);
            }

            if (i > 0 && i <= 8)
            {
                if (Input.GetKeyDown(ALPHA[i]))
                {
                    GameDispatcher.Dispatch<int>(GameEvent.INPUT_KEY_NUM_DOWN, i);
                    // GameDispatcher.Dispatch("INPUT_KEY_" + i + "_DOWN");
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Equals) || Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_PLUS_DOWN);
            // System.Reflection.FieldInfo info = typeof(GameEvent).GetField("INPUT_KEY_1_DOWN");
            //Debug.LogError(info.GetValue(null)+info.Name);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_UPARROW_UP);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_DOWNARROW_UP);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_LEFTARROW_UP);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_KEY_RIGHTARROW_UP);
        }
#endif
        //操作体验优化
        if (Input.GetKeyDown(KeyCode.F1))
            Screen.lockCursor = !Screen.lockCursor;
        if (!Screen.lockCursor)
        {
            if (!Driver.isMobilePlatform)
            {
                if (UIManager.GetPanel<PanelBattle>() != null
                    && UIManager.GetPanel<PanelBattle>().FightDirectionPanel  != null && 
                    UIManager.GetPanel<PanelBattle>().FightDirectionPanel.activeSelf == true)
                {
                    PanelBattleCursorManager.lockCursor(false);
                    return;
                }
                if (Input.GetKeyDown(KeyCode.Mouse0))
                    PanelBattleCursorManager.lockCursor(true);
            }
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            PanelBattle.IsShootDown = true;
            GameDispatcher.Dispatch(GameEvent.INPUT_MOUSELEFT_ACTION);
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, true);
        }

        else if (Input.GetMouseButtonUp(0))
        {
            PanelBattle.IsShootDown = false;
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, false);
        }
        if (Input.GetMouseButtonDown(1))
        {
            //GameDispatcher.Dispatch(GameEvent.INPUT_MOUSERIGHT_ACTION);
            GameDispatcher.Dispatch(GameEvent.INPUT_MOUSERIGHT_ACTION, true);
        }

        else if (Input.GetMouseButtonUp(1))
            GameDispatcher.Dispatch(GameEvent.INPUT_MOUSERIGHT_ACTION, false);

        if (Driver.singleton != null && Driver.singleton.controlMode == Driver.CONTROL_MODE_INPUT)
        {
            /*if (MainPlayer.singleton.ForbidLookAround)
                return;
             float xDelta = Input.GetAxis("Mouse X") * MOUSE_MOVE_SENSITIVITY;
                float yDelta = Input.GetAxis("Mouse Y") * MOUSE_MOVE_SENSITIVITY;
                m_kMP.mainplayerController.LookRotate(xDelta, yDelta);
             */

            if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
            {
                //第一人称观看不处理
            }
            else if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                float xx = Input.GetAxis("Mouse X");
                float yy = Input.GetAxis("Mouse Y");

                float xDelta = xx * MOUSE_MOVE_SENSITIVITY;
                float yDelta = yy * MOUSE_MOVE_SENSITIVITY;
                FreeViewPlayer.singleton.playerController.LookRotate(xDelta, yDelta);
            }
            else
            {
                lookOtherPersonDeal();
            }

            //m_kMP.LookRotate(xDelta, yDelta);
            //PanelOperation panelOp = UIManager.GetPanel<PanelOperation>();
            //if(panelOp != null)
            //{
            //    panelOp.RotateBeHit(xDelta);
            //}
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void lookOtherPersonDeal()
    {
        float xx = Input.GetAxis("Mouse X");
        float yy = Input.GetAxis("Mouse Y");

        if (xx != 0 || yy != 0)
        {
            MainPlayer.singleton.SetLastAction(ActionType.MOVE_MOUSE);
        }

        float xDelta = xx * MOUSE_MOVE_SENSITIVITY;
        float yDelta = yy * MOUSE_MOVE_SENSITIVITY;
        if (MainPlayer.singleton.sceneCameraSlot.childCount > 0)
        {
            //看自己
            if (MainPlayer.singleton.ForbidLookAround)
            {
                SceneCameraController.singleton.OnDrag(xDelta, yDelta);
                return;
            }
            if (MainPlayer.singleton.curWeapon is WeaponGun && MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SNIPER_GUN && ((WeaponGun)MainPlayer.singleton.curWeapon).isZoom)
            {
                xDelta = xx * MOUSE_MOVE_SENSITIVITY_SNIPER;
                yDelta = yy * MOUSE_MOVE_SENSITIVITY_SNIPER;
            }
            m_kMP.mainplayerController.LookRotate(xDelta, yDelta);
        }
        else
        {
            SceneCameraController.singleton.OnDrag(xDelta, yDelta);
        }
    }

}
