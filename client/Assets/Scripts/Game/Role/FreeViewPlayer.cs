﻿using System;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 自由视角玩家
/// </summary>
public class FreeViewPlayer
{
    public static FreeViewPlayer singleton;
    private GameObject m_go;
    private CharacterController m_characterCtrler;
    private FreeViewPlayerController m_playerController;
    private Transform m_trans;
    private Transform m_cameraSlot;
    private Transform m_center;
    private bool m_isRuning;

    public FreeViewPlayer()
    {
        singleton = this;
    }

    public void LoadRes()
    {
        if(m_go == null)
        {
            m_go = (new GameObject("FreeViewPlayer"));
            m_trans = m_go.transform;
            m_cameraSlot = (new GameObject("CameraSlot")).transform;
            m_center = (new GameObject("Center")).transform;
            m_characterCtrler = m_go.AddMissingComponent<CharacterController>();
            m_characterCtrler.height = 0.1f;
            m_characterCtrler.radius = 0.1f;
            m_playerController = m_go.AddMissingComponent<FreeViewPlayerController>();
            m_playerController.BindPlayer(this);
            m_cameraSlot.SetParent(m_trans);
            m_cameraSlot.ResetLocal();
            m_center.SetParent(m_trans);
            m_center.ResetLocal();
            m_go.layer = GameSetting.LAYER_VALUE_FREEVIEW_PLAYER;
        }
        else
        {
            m_cameraSlot.ResetLocal();
        }
    }

    public void Run()
    {
        m_isRuning = true;
        LoadRes();
        AttachCamera();
        m_playerController.Run();
    }

    private void AttachCamera()
    {
        m_trans.position = SceneCamera.singleton.position;

        m_cameraSlot.localEulerAngles = new Vector3(SceneCamera.singleton.eulerAngles.x, 0, 0);
        m_trans.localEulerAngles = new Vector3(0, SceneCamera.singleton.eulerAngles.y, 0);

        SceneCamera.singleton.SetParent(m_cameraSlot, true);
        //SceneCamera.singleton.ResetLocalTransform();
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, true);
    }

    public void Stop()
    {
        m_isRuning = false;
        m_playerController.Stop();
    }

    public void Move(Vector3 vec3Move)
    {
        m_characterCtrler.Move(vec3Move);
    }

    public void TouchLookRotate(float pitch, float yaw)
    {
        var pitchAngle = m_cameraSlot.localEulerAngles;
        pitchAngle += new Vector3(pitch, 0);
        if (pitchAngle.x > 180)
            pitchAngle.x -= 360;
        else if (pitchAngle.x < -180)
            pitchAngle.x += 360;
        pitchAngle.x = Mathf.Clamp(pitchAngle.x, -90f, 90f);
        m_cameraSlot.localEulerAngles = pitchAngle;

        m_trans.localEulerAngles += new Vector3(0, yaw);
    }

    public void KeyboardLookRotate(float xDelta, float yDelta)
    {
        if (xDelta == 0 && yDelta == 0)
            return;

        Vector3 vec3Euler = m_cameraSlot.localEulerAngles;
        float x = vec3Euler.x - yDelta;
        if (x > 180)
            x -= 360;
        else if (x < -180)
            x += 360;
        if (x > 90)
            x = 90;
        else if (x < -90)
            x = -90;

        m_cameraSlot.localEulerAngles = new Vector3(x, 0, 0);
        m_trans.Rotate(Vector3.up, xDelta);

        //SendPos();
    }

    #region Get Set

    public bool isRuning
    {
        get { return m_isRuning; }
    }

    public Transform cameraSlot
    {
        get { return m_cameraSlot; }
        set { m_cameraSlot = value; }
    }

    public Transform center
    {
        get { return m_center; }
        set { m_center = value; }
    }

    public Transform transform
    {
        get { return m_trans; }
        set { m_trans = value; }
    }

    public GameObject gameObject
    {
        get { return m_go; }
        set { m_go = value; }
    }

    public FreeViewPlayerController playerController
    {
        get { return m_playerController; }
        set { m_playerController = value; }
    }
    #endregion
}