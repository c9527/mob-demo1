﻿/// <summary>
/// 用来封装玩家战斗数据类，变量字段什么的，在这里扩展，
/// 从面避免在BasePlayer中扩展太多Get Set方法
/// </summary>
/// 

using System.Collections.Generic;
using UnityEngine;
using System;



public class PlayerServerData
{
    public const string MSG_CROUCH = "crouch";
    public const string MSG_STAND = "stand";
    public const string MSG_JUMP = "jump";
    public const string MSG_RELOAD = "reload";
    public const string MSG_SWITCH = "switch";
    public const string MSG_FIRE = "fire";

    private BasePlayer m_player;

    private p_fight_role m_roleData;
    internal p_fight_prop[] m_actorProps;
    //private p_fight_prop m_actorProp;
    private p_actor_state_toc m_actorState;

    private float[] m_arrPos;
    private Vector3 m_serverPos;
    private Vector3 m_serverRot;

    private toc_fight_equip_info m_kEquipData;
    private Dictionary<int, toc_fight_equip_info.EquipInfo> m_dicEquipInfo;
    private List<int> m_listWeapon;
    private int m_grenadeID;
    private int m_flashBombID;
    private int m_itemRoleID;
    private List<int> m_listWidget;
    
    private int m_kAtkType;

    private Action m_funServerPosChanged;
    //private CodeUtil m_codeUtil;
    private ConfigItemWeaponLine m_testWL;


 #region Static Function

    static public Vector3 GetPos(float[] serverPos)
    {
        Vector3 vec3Pos = Vector3.zero;

        if (serverPos != null && serverPos.Length >= 3)
            vec3Pos = new Vector3(serverPos[0], serverPos[1], serverPos[2]);

        return vec3Pos;
    }

    static public Vector3 GetRot(float[] serverPos)
    {
        Vector3 vec3Rot = Vector3.zero;

        if (serverPos != null && serverPos.Length >= 6)
            vec3Rot = new Vector3(0f, serverPos[4], 0f);

        return vec3Rot;
    }

    static public void GetPos(float[] serverPos, ref Vector3 pos)
    {
        if (serverPos != null && serverPos.Length >= 3)
            pos.Set(serverPos[0], serverPos[1], serverPos[2]);
    }

    static public void GetRot(float[] serverPos, ref Vector3 rot)
    {
        if (serverPos != null && serverPos.Length >= 6)
            rot.Set(0f, serverPos[4], 0f);
    }
#endregion

    public PlayerServerData(BasePlayer player)
    {
        //m_codeUtil = new CodeUtil();

        m_player = player;
        m_listWeapon = new List<int>();
        m_listWidget = new List<int>();
        m_dicEquipInfo = new Dictionary<int, toc_fight_equip_info.EquipInfo>();
        m_actorProps = new p_fight_prop[Ref_WeaponAtkType.MAX_COUNT + 1];
        //m_roleData = new CDict();
        //m_actorProp = new CDict();
    }

    public void UpdateTestWeaponConfig(ConfigItemWeaponLine config)
    {
        m_testWL = config;
    }

    //public void ClearData()
    //{
    //    // 这里只清除装备数据，其它数据死亡后可能会用到
    //    m_kEquipData = null;
    //    m_listWeapon.Clear();
    //    m_dicEquipInfo.Clear();
    //}

    public void SetPosChangedCallBack(Action callback)
    {
        m_funServerPosChanged = callback;
    }

#region 角色数据
    internal p_fight_role roleData
    {
        get
        {
            return m_roleData;
        }
        set 
        {
            m_roleData = value;
            SetServerPos(m_roleData.pos, false);
        }
    }

    public int id
    {
        get { return m_roleData.id; }
    }

    public long pid
    {
        get { return m_roleData.pid; }
    }

    public int life
    {
        get { return m_roleData.life; }
        set {
            m_roleData.life = value;         
        } //if (value == 0 && FightInTurnBehavior.singleton != null && !FightInTurnBehavior.singleton.isSelfFighter) Logger.Error(m_player.PlayerName + ", curhp: " + value); }
    }

    public int last_armor
    {
        get { return m_roleData.last_armor; }
        set { m_roleData.last_armor = value; }
    }

    public int last_damage_time
    {
        get { return m_roleData.last_damage_time; }
        set { m_roleData.last_damage_time = value; }
    }

    public int last_act_time
    {
        get { return m_roleData.last_act_time; }
        set { m_roleData.last_act_time = value; }
    }

    public int state
    {
        get { return m_roleData.state; }
        set { m_roleData.state = value; }
    }

    public bool alive
    {
        get { return state == GameConst.PLAYER_STATUS_ALIVE; }
    }

    public bool active
    {
        get { return state > 0; }
    }

    public int camp
    {
        get { return m_roleData.camp; }
        set { m_roleData.camp = value; }
    }

    public string name
    {
        get { return PlayerSystem.GetRoleNameFull(m_roleData.name, m_roleData.pid); }
        set { m_roleData.name = value; }
    }

    public int immune
    {
        get { return m_roleData.immune; }
        set { m_roleData.immune = value; }
    }

    public int actor_type
    {
        get { return m_roleData.actor_type; }
        set { m_roleData.actor_type = value ; }
    }

    public int icon
    {
        get {return m_roleData.icon;}
    }
    
#endregion

#region 角色状态数据

    internal p_actor_state_toc actorState
    {
        get { return m_actorState; }
        set { m_actorState = value; }
    }

    public int fight_level
    {
        get { return m_actorState!=null ? m_actorState.fight_level : 0; }
        set { if (m_actorState != null) m_actorState.fight_level = value; }
    }

    internal p_actor_buff_toc[] buff_list
    {
        get { return m_actorState != null ? m_actorState.buff_list : null; }
    }

#endregion

#region 属性数据
    /// <summary>
    /// 角色创建时调用，后续属性变化不会使用
    /// </summary>
    internal p_fight_prop actorprop
    {
        set
        {
            var actorProp = value;

            //m_codeUtil.CodeValue(ref actorProp.Weight);
            //m_codeUtil.CodeValue(ref actorProp.StandMoveSpeed);
            //m_codeUtil.CodeValue(ref actorProp.CrouchMoveSpeed);
            //m_codeUtil.CodeValue(ref actorProp.ReloadTime);
            //m_codeUtil.CodeValue(ref actorProp.Decelerate);
            //m_codeUtil.CodeValue(ref actorProp.DecelerateTime);
            //m_codeUtil.CodeValue(ref actorProp.Impulse);
            //m_codeUtil.CodeValue(ref actorProp.FireRate);
            //m_codeUtil.CodeValue(ref actorProp.ZoomFireRate);
            //m_codeUtil.CodeValue(ref actorProp.StandSpreadMax);
            //m_codeUtil.CodeValue(ref actorProp.CrouchSpreadMax);
            //m_codeUtil.CodeValue(ref actorProp.RecoilPitchMin);
            //m_codeUtil.CodeValue(ref actorProp.RecoilPitchMax);
            //m_codeUtil.CodeValue(ref actorProp.RecoilYawMin);
            //m_codeUtil.CodeValue(ref actorProp.RecoilYawMax);
            //m_codeUtil.CodeValue(ref actorProp.JumpHeightAdd);
            //m_codeUtil.CodeValue(ref actorProp.PveExtraWeight);
            //m_codeUtil.CodeValue(ref actorProp.ZombieExtraWeight);
            m_actorProps[0] = actorProp;

            GameDispatcher.Dispatch(GameEvent.PLAYER_ACTORPROP_UPDATE, id);

            m_kAtkType = 0;
            m_actorProps[Ref_WeaponAtkType.MAX_COUNT] = actorProp;

            int tmp = -1;
            m_itemRoleID = -1;
            m_listWidget.Clear();
            for (int i = 0; i < actorProp.DispPart.Length; i++)
            {
                if (int.TryParse(actorProp.DispPart[i], out tmp) == true)
                {
                    ConfigItemRoleLine role = GlobalConfig.configRole.GetLine(tmp);
                    if (role != null)
                    {
                        m_itemRoleID = tmp;
                        continue;
                    }

                    ConfigItemDecorationLine decoration = GlobalConfig.configDecoration.GetLine(tmp);
                    if (decoration != null)
                    {
                        m_listWidget.Add(tmp);
                        continue;
                    }
                }
            }
            m_listWidget.Sort();
        }
    }

    internal toc_fight_actorprop.prop_info[] actorprops
    {
        set 
        {
            foreach (var propInfo in value)
            {
                var actorProp = propInfo.prop;
                //m_codeUtil.CodeValue(ref actorProp.Weight);
                //m_codeUtil.CodeValue(ref actorProp.StandMoveSpeed);
                //m_codeUtil.CodeValue(ref actorProp.CrouchMoveSpeed);
                //m_codeUtil.CodeValue(ref actorProp.ReloadTime);
                //m_codeUtil.CodeValue(ref actorProp.Decelerate);
                //m_codeUtil.CodeValue(ref actorProp.DecelerateTime);
                //m_codeUtil.CodeValue(ref actorProp.Impulse);
                //m_codeUtil.CodeValue(ref actorProp.FireRate);
                //m_codeUtil.CodeValue(ref actorProp.ZoomFireRate);

                //m_codeUtil.CodeValue(ref actorProp.StandSpreadMax);
                //m_codeUtil.CodeValue(ref actorProp.CrouchSpreadMax);
                //m_codeUtil.CodeValue(ref actorProp.RecoilPitchMin);
                //m_codeUtil.CodeValue(ref actorProp.RecoilPitchMax);
                //m_codeUtil.CodeValue(ref actorProp.RecoilYawMin);
                //m_codeUtil.CodeValue(ref actorProp.RecoilYawMax);
                //m_codeUtil.CodeValue(ref actorProp.AntiDecelerate);
                //m_codeUtil.CodeValue(ref actorProp.HitBackSpeed);
                //m_codeUtil.CodeValue(ref actorProp.HitBackTime);
                //m_codeUtil.CodeValue(ref actorProp.JumpHeightAdd);
                //m_codeUtil.CodeValue(ref actorProp.PveExtraWeight);
                //m_codeUtil.CodeValue(ref actorProp.ZombieExtraWeight);
                m_actorProps[propInfo.atkType] = actorProp;
            }

            // 记录ItemRole
            m_kAtkType = value[0].atkType;
            m_actorProps[Ref_WeaponAtkType.MAX_COUNT] = m_actorProps[m_kAtkType];

            GameDispatcher.Dispatch(GameEvent.PLAYER_ACTORPROP_UPDATE, id);

            var _actorProp = value[0].prop;
            int tmp = -1;
            //for (int i = 0; i < _actorProp.DispPart.Length; i++)
            //{
            //    if (int.TryParse(_actorProp.DispPart[i], out tmp) == true)
            //    {
            //        if (m_itemRoleID == tmp)
            //            break;
            //    }
            //}
            //if (m_itemRoleID != tmp)
            //{
            m_itemRoleID = -1;
            m_listWidget.Clear();
            for (int i = 0; i < _actorProp.DispPart.Length; i++)
            {
                if (int.TryParse(_actorProp.DispPart[i], out tmp) == true)
                {
                    ConfigItemRoleLine role = GlobalConfig.configRole.GetLine(tmp);
                    if (role != null)
                    {
                        m_itemRoleID = tmp;
                        continue;
                    }

                    ConfigItemDecorationLine decoration = GlobalConfig.configDecoration.GetLine(tmp);
                    if (decoration != null)
                    {
                        m_listWidget.Add(tmp);
                        continue;
                    }
                }
            }
            //}
        }
    }

    public int maxhp(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].MaxHP; }
    public float weight(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].Weight; }
    public float standMoveSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].StandMoveSpeed; }
    public float crouchMoveSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].CrouchMoveSpeed; }
    // 第三人称同步下来的当前武器id，对应武器表里的WeaponID字段
    public string propWeapon(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? "" : m_actorProps[atkType].WeaponId; }
    public string[] dispPart(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? null : m_actorProps[atkType].DispPart; }
//    public int initClipSize{get { return m_actorProp.Int("InitClipSize"); }}
    public int clipSize(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ClipSize; }
//    public int extraAmmoNum{get { return m_actorProp.Int("ExtraAmmoNum"); }}
    public float reloadTime(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ReloadTime; }
    public float decelerate(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].Decelerate; }
    public float decelerateTime(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 :m_actorProps[atkType].DecelerateTime; }
    public int maxArmor(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].MaxArmor; }
    public int armorRecoverWait(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ArmorRecoverWait; }
    public int armorRecoverSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ArmorRecoverSpeed; }
    public int hPRecoverWait(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].HPRecoverWait; }
    public int hPRecoverSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].HPRecoverSpeed; }
    public float Impulse(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].Impulse; }
    public float ImpulseSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ImpulseSpeed; }
    public float FireRate(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 100 : m_actorProps[atkType].FireRate; }
    public float ZoomFireRate(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ZoomFireRate; }
    
    //反减速
    public float AntiDecelerate(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].AntiDecelerate; }

    public float HitBackSpeed(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].HitBackSpeed; }
    public float HitBackTime(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].HitBackTime; }

    public int itemRoleID { get { return m_itemRoleID; } set { m_itemRoleID = value; } }

    public float JumpHeightAdd(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].JumpHeightAdd; }
    public float PveExtraWeight(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].PveExtraWeight; }
    public float ZombieExtraWeight(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].ZombieExtraWeight; }

    //武器后座力散射属性
    public float StandSpreadMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].StandSpreadMax; }
    public float CrouchSpreadMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].CrouchSpreadMax; }
    public float RecoilPitchMin(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].RecoilPitchMin; }
    public float RecoilPitchMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].RecoilPitchMax; }
    public float RecoilYawMin(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].RecoilYawMin; }
    public float RecoilYawMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].RecoilYawMax; }
    public int MaxHitCount(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? (EptInt)0 : m_actorProps[atkType].MaxHitCount; }
    public int RecoilCountMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? (EptInt)0 : m_actorProps[atkType].RecoilCountMax; }
    public float StandSpreadInitial(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].StandSpreadInitial; }
    public float StandSpreadFireAdd(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].StandSpreadFireAdd; }
    public float CrouchSpreadInitial(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].CrouchSpreadInitial; }
    public float CrouchSpreadFireAdd(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].CrouchSpreadFireAdd; }
    public float SpreadMovementMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].SpreadMovementMax; }
    public float SpreadJumpMax(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].SpreadJumpMax; }
    public float SpreadSightsFactor(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].SpreadSightsFactor; }
    public float SpreadDispMin(int atkType = Ref_WeaponAtkType.MAX_COUNT) { return IsEmpty(atkType) ? 0 : m_actorProps[atkType].SpreadDispMin; }

#endregion

#region 装备数据
    internal toc_fight_equip_info equipData
    {
        set
        {
            m_kEquipData = value;

            //m_listWeapon.Clear();
            m_dicEquipInfo.Clear();
            List<int> lw = new List<int>();
            m_grenadeID = -1;
            m_flashBombID = -1;
            foreach (toc_fight_equip_info.EquipInfo ei in m_kEquipData.list)
            {
                ConfigItemWeaponLine wl = ItemDataManager.GetItemWeapon(ei.itemid);
                if (wl != null)
                {
                    lw.Add(ei.itemid);
                    m_dicEquipInfo.Add(ei.itemid, ei);

                    // 是否有手雷
                    if ( wl.SubType == GameConst.WEAPON_SUBTYPE_GRENADE)
                    {
                        m_grenadeID = ei.itemid;
                    }
                    else if (wl.SubType == GameConst.WEAPON_SUBTYPE_FLASHBOMB)
                    {
                        m_flashBombID = ei.itemid;
                    }
                }
            }

            bool ok = true;
            if(lw.Count == m_listWeapon.Count)
            {
                for(int i=0; i<lw.Count; i++)
                {
                    if(m_listWeapon.Contains(lw[i]) == false)
                    {
                        ok = false;
                        break;
                    }
                }
            }
            else
            {
                ok = false;
            }

            if(ok == false)
            {
                m_listWeapon = lw;
                m_listWeapon.Sort(CompareWeaponList);

                
            }

            //Logger.Log("Set weaponVer : " + value.weaponVer);
        }
    }

    // 主角同步下来的当前武器数据是武器类型：主武器，副武器等，不是武器id
    // 所以这里要根据武器子类型去找到对应的武器id
    public int curWeapon   
    {
        get {
            foreach(int iWeaponID in m_listWeapon)
            {
                ConfigItemWeaponLine kWL = GlobalConfig.configWeapon.GetLine(iWeaponID);
                if (kWL != null && kWL.SubType == m_kEquipData.weapon)
                    return iWeaponID;
                    
            }
            return -1;
        }
    }

    public string curWeaponSubType
    {
        get
        {
            return m_kEquipData != null ? m_kEquipData.weapon : string.Empty;
        }
    }

    int CompareWeaponList( int weaponId1,int weaponId2 )
    {
        ConfigItemWeapon weaponConfig = GlobalConfig.configWeapon;
        ConfigItemWeaponLine line1 = weaponConfig.GetLine(weaponId1);
        ConfigItemWeaponLine line2 = weaponConfig.GetLine(weaponId2);

        int priority1 = 0;
        int priority2 = 0;
        if( line1.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 )
        {
            priority1 = 0;
        }
        else if( line1.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2  )
        {
            priority1 = 1;
        }
        else if(   line1.SubType == GameConst.WEAPON_SUBTYPE_DAGGER )
        {
            priority1 = 2;
        }
        else
        {
            priority1 = 3;
        }

        ///////////////////////////////////////////////////////

        if (line2.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1)
        {
            priority2 = 0;
        }
        else if (line2.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2)
        {
            priority2 = 1;
        }
        else if (line2.SubType == GameConst.WEAPON_SUBTYPE_DAGGER)
        {
            priority2 = 2;
        }
        else
        {
            priority2 = 3;
        }

        return priority1 - priority2;
    }

    public List<int> weaponList
    {
        get
        {
            return m_listWeapon; 
        }
    }

    public List<int> listWidget
    {
        get
        {
            return m_listWidget;
        }
    }

    public int widgetVaule
    {
        get
        {
            if (m_listWidget == null || m_listWidget.Count == 0)
                return 0;
            int r = 0,v = 1;
            for (int i = 0, imax = m_listWidget.Count; i < imax; ++i )
            {
                r += m_listWidget[i] * v;
                v *= 10;
            }
            return r;
        }
    }

    internal toc_fight_equip_info.EquipInfo GetEquipInfo(int weaponID)
    {
        toc_fight_equip_info.EquipInfo ei = null;
        m_dicEquipInfo.TryGetValue(weaponID, out ei);
        return ei;
    }

    public bool hasGrenade
    {
        get {
            if (grenadeID == -1)
                return false;

            LocalWeaponData lwd = m_player.localPlayerData.GetData(grenadeID);
            if (lwd == null || (lwd.curClipAmmoNum + lwd.extraAmmoNum) > 0)
                return true;
            else
                return false;
        }
    }

    public bool hasFlashBomb
    {
        get
        {
            if (flashBombID == -1)
                return false;

            LocalWeaponData lwd = m_player.localPlayerData.GetData(flashBombID);
            if (lwd == null || (lwd.curClipAmmoNum + lwd.extraAmmoNum) > 0)
                return true;
            else
                return false;
        }
    }

    public int grenadeID
    {
        get{ return m_grenadeID; }
    }

    public int flashBombID
    {
        get { return m_flashBombID; }
    }

    //public bool hasFlashBomb
    //{
    //    get {   return m_hasFlashBomb;  }
    //}

    public int weaponVer
    {
        get {
            if (m_kEquipData == null)
                return -1;
            return m_kEquipData.weaponVer;
        }
        set{ m_kEquipData.weaponVer = value; }
    }

    public bool hasWeapon(int id)
    {
        if (weaponList == null)
            return false;
        return weaponList.Contains(id);
    }

#endregion

#region 其它

    public float[] pos
    {
        get { return m_arrPos; }
        //set { 
        //    m_arrPos = value; 
        //    if(value != null)
        //    {
        //        if(value.Length >= 3)
        //        {
        //             GetPos(value, ref m_serverPos);
        //             if (m_funServerPosChanged != null)
        //                 m_funServerPosChanged();
        //        }

        //        if (value.Length >= 6)
        //            GetRot(value, ref m_serverRot);
        //    }
        //}
    }

    public void SetServerPos(float[] value, bool fromTween)
    {
        if (value == null)
            return;

        m_arrPos = value;
        if (value != null)
        {
            if (value.Length >= 3)
            {
                GetPos(value, ref m_serverPos);
                if (fromTween == false && m_funServerPosChanged != null)
                    m_funServerPosChanged();
            }

            if (value.Length >= 6)
                GetRot(value, ref m_serverRot);
        }
    }

    public Vector3 serverPos
    {
        get { return m_serverPos; }
    }

    public Vector3 serverRot
    {
        get { return m_serverRot; }
    }

#endregion

    private bool IsEmpty(int atkType)
    {
        if(m_actorProps[atkType] == null)
        {
            Logger.Warning("PlayerServerData 武器类型atkType=" + (atkType == Ref_WeaponAtkType.MAX_COUNT ? m_kAtkType : atkType) + " 数据为空");
            return true;
        }
        return false;
    }
}