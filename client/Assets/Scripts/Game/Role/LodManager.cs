﻿using System;
using System.Collections.Generic;
using UnityEngine;

class LodItem
{
    public GameObject gameObject;
    public Mesh mesh;
    public string modelName;
}

public class LodManager
{
    static private Dictionary<string, List<LodItem>> ms_dicCache = new Dictionary<string, List<LodItem>>();

    private const int MAX_IOD_LEVEL = 1;
    private const int UPDATE_FRAME_INTERVAL = 3;

    private static float[] IodLevelDistance;

    private bool m_release;
    private int m_level;
    private BasePlayer m_player;
    private Mesh m_mesh;

    private LodItem m_kLod;

    private int m_tick;

    //private Transform m_tfSceneCamera;
    private float m_distance;

    public bool enable { get; set; }

    public LodManager(BasePlayer player)
    {
        m_player = player;
        IodLevelDistance = GameSetting.IodLevelDistance;
    }

    public void Init()
    {
        enable = m_player.configData.LodModel != null && m_player.configData.LodModel.Length > 0 && IodLevelDistance != null && IodLevelDistance.Length > 0 && (m_player.renderer as SkinnedMeshRenderer) != null;
        if (enable)
        {
            m_mesh = (m_player.renderer as SkinnedMeshRenderer).sharedMesh;
            //m_tfSceneCamera = SceneCamera.singleton.gameObject.transform;
        }
    }

    /// <summary>
    /// 设置LOD级别
    /// 当level为0表示还原默认
    /// </summary>
    /// <param name="level"></param>
    public void SetLodLevel(int level)
    {
        if (!enable || m_level == level)
            return;

        if (m_level != 0 && m_kLod != null)
        {
            Cache(m_kLod);
            m_kLod = null;
        }

        m_level = level;
        if (m_level == 0)
        {
            //Logger.Log("SetLodLevel level=" + m_level + " sharedMesh=" + m_player.gameObject.name);
            SkinnedMeshRenderer smr = m_player.renderer as SkinnedMeshRenderer;
            if (smr != null)
                smr.sharedMesh = m_mesh;
            else
                Logger.Error("SetLodLevel level=" + m_level + " sharedMesh=" + m_player.gameObject.name + "  ERR!");
        }
        else
        {
            LoadRes(GetIodModel(m_level - 1));
        }
    }

    /// <summary>
    /// 加载
    /// </summary>
    /// <param name="modelName">模型名字</param>
    private void LoadRes(string modelName)
    {
        if (string.IsNullOrEmpty(modelName))
            return;

        string path = BasePlayer.GetRolePrefablPath(m_player.modelType, modelName);
        Action<GameObject> loadCallBack = (go) =>
        {
            if (m_player == null || m_player.released)
            {
                Logger.Error("m_player is null or released");
                return;
            }

            if (go == null)
            {
                Logger.Error("Player model load failed, path: " + path);
                return;
            }

            SkinnedMeshRenderer r = null;
            go.transform.VisitChild((child) =>
            {
                if (r == null)
                    r = child.GetComponent<SkinnedMeshRenderer>();
            }, false);

            m_kLod = new LodItem();
            m_kLod.gameObject = go;
            m_kLod.mesh = r.sharedMesh;

            if (m_release)
            {
                Cache(m_kLod);
                m_kLod = null;
                return;
            }

            string _modelName = GameObjectHelper.GetNameWithoutClone(go.name);
            if (modelName != _modelName)
            {
                Cache(m_kLod);
                m_kLod = null;
                return;
            }

            //go.transform.SetParent(SceneManager.singleton.reclaimRoot);
            //go.transform.localPosition = Vector3.zero;

            //Logger.Log("LoadRes level=" + m_level + " iod.renderer.sharedMesh=" + m_kLod.gameObject.name);

            //替换
            if (m_player.renderer != null)
            {
                var sm = m_player.renderer as SkinnedMeshRenderer;
                if(sm != null)
                    sm.sharedMesh = m_kLod.mesh;
            }
        };

        LodItem cached = GetCache(modelName);
        if (cached != null)
        {
            m_kLod = cached;
            (m_player.renderer as SkinnedMeshRenderer).sharedMesh = cached.mesh;
        }
        else
            ResourceManager.LoadModelRef(path, loadCallBack, false);
    }

    private string GetIodModel(int level)
    {
        if (m_player.configData.LodModel.Length >= level)
            return m_player.configData.LodModel[level];
        return string.Empty;
    }

    public void Update()
    {
        if(enable && ++m_tick == UPDATE_FRAME_INTERVAL)
        {
            if (m_player == null || m_player.released || !m_player.isAlive || m_player.gameObject == null || SceneCamera.singleton == null)
                return;

            m_tick = 0;

            //m_distance = Vector3.Distance(m_tfSceneCamera.position, m_player.position);
            m_distance = Vector3.Distance(SceneCamera.singleton.position, m_player.position);
            if (m_player.curWeapon != null && (m_player.curWeapon is WeaponGun) && (m_player.curWeapon as WeaponGun).isZoom)
                m_distance /= m_player.curWeapon.weaponConfigLine.ZoomFactor;

            //查找最合适的级别
            for(int i = MAX_IOD_LEVEL -1; i >= 0 ; --i)
            {
                if (m_distance >= IodLevelDistance[i])
                {
                    SetLodLevel(i + 1);
                    return;
                }
            }
            SetLodLevel(0);
        }
    }

    public void Release()
    {
        m_release = true;
        if(m_level != 0)
        {
            if ( m_kLod != null)
            {
                Cache(m_kLod);
            }
            m_level = 0;
        }
        enable = false;
    }

    #region 模型缓存
    private void Cache(LodItem item)
    {
        if (item == null)
            return;

        string name = item.gameObject.name;
        List<LodItem> listLod = null;
        if (ms_dicCache.TryGetValue(name, out listLod) == false)
        {
            listLod = new List<LodItem>();
            ms_dicCache.Add(name, listLod);
        }
        listLod.Add(item);
    }

    private LodItem GetCache(string name)
    {
        List<LodItem> listLod = null;
        ms_dicCache.TryGetValue(name, out listLod);
        if (listLod == null || listLod.Count <= 0)
            return null;

        LodItem i = listLod[listLod.Count - 1];
        listLod.RemoveAt(listLod.Count - 1);

        return i;
    }

    static public void ReleaseCacheGO()
    {
        foreach (KeyValuePair<string, List<LodItem>> kvp in ms_dicCache)
        {
            for (int i = 0; i < kvp.Value.Count; i++)
            {
                kvp.Value[i] = null;
            }
        }
        ms_dicCache.Clear();
    }
    #endregion


}
