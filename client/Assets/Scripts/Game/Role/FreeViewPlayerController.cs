﻿using UnityEngine;
using System.Collections.Generic;


public class FreeViewPlayerController : MonoBehaviour
{

    FreeViewPlayer m_kPlayer;
    float m_moveSpeed;
    protected Dictionary<string, bool> m_dicCtrlMark = new Dictionary<string, bool>();

    public void BindPlayer(FreeViewPlayer player)
    {
        m_kPlayer = player;
    }

    public void Run()
    {
        GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_MOVE, OnMoveKey);
    }

    public void Stop()
    {
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOVE, OnMoveKey);
    }

    public void Release()
    {
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOVE, OnMoveKey);
    }

    private void SetCtrlMark(string strType, bool bValue)
    {
        if (m_dicCtrlMark.ContainsKey(strType) == false)
            m_dicCtrlMark.Add(strType, bValue);
        else
            m_dicCtrlMark[strType] = bValue;
    }

    private bool GetCtrlMark(string strType)
    {
        if (m_dicCtrlMark.ContainsKey(strType) == false)
            m_dicCtrlMark.Add(strType, false);
        return m_dicCtrlMark[strType];
    }

    public Vector3 GetMoveDir()
    {
        if (m_kPlayer == null || m_kPlayer.transform == null)
            return Vector3.zero;

        Vector3 vec3Dir = Vector3.zero;
        Transform kTrans = m_kPlayer.transform;

        if (MainPlayerControlInputReceiver.singleton != null &&
            MainPlayerControlInputReceiver.singleton.enabled == true)
        {
            vec3Dir += MainPlayerControlInputReceiver.singleton.moveForward ? m_kPlayer.cameraSlot.forward :
                MainPlayerControlInputReceiver.singleton.moveBack ? -m_kPlayer.cameraSlot.forward : Vector3.zero;
            vec3Dir += MainPlayerControlInputReceiver.singleton.moveLeft ? kTrans.localToWorldMatrix.MultiplyVector(Vector3.left) : MainPlayerControlInputReceiver.singleton.moveRight ? kTrans.localToWorldMatrix.MultiplyVector(Vector3.right) : Vector3.zero;
        }
        if (vec3Dir == Vector3.zero && UIManager.HasPanel<PanelBattle>())
        {
            vec3Dir = UIManager.GetPanel<PanelBattle>().GetMoveDir();
            vec3Dir = kTrans.localToWorldMatrix.MultiplyVector(vec3Dir);
            vec3Dir += new Vector3(0, m_kPlayer.cameraSlot.forward.y * vec3Dir.z, 0);
        }
        vec3Dir.Normalize();

        return vec3Dir;
    }

    private float GetSpeed(Vector3 vec3MoveDir)
    {
        float speed = 0;

        string dirName = VectorHelper.GetDir(m_kPlayer.transform.forward, Vector3.Normalize(vec3MoveDir));
        switch (dirName)
        {
            case VectorHelper.DIR_FORWARD:
                speed = 6f;
                break;
            case VectorHelper.DIR_BACK:
                speed = 6f;
                break;
            case VectorHelper.DIR_LEFT:
            case VectorHelper.DIR_RIGHT:
                speed = 6f;
                break;
        }
        m_moveSpeed = speed;
        return speed;
    }

    private void Update()
    {
        if (m_kPlayer != null)
            OnMoveKeyPressUpdate();
    }

    private void OnMoveKey(bool keyDown)
    {
        if (m_kPlayer != null)
            SetCtrlMark(GameEvent.INPUT_MOVE, keyDown);
    }

    private void OnMoveKeyPressUpdate()
    {
        bool inputMove = GetCtrlMark(GameEvent.INPUT_MOVE);

        if (inputMove == false)
            return;

       Vector3 vec3MoveDir = GetMoveDir();
       float speedRate = 1;
       float fSpeed = 0;

       fSpeed = GetSpeed(vec3MoveDir) * speedRate;
       float fDeltaSpeed = fSpeed * Time.smoothDeltaTime;
       Vector3 vec3Move = fDeltaSpeed * vec3MoveDir;
       m_kPlayer.Move(vec3Move);
    }

    public void LookRotate(float xDelta, float yDelta)
    {
        m_kPlayer.KeyboardLookRotate(xDelta, yDelta);

    }

    public void TouchLookRotate(float pitch, float yaw)
    {
        m_kPlayer.TouchLookRotate(pitch, yaw);
    }


}

