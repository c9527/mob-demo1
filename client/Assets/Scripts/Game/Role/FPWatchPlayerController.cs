﻿using UnityEngine;

public class FPWatchPlayerController : BasePlayerController
{
    protected FPWatchPlayer m_kPlayer;
    private float m_targetPitchAngle;
    private float m_pitchSpeed;
    private float m_curPitchAngle;
    private Transform m_transSceneCameraSlot;

    private EptFloat m_fAimPointRayTestTick = 0f;


    public override void Release()
    {
        m_kPlayer = null;
    }

    public override void BindPlayer(BasePlayer kBP)
    {
        base.BindPlayer(kBP);
        m_kPlayer = kBP as FPWatchPlayer;
        m_transSceneCameraSlot = m_kPlayer.sceneCameraSlot;
    }

    protected override void Update()
    {
        if (m_kPlayer == null)
            return;

        float deltaTime = Time.smoothDeltaTime;
        PlayerStatus status = m_kPlayer.status;

        if (status.fire || status.reload || status.switchWeapon)
        {
            if (status.move && m_kPlayer.cameraMoveAniPlaing == true)
                m_kPlayer.StopCameraWalkAni();

            if (!status.move && m_kPlayer.cameraIdleAniPlaying == true)
                m_kPlayer.StopCameraIdleAni();
        }
        else
        {
            if (status.move && m_kPlayer.cameraMoveAniPlaing == false)
                m_kPlayer.PlayCameraWalkAni();

            if (!status.move && m_kPlayer.cameraIdleAniPlaying == false)
                m_kPlayer.PlayCameraIdleAni();
        }

		if(m_curPitchAngle != m_targetPitchAngle)
        {
            float plusMinus = m_targetPitchAngle > m_curPitchAngle ? 1 : -1;
            m_curPitchAngle += Time.deltaTime * m_pitchSpeed * plusMinus;
            if (plusMinus == 1 && m_curPitchAngle >= m_targetPitchAngle || plusMinus == -1 && m_curPitchAngle <= m_targetPitchAngle)
            {
                m_curPitchAngle = m_targetPitchAngle;
            }
            m_transSceneCameraSlot.localEulerAngles = new Vector3(m_curPitchAngle, m_transSceneCameraSlot.localEulerAngles.y, m_transSceneCameraSlot.localEulerAngles.z);
        }

        m_fAimPointRayTestTick += deltaTime;
        if (m_fAimPointRayTestTick >= GameSetting.AIM_POINT_RAYTEST_INTERVAL)
        {
            m_fAimPointRayTestTick = 0f;
            if (!WorldManager.singleton.isHideModeOpen) //躲猫猫模式不开启检测
            {
                AimPointRayTest();
            }
        }
    }

    private void AimPointRayTest()
    {
        //if (GameSetting.enableRayName == false)
        //    return;

        Ray kRay = AutoAim.CurAimRay;
        RaycastHit kRH;

        if (Physics.Raycast(kRay, out kRH, GameSetting.FIRE_RAYTEST_LENGTH,
            GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER))
        {
            GameObject goPlayer = null;
            Transform transRoot = kRH.collider.transform.root; // 玩家GameObject一定要放在场景根结点下
            if (transRoot != null)
                goPlayer = transRoot.gameObject;
            else
                goPlayer = kRH.collider.gameObject;

            BasePlayer kBP = WorldManager.singleton.GetPlayerWithGOHash(goPlayer.GetHashCode());
            if (kBP != null && kBP.serverData != null && kBP.alive && kBP.isVisible)
                GameDispatcher.Dispatch(GameEvent.UI_SHOW_AIM_PLAYER_NAME, kBP, m_kPlayer.IsEnemy(kBP));
            else
                GameDispatcher.Dispatch(GameEvent.UI_CANCEL_AIM_PLAYER_NAME);
        }
        else
        {
            GameDispatcher.Dispatch(GameEvent.UI_CANCEL_AIM_PLAYER_NAME);
        }
    }

    override public void StartJump()
    {
        m_kPlayer.status.jump = true;
        jumpStatus = JUMP_STATUS_UP;
    }

    public override void JumpComplete()
    {
        m_kPlayer.JumpComplete();
    }

    override public void SetPitchAngle(float angle, float tweenTime)
    {
        angle *= 2f;
        angle += GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT.x;

        float newAngle = angle > 180 ? angle - 360 : angle;
        float curAngle = m_transSceneCameraSlot.localEulerAngles.x > 180 ? m_transSceneCameraSlot.localEulerAngles.x - 360 : m_transSceneCameraSlot.localEulerAngles.x;
        float deltaAngle = newAngle - curAngle;

        if (deltaAngle == 360f || Mathf.Abs(deltaAngle) <= 0.01f)
            return;

        m_targetPitchAngle = newAngle;

        if (tweenTime != 0)
            m_pitchSpeed = Mathf.Abs(deltaAngle) / tweenTime;
        else
            m_pitchSpeed = float.MaxValue;

        //if (m_pitchSpeed > 1000)
        //    Logger.Log("Set Pitch: " + " speed=" + m_pitchSpeed  + " deltaAngle=" +deltaAngle + " angle=" + angle + " newAngle=" + newAngle + " cur=" +  m_transSceneCameraSlot.localEulerAngles.x + " newCur=" + curAngle + "time=" + tweenTime); 
        //else
        //    Logger.Log("Set Pitch: " + " speed=" + m_pitchSpeed  + " deltaAngle=" +deltaAngle + " angle=" + angle + " newAngle=" + newAngle + " cur=" +  m_transSceneCameraSlot.localEulerAngles.x + " newCur=" + curAngle + "time=" + tweenTime); 
    }
}
