﻿using UnityEngine;
using System;


public struct PlayerDeadCameraSwitchData
{
    public Vector3 pos;
    public Vector3 face;
    // public Vector3 right;
    public Vector3 killerPos;
    public BasePlayer killer;
    public string model;
    public string deadAni;
    public bool isShowModel;
    public bool isSimpleDeadAni;
    public bool isShake;
};

public class DeadPlayer : Singleton<DeadPlayer>
{
    private int RESCUE_WAIT_TIME_SEC;

    private GameObject m_go;
    private Transform m_trans;
    private Animator m_kAnimator;
    //private RuntimeAnimatorController m_kRAC;
    protected Transform m_transSceneCameraSlot;

    private PlayerDeadCameraSwitchData m_kData;
    private PlayerDeadCameraSwitchData m_kDataPre;

    private SphereCollider spCollider;
    private GameObject goDyingEffect = null;
    private GameObject goDyingGameObject = null;
    private UIRescueTip uiDirectionTip = null;
    private UIThroughTheWall rescuingTip = null;

    private Action funcCallBack = null;

    private bool m_show;
    

    public DeadPlayer()
    {
        
    }

    public void Release()
    {
        ResourceManager.Destroy(m_go);
        ResourceManager.Destroy(goDyingEffect);
        funcCallBack = null;
        goDyingEffect = null;
        m_go = null;
        //m_kRAC = null;
        m_kAnimator = null;
    }

    public void SetData(PlayerDeadCameraSwitchData data)
    {
        m_kDataPre = m_kData;
        m_kData = data;        
    }

    public void PreInstantiate()
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.configData != null && MainPlayer.singleton.IsShowOPModel())
        {            
                string strPath = MainPlayer.singleton.GetMPOPPrefabPath();
                ResourceManager.LoadBattlePrefab(strPath, (go, crc) =>
                {
                    if (go == null)
                        return;                     
                    SetGO(go);
                    if (MainPlayer.singleton != null && MainPlayer.singleton.configData != null && MainPlayer.singleton.IsShowOPModel())
                    {
                        PlayerDeadCameraSwitchData data = new PlayerDeadCameraSwitchData();
                        data.model = MainPlayer.singleton.configData.OPModel;
                        SetData(data);
                    }
                    Active(false);                    
                });            
        }
    }

    public void Show(Action<bool> funLoaded)
    {
        m_show = true;        
        if (m_go != null && string.Equals(m_kDataPre.model, m_kData.model))
        {
            Active(true);
            m_go.transform.position = SceneManager.GetGroundPoint(m_kData.pos);
            m_go.transform.forward = m_kData.face;            

            PlayDeadAni();

            if (funLoaded != null)
                funLoaded(true);  
        }
        else
        {
            ResourceManager.Destroy(m_go);
            m_go = null;
            m_kAnimator = null;
            string strPath = PathHelper.GetRolePrefabPath(m_kData.model);
            ResourceManager.LoadBattlePrefab(strPath, (go, crc) =>
            {
                CallBack(go, crc, funLoaded);
            });                        
                //if (m_kRAC == null)
                //{
                //    string aniCtrlPath = PathHelper.GetRoleAnimatorControllerPath(m_kData.isBiochemical ? AniConst.BIO_DEAD_CONTROLLER_NAME : AniConst.PLAYER_DEAD_CONTROLLER_NAME);
                //    ResourceManager.LoadAnimatorController(aniCtrlPath, (aniCtrl) =>
                //    {
                //        bool bRet = true;
                //        if (aniCtrl == null)
                //        {
                //            Logger.Error("AnimatorController " + aniCtrlPath + " is null");
                //            Release();
                //            bRet = false;
                //        }
                //        else
                //        {
                //            m_kAnimator.runtimeAnimatorController = aniCtrl;
                //            m_kRAC = aniCtrl;
                //            PlayDeadAni();
                //        }
                       
                //        if (funLoaded != null)
                //            funLoaded(bRet);
                //    });
                //}
                //else
                //{
                //    m_kAnimator.runtimeAnimatorController = m_kRAC;
                //    PlayDeadAni();
                //    if (funLoaded != null)
                //        funLoaded(true);
                //}       
        }
    }


    private void SetGO(GameObject go)
    {
        m_go = go;
        m_trans = go.transform;
        m_transSceneCameraSlot = GameObjectHelper.CreateChildIfNotExisted(go.transform, ModelConst.SLOT_SCENE_CAMERA).transform;
        CharacterController kCC = go.GetComponent<CharacterController>();
        if (kCC != null)
            kCC.enabled = false;
        m_kAnimator = GameObjectHelper.GetComponent<Animator>(go);
        m_kAnimator.applyRootMotion = false;
    }

    private void CallBack(GameObject go, uint crc, Action<bool> funLoaded)
    {
        if (go == null && funLoaded != null)
        {
            funLoaded(false);
            return;
        }
        SetGO(go);
        if (m_show == false)
        {
            Active(false);
            return;
        }

        m_trans.position = SceneManager.GetGroundPoint(m_kData.pos);
        m_trans.forward = m_kData.face;

        PlayDeadAni();

        if (funLoaded != null)
            funLoaded(true);
    }

    public void Hide()
    {
        m_show = false;

        Active(false);
    }

    private void PlayDeadAni()
    {
        m_kAnimator.SetLayerWeight(1, 0);
        m_kAnimator.SetLayerWeight(2, 0);
        m_kAnimator.Play(m_kData.deadAni, -1, 0);

        if (funcCallBack != null)
        {
            funcCallBack();
            funcCallBack = null;
        }
        //if (isDying)
        //{
        //    doDying();
        //}
        //else
        //{
        //    TimerManager.SetTimeOut(GameSetting.OTHER_PLAYER_DEAD_DISAPPEAR_TIME, () =>
        //    {
        //        Active(false);
        //    });
        //}
    }

    private void Active(bool value)
    {
        if (m_go == null)
            return;

        if(value == false)
        {
            //子接点等于1表示有挂载相机
            if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
            {
                SceneCamera.singleton.SetParent(null);
            }
            m_trans.SetParent(SceneManager.singleton.reclaimRoot);
            m_trans.localPosition = Vector3.zero;
        }
        else
        {
            m_trans.SetParent(null);
        }
        m_kAnimator.enabled = value;
    }

    #region 死亡相机拖动相关
    public  bool isActive
    {
        get { return m_trans == null ? false : m_trans.parent == SceneManager.singleton.reclaimRoot; }
    }

    public Transform sceneCameraSlot
    {
        get { return m_transSceneCameraSlot; }
    }
    public Vector3 diePos
    {
        get
        {
            return m_kData.pos;
        }
    }

    public Vector3 dirForward
    {
        get
        {
            return m_kData.face;
        }
    }

    //public Transform transform
    //{
    //    get { return m_trans; }
    //}

    public GameObject gameObject
    {
        get { return m_go; }
    }
    #endregion

    #region 濒死状态处理
    public void doDying()
    {
        if (m_go == null)
        {
            funcCallBack = doDying;
            return;
        }

        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        RESCUE_WAIT_TIME_SEC = line.RescueWaitTime;

        if (goDyingGameObject == null)
        {
            var dyingTrans = m_trans.Find("goDyingGameObject");
            if (dyingTrans == null)
            {
                goDyingGameObject = new GameObject("goDyingGameObject");
                goDyingGameObject.transform.SetParent(m_trans, false);
            }
            else
            {
                goDyingGameObject = dyingTrans.gameObject;
            }
        }
        else
        {
            goDyingGameObject.TrySetActive(true);
        }

        if (uiDirectionTip != null)
        {
            uiDirectionTip.enabled = true;
            uiDirectionTip.ForceHide(false);
            uiDirectionTip.ShowDownTimer(RESCUE_WAIT_TIME_SEC);
        }
        else
        {
            ResourceManager.LoadSprite(AtlasName.Battle, "plus", sp =>
            {
                if (m_go == null || goDyingGameObject == null) return;
                uiDirectionTip = goDyingGameObject.AddMissingComponent<UIRescueTip>();
                uiDirectionTip.SetTipSprite(sp, true);
                uiDirectionTip.SetPosAndScale(new Vector3(0, 100, 0), new Vector3(1.5f, 1.5f, 1), new Vector3(0, 1.5f, 0));
                uiDirectionTip.SetPosAndScale2(new Vector3(-28, 420, 0), new Vector3(48, 420, 0), new Vector3(6, 6, 1));
                uiDirectionTip.ShowDownTimer(RESCUE_WAIT_TIME_SEC);
            });
        }

        if (goDyingEffect != null)
        {
            goDyingEffect.TrySetActive(true);
        }
        else
        {
            spCollider = goDyingGameObject.AddMissingComponent<SphereCollider>();
            spCollider.center = new Vector3(0, 0.5f, 0);
            spCollider.radius = 1;
            spCollider.isTrigger = true;
            PointTriggerEvent script = goDyingGameObject.AddMissingComponent<PointTriggerEvent>();
            script.type = "Rescue";
            //script.id = PlayerId;

            ResourceManager.LoadBattlePrefab("Effect/help", (go, crc) =>
            {
                if (go == null || m_go == null) return;
                goDyingEffect = go;
                go.transform.SetParent(goDyingGameObject.transform, false);
            });
        }
    }

    private void playRescuingTip(bool isShow)
    {
        if (!isShow)
        {
            if (rescuingTip != null) rescuingTip.stop();
        }
        else
        {
            if (rescuingTip == null) rescuingTip = UIThroughTheWall.createUI();
            ThroughTheWallProp prop = new ThroughTheWallProp() { isTweenPos = true, imgProp = new RectTransformProp() { scale = new Vector3(4f, 4f, 1) } };
            rescuingTip.play(AtlasName.Battle, "rescuing", goDyingGameObject.transform, new Vector3(0, 1.2f, 0f), prop);
        }
    }

    public void HideRescue()
    {
        if (uiDirectionTip != null)
        {
            uiDirectionTip.enabled = false;
        }

        playRescuingTip(false);

        if (goDyingEffect != null)
        {
            goDyingEffect.TrySetActive(false);
        }
        if (goDyingGameObject != null)
        {
            goDyingGameObject.TrySetActive(false);
        }
        Active(false);
    }

    public void ShowDyingDrectionTip(bool isShow, int sec = 0)
    {
        if (uiDirectionTip != null)
        {
            uiDirectionTip.ForceHide(!isShow);
            uiDirectionTip.ShowSprites(isShow);
            uiDirectionTip.ShowDownTimer(sec);
        }

        playRescuingTip(!isShow);
    }
    #endregion
}
 
