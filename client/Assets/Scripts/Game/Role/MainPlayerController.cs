﻿using UnityEngine;
using System.Collections.Generic;

class HitBackDistanceInfo
{
    public float distance;
    public float speed;
    public Vector3 dir;
}

class HitBackSpeedInfo
{
    public float speed;
    public float time;
}


public class MainPlayerController : BasePlayerController
{
    private const string JUMP_MOVE_ORDER_JUMP_FIRST = "jump_move_order_jump_first";
    private const string JUMP_MOVE_ORDER_MOVE_FIRST = "jump_move_order_move_first";
    private const string JUMP_MOVE_ORDER_UNKNOWN = "jump_move_order_unknown";

    private const float FLOAT_WAIT_TIME = 2.0f;
    private const int HIT_COLLIDER_NUM = 10;
    private const float PENETRATE_CHECK_TIME = 0.08f;

    public const int MAX_PLAYER_COUNT = 20;

    protected MainPlayer m_kPlayer;
    protected Dictionary<string, bool> m_dicCtrlMark = new Dictionary<string, bool>();

    private EptFloat m_fAimPointRayTestTick = 0f;

    private VtfFloatCls m_moveSpeed = new VtfFloatCls();

    private VtfFloatCls m_jumpUpTick = new VtfFloatCls();
    protected VtfFloatCls m_jumpStartSpeed = new VtfFloatCls();   // 起跳速度
    private string m_strJumpMoveOrder;
    private EptVector3 m_moveDirBeforeJump;
   
    protected VtfFloatCls m_gravityTick = new VtfFloatCls();          // 重力下落持续时间

    private bool m_hitBackDistance;
    private bool m_hitBackSpeed;
    private EptFloat m_fCurHitBackSpeed = 0;

    private List<HitBackDistanceInfo> m_listHBDI = new List<HitBackDistanceInfo>();
    private HitBackSpeedInfo[] m_HBSI = new HitBackSpeedInfo[MAX_PLAYER_COUNT];
    private List<Vector3> m_prePosList = new List<Vector3>();
    private List<Collider> m_listHitCollider = new List<Collider>();

    private EptFloat m_floatTime;
    private bool m_bPenetrate = false;
    private float m_penetrateCheckTime;
    private Vector3 m_lastRightPos;
    private Vector3 m_lastPenetratePos; // 最后一次穿透前的正确位置
    private bool m_outMap;  // 跑到地图外了 
    private Vector3 CORRECT_POS_OFFSET = new Vector3(0, 0.2f, 0);

    private GameObject m_goLadder;

    private bool m_bIsForceMove = false;
    private bool m_bIsJumpPressReverse = false;
    private EptFloat m_fReverseMoveTick;

    private bool m_bIsInit = false;
    private float m_flastPosY = 0;  //最后一次玩家的Y坐标

    private bool m_bIsBouncing = false;     //是否正在弹起中
    private bool m_bBounceForceMove;        //弹起强制移动
    private EptVector3 m_vecBounceForceMoveDir;   //弹起强制移动方向
    private EptFloat m_fBounceForceSpeed;      //强制弹起速度

    private EptFloat m_decelerationLimitSpeed; //减速上限

    private bool m_bIsRushMove;   //冲刺移动
    private EptFloat m_fRushTime;    //冲刺时间
    private Vector3 m_fRushDir;   //冲刺方向

    // 瞬移作弊检测
    private VtfFloatCls m_posCheckTime = new VtfFloatCls();

    // Debug Info
    public string DebugInfo = "";
    private bool IsFlyMode;
    private EptFloat flySpeed = 0f;
  

    public override void Release()
    {
        m_kPlayer = null;

        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOVE, OnMoveKey);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOVE_SLOW, OnMoveSlowKey);

        GameDispatcher.RemoveEventListener(GameEvent.INPUT_JUMP, OnJumpKeyDown);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_JUMP_END, OnJumpKeyUp);
        
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_CROUCH, OnCrouchKeyDown);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_STAND, OnStandKeyDown);

        GameDispatcher.RemoveEventListener<string>(GameEvent.INPUT_SWITCH_WEAPON, OnSwitchKeyDown);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_FIRE, OnFireKey);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_FIRE_SUB, OnFireSubKey);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_ZOOM, OnZoom);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_RELOAD_AMMO, OnReloadKeyDown);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_ENABLE_GUN_SCOPE, OnEnableGunScope);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_FIRE_GRENADE, OnFireGrande);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CANCLE_FIRE, OnCancleFire);
        GameDispatcher.RemoveEventListener<bool, GameObject>(GameEvent.MAINPLAYER_CLIMB, SetClimb);
        GameDispatcher.RemoveEventListener<bool, float, bool, Vector3, float>(GameEvent.MAINPLAYER_BOUNCE, Bounce);

        m_bIsInit = false;
        m_released = true;
    }

    public void Reset()
    {
        m_dicCtrlMark.Clear();
        m_fAimPointRayTestTick = 0;
        jumpUpTick = 0;
        m_strJumpMoveOrder = JUMP_MOVE_ORDER_UNKNOWN;
        jumpStatus = JUMP_STATUS_COMPLETE;

        JumpComplete();

        m_prePosList.Clear();
        m_listHitCollider.Clear();

        m_enterDeathBoxTick = 0;
    }

    public override void BindPlayer(BasePlayer kBP)
    {
        base.BindPlayer(kBP);

        m_kPlayer = kBP as MainPlayer;

        if (!m_bIsInit)
        {
            m_decelerationLimitSpeed = ConfigMisc.GetInt("deceleration_limit_speed") / 1000f;

            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_MOVE, OnMoveKey);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_MOVE_SLOW, OnMoveSlowKey);

            GameDispatcher.AddEventListener(GameEvent.INPUT_JUMP, OnJumpKeyDown);
            GameDispatcher.AddEventListener(GameEvent.INPUT_JUMP_END, OnJumpKeyUp);

            GameDispatcher.AddEventListener(GameEvent.INPUT_CROUCH, OnCrouchKeyDown);
            GameDispatcher.AddEventListener(GameEvent.INPUT_STAND, OnStandKeyDown);

            GameDispatcher.AddEventListener<string>(GameEvent.INPUT_SWITCH_WEAPON, OnSwitchKeyDown);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_FIRE, OnFireKey);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_FIRE_SUB, OnFireSubKey);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_ZOOM, OnZoom);
            GameDispatcher.AddEventListener(GameEvent.INPUT_RELOAD_AMMO, OnReloadKeyDown);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_ENABLE_GUN_SCOPE, OnEnableGunScope);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_FIRE_GRENADE, OnFireGrande);
            GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_FIRE_FLASHBOMB, OnFireFlashBomb);
            GameDispatcher.AddEventListener(GameEvent.UI_CANCLE_FIRE, OnCancleFire);
            GameDispatcher.AddEventListener<bool, GameObject>(GameEvent.MAINPLAYER_CLIMB, SetClimb);
            GameDispatcher.AddEventListener<bool, float, bool, Vector3, float>(GameEvent.MAINPLAYER_BOUNCE, Bounce);
            m_bIsInit = true;
        }
    }

    public bool canControl
    {
        get
        {
            if (this != null && m_kPlayer != null && m_kPlayer.alive == true && m_kPlayer.gameObject != null && SceneManager.singleton.battleSceneLoaded)
                return true;

            return false;
        }
    }

    protected void SetClimb(bool value, GameObject ladder)
    {
        if (m_kPlayer != null)
        {
            m_kPlayer.status.climb = value;
            if (value)
            {
                m_goLadder = ladder;
            }
            else
            {
                m_goLadder = null;
            }
        }
    }

    //弹起
    protected void Bounce(bool isBouncing, float jumpHeight, bool forceMove, Vector3 moveDir, float forceSpeed)
    {
        m_bIsBouncing = isBouncing;

        if (!m_bIsBouncing)
            return;

        m_bBounceForceMove = forceMove;
        m_vecBounceForceMoveDir = moveDir;
        m_fBounceForceSpeed = forceSpeed;

        if (m_kPlayer.status.move == true)
            m_strJumpMoveOrder = JUMP_MOVE_ORDER_MOVE_FIRST;
        else
            m_strJumpMoveOrder = JUMP_MOVE_ORDER_JUMP_FIRST;

        if (m_kPlayer.status.crouch == true)
        {
            OnStandKeyDown();
        }

        //去掉重力影响
        gravityTick = 0;

        m_kPlayer.Jump();

        if (m_kPlayer.status.jump)
        {
            m_bIsForceMove = true;
            jumpStatus = JUMP_STATUS_UP;
            m_moveDirBeforeJump = GetMoveDir();
            jumpStartSpeed = Mathf.Sqrt(2 * GameSetting.GRAVITY_JUMPUP * jumpHeight);
        }
    }

    public void Rush(float distance, float time)
    {
        m_fRushTime = time;
        moveSpeed = distance / time;
        m_fRushDir = m_kPlayer.transform.forward;
    }

    private void UpdateRush()
    {
        if (m_fRushTime > 0)
        {
            float fDeltaRusMove = 0;
            m_fRushTime -= Time.smoothDeltaTime;
            if (m_fRushTime <= 0)
            {
                fDeltaRusMove = moveSpeed * (m_fRushTime + Time.smoothDeltaTime);
                RushComplete();
            }
            else
                fDeltaRusMove = moveSpeed * Time.smoothDeltaTime;
            Vector3 vec3RusMove = fDeltaRusMove * m_fRushDir;
            m_kPlayer.Move(vec3RusMove, false);
        }
    }

    private void RushComplete()
    {
        m_fRushTime = 0f;
        moveSpeed = 0f;
        m_kPlayer.RushComplete();
    }

    public override void JumpComplete()
    {
        m_strJumpMoveOrder = JUMP_MOVE_ORDER_UNKNOWN;
        m_bIsForceMove = false;
        m_bIsJumpPressReverse = false;
        m_fReverseMoveTick = 0;
        m_bBounceForceMove = false;

        if (null != m_kPlayer)
        {
            m_kPlayer.status.move = false;
            m_kPlayer.status.jump2 = false;
            m_kPlayer.JumpComplete();
        }
    }

    private void AimPointRayTest()
    {
        //if (GameSetting.enableRayName == false)
        //    return;

        Ray kRay = AutoAim.CurAimRay;
        RaycastHit kRH;

        if (Physics.Raycast(kRay, out kRH, GameSetting.FIRE_RAYTEST_LENGTH,
            GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER))
        {
            GameObject goPlayer = null;
            Transform transRoot = kRH.collider.transform.root; // 玩家GameObject一定要放在场景根结点下
            if (transRoot != null)
                goPlayer = transRoot.gameObject;
            else
                goPlayer = kRH.collider.gameObject;

            BasePlayer kBP = WorldManager.singleton.GetPlayerWithGOHash(goPlayer.GetHashCode());
            if (kBP != null && kBP.serverData != null && kBP.alive && kBP.isVisible)
                GameDispatcher.Dispatch(GameEvent.UI_SHOW_AIM_PLAYER_NAME, kBP, m_kPlayer.IsEnemy(kBP));
            else
                GameDispatcher.Dispatch(GameEvent.UI_CANCEL_AIM_PLAYER_NAME);
        }
        else
        {
            GameDispatcher.Dispatch(GameEvent.UI_CANCEL_AIM_PLAYER_NAME);
        }
    }

    private void SetCtrlMark(string strType, bool bValue)
    {
        if (m_dicCtrlMark.ContainsKey(strType) == false)
            m_dicCtrlMark.Add(strType, bValue);
        else
            m_dicCtrlMark[strType] = bValue;
    }

    private bool GetCtrlMark(string strType)
    {
        if (m_dicCtrlMark.ContainsKey(strType) == false)
            m_dicCtrlMark.Add(strType, false);
        return m_dicCtrlMark[strType];
    }

    public Vector3 GetMoveDir()
    {
        if (m_kPlayer == null || m_kPlayer.transform == null)
            return Vector3.zero;

        Vector3 vec3Dir = Vector3.zero;
        Transform kTrans = m_kPlayer.transform;

        if (MainPlayerControlInputReceiver.singleton != null &&
            MainPlayerControlInputReceiver.singleton.enabled == true)
        {
            vec3Dir = MainPlayerControlInputReceiver.singleton.GetMoveDir();
        }
        if (vec3Dir == Vector3.zero && UIManager.HasPanel<PanelBattle>())
        {
            vec3Dir = UIManager.GetPanel<PanelBattle>().GetMoveDir();
        }

        vec3Dir = kTrans.localToWorldMatrix.MultiplyVector(vec3Dir);
        vec3Dir.y = 0;
        vec3Dir.Normalize();

        return vec3Dir;
    }

    private float GetSpeed(Vector3 vec3MoveDir)
    {
        float speed = 0;

        bool crouch = m_kPlayer.status.crouch;
        //LocalPlayerData pd = m_kPlayer.localPlayerData;
        string dirName = VectorHelper.GetDir(m_kPlayer.transform.forward, Vector3.Normalize(vec3MoveDir));
        switch (dirName)
        {
            case VectorHelper.DIR_FORWARD:
                speed = crouch ? m_kPlayer.crouchMoveForwardSpeed : m_kPlayer.standMoveForwardSpeed;
                break;
            case VectorHelper.DIR_BACK:
                speed = crouch ? m_kPlayer.crouchMoveBackSpeed : m_kPlayer.standMoveBackSpeed;
                break;
            case VectorHelper.DIR_LEFT:
            case VectorHelper.DIR_RIGHT:
                speed = crouch ? m_kPlayer.crouchMoveSideSpeed : m_kPlayer.standMoveSideSpeed;
                break;
        }

#if UNITY_EDITOR
        speed *= GameSetting.MPSpeedFactor;
#endif

        moveSpeed = speed;

        if (GetCtrlMark(GameEvent.INPUT_MOVE_SLOW))
        {
            float factor = m_kPlayer.configData.MoveSlowSpeedFactor;

            DebugInfo = "MoveSlow " + factor + " " + speed * factor;
            return speed * factor;
        }

        DebugInfo = dirName + " " + crouch + " " + speed;

        return speed;
    }

    protected override void Update()
    {
        if (m_kPlayer == null || m_kPlayer.released == true || m_kPlayer.alive == false)
            return;

        PlayerStatus status = m_kPlayer.status;
        float deltaTime = Time.smoothDeltaTime;

        UpdateGravity();

        UpdateFly();

        UpdateJump();

        UpdateRush();

        UpdateHitBackDistance();

        UpdateHitBackSpeed();
        m_kPlayer.skillMgr.flm.UpdateFly();
        if (m_bPenetrate == false && !m_outMap)
        {
            Vector3 playerPos = m_kPlayer.position;

            if(playerPos != Vector3.zero)
            {
                int index = -1;
                for (int i = 0; i < m_prePosList.Count; i++)
                {
                    if (m_prePosList[i] == playerPos)
                    {
                        index = i;
                        break;
                    }
                }

                if (index < 0)
                {
                    if (m_prePosList.Count == 100)
                        m_prePosList.RemoveAt(0);
                    m_prePosList.Add(playerPos);
                }
                m_lastRightPos = playerPos;
            }
        }
        else
        {
            m_lastPenetratePos = m_lastRightPos;
        }

        OnMoveKeyPressUpdate();

        CheckPlayerPenetrate();

        m_fAimPointRayTestTick += deltaTime;
        if (m_fAimPointRayTestTick >= GameSetting.AIM_POINT_RAYTEST_INTERVAL)
        {
            m_fAimPointRayTestTick = 0f;
            if (!WorldManager.singleton.isHideModeOpen) //躲猫猫模式不开启检测
            {
                AimPointRayTest();
            }
            AutoAim.AutoAimTargetRayTest();
        }

        AutoAim.LookFollow();

        // Camera Ani
        if (status.fire || status.reload || status.switchWeapon)
        {
            if (status.move && m_kPlayer.cameraMoveAniPlaing == true)
                m_kPlayer.StopCameraWalkAni();

            if (!status.move && m_kPlayer.cameraIdleAniPlaying == true)
                m_kPlayer.StopCameraIdleAni();
        }
        else
        {
            if (status.move && m_kPlayer.cameraMoveAniPlaing == false)
                m_kPlayer.PlayCameraWalkAni();

            if (!status.move && m_kPlayer.cameraIdleAniPlaying == false)
                m_kPlayer.PlayCameraIdleAni();
        }

        m_kPlayer.SendPos();

        // Check Pos Cheat
        if (Time.timeSinceLevelLoad - m_posCheckTime.val >= 2f)
        {
            m_posCheckTime.val = Time.timeSinceLevelLoad;
            if (m_kPlayer.alive)
            {
                float dis = Vector3.SqrMagnitude(m_kPlayer.ctrlPos - m_kPlayer.position);
                if (dis >= 4 * 4)
                {
                    AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_Pos, dis.ToString());
                }

                if(GameSetting.openCheckPosMsg)
                    Logger.Log("CheckPos => " + "dis = " + dis + " CtlPos = " + (Vector3)m_kPlayer.ctrlPos + " Pos = " + m_kPlayer.position);
            }
        }

        //if (Input.GetKeyDown(KeyCode.U))
        //{
        //    _temp = transform.position;
        //    Logger.Log("U " + _temp);
        //}
        //else if (Input.GetKeyDown(KeyCode.I))
        //{
        //    Logger.Log("I ");

        //    transform.position = _temp;
        //    MainPlayer.singleton.m_moveSpeedData.ctrlPos.SetPos(_temp);

        //    VertificateUtil.GetVtfCode();
        //}
        //else if (Input.GetKeyDown(KeyCode.O))
        //{
        //    VertificateUtil.GetVtfCode();
        //}
    }

    Vector3 _temp;

    //protected void LateUpdate()
    //{
    //    if (m_kPlayer != null && m_kPlayer.alive)
    //        m_kPlayer.SendPos();
    //}

    public void HitBackDistance(float distance, Vector3 moveDir, float speed)
    {
        if (m_kPlayer.alive == false || distance <= 0 || speed <= 0)
            return;

        moveDir.y = 0;
        HitBackDistanceInfo hbi = new HitBackDistanceInfo { distance = distance, dir = Vector3.Normalize(moveDir), speed = speed };
        m_listHBDI.Add(hbi);

        m_hitBackDistance = true;
    }

    public void UpdateHitBackDistance()
    {
        if (m_hitBackDistance == false)
            return;

        if (m_listHBDI.Count == 0)
        {
            m_hitBackDistance = false;
            return;
        }

        for (int i = m_listHBDI.Count - 1; i >= 0; i--)
        {
            HitBackDistanceInfo hbi = m_listHBDI[i];
            float speed = hbi.speed * Time.smoothDeltaTime;
            if (speed > hbi.distance)
                speed = hbi.distance;

            m_kPlayer.Move(hbi.dir * speed);

            hbi.distance -= speed;
            if (hbi.distance <= 0)
                m_listHBDI.RemoveAt(i);
        }
    }

    public void HitBackSpeed(int hiter, float speed, float time)
    {
        if (m_kPlayer.alive == false || speed <= 0 || time <= 0)
            return;

        if (m_HBSI[hiter % MAX_PLAYER_COUNT] == null)
            m_HBSI[hiter % MAX_PLAYER_COUNT] = new HitBackSpeedInfo();

        m_HBSI[hiter % MAX_PLAYER_COUNT].speed = speed;
        m_HBSI[hiter % MAX_PLAYER_COUNT].time = time;

        m_hitBackSpeed = true;
    }




    public void UpdateHitBackSpeed()
    {
        if (m_hitBackSpeed == false)
            return;

        m_fCurHitBackSpeed = 0;

        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        {
            if (m_HBSI[i] == null || m_HBSI[i].time <= 0)
                continue;
            m_fCurHitBackSpeed += m_HBSI[i].speed;
            m_HBSI[i].time -= Time.smoothDeltaTime;
        }

        if (m_fCurHitBackSpeed == 0)
            m_hitBackSpeed = false;
    }

    public void RemoveHitBack()
    {
        m_listHBDI.Clear();
        m_hitBackSpeed = false;
    }

    override protected void UpdateGravity()
    {
        // 不要放在FixedUpdate是调用，角色下坡会一卡一卡，检测频率太低了

        if (GameSetting.openGravity == false||m_kPlayer.status.fly)
            return;

        float rayDis = 0;

        //bool isGrounded = m_kPlayer.IsGrounded(out rayDis);
        bool isRayCheckGrounded = m_kPlayer.IsGrounded(out rayDis);
        bool isGrounded = isRayCheckGrounded;
        if (m_kPlayer.characterControler.enabled == true)
        {

            float curPosY = m_kPlayer.position.y;
            // IsGrounded 与 CharacterController.isGrounded 同时判断，防止下坡时一卡一卡，自己写的IsGrounded计算没有CharacterController.isGrounded及时
            if (isRayCheckGrounded && (m_flastPosY == curPosY || m_kPlayer.characterControler.isGrounded))
                isGrounded = true;
            else
            {
                isGrounded = false;
            }
            m_flastPosY = curPosY;
        }

        if (SceneManager.singleton.battleSceneLoaded && !m_kPlayer.ForbidGravity && jumpStatus != JUMP_STATUS_UP && !isGrounded && !m_kPlayer.status.climb)
        {
            if (rayDis == float.MaxValue)
            {
                m_outMap = true;
                m_floatTime += Time.smoothDeltaTime;
                if (m_floatTime >= FLOAT_WAIT_TIME)
                {
                    m_floatTime = 0;
                    m_kPlayer.position = m_lastPenetratePos + CORRECT_POS_OFFSET;
                    Logger.Error("玩家跑到地图外，拉回到上一个正常的点 " + m_lastPenetratePos);
                }
                return;
            }
            else
            {
                m_outMap = false;
            }

            gravityTick += Time.smoothDeltaTime;
            float fallSpeed = -gravity * gravityTick;
            
            m_kPlayer.Move(new Vector3(0, fallSpeed * Time.smoothDeltaTime, 0), true);
        }
        else
        {
            //if (m_bIsGroundedBefore == false && isRayCheckGrounded == true && m_fGravityTick > GameSetting.MAIN_PLAYER_CAMERA_MOVE_GRAVITY_TIME)
            //{
            //    SceneManager.singleton.PlaySceneCameraMoveDown();
            //}
            gravityTick = 0;
        }
    }


    public void UpdateFly()
    {
        if (m_kPlayer.status.fly == false)
            return;
        if (m_kPlayer.skillMgr.flm.flyup)
        {
            m_kPlayer.Move(new Vector3(0, m_kPlayer.skillMgr.flm.upSpeed*Time.smoothDeltaTime, 0), false, true);
        }
        else if (m_kPlayer.skillMgr.flm.flydown)
        {
            m_kPlayer.Move(new Vector3(0, (-1)*m_kPlayer.skillMgr.flm.downSpeed * Time.smoothDeltaTime, 0), false, true);
            if (m_kPlayer.IsGrounded())
            {
                m_kPlayer.status.fly = false;
                m_kPlayer.skillMgr.flm.flyup = false;
                m_kPlayer.skillMgr.flm.flydown = false;
            }
        }

    }

    override protected void UpdateJump()
    {
        if (m_kPlayer.status.jump == false)
            return;

        if (MainPlayer.singleton.ForbidJump)
            return;

        if (jumpStatus == JUMP_STATUS_UP)
        {
            jumpUpTick += Time.smoothDeltaTime;

            float speed = jumpStartSpeed - gravity * jumpUpTick;
            if (speed <= 0)
            {
                jumpStatus = JUMP_STATUS_DOWN;
                jumpUpTick = 0;
                m_kPlayer.JumpTopPoint();
                if (GameSetting.jumpUpMsg)
                    Logger.Log("Jump_UP Finished");
                return;
            }

            if (GameSetting.openJumpUp)
                m_kPlayer.Move(new Vector3(0, speed * Time.smoothDeltaTime, 0), true, true);

            if(GameSetting.jumpUpMsg)
                Logger.Log("JumpUp=> " + (speed * Time.smoothDeltaTime) + " speed = " + speed + " jss = " + jumpStartSpeed + " g = " + gravity + " tick = " + jumpUpTick + " dt = " + Time.smoothDeltaTime + " move = " );

            if (jumpStartSpeed > 18)   // 按公式计算，最大值不会超过15，否则为作弊
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_JumpStartSpeed, jumpStartSpeed.ToString());
        }
        else if (jumpStatus == JUMP_STATUS_DOWN)
        {
            if (m_kPlayer.IsGrounded() || m_kPlayer.status.climb)
            {
                jumpStatus = JUMP_STATUS_COMPLETE;
                JumpComplete();
            }
        }

        if(jumpStatus < 0 || jumpStatus > JUMP_STATUS_COMPLETE)
        {
            AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_JumpStatus, jumpStatus.ToString());
        }

    }


    private float m_enterDeathBoxTick;
    private void OnTriggerStay(Collider hitCollider)
    {
        GameObject goHitGO = hitCollider.gameObject;

        //Logger.Log("OnTriggerStay " + hitCollider.name);

        if (goHitGO.layer == GameSetting.LAYER_VALUE_BUILDING || goHitGO.layer == GameSetting.LAYER_VALUE_AIR_WALL)
        {
            if (m_listHitCollider.IndexOf(hitCollider) == -1)
            {
                if (m_listHitCollider.Count >= HIT_COLLIDER_NUM)
                {
                    m_listHitCollider.RemoveAt(0);
                }
                m_listHitCollider.Add(hitCollider);
            }
        }
        else if (goHitGO.layer == GameSetting.LAYER_VALUE_DEATH_BOX)
        {
            if (m_enterDeathBoxTick == 0)
            {
                m_enterDeathBoxTick = Time.realtimeSinceStartup;
                Logger.Log("EnterDathBox");
            }
            else if (m_enterDeathBoxTick != -1 && Time.realtimeSinceStartup - m_enterDeathBoxTick >= 1.0f)
            {
                m_enterDeathBoxTick = -1;
                NetLayer.Send(new tos_fight_enter_deathbox() { id = m_kPlayer.PlayerId });
                Logger.Log("EnterDeathBox TimeOut");
            }
        }
    }

    private void OnTriggerExit(Collider hitCollider)
    {
        if (hitCollider.gameObject.layer == GameSetting.LAYER_VALUE_DEATH_BOX)
        {
            m_enterDeathBoxTick = 0;
            Logger.Log("ExitDeathBox");
        }
    }

    private void CheckPlayerPenetrate()
    {
        if (Time.realtimeSinceStartup - m_penetrateCheckTime < PENETRATE_CHECK_TIME)
            return;

        m_penetrateCheckTime = Time.realtimeSinceStartup;

        bool bPenetrate = false;
        string name = "";
        for (int i = 0; i < m_listHitCollider.Count; i++)
        {
            Collider collider = m_listHitCollider[i];
            if (collider.bounds.Intersects(m_kPlayer.characterControler.bounds))
            {
                bPenetrate = true;
                name = collider.name;
            }
        }

        if (bPenetrate)
        {
            if (m_prePosList.Count == 0)
            {
                //m_kPlayer.position = m_kPlayer.bornPos;
                if (m_lastPenetratePos != Vector3.zero)
                {
                    m_kPlayer.position = m_lastPenetratePos + CORRECT_POS_OFFSET;
                    Logger.Warning("CheckPlayerPenetrate 拉回最后一个正确的位置 " + name + " ;" + m_lastPenetratePos);
                }
            }
            else
            {
                int index = m_prePosList.Count - 1;
                Vector3 prePos = m_prePosList[index];
                if(prePos != Vector3.zero)
                    m_kPlayer.position = prePos + CORRECT_POS_OFFSET;

                Logger.Warning("CheckPlayerPenetrate 拉回上一个未穿透位置 " + name + ";" + prePos);
                m_prePosList.RemoveAt(index);
            }
        }

        m_bPenetrate = bPenetrate;
        m_listHitCollider.Clear();
    }


    #region Get Set

    public float moveSpeed
    {
        get { return m_moveSpeed.val; }
        set {
            m_kPlayer.clientMoveSpeed = value;
            m_moveSpeed.val = value;
        }
    }

    private float jumpUpTick
    {
        get{ return m_jumpUpTick.val; }
        set{ m_jumpUpTick.val = value;}
    }

    private float jumpStartSpeed
    {
        get { return m_jumpStartSpeed.val; }
        set { m_jumpStartSpeed.val = value; }
    }

    public float gravity
    {
        get
        {
            float gravity = GameSetting.GRAVITY;
            if (jumpStatus == JUMP_STATUS_UP)
                gravity = GameSetting.GRAVITY_JUMPUP;
            return gravity;
        }
    }

    public float gravityTick
    {
        get { return m_gravityTick.val; }
        set { m_gravityTick.val = value; }
    }

    #endregion

    #region OnKey Handle

    private void OnFireKey(bool keyDown)
    {
        if (canControl == false)
            return;
        //Logger.Error("who on fire key");
        if (m_kPlayer.isDmmModel)
        {
            if (!MainPlayer.singleton.IsHideView && keyDown)
            {
                if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
                {
                    //PC版上 躲猫猫模式 只能用鼠标锁定不能解除锁定  解除锁定需用快捷键E
                    if (m_kPlayer.IsFrezee == false)
                    {
                        m_kPlayer.SetHideView(true);
                    }
                }
                else
                {
                    m_kPlayer.SetHideView(!m_kPlayer.IsFrezee);
                }
            }
        }
        else
        {
            if (MainPlayer.singleton.ForbidFire)
            {
                SetCtrlMark(GameEvent.INPUT_FIRE, false);
                return;
            }

            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

            SetCtrlMark(GameEvent.INPUT_FIRE, keyDown);

            if (m_kPlayer.weaponMgr != null && m_kPlayer.weaponMgr.curWeapon != null)
            {
                if (keyDown)
                {
                    m_kPlayer.weaponMgr.curWeapon.OnFireKeyDown();
                    if (m_kPlayer.weaponMgr.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_GRENADE)
                    {
                        //扔雷操作后  特殊处理 将不能再选择背包
                        PlayerBattleModel.Instance.bag_selected = true;
                    }
                    GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, true);
                }
                else
                {
                    m_kPlayer.weaponMgr.curWeapon.OnFireKeyUp();
                    GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, false);
                }
            }
        }

    }

    private void OnFireSubKey(bool keyDown)
    {
        if (canControl == false)
            return;

        if (MainPlayer.singleton.ForbidFire)
        {
            SetCtrlMark(GameEvent.INPUT_FIRE, false);
            return;
        }

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        SetCtrlMark(GameEvent.INPUT_FIRE, keyDown);

        if (m_kPlayer.weaponMgr != null && m_kPlayer.weaponMgr.subWeapon != null)
        {
            if (keyDown)
            {
                m_kPlayer.weaponMgr.subWeapon.OnFireKeyDown();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, true);
            }
            else
            {
                m_kPlayer.weaponMgr.subWeapon.OnFireKeyUp();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, false);
            }
        }
    }

    private void OnZoom(bool keyDown)
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (keyDown)
            m_kPlayer.weaponMgr.curWeapon.OnZoomIn();
        else
            m_kPlayer.weaponMgr.curWeapon.OnZoomOut();
    }

    private void OnMoveSlowKey(bool keyDown)
    {

        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);
        SetCtrlMark(GameEvent.INPUT_MOVE_SLOW, keyDown);
        m_kPlayer.SlowMove = keyDown;
    }

    private void OnMoveKey(bool keyDown)
    {
        if (MainPlayer.singleton.ForbidMove)
        {
            SetCtrlMark(GameEvent.INPUT_MOVE, false);
            return;
        }

        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        SetCtrlMark(GameEvent.INPUT_MOVE, keyDown);

        if (keyDown)
        {
            if (m_kPlayer.status.jump == true)
                m_strJumpMoveOrder = JUMP_MOVE_ORDER_JUMP_FIRST;
            else
            {
                if (m_kPlayer.status.climb)
                    m_moveDirBeforeJump = m_goLadder.transform.forward;
                else
                    m_moveDirBeforeJump = GetMoveDir();
                m_strJumpMoveOrder = JUMP_MOVE_ORDER_MOVE_FIRST;
            }
        }
        else
        {
            //m_kBP.StopCameraWalkAni();
            moveSpeed = 0;
            m_kPlayer.status.move = false;
        }
    }

    private void OnMoveKeyPressUpdate()
    {
        bool inputMove = GetCtrlMark(GameEvent.INPUT_MOVE);

        if (inputMove == false && m_bIsForceMove == false)
            return;

        if (MainPlayer.singleton.ForbidMove)
            return;

        PlayerStatus status = m_kPlayer.status;

        if (status.freeze)
            status.move = false;
        else
            status.move = true;

        if (status.move == true)
        {
            Vector3 vec3MoveDir = GetMoveDir();
            float speedRate = 1;
            float fSpeed = 0;

            if (m_kPlayer.status.jump)
            {
                if (m_bBounceForceMove)
                    vec3MoveDir = m_vecBounceForceMoveDir;
                else if (!m_bIsBouncing)
                {
                    if (!inputMove || m_bIsJumpPressReverse || Vector3.Dot(vec3MoveDir, m_moveDirBeforeJump) < 0)
                    {
                        m_bIsJumpPressReverse = true;
                        m_fReverseMoveTick += Time.deltaTime;
                        speedRate -= m_fReverseMoveTick / GameSetting.PLAYER_JUMP_MOVE_SLOW_TIME;
                        if (speedRate <= 0)
                        {
                            m_kPlayer.status.move = false;
                            return;
                        }
                    }
                    vec3MoveDir = m_moveDirBeforeJump;
                }
            }
            else
            {
                if (status.climb && !m_kPlayer.characterControler.isGrounded)
                {
                    float y = -Vector3.Dot(vec3MoveDir, m_goLadder.transform.forward);
                    vec3MoveDir = vec3MoveDir - m_goLadder.transform.forward;
                    vec3MoveDir = new Vector3(vec3MoveDir.x, y, vec3MoveDir.z);
                }
            }

            if (m_bBounceForceMove)
            {
                fSpeed = m_fBounceForceSpeed;
                DebugInfo = "BFM = " + fSpeed;
            }               
            else
            {
                fSpeed = GetSpeed(vec3MoveDir) * speedRate;
                DebugInfo += " " + speedRate;
            }

            int a = 15;
            int b = 16;
            int c = 15;
            b = a = c;
            if (!m_bBounceForceMove && (fSpeed > 16 || fSpeed < 0))
            {
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_Speed, fSpeed.ToString());
            }

            if (m_hitBackSpeed)
            {
                var hitBackSpeedRate = m_fCurHitBackSpeed / m_kPlayer.standMoveForwardSpeed;
                fSpeed -= fSpeed * hitBackSpeedRate;
                if (fSpeed < m_decelerationLimitSpeed)
                    fSpeed = m_decelerationLimitSpeed;
            }

            // Walk的时候Jump
            if (status.jump == true && m_strJumpMoveOrder == JUMP_MOVE_ORDER_MOVE_FIRST)
            {
                Vector3 vec3JumpMove = fSpeed * Time.smoothDeltaTime * vec3MoveDir;
                m_kPlayer.Move(vec3JumpMove);
            }

            // Jump时Walk
            if (status.jump == true && m_strJumpMoveOrder == JUMP_MOVE_ORDER_JUMP_FIRST)
            {
                Vector3 vec3JumpMove = fSpeed * Time.smoothDeltaTime * vec3MoveDir;
                m_kPlayer.Move(vec3JumpMove);
            }

            // Walk
            if (status.jump == false)
            {
                Vector3 vec3Move = fSpeed * Time.smoothDeltaTime * vec3MoveDir;

                DebugInfo += " " + vec3Move.ToStrFloat();

                m_kPlayer.Move(vec3Move);
            }
        }
    }

    private void OnJumpKeyDown()
    {
        if (canControl == false)
            return;

        if (MainPlayer.singleton.ForbidJump)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (m_kPlayer.status.move == true)
            m_strJumpMoveOrder = JUMP_MOVE_ORDER_MOVE_FIRST;
        else
            m_strJumpMoveOrder = JUMP_MOVE_ORDER_JUMP_FIRST;

        if (m_kPlayer.status.crouch == true)
        {
            OnStandKeyDown();
        }
        else if (m_kPlayer.status.jump == false&&!m_kPlayer.canFly)
        {
            m_kPlayer.Jump();

            if (m_kPlayer.status.jump)
            {
                m_bIsForceMove = m_kPlayer.status.move;
                jumpStatus = JUMP_STATUS_UP;
                if (m_kPlayer.status.climb)
                    m_moveDirBeforeJump = m_goLadder.transform.forward;
                else
                    m_moveDirBeforeJump = GetMoveDir();

                jumpStartSpeed = Mathf.Sqrt(2 * GameSetting.GRAVITY_JUMPUP * m_kPlayer.jumpMaxHeight);
            }
        }
        else if (m_kPlayer.canJump2 && m_kPlayer.status.jump2 == false&&!m_kPlayer.canFly)
        {
            m_kPlayer.Jump2();
            if (m_kPlayer.status.jump2)
            {
                m_bIsForceMove = m_kPlayer.status.move;
                m_bIsJumpPressReverse = false;
                jumpUpTick = 0;
                jumpStatus = JUMP_STATUS_UP;
                m_moveDirBeforeJump = GetMoveDir();
                jumpStartSpeed = Mathf.Sqrt(2 * GameSetting.GRAVITY_JUMPUP * m_kPlayer.jump2MaxHeight);
            }
        }
        else if (m_kPlayer.canFly)
        {
            m_kPlayer.Fly();
            m_kPlayer.skillMgr.flm.flydown = false;
            m_kPlayer.skillMgr.flm.flyup = true;
            GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_UP);
            //Logger.Error("开始飞");
        }
    }

    private void OnJumpKeyUp()
    {
        if (canControl == false)
            return;

        if (MainPlayer.singleton.ForbidJump)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);
        if (m_kPlayer.status.fly)
        {
            m_kPlayer.skillMgr.flm.flydown = true;
            m_kPlayer.skillMgr.flm.flyup = false;
            GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_END);
            //Logger.Error("结束飞");
        }
    }


    private void OnStandKeyDown()
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (m_kPlayer.status.stand == false)
        {
            m_kPlayer.Stand(true);
        }
    }

    private void OnCrouchKeyDown()
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (m_kPlayer.status.crouch == false)
        {
            m_kPlayer.Crouch();
        }
    }

    private void OnReloadKeyDown()
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        PlayerStatus kPlayerStatus = m_kPlayer.status;
        if (!(kPlayerStatus.reload || kPlayerStatus.switchWeapon || kPlayerStatus.fire || GetCtrlMark(GameEvent.INPUT_FIRE)))
        {
            m_kPlayer.curWeapon.ReloadAmmo();
        }
    }

    private void OnSwitchKeyDown(string strWeaponName)
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        PlayerStatus kPlayerStatus = m_kPlayer.status;
        if (!(kPlayerStatus.fire || GetCtrlMark(GameEvent.INPUT_FIRE)))
        {
            m_kPlayer.curWeapon.StopFire();
            //Logger.Log("手动切枪 " + strWeaponName);
        }
        m_kPlayer.SwitchWeapon(strWeaponName, true, false);
    }

    private void OnEnableGunScope(bool bEnable)
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (bEnable)
            m_kPlayer.weaponMgr.curWeapon.OnZoomIn();
        else
            m_kPlayer.weaponMgr.curWeapon.OnZoomOut();
    }

    private void OnFireGrande(bool keyDown)
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (keyDown)
        {
            if (m_kPlayer.SwitchGrenade())
            {
                SetCtrlMark(GameEvent.INPUT_FIRE_GRENADE, true);
                m_kPlayer.curWeapon.OnFireKeyDown();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, true);
            }
        }
        else
        {
            if (GetCtrlMark(GameEvent.INPUT_FIRE_GRENADE))
            {
                SetCtrlMark(GameEvent.INPUT_FIRE_GRENADE, false);
                m_kPlayer.curWeapon.OnFireKeyUp();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, false);
            }
        }
    }

    private void OnFireFlashBomb(bool keyDown)
    {
        if (canControl == false)
            return;

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BE_CONTROLLED);

        if (keyDown)
        {
            if (m_kPlayer.SwitchFlashBomb())
            {
                SetCtrlMark(GameEvent.INPUT_FIRE_FLASHBOMB, true);
                m_kPlayer.curWeapon.OnFireKeyDown();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, true);
            }
        }
        else
        {
            if (GetCtrlMark(GameEvent.INPUT_FIRE_FLASHBOMB))
            {
                SetCtrlMark(GameEvent.INPUT_FIRE_FLASHBOMB, false);
                m_kPlayer.curWeapon.OnFireKeyUp();
                GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, m_kPlayer, false);
            }
        }
    }

    private void OnCancleFire()
    {
        if (m_kPlayer.curWeapon != null)
            m_kPlayer.curWeapon.OnCancleFire();
    }

    public void LookRotate(float xDelta, float yDelta)
    {
        if (canControl == false)
            return;

        if (m_kPlayer.isDmmModel)
            m_kPlayer.DmmLookRotate(xDelta, yDelta);
        else
            m_kPlayer.KeyboardLookRotate(xDelta, yDelta);

    }

    public void TouchLookRotate(float pitch, float yaw)
    {
        if (canControl == false)
            return;
        if (m_kPlayer.isDmmModel)
            m_kPlayer.DmmTouchLookRotate(pitch, yaw);
        else
            m_kPlayer.TouchLookRotate(pitch, yaw);
    }
    #endregion


}

