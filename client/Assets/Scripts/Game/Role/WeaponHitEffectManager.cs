﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponHitEffectManager 
{
    public const string TYPE_UI = "ui";
    public const string TYPE_BODY_COLOR = "bodyColor";


    private BasePlayer m_ower;
    private bool m_isMainPlayer;
    private Dictionary<string, List<WeaponHitEffect>> m_cache = new Dictionary<string, List<WeaponHitEffect>>();
    private WeaponHitEffect m_curEffect;


    public WeaponHitEffectManager(BasePlayer player)
    {
        m_ower = player;
        m_isMainPlayer = player is MainPlayer;
    }

    public void Release()
    {
        foreach(KeyValuePair<string ,List<WeaponHitEffect>> kvp in m_cache)
        {
            for(int i=0; i<kvp.Value.Count; i++)
            {
                kvp.Value[i].Release();
            }
            kvp.Value.Clear();
        }
        m_cache.Clear();

        if(m_curEffect != null)
            m_curEffect.Release();
    }

    public void Update()
    {
        if(m_curEffect != null)
            m_curEffect.Update();
    }

    public void StopEffect()
    {
        if (m_curEffect != null)
            m_curEffect.Stop();
    }

    private WeaponHitEffect GetEffect(string type, string assetName = "")
    {
        WeaponHitEffect effect = null;
        List<WeaponHitEffect> list;
        m_cache.TryGetValue(type, out list);
        if(list == null)
        {
            list = new List<WeaponHitEffect>();
            m_cache.Add(type, list);
        }
        if (assetName.IsNullOrEmpty() == false)
        {
            for(int i=0; i<list.Count; i++)
            {
                if(list[i].assetName == assetName)
                {
                    effect = list[i];
                    list.RemoveAt(i);
                    break;
                }
            }
        }
        else
        {
            if (list.Count > 0)
            {
                effect = list[0];
                list.RemoveAt(0);
            }
        }

        if(effect == null)
        {
            switch(type)
            {
                case WeaponHitEffectManager.TYPE_UI:
                    effect = new WeaponHitEffect_UI();
                    effect.Init(this, assetName);
                    break;
                case WeaponHitEffectManager.TYPE_BODY_COLOR:
                    effect = new WeaponHitEffect_BodyColor();
                    effect.Init(this);
                    break;
            }
        }

        return effect;
    }


    internal void Reclaim(WeaponHitEffect effect)
    {
        List<WeaponHitEffect> list;
        m_cache.TryGetValue(effect.type, out list);
        if (list == null)
        {
            list = new List<WeaponHitEffect>();
            m_cache.Add(effect.type, list);
        }

        if (list.Contains(effect) == false)
            list.Add(effect);
    }

    public void BeHit(string weaponID)
    {
        int id = 0;
        if (int.TryParse(weaponID, out id) == false)
            return;

        ConfigItemWeaponLine configWeapon = GlobalConfig.configWeapon.GetLine(id);
        if (configWeapon == null)
            return;

        if (m_isMainPlayer)
            MPBeHit(configWeapon);
        else
            OPBeHit(configWeapon);
    }

    private void MPBeHit(ConfigItemWeaponLine configWeapon)
    {
        if (configWeapon.MPHitEffect.IsNullOrEmpty() != true && configWeapon.MPHitEffectTime > 0)
        {
            if (m_curEffect != null)
                m_curEffect.Stop();

            Transform transParent = UIManager.GetPanel<PanelBattle>().m_transScreenEffect;

            m_curEffect = GetEffect(TYPE_UI, configWeapon.MPHitEffect);
            m_curEffect.Show(transParent, configWeapon.MPHitEffectTime);
        }
    }

    private void OPBeHit(ConfigItemWeaponLine configWeapon)
    {
        if (configWeapon.OPHitEffect.Length >= 3 && configWeapon.OPHitEffectTime > 0)
        {
            if (m_curEffect != null)
                m_curEffect.Stop();

            Color color = new Color(){
                r = configWeapon.OPHitEffect[0] / 255,
                g = configWeapon.OPHitEffect[1] / 255,
                b = configWeapon.OPHitEffect[2] / 255,
                a = 1
            };

            m_curEffect = GetEffect(TYPE_BODY_COLOR);
            m_curEffect.Show(color, configWeapon.OPHitEffectTime);
        }
    }

    public BasePlayer player
    {
        get { return m_ower; }
    }
}

class WeaponHitEffect
{
    protected WeaponHitEffectManager m_mgr;
    protected string m_type;
    protected bool m_released;
    protected float m_lifeTime;
    protected bool m_show;

    protected string m_assetName;

    virtual public void Release()
    {
        m_released = true;
    }

    virtual public void Init(WeaponHitEffectManager mgr)
    {
        m_mgr = mgr;
    }

    virtual public void Init(WeaponHitEffectManager mgr, string assetName)
    {
        m_mgr = mgr;
        m_assetName = assetName;
    }

    virtual public void Show(Transform parent, float time)
    {
        m_show = true;
        m_lifeTime = time;
    }

    virtual public void Show(Color color, float time)
    {
        m_show = true;
        m_lifeTime = time;
    }

    virtual public void Stop()
    {
        m_show = false;
        m_mgr.Reclaim(this);
    }

    virtual public void Update()
    {
        if (m_show == false)
            return;

        m_lifeTime -= Time.deltaTime;

        if (m_lifeTime <= 0)
            Stop();
    }

    public string assetName
    {
        get { return m_assetName; }
    }

    public string type
    {
        get { return m_type; }
    }
}

class WeaponHitEffect_BodyColor : WeaponHitEffect
{
    private OtherPlayer m_player;

    public WeaponHitEffect_BodyColor()
    {
        m_type = WeaponHitEffectManager.TYPE_BODY_COLOR;
    }

    public override void Init(WeaponHitEffectManager mgr)
    {
        base.Init(mgr);
        m_player = mgr.player as OtherPlayer;
    }

    public override void Stop()
    {
        base.Stop();
        m_player.FadeColor(false, Color.white);
    }

    public override void Show(Color color, float time)
    {
        base.Show(color, time);
        m_player.FadeColor(true, color);
    }
}

class WeaponHitEffect_UI : WeaponHitEffect
{
    private GameObject m_go;
    private bool m_loading;     
    private UIImgNumText m_uiNumText;

    public WeaponHitEffect_UI()
    {
        m_type = WeaponHitEffectManager.TYPE_UI;
    }

    override public void Release()
    {
        base.Release();
        if (m_go != null)
            ResourceManager.Destroy(m_go);

    }
    override public void Show(Transform parent, float time)
    {
        if (parent == null)
            return;

        base.Show(parent, time);

        if(m_go == null && m_loading == false)
        {
            m_loading = true;
            ResourceManager.LoadUI("UI/Battle/" + m_assetName, (go) =>
            {
                if (go == null || m_released)
                {
                    ResourceManager.Destroy(go);
                    return;
                }

                m_go = go;
                Transform transTime = go.transform.Find("Time");
                if (transTime != null)
                {
                    m_uiNumText = new UIImgNumText(transTime, "watch", AtlasName.Common);
                }

                if (m_show)
                {
                    m_go.TrySetActive(true);
                    m_go.transform.SetParent(parent, false);
                }
                else
                {
                    m_go.TrySetActive(false);
                }
            });
        }
        else if(m_go != null)
        {
            m_go.TrySetActive(true);
            m_go.transform.SetParent(parent, false);
        }
    }

    override public void Stop()
    {
        m_show = false;
        if (m_go != null)
            m_go.TrySetActive(false);
        m_mgr.Reclaim(this);
    }

    override public void Update()
    {
        base.Update();

        if (m_show == false)
            return;

        if (m_uiNumText != null)
            m_uiNumText.value = (int)m_lifeTime;
    }
    
}
