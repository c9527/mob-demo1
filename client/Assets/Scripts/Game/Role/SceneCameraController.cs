﻿using UnityEngine;
using System.Collections;


public class SceneCameraController : MonoBehaviour {

    static public SceneCameraController singleton;

    private bool m_run;
    private CameraModeBase m_cm;

 
    void Awake()
    {
        singleton = this;

        
    }
    public void Stop()
    {
        m_run = false;

        if (m_cm != null)
            m_cm.Stop();
    }

    public void FollowPlayer(BasePlayer player, float height, float distance, bool isDragable, bool isReLocal)
    {
        CameraModeFollowPlayer cmf = CameraModeFollowPlayer.GetInstance();
        cmf.Run(player, height, distance, isDragable, isReLocal);

        m_run = true;
        m_cm = cmf;
    }
    
    private void LookFixedPoint(Vector3 targetPos, Vector3 forward)
    {
        CameraModeFixPoint cmp = CameraModeFixPoint.GetInstance();
        cmp.Run(targetPos, forward, GameSetting.CAMERA_MODE_FIXEDPOS_HEIGHT, GameSetting.CAMERA_MODE_FIXEDPOS_DISTANCE);

        m_run = true;
        m_cm = cmp;
    }

    public void LookMainPlayerDeadPoint()
    {
        if (DeadPlayer.Instance.sceneCameraSlot == null)
            return;

        if (MainPlayer.singleton != null && !MainPlayer.singleton.isDying)
            return;

        WorldManager.singleton.CameraSwitcher.StopSwitching();

        //Vector3 pos = DeadPlayer.Instance.diePos;
        //Vector3 forward = DeadPlayer.Instance.forward;
        Vector3 pos = MainPlayer.singleton.diePos;
        Vector3 forward = MainPlayer.singleton.dieFace;

        LookFixedPoint(pos, forward);        
    }

    public void LookKiller(Vector3 centerPos, BasePlayer player)
    {
        CameraModeLookPlayer cmlp = CameraModeLookPlayer.GetInstance();
        cmlp.Run(centerPos, player, 0, GameSetting.configGameSettingLine.WatchDeadCameraDis);

        m_cm = cmlp;
        m_run = true;
    }

    public void StopControl()
    {
        if (m_cm != null)
            m_cm.Stop();
        m_run = false;
    }

    public void OnDrag(float x, float y)
    {
        if (m_run == false)
            return;

        if (m_cm != null)
            m_cm.OnDrag(x, y);
    }

    public void OnRotate(float x, float y)
    {
        if (m_run == false)
            return;

        if (m_cm != null)
            m_cm.OnRotate(x, y);
    }

    public void OnTouch(float pitch, float yaw)
    {
        if (m_run == false)
            return;

        if (m_cm != null)
            m_cm.OnTouch(pitch, yaw);
    }

    void LateUpdate()
    {
        if (m_run == false)
            return;

        if (m_cm != null)
            m_cm.LateUpdate();
    }

    public void SetEnableDrag(bool isEnable)
    {
        if (m_cm != null)
            m_cm.isDragable = isEnable;
    }

    public void SetDir(Vector3 dir)
    {
        if (m_cm != null)
            m_cm.SetDir(dir);
    }

    void OnDestroy()
    {
        if (SceneCamera.singleton != null)
        {
            SceneCamera.singleton.Release();
        }
    }

}

#region CameraMode

class CameraModeBase
{   
    protected float m_height;
    protected float m_distance;
    protected bool m_bIsDragable; //是否可拖

    virtual public void Stop()
    {
        
    }

    protected void Run(float height, float distance)
    {
        m_height = height;
        m_distance = distance;
    }

    virtual public void OnDrag(float x, float y)
    {

    }

    virtual public void LateUpdate() { }

    virtual public void OnRotate(float x, float y)
    {

    }

    virtual public void OnTouch(float pitch, float yaw)
    {
        OnDrag(yaw, -pitch);
    }

    virtual public bool isDragable
    {
        get{return m_bIsDragable;}
        set{m_bIsDragable = value;}
    }

    virtual public void SetDir(Vector3 dir)
    {

    }
}

/// <summary>
/// 队友跟随视角控制
/// </summary>
class CameraModeFollowPlayer : CameraModeBase
{
    static private CameraModeFollowPlayer ms_instance; 

    private BasePlayer m_player;
    private Transform m_cameraSlot;

    static public CameraModeFollowPlayer GetInstance()
    {
        if (ms_instance == null)
            ms_instance = new CameraModeFollowPlayer();

        return ms_instance;
    }

    public void Run(BasePlayer player, float height, float distance, bool isDragable, bool isReLocal)
    {
        float _distance = Mathf.Sqrt(Mathf.Pow(distance, 2) + Mathf.Pow(height, 2));

        base.Run(height, _distance);

        m_player = player;
        m_bIsDragable = isDragable;

        m_cameraSlot = player.sceneCameraSlot;

        SceneCamera.singleton.SetParent(m_cameraSlot, true);
        //SceneCamera.singleton.ResetLocalTransform();

        if (isReLocal)
        {
            m_cameraSlot.localPosition = new Vector3(0, height, -distance);
            OnRotate(0, GameSetting.FOLLOW_PLAYER_DEFAULT_VERTICAL_ROTATE);
        }

        m_cameraSlot.LookAt(m_player.headPos);

        //先调整一次位置
        LateUpdate();
    }

    public override void Stop()
    {
        base.Stop();

        if (SceneCamera.singleton.parent == m_cameraSlot)
            SceneCamera.singleton.SetParent(null);

        m_player = null;
        m_cameraSlot = null;
    }

    public override void OnDrag(float x, float y)
    {
        if (m_player == null || m_player.serverData == null || m_cameraSlot == null || !m_bIsDragable)
            return;

        Vector3 centerPos = m_player.headPos;
        Vector3 dir1 = m_cameraSlot.position - centerPos;

        // Rotate Vertical
        float angleY = Vector3.Angle(dir1, Vector3.up);
        float rotY = -y * GameSetting.CAMERA_DRAG_FACTOR;
        float minAngle = GameSetting.CAMERA_MODE_FOLLOW_ROTY_MIN;
        float maxAngle = GameSetting.CAMERA_MODE_FOLLOW_ROTY_MAX;
        if (angleY > minAngle && angleY < maxAngle || angleY <= minAngle && rotY < 0 || angleY >= maxAngle && rotY > 0)
        {
            if (-rotY + angleY > maxAngle)
                rotY = angleY - maxAngle;
            else if (-rotY + angleY < minAngle)
                rotY = angleY - minAngle;
            m_cameraSlot.RotateAround(centerPos, m_cameraSlot.right, rotY);
        }

        // Rotate Horizontal
        m_cameraSlot.RotateAround(centerPos, Vector3.up, x * GameSetting.CAMERA_DRAG_FACTOR);

        m_cameraSlot.LookAt(m_player.headPos);
    }

    public override void OnRotate(float x, float y)
    {
        if (m_player == null || (x == 0 && y == 0))
            return;

        Vector3 centerPos = m_player.headPos;
        Vector3 dir1 = m_cameraSlot.position - centerPos;

        // Rotate Vertical
        float angleY = Vector3.Angle(dir1, Vector3.up);
        float minAngle = GameSetting.CAMERA_MODE_FOLLOW_ROTY_MIN;
        float maxAngle = GameSetting.CAMERA_MODE_FOLLOW_ROTY_MAX;

        if (angleY > minAngle && angleY < maxAngle || angleY <= minAngle && y > 0 || angleY >= maxAngle && y < 0)
        {
            if (y + angleY > maxAngle)
                y = maxAngle - angleY;
            else if (y + angleY < minAngle)
                y = minAngle - angleY;
            m_cameraSlot.RotateAround(centerPos, -m_cameraSlot.right, y);
        }
        m_cameraSlot.LookAt(centerPos);

        // Rotate Horizontal
        m_player.transform.Rotate(Vector3.up, x);
    }

    public override void LateUpdate()
    {
        if (m_player.gameObject == null || m_cameraSlot == null)
            return;

        Vector3 dir = Vector3.Normalize(m_cameraSlot.position - m_player.headPos);

        SetDir(dir);
    }

    override public void SetDir(Vector3 dir)
    {
        Vector3 centerPos = m_player.headPos;
        RaycastHit kRH;
        Vector3 pos;

        if (Physics.Raycast(centerPos, dir, out kRH, m_distance, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_AIR_WALL))
        {
            pos = kRH.point - dir * GameSetting.CAMERA_RAYTEST_OFFSET;
            if (Vector3.Distance(kRH.point, pos) > Vector3.Distance(kRH.point, m_player.headPos))
            {
                pos = kRH.point;
            }
        }
        else
            pos = centerPos + dir * m_distance;

        m_cameraSlot.position = pos;
        m_cameraSlot.LookAt(centerPos);
    }
}

/// <summary>
/// 濒死状态下相机控制
/// </summary>
class CameraModeFixPoint : CameraModeBase
{
    static private CameraModeFixPoint ms_instance;

    private Vector3 m_centerPos;

    static public CameraModeFixPoint GetInstance()
    {
        if (ms_instance == null)
            ms_instance = new CameraModeFixPoint();

        return ms_instance;
    }

    public void Run(Vector3 centerPos, Vector3 forward, float deltaHeight, float distance)
    {
        base.Run(deltaHeight, distance);

        m_centerPos = centerPos + new Vector3(0, m_height, 0);

        float dis = Mathf.Sqrt(Mathf.Pow(m_distance, 2) - Mathf.Pow(m_height, 2));
        Vector3 dir = -Vector3.Normalize(forward);
        SceneCamera.singleton.position = centerPos + dir * dis;
        SceneCamera.singleton.LookAt(centerPos);
    }

    public override void OnDrag(float x, float y)
    {
        if (SceneCamera.singleton == null)
            return;

        Vector3 centerPos = m_centerPos;
        Vector3 dir1 = SceneCamera.singleton.position - centerPos;

        // Rotate Vertical
        float angleY = Vector3.Angle(dir1, Vector3.up);
        float minAngle = GameSetting.CAMERA_MODE_RESCURE_ROTY_MIN;
        float maxAngle = GameSetting.CAMERA_MODE_RESCURE_ROTY_MAX;

        if (angleY > minAngle && angleY < maxAngle || angleY <= minAngle && y > 0 || angleY >= maxAngle && y < 0)
        {
            if (y + angleY > maxAngle)
                y = maxAngle - angleY;
            else if (y + angleY < minAngle)
                y = minAngle - angleY;
            SceneCamera.singleton.RotateAround(centerPos, -SceneCamera.singleton.right, y);
        }

        // Rotate Horizontal
        SceneCamera.singleton.RotateAround(centerPos, Vector3.up, x * GameSetting.CAMERA_DRAG_FACTOR);

        SceneCamera.singleton.LookAt(centerPos);
    }

    public override void LateUpdate()
    {
        Vector3 dir = Vector3.Normalize(SceneCamera.singleton.position - m_centerPos);
        RaycastHit kRH;
        Vector3 pos;

        if (Physics.Raycast(m_centerPos, dir, out kRH, m_distance, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {
            pos = kRH.point - dir * GameSetting.CAMERA_RAYTEST_OFFSET;
            if (Vector3.Distance(kRH.point, pos) > Vector3.Distance(kRH.point, m_centerPos))
            {
                pos = kRH.point;
            }
        }
        else
            pos = m_centerPos + dir * m_distance;

        SceneCamera.singleton.position = pos;
        SceneCamera.singleton.LookAt(m_centerPos);
    }
}

class CameraModeLookPlayer : CameraModeBase
{
    static private CameraModeLookPlayer ms_instance;

    private Vector3 m_centerPos;
    private BasePlayer m_player;


    static public CameraModeLookPlayer GetInstance()
    {
        if (ms_instance == null)
            ms_instance = new CameraModeLookPlayer();

        return ms_instance;
    }

    public void Run(Vector3 centerPos, BasePlayer player, float deltaHeight, float distance)
    {
        base.Run(deltaHeight, distance);

        m_centerPos = centerPos;
        m_player = player;

        float dis = Mathf.Sqrt(Mathf.Pow(m_distance, 2) - Mathf.Pow(m_height, 2));
        Vector3 dir = -Vector3.Normalize(player.position - centerPos);

        SceneCamera.singleton.position = centerPos + dir * dis + new Vector3(0, m_height, 0);

        SceneCamera.singleton.LookAt(centerPos);
    }

    public override void Stop()
    {
        base.Stop();

        m_player = null;
    }

    public override void OnDrag(float x, float y)
    {
      
    }

    public override void LateUpdate()
    {
        if (m_player == null || m_player.serverData == null)
            return;

        Vector3 killerDir = VectorHelper.NormalizeXZ(m_player.position - m_centerPos);
        Vector3 dir1 = m_centerPos - SceneCamera.singleton.position;

        float angle = VectorHelper.AngleXZ(dir1, killerDir);
        if (angle >= 0)
        {
            //float maxSpeed = GameSetting.configGameSettingLine.WatchDeadCameraMaxRotAngle;
            //angle = angle > maxSpeed ? maxSpeed : angle;
            angle = Vector3.Cross(dir1, killerDir).y >= 0 ? angle : -angle;
            SceneCamera.singleton.RotateAround(m_centerPos, Vector3.up, angle);

            RaycastHit kRH;

            float distance = m_distance;
            if (Physics.Raycast(m_centerPos, -killerDir, out kRH, m_distance, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
            {
                distance = kRH.distance - GameSetting.CAMERA_RAYTEST_OFFSET;
                distance = distance < 0 ? 0 : distance;
            }

            SceneCamera.singleton.position = m_centerPos - killerDir * distance;
        }

        SceneCamera.singleton.LookAt(m_player.headPos);
        if (SceneCamera.singleton.cameraShake.isShake)
            SceneCamera.singleton.cameraShake.Shake();
    }
}

#endregion


