﻿using System.Collections.Generic;

/// <summary>
/// Player的状态有很多，其中有一部分是互斥的，同一时刻只能存在一个，有些状态间是可以共存的
/// 在设置一个互斥状态时，要删除与其互斥的状态,可以在SetValue()中操作
/// </summary>
public class PlayerStatus
{
    public const string STATUS_FREEZE = "freeze";
    public const string STATUS_DEAD = "dead";
    public const string STATUS_GOD = "god";             // 无敌

    public const string STATUS_IDLE = "idle";
    public const string STATUS_STAND = "stand";
    public const string STATUS_MOVE = "move";
    public const string STATUS_JUMP = "jump";
    public const string STATUS_CROUCH = "crouch";
    public const string STATUS_FIRE = "fire";
    public const string STATUS_AIM = "aim";
    public const string STATUS_RELOAD = "reload";
    public const string STATUS_SWITCH = "switch";
    public const string STATUS_JUMP2 = "jump2";         //二次跳跃

    public const string STATUS_CLIMB = "climb";
    public const string STATUS_POWERON = "poweron";
    public const string STATUS_FLY = "fly";

    protected Dictionary<string, EptInt> m_dicStatus;
    protected BasePlayer m_kBP;


    public PlayerStatus(BasePlayer kBP)
    {
        m_dicStatus = new Dictionary<string, EptInt>();
        m_kBP = kBP;
        //dead = true;
    }

    public void Release()
    {
        m_kBP = null;
    }

    public void ClearAllStatus()
    {
        m_dicStatus.Clear();
        //stand = true;
    }

    private bool GetValue(string strKey)
    {
        if (m_dicStatus.ContainsKey(strKey) == false)
            m_dicStatus.Add(strKey, -1);
        return m_dicStatus[strKey] == 1;
    }

    private void SetValue(string strKey, bool bValue)
    {
        if (m_dicStatus.ContainsKey(strKey) == false)
            m_dicStatus.Add(strKey, -1);

        m_dicStatus[strKey] = bValue ? 1 : -1;
    }

#region status get set
    public bool freeze
    {
        get { return GetValue(STATUS_FREEZE); }
        set { SetValue(STATUS_FREEZE, value); }
    }

    //public bool dead
    //{
    //    get { return GetValue(STATUS_DEAD); }
    //    set {
    //        if (value == true)
    //            ClearAllStatus();
    //        SetValue(STATUS_DEAD, value);                
    //    }
    //}

    public bool god
    {
        get { return GetValue(STATUS_GOD); }
        set { SetValue(STATUS_GOD, value); }
    }

    //public bool idle
    //{
    //    get { return GetValue(STATUS_IDLE); }
    //    set
    //    {
    //        SetValue(STATUS_IDLE, value);
    //    }
    //}

    public bool stand
    {
        get { return GetValue(STATUS_STAND); }
        set { 
            SetValue(STATUS_STAND, value);
            if(value == true)
            {                
                jump = false;
            }               
        }
    }

    public bool move
    {
        get { return GetValue(STATUS_MOVE); }
        set
        {
            SetValue(STATUS_MOVE, value);
        }
    }

    public bool jump
    {
        get { return GetValue(STATUS_JUMP); }
        set { SetValue(STATUS_JUMP, value); }
    }

    public bool jump2
    {
        get { return GetValue(STATUS_JUMP2); }
        set { SetValue(STATUS_JUMP2, value); }
    }

    public bool crouch
    {
        get { return GetValue(STATUS_CROUCH); }
        set { SetValue(STATUS_CROUCH, value); }
    }

    public bool fire
    {
        get { return GetValue(STATUS_FIRE); }
        set { SetValue(STATUS_FIRE, value); }
    }

    public bool aim
    {
        get { return GetValue(STATUS_AIM); }
        set { SetValue(STATUS_AIM, value); }
    }

    public bool reload
    {
        get { return GetValue(STATUS_RELOAD); }
        set { SetValue(STATUS_RELOAD, value); }
    }

    public bool switchWeapon
    {
        get { return GetValue(STATUS_SWITCH); }
        set { SetValue(STATUS_SWITCH, value); }
    }

    public bool climb
    {
        get { return GetValue(STATUS_CLIMB); }
        set { SetValue(STATUS_CLIMB, value); }
    }

    public bool poweron
    {
        get { return GetValue(STATUS_POWERON); }
        set { SetValue(STATUS_POWERON, value); }
    }

    public bool fly
    {
        get { return GetValue(STATUS_FLY); }
        set { SetValue(STATUS_FLY, value); }
    }
    #endregion

}
