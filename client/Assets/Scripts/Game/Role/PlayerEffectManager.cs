﻿using UnityEngine;
public class PlayerEffectManager
{
    protected BasePlayer m_player;

    //大小
    protected VtfFloatCls m_targetScale = new VtfFloatCls();          //当前倍率（模型预设大小）
    protected float m_curScale;             //当前的倍率
    protected float m_scaleLeftTime = -1;   //设置倍率剩余时间
    protected float m_scaleSpeed;           //大小变化速度

    //相机Fov
    protected float m_preFov;             //上一次的fov
    protected float m_targetFov;          //目标倍率（模型预设大小）
    protected float m_curFov;             //当前的倍率
    protected float m_fovLeftTime = -1;   //设置倍率剩余时间
    protected float m_fovSpeed;           //大小变化速度


    public PlayerEffectManager(BasePlayer player)
    {
        m_player = player;
        m_targetScale.val = 1.0f;
    }

    public void Update()
    {
        if (m_scaleLeftTime > 0)
        {
            float deltaTime = Time.deltaTime;
            m_scaleLeftTime -= deltaTime;
            if (m_scaleLeftTime <= 0)
            {
                m_curScale = m_targetScale.val;
                m_scaleLeftTime = -1;
            }
            else
            {
                m_curScale += m_scaleSpeed * deltaTime;
            }
            m_player.SetScale(m_curScale);
        }

        if (m_fovLeftTime > 0)
        {
            float deltaTime = Time.deltaTime;
            m_fovLeftTime -= deltaTime;
            if (m_fovLeftTime <= 0)
            {
                m_curFov = m_targetFov;
                m_fovLeftTime = -1;
            }
            else
            {
                m_curFov += m_fovSpeed * deltaTime;
            }
            m_player.SetFov(m_curFov);
        }
    }

    public void Release()
    {

    }

    public void SetBigHead(float headScale)
    {
        bigHeadScale = headScale;
        m_player.SetHeadScale(headScale);
    }

    public void SetScale(float _scale, bool isFade = false)
    {
        m_targetScale.val = _scale;
        if (isFade)
        {
            m_scaleSpeed = (m_targetScale.val - m_curScale) / GameSetting.PLAYER_SET_SCALE_TIME;
            m_scaleLeftTime = GameSetting.PLAYER_SET_ALPHA_TIME;
        }
        else
        {
            m_scaleSpeed = 0;
            m_scaleLeftTime = -1;
            m_curScale = m_targetScale.val;
            m_player.SetScale(m_targetScale.val);
        }
    }

    public void SetFov(float _fov, bool isFade = false)
    {
        m_preFov = m_targetFov;
        m_targetFov = _fov;
        if (isFade)
        {
            m_fovSpeed = (m_targetFov - m_curFov) / GameSetting.PLAYER_SET_SCALE_TIME;
            m_fovLeftTime = GameSetting.PLAYER_SET_ALPHA_TIME;
        }
        else
        {
            m_fovSpeed = 0;
            m_fovLeftTime = -1;
            m_curFov = m_targetFov;
            m_player.SetFov(m_targetFov);
        }
    }

    public EptFloat bigHeadScale { get; set; } //头大小

    public float scale
    {
        get { return m_targetScale.val; }
        set { m_targetScale.val = value; }
    }

    public EptFloat lastBigHeadScale { get; set; } //头大小
    public float lastScale { get; set; } //头大小

    public float fov
    {
        get { return m_targetFov; }
    }

    public float preFov
    {
        get { return m_preFov; }
    }
}
