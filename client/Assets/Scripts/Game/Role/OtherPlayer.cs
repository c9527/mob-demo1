﻿using UnityEngine;
using System;
using Object = UnityEngine.Object;
using System.Collections.Generic;


public class OtherPlayer : ServerPlayer
{
    //static private Dictionary<string, List<GameObject>> ms_dicGOCache = new Dictionary<string, List<GameObject>>();

    private readonly int RESCUE_WAIT_TIME_SEC;

    private CapsuleCollider m_cc;
    protected BoxCollider[] m_arrBodyPartCollider;
    protected Dictionary<string, List<Vector3>> m_dicColliderInfo = new Dictionary<string, List<Vector3>>();
    private Collider m_autoTargetCollider;

    private Transform m_transHeadSlot;
    private Transform m_transBipHead;
    private Transform m_transSpine;
    private Transform m_transChest;

    private UIThroughTheWall ESGViewTip = null;
    private UIThroughTheWall logoUiDirectionTip = null;
    private UIThroughTheWall c4UiDirectionTip = null;
    private UIThroughTheWall uiFlashingShowPos = null;

    private UIHpBar m_hpBar = null;

    //标记类型：LogoTeam-敌我标记；LogoZombie-特殊生化体标记；
    private string _logo_type;
    private bool _data_updated = true;
    
    private float _last_time;


    private ColorFade m_colorFade;

    private long Now;

    private GameObject goDyingEffect = null;
    private GameObject goDyingGameObject = null;
    private UIRescueTip uiDirectionTip = null;
    private UIThroughTheWall rescuingTip = null;
    private SphereCollider spCollider;
    private PointTriggerEvent pointTriggerScript;

    private bool m_isReAttachedCamera;    //换模型前是否已经挂了照相机

    private WidgetManager m_kWidgetManager;

    private EptFloat m_headScale;

    private LodManager m_kLodManger;

    //private float m_lockPosTime;


    public OtherPlayer()
    {
        Now = DateTime.Now.Ticks;
        //ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        //ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        RESCUE_WAIT_TIME_SEC = WorldManager.singleton.gameRuleLine.RescueWaitTime;
    }

    protected override void Init()
    {
        base.Init();
        m_kWidgetManager = new WidgetManager(this);
        m_kLodManger = new LodManager(this);
    }

    override public void Release(bool releaseGO)
    {
        if (m_kLodManger != null)
            m_kLodManger.SetLodLevel(0);

        base.Release(releaseGO);

        if (m_hpBar != null)
            m_hpBar.Stop();
        if (m_kWidgetManager != null)
            m_kWidgetManager.DetachAllWidget();

        if (releaseGO)
        {
            if (m_kPlayerController != null)
                m_kPlayerController.Release();
            m_kPlayerController = null;
            if (m_colorFade != null)
                m_colorFade.Release();
            m_colorFade = null;
            m_hpBar = null;
            if (m_kLodManger != null)
                m_kLodManger.Release();
            m_kLodManger = null;
            if (m_kWidgetManager != null)
                m_kWidgetManager.Release();
            m_kWidgetManager = null;
        }
        else
        {
            if (isDmmModel)
            {
                if (m_kPlayerController != null)
                    m_kPlayerController.Release();
            }
        }
    }

    public override void ReActive()
    {
        base.ReActive();
        if (m_kPlayerController != null)
            m_kPlayerController.ReActive();
    }

    override protected void DestroyPlayerOldModel()
    {
        if (m_colorFade != null)
            m_colorFade.Release();
        base.DestroyPlayerOldModel();
        PanelBattleSurvivalScore.TryDeleteBossHPBar(this);
        PanelBattleDotaScore.TryDeleteBossHPBar(this);
    }

    override public void LoadRes()
    {
        if (SceneManager.singleton.battleSceneLoaded == false)
            return;

        if (m_strModel == null || m_strModelPre == m_strModel && m_loadModel)
        {
            if (m_kWidgetManager != null && serverData != null && serverData.widgetVaule != m_kWidgetManager.widgetVaule)
            {
                DetachWidget();
                AttachWidget();
            }
            return;
        }

        if (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX ||
            serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX)
            AudioManager.PlayFightUISound(AudioConst.bioMotherShow);
        //Logger.Log("m_strModel: " + m_strModel + ",   m_iModelType: " + m_iModelType + ", 读表的ItemRoadID: " + serverData.itemRoleID);

        if (m_loadModel == false)
        {
            m_strModelPre = m_strModel;
            m_iModelTypePre = m_iModelType;
            m_loadModel = true;
        }

        if (!m_isReAttachedCamera)
            m_isReAttachedCamera = m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1;

        m_vecSceneCamSlotDir = Vector3.zero;

        if (m_strModelPre != m_strModel && gameObject != null)
        {
            WorldManager.singleton.UnRegisterPlayerGOHash(m_kGo.GetHashCode());
            DestroyPlayerOldModel();
        }

        //string path = m_iModelType == GameConst.MODEL_TYPE_DMM ? PathHelper.GetDmmPrefabPath(m_strModel) : PathHelper.GetRolePrefabPath(m_strModel);
        string path = GetRolePrefablPath(m_iModelType, m_strModel);
        //Logger.Log("OhterPlayer,   GetRolePrefablPath: " + path);
        Action<GameObject, uint> loadCallBack = (go, crc) =>
        {

#if UNITY_EDITOR
            if (crc == 4399)
            {
                GameObjectHelper.RefreshMaterial(go.transform);
            }
#endif

            if (go == null)
            {
                Logger.Error("Player model load failed, path: " + m_strModel);
                return;
            }

            if (released)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                //TryToDetachSceneCamera();
                CacheGO(go);
                return;
            }

            string modelName = GameObjectHelper.GetNameWithoutClone(go.name);
            if (modelName != m_strModel)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                //TryToDetachSceneCamera();
                CacheGO(go);
                return;
            }

            //status.ClearAllStatus();
            SetGO(go);
            SetPosNow(serverData.pos);

            DetachWidget();
            AttachWidget();

            SetState(serverData.state, false);

            if (serverData.alive)
            {
                if (status.crouch)
                    Crouch();
                else
                {
                    Stand(false, true);

                    //if (m_kPlayerAniController.animation == null || m_kPlayerAniController.animation.enabled == false)
                    //    Logger.Error("OtherPlayer SetGO Revive Stand " + " animation == " + m_kPlayerAniController.animation + " enable " + m_kPlayerAniController.animation.enabled);
                }
                //重新设置无敌状态
                if (m_fGodTime > 0)
                {
                    GodMode(true);
                }

                SwitchWeapon(serverData.propWeapon(), false, true);

                if (isAlive)
                {
                    if (serverData.actorState != null)
                        SetBuff(serverData.actorState.buff_list);
                    else if (serverData.actorState == null && serverData.roleData.buff_list != null)
                        SetBuff(serverData.roleData.buff_list);
                }
                //僵尸素材加载完后  播放变成僵尸特效
                if ((serverData.actor_type == GameConst.ACTOR_TYPE_ELITE || serverData.actor_type == GameConst.ACTOR_TYPE_GENERAL) && m_strModelPre != m_strModel)
                {
                    p_actor_buff_toc buff = new p_actor_buff_toc();
                    buff.buff_id = GameConst.EFFECT_CONST_GANRAN_ID;
                    buff.start_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
                    buff.end_time = buff.start_time  + GameConst.EFFECT_CONST_GANRAN_TIME;
                    m_kBM.AddBuff(buff);
                }
            }
            else if (serverData.state == GameConst.SERVER_ACTOR_STATUS_DYING)
            {
                SetActive(true);
                Die(null, -1);
            }
            else
                SetActive(false);

            WorldManager.singleton.RegisterPlayerGOHash(go.GetHashCode(), this);
            BasePlayer bp = this;
            GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_JOIN, bp);

            if (FPWatchPlayer.singleton.relatePlayer == this)
                FPWatchPlayer.singleton.StartWatch(this);

            //Logger.Error("joinlllllllll");
        };

        GameObject goCached = GetCacheGO(m_strModel);
        if (goCached != null)
            loadCallBack(goCached, 4399);
        else
            //ResourceManager.LoadModel(path, loadCallBack);
            ResourceManager.LoadBattlePrefab(path, loadCallBack);
    }

    override protected void SetGO(GameObject go)
    {
        base.SetGO(go);

        CharacterController cc = go.GetComponent<CharacterController>();
        ResourceManager.Destroy(cc);

        

        if (m_iModelType == GameConst.MODEL_TYPE_ROLE || m_iModelType == GameConst.MODEL_TYPE_STORY)
        {
            height = GetPlayerHeightStand();
            m_cc = m_mcc.AddMissingComponent<CapsuleCollider>("CapsuleCollider", go);
            m_cc.enabled = true;
            m_cc.isTrigger = false;
            m_cc.radius = GetPlayerRadius();
            m_cc.height = height;
            m_cc.center = GetPlayerCenter() + new Vector3(0, GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        }

        m_colorFade = go.AddMissingComponent<ColorFade>();
        //m_colorFade.Release();
        m_colorFade.enabled = true;

        m_transHeadSlot = GameObjectHelper.CreateChildIfNotExisted(m_kTrans, ModelConst.SLOT_HEAD).transform;
        m_playerEffectMgr.lastBigHeadScale = m_transHeadSlot.localScale.x;
        m_playerEffectMgr.bigHeadScale = m_playerEffectMgr.lastBigHeadScale;
        m_playerEffectMgr.SetBigHead(m_playerEffectMgr.lastBigHeadScale);

        m_kPlayerController = m_mcc.AddMissingComponent<OtherPlayerController>("OtherPlayerController", go);
        m_kPlayerController.enabled = true;
        m_kPlayerController.BindPlayer(this);
        
        UpdateHeadSlotPos();

        AddAutoTargetObject(go);

        m_transSceneCameraSlot = GameObjectHelper.CreateChildIfNotExisted(m_kTrans, ModelConst.SLOT_SCENE_CAMERA).transform;
        m_transSceneCameraSlot.ResetLocal();

        m_transChest = null;
        m_transSpine = null;
        m_transBipHead = null;
        m_transHips = null;

        _last_time = 0;
        _data_updated = true;
        update_ESG_View = true;
        List<BoxCollider> listBoxCollider = new List<BoxCollider>();
        if (m_mcc.HasInited(ModelConst.BIP_HEAD))
        {
            m_transChest = m_mcc.FindComponent<Transform>(ModelConst.BIP_CHEST, go);
            m_transSpine = m_mcc.FindComponent<Transform>(ModelConst.BIP_SPINE, go);
            m_transBipHead = m_mcc.FindComponent<Transform>(ModelConst.BIP_HEAD, go);
            m_transHips = m_mcc.FindComponent<Transform>(ModelConst.BIP_NAME_PEVIS, go);
            listBoxCollider = m_mcc.modelBoxColider;
            m_arrBehaviour = m_mcc.modelBehaviours;
        }
        else
        {
            GameObjectHelper.VisitChildByTraverse(m_kTrans, (child) =>
            {
                BoxCollider bc = child.GetComponent<BoxCollider>();
                if (bc != null)
                {
                    listBoxCollider.Add(bc);
                }

                switch (child.name)
                {
                    case ModelConst.BIP_CHEST:
                        m_transChest = child.transform;
                        break;
                    case ModelConst.BIP_SPINE:
                        m_transSpine = child.transform;
                        break;
                    case ModelConst.BIP_HEAD:
                        m_transBipHead = child.transform;
                        break;
                    case ModelConst.BIP_NAME_PEVIS:
                        m_transHips = child.transform;
                        break;
                }
            });

            m_mcc.CacheComponent(ModelConst.BIP_CHEST, m_transChest);
            m_mcc.CacheComponent(ModelConst.BIP_SPINE, m_transSpine);
            m_mcc.CacheComponent(ModelConst.BIP_HEAD, m_transBipHead);
            m_mcc.CacheComponent(ModelConst.BIP_NAME_PEVIS, m_transHips);
            m_mcc.CacheBoxCollider(listBoxCollider);

            m_arrBehaviour = m_kGo.GetComponentsInChildren<Behaviour>();
            m_mcc.modelBehaviours = m_arrBehaviour;
        }
 
        List<BoxCollider> bodyPartCollider = new List<BoxCollider>();
        if (listBoxCollider != null)
        {
            for (int i = 0; i < listBoxCollider.Count; i++)
            {
                if (listBoxCollider[i] != null)
                {
                    listBoxCollider[i].isTrigger = true;
                    if (listBoxCollider[i].name.StartsWith(ModelConst.BIP_PREFIX))
                        bodyPartCollider.Add(listBoxCollider[i]);
                }
            }
        }

        m_dicColliderInfo.Clear();
        m_arrBodyPartCollider = bodyPartCollider.ToArray();
        for (int i = 0; i < m_arrBodyPartCollider.Length; i++)
        {
            if (m_arrBodyPartCollider.Length > GlobalConfig.gameSetting.OptimizeRoleColliderNum)  // 默认关闭部位碰撞体，若数量太多
                m_arrBodyPartCollider[i].enabled = false;

            List<Vector3> list = new List<Vector3>();
            list.Add(m_arrBodyPartCollider[i].center);
            list.Add(m_arrBodyPartCollider[i].size);
            m_dicColliderInfo.Add(m_arrBodyPartCollider[i].name, list);
        }
           
        //AudioSource[] arrAS = go.GetComponentsInChildren<AudioSource>();
        //for (int i = 0; i < arrAS.Length; i++)
        //{
        //    GameObject.Destroy(arrAS[i]);
        //}

        int numAS = 3;
        if (m_mcc.audiosources.Count < numAS)
        {
            List<AudioSource> listAS = new List<AudioSource>();
            for (int i = 0; i < numAS; i++)
            {
                listAS.Add(go.AddComponent<AudioSource>());
            }
            m_mcc.audiosources = listAS;
        }
        m_kStepSoundPlayer.SetAudioSource(m_mcc.audiosources[0]);
        m_kHeartSoundPlayer.SetAudioSource(m_mcc.audiosources[1]);
        m_kVoiceSoundPlayer.SetAudioSource(m_mcc.audiosources[2]);
         
        GodMode(false);

        m_kLodManger.Init();
    }

    /// <summary>
    /// 尝试添加自动瞄准对象
    /// </summary>
    /// <param name="go"></param>
    private void AddAutoTargetObject(GameObject go)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }

        var tranAutoTarget = m_kTrans.FindChild("autoTarget");
        if (tranAutoTarget == null)
        {
            tranAutoTarget = new GameObject("autoTarget").transform;
            tranAutoTarget.parent = go.transform;
            tranAutoTarget.localPosition = Vector3.zero;
            tranAutoTarget.gameObject.layer = GameSetting.LAYER_VALUE_AUTOTARGET;
        }

        m_autoTargetCollider = tranAutoTarget.gameObject.AddMissingComponent<CapsuleCollider>();
        m_autoTargetCollider.isTrigger = true; 


        if (configData.SubType != GameConst.ROLE_SUBTYPE_OBJECT)
        {
            AutoAimSpace aas = tranAutoTarget.gameObject.AddMissingComponent<AutoAimSpace>();
            aas.Init(this);
        }
           
    }

    private void UpdateHeadSlotPos()
    {
        if (m_cc == null) return;
        Vector3 vec3HeadSlotLocalPos = Vector3.zero;
        vec3HeadSlotLocalPos.y = height + GameSetting.PLAYER_HEAD_SLOT_OFFSET;
        m_transHeadSlot.localPosition = vec3HeadSlotLocalPos;
    }

    /// <summary>
    /// 主角死亡后，相机会跟随在队友后面
    /// </summary>
    override public void AttachSceneCamera()
    {
        if (gameObject == null)
            return;
        SceneCamera.singleton.enableCamera = true;
        m_isReAttachedCamera = false;
        if (SceneCameraController.singleton != null)
        {
            SceneCameraController.singleton.FollowPlayer(this, FollowHeight, FollowDistance, true, true);
            if (isDmmModel && m_iModelType == m_iModelTypePre && m_vecSceneCamSlotDir != Vector3.zero)
            {
                SceneCameraController.singleton.SetDir(m_vecSceneCamSlotDir);
            }
        }
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, false);
    }

    /// <summary>
    /// 相机跟随队友时若队友死亡，则观看其死亡后，切到逻辑在changeTeamView里面的playerdie
    /// </summary>
    private void FollowedPartnerDead(BasePlayer killer)
    {
        Vector3 pos1 = position;
        pos1.y += GameSetting.WATCH_DEAD_CAMERA_HEIGHT;
        RaycastHit kRH;
        float dis = GameSetting.WATCH_DEAD_CAMERA_DISTANCE;

        Vector3 killerDir = transform.forward;
        if (killer != null)
        {
            killerDir = killer.position - position;
            killerDir.y = 0;
            killerDir.Normalize();
        }

        if (Physics.Raycast(pos1, -killerDir, out kRH, GameSetting.WATCH_DEAD_CAMERA_DISTANCE, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {
            dis = kRH.distance - 0.1f;
            dis = dis < 0 ? 0 : dis;
        }

        Vector3 cameraPos = position - dis * killerDir;
        // 防止角色被回收，此时相机也会跟到回收点
        if (m_bIsActive == false)
            cameraPos = serverPos - dis * killerDir;
        
        cameraPos.y += GameSetting.WATCH_DEAD_CAMERA_HEIGHT;

        SceneCamera.singleton.SetParent(null);
        SceneCamera.singleton.position = cameraPos;
        SceneCamera.singleton.forward = killerDir;
        SceneCamera.singleton.Rotate(GameSetting.WATCH_DEAD_CAMERA_ROTATION_X, 0, 0, Space.Self);
    }

    override public void UpdateGameObjectLayer()
    {
        if (gameObject != null)
        {
            if (m_iModelType == GameConst.MODEL_TYPE_DMM)
            {
                gameObject.ApplyChildrenLayer(GameSetting.LAYER_VALUE_DMM_PLAYER, new int[] { GameSetting.LAYER_VALUE_AUTOTARGET });
                gameObject.layer = GameSetting.LAYER_VALUE_DMM_PLAYER;
            }
            else if(m_iModelType == GameConst.MODEL_TYPE_STORY)
            {
                gameObject.ApplyChildrenLayer(GameSetting.LAYER_VALUE_STORY_PLAYER, new int[] { GameSetting.LAYER_VALUE_AUTOTARGET });
                gameObject.layer = GameSetting.LAYER_VALUE_STORY_PLAYER;
            }
            else
            {
                int iPlayerLayer = WorldManager.singleton.GetOtherPlayerLayer(serverData.camp);
                if(iPlayerLayer != m_mcc.goLayer)
                {
                    gameObject.ApplyChildrenLayer(iPlayerLayer, new int[] { GameSetting.LAYER_VALUE_AUTOTARGET });
                    gameObject.layer = GameSetting.LAYER_VALUE_OTHER_PLAYER;
                    if (goDyingGameObject != null)
                        goDyingGameObject.layer = GameSetting.LAYER_VALUE_DEFAULT;
                    m_mcc.goLayer = iPlayerLayer;
                }
                
            }
        }
    }

    override public void Update()
    {
        base.Update();

        if (m_bIsActive == false || alive == false)
            return;

        TipC4BombOnHead();

        if (_data_updated == true)
        {
            setupLogoGO();
        }
        if (update_ESG_View == true && PlayerBattleModel.Instance.battle_info != null &&
            PlayerBattleModel.Instance.battle_info.spectator_switch== true && WorldManager.singleton.isViewBattleModel == true)
        {
            playESGViewDirectionTip(true);
        }
        if (Camera.main != null && serverData != null)
        {
            if ((Time.time - _last_time) >= 1 && MainPlayer.singleton != null)
            {
                if (MainPlayer.singleton.ScoutSkillState == GameConst.SCOUT_SKILL_STATE_START)
                    ShowLogoBySkill(true);
                else if (MainPlayer.singleton.ScoutSkillState == GameConst.SCOUT_SKILL_STATE_END)
                {
                    ShowLogoBySkill(false);
                    UpdateLogo();
                }
            }

            if ((Time.time - _last_time) >= 3)
            {
                _last_time = Time.time;

                if (MainPlayer.singleton != null && MainPlayer.singleton.ScoutSkillState == GameConst.SCOUT_SKILL_STATE_NODEFINE)
                {
                    UpdateLogo();
                }
            }
        }

        m_kLodManger.Update();

        if (null != m_skillMgr)
            m_skillMgr.UpdateCheckAlert();

        //if(m_lockPosTime != -1)
        //{
        //    if (m_lockPosTime > 0)
        //        m_lockPosTime -= Time.deltaTime;

        //    if (m_lockPosTime <= 0)
        //    {
        //        m_lockPosTime = -1;
        //    }
        //}
    }

    public void UpdateLogo()
    {
        
        if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && !MainPlayer.singleton.alive)
        {
            ShowZombieLogo(false);
            ShowTeamerLogo(false);
        }
        else if (WorldManager.singleton.isZombieTypeModeOpen)
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && (MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX || MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX))//自己是生化母体
            {
                ShowZombieLogo(false);
                ShowTeamerLogo(true);
            }
            else if (serverData.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX ||  serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX))//生化母体
            {
                ShowZombieLogo(true);
                ShowTeamerLogo(false);
            }
            else if (MainPlayer.singleton != null && MainPlayer.singleton.Camp == serverData.camp)
            {
                ShowZombieLogo(false);
                ShowTeamerLogo(true);
            }
            else
            {
                ShowZombieLogo(false);
                ShowTeamerLogo(false);
            }
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.Camp == serverData.camp && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
            {
                ShowTeamerLogo(true);
            }
            else
            {
                ShowTeamerLogo(false);
            }
        }
        else if(WorldManager.singleton.isTeamHeroModeOpen)
        {
            ShowTeamHeroLogo(true);
        }
        else if (serverData.camp != (int)WorldManager.CAMP_DEFINE.ALONE && MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.Camp == serverData.camp)
        {
            //ShowZombieLogo(true);
            ShowTeamerLogo(true);
        }
    }

    private void ShowZombieLogo(bool isShow)
    {
        if (_logo_type == "LogoZombie")
        {
            playLogoUiDirectionTip(isShow);
        }
    }

    private void ShowTeamerLogo(bool isShow)
    {
        if (_logo_type == "LogoTeam")
        {
            playLogoUiDirectionTip(isShow);
        }
    }

    private void ShowTeamHeroLogo(bool isShow)
    {
        if (_logo_type == "LogoTeamHero")
        {
            playLogoUiDirectionTip(isShow);
        }
    }

    private void ShowLogoBySkill(bool isShow)
    {
        playLogoUiDirectionTip(isShow);
    }

    public void setupLogoGO()
    {
        if (serverData == null || MainPlayer.singleton == null || transform == null)
        {
            return;
        }
        _logo_type = "";
        if (WorldManager.singleton.isZombieTypeModeOpen)
        {
            if (serverData.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX || serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX))
            {
                _logo_type = "LogoZombie";
            }
            else if (serverData.camp != (int)WorldManager.CAMP_DEFINE.ALONE)
            {
                _logo_type = "LogoTeam";
            }
        }
        else if(WorldManager.singleton.isBigHeadModeOpen || WorldManager.singleton.isDotaModeOpen)
        {
            if (serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_KING)
            {
                _logo_type = "LogoBigHeadKing";
            }
            else if (serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_HERO)
            {
                _logo_type = "LogoBigHeadHero";
            }
            //else if (serverData.camp != (int)WorldManager.CAMP_DEFINE.ALONE && serverData.camp == MainPlayer.singleton.Camp)
            //{
            //    _logo_type = "LogoTeam";
            //}
        }
        else if(WorldManager.singleton.isTeamHeroModeOpen)
        {
            if (serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
            {
                _logo_type = "LogoTeamHero";
            }
            else if (serverData.camp != (int)WorldManager.CAMP_DEFINE.ALONE && serverData.camp == MainPlayer.singleton.Camp)
            {
                _logo_type = "LogoTeamHero";
            }
        }
        else if (serverData.camp != (int)WorldManager.CAMP_DEFINE.ALONE && serverData.camp == MainPlayer.singleton.Camp)
        {
            _logo_type = "LogoTeam";
        }


        if (string.IsNullOrEmpty(_logo_type))
        {
            playLogoUiDirectionTip(false);
            _data_updated = false;
            return;
        }

        else
        {
            if (gameObject == null || m_transBipHead == null) return;
            playLogoUiDirectionTip(true);
            _last_time = 0;
            _data_updated = false;
        }
    }

    private void ShowHpBar(bool isShow)
    {
        if (isShow)
        {
            var curPlayer = WorldManager.singleton.curPlayer == null ? MainPlayer.singleton : WorldManager.singleton.curPlayer;

            if (WorldManager.singleton.gameRuleLine.ShowHp == GameConst.SHOWHP_ALL
            || WorldManager.singleton.gameRuleLine.ShowHp == GameConst.SHOWHP_ENEMY && (curPlayer != null && Camp != curPlayer.Camp)
            || WorldManager.singleton.gameRuleLine.ShowHp == GameConst.SHOWHP_FRIEND && (curPlayer != null && Camp == curPlayer.Camp) && (null != serverData && serverData.actor_type == GameConst.ACTOR_TYPE_PERSON)
            )
            {
                if (m_hpBar == null)
                {
                    m_hpBar = UIHpBar.Create();
                    m_hpBar.BindPlayer(this);
                }
                m_hpBar.Play(headSlot, new Vector3(0, GameConst.HEAD_SLOT_HEIGHT_HP_BAR * m_headScale, 0f));
                return;
            }
        }

        if (m_hpBar != null)
        {
            m_hpBar.Stop();
        }
    }

    override protected void GodMode(bool value)
    {
        if(value != m_bIsGodMode)
            FadeColor(value, Color.red);
        base.GodMode(value);
    }

    public void FadeColor(bool value, Color color)
    {
        if (m_colorFade == null)
            return;

        //Logger.Log("Player=" + ColorName + "   color=" + color + "   value=" + value);

        m_colorFade.Run(value, color);
    }

    public void SetBoneWeight(SkinQuality weight)
    {
        if (m_renderer != null && m_renderer is SkinnedMeshRenderer)
        {
            SkinnedMeshRenderer smr = m_renderer as SkinnedMeshRenderer;
            if (smr.quality != weight)
                smr.quality = weight;
            //Logger.Log(weight.ToString());
        }
    }

    public override Transform headSlot
    {
        get { return m_transHeadSlot; }
    }

    public void EnableCC(bool ok)
    {
        m_cc.isTrigger = !ok;
    }


    internal override void UseSkill(toc_fight_use_skill proto)
    {
        if (m_skillMgr != null)
            m_skillMgr.OPUseSkill(proto.skill_id, proto.pos, proto.target_ids, proto.skill_param, proto.tauntedPlayerId);
    }


    public void EnableBodyPartTrigger(bool value, bool force)
    {
        if (m_arrBodyPartCollider == null)
            return;

        for (int i = 0; i < m_arrBodyPartCollider.Length; i++)
        {
            if (m_arrBodyPartCollider[i] != null)
                m_arrBodyPartCollider[i].isTrigger = value;
            else
            {
                Logger.Warning("EnableBodyPartCollider collider null err, player:" + PlayerName + "  i:" + i + "  set value:" + value);
            }
        }
    }

    override public void EnableBodyPartCollider(bool value, bool force)
    {
        if (m_arrBodyPartCollider == null)
            return;

        if (force == false && m_arrBodyPartCollider.Length <= GlobalConfig.gameSetting.OptimizeRoleColliderNum)
            return;

        for (int i = 0; i < m_arrBodyPartCollider.Length; i++)
        {
            if (m_arrBodyPartCollider[i] != null)
                m_arrBodyPartCollider[i].enabled = value;
            else
            {
                Logger.Warning("EnableBodyPartCollider collider null err, player:" + PlayerName + "  i:" + i + "  set value:" + value );
            }
        }

        //Logger.Log("EnableBodyPartCollider " + value + " " + gameObject.name);
    }

    public bool isEnemy
    {
        get
        {
            if (MainPlayer.singleton == null || MainPlayer.singleton.Camp != Camp || Camp == 0)
                return true;

            return false;
        }
    }

    public Transform transSpine
    {
        get { return m_transSpine; }
    }

    public Transform transChest
    {
        get { return m_transChest; }
    }

    public Transform transBipHead
    {
        get { return m_transBipHead; }
    }

    public void LockKillPos(Vector3 pos, Vector3 targetPos, float time)
    {
        if (m_kTrans == null)
            return;

        
        m_kTrans.position = pos;

        Vector3 dir = targetPos - pos;
        dir.y = 0;
        m_kTrans.localEulerAngles = new Vector3(0, Vector3.forward.Angle2(dir), 0);

        //m_lockPosTime = time;
    }

    public void ReleaseLockPos()
    {

    }

    public Dictionary<string, List<Vector3>> colliderInfo
    {
        get { return m_dicColliderInfo; }
    }

    //
    public void TurnColliderTrigger(bool on)
    {
        m_cc.isTrigger = on;
    }

    #region OnData
    internal override void OnData_ActorProp(toc_fight_actorprop proto)
    {
        base.OnData_ActorProp(proto);
        localPlayerData.UpdateLocalWeaponAwakenProps(proto.awaken_props);

        if (gameObject != null)
        {
            string newWeaponID = m_kServerData.propWeapon();
            if (string.IsNullOrEmpty(newWeaponID) == false)
                SwitchWeapon(newWeaponID, false, false);
            //else
            //Logger.Error("OnData_ActorProp, propWeaponID is null");
        }
    }

    internal override void OnData_ActorData(p_fight_role proto)
    {
        base.OnData_ActorData(proto);
    }

    internal override void OnData_ActorState(p_actor_state_toc state)
    {
        if (serverData == null || serverData.camp != state.camp || serverData.state != state.state)
        {
            _last_time = 0;
            _data_updated = true;
            update_ESG_View = true;
        }
        
        //base.OnData_ActorState(state);
        base.OnData_ActorState(state);
        //FJS 角色从人变成僵尸 增加一个感染的特效  僵尸复活状态
        //  bool isShowGanRanEffect = false;
        //if (serverData.actor_type == GameConst.ZOMBIE_TYPE_PERSON && state.actor_type == GameConst.ZOMBIE_TYPE_ELITE&&state.state == GameConst.SERVER_ACTOR_STATUS_LIVE)
        //当前是僵尸状态  且是复活状态  模型不一致
        if ((state.actor_type == GameConst.ACTOR_TYPE_ELITE || state.actor_type == GameConst.ACTOR_TYPE_GENERAL) && state.state == GameConst.SERVER_ACTOR_STATUS_LIVE && m_strModelPre != m_strModel)
        {
            p_actor_buff_toc buff = new p_actor_buff_toc();
            buff.buff_id = GameConst.EFFECT_CONST_GANRAN_ID;
            buff.start_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
            buff.end_time = buff.start_time + GameConst.EFFECT_CONST_GANRAN_TIME;
            m_kBM.AddBuff(buff);
        }
        UpdateGameObjectLayer();
    }

    public override void SetActorType(int actorType)
    {
        base.SetActorType(actorType);
        _data_updated = true;
        update_ESG_View = true;
    }

    #endregion

    #region Switch Weapon

    public override bool SwitchWeapon(string strWeaponID, bool bSendMsg, bool sameSwitch, bool delaySendMsg = true)
    {
        if (!sameSwitch && curWeapon != null && curWeapon.weaponId == strWeaponID)
            return true;

        if (base.SwitchWeapon(strWeaponID, bSendMsg, sameSwitch) == false)
            return false;

       // ConfigItemWeaponLine kWL = m_kWeaponMgr.curWeapon.weaponConfigLine;
        //m_kPlayerAniController.CrossFade(kWL.OPSwitchAni, 0.2f);

        if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
        {
            m_kPlayerAniController.SetTrigger("brestSwitch");
        }

        return true;
    }

    public override void WeaponAttachComplete()
    {
        base.WeaponAttachComplete();

        if (released)
            return;

        if (curWeapon != null)
            curWeapon.SetLayer(GameSetting.LAYER_VALUE_OTHER_PLAYER);

        if (!isSwitchFPV)
            m_kPlayerAniController.Play(curWeapon.weaponConfigLine.OPSwitchAni);
    }
    #endregion

    #region Reload Ammo

    override public void OnReloadAmmoComplete()
    {
        base.OnReloadAmmoComplete();
    }

    override internal void ReloadAmmo2(toc_fight_reload arg)
    {
        if (released)
            return;

        if (curWeapon != null)
        {
            curWeapon.ReloadAmmo2(arg);
            if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
            {
                m_kPlayerAniController.SetTrigger("brestReload");
            }
        }
    }
    #endregion

    #region Fire

    override internal void Fire2(toc_fight_fire arg)
    {
        if (curWeapon != null)
        {
            curWeapon.Fire2(arg);
            if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
            {
                m_kPlayerAniController.SetTrigger("brestFire");
            }
        }


    }

    override internal void Atk(toc_fight_atk proto)
    {
        if (curWeapon is WeaponGun)
        {
            WeaponGun weapon = curWeapon as WeaponGun;
            weapon.PlayOpWeaponFire();
        }


        if (WeaponAtkTypeUtil.IsSubWeaponType(proto.atkType))
        {
            if (subWeapon != null)
            {
                subWeapon.Atk(proto);
                if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
                {
                    m_kPlayerAniController.SetTrigger("brestFire");
                }
                GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ATK, this);
            }
        }
        else
        {
            if (curWeapon != null)
            {
                curWeapon.Atk(proto);
                if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
                {
                    m_kPlayerAniController.SetTrigger("brestFire");
                }
                GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ATK, this);
            }
        }

    }
    #endregion

    #region 倒数闪烁显示
    public void ShowPosFlashingUI(bool isPlay)
    {
        if (uiFlashingShowPos == null && isPlay) uiFlashingShowPos = UIThroughTheWall.createUI();
        if (isPlay)
        {
            float interval = (float)ConfigMisc.GetInt("arena_flashing_interval") / 1000f;
            int dis = ConfigMisc.GetInt("arena_visible_range");
            ThroughTheWallProp prop = new ThroughTheWallProp()
            {
                interval = interval,
                distance = dis,
                msg = PlayerName,
                textPos = new Vector2(0, 40),
                //imgProp = new RectTransformProp(){scale = new Vector3(2, 2, 1)},
                textProp = new TextProp() { fontSize = 20, rect = new RectTransformProp() { scale = new Vector3(1, 1, 1) } }
            };
            uiFlashingShowPos.play(AtlasName.Battle, "red_point", transform, new Vector3(0, 1.7f, 0), prop);
        }
        else
        {
            if (uiFlashingShowPos != null) uiFlashingShowPos.stop();
        }
    }

    public bool isShowPosFlashingUI
    {
        get { return uiFlashingShowPos != null && uiFlashingShowPos.isPlaying; }
    }
    #endregion

    #region 濒死状态处理
    public void doDying()
    {
        BasePlayer player = WorldManager.singleton.curPlayer;
        if (player == null || player.Camp != Camp)
        {
            return;
        }

        ShowZombieLogo(false);
        ShowHpBar(false);

        if (goDyingGameObject == null)
        {
            if (m_kTrans == null)
                return;
            var dyingTrans = m_kTrans.Find("goDyingGameObject");

            if (dyingTrans == null)
            {
                goDyingGameObject = new GameObject("goDyingGameObject");
                goDyingGameObject.transform.SetParent(m_kTrans, false);
                goDyingGameObject.layer = GameSetting.LAYER_VALUE_DEFAULT;
            }
            else
            {
                goDyingGameObject = dyingTrans.gameObject;
            }
        }
        else
        {
            goDyingGameObject.TrySetActive(true);
        }

        if (uiDirectionTip != null)
        {
            uiDirectionTip.enabled = true;
            uiDirectionTip.ForceHide(false);
            uiDirectionTip.ShowDownTimer(RESCUE_WAIT_TIME_SEC);
        }
        else
        {
            ResourceManager.LoadSprite(AtlasName.Battle, "plus", sp =>
            {
                if (gameObject == null || goDyingGameObject == null) return;
                uiDirectionTip = goDyingGameObject.AddMissingComponent<UIRescueTip>();
                uiDirectionTip.SetTipSprite(sp, true);
                uiDirectionTip.AroundScreen(true);
                uiDirectionTip.SetPosAndScale(new Vector3(0, 100, 0), new Vector3(0.5f, 0.5f, 1), Vector3.zero);
                uiDirectionTip.SetPosAndScale2(new Vector3(-8, 200, 0), new Vector3(18, 200, 0), new Vector3(2, 2, 1));
                uiDirectionTip.ShowDownTimer(RESCUE_WAIT_TIME_SEC);
            });
        }

        if (goDyingEffect != null)
        {
            goDyingEffect.TrySetActive(true);
        }
        else
        {
            spCollider = goDyingGameObject.AddMissingComponent<SphereCollider>();
            spCollider.center = new Vector3(0, 0.5f, 0);
            spCollider.radius = 1;
            spCollider.isTrigger = true;
            pointTriggerScript = goDyingGameObject.AddMissingComponent<PointTriggerEvent>();
            pointTriggerScript.type = "Rescue";
            pointTriggerScript.id = PlayerId;

            ResourceManager.LoadBattlePrefab("Effect/help", (go, crc) =>
            {
                if (go == null || gameObject == null) return;
                goDyingEffect = go;
                go.transform.SetParent(goDyingGameObject.transform, false);
            });
        }
    }

    private void playRescuingTip(bool isShow)
    {
        if (!isShow)
        {
            if (rescuingTip != null) rescuingTip.stop();
        }
        else
        {
            if (rescuingTip == null) rescuingTip = UIThroughTheWall.createUI();
            ThroughTheWallProp prop = new ThroughTheWallProp() { isTweenPos = true };
            if (goDyingGameObject == null) return;
            rescuingTip.play(AtlasName.Battle, "rescuing", goDyingGameObject.transform, new Vector3(0, 1.2f, 0f), prop);
        }
    }

    private void HideRescue()
    {
        ShowPosFlashingUI(false);
        if (uiDirectionTip != null)
        {
            uiDirectionTip.enabled = false;
        }

        playRescuingTip(false);

        if (goDyingEffect != null)
        {
            goDyingEffect.TrySetActive(false);
        }
        if (goDyingGameObject != null)
        {
            goDyingGameObject.TrySetActive(false);
        }
    }
    #endregion

    #region Player Control

    public override void SetActive(bool value)
    {
        base.SetActive(value);

        if (m_cc != null)
            m_cc.enabled = value;

        if (m_autoTargetCollider != null)
            m_autoTargetCollider.enabled = value;

        if (value == true && m_arrBodyPartCollider.Length <= GlobalConfig.gameSetting.OptimizeRoleColliderNum)
        {
            EnableBodyPartCollider(true, true);
        }

        if (m_kPlayerAniController != null)
            m_kPlayerAniController.SetSpeed(1f);

        if (value == false)
        {
            HideRescue();
            if (m_kLodManger != null)
                m_kLodManger.SetLodLevel(0);
        }
    }

    override internal void Behit(p_hit_info_toc hitInfo, BasePlayer kAttacker = null, Vector3 hitPos = default(Vector3), int skillId = 0, System.Object arg = null)
    {
        int hitValue = m_kServerData.life - hitInfo.life;

        base.Behit(hitInfo, kAttacker, hitPos);

        if (serverData.alive == false)
            return;

        
        if (hitValue > 0)
        {
            m_kPlayerAniController.SetTrigger("behit");
            //m_kPlayerAniController.CrossFade("behit", 0.16f);
            // 如果当前玩家不可见，则不显示血花
            if (m_kTrans != null && isRendererVisible && isMaxBloodEffect == false)
            {
                if (kAttacker == null || MainPlayer.singleton == null || kAttacker.PlayerId != MainPlayer.singleton.PlayerId)
                {
                    Vector3 _hitPos = new Vector3(0, height * 0.5f, 0);
                    string strBloodEffectID = EffectManager.GetPlayerHitEffect(hitInfo.bodyPart);
                    m_bloodEffectNum++;
                    EffectManager.singleton.GetEffect<EffectTimeout>(strBloodEffectID, (kEffect) =>
                    {
                        if (m_kTrans == null) return;
                        Vector3 vec3WorldHitPos = m_kTrans.localToWorldMatrix.MultiplyPoint(_hitPos);
                        kEffect.Attach(vec3WorldHitPos, Vector3.zero, 0);
                        kEffect.SetTimeOut(-1);
                        kEffect.Play();
                        kEffect.SetCompleteCallBack(null, (param) =>
                        {
                            m_bloodEffectNum--;
                        });
                    });
                }
            }
        }
    }

    override public void Die(BasePlayer killer, int weapon, int hitPart = BODY_PART_ID_BODY, Vector3 bulletDir = default(Vector3), Vector3 diePos = default(Vector3), Vector3 killerPos = default(Vector3))
    {
        base.Die(killer, weapon, hitPart, bulletDir, diePos, killerPos);
        var config=ConfigManager.GetConfig<ConfigItemRole>().GetLine(RoleId);        
        if(config!=null && config.SubType== GameConst.PLAYER_SUBTYPE_BOSS)
        {                        
            PanelBattleSurvivalScore.TryDeleteBossHPBar(this);
            if (PanelBattleSurvivalScore.listBoss.Count==0)
            {
                AudioManager.PlayBgMusic();
            }
            PanelBattleDotaScore.TryDeleteBossHPBar(this);
            if (PanelBattleDotaScore.listBossRight.Count == 0)
            {
                AudioManager.PlayBgMusic();
            }
        }
        GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_DETACH, this);
        ShowPosFlashingUI(false);
        playESGViewDirectionTip(false);
        if (gameObject != null)
        {
            if (m_posTweener != null)
                m_posTweener.enabled = false;

            if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
            {
                if (SceneCameraController.singleton != null)
                    SceneCameraController.singleton.StopControl();
                FollowedPartnerDead(killer);
            }

            Vector3 effectPos = centerPos;

            //string msg = "ServerDiePos=" + diePos + " DiePos=" + position;
            FallOnTheGround();
            //msg += " FallPos=" + position;
            //Logger.Log(msg);

            if (m_cc != null)
                m_cc.enabled = false;

            if (m_configData.SubType == GameConst.ITEMS_SUBTYPE_OBJECT) //躲猫猫物件
            {
                AudioManager.PlayFightUISound(AudioConst.hideFound, false);
            }

            //if (m_iModelType == GameConst.MODEL_TYPE_DMM)
            if (!string.IsNullOrEmpty(m_configData.DieEffect))
            {
                //EffectManager.singleton.GetEffect<EffectTimeout>(EffectConst.FIGHT_HIDE_ITEM_DIE, (effect) =>
                EffectManager.singleton.GetEffect<EffectTimeout>(m_configData.DieEffect, (effect) =>
                {
                    effect.Attach(effectPos);
                    effect.SetTimeOut(-1);
                    effect.Play();
                });
            }
            else
            {

                ConfigItemWeaponLine wc = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weapon);
                int dieAniType = wc != null ? wc.DieAniType : 1;
                Vector3 fromPos = killer != null ? killer.position : Vector3.zero;

                m_kPlayerAniController.EnableUpBodyLayer(false);
                string deadAni = GetOpModelIsSimpleDeadAni() ?
                        AniConst.GetSimpleDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, status.stand) :
                        AniConst.GetPlayerDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, dieAniType, status.stand, hitPart == BODY_PART_ID_HEAD);
                m_kPlayerAniController.SetSpeed(0.75f);
                m_kPlayerAniController.Play(deadAni, -1, 0);
            }

            //微调死亡方向
            var a = Mathf.Atan2(m_kTrans.forward.z, m_kTrans.forward.x);    //计算反正切，范围-180~180
            var b = Mathf.Atan2(bulletDir.z, bulletDir.x);
            var dir = (b - a) * 180 / Mathf.PI + 45;    //计算角度差值，加上45偏转
            dir = (dir + 360) % 360;    //角度规定化到最小周期内
            //微调死亡动作的方向，使得和中弹方向一致
            var rotation = 0f;
            if (dir >= 0 && dir < 90)
                rotation = dir - 45;
            else if (dir >= 90 && dir < 180)
                rotation = dir - 135;
            else if (dir >= 180 && dir < 270)
                rotation = dir - 225;
            else if (dir >= 270 && dir < 360)
                rotation = dir - 315;
            transform.localRotation = Quaternion.Euler(m_kTrans.rotation.eulerAngles + new Vector3(0, -rotation, 0));

            //TimerManager.SetTimeOut(0.2f, () =>
            //{
            //    if (isDying)
            //    {
            //        doDying();
            //    }
            //    else
            //    {
            //        TimerManager.SetTimeOut(GameSetting.OTHER_PLAYER_DEAD_DISAPPEAR_TIME, () =>
            //        {
            //            if (gameObject != null && (serverData == null || serverData.alive == false) && isDead)
            //            {
            //                SetActive(false);
            //            }
            //        });
            //    }
            //});
        }
        ClearState();
    }

    protected override void Lock()
    {
        // 移除场景相机
        if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
        {
            if (SceneCameraController.singleton != null)
                SceneCameraController.singleton.StopControl();

            if (m_transSceneCameraSlot.childCount > 0)
            {
                Transform transCamera = m_transSceneCameraSlot.GetChild(0);
                transCamera.SetParent(null);
            }
            m_isReAttachedCamera = true;            
            Logger.Warning("OtherPlayer Lock, SceneCamera");
        }

        base.Lock();
    }

    protected override void Revive(bool dataRevive)
    {
        HideRescue();
        var config=ConfigManager.GetConfig<ConfigItemRole>().GetLine(RoleId);

        if (config!=null&&config.SubType=="BOSS")
       {
            if (null != WorldManager.singleton && WorldManager.singleton.isDotaModeOpen)
            {
                PanelBattleDotaScore.TryAddBossHPBar(this);
            }
            else
            {
                PanelBattleSurvivalScore.TryAddBossHPBar(this);
            }
        }
        GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ATTACH, this);
        base.Revive(dataRevive);
        
        if (gameObject != null && !isSwitchFPV)
        {
            ShowZombieLogo(true);
            ShowHpBar(true);

            if (m_cc != null)
                m_cc.enabled = true;

            m_kPlayerAniController.EnableUpBodyLayer(true);
            m_kPlayerAniController.SetSpeed(1f);

            if (status.crouch)
                Crouch();
            else
            {
                Stand(false, false);
                //if(alive == false || m_kPlayerAniController.animation == null || m_kPlayerAniController.animation.enabled == false)
                //    Logger.Error("OtherPlayer Revive Stand alive = " + alive + " animation == " + m_kPlayerAniController.animation + " enable " + m_kPlayerAniController.animation.enabled);
            }

            SwitchWeapon(serverData.propWeapon(), false, true);

            if (m_isReAttachedCamera)
                AttachSceneCamera();
        }
    }

    //internal override void Jump(toc_fight_jump data)
    //{
    //    if (m_kPlayerController == null || alive == false)
    //    {
    //        return;
    //    }

    //    //Logger.Log("Receive JumpProto, Stage = " + data.type);
    //    //TweenPos(data.pos, -1, -1, data.pos[5], data.type);
    //    TweenPos(data.pos, -1, -1, data.pos[5], data.type);
    //}

    override public void Crouch()
    {
        if (alive == false)
            return;

        base.Crouch();

        m_kPlayerAniController.SetBool("crouch", true);  // 这里只是控制变量， crouchMove与standMove间的转换
        m_kPlayerAniController.Play(AniConst.crouch);    // 这里才是播放crouch动画

        height = GetPlayerHeightCrouch();
        if (m_cc != null)
        {
            m_cc.height = height;
            float fR = GetPlayerHeightCrouch() / GetPlayerHeightStand();
            m_cc.center = (GetPlayerCenter() + new Vector3(0, GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0)) * fR;
        }
        
        UpdateHeadSlotPos();
    }

    override public void Stand(bool bSend2Server = false, bool bNeedGrounded = true)
    {
        if (alive == false)
            return;

        base.Stand(bSend2Server, bNeedGrounded);

        m_kPlayerAniController.SetBool("crouch", false);     // 这里只是控制变量， crouchMove与standMove间的转换
        m_kPlayerAniController.Play(AniConst.stand);         // 这里才是播放stand动画

        height = GetPlayerHeightStand();
        if (m_cc != null)
        {
            m_cc.height = GetPlayerHeightStand();
            m_cc.center = GetPlayerCenter() + new Vector3(0, GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        }

        UpdateHeadSlotPos();
    }


    public override void PowerOn()
    {
        if (alive == false)
        {
            return;
        }
        //base.PowerOn();
        //m_kPlayerAniController.Play(AniConst.poweron);
    }

    public void TipC4BombOnHead()
    {
        if (MainPlayer.singleton == null || headSlot == null)
        {
            return;
        }

        if (!WorldManager.singleton.isExplodeModeOpen)
            return;

        if (PlayerBattleModel.Instance.bombHolder == PlayerId && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.REBEL)
        {
            if (gameObject == null || m_transBipHead == null) return;
            playC4UiDirectionTip(true);
        }
        else
        {
            playC4UiDirectionTip(false);
        }
    }

    private void playC4UiDirectionTip(bool isShow)
    {
        if (!isShow)
        {
            if (c4UiDirectionTip != null) c4UiDirectionTip.stop();
        }
        else
        {
            if (c4UiDirectionTip == null) c4UiDirectionTip = UIThroughTheWall.createUI();
            c4UiDirectionTip.play(AtlasName.Battle, "logo_bomb", m_transBipHead, new Vector3(0, 0.3f, 0f), null);
        }
    }

    private void playLogoUiDirectionTip(bool isShow)
    {
        if (!isShow)
        {
            if (logoUiDirectionTip != null) logoUiDirectionTip.stop();
        }
        else
        {
            if (gameObject == null || m_transBipHead == null) return;
            if (logoUiDirectionTip == null) 
                logoUiDirectionTip = UIThroughTheWall.createUI();
            else 
                logoUiDirectionTip.stop();

            if (_logo_type == "LogoZombie")
            {
                logoUiDirectionTip.play(AtlasName.Battle, "logo_matrix", m_transBipHead, new Vector3(0, 0.3f, 0f), null);
            }
            else if (_logo_type == "LogoBigHeadKing")
            {
                logoUiDirectionTip.play(AtlasName.Battle, "big_head_king_icon", headSlot, new Vector3(0, GameConst.HEAD_SLOT_HEIGHT_BIG_HEAD_KING, 0f), null);
            }
            else if (_logo_type == "LogoBigHeadHero")
            {
                logoUiDirectionTip.play(AtlasName.Battle, "big_head_hero_icon", headSlot, new Vector3(0, GameConst.HEAD_SLOT_HEIGHT_BIG_HEAD_HERO, 0f), null);
            }
            else if (_logo_type == "LogoTeamHero")
            {
                var thi = TeamHeroBehaviour.singleton.GetTeamHeroInfo(PlayerId);
                int level = thi != null ? thi.heroLevel : 0;
                if (level > 0)
                {
                    if (Camp == (int)WorldManager.CAMP_DEFINE.UNION)
                        logoUiDirectionTip.play(AtlasName.Battle, "head_icon_hero_orange", headSlot, new Vector3(0, 0.3f, 0f), new ThroughTheWallProp() { starLevel = thi.heroLevel, starPos = new Vector2(0, -22f) });
                    else
                        logoUiDirectionTip.play(AtlasName.Battle, "head_icon_hero_blue", headSlot, new Vector3(0, 0.3f, 0f), new ThroughTheWallProp() { starLevel = thi.heroLevel, starPos = new Vector2(0, -22f) });
                }
                else
                    ShowRelationshipDrectionTip();
            }
            else
            {
                ShowRelationshipDrectionTip();
            }
        }
    }

    private void ShowRelationshipDrectionTip()
    {
        bool isSameCamp = Camp == MainPlayer.singleton.Camp;
        string showLogo = "";
        string logoText = "";
        if (isSameCamp)
        {
            showLogo = "logo_team";

            if (FamilyDataManager.Instance.IsFamilyMember(serverData.pid)) //家族关系
            {
                logoText = FamilyDataManager.Instance.GetShortName(serverData.pid);
            }
            else
            {
                int masterOrStudent = PlayerFriendData.singleton.CheckIsMasterOrStudent(serverData.pid);
                if (masterOrStudent == 1) //师父
                {
                    logoText = "师";
                }
                else if (masterOrStudent == 2) //徒弟
                {
                    logoText = "徒";
                }
                else if (PlayerFriendData.singleton.isFriend(serverData.pid)) //好友关系
                {
                    logoText = "友";
                }
            }
        }
        else
        {
            showLogo = "logo_enemy";
        }

        if(!string.IsNullOrEmpty(showLogo))
        {
            ThroughTheWallProp prop = new ThroughTheWallProp()
            {
                msg = logoText,
                textPos = new Vector2(0, 0),
                textProp = new TextProp() { fontSize = 20, rect = new RectTransformProp() { scale = new Vector3(1f, 1f, 1) } }
            };
            logoUiDirectionTip.play(AtlasName.Battle, showLogo, m_transBipHead, new Vector3(0, 0.3f, 0f), prop);
            return;
        }
    }

    private void playESGViewDirectionTip(bool isShow)
    {
        if (!isShow)
        {
            if (ESGViewTip != null) ESGViewTip.stop();
            update_ESG_View = false;
        }
        else
        {
            if (gameObject == null || m_transBipHead == null) return;
            if (ESGViewTip == null)
                ESGViewTip = UIThroughTheWall.createUI();
            else
                ESGViewTip.stop();
            bool isSameCamp = false;
            string showLogo = "";
            string logoText = PlayerName;
            ThroughTheWallProp prop = new ThroughTheWallProp()
            {
                msg = logoText,
                textPos = new Vector2(0, -35),
                textProp = new TextProp() { fontSize = 20, rect = new RectTransformProp() { scale = new Vector3(1f, 1f, 1f) } }
            };
            if (PlayerBattleModel.Instance.battle_info.camps.Count == 1)
            {
                //不分阵营
                isSameCamp = PlayerId != (WorldManager.singleton.curPlayer == null? -2: WorldManager.singleton.curPlayer.PlayerId);
                foreach (KeyValuePair<int, BasePlayer> kvp in WorldManager.singleton.allPlayer)
                {
                    OtherPlayer op = kvp.Value as OtherPlayer;
                    if (op != null && op.ESGViewTip != null)
                    {
                        if (op.PlayerId == WorldManager.singleton.curPlayer.PlayerId)
                        {
                            op.ESGViewTip.ChangeSprite("logo_team");
                        }
                        else
                        {
                            op.ESGViewTip.ChangeSprite("logo_enemy");
                        }
                    }
                }
            }
            else
            {
                isSameCamp = Camp == (int)WorldManager.CAMP_DEFINE.UNION;
            }
            if (isSameCamp)
            {
                showLogo = "logo_enemy";
            }
            else
            {
                showLogo = "logo_team";
            }

            ESGViewTip.play(AtlasName.Battle, showLogo, m_transBipHead, new Vector3(0, 0.4f, 0f), prop);
            update_ESG_View = false;
        }
    }

    public void ShowDyingDrectionTip(bool isShow, int sec = 0)
    {
        if (uiDirectionTip != null)
        {
            uiDirectionTip.ForceHide(!isShow);
            uiDirectionTip.ShowSprites(isShow);
            uiDirectionTip.ShowDownTimer(sec);
        }

        playRescuingTip(!isShow);
    }

    override public float GetPlayerRadius()
    {
        if (m_configData != null && m_configData.OPRadius != 0)
        {
            return m_configData.OPRadius;
        }
        return base.GetPlayerRadius();
    }

    override public Vector3 GetPlayerCenter()
    {
        if (m_configData != null && m_configData.OPCenter != Vector3.zero)
        {
            return m_configData.OPCenter;
        }
        return base.GetPlayerCenter();
    }
    #endregion

    #region 同步

    override protected void PlayMoveOrStandAni(Vector3 targetPos, Vector3 prePos)
    {
        //if (status.jump || m_curTweenPosData != null && m_curTweenPosData.jumpStage == GameConst.JUMP_STAGE_GROUND)
        //    return;

        var from = new Vector2(prePos.x, prePos.z);
        var to = new Vector2(targetPos.x, targetPos.z);
        var moveDir = to - from;
        var forward = m_kTrans.forward;
        var right = m_kTrans.right;

        var dirForwardBack = 0;
        var dotForwardBack = forward.x * moveDir.x + forward.z * moveDir.y;
        if (dotForwardBack > 0.1f)
            dirForwardBack = 1;
        else if (dotForwardBack < -0.1f)
            dirForwardBack = -1;

        var dirLeftRight = 0;
        var dotLeftRight = right.x * moveDir.x + right.z * moveDir.y;
        if (dotLeftRight > 0.1f)
            dirLeftRight = 1;
        else if (dotLeftRight < -0.1f)
            dirLeftRight = -1;

        m_kPlayerAniController.SetFloat(AniConst.FLOAT_DIR_FORWARD_BACK, dirForwardBack);
        m_kPlayerAniController.SetFloat(AniConst.FLOAT_DIR_LEFT_RIGHT, dirLeftRight);

        bool play = dirForwardBack != 0 || dirLeftRight != 0;
        //if (play == false)
        //    Logger.Log("CheckIfPlayMoveAni false");
        PlayMoveAni(play);
    }

    override protected void PlayMoveAni(bool isMove)
    {
        if (status.jump)
            return;

        //status.move = isMove;
        m_kPlayerAniController.SetBool("move", isMove);
        if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
        {
            m_kPlayerAniController.SetBool("brestWalk", isMove);
        }

        //if(!isMove)
        //    Logger.Warning(isMove ? "走" : "站");
    }

    #endregion

    #region 模型缓存
    override protected void CacheGO(GameObject go)
    {
        if (go == null)
            return;

        if (m_kPlayerAniController != null)
            m_kPlayerAniController.EnableUpBodyLayer(true);

        DetachWidget();

        base.CacheGO(go);

        if (m_bReleaseGO)
            return;

        //string name = go.name;
        //List<GameObject> listGO = null;
        //if (ms_dicGOCache.TryGetValue(name, out listGO) == false)
        //{
        //    listGO = new List<GameObject>();
        //    ms_dicGOCache.Add(name, listGO);
        //}
        //
        ////Logger.Log(this + " CacheGO " + go.name + " " + go.GetInstanceID());
        //
        //listGO.Add(go);
        PlayerManager.CacheGO(go.name + "OP", go);
    }

    override protected GameObject GetCacheGO(string name)
    {
        return PlayerManager.GetGO(name + "OP");
        //List<GameObject> listGO = null;
        //ms_dicGOCache.TryGetValue(name, out listGO);
        //if (listGO == null || listGO.Count <= 0)
        //    return null;
        //
        //GameObject go = listGO[listGO.Count - 1];
        //listGO.RemoveAt(listGO.Count - 1);
        //
        ////Logger.Log(this + " GetCacheGO " + go.name + " " + go.GetInstanceID());
        //
        //return go;
    }

    static public void ReleaseCacheGO()
    {
        //foreach (KeyValuePair<string, List<GameObject>> kvp in ms_dicGOCache)
        //{
        //    for (int i = 0; i < kvp.Value.Count; i++)
        //    {
        //        ResourceManager.Destroy(kvp.Value[i]);
        //    }
        //}
        //ms_dicGOCache.Clear();
    }
    #endregion

    #region Model相关
    override public void SetModelName()
    {
        m_strModel = m_configData == null ? "" : m_configData.OPModel;
    }

    override protected void SetModelSex()
    {
        if (string.IsNullOrEmpty(m_strModel))
            m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        else
        {
            string lowerModelName = m_strModel.ToLower();
            if (lowerModelName.Contains(ModelConst.MODEL_PLAYER_GIRL_CHAR))
                m_iModelSex = ModelConst.MODEL_PLAYER_GIRL_SEX;
            else
                m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        }
    }

    #endregion

    #region 技能相关
    internal override void UseWeaponAwakenSkill(toc_fight_weapon_awaken_skill proto)
    {
        if (skillMgr != null)
            m_skillMgr.OPUseWeaponAwakenSkill(proto.awaken_id, proto.skill_id, proto.pos, proto.target_ids, "");
    }

    internal override void DoAIAction(toc_fight_ai_action proto)
    {
        if (m_skillMgr != null)
            m_skillMgr.DoAIAction(proto.action, proto.pos);
    }

    internal override void DoAISkill(toc_fight_ai_skill proto)
    {
        if (m_skillMgr != null)
            m_skillMgr.DoAISkill(proto.skill, proto.targets);
    }

    //internal override void DoAISkill(int skill, float[][] targets)
    //{
    //    m_skillMgr.DoAISkill(skill, targets);
    //}

    internal override void Rush(float distance, float time)
    {
        if (m_kPlayerAniController != null)
            m_kPlayerAniController.Play("fire4");
        m_posTweener.enabled = true;

        m_actQueue.Clear();

        Vector3 targetPos = transform.position + transform.forward * distance;

        m_posTweener.TweenPos(time, targetPos);

        m_lastTweenTime = time;
    }

    public override void RushDone()
    {
        if (m_kPlayerAniController != null)
            m_kPlayerAniController.SetTrigger("rush_done");
    }

    internal override void ClientMove(Vector3 targetPos, float time)
    {
        if (null != m_posTweener)
        {
            m_posTweener.enabled = true;
            m_actQueue.Clear();

            m_posTweener.TweenPos(time, targetPos);
            m_lastTweenTime = time;
        }
    }

    internal override void ClientStop(float time)
    {
        if (null != m_posTweener && time > 0)
        {
            m_posTweener.enabled = true;
            //m_posQueue.Clear();
            m_posTweener.ClientStop(time);
        }
    }

    #endregion

    #region 挂件相关
    public void AttachWidget()
    {
        if (m_kWidgetManager != null && serverData != null)
        {
            m_kWidgetManager.AttachWidget(serverData.listWidget, (widget) =>
            {
                if (widget == null || widget.gameObject == null)
                    return;
                //Logger.Log(string.Format("AttachWidget 玩家【{0}(id:{1})】挂上挂件【{2}(id:{3})】 instanceId:{4}", ColorName, PlayerId, widget.config.DispName, widget.id, widget.gameObject.GetInstanceID()));
                ExtendFuncHelper.CallBackBool<GameObject> funVisit = ((child) =>
                {
                    Renderer r = child.GetComponent<Renderer>();
                    if (r != null)
                    {
                        m_kVM.AddRenderer(r);
                    }
                    return true;
                });

                widget.transform.VisitChild(funVisit, true);
                m_kVM.RefreshVisable();
            });
        }
    }
    public void DetachWidget()
    {
        if (m_kWidgetManager != null)
        {
            m_kWidgetManager.DetachAllWidget();
        }

    }
    #endregion

    #region 角色设置
    override public void SetHeadScale(float scale)
    {
        m_headScale = scale;
        if (m_kPlayerController != null)
            m_kPlayerController.SetHeadScale(scale);
        if (m_hpBar != null && m_hpBar.isPlaying)
            m_hpBar.offsetPos = new Vector3(0, (GameConst.HEAD_SLOT_HEIGHT_HP_BAR + 0.05f) * m_headScale, 0);
    }

    public override void SetScale(float scale)
    {
        if (gameObject != null)
        {
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    public override void RecoverView(BasePlayer fpBasePlayer)
    {
        base.RecoverView(fpBasePlayer);
        if (m_headScale != 0)
            SetHeadScale(m_headScale);
    }

    public override void HPAlert(bool isAlert)
    {
        if(m_bIsHPAlert != isAlert)
            FadeColor(isAlert, Color.green);
        base.HPAlert(isAlert);
    }
    #endregion

    #region 其他
    protected override void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        m_iOldState = -1;
        if (data.state == GameConst.Fight_State_GameStart)
        {
            buffManager.RemoveAllBuff();
            _data_updated = true;
            update_ESG_View = true;
        }
    }
    #endregion
}


