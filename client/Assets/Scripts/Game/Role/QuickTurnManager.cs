﻿using UnityEngine;
using System.Collections;

public class QuickTurnManager 
{
    private static QuickTurnManager _instance;
    public static QuickTurnManager Ins
    {
        get
        {
            if (_instance == null)
                _instance = new QuickTurnManager();
            return _instance;
        }        
    }
    /// <summary>
    /// 快速转身开关
    /// </summary>
    public bool quickTurnOpen = false;
    /// <summary>
    /// 转身角度
    /// </summary>
    public float trunEulerAngle;
    /// <summary>
    /// 扩增系数
    /// </summary>
    public float plusValue;
    public bool isShowGUI = false;
    private string str;
    public QuickTurnManager()
    {
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("quick_turn");
        string[] arr = cfg.ValueStr.Split(';');
        quickTurnOpen = int.Parse(arr[0]) == 1 ? true : false;
        trunEulerAngle = float.Parse(arr[1]);
        plusValue = float.Parse(arr[2]);
        str = plusValue.ToString();
    }
	
    public float QuickTurnCheck( float eulerAngelY )
    {
        if (!Driver.isMobilePlatform)
            return eulerAngelY;
        if (quickTurnOpen && eulerAngelY > trunEulerAngle)
            eulerAngelY *= plusValue;
        return eulerAngelY;
    }
    
    public void DrawGUI()
    {
        quickTurnOpen = GUI.Toggle(new Rect(300, 0, 100, 20), quickTurnOpen,"快速转身");
        GUI.Label(new Rect(250, 100, 100, 20), "角度" + trunEulerAngle.ToString());
        trunEulerAngle = GUI.HorizontalSlider(new Rect(300, 100, 300, 40), trunEulerAngle, 0, 180);
        str =GUI.TextArea(new Rect(300, 160, 50, 20), str);
        if(GUI.Button(new Rect(400,160,50,30),"确定"))
        {
            plusValue = float.Parse(str);
        }
    }

}
