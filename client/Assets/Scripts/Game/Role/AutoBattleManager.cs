﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 自动战斗管理器
/// </summary>
public class AutoBattleManager
{
    private MainPlayer m_kPlayer;

    private bool m_isCanAutoFire = false;
    private bool m_isAutoFire = false;
    private bool m_isSwitchAutoOn = false;
    private int m_lastGunID;
    private int m_lastBulletCont;
    private bool m_isAimEnemy = false;
    private bool m_isAutoRotateMode = false;
    private bool m_isAutoRotate = false;
    private float m_fPlayerLastActionTime = 0f;
    private bool m_isPlayerOperated = false;
    private bool m_isAutoAimBefore = AutoAim.Enable;
    private float m_fTick;
    private const float UPDATE_TICK_INTERVAL = 3f;

    public AutoBattleManager(MainPlayer player)
    {
        m_kPlayer = player;
        m_isAutoRotateMode = WorldManager.singleton.gameRuleLine.CanAutoShoot == 2;
        m_isSwitchAutoOn = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE) > 0.1f;
        RegisterEvent();
    }

    /// <summary>
    /// 注册事件
    /// </summary>
    private void RegisterEvent()
    {
        GameDispatcher.AddEventListener<BasePlayer, bool>(GameEvent.UI_SHOW_AIM_PLAYER_NAME, OnAimPlayer);
        GameDispatcher.AddEventListener(GameEvent.UI_CANCEL_AIM_PLAYER_NAME, OnCancelAimPlayer);
        GameDispatcher.AddEventListener<bool>(GameEvent.MAINPLAYER_SET_IS_CAN_AUTOFIRE, SetIsCanAutoFire);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_SWITCH_AUTOFIRE, SwitchAutoBattle);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_STOP_AUTOFIRE, StopAutoFire);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
    }

    /// <summary>
    /// 注销事件
    /// </summary>
    private void UnRegisterEvent()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, bool>(GameEvent.UI_SHOW_AIM_PLAYER_NAME, OnAimPlayer);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CANCEL_AIM_PLAYER_NAME, OnCancelAimPlayer);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.MAINPLAYER_SET_IS_CAN_AUTOFIRE, SetIsCanAutoFire);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_SWITCH_AUTOFIRE, SwitchAutoBattle);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_STOP_AUTOFIRE, StopAutoFire);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
    }

    public void Update()
    {
        if (m_kPlayer != null && m_isCanAutoFire && ++m_fTick >= UPDATE_TICK_INTERVAL)
        {
            m_fTick = 0;
            UpdateAutoBattle();
            AutoAim.AutoBattleAim();
        }
    }

    /// <summary>
    /// 更新自动战斗
    /// </summary>
    public void UpdateAutoBattle()
    {
        if (!m_isAimEnemy)
        {
            if (m_isAutoRotateMode )
            {
                float now = Time.time;

                if (m_kPlayer.status.move || m_kPlayer.status.jump)
                    m_fPlayerLastActionTime = now;

                if((now - m_fPlayerLastActionTime) >= 3f)
                    m_isPlayerOperated = false;

                if (m_isSwitchAutoOn && !m_isPlayerOperated)
                    DoAutoRotate();//做自动转向
                else
                {
                    AutoAim.Enable = m_isAutoAimBefore;
                    AutoAim.AutoAimTarget = null;
                    AutoAim.AutoBattleTarget = null;
                }
            }
            if (m_isAutoFire)
            {
                m_isAutoFire = false;
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, false);
                m_isPlayerOperated = false;
            }
            return;
        }

        if (m_isAutoRotateMode)
        {
            m_isAutoRotate = false;
            AutoAim.Enable = m_isAutoAimBefore;
        }

        DoAutoFire();
    }

    /// <summary>
    /// 执行自动转向
    /// </summary>
    void DoAutoRotate()
    {
        if (AutoAim.AutoAimTarget != null)
        {
            BasePlayer bpTarget = WorldManager.singleton.GetPlayerWithGOHash(AutoAim.AutoAimTarget.gameObject.GetHashCode());
            if (bpTarget != null && AutoAim.IsAutoAimTarget(bpTarget))
                return;
        }

        List<BasePlayer> targetPlayers = new List<BasePlayer>();

        foreach(var player in WorldManager.singleton.allPlayer.Values)
        {
            if (player.gameObject == null)
                continue;

            if (AutoAim.IsAutoAimTarget(player))
                targetPlayers.Add(player);
        }

        int count = targetPlayers.Count;

        if (count == 0)
        {
            AutoAim.Enable = m_isAutoAimBefore;
            AutoAim.AutoAimTarget = null;
            AutoAim.AutoBattleTarget = null;
            return;
        }
        else if (count > 1)
        {
            targetPlayers.Sort(delegate(BasePlayer p1, BasePlayer p2)
            {
                return Vector3.Distance(m_kPlayer.position, p1.position) - Vector3.Distance(m_kPlayer.position, p2.position) >= 0 ? 1 : -1;
            });
        }

        BasePlayer targetBp = targetPlayers[0];
        AutoAim.Enable = false;
        AutoAim.AutoAimTarget = null;
        AutoAim.AutoBattleTarget = targetBp.transform;
        m_isAutoRotate = true;
    }

    /// <summary>
    /// 执行自动开火
    /// </summary>
    void DoAutoFire()
    {
        if (!m_isSwitchAutoOn)
        {
            if (m_isAutoFire)
            {
                m_isAutoFire = false;
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, m_isAutoFire);
            }
            return;
        }
        else
        {
            var weapon = m_kPlayer.curWeapon;
            if (weapon == null || weapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_GRENADE || weapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_RIFLEGRENADE)
                return;
            var playerStatu = m_kPlayer.status;
            if (weapon.ammoNumInClip == 0 || playerStatu.reload || playerStatu.switchWeapon)
            {
                if (m_isAutoFire)
                {
                    m_isAutoFire = false;
                    GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, false);
                }
            }
            else if (weapon.weaponConfigLine.FireMode != 1)//可连射,自动开火只开启一次
            {
                if (weapon.weaponConfigLine.FireMode == WeaponBase.FIRE_MODE_BURST)
                {
                    if ((Time.time - weapon.lastFireTime) > weapon.FireRate)
                    {
                        GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, true);
                    }
                }
                else
                {
                    if (!m_isAutoFire)
                    {
                        m_isAutoFire = true;
                        GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, true);
                    }
                }
                
            }
            else if (weapon.weaponConfigLine.SubType == "WEAPON2" || weapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SHOT_GUN) //副武器或者是霰弹枪
            {
                m_isAutoFire = true;
                if ((Time.time - weapon.lastFireTime) > weapon.FireRate)
                {
                    GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, true);
                }
            }
            m_isPlayerOperated = false;
        }
    }

    /// <summary>
    /// 停止自动开火状态
    /// </summary>
    private void StopAutoFire()
    {
        m_isAutoFire = false;
    }

    public void SwitchAutoBattle()
    {
        if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE) < 0.1f)
        {
            GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE, 1.0f);
            m_isSwitchAutoOn = true;
            m_isPlayerOperated = false;
        }
        else
        {
            GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE, 0);
            m_isSwitchAutoOn = false;
            if (m_isAutoRotateMode)
                AutoAim.Enable = m_isAutoAimBefore;
        }
    }

    /// <summary>
    /// 设置能否自动开火
    /// </summary>
    /// <param name="isCanAutoFire"></param>
    public void SetIsCanAutoFire(bool isCanAutoFire)
    {
        m_isCanAutoFire = isCanAutoFire;
    }

    public void Release()
    {
        UnRegisterEvent();
    }

    private void OnAimPlayer(BasePlayer player, bool isAimEnemy)
    {
        m_isAimEnemy = isAimEnemy;
        if (m_isAutoRotateMode && isAimEnemy && m_isAutoRotate)
        {
            AutoAim.AutoAimTarget = player.transform;
            m_isAutoRotate = false;
        }
    }

    private void OnCancelAimPlayer()
    {
        m_isAimEnemy = false;
    }

    private void OnPlayerDoAnyThing()
    {
        m_fPlayerLastActionTime = Time.time;
        m_isPlayerOperated = true;
    }

    public void OnPlayerRotate()
    {
        m_fPlayerLastActionTime = Time.time;
        m_isPlayerOperated = true;
    }

}
