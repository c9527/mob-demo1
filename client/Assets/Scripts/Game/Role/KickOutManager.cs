﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType
{
    NONE,
    INPUT_KEY,
    MOVE_MOUSE,
    TOUCH,
}

class KickOutManager
{
    private const float AlertTime = 15f; //预警时间（秒）
    private const string AlertTip = "您长时间未操作，系统即将把您移出房间\n\n进行任意操作继续游戏\n";
    private const string KickTip = "您长时间未操作，系统已把您移出房间";

    private float m_timeOutTime;
    private float m_lastActionTime;
    private ActionType m_lastActionType;

    private bool m_bActive;
    private bool m_bIsAlert;

    public KickOutManager()
    {

    }

    public void Init(float timeOutTime)
    {
        m_timeOutTime = timeOutTime;
        m_bActive = false;
        m_bIsAlert = false;
        if (m_timeOutTime > 0)
            Run();
    }

    public void Run()
    {
        if (m_bIsAlert)
            UIManager.HideTipPanel();
        m_bActive = true;
        m_bIsAlert = false;
        m_lastActionType = ActionType.NONE;
        m_lastActionTime = Time.time;
    }

    public void Update()
    {
        if (m_bActive)
        {
            float totalTime = Time.time - m_lastActionTime;
            if (!m_bIsAlert && (totalTime >= (m_timeOutTime - AlertTime)))
            {
                m_bIsAlert = true;
                UIManager.ShowTipPanel(AlertTip, Run, null, false, false, "继续游戏", null, m_timeOutTime - totalTime);
            }
            else if(totalTime >= m_timeOutTime)
            {
                m_bActive = false;
                m_bIsAlert = false;
                UIManager.ShowTipPanel(KickTip).DontDestroyOnLoad = true;
                //发送退出的协议
                NetLayer.Send(new tos_room_leave());
                RoomModel.SetCustomRoomLeaveType(RoomLeaveType.Force);
            }
        }
    }

    public void SetLastAction(ActionType lastActionType)
    {
        if (m_bIsAlert)
            Run();
        else
        {
            m_lastActionTime = Time.time;
            m_lastActionType = lastActionType;
        }
    }

    public void Release()
    {
        m_bActive = false;
        if (m_bIsAlert)
            UIManager.HideTipPanel();
    }

    public float lastTime
    {
        get { return m_lastActionTime; }
    }

    public ActionType lastActionType
    {
        get { return m_lastActionType; }
    }

}
