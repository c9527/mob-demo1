﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;


public class MainPlayer : BasePlayer
{
    //static private Dictionary<string, List<GameObject>> ms_dicGOCache = new Dictionary<string, List<GameObject>>();

    public static MainPlayer singleton;
    public List<HarmInfo> m_BeAttckHarmList = new List<HarmInfo>();



    protected CharacterController m_kCharacterCtrler;

    // Component
    private Transform m_transMainPlayerCamera;
    private Camera m_cameraMainPlayer;
    private int m_kWeaponHoldHand = GameConst.PLAYER_WEAPON_HOLD_HADN_RIGHT;
    private Transform m_transMainPlayerCameraSlot;
    private Transform m_transMainPlayerCameraShakeTarget;
    private Animation m_kMainPlayerCameraAnimation;
    private CapsuleCollider m_cc;
    protected MainPlayerController m_kMPC;

    private MainPlayerCameraShaker m_kMPCS;
    private DecelerateManager m_deceMgr;    
    //
    private VtfIntCls m_canJump2 = new VtfIntCls();
    public bool canFly = false;

    public static float[] m_pitchRange
    {
        get
        {
            ConfigMapListLine cfg = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString());
             return cfg == null ? new float[] { -90, 90 } : cfg.PitchRange;            
        }
    }

    public class MoveSpeedData
    {
        public EptFloat standMoveForwardSpeed;
        public EptFloat standMoveSideSpeed;
        public EptFloat standMoveBackSpeed;
        public EptFloat crouchMoveForwardSpeed;
        public EptFloat crouchMoveSideSpeed;
        public EptFloat crouchMoveBackSpeed;

        public VtfVector3Cls ctrlPos = new VtfVector3Cls();
    }

    public MoveSpeedData m_moveSpeedData;
    // Role Data

    //
    private bool m_bCameraMoveAniPlaying;
    private bool m_bCameraIdleAniPlaying;
    private EptFloat m_fSceneCameraSlotLocalY;
    private Vector3 m_vec3PosLastSend;
    private Vector3 m_vec3AngleLastSend;
    protected int m_iSwitchTimer = -1;
    protected int m_iSwitchSendProtoTimer = -1; //发送协议定时器

    private uint m_fixBulletTime = 0;
    private bool m_bUseAdvAmmo;
    private bool m_bChangeModel;
    private bool m_bReplaySwitchWeapon;
    private float m_sendPosTime;
    private float m_motionlessTime;
    private bool m_bFirstLoad;

    public bool firstLoad
    {
        get { return m_bFirstLoad; }
    }

    private UIEffect m_addLifeEffect;
    /// <summary>
    /// 标志是否移动慢走
    /// </summary>
    private bool m_bSlowMove;

    public bool SlowMove
    {
        get
        {
            return m_bSlowMove;
        }
        set
        {
            m_bSlowMove = value;
        }
    }

    //禁止移动
    private bool m_bForbidMove;
    public bool ForbidMove
    {
        get
        {
            return m_bForbidMove;
        }
        set
        {
            m_bForbidMove = value;
            if (m_bForbidMove)
            {
                status.move = false;
                m_bCameraIdleAniPlaying = false;
                m_bCameraMoveAniPlaying = false;
            }
        }
    }


    //禁止使用技能
    public bool ForbidUseSkill;

    public bool ForbidFire;
    public bool ForbidLookAround;
    public bool ForbidJump;
    public bool ForbidGravity;

    private bool m_bForbidMainCam;
    public bool ForbidMainCam
    {
        get { return m_bForbidMainCam; }
        set
        {
            m_bForbidMainCam = value;
            if (gameObject != null)
            {
                EnableMainPlayerCamera(!m_bForbidMainCam && alive);
            }
        }
    }


    private bool m_bForbidSceneCam;
    public bool ForbidSceneCam
    {
        get { return m_bForbidSceneCam; }
        set
        {
            m_bForbidSceneCam = value;
            if (gameObject != null && SceneCamera.singleton != null)
            {
                SceneCamera.singleton.enableCamera = !m_bForbidSceneCam;
            }
        }
    }

    public bool IsFrezee;    //是否在冻结状态（不能动）
    public bool IsHideView;  //是否在躲藏观战状态
    private Vector3 m_vecSceneCamSlotFrezeePos;
    private Vector3 m_vceSceneCamSlotFrezeeRot;

    private PassiveSkillManager m_kPSM;
    private PassiveSkillTextManager m_kPSTM;
    private KickOutManager m_kKOMgr;
    private AutoBattleManager m_kABM;

    private float m_sceneFov;
    private short m_sendTime;

    public MainPlayer()
    {
        singleton = this;       
    }

    protected override void Init()
    {
        base.Init();

        serverData.SetPosChangedCallBack(OnServerPosChanged);

        m_kPSM = new PassiveSkillManager();
        m_kPSTM = new PassiveSkillTextManager();
        m_kKOMgr = new KickOutManager();
        m_kKOMgr.Init(WorldManager.singleton.gameRuleLine.KickOutTime);
        m_kABM = new AutoBattleManager(this);
        
        //m_kCameraSwitcher = new CameraSwitcher();
        m_deceMgr = new DecelerateManager();
        
        m_moveSpeedData = new MoveSpeedData();

        ReActive();
        //GameDispatcher.AddEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnFightStatInfoUpdate);
        
    }

    public void SetSceneFov(float value)
    {
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.fieldOfView = SceneFov * value;
    }

    public void SetMainPlayerFov(float value)
    {
        m_cameraMainPlayer.fieldOfView = MainFov * value;
    }

    public void EnableMainPlayerCamera(bool value)
    {
        if (m_cameraMainPlayer != null)
        {
            m_cameraMainPlayer.enabled = value;
        }
        if (value && FPWatchPlayer.singleton != null)
        {
            FPWatchPlayer.singleton.EnableMainPlayerCamera(false);
        }
    }

    public void FlipMainCamera(int holdHand)
    {
        if (m_cameraMainPlayer != null && holdHand != m_kWeaponHoldHand)
        {
            m_kWeaponHoldHand = holdHand;
            Matrix4x4 mat = m_cameraMainPlayer.projectionMatrix;
            mat *= Matrix4x4.Scale(new Vector3(-1, 1, 1));
            m_cameraMainPlayer.projectionMatrix = mat;
        }
    }

    public void ResetCameraFov()
    {
        if (SceneCamera.singleton != null)
            SceneCamera.singleton.fieldOfView = SceneFov;
        if (m_cameraMainPlayer != null)
            m_cameraMainPlayer.fieldOfView = MainFov;
    }

    private void ReloadConfig()
    {
        //m_recoil = curWeapon.weaponConfigLine;
    }

    public override void ReActive()
    {
        base.ReActive();
        if (m_kMPC != null)
            m_kMPC.ReActive();
        ForbidMove = false;
        ForbidFire = false;
        ForbidLookAround = false;
        IsHideView = false;
        m_sendTime = 0;
        GameDispatcher.AddEventListener(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE, ReloadConfig);
        GameDispatcher.AddEventListener<int>(GameEvent.UI_BATTLEPARAM_SETTING_HAND_TYPE, FlipMainCamera);
    }

    override public void Release(bool releaseGO)
    {
        base.Release(releaseGO);

        if (SceneCamera.singleton != null)
        {
            SceneCamera.singleton.SetParent(null);
            if (SceneCamera.singleton.cameraShake != null)
                SceneCamera.singleton.cameraShake.RemoveCamera(m_cameraMainPlayer);
        }

        ForbidMove = false;
        ForbidFire = false;
        ForbidLookAround = false;
        ForbidJump = false;
        ForbidGravity = false;
        ForbidMainCam = false;
        ForbidSceneCam = false;
        //m_bFireAniPlayed = false;
        m_bFirstLoad = false;

        if (releaseGO)
        {
            if (m_kMPC != null)
                m_kMPC.Release();
            m_kMPC = null;

            if (m_kPSM != null)
                m_kPSM.Release();
            m_kPSM = null;

            if (m_kPSTM != null)
                m_kPSTM.Release();
            m_kPSTM = null;

            if (m_kKOMgr != null)
                m_kKOMgr.Release();
            m_kKOMgr = null;

            if (m_kABM != null)
                m_kABM.Release();
            m_kABM = null;

            m_deceMgr = null;
            m_cameraMainPlayer = null;
            m_transMainPlayerCamera = null;
            m_transSceneCameraSlot = null;
            m_transMainPlayerCameraSlot = null;
            m_transMainPlayerCameraShakeTarget = null;
            m_kMainPlayerCameraAnimation = null;
            m_kMPCS = null;
            if (m_addLifeEffect != null)
            {
                m_addLifeEffect.Destroy();
                m_addLifeEffect = null;
            }
        }
        else
        {
            if (isDmmModel)
            {
                if (m_kMPC != null)
                    m_kMPC.Release();
            }
        }

        GameDispatcher.RemoveEventListener(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE, ReloadConfig);
        GameDispatcher.RemoveEventListener<int>(GameEvent.UI_BATTLEPARAM_SETTING_HAND_TYPE, FlipMainCamera);
        //GameDispatcher.RemoveEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnFightStatInfoUpdate);
    }

    override public void LoadRes()
    {
        if (SceneManager.singleton.battleSceneLoaded == false)
        {
            //Logger.Log("LoadRes battleSceneLoaded == false", this);
            return;
        }

        if (m_strModel == null || m_strModelPre == m_strModel && m_loadModel)    // 模型只加载一次
        {
            //Logger.Log("LoadRes " + m_strModel + " pre: "+m_strModelPre);
            return;
        }

        if (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX ||
            serverData.actor_type == GameConst.ACTOR_TYPE_ELITE ||
            serverData.actor_type == GameConst.ACTOR_TYPE_GENERAL ||
            serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX)
            AudioManager.PlayFightUISound(AudioConst.humenBecomeBio);

        if (serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX ||
            serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX)
            AudioManager.PlayFightUISound(AudioConst.bioMotherShow);

        if (m_loadModel == false)
        {
            m_strModelPre = m_strModel;
            m_iModelTypePre = m_iModelType;
            m_loadModel = true;
        }

        m_kWeaponMgr.curWeapon = null;
        m_kWeaponMgr.preWeapon = null;

        if (m_strModelPre != m_strModel && gameObject != null)
        {
            m_bChangeModel = true;
            WorldManager.singleton.UnRegisterPlayerGOHash(m_kGo.GetHashCode());
            DestroyPlayerOldModel();
        }

        string path = GetRolePrefablPath(m_iModelType, m_strModel);
        //string path = m_iModelType == GameConst.MODEL_TYPE_DMM ? PathHelper.GetDmmPrefabPath(m_strModel) : PathHelper.GetRolePrefabPath(m_strModel);
        GameObject goCC = null;

        Action<GameObject, uint> loadCallBack = (go, crc) =>
        {
            if (go == null)
            {
                Logger.Error("MainPlayer model load failed, " + m_strModel);
                return;
            }

#if UNITY_EDITOR
            if (crc == 4399)
            {
                GameObjectHelper.RefreshMaterial(go.transform);
            }
#endif

            string modelName = GameObjectHelper.GetNameWithoutClone(go.name);

            if (goCC != null && !go.transform.FindChild(modelName))
            {
                goCC.name = modelName;
                go.transform.SetParent(goCC.transform);
                go.transform.ResetLocal();
                go = goCC;
            }

            if (released)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                //TryToDetachSceneCamera();
                CacheGO(go);
                //GameObject.Destroy(go);
                return;
            }

            if (modelName != m_strModel)
            {
                WorldManager.singleton.UnRegisterPlayerGOHash(go.GetHashCode());
                //TryToDetachSceneCamera();
                CacheGO(go);
                //GameObject.Destroy(go);
                Logger.Warning("MainPlayer loaded model is not the current model,  " + modelName + " " + m_strModel);
                return;
            }

            if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
                FPWatchPlayer.singleton.EndWatch();

            if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }

            float[] prePos = new float[] { position.x, position.y, position.z, eulerAngle.x, eulerAngle.y, eulerAngle.z };
            SetGO(go);

            if (modelType == m_iModelTypePre && isDmmModel)
            {
                SetPosNow(prePos);
            }
            else
            {
                SetPosNow(serverData.pos);
            }

            //if (!isDmmModel || modelType != m_iModelTypePre)
            //{
            //    SetFreeze(false);
            //}
            SetFreeze(false);
            SetState(serverData.state, false);

            if (isAlive)
            {
                if (serverData.actorState != null)
                    SetBuff(serverData.actorState.buff_list);
                else if (serverData.actorState == null && serverData.roleData.buff_list != null)
                    SetBuff(serverData.roleData.buff_list);
            }

            if (!m_bChangeModel && m_bReplaySwitchWeapon)
            {
                if (curWeapon != null)
                {
                    m_kPlayerAniController.Play(curWeapon.weaponConfigLine.MPSwitchAni);
                }
                m_bReplaySwitchWeapon = false;
            }
            m_bChangeModel = false;

            WorldManager.singleton.RegisterPlayerGOHash(go.GetHashCode(), this);
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_INIT_DONE);
            if (!m_bFirstLoad)
            {
                m_bFirstLoad = true;
                BattleNoticeManager.Instance.OnPlayerJoin(this);
                NetLayer.Send(new tos_fight_res_load_done());
            }


            //ResourceManager.ClearDepend(path);
        };

        GameObject goCached = GetCacheGO(m_strModel);
        if (goCached != null)
        {
            Logger.Log("获得缓存模型:" + m_strModel);
            loadCallBack(goCached, 4399);
        }
        else
        {
            ResourceManager.LoadAsset(PathHelper.GetRolePrefabPath(ModelConst.MODEL_MAINPLAYER_CC_GO), (obj) =>
            {
                goCC = Object.Instantiate(obj) as GameObject;
                //ResourceManager.LoadModel(path, loadCallBack);
                ResourceManager.LoadBattlePrefab(path, loadCallBack, EResType.Battle, true, false);
            }, false);
        }
    }

    protected override void DestroyPlayerOldModel()
    {
        m_kMPC.Release();
        base.DestroyPlayerOldModel();
        if(m_cameraMainPlayer != null)
        {
            if (SceneCamera.singleton != null && SceneCamera.singleton.cameraShake != null)
            {
                SceneCamera.singleton.cameraShake.RemoveCamera(m_cameraMainPlayer);
            }
            //还原成右手
            FlipMainCamera(GameConst.PLAYER_WEAPON_HOLD_HADN_RIGHT);
            m_cameraMainPlayer = null;
        }
    }

    override protected void SetGO(GameObject go)
    {
        base.SetGO(go);

        GameObjectHelper.GetComponentsByTraverse<Renderer>(go).useLightProbes = true;

        if (m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            m_transSceneCameraSlot.localPosition = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
            m_transSceneCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT;
            m_fSceneCameraSlotLocalY = m_transSceneCameraSlot.localPosition.y;

            m_transMainPlayerCameraSlot = GameObjectHelper.CreateChildIfNotExisted(go.transform, ModelConst.SLOT_MAINPLAYER_CAMERA).transform;
            m_transMainPlayerCameraSlot.localPosition = GameSetting.MAIN_PLAYER_CAMERA_LOCALPOS;
            m_transMainPlayerCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_CAMERA_LOCALROT;
            InitMainPlayerCamera();
        }

        m_transHips = null;
        GameObjectHelper.VisitChildByTraverse(go.transform, (child) =>
        {
            if (isDmmModel && child.GetComponent<Collider>() != null)
                child.gameObject.DestroyComponent<Collider>();

            if (child.name == ModelConst.BIP_NAME_PEVIS)
                m_transHips = child.transform;
        });

        height = GetPlayerHeightStand();

        //添加CharacterController
        m_kCharacterCtrler = go.AddMissingComponent<CharacterController>();
        m_kCharacterCtrler.radius = GetPlayerRadius();
        m_kCharacterCtrler.height = height;
        //m_kCharacterCtrler.center = new Vector3(0, m_kCharacterCtrler.height * 0.5f + GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        m_kCharacterCtrler.center = GetPlayerCenter() + new Vector3(0, GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0);
        m_kCharacterCtrler.slopeLimit = GameSetting.PlayerColliderSlope;
        m_kCharacterCtrler.stepOffset = GameSetting.MPStepOffset;

        CapsuleCollider cc = go.AddMissingComponent<CapsuleCollider>();
        m_cc = cc;
        cc.isTrigger = true;
        cc.height = m_kCharacterCtrler.height;
        cc.radius = m_kCharacterCtrler.radius;
        cc.center = m_kCharacterCtrler.center;

        m_kMPC = GameObjectHelper.GetComponent<MainPlayerController>(go);
        m_kMPC.BindPlayer(this);
        AutoAim.Init();

        m_kMPCS = GameObjectHelper.GetComponent<MainPlayerCameraShaker>(go);
        m_kMPCS.BindMainPalyerCamera(m_transMainPlayerCameraShakeTarget);
        if (m_iModelType == GameConst.MODEL_TYPE_ROLE && m_kPlayerAniController is PlayerAnimatorController)
        {
            m_kPlayerAniController.animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
        }

        m_kStepSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());
        m_kHeartSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());
        m_kVoiceSoundPlayer.SetAudioSource(SceneCamera.singleton.gameObject.AddComponent<AudioSource>());

        m_arrBehaviour = m_kGo.GetComponentsInChildren<Behaviour>();

        SetActive(false);
    }

    public override void SetState(int state, bool dataRevive)
    {
        base.SetState(state, dataRevive);
        if (state == GameConst.SERVER_ACTOR_STATUS_DEAD) //直接进入观察者
        {
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_GAME_START_DIE);
        }
    }

    override protected void ClearState()
    {
        base.ClearState();
        if (m_kMPC != null)
            m_kMPC.Reset();
    }

    private string GetArmModel()
    {
        return "arm";
    }

    private void InitMainPlayerCamera()
    {
        m_transMainPlayerCameraShakeTarget = m_transMainPlayerCameraSlot.FindChild("MainPlayerCameraShakeTarget");
        if (m_transMainPlayerCameraShakeTarget == null)
        {
            GameObject goMainPlayerCameraShakeTarget = new GameObject("MainPlayerCameraShakeTarget");
            m_transMainPlayerCameraShakeTarget = goMainPlayerCameraShakeTarget.transform;
            m_transMainPlayerCameraShakeTarget.SetParent(m_transMainPlayerCameraSlot);
            m_transMainPlayerCameraShakeTarget.ResetLocal();
        }

        m_transMainPlayerCamera = m_transMainPlayerCameraShakeTarget.FindChild("MainPlayerCamera");
        if (m_transMainPlayerCamera == null)
        {
            GameObject goMainPlayerCamera = new GameObject("MainPlayerCamera");
            m_transMainPlayerCamera = goMainPlayerCamera.transform;
            m_transMainPlayerCamera.SetParent(m_transMainPlayerCameraShakeTarget);
            m_transMainPlayerCamera.ResetLocal();
        }

        m_cameraMainPlayer = GameObjectHelper.GetComponent<Camera>(m_transMainPlayerCamera.gameObject);
        //m_transMainPlayerCamera.gameObject.AddMissingComponent<CameraEffect>();

        m_cameraMainPlayer.orthographic = false;
        m_cameraMainPlayer.fieldOfView = GameSetting.FOV_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.nearClipPlane = GameSetting.MAINPLAYER_CAMERA_NEARPLANE;
        m_cameraMainPlayer.farClipPlane = GameSetting.MAINPLAYER_CAMERA_FARPLANE;
        m_cameraMainPlayer.cullingMask = GameSetting.LAYER_MASK_MAIN_PLAYER;
        m_cameraMainPlayer.depth = GameSetting.CAMERA_DEEPTH_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.clearFlags = CameraClearFlags.Depth;
        m_cameraMainPlayer.useOcclusionCulling = false;

        m_kMainPlayerCameraAnimation = GameObjectHelper.GetComponent<Animation>(m_transMainPlayerCamera.gameObject);
        m_kMainPlayerCameraAnimation.playAutomatically = false;

        FlipMainCamera((int)GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_HOLD_HAND));

        if (SceneCamera.singleton != null && SceneCamera.singleton.cameraShake != null)
        {
            SceneCamera.singleton.cameraShake.AddCamera(m_cameraMainPlayer, true, m_cameraMainPlayer.transform.parent);
        }

        if (StoryManager.singleton != null && StoryManager.singleton.isPlaying)
        {
            m_cameraMainPlayer.enabled = false;
        }
    }

    public override void UpdateGameObjectLayer()
    {
        if (gameObject != null)
        {
            if (m_iModelType == GameConst.MODEL_TYPE_ROLE)
                gameObject.ApplyLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER, null, null, true);   // 主角相机要用
            else
                gameObject.ApplyLayer(GameSetting.LAYER_VALUE_DMM_PLAYER, null, null, true);   // 躲猫猫用
        }
    }

    bool m_bStopSendPos = false;
    public void SendPos()
    {
        if (m_bStopSendPos)
            return;

        if (Time.realtimeSinceStartup - m_sendPosTime >= GlobalConfig.gameSetting.SendPosInterval)
        {
            if (m_kGo == null || alive == false)
                return;

            bool needSend = false;

            if (IsPosInfoChanged() == false)
            {
                if (m_motionlessTime == -1)
                    m_motionlessTime = Time.realtimeSinceStartup;

                if (Time.realtimeSinceStartup - m_motionlessTime >= GameSetting.PLAYER_NOTMOVE_SEND_POS_INTERVAL)
                {
                    m_motionlessTime = Time.realtimeSinceStartup;
                    needSend = true;
                }
            }
            else
            {
                needSend = true;
                m_motionlessTime = -1;
            }

            if (needSend)
            {
                Vector3 pos = position;
                Vector3 ang = transform.eulerAngles;

                tos_fight_move proto = new tos_fight_move()
                {
                    pos = new short[] { (short)(100 * pos.x), (short)(100 * pos.y), (short)(100 * pos.z), (short)(50*ang.y), (short)(50*GetPitchAngle())},
                    angleY = (byte)(ang.y / 2),
                    anglePitch = (byte)GetPitchAngle(),
                    moveState = status.jump ? GameConst.MOVE_STATE_JUMP : GameConst.MOVE_STATE_WALK,
                    clientSpeed = (byte)(m_kMPC.moveSpeed * 10),
                    time = m_sendTime++,
                    timeStamp = Time.timeSinceLevelLoad
                };

                //Logger.Log("SendPos AngleY = " + proto.angleY + "/" + ang.y);

                WorldManager.SendUdp(proto);
                UDPping.Instance.SendUDP(proto.sid);

                m_vec3PosLastSend = pos;
                m_vec3AngleLastSend = ang;
                m_sendPosTime = Time.realtimeSinceStartup;
            }
        }
    }

    private float GetPitchAngle()
    {
        if (m_transSceneCameraSlot == null)
            return 0;

        float pitchAngle = m_transSceneCameraSlot.localEulerAngles.x - GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT.x;
        pitchAngle /= 2f;

        ////float pitchAngle = m_transSceneCameraSlot.localEulerAngles.x - GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT.x;

        //if (pitchAngle >= 180)
        //    pitchAngle = pitchAngle - 360;

        //pitchAngle = Mathf.Clamp(pitchAngle, -GlobalConfig.gameSetting.OPPitchUpMaxAngle, GlobalConfig.gameSetting.OPPitchDownMaxAngle);

        //pitchAngle += 90;   // 将负数转换为正数[-90,90] => [0, 180]

        return pitchAngle;
    }

    public void SendSwitchWeaponMsg(string weapon, int atkType = GameConst.WEAPON_ATKTYPE_NORMAL)
    {
        NetLayer.Send(new tos_fight_switch_weapon() { weapon = weapon, atk_type = atkType });
        serverData.weaponVer++;
        //Logger.Error("sendswitchmsg, weapon: "+weapon+", adv: "+advbullet);
    }

    public bool IsPosInfoChanged()
    {
        if (m_kTrans != null)
        {
            if (m_vec3PosLastSend != m_kTrans.position || m_vec3AngleLastSend != m_kTrans.eulerAngles)
                return true;
        }

        return false;
    }

    override public void Jump()
    {
        if (!alive || status.freeze || status.jump)
            return;

        base.Jump();
        PlayJumpBeginSound();

        if (curWeapon != null && curWeapon.weaponSpread != null)
            curWeapon.weaponSpread.InitJumpSpread();
    }

    public override void Fly()
    {
        if (!alive || status.freeze || status.fly)
            return;
        base.Fly();
    }




    //二断跳
    override public void Jump2()
    {
        if (!alive || status.freeze || status.jump2)
            return;
        base.Jump2();

        status.jump2 = true;
        SendJumpMsg(GameConst.JUMP_STAGE_START);
    }

    public void JumpTopPoint()
    {
        if (!alive || status.freeze || !status.jump)
            return;

        SendJumpMsg(GameConst.JUMP_STAGE_TOP);
    }

    override public void JumpComplete()
    {
        if (!alive || status.freeze || !status.jump)
            return;

        base.JumpComplete();
        if (ghostJumpRecord.isJumping)
        {
            //ghostJumpRecord.isJumping = false;
            //ghostJumpRecord.ResetTimeRecord();
            ghostJumpRecord.GhostJump("jump_end");
            //NetLayer.Send(new tos_fight_ghost_jump() { status = (int)GhostJumpStatus.Invisiable });            
        }
        SendJumpMsg(GameConst.JUMP_STAGE_GROUND);
    }

    private void SendJumpMsg(int jumpStage)
    {
        if (jumpStage != GameConst.JUMP_STAGE_GROUND)
            return;

        Vector3 curPos = position;
        Vector3 angle = m_kTrans.eulerAngles;

        Vector3 pos = position;
        Vector3 ang = transform.eulerAngles;

        int moveState = status.jump ? GameConst.MOVE_STATE_JUMP : GameConst.MOVE_STATE_WALK;
        tos_fight_move proto = new tos_fight_move()
        {
            pos = new short[] { (short)(100 * pos.x), (short)(100 * pos.y), (short)(100 * pos.z), (short)(50 * ang.y), (short)(50 * GetPitchAngle()) },
            angleY = (byte)(ang.y / 2),
            anglePitch = (byte)GetPitchAngle(),
            moveState = status.jump ? GameConst.MOVE_STATE_JUMP : GameConst.MOVE_STATE_WALK,
            clientSpeed = (byte)(m_kMPC.moveSpeed * 10),
            time = m_sendTime++,
            timeStamp = Time.timeSinceLevelLoad
        };

        WorldManager.SendUdp(proto);

        m_vec3PosLastSend = pos;
        m_vec3AngleLastSend = ang;
        m_sendPosTime = Time.realtimeSinceStartup;
    }



    public void Move(Vector3 vec3Move, bool checkCheat = true, bool jumpUp = false)
    {
        if (GameSetting.openMove == false || m_kCharacterCtrler == null)
            return;

        //if (GameSetting.openMoveStack)
        //    Logger.Log("Move : " + vec3Move / Time.smoothDeltaTime + " " + StackTraceUtility.ExtractStackTrace());

        if (!alive || status.freeze)   // 不要排除跳跃的状态，因为要实现跳跃的时候按行走，当跳跃结束后，人会继续行走
            return;

        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
        Vector3 prePos = transform.position;
        m_kCharacterCtrler.Move(vec3Move);
        Vector3 nowPos = transform.position;

        if(jumpUp)
        {
            float dis1 = vec3Move.magnitude;
            float dis2 = (nowPos - prePos).magnitude;
            if (dis2 - dis1 > 0.02f)
            {
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_MPCCMoveDis, dis1 + "/" + dis2);
            }
        }

        if (checkCheat)
        {
            float delta = Vector3.Magnitude(nowPos - ctrlPos) - vec3Move.magnitude;
            if(delta > 1)
            {
                string msg = delta +";" + vec3Move + ";" + position + ";" + ctrlPos + ";" + moveSpeed;
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_MPCCMove, msg);
                return;
            }
        }

        ctrlPos = position;
        m_kServerData.last_act_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
    }




    override public void Crouch()
    {
        if (!alive || status.jump || (IsGrounded() == false && !status.climb))
            return;

        status.stand = false;
        status.crouch = true;

        // 突然设置Collider高度，会导致人物突然穿过障碍物
        //characterControler.height = GameSetting.PLAYER_COLLIDER_HEIGHT_CROUCH;
        //Vector3 vec3Center = characterControler.center;
        //vec3Center.y = characterControler.height * 0.5f;
        //characterControler.center = vec3Center;

        if (m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            float fR = GetPlayerHeightCrouch() / GetPlayerHeightStand();
            if (fR >= 4)
            {
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_CrouchStandHeightRadio, fR.ToString());
            }
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY * fR * scale;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        } 
         
        //SendMsg(PlayerServerData.MSG_CROUCH);
        NetLayer.Send(new tos_fight_crouch());
    }


    public override void PowerOn()
    {
        base.PowerOn();
        m_kPlayerAniController.Play(curWeapon.weaponConfigLine.MPPowerOnAni);


    }

    override public void Stand(bool bSend2Server = false, bool bNeedGrounded = true)
    {
        if (!alive || status.jump || bNeedGrounded && (IsGrounded() == false && !status.climb))
            return;

        base.Stand(bSend2Server, bNeedGrounded);

        if (gameObject == null)
            return;

        // 突然设置Collider高度，会导致人物突然穿过障碍物
        //characterControler.height = GameSetting.PLAYER_COLLIDER_HEIGHT_STAND;
        //Vector3 vec3Center = characterControler.center;
        //vec3Center.y = characterControler.height * 0.5f;
        //characterControler.center = vec3Center;
        if (m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY * scale;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        }

        if (bSend2Server)
        {
            //SendMsg(PlayerServerData.MSG_STAND);
            NetLayer.Send(new tos_fight_stand());
        }
    }

    public static void SendMsg(string key, object value = null)
    {
        CDict proto = new CDict();
        proto["typ"] = "fight";
        proto["key"] = key;
        if (value != null)
            proto["val"] = value;
        NetLayer.SendPack(proto);
    }

    public void TouchLookRotate(float pitch, float yaw)
    {
        if (pitch == 0 && yaw == 0)
            return;

        var pitchAngle = m_transSceneCameraSlot.localEulerAngles.x;
        pitchAngle += pitch;
        if (pitchAngle > 180)
            pitchAngle -= 360;
        else if (pitchAngle < -180)
            pitchAngle += 360;
        pitchAngle = Mathf.Clamp(pitchAngle, m_pitchRange[0], m_pitchRange[1]);
        m_transSceneCameraSlot.Rotate(pitchAngle - m_transSceneCameraSlot.localEulerAngles.x, 0, 0, Space.Self);

        yaw = QuickTurnManager.Ins.QuickTurnCheck(yaw);
        m_kTrans.localEulerAngles += new Vector3(0, yaw);

        if (m_kABM != null)
            m_kABM.OnPlayerRotate();
    }

    public void DmmTouchLookRotate(float pitch, float yaw)
    {
        SceneCameraController.singleton.OnRotate(yaw, -pitch);
        if (m_kABM != null)
            m_kABM.OnPlayerRotate();
    }

    public void KeyboardLookRotate(float xDelta, float yDelta)
    {
        if (xDelta == 0 && yDelta == 0)
            return;

        var pitchAngle = m_transSceneCameraSlot.localEulerAngles.x;
        pitchAngle -= yDelta;
        if (pitchAngle  > 180)
            pitchAngle -= 360;
        else if (pitchAngle < -180)
            pitchAngle += 360;
        pitchAngle = Mathf.Clamp(pitchAngle, m_pitchRange[0], m_pitchRange[1]);
        m_transSceneCameraSlot.Rotate(pitchAngle - m_transSceneCameraSlot.localEulerAngles.x, 0, 0, Space.Self);

        xDelta = QuickTurnManager.Ins.QuickTurnCheck(xDelta);
        m_kTrans.Rotate(Vector3.up, xDelta);
        //SendPos();
        if (m_kABM != null)
            m_kABM.OnPlayerRotate();
    }

    public void DmmLookRotate(float xDelta, float yDelta)
    {
        SceneCameraController.singleton.OnRotate(xDelta, yDelta);
        if (m_kABM != null)
            m_kABM.OnPlayerRotate();
    }

    internal override void Act(PlayerActParam param)
    {
        base.Act(param);

        if(param.type == GameConst.ACT_TYPE_MOVE)
        {
            UDPping.Instance.Receive(param.protoMove.sid);
        }
    }

    public override void SetPosNow(float[] pos)
    {
        serverData.SetServerPos(pos, false);
        position = PlayerServerData.GetPos(pos) + new Vector3(0, -GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET, 0) + GameConst.SetPosOffset;
        eulerAngle = PlayerServerData.GetRot(pos);
    }

    public override Vector3 position
    {
        get
        {
            return base.position;
        }
        set
        {
            base.position = value;
            ctrlPos = position;
        }
    }

    private void OnServerPosChanged()
    {
        ctrlPos = serverPos;
    }

    public override void AttachSceneCamera()
    {
        SceneCamera.singleton.SetParent(m_transSceneCameraSlot, true);
        //SceneCamera.singleton.ResetLocalTransform();

        ResetCameraFov();

        if (isDmmModel)
        {
            //如果是躲猫猫则继续开启追随模式
            SceneCameraController.singleton.FollowPlayer(this, FollowHeight, FollowDistance, IsFrezee, true);
            //如果之前是固定状态，则设置相机的挂载位置
            //if (IsFrezee)
            //{
            //    SceneCameraController.singleton.SetDir(m_vecSceneCamSlotDir);
            //}
        }
        else
        {
            m_transSceneCameraSlot.localPosition = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
            m_transSceneCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT;
        }

        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_ATTACH_SCENE_CAMERA);
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, true);
    }

    public void PlayCameraWalkAni()
    {
        m_bCameraIdleAniPlaying = false;
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            m_bCameraMoveAniPlaying = true;
            if (m_kWeaponMgr.curWeapon != null)
            {
                string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
                AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
                if (state != null)
                {
                    state.speed = 1;
                    m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                    m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
                }
            }
        }
    }

    public void PlayCameraIdleAni()
    {
        if (m_kWeaponMgr.curWeapon == null)
            return;

        m_bCameraMoveAniPlaying = false;
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            m_bCameraIdleAniPlaying = true;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
            {
                state.speed = 1;
                m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
            }
        }
    }

    public void StopCameraIdleAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_bCameraIdleAniPlaying = false;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    public void StopCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        { 
            m_bCameraMoveAniPlaying = false;
            string strCameraAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    override internal void Behit(p_hit_info_toc hitInfo, BasePlayer kAttacker = null, Vector3 hitPos = default(Vector3), int skillId = 0, System.Object arg = null)
    {
        if (serverData.alive == false)
            return;

        int hitValue = m_kServerData.life - hitInfo.life;

        base.Behit(hitInfo, kAttacker, hitPos);
        
        //技能击退
        if (skillId != 0 && m_skillMgr != null)
        {
            m_skillMgr.HitBackDiatance(kAttacker, skillId);
        }

        if (hitValue > 0)
        {
            if (kAttacker != null && transform != null)
            {
                if ((WorldManager.singleton.isZombieTypeModeOpen) && !m_bIsCancelHitBack)
                {
                    //Vector3 moveDir = position - kAttacker.position;
                    //m_kMPC.HitBackDistance(kAttacker.serverData.Impulse(), moveDir, GameConst.PLAYER_HIT_BACK_SPEED);
                    m_kMPC.HitBackSpeed(kAttacker.PlayerId, kAttacker.serverData.HitBackSpeed(), kAttacker.serverData.HitBackTime());
                }

                if (kAttacker.serverData.Impulse() > 0)
                {
                    //全模式有开启推力
                    Vector3 moveDir = position - kAttacker.position;
                    m_kMPC.HitBackDistance(kAttacker.serverData.Impulse(), moveDir, kAttacker.serverData.ImpulseSpeed());
                }

                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BEHIT, kAttacker.position);
            }

            //播2d版的受击音效
            if (curWeapon != null)
            {
                if (kAttacker != null && (kAttacker.curWeapon is WeaponKnife || kAttacker.curWeapon is WeaponPaw))
                    curWeapon.PlayerHitSound(position, hitInfo.bodyPart, Effect.MAT_EFFECT_KNIFE, 0.5f);
                else
                    curWeapon.PlayerHitSound(position, hitInfo.bodyPart, Effect.MAT_EFFECT_GUN, 0.5f);
            }
        }
        else if (hitValue < 0 && kAttacker.Camp == Camp)
        {
            if (m_addLifeEffect != null)
                m_addLifeEffect.Play();
            else
                m_addLifeEffect = UIEffect.ShowEffect(UIManager.GetPanel<PanelBattle>().transform, EffectConst.UI_PLAYER_ADD_LIFE);
        }
    }

    protected override void OnDecelerate(BasePlayer kAttacker, float value, float time)
    {
        base.OnDecelerate(kAttacker, value, time);
        m_deceMgr.Decelerate(value, time);
    }

    override internal void AtkWrong(toc_fight_atk_wrong proto)
    {
        base.AtkWrong(proto);
        if (serverData.alive == false)
            return;
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_BEHIT, position);
    }

    override public void Die(BasePlayer killer, int weapon, int hitPart = BODY_PART_ID_BODY, Vector3 bulletDir = default(Vector3), Vector3 diePos = default(Vector3), Vector3 killerPos = default(Vector3))
    {
        base.Die(killer, weapon, hitPart, bulletDir, diePos, killerPos);

        this.diePos = diePos;
        this.dieFace = m_kTrans == null ? Vector3.forward : m_kTrans.forward;

        ConfigItemWeaponLine wc = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weapon);
        OtherPlayer opkiller = killer as OtherPlayer;
        if (wc != null && wc.Class != WeaponManager.WEAPON_TYPE_GRENADE && wc.Class != WeaponManager.WEAPON_TYPE_RIFLEGRENADE && wc.Class != WeaponManager.WEAPON_TYPE_PAWGUN)
        {
            if (opkiller != null && opkiller.configData != null && opkiller.configData.SubType != GameConst.ROLE_SUBTYPE_BOSS && opkiller.configData.SubType != GameConst.ROLE_SUBTYPE_OBJECT)
                opkiller.LockKillPos(killerPos, diePos, 0);
        }

        //Logger.Log("MP Die: " + diePos + " " + opkiller + " " + killerPos);

        if (ScreenEffectManager.singleton != null)
            ScreenEffectManager.singleton.HideAllEffect();

        Vector3 effectPos = centerPos;

        SetFreeze(false);
        FallOnTheGround();

        if (m_kMPC != null)
            m_kMPC.RemoveHitBack(); //移除击退效果

        //if (curWeapon != null && !curWeapon.activeSkillIsRevive)
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_WEAPON_SKILL, false);
        GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, false);

        //useAdvAmmo = false;
        //localPlayerData.ClearData();    // TODO: 死亡后，清除记录的武器弹药数据

        if (gameObject != null)
        {
            m_cc.enabled = false;

            if (m_kMainPlayerCameraAnimation != null)
                m_kMainPlayerCameraAnimation.Stop();

            if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }

            if (SceneCamera.singleton != null)
            {
                SceneCamera.singleton.SetParent(null);
            }

            SceneCameraController.singleton.StopControl();

            characterControler.enabled = false;

            if (curWeapon != null)
            {
                curWeapon.StopFire();
                curWeapon.Reset();
            }

            
            int dieAniType = wc != null ? wc.DieAniType : 1;
            Vector3 fromPos = killer != null ? killer.position : Vector3.zero;

            bool _isSimpleDeadAni = GetOpModelIsSimpleDeadAni();
            PlayerDeadCameraSwitchData kData = new PlayerDeadCameraSwitchData()
            {
                pos = diePos,
                face = transform.forward,
                killerPos = fromPos,
                killer = killer,
                model = m_configData.OPModel,
                isShowModel = string.IsNullOrEmpty(m_configData.DieEffect),
                isSimpleDeadAni = _isSimpleDeadAni,
                isShake = (wc.AmmoID != 0 && ConfigManager.GetConfig<ConfigItemBullet>().GetLine(wc.AmmoID).ShakeID != 0),
                deadAni = _isSimpleDeadAni ?
                    AniConst.GetSimpleDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, status.stand) :
                    AniConst.GetPlayerDeadAni(m_kTrans.forward, m_kTrans.position, fromPos, dieAniType, status.stand, hitPart == BODY_PART_ID_HEAD)
            };
            WorldManager.singleton.CameraSwitcher.PlayMainPlayerDeadSwitch(kData);
            //if (m_iModelType == GameConst.MODEL_TYPE_DMM)
            if (!string.IsNullOrEmpty(m_configData.DieEffect))
            {
                //EffectManager.singleton.GetEffect<EffectTimeout>(EffectConst.FIGHT_HIDE_ITEM_DIE, (effect) =>
                EffectManager.singleton.GetEffect<EffectTimeout>(m_configData.DieEffect, (effect) =>
                {
                    SetActive(false);
                    effect.Attach(effectPos);
                    effect.SetTimeOut(-1);
                    effect.Play();
                });
            }
            else
            {
                SetActive(false);
            }
        }
        ClearState();
    }

    public bool IsShowOPModel()
    {
        return string.IsNullOrEmpty(m_configData.DieEffect);
    }

    protected override void Lock()
    {
        // 移除场景相机
        if (m_transSceneCameraSlot != null && m_transSceneCameraSlot.childCount == 1)
        {
            m_transSceneCameraSlot.GetChild(0).SetParent(null);
            Logger.Warning("MainPlayer Lock, Remove SceneCamera");
        }

        base.Lock();
    }

    protected override void Revive(bool dataRevive)
    {
        //if (dataRevive)
        //    VertificateUtil.Reset();

        DeadPlayer.Instance.HideRescue();
        skillMgr.flm.revive();
        if (curWeapon != null)
        {
            curWeapon.StopFire();
        }

        base.Revive(dataRevive);

        if (SceneCameraController.singleton != null)
            SceneCameraController.singleton.StopControl();

        if (gameObject != null)
        {

            position = position + new Vector3(0, GameConst.PLAYER_POSITION_FIXED_HIEGHT, 0);

            characterControler.enabled = true;
            m_cc.enabled = true;

            m_bCameraIdleAniPlaying = false;
            m_bCameraMoveAniPlaying = false;

            m_kMPC.Reset();
            WorldManager.singleton.CameraSwitcher.StopSwitching();

            if (curWeapon != null)
                curWeapon.Reset();

            Stand(true, false);
            AttachSceneCamera();

            SwitchDefaultWeapon(false);

            if (StoryManager.singleton != null && !StoryManager.singleton.isPlaying)
            {
                SceneCamera.singleton.enableCamera = !ForbidSceneCam;
                EnableMainPlayerCamera(!ForbidMainCam);
            }

            if (Driver.singleton != null && Driver.singleton.controlMode == Driver.CONTROL_MODE_INPUT)
                MainPlayerControlInputReceiver.singleton.enabled = true;

            GameDispatcher.Dispatch(GameEvent.UI_SHOW_FIRE_AIM_POINT, true);
        }
    }

    override public void WeaponAttachComplete()
    {
        base.WeaponAttachComplete();

        if (released)
            return;

        if (!m_bChangeModel && !curWeapon.isFirePressed)
        {
            m_kPlayerAniController.Play(curWeapon.weaponConfigLine.MPSwitchAni);
        }
        else
            m_bReplaySwitchWeapon = true;

        ConfigItemWeaponLine kWL = curWeapon.weaponConfigLine;

        m_kMPCS.apply = false;
        m_kMPCS._RecoilValue = kWL.RecoilDis;
        m_kMPCS._RecoilRecoverSpeed = kWL.RecoilRecoverSpeed;

        m_bCameraIdleAniPlaying = false;
        m_bCameraMoveAniPlaying = false;

        // 主角相机也能看到主角的枪
        if (curWeapon != null)
            curWeapon.ApplyMainPlayerLayer();

        if (reuseWeaponSkill)
        {
            m_reuseWeaponSkill = false;
            curWeapon.SkillFire(SwitchPreWeapon);
        }
    }

    public void UpdateMoveSpeed()
    {
        // ！！！ 速度的计算，放在用的时候再计算，防作弊，修改内存

        // 切换武器，属性变化时，重新计算移动速度

        //if (m_kWeaponMgr.curWeapon == null)
            //return;
        //if (m_kWeaponMgr.curWeapon == null)
        //    return;

        //ConfigItemRoleLine rl = m_configData;
        //ConfigItemRoleLine rl = m_configData;

        //float factor1 = 1 - serverData.weight() * 0.01f;
        //float factor1 = 1 - serverData.weight() * 0.01f;

        //WeaponGun gun = m_kWeaponMgr.curWeapon as WeaponGun;
        //if (gun != null && gun.isZoomOrOut == true)
            //factor1 *= gun.weaponConfigLine.ZoomMoveFactor;
        //WeaponGun gun = m_kWeaponMgr.curWeapon as WeaponGun;
        //if (gun != null && gun.isZoomOrOut == true)
        //    factor1 *= gun.weaponConfigLine.ZoomMoveFactor;

        //m_moveSpeedData.standMoveForwardSpeed = m_kServerData.standMoveSpeed() * factor1;
        //m_moveSpeedData.standMoveBackSpeed = m_moveSpeedData.standMoveForwardSpeed * rl.BackMoveFactor;
        //m_moveSpeedData.standMoveSideSpeed = m_moveSpeedData.standMoveForwardSpeed * rl.SideMoveFactor;
        //m_moveSpeedData.standMoveForwardSpeed = m_kServerData.standMoveSpeed() * factor1;
        //m_moveSpeedData.standMoveBackSpeed = m_moveSpeedData.standMoveForwardSpeed * rl.BackMoveFactor;
        //m_moveSpeedData.standMoveSideSpeed = m_moveSpeedData.standMoveForwardSpeed * rl.SideMoveFactor;

        //m_moveSpeedData.crouchMoveForwardSpeed = m_kServerData.crouchMoveSpeed() * factor1 ;
        //m_moveSpeedData.crouchMoveBackSpeed = m_moveSpeedData.crouchMoveForwardSpeed * rl.BackMoveFactor;
        //m_moveSpeedData.crouchMoveSideSpeed = m_moveSpeedData.crouchMoveForwardSpeed * rl.SideMoveFactor;

        //Logger.Warning("UpdateMoveSpeed serverData.weight:" + serverData.weight() + "  factor1:" + factor1 + " m_kServerData.standMoveSpeed():" + m_kServerData.standMoveSpeed() + " m_standMoveForwardSpeed:" + m_standMoveForwardSpeed);
    }

    private void AddMainPlayerCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation == null)
            return;

        string strCameraWalkAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraWalkAni;
        if (string.IsNullOrEmpty(strCameraWalkAni) == false)
        {
            string strPath = PathHelper.GetCameraAni(strCameraWalkAni);
            if (m_kMainPlayerCameraAnimation.GetClip(strCameraWalkAni) == null)
            {
                ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                {
                    if (kAniClip != null && m_kMainPlayerCameraAnimation != null)
                    {
                        kAniClip.wrapMode = WrapMode.Loop;
                        m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraWalkAni);
                    }
                });
            }
        }

        string strCameraIdleAni = m_kWeaponMgr.curWeapon.weaponConfigLine.CameraIdleAni;
        if (string.IsNullOrEmpty(strCameraIdleAni) == false)
        {
            string strPath = PathHelper.GetCameraAni(strCameraIdleAni);
            if (m_kMainPlayerCameraAnimation.GetClip(strCameraIdleAni) == null)
            {
                ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                {
                    if (m_kMainPlayerCameraAnimation != null && kAniClip != null)
                    {
                        kAniClip.wrapMode = WrapMode.Loop;
                        m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraIdleAni);
                    }
                });
            }
        }

    }

    private void DoNoAmmo()
    {
        Logger.Log("没有弹药");
    }

    public bool cameraMoveAniPlaing
    {
        get { return m_bCameraMoveAniPlaying; }
    }

    public bool cameraIdleAniPlaying
    {
        get { return m_bCameraIdleAniPlaying; }
    }

    public override WeaponBase curWeapon
    {
        get { return m_kWeaponMgr != null ? m_kWeaponMgr.curWeapon : null; }
    }

    public override WeaponBase subWeapon
    {
        get { return m_kWeaponMgr != null ? m_kWeaponMgr.subWeapon : null; }
    }

    override public MainPlayerCameraShaker cameraShaker
    {
        get { return m_kMPCS; }
    }

    public MainPlayerController mainplayerController
    {
        get { return m_kMPC; }
    }

    override public void Update()
    {
        base.Update();

        WorldManager.singleton.CameraSwitcher.Update();
        m_deceMgr.Update();
        m_kKOMgr.Update();
        m_kABM.Update();
        UDPping.Instance.Update();
    }

    public override void SetActive(bool value)
    {
        base.SetActive(value);

        if (m_cc != null)
            m_cc.enabled = value;

        if (m_kCharacterCtrler != null)
            m_kCharacterCtrler.enabled = value;

        if (value)
            Physics.IgnoreCollision(m_cc, m_kCharacterCtrler, true);

        if (m_cameraMainPlayer != null)
        {
            if(value && !(StoryManager.singleton != null && StoryManager.singleton.isPlaying))
                m_cameraMainPlayer.enabled = true;
            else
                m_cameraMainPlayer.enabled = false;
        }
    }

    public bool IsGrounded()
    {
        //if(GameSetting.enableGravityRay == false)
        //{
        //    return true;
        //}

        RaycastHit kRH;
        //float radius = this is MainPlayer ? GameSetting.MPColliderRadius : GameSetting.OPColliderRadius;
        float radius = m_kCharacterCtrler.radius;
        Vector3 oriPos = position + Vector3.up * radius;

        if (Physics.SphereCast(oriPos, radius, Vector3.down, out kRH, GameSetting.GRAVITY_RAY_DISTANCE, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {

            if (kRH.distance <= GameSetting.PLAYER_GROUNDED_OFFSET)
                return true;
        }
        return false;
    }

    //GameObject _goSphere;
    public bool IsGrounded(out float rayDis)
    {
        //if (GameSetting.enableGravityRay == false)
        //{
        //    rayDis = 0;
        //    return true;
        //}

        RaycastHit kRH;
        //float radius = this is MainPlayer ? GameSetting.MPColliderRadius : GameSetting.OPColliderRadius;
        float radius = m_kCharacterCtrler.radius;
        Vector3 oriPos = position + Vector3.up * radius;

        rayDis = float.MaxValue;
        if (Physics.SphereCast(oriPos, radius, Vector3.down, out kRH, GameSetting.GRAVITY_RAY_DISTANCE, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_OTHER_PLAYER | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {
            rayDis = kRH.distance;

            //Logger.Log(kRH.collider.name + " " + kRH.distance);
            //if (_goSphere == null)
            //{
            //    _goSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //    _goSphere.name = "Fuck";
            //    _goSphere.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            //}
            //_goSphere.transform.position = kRH.point;

            if (kRH.distance <= GameSetting.PLAYER_GROUNDED_OFFSET)
                return true;
        }
        return false;
    }

    //public bool IsGrounded(out float rayDis)
    //{
    //    rayDis = 0;
    //    if(m_kCharacterCtrler != null && m_kCharacterCtrler.enabled)
    //    {
    //        return m_kCharacterCtrler.isGrounded;
    //    }
    //    else
    //    {
    //        RaycastHit kRH;
    //        if (Physics.Raycast(position, Vector3.down, out kRH, GameSetting.GRAVITY_RAY_DISTANCE, GameSetting.LAYER_MASK_BUILDING))
    //        {
    //            rayDis = kRH.distance;
    //            if (kRH.distance <= GameSetting.PLAYER_GROUNDED_OFFSET)
    //                return true;
    //            else
    //                return false;
    //        }
    //        else
    //        {
    //            rayDis = float.MaxValue;
    //            return false;
    //        }
    //    }
    //}

    //public bool IsGrounded()
    //{
    //    if (m_kCharacterCtrler != null && m_kCharacterCtrler.enabled)
    //    {
    //        return m_kCharacterCtrler.isGrounded;
    //    }
    //    else
    //    {
    //        RaycastHit kRH;
    //        if (Physics.Raycast(position, Vector3.down, out kRH, GameSetting.GRAVITY_RAY_DISTANCE, GameSetting.LAYER_MASK_BUILDING))
    //        {

    //            if (kRH.distance <= GameSetting.PLAYER_GROUNDED_OFFSET)
    //                return true;
    //            else
    //                return false;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //}

    #region 技能相关
    public void UseSkill(int skillId)
    {
        PlayerBattleModel.Instance.TosUseSkill(skillId);
    }

    override internal void UseSkill(toc_fight_use_skill proto)
    {
        if (m_skillMgr != null)
            m_skillMgr.MPUseSkill(proto.skill_id, proto.pos, proto.target_ids, proto.skill_param, proto.tauntedPlayerId);
    }

    override internal void UseWeaponAwakenSkill(toc_fight_weapon_awaken_skill proto)
    {
        serverData.life = proto.life;
        if (m_skillMgr != null)
            m_skillMgr.MPUseWeaponAwakenSkill(proto.awaken_id, proto.skill_id, proto.pos, proto.target_ids, "");
        GameDispatcher.Dispatch<bool>(GameEvent.UI_REFRESH_WEAPON_SKILL, true);
    }

    internal override void Rush(float distance, float time)
    {
        ForbidMove = true;
        ForbidLookAround = true;
        ForbidJump = true;
        m_kMPC.Rush(distance, time);
    }

    public void RushComplete()
    {
        ForbidMove = false;
        ForbidLookAround = false;
        ForbidJump = false;
        NetLayer.Send(new tos_fight_rush_done());
    }

    public void FlyModeBegin()
    {

    }

    public void FlyModeEnd()
    {

    }

    #endregion

    public string GetMPPrefabPath()
    {
        return GetRolePrefablPath(m_iModelType, m_strModel);
    }

    public string GetMPOPPrefabPath()
    {
        if (configData != null)
        {
            return PathHelper.GetRolePrefabPath(configData.OPModel);
        }
        return "";
    }

    public string GetMPDefaultPrefabPath()
    {
        return m_iModelSex == ModelConst.MODEL_PLAYER_BOY_SEX ? PathHelper.GetRolePrefabPath(GameConst.PLAYER_MP_BOY_DEFAULT_MODEL) : PathHelper.GetRolePrefabPath(GameConst.PLAYER_MP_GIRL_DEFAULT_MODEL);
    }

    public string GetMPDefaultMatrixZombiePath()
    {
        int matrix_zombie_id = ConfigMisc.GetInt("matrix_zombie_id");
        var roleConfig = ConfigManager.GetConfig<ConfigItemRole>().GetLine(matrix_zombie_id);
        string modelName = roleConfig == null ? GameConst.PLAYER_MP_MATRIX_ZOMBIE_DEFAULT_MODEL : roleConfig.MPModel;
        return PathHelper.GetRolePrefabPath(modelName);
    }

    public string GetMPDefaultEliteZombiePath()
    {
        int elite_zombie_id = ConfigMisc.GetInt("elite_zombie_id");
        var roleConfig = ConfigManager.GetConfig<ConfigItemRole>().GetLine(elite_zombie_id);
        string modelName = roleConfig == null ? GameConst.PLAYER_MP_ELITE_ZOMBIE_DEFAULT_MODEL : roleConfig.MPModel;
        return PathHelper.GetRolePrefabPath(modelName);
    }
    public string GetMPDefaultUltimateZombiePath()
    {
        int ultimate_zombie_id = ConfigMisc.GetInt("ultimate_zombie_id");
        var roleConfig = ConfigManager.GetConfig<ConfigItemRole>().GetLine(ultimate_zombie_id);
        string modelName = roleConfig == null ? GameConst.PLAYER_MP_ULTIMATE_ZOMBIE_DEFAULT_MODEL : roleConfig.MPModel;
        return PathHelper.GetRolePrefabPath(modelName);
    }

    private void SetJumpInfo()
    {
        ConfigMapRuleLine mrl = WorldManager.singleton.mapRuleLine;
        if (mrl != null)
        {
            for (int j = 0; j < mrl.CanJump2Camps.Length; j++)
            {
                if (Camp == mrl.CanJump2Camps[j])
                {
                    canJump2 = true;
                    return;
                }
            }
        }

        canJump2 = false;
    }


    #region Get Set

    public CharacterController characterControler
    {
        get { return m_kCharacterCtrler; }
    }

    public CapsuleCollider capsuleCollider
    {
        get { return m_cc; }
    }

    public bool useAdvAmmo
    {
        get
        {
            return m_bUseAdvAmmo;
        }
        set
        {
            //if (m_bUseAdvAmmo != value)
            //{
            //    float time = 0f;
            //    if (value == false && curWeapon != null && curWeapon.advNum == 0)   // 如果是最后一发穿甲弹，useAdvAmmo由true变为false则延时发送，保证这个udp包后到
            //        time = 0.1f;
            //    //Logger.Error("set adv ammo-----");
            //
            //    TimerManager.SetTimeOut(time, () =>
            //    {
            //        SendSwitchWeaponMsg(serverData.curWeaponSubType, WeaponAtkTypes.GetWeaponAtkType(serverData.curWeaponSubType, value));
            //        //NetLayer.Send(new tos_fight_switch_weapon() 
            //        //{ 
            //        //    //weapon = m_kWeaponMgr.curWeapon.weaponConfigLine.SubType, 
            //        //    weapon = serverData.curWeaponSubType,
            //        //    advbullet = value
            //        //});
            //    });
            //}
            //现在方案由前端决定
            m_bUseAdvAmmo = false;
            if (value)
            {
                if (curWeapon != null && curWeapon.advNum != 0 && curWeapon.hasAmmoCurClip)
                    m_bUseAdvAmmo = true;
            }
        }
    }

    public bool serverSetUseAdvAmmo
    {
        set
        {
            m_bUseAdvAmmo = value;
        }
    }

    public float standMoveForwardSpeed
    {
        get 
        {
            float factor = 1 - Weight * 0.01f;

            if (curWeapon != null && curWeapon.isZoomOrOut)
                factor *= curWeapon.weaponConfigLine.ZoomMoveFactor;
            return m_kServerData.standMoveSpeed() * factor * m_deceMgr.deceFactor;
        }
    }

    public float standMoveSideSpeed
    {
        get { return standMoveForwardSpeed * m_configData.SideMoveFactor; }
    }

    public float standMoveBackSpeed
    {
        get { return standMoveForwardSpeed * m_configData.BackMoveFactor; }
    }

    public float crouchMoveForwardSpeed
    {
        get
        {

            float factor = 1 - Weight * 0.01f;

            if (curWeapon != null && curWeapon.isZoomOrOut)
                factor *= curWeapon.weaponConfigLine.ZoomMoveFactor;

            return m_kServerData.crouchMoveSpeed() * factor * m_deceMgr.deceFactor;
        }
    }

    public float crouchMoveSideSpeed
    {
        get { return crouchMoveForwardSpeed * m_configData.SideMoveFactor; }
    }

    public float crouchMoveBackSpeed
    {
        get { return crouchMoveForwardSpeed * m_configData.BackMoveFactor; }
    }

    public EptVector3 ctrlPos
    {
        get {
            return m_moveSpeedData.ctrlPos.val;
        }
        set {
            m_moveSpeedData.ctrlPos.val = value;
        }
    }

    public float Weight
    {
        get
        {
            float weight = serverData.weight();
            if (WorldManager.singleton.isPve)
            {
                weight += serverData.PveExtraWeight();
            }
            if (WorldManager.singleton.isZombieMode)
            {
                weight += serverData.ZombieExtraWeight();
            }
            return weight;
        }
    }
    public float moveSpeed
    {
        get { return m_kMPC != null ? m_kMPC.moveSpeed : 0f; }
    }

    public Vector3 moveDir
    {
        get { return m_kMPC != null ? m_kMPC.GetMoveDir() : Vector3.zero; }
    }

    public Camera cameraMainPlayer
    {
        get { return m_cameraMainPlayer; }
    }

    private float jumpMaxHeightConfig
    {
        get
        {
            ConfigMapRuleLine ml = WorldManager.singleton.mapRuleLine;
            if (ml != null)
            {
                return ml.JumpMaxHeight;
            }
            else
            {
                return GameSetting.PLAYER_JUMP_MAX_HEIGHT;
            }
        }
    }

    private float jump2MaxHeightConfig
    {
        get
        {
            ConfigMapRuleLine ml = WorldManager.singleton.mapRuleLine;
            if (ml != null)
            {
                int[] canJump2Camps = ml.CanJump2Camps;
                for (int j = 0; j < canJump2Camps.Length; j++)
                {
                    if (Camp == canJump2Camps[j])
                    {
                        return ml.Jump2MaxHeight;
                    }
                }
            }
            return 0;
        }
    }

    public float jumpMaxHeight
    {
        get
        {
            return jumpMaxHeightConfig + (serverData != null ? serverData.JumpHeightAdd() : 0f);
        }
    }

    public float jump2MaxHeight
    {
        get
        {
            return jump2MaxHeightConfig + (serverData != null ? serverData.JumpHeightAdd() : 0f);
        }
    }

    public bool canJump2
    {
        get { return m_canJump2.val == 1; }
        set { m_canJump2.val = value ? 1 : -1; }
    }

    public Vector3 diePos
    {
        get;
        set;
    }

    public Vector3 dieFace
    {
        get;
        set;
    }

    #endregion

    #region 模型相关
    override public void SetModelName()
    {
        m_strModel = m_configData == null ? "" : RoomModel.IsGuideStage ? m_configData.MPModel + "_freshman" : m_configData.MPModel;
    }

    override protected void SetModelSex()
    {
        if (string.IsNullOrEmpty(m_strModel))
            m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        else
        {
            string lowerModelName = m_strModel.ToLower();
            if (lowerModelName.Contains(ModelConst.MODEL_MAIN_PLAYER_GIRL_CHAR))
                m_iModelSex = ModelConst.MODEL_PLAYER_GIRL_SEX;
            else
                m_iModelSex = ModelConst.MODEL_PLAYER_BOY_SEX;
        }
    }

    #endregion

    #region OnData

    internal override void OnData_ActorState(p_actor_state_toc state)
    {
        int campOld = serverData.camp;

        base.OnData_ActorState(state);

        if (campOld != serverData.camp)
        {
            foreach (KeyValuePair<int, BasePlayer> kvp in WorldManager.singleton.allPlayer)
            {
                kvp.Value.UpdateGameObjectLayer();
            }
        }


    }

    internal override void OnData_ActorProp(toc_fight_actorprop proto)
    {
        base.OnData_ActorProp(proto);
        //Logger.Log("Prop: " + m_kServerData.m_actorProps[0].FireRate.ToString());

        UpdateMoveSpeed();
        ConfigItemRoleLine rl = m_configData;
        if (rl != null)
        {
            if (m_localPlayerData.itemRoleID == 0 || m_localPlayerData.itemRoleID != rl.ID)
            {
                m_localPlayerData.itemRoleID = rl.ID;
                ResetCameraFov();
            }
        }
    }

    internal override void OnData_ActorData(p_fight_role proto)
    {
        base.OnData_ActorData(proto);
        m_sceneFov = m_configData == null ? GameSetting.FOV_SCENE_CAMERA_DEFAULT : m_configData.RealFov;
    }

    protected override void OnData_Actor()
    {
        base.OnData_Actor();

        if (m_configData != null)
        {
            SetJumpInfo();
        }
    }

    internal void OnData_Equip_Info(toc_fight_equip_info proto)
    {
        //string msg = "";
        //if(curWeapon != null)
        //    msg = "OnData_Equip_Info " + curWeapon.ammoNumInClip;

        //int oldWeaponVer = serverData.weaponVer;
        //int newWeaponVer = proto.weaponVer;

        //如果是在装弹中
        if (status.reload && curWeapon != null)
        {
            if (serverData.curWeaponSubType == proto.weapon)
            {
                int weapon = serverData.curWeapon;
                int curCount = 0;
                for (int i = 0; i < proto.list.Length; ++i)
                {
                    if (proto.list[i].itemid == weapon)
                        curCount = proto.list[i].cur_count;
                }

                //Logger.Log("OnData_Equip_Info  preCount:" + serverData.GetEquipInfo(weapon).cur_count + "   cur:" + curCount);

                if (serverData.GetEquipInfo(weapon).cur_count == curCount && (proto.weaponVer - serverData.weaponVer == -1))
                    curWeapon.StopReload();
            }
        }

        if (curWeapon != null)
        {
            int weapon1 = serverData.curWeapon;
            int curCount1 = 0;
            int extCount1 = 0;
            for (int i = 0; i < proto.list.Length; ++i)
            {
                if (proto.list[i].itemid == weapon1)
                {
                    curCount1 = proto.list[i].cur_count;
                    extCount1 = proto.list[i].ext_count;
                }
            }
            //Logger.Log("OnData_Equip_Info  weapon1:" + weapon1 + "    name:" + ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(weapon1).DispName + "  |   preCount:" + serverData.GetEquipInfo(weapon1).cur_count + "   preExtCount:" + serverData.GetEquipInfo(weapon1).ext_count + "  |  curCount:" + curCount1 + "    extCount:" + extCount1);
        }

        serverData.equipData = proto;

        m_localPlayerData.UpdateLocalWeaponData_EquipInfo();
        //if (curWeapon != null)
        //{
        //    msg += " " + curWeapon.ammoNumInClip;
        //    Logger.Log(msg);
        //}
        SwitchDefaultWeapon(proto.unswitch);

        //LocalWeaponData lwd = localPlayerData.GetData(serverData.curWeapon);
        //serverSetUseAdvAmmo = lwd != null ? lwd.use_adv : false;
        //Logger.Error("cur weapon: "+serverData.curWeapon+", isadv: "+lwd.use_adv);
        if (curWeapon != null)
        {
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, curWeapon.ammoNumInClip, curWeapon.ammoNumLeft, curWeapon.advNum);
        }
        if (subWeapon != null)
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subWeapon.subAmmoNum);
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_EQUIP_INFO_UPDATE);

        if (SceneManager.singleton.battleSceneLoaded)
        {
            GameDispatcher.Dispatch(GameEvent.UI_EQUIP_UPDATE);
        }

        if (curWeapon != null)
        {
            if (curWeapon.ammoNumInClip <= 0 && curWeapon.ammoNumLeft > 0)
            {
                GameDispatcher.Dispatch<float, bool>(GameEvent.MAINPLAYER_RELOADBULLET_CD_MASK, Time.realtimeSinceStartup, true);
            }
            else
            {
                GameDispatcher.Dispatch<float, bool>(GameEvent.MAINPLAYER_RELOADBULLET_CD_MASK, Time.realtimeSinceStartup, false);
            }

        }
        //装弹之后如果还在按着射击键，就直接继续射击
        //        if (PanelBattle.IsShootDown)
        //            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, true);
        //Debug.LogError("OnData_Equip_Info " + serverData.curWeapon.ToString());
        //Logger.Log("OnData_ActorProp = " + serverData.weaponList.ToStr());

    }

    internal void OnData_FixBullet(toc_fight_fixbullet proto)
    {
        if (proto.weaponVer != serverData.weaponVer)
            return;
        if (proto.sid <= m_fixBulletTime)
            return;
        m_fixBulletTime = NetLayer.SessionId;

        if (WeaponAtkTypeUtil.IsSubWeaponType(proto.atkType))
        {
            WeaponBase weapon = m_kWeaponMgr.subWeapon;
            if (weapon == null)
                return;
            weapon.ammoNumInClip += proto.bulletCountD;
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, weapon.subAmmoNum);

            Logger.Log("OnData_FixBullet SubWeapon atkType=" + proto.atkType + "  ammoNumInClip=" + weapon.ammoNumInClip + "  bulletCountD=" + proto.bulletCountD + "   weapon.advNum=" + weapon.advNum + "  proto.relyBulletCountD=" + proto.relyBulletCountD);
        }
        else
        {
            WeaponBase weapon = m_kWeaponMgr.curWeapon;
            if (weapon == null)
                return;
            weapon.ammoNumInClip += proto.bulletCountD;
            if (WeaponAtkTypeUtil.IsAdvWeaponType(proto.atkType))
                weapon.advNum += proto.relyBulletCountD;
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, weapon.ammoNumInClip, weapon.ammoNumLeft, weapon.advNum);

            Logger.Log("OnData_FixBullet MainWeapon atkType=" + proto.atkType + "  ammoNumInClip=" + weapon.ammoNumInClip + "  bulletCountD=" + proto.bulletCountD + "   weapon.advNum=" + weapon.advNum + "  proto.relyBulletCountD=" + proto.relyBulletCountD);
        }
    }

    #endregion

    #region Fire

    override public void StopFire()
    {
        status.fire = false;

        if (m_kMPCS != null)
            m_kMPCS.apply = false;

        if (curWeapon != null && curWeapon.curFireType == WeaponBase.WEAPON_FIRE_TYPE_STRAFE)
        {
            m_kPlayerAniController.SetBool(AniConst.TriggerStrafeFire, false);
        }
    }

    #endregion

    #region Reload Ammo

    public override void OnReload_Begin()
    {
        AudioManager.PlayFightUIVoice(AudioConst.reloading, true);
    }

    public override void OnReload_NoAmmo()
    {

    }

    public override void OnReload_AmmoFull()
    {

    }

    public void CancleReloadAmmo()
    {

    }
    override public void OnReloadAmmoComplete()
    {
        base.OnReloadAmmoComplete();

        if (released)
            return;

        if (PanelBattle.IsShootDown)
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, true);
    }
    #endregion

    #region Switch

    public void SwitchDefaultWeapon(bool sameSwitch)
    {
        // 装备更新消息可能在Revive前收到，也可能在Revive后收到，这俩个地方都会调用SwitchDefaultWeapon，作兼容
        SwitchWeapon(serverData.curWeapon.ToString(), false, sameSwitch);
    }

    public override bool SwitchWeapon(string strWeaponID, bool bSendMsg, bool sameSwitch, bool delaySendMsg = true)
    {
        if (alive == false || m_kGo == null || m_kGo.activeSelf == false)
            return false;

        //sameSwitch-相同的枪能不能显示切枪
        if (!sameSwitch && curWeapon != null && curWeapon.weaponId == strWeaponID)
            return true;

        if (base.SwitchWeapon(strWeaponID, bSendMsg, sameSwitch) == false)
            return false;

        status.switchWeapon = true;
        AddMainPlayerCameraWalkAni();
        UpdateMoveSpeed();

        if (m_iSwitchTimer != -1)
        {
            TimerManager.RemoveTimeOut(m_iSwitchTimer);
            m_iSwitchTimer = -1;
        }

        if (m_kWeaponMgr.curWeapon.weaponConfigLine.SwitchTime > 0)
            m_iSwitchTimer = TimerManager.SetTimeOut(m_kWeaponMgr.curWeapon.weaponConfigLine.SwitchTime, SwitchWeaponComplete);
        else
            SwitchWeaponComplete();

        //Logger.Error("switch, ani: "+curWeapon.weaponConfigLine.MPSwitchAni);
        if (bSendMsg)
        {
            //LocalWeaponData lw = localPlayerData.GetData(curWeapon.weaponConfigLine.ID);

            //延迟发送切枪协议
            if (delaySendMsg)
            {
                if (m_iSwitchSendProtoTimer != -1)
                {
                    TimerManager.RemoveTimeOut(m_iSwitchSendProtoTimer);
                    m_iSwitchSendProtoTimer = TimerManager.SetTimeOut(GameSetting.PLAYER_SWITCHWEAPON_SEND_PROTO_INTERVAL,
                    () => { if (!released && alive && m_kWeaponMgr.curWeapon != null) SendSwitchWeaponMsg(curWeapon.weaponConfigLine.SubType, curWeapon.atkType); });
                }
                else
                {
                    SendSwitchWeaponMsg(curWeapon.weaponConfigLine.SubType, curWeapon.atkType);
                    m_iSwitchSendProtoTimer = TimerManager.SetTimeOut(GameSetting.PLAYER_SWITCHWEAPON_SEND_PROTO_INTERVAL, () => { m_iSwitchSendProtoTimer = -1; });
                }

            }
            else
            {
                if (!released && alive) SendSwitchWeaponMsg(curWeapon.weaponConfigLine.SubType, curWeapon.atkType);
            }

            //NetLayer.Send(new tos_fight_switch_weapon() { weapon = m_kWeaponMgr.curWeapon.weaponConfigLine.SubType, advbullet = false });
            //m_kServerData.weaponVer++;
            //Logger.Log("SwitchWeapon weaponVer : " + m_kServerData.weaponVer);
        }

        WeaponBase weapon = m_kWeaponMgr.curWeapon;
        //GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, weapon.ammoNumInClip, weapon.ammoNumLeft, weapon.advNum);
        GameDispatcher.Dispatch(GameEvent.UI_UPDATE_GUNLIST, weapon.weaponId);

        return true;
    }

    public bool SwitchGrenade()
    {
        LocalWeaponData lwd = localPlayerData.GetData(serverData.grenadeID);
        if (lwd != null && lwd.hasAmmo == false)
            return false;

        return SwitchWeapon(serverData.grenadeID.ToString(), true, false, false);
    }

    public bool SwitchFlashBomb()
    {
        LocalWeaponData lwd = localPlayerData.GetData(serverData.flashBombID);
        if (lwd != null && lwd.hasAmmo == false)
            return false;

        return SwitchWeapon(serverData.flashBombID.ToString(), true, false, false);
    }

    virtual public void SwitchWeaponComplete()
    {
        if (released || alive == false || curWeapon == null)
            return;

        m_iSwitchTimer = -1;
        status.switchWeapon = false;
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, curWeapon.ammoNumInClip, curWeapon.ammoNumLeft, curWeapon.advNum);

        if (subWeapon != null)
            GameDispatcher.Dispatch(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, subWeapon.subAmmoNum);

        if (PanelBattle.IsShootDown)
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, true);
    }
    #endregion

    #region Other

    //检测场景相机,如果位置不对就修正
    // private void checkSceneCamera()
    //{
    //    if (!attachedSceneCamera)
    //        return;
    //     Transform transSceneCamera = SceneManager.singleton.transSceneCamera;
    //     if (transSceneCamera == null || transSceneCamera.parent == null || transSceneCamera.parent != m_transSceneCameraSlot)
    //     {
    //         Logger.Error("checkSceneCamera error 0, fix it!!!! scenecamera: "+transSceneCamera+", slot: "+m_transSceneCameraSlot);
    //         return;
    //     }

    //     //if (m_transSceneCameraSlot.localPosition != GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS || 
    //     //    m_transSceneCameraSlot.localEulerAngles.y != GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT.y ||
    //     //    m_transSceneCameraSlot.localEulerAngles.z != GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT.z)
    //     //{
    //     //    Logger.Error("checkSceneCamera error 1, pos: " + m_transSceneCameraSlot.localPosition + ", rot: " + m_transSceneCameraSlot.localEulerAngles);
    //     //    m_transSceneCameraSlot.localPosition = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
    //     //    m_transSceneCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALROT;
    //     //}

    //     //if (transSceneCamera.localPosition != Vector3.zero || transSceneCamera.localEulerAngles != Vector3.zero)
    //     //{
    //     //    Logger.Error("checkSceneCamera error 2, pos: "+transSceneCamera.localPosition+", rot: "+transSceneCamera.localEulerAngles);
    //     //    GameObjectHelper.ResetLocalTransform(transSceneCamera);
    //     //}
    //}

    override public float GetPlayerRadius()
    {
        if (m_configData != null && m_configData.MPRadius != 0)
        {
            return m_configData.MPRadius;
        }
        return base.GetPlayerRadius();
    }

    override public Vector3 GetPlayerCenter()
    {
        if (m_configData != null && m_configData.MPCenter != Vector3.zero)
        {
            return m_configData.MPCenter;
        }
        return base.GetPlayerCenter();
    }

    public void SetFreeze(bool isFreeze, bool isSetSlot = false)
    {
        IsFrezee = isFreeze;
        ForbidGravity = isFreeze;
        ForbidMove = isFreeze;
        ForbidLookAround = isFreeze;
        ForbidJump = isFreeze;
        ForbidFire = isFreeze;
        if (isSetSlot)
        {
            if (isFreeze)
            {
                m_vecSceneCamSlotFrezeePos = sceneCameraSlot.position;
                m_vceSceneCamSlotFrezeeRot = sceneCameraSlot.eulerAngles;
            }
            else
            {
                sceneCameraSlot.position = m_vecSceneCamSlotFrezeePos;
                sceneCameraSlot.eulerAngles = m_vceSceneCamSlotFrezeeRot;
            }
        }
        GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SETFREEZE, isFreeze);
    }

    public void SetHideView(bool isHide)
    {
        if (isHide)
        {
            SetFreeze(true, true);
            GameDispatcher.Dispatch(GameEvent.UI_SWITCH_FREE_VIEW);
        }
        else
        {
            SetFreeze(false, true);
            GameDispatcher.Dispatch(GameEvent.PlAYER_HIDE_SWITCH_BACK);
        }
    }

    override protected void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        m_iOldState = -1;
        if (data.state == GameConst.Fight_State_GameStart)
        {
            if (buffManager != null)
                buffManager.RemoveAllBuff();
            if (m_skillMgr != null)
                m_skillMgr.RemoveAll();
            if (!WorldManager.singleton.isSurvivalTypeModeOpen)
                GameDispatcher.Dispatch<bool>(GameEvent.UI_REFRESH_WEAPON_SKILL, true);
            if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
                FPWatchPlayer.singleton.EndWatch();
            if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }
        }
        else if (data.state == GameConst.Fight_State_RoundStart)
        {
            if (SceneCamera.singleton != null)
                SceneCamera.singleton.StopShake();
        }
    }

    public override void SetScale(float scale)
    {
        if (gameObject != null && m_iModelType != GameConst.MODEL_TYPE_DMM)
        {
            Vector3 vec3LocalPos = m_transSceneCameraSlot.localPosition;
            vec3LocalPos.y = m_fSceneCameraSlotLocalY * scale;
            m_transSceneCameraSlot.localPosition = vec3LocalPos;
        }
    }

    public override void SetFov(float fov)
    {
        m_sceneFov = fov;
        SetSceneFov(1f);
    }

    public float SceneFov
    {
        get { return m_sceneFov; }
    }

    public float MainFov
    {
        get { return GameSetting.FOV_MAINPLAYER_CAMERA; }
    }

    #endregion

    #region 超时踢人

    public void SetLastAction(ActionType lastActionType)
    {
        if (m_kKOMgr != null)
            m_kKOMgr.SetLastAction(lastActionType);
    }

    public float GetLastActionTime()
    {
        if (m_kKOMgr != null)
            return m_kKOMgr.lastTime;
        return 0;
    }

    public ActionType GetLastActionType()
    {
        if (m_kKOMgr != null)
            return m_kKOMgr.lastActionType;
        return ActionType.NONE;
    }

    #endregion

    #region 模型缓存
    override protected void CacheGO(GameObject go)
    {
        if (go == null)
            return;

        base.CacheGO(go);

        if (m_bReleaseGO)
            return;

        if (m_kGo == null)
            EnableMainPlayerCamera(false);

        //string name = go.name;
        //
        //List<GameObject> listGO = null;
        //if (ms_dicGOCache.TryGetValue(name, out listGO) == false)
        //{
        //    listGO = new List<GameObject>();
        //    ms_dicGOCache.Add(name, listGO);
        //}
        //
        ////Logger.Log(this + " CacheGO " + go.name + " " + go.GetInstanceID());
        //
        //listGO.Add(go);
        PlayerManager.CacheGO(go.name + "MP", go);
    }

    override protected GameObject GetCacheGO(string name)
    {
        //List<GameObject> listGO = null;
        //ms_dicGOCache.TryGetValue(name, out listGO);
        //if (listGO == null || listGO.Count <= 0)
        //    return null;
        //
        //GameObject go = listGO[listGO.Count - 1];
        //listGO.RemoveAt(listGO.Count - 1);
        //
        ////Logger.Log(this + " GetCacheGO " + go.name + " " + go.GetInstanceID());
        //
        //return go;
        return PlayerManager.GetGO(name + "MP");
    }

    static public void ReleaseCacheGO()
    {
        //foreach (KeyValuePair<string, List<GameObject>> kvp in ms_dicGOCache)
        //{
        //    for (int i = 0; i < kvp.Value.Count; i++)
        //    {
        //        ResourceManager.Destroy(kvp.Value[i]);
        //    }
        //}
        //ms_dicGOCache.Clear();
    }
    #endregion


}

class DecelerateManager
{
    private float m_deceFactor = 1;
    private float m_deceDuration;
    private float m_startTime;

    private bool m_update;

    public void Decelerate(float value, float duration)
    {
        if (m_update == false || m_update == true && (Time.realtimeSinceStartup - m_startTime <= duration))
        {
            m_deceFactor = 1 - value * 0.01f;
            m_deceFactor = m_deceFactor < 0 ? 0 : m_deceFactor;
            m_deceFactor = m_deceFactor > 1 ? 1 : m_deceFactor;
            m_deceDuration = duration;
            m_startTime = Time.realtimeSinceStartup;
            m_update = true;
        }
    }

    public float deceFactor
    {
        get { return m_deceFactor; }
    }

    public void Update()
    {
        if (m_update == false)
            return;

        if (Time.realtimeSinceStartup - m_startTime >= m_deceDuration)
        {
            m_deceFactor = 1;
            m_deceDuration = 0;
            m_update = false;
        }
    }
}