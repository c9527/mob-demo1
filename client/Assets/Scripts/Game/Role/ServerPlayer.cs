﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ServerPlayer : BasePlayer
{
    protected BasePlayerController m_kPlayerController;

    // 同步相关
    protected ActQueue m_actQueue = new ActQueue();
    protected TweenData m_curTweenPosData;
    protected TweenData m_preTweenPosData;
    protected TweenData m_preMoveData;
    protected bool m_tweening;

    protected float m_lastReceivePosTime;
    protected short m_lastTweenIndex;          // 刚刚缓动的那个包的时间

    protected PlayerRotTweener m_rotTweener;
    protected PlayerPosTweener m_posTweener;

    protected float m_lastAnglePitch;
   
    // debug
    protected float m_lastTweenTime;
    private float m_preMoveDataTime;


    protected override void SetGO(GameObject go)
    {
        base.SetGO(go);

        m_posTweener = m_mcc.AddMissingComponent<PlayerPosTweener>("PlayerPosTweener", go);
        m_posTweener.enabled = true;
        m_posTweener.AddOnFinished(PosTweenFinished);


        m_rotTweener = m_mcc.AddMissingComponent<PlayerRotTweener>("PlayerRotTweener", go);
        m_rotTweener.enabled = true;
        //m_rotTweener.AddOnFinished(TweenRotFinished);
    }

    public override void Jump()
    {
        base.Jump();
        m_kPlayerController.StartJump();
    }

    protected override void Revive(bool dataRevive)
    {
        base.Revive(dataRevive);

        m_tweening = false;
        m_lastTweenIndex = 0;
        m_actQueue.Clear();
    }

    // 一局结束后，服务器停止转发坐标，此时有些人还在空中
    public override void GameRoundEnd()
    {
        if (isDmmModel == false && isAlive && !released)
        {
            SetPosNow(m_actQueue.DequeueLastPos());
            AllActFinished();
            FallOnTheGround();
            if (m_kPlayerController != null)
                m_kPlayerController.JumpComplete();
        }
    }

#region 同步相关

    private bool IsServerPosChanged(float[] pos, float rotY)
    {
        if (m_kServerData.pos == null)
            return true;

        if (pos == null)
            return false;

        if (m_kServerData.pos.Length >= 5 && m_kServerData.pos[4] != rotY)
            return true;

        for (int i = 0; i < 3; i++)
        {
            if (pos[i] != m_kServerData.pos[i])
                return true;
        }

        return false;
    }

    internal override void Act(PlayerActParam param)
    {
        if (!alive)
            return;

        if (m_kTrans == null && param.pos != null)
            SetPosNow(param.pos, param.angleY);
        else
        {
            CacheAct(param);
            base.Act(param);
        }
    }

    virtual internal void CacheAct(PlayerActParam param)
    {
        if (param.index != -1 && m_lastTweenIndex != 0 && param.index < m_lastTweenIndex)
        {
            Logger.Error("CachePos 忽略过期坐标 index = " + param.index + " lastIndex = " + m_lastTweenIndex + " pos = " + param.pos.ToStr() + " prePos = " + (m_preMoveData == null ? "null" : m_preMoveData.pos.ToStr()));
            return;
        }

        param.isAI = isAI;
        m_actQueue.Enqueue(param);

        if (m_tweening == false || m_curTweenPosData != null && m_curTweenPosData.canBreak && param.type == GameConst.ACT_TYPE_MOVE)
        {
            if (m_curTweenPosData != null && m_curTweenPosData.canBreak)
            {
                ActNow(m_curTweenPosData, false);
            }

            DoNextAct();
        }
    }

    private void DoNextAct()
    {
        int maxMoveCnt = isAI ? GlobalConfig.gameSetting.AIPosQueueMaxNum : GlobalConfig.gameSetting.PosQueueMaxNum;
        if (m_actQueue.moveCount >= maxMoveCnt)
        {
            Logger.Warning("队列中包太多瞬移到最后一个位置 MoveCnt = " + m_actQueue.moveCount + " MaxNum = " + maxMoveCnt);

            // TODO: 这里可能会有问题，没有补跳跃的相关动画
            TweenData lastPD = m_actQueue.DequeueLastPos();
            ActNow(lastPD, true);
            //ActAllAct();
            return;
        }

        int moveNum = m_actQueue.moveCount;
        if (moveNum > 0)
        {
            if(moveNum > GlobalConfig.gameSetting.PosQueueNormalNum)
            {
                float speedFactor = isAI ? GlobalConfig.gameSetting.AITweenSpeedUpFactor : GlobalConfig.gameSetting.TweenSpeedUpFactor;
                float speedUp = speedFactor * moveNum / GlobalConfig.gameSetting.PosQueueNormalNum;
                m_actQueue.SpeedUp(speedUp);
                Logger.Log("SpeedUp = " + speedUp + " MoveNum=" + moveNum + " NorNum=" + GlobalConfig.gameSetting.PosQueueNormalNum + " factor=" + speedFactor);
            }
            else
            {
                m_actQueue.SpeedUp(1);
            }
        }
        
        TweenData pd = m_actQueue.Dequeue();

        if (pd != null)
        {
            if (pd.noTween)
            {
                ActNow(pd, false);
                DoNextAct();
            }
            else
                ActWithTween(pd);
        }
        else
        {
            if(status.jump && !isDmmModel) // 跳跃没结束但没包了，需要伪造一个落地包，否则会定在空中
            {
                Logger.Warning("跳跃没包，手动加包 index = " + m_lastTweenIndex);

                Vector3 groundPoint = SceneManager.GetGroundPoint(position);
                pd = new TweenData()
                {
                    type = GameConst.ACT_TYPE_MOVE,
                    pos = new float[] { groundPoint.x, groundPoint.y, groundPoint.z}, 
                    angleY = 4399,
                    anglePitch = 4399,
                    clientSpeed = -1,
                    maxSpeed = -1,
                    index = m_lastTweenIndex,
                    canBreak = true,
                    moveState = GameConst.MOVE_STATE_WALK,
                    fakeJumpGroundData = true
                };

                ActWithTween(pd);
            }
            else
            {
                 AllActFinished();
            }
        }
        
    }

    private void ActAllAct()
    {
        TweenData lastPD = m_actQueue.DequeueLastPos();
        ActNow(lastPD, true);

        for(int i=0; i<m_actQueue.count; i++)
        {
            bool setPos = i == m_actQueue.count -1;
            ActNow(m_actQueue.Dequeue(), setPos);
        }
    }

    internal void ActWithTween(TweenData td)
    {
        m_tweening = true;

        m_preTweenPosData = m_curTweenPosData;
        if (m_curTweenPosData != null && m_curTweenPosData.type == GameConst.ACT_TYPE_MOVE)
            m_preMoveData = m_curTweenPosData;
        m_curTweenPosData = td;

        TweenPos(td);
    }

    virtual internal void TweenPos(TweenData td)
    {
        float[] pos = td.pos;
        float speed = td.maxSpeed;
        m_lastTweenIndex = (short)td.index;

        // start jump: walkState -> jumpState
        if (m_preMoveData != null && m_preMoveData.fakeJumpGroundData == false &&
            m_preMoveData.moveState == GameConst.MOVE_STATE_WALK  &&
            td.type == GameConst.ACT_TYPE_MOVE && td.moveState == GameConst.MOVE_STATE_JUMP)
        {
            //Logger.Warning("跳跃 开始");
            Jump();
        }

        float tweenTime = 0;
        float tweenTimeAngle = 0;
        Vector3 targetPos = new Vector3(pos[0], pos[1], pos[2]);
        if (position != targetPos)
        {
            if (speed > 0 && Vector3.SqrMagnitude(targetPos - position) >= Mathf.Pow(GlobalConfig.gameSetting.OPPosDelayTime * speed, 2))
            {
                tweenTime = 0;
                Logger.Error("本次移动距离过大，直接瞬移 " + Vector3.Distance(targetPos, position) + " >= " + GlobalConfig.gameSetting.OPPosDelayTime * speed);
            }
            else
            {
                if (status.jump)    // 跳跃阶段，包是可以冲掉前一个，所以可以使用统一缓动时间
                {
                    if(m_curTweenPosData.type == GameConst.ACT_TYPE_MOVE)
                    {
                        if (m_curTweenPosData.fakeJumpGroundData)
                            tweenTime = Vector3.Distance(position, targetPos) / GlobalConfig.gameSetting.TweenFakeJumpSpeed;     // 这里时间计算可能会有问题
                        else if (m_preTweenPosData != null && m_preTweenPosData.moveState == GameConst.MOVE_STATE_JUMP && m_curTweenPosData.moveState == GameConst.MOVE_STATE_WALK)
                            tweenTime = GlobalConfig.gameSetting.TweenJumpGroundTime;
                        else
                            tweenTime = GlobalConfig.gameSetting.TweenJumpTime;
                    }
                    else
                    {
                        tweenTime = -2;     // 标记跳跃过程中的FireData
                    }
                }
                else
                {
                    tweenTime = td.tweenTime;
                }

                tweenTimeAngle = tweenTime;

                if (m_curTweenPosData.type == GameConst.ACT_TYPE_MOVE)
                {
                    if (m_preMoveData == null)
                    {
                        PlayMoveOrStandAni(targetPos, position);
                        //Logger.Warning("PlayMoveOrStandAni, position");
                    }
                    else
                    {
                        PlayMoveOrStandAni(targetPos, m_preMoveData.pos.ToVector3());
                        //Logger.Warning("PlayMoveOrStandAni, " + m_preMoveData.index + " " + m_curTweenPosData.index);

                        //float dt = Time.timeSinceLevelLoad - m_preMoveDataTime;
                        //if (dt > GlobalConfig.gameSetting.TweenTime)
                        //    Logger.Warning("CostTime = " + dt + " DeltaTime = " + (dt - GlobalConfig.gameSetting.TweenTime));
                        //else
                        //    Logger.Log("CostTime = " + dt + " DeltaTime = " + (dt - GlobalConfig.gameSetting.TweenTime));
                    }
                }
            }
        }
        else if(td.angleY != 4399 && td.angleY != eulerAngle.y)
        {
            tweenTimeAngle = GlobalConfig.gameSetting.TweenTime;
        }

        if (m_curTweenPosData.type == GameConst.ACT_TYPE_MOVE)
            m_preMoveDataTime = Time.timeSinceLevelLoad;

        // Rotate
        if (td.angleY != 4399 && td.angleY != eulerAngle.y)
        {
            // TODO: Y旋转缓动时间要与位移时间相同
            m_rotTweener.enabled = true;
            m_rotTweener.TweenRot(tweenTimeAngle, td.angleY);
        }
        //else
        //    Logger.Log("TweenRot: " + (td.angleY == 4399) + " " + (td.angleY == eulerAngle.y));

        if (td.anglePitch != 4399)
        {
            m_lastAnglePitch = td.anglePitch;

            // TODO: Pitch缓动时间要与位移时间相同
            m_kPlayerController.SetPitchAngle(m_lastAnglePitch, tweenTimeAngle);
        }

        // TweenPos
        m_lastTweenTime = tweenTime;
        if(position == targetPos || tweenTime == -2 || tweenTime == 0)
        {
            if (tweenTime == 0)
                SetPosNow(td);
            else
                PosTweenFinished();
        }
        else
        {
            m_posTweener.enabled = true;
            if (tweenTime > 0)
                clientMoveSpeed = Vector3.Magnitude(td.pos.ToVector3() - position) / tweenTime;
            else if(tweenTime < 0)
            {
                Logger.Error("TweenTime Error " + tweenTime + " " + td.tweenTime + " " + td.speedUpFactor);
                tweenTime = 0;
            }

            status.move = true;
            m_posTweener.TweenPos(tweenTime, targetPos);
        }

        //float dis = (td.pos.ToVector3() - position).magnitude;
        //string msg = "TweenPos: " + " type=" + td.type + " speed=" + (tweenTime == 0 ? 4399 : dis / tweenTime) + " dis=" + dis + " tweenTime=" + tweenTime + " td.tweenTime=" + td.tweenTime + " " + td.pos.ToVector3() + " " + position;
        //if (tweenTime == 0 && dis != 0)
        //    Logger.Error(msg);
        //else
        //    Logger.Log(msg);
    }

    //private void TweenRotFinished()
    //{
    //    if (alive == false)
    //        return;

    //    //m_tweenRot = null;
    //}

    private void PosTweenFinished()
    {
        //Logger.Warning("StepTweenFinished");

        m_tweening = false;

        if (alive == false)
            return;

        if (m_preMoveData != null && (m_preMoveData.fakeJumpGroundData || m_preMoveData.moveState == GameConst.MOVE_STATE_JUMP) &&
            m_curTweenPosData != null && m_curTweenPosData.moveState == GameConst.MOVE_STATE_WALK)
        {
            m_kPlayerController.JumpComplete();
        }

        ActNow(m_curTweenPosData, false);
        if (m_actQueue.moveCount <= 0)
            status.move = false;

        DoNextAct();
    }

    private void ActNow(TweenData td, bool setPos)
    {
        m_tweening = false;

        if (td != null)
        {
            if (setPos)
                SetPosNow(td.pos, td.angleY);

            switch (td.type)
            {
                case GameConst.ACT_TYPE_MOVE:
                    break;
                case GameConst.ACT_TYPE_FIRE:
                    WorldManager.DoProto_fire(td.protoFire);
                    if(curWeapon != null)
                        curWeapon.localWeaponData.curClipAmmoNum = td.protoFire.notPlayAni >> 4;
                    if (td.cameraRot != GameConst.Vector4399)
                        FireCameraRot(td.cameraRot);
                    break;
                case GameConst.ACT_TYPE_RELOAD:
                    WorldManager.DoProto_reload(td.protoReload);
                    break;
                case GameConst.ACT_TYPE_WEAPON_MOTION:
                    WorldManager.DoProto_WeaponMotion(td.protoWeaponMotion);
                    break;
            }
        }
    }

    virtual protected void FireCameraRot(Vector3 rot)
    {

    }

    private void AllActFinished()
    {
        m_actQueue.Clear();
        if (m_posTweener != null)
            m_posTweener.enabled = false;
        m_tweening = false;

        if(m_curTweenPosData != null && m_curTweenPosData.type == GameConst.ACT_TYPE_MOVE)
            PlayMoveAni(false);

        //Logger.Warning("位置走完了");
    }

    private void SetPosNow(TweenData pd)
    {
        //if (pd == null || m_curTweenPosData != pd)
        //{
        //    m_preTweenPosData = m_curTweenPosData;
        //    m_curTweenPosData = pd;
        //}

        if (pd == null)
            return;

        if (m_posTweener != null)
            m_posTweener.enabled = false;

        m_lastTweenIndex = (short)pd.index;

        SetPosNow(pd.pos);

        PosTweenFinished();
    }

    public override void ClearPosQueue()
    {
        m_actQueue.Clear();
    }

    virtual protected void PlayMoveOrStandAni(Vector3 targetPos, Vector3 prePos)
    {
       
    }

    virtual protected void PlayMoveAni(bool isMove)
    {
        //status.move = isMove;
    }

#endregion

#region property

    public int posCount
    {
        get { return m_actQueue.moveCount; }
    }

    public bool tweening
    {
        get { return m_tweening; }
    }

    public float lastTweenTime
    {
        get { return m_lastTweenTime; }
    }

    public bool tweenerEnable
    {
        get { return m_posTweener == null ? false : m_posTweener.enabled; }
    }

    public PlayerPosTweener posTweener
    {
        get { return m_posTweener; }
    }

    public PlayerRotTweener rotTweener
    {
        get { return m_rotTweener; }
    }

    public float lastAnglePitch
    {
        get { return m_lastAnglePitch; }
    }

    #endregion

}

public class TweenData
{
    public int type;
    public float[] pos;          // length == 3
    public Vector3 cameraRot = GameConst.Vector4399;
    public float angleY;
    public float anglePitch;
    public float maxSpeed;
    public float clientSpeed;
    public int index;

    public bool canBreak;
    public int moveState;
    public bool fakeJumpGroundData;
    public bool noTween;

    private float _tweenTime;     // -1 表示此时间还未计算，一般为非move包，此状态下，就不要dequeue此包
    public float speedUpFactor = 1;

    private PlayerActParam pap;

    // 有可能不会调用这里
    internal void InitWithPAP(PlayerActParam param)
    {
        pap = param;

        type = param.type;
        pos = param.pos;
        cameraRot = param.cameraRot;
        angleY = param.angleY;
        anglePitch = param.anglePitch;
        moveState = param.moveState;
        index = param.index;
        canBreak = param.moveState == GameConst.MOVE_STATE_JUMP ? true : false;
        noTween = param.noTween;
    }

    #region property

    internal toc_fight_fire protoFire
    {
        get { return pap.protoFire; }
    }

    public float tweenTime
    {
        set { _tweenTime = value; }
        get {
            if (_tweenTime > 0)
            {
                if (speedUpFactor != 1)
                    Logger.Log("tweenTime=" + _tweenTime + " factor=" + speedUpFactor);
                return _tweenTime / speedUpFactor;
            }
                
            else
                return _tweenTime;
        }
    }

    internal toc_fight_reload protoReload
    {
        get { return pap.protoReload; }
    }

    internal toc_fight_weapon_motion protoWeaponMotion
    {
        get { return pap.protoWeaponMotion; }
    }

    #endregion

    public override string ToString()
    {
        return " moveState = " + moveState + " canBreak = " + canBreak + " time = " + index;
    }
}

public class ActQueue
{
    List<TweenData> m_list = new List<TweenData>();
    private int m_moveDataCnt;
    private Vector3 m_lastDequeueMovePos = GameConst.Vector4399;
    private Vector3 m_lastPos;

    internal TweenData Enqueue(PlayerActParam param)
    {
        TweenData td = new TweenData();
        td.InitWithPAP(param);

        float tweenTime = param.isAI ? GlobalConfig.gameSetting.AITweenTime : GlobalConfig.gameSetting.TweenTime;

        // 若收到的为move包，则评估此包与前一个move包间fire包的time
        if(td.type == GameConst.ACT_TYPE_MOVE)
        {
            td.tweenTime = tweenTime;

            // find fire index
            TweenData preMoveData = null;
            int fireStartIndex = 0;
            for (int i = m_list.Count - 1; i >= 0; i-- )
            {                
                if(m_list[i].type == GameConst.ACT_TYPE_MOVE)
                {
                    preMoveData = m_list[i];
                    fireStartIndex = i + 1;
                    break;
                }
            }

            // evaluate fire tween time
            if (m_list.Count != 0 && fireStartIndex <= m_list.Count - 1)
            {
                if (td.moveState == GameConst.MOVE_STATE_JUMP)
                {
                    // 跳跃过程中，firepos不要走，只作fire动作                
                    for (int i = fireStartIndex; i < m_list.Count; i++)
                    {
                        m_list[i].tweenTime = -2;
                    }
                }
                else
                {
                    Vector3 preMoveDataPos;
                    if (m_lastDequeueMovePos == GameConst.Vector4399)
                    {
                        preMoveDataPos = m_list[fireStartIndex].pos.ToVector3();
                        Logger.Error("Fuck m_lastDequeueMovePos not init");
                    }
                    else
                        preMoveDataPos = m_lastDequeueMovePos;

                    //Logger.Log("================ Evaluate Time ================");
                    //Logger.Log("StartIndex = " + fireStartIndex + " EndIndex = " + (m_list.Count - 1));

                    //string msg="";
                    //float sum = 0;

                    Vector3 prePos = preMoveDataPos;
                    float dis = 0;
                    for (int i = fireStartIndex; i < m_list.Count; i++)
                    {
                        if (m_list[i].type != GameConst.ACT_TYPE_FIRE)
                            continue;

                        dis += (m_list[i].pos.ToVector3() - prePos).magnitude;
                        prePos = m_list[i].pos.ToVector3();
                    }
                    dis += (td.pos.ToVector3() - prePos).magnitude;

                    prePos = preMoveDataPos;
                    float et = tweenTime - Time.smoothDeltaTime;    // 这里减去一帧的时间，因为实测消耗的时候会多一帧的时间
                    float time = 0;
                    for (int i = fireStartIndex; i < m_list.Count; i++)
                    {
                        if (m_list[i].type != GameConst.ACT_TYPE_FIRE)
                            continue;

                        if (dis == 0)
                            m_list[i].tweenTime = 0;
                        else
                            m_list[i].tweenTime = et * (m_list[i].pos.ToVector3() - prePos).magnitude / dis;

                        //if (m_list[i].tweenTime == 0)
                        //    Logger.Warning("Evaluate fireTweenTime = 0, " + m_list[i].pos.ToVector3() + " " + prePos);
                        //else
                        //    Logger.Log("Evaluate fireTweenTime = 0, " + m_list[i].pos.ToVector3() + " " + prePos);

                        //msg += m_list[i].tweenTime + " ";
                        //sum += m_list[i].tweenTime;

                        time += m_list[i].tweenTime;
                        prePos = m_list[i].pos.ToVector3();
                        //Logger.Log("FireData " + m_list[i].pos.ToVector3() + " " + m_list[i].tweenTime);
                    }

                    td.tweenTime = et - time;
                    float d = (td.pos.ToVector3() - prePos).magnitude;
                    if (td.tweenTime <= 0 && d > 0)
                    {
                        //Logger.Error("Evaluated movedata tweenTime <= 0 , " + td.tweenTime + " dis= " + dis);
                        td.tweenTime = 0;
                    }

                    //msg += td.tweenTime;
                    //sum += td.tweenTime;

                    //if (sum > GlobalConfig.gameSetting.TweenTime)
                    //    Logger.Error("Sum = " + sum.ToString() + " = " + msg );
                    //else
                    //    Logger.Log("Sum = " + sum.ToString() + " = " + msg);

                    //Logger.Log("MoveData " + td.pos.ToVector3() + " " + preMoveDataPos + " " + td.tweenTime);
                    //Logger.Log("================================================");
                }
            }
            else
            {
                //Logger.Log("NoFireData, tweenTime = " + td.tweenTime);
            }
        }
        else
        {
            td.tweenTime = -1;
        }

        //if(param.index != -1)
        //{
        //    foreach(TweenData t in m_list)
        //    {
        //        if (t.index == param.index)
        //            Logger.Error("Fuck same index");
        //    }
        //}

        if (param.index == -1)
        {
            m_list.Add(td);
        }
        else
        {
            int index = -1;
            for (int i = 0; i < m_list.Count; i++)
            {
                if (m_list[i].index == -1)
                    continue;

                if (td.index < m_list[i].index)
                {
                    index = i;
                    break;
                }
            }

            if (index == -1)
                m_list.Add(td);
            else
                m_list.Insert(index, td);
        }
        //m_list.Add(td);

        if (td.type == GameConst.ACT_TYPE_MOVE)
            m_moveDataCnt++;

        return td;
    }

    public TweenData Dequeue()
    {
        if (m_list.Count == 0)
            return null;

        TweenData pd = m_list[0];

        if (!pd.noTween && pd.tweenTime == -1)     // -1表示firePos没有评估过
        {
            // 如果FireData位置跟上个位置距离不远，那就没必要评估时间了；
            // 而且主角原地不动时，2s才发一次包，这样其间的FireData就不会执行，这里也是为了避免这个问题
            if ((pd.pos.ToVector3() - m_lastPos).magnitude > 0.05f)  
                return null;
            else
            {
                pd.tweenTime = 0;
            }
        }

        //if(pd.tweenTime == 0 && pd.type == GameConst.ACT_TYPE_FIRE)
        //    Logger.Warning("Dequeue, tweenTime = 0, " + pd.type + " " + pd.pos.ToVector3() + " " + m_lastPos);
        //else
        //    Logger.Log("Dequeue, tweenTime = 0, " + pd.type + " " + pd.pos.ToVector3() + " " + m_lastPos);
            
        m_list.RemoveAt(0);

        if (pd.type == GameConst.ACT_TYPE_MOVE)
        {
            m_lastDequeueMovePos = pd.pos.ToVector3();
            m_moveDataCnt--;
        }

        if(!pd.noTween)
            m_lastPos = pd.pos.ToVector3();

        return pd;
    }

    public TweenData DequeueLastPos()
    {
        //取时间最新的那个包，因为是排好序的，所有直接取最后一个就可以了
        if (m_list.Count == 0)
            return null;

        TweenData pd = m_list[m_list.Count - 1];
        Clear();

        return pd;
    }

    public void SpeedUp(float factor)
    {
        for(int i=0; i<m_list.Count; i++)
        {
            m_list[i].speedUpFactor = factor;
        }
    }

    public void Clear()
    {
        m_list.Clear();
        m_moveDataCnt = 0;
    }

    //public int count
    //{
    //    get { return m_list.Count; }
    //}

    public int moveCount
    {
        get { return m_moveDataCnt; }
    }

    public int count
    {
        get { return m_list.Count; }
    }
}

struct PlayerActParam
{
    public int type;           // 1: move 2:fire

    public float[] pos;        // length == 3
    public int index;
    public int moveState;
    public float anglePitch;
    public float angleY;
    public Vector3 cameraRot;

    //public float receiveTime;  // client收到包的时间

    // fire
    public toc_fight_move protoMove;
    public toc_fight_fire protoFire;
    public toc_fight_reload protoReload;
    public toc_fight_weapon_motion protoWeaponMotion;

    public bool isAI;
    public bool noTween;


    public void InitWithProto(toc_fight_move proto)
    {
        protoMove = proto;

        type = GameConst.ACT_TYPE_MOVE;
        pos = InitPos(proto.pos);
        cameraRot = GameConst.Vector4399;

        if (proto.pos != null && proto.pos.Length >= 4)
            angleY = proto.pos[3] / 50f;
        else
            angleY = proto.angleY * 2;

        if (proto.pos != null && proto.pos.Length >= 5)
            anglePitch = proto.pos[4] / 50f;
        else
            anglePitch = proto.anglePitch;

        index = proto.time;
        moveState = proto.moveState;
        //receiveTime = Time.timeSinceLevelLoad;
    }

    public void InitWithProto(toc_fight_fire proto)
    {
        protoFire = proto;

        type = GameConst.ACT_TYPE_FIRE;
        pos = InitPos(proto.firePos);
        cameraRot = InitCameraRot(proto.cameraRot);
        anglePitch = 4399;
        angleY = 4399;
        index = -1;
        moveState = -1;
    }

    public void InitWithProto(toc_fight_reload proto)
    {
        protoReload = proto;
        noTween = true;

        type = GameConst.ACT_TYPE_RELOAD;
        pos = null;
        cameraRot = GameConst.Vector4399;
        anglePitch = 4399;
        angleY = 4399;
        index = -1;
        moveState = -1;
    }

    public void InitWithProto(toc_fight_weapon_motion proto)
    {
        protoWeaponMotion = proto;
        noTween = true;

        type = GameConst.ACT_TYPE_WEAPON_MOTION;
        pos = null;
        cameraRot = GameConst.Vector4399;
        anglePitch = 4399;
        angleY = 4399;
        index = -1;
        moveState = -1;
    }

    private float[] InitPos(short[] pos)
    {
        float[] p = new float[3];
        p[0] = pos[0] * 0.01f;
        p[1] = pos[1] * 0.01f;
        p[2] = pos[2] * 0.01f;
        return p;
    }

    private Vector3 InitCameraRot(short[] rot)
    {
        Vector3 r = new Vector3();
        if(rot.Length >= 2)
        {
            r.x = rot[0] / 50f;
            r.y = rot[1] / 50f;
        }
        return r;        
    }

}

