﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Widget
{
    public int id;
    public GameObject gameObject;
    public Transform transform;
    public ConfigItemDecorationLine config;
    public string model;
    public int sex;

    public List<Action<Widget>> lstCallBack = new List<Action<Widget>>();

    public void AddCallBack(Action<Widget> cb)
    {
        lstCallBack.Add(cb);
    }

    public void SetParent(Transform parent)
    {
        if (gameObject != null)
            gameObject.transform.SetParent(parent);
    }

    public void LoadRes(int id, int sex, Action<Widget> funLoaded)
    {
        ConfigItemDecorationLine config = ConfigManager.GetConfig<ConfigItemDecoration>().GetLine(id);
        if (config == null)
            return;
        this.id = id;
        this.config = config;
        this.sex = sex;
        model = (sex == ModelConst.MODEL_PLAYER_BOY_SEX ? config.BoyModel : config.GirlModel);
        string path = PathHelper.GetWidgetPath(model);
        ResourceManager.LoadBattlePrefab(path, (go, crc) =>
        {
            if (go == null)
                return;
            gameObject = go;
            transform = go.transform;
            Deactive();
            if (funLoaded != null)
                funLoaded(this);
            if(lstCallBack.Count != 0)
            {
                for(int i = 0, imax = lstCallBack.Count; i < imax; ++i)
                {
                    lstCallBack[i](this);
                }
                lstCallBack.Clear();
            }
        });
    }

    public void Attach(Transform root)
    {
        if (gameObject == null || root == null)
            return;

        Transform slotParent = GameObjectHelper.FindChildByTraverse(root, config.SlotPath);
        if (slotParent == null)
        {
            Logger.Error("Attach Player:" + root.root.name + "  SlotPath:" + config.SlotPath + "  Not found!");
            return;
        }
        SetParent(slotParent);

        if (sex == ModelConst.MODEL_PLAYER_BOY_SEX)
        {
            transform.localPosition = config.BoyPos;
            transform.localEulerAngles = config.BoyRot;
        }
        else if (sex == ModelConst.MODEL_PLAYER_GIRL_SEX)
        {
            transform.localPosition = config.GirlPos;
            transform.localEulerAngles = config.GirlRot;
        }
        transform.localScale = Vector3.one;
    }

    public void Deactive()
    {
        if (gameObject != null)
        {
            if (SceneManager.singleton.reclaimRoot != null)
            {
                SetParent(SceneManager.singleton.reclaimRoot);
                transform.localPosition = Vector3.zero;
            }
            else
            {
                SetParent(null);
                transform.localPosition = new Vector3(4399, 4399);
            }
        }
    }

    public void Release()
    {
        ResourceManager.Destroy(gameObject);
        lstCallBack.Clear();
    }
}

public class WidgetManager
{
    static private Dictionary<int, List<Widget>> m_dicWidgetCache = new Dictionary<int, List<Widget>>();
    static private List<Widget> m_listAllWidget = new List<Widget>();

    private Dictionary<int, Widget> m_kdicWidget = new Dictionary<int, Widget>();
    private BasePlayer m_player;

    private int m_widgetValue;

    public WidgetManager(BasePlayer player)
    {
        m_player = player;
    }

    public void AttachWidget(List<int> widgetList, Action<Widget> callback = null)
    {
        if (widgetList == null || widgetList.Count == 0)
            return;

        int v = 1;
        m_widgetValue = 0;
        for(int i = 0, imax = widgetList.Count; i < imax; ++i)
        {
            m_widgetValue += widgetList[i] * v;
            v *= 10;
            AttachWidget(widgetList[i], callback);
        }
    }

    private void AttachWidget(int id, Action<Widget> callback = null)
    {
        int sex = m_player.configData.ModelSex;
        int widgetId = GetCacheId(id, sex);

        if (m_kdicWidget.ContainsKey(widgetId))
        {
            Widget widget = m_kdicWidget[widgetId];

            Action<Widget> cb = (w) =>
            {
                w.Attach(m_player.transform);
                if (callback != null)
                    callback(w);
            };

            if (widget.gameObject != null)
            {
                cb(widget);
            }
            else
            {
                widget.AddCallBack(callback);
            }
        }
        else
        {
            Widget w = GetWidget(id, sex, (widget) =>
            {
                widget.Attach(m_player.transform);
                if (callback != null)
                    callback(widget);
            });
            m_kdicWidget.Add(widgetId, w);
        }
    }

    public void DetachAllWidget()
    {
        foreach (var widget in m_kdicWidget.Values)
        {
            Deactive(widget);
        }
        m_kdicWidget.Clear();
        m_widgetValue = 0;
    }

    public Widget GetWidget(int id)
    {
        Widget widget = null;
        m_kdicWidget.TryGetValue(id, out widget);
        return widget;
    }

    public List<Widget> GetAllWidget()
    {
        List<Widget> allwidget = m_kdicWidget.Values.ToList<Widget>();
        return allwidget;
    }

    public void Release()
    {
        DetachAllWidget();
    }

    public int widgetVaule
    {
        get
        {
            return m_widgetValue;
        }
    }

    static private Widget GetWidget(int id, int sex, Action<Widget> funLoaded)
    {
        List<Widget> listWidget = null;
        int cacheId = GetCacheId(id, sex);
        m_dicWidgetCache.TryGetValue(cacheId, out listWidget);
        if (listWidget == null)
        {
            listWidget = new List<Widget>();
            m_dicWidgetCache.Add(cacheId, listWidget);
        }

        Widget widget = null;

        int iRearIndex = listWidget.Count - 1;
        if (iRearIndex != -1)
        {
            widget = listWidget[iRearIndex];
            listWidget.RemoveAt(iRearIndex);
            if (funLoaded != null)
                funLoaded(widget);
        }
        else
        {
            widget = new Widget();
            m_listAllWidget.Add(widget);
            widget.LoadRes(id, sex, funLoaded);
        }

        return widget;
    }

    static private int GetCacheId(int id, int sex)
    {
        return id + sex * 100000000;
    }

    static private void Deactive(Widget widget)
    {
        if (widget == null)
            return;

        List<Widget> listWidget = null;
        int cacheId = GetCacheId(widget.id, widget.sex);
        m_dicWidgetCache.TryGetValue(cacheId, out listWidget);
        if (listWidget == null)
        {
            listWidget = new List<Widget>();
            m_dicWidgetCache.Add(cacheId, listWidget);
        }
        widget.Deactive();
        listWidget.Add(widget);
    }

    static public void ReleaseAllRes()
    {
        foreach (Widget widget in m_listAllWidget)
        {
            if (widget != null)
                widget.Release();
        }
        m_listAllWidget.Clear();
        m_dicWidgetCache.Clear();
    }
}
