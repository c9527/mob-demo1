﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerAniBaseController 
{
    protected string m_strCurAniName;
    protected GameObject m_go;
    protected BasePlayer m_player;
    protected bool m_isLooping;

    public PlayerAniBaseController()
    {
    }

    public PlayerAniBaseController(BasePlayer player)
    {
        m_player = player;
    }

    virtual public void SetAnimation(Animation kAni)
    {
    }

    virtual public void SetAnimator(Animator kAni)
    {
    }

    virtual public void Play(string strAniName, int iLayer = -1, float normalizedTime = 0f)
    {
    }

    virtual public void SetLooping(bool isLooping)
    {
        m_isLooping = isLooping;
    }

    virtual public void CrossFade(string strAniName, float transitionDuration = 0.2f, int iLayer = -1)
    {
    }

    virtual public void SetInteger(string strPropertyName, int iValue)
    {
    }

    virtual public void SetTrigger(string strName)
    {
    }

    virtual public void SetBool(string strName, bool bValue)
    {
    }

    virtual public void SetFloat(string strName, float bValue)
    {
    }

    virtual public void EnableUpBodyLayer(bool val)
    {
    }

    /// <summary>
    /// 是否打开WalkWaggle
    /// </summary>
    /// <param name="val"></param>
    virtual public void EnableWalkWaggleLayer(bool val)
    {
    }

    virtual public void SetSpeed(float speed)
    {
    }

    virtual public bool IsCurrentAni(string aniName, int layer = 0)
    {
        return false;
    }

    virtual public void Release()
    {

    }

    virtual public bool enable
    {
        set
        {

        }
    }

    virtual public Animator animator
    {
        get { return null; }
    }

    virtual public Animation animation
    {
        get { return null; }
    }

}

