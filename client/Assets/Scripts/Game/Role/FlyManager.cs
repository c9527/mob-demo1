﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class FlyManager
{
    public float upSpeed;
    public float downSpeed;
    public float maxPower;
    public float powerUpSpeed;
    public float powerDownSpeed;
    public float nowPower;
    public bool flyup = false;
    public bool flydown = false;
    public FlyManager()
    {

    }

    public void init(TDSKillInfo ski)
    {
        string[] paramArr = ski.Param.Split(',', ';');
        upSpeed = float.Parse(paramArr[0]);
        downSpeed = float.Parse(paramArr[1]);
        maxPower = float.Parse(paramArr[2]);
        powerDownSpeed = float.Parse(paramArr[3]);
        powerUpSpeed = float.Parse(paramArr[4]);
        nowPower = maxPower;
    }

    public void UpdateFly()
    {
        if (flyup && !flydown)
        {
            nowPower -= Time.smoothDeltaTime * powerDownSpeed;
            if (nowPower <= 0)
            {
                flyup = false;
                flydown = true;
                GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_POWER_OFF);
                //Logger.Error("没气了");
            }
        }
        if (!flyup)
        {
            if (nowPower < maxPower)
            {
                nowPower += Time.smoothDeltaTime * powerUpSpeed;
            }
            else
            {
                nowPower = maxPower;
            }
        }
    }

    public void revive()
    {
        nowPower = maxPower;
    }

    public bool isflyup()
    {
        return flyup && !flydown;
    }


}

