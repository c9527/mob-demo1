﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ModelComponentCache : MonoBehaviour {

    private Dictionary<string, Component> _dicComponent = new Dictionary<string, Component>();
    private Dictionary<string, Transform> _dicChild = new Dictionary<string, Transform>();
    public List<BoxCollider> modelBoxColider = new List<BoxCollider>();
    public Behaviour[] modelBehaviours;
    public List<AudioSource> audiosources = new List<AudioSource>();
    public List<Transform> _renderers = new List<Transform>();
    public List<Renderer> renderers
    {
        get
        {
            List<Renderer> result = new List<Renderer>();
            for (int i = 0, imax = _renderers.Count; i < imax; ++i)
            {
                result.Add(_renderers[i].GetComponent<Renderer>());
            }
            return result;
        }
        set
        {
            _renderers.Clear();
            for(int i = 0, imax = value.Count; i < imax; ++i)
            {
                _renderers.Add(value[i].transform);
            }
        }
    }
    public GameObject effect1;
    public Vector3 defaultScale = new Vector3(1, 1, 1);
    public int goLayer = -1;

    public bool HasInited(string name)
    {
        return _dicComponent.ContainsKey(name);
    }

    /// <summary>
    /// 先在缓存中寻找组件，如果没有就遍历寻找，找不到返回null，但不会添加组件
    /// </summary>
    public T FindComponent<T>(string name, GameObject go, bool recursive=true) where T : Component
    {
        T ret = null;
        Component compt;

        if (_dicComponent.TryGetValue(name, out compt))
        {
            if (compt != null)
            {
                if (typeof(T) == typeof(Renderer))
                {
                    compt = compt.GetComponent<Renderer>();
                }
                else if (typeof(T) == typeof(SkinnedMeshRenderer))
                {
                    compt = compt.GetComponent<SkinnedMeshRenderer>();
                }
                ret = compt as T;
            }
        }
        else
        {
            if(go != null)
            {
                if(typeof(T) == typeof(Transform))
                {
                    ret = go.transform.Find2(name, false) as T;
                }
                else
                {
                    if (recursive)
                        ret = go.GetComponentInChildren<T>();
                    else
                        ret = go.GetComponent<T>();
                }
            }
            if (typeof(T) == typeof(Renderer) || typeof(T) == typeof(SkinnedMeshRenderer))
                _dicComponent.Add(name, ret.transform);
            else
                _dicComponent.Add(name, ret);
        }
        
        return ret;
    }

    /// <summary>
    /// 递归寻找子节点，大小写敏感
    /// </summary>
    //public Transform FindChild(string name, Transform root)
    //{
    //    Transform target = null;
    //    if (_dicChild.TryGetValue(name, out target) == false)
    //    {
    //        target = root.Find2(name, false);
    //        _dicChild.Add(name, target);
    //    }

    //    return target;
    //}

    /// <summary>
    /// 先在缓存找是否添加过，没有就添加，只操作根节点，不会遍历
    /// </summary>
    public T AddMissingComponent<T>(string name, GameObject go) where T : Component
    {
        T ret = null;
        Component compt;
        _dicComponent.TryGetValue(name, out compt);
        ret = compt == null ? null : compt as T;
        if(ret == null)
        {
            ret = go.AddMissingComponent<T>();
            CacheComponent(name, ret);
        }

        return ret;
    }

    public void DeleteComponent(string name, Component compt)
    {
        if (compt != null && (compt is Renderer || compt is SkinnedMeshRenderer))
            compt = compt.transform;

        if (_dicComponent.ContainsKey(name) )
        {
            _dicComponent.Remove(name);
            GameObject.Destroy(compt);
        }
    }

    public void CacheComponent(string name, Component compt)
    {
        if (compt != null && (compt is Renderer || compt is SkinnedMeshRenderer))
            compt = compt.transform;

        if (_dicComponent.ContainsKey(name) == false)
            _dicComponent.Add(name, compt);
        else if (compt != null)
        {
            _dicComponent[name] = compt;
        }
    }

    //public Transform CreateChildIfNotExisted(Transform trans, string name)
    //{
    //    return null;
    //}

    public void CacheBoxCollider(List<BoxCollider> boxcolliders)
    {
        modelBoxColider = boxcolliders;
    }

    //public void CacheBehaviour(Behaviour[] behaviours)
    //{
    //    modelBehaviours = behaviours;
    //}

    //public void CacheAudioSource(List<AudioSource> audiosources)
    //{

    //}

    //public void CacheRenderers(List<Renderer> renderers)
    //{

    //}


   
   

	 
}
