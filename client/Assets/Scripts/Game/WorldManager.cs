﻿using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;

public class WorldManager
{
    static public WorldManager singleton;

    public static string[] CAMP_NAME = new string[] { "自由", "联盟军", "反叛者","生化人", "猫猫" };
    public static string[] CAMP_COLOR = new string[] { "#00d2ff", "#ff8a00", "#00d2ff", "#00d2ff", "#00d2ff" };

    public static int VisibleSkinnedMeshNum = 0;
    private float m_lastRecvUDPTime;
    
    public enum CAMP_DEFINE
    {
        ALONE = 0,
        UNION = 1,
        REBEL = 2,
        ZOMBIE = 3,
        CAT = 4, //猫猫
        SURVIVAL_PEOPLE = 11,
        SURVIVAL_ZOMBIE = 12,
    }

    static private bool m_fightEntered;

    private Dictionary<int, BasePlayer> m_allPlayer = new Dictionary<int, BasePlayer>();
    private Dictionary<int, BasePlayer> m_dicGOHashIDPlayer = new Dictionary<int, BasePlayer>();
    private static Dictionary<string, int> m_ParamSettingStatusDic = new Dictionary<string, int>();

    private int m_fightId;
    private uint m_room_handle = 0;
    private uint m_rand_id = 0;
    private string m_gameRule = "";
    private string m_fightType = "";

    private ConfigGameRuleLine m_gameRuleLine = null;
    private ConfigMapRuleLine m_mapRuleLine;

    public string ModeWeaponTYPE
    {
        get { return m_gameRuleLine.WeaponType; }
    }

    private float m_startTime;
    
    private p_fight_role[] m_roleList = null;
    public int[] m_rolepraise;
    public p_bag[] m_bagdata;
    public p_item[] m_roledata;
    private CameraSwitcher m_kCameraSwitcher;

    private bool m_bIsViewBattleModel = false;

    private bool m_isPve;
    private bool m_isZombieMode;

    static private bool s_hasZombieCloth;
    static public bool hasZombieCloth
    {
        get { return s_hasZombieCloth; }
    }

    static private bool s_hasZombieBlade;
    static public bool hasZombieBlade
    {
        get { return s_hasZombieBlade; }
    }

    private int m_gameStartTime;
    public int gameStartTime
    {
        get { return m_gameStartTime; }
        set { m_gameStartTime = value; }
    }

    public List<p_halo_info> m_haloList = new List<p_halo_info>();
    public Dictionary<int, Effect> m_haloEffectDic = new Dictionary<int, Effect>();


    public WorldManager()
    {
        if (singleton != null)
            Logger.Error("WorldManager is a singleton");
        singleton = this;
        Init();
    }

    private void Init()
    {
        m_bagdata = new p_bag[10];
    }

    public void ReleaseAllPlayerRes()
    {
        foreach (KeyValuePair<int, BasePlayer> kvp in m_allPlayer)
        {
            kvp.Value.Release(true);
        }

        if (FPWatchPlayer.singleton != null)
            FPWatchPlayer.singleton.Release(true);

        PlayerManager.ReleaseGO();
        PlayerManager.ReleaseObj();
        //MainPlayer.ReleaseCacheGO();
        //OtherPlayer.ReleaseCacheGO();
        //FPWatchPlayer.ReleaseCacheGO();
        LodManager.ReleaseCacheGO();
        WidgetManager.ReleaseAllRes();

        m_allPlayer.Clear();
        m_dicGOHashIDPlayer.Clear();
        MainPlayer.singleton = null;
        FPWatchPlayer.singleton = null;

        m_roleList = null;
        m_rolepraise = null;
        if (m_kCameraSwitcher != null)
            m_kCameraSwitcher.Release();
        m_kCameraSwitcher = null;
    }

    static OtherPlayer GetOtherPlayer(toc_fight_base proto)
    {
        if (proto.from == singleton.m_fightId)
            return null;

        BasePlayer player = singleton.GetPlayerById(proto.from);
        return player as OtherPlayer;
    }

    #region 协议监听

    static void Toc_fight_msg(toc_fight_msg proto)
    {
        WorldManager.ShowTextMsgInPanel(string.Format("消息：{0}", proto.message), Color.cyan);
    }


    static void Toc_fight_halo_list(toc_fight_halo_list proto)
    {
        var prevHaloList = singleton.m_haloList;
        var haloList = proto.halo_infos.ToList();
        singleton.m_haloList = haloList;
        foreach (var halo in haloList)
        {
            if (!singleton.m_haloEffectDic.ContainsKey(halo.halo_id))
            {
                if (halo.actor_id == 0)
                {
                    var pos = halo.pos + new Vector3(0, 0.1f);
                    singleton.CreateHaloEffectOnPos(halo.halo_id, halo.pos, halo.end_time);
                }
                else
                {
                    singleton.CreateHaloEffectOnPlayer(halo.halo_id, halo.actor_id, halo.end_time);
                }
            }
        }
        // remove effect not in the current halo list
        foreach (var haloInfo in prevHaloList)
        {
            var stillExist = false;
            for (int i = 0; i < haloList.Count; i++)
            {
                var haloId = haloList[i].halo_id;
                if (haloId == haloInfo.halo_id)
                {
                    stillExist = true;
                    break;
                }
            }
            if (!stillExist && singleton.m_haloEffectDic.ContainsKey(haloInfo.halo_id))
            {
                var effect = singleton.m_haloEffectDic[haloInfo.halo_id];
                EffectManager.singleton.Reclaim(effect);
                singleton.m_haloEffectDic.Remove(haloInfo.halo_id);
            }
        }
    }


    private void CreateHaloEffectOnPos(int haloId, Vector3 pos, int end_time)
    {
        pos = SceneManager.GetGroundPoint(pos);
        var cfgLine = ConfigManager.GetConfig<ConfigHalo>().GetLine(haloId);
        if (cfgLine != null)
        {
            EffectManager.singleton.GetEffect<Effect>(cfgLine.Effect,
                (effect) =>
                {
                    effect.Attach(pos);
                    m_haloEffectDic[haloId] = effect;
                    effect.Play();
                }
                );
        }
    }

    private void CreateHaloEffectOnPlayer(int haloId, int actor_id, int end_time)
    {
        var cfgLine = ConfigManager.GetConfig<ConfigHalo>().GetLine(haloId);
        var player = GetPlayerById(actor_id);
        if (cfgLine != null && player != null)
        {
            EffectManager.singleton.GetEffect<Effect>(cfgLine.Effect,
                (effect) =>
                {
                    effect.Attach(player.transform);
                    m_haloEffectDic[haloId] = effect;
                    effect.Play();
                }
                );
        }
    }

    public void AddPraise(long id,int num)
    {
        for (int i = 0; i < m_roleList.Length; i++)
        {
            if (m_roleList[i].pid == id)
            {
                m_roleList[i].praised = num;
            }
        }
    }

    public int GetPraise(int id)
    {
        long pid = 0;
        if (WorldManager.singleton.GetPlayerById(id) != null)
        {
            pid = WorldManager.singleton.GetPlayerById(id).serverData.pid;
        }
        for (int i = 0; i < m_roleList.Length; i++)
        {
            if (m_roleList[i].pid == pid)
            {
                return m_roleList[i].praised;
            }
        }
        return 0;
    }

    static void Toc_fight_praise(toc_fight_praise data)
    {
        singleton.AddPraise(data.pid, data.praised);
    }

    static void Toc_fight_enter(toc_fight_enter proto)
    {
        PlayerSystem.last_intbattle_time = TimeUtil.GetNowTimeStamp();
        //Logger.Log("Toc_fight_enter");
        for (int i = 0; i < 3; i++)
        {
            try
            {
                NetLayer.ConnectUDP(proto.server_addr, proto.server_port);
            }
            catch (Exception)
            {
                if (i >= 2)
                {
                    var msg = "";
                    switch (Driver.m_platform)
                    {
                        case EnumPlatform.web:msg = "请刷新游戏(F5)";break;
                        case EnumPlatform.pc:msg = "请重新启动游戏";break;
                        default:msg = "请重新登录";break;
                    }
                    UIManager.ShowTipPanel("无法重连服务器，" + msg, SceneManager.ReturnToLogin, null, false);
                    return;
                }
                continue;
            }
            break;
        }

        //检查包外地图的资源完整性，对缺失的进行上报
        var mapid = proto.map;
        var configMap = ConfigManager.GetConfig<ConfigMapList>().GetLine(mapid);
        if (configMap != null && configMap.PackageOutside)
        {
            var scenePath = "Scenes/" + PathHelper.GetBattleScenePath(mapid);
            var prefabPath = PathHelper.GetScenePrefabPath(mapid);
            var lightMapPath = PathHelper.GetLightMapPath(mapid);

            var lightProbePath = "";
            if (!configMap.LightProbe.IsNullOrEmpty())
                lightProbePath = PathHelper.GetLightProbePath(mapid, configMap.LightProbe);
            var paths = new[] { scenePath, prefabPath, lightMapPath, lightProbePath };

            var check = ResourceManager.CheckOutsideAssetsCrc(paths);
            if (!check)
                Driver.SendLogServer(1205, "进入战斗时包外地图资源缺失:{0}", mapid);
        }

        m_fightEntered = true;
        AutoAim.Enable = proto.auto_aim && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
        s_hasZombieCloth = proto.has_zombie_cloth;
        s_hasZombieBlade = proto.has_zombie_blade;
        singleton.OnEnterGame(proto);
       
        NetLayer.CleanProtoCount();
    }

    static void Toc_fight_game_info(toc_fight_game_info proto)
    {
        singleton.OnGameInfo(proto);
    }

   public delegate void FightExitCallBack();
    static void Toc_player_leave_scene(CDict proto)
    {
        
        if( FightVideoManager.isPlayFight == true )
        {
            FightExitCallBack callBack = ExitS;
            if (FightVideoManager.isCloseBySettingPanel == false)
            {
                UIManager.PopPanel<PanelFightVideoMask>(new object[] { callBack }, true);
                return;
            }
            else
            {
                FightVideoManager.isCloseBySettingPanel = false;
            }
        }
        ExitS();
    }

    static void ExitS()
    {
        m_fightEntered = false;

        SceneManager.singleton.ExitBattleScene();

        singleton.RunBattleModeScript(false);

        PlayerBattleModel.Instance.clear();
    }

    static void Toc_fight_join(toc_fight_join proto)
    {
        OtherPlayer player = singleton.CreatePlayer(proto.role);
        if(proto.role.pid > 0) WorldManager.ShowTextMsgInPanel(string.Format("{0} 加入游戏", player.ColorName), Color.cyan);
        PlayerBattleModel.Instance.AddPlayer(proto.role);
    }

    static void Toc_fight_leave(toc_fight_leave proto)
    {
        singleton.RemovePlayer(proto.val);
        PlayerBattleModel.Instance.RemovePlayer(proto.val);
    }

    
    static void Toc_fight_move(toc_fight_move proto)
    {
        singleton.m_lastRecvUDPTime = Time.timeSinceLevelLoad;
        BasePlayer player = singleton.GetPlayerById(proto.from);

        //if (proto.from > 1)
        //    Logger.Error("-----toc_fight_move: , x:" + proto.pos[0] + ", y: " + proto.pos[1] + ", z: " + proto.pos[2]);

        if (player != null)
        {
            //player.TweenPos(proto);
            PlayerActParam param = new PlayerActParam();
            param.InitWithProto(proto);
            player.Act(param);
        }
    }

    static void Toc_fight_fixpos(toc_fight_fixpos proto)
    {
        if(MainPlayer.singleton != null && MainPlayer.singleton.alive)
        {
            MainPlayer.singleton.SetPosNow(new float[3] { proto.pos.x, proto.pos.y, proto.pos.z });
            UIManager.ShowMask("网络不给力啊~ 努力重连中...", 1.5f);
            Logger.Error("FixPos " + proto.pos);
        }
    }

    /// <summary>
    /// 人物最后伤害信息数组
    /// </summary>
    static p_hit_info_toc[] m_playerLastHitInfos = new p_hit_info_toc[GameConst.GameSceneMaxPlayerCount];
    /// <summary>
    /// 及时武器击中协议（刀、枪等）
    /// </summary>
    /// <param name="proto"></param>
    static void Toc_fight_fire(toc_fight_fire proto)
    {
        if (proto.from == singleton.m_fightId)
        {
            PlayerBattleModel.Instance.bag_selected = true;
            DoProto_fire(proto);
        }
        else
        {
            BasePlayer player = singleton.GetPlayerById(proto.from);
            if (player != null)
            {
                PlayerActParam pap = new PlayerActParam();
                pap.InitWithProto(proto);
                player.Act(pap);  
            }
        }
    }

    internal static void DoProto_fire(toc_fight_fire proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.from);
        if (player == null)
            return;

        if (proto.from != 0 && proto.from != singleton.m_fightId)
        {
            if (player.alive)
                player.Fire2(proto);
        }

        if (player != null && player.curWeapon != null && player.curWeapon is WeaponShotGun)
        {
            int minPlayerId = 100;
            int maxPlayerId = 0;
            foreach (p_hit_info_toc hitInfo in proto.hitList)
            {
                if (hitInfo.life < 0 || hitInfo.actorId >= GameConst.GameSceneMaxPlayerCount)   // 没打到人
                {
                    Logger.Error("Toc_fight_fire hitInfo.actorId:" + hitInfo.actorId + " is larger than " + GameConst.GameSceneMaxPlayerCount + " err!");
                    continue;
                }
                m_playerLastHitInfos[hitInfo.actorId] = hitInfo;
                if (hitInfo.actorId < minPlayerId) minPlayerId = hitInfo.actorId;
                if (hitInfo.actorId > maxPlayerId) maxPlayerId = hitInfo.actorId;
            }

            for(int i = minPlayerId; i <= maxPlayerId; ++i )
            {
                if (m_playerLastHitInfos[i] == null)
                    continue;
                p_hit_info_toc hitInfo = m_playerLastHitInfos[i];
                BasePlayer hitBp = singleton.GetPlayerById(hitInfo.actorId);
                if (hitBp != null)
                    hitBp.Behit(hitInfo, player, proto.hitPos, 0, null);
                m_playerLastHitInfos[i] = null;
            }
        }
        else
        {
            foreach (p_hit_info_toc hitInfo in proto.hitList)
            {
                if (hitInfo.life < 0)   // 没打到人
                    continue;

                BasePlayer hitBp = singleton.GetPlayerById(hitInfo.actorId);
                if (hitBp != null)
                    hitBp.Behit(hitInfo, player, proto.hitPos, 0, null);
            }
        }
    }

    /// <summary>
    /// 非及时武器击中协议（手榴弹等）
    /// </summary>
    /// <param name="proto"></param>
    static void Toc_fight_hit(toc_fight_hit proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.from);

        foreach (p_hit_info_toc hitInfo in proto.hitList)
        {
            if (hitInfo.life < 0)   // 没打到人
                continue;

            BasePlayer hitBp = singleton.GetPlayerById(hitInfo.actorId);
            if (hitBp != null)
            {
                if (proto.atkIdx >= 0)
                    hitBp.Behit(hitInfo, player, hitBp.position, proto.skillId, proto.atkIdx);
                else
                    hitBp.Behit(hitInfo, player, hitBp.position, proto.skillId, null);
            }
                //hitBp.Behit(hitInfo, player, hitBp.position, new List<float> { GameConst.GRENADE_DECELERATE, GameConst.GRANDE_DECELERATE_TIME });
        }
    }

    static void Toc_fight_atk(toc_fight_atk proto)
    {
        int fromId = proto.from;

        BasePlayer player = singleton.GetPlayerById(fromId);
        if (fromId != 0 && player != null && !player.released && !(player is MainPlayer))
        {
            if (proto.index >= 0)
                player.localPlayerData.SetDelayDamageData(proto.index, proto.atkType);
            player.Atk(proto);
        }
    }

    static void Toc_fight_poweroff(toc_fight_poweroff proto)
    {
        int fromId = proto.from;
        if (fromId == 0)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
        {
            //Debug.LogError("玩家" + fromId.ToString() + "结束蓄力");
            player.powerOnEndTime = proto.timeStamp;
        }

        if (fromId != 0 && player != null && !player.released && !(player is MainPlayer))
        {
            if (proto.index >= 0)
                player.localPlayerData.SetDelayDamageData(proto.index, proto.atkType);
            //player.Atk(proto);
            var data = new toc_fight_atk();
            data.atkType = proto.atkType;
            data.fireDir = proto.fireDir;
            data.firePose = proto.firePose;
            data.from = proto.from;
            data.index = proto.index;
            player.Atk(data);
            
        }
    }

    static void Toc_fight_atk_wrong(toc_fight_atk_wrong proto)
    {
        int playerId = proto.from;
        BasePlayer bp = singleton.GetPlayerById(playerId);
        if (bp != null)
        {
            bp.AtkWrong(proto);
        }
    }
     
    static void Toc_fight_reload(toc_fight_reload proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
        {
            PlayerActParam pa = new PlayerActParam();
            pa.InitWithProto(proto);
            player.Act(pa);
        }
    }

    internal static void DoProto_reload(toc_fight_reload proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
        {
            player.ReloadAmmo2(proto);
        }
    }

    static void Toc_fight_jump(toc_fight_jump proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
            player.Jump(proto);
    }

    static void Toc_fight_crouch(toc_fight_crouch proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
            player.Crouch();
    }   

    static void Toc_fight_stand(toc_fight_stand proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
            player.Stand();
    }

    static void Toc_fight_rush_done(toc_fight_rush_done proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
            player.RushDone();
    }

    static void Toc_fight_poweron(toc_fight_poweron proto)
    {
        int fromId = proto.from;
        if (fromId == 0 || fromId == singleton.m_fightId)
            return;
        BasePlayer player = singleton.GetPlayerById(fromId);
        if (player != null)
        {
            //Debug.LogError("玩家" + fromId.ToString() + "开始蓄力");
            player.powerOnStartTime = proto.timeStamp;
            player.PowerOn();
        }
    }

    



    static void Toc_fight_actorstate(toc_fight_actorstate proto)
    {
        foreach (p_actor_state_toc state in proto.list)
        {
            BasePlayer player = singleton.GetPlayerById(state.id);
            if (player == null || player.released)
            {
                //Logger.Warning("Toc_fight_actorstate, Can't get player, id " + state.id);
                continue;
            }
            player.GodTime = state.god_end;
            player.OnData_ActorState(state);
        }

        if (PlayerBattleModel.Instance.battle_info != null)
            PlayerBattleModel.Instance.battle_info.UpdatePlayerState(proto);
    }

    static void Toc_fight_actorprop(toc_fight_actorprop proto)
    {                
        int id = proto.id;
        BasePlayer player = singleton.GetPlayerById(id);
        if(player == null)
        {
            Logger.Error("Toc_fight_actorprop, Can't get player, id " + id);
            return;
        }

        player.OnData_ActorProp(proto);
    }

    static void Toc_fight_equip_info(toc_fight_equip_info proto)
    {
        for (int i = 0; i < proto.list.Length; i++)
        {
            if (proto.list[i].type == GameConst.ITEM_TYPE_ROLE)
            {
                PlayerBattleModel.Instance.fight_role = proto.list[i].itemid;
                break;
            }
        }

        if (MainPlayer.singleton != null)
            MainPlayer.singleton.OnData_Equip_Info(proto);
    }

    static void Toc_fight_fixbullet(toc_fight_fixbullet proto)
    {
        MainPlayer.singleton.OnData_FixBullet(proto);
    }

    //死亡状态
    static void Toc_fight_actor_die(toc_fight_actor_die proto)
    {
        BasePlayer death = singleton.GetPlayerById(proto.actor_id);

        // 死亡协议会推两次udp,tcp各推一次，因为tcp延时太大,所以这里要排除重复的那一次
        if (death == null || death.dieTimeStamp == proto.timeStamp)
        {
            //Logger.Warning("No Handle Die " + proto.actor_id + " " + proto.timeStamp);
            return;
        }
        
        //Logger.Warning("Handle PlayerDie " + proto.actor_id + " curTime=" + death.dieTimeStamp + " protoTime=" + proto.timeStamp.ToString());
        death.dieTimeStamp = proto.timeStamp;

        PlayerBattleModel.Instance.battle_info.PlayerKilled(proto);

        // killer有可能为空，但逻辑要继续执行
        BasePlayer killer = singleton.GetPlayerById(proto.killer);

        if (death is MainPlayer && killer != null && killer is FPWatchPlayer)
        {
            killer = FPWatchPlayer.singleton.relatePlayer;
            FPWatchPlayer.singleton.EndWatch();
            death.AttachSceneCamera();
        }

        float time = 0;
        if (killer != null && killer.isSwitchFPV)
            time = GlobalConfig.gameSetting.TweenFakeJumpSpeed;

        TimerManager.SetTimeOut(time, () =>
        {
            if (killer != null && killer.transform != null)
                death.Die(killer, proto.weapon, proto.part, killer.transform.forward, proto.diePos, proto.killerPos);
            else
                death.Die(null, proto.weapon, proto.part, Vector3.zero, proto.diePos, proto.killerPos);

            GameDispatcher.Dispatch(GameEvent.PLAYER_KILLED, killer, death, proto.part, proto.weapon);
            GameDispatcher.Dispatch(GameEvent.PLAYER_KILLED_EX, killer, death, proto);
        });
    }

    //血量同步
    static void Toc_fight_actor_life(toc_fight_actor_life proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if (player == null)
        {
            return;
        }
        player.SetLife(proto.life);
    }

    static void Toc_fight_actor_god_end(toc_fight_actor_god_end proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if (player == null)
        {
            return;
        }
        player.GodTime = proto.god_end;
    }

    static void Toc_fight_actor_buff_list(toc_fight_actor_buff_list proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if (player == null)
        {
            return;
        }
        if (player.serverData != null && player.serverData.actorState != null)
            player.serverData.actorState.buff_list = proto.buff_list;
        player.SetBuff(proto.buff_list);
    }

    static void Toc_fight_actor_pos(toc_fight_actor_pos proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if (player == null)
        {
            return;
        }
        player.SetPosNow(proto.pos);
    }

    static void Toc_fight_actor_fight_level(toc_fight_actor_fight_level proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if (player == null)
        {
            return;
        }
        player.serverData.fight_level = proto.fight_level;
        if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_BIG_HEAD)
        {
            GameDispatcher.Dispatch<BasePlayer, bool>(GameEvent.PLAYER_LEVEL_UP, player, proto.is_level_up);
        }
    }


    static void Toc_fight_scene_items(toc_fight_scene_items proto)
    {
        SceneManager.singleton.Toc_ShowSceneItems(proto);
    }

    static void Toc_fight_total_scene_items(toc_fight_total_scene_items proto)
    {
        SceneManager.singleton.Toc_TotalSceneItems(proto);
    }

    static void Toc_fight_weapon_motion(toc_fight_weapon_motion proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_weapon_motion, Can't get player, id " + id);
            return;
        }

        PlayerActParam pa = new PlayerActParam();
        pa.InitWithProto(proto);
        player.Act(pa);
    }

    internal static void DoProto_WeaponMotion(toc_fight_weapon_motion proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null || player.released)
            return;

       player.WeaponMotion(proto);
    }

    static void Toc_fight_use_skill(toc_fight_use_skill proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);        
        if (player == null)
        {
            Logger.Error("Toc_fight_use_skill, Can't get player, id " + id);
            return;
        }
        player.UseSkill(proto);
    }

    static void Toc_fight_weapon_awaken_skill(toc_fight_weapon_awaken_skill proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_weapon_awaken_skill, Can't get player, id " + id);
            return;
        }
        player.UseWeaponAwakenSkill(proto);
    }

    static void Toc_fight_change_role(toc_fight_change_role proto)
    {
        int id = proto.id;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_change_role, Can't get player, id " + id);
            return;
        }
        if(UIManager.GetPanel<PanelBattle>().IsOpen())
        {
            UIManager.GetPanel<PanelBattle>().PlayChangeRoleNotice(player, proto.role);
        }
    }

    static void Toc_fight_actor_type(toc_fight_actor_type proto)
    {
        int id = proto.id;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_actor_type, Can't get player, id " + id);
            return;
        }
        player.SetActorType(proto.type);
        GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_ACTOR_TYPE_UPDATE, player);
    }

    static void Toc_fight_ai_skill(toc_fight_ai_skill proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_ai _skill, Can't get player, id " + id);
            return;
        }
        player.DoAISkill(proto);
    }

    static void Toc_fight_ai_action(toc_fight_ai_action proto)
    {
        int id = proto.from;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("Toc_fight_ai _action, Can't get player, id " + id);
            return;
        }
        player.DoAIAction(proto);
    }

    //private int m_testTimerId;
    public void GMClientAISkill(int skillid)
    {
        int id = 2;
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
        {
            Logger.Error("GMClientAISkill, Can't get player, id " + id);
            return;
        }
        if (1 == skillid)
        {
            player.playerAniController.SetBool("move", true);
        }
        else if (2 == skillid)
        {
            player.playerAniController.SetBool("move", false);
        }
        else
        {
            player.playerAniController.SetBool("dirLeftRight", true);
        }
        return;

        int skill;
        float[][] targets;
        if (2 == skillid)
        {
            skill = 2;
            targets = new float[4][];
            targets[0] = new float[3] { 6, 6, 6 };
            targets[1] = new float[3] { 1, 1, 1 };
            targets[2] = new float[3] { 10, 1, 10 };
            targets[3] = new float[3] { -5, 1, -5 };
        }
        else if (4 == skillid)
        {
            skill = 4;
            targets = new float[1][];
            targets[0] = new float[3] { 5, 5, 5 };
        }
        else
        {
            skill = 1;
            targets = new float[1][];
            targets[0] = new float[3] { 1, 1, 1 };
        }
        toc_fight_ai_skill tempToc = new toc_fight_ai_skill();
        tempToc.from = 2;
        tempToc.skill = skill;
        tempToc.targets = targets;
        player.DoAISkill(tempToc);

        //TimerManager.RemoveTimeOut(m_testTimerId);
        //m_testTimerId = TimerManager.SetTimeOut(4f, () =>
        //{
        //    player.DoAISkill(skill, targets);
        //});
    }

    private bool _isGMTesting;
    public bool GMAITest
    {
        set
        {
            _isGMTesting = value;
            if (_isGMTesting)
                GMClientAISkill(1);
        }
        get { return _isGMTesting; }
    }

    static void Toc_fight_actor_immune(toc_fight_actor_immune proto)
    {
        BasePlayer player = singleton.GetPlayerById(proto.id);
        if(player != null && !player.released)
        {
            player.serverData.immune = proto.immune;
            Logger.Warning("Toc_fight_actor_immune = " + proto.immune);
        }
    }

    /// <summary>
    /// 设置面板
    /// </summary>
    static void Toc_player_options(toc_player_options data)
    {

        string key = "";
        for( int i = 0; i < data.options.Length; i++ )
        {
            key = data.options[i].option_type;
            if( m_ParamSettingStatusDic.ContainsKey(key))
            {
                m_ParamSettingStatusDic[key] = data.options[i].option_val;
            }
            else
            {
                m_ParamSettingStatusDic.Add(key, data.options[i].option_val);
            }
            
        }
    }

    static void Toc_fight_game_start_time(toc_fight_game_start_time proto)
    {
        Logger.Log("Toc_fight_game_start_time  lefttime=" + (proto.time - TimeUtil.GetNowTimeStamp()));
        if (proto.time > TimeUtil.GetNowTimeStamp())
        {
            singleton.gameStartTime = proto.time;

            if (BattleNoticeManager.Instance != null)
                BattleNoticeManager.Instance.PlayGameStartNotice();
        }
    }

    static void Toc_fight_wait_time(toc_fight_wait_time proto)
    {
        Logger.Log("Toc_fight_wait_time  lefttime=" + proto.time);
        if (proto.time > 0)
        {
            singleton.gameStartTime = proto.time + TimeUtil.GetNowTimeStamp();

            if (BattleNoticeManager.Instance != null)
                BattleNoticeManager.Instance.PlayGameStartNotice();
        }
    }

    static void Toc_fight_role_select_list(toc_fight_role_select_list proto)
    {
        PanelChooseHero.heroIDArray = proto.list;
        if (!UIManager.IsOpen<PanelChooseHero>())
        {
            UIManager.PopPanel<PanelChooseHero>();
        }
    }


    #endregion

    /// <summary>
    /// 根据设置类型获取设置状态
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public int GetParamSettingStatus(string key)
    {
        var enumerator = m_ParamSettingStatusDic.GetEnumerator();
        while( enumerator.MoveNext() )
        {
            if( enumerator.Current.Key == key )
            {
                
                return enumerator.Current.Value;
            }
        }
        return 0;
    }

    private void OnEnterGame(toc_fight_enter data)
    {
        // 服务端有bug,进入新战斗时，可能还未推出上次战斗，导致客户端上次资源未释放
        SceneManager.singleton.ReleaseBattleSceneRes();
        ResourceManager.Clear(EResType.Battle, true);
        PlayerBattleModel.Instance.Release();
        BasePlayer.ClearDieStamp();

        VertificateUtil.Reset();
        UDPping.Instance.Init();

        if(Util.NeedClearAll())
        {
            ResourceManager.Clear(ResTag.Forever, true, true);
            Logger.Warning("卸载所有资源 OnEnterGame");
        }

        Logger.Warning("EnterGame RamInfo = " + Util.GetMemoryInfo());

        UIManager.DestroyAllPanel();
        ResourceManager.GarbageCollect();

       
        m_fightId = data.myid;
        m_room_handle = data.rhd;
        m_rand_id = data.rid;
        
        m_gameRule = data.rule;
        m_fightType = data.type;

        Logger.Warning("OnEnterGame, m_fightType: " + m_fightType + " m_gameRule " + m_gameRule);
        
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        m_gameRuleLine = configGameRule.GetLine(m_gameRule);
        m_mapRuleLine = ConfigManager.GetConfig<ConfigMapRule>().GetRule(data.map, gameRule);
        SceneManager.singleton.curMapId = data.map;
        if (FightVideoManager.isPlayFight == true)
        {
            RoomModel.ChannelType = ChannelTypeEnum.pvp;
        }
        var cfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        m_isPve = cfg == null ? false : cfg.IsPve;
        m_isZombieMode = gameRuleLine.CheckGameTags(GameConst.ZOMBIE_MODE);

        //ConfigMisc.GetInt("net_reconect_" + m_fightType.Substring(0, m_fightType.IndexOf("#")));

        SendUdp(new tos_fight_spectate() { player_id = PlayerSystem.roleId });
        AudioManager.StopBgMusic();
        UIManager.ShowLoadingWithProBar(GetPreloadRes(), () => SceneManager.singleton.LoadBattleScene());
        if (gameRule == "boss4newer")
        {
            PanelHallBattle.m_imgNewHandWorldBoss.TrySetActive(false);
            PanelHallBattle.m_newHandWorldBossEffect.Hide();
            GlobalConfig.clientValue.newHandWordBoss_enter = true;
            GlobalConfig.SaveClientValue("newHandWordBoss_enter");
        }
    }

    private void OnGameInfo(toc_fight_game_info data)
    {
        Logger.Warning("OnGameInfo Start");

        m_startTime = Time.realtimeSinceStartup - data.now / 1000f;
        m_roleList = data.list;
        
        Logger.LogCostTime(InitMainPlayer);
        Logger.LogCostTime(InitOtherPlayer);
        Logger.LogCostTime(InitWatchPlayer);
        RunBattleModeScript(true);

        PlayerBattleModel.Instance.initData(data);

        m_lastRecvUDPTime = 0;
        TimerManager.SetTimeOut(2, CheckWebUDP);
    }

    private void CheckWebUDP()
    {
        if (Driver.m_platform == EnumPlatform.web)
        {
            if (m_lastRecvUDPTime == 0)
            {
                NetLayer.ReconnectUDP();
                TimerManager.SetTimeOut(2, CheckWebUDP);
            }
        }
    }

    private List<ResourceItem> GetPreloadRes()
    {
        var preloads = new List<ResourceItem>();
        preloads.AddRange(new PanelBattle().GetPreloadRes());
        preloads.Add(new ResourceItem(PathHelper.GetScenePrefabPath(SceneManager.singleton.curMapId), EResType.Battle, 0, true));
        preloads.Add(new ResourceItem(PathHelper.GetLightMapPath(SceneManager.singleton.curMapId), EResType.Battle, 0, false));
        if (RoomModel.IsGuideStage)
        {
            if (Driver.isMobilePlatform)
                preloads.AddRange(new PanelGuide().GetPreloadRes());
            else
                preloads.AddRange(new PanelGuidePC().GetPreloadRes());
        }

        if (CheckGameTags(GameConst.GAME_RULE_KING_SOLO, GameConst.GAME_RULE_KING_CAMP))
            preloads.Add(new ResourceItem("Atlas/GunKing", EResType.Atlas));
        if (isHideModeOpen)
            preloads.Add(new ResourceItem("Effect/" + EffectConst.FIGHT_HIDE_ITEM_DIE, EResType.Battle));
        if (isDotaModeOpen)
        {
            preloads.Add(new ResourceItem("Effect/" + EffectConst.FIGHT_SURVIVAL_DIE, EResType.Battle));
            preloads.Add(new ResourceItem("Effect/" + EffectConst.UI_FIGHT_YUJING_ALERT, EResType.Battle));
            preloads.Add(new ResourceItem("Atlas/Survival", EResType.Atlas));
            preloads.Add(new ResourceItem("UI/Battle/PanelBattleDotaScore", EResType.UI));
            //preloads.Add(new ResourceItem("UI/Battle/PanelBattleSurvival", EResType.UI));
            preloads.Add(new ResourceItem("Atlas/BigHead", EResType.Atlas));
            preloads.Add(new ResourceItem("UI/Battle/PanelBattleBigHead", EResType.UI));
        }
        if (isSurvivalTypeModeOpen)
        {
            preloads.Add(new ResourceItem("Effect/" + EffectConst.FIGHT_SURVIVAL_DIE, EResType.Battle));
            preloads.Add(new ResourceItem("Effect/" + EffectConst.UI_FIGHT_YUJING_ALERT, EResType.Battle));
            preloads.Add(new ResourceItem("Atlas/Survival", EResType.Atlas));
            preloads.Add(new ResourceItem("UI/Battle/PanelBattleSurvivalScore", EResType.UI));
            preloads.Add(new ResourceItem("UI/Battle/PanelBattleSurvival", EResType.UI));
        }
        if (isZombieTypeModeOpen)
        {
            preloads.Add(new ResourceItem("Atlas/Role", EResType.Atlas));
            preloads.Add(new ResourceItem("UI/Zombie/PanelZombieSelect", EResType.UI));
        }
        if (isBigHeadModeOpen)
        {
            preloads.Add(new ResourceItem("Atlas/BigHead", EResType.Atlas));
            preloads.Add(new ResourceItem("UI/Battle/PanelBattleBigHead", EResType.UI));
        }

        //战斗开始特效音效
        preloads.Add(new ResourceItem("Effect/" + EffectConst.UI_FIGHT_GAME_BEGIN, EResType.UIEffect));
        preloads.Add(new ResourceItem("Sounds/2D/" + AudioConst.battleStart, EResType.Audio));

        //预加载前3个背包的所有武器
        var bags = ItemDataManager.m_bags;
        for (int i = 0; i < 3 && i < bags.Length; i++)
        {
            var configWeapon = ItemDataManager.GetItemByUid(bags[i].equip_list.WEAPON1) as ConfigItemWeaponLine;
            if (configWeapon != null)
                preloads.Add(new ResourceItem("Weapons/" + configWeapon.MPModel, EResType.Battle));
            configWeapon = ItemDataManager.GetItemByUid(bags[i].equip_list.WEAPON2) as ConfigItemWeaponLine;
            if (configWeapon != null)
                preloads.Add(new ResourceItem("Weapons/" + configWeapon.MPModel, EResType.Battle));
            configWeapon = ItemDataManager.GetItemByUid(bags[i].equip_list.DAGGER) as ConfigItemWeaponLine;
            if (configWeapon != null)
            {
                preloads.Add(new ResourceItem("Weapons/" + configWeapon.MPModel, EResType.Battle));
                if (!string.IsNullOrEmpty(configWeapon.MPModel2))
                    preloads.Add(new ResourceItem("Weapons/" + configWeapon.MPModel2, EResType.Battle));
            }
        }

        // 预加载主角
        if (MainPlayer.singleton != null)
        {
            string mpPrefabPath = MainPlayer.singleton.GetMPPrefabPath();
            if (mpPrefabPath.IsNullOrEmpty() == false)
                preloads.Add(new ResourceItem(mpPrefabPath, EResType.Battle));
            //Logger.Warning("预加载主角 " + mpPrefabPath);

            //如果主角默认手臂
            string mpDefaultPrefabPath = MainPlayer.singleton.GetMPDefaultPrefabPath();
            if (mpPrefabPath != mpDefaultPrefabPath)
            {
                if (mpDefaultPrefabPath.IsNullOrEmpty() == false)
                    preloads.Add(new ResourceItem(mpDefaultPrefabPath, EResType.Battle));
                //Logger.Warning("预加载主角默认手臂 " + mpDefaultPrefabPath);
            }

            // 预加载生化模式模型
            if (WorldManager.singleton.isZombieTypeModeOpen)
            {
                //母体
                string mpDefaultMatrixZombiePath = MainPlayer.singleton.GetMPDefaultMatrixZombiePath();
                if (mpDefaultMatrixZombiePath.IsNullOrEmpty() == false)
                    preloads.Add(new ResourceItem(mpDefaultMatrixZombiePath, EResType.Battle));
                //精英
                string mpDefaultEliteZombiePath = MainPlayer.singleton.GetMPDefaultEliteZombiePath();
                if (mpDefaultEliteZombiePath.IsNullOrEmpty() == false)
                    preloads.Add(new ResourceItem(mpDefaultEliteZombiePath, EResType.Battle));

                //终极母体
                //if(WorldManager.singleton.isZombieUltimateModeOpen)
                //{
                //    string mpDefaultUltimateZombiePath = MainPlayer.singleton.GetMPDefaultUltimateZombiePath();
                //    if (mpDefaultUltimateZombiePath.IsNullOrEmpty() == false)
                //        preloads.Add(new ResourceItem(mpDefaultUltimateZombiePath, EResType.Battle));
                //}

            }

            // 预加载主角的第三人称
            if (RoomModel.IsGuideStage == false)
            {
                string mpopPrefabPath = MainPlayer.singleton.GetMPOPPrefabPath();
                if (mpopPrefabPath.IsNullOrEmpty() == false)
                    preloads.Add(new ResourceItem(mpopPrefabPath, EResType.Battle));
                //Logger.Warning("预加载主角第三人称 " + mpopPrefabPath);
            }
        }

        //根据gamerule中配置进行预加载
        if (m_gameRuleLine != null && m_gameRuleLine.PreloadRes.Length > 0)
        {
            for (int i = 0; i < m_gameRuleLine.PreloadRes.Length; i++)
            {
                preloads.Add(new ResourceItem(m_gameRuleLine.PreloadRes[i], EResType.Battle));
            }
        }

        return preloads;
    } 

    public void InitMainPlayer()
    {
        bool hasPlayer = true;
        p_fight_role roleData = null;
        foreach (p_fight_role role in m_roleList)
        {
            if (role.id == m_fightId)
            {
                roleData = role;
                break;
            }
        }
        if (roleData == null)
        {
            Logger.Log("战斗数据中无主角数据");
            hasPlayer = false;
        }

        m_kCameraSwitcher = new CameraSwitcher(hasPlayer);

        m_bIsViewBattleModel = !hasPlayer;
        if (m_bIsViewBattleModel)
        {
            return;
        }

        MainPlayer mp = new MainPlayer();

        mp.serverData.roleData = roleData;
        mp.GodTime = roleData.god_end;
        mp.OnData_ActorData(roleData);
        mp.SetState(roleData.state, true);
        m_allPlayer[roleData.id] = MainPlayer.singleton;
    }

    public void InitOtherPlayer()
    {
        if (m_roleList == null)
            return;

        foreach (p_fight_role role in m_roleList)
        {
            if (role.id != m_fightId)
                CreatePlayer(role);
        }
    }

    public void InitWatchPlayer()
    {
        new FPWatchPlayer();
        new FreeViewPlayer();
    }

    private OtherPlayer CreatePlayer(p_fight_role roleData)
    {
        OtherPlayer player = PlayerManager.NewObj<OtherPlayer>();

        player.serverData.roleData = roleData;
        player.GodTime = roleData.god_end;

        player.OnData_ActorData(roleData);
        player.SetState(roleData.state, true);

        m_allPlayer[roleData.id] = player;

        //Logger.Error("创建角色 id: " + roleData.id + ", 名：" + roleData.name + ", pid: " + roleData.pid + ", camp: " + roleData.camp + ", weaponId: " + roleData.prop.WeaponId);

        return player;
    }
    
    private BasePlayer RemovePlayer(int id)
    {
        BasePlayer player = singleton.GetPlayerById(id);
        if (player == null)
            return null;

        if (player is FPWatchPlayer)
        {
            player = FPWatchPlayer.singleton.Leave();
        }

        m_allPlayer.Remove(id);

        GameDispatcher.Dispatch<BasePlayer>(GameEvent.PLAYER_LEAVE, player);


        if (isSurvivalTypeModeOpen && player.isAI)
        {
            if (player.isAlive)
            {
                player.Die(null, player.serverData.curWeapon);
                player.SetState(GameConst.SERVER_ACTOR_STATUS_DEAD, true);
            }

            if (player.configData != null && string.IsNullOrEmpty(player.configData.DieEffect))
            {
                //对生存模式的BOSS进行延迟回收
                TimerManager.SetTimeOut(player.deadDisappearTime, () =>
                {
                    player.Release(false);
                    PlayerManager.CacheObj(player);
                });
            }
            else
            {
                player.Release(false);
                PlayerManager.CacheObj(player);
            }
        }
        else
        {
            player.Release(false);
            PlayerManager.CacheObj(player);
        }
        
        return player;
    }

    public void RegisterPlayerGOHash(int iHashID, BasePlayer kBP)
    {
        if (m_dicGOHashIDPlayer.ContainsKey(iHashID))
        {
            Logger.Warning("RegisterPlayerGOHashID, iHashID is existed " + iHashID);
            return;
        }

        m_dicGOHashIDPlayer.Add(iHashID, kBP);
    }

    public void UnRegisterPlayerGOHash(int iHashID)
    {
        if (m_dicGOHashIDPlayer.ContainsKey(iHashID) == false)
        {
            return;
        }

        //Logger.Log("UnRegisterPlayerGOHash " + iHashID);

        m_dicGOHashIDPlayer.Remove(iHashID);
    }

    public BasePlayer GetPlayerWithGOHash(int iHashID)
    {
        BasePlayer kBP = null;
        m_dicGOHashIDPlayer.TryGetValue(iHashID, out kBP);

        //Logger.Log("GetPlayerWithGOHash iHashID");

        return kBP;
    }

    //public void ShowInfo(bool show)
    //{
    //    //m_infoPanel.TrySetActive(show);
    //    if (show)
    //    {
    //        UpdateInfoPanel();
    //    }
    //}

    public void UpdatePlayer()
    {
        VisibleSkinnedMeshNum = 0;
        foreach (var kv in m_allPlayer)
        {
            if (kv.Value.released == false)
                kv.Value.Update();

            if (kv.Value.isRendererVisible)
                VisibleSkinnedMeshNum++;
        }
    }

    public int GetOtherPlayerLayer(int iCamp)
    {
        if (MainPlayer.singleton == null)
        {
            //Logger.Log("WorldManager.GetOtherPlayerLayer MainPlayer.singleton is null");
            return 0;
        }

        PlayerServerData kSD = MainPlayer.singleton.serverData;
        if (kSD.camp == 0)
        {
            return GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY;
        }
        else
        {
            int iOtherPlayerLayer = iCamp == kSD.camp ? GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND : GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY;
            return iOtherPlayerLayer;
        }
    }

    public static void ShowTextMsgInPanel(string text, Color textColor, int fontSize = 18)
    {
        GameDispatcher.Dispatch(GameEvent.UI_TIP_INFO, text, textColor, fontSize);
    }

    public void SetPlayerById(int id, BasePlayer player)
    {
        singleton.m_allPlayer[id] = player;
    }

    public BasePlayer GetPlayerById( int id )
    {
        BasePlayer player = null;

        singleton.m_allPlayer.TryGetValue(id, out player);

        return player;
                
    }

    public BasePlayer GetPlayerByPid(long pid)
    {
        foreach (BasePlayer player in singleton.allPlayer.Values)
        {
            if (player != null && player.serverData != null && player.serverData.pid == pid)
            {
                return player;
            }
        }
        return null;
    }

    public void EnableRoleBigCollider(bool value, bool enemyOnly = true)
    {
        foreach (KeyValuePair<int, BasePlayer> kvp in m_allPlayer)
        {
            OtherPlayer op = kvp.Value as OtherPlayer;
            if (op != null && op.alive )
            {
                op.EnableCC(value);
            }
        }
    }

    public void EnableRoleBodyTrigger(bool value, bool enemyOnly = true)
    {
        foreach (KeyValuePair<int, BasePlayer> kvp in m_allPlayer)
        {
            OtherPlayer op = kvp.Value as OtherPlayer;
            if (op != null && op.alive)
            {
                op.EnableBodyPartTrigger(value, false);
            }
        }
    }

    public void EnableRoleBodyPartCollider(bool value, bool enemyOnly = true)
    {
        
        foreach(KeyValuePair<int, BasePlayer> kvp in m_allPlayer)
        {
            OtherPlayer op = kvp.Value as OtherPlayer;
            if (op != null && op.alive && (isBigHeadModeOpen || isDotaModeOpen || op.isEnemy ||
                MainPlayer.singleton.curWeapon.IsHeal))// 安娜的治愈武器要能击中友方和敌方
            {
                op.EnableBodyPartCollider(value, false);
            }
        }
    }

   

    public void DisanbleTeamMateCollider(bool on)
    {
        foreach (KeyValuePair<int, BasePlayer> kvp in m_allPlayer)
        {
            OtherPlayer op = kvp.Value as OtherPlayer;
            
            if (op!=null&&!op.isEnemy)
            {
                op.TurnColliderTrigger(!on);
            }
        }
    }



    /// 获取同阵营的队友 的id列表
    public List<int> GetCampTeams( int camp, PlayerType type = PlayerType.All )
    {
        List<int> teamList = new List<int>();
        foreach( BasePlayer teamPlayer in m_allPlayer.Values )
        {
            switch(type)
            {
                case PlayerType.All:
                    teamList.Add(teamPlayer.PlayerId);
                    break;
                case PlayerType.AllNoAI:
                    if (!teamPlayer.isAI)
                        teamList.Add(teamPlayer.PlayerId);
                    break;
                case PlayerType.AllNoAIButBase:
                    if (!teamPlayer.isAI || (teamPlayer.configData != null && teamPlayer.configData.SubType == GameConst.PLAYER_SUBTYPE_BASE))
                        teamList.Add(teamPlayer.PlayerId);
                    break;
                case PlayerType.CampAll:
                    if (teamPlayer.serverData != null && teamPlayer.Camp == camp)
                        teamList.Add(teamPlayer.PlayerId);
                    break;
                case PlayerType.CampNoAI:
                    if (teamPlayer.serverData != null && teamPlayer.Camp == camp && !teamPlayer.isAI)
                        teamList.Add(teamPlayer.PlayerId);
                    break;
                case PlayerType.CampNoAIButBase:
                    if (teamPlayer.serverData != null && teamPlayer.Camp == camp && (!teamPlayer.isAI || (teamPlayer.configData != null && teamPlayer.configData.SubType == GameConst.PLAYER_SUBTYPE_BASE)))
                        teamList.Add(teamPlayer.PlayerId);
                    break;
            }
        }
        return teamList;
    }

    public List<int> GetHideTeams(BasePlayer player)
    {
        List<int> teamList = new List<int>();
        if (MainPlayer.singleton.isAlive)
        {
            foreach (BasePlayer teamPlayer in m_allPlayer.Values)
            {
                teamList.Add(teamPlayer.PlayerId);
            }
        }
        else
        {
            foreach (BasePlayer teamPlayer in m_allPlayer.Values)
            {
                if (teamPlayer.Camp == 1)
                {
                    teamList.Add(teamPlayer.PlayerId);
                }
            }
        }
        return teamList;
    }

    public List<int> GetHumanTeams(BasePlayer player)
    {
        List<int> teamlist = new List<int>();
        foreach (BasePlayer teamPlayer in m_allPlayer.Values)
        {
            if (teamPlayer.Camp == 1)
            {
                teamlist.Add(teamPlayer.PlayerId);
            }
        }
        return teamlist;
    }
    
    internal static void SendUdp(tos_base proto)
    {
        NetLayer.SendUdp(singleton.m_room_handle, singleton.m_rand_id, proto);
    }

    public void RunBattleModeScript(bool run)
    {
        Transform tranDriver = Driver.singleton.transform;
        Transform tran = tranDriver.FindChild("BattleModeScript");
        
        if (!run)
        {
            if (tran != null)
                GameObject.Destroy(tran.gameObject);
            if (BattleNoticeManager.Instance != null)
                BattleNoticeManager.Instance.Release();
            return;
        }

        GameObject go = null;
        if (tran == null)
        {
            go = new GameObject("BattleModeScript");
            go.transform.SetParent(tranDriver);
        }
        else
            go = tran.gameObject;

        if (singleton.isDropGunModeOpen)
        {
            go.AddMissingComponent<DropGunBehavior>();
        }

        //是否显示伤害
        if (singleton.isShowDamge)
        {
            DamageTipsManager dtm = go.AddMissingComponent<DamageTipsManager>();
            dtm.damgeShowType = m_gameRuleLine.DamgeShowType;
        }

        if (singleton.isDotaModeOpen)
        {
            go.AddMissingComponent<DotaBehavior>();
            //go.AddMissingComponent<DotaBigHeadBehavior>();
        }

        if (singleton.isSurvivalModeOpen)
        {
            go.AddMissingComponent<SurvivalEradicateBehavior>();
        }

        if (singleton.isDefendModeOpen)
        {
            go.AddMissingComponent<SurvivalDefendBehavior>();
        }

        if (singleton.isWorldBossModeOpen)
        {
            go.AddMissingComponent<SurvivalBehavior>();
        }

        if (singleton.isRescueModeOpen)
        {
            go.AddMissingComponent<RescueBehavior>();
        }

        if (singleton.isExplodeModeOpen)
        {
            go.AddMissingComponent<BurstBehavior>();
        }

        if (singleton.isGhostModeOpen)
        {
            go.AddMissingComponent<GhostModeBehavior>();
        }

        if (singleton.isZombieModeOpen)
        {
            go.AddMissingComponent<ZombieBehaviour>();
        }

        if (singleton.isZombieHeroModeOpen)
        {
            go.AddMissingComponent<ZombieHeroBehaviour>();
        }

        if (singleton.isZombieUltimateModeOpen)
        {
            go.AddMissingComponent<ZombieUltimateBehaviour>();
        }

        if (singleton.isHideModeOpen)
        {
            go.AddMissingComponent<HideBehavior>();
        }

        if (singleton.isFightInTurnModeOpen)
        {
            go.AddMissingComponent<FightInTurnBehavior>();
        }

        if(singleton.isBigHeadModeOpen)
        {
            go.AddMissingComponent<BigHeadBehavior>();
        }

        if(singleton.isTeamHeroModeOpen)
        {
            go.AddMissingComponent<TeamHeroBehaviour>();
        }

        if(singleton.isChallengeModeOpen)
        {
            go.AddMissingComponent<SurvivalChallengeBehavior>();
        }
        if( singleton.isDaggerOW)
        {
            go.AddMissingComponent<DaggerPointBehavior>();
        }
        go.AddMissingComponent<StoryManager>();
        go.AddMissingComponent<TargetGuideManager>();
        go.AddMissingComponent<BattleSpectatorManager>();

        new BattleNoticeManager();
    }
#region Get Set
    public int myid
    {
        get
        {
            return m_fightId;
        }
    }

    public Dictionary<int, BasePlayer> allPlayer
    {
        get { return m_allPlayer; }
    }

    public float StartTime { get { return m_startTime; } }

    /// <summary>
    /// 【注意】不要使用该值来判断当前战斗模式，应该使用CheckGameTags方法，只能用来取对应的配置项
    /// </summary>
    public string gameRule
    {
        get { return m_gameRule; }
    }

    public ConfigGameRuleLine gameRuleLine
    {
        get { return m_gameRuleLine; }
    }

    public ConfigMapRuleLine mapRuleLine
    {
        get { return m_mapRuleLine; }
    }

    /// <summary>
    /// 检查当前游戏模式是否是指定模式中的任意一个
    /// </summary>
    /// <param name="checkTags"></param>
    /// <returns></returns>
    public bool CheckGameTags(params string[] checkTags)
    {
        var result = false;
        if (m_gameRuleLine == null)
            return false;
        var curTags = m_gameRuleLine.Tags;
        for (int i = 0; i < curTags.Length; i++)
        {
            for (int j = 0; j < checkTags.Length; j++)
            {
                if (curTags[i] == checkTags[j])
                {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /// <summary>
    /// 战斗类型 state:pve room:pvp
    /// </summary>
    public string fightType { get { return m_fightType; } }

    public int fightId { get { return m_fightId; } }

    public bool fightEntered
    {
        get { return m_fightEntered; }
    }

    public CameraSwitcher CameraSwitcher
    {
        get { return m_kCameraSwitcher; }
    }

    public float RoundBeginWaitTime
    {
        get { return m_gameRuleLine != null ? m_gameRuleLine.Time_begin : 0; }
    }

    public bool isViewBattleModel
    {
        get { return m_bIsViewBattleModel; }
    }

    public bool isRescueModeOpen
    {
        get { return m_gameRuleLine != null ? m_gameRuleLine.MaxRescueTimes > 0 : false; }
    }

    public string[] backgroundMusics
    {
        get { return m_gameRuleLine != null ? m_gameRuleLine.Music : null; }
    }

    public bool isGrenadeModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_GRENADE; }
    }

    public bool isGrenadeInfinite
    {
        get { return gameRule == GameConst.GAME_RULE_GRENADE || gameRule == GameConst.GAME_RULE_KING_SOLO || gameRule == GameConst.GAME_RULE_KING_CAMP; }
    }

    public bool isExplodeModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_BURST || gameRule == GameConst.GAME_RULE_EXPLODE_REVIVE || gameRule == GameConst.GAME_RULE_GHOST; }
    }

    public bool isGhostModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_GHOST; }
    }

    /// <summary>
    /// 是否生化类模式
    /// </summary>
    public bool isZombieTypeModeOpen
    {
        get { return isZombieModeOpen || isZombieHeroModeOpen || isZombieUltimateModeOpen; }
    }

    public bool isZombieModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_ZOMBIE; }
    }

    public bool isZombieHeroModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_ZOMBIE_HERO; }
    }

    public bool isZombieUltimateModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_ZOMBIE_ULTIMATE; }
    }

    public bool isSurvivalTypeModeOpen
    {
        get { return isSurvivalModeOpen || isDefendModeOpen || isWorldBossModeOpen || isChallengeModeOpen; }
    }

    public bool isSurvivalModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_ERADICATE || gameRule == GameConst.GAME_RULE_ERADICATE2 || gameRule == GameConst.GAME_RULE_SURVIVAL; }
    }

    public bool isDefendModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_DEFEND || gameRule == GameConst.GAME_RULE_DEFEND0 || gameRule == GameConst.GAME_RULE_DEFEND2 || gameRule == GameConst.GAME_RULE_SUR_SNIPE; }
    }

    public bool isWorldBossModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_WORLD_BOSS || gameRule == GameConst.GAME_RULE_BOSS_NEWER; }
    }

    public bool isChallengeModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_CHALLENGE; }
    }

    public bool isHideModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_HIDE; }
    }

    public bool isFightInTurnModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_FIGHT_IN_TURN; }
    }

    public bool isDropGunModeOpen
    {
        get { return m_gameRuleLine != null && m_gameRuleLine.IsDropGunOpen == 1; }
    }

    public bool isBigHeadModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_BIG_HEAD; }
    }

    public bool isTeamHeroModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_TEAM_HEAO; }
    }

    /// <summary>
    /// 遗迹守护模式
    /// </summary>
    public bool isDotaModeOpen
    {
        get { return gameRule == GameConst.GAME_RULE_DOTA; }
    }

    public bool isShowDamge
    {
        get { return m_gameRuleLine != null && m_gameRuleLine.DamgeShowType != 0; }
    }

    public bool isDaggerOW
    {
        get { return gameRule == GameConst.GAME_RULE_DAGGER_OW; }
    }

    /// <summary>
    /// 玩家视角的模型是否已经加载出来
    /// </summary>
    public bool isMyViewerLoaded
    {
        get
        {
            BasePlayer curPlayer = null;
            if (isViewBattleModel)
            {
                curPlayer = GetPlayerById(ChangeTeamView.watchedPlayerID);
            }
            else if (MainPlayer.singleton != null && MainPlayer.singleton.isDead)
            {
                curPlayer = GetPlayerById(ChangeTeamView.watchedPlayerID);
            }
            else
                curPlayer = MainPlayer.singleton;

            return curPlayer != null && curPlayer.gameObject != null;
        }
    }

    public BasePlayer curPlayer
    {
        get
        {
            BasePlayer curPlayer=null;
            if (!isViewBattleModel && (MainPlayer.singleton != null && MainPlayer.singleton.attachedSceneCamera))
                curPlayer = MainPlayer.singleton;
            else
                curPlayer = GetPlayerById(ChangeTeamView.watchedPlayerID);
            return curPlayer;
        }
    }

    public bool isPve
    {
        get{ return m_isPve; }
    }

    public bool isZombieMode
    {
        get { return m_isZombieMode; }
    }
#endregion
}
