﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class ScreenEffectManager 
{
    public const string EFFECT_FLASH = "flash";

    static public ScreenEffectManager singleton;

    private Dictionary<string, ScreenEffectBase> m_dicCache = new Dictionary<string,ScreenEffectBase>();

    private Transform m_screenEftRoot;
    private Image m_imgBg;
    private Image m_imgPic;
    private ScreenEffectBase m_curEffect;

   
    public ScreenEffectManager()
    {
        singleton = this;
    }

    public void Init(Transform eftRoot)
    {
        m_screenEftRoot = eftRoot;

        Transform transBg = eftRoot.Find("Background");
        if(transBg != null)
        {
            transBg.gameObject.TrySetActive(true);
            m_imgBg = transBg.gameObject.GetComponent<Image>();
        }

        Transform transPic = eftRoot.Find("Picture");
        if(transPic != null)
        {
            transPic.gameObject.TrySetActive(true);
            m_imgPic = transPic.gameObject.GetComponent<Image>();
        }
    }

    public void Release()
    {
        singleton = null;
        m_imgBg = null;
        m_curEffect = null;

        foreach (KeyValuePair<string, ScreenEffectBase> kvp in m_dicCache)
        {
            kvp.Value.Release();
        }
        m_dicCache.Clear();
    }

    internal ScreenEffectBase GetEffect(string type)
    {
        ScreenEffectBase effectBase;
        m_dicCache.TryGetValue(type, out effectBase);
        if(effectBase != null)
            return effectBase;

        if(effectBase == null)
        {
            switch(type)
            {
                case EFFECT_FLASH:
                    effectBase = new ScreenEffect_Flash();
                    effectBase.Init(this);
                    break;
            }

            if (effectBase != null)
                m_dicCache.Add(type, effectBase);
        }

        return effectBase;
    }

    public void HideAllEffect()
    {
        if (m_curEffect != null)
        {
            m_curEffect.Hide();
            m_curEffect = null;
        }
    }

    internal void SetCurEffect(ScreenEffectBase effect)
    {
        m_curEffect = effect;
    }

    public void Update()
    {
        if (m_curEffect != null)
            m_curEffect.Update();
    }

    public Image background
    {
        get { return m_imgBg; }
    }

    public Image picture
    {
        get { return m_imgPic; }
    }
}

internal class ScreenEffectBase
{
    protected ScreenEffectManager m_seMgr;
    protected Image m_bg;
    protected Image m_pic;
    protected bool m_show;


    public void Init(ScreenEffectManager seMgr)
    {
        m_seMgr = seMgr;
        m_bg = seMgr.background;
        m_pic = seMgr.picture;
    }

    virtual public void Show(params System.Object[] arg)
    {
        m_seMgr.HideAllEffect();

        m_show = true;
        if (m_bg!= null)
            m_bg.enabled = true;
        if (m_pic != null)
            m_pic.enabled = true;

        m_seMgr.SetCurEffect(this);
    }

    virtual public void Hide()
    {
        m_show = false;

        if (m_bg != null)
            m_bg.enabled = false;
        if (m_pic != null)
            m_pic.enabled = false;
    }

    virtual protected void FadeOut(float time)
    {

    }

    virtual public void Release()
    {
        m_bg = null;
        m_pic = null;
    }

    virtual public void Update()
    {

    }
}

internal class ScreenEffect_Flash : ScreenEffectBase
{
    private float m_lifeTick;
    private bool m_fading;
    private float m_fadeSpeed;

    private Color m_color;
    

    public override void Show(params System.Object[] arg)
    {
        base.Show(arg);

        if (m_bg == null || m_pic == null)
            return;

        m_fading = false;
        m_lifeTick = (float)arg[0];

        m_bg.SetSprite(null);
        m_bg.color = Color.white;

        m_pic.color = Color.white;

        string picName = (string)arg[1];
        if(m_pic.sprite == null || m_pic.sprite.name != picName)
        {
            m_pic.SetSprite(null);
            if (picName.IsNullOrEmpty() == false)
            {
                ResourceManager.LoadSprite(AtlasName.Split, picName, (sp) =>
                {
                    if (m_pic != null)
                    {
                        m_pic.SetSprite(sp);
                        m_pic.SetNativeSize();
                    }
                });
            }
        }
    }

    public override void Hide()
    {
        base.Hide();
        m_fading = false;
    }

    protected override void FadeOut(float time)
    {
        if (m_bg == null || m_pic == null)
            return;

        m_fading = true;
        time = time <= 0 ? 0.2f : time;
        m_fadeSpeed = 1 / time;
        m_color = Color.white;
    }

    public override void Update()
    {
        if (m_show == false)
            return;

        if(m_lifeTick != -4399)
        {
            m_lifeTick -= Time.deltaTime;
            if (m_lifeTick <= 0)
            {
                m_lifeTick = -4399;
                FadeOut(0.4f);
            }
        }

        if(m_fading)
        {
            m_color.a -= Time.deltaTime * m_fadeSpeed;
            m_bg.color = m_color;
            m_pic.color = m_color;

            if (m_color.a <= 0)
            {
                Hide();
            }
        }
    }


}
