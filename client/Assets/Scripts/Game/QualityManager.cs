﻿using UnityEngine;
using System.Collections;

public class QualityManager
{
    static private ConfigQualityLine m_curQualityLine;
    static public bool enableOPFileLine = true;
    static public bool enableOPDanHeng = true;
    static public bool enableUIWorldAchievement = true;         // 世界击杀成就提示
    static public bool enableUIKillList = true;                 // 右上角击杀记录
    static public bool enableRigRot = true;                     // 上半身骨骼旋转

    private const int LEVEL_SUPER = 0;
    private const int LEVEL_NORMAL = 1;
    private const int LEVEL_LOW = 2;
    private const int LEVEL_SHIT = 3;

    static private int m_deviceLevel;
    static private float m_tick;
    static private int m_fps;
    static private int m_fpsTmp;
    static public int m_fpsAvg;
    static private int[] m_fpsCache = new int[5];
    static private int m_fpsCacheIndex;


    static public void Init()
    {
        ConfigQualityLine[] arrData = ConfigManager.GetConfig<ConfigQuality>().m_dataArr;
        m_deviceLevel = GetDeviceLevel();
        m_curQualityLine = arrData[m_deviceLevel];
    }
    
    static public void InitSceneCamera(Camera camera)
    {
        float[] layerCullDis = new float[32];
        layerCullDis[GameSetting.LAYER_VALUE_SKYBOX] = m_curQualityLine.CullDisSkyBox;
        layerCullDis[GameSetting.LAYER_VALUE_BUILDING] = m_curQualityLine.CullDisBuilding;
        layerCullDis[GameSetting.LAYER_VALUE_OTHER_PLAYER_ENEMY] = m_curQualityLine.CullDisPlayer;
        layerCullDis[GameSetting.LAYER_VALUE_OTHER_PLAYER_FRIEND] = m_curQualityLine.CullDisPlayer;
        layerCullDis[GameSetting.LAYER_VALUE_DMM_PLAYER] = m_curQualityLine.CullDisPlayer;
        layerCullDis[GameSetting.LAYER_VALUE_BATTLE_EFFECT] = m_curQualityLine.CullDisBattleEffect;
        layerCullDis[GameSetting.LAYER_VALUE_SCENE_EFFECT] = m_curQualityLine.CullDisSceneEffect;
        
        camera.layerCullDistances = layerCullDis;
        //camera.nearClipPlane = m_curQualityLine.CullDisNear;
        camera.farClipPlane = m_curQualityLine.CullDisFar;
    }

    static private int GetDeviceLevel()
    {
        if (Application.isEditor)
            return LEVEL_NORMAL;

        // 机型
        if (Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXWebPlayer)
            return LEVEL_SUPER;

        int ret = LEVEL_NORMAL;
        int levelRam = LEVEL_NORMAL;
        int levelCPU = LEVEL_NORMAL;
        int levelGPU = LEVEL_NORMAL;

        // Ram
        //int ram = SystemInfo.systemMemorySize + SystemInfo.graphicsMemorySize;
        int ram = SystemInfo.systemMemorySize;  // 除去显卡分配的内存
        if (ram <= 512)
            levelRam = LEVEL_LOW;        

        // CPU

        // GPU

        // 取最差指标
        ret = levelRam > levelCPU ? levelRam : levelCPU;
        ret = ret > levelGPU ? ret : levelGPU;

        return ret;
    }

    static public int lowBoneWeightDis
    {
        get{
            return m_curQualityLine.LowBoneWeightDis;
        }
    }


    // 统计战斗中的FPS，当低于不同阈值时，关闭部分功能，特效
    static public void Update()
    {
        if (SceneManager.singleton == null) 
            return;

        if(SceneManager.singleton.battleSceneLoaded == false)
        {
            if(m_fpsCache[0] != 0)
            {
                for (int i = 0; i < m_fpsCache.Length; i++)
                    m_fpsCache[i] = 0;
                m_fpsAvg = 0;
            }
            return;
        }

        m_fpsTmp++;
        m_tick += Time.deltaTime;
        if(m_tick >=1)
        {
            m_fps = m_fpsTmp;
            m_tick = 0;
            m_fpsTmp = 0;

            m_fpsCacheIndex = (m_fpsCacheIndex + 1) % m_fpsCache.Length;
            m_fpsCache[m_fpsCacheIndex] = m_fps;

            //
            m_fpsAvg = 0;
            int j = 0;
            for (int i = 0; i < m_fpsCache.Length; i++)
            {
                m_fpsAvg += m_fpsCache[i];
                if (m_fpsCache[i] != 0)
                    j++;
            }

            if (j == 0)
                return;

            m_fpsAvg /= j;

            if (m_fpsAvg >= 35)                           // 很流畅
            {
                enableOPFileLine = true;
                enableOPDanHeng = true;
                enableUIWorldAchievement = true;
                enableUIKillList = true;
                enableRigRot = true;
            }
            else if (m_fpsAvg >= 30 && m_fpsAvg < 35)    // 基本流畅
            {
                enableOPFileLine = true;
                enableOPDanHeng = false;
                enableUIWorldAchievement = false;
                enableUIKillList = true;
                enableRigRot = true;
            }
            if (m_fpsAvg >= 25 && m_fpsAvg < 30)         // 卡的边缘
            {
                enableOPFileLine = false;
                enableOPDanHeng = false;
                enableUIWorldAchievement = false;
                enableUIKillList = true;
                enableRigRot = false;
            }
            else if (m_fpsAvg < 25)    // 已经开始卡了
            {
                enableOPFileLine = false;
                enableOPDanHeng = false;
                enableUIWorldAchievement = false;
                enableUIKillList = false;
                enableRigRot = false;
            }
        }

    }

	 
}

 