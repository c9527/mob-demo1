﻿using fps.game;
using UnityEngine;

public class EffectGunFire : Effect 
{
    private EffectPlugin_Fadeout m_kFadeout;

	public EffectGunFire()
    {

    }

    public override void Release()
    {
        base.Release();
        m_kFadeout = null;
    }


    override public void SetGO(GameObject go)
    {
        base.SetGO(go);
    }

    public override void Play()
    {
        base.Play();

        if (m_kFadeout != null)
            m_kFadeout.Play();
    }

    public override void Stop()
    {
        base.Stop();

        if (m_kFadeout != null)
            m_kFadeout.Stop();
    }

    public void SetFadeOut(float fadeSpeed,float initAlpha)
    {                      
        m_kFadeout = GameObjectHelper.GetComponent<EffectPlugin_Fadeout>(m_goEffect);
        if (m_kFadeout != null)
        {
            m_kFadeout.fadeSpeed = fadeSpeed;
            m_kFadeout.initAlpha = initAlpha;
        }
    }
}

