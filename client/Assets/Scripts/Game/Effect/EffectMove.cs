﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class EffectMove : Effect
{
    public float timeOut = 10;
    public float distance;
    public float speed;
    //public bool isRelativeSpeed;


    protected EffectPlugin_Move m_kMove;


    public override void SetGO(GameObject go)
    {
        base.SetGO(go);

        if (go == null)
            return;

        m_kMove = GameObjectHelper.GetComponent<EffectPlugin_Move>(go);
        m_kMove.SetMoveCompleteCallBack(OnMoveComplete);
    }

    public override void Play()
    {
        base.Play();

        m_kMove.enabled = true;

        m_kMove.speed = speed;
        m_kMove.distance = distance;
        m_kMove.timeOut = timeOut;
        //m_kMove.isRelativeSpeed = isRelativeSpeed;
        m_kMove.Play();
    }

    public override void Stop()
    {
        if (m_playing == false)
            return;

        base.Stop();

        m_kMove.Stop();
    }

    public override void Reset()
    {
        base.Reset();
    }

    protected void OnMoveComplete()
    {
        Stop();

        EffectManager.singleton.Reclaim(this);

        if (m_completeCallBack != null)
        {
            m_completeCallBack(m_completeParam);
            m_completeCallBack = null;
            m_completeParam = null;
        }
    }


}
