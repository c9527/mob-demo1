﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class EffectTimeout : Effect
{
    protected EffectPlugin_Timeout m_kTimeout;
    private float m_selfLifeTime = 0;

 
    override public void Reset()
    {
        if(m_kTimeout != null)
            m_kTimeout.Reset();
    }

    override public void Play()
    {
        base.Play();
        if (m_goEffect != null && m_kTimeout != null)
            m_kTimeout.Play();
    }

    /// <summary>
    /// time == -1 表示使用粒子自身生命时间
    /// </summary>
    public void SetTimeOut(float time)
    {
        if(time == -1)
        {
            if(m_selfLifeTime <= 0)
                m_selfLifeTime = GetParticleLife();

            time = m_selfLifeTime;
        }

        if (m_kTimeout != null)
            m_kTimeout.lifeTime = time;
    }

    override public void Stop()
    {
        if (m_playing == false)
            return;

        base.Stop();

        m_kTimeout.Stop();
    }

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        if (go == null)
            return;

        m_kTimeout = go.AddMissingComponent<EffectPlugin_Timeout>();
        m_kTimeout.SetTimeoutCallBack(OnEffectTimeout, effectData.id);
    }

    virtual protected void OnEffectTimeout(GameObject go, string ID)
    {
        Stop();

        EffectManager.singleton.Reclaim(this);

        if(m_completeCallBack != null)
        {
            m_completeCallBack(m_completeParam);
            m_completeCallBack = null;
            m_completeParam = null;
        }
    }

}

