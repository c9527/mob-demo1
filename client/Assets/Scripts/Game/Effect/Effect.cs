﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class Effect
{
    public const string EFFECT_TYPE_GUN_FIRE = "effect_type_gun_fire";
    public const string EFFECT_TYPE_TIMEOUT = "effet_type_timeout";

    public const string MAT_EFFECT_KNIFE = "mat_effect_knife";
    public const string MAT_EFFECT_EXP = "mat_effect_exp";
    public const string MAT_EFFECT_GUN = "mat_effect_gun";

    public const float PARTICLE_LIFE_OFFSET = 0.3f;

    protected GameObject m_goEffect;
    protected Transform m_trans;
    protected EffectData m_kEffectData;
    protected object m_completeParam;
    protected Action<object> m_completeCallBack;
    
    //private ParticleSystem m_psRoot;
    protected List<ParticleSystem> m_listPS = new List<ParticleSystem>();
    protected List<TrailRenderer> m_listTR = new List<TrailRenderer>();
    protected List<Animator> m_listAnitor = new List<Animator>();
    protected List<Animation> m_listAni = new List<Animation>();
    protected List<Renderer> m_listRenderer = new List<Renderer>();

    private int m_lastSetLayer = -1;
    protected bool m_playing;


    public Effect()
    {
        
    }

    virtual public void Release()
    {
        ResourceManager.Destroy(m_goEffect);
        m_goEffect = null;
    }

    virtual public void Reset()
    {
        m_playing = false;
        if (m_goEffect != null)
        {
            Stop();
            m_trans.SetParent(SceneManager.singleton.reclaimRoot);
            m_trans.localPosition = Vector3.zero;
        }
            
    }

    virtual public void SetData(EffectData kED)
    {
        m_kEffectData = kED;
    }

    virtual public void LoadRes(Action<bool> funcLoaded)
    {
        if(gameObject != null)
        {
            if (funcLoaded != null)
                funcLoaded(true);
            return;
        }

        string strPrefabPath = PathHelper.GetEffectPrefabPath(m_kEffectData.id);
        ResourceManager.LoadBattlePrefab(strPrefabPath, (go, crc) =>
        {
            SetGO(go);

            if (funcLoaded != null)
                funcLoaded(go == null ? false : true);
        });         
    }

    virtual public void SetGO(GameObject go)
    {
        if (go == null)
        {
            //Logger.Error("Effect gameObject is null! id " + m_kEffectData.id);
            return;
        }

        m_goEffect = go;
        m_trans = go.transform;

        //m_psRoot = go.GetComponent<ParticleSystem>();
        //if (m_psRoot != null)
        //    m_psRoot.playOnAwake = false;

        m_listPS.Clear();
        m_listTR.Clear();
        m_listAnitor.Clear();
        m_listAni.Clear();

        GameObjectHelper.VisitChildByTraverse(m_trans, (child) =>
        {
            child.layer = GameSetting.LAYER_VALUE_BATTLE_EFFECT;

            ParticleSystem ps = child.GetComponent<ParticleSystem>();
            if (ps != null)
            {
                ps.playOnAwake = false;
                m_listPS.Add(ps);
            }

            TrailRenderer tr = child.GetComponent<TrailRenderer>();
            if (tr != null)
                m_listTR.Add(tr);

            Animator anitor = child.GetComponent<Animator>();
            if(anitor != null)
            {
                anitor.cullingMode = AnimatorCullingMode.BasedOnRenderers;
                anitor.applyRootMotion = false;
                m_listAnitor.Add(anitor);
            }

            Animation ani = child.GetComponent<Animation>();
            if (ani != null)
            {
                ani.cullingType = AnimationCullingType.BasedOnRenderers;
                ani.playAutomatically = false;
                m_listAni.Add(ani);
            }

            Renderer renderer = child.GetComponent<Renderer>();
            if (renderer != null)
                m_listRenderer.Add(renderer);

        }, true);

        //go.TrySetActive(false);
    }   

    public void Attach(Transform kParentTrans)
    {
        if (gameObject == null)
            return;

        transform.parent = kParentTrans;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0f, 0f, 1.0f);
        //gameObject.transform.localScale = Vector3.one;
    }

    public void Attach(Transform kParentTrans, Vector3 vec3Dir, float zOffset = 0.01f, float rotZ = 0f)
    {
        if (gameObject == null)
            return;

        transform.parent = kParentTrans;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0f, 0f, 1.0f);
        //transform.localScale = Vector3.one;
        transform.forward = vec3Dir;
        transform.position = vec3Dir * zOffset;
        transform.Rotate(Vector3.forward, rotZ, Space.Self);
    }

    public void Attach(Vector3 vec3Pos)
    {
        if (gameObject == null)
            return;

        transform.parent = null;
        transform.position = vec3Pos;
        transform.rotation = Quaternion.Euler(0f, 0f, 1.0f);
    }

    public void Attach(Vector3 vec3Pos, Vector3 vec3Dir, Space space = Space.World, Transform transParent = null, float zOffset = 0.01f, float rotZ = 0f)
    {
        if (gameObject == null)
            return;

        if (transParent != null)
            transform.SetParent(transParent);
        else
            transform.SetParent(SceneManager.singleton.effectRoot);

        //transform.rotation = Quaternion.Euler(0f, 0f, 1.0f);
        transform.localEulerAngles = Vector3.zero;

        if (vec3Dir != Vector3.zero)
            transform.forward = vec3Dir;

        if (space == Space.World)
            transform.position = vec3Pos + vec3Dir * zOffset;
        else
            transform.localPosition = vec3Pos + vec3Dir * zOffset;
        
        transform.Rotate(Vector3.forward, rotZ, Space.Self);
    }

    virtual public void SetLayer(int layer)
    {
        if (m_goEffect == null)
            return;

        if (layer != m_lastSetLayer)
        {
            m_lastSetLayer = layer;
            m_goEffect.ApplyLayer(layer, null, null, true);
        }
    }

    virtual public void Play()
    {
        if (m_goEffect == null)
            return;

        m_playing = true;

        if (m_goEffect.activeSelf == false)
            m_goEffect.TrySetActive(true);

       
        //if (m_psRoot != null)
        //    m_psRoot.Play(true);

        for (int i = 0; i < m_listPS.Count; i++)
        {
            m_listPS[i].Play();
        }

        for (int i = 0; i < m_listTR.Count; i++)
        {
            m_listTR[i].enabled = true;
        }

        for(int i=0; i<m_listAnitor.Count; i++)
        {
            m_listAnitor[i].enabled = true;
        }

        for(int i=0; i<m_listAni.Count; i++)
        {
            m_listAni[i].enabled = true;
            m_listAni[i].Play();
        }
    }

    virtual public void Stop()
    {
        if (m_goEffect == null || m_playing == false)
            return;

        m_playing = false;

        //if (m_psRoot != null)
        //    m_psRoot.Stop(true);

        for (int i = 0; i < m_listPS.Count; i++)
        {
            m_listPS[i].Stop();
        }

        for (int j = 0; j < m_listTR.Count; j++)
        {
            m_listTR[j].enabled = false;
        }

        for (int i = 0; i < m_listAnitor.Count; i++)
        {
            m_listAnitor[i].enabled = false;
        }

        for (int i = 0; i < m_listAni.Count; i++)
        {
            m_listAni[i].enabled = false;
        }
    }

    virtual public void SetCompleteCallBack(object param, Action<object> cb)
    {
        m_completeParam = param;
        m_completeCallBack = cb;
    }

    protected float GetParticleLife()
    {
        float maxTime = 0;

        for (int i=0; i<m_listPS.Count; i++)
        {
            ParticleSystem ps = m_listPS[i];
            float t = ps.startDelay + ps.startLifetime;
            if (t > maxTime)
                maxTime = t;
        }

        return maxTime + PARTICLE_LIFE_OFFSET;
    }

    public void SetParent(Transform parent)
    {
        if (m_trans == null)
            return;

        m_trans.SetParent(parent);
    }

#region Get Set

    public EffectData effectData
    {
        get { return m_kEffectData; }
    }

    public GameObject gameObject
    {
        get { return m_goEffect; }
    }

    public Transform transform
    {
        get { return m_trans; }
    }

    //virtual public bool autoReclaim
    //{
    //    get;
    //    set;
    //}
    public List<Renderer> rendererList
    {
        get { return m_listRenderer; }
    }

#endregion
     

}

