﻿using UnityEngine;
using System;


public class EffectLiveTimer : MonoBehaviour {

    public int lifeTime = 3000;     // 生命周期，ms 如果为0则不结束
    public int minPlayTime = 0;     // 至少要播放多久才会被停止, ms

    private Action<GameObject, string> m_funCallBack;
    private string m_strEffectID;
    private float m_fTick = 0f;
    private bool m_bPlayinging = false;


	void Start () {
	
	}

    public void Reset()
    {
        m_bPlayinging = false;
        m_fTick = 0f;
        m_funCallBack = null;
    }

    public float tickTime
    {
        get { return m_fTick; }
    }

    public void StartTick()
    {
        m_bPlayinging = true;
        m_fTick = 0;
    }

    public void StopTick()
    {
        m_bPlayinging = false;
    }
    
    public void SetTimeoutCallBack(Action<GameObject, string> callBack, string effectID = "")
    {
        m_funCallBack = callBack;
        m_strEffectID = effectID;
    }
	
	void Update () 
    {
        // 至少要播放这么久才会被结束
        if (m_bPlayinging == false && m_fTick < minPlayTime)
        {
            m_fTick += Time.deltaTime * 1000;
            if(m_fTick >= minPlayTime)
            {
                if (m_funCallBack != null)
                    m_funCallBack(gameObject, m_strEffectID);
                m_funCallBack = null;
                m_strEffectID = "";
                return;
            }
        }

        if (m_bPlayinging == true && lifeTime != 0)
        {
            m_fTick += Time.deltaTime * 1000;
            if (m_fTick >= lifeTime)
            {
                if (m_funCallBack != null)
                    m_funCallBack(gameObject, m_strEffectID);
                m_funCallBack = null;
                m_strEffectID = "";
            }
        }
	}
}
