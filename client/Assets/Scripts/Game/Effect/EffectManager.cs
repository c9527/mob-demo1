﻿using UnityEngine;
using Object = UnityEngine.Object;
using System;
using System.Collections.Generic;


public class EffectManager
{
    static public EffectManager singleton;
    static private Dictionary<string, List<Effect>> m_dicEffectCache = new Dictionary<string, List<Effect>>();
    static private List<Effect> m_listAllEffect = new List<Effect>();


    public EffectManager()
    {
        if (singleton != null)
            Logger.Error("EffectManager is a singleton!");

        singleton = this;
    }

   
    public void ReleaseAllRes()
    {
        foreach (Effect effect in m_listAllEffect)
        {
            if (effect != null)
                effect.Release();
        }
        m_listAllEffect.Clear();
        m_dicEffectCache.Clear();

        /*暂时不需要
        foreach (Effect effect in s_dictModelEffect.Values)
        {
            if (effect != null)
                effect.Release();
        }
        */
        s_dictModelEffect.Clear();
    }

    public void GetEffect<T>(string id, Action<T> funLoaded) where T : Effect
    {
        List<Effect> listEffect = null;
        m_dicEffectCache.TryGetValue(id, out listEffect);
        if(listEffect == null)
        {
            listEffect = new List<Effect>();
            m_dicEffectCache.Add(id, listEffect);
        }

        T kEffectRet;

        int iRearIndex = listEffect.Count - 1;
        while(iRearIndex != -1)
        {
            kEffectRet = listEffect[iRearIndex] as T;
            listEffect.RemoveAt(iRearIndex);
            if (null != kEffectRet)
            {
                kEffectRet.Reset();
                if (null != funLoaded)
                    funLoaded(kEffectRet);
                return;
            }
            iRearIndex = listEffect.Count - 1;
        }

        // temp code
        EffectData kED = new EffectData();
        kED.id = id;

        kEffectRet = System.Activator.CreateInstance<T>();
        kEffectRet.SetData(kED);
        m_listAllEffect.Add(kEffectRet);

        kEffectRet.LoadRes((bSuccess) =>
        {
            if (funLoaded != null)
            {
                kEffectRet.Reset();
                funLoaded(kEffectRet);
            }
        });
    }

    public void Reclaim(Effect kEffect)
    {
        if (kEffect == null)
            return;

        kEffect.Stop();

        List<Effect> listEffect = null;
        m_dicEffectCache.TryGetValue(kEffect.effectData.id, out listEffect);
        if(listEffect == null)
        {
            listEffect = new List<Effect>();
            m_dicEffectCache.Add(kEffect.effectData.id, listEffect);
        }

        listEffect.Add(kEffect);
        if (kEffect.gameObject != null)
        {
            //kEffect.gameObject.TrySetActive(false);
            if (SceneManager.singleton.reclaimRoot != null)
            {
                kEffect.SetParent(SceneManager.singleton.reclaimRoot);
                kEffect.transform.localPosition = Vector3.zero;
            }
            else
            {
                kEffect.SetParent(null);
                kEffect.transform.localPosition = new Vector3(4399, 4399);
            }
        }
    }

    static public string GetPlayerHitEffect(int iBodyPartID, string matEffectType = Effect.MAT_EFFECT_GUN)
    {
        int iMatID = (int)EMat.Body;
        switch (iBodyPartID)
        {
            case BasePlayer.BODY_PART_ID_HEAD:
                iMatID = (int)EMat.Head;
                break;
            case BasePlayer.BODY_PART_ID_BODY:
                iMatID = (int)EMat.Body;
                break;
            case BasePlayer.BODY_PART_ID_LIMBS:
                iMatID = (int)EMat.Limbs;
                break;
            case BasePlayer.BODY_PART_ID_SHIELD:
                iMatID = (int)EMat.Shield;
                break;
        }

        ConfigMaterialLine kML = ConfigManager.GetConfig<ConfigMaterial>().GetLine(iMatID);
        if(kML != null)
        {
            if (matEffectType == Effect.MAT_EFFECT_KNIFE)
                return kML.KnifeEffect;
            else
                return kML.HitEffect;
        }

        return "";
    }

    static public ConfigMaterialLine GetPlayerHitEffectCfg(int iBodyPartID)
    {
        int iMatID = (int)EMat.Body;
        switch (iBodyPartID)
        {
            case BasePlayer.BODY_PART_ID_HEAD:
                iMatID = (int)EMat.Head;
                break;
            case BasePlayer.BODY_PART_ID_BODY:
                iMatID = (int)EMat.Body;
                break;
            case BasePlayer.BODY_PART_ID_LIMBS:
                iMatID = (int)EMat.Limbs;
                break;
            case BasePlayer.BODY_PART_ID_SHIELD:
                iMatID = (int)EMat.Shield;
                break;
        }
        return ConfigManager.GetConfig<ConfigMaterial>().GetLine(iMatID);
    }



    static private Dictionary<int, Effect> s_dictModelEffect = new Dictionary<int, Effect>();

    /// <summary>
    /// 非管理器播放特效方式
    /// </summary>
    /// <param name="tran"></param>
    /// <param name="isPlay"></param>
    static public void PlayEffect(Transform tran, bool isPlay, bool isMainplayEffect = false)
    {
        if (tran == null)
            return;

        Effect effect = null;
        if (!s_dictModelEffect.TryGetValue(tran.GetInstanceID(), out effect))
        {
            effect = new Effect();
            effect.SetGO(tran.gameObject);
            if (isMainplayEffect)
                effect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            s_dictModelEffect.Add(tran.GetInstanceID(), effect);
        }
        if (isPlay)
        {
            tran.gameObject.TrySetActive(false);
            tran.gameObject.TrySetActive(true);
            effect.Play();
        }
        else
        {
            //if (s_dictModelEffect.ContainsKey(tran.GetInstanceID()))
            //    s_dictModelEffect.Remove(tran.GetInstanceID());
            tran.gameObject.TrySetActive(false);
            //effect.Release();
        }
    }
}
