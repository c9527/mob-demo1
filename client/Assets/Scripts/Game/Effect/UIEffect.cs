﻿using System;
using UnityEngine;

public class UIEffect
{
    private bool m_destroy;
    public bool m_hide;
    private int m_timerUid;
    private bool m_useOriginalOffset;
    private float m_liveTime;
    private Vector3 m_offset;
    private Transform m_target;
    private UIEffectShowType m_showType;
    private object m_completeParam;
    private Action<object> m_completeCallBack;
    public GameObject m_goEffect;

    public static UIEffect ShowEffect(Transform parent, string name, bool useOriginalOffset = false)
    {
        return ShowEffect(parent, UIEffectShowType.Parent, name, 0, useOriginalOffset, Vector3.zero);
    }

    public static UIEffect ShowEffect(Transform parent, string name, Vector3 offset)
    {
        return ShowEffect(parent, UIEffectShowType.Parent, name, 0, false, offset);
    }

    public static UIEffect ShowEffect(Transform parent, string name, float liveTime, bool useOriginalOffset = false, bool rebuildLayer = true)
    {
        return ShowEffect(parent, UIEffectShowType.Parent, name, liveTime, useOriginalOffset, Vector3.zero, rebuildLayer);
    }

    public static UIEffect ShowEffect(Transform parent, string name, float liveTime, Vector3 offset, bool rebuildLayer = true)
    {
        return ShowEffect(parent, UIEffectShowType.Parent, name, liveTime, false, offset, rebuildLayer);
    }

    public static UIEffect ShowEffectAfter(Transform target, string name, bool useOriginalOffset = false)
    {
        return ShowEffect(target, UIEffectShowType.SiblingAfter, name, 0, useOriginalOffset, Vector3.zero);
    }

    public static UIEffect ShowEffectAfter(Transform target, string name, Vector3 offset)
    {
        return ShowEffect(target, UIEffectShowType.SiblingAfter, name, 0, false, offset);
    }

    public static UIEffect ShowEffectBefore(Transform target, string name, bool useOriginalOffset = false)
    {
        return ShowEffect(target, UIEffectShowType.SiblingBefore, name, 0, useOriginalOffset, Vector3.zero);
    }

    public static UIEffect ShowEffectBefore(Transform target, string name, Vector3 offset)
    {
        return ShowEffect(target, UIEffectShowType.SiblingBefore, name, 0, false, offset);
    }

    public static UIEffect ShowEffect(Transform target, string name, float liveTime, Action<object> m_completeCallBack, object m_completeParam)
    {
        return ShowEffect(target, UIEffectShowType.Parent, name, liveTime, false, Vector3.zero, true, m_completeCallBack, m_completeParam);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="target">父对象</param>
    /// <param name="showType">显示类型</param>
    /// <param name="name">特效名</param>
    /// <param name="liveTime">持续时间</param>
    /// <param name="useOriginalOffset">是否使用Prefab本身的坐标位置</param>
    /// <param name="offset">指定偏移</param>
    /// <param name="rebuildLayer">是否重新计算渲染层次</param>
    /// <returns></returns>
    public static UIEffect ShowEffect(Transform target, UIEffectShowType showType, string name, float liveTime, bool useOriginalOffset, Vector3 offset, bool rebuildLayer = true, Action<object> completeCallBack = null, object completeParam = null)
    {
        var uiEffect = new UIEffect();
        uiEffect.m_liveTime = liveTime;
        uiEffect.m_useOriginalOffset = useOriginalOffset;
        uiEffect.m_offset = offset;
        uiEffect.m_showType = showType;
        uiEffect.m_target = target;
        uiEffect.m_completeCallBack = completeCallBack;
        uiEffect.m_completeParam = completeParam;
        if (!string.IsNullOrEmpty(name))
            LoadEffect(target, "Effect/"+name, uiEffect);
        return uiEffect;
    }

    private static void LoadEffect(Transform target, string path, UIEffect uiEffect)
    {
        ResourceManager.LoadUIEffect(path, go =>
        {
            if (uiEffect.m_destroy)
                return;

            uiEffect.m_goEffect = GameObject.Instantiate(go) as GameObject;
            if (uiEffect.m_goEffect == null)
                return;
            if (uiEffect.m_hide)
            {
                uiEffect.m_goEffect.TrySetActive(false);                
            }

#if UNITY_EDITOR
            var arr = uiEffect.m_goEffect.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].material != null)
                    arr[i].material.shader = Shader.Find(arr[i].material.shader.name);
            }
#endif

            uiEffect.m_goEffect.ApplyLayer(GameSetting.LAYER_VALUE_UI, null, null, true);
            var tran = uiEffect.m_goEffect.transform;
            tran.SetParent(UIManager.m_rootCanvas, false);//先挂到画布下，使其缩放正确
            if (uiEffect.m_showType == UIEffectShowType.Parent)
            {
                tran.SetParent(target);
            }
            else if (uiEffect.m_showType == UIEffectShowType.SiblingBefore)
            {
                tran.SetParent(target.parent);
                tran.SetSiblingIndex(target.GetSiblingIndex()+1);
                target.gameObject.AddMissingComponent<SortingOrderRenderer>();
            }
            else if (uiEffect.m_showType == UIEffectShowType.SiblingAfter)
            {
                tran.SetParent(target.parent);
                tran.SetSiblingIndex(target.GetSiblingIndex());
                target.gameObject.AddMissingComponent<SortingOrderRenderer>();
            }

            if (!uiEffect.m_useOriginalOffset)
                    tran.localPosition = uiEffect.m_offset;

            if (uiEffect.m_liveTime > 0)
            {
                uiEffect.m_timerUid = TimerManager.SetTimeOut(uiEffect.m_liveTime, o =>
                {
                    if (uiEffect.m_completeCallBack != null)
                        uiEffect.m_completeCallBack(uiEffect.m_completeParam);
                    ((UIEffect) o).Destroy();
                    ((UIEffect) o).m_timerUid = 0;
                }, uiEffect);
            }

            uiEffect.m_goEffect.AddComponent<SortingOrderRenderer>();
            SortingOrderRenderer.RebuildAll();
        });
    }

    public void Destroy()
    {
        m_destroy = true;
        if (m_timerUid > 0)
            TimerManager.RemoveTimeOut(m_timerUid);
        if (m_goEffect)
            GameObject.Destroy(m_goEffect);
    }

    public void Hide()
    {
        m_hide = true;
        if (m_timerUid > 0)
            TimerManager.RemoveTimeOut(m_timerUid);
        if (m_goEffect)
            m_goEffect.TrySetActive(false);
    }

    public void Play(bool reCalcDepth = false)
    {
        m_hide = false;
        if (m_goEffect)
        {
            m_goEffect.TrySetActive(false);
            m_goEffect.TrySetActive(true);
        }
        if (reCalcDepth)
            SortingOrderRenderer.RebuildAll();
    }

    public bool isDestroy
    {
        get { return m_destroy; }
    }
}

public enum UIEffectShowType
{
    Parent,
    SiblingBefore,
    SiblingAfter,
}