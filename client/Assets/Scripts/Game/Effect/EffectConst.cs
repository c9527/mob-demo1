﻿class EffectConst
{
    //UI特效
    public const string UI_KILL_NORMAL_1 = "UI_jisha_1";
    public const string UI_KILL_NORMAL_2 = "UI_jisha_2";
    public const string UI_KILL_NORMAL_3 = "UI_jisha_3";
    public const string UI_KILL_NORMAL_4 = "UI_jisha_4";
    public const string UI_KILL_NORMAL_5 = "UI_jisha_5";
    public const string UI_KILL_NORMAL_6 = "UI_jisha_6";
    public const string UI_KILL_NORMAL_GOD_LIKE = "UI_jisha_chaoshen";
    public const string UI_KILL_HEAD = "UI_jisha_baotou";
    public const string UI_KILL_HEAD_GOLD = "UI_jisha_huangjinbaotou";

    public const string UI_MALL_HALO = "UI_wuqiyulan";

    public const string UI_EVENT_FIRST_RECHARGE = "UI_shouchong";

    public const string UI_LOGIN_VIEW_BG = "UI_denglujiemian";
    public const string UI_LOGIN_QIAN = "UI_denglujiemian_qian";
    public const string UI_LOGIN_HOU = "UI_denglujiemian_hou";
    public const string UI_LOGIN_QIANG = "UI_denglujiemian_qiang";
    public const string UI_LOGIN_XIAN = "UI_denglujiemian_xian";
    public const string UI_LOGIN_BTN_EFFECT = "UI_dianji_dengluyouxi";

    public const string UI_HALL_CLICK = "UI_dianji";
    public const string UI_HALL_COIN = "UI_jinqian";
    public const string UI_HALL_DIAMOND = "UI_zuanshi";
    public const string UI_HALL_MEDAL = "UI_rongyu";
    //public const string UI_HALL_BG_CLOUD = "UI_zhuyebeijing";
    public const string UI_HALL_BG_CLOUD = "UI_zhujiemian_huoxing";
    public const string UI_HALL_BG_LIGHT = "UI_zhuyeliuguang";
    public const string UI_HALL_BTN_MALL = "UI_shangcheng";

    public const string UI_HALLBATTLE_CARD = "UI_youximoshi";
    public const string UI_HALLBATTLE_PVE_CARD = "UI_youximoshi_chuangguan_1";
    public const string UI_HALLBATTLE_PVE_ROLE = "UI_youximoshi_chuangguan_2";
    public const string UI_HALLBATTLE_PVE_NEW = "UI_youximoshi_chuangguan_xin";
    public const string UI_HALLBATTLE_PVP_CARD = "UI_youximoshi_duizhan_1";
    public const string UI_HALLBATTLE_PVP_ROLE = "UI_youximoshi_duizhan_2";
    public const string UI_HALLBATTLE_PVP_NEW = "UI_youximoshi_duizhan_xin";
    public const string UI_HALLBATTLE_CORPSMATCH_NEW = "UI_youximoshi_zhanduiliansai";
    public const string UI_HALLBATTLE_RANK_CARD = "UI_youximoshi_paiwei_1";
    public const string UI_HALLBATTLE_RANK_ROLE = "UI_youximoshi_paiwei_2";
    public const string UI_HALLBATTLE_RANK_New = "UI_youximoshi_paiwei_xin";
    public const string UI_HALLBATTLE_LEGION_CORPS = "UI_zhuye_kapai_01";
    //    public const string UI_HALLBATTLE_REWARD = "UI_fuli";
    //    public const string UI_HALLBATTLE_ACTIVITY = "UI_huodong";
    //    public const string UI_HALLBATTLE_RANK = "UI_paihangbang";
    public const string UI_HALLBATTLE_HASREWARD = "UI_denglujiangli";
    public const string UI_ROOM_BG_FOG = "UI_fangjianbeijing";

    public const string UI_PVEMISSION_EFFECT = "UI_youxiguanqia";
    public const string UI_PVEMISSION_LIGHT_EFFECT = "UI_youxiguanqia_liuguang";
    public const string UI_PVEMISSION_LIGHT_EFFECT_ZOMBIE = "UI_youxiguanqia_lv";

    public const string UI_STRENGTHEN_LEVELUP = "UI_qiangxieshengji";
    public const string UI_STRENGTHEN_UPGRADE = "UI_qiangxiejinjie";
    public const string UI_STRENGTHEN_UPGRADE_STAR = "UI_qiangxiejinjie_xiangqian";
    public const string UI_STRENGTHEN_WEAPON_SHOW = "UI_qiangxietujian";
    public const string UI_STRENGTHEN_WEAPON_BG = "UI_qiangxietujian_lengqi";
    public const string UI_STRENGTHEN_EMBED_PART = "UI_xiangqiancao";
    public const string UI_STRENGTHEN_TEST_PART = "UI_peijianku";
    public const string UI_STRENGTHEN_SUIT_EFFECT_1 = "UI_xiangqiancao_1";
    public const string UI_STRENGTHEN_SUIT_EFFECT_2 = "UI_xiangqiancao_2";
    public const string UI_STRENGTHEN_SUIT_EFFECT_3 = "UI_xiangqiancao_3";
    public const string UI_STRENGTHEN_AWAKEN_BG = "UI_juexing01";
    public const string UI_STRENGTHEN_AWAKEN = "UI_juexing02";//2s
    public const string UI_STRENGTHEN_AWAKEN_SUCCESS = "UI_juexingchenggong";//1s
    public const string UI_STRENGTHEN_AWAKEN_FAIL = "UI_juexingshibai";//1s

    public const string UI_CONVERT = "UI_duihuan";

    public const string UI_SETTLE_REWARD_DOUBLE_1 = "UI_jianglifanbei_1";
    public const string UI_SETTLE_REWARD_DOUBLE_2 = "UI_jianglifanbei_2";
    public const string UI_SETTLE_REWARD_GONGXI = "UI_gongxinihuode";

    public const string UI_FIGHT_ROUND_BEGIN = "UI_huihekaishi";
    public const string UI_FIGHT_ROUND_DRAW = "UI_huihepingju";
    public const string UI_FIGHT_ROUND_WIN = "UI_huiheshengli";
    public const string UI_FIGHT_ROUND_LOSE = "UI_huiheshibai";

    public const string UI_FIGHT_CHALLENGE_ROUND_WIN = "UI_survival_roundwin";

    public const string UI_FIGHT_GAME_BEGIN = "UI_zhandoukaishi";
    public const string UI_FIGHT_GAME_END = "UI_zhandoujieshu";
    public const string UI_FIGHT_GAME_WIN = "UI_zhandoushengli";
    public const string UI_FIGHT_GAME_LOSE = "UI_zhandoushibai";

    public const string UI_FIGHT_HUMAN_WIN = "UI_renleishengli";
    public const string UI_FIGHT_ZOMBIE_WIN = "UI_shenghuatishengli";

    public const string UI_FIGHT_KINGSOLO_END = "UI_qiangwangzhiwang";
    public const string UI_FIGHT_HIDE_WIN = "UI_maomaofangshengli";

    public const string UI_RECHARGE_EFFECT = "UI_chongzhi";
    public const string UI_KUAISU_KAISHI = "UI_kuaisukaishi";

    public const string UI_LOTTERY_AWARD = "UI_choujiang_1";//t=0
    public const string UI_LOTTERY_BUTTON = "UI_choujiang_2";//t=0
    public const string UI_LOTTERY_CLICK = "UI_choujiang_5";//t=0.5s
    public const string UI_LOTTERY_HIGHLIGHT = "UI_choujiang_3";//t=0
    public const string UI_LOTTERY_SP_HIT = "UI_choujiang_4_01_da";//t=1.5s
    public const string UI_LOTTERY_HIT = "UI_choujiang_4_02_da";//t=1.5s
    public const string UI_LOTTERY_CARD_COVER = "UI_choujiang_kapai";//t=1s
    public const string UI_LOTTERY_LITTLE_STAR = "UI_choujiang_6";
    public const string UI_LOTTERY_SP_REWARD = "UI_choujiang_7";//t=2s
    public const string UI_GETITEM_ITEM = "UI_choujiang_shilianchou";//t=0.5s
    public const string UI_GETITEM_GETWEAPON = "UI_shouchong_gongxinihuodekuxuanwuqi(nowuqi)";
    public const string UI_FIRST_RECHARGE_GIFT = "UI_shouchong_gongxinihuodekuxuanwuqi";
    public const string UI_FIRST_RECHARGE_ZHUYE = "UI_shouchong_zhuye";
    public const string UI_BUY_NORMAL_MEMBER = "UI_chongzhi_gongxinichengweihuiyuan";
    public const string UI_BUY_HIGH_MEMBER = "UI_chongzhi_gongxinichengweizunguihuiyuan";
    public const string UI_EQUIP_BOX_EFFECT = "UI_choujiang_xiangzi_1";
    public const string UI_EQUIP_BOX_EXPLODE_EFFECT = "UI_choujiang_xiangzi";//t=4.5s
    public const string UI_HUODEZUANSHI = "UI_huodezuanshi";
    public const string UI_CORPS_TASK_ACTIVENESS_REWARD = "UI_zhanduirenwulingqu";
    public const string UI_BIG_HEAD_PROGRESS_BAR = "UI_datouwang_jindutiao";
    public const string UI_PVEVP_PROGRESS_BAR = "UI_pvevp_jindutiao";
    public const string UI_JUNHUOKU_XULI= "UI_junhuoku_xuli";//军火库点击效果
    public const string UI_JUNHUOKU_ZHONGJIAN = "UI_junhuoku_zhongjian";//军火库闪光效果

    public const string UI_PLAYER_ADD_LIFE = "UI_jiaxue_MP";

    public const string UI_JIESUAN_SHANGUANG = "UI_jiesuan_shanguang";//枪械可进阶进度条

    //战斗特效
    public const string FIGHT_HIDE_ITEM_DIE = "duomaomao_xiaosan";
    public const string FIGHT_SURVIVAL_DIE = "baozha_siwang_tongyong";

    public const string ROLE_LEVELUP_TITLE = "UI_wanjiashengji";
    public const string ROLE_LEVELUP_DIGUANG = "UI_wanjiashengji_diguang";
    public const string ROLE_LEVELUP_PICE = "UI_wanjiashengji_suipian";
    public const string ROLE_LEVELUP_ITEM_EFFECT = "UI_wanjiashengji_biankuang_liuguang";
    public const string ROLE_LEVELUP = "UI_junxianshengji01";//t=2s

    public const string UI_GANRANSHIBAI = "UI_GanRanShiBai";
    public const string UI_GONGXI_DACHENG_CHENGJIU = "UI_gongxidachengchengjiu";

    public const string UI_SHUBIAO01 = "UI_shubiao01";
    public const string UI_SHUBIAO02 = "UI_shubiao02";

    public const string UI_JUNTUANSHILIFENBU = "UI_juntuanshilifenbu";

    public const string UI_FIGHT_YUJING_ALERT = "UI_yujing";
    public const string UI_HEAD_HERO_CARD = "UI_wanjiatouxiangkuang";
    public const string UI_HEAD_HERO_CARD_XIAO = "UI_wanjiatouxiangkuang_xiao";

    public const string UI_HIDE_BEIJISHAFANG = "UI_beijishafang";//躲猫猫被击杀方

    public const string UI_QIANDAO_LEIJI = "UI_qiandao_lingjiang";

    public const string UI_BIGHEAD_MAINPLAYER_LEVEL_UP = "UI_renwushengji";//大头模式主角升级
    public const string BIGHEAD_OTHERPLAYER_LEVEL_UP = "renwushengji_OP";//大头模式第三人称升级

    public const string UI_TANCEQI_LV = "UI_tanceqi_lv";//幽灵模式视野探测器--绿色
    public const string UI_TANCEQI_HUANG = "UI_tanceqi_huang";//幽灵模式视野探测器--黄色
    public const string UI_TANCEQI_HONG = "UI_tanceqi_hong";//幽灵模式视野探测器--红色

    public const string C4_EFFECT = "C4_effect";//C4掉落或者安装特效

    public const string LASER = "laser";//激光特效

    public const string UI_DAGGEROW_ZHENGDUOZHONG = "UI_daofengzhandian_zhengduozhong";//刀锋争夺中
    public const string DAGGEROW_GRAY = "daofengzhandian_bai";//刀锋据点-灰色
    public const string DAGGEROW_RED = "daofengzhandian_cheng";//刀锋据点-橙色
    public const string DAGGEROW_BLUE = "daofengzhandian_lan";//刀锋据点-蓝色
    public const string UI_DAGGEROW_OVERTIME = "UI_jiashi_tuowei";//刀锋据点加时特效

}
