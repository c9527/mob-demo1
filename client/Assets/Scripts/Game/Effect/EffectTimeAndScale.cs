﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// 有倒计时同时也支持缩放的effect
/// </summary>
public class EffectTimeAndScale : EffectTimeout
{
    protected ParticleScaler m_ps;

    override public void SetGO(GameObject go)
    {
        base.SetGO(go);

        if (go == null)
            return;

        m_ps = go.AddMissingComponent<ParticleScaler>();
    }

    override public void Play()
    {
        m_ps.UpdateScale();

        base.Play();
    }
}