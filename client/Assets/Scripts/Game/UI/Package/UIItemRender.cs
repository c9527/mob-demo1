﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 通用的Item UI render
/// 支持控制Item Frame，Item Image，Item Text,Item Selected（独立于Toggle）
/// </summary>
class UIItemRender : ItemRender
{
    CDItemData _data;

    string _frame_atlas_name = AtlasName.Strengthen;
    string _frame_sprite_prefix = "item_frame_";
    int _size_mode;//0：native size，1: fixed size，2：auto fit

    //必选组件
    Image _frame_img;
    Image _item_img;

    //可选组件
    Text _name_txt;
    Text _num_txt;
    Image _selected_img;

    public override void Awake()
    {
        _frame_img = transform.Find("Frame").GetComponent<Image>();
        _item_img = transform.Find("Image").GetComponent<Image>();
        //可选组件,使用前需要判断null
        var trans = transform.Find("Text");
        if (trans != null)
        {
            _name_txt = trans.GetComponent<Text>();
        }
        trans = transform.Find("Num");
        if (trans != null)
        {
            _num_txt = trans.GetComponent<Text>();
        }
        trans = transform.Find("Selected");
        if (trans != null)
        {
            _selected_img = trans.GetComponent<Image>();
        }
    }

    public void SetStyle(string atlas,string prefix,int size_mode=0)
    {
        if (!string.IsNullOrEmpty(atlas) && !string.IsNullOrEmpty(prefix))
        {
            _frame_atlas_name = atlas;
            _frame_sprite_prefix = prefix;
        }
        _size_mode = size_mode;
    }

    public CDItemData data
    {
        get { return _data; }
    }

    public bool selected
    {
        get 
        {
            return _data != null && _data.selected == true;
        }
        set
        {
            if (_data != null)
            {
                _data.selected = value;
                if (_selected_img != null && _selected_img.enabled != _data.selected)
                {
                    _selected_img.enabled = _data.selected;
                }
            }
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDItemData;
        if (_data != null && _data.info!=null)
        {
            var quality = _data.GetQuality();
            _frame_img.enabled = quality>0;
            _frame_img.SetSprite(quality > 0 ? ResourceManager.LoadSprite(_frame_atlas_name, _frame_sprite_prefix + quality) : null);
            _item_img.enabled = true;
            _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, _data.info.Icon));
            if (_data!= null && _data.info is ConfigItemWeaponLine)
            {
                _item_img.transform.localEulerAngles = new Vector3(0, 0, -15);
            }
            else
            {
                _item_img.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            if (_size_mode == 0)
            {
                _item_img.SetNativeSize();
            }
            else if (_size_mode==2)
            {
                if (_item_img.sprite != null)
                {
                    var scale = Mathf.Min(_item_img.rectTransform.sizeDelta.y / _item_img.sprite.rect.height, 1);
                    _item_img.rectTransform.sizeDelta = new Vector2(_item_img.sprite.rect.width * scale, _item_img.sprite.rect.height * scale);
                }
            }
        }
        else
        {
            _frame_img.enabled = false;
            _item_img.enabled = false;
            _item_img.SetSprite(null);
        }
        if (_name_txt != null)
        {
            if (_data != null && !string.IsNullOrEmpty(_data.overrideName))
                _name_txt.text = _data.overrideName;
            else if (_data != null && _data.info != null)
                _name_txt.text = _data.info.DispName;
            else
                _name_txt.text = "";
        }
        if (_num_txt != null)
        {
            _num_txt.text = _data != null ? ("X" + _data.sdata.item_cnt) : "";
        }
        if (_selected_img != null)
        {
            _selected_img.enabled = _data!=null && _data.selected;
        }
    }
}

public class BuffInfo : ItemInfo
{
    public string buff_name;
    public string icon;
}

public class CDItemData
{
    public p_item sdata;
    public ConfigItemLine info;
    public bool selected;
    public object extend;
    public string overrideName; //如果非空，则覆盖道具本身的名字

    public CDItemData(int code=0)
    {
        sdata = new p_item() { uid=0, item_id=code };
        info = code>0 ? ItemDataManager.GetItem(code) : null;
        selected = false;
    }

    public CDItemData(p_item data)
    {
        sdata = data!=null ? data : new p_item(){ uid=0, item_id=0};
        info = sdata.item_id>0 ? ItemDataManager.GetItem(sdata.item_id) : null;
        selected = false;
    }

    public void UpdateData(int code,int uid=0,int count=0)
    {
        if (sdata.item_id != code)
        {
            sdata.item_id = code;
            info = sdata.item_id > 0 ? ItemDataManager.GetItem(sdata.item_id) : null;
        }
        sdata.uid = uid;
        sdata.item_cnt = count;
    }

    public int GetQuality()
    {
        if (sdata!=null && sdata.accessory_values!=null && sdata.accessory_values.Length>0 && (info is ConfigItemAccessoriesLine))
        {
            return (info as ConfigItemAccessoriesLine).GetQuality(sdata.accessory_values[0]);
        }
        return 0;
    }
}
