﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelRenew : BasePanel
{
    public class ItemRenew : ItemRender
    {
        Image icon;
        Text name;
        p_item info;
        ConfigMallLine mallinfo;
        public override void Awake()
        {
            icon = transform.Find("icon").GetComponent<Image>();
            name = transform.Find("Text").GetComponent<Text>();
            UGUIClickHandler.Get(transform.Find("Button")).onPointerClick += RenewItem;
        }

        void RenewItem(GameObject target, PointerEventData data)
        {
            UIManager.PopChatPanel<PanelBuy>(new object[] { mallinfo }, true);
        }
        protected override void OnSetData(object data)
        {
            var info = data as p_item;
            icon.SetSprite(ResourceManager.LoadIcon(info.item_id));
            icon.SetNativeSize();
            var config = ConfigManager.GetConfig<ConfigMall>();
            mallinfo = config.GetMallLine(info.item_id);
            name.text = mallinfo.Name;
        }


    }

    private DataGrid m_renewList;

    public PanelRenew()
    {
        SetPanelPrefabPath("UI/Package/PanelRenew");

        AddPreLoadRes("UI/Package/PanelRenew", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_renewList = m_tran.Find("renewList/content").gameObject.AddComponent<DataGrid>();
        m_renewList.SetItemRender(m_tran.Find("item").gameObject, typeof(ItemRenew));
    }

    public override void OnBack()
    {

    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel(); };
        AddEventListener("buy_item_update", UpdateRenewList);
        AddEventListener("new_renew_item", UpdateRenewList);
    }

    public void UpdateRenewList()
    {
        if (ItemDataManager.renewlist.Count > 0)
        {
            m_renewList.Data = ItemDataManager.renewlist.ToArray();
        }
        else
        {
            this.HidePanel();
        }
    }

    public override void OnDestroy()
    {

    }

    public override void OnShow()
    {
        m_renewList.Data = ItemDataManager.renewlist.ToArray();
        for (int i = 0; i < ItemDataManager.renewlist.Count; i++)
        {
           var info = ItemDataManager.GetItem(ItemDataManager.renewlist[i].item_id);
           if (info.Type == "ROLE")
           {
               if (PlayerSystem.getEquipRoleInfo(1).ID == info.ID)
               {
                   if (PlayerSystem.roleData.sex == 1)
                   {
                       int uid = ItemDataManager.GetDepotItem(4002).uid;
                       int camp = 1;
                       PlayerSystem.TosSetDefaultRole(uid, camp);
                   }
                   else
                   {
                       int uid = ItemDataManager.GetDepotItem(4004).uid;
                       int camp = 1;
                       PlayerSystem.TosSetDefaultRole(uid, camp);
                   }
               }
               else if (PlayerSystem.getEquipRoleInfo(2).ID == info.ID)
               {
                   if (PlayerSystem.roleData.sex == 1)
                   {
                       int uid = ItemDataManager.GetDepotItem(4001).uid;
                       int camp = 1;
                       PlayerSystem.TosSetDefaultRole(uid, camp);
                   }
                   else
                   {
                       int uid = ItemDataManager.GetDepotItem(4003).uid;
                       int camp = 1;
                       PlayerSystem.TosSetDefaultRole(uid, camp);
                   }
               }
           }
        }
    }

    public override void OnHide()
    {
        for (int i = 0; i < ItemDataManager.renewlist.Count; i++)
        {
            if (ItemDataManager.hasrenewedlist.Contains(ItemDataManager.renewlist[i]))
            {
                continue;
            }
            ItemDataManager.hasrenewedlist.Add(ItemDataManager.renewlist[i]);
        }
        ItemDataManager.renewlist.Clear(); 
    }

    public override void Update()
    {

    }
}