﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelDepot : BasePanel
{
    private DataGrid m_dataGrid;
    private Text m_txtName;
    private Text m_txtDesc;
    private Image m_imgEquip;
    private RawImage m_weaponModel;
    private GameObject m_weaponDrag;
    private Image m_goDurability;
    private Image m_imgDurability;
    private Text m_txtDurability;
    private Image m_imgBroken;

    private Toggle m_lastToggle;

    private GameObject m_goTabbar;
    private Transform m_tabWeaponTran;
    private Toggle m_tabDecoration;
    private Toggle m_tabOther;

    private Text m_weaponTxt;
    private Text m_partTxt;
    private Text m_fragmentTxt;
    private Text m_roleTxt;
    private Text m_otherTxt;
    private Text m_decorationTxt;

    private DataGrid m_dataGridProp;
    private Transform m_part_group;
    private Transform m_tranStar;
    private Transform m_tranAwaken;

    private ItemDropInfo[] m_weaponClassifyArr;
    private ItemDropInfo[] m_armorClassifyArr;
    private ItemDropInfo[] m_partClassifyArr;
    private ItemDropInfo[] m_fragmentClassifyArr;
    private ItemDropInfo[] m_otherClassifyArr;
    private ItemDropInfo[] m_roleClassifyArr;
    private ItemDropInfo[] m_decorationClassifyArr;
    private string m_selectedType;
    private string m_selectedSubType;
    private int m_itemid;
    private bool m_selectMode;  //是否是从背包中点击装备槽跳转过来的
    private Image m_bagid;
    private Button m_btnClose;
//    private Button m_btnEquip;
//    private Button m_btnContinue;
//    private Button m_btnMerge;
    private Button m_btnDel;
    private Button m_btnFix;
    private Button m_btnSell;
    private Button m_btnStrengthen;
    private Button m_btnRoleDesc;
    private Button m_btnCompare;

    private GameObject m_specialAttr;
    private Text m_specialAttrName;
    private DataGrid m_starList;

    private Image m_imgNextBagPage;
    private DataGrid m_bagDataGrid;
    private BagRenderInfo[] m_bagInfos;
    private DataGrid m_equipDataGrid;
    private SlotRenderInfo[] m_equipInfos;
    private DataGrid m_weaponDataGrid;
    private SlotRenderInfo[] m_weaponInfos;
    private RectTransform m_tranRight;
    private RectTransform m_tranrightDetail;

    private int m_heroDefaultFightRoles = 0;  //默认战斗英雄角色
    private int m_heroSex = 0;                //默认英雄性别

    private Text m_phasedTxt;
    private GameObject m_phasedBtn;
    CDItemData m_itemselect;

    private DataGrid m_decorationGrid;
    private SlotRenderInfo[] m_decorationInfos;

    bool isDecoration;

    

    private Dictionary<string, int> weaponSort = new Dictionary<string, int>() { { "RIFLE", 0 },
                                                                                 { "SUBMACHINE",1},
                                                                                 {"MACHINEGUN",2},
                                                                                 {"SHOTGUN",3},
                                                                                 {"SNIPER",4},
                                                                                 {"PISTOL",5},
                                                                                 {"DAGGER",6},
                                                                                 {"GRENADE",7},
                                                                                 {"FLASHBOMB",8},
                                                                                 {"RIFLEGRENADE",9},
                                                                                 {"BOW",10},
                                                                                 {"OTHER",10}};

    private Dictionary<string, Text> textGroup;



    public PanelDepot()
    {
        SetPanelPrefabPath("UI/Package/PanelDepot2");
        AddPreLoadRes("UI/Package/PanelDepot2", EResType.UI);
//        AddPreLoadRes("UI/Package/ItemDepotRender", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPropRender", EResType.UI);
        AddPreLoadRes("UI/Common/DropList/DropList", EResType.UI);
        AddPreLoadRes("UI/Common/DropList/ItemDropListRender", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON, EResType.Material);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON_CRYSTAL, EResType.Material);
    }

    public override void Init()
    {
        isDecoration = false;
        m_selectMode = false;
        ItemDataManager.bagselect = 0;
        m_weaponClassifyArr = new[]
        {
            new ItemDropInfo { type = "WEAPON", subtype = "ALL", label = "全部"}, 
            new ItemDropInfo { type = "WEAPON", subtype = "MACHINEGUN", label = "机枪"}, 
            new ItemDropInfo { type = "WEAPON", subtype = "PISTOL", label = "手枪" },
            new ItemDropInfo { type = "WEAPON", subtype = "RIFLE", label = "步枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SHOTGUN", label = "散弹枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SNIPER", label = "狙击枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SUBMACHINE", label = "冲锋枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "GRENADE", label = "近战/手雷",sublabel="近战/雷" }
        };

        m_armorClassifyArr = new[]
        {
            new ItemDropInfo { type = "ARMOR", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "ARMOR", subtype = "FLAKHELMET", label = "防弹头盔" }, 
            new ItemDropInfo { type = "ARMOR", subtype = "FLAKJACKET", label = "防弹衣" },
            new ItemDropInfo { type = "ARMOR", subtype = "FLAKBOOT", label = "防弹靴" }, 
        };

        m_partClassifyArr = new ItemDropInfo[] {
            new ItemDropInfo { type = "ACCESSORY", subtype = "ALL", label = "全部" },
            new ItemDropInfo(){ type="ACCESSORY",subtype="1",label="枪口配件",sublabel = "枪口"},
            new ItemDropInfo(){ type="ACCESSORY",subtype="2",label="前握把"},
            new ItemDropInfo(){ type="ACCESSORY",subtype="3",label="弹夹"},
            new ItemDropInfo(){ type="ACCESSORY",subtype="4",label="瞄具"},
        };
        m_fragmentClassifyArr = new[]
        {
            new ItemDropInfo { type = "FRAGMENT", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "MACHINEGUN", label = "机枪" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "PISTOL", label = "手枪" },
            new ItemDropInfo { type = "FRAGMENT", subtype = "RIFLE", label = "步枪" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "SHOTGUN", label = "散弹枪" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "SNIPER", label = "狙击枪" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "SUBMACHINE", label = "冲锋枪" }, 
            new ItemDropInfo { type = "FRAGMENT", subtype = "GRENADE", label = "近战/手雷",sublabel = "近战/雷"}
        };
        m_otherClassifyArr = new[]
        {
            new ItemDropInfo { type = "OTHER", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "OTHER", subtype = "ARMOR", label = "装甲" }, 
            new ItemDropInfo { type = "OTHER", subtype = "GIFT", label = "礼包" },
            new ItemDropInfo { type = "OTHER", subtype = "CLIP", label = "弹夹" }, 
            new ItemDropInfo { type = "OTHER", subtype = "BULLET", label = "子弹" }, 
            new ItemDropInfo { type = "OTHER", subtype = "", label = "其他" }, 
        };
        m_roleClassifyArr = new[]
        {
            new ItemDropInfo { type = "ROLE", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "ROLE", subtype = "WATCHER", label = "守护者" }, 
            new ItemDropInfo { type = "ROLE", subtype = "PATRIOT", label = "爱国者" },
            new ItemDropInfo { type = "ROLE", subtype = "BIO", label = "生化幽灵" ,sublabel="生化"}, 
        };

        m_decorationClassifyArr = new[]{
            new ItemDropInfo { type = "DECORATION", subtype = "1", label = "头部" }, 
            new ItemDropInfo { type = "DECORATION", subtype = "2", label = "脸部" }, 
            new ItemDropInfo { type = "DECORATION", subtype = "3", label = "肩部" },
            new ItemDropInfo { type = "DECORATION", subtype = "4", label = "胸部" }, 
            new ItemDropInfo { type = "DECORATION", subtype = "5", label = "戒指" }, 
        };
        m_selectedType = m_weaponClassifyArr[0].type;
        m_selectedSubType = m_weaponClassifyArr[0].subtype;

        m_tranRight = m_tran.Find("right").GetComponent<RectTransform>();
        m_tranrightDetail = m_tran.Find("rightDetail").GetComponent<RectTransform>();
        m_txtName = m_tran.Find("rightDetail/txtEquipName").GetComponent<Text>();
        m_txtDesc = m_tran.Find("rightDetail/txtEquipDesc").GetComponent<Text>();
        m_imgEquip = m_tran.Find("rightDetail/imgEquip").GetComponent<Image>();
        m_weaponModel = m_tran.Find("rightDetail/weaponModel").GetComponent<RawImage>();
        m_weaponModel.enabled = true;
        m_weaponModel.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;
        m_weaponDrag = m_tran.Find("rightDetail/weaponDrag").gameObject;
        m_imgBroken = m_tran.Find("rightDetail/imgDurability/imgBroken").GetComponent<Image>();
        m_goDurability = m_tran.Find("rightDetail/imgDurability").GetComponent<Image>();
        m_imgDurability = m_tran.Find("rightDetail/imgDurability/value").GetComponent<Image>();
        m_txtDurability = m_tran.Find("rightDetail/imgDurability/Text").GetComponent<Text>();
        m_tranStar = m_tran.Find("rightDetail/starGroup/star");
        m_tranAwaken = m_tran.Find("rightDetail/awakenGroup/avatar");
        m_bagid = m_tran.Find("rightDetail/bagid").GetComponent<Image>();
        m_btnClose = m_tran.Find("rightDetail/btnClose").GetComponent<Button>();
        m_part_group = m_tran.Find("rightDetail/part_group");
        for (int i = 0; i < m_part_group.childCount; i++)
        {
            var render = m_part_group.GetChild(i).gameObject.AddComponent<UIPartItemRender>();
            render.SetStyle(AtlasName.Package, "part_frame_");
//            UGUIClickHandler.Get(render.gameObject).onPointerClick += OnPartItemClick;
        }

        m_dataGridProp = m_tran.Find("rightDetail/list").gameObject.AddComponent<DataGrid>();
        m_dataGridProp.useClickEvent = false;
        m_dataGridProp.autoSelectFirst = false;
        m_dataGridProp.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemPropRender"), typeof(ItemPropRender));
//        m_btnEquip = m_tran.Find("rightDetail/btnEquip").GetComponent<Button>();
//        m_btnContinue = m_tran.Find("rightDetail/btnContinue").GetComponent<Button>();
//        m_btnMerge = m_tran.Find("rightDetail/btnMerge").GetComponent<Button>();
        m_btnFix = m_tran.Find("rightDetail/btnlist/btnFix").GetComponent<Button>();
        m_btnStrengthen = m_tran.Find("rightDetail/btnlist/btnStrengthen").GetComponent<Button>();
        m_btnSell = m_tran.Find("rightDetail/btnlist/btnSell").GetComponent<Button>();
        m_btnDel = m_tran.Find("rightDetail/btnlist/btnDel").GetComponent<Button>();
        m_btnRoleDesc = m_tran.Find("rightDetail/btnlist/btnRoleDesc").GetComponent<Button>();
        m_btnCompare = m_tran.Find("rightDetail/btnlist/btnCompare").GetComponent<Button>();
        m_specialAttr = m_tran.Find("rightDetail/specialAttr").gameObject;
        m_specialAttrName = m_tran.Find("rightDetail/specialAttr/name").GetComponent<Text>();
        m_starList = m_tran.Find("rightDetail/specialAttr").gameObject.AddComponent<DataGrid>();        
        m_starList.SetItemRender(m_tran.Find("rightDetail/specialAttr/starItem").gameObject, typeof(ItemStarRender));
        
        m_goTabbar = m_tran.Find("right/tabbar").gameObject;
        m_tabWeaponTran = m_tran.Find("right/tabbar/tabWeapon");
        m_dataGrid = m_tran.Find("right/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("right/ScrollDataGrid/content/ItemDepotRender").gameObject, typeof(ItemDepotRender));
        m_dataGrid.SetItemClickSound(AudioConst.selectMallItem);
        m_dataGrid.useLoopItems = true;
        m_dataGrid.autoSelectFirst = false;

        m_imgNextBagPage = m_tran.Find("leftBottom/btnNextPage").GetComponent<Image>();
        m_bagDataGrid = m_tran.Find("leftBottom/bagBar/content").gameObject.AddComponent<DataGrid>();
        m_bagDataGrid.SetItemRender(m_tran.Find("leftBottom/bagBar/content/bag").gameObject, typeof(ItemBagRender));
        m_bagDataGrid.onItemSelected += OnBagItemSelected;
        m_bagInfos = new BagRenderInfo[8];
        for (int i = 0; i < m_bagInfos.Length; i++)
        {
            var data = new BagRenderInfo();
            data.bagId = i + 1;
            data.label = (i + 1).ToString();
            m_bagInfos[i] = data;
        }

        m_equipDataGrid = m_tran.Find("leftTop/equipSlot").gameObject.AddComponent<DataGrid>();
        m_equipDataGrid.SetItemRender(m_tran.Find("leftTop/equipSlot/item").gameObject, typeof(ItemEquipSlotRender));
        m_equipDataGrid.onItemSelected += OnEquipSlotSelected;
        m_equipDataGrid.autoSelectFirst = false;
        m_equipInfos = new SlotRenderInfo[5];
        m_equipInfos[0] = new SlotRenderInfo() { bgSpriteName = "shadow03", togglePath = "right/tabbar/tabOther", type = "OTHER", subType = "ARMOR" };
        m_equipInfos[1] = new SlotRenderInfo() { bgSpriteName = "shadow01", togglePath = "right/tabbar/tabOther", type = "OTHER", subType = "ARMOR" };
        m_equipInfos[2] = new SlotRenderInfo() { bgSpriteName = "shadow08", togglePath = "right/tabbar/tabOther", type = "OTHER", subType = "ARMOR" };
        m_equipInfos[3] = new SlotRenderInfo() { bgSpriteName = "shadow05", togglePath = "right/tabbar/tabWeapon", type = "WEAPON", subType = "GRENADE" };
        m_equipInfos[4] = new SlotRenderInfo() { bgSpriteName = "shadow06", togglePath = "right/tabbar/tabWeapon", type = "WEAPON", subType = "GRENADE" };

        m_weaponDataGrid = m_tran.Find("leftBottom/weaponSlot").gameObject.AddComponent<DataGrid>();
        m_weaponDataGrid.SetItemRender(m_tran.Find("leftBottom/weaponSlot/item").gameObject, typeof(ItemEquipSlotRender));
        m_weaponDataGrid.onItemSelected += OnWeaponSlotSelected;
        m_weaponDataGrid.autoSelectFirst = false;
        m_weaponInfos = new SlotRenderInfo[3];
        m_weaponInfos[0] = new SlotRenderInfo() { bgSpriteName = "shadow09", togglePath = "right/tabbar/tabWeapon", type = "WEAPON", subType = "ALL" };
        m_weaponInfos[1] = new SlotRenderInfo() { bgSpriteName = "shadow07", togglePath = "right/tabbar/tabWeapon", type = "WEAPON", subType = "PISTOL" };
        m_weaponInfos[2] = new SlotRenderInfo() { bgSpriteName = "shadow02", togglePath = "right/tabbar/tabWeapon", type = "WEAPON", subType = "GRENADE" };

        m_phasedBtn = m_tran.Find("rightDetail/phasedbtn").gameObject;
        m_phasedTxt = m_tran.Find("rightDetail/phasedText").GetComponent<Text>();

        m_decorationGrid = m_tran.Find("leftTop/decorationSlot").gameObject.AddComponent<DataGrid>();
        m_decorationGrid.SetItemRender(m_tran.Find("leftTop/decorationSlot/item").gameObject, typeof(ItemEquipSlotRender));
        m_decorationGrid.autoSelectFirst = false;
        m_decorationGrid.onItemSelected += OnEquipSlotSelected; 
        m_decorationInfos = new SlotRenderInfo[5];
        
        m_decorationInfos[0] = new SlotRenderInfo() { bgSpriteName = "head", togglePath = "right/tabbar/tabDecoration", type = "OTHER", subType = "HEAD" };
        m_decorationInfos[1] = new SlotRenderInfo() { bgSpriteName = "face", togglePath = "right/tabbar/tabDecoration", type = "OTHER", subType = "FACE" };
        m_decorationInfos[2] = new SlotRenderInfo() { bgSpriteName = "shoudler", togglePath = "right/tabbar/tabDecoration", type = "OTHER", subType = "SHOULDER" };
        m_decorationInfos[3] = new SlotRenderInfo() { bgSpriteName = "yaodai", togglePath = "right/tabbar/tabDecoration", type = "OTHER", subType = "WAIST" };
        m_decorationInfos[4] = new SlotRenderInfo() { bgSpriteName = "rings", togglePath = "right/tabbar/tabDecoration", type = "OTHER", subType = "FINGER" };

        m_tabDecoration = m_tran.Find("right/tabbar/tabDecoration").GetComponent<Toggle>();
        m_tabOther = m_tran.Find("right/tabbar/tabOther").GetComponent<Toggle>();

        m_weaponTxt = m_tran.Find("right/tabbar/tabWeapon/Background/Text").GetComponent<Text>();
        m_partTxt = m_tran.Find("right/tabbar/tabPart/Background/Text").GetComponent<Text>();
        m_fragmentTxt = m_tran.Find("right/tabbar/tabFragment/Background/Text").GetComponent<Text>();
        m_roleTxt = m_tran.Find("right/tabbar/tabRole/Background/Text").GetComponent<Text>();
        m_otherTxt = m_tran.Find("right/tabbar/tabOther/Background/Text").GetComponent<Text>();
        m_decorationTxt = m_tran.Find("right/tabbar/tabDecoration/Background/Text").GetComponent<Text>();

        textGroup = new Dictionary<string, Text>() { { "WEAPON", m_weaponTxt }, { "ACCESSORY", m_partTxt }, { "FRAGMENT", m_fragmentTxt }, { "ROLE", m_roleTxt }, { "OTHER", m_otherTxt }, { "DECORATION", m_decorationTxt } };

    }

    private void resetTabTxt()
    {
        m_weaponTxt.text = "武器";
        m_partTxt.text = "配件";
        m_fragmentTxt.text = "零件";
        m_roleTxt.text = "角色";
        m_otherTxt.text = "其他";
        m_decorationTxt.text = "饰品";
    }

    private void OnEquipSlotSelected(object renderData)
    {
        var vo = renderData as SlotRenderInfo;
        if (vo == null)
            return;

        p_item[] arr = ItemDataManager.GetDepotItemsByMallType("DECORATION", "ALL");
        //if (arr.Length == 0)
        //{
        //    UIManager.ShowTipPanel("您的仓库中没有此类装备，可以前往商城选购", () => UIManager.ShowPanel<PanelMall>(new object[] { "DECORATION", "ALL" }), null, true, false, "前往商城");
        //    return;
        //}

        sortWeapon(arr);
        m_dataGrid.Data = arr;
        m_tran.Find(vo.togglePath).GetComponent<Toggle>().isOn = true;
    }

    private void OnDecorationSlotSelected(object renderData)
    {
        var vo = renderData as SlotRenderInfo;
        if (vo == null)
            return;
        p_item[] arr = ItemDataManager.GetDepotItemsByMallType(vo.type, vo.subType);
    }

    private void OnWeaponSlotSelected(object renderData)
    {
        var vo = renderData as SlotRenderInfo;
        if (vo == null)
            return;

        var pitem = ItemDataManager.GetDepotItemByUid(vo.uid);
        if (pitem != null)
            m_dataGrid.Select(pitem);
        else
        {
            p_item[] arr = ItemDataManager.GetDepotItemsByMallType(vo.type, vo.subType);
            sortWeapon(arr);
            m_dataGrid.Data = arr;
            m_tran.Find(vo.togglePath).GetComponent<Toggle>().isOn = true;
            ShowMallView();
        }
    }

    private void OnBagItemSelected(object renderData)
    {
        var vo = renderData as BagRenderInfo;
        if (vo == null)
            return;
        if (vo.isLock)
        {
            if (!vo.isCanBuy)
                return;

            var config = ConfigManager.GetConfig<ConfigEquipBag>().GetLine(ItemDataManager.m_bags.Length + 1);
            if (config == null)
            {
                UIManager.ShowTipPanel("背包数量已达到上限");
                return;
            }

            UIManager.ShowTipPanel("是否愿意花" + "<color=#00DEFF>" + config.Price + ItemDataManager.GetMoneyName(config.MoneyType) + "</color>" + "购买一个新背包？",
                () => NetLayer.Send(new tos_player_buy_equip_bag()), null, true, false, "购买");
            return;
        }
        ItemDataManager.bagselect = vo.bagId - 1;
        var bag = ItemDataManager.m_bags[vo.bagId-1];
        
        m_equipInfos[0].uid = bag.equip_list.HELMET;
        m_equipInfos[1].uid = bag.equip_list.ARMOR;
        m_equipInfos[2].uid = bag.equip_list.BOOTS;
        m_equipInfos[3].uid = bag.equip_list.GRENADE;
        m_equipInfos[4].uid = bag.equip_list.FLASHBOMB;
        m_equipDataGrid.Data = m_equipInfos;

        m_weaponInfos[0].uid = bag.equip_list.WEAPON1;
        m_weaponInfos[1].uid = bag.equip_list.WEAPON2;
        m_weaponInfos[2].uid = bag.equip_list.DAGGER;
        m_weaponDataGrid.Data = m_weaponInfos;

        

        m_decorationInfos[0].uid = 0;
        m_decorationInfos[1].uid = 0;
        m_decorationInfos[2].uid = 0;
        m_decorationInfos[3].uid = 0;
        m_decorationInfos[4].uid = 0;


        for (int i = 0; i < bag.decorations.Length; i++)
        {
            m_decorationInfos[bag.decorations[i].part-1].uid = bag.decorations[i].decoration;
        }
        
        m_decorationGrid.Data = m_decorationInfos;

        if (bag.equip_list.WEAPON1 != 0)
        {
            ModelDisplay.ConfigWeapon = ItemDataManager.GetItemWeapon(ItemDataManager.GetDepotItemByUid(bag.equip_list.WEAPON1).item_id);
        }
        else
        {
            ModelDisplay.ConfigWeapon = ItemDataManager.GetItemWeapon(ItemDataManager.GetDepotItemByUid(bag.equip_list.WEAPON2).item_id);
        }
        ConfigItemDecorationLine[] decoInfos = new ConfigItemDecorationLine[5];
        for (int i = 0; i < m_decorationInfos.Length; i++)
        {
            decoInfos[i] = ItemDataManager.GetItemByUid(m_decorationInfos[i].uid) as ConfigItemDecorationLine;
        }
        ModelDisplay.ConfigDecoration = decoInfos;


        ModelDisplay.UpdateModel();
        if (m_selectedType == "DECORATION")
        {
            m_dataGrid.UpdateView();
        }
    }




    private void OnPartItemClick(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        if (data == null || data.uid == 0)
        {
            return;
        }
        var vo = target.GetComponent<UIItemRender>().data;
        var pos_key = vo != null && vo.extend is KeyValue<int, int> ? ((KeyValue<int, int>)vo.extend).value.ToString() : "";
        UIManager.ShowPanel<PanelStrengthenMain>(new object[] { "detail#" + data.item_id + "#" + pos_key });
    }

    public override void InitEvent()
    {
//        UGUIClickHandler.Get(m_tran.Find("btnRole")).onPointerClick += delegate { UIManager.ShowPanel<PanelRole>(); };
//        UGUIClickHandler.Get(m_tran.Find("btnPackage")).onPointerClick += (target, data) => UIManager.ShowPanel<PanelPackage>();
//        UGUIClickHandler.Get(m_tran.Find("btnDepot")).onPointerClick += (target, data) => UIManager.ShowPanel<PanelDepot>();
//        UGUIClickHandler.Get(m_btnEquip.gameObject).onPointerClick += OnClickEquipBtn;
//        UGUIClickHandler.Get(m_btnContinue.gameObject).onPointerClick += OnClickContinueBtn;
//        UGUIClickHandler.Get(m_btnMerge.gameObject).onPointerClick += onClickMergeBtn;
        UGUIClickHandler.Get(m_btnFix.gameObject).onPointerClick += OnClickFixBtn;
        UGUIClickHandler.Get(m_btnStrengthen.gameObject).onPointerClick += OnClickStrengthenBtn;
        UGUIClickHandler.Get(m_btnSell.gameObject).onPointerClick += OnClickSellBtn;
        UGUIClickHandler.Get(m_btnDel.gameObject).onPointerClick += OnDelClick;
        UGUIClickHandler.Get(m_btnRoleDesc.gameObject).onPointerClick += OnRoleDescClick;
        UGUIClickHandler.Get(m_btnCompare.gameObject).onPointerClick += OnCompareClick;

        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabWeapon")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabArmor")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabPart")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabFragment")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabRole")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabOther")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("right/tabbar/tabDecoration")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_imgNextBagPage.gameObject).onPointerClick += OnBagPageClick;
        UGUIDragHandler.Get(m_weaponDrag).onDrag += OnDrag;
        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += delegate
        {
            ModelDisplay.ConfigRole = null;
            if (m_weaponInfos[0].uid != 0)
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[0].uid) as ConfigItemWeaponLine;
            }
            else
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[1].uid) as ConfigItemWeaponLine;
            }
            ConfigItemDecorationLine[] decoInfos = new ConfigItemDecorationLine[5];
            for (int i = 0; i < m_decorationInfos.Length; i++)
            {
                decoInfos[i] = ItemDataManager.GetItemByUid(m_decorationInfos[i].uid) as ConfigItemDecorationLine;
            }
            ModelDisplay.ConfigDecoration = decoInfos;
            ModelDisplay.UpdateModel();
            ShowMallView();
        };
        m_dataGrid.onItemSelected += OnItemSelected;
        AddEventListener<p_item>(GameEvent.UI_DOUBLE_CLICK_DEPOTWEAPON, EquipBtnDeal);
        AddEventListener(GameEvent.PROXY_PLAYER_DEFAULT_ROLE_UPDATED, ModelDisplay.UpdateModel);
        UGUIClickHandler.Get(m_phasedBtn).onPointerClick += OnBuyPhased;
        UGUIClickHandler.Get(m_tran.Find("leftTop/switch")).onPointerClick += OnSlotSwitch;
    }

    void OnSlotSwitch(GameObject target, PointerEventData eventData)
    {
        isDecoration = !isDecoration;
        if (isDecoration)
        {
            m_equipDataGrid.gameObject.transform.localPosition = new Vector3(-568, 311f, 0);
            m_decorationGrid.gameObject.transform.localPosition = new Vector3(-468, 311f, 0);
            //if (!m_tabDecoration.isOn)
            //{
            //    OnTabbarClick(m_tabDecoration.gameObject, null);
            //}
            //m_tabDecoration.isOn = true;
            
        }
        else
        {
            m_equipDataGrid.gameObject.transform.localPosition = new Vector3(-468, 311f, 0);
            m_decorationGrid.gameObject.transform.localPosition = new Vector3(-568, 311f, 0);

            //OnTabbarClick(m_tabOther.gameObject, null);
            //m_tabOther.isOn = true;
        }

    }

    void OnBuyPhased(GameObject target, PointerEventData eventData)
    {
        var itemp = ItemDataManager.GetPhasedInfoByUid(m_itemselect.sdata.uid);
        var config = ConfigManager.GetConfig<ConfigMall>().GetLine(itemp.mall_id);
        UIManager.PopPanel<PanelBuyInstallment>(new object[] { config }, true);
    }

    void Toc_player_phased_item_update(toc_player_phased_item_update tdata)
    {
        if (m_itemselect.sdata.uid == tdata.item.item_uid)
        {
            m_phasedTxt.text = "已购买" + tdata.item.phased_number + "期";
            m_dataGrid.UpdateView();
        }
        
    }

    void Toc_player_phased_item_del(toc_player_phased_item_del tdata)
    {
        if (m_itemselect.sdata.uid == tdata.item_uid)
        {
            m_phasedTxt.gameObject.TrySetActive(false);
            m_phasedBtn.TrySetActive(false);
            m_dataGrid.UpdateView();
        }
    }


    private void ShowDetailView()
    {
        TweenAnchoredPosition.Begin(m_tranRight.gameObject, 0.2f, new Vector3(100, 0)).method = UITweener.Method.EaseIn;
        TweenAnchoredPosition.Begin(m_tranrightDetail.gameObject, 0.2f, new Vector3(-480, 0)).method = UITweener.Method.EaseIn;
    }

    private void ShowMallView()
    {
        TweenAnchoredPosition.Begin(m_tranrightDetail.gameObject, 0.2f, new Vector3(100, 0)).method = UITweener.Method.EaseIn;
        TweenAnchoredPosition.Begin(m_tranRight.gameObject, 0.2f, new Vector3(-480, 0)).method = UITweener.Method.EaseIn;
    }

    private void OnDrag(GameObject target, PointerEventData eventData)
    {
        WeaponDisplay.Rotation(eventData.delta.x);
    }

    private void OnBagPageClick(GameObject target, PointerEventData eventData)
    {
        if (Mathf.Abs(m_bagDataGrid.horizonPos) < 0.1f)
        {
            m_imgNextBagPage.rectTransform.localScale = new Vector3(-1, 1, 1);
            m_bagDataGrid.ResetScrollPosition(5, true);
        }
        else
        {
            m_imgNextBagPage.rectTransform.localScale = Vector3.one;
            m_bagDataGrid.ResetScrollPosition(0, true);
        }
    }

    private void OnCompareClick(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        UIManager.PopPanel<PanelCompare>(new object[] { data.item_id, "depot" }, false, this);
    }

    private void OnRoleDescClick(GameObject target, PointerEventData eventData)
    {
        var pitem = m_dataGrid.SelectedData<p_item>();
        if (pitem == null)
            return;
        var itemVo = new CDItemData(pitem);
        if (itemVo.info is ConfigItemRoleLine)
        {
            var desc = itemVo.info.Introduction;
            if (!string.IsNullOrEmpty(desc))
                desc = desc.Replace("<br/>", "\n");
            UIManager.ShowTipMulTextPanel(desc, "人物介绍");
        }
    }

    private void OnDelClick(GameObject target, PointerEventData eventData)
    {
        p_item item = m_dataGrid.SelectedData<p_item>();
        if (item == null)
            return;
        var item_vo = new CDItemData(m_dataGrid.SelectedData<p_item>());
        var isOverdue = item_vo.sdata.time_limit != 0 && item_vo.sdata.time_limit < TimeUtil.GetNowTimeStamp();
        if (!isOverdue)
            return;
        var strengthenInfo = StrengthenModel.Instance.getSStrengthenDataByCode(item_vo.sdata.item_id);
        var part_data = StrengthenModel.Instance.GenWeaponPartCDData(item_vo.sdata, strengthenInfo.state);
        bool hasPart = false;
        for (int i = 0; i < part_data.Length; i++)
        {
            if (part_data[i].info != null)
            {
                hasPart = true;
                break;
            }
        }
        if (!hasPart)
        {
            ItemDataManager.SendDelItem(item_vo.sdata.uid);
        }
        else
        {
            UIManager.ShowTipPanel("该武器镶嵌有配件，清除武器后附件也将消失，确定要清除吗?", () =>
            {
                ItemDataManager.SendDelItem(item_vo.sdata.uid);
            }, null, true, true);
        }

    }

    private void onClickMergeBtn(GameObject target, PointerEventData eventData)
    {

        var data = m_dataGrid.SelectedData<p_item>();
        int result = ItemDataManager.isItemPartsCanMerge(data);
        switch (result)
        {
            case 0:
                NetLayer.Send(new tos_player_use_item() { item_uid = data.uid });
                break;
            case -1:
                TipsManager.Instance.showTips("数量不足");
                break;
            case -2:
                TipsManager.Instance.showTips("已经拥有永久武器不能合成");
                break;
        }
    }

    private void OnClickSellBtn(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        if (data == null)
        {
            return;
        }
        var cost_info = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(Mathf.Max(new CDItemData(data).GetQuality(), 1));
        UIManager.ShowTipPanel(string.Format("卖给系统可获得{0}金币？", cost_info != null ? cost_info.BuybackCoin : 0), () => { StrengthenModel.Instance.TosSellWeaponPart(new int[] { data.uid }); }, null, true, true);
    }

    public override void OnShow()
    {
        m_weaponModel.texture = WeaponDisplay.RenderTexture;
        WeaponDisplay.Visible = true;
        ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.2f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
        m_lastToggle = null;
        UIManager.HideDropList();

        m_selectMode = HasParams();

        NetLayer.Send(new tos_player_item_equip_data());
        if (m_selectMode)
        {
            m_goTabbar.TrySetActive(false);
            var arr = m_params[0] as p_item[];
            sortWeapon(arr);
            m_dataGrid.Data = arr;
        }
        else
        {
            m_selectedSubType = "ALL";
            m_selectedType = "WEAPON";
            m_goTabbar.TrySetActive(true);
            m_tabWeaponTran.GetComponent<Toggle>().isOn = true;
            NetLayer.Send(new tos_player_item_equip_data());
        }
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.CheckChosen(panelHall.m_btgDepot);

        m_imgNextBagPage.rectTransform.localScale = Vector3.one;
        m_tranRight.anchoredPosition = new Vector2(-480, 0);
        m_tranrightDetail.anchoredPosition = new Vector2(100, 0);
    }

    /// <summary>fjs  20151030
    /// 对背包中的枪械 进行排序 过期的排在后面
    /// </summary>
    /// <param name="arr"></param>
    private void sortWeapon(p_item[] arr)
    {
        if (arr == null)
            return;
        Array.Sort<p_item>(arr, (a, b) =>
        {
            var result = 0;
            var a_value = a != null && TimeUtil.GetRemainTimeType(a.time_limit) != -1 ? 1 : 0;
            var b_value = b != null && TimeUtil.GetRemainTimeType(b.time_limit) != -1 ? 1 : 0;
            var configa = ItemDataManager.GetItemWeapon(a.item_id);
            var configb = ItemDataManager.GetItemWeapon(b.item_id);
            result = b_value - a_value;
            if (result == 0&&configa!=null&&configb!=null)
            {
                var sa = weaponSort[configa.WeaponType];
                var sb = weaponSort[configb.WeaponType];
                result = sa - sb;
            }
            if (result == 0 && configa != null && configb != null)
            {

                result = configb.RareType - configa.RareType;
            }
            if (result == 0)
            {
                result = a.item_id - b.item_id;
            }
            return result;
        });
    }



    public override void OnHide()
    {
        m_selectMode = false;
        ModelDisplay.ConfigRole = null;
        ModelDisplay.ConfigWeapon = null;
        ModelDisplay.ConfigDecoration = null;
        ModelDisplay.UpdateModel();

        WeaponDisplay.Visible = false;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
        m_dataGrid = null;
    }

    public override void Update()
    {
    }

    private void OnClickFixBtn(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        if (data == null)
            return;

        if (data.durability == ItemDataManager.MAX_DURABILITY)
            TipsManager.Instance.showTips("无需修复");
        else
            UIManager.PopPanel<PanelFix>(new object[] { data.uid }, true, this);
    }

    private void OnClickEquipBtn(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        EquipBtnDeal(data);
    }
    private void EquipBtnDeal(p_item data)
    {
        if (data == null)
        {
            return;
        }
        if (data.is_arsenal)
        {
            TipsManager.Instance.showTips("该武器已经借出，暂时不能装备");
            return;
        }
        ConfigItemLine info = data != null ? ItemDataManager.GetItem(data.item_id) : null;
        if (info.Type == GameConst.ITEM_TYPE_MATERIAL && info.SubType == GameConst.WEAPON_SUBTYPE_ACCESSORY)
        {
            UIManager.ShowTipPanel(string.Format("鉴定需要{0}金币？", ConfigMisc.GetInt("accessory_meterial_test_cost")), () => { StrengthenModel.Instance.TosTestPartMeterial(new int[] { data.uid }); }, null, true, true);
        }
        else if (info.Type == GameConst.ITEM_TYPE_EQUIP)
        {
            if (info.SubType == GameConst.WEAPON_SUBTYPE_ACCESSORY)
            {
                UIManager.ShowPanel<PanelStrengthenMain>();
            }
            else if (m_selectMode)
            {
                NetLayer.Send(new tos_player_equip_item() { group = (int)m_params[1], uid = data.uid });
                UIManager.ShowPanel<PanelPackage>();
            }
            else
            {
                var isOverdue = data.time_limit != 0 && data.time_limit < TimeUtil.GetNowTimeStamp();
                if (isOverdue)
                {
                    if (ItemDataManager.GetPhasedInfoByUid(data.uid) != null)
                    {
                        var item = ConfigManager.GetConfig<ConfigMall>().GetLine(ItemDataManager.GetPhasedInfoByUid(data.uid).mall_id);
                        UIManager.PopPanel<PanelBuyInstallment>(new object[] { item }, true);
                    }
                    else
                    {
                        var item = ConfigManager.GetConfig<ConfigMall>().GetMallLine(data.item_id);
                        if (item != null && ItemDataManager.IsSale(item))
                            UIManager.PopPanel<PanelBuy>(new object[] { item, "continue" }, true);
                        else
                            TipsManager.Instance.showTips("商品不可购买");
                    }
                }
                else
                {
                    if (ItemDataManager.GetItemByUid(data.uid).SubType != "DECORATION")
                    {
                        NetLayer.Send(new tos_player_equip_item() { group = m_bagDataGrid.SelectedData<BagRenderInfo>().bagId, uid = data.uid });
                    }
                    else
                    {
                        for (int i = 0; i < ItemDataManager.m_bags[ItemDataManager.bagselect].decorations.Length; i++)
                        {
                            var config = ItemDataManager.GetDepotItemByUid(ItemDataManager.m_bags[ItemDataManager.bagselect].decorations[i].decoration);
                            if (config != null)
                            {
                                if (config.uid == data.uid)
                                {
                                    var deco = ItemDataManager.GetItemByUid(data.uid) as ConfigItemDecorationLine;
                                    NetLayer.Send(new tos_player_unequip_decoration() { group = ItemDataManager.bagselect + 1, part = int.Parse(deco.Part) });
                                    return;
                                }
                            }
                           
                        }
                        NetLayer.Send(new tos_player_equip_item() { group = m_bagDataGrid.SelectedData<BagRenderInfo>().bagId, uid = data.uid });
                    }
                }
//                UIManager.PopPanel<PanelChooseGroup>(new object[] { data.uid }, true);
            }
        }
        else if (info.Type == GameConst.ITEM_TYPE_ITEMS && info.SubType == GameConst.ITEMS_SUBTYPE_MERGE)
        {
            int result = ItemDataManager.isItemPartsCanMerge(data);
            switch (result)
            {
                case 0:
                    NetLayer.Send(new tos_player_use_item() { item_uid = data.uid });
                    break;
                case -1:
                    TipsManager.Instance.showTips("数量不足");
                    break;
                case -2:
                    TipsManager.Instance.showTips("已经拥有永久武器不能合成");
                    break;
            }
        }
        else if (info.Type == GameConst.ITEM_TYPE_ITEMS && !string.IsNullOrEmpty(info.UseType))
        {
            if (info.SubType == GameConst.ITEMS_SUBTYPE_RENAMECARD && info.UseType == "rename_role")
            {
                UIManager.PopPanel<PanelRename>(new object[] {data}, true, this);
            }
            else if (info.SubType == GameConst.ITEMS_SUBTYPE_RENAMECARD && info.UseType == "rename_corps")
            {
                if (!CorpsDataManager.IsJoinCorps() || !CorpsDataManager.IsLeader())
                {
                    TipsManager.Instance.showTips("只有战队队长才有权修改战队信息");
                }
                else
                {
                    UIManager.PopPanel<PanelCorpsMotify>(null, true);
                }                
            }
            else
            {
                var tips = "";
                if (info.SubType == GameConst.ITEMS_SUBTYPE_BUFFCARD)
                {
                    tips = "即将开启个人专属24小时双倍掉落，你准备好了吗？";
                }
                if (!string.IsNullOrEmpty(tips))
                {
                    UIManager.ShowTipPanel(tips, () =>
                    {
                        if (info.UseType == "animal_weapons")
                        {
                            ItemDataManager.ResetTempAddedRecord(ItemDataManager.UPDATE_REASON_EQUIP_BOX);
                        }
                        NetLayer.Send(new tos_player_use_item() {item_uid = data.uid});
                    }, null, true, true);
                }
                else
                {
                    if (info.UseType == "animal_weapons")
                    {
                        ItemDataManager.ResetTempAddedRecord(ItemDataManager.UPDATE_REASON_EQUIP_BOX);
                    }
                    if (info.SubType == GameConst.ITEMS_SUBTYPE_MERGE)
                    {
                        return;
                    }
                    ConfigItemOtherLine cfg = info as ConfigItemOtherLine;
                    PanelRewardItemTipsWithEffect.isSkip = cfg.EffectType.Length == 2;
                    NetLayer.Send(new tos_player_use_item() {item_uid = data.uid});
                }
            }
        }
        else if (info.Type == GameConst.ITEM_TYPE_ROLE)
        {
            var configRole = info as ConfigItemRoleLine;
            if (configRole != null)
            {
                var isOverdue = data.time_limit != 0 && data.time_limit < TimeUtil.GetNowTimeStamp();
                if (!isOverdue)
                {
                    
                    if (configRole.SubType != GameConst.ITEMS_SUBTYPE_HERO)
                    {
                        ModelDisplay.ModelDefaultCamp = (WorldManager.CAMP_DEFINE)Enum.Parse(typeof(WorldManager.CAMP_DEFINE), configRole.RoleCamp.ToString());
                        PlayerSystem.TosSetDefaultRole(data.uid, configRole.RoleCamp);
                    }
                    else
                    {
                        PlayerSystem.TosSetHeroSex(PlayerSystem.heroSex == 0 ? 1 : 0);
                    }
                }
                else
                {
                    var item = ConfigManager.GetConfig<ConfigMall>().GetMallLine(data.item_id);
                    if (item != null && ItemDataManager.IsSale(item))
                        UIManager.PopPanel<PanelBuy>(new object[] { item, "continue" }, true);
                    else
                        TipsManager.Instance.showTips("商品不可购买");
                }
            }
        }
        else
        {
            //UIManager.ShowTipPanel("未知物品类型使用方式");
        }

    }

    private void OnClickContinueBtn(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        var item = ConfigManager.GetConfig<ConfigMall>().GetMallLine(data.item_id);
        if (item != null && ItemDataManager.IsSale(item))
            UIManager.PopPanel<PanelBuy>(new object[] { item, "continue" }, true);
        else
            TipsManager.Instance.showTips("商品不可购买");
    }
    private void OnClickStrengthenBtn(GameObject target, PointerEventData eventData)
    {
        var data = m_dataGrid.SelectedData<p_item>();
        UIManager.ShowPanel<PanelStrengthenMain>(new object[] { "detail#" + data.item_id });
    }

    private void UpdateModel(object renderData)
    {
        if (!(renderData is p_item))
            return;
        var item_vo = new CDItemData(renderData as p_item);
        if (item_vo.info is ConfigItemWeaponLine && (item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2))
        {
            var weapon = item_vo.info as ConfigItemWeaponLine;
            ModelDisplay.ConfigRole = null;
            
            ConfigItemDecorationLine[] decoInfos = new ConfigItemDecorationLine[5];
            for (int i = 0; i < m_decorationInfos.Length; i++)
            {
                decoInfos[i] = ItemDataManager.GetItemByUid(m_decorationInfos[i].uid) as ConfigItemDecorationLine;
            }
            ModelDisplay.ConfigDecoration = decoInfos;


            ModelDisplay.ConfigWeapon = weapon;
            ModelDisplay.UpdateModel();
        }
        else if (item_vo.info is ConfigItemRoleLine)
        {
            ModelDisplay.ConfigRole = item_vo.info as ConfigItemRoleLine;
            ModelDisplay.ConfigWeapon = null;

            ConfigItemDecorationLine[] decoInfos = new ConfigItemDecorationLine[5];
            for (int i = 0; i < m_decorationInfos.Length; i++)
            {
                decoInfos[i] = ItemDataManager.GetItemByUid(m_decorationInfos[i].uid) as ConfigItemDecorationLine;
            }
            ModelDisplay.ConfigDecoration = decoInfos;

            if ( m_weaponInfos[0].uid != 0)
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[0].uid) as ConfigItemWeaponLine;
            }
            else
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[1].uid) as ConfigItemWeaponLine;
            }

            ModelDisplay.UpdateModel();

        }
        else if (item_vo.info is ConfigItemDecorationLine)
        {
            var configs = ModelDisplay.ConfigDecoration;
            var deco = item_vo.info as ConfigItemDecorationLine;
            configs[int.Parse(deco.Part) - 1] = deco;
            ModelDisplay.ConfigRole = null;
            if (m_weaponInfos[0].uid != 0)
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[0].uid) as ConfigItemWeaponLine;
            }
            else
            {
                ModelDisplay.ConfigWeapon = ItemDataManager.GetItemByUid(m_weaponInfos[1].uid) as ConfigItemWeaponLine;
            }
            ModelDisplay.ConfigDecoration = configs;
            ModelDisplay.UpdateModel();
        }
    }

    private void OnItemSelected(object renderData)
    {
        UIManager.HideDropList();
        m_tranStar.parent.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
        if (renderData == null)
        {
            m_txtName.text = "";
            m_imgEquip.enabled = false;
//            m_btnContinue.gameObject.TrySetActive(false);
//            m_btnEquip.gameObject.TrySetActive(false);
            m_btnStrengthen.gameObject.TrySetActive(false);
            m_btnSell.gameObject.TrySetActive(false);
            m_dataGridProp.Data = new object[0];
            m_tranStar.parent.gameObject.TrySetActive(false);
            m_tranAwaken.parent.gameObject.TrySetActive(false);
            m_part_group.gameObject.TrySetActive(false);
            m_btnFix.gameObject.TrySetActive(false);
            m_goDurability.gameObject.TrySetActive(false);
//            m_btnMerge.gameObject.TrySetActive(false);
            return;
        }

        ShowDetailView();

//        Util.SetNormalShader(m_btnEquip);

        UpdateModel(renderData);
        var item_vo = new CDItemData(renderData as p_item);
        m_itemselect = item_vo;
        m_txtName.text = item_vo.info.DispName;
        if (item_vo.info is ConfigItemWeaponLine && 
            (item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 ||
             item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_DAGGER || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_GRENADE ||
             item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_FLASHBOMB))
        {
            var weapon = item_vo.info as ConfigItemWeaponLine;
            m_imgEquip.enabled = false;
            m_weaponModel.enabled = true;
            WeaponDisplay.UpdateMode(weapon);
        }
        else if (item_vo.info is ConfigItemRoleLine)
        {
            m_weaponModel.enabled = false;
            m_imgEquip.enabled = true;
        }
        else
        {
            m_weaponModel.enabled = false;
            m_imgEquip.enabled = true;
        }

        m_btnCompare.gameObject.TrySetActive(item_vo.info is ConfigItemWeaponLine);

        if (!string.IsNullOrEmpty(item_vo.info.Icon))
        {
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(item_vo.info.Icon),item_vo.info.SubType);
            m_imgEquip.SetNativeSize();
//            m_imgEquip.enabled = true;
//            m_imgEquip.rectTransform.localRotation = Quaternion.Euler(0, 0, item_vo.info.IconRotation);
//            m_imgEquip.rectTransform.localScale = new Vector3(0.2f, 0.2f, 1);
//            if (item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2")
//            {
//                m_imgEquip.rectTransform.localPosition = new Vector3(-5, 150, 0);
//            }
//            else
//            {
//                m_imgEquip.rectTransform.localPosition = new Vector3(-5, 165, 0);
//            }
//            TweenScale.Begin(m_imgEquip.gameObject, 0.1f, new Vector3(0.9f, 0.9f, 1));
        }
        else
        {
            m_imgEquip.enabled = false;
        }
        if ((item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 ||
             item_vo.info.SubType == GameConst.GAME_RULE_DAGGER) && ItemDataManager.IsBagHas(item_vo.sdata.uid))
        {
            m_bagid.enabled = true;
            m_bagid.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "bag" + ItemDataManager.GetBagGroupId(item_vo.sdata.uid)));
        }
        else
        {
            m_bagid.enabled = false;
        }
        m_btnFix.gameObject.TrySetActive(item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2");
        m_goDurability.gameObject.TrySetActive(item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2");
//        m_btnMerge.gameObject.TrySetActive(item_vo.info.Type == GameConst.ITEM_TYPE_ITEMS && item_vo.info.SubType == GameConst.ITEMS_SUBTYPE_MERGE);
        m_imgDurability.fillAmount = 1f * item_vo.sdata.durability / ItemDataManager.MAX_DURABILITY;
        m_txtDurability.text = "耐久度：" + item_vo.sdata.durability + "/" + ItemDataManager.MAX_DURABILITY;
        m_imgBroken.gameObject.TrySetActive(item_vo.sdata.durability == 0);

        var show_strengthen = item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2";
        if (show_strengthen)
        {
            var strengthenInfo = StrengthenModel.Instance.getSStrengthenDataByCode(item_vo.sdata.item_id);
            m_tranAwaken.parent.gameObject.TrySetActive(strengthenInfo.awaken_data != null && strengthenInfo.awaken_data.level > 0);
            var show_num = strengthenInfo.awaken_data != null ? Mathf.Ceil(strengthenInfo.awaken_data.level / 2f) : 0;
            for (int i = 0; m_tranAwaken.parent.gameObject.activeSelf && i < m_tranAwaken.childCount; i++)
            {
                var img = m_tranAwaken.GetChild(i).GetComponent<Image>();
                img.gameObject.TrySetActive(i < show_num);
                if (img.gameObject.activeSelf)
                {
                    img.fillAmount = strengthenInfo.awaken_data.level < (i + 1) * 2 ? 0.5f : 1f;
                }
            }
            m_tranStar.parent.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen && (strengthenInfo.awaken_data == null || strengthenInfo.awaken_data.level == 0));
            for (int i = 0; m_tranStar.parent.gameObject.activeSelf && i < m_tranStar.childCount; i++)
            {
                m_tranStar.GetChild(i).gameObject.TrySetActive(i < strengthenInfo.state);
            }
            m_part_group.gameObject.TrySetActive(true);
            var part_data = StrengthenModel.Instance.GenWeaponPartCDData(item_vo.sdata, strengthenInfo.state);
            for (int i = 0; i < m_part_group.childCount; i++)
            {
                var render = m_part_group.GetChild(i).GetComponent<UIPartItemRender>();
                var part_vo = part_data != null && i < part_data.Length ? part_data[i] : null;
                render.SetData(part_vo);
            }
        }
        else
        {
            m_tranAwaken.parent.gameObject.TrySetActive(false);
            m_tranStar.parent.gameObject.TrySetActive(false);
            m_part_group.gameObject.TrySetActive(false);
        }
        m_txtDesc.text = item_vo.info.Desc;
        var isOverdue = item_vo.sdata.time_limit != 0 && item_vo.sdata.time_limit < TimeUtil.GetNowTimeStamp();
        m_btnDel.gameObject.TrySetActive(isOverdue&&ItemDataManager.GetPhasedInfoByUid(item_vo.sdata.uid)==null);
        m_btnRoleDesc.gameObject.TrySetActive(item_vo.info is ConfigItemRoleLine);
        
        m_btnStrengthen.gameObject.TrySetActive(!isOverdue && (item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2));
        m_btnSell.gameObject.TrySetActive(!isOverdue && item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_ACCESSORY);
        if (item_vo.info is ConfigItemWeaponLine)
        {
            m_dataGridProp.Data = ItemDataManager.GetPropRenderInfos(item_vo.info as ConfigItemWeaponLine, true, true);
        }
        else if (item_vo.info is ConfigItemAccessoriesLine)
        {
            m_dataGridProp.Data = ItemDataManager.GetPropRenderInfos(item_vo.sdata);
        }
        else
        {
            m_dataGridProp.Data = new object[0];
        }

        var itemWeapon = item_vo.info as ConfigItemWeaponLine;
        if (itemWeapon != null && itemWeapon.SpecialAtt.Length >= 2)
        {
            var config = ConfigManager.GetConfig<ConfigWeaponSpecialAttr>().GetLine(itemWeapon.SpecialAtt[0]);
            m_specialAttrName.text = config.Name;
            var strs = new string[itemWeapon.SpecialAtt[1]];
            for (int i = 0; i < strs.Length;i++ )
            {
                strs[i] = config.Icon;
            }
            m_starList.Data = strs;
            m_specialAttr.TrySetActive(true);
        }
        else
            m_specialAttr.TrySetActive(false);


        var itemp = ItemDataManager.GetPhasedInfoByUid(item_vo.sdata.uid);
        if (itemp != null)
        {
            m_phasedTxt.gameObject.TrySetActive(true);
            m_phasedBtn.TrySetActive(true);
            m_phasedTxt.text = "已购买" + itemp.phased_number + "期";

        }
        else
        {
            m_phasedTxt.gameObject.TrySetActive(false);
            m_phasedBtn.TrySetActive(false);
        }
    }

    private void OnDropListItemSelected(object renderData)
    {
        UIManager.HideDropList();
        var data = renderData as ItemDropInfo;
        if (data == null)
            return;
        p_item[] arr = ItemDataManager.GetDepotItemsByMallType(data.type, data.subtype);
        sortWeapon(arr);
        m_dataGrid.Data = arr;
        m_selectedType = data.type;
        m_selectedSubType = data.subtype;
        if (data.sublabel == null)
        {
            textGroup[data.type].text = data.label;
        }
        else
        {
            textGroup[data.type].text = data.sublabel;
        }
    }



    private void OnTabbarClick(GameObject target, PointerEventData data)
    {
        var toggle = target.GetComponent<Toggle>();
        var secondClick = toggle.isOn && toggle == m_lastToggle;
        ItemDropInfo[] dropListData = null;

        switch (target.name)
        {
            case "tabWeapon": m_selectedType = "WEAPON"; m_selectedSubType = "ALL"; break;
            case "tabArmor": m_selectedType = "ARMOR"; m_selectedSubType = "ALL"; break;
            case "tabPart": m_selectedType = "ACCESSORY"; m_selectedSubType = "ALL"; break;
            case "tabOther": m_selectedType = "OTHER"; m_selectedSubType = "ALL"; break;
            case "tabFragment": m_selectedType = "FRAGMENT"; m_selectedSubType = "ALL"; break;
            case "tabRole": m_selectedType = "ROLE"; m_selectedSubType = "ALL"; break;
            case "tabDecoration": m_selectedType = "DECORATION"; m_selectedSubType = "ALL"; break;
        }

        if (!secondClick)
        {
            resetTabTxt();
            m_dataGrid.ResetScrollPosition();
            if (m_selectedType == "DECORATION")
            {
                if (!isDecoration)
                {
                    m_equipDataGrid.gameObject.transform.localPosition = new Vector3(-568, 311f, 0);
                    m_decorationGrid.gameObject.transform.localPosition = new Vector3(-468, 311f, 0);
                    isDecoration = true;
                }
            }
        }

        if (secondClick && m_selectedType == "WEAPON")
            dropListData = m_weaponClassifyArr;
        else if (secondClick && m_selectedType == "ARMOR")
            dropListData = m_armorClassifyArr;
        else if (secondClick && m_selectedType == "ACCESSORY")
            dropListData = m_partClassifyArr;
        else if (secondClick && m_selectedType == "FRAGMENT")
            dropListData = m_fragmentClassifyArr;
        else if (secondClick && m_selectedType == "OTHER")
            dropListData = m_otherClassifyArr;
        else if (secondClick && m_selectedType == "ROLE")
            dropListData = m_roleClassifyArr;
        else if (secondClick && m_selectedType == "DECORATION")
        {
            dropListData = m_decorationClassifyArr;
        }
        else
        {
            p_item[] arr = ItemDataManager.GetDepotItemsByMallType(m_selectedType, m_selectedSubType);

            sortWeapon(arr);
            m_dataGrid.Data = arr;
        }


        if (secondClick && dropListData != null)
            UIManager.ShowDropList(dropListData, OnDropListItemSelected, m_tran, target);
        else
            UIManager.HideDropList();

        m_lastToggle = toggle;
    }

    private void UpdateBagBar()
    {
        var bags = ItemDataManager.m_bags;
        for (int i = 0; i < m_bagInfos.Length; i++)
        {
            var data = m_bagInfos[i];
            data.isLock = i >= bags.Length;
            data.isCanBuy = i == bags.Length;
        }

        m_bagDataGrid.UpdateData = m_bagInfos;
    }

    public void UpdateView(int uid = 0)
    {
        UpdateBagBar();
        if (!m_selectMode)
        {
            var arr = ItemDataManager.GetDepotItemsByMallType(m_selectedType, m_selectedSubType);
            sortWeapon(arr);
            m_dataGrid.Data = arr;
//            if (uid != 0)
//            {
//                for (int i = 0; i < arr.Length; i++)
//                {
//                    if (arr[i].uid == uid)
//                    {
//                        m_dataGrid.Select(arr[i]);
//                        break;
//                    }
//                }
//            }
        }
        else
        {
            var item = ItemDataManager.GetItem(m_itemid);
            p_item[] arr = null;
            if (item == null)
            {
                arr = ItemDataManager.GetDepotItemsByMallType(m_selectedType, m_selectedSubType);
            }
            else
            {
                arr = ItemDataManager.GetDepotItemsByType(item.Type, item.SubType);
            }
            sortWeapon(arr);
            m_dataGrid.Data = arr;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].uid == uid)
                {
                    m_dataGrid.Select(arr[i]);
                    break;
                }
            }
        }
    }

    void Toc_item_new(toc_item_new data)
    {
        UpdateView();
    }

    void Toc_item_update(toc_item_update data)
    {
        m_itemid = data.item.item_id;
        UpdateView(data.item.uid);
    }

    void Toc_item_del(toc_item_del data)
    {
        UpdateView();
    }

    void Toc_player_item_equip_data(toc_player_item_equip_data data)
    {
        UpdateView();
        m_bagDataGrid.Select(0);
        m_bagDataGrid.ResetScrollPosition();
    }

    void Toc_player_equip_item(toc_player_equip_item data)
    {
        UpdateView();
    }

    void Toc_player_unequip_decoration(toc_player_unequip_decoration data)
    {
        UpdateView();
    }


    void Toc_player_equip_bag(toc_player_equip_bag data)
    {
        UpdateView();
    }

    void Toc_player_buy_equip_bag(toc_player_buy_equip_bag data)
    {
        UpdateBagBar();
    }

    void Toc_player_test_accessory_meterial(toc_player_test_accessory_meterial data)
    {
        if (data.accessory_list != null && data.accessory_list.Length > 0)
        {
            var sdata = ItemDataManager.GetDepotItemByUid(data.accessory_list[0].uid);
            m_dataGrid.ShowItemOnTop(sdata);
            UIEffect.ShowEffect(m_imgEquip.transform, EffectConst.UI_HALL_CLICK, .5f);
            var info = ItemDataManager.GetItem(sdata.item_id) as ConfigItemAccessoriesLine;
            var add_props = info != null ? info.GetAddProp() : null;
            if (add_props != null && add_props.Length > 0)
            {
                var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
                var result = new List<string>();
                for (int i = 0; add_props != null && i < add_props.Length; i++)
                {
                    if (sdata.accessory_values != null && i < sdata.accessory_values.Length)
                    {
                        if (add_props[i] != null)
                        {
                            var add_value = Mathf.Abs(sdata.accessory_values[i]);
                            result.Add(string.Format("{0}+{1}", prop_config.GetDispPropName(info.DispProps[i]), add_props[i].AddType == 1 ? (add_value / 10f).ToString("0.##") + "%" : add_value.ToString()));
                        }
                    }
                }
                if (result.Count > 0)
                {
                    FloatTextUIEffect.Instance.Play(result.ToArray(), m_imgEquip.transform, 18, 0.5f, 0.25f);
                }
            }
        }
    }

    void Toc_player_repair_weapons(toc_player_repair_weapons data)
    {
        var pitem = m_dataGrid.SelectedData<p_item>();
        var uid = pitem != null ? pitem.uid : 0;
        UpdateView(uid);

        TipsManager.Instance.showTips("修理成功");
        AudioManager.PlayUISound(AudioConst.levelUp);
    }

    void Toc_player_set_hero_sex(toc_player_set_hero_sex proto)
    {

    }

    private void Toc_player_set_default_role(toc_player_set_default_role data)
    {
        m_dataGrid.UpdateView();
        
    }

    void Toc_corps_change_name()
    {
        TipsManager.Instance.showTips("战队名修改成功");
    }
}
