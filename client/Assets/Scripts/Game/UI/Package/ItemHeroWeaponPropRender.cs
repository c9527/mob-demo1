﻿using UnityEngine.UI;

public class ItemHeroWeaponPropRender : ItemRender
{
    private Text m_text;
    public override void Awake()
    {
        m_text = transform.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_text.text = data.ToString();
    }
}
