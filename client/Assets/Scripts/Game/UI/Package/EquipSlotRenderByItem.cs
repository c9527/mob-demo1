﻿using UnityEngine;
using UnityEngine.UI;

public class SlotRenderInfoByItem
{
    public int itemid;
    public string bgSpriteName;
    public string type;
    public string subType;
    public string togglePath;
}

public class ItemEquipSlotRenderByItem : ItemRender
{
    private Image m_imgEquip;
    private Image m_imgPlus;
    private Vector2 m_originScale;

    public override void Awake()
    {
        var tran = transform;
        m_imgEquip = tran.Find("Image").GetComponent<Image>();
        m_originScale = m_imgEquip.rectTransform.localScale;
        if (tran.Find("imgPlus"))
            m_imgPlus = tran.Find("imgPlus").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var vo = data as SlotRenderInfoByItem;
        if (vo == null)
            return;

        var item = vo.itemid > 0 ? ItemDataManager.GetItem(vo.itemid) : null;
        if (item != null)
        {
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(item.Icon));
            m_imgEquip.rectTransform.localScale = m_originScale;
            m_imgEquip.gameObject.TrySetActive(true);
        }
        else
        {
            //m_imgEquip.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, vo.bgSpriteName));
            //m_imgEquip.rectTransform.localScale = Vector2.one;
            m_imgEquip.gameObject.TrySetActive(false);
        }

        if (m_imgPlus)
            m_imgPlus.enabled = item == null;
        m_imgEquip.SetNativeSize();
    }
}

