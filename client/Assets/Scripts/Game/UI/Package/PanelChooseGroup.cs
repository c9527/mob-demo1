﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelChooseGroup : BasePanel
{
    private Text m_txtName;
    private Text m_txtBagName;
    private Image m_imgEquip;

    private int m_selectedGroup;
    private int m_uid;
    private static Queue<int> m_queue = new Queue<int>();
    private static bool isShowing = false;    
    public PanelChooseGroup()
    {
        SetPanelPrefabPath("UI/Package/PanelChooseGroup");
        AddPreLoadRes("UI/Package/PanelChooseGroup", EResType.UI);
    }

    public override void Init()
    {
        m_txtName = m_tran.Find("background/txtName").GetComponent<Text>();
        m_txtBagName = m_tran.Find("background/txtBagName").GetComponent<Text>();
        m_imgEquip = m_tran.Find("background/imgEquip").GetComponent<Image>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnLeft")).onPointerClick += OnLeftBtnClick;
        UGUIClickHandler.Get(m_tran.Find("background/btnRight")).onPointerClick += OnRightBtnClick;
        UGUIClickHandler.Get(m_tran.Find("background/btnEquip")).onPointerClick += OnEquipBtnClick;
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    private void OnEquipBtnClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_equip_item() { group = m_selectedGroup, uid = m_uid });
        HidePanel();        
    }

    private void OnLeftBtnClick(GameObject target, PointerEventData eventData)
    {
        m_selectedGroup = Mathf.Max(1, m_selectedGroup - 1);
        m_txtBagName.text = "背包"+m_selectedGroup;
    }

    private void OnRightBtnClick(GameObject target, PointerEventData eventData)
    {
        m_selectedGroup = Mathf.Min(m_selectedGroup + 1, ItemDataManager.m_bags.Length);
        m_txtBagName.text = "背包" + m_selectedGroup;
    }

    public static void DoShow(object[] paramas = null, bool addMask = false, BasePanel parentPanel = null, LayerType layer = LayerType.POP)
    {
        if(paramas!=null&& paramas.Length>0)        
            m_queue.Enqueue((int)paramas[0]);        
        if (isShowing)
            return;
        isShowing = true;
        UIManager.PopPanel<PanelChooseGroup>(null,addMask,parentPanel,layer);
    }

    public override void OnShow()
    {
        p_item pitem;
        if(m_queue.Count==0)
        {
            if (HasParams())
                pitem = ItemDataManager.GetDepotItemByUid((int)m_params[0]);
            else return;
        }
        else
        {
            pitem = ItemDataManager.GetDepotItemByUid(m_queue.Dequeue());
        }        
        var item = ItemDataManager.GetItem(pitem.item_id);
        m_uid = pitem.uid;
        m_imgEquip.SetSprite(ResourceManager.LoadIcon(item.Icon),item.SubType);
        m_imgEquip.SetNativeSize();

//        m_txtName.color = ItemDataManager.GetItemColor(item.RareType);
        m_txtName.text = string.Format("{0} ({1})",item.DispName, TimeUtil.GetRemainTimeString(pitem.time_limit));

        m_selectedGroup = 1;
        m_txtBagName.text = "背包" + m_selectedGroup;
    }

    public override void OnHide()
    {        
        isShowing = false;
        if(m_queue.Count>0)
        {
            PanelChooseGroup.DoShow(null,true);
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}