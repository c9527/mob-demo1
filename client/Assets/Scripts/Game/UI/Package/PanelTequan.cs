﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;


public class PanelTequan : BasePanel
{

    public class ItemTequan : ItemRender
    {
        Text m_text;

        public override void Awake()
        {
            m_text = transform.Find("Text").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            var info = data as string;
            m_text.text = info;

        }
    }


    public PanelTequan()
    {
        SetPanelPrefabPath("UI/Package/PanelTequan");
        AddPreLoadRes("UI/Package/PanelTequan", EResType.UI);
        AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/BattleNew", EResType.Atlas);
    }

    Text m_gunName;
    Image m_gunHead;
    Image m_killIcon;
    DataGrid m_tequans;
    GameObject m_block;

    public override void Init()
    {
        m_gunName = m_tran.Find("Text").GetComponent<Text>();
        m_gunHead = m_tran.Find("ScrollDataGrid/content/ItemIcon/block1/head/img").GetComponent<Image>();
        m_block = m_tran.Find("ScrollDataGrid/content/ItemIcon").gameObject;
        m_killIcon = m_tran.Find("ScrollDataGrid/content/ItemIcon/block2/killIcon").GetComponent<Image>();
        m_tequans = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_tequans.SetItemRender(m_tran.Find("ItemTequan").gameObject, typeof(ItemTequan));

    }

    public override void OnBack()
    {
        
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            var info = m_params[0] as ConfigWeaponSacredPropLine;
            var name = m_params[1] as string;
            m_gunName.text = name;
            if (info.HeadIcon != "0" && info.HeadIcon != "")
            {
                m_gunHead.SetSprite(ResourceManager.LoadRoleIcon(int.Parse(info.HeadIcon)));
                m_block.TrySetActive(false);
            }
            else
            {
                m_block.TrySetActive(false);
            }
            //m_killIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, line.KillIcon));
            m_tequans.Data = info.PropDesc;
        }
    }

    public override void Update()
    {
        
    } 
}

