﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelPackage : BasePanel
{
    private Transform m_tabbarParent;
    private Transform m_imgLockParent;
    private ToggleGroup m_toggleGroup;
    private int m_openedPageCount = 0;
    private int m_selectedGroupId = 1;

    PackageRenderInfo[] _page_data;
    PackageItemRender[] _weapon_list;

    public PanelPackage()
    {
        SetPanelPrefabPath("UI/Package/PanelPackage");
        AddPreLoadRes("UI/Package/PanelPackage", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPageIcon", EResType.UI);
        AddPreLoadRes("UI/Package/ItemLockIcon", EResType.UI);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_tabbarParent = m_tran.Find("pageTabbar");
        m_imgLockParent = m_tran.Find("pageLock");

        _page_data = new PackageRenderInfo[]
        {
            new PackageRenderInfo() {defaultName = "主武器", subType = "WEAPON1", uid = 0, width = 240, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "副武器", subType = "WEAPON2", uid = 0, width = 240, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "近战武器", subType = "DAGGER", uid = 0, width = 166, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "闪光弹", subType = "FLASHBOMB", uid = 0, width = 166, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "手雷", subType = "GRENADE", uid = 0, width = 166, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "靴子", subType = "BOOTS", uid = 0, width = 166, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "防弹衣", subType = "ARMOR", uid = 0, width = 166, icon = "",group=0},
            new PackageRenderInfo() {defaultName = "头盔", subType = "HELMET", uid = 0, width = 166, icon = "",group=0},
        };
        _weapon_list = new PackageItemRender[_page_data.Length];
        for (int i = 0; i < _weapon_list.Length; i++)
        {
            var tran = m_tran.Find("weapon_list/item_" + i);
            if (tran != null)
            {
                var render = tran.GetComponent<PackageItemRender>();
                if (render == null)
                {
                    render = tran.gameObject.AddComponent<PackageItemRender>();
                }
                _weapon_list[i] = render;
                render.SetData(_page_data[i]);
            }
        }

        m_toggleGroup = m_tabbarParent.GetComponent<ToggleGroup>();
        SetPageCount(1);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnRole")).onPointerClick += delegate { UIManager.ShowPanel<PanelRole>(); };
        UGUIClickHandler.Get(m_tran.Find("btnPackage")).onPointerClick += delegate{ UIManager.ShowPanel<PanelPackage>();};
        UGUIClickHandler.Get(m_tran.Find("btnDepot")).onPointerClick += delegate { UIManager.ShowPanel<PanelDepot>(); };
        UGUIClickHandler.Get(m_tran.Find("btnWeaponPreview")).onPointerClick += null;
        UGUIClickHandler.Get(m_tran.Find("btnBuyPackagePage")).onPointerClick += OnBuyPackagePage;

        for (int i = 0; i < _weapon_list.Length; i++)
        {
            var render = _weapon_list[i];
            UGUIClickHandler.Get(render.gameObject).onPointerClick += OnItemSelected;
        }
    }

    private void OnBuyPackagePage(GameObject target, PointerEventData eventData)
    {
        var config = ConfigManager.GetConfig<ConfigEquipBag>().GetLine(m_openedPageCount+1);
        if (config == null)
        {
            UIManager.ShowTipPanel("已达到上限");
            return;
        }

        UIManager.ShowTipPanel("是否愿意花"+"<color=#00DEFF>"+config.Price+ItemDataManager.GetMoneyName(config.MoneyType)+"</color>"+"购买一个新背包？",
            () => NetLayer.Send(new tos_player_buy_equip_bag()), null, true, false, "购买");
    }

    private void OnItemSelected(GameObject target, PointerEventData eventData)
    {
        var render = target.GetComponent<PackageItemRender>();
        if (render != null && render.m_renderData is PackageRenderInfo)
        {
            var item_data = render.m_renderData as PackageRenderInfo;
            var ary = ItemDataManager.GetDepotItemsByType("EQUIP", item_data.subType);
            if (ary.Length == 0)
            {
                UIManager.ShowTipPanel("您的仓库中没有此类装备，可以前往商城选购", () => UIManager.ShowPanel<PanelMall>(new object[] { "EQUIP", item_data.subType }), null, true, false, "前往商城");
            }
            else
            {
                UIManager.ShowPanel<PanelDepot>(new object[] { ary, m_selectedGroupId });
            }
        }
    }

    public override void OnShow()
    {
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.CheckChosen(panelHall.m_btgDepot);
        NetLayer.Send(new tos_player_item_equip_data());
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void SetPageCount(int openCount)
    {
        var maxCount = 8;
        for (int i = m_openedPageCount; i < openCount; i++)
        {
            var page_item = ResourceManager.LoadUI("UI/Package/ItemPageIcon");
            var page = i + 1;
            page_item.name = page.ToString();
            var label_0 = page_item.transform.Find("Background/Label").GetComponent<Image>();
            label_0.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "num_b" + page));
//            label_0.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "num_b" + page % maxCount));
//            if (page >= maxCount)
//            {
//                var label_1 = page_item.transform.Find("Background/Label1").GetComponent<Image>();
//                label_1.gameObject.TrySetActive(true);
//                label_1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "num_b" + (int)(page / maxCount)));
//                label_1.transform.localPosition = new Vector3(-7,0,0);
//                label_0.transform.localPosition = new Vector3(7, 0, 0);
//            }
            var toggle = page_item.GetComponent<Toggle>();
            toggle.group = m_toggleGroup;
            toggle.isOn = i== 0;
            page_item.transform.SetLocation(m_tabbarParent);
            page_item.TrySetActive(true);
            UGUIClickHandler.Get(page_item).onPointerClick += OnPageTabbarClick;
        }
        for (int i = openCount; i < m_openedPageCount; i++)
        {
            GameObject.DestroyImmediate(m_tabbarParent.GetChild(m_tabbarParent.childCount-1).gameObject);
        }

        for (int i = m_imgLockParent.childCount; i < maxCount - openCount; i++)
        {
            var itemLock = ResourceManager.LoadUI("UI/Package/ItemLockIcon"); ;
            itemLock.transform.SetLocation(m_imgLockParent);
            itemLock.TrySetActive(true);
        }
        var lockItemCount = m_imgLockParent.childCount;
        for (int i = maxCount - openCount; i < lockItemCount; i++)
        {
            GameObject.DestroyImmediate(m_imgLockParent.GetChild(0).gameObject);
        }
        m_openedPageCount = openCount;
    }

    private void OnPageTabbarClick(GameObject target, PointerEventData eventData)
    {
        SelectPage(int.Parse(target.name));
    }

    private void SelectPage(int groupId)
    {
        m_selectedGroupId = groupId;
        
        for (int j = 0; j < _page_data.Length; j++)
        {
            _page_data[j].uid = 0;
            _page_data[j].group = m_selectedGroupId;
        }
        var bags = ItemDataManager.m_bags;
        for (int i = 0; i < bags.Length; i++)
        {
            if (bags[i].group_id == groupId)
            {
                var bag = bags[i];
                _page_data[0].uid = bag.equip_list.WEAPON1;
                _page_data[1].uid = bag.equip_list.WEAPON2;
                _page_data[2].uid = bag.equip_list.DAGGER;
                _page_data[3].uid = bag.equip_list.FLASHBOMB;
                _page_data[4].uid = bag.equip_list.GRENADE;
                _page_data[5].uid = bag.equip_list.BOOTS;
                _page_data[6].uid = bag.equip_list.ARMOR;
                _page_data[7].uid = bag.equip_list.HELMET;
                break;
            }
        }
        for (int k = 0; k < _weapon_list.Length; k++)
        {
            _weapon_list[k].SetData(_page_data[k]);
        }
    }

    void Toc_player_item_equip_data(toc_player_item_equip_data data)
    {
        SelectPage(m_selectedGroupId);
        SetPageCount(data.equip_groups.Length);
    }

    void Toc_player_equip_item(toc_player_equip_item data)
    {
        SelectPage(m_selectedGroupId);
    }

    void Toc_player_equip_bag(toc_player_equip_bag data)
    {
        SelectPage(m_selectedGroupId);
    }

    void Toc_player_buy_equip_bag(toc_player_buy_equip_bag data)
    {
        SetPageCount(data.bag.group_id);
    }
}
