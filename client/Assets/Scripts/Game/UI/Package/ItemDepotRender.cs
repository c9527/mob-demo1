﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemDepotRender : ItemRender
{
    private p_item m_data;
    private Text m_txtEquipName;
    private Text m_txtRemainTime;
    private Image m_imgEquip;
    private Image m_imgBg;
    private Image m_imgBgBar;
    private Image m_bagid;
    private Image m_imgBroken;
    private Image m_imgDurability;
    private GameObject m_goDurability;
    private Transform m_tranStar;
    private Transform m_tranAwaken;
    private GameObject m_masterFlag;
    private Button m_btn;
    private Text m_btnText;
    private Transform m_part_group;
    private UIPartItemRender[] m_partItems;
    private Text m_phasedTxt;

    public override void Awake()
    {
        var tran = transform;
        m_txtEquipName = tran.Find("txtEquipName").GetComponent<Text>();
        m_txtRemainTime = tran.Find("txtRemainTime").GetComponent<Text>();
        m_imgBg = tran.Find("imgBg").GetComponent<Image>();
        m_imgBgBar = tran.Find("imgBgBar").GetComponent<Image>();
        m_imgEquip = tran.Find("imgEquip").GetComponent<Image>();
        m_bagid = tran.Find("bagid").GetComponent<Image>();
        m_imgBroken = tran.Find("imgDurability/imgBroken").GetComponent<Image>();
        m_goDurability = tran.Find("imgDurability").gameObject;
        m_imgDurability = tran.Find("imgDurability/value").GetComponent<Image>();
        m_tranStar = tran.Find("starGroup/star");
        m_tranAwaken = tran.Find("awakenGroup/avatar");
        m_masterFlag = tran.Find("MasterFlag").gameObject;
        m_btn = tran.Find("Button").GetComponent<Button>();
        m_btnText = tran.Find("Button/Text").GetComponent<Text>();

        m_part_group = tran.Find("part_group");
        m_phasedTxt = tran.Find("phasedInfo").GetComponent<Text>();
        m_partItems = new UIPartItemRender[m_part_group.childCount];
        for (int i = 0; i < m_part_group.childCount; i++)
        {
            var render = m_part_group.GetChild(i).gameObject.AddComponent<UIPartItemRender>();
            render.SetStyle(AtlasName.Package, "part_frame_");
            m_partItems[i] = render;
        }

        UGUIClickHandler.Get(m_btn.gameObject).onPointerClick += delegate { GameDispatcher.Dispatch(GameEvent.UI_DOUBLE_CLICK_DEPOTWEAPON, m_data); };
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        UGUIClickHandler.Get(gameObject).onDoublePointerClick += delegate { GameDispatcher.Dispatch(GameEvent.UI_DOUBLE_CLICK_DEPOTWEAPON, m_data); };
#endif
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_item;
        var item_vo = new CDItemData(m_data);
        m_tranStar.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
        if (item_vo.info == null)
            return;

        var colorValue = item_vo.info.RareType;
        if (colorValue == 0)
            colorValue = item_vo.GetQuality();

        m_imgBroken.enabled = ItemDataManager.IsBrokenByUid(item_vo.sdata.uid);
        m_goDurability.TrySetActive(item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2");
        m_imgDurability.fillAmount = 1f * item_vo.sdata.durability / ItemDataManager.MAX_DURABILITY;
        m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_"+ColorUtil.GetRareColorName(colorValue)));
        m_imgBgBar.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, ColorUtil.GetRareColorName(colorValue) + "_bg"));
        m_txtEquipName.text = item_vo.info.DispName;
        m_txtEquipName.color = ColorUtil.GetRareColor(item_vo.info.RareType);
        if (!string.IsNullOrEmpty(item_vo.info.Icon))
        {
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(item_vo.info.Icon),item_vo.info.SubType);
            m_imgEquip.SetNativeSize();
            m_imgEquip.enabled = true;
        }
        else
        {
            m_imgEquip.enabled = false;
        }

        var isOverdue = item_vo.sdata.time_limit != 0 && item_vo.sdata.time_limit < TimeUtil.GetNowTimeStamp();
        if ((item_vo.info.Type == GameConst.ITEM_TYPE_EQUIP && item_vo.info.SubType != GameConst.WEAPON_SUBTYPE_BULLET)
            || (item_vo.info.Type == GameConst.ITEM_TYPE_ITEMS && !string.IsNullOrEmpty(item_vo.info.UseType))
            || (item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_ACCESSORY) || (item_vo.info.Type == GameConst.ITEM_TYPE_ROLE)
            )
        {
            if (item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_ACCESSORY)
                m_btnText.text = item_vo.info.Type == GameConst.ITEM_TYPE_EQUIP ? "镶嵌" : "鉴定";
            else if (item_vo.info.Type == GameConst.ITEM_TYPE_EQUIP)
                m_btnText.text = isOverdue ? "续费" : "装备";
            else if (item_vo.info.Type == GameConst.ITEM_TYPE_ITEMS && item_vo.info.SubType == GameConst.ITEMS_SUBTYPE_MERGE)
                m_btnText.text = "合成";
            else if (item_vo.info.Type == GameConst.ITEM_TYPE_ROLE && item_vo.info.SubType == GameConst.ITEMS_SUBTYPE_HERO)
                m_btnText.text = isOverdue ? "续费" : (PlayerSystem.heroSex == 0 ? "装备" : "卸下");
            else 
                m_btnText.text = isOverdue ? "续费" : "使用";

            m_btn.gameObject.TrySetActive(true);
            if (item_vo.info.Type == GameConst.ITEM_TYPE_ROLE && item_vo.info.SubType != GameConst.ITEMS_SUBTYPE_HERO)
            {
                if (PlayerSystem.isEquipRole(item_vo.sdata.uid))
                {
                    m_btn.gameObject.TrySetActive(false);
                }
                if (item_vo.info.RoleCamp == 3)
                {
                    m_btn.gameObject.TrySetActive(false);
                }
            }
            if (item_vo.info.Type == GameConst.ITEM_TYPE_EQUIP && item_vo.info.SubType == "DECORATION")
            {
                for (int i = 0; i < ItemDataManager.m_bags[ItemDataManager.bagselect].decorations.Length; i++)
                {
                    var config = ItemDataManager.GetDepotItemByUid(ItemDataManager.m_bags[ItemDataManager.bagselect].decorations[i].decoration);
                    if (config != null)
                    {

                        if (config.uid == item_vo.sdata.uid)
                        {
                            m_btnText.text = isOverdue ? "续费" : "卸下";
                        }
                    }
                }
            }
        }
        else
        {
            m_btn.gameObject.TrySetActive(false);
        }

        m_txtRemainTime.gameObject.TrySetActive(m_data.item_id != 0);
        if ((item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || item_vo.info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2) && ItemDataManager.IsBagHas(m_data.uid))
        {
            m_bagid.enabled = true;
            m_bagid.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "bag" + ItemDataManager.GetBagGroupId(m_data.uid)));
        }
        else
        {
            m_bagid.enabled = false;
        }
        if (m_data.item_id != 0)
        {
            var pitem = ItemDataManager.GetDepotItemByUid(m_data.uid);
            if (item_vo.info.Type == GameConst.ITEM_TYPE_ITEMS && item_vo.info.SubType == GameConst.ITEMS_SUBTYPE_MERGE)
            {
                m_txtRemainTime.text = string.Format("{0}/{1}个", pitem.item_cnt,(item_vo.info as ConfigItemPartsLine).MergeCost);
            }
            else
                m_txtRemainTime.text = item_vo.info.AddRule == 1 || ((item_vo.info.AddRule == 4)||(item_vo.info.AddRule==5)) ? TimeUtil.GetRemainTimeString(pitem.time_limit) : TimeUtil.GetRemainCountString(pitem.item_cnt);
        }

        var show_strengthen = item_vo.info.SubType == "WEAPON1" || item_vo.info.SubType == "WEAPON2";
        if (show_strengthen)
        {
            m_part_group.gameObject.TrySetActive(true);
            var strengthenInfo = StrengthenModel.Instance.getSStrengthenDataByCode(item_vo.info.ID);
            if (m_tranAwaken != null)
            {
                m_tranAwaken.parent.gameObject.TrySetActive(strengthenInfo.awaken_data != null && strengthenInfo.awaken_data.level > 0);
                var show_num = strengthenInfo.awaken_data != null ? Mathf.Ceil(strengthenInfo.awaken_data.level / 2f) : 0;
                for (int i = 0; m_tranAwaken.parent.gameObject.activeSelf && i < m_tranAwaken.childCount; i++)
                {
                    var img = m_tranAwaken.GetChild(i).GetComponent<Image>();
                    img.gameObject.TrySetActive(i < show_num);
                    if (img.gameObject.activeSelf)
                    {
                        img.fillAmount = strengthenInfo.awaken_data.level < (i + 1) * 2 ? 0.5f : 1f;
                    }
                }
            }
            if (m_tranStar != null)
            {
                m_tranStar.parent.gameObject.TrySetActive((strengthenInfo.awaken_data == null || strengthenInfo.awaken_data.level == 0) && PanelStrengthenMain.IsStrengthenOpen);
                for (int i = 0; m_tranStar.parent.gameObject.activeSelf && i < m_tranStar.childCount; i++)
                {
                    m_tranStar.GetChild(i).gameObject.TrySetActive(i < strengthenInfo.state);
                }
            }
            var partData = StrengthenModel.Instance.GenWeaponPartCDData(item_vo.sdata, strengthenInfo.state);
            for (int i = 0; i < m_partItems.Length; i++)
            {
                var partVo = partData != null && i < partData.Length ? partData[i] : null;
                m_partItems[i].SetData(partVo);
            }
        }
        else
        {
            m_part_group.gameObject.TrySetActive(false);
            if (m_tranAwaken != null)
            {
                m_tranAwaken.parent.gameObject.TrySetActive(false);
            }
            if (m_tranStar != null)
            {
                m_tranStar.parent.gameObject.TrySetActive(false);
            }
        }
        // 师徒库中的武器 在仓库中显示已借出状态
        m_masterFlag.TrySetActive(m_data.is_arsenal);
        //fjs:20151030  仓库中的武器如果过期，则将底板置灰
//        bool has_own = m_data != null && (m_data.time_limit == 0 || TimeUtil.GetNowTimeStamp() < m_data.time_limit);
//        if (!has_own)
//            Util.SetGrayShader(transform);
//        else
//            Util.SetNormalShader(transform);
        var itemp = ItemDataManager.GetPhasedInfoByUid(m_data.uid);


        if(itemp!=null)
        {
            m_phasedTxt.gameObject.TrySetActive(true);
            m_phasedTxt.text = "分期付" + GetReserveTime();
        }
        else
        {
            m_phasedTxt.gameObject.TrySetActive(false);
        }




    }

    string GetReserveTime()
    {
        var itemp = ItemDataManager.GetPhasedInfoByUid(m_data.uid);
        var config = ConfigManager.GetConfig<ConfigPhasedPurchase>().GetLine(itemp.mall_id);
        if (config!=null)
        {
            var item = ItemDataManager.GetDepotItemByUid(m_data.uid);
            var targetTime = item.create_time + config.ReservedTime;
            var span = TimeUtil.GetRemainTime((uint)targetTime);
            if (span.TotalDays >1)
            {
                if (span.TotalDays <= 60)
                {
                    return ":"+((int)span.Days).ToString() + "天";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                if (span.TotalHours > 1)
                {
                    return ":" + span.Hours.ToString() + "小时";
                }
                else
                {
                    if (span.TotalMinutes > 1)
                    {
                        return ":" + span.Minutes.ToString() + "分钟";
                    }
                    else
                    {
                        m_phasedTxt.gameObject.TrySetActive(false);
                    }
                }
            }
        }

        return null;
    }

    

}
