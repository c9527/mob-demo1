﻿using UnityEngine;
using UnityEngine.UI;

public class OtherItemPackageRender : ItemRender
{
    protected OtherBagRenderInfo m_data;

    protected Text _name_txt;
    protected Image _item_img;
    private Transform m_tranStarParent;
    private Transform m_part_group;

    private Transform m_levelTagTran;
    private Image m_levelTag;
    private Text m_timeText;

    public override void Awake()
    {
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _item_img = transform.Find("item_img").GetComponent<Image>();
        m_tranStarParent = transform.Find("starGroup/star");
        m_part_group = transform.Find("part_group");
        m_timeText = transform.Find("time_txt").GetComponent<Text>();
        if (m_part_group != null && m_part_group.childCount > 0)
        {
            for (int i = 0; i < m_part_group.childCount; i++)
            {
                var render = m_part_group.GetChild(i).gameObject.AddComponent<UIItemRender>();
                render.SetStyle(AtlasName.Package, "part_frame_");
            }
        }
        m_levelTagTran = transform.Find("levelTag");
        if (m_levelTagTran != null)
            m_levelTag = m_levelTagTran.GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as OtherBagRenderInfo;
        if (m_data == null)
            return;
        var info = ItemDataManager.GetItem(m_data.itemId);
        ConfigItemWeaponLine cfgWeapon = info != null ? ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(info.ID) : null; ;
        if (cfgWeapon != null && !string.IsNullOrEmpty(cfgWeapon.LevelTag) && m_levelTagTran != null)
        {
            m_levelTagTran.gameObject.TrySetActive(true);
            m_levelTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, cfgWeapon.LevelTag));
            m_levelTag.SetNativeSize();
        }
        else
        {
            if (m_levelTagTran != null)
                m_levelTagTran.gameObject.TrySetActive(false);
        }
//        _name_txt.color = m_data.uid == 0 ? Color.white : ItemDataManager.GetItemColor(info.RareType);
        if (m_data.itemId != 0)
        {
            if (m_data.subType == "WEAPON1" || m_data.subType == "WEAPON2" || m_data.subType == "DAGGER")
            {
                _name_txt.text = info.DispName;
            }
            else
            {
                _name_txt.text = info.DispName;
            }
        }
        else
        {
            _name_txt.text = m_data.defaultName;
        }
        m_timeText.text = (m_data.itemId != 0 && m_data.time == 0) ? "永久" : "";
        _item_img.gameObject.TrySetActive(m_data.itemId != 0);
        if (m_data.itemId != 0 && !string.IsNullOrEmpty(info.Icon))
        {
            _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            _item_img.SetNativeSize();
            _item_img.enabled = true;
        }
        else
        {
            _item_img.enabled = false;
        }

        if (m_tranStarParent != null)
        {
            int count = m_data.stage;
            foreach (Transform tran in m_tranStarParent)
            {
                tran.gameObject.TrySetActive(count > 0);
                count--;
            }
        }
        if (m_part_group != null)
        {
            m_part_group.gameObject.TrySetActive(m_data.subType == GameConst.WEAPON_SUBTYPE_WEAPON1 || m_data.subType == GameConst.WEAPON_SUBTYPE_WEAPON2);
            if (m_part_group.gameObject.activeSelf)
            {
                for (int i = 0; i < m_part_group.childCount; i++)
                {
                    var render = m_part_group.GetChild(i).GetComponent<UIItemRender>();
                    if (m_data.part_data != null && i < m_data.part_data.Length && m_data.part_data[i].sdata.item_id != -1)
                    {
                        render.gameObject.TrySetActive(true);
                        render.SetData(m_data.part_data[i]);
                    }
                    else
                    {
                        render.gameObject.TrySetActive(false);
                    }
                }
            }
        }
    }
}

public class OtherBagRenderInfo
{
    public string defaultName;
    public string subType;
    public int itemId;
    public int stage;
    public string icon;
    public float width;
    public int group;
    public int time;
    public CDItemData[] part_data;
}