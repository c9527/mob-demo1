﻿using UnityEngine;
using UnityEngine.UI;

public class SlotRenderInfo
{
    public int uid;
    public string bgSpriteName;
    public string type;
    public string subType;
    public string togglePath;
}

public class ItemEquipSlotRender : ItemRender
{
    private Image m_imgEquip;
    private Image m_imgPlus;
    private Vector2 m_originScale;

    public override void Awake()
    {
        var tran = transform;
        m_imgEquip = tran.Find("Image").GetComponent<Image>();
        m_originScale = m_imgEquip.rectTransform.localScale;
        if (tran.Find("imgPlus"))
            m_imgPlus = tran.Find("imgPlus").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var vo = data as SlotRenderInfo;
        if (vo == null)
            return;

        var item = vo.uid > 0 ? ItemDataManager.GetItemByUid(vo.uid) : null;
        if (item != null)
        {
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(item.Icon));
            m_imgEquip.rectTransform.localScale = m_originScale;
        }
        else
        {
            m_imgEquip.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, vo.bgSpriteName));
            m_imgEquip.rectTransform.localScale = Vector2.one;
        }

        if (m_imgPlus)
            m_imgPlus.enabled = item == null;
        m_imgEquip.SetNativeSize();
    }
}

