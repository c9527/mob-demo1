﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBuyDotaBossBuff : BasePanel
{
    //private Vector3 m_vec0 = new Vector3(25, 0, 0);
    //private Vector3 m_vec1 = new Vector3(120, 0, 0);
    //private Vector3 m_vec2 = new Vector3(-66, -48, 0);
    //private Vector3 m_vec3 = new Vector3(-4, -48, 0);

    private Text m_txtSupplyAllNumTip;
    private Text m_txtSupplyAllNum;

    private GameObject m_goHas;

    private DotaBuffItem buff_bossRevive;

    public PanelBuyDotaBossBuff()
    {
        SetPanelPrefabPath("UI/Package/PanelBuyDotaBossBuff");
        AddPreLoadRes("UI/Package/PanelBuyDotaBossBuff", EResType.UI);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtSupplyAllNumTip = m_tran.Find("background/txtSupplyTip").GetComponent<Text>();
        m_txtSupplyAllNum = m_tran.Find("background/has/txtSupplyNum").GetComponent<Text>();

        buff_bossRevive = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_BOSS_REVIVE);

        m_goHas = m_tran.Find("background/has").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    public void Refresh()
    {
        OnShow();
    }

    public override void OnShow()
    {
        int supplypoint = -1;
        DotaBehavior behavior = null;
        behavior = DotaBehavior.singleton;
        if (behavior != null)
        {
            supplypoint = behavior.supplypoint;
            m_txtSupplyAllNum.text = supplypoint.ToString();
        }

        buff_bossRevive.onShow();

#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = false;
#endif
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = true;
#endif
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}