﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public partial class PanelMall : BasePanel
{
    private Text m_txtName;
    private Text m_txtDesc;
    private Text m_txtGetMethod;
    private Image m_imgEquip;
    private RawImage m_weaponModel;
    private GameObject m_weaponDrag;

    private DataGrid m_dataGrid;
    private GameObject m_goTabbar;
    private Button m_btnMoney;
    private Button m_btnType;
    private Text m_txtMoney;
    private Text m_txtType;
    private Image m_imageSign;
    private Image m_imageMoney;

    private DataGrid m_dataGridProp;

    private Toggle m_lastToggle;
    private string m_curTabName;
    private int m_curShopGroup;
    private int m_curShopType;

    private EnumMoneyType m_meneyFilter;
    private string m_curType;
    private string m_curSubType;
    private bool m_curVipTag;
    private ItemDropInfo[] m_moneyClassifyArr;
    private ItemDropInfo[] m_weaponClassifyArr;
    private ItemDropInfo[] m_armorClassifyArr;
    private ItemDropInfo[] m_roleClassifyArr;
    private ItemDropInfo[] m_otherClassifyArr;

    public ConfigMallLine m_selected;
    public bool m_nowItemHas;

    public static bool gotoVip;
    public bool isFromPackage;

    private toc_player_blackmarket_list _sp_shop_data;
    private toc_player_mall_record _toc_mall_record;

    private Toggle m_btnMall;
    private Toggle m_btnHeroWeaponMall;
    private Toggle m_btnHeroCardMall;
    private Toggle m_btnInstallment;
    private Toggle m_btnActivityMall;

    private ToggleGroup m_tabbar;
    private Toggle m_tabHot;
    private Toggle m_tabWeapon;
    private Toggle m_tabWeaponAdv;
    private Toggle m_tabArmor;
    private Toggle m_tabRole;
    private Toggle m_tabOther;
    private Toggle m_tabVip;
    private Toggle m_tabEventCoin;
    private Toggle m_tabNonSale;

    private RectTransform m_tranCurRight;
    private RectTransform m_tranRightMall;
    private RectTransform m_tranRightHeroWeapon;
    private RectTransform m_tranRightDetail;
    private RectTransform m_tranRightInstallmentMall;
    private Button m_btnClose;
    private GameObject m_goSendBtn;
    private CDMallItem m_selectItem;
    private GameObject m_goBuyBtn;
    private GameObject m_goRoleDescBtn;
    private GameObject m_goCompareBtn;

    private GameObject m_specialAttr;
    private Text m_specialAttrName;
    private DataGrid m_starList;
    private int m_restTime;
    private float m_timeClick;
    private GameObject eventTicket;
    private Text m_txtTicket;
    private Text m_txtRemain;

    private DataGrid m_phasedText;
    private Text m_phasedTime;
    private ConfigEventListLine m_dataCfg;

    public class ItemPhasedInfo : ItemRender
    {
        Text m_text;
        public override void Awake()
        {
            m_text = transform.Find("Text").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            string txt = data as string;
            m_text.text = txt;
        }
    }


    public PanelMall()
    {
        SetPanelPrefabPath("UI/Package/PanelMall2");
        AddPreLoadRes("UI/Package/PanelMall2", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPropRender", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON, EResType.Material);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON_CRYSTAL, EResType.Material);
    }

    public override void Init()
    {
        gotoVip = false;
        m_nowItemHas = true;
        m_moneyClassifyArr = new[]
        {
            new ItemDropInfo { type = "MONEY", subtype = "ALL", label = "查看全部"},
            new ItemDropInfo { type = "MONEY", subtype = "DIAMOND", label = "钻石购买"},
            new ItemDropInfo { type = "MONEY", subtype = "COIN", label = "金币购买"},
            new ItemDropInfo { type = "MONEY", subtype = "MEDAL", label = "勋章购买"},
            new ItemDropInfo { type = "MONEY", subtype = "EVENTCOIN_002", label = "点券购买"}
        };
        m_weaponClassifyArr = new[]
        {
            new ItemDropInfo { type = "WEAPON", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "MACHINEGUN", label = "机枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "PISTOL", label = "手枪" },
            new ItemDropInfo { type = "WEAPON", subtype = "RIFLE", label = "步枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SHOTGUN", label = "散弹枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SNIPER", label = "狙击枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SUBMACHINE", label = "冲锋枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "GRENADE", label = "近战/手雷" }
        };

        m_armorClassifyArr = new[]
        {
            new ItemDropInfo { type = "ARMOR", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "ARMOR", subtype = "FLAKHELMET", label = "防弹头盔" }, 
            new ItemDropInfo { type = "ARMOR", subtype = "FLAKJACKET", label = "防弹衣" },
            new ItemDropInfo { type = "ARMOR", subtype = "BOOTS", label = "靴子" }, 
        };

        m_roleClassifyArr = new[]
        {
            new ItemDropInfo { type = "ROLE", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "ROLE", subtype = "PATRIOT", label = "爱国者" }, 
            new ItemDropInfo { type = "ROLE", subtype = "WATCHER", label = "守护者" },
            new ItemDropInfo { type = "ROLE", subtype="DECORATION", label = "饰品"},
        };

        m_otherClassifyArr = new[]
        {
            new ItemDropInfo { type = "OTHER", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "OTHER", subtype = "ARMOR", label = "装甲" }, 
            new ItemDropInfo { type = "OTHER", subtype = "GIFT", label = "礼包" },
            new ItemDropInfo { type = "OTHER", subtype = "CLIP", label = "弹夹" }, 
            new ItemDropInfo { type = "OTHER", subtype = "BULLET", label = "子弹" }, 
            new ItemDropInfo { type = "OTHER", subtype = "", label = "其他" }, 
        };

        m_btnMall = m_tran.Find("leftTop/mallList/btnMall").GetComponent<Toggle>();
        m_btnHeroWeaponMall = m_tran.Find("leftTop/mallList/btnHeroWeaponMall").GetComponent<Toggle>();
        m_btnHeroCardMall = m_tran.Find("leftTop/mallList/btnHeroCardMall").GetComponent<Toggle>();
        m_btnInstallment = m_tran.Find("leftTop/mallList/btnInstallmentMall").GetComponent<Toggle>();
        m_btnActivityMall = m_tran.Find("leftTop/mallList/btnActivityMall").GetComponent<Toggle>();
        m_btnActivityMall.gameObject.TrySetActive(false);

        m_tranRightMall = m_tran.Find("rightMall").GetComponent<RectTransform>();
        m_dataGrid = m_tran.Find("rightMall/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("rightMall/ScrollDataGrid/content/ItemMallRender").gameObject, typeof(ItemMallRender));
        m_dataGrid.SetItemClickSound(AudioConst.selectMallItem);
        m_dataGrid.useLoopItems = true;
        
        m_btnMoney = m_tran.Find("rightMall/btnMoney").GetComponent<Button>();
        m_btnType = m_tran.Find("rightMall/btnType").GetComponent<Button>();
        m_txtMoney = m_tran.Find("rightMall/btnMoney/Text").GetComponent<Text>();
        m_txtType = m_tran.Find("rightMall/btnType/Text").GetComponent<Text>();
        m_imageSign = m_tran.Find("rightMall/btnType/Image").GetComponent<Image>();
        m_imageMoney = m_tran.Find("rightMall/btnMoney/Image").GetComponent<Image>();
        eventTicket = m_tran.Find("rightMall/eventTicket").gameObject;
        m_txtTicket = m_tran.Find("rightMall/eventTicket/txtTicket").GetComponent<Text>();
        m_txtRemain = m_tran.Find("rightMall/eventTicket/txtRemain").GetComponent<Text>();
        eventTicket.TrySetActive(false);

        m_goTabbar = m_tran.Find("rightMall/tabbar").gameObject;
        m_tabbar = m_tran.Find("rightMall/tabbar").GetComponent<ToggleGroup>();
        m_tabHot = m_tran.Find("rightMall/tabbar/tabHot").GetComponent<Toggle>();
        m_tabWeapon = m_tran.Find("rightMall/tabbar/tabWeapon").GetComponent<Toggle>();
        m_tabWeaponAdv = m_tran.Find("rightMall/tabbar/tabWeaponAdv").GetComponent<Toggle>();
        m_tabArmor = m_tran.Find("rightMall/tabbar/tabArmor").GetComponent<Toggle>();
        m_tabRole = m_tran.Find("rightMall/tabbar/tabRole").GetComponent<Toggle>();
        m_tabOther = m_tran.Find("rightMall/tabbar/tabOther").GetComponent<Toggle>();
        m_tabVip = m_tran.Find("rightMall/tabbar/tabVip").GetComponent<Toggle>();
        m_tabEventCoin = m_tran.Find("rightMall/tabbar/tabEventCoin").GetComponent<Toggle>();
        m_tabNonSale = m_tran.Find("rightMall/tabbar/tabNonSale").GetComponent<Toggle>();
        
        m_tranRightDetail = m_tran.Find("rightDetail").GetComponent<RectTransform>();
        m_dataGridProp = m_tran.Find("rightDetail/list").gameObject.AddComponent<DataGrid>();
        m_dataGridProp.useClickEvent = false;
        m_dataGridProp.autoSelectFirst = false;
        m_dataGridProp.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemPropRender"), typeof(ItemPropRender));
        m_btnClose = m_tran.Find("rightDetail/btnClose").GetComponent<Button>();
        m_txtName = m_tran.Find("rightDetail/txtEquipName").GetComponent<Text>();
        m_txtDesc = m_tran.Find("rightDetail/txtEquipDesc").GetComponent<Text>();
        m_txtGetMethod = m_tran.Find("rightDetail/txtGetMethod").GetComponent<Text>();
        m_imgEquip = m_tran.Find("rightDetail/imgEquip").GetComponent<Image>();
        m_weaponModel = m_tran.Find("rightDetail/weaponModel").GetComponent<RawImage>();
        m_weaponModel.enabled = true;
        m_weaponModel.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;
        m_weaponDrag = m_tran.Find("rightDetail/weaponDrag").gameObject;
        m_goSendBtn = m_tran.Find("rightDetail/btnlist/btnSend").gameObject;
        m_goBuyBtn = m_tran.Find("rightDetail/btnlist/btnBuy").gameObject;
        m_goRoleDescBtn = m_tran.Find("rightDetail/btnlist/btnRoleDesc").gameObject;
        m_goCompareBtn = m_tran.Find("rightDetail/btnlist/btnCompare").gameObject;
        m_specialAttr = m_tran.Find("rightDetail/specialAttr").gameObject;
        m_specialAttrName = m_tran.Find("rightDetail/specialAttr/name").GetComponent<Text>();
        m_starList = m_tran.Find("rightDetail/specialAttr").gameObject.AddComponent<DataGrid>();
        m_starList.SetItemRender(m_tran.Find("rightDetail/specialAttr/starItem").gameObject, typeof(ItemStarRender));

        m_phasedTime = m_tran.Find("rightDetail/phasedtime").GetComponent<Text>();

        m_phasedText = m_tran.Find("rightDetail/listphased").gameObject.AddComponent<DataGrid>();
        m_phasedText.SetItemRender(m_tran.Find("rightDetail/listphased/item").gameObject, typeof(ItemPhasedInfo));


        InitHeroWeapon();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabHot")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabWeapon")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabWeaponAdv")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabArmor")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabRole")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabOther")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabVip")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabEventCoin")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("rightMall/tabbar/tabNonSale")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_btnMoney.gameObject).onPointerClick += OnBtnMoneyClick;
        UGUIClickHandler.Get(m_btnType.gameObject).onPointerClick += OnBtnTypeClick;
        UGUIClickHandler.Get(m_goSendBtn).onPointerClick += OnSendBtnClick;
        UGUIDragHandler.Get(m_weaponDrag).onDrag += OnDrag;
        UGUIClickHandler.Get(m_goBuyBtn).onPointerClick += OnBuyBtnClick;
        UGUIClickHandler.Get(m_goRoleDescBtn).onPointerClick += OnRoleDescClick;
        UGUIClickHandler.Get(m_goCompareBtn).onPointerClick += OnCompareClick;
        UGUIClickHandler.Get(m_btnMall.gameObject).onPointerClick += OnMallBtnClick;
        UGUIClickHandler.Get(m_btnHeroWeaponMall.gameObject).onPointerClick += OnMallBtnClick;
        UGUIClickHandler.Get(m_btnHeroCardMall.gameObject).onPointerClick += OnMallBtnClick;
        UGUIClickHandler.Get(m_btnInstallment.gameObject).onPointerClick += OnMallBtnClick;
        UGUIClickHandler.Get(m_btnActivityMall.gameObject).onPointerClick += OnMallBtnClick;
        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += OnbtnDetailCloseClick;
        m_dataGrid.onItemSelected += OnItemSelected;


        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
        AddEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, UpdateBtnHeroCardMall);
        AddEventListener(GameEvent.MAINPLAYER_INFO_CHANGE, UpdateActiveCoin);
        InitEventHeroWeapon();
    }

    private void OnCompareClick(GameObject target, PointerEventData eventdata)
    {
        UIManager.PopPanel<PanelCompare>(new object[]{m_selectItem.info.ItemId}, false, this);
    }

    private void OnDrag(GameObject target, PointerEventData eventData)
    {
        WeaponDisplay.Rotation(eventData.delta.x);
    }

    private void OnbtnDetailCloseClick(GameObject target, PointerEventData eventData)
    {
        if (m_tranCurRight != m_tranRightHeroWeapon)
        {
            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();
        }
        
        TweenAnchoredPosition.Begin(m_tranRightDetail.gameObject, 0.2f, new Vector3(100, 0)).method = UITweener.Method.EaseIn;
        TweenAnchoredPosition.Begin(m_tranCurRight.gameObject, 0.2f, new Vector3(-480, 0)).method = UITweener.Method.EaseIn;
    }

    /// <summary>
    /// 不同商城的入口
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventdata"></param>
    private void OnMallBtnClick(GameObject target, PointerEventData eventdata)
    {
        if (target == m_btnMall.gameObject)
        {
            m_curShopGroup = 0;
            m_tranCurRight = m_tranRightMall;
            m_tranRightMall.gameObject.TrySetActive(true);
            m_tranRightHeroWeapon.gameObject.TrySetActive(false);
            m_tranRightMall.anchoredPosition = new Vector2(-480, 0);
            m_tranRightDetail.anchoredPosition = new Vector2(100, 0);

            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();

            m_tabHot.gameObject.TrySetActive(true);
            m_tabWeaponAdv.gameObject.TrySetActive(true);
            m_tabRole.gameObject.TrySetActive(true);
            m_tabNonSale.gameObject.TrySetActive(true);
            eventTicket.TrySetActive(false);
            m_tabOther.gameObject.TrySetActive(true);
            m_tabHot.isOn = true;
            m_tabbar.NotifyToggleOn(m_tabHot);
            OnTabbarClick(m_tabHot.gameObject, null);
        }
        else if (target == m_btnHeroCardMall.gameObject)
        {
            m_curShopGroup = 1;
            m_tranCurRight = m_tranRightMall;
            m_tranRightMall.gameObject.TrySetActive(true);
            m_tranRightHeroWeapon.gameObject.TrySetActive(false);
            m_tranRightMall.anchoredPosition = new Vector2(-480, 0);
            m_tranRightDetail.anchoredPosition = new Vector2(100, 0);
            m_tabOther.gameObject.TrySetActive(true);
            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();

            m_tabHot.gameObject.TrySetActive(false);
            m_tabWeaponAdv.gameObject.TrySetActive(false);
            m_tabRole.gameObject.TrySetActive(false);
            m_tabNonSale.gameObject.TrySetActive(false);
            eventTicket.TrySetActive(false);
            m_tabWeapon.isOn = true;
            m_tabbar.NotifyToggleOn(m_tabWeapon);
            OnTabbarClick(m_tabWeapon.gameObject, null);
        }
        else if (target == m_btnHeroWeaponMall.gameObject)
        {
            m_curShopGroup = 2;
            m_tranCurRight = m_tranRightHeroWeapon;
            m_tranRightMall.gameObject.TrySetActive(false);
            m_tranRightHeroWeapon.gameObject.TrySetActive(true);
            eventTicket.TrySetActive(false);
            m_tranRightHeroWeapon.anchoredPosition = new Vector2(-480, 0);
            m_tranRightDetail.anchoredPosition = new Vector2(100, 0);

            var arr = ItemDataManager.GetMallItemsByShopType(4);
            var items = new CDMallItem[arr.Length];
            for (int i = 0; i < items.Length; i++)
            {
                items[i] = new CDMallItem() { info = arr[i] };
            }
            m_heroWeaponTabbar.Data = items;
            m_heroWeaponTabbarPage = 0;

            if (arr.Length > 0)
                WeaponDisplay.UpdateMode(ItemDataManager.GetItemWeapon(arr[0].ItemId));
        }
        else if (target == m_btnInstallment.gameObject)
        {
            m_curShopGroup = 4;
            m_tranCurRight = m_tranRightMall;
            m_tranRightMall.gameObject.TrySetActive(true);
            m_tranRightHeroWeapon.gameObject.TrySetActive(false);
            m_tranRightMall.anchoredPosition = new Vector2(-480, 0);
            m_tranRightDetail.anchoredPosition = new Vector2(100, 0);
            var arr = ItemDataManager.GetInstallmentItem();

            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();

            m_tabHot.gameObject.TrySetActive(false);
            m_tabWeaponAdv.gameObject.TrySetActive(false);
            m_tabRole.gameObject.TrySetActive(false);
            m_tabNonSale.gameObject.TrySetActive(false);
            m_tabOther.gameObject.TrySetActive(false);
            eventTicket.TrySetActive(false);
            m_tabWeapon.isOn = true;
            m_tabbar.NotifyToggleOn(m_tabWeapon);
            OnTabbarClick(m_tabWeapon.gameObject, null);
        }
        else if (target == m_btnActivityMall.gameObject)
        {
            m_curShopGroup = 3;
            m_tranCurRight = m_tranRightMall;
            m_tranRightMall.gameObject.TrySetActive(true);
            m_tranRightHeroWeapon.gameObject.TrySetActive(false);
            m_btnMoney.gameObject.TrySetActive(false);
            m_btnType.gameObject.TrySetActive(false);
            m_tabOther.gameObject.TrySetActive(true);
            eventTicket.TrySetActive(true);

            m_tranRightMall.anchoredPosition = new Vector2(-480, 0);
            m_tranRightDetail.anchoredPosition = new Vector2(100, 0);

            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();

            m_tabHot.gameObject.TrySetActive(false);
            m_tabWeaponAdv.gameObject.TrySetActive(false);
            m_tabRole.gameObject.TrySetActive(false);
            m_tabNonSale.gameObject.TrySetActive(false);
            m_tabWeapon.isOn = true;
            m_tabbar.NotifyToggleOn(m_tabWeapon);
            OnTabbarClick(m_tabWeapon.gameObject, null);
        }
    }

    private void OnSendBtnClick(GameObject target, PointerEventData eventData)
    {
        var level = ConfigMisc.GetInt("send_gift_req_level");
        if (PlayerSystem.roleData.level >= level)
            UIManager.PopPanel<PanelMallSend>(new object[] { m_selectItem.info }, true, UIManager.GetPanel<PanelMall>());
        else
            UIManager.ShowTipPanel(string.Format("达到{0}级可以赠送道具", level));
    }

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        int index = HeroCardDataManager.MallCardArray.ToList<int>().IndexOf(m_selectItem.info.ID);
        if (index != -1)
        {
            if (index + 1 < (int)HeroCardDataManager.getHeroMaxCardType())
            {
                //不能购买更低等级英雄卡
                TipsManager.Instance.showTips("不能购买比当前等级低的英雄卡");
                return;
            }
        }
        if (m_selectItem.info.MoneyType == EnumMoneyType.NON_SALE)
        {
            var item = ItemDataManager.GetItem(m_selectItem.info.ItemId);
            if (item != null && !string.IsNullOrEmpty(item.GetMethodGoto))
                UIManager.GotoPanel(item.GetMethodGoto);
            else
                TipsManager.Instance.showTips("该武器未开放获得途径");
            return;
        }
        if (m_selectItem.info.ShopType == 8)
        {
            UIManager.PopPanel<PanelBuyInstallment>(new object[] { m_selectItem.info }, true);
            return;
        }

        if (UIManager.IsOpen<PanelMall>())
        {
            UIManager.GetPanel<PanelMall>().m_selected = m_selectItem.info;
            if (ItemDataManager.GetItem(m_selectItem.info.ItemId).Type == "EQUIP")
            {
                UIManager.GetPanel<PanelMall>().m_nowItemHas = ItemDataManager.GetDepotItem(m_selectItem.info.ItemId) != null;
            }
        }
        UIManager.PopPanel<PanelBuy>(new object[] { m_selectItem.info, "", m_selectItem != null && m_selectItem.black_market }, true);
    }

    private void OnRefreshBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_sp_shop_data != null && _sp_shop_data.refresh_cost!=-1)
        {
            NetLayer.Send(new tos_player_blackmarket_refresh());
        }
        else
        {
            TipsManager.Instance.showTips("你的刷新次数已经用完");
        }
    }

    private void OnItemSelected(object renderData)
    {
        UIManager.HideDropList();
        var vo = renderData as CDMallItem;
        m_selectItem = vo;
        UpdateDetailView();
        UpdateModel();

        TweenAnchoredPosition.Begin(m_tranCurRight.gameObject, 0.2f, new Vector3(100, 0)).method = UITweener.Method.EaseIn;
        TweenAnchoredPosition.Begin(m_tranRightDetail.gameObject, 0.2f, new Vector3(-480, 0)).method = UITweener.Method.EaseIn;
    }

    private void UpdateModel()
    {
        var vo = m_selectItem as CDMallItem;
        if (vo == null || vo.info == null || vo.locked == true)
            return;

        var titem = ItemDataManager.GetItem(vo.info.ItemId);
        if (titem is ConfigItemWeaponLine && (titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2))
        {
            var weapon = titem as ConfigItemWeaponLine;
            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = weapon;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();
        }
        else if (titem is ConfigItemRoleLine)
        {
            ModelDisplay.ConfigRole = titem as ConfigItemRoleLine;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.ConfigDecoration = null;
            ModelDisplay.UpdateModel();
        }
        else if (titem is ConfigItemDecorationLine)
        {

            var configs = ModelDisplay.ConfigDecoration;
            var deco = titem as ConfigItemDecorationLine;
            configs[int.Parse(deco.Part) - 1] = deco;
            ModelDisplay.ConfigDecoration = configs;
            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = null;
            ModelDisplay.UpdateModel();
        }
    }

    private void UpdateDetailView()
    {
        var vo = m_selectItem as CDMallItem;
        if (vo == null || vo.info == null || vo.locked == true)
        {
            m_txtName.text = "";
            m_txtDesc.text = "";
            m_imgEquip.enabled = false;
            m_dataGridProp.Data = new object[0];
            return;
        }
        
        var nonSale = vo.info.MoneyType == EnumMoneyType.NON_SALE;
        var titem = ItemDataManager.GetItem(vo.info.ItemId);
        if (titem is ConfigItemWeaponLine &&
            (titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 ||
             titem.SubType == GameConst.WEAPON_SUBTYPE_DAGGER || titem.SubType == GameConst.WEAPON_SUBTYPE_GRENADE ||
             titem.SubType == GameConst.WEAPON_SUBTYPE_FLASHBOMB))
        {
            var weapon = titem as ConfigItemWeaponLine;
            m_imgEquip.enabled = false;
            m_weaponModel.enabled = true;
            WeaponDisplay.UpdateMode(weapon);
        }
        else if (titem is ConfigItemRoleLine)
        {
            m_weaponModel.enabled = false;
            m_imgEquip.enabled = true;
        }
        else
        {
            m_weaponModel.enabled = false;
            m_imgEquip.enabled = true;
        }
        m_txtName.text = titem.DispName;
        m_txtDesc.text = titem.Desc;
        if (!string.IsNullOrEmpty(titem.Icon))
        {
            //            m_imgEquip.enabled = true;
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(titem.Icon), titem.SubType);
            m_imgEquip.SetNativeSize();
            //            m_imgEquip.rectTransform.anchoredPosition = m_equipImagePos + vo.info.IconOffset;
            //            m_imgEquip.rectTransform.localRotation = Quaternion.Euler(0, 0, titem.IconRotation);
            //            m_imgEquip.rectTransform.localScale = new Vector3(0.2f, 0.2f, 1);
            //            TweenScale.Begin(m_imgEquip.gameObject, 0.1f, new Vector3(1, 1, 1));
        }
        else
        {
            m_imgEquip.enabled = false;
        }

        m_goCompareBtn.TrySetActive(titem is ConfigItemWeaponLine);
        m_goSendBtn.TrySetActive((vo.info.UnlockVipLevel == 0 && vo.black_market == false) && (vo.info.CanSend != 0));
        m_goRoleDescBtn.gameObject.TrySetActive(titem is ConfigItemRoleLine);
        var item = ItemDataManager.GetItem(vo.info.ItemId);
        //        if (item != null && (item is ConfigItemWeaponLine || item is ConfigItemOtherLine))
        //            m_goTeQuan.TrySetActive(item.RareType == 5);

        int index = HeroCardDataManager.MallCardArray.ToList<int>().IndexOf(vo.info.ID);
        Util.SetGoGrayShader(m_goBuyBtn, false);
        m_goBuyBtn.transform.Find("Text").GetComponent<Text>().text = "购买";
        int cardLv = (int)HeroCardDataManager.getHeroMaxCardType();
        if (index != -1)
        {
            if (index + 1 < cardLv)
            {
                //不能购买更低等级英雄卡
                Util.SetGoGrayShader(m_goBuyBtn, true);
            }
            else
            {
                //英雄卡
                if (index + 1 != cardLv && cardLv != 0)
                    m_goBuyBtn.transform.Find("Text").GetComponent<Text>().text = "升级";
            }
        }
        var pitem = ItemDataManager.GetDepotItem(vo.info.ItemId);
        var forever = pitem != null && pitem.time_limit == 0 && (item.AddRule == 1);
        var vipUnlock = PlayerSystem.roleData.vip_level >= vo.info.UnlockVipLevel;
        var unlock = PlayerSystem.roleData.level >= vo.info.UnlockLevel;

        if (nonSale)
        {
            m_goBuyBtn.TrySetActive(true);
            m_goBuyBtn.transform.Find("Text").GetComponent<Text>().text = "获取途径";
            m_txtGetMethod.text = "获取途径：" + titem.GetMethodDesc;
        }
        else if (forever)
            m_goBuyBtn.TrySetActive(false);
        else if (vo.info.UnlockVipLevel > 0)
            m_goBuyBtn.TrySetActive(vipUnlock);
        else
            m_goBuyBtn.TrySetActive(unlock);

        m_txtGetMethod.enabled = nonSale && !string.IsNullOrEmpty(titem.GetMethodDesc);

        var itemWeapon = item as ConfigItemWeaponLine;
        if (itemWeapon != null && itemWeapon.SpecialAtt.Length >= 2)
        {
            var config = ConfigManager.GetConfig<ConfigWeaponSpecialAttr>().GetLine(itemWeapon.SpecialAtt[0]);
            m_specialAttrName.text = config.Name;
            var strs = new string[itemWeapon.SpecialAtt[1]];
            for (int i = 0; i < strs.Length; i++)
            {
                strs[i] = config.Icon;
            }
            m_starList.Data = strs;
            m_specialAttr.TrySetActive(true);
        }
        else
            m_specialAttr.TrySetActive(false);

        m_dataGridProp.Data = titem is ConfigItemWeaponLine ? ItemDataManager.GetPropRenderInfos(titem as ConfigItemWeaponLine) : new object[0];

        if (vo.info.ShopType == 8)
        {
            m_dataGridProp.gameObject.TrySetActive(false);
            m_phasedText.gameObject.TrySetActive(true);
            m_phasedTime.gameObject.TrySetActive(true);
            m_phasedTime.text = vo.info.Price.Length.ToString() + "期";
        }
        else
        {
            m_dataGridProp.gameObject.TrySetActive(true);
            m_phasedText.gameObject.TrySetActive(false);
            m_phasedTime.gameObject.TrySetActive(false);
        }
    }

    private void OnDropListItemSelected(ItemDropInfo data)
    {
        m_dataGrid.ResetScrollPosition();
        if (data.type == "MONEY")
        {
            m_meneyFilter = (EnumMoneyType) Enum.Parse(typeof (EnumMoneyType), data.subtype);
            m_dataGrid.Data = CreateGroupItems(m_curType, m_curSubType, m_curVipTag, m_meneyFilter, m_curShopType);
            if (data.subtype == EnumMoneyType.ALL.ToString())
                m_txtMoney.text = "货币";
            else if (data.subtype == EnumMoneyType.DIAMOND.ToString())
                m_txtMoney.text = "钻石";
            else if (data.subtype == EnumMoneyType.COIN.ToString())
                m_txtMoney.text = "金币";
            else if (data.subtype == EnumMoneyType.MEDAL.ToString())
                m_txtMoney.text = "勋章";
            else if (data.subtype == EnumMoneyType.EVENTCOIN_002.ToString())
                m_txtMoney.text = "点券";
        }
        else
        {
            m_dataGrid.Data = CreateGroupItems(data.type, data.subtype, m_curVipTag, m_meneyFilter, m_curShopType);
            if (data.subtype == "ALL")
                m_txtType.text = "类型";
            else
                m_txtType.text = data.label;
        }
    }

    private void UpdateFilterBtnLabel()
    {
        if (m_meneyFilter == EnumMoneyType.ALL)
            m_txtMoney.text = "货币";

        if (m_curSubType == "ALL")
            m_txtType.text = "类型";
    }

    public override void OnShow()
    {
        m_weaponModel.texture = WeaponDisplay.RenderTexture;
        WeaponDisplay.Visible = true;
        ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.11f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
        _sp_shop_data = null;
        m_lastToggle = null;
        UIManager.HideDropList();
        m_dataGrid.ResetScrollPosition();
        m_meneyFilter = EnumMoneyType.ALL;
        m_txtMoney.text = "货币";
        m_curType = "HOT";
        m_curSubType = "ALL";
        m_btnMoney.gameObject.TrySetActive(false);
        m_btnType.gameObject.TrySetActive(false);
        UpdateBubble();
        isFromPackage = false;

        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.CheckChosen(null);

        UpdateBtnHeroCardMall();
        
        //请求限购数据
        NetLayer.Send(new tos_player_mall_record());
        if(HasParams())
        {
            m_btnActivityMall.isOn = true;
            OnMallBtnClick(m_btnActivityMall.gameObject, null);
        }
        else
        {
            m_btnMall.isOn = true;
            OnMallBtnClick(m_btnMall.gameObject, null);
        }
    }

    private void UpdateBtnHeroCardMall()
    {
        var heroType = HeroCardDataManager.getHeroMaxCardType();
        m_btnHeroCardMall.gameObject.TrySetActive(heroType != EnumHeroCardType.none);
    }

    private void UpdateActiveCoin()
    {
        m_txtTicket.text = PlayerSystem.roleData.active_coin.ToString();
    }

//    private void ResetPos(RectTransform tran = null)
//    {
//        if (tran == null)
//            tran = m_tranRightMall;
//        m_tranCurRight = tran;
//        tran.anchoredPosition = new Vector2(-480, 0);
//        m_tranRightDetail.anchoredPosition = new Vector2(100, 0);
//    }

    public override void OnHide()
    {
        ModelDisplay.ConfigRole = null;
        ModelDisplay.ConfigWeapon = null;
        ModelDisplay.ConfigDecoration = null;
        ModelDisplay.UpdateModel();

        WeaponDisplay.Visible = false;
//        if (m_halo != null)
//            m_halo.Destroy();
//        m_halo = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
        m_dataGrid = null;
//        if (m_halo != null)
//            m_halo.Destroy();
//        m_halo = null;
    }

    public override void Update()
    {
        if(m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtRemain.text = "商城关闭倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true);
            }
        }
        UpdateActiveCoinMall();
    }

    private void UpdateActiveCoinMall()
    {
        if (m_btnActivityMall == null)
            return;
        int curTime = TimeUtil.GetNowTimeStamp();
        m_dataCfg = ConfigManager.GetConfig<ConfigEventList>().GetLine(1600);
        if (m_dataCfg != null)
        {
            if (curTime >= uint.Parse(m_dataCfg.TimeStart) && curTime <= uint.Parse(m_dataCfg.TimeEnd))
                m_btnActivityMall.gameObject.TrySetActive(true);
            else
                m_btnActivityMall.gameObject.TrySetActive(false);
        }
    }

    private void UpdateBubble()
    {
        if (IsOpen() == false)
        {
            return;
        }
        var tab_btn = m_goTabbar.transform.Find("tabVip");
        if (tab_btn != null)
        {
            tab_btn.Find("bubble").gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.MallSPNotice));
        }
    }

    private CDMallItem[] CreateGroupItems(string type, string subtype, bool vip,EnumMoneyType moneyType = EnumMoneyType.ALL, int forceShopType = 0)
    {
        var isHot = type == "HOT";
        var list = new List<CDMallItem>();
        var arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
        m_curType = type;
        m_curSubType = subtype;
        m_curVipTag = vip;
        m_meneyFilter = moneyType;
        m_curShopType = forceShopType;
        //bool isLevelShow = false; //处理等级显示
        int alreadyNo = 0; //处理已购限购
        if (m_dataGrid.Data != null)
        {
            
        }
        for (int i = 0; i < arr.Length; i++)
        {
            var item = arr[i];
            if (!isHot)
            {
//                if (forceShopType < 0 && item.ShopType != m_curShopType)
//                    continue;
                if (forceShopType >= 0 && item.ShopType != forceShopType)
                    continue;
            }
            if ((!vip && item.UnlockVipLevel > 0) || (vip && item.UnlockVipLevel == 0))
                continue;
            if (!ItemDataManager.IsSale(item))
                continue;
            if (m_meneyFilter != EnumMoneyType.ALL && item.MoneyType != m_meneyFilter )
                continue;
            //处理等级显示
            if (null != item.ShowLevel && item.ShowLevel.Length > 0)
                if (PlayerSystem.roleData.level < item.ShowLevel[0] || PlayerSystem.roleData.level > item.ShowLevel[1])
                    continue;
            //处理已购限购
            alreadyNo = 0;
            if (null != _toc_mall_record)
            {
                for (int j = 0; j < _toc_mall_record.p_record.Length; j++)
                {
                    if (item.ID == _toc_mall_record.p_record[j].mall_id)
                    {
                        alreadyNo = _toc_mall_record.p_record[j].amount;
                        break;
                    }
                }
            }
            if (isHot && ItemDataManager.IsHot(item) || (!isHot && ((type == "ALL" || item.Type == type) && (subtype == "ALL" || item.SubType == subtype))))
            {
                list.Add(new CDMallItem() { info = item, alreadBuyNo = alreadyNo });
                m_restTime = -1;
                if (forceShopType == 9)
                {
                    m_restTime = (int)TimeUtil.GetRemainTime((uint)item.SaleEndTime).TotalSeconds;
                    m_txtTicket.text = PlayerSystem.roleData.active_coin.ToString();
                    m_txtRemain.text = "商城关闭倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true);
                }
            }                
        }
        
        if (isHot)
            list.Sort((x, y) => x.info.TopIndex - y.info.TopIndex);
        else
            list.Sort((x, y) => x.info.NormalIndex - y.info.NormalIndex);
        return list.ToArray();
    }

    private CDMallItem[] CreateGroupItemsByMoney(EnumMoneyType type)
    {
        var isHot = false;
        var list = new List<CDMallItem>();
        var arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
        m_curType = "ALL";
        m_curSubType = "ALL";
        m_curVipTag = false;
        m_meneyFilter = type;
        //bool isLevelShow = false; //处理等级显示
        int alreadyNo = 0; //处理已购限购
        if (m_dataGrid.Data != null)
        {

        }
        for (int i = 0; i < arr.Length; i++)
        {
            var item = arr[i];
            if (item.ShopType != 0)
                continue;
            if ((!false && item.UnlockVipLevel > 0) || (false && item.UnlockVipLevel == 0))
                continue;
            if (!ItemDataManager.IsSale(item))
                continue;
            if (item.MoneyType != type)
                continue;
            //处理等级显示
            if (null != item.ShowLevel && item.ShowLevel.Length > 0)
                if (PlayerSystem.roleData.level < item.ShowLevel[0] || PlayerSystem.roleData.level > item.ShowLevel[1])
                    continue;
            //处理已购限购
            alreadyNo = 0;
            if (null != _toc_mall_record)
            {
                for (int j = 0; j < _toc_mall_record.p_record.Length; j++)
                {
                    if (item.ID == _toc_mall_record.p_record[j].mall_id)
                    {
                        alreadyNo = _toc_mall_record.p_record[j].amount;
                        break;
                    }
                }
            }
            if (isHot && ItemDataManager.IsHot(item) || (!isHot))
                list.Add(new CDMallItem() { info = item, alreadBuyNo = alreadyNo });
        }
        if (isHot)
            list.Sort((x, y) => x.info.TopIndex - y.info.TopIndex);
        else
            list.Sort((x, y) => x.info.NormalIndex - y.info.NormalIndex);
        return list.ToArray();
    }

    private CDMallItem[] CreateSPShopItems()
    {
        m_curType = "ALL";
        m_curSubType = "ALL";
        m_curVipTag = true;
        var list = new List<CDMallItem>();
        if (_sp_shop_data != null && _sp_shop_data.item_list != null && _sp_shop_data.item_list.Length > 0)
        {
            for (var i = 0; i < _sp_shop_data.item_list.Length; i++)
            {
                list.Add(new CDMallItem(_sp_shop_data.item_list[i]) { index = i, locked = i >= _sp_shop_data.show_count, black_market=true });
            }
        }
        return list.ToArray();
    }

    private void OnTabbarClick(GameObject target, PointerEventData data)
    {
        var toggle = target.GetComponent<Toggle>();
        var secondClick = toggle.isOn && toggle == m_lastToggle;
        ItemDropInfo[] dropListData = null;
        m_curTabName = toggle.name;

        if (!secondClick)
            m_dataGrid.ResetScrollPosition();

        if (m_curShopGroup == 0)
            m_curShopType = 0;
        if (m_curShopGroup == 1)
            m_curShopType = 6;
        if (m_curShopGroup == 3)
            m_curShopType = 9;
        if (m_curShopGroup == 4)
        {
            m_curShopType = 8;
        }
        var typeBtnVisible = false;
        var moneyBtnVisible = false;
        var eventCoinVisible = false;
        switch (target.name)
        {
            case "tabHot":
                m_dataGrid.Data = CreateGroupItems("HOT", "ALL", false, EnumMoneyType.ALL, m_curShopType); 
                moneyBtnVisible = true; 
                break;
            case "tabWeapon":
                m_dataGrid.Data = CreateGroupItems("WEAPON", "ALL", false, EnumMoneyType.ALL, m_curShopType);
                typeBtnVisible = true;
                moneyBtnVisible = true;
                break;
            case "tabWeaponAdv":
                m_dataGrid.Data = CreateGroupItems("WEAPON", "ALL", false, EnumMoneyType.ALL, 5);
                typeBtnVisible = true;
                break;
            case "tabArmor":
                m_dataGrid.Data = CreateGroupItems("ARMOR", "ALL", false, EnumMoneyType.ALL, m_curShopType); 
                typeBtnVisible = true;
                moneyBtnVisible = true;
                break;
            case "tabRole":
                m_dataGrid.Data = CreateGroupItems("ROLE", "ALL", false, EnumMoneyType.ALL, m_curShopType);
                typeBtnVisible = true;
                moneyBtnVisible = true;
                break;
            case "tabOther":
                m_dataGrid.Data = CreateGroupItems("OTHER", "ALL", false, EnumMoneyType.ALL, m_curShopType);
                typeBtnVisible = true;
                moneyBtnVisible = true;
                break;
//            case "tabVip":
//                BubbleManager.SetBubble(BubbleConst.MallSPNotice, false);
//                m_curVipTag = true;
//                if (_sp_shop_data == null)
//                    NetLayer.Send(new tos_player_blackmarket_list());
//                else
//                    Toc_player_blackmarket_list(_sp_shop_data);
//                break;
//            case "tabEventCoin":
//                m_dataGrid.Data = CreateGroupItems("ALL", "ALL", false, EnumMoneyType.EVENTCOIN_002);
//                break;
            case "tabNonSale":
                m_dataGrid.Data = CreateGroupItems("ALL", "ALL", false, EnumMoneyType.NON_SALE, 7);
                typeBtnVisible = true;
                break;

        }

        if (target.name == "tabVip")
        {
            m_btnMoney.gameObject.TrySetActive(false);
            m_btnType.gameObject.TrySetActive(false);
//            m_tips.gameObject.TrySetActive(true);
//            m_btnRefresh.gameObject.TrySetActive(true);
        }
        else
        {
            m_btnMoney.gameObject.TrySetActive(true);
            m_btnType.gameObject.TrySetActive(true);
//            m_tips.gameObject.TrySetActive(false);
//            m_btnRefresh.gameObject.TrySetActive(false);
            if (target.name == "tabEventCoin")
            {
                eventCoinVisible = true;
            }
        }

        if (secondClick && dropListData != null)
            UIManager.ShowDropList(dropListData, OnDropListItemSelected, m_tran, target, 240);
        else
            UIManager.HideDropList();

        SetTypeBtnVisible(typeBtnVisible);
        SetMoneyBtnVisible(moneyBtnVisible);    //shopType==5钻石商城不给选货币类型
        SetEventCoinMode(eventCoinVisible);
        if (m_curShopGroup == 3)
        {
            m_btnMoney.gameObject.TrySetActive(false);
            m_btnType.gameObject.TrySetActive(false);
        }
        m_lastToggle = toggle;
        m_nowItemHas = true;
        UpdateFilterBtnLabel();
    }
    void SetEventCoinMode(bool isEvent)
    {
        m_btnMoney.gameObject.TrySetActive(!isEvent);
        m_btnType.gameObject.TrySetActive(!isEvent);
//        m_tips.gameObject.TrySetActive(!isEvent);
//        m_eventCoin.TrySetActive(isEvent);
//        m_coinNum.text = PlayerSystem.roleData.eventcoin_002.ToString();
    }

    private void SetTypeBtnVisible(bool visible)
    {
        if (visible)
        {
            m_btnType.interactable = true;
            m_txtType.color = new Color32(158, 158, 158, 255);
            m_imageSign.color = Color.white;
        }
        else
        {
            m_btnType.interactable = false;
            m_txtType.color = new Color32(100, 100, 100, 255);
            m_imageSign.color = new Color32(100, 100, 100, 255);
        }
    }

    private void SetMoneyBtnVisible(bool visible)
    {
        if (visible)
        {
            m_btnMoney.interactable = true;
            m_txtMoney.color = new Color32(158, 158, 158, 255);
            m_imageMoney.color = Color.white;
        }
        else
        {
            m_txtMoney.text = "货币";
            m_btnMoney.interactable = false;
            m_txtMoney.color = new Color32(100, 100, 100, 255);
            m_imageMoney.color = new Color32(100, 100, 100, 255);
        }
    }
    

    private void OnBtnMoneyClick(GameObject target, PointerEventData eventdata)
    {
        if (!m_btnMoney.interactable)
            return;
        UIManager.ShowDropList(m_moneyClassifyArr, OnDropListItemSelected, m_tran, target, 240);
    }

    private void OnBtnTypeClick(GameObject target, PointerEventData eventdata)
    {
        if (!m_btnType.interactable)
            return;
        ItemDropInfo[] arr;
        if (m_curType == "ALL")
            arr = m_weaponClassifyArr;
        else if (m_curType == "WEAPON")
            arr = m_weaponClassifyArr;
        else if (m_curType == "ARMOR")
            arr = m_armorClassifyArr;
        else if (m_curType == "ROLE")
            arr = m_roleClassifyArr;
        else if (m_curType == "OTHER")
            arr = m_otherClassifyArr;
        else
            arr = new ItemDropInfo[0];
        UIManager.ShowDropList(arr, OnDropListItemSelected, m_tran, target, 240);
    }

    void Toc_player_buy_item(toc_player_buy_item data)
    {
        m_dataGrid.UpdateView();
        UpdateDetailView();
        if (m_curShopGroup == 2) //英雄武器
            OnHeroWeaponTabbarSelected(m_selectItem);

        if (!m_nowItemHas)
        {
            OnClickEquip();
        }

        //黑市每次买完刷新
        if (m_curTabName == "tabVip")
        {
            NetLayer.Send(new tos_player_blackmarket_list());
        }
    }

    private void OnRoleDescClick(GameObject target, PointerEventData eventData)
    {
        var item = m_dataGrid.SelectedData<CDMallItem>();
        if (item == null || item.info == null)
            return;
        var itemVo = ItemDataManager.GetItem(item.info.ItemId);
        if (itemVo is ConfigItemRoleLine)
        {
            var desc = itemVo.Introduction;
            if (!string.IsNullOrEmpty(desc))
                desc = desc.Replace("<br/>", "\n");
            UIManager.ShowTipMulTextPanel(desc, "人物介绍");
        }
    }

    private void OnClickEquip()
    {
        var data = m_selected;
        var item = ItemDataManager.GetDepotItem(data.ItemId);
        m_nowItemHas = true;
    }

    private bool CheckIfHas(int id)
    {
        if (ItemDataManager.GetDepotItem(id) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }                                                                                                                                                                                                                                                                                                                     

    //===================================s to c
    private void Toc_player_blackmarket_list(toc_player_blackmarket_list rmo)
    {
        _sp_shop_data = rmo;
        WakeupTimer(null);
        if (m_lastToggle!=null && m_curTabName=="tabVip")
        {
//            m_btnRefresh.transform.Find("value_txt").GetComponent<Text>().text = _sp_shop_data.refresh_cost>0 ? _sp_shop_data.refresh_cost.ToString() : "0";
            m_dataGrid.Data = CreateSPShopItems();
        }
    }

    private void Toc_player_blackmarket_notice(toc_player_blackmarket_notice rmo)
    {
        NetLayer.Send(new tos_player_blackmarket_list());
    }

    private void WakeupTimer(object running)
    {
        if (_sp_shop_data == null)
        {
            TimerManager.RemoveTimeOut(WakeupTimer);
            return;
        }
        if (running == null)
        {
            TimerManager.RemoveTimeOut(WakeupTimer);
            var now_time = TimeUtil.GetNowTime();
            var deadline = new DateTime(now_time.Year, now_time.Month, now_time.Day, 23, 59, 59);
            var cd = TimeUtil.GetTimeStamp(deadline) - TimeUtil.GetTimeStamp(now_time);
            if (cd > 0 && cd < 3600)
            {
                Logger.Log("==========================WakeupTimer: " + cd + "秒后启动自动触发tos_player_blackmarket_list");
                TimerManager.SetTimeOut(cd + 3, WakeupTimer, true);
            }
            else
            {
                Logger.Log("==========================WakeupTimer: 剩余" + cd + "秒，不启动计时器");
            }
        }
        else if((bool)running==true)
        {
            Logger.Log("==========================WakeupTimer: 自动触发tos_player_blackmarket_list");
            NetLayer.Send(new tos_player_blackmarket_list());
        }
    }

    private void Toc_player_mall_record(toc_player_mall_record tocData)
    {
        _toc_mall_record = tocData;
        if (isFromPackage)
        {
            m_dataGrid.Data = AddRecordData();
        }
        else
        {
            m_dataGrid.Data = CreateGroupItems(m_curType, m_curSubType, m_curVipTag, m_meneyFilter, m_curShopType);
        }
    }


    
    CDMallItem[] AddRecordData()
    {
        List<CDMallItem> list = new List<CDMallItem>();
        if (m_dataGrid.Data != null)
        {
            for (int i = 0; i < m_dataGrid.Data.Length; i++)
            {
                var item = m_dataGrid.Data[i] as CDMallItem;
                if (null != _toc_mall_record)
                {
                    for (int j = 0; j < _toc_mall_record.p_record.Length; j++)
                    {
                        if (item.info.ID == _toc_mall_record.p_record[j].mall_id)
                        {
                            var alreadyNo = _toc_mall_record.p_record[j].amount;
                            item.alreadBuyNo = alreadyNo;
                            break;
                        }
                    }
                }
                list.Add(item);
            }
        }
        return list.ToArray();
    }
}
