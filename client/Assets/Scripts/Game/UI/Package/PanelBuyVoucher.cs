﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBuyVoucher : PanelBuy
{
    private Text m_txtTips;
    private string m_descStr;
    DataGrid m_grid;

    public class WayData
    {
        public ConfigVoucherGetWayLine data;
        public int index;
    }

    public class VoucherItem : ItemRender
    {
        Text name;
        GameObject GoPanel;
        GameObject get;
        ConfigVoucherGetWayLine _data;

        public override void Awake()
        {
            name = transform.Find("Text").GetComponent<Text>();
            GoPanel = transform.Find("Button").gameObject;
            get = transform.Find("Image").gameObject;
            UGUIClickHandler.Get(GoPanel).onPointerClick += delegate { UIManager.GotoPanel(_data.ToPanel); UIManager.HidePanel("PanelBuyVoucher"); };

        }

        protected override void OnSetData(object data)
        {
            var info = data as WayData;
            _data = info.data;
            
            if (GetVoucherNum(_data.Key) != 0)
            {
                name.text = info.index.ToString() + "." + info.data.Name + GetStr();
            }
            else
            {
                name.text = info.index.ToString() + "." + info.data.Name; 
            }
            GoPanel.TrySetActive((GetVoucherNum(_data.Key) != 0) && (GetVoucherNum(_data.Key) > GetVoucherNumComplete(_data.Key)));
            get.TrySetActive((GetVoucherNum(_data.Key) != 0) && (GetVoucherNum(_data.Key) == GetVoucherNumComplete(_data.Key)));
            
            
        }

        string GetStr()
        {
            return string.Format("({0}/{1})", GetVoucherNumComplete(_data.Key), GetVoucherNum(_data.Key));
        }


        bool CheckHasleft()
        {
            if (GetVoucherNum(_data.Key) == GetVoucherNumComplete(_data.Key))
            {
                return false;
            }
            return true;
        }

        int GetVoucherNum(string key)
        {
            if (_data.Key == "Mission")
            {
                return PlayerMissionData.GetAllVoucherMission();
            }
            if (_data.Key == "OnLine")
            {
                return WelfareDataManger.onlineVoucher;
            }
            if (_data.Key == "Sign")
            {
                return WelfareDataManger.signVoucher;
            }
            if (_data.Key == "corpsMission")
            {
                if (CorpsDataManager.IsJoinCorps())
                {
                    return CorpsModel.corpsVoucher;
                }
                else
                {
                    int corpsVoucher = 0;
                    var source_data = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().m_dataArr;
                    for (int i = 0; i < source_data.Length; i++)
                    {
                        var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(source_data[i].Reward);
                        if (reward != null)
                        {
                            for (int j = 0; j < reward.ItemList.Length; j++)
                            {
                                if (reward.ItemList[j].ID == 5052)
                                {
                                    corpsVoucher += reward.ItemList[j].cnt;
                                }

                            }

                        }
                    }
                    return corpsVoucher;
                }
            }
            if (key == "NewSign")
            {
                return SevenDayRewardCache.voucher;
            }


            return 0;
        }

        int GetVoucherNumComplete(string key)
        {
            if (_data.Key == "Mission")
            {
                return PlayerMissionData.GetAllVoucherMissionComplete();
            }
            if (_data.Key == "OnLine")
            {
                return WelfareDataManger.onlineVoucherGet;
            }
            if (_data.Key == "Sign")
            {
                return WelfareDataManger.signVoucherGet;
            }
            if (_data.Key == "corpsMission")
            {
                if (CorpsDataManager.IsJoinCorps())
                {
                    return CorpsModel.corpsVoucherGet;
                }
               
            }
            if (key == "NewSign")
            {
                return SevenDayRewardCache.voucherGet;
            }
            return 0;
        } 

    }


    public PanelBuyVoucher()
    {
        SetPanelPrefabPath("UI/Package/PanelBuyVoucher");
        AddPreLoadRes("UI/Package/PanelBuyVoucher", EResType.UI);
        AddPreLoadRes("UI/Package/ItemBuyRender", EResType.UI);
    }

    public override void Init()
    {
        base.Init();
        m_txtTips = m_tran.Find("background/desc/content").GetComponent<Text>();

        if (m_params != null)
        {
            m_descStr = m_params[3] as string;
        }
        m_grid = m_tran.Find("background/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_grid.SetItemRender(m_tran.Find("background/ScrollDataGrid/content/ItemMission").gameObject, typeof(VoucherItem));

        var list = new List<ConfigVoucherGetWayLine>(ConfigManager.GetConfig<ConfigVoucherGetWay>().m_dataArr);
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Key == "NewSign")
            {
                if (!PanelHallBattle.showSevenDay)
                {
                    list.RemoveAt(i);
                }
            }
        }



        WayData[] datas = new WayData[list.Count];
        
        for (int i = 0; i < list.Count; i++)
        {
            datas[i] = new WayData();
            datas[i].index = i + 1;
            datas[i].data = list[i];
        }
        m_grid.Data = datas;
    }

    public override void OnShow()
    {
        base.OnShow();
        if (!String.IsNullOrEmpty(m_descStr))
        {
            m_txtTips.text = m_descStr;
        }
        var list = new List<ConfigVoucherGetWayLine>(ConfigManager.GetConfig<ConfigVoucherGetWay>().m_dataArr);
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Key == "NewSign")
            {
                if (!PanelHallBattle.showSevenDay)
                {
                    list.RemoveAt(i);
                }
            }
        }



        WayData[] datas = new WayData[list.Count];

        for (int i = 0; i < list.Count; i++)
        {
            datas[i] = new WayData();
            datas[i].index = i + 1;
            datas[i].data = list[i];
        }
        m_grid.Data = datas;


    }

    public override void InitEvent()
    {
        base.InitEvent();
    }
}