﻿using UnityEngine.UI;

public class BagRenderInfo
{
    public bool isLock;
    public bool isCanBuy;
    public int bagId;
    public string label;
}

public class ItemBagRender:ItemRender
{
    private Image m_imgLock;
    private Text m_txtBagNo;
    private Toggle m_toggle;

    public override void Awake()
    {
        var tran = transform;
        m_toggle = GetComponent<Toggle>();
        m_imgLock = tran.Find("lock").GetComponent<Image>();
        m_txtBagNo = tran.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_renderData = data;
        var vo = data as BagRenderInfo;
        if (vo == null)
            return;

        m_toggle.interactable = !vo.isLock;
        m_imgLock.enabled = vo.isLock && !vo.isCanBuy;
        m_txtBagNo.enabled = !vo.isLock || vo.isCanBuy;
        m_txtBagNo.text = !vo.isLock ? vo.label : "+";
        m_txtBagNo.fontSize = vo.isCanBuy ? 40 : 22;
    }
}