﻿using UnityEngine;
using UnityEngine.UI;

public class PackageItemRender : ItemRender
{
    protected PackageRenderInfo m_data;
    
    protected Text _name_txt;
    protected Text _time_txt;
    protected Text _bullet_txt;
    protected Image _item_img;
    private Transform m_tranStarParent;
    private Transform m_tranAwakenParent;
    private Transform m_part_group;
    private Image m_imgDurability;
    private Text m_txtDurability;
    private Image m_imgBroken;
    private Transform m_levelTagTran;
    private Image m_levelTag;
    private GameObject m_starGroup;
    public override void Awake()
    {
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _time_txt = transform.Find("time_txt").GetComponent<Text>();
        var tran = transform.Find("bullet_txt");
        _bullet_txt = tran!=null ? tran.GetComponent<Text>() : null;

        _item_img = transform.Find("item_img").GetComponent<Image>();
        m_tranStarParent = transform.Find("starGroup/star");
        Transform t = transform.Find("starGroup");
        m_starGroup = t == null ? null : t.gameObject;            
        m_tranAwakenParent = transform.Find("awakenGroup/avatar");
        m_part_group = transform.Find("part_group");
        if (m_part_group != null && m_part_group.childCount > 0)
        {
            for (int i = 0; i < m_part_group.childCount; i++)
            {
                var render = m_part_group.GetChild(i).gameObject.AddComponent<UIItemRender>();
                render.SetStyle(AtlasName.Package,"part_frame_");
                UGUIClickHandler.Get(render.gameObject).onPointerClick += OnPartItemClick;
            }
        }

        m_levelTagTran = transform.Find("levelTag");
        if (m_levelTagTran != null)
            m_levelTag = m_levelTagTran.GetComponent<Image>();
        var tranDurability = transform.Find("imgDurability/value");
        if (tranDurability != null)
        {
            m_imgDurability = tranDurability.GetComponent<Image>();
            m_txtDurability = transform.Find("imgDurability/Text").GetComponent<Text>();
            m_imgBroken = transform.Find("imgDurability/imgBroken").GetComponent<Image>();
        }
        if (_bullet_txt!=null)
        {
            UGUIClickHandler.Get(_bullet_txt.gameObject).onPointerClick += onBulletItemClick;
        }
    }

    private void OnPartItemClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (m_data == null || m_data.uid == 0)
        {
            return;
        }
        var info = ItemDataManager.GetItemByUid(m_data.uid);
        if (info != null)
        {
            var vo = target.GetComponent<UIItemRender>().data;
            var pos_key = vo != null && vo.extend is KeyValue<int, int> ? ((KeyValue<int, int>)vo.extend).value.ToString() : "";
            UIManager.ShowPanel<PanelStrengthenMain>(new object[] { "detail#" + info.ID + "#" + pos_key });
        }
    }

    private void onBulletItemClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (m_data == null || m_data.uid==0)
        {
            UIManager.ShowTipPanel("未装备主武器无法查看穿甲弹");
            return;
        }
        var info = ItemDataManager.GetItemByUid(m_data.uid) as ConfigItemWeaponLine;
        var bullet_info = ItemDataManager.GetItem(info.AmmoID) as ConfigItemBulletLine;
        if (bullet_info != null)
        {
            var bullet_data = ItemDataManager.GetDepotItem(info.AmmoID);
            if (bullet_data == null || bullet_data.item_cnt == 0)
            {
                UIManager.ShowTipPanel("您的仓库中没有此类穿甲弹，可以前往商城选购", () => UIManager.ShowPanel<PanelMall>(new object[] { "EQUIP", bullet_info.SubType }), null, true, false, "前往商城");
            }
        }
        else
        {
            UIManager.ShowTipPanel("找不到该穿甲弹配置信息");
        }
    } 

    protected override void OnSetData(object data)
    {
        m_data = data as PackageRenderInfo;
        if (m_data == null)
        {
            return;
        }        
        if(m_starGroup!=null)
            m_starGroup.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
        var info = ItemDataManager.GetItemByUid(m_data.uid);
        ConfigItemWeaponLine cfgWeapon = info != null ? ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(info.ID) : null; ;
        if (cfgWeapon != null && !string.IsNullOrEmpty(cfgWeapon.LevelTag)&&m_levelTagTran!=null)
        {
            m_levelTagTran.gameObject.TrySetActive(true);
            m_levelTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, cfgWeapon.LevelTag));
            m_levelTag.SetNativeSize();
        }
        else
        {
            if(m_levelTagTran!=null)
                m_levelTagTran.gameObject.TrySetActive(false);
        }
        var pitem = m_data.uid > 0 ? ItemDataManager.GetDepotItemByUid(m_data.uid) : null;
        if (m_data.uid != 0)
        {
            if (m_data.subType == "WEAPON1" || m_data.subType == "WEAPON2" || m_data.subType == "DAGGER")
            {
                _name_txt.text = info.DispName;
            }
            else
            {
                _name_txt.text = info.DispName;
            }
        }
        else
        {
            _name_txt.text = m_data.defaultName;
        }
        if (m_imgDurability != null)
            m_imgDurability.fillAmount = pitem != null ? 1f * pitem.durability / ItemDataManager.MAX_DURABILITY : 0;
        if (m_txtDurability != null)
            m_txtDurability.text = "耐久度：" + (pitem != null ? pitem.durability + "/" + ItemDataManager.MAX_DURABILITY : "0/0"); 
        if (m_imgBroken != null)
            m_imgBroken.gameObject.TrySetActive(pitem != null && pitem.durability == 0);
        _item_img.gameObject.TrySetActive(m_data.uid != 0);
        if (m_data.uid != 0 && !string.IsNullOrEmpty(info.Icon))
        {
            _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            _item_img.SetNativeSize();
            _item_img.enabled = true;
        }
        else
        {
            _item_img.enabled = false;
        }
        _time_txt.gameObject.TrySetActive(pitem!=null);
        if (pitem != null)
        {
            _time_txt.text = info.AddRule == 1 || info.AddRule == 4 ? TimeUtil.GetRemainTimeString(pitem.time_limit) : TimeUtil.GetRemainCountString(pitem.item_cnt);
        }
        if (_bullet_txt != null && info is ConfigItemWeaponLine)
        {
            var weaponInfo = info as ConfigItemWeaponLine;
            var bullet_info = info != null ? ItemDataManager.GetItem(weaponInfo.AmmoID) : null;
            if (bullet_info != null)
            {
                var bullet_data = ItemDataManager.GetDepotItem(weaponInfo.AmmoID);
                _bullet_txt.text = bullet_info.DispName + "\n" + (bullet_data!=null ? bullet_data.item_cnt : 0);
            }
            else
            {
                _bullet_txt.text = "穿甲弹：0";
            }
        }
        
        if (m_tranStarParent != null || m_part_group!=null || m_tranAwakenParent!=null)
        {
            var strengthenInfo = info != null ? StrengthenModel.Instance.getSStrengthenDataByCode(info.ID) : null;
            if (m_tranAwakenParent != null)
            {
                m_tranAwakenParent.parent.gameObject.TrySetActive(strengthenInfo != null && strengthenInfo.awaken_data != null && strengthenInfo.awaken_data.level > 0);
                if (m_tranAwakenParent.parent.gameObject.activeSelf)
                {
                    var show_num = Mathf.Ceil(strengthenInfo.awaken_data.level / 2f);
                    for (int i = 0; i < m_tranAwakenParent.childCount; i++)
                    {
                        var img = m_tranAwakenParent.GetChild(i).GetComponent<Image>();
                        img.gameObject.TrySetActive(i < show_num);
                        if (img.gameObject.activeSelf)
                        {
                            img.fillAmount = strengthenInfo.awaken_data.level < (i + 1) * 2 ? 0.5f : 1f;
                        }
                    }
                }
            }
            if (m_tranStarParent != null)
            {
                m_tranStarParent.parent.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen&& strengthenInfo != null && (m_tranAwakenParent==null || !m_tranAwakenParent.parent.gameObject.activeSelf));
                if (m_tranStarParent.parent.gameObject.activeSelf)
                {
                    for (int i = 0; i < m_tranStarParent.childCount; i++)
                    {
                        m_tranStarParent.GetChild(i).gameObject.TrySetActive(i < strengthenInfo.state);
                    }
                }
            }
            if (m_part_group != null)
            {
                var part_data = StrengthenModel.Instance.GenWeaponPartCDData(pitem, strengthenInfo != null ? strengthenInfo.state : 0);
                for (int i = 0; i < m_part_group.childCount; i++)
                {
                    var render = m_part_group.GetChild(i).GetComponent<UIItemRender>();
                    var part_vo = part_data != null && i < part_data.Length ? part_data[i] : null;
                    render.SetData(part_vo);
                    var status_img = render.transform.Find("status_img").GetComponent<Image>();
                    if(part_vo==null || part_vo.sdata.item_id<=0)
                    {
                        status_img.enabled = true;
                        status_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, part_vo == null || part_vo.sdata.item_id == -1 ? "lock" : "unknow"));
                        status_img.SetNativeSize();
                    }
                    else
                    {
                        status_img.enabled = false;
                    }
                }
            }
        }
    }
}

public class PackageRenderInfo
{
    public string defaultName;
    public string subType;
    public int uid;
    public string icon;
    public float width;
    public int group;
}