﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelMallSend : BasePanel
{
    ConfigMallLine _data;

    int m_selectIndex;
    FriendData _selected_friend;

    Image _item_img;
    Text _name_txt;
    Text m_txtMoney;
    Image m_imgCoin;
    InputField _friend_txt;
    InputField _content_txt;
    Transform _friend_list;
    DataGrid _friend_dg;
    DataGrid m_dataGrid;
    Button m_btnSend;
    Text m_txtLockTip;
    private Toggle m_friend;
    private Image m_imgBg;
    private Image m_imgBgBar;

    public PanelMallSend()
    {
        SetPanelPrefabPath("UI/Package/PanelMallSend");

        AddPreLoadRes("UI/Package/PanelMallSend", EResType.UI);
        AddPreLoadRes("UI/Package/ItemBuyRender", EResType.UI);
//        AddPreLoadRes("UI/Package/FriendListItemRender", EResType.UI);
    }

    public override void Init()
    {
        m_btnSend = m_tran.Find("content/ok_btn").GetComponent<Button>();
        _item_img = m_tran.Find("content/item_img").GetComponent<Image>();
        _name_txt = m_tran.Find("content/name_txt").GetComponent<Text>();
        m_txtMoney = m_tran.Find("content/txtMoney").GetComponent<Text>();
        m_imgCoin = m_tran.Find("content/imgCoin").GetComponent<Image>();
        _friend_txt = m_tran.Find("content/friend_txt").GetComponent<InputField>();
        _friend_txt.gameObject.AddComponent<InputFieldFix>();
        _content_txt = m_tran.Find("content/content_txt").GetComponent<InputField>();
        _content_txt.gameObject.AddComponent<InputFieldFix>();
        m_friend = m_tran.Find("content/friend_btn").GetComponent<Toggle>();

        m_txtLockTip = m_tran.Find("content/txtLockTip").GetComponent<Text>();
        m_imgBg = m_tran.Find("content/imgBg").GetComponent<Image>();
        m_imgBgBar = m_tran.Find("content/imgBgBar").GetComponent<Image>();

        _friend_list = m_tran.Find("content/friend_list");
        _friend_dg = _friend_list.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        _friend_dg.autoSelectFirst = false;
        _friend_dg.useLoopItems = true;
        _friend_dg.SetItemRender(m_tran.Find("content/friend_list/ScrollDataGrid/content/FriendListItemRender").gameObject, typeof(FriendListItemRender));

        m_dataGrid = m_tran.Find("content/group").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemBuyRender"), typeof(ItemBuyRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("content/close_btn")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("content/friend_btn")).onPointerClick += OnFriendBtnClick;
        UGUIClickHandler.Get(m_btnSend.gameObject).onPointerClick += OnSendBtnClick;

        _friend_txt.onEndEdit = new InputField.SubmitEvent();
        _friend_txt.onEndEdit.AddListener(OnChangedFriendData);

        m_dataGrid.onItemSelected = OnChangeSelectedMode;
        _friend_dg.onItemSelected = OnChangedFriendData;
    }

    private void OnChangedFriendData(object data)
    {
        if (data == null)
        {
            return;
        }
        if (data is FriendData)
        {
            _selected_friend = data as FriendData;
            _friend_txt.text = _selected_friend.m_name;
        }
        else
        {
            _selected_friend = null;
            var name_str = data as string;
            for (int i = 0; i < PlayerFriendData.singleton.friendList.Count; i++)
            {
                if (PlayerFriendData.singleton.friendList[i].m_name == name_str)
                {
                    _selected_friend = PlayerFriendData.singleton.friendList[i];
                    break;
                }
            }
        }

        m_friend.isOn = false;
        _friend_list.gameObject.TrySetActive(false);
    }

    private void OnChangeSelectedMode(object renderData)
    {
        ItemBuyRender target = null;
        foreach (var render in m_dataGrid.ItemRenders)
        {
            if (render.m_renderData == renderData)
            {
                target = render as ItemBuyRender;
            }
        }
        if (target != null && target.enabled)
        {
            var data = renderData as ItemBuyInfo;
            m_selectIndex = data.index;
            m_txtMoney.text = ItemDataManager.GetPrice(_data, data.price).ToString();
            m_imgCoin.SetSprite(ResourceManager.LoadMoneyIcon(data.priceType == EnumMoneyType.NONE ? _data.MoneyType : data.priceType));
            m_imgCoin.SetNativeSize();

            if (data.needUnlock)
            {
                var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(data.item_id);
                if ((sdata.state >= 3 && sdata.level >= 9))
                {
                    m_btnSend.interactable = true;
                    m_txtLockTip.enabled = false;
                }
                else
                {
                    m_btnSend.interactable = false;
                    m_txtLockTip.enabled = true;
                }
            }
            else
            {
                m_btnSend.interactable = true;
                m_txtLockTip.enabled = false;
            }
        }
    }

    private void OnFriendBtnClick(GameObject target, PointerEventData eventData)
    {
        _friend_list.gameObject.TrySetActive(m_friend.isOn);
        if (_friend_list.gameObject.activeSelf)
        {
            _friend_dg.Data = PlayerFriendData.singleton.friendList.ToArray();
        }
    }

    private void OnSendBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data == null)
        {
            return;
        }
        if (_selected_friend == null)
        {
            TipsManager.Instance.showTips("只有你的好友才能收到你的礼物哦");
            return;
        }
        NetLayer.Send(new tos_player_send_mail_gift() { to_pid=_selected_friend.m_id, mall_id=_data.ID, index=m_selectIndex, title="赠送物品", content=string.IsNullOrEmpty(_content_txt.text) ? "送你一个好东西":_content_txt.text });
        HidePanel();
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _data = m_params[0] as ConfigMallLine;
        }
        m_selectIndex = 0;
        m_friend.isOn = false;
        _friend_list.gameObject.TrySetActive(false);

        var info = ItemDataManager.GetItem(_data.ItemId);
        ResourceManager.LoadSprite(AtlasName.ICON, info.Icon, res => { _item_img.SetSprite(res); _item_img.SetNativeSize(); });
        _name_txt.text = info.DispName;
        _friend_txt.text = "";
        _content_txt.text = "";

        m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(info.RareType)));
        m_imgBgBar.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, ColorUtil.GetRareColorName(info.RareType) + "_bg"));

        var addRule = info.AddRule;
        var data = new List<ItemBuyInfo>();
        for (int i = 0; i < _data.Price.Length; i++)
        {
            var priceItem = _data.Price[i];
            data.Add(new ItemBuyInfo() { index = i + 1, item_id = _data.ItemId, addRule = addRule, number = priceItem.time, price = priceItem.price, priceType = priceItem.type, needUnlock = _data.Type == "WEAPON" && priceItem.time == 0 && _data.CheckStrengthenLevel != 0 });
        }
        m_dataGrid.Data = data.ToArray();
    }

    public override void OnHide()
    {
        _data = null;
        _selected_friend = null;
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}

