﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PanelBuyInstallment:BasePanel
{
    public class ItemBuy : ItemRender
    {


        Text m_text;
        public ItemBuyInfo info;

        public override void Awake()
        {
            m_text = transform.Find("Text").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            info = data as ItemBuyInfo;
            m_text.text = "第" + info.index + "期";
            if (info.index == UIManager.GetPanel<PanelBuyInstallment>().haspayed + 1)
            {
                Util.SetNormalShader(transform);
            }
            else
            {
                Util.SetGrayShader(transform);
            }
        }
    }




    public Text m_name;
    public Image m_coin;
    public DataGrid m_buylist;
    public Text m_money;
    public ConfigMallLine data;
    public int indexselect;
    public int haspayed;
    
    public PanelBuyInstallment()
    {
        SetPanelPrefabPath("UI/Package/PanelBuyInstallment");
        AddPreLoadRes("UI/Package/PanelBuyInstallment", EResType.UI);
    }

    


    public override void Init()
    {
        m_name = m_tran.Find("background/txtName").GetComponent<Text>();
        m_coin = m_tran.Find("background/imgCoin").GetComponent<Image>();
        m_money = m_tran.Find("background/txtMoney").GetComponent<Text>();
        m_buylist = m_tran.Find("background/group").gameObject.AddComponent<DataGrid>();
        m_buylist.SetItemRender(m_tran.Find("background/group/ItemBuyRender").gameObject, typeof(ItemBuy));
        indexselect = 1;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("background/btnBuy")).onPointerClick += buyItem;
        m_buylist.onItemSelected += OnItemSelected;
    }


    private void OnItemSelected(object renderData)
    {
        ItemBuy target = null;
        foreach (var render in m_buylist.ItemRenders)
        {
            if (render.m_renderData == renderData)
            {
                target = render as ItemBuy;
            }
        }

        m_money.text = target.info.price.ToString();
        var m_priceType = target.info.priceType == EnumMoneyType.NONE ? data.MoneyType : target.info.priceType;
        m_coin.SetSprite(ResourceManager.LoadMoneyIcon(m_priceType));
        indexselect = target.info.index;
        
    }

    void buyItem(GameObject target, PointerEventData pdata)
    {
        NetLayer.Send(new tos_player_buy_item() { mall_id = data.ID, days = indexselect, in_blackmarket = false });
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        if (!HasParams())
        {
            return;
        }
        data = m_params[0] as ConfigMallLine;
        var data_list = new List<ItemBuyInfo>();
        var addRule = ItemDataManager.GetItem(data.ItemId).AddRule;
        for (int i = 0; i < data.Price.Length; i++)
        {
            var priceItem = data.Price[i];
            data_list.Add(new ItemBuyInfo()
            {
                index = i + 1,
                item_id = data.ItemId,
                addRule = addRule,
                number = priceItem.time,
                price = priceItem.price,
                priceType = priceItem.type,
                needUnlock = false
            });
        }
        m_name.text = data.Name;
        m_buylist.Data = data_list.ToArray();
        var ph = ItemDataManager.GetPhasedInfoByItemId(data.ItemId, data.Price.Length);
        if (ph != null)
        {
            haspayed = ph.phased_number;
        }
        else
        {
            haspayed = 0;
        }

        m_buylist.UpdateView();
        if (ItemDataManager.GetPhasedInfoByMallId(data.ID)!=null)
        {
            m_buylist.Select(ItemDataManager.GetPhasedInfoByMallId(data.ID).phased_number);

        }
        else
        {
            m_buylist.Select(0);
        }

        
    }

    void Toc_player_phased_item_update(toc_player_phased_item_update tdata)
    {
        var ph = ItemDataManager.GetPhasedInfoByItemId(data.ItemId, data.Price.Length);
        if (ph != null)
        {
            haspayed = ph.phased_number;
            m_buylist.Select(ItemDataManager.GetPhasedInfoByMallId(data.ID).phased_number);
        }
        else
        {
            haspayed = 0;
            m_buylist.Select(0);
        }

        m_buylist.UpdateView();
        
    }

    void Toc_player_phased_item_new(toc_player_phased_item_new tdata)
    {
        var ph = ItemDataManager.GetPhasedInfoByItemId(data.ItemId, data.Price.Length);
        if (ph != null)
        {
            haspayed = ph.phased_number;
            m_buylist.Select(ItemDataManager.GetPhasedInfoByMallId(data.ID).phased_number);
        }
        else
        {
            haspayed = 0;
            m_buylist.Select(0);
        }

        m_buylist.UpdateView();
    }

    void Toc_player_phased_item_del(toc_player_phased_item_del tdata)
    {
        var ph = ItemDataManager.GetPhasedInfoByItemId(data.ItemId, data.Price.Length);
        if (ph != null)
        {
            haspayed = ph.phased_number;
            m_buylist.Select(ItemDataManager.GetPhasedInfoByMallId(data.ID).phased_number);
        }
        else
        {
            haspayed = 0;
            m_buylist.Select(0);
        }

        m_buylist.UpdateView();
        
    }

    void ChooseNowTime(int i)
    {
        m_buylist.Select(i);
    }


    public override void Update()
    {
 
    }
}

