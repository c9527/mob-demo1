﻿using UnityEngine.UI;

public class ItemHeroWeaponTabbarRender : ItemRender
{
    private Text m_text;
    public override void Awake()
    {
        m_text = transform.Find("Background/Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_renderData = data;
        var line = data as CDMallItem;
        if (line == null)
        {
            m_text.text = "";
            return;
        }
        m_text.text = line.info.Name;
    }
}
