﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class FriendListItemRender : ItemRender
{
    FriendData _data;

    Image _avatar_img;
    Text _name_txt;

    public override void Awake()
    {
        _avatar_img = transform.Find("avatar_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as FriendData;
        if (_data == null)
        {
            return;
        }
        _avatar_img.SetSprite(ResourceManager.LoadRoleIcon(_data.m_icon));
        //_avatar_img.SetNativeSize();
        _name_txt.text = _data.m_name;
    }
}

