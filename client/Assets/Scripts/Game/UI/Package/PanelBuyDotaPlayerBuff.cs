﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// dota模式的buff枚举，与gameshop对应
/// </summary>
public enum DotaBuffType
{
    /// <summary>
    /// 自己血量增加
    /// </summary>
    DotaBuff_PVP = 3,
    /// <summary>
    /// BOSS复活
    /// </summary>
    DotaBuff_BOSS_REVIVE = 4,
    /// <summary>
    /// BOSS血量增加
    /// </summary>
    //DotaBuff_BOSS = 5,
    /// <summary>
    /// PVE武器伤害增加
    /// </summary>
    DotaBuff_PVE = 6,
    /// <summary>
    /// 治疗
    /// </summary>
    DotaBuff_CURE = 5,
    /// <summary>
    /// 无技能
    /// </summary>
    DotaBuff_NONE = 0,
}

public class DotaBuffItem
{
    protected GameObject m_go;
    protected RectTransform m_tran;

    public DotaBuffType m_type;
    private string prefix;

    private Text m_costText;
    private Text m_btnLabel;

    private GameObject m_goCost;
    private GameObject m_goBtn;

    public int m_selectIndex;
    public bool isSendMsg = false;
    public bool isFull = false;

    private GameObject m_goprogressTextUp;
    private GameObject m_goprogressTextDown;
    private GameObject m_goprogressImage;

    private List<Text> m_progressTextUpList = new List<Text>();
    private List<Text> m_progressTextDownList = new List<Text>();
    private List<Image> m_progressImageList = new List<Image>();

    private const int ProgressItemCount = 5;

    public DotaBuffItem(GameObject go, RectTransform tran, DotaBuffType type = DotaBuffType.DotaBuff_BOSS_REVIVE)
    {
        m_go = go;
        m_tran = tran;
        m_type = type;
        if (m_type == DotaBuffType.DotaBuff_PVE)
            prefix = "background/group/damageAI/";
        else if (m_type == DotaBuffType.DotaBuff_PVP)
            prefix = "background/group/damagePlayer/";
        //else if (m_type == DotaBuffType.DotaBuff_BOSS)
        //    prefix = "background/group/boss/";
        else if (m_type == DotaBuffType.DotaBuff_CURE)
            prefix = "background/group/cure/";
        else
            prefix = "background/group/bossrevive/";
        Logger.Log(string.Format("DotaBuffItem, type: {0}", m_type));

        m_costText = m_tran.Find(prefix + "cost/txtMoney").GetComponent<Text>();
        m_btnLabel = m_tran.Find(prefix + "btnBuyBg/btnBuy/Text").GetComponent<Text>();

        m_goCost = m_tran.Find(prefix + "cost").gameObject;
        m_goBtn = m_tran.Find(prefix + "btnBuyBg").gameObject;

        UGUIClickHandler.Get(m_tran.Find(prefix + "btnBuyBg/btnBuy").gameObject).onPointerClick += OnBuyBtnClick;

        if (m_type != DotaBuffType.DotaBuff_BOSS_REVIVE)
        {
            m_goprogressTextUp = m_tran.Find(prefix + "ProgressBar/ProgressUpTextGroup").gameObject;
            m_goprogressTextDown = m_tran.Find(prefix + "ProgressBar/ProgressDownTextGroup").gameObject;
            m_goprogressImage = m_tran.Find(prefix + "ProgressBar/ProgressImageGroup").gameObject;

            m_goprogressTextUp.TrySetActive(true);
            m_goprogressTextDown.TrySetActive(m_type == DotaBuffType.DotaBuff_CURE);
            m_goprogressImage.TrySetActive(true);

            for (int i = 0; i < ProgressItemCount; ++i)
            {
                m_progressTextUpList.Add(m_goprogressTextUp.transform.Find("ProgressUpText_" + (i + 1)).GetComponent<Text>());
                m_progressTextDownList.Add(m_goprogressTextDown.transform.Find("ProgressDownText_" + (i + 1)).GetComponent<Text>());
                m_progressImageList.Add(m_goprogressImage.transform.Find("ProgressImage_" + i + "/ProgressImageBg/ProgressImageFg").GetComponent<Image>());
            }
            m_tran.Find(prefix + "btnBuyBg/btnBuy/img").gameObject.TrySetActive(false);
            if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web))
            {
                m_tran.Find(prefix + "btnBuyBg/btnBuy/img").gameObject.TrySetActive(true);
            }
        }
    }

    public void onShow()
    {
        Logger.Log(string.Format("buff类型~~~:{0}", (int)m_type));
        int curBuffShopId = -1;
        int startBuffShopId = -1;
        ConfigGameShopLine nextBuffLine = null;
        int supplypoint = -1;

        DotaBehavior behavior = null;
        behavior = DotaBehavior.singleton;
        if (behavior != null)
        {
            curBuffShopId = GetCurShopId();
            startBuffShopId = GetStartShopId();
            supplypoint = behavior.supplypoint;

            Logger.Log(string.Format("curBuffShopId: {0}", curBuffShopId));
            Logger.Log(string.Format("startBuffShopId: {0}", startBuffShopId));

            if (m_type != DotaBuffType.DotaBuff_BOSS_REVIVE)
            {
                ConfigBuff configBuff = ConfigManager.GetConfig<ConfigBuff>();
                float effectUp = 0;
                int effectDown = 0;
                if (startBuffShopId != -1)
                {
                    for (int i = 0; i < ProgressItemCount; ++i)
                    {
                        var shopLine = ConfigManager.GetConfig<ConfigGameShop>().GetLine(startBuffShopId + i);

                        if (shopLine == null)
                        {
                            break;
                        }
                        switch (m_type)
                        {
                            case DotaBuffType.DotaBuff_PVE:
                                effectUp += configBuff.GetLine(shopLine.Param).DotaPveDamageRate;
                                break;
                            case DotaBuffType.DotaBuff_PVP:
                                effectUp += configBuff.GetLine(shopLine.Param).DotaPvpDamageRate;
                                break;
                            case DotaBuffType.DotaBuff_CURE:
                                {
                                    effectUp += configBuff.GetLine(shopLine.Param).TeammateCure;
                                    effectDown += configBuff.GetLine(shopLine.Param).MaxHP;
                                    m_progressTextDownList[i].text = effectDown.ToString();
                                }
                                break;
                        }
                        
                        m_progressTextUpList[i].text = (effectUp * 100) + "%";
                        m_progressImageList[i].enabled = (curBuffShopId >= startBuffShopId + i);
                    }
                }
            }

            if (curBuffShopId != -1)
            {
                nextBuffLine = ConfigManager.GetConfig<ConfigGameShop>().GetLine(curBuffShopId + 1);
            }
            else
                nextBuffLine = ConfigManager.GetConfig<ConfigGameShop>().GetLine(startBuffShopId);

            if (nextBuffLine == null || nextBuffLine.Type != (int)m_type)
            {
                if (!isBossRevive)
                {

                }
                m_goCost.TrySetActive(false);
                m_goBtn.TrySetActive(false);

                isFull = true;
                isSendMsg = false;

                //m_txtSupplyAllNumTip.text = "已达满级";
                m_btnLabel.text = "确    定";
            }
            else
            {
                m_goCost.TrySetActive(true);
                m_goBtn.TrySetActive(true);

                int all = supplypoint;
                int need = nextBuffLine.Costs;

                isFull = false;
                isSendMsg = all >= need;
                m_costText.text = need.ToString();

                if (!isBossRevive)
                {
                    m_btnLabel.text = "提升";
                    m_selectIndex = nextBuffLine.ShopId;
                }
                else
                {
                    m_btnLabel.text = "购买";
                    m_selectIndex = nextBuffLine.ShopId;
                }
            }
            Logger.Log(string.Format("商店id:{0}", m_selectIndex));
        }
    }

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        Logger.Log(string.Format("OnBuyBtnClick, type: {0}, shopId:{1}", m_type, m_selectIndex));
        if (isSendMsg)
        {
            Logger.Log("OnBuyBtnClick: index: " + m_selectIndex);
            NetLayer.Send(new tos_fight_game_shop() { shop_id = m_selectIndex });
        }
        else
        {
            if (!isFull) TipsManager.Instance.showTips("补给点不足");
        }
    }

    private bool isBossRevive
    {
        get { return m_type == DotaBuffType.DotaBuff_BOSS_REVIVE; }
    }

    private int GetCurShopId()
    {
        if (m_type == DotaBuffType.DotaBuff_PVE)
            return DotaBehavior.singleton.pveBuffShopIdSrv;
        else if (m_type == DotaBuffType.DotaBuff_PVP)
            return DotaBehavior.singleton.pvpBuffShopIdSrv;
        //else if (m_type == DotaBuffType.DotaBuff_BOSS)
        //    return DotaBehavior.singleton.bossBuffShopIdSrv;
        else if (m_type == DotaBuffType.DotaBuff_CURE)
            return DotaBehavior.singleton.cureBuffShopIdSrv;
        else
            return DotaBehavior.singleton.bossReviveBuffShopIdSrv;
    }

    private int GetStartShopId()
    {
        if (m_type == DotaBuffType.DotaBuff_PVE)
            return DotaBehavior.singleton.pveBuffShopIdStart;
        else if (m_type == DotaBuffType.DotaBuff_PVP)
            return DotaBehavior.singleton.pvpBuffShopIdStart;
        //else if (m_type == DotaBuffType.DotaBuff_BOSS)
        //    return DotaBehavior.singleton.bossBuffShopIdStart;
        else if (m_type == DotaBuffType.DotaBuff_CURE)
            return DotaBehavior.singleton.cureBuffShopIdStart;
        else
            return DotaBehavior.singleton.bossReviveBuffShopIdStart;
    }
}

public class PanelBuyDotaPlayerBuff : BasePanel
{
    //private Vector3 m_vec0 = new Vector3(25, 0, 0);
    //private Vector3 m_vec1 = new Vector3(120, 0, 0);
    //private Vector3 m_vec2 = new Vector3(-66, -48, 0);
    //private Vector3 m_vec3 = new Vector3(-4, -48, 0);

    private Text m_txtSupplyAllNum;

    private GameObject m_goHas;

    private DotaBuffItem buff_attack;
    private DotaBuffItem buff_blood;
    //private DotaBuffItem buff_boss;
    private DotaBuffItem buff_cure;
    //private DotaBuffItem buff_bossRevive;

    public PanelBuyDotaPlayerBuff()
    {
        SetPanelPrefabPath("UI/Package/PanelBuyDotaPlayerBuff");
        AddPreLoadRes("UI/Package/PanelBuyDotaPlayerBuff", EResType.UI);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtSupplyAllNum = m_tran.Find("background/has/txtSupplyNum").GetComponent<Text>();

        buff_attack = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_PVE);
        buff_blood = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_PVP);
        //buff_boss = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_BOSS);
        buff_cure = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_CURE);
        //buff_bossRevive = new DotaBuffItem(m_go, m_tran, DotaBuffType.DotaBuff_BOSS_REVIVE);
        m_tran.Find("background/tips").gameObject.TrySetActive(false);
        m_goHas = m_tran.Find("background/has").gameObject;
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            m_tran.Find("background/tips").gameObject.TrySetActive(true);
            AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () =>
            {
                if (m_go.activeSelf == false) return;
                OnBuy(buff_attack.isSendMsg, buff_attack.isFull, buff_attack.m_selectIndex);
            });
            AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () =>
            {
                if (m_go.activeSelf == false) return;
                OnBuy(buff_blood.isSendMsg, buff_blood.isFull, buff_blood.m_selectIndex);
            });
            AddEventListener(GameEvent.INPUT_KEY_3_DOWN, () =>
            {
                if (m_go.activeSelf == false) return;
                OnBuy(buff_cure.isSendMsg, buff_cure.isFull, buff_cure.m_selectIndex);
            });
        }
    }

    private void OnBuy(bool isSendMsg, bool isFull,int selectIndex)
    {
        Logger.Log(string.Format("OnBuy, shopId:{0}", selectIndex));
        if (isSendMsg)
        {
            Logger.Log("OnBuy: index: " + selectIndex);
            NetLayer.Send(new tos_fight_game_shop() { shop_id = selectIndex });
        }
        else
        {
            if (!isFull) TipsManager.Instance.showTips("补给点不足");
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    public void Refresh()
    {
        OnShow();
    }

    public override void OnShow()
    {
        int supplypoint = -1;
        DotaBehavior behavior = null;
        behavior = DotaBehavior.singleton;
        if (behavior != null)
        {
            supplypoint = behavior.supplypoint;
            m_txtSupplyAllNum.text = supplypoint.ToString();
        }

        buff_attack.onShow();
        buff_blood.onShow();
        //buff_boss.onShow();
        buff_cure.onShow();
        //buff_bossRevive.onShow();

#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = false;
#endif
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = true;
#endif
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}