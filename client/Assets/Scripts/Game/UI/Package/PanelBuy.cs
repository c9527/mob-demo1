﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBuy : BasePanel
{
    private Text m_txtName;
    private Text m_txtMoney;
    private Image m_imgCoin;
    private Text m_btnLabel;
    private Button m_btnBuy;
    private Text m_txtLockTip;

    private DataGrid m_dataGrid;

    private int m_selectIndex;
    private ConfigMallLine m_data;
    private bool m_is_backmarket;

    private int m_finalPrice;
    private EnumMoneyType m_priceType;
    private bool m_moneyEnough;

    public PanelBuy()
    {
        SetPanelPrefabPath("UI/Package/PanelBuy");
        AddPreLoadRes("UI/Package/PanelBuy", EResType.UI);
        AddPreLoadRes("UI/Package/ItemBuyRender", EResType.UI);
    }

    public override void Init()
    {
        m_txtName = m_tran.Find("background/txtName").GetComponent<Text>();
        m_txtMoney = m_tran.Find("background/txtMoney").GetComponent<Text>();
        m_imgCoin = m_tran.Find("background/imgCoin").GetComponent<Image>();
        m_btnLabel = m_tran.Find("background/btnBuy/Text").GetComponent<Text>();
        m_btnBuy = m_tran.Find("background/btnBuy").GetComponent<Button>();
        m_txtLockTip = m_tran.Find("background/txtLockTip").GetComponent<Text>();

        m_dataGrid = m_tran.Find("background/group").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemBuyRender"), typeof(ItemBuyRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_btnBuy.gameObject).onPointerClick += OnBuyBtnClick;

        m_dataGrid.onItemSelected = OnItemSelected;
    }

    private void OnItemSelected(object renderData)
    {
        ItemBuyRender target = null;
        foreach (var render in m_dataGrid.ItemRenders)
        {
            if (render.m_renderData == renderData)
            {
                target = render as ItemBuyRender;
            }
        }
        if (target != null && target.enabled)
        {
            var data = renderData as ItemBuyInfo;
            m_selectIndex = data.index;
            m_finalPrice = ItemDataManager.GetPrice(m_data, data.price);
            m_txtMoney.text = m_finalPrice.ToString();
            int index = HeroCardDataManager.MallCardArray.ToList<int>().IndexOf(m_data.ID);
            if (index != -1&&index!=0)
            {
                m_txtMoney.text = HeroCardDataManager.getUpCost((EnumHeroCardType)(index + 1), data.price, (int)data.number)+"";
            }
            m_priceType = data.priceType == EnumMoneyType.NONE ? m_data.MoneyType : data.priceType;
            m_imgCoin.SetSprite(ResourceManager.LoadMoneyIcon(m_priceType));
            m_imgCoin.SetNativeSize();

            m_moneyEnough = PlayerSystem.CheckMoney(m_finalPrice, m_priceType);
            m_txtMoney.color = m_moneyEnough ? Color.white : Color.red;

            if (data.needUnlock)
            {
                var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(data.item_id);
                if ((sdata.state >= 3 && sdata.level >= 9))
                {
                    m_btnBuy.interactable = true;
                    m_txtLockTip.enabled = false;
                }
                else
                {
                    m_btnBuy.interactable = false;
                    m_txtLockTip.enabled = true;
                }
            }
            else
            {
                m_btnBuy.interactable = true;
                m_txtLockTip.enabled = false;
            }
        }
    }

    private uint m_sid;
    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!m_btnBuy.interactable)
            return;
        if (!m_moneyEnough && m_priceType == EnumMoneyType.DIAMOND)
        {
            UIManager.ShowTipPanel("钻石不足，是否前往充值？", () =>
            {
                SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
                HidePanel();
            });
            return;
        }
        HidePanel();
        if (!m_moneyEnough && m_priceType == EnumMoneyType.EVENTCOIN_002)
        {
            var needCoin = m_finalPrice - PlayerSystem.roleData.eventcoin_002;
            UIManager.ShowTipPanel(string.Format("要花费{0}钻石购买缺少的点券吗？", needCoin*2), () =>
            {
                NetLayer.Send(new tos_player_diamond2eventcoin_002() {count = needCoin});
                m_sid = NetLayer.SessionId;
            });
            return;
        }
        if (!m_moneyEnough && m_priceType == EnumMoneyType.ACTIVE_COIN)
        {
            string cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("active_coin_not_enough").ValueStr;
            if(!cfg.IsNullOrEmpty())
            {
                UIManager.ShowTipPanel(cfg, () =>
                {
                    if (UIManager.GetPanel<PanelPvpRoomList>() != null)
                    {
                        UIManager.GetPanel<PanelPvpRoomList>().m_filterRule = "";
                        UIManager.GetPanel<PanelPvpRoomList>().UpdateRule();
                        UIManager.GetPanel<PanelPvpRoomList>().UpdateRoomList();
                    }
                    UIManager.GotoPanel("battle");
                }, null, true, false, "立即前往", null);
            }
        }




        NetLayer.Send(new tos_player_buy_item() { mall_id = m_data.ID, days = m_selectIndex, in_blackmarket = m_is_backmarket });
    }

    private void Toc_player_diamond2eventcoin_002(toc_player_diamond2eventcoin_002 proto)
    {
        if (proto.sid != m_sid)
            return;

        NetLayer.Send(new tos_player_buy_item() { mall_id = m_data.ID, days = m_selectIndex, in_blackmarket = m_is_backmarket });
        HidePanel();
    }

    public override void OnShow()
    {
        m_finalPrice = 0;
        m_priceType = EnumMoneyType.NONE;
        m_moneyEnough = true;
        if (!Driver.isMobilePlatform)
        {
            if (UIManager.IsBattle())
                Screen.lockCursor = false;
        }
        if (m_params[0] == null)
        {
            TipsManager.Instance.showTips("暂未开放购买");
            this.HidePanel();
            return;
        }
        m_data = m_params[0] as ConfigMallLine;
        if (PlayerSystem.roleData.level < m_data.UnlockLevel)
        {
            HidePanel();
            UIManager.ShowTipPanel("等级达到"+m_data.UnlockLevel+"后开放购买");
            return;
        }
        if (PlayerSystem.roleData.vip_level < m_data.UnlockVipLevel)
        {
            HidePanel();
            if (m_data.UnlockVipLevel == 2)
                UIManager.ShowTipPanel("尊贵会员可买");
            else if (m_data.UnlockVipLevel == 1)
                UIManager.ShowTipPanel("会员可买");
            return;
        }

        var addRule = ItemDataManager.GetItem(m_data.ItemId).AddRule;

        var data = new List<ItemBuyInfo>();
        for (int i = 0; i < m_data.Price.Length; i++)
        {
            var priceItem = m_data.Price[i];
            data.Add(new ItemBuyInfo()
            {
                index = i + 1,
                item_id = m_data.ItemId,
                addRule = addRule,
                number = priceItem.time,
                price = priceItem.price,
                priceType = priceItem.type,
                needUnlock = m_data.Type == "WEAPON" && priceItem.time == 0 && priceItem.type == EnumMoneyType.NONE && m_data.CheckStrengthenLevel != 0
            });
        }
        m_dataGrid.Data = data.ToArray();
        m_txtName.text = m_data.Name;
        m_btnLabel.text = m_params.Length > 1 && m_params[1].ToString() == "continue" ? "确认续费" : "确认购买";
        m_is_backmarket = m_params.Length > 2 ? (bool)m_params[2] : false;
    }

    public override void OnHide()
    {
        if (!Driver.isMobilePlatform)
        {
            if (UIManager.IsBattle())
                Screen.lockCursor = true;
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}