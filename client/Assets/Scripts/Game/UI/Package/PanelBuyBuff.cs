﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBuyBuff : BasePanel
{
    private Vector3 m_vec0 = new Vector3(25, 0, 0);
    private Vector3 m_vec1 = new Vector3(120, 0, 0);

    private Vector3 m_vec2 = new Vector3(-66, -48, 0);
    private Vector3 m_vec3 = new Vector3(-4, -48, 0);

    private Text m_txtAllNumTip;
    private Text m_txtAllNum;
    private Text m_txtMoney;
    private Text m_txtFrom;
    private Text m_txtTo;
    private Text m_btnLabel;

    private GameObject m_goArrow;
    private GameObject m_goFrom;
    private GameObject m_goTo;
    private GameObject m_goHas;
    private GameObject m_goCost;

    //private DataGrid m_dataGrid;

    private int m_selectIndex;

    private bool isSendMsg = false;
    private bool isFull = false;

    public PanelBuyBuff()
    {
        SetPanelPrefabPath("UI/Package/PanelBuyBuff");
        AddPreLoadRes("UI/Package/PanelBuyBuff", EResType.UI);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtAllNumTip = m_tran.Find("background/txtSupplyTip").GetComponent<Text>();
        m_txtAllNum = m_tran.Find("background/has/txtSupplyNum").GetComponent<Text>();
        m_txtMoney = m_tran.Find("background/cost/txtMoney").GetComponent<Text>();
        m_txtFrom = m_tran.Find("background/from/txtfromNum").GetComponent<Text>();
        m_txtTo = m_tran.Find("background/to/txttoNum").GetComponent<Text>();
        m_btnLabel = m_tran.Find("background/btnBuy/Text").GetComponent<Text>();
        m_goArrow = m_tran.Find("background/Arrow").gameObject;
        m_goFrom = m_tran.Find("background/from").gameObject;
        m_goTo = m_tran.Find("background/to").gameObject;
        m_goHas = m_tran.Find("background/has").gameObject;
        m_goCost = m_tran.Find("background/cost").gameObject;
        //m_dataGrid = m_tran.Find("background/group").gameObject.AddComponent<DataGrid>();
        //m_dataGrid.autoSelectFirst = false;
        //m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemBuyRender"), typeof(ItemBuyBuffRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("background/btnBuy")).onPointerClick += OnBuyBtnClick;

        //m_dataGrid.onItemSelected = OnItemSelected;
    }

    //private void OnItemSelected(object renderData)
    //{
    //    var data = renderData as ItemBuyInfo;
    //    m_selectIndex = data.index;
    //    m_txtMoney.text = data.price.ToString();
    //}

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        if (isSendMsg)
        {
            //Logger.Error("OnBuyBtnClick: index: " + m_selectIndex);
            //NetLayer.Send(new tos_fight_survival_expenditure() { type = 3, item_id = m_selectIndex });
            NetLayer.Send(new tos_fight_game_shop() { shop_id = m_selectIndex });
        }
        else
        {
            if (!isFull) TipsManager.Instance.showTips("补给点不足");
        }
        if (isFull) HidePanel();
    }

    public void Refresh()
    {
        OnShow();
    }

    public override void OnShow()
    {
        int curBuffShopId = -1;
        int startBuffShopId = -1;
        ConfigGameShopLine nextBuffLine = null;
        int supplypoint = -1;
        SurvivalBehavior behavior = null;

        behavior = SurvivalBehavior.singleton;

        if (behavior != null)
        {
            curBuffShopId = behavior.buffShopId;
            startBuffShopId = behavior.startBuffShopId;
            supplypoint = behavior.supplypoint;
            int from = 0;

            if (curBuffShopId != -1)
            {
                nextBuffLine = ConfigManager.GetConfig<ConfigGameShop>().GetLine(curBuffShopId + 1);
                from = behavior.GetAllBuffEffect(curBuffShopId - startBuffShopId);
            }
            else
                nextBuffLine = ConfigManager.GetConfig<ConfigGameShop>().GetLine(startBuffShopId);
            int to = from;

            if (nextBuffLine == null || nextBuffLine.Type != GameConst.SHOP_ITEM_TYPE_PVPBUFF)
            {
                m_goTo.TrySetActive(false);
                m_goHas.TrySetActive(false);
                m_goCost.TrySetActive(false);
                m_goArrow.TrySetActive(false);

                m_goFrom.transform.localPosition = m_vec1;
                m_txtAllNumTip.transform.localPosition = m_vec3;

                isFull = true;
                isSendMsg = false;

                m_txtAllNumTip.text = "已达满级";
                m_btnLabel.text = "确定";
            }
            else
            {
                m_goTo.TrySetActive(true);
                m_goHas.TrySetActive(true);
                m_goCost.TrySetActive(true);
                m_goArrow.TrySetActive(true);

                int all = supplypoint;
                int need = nextBuffLine.Costs;

                isFull = false;
                isSendMsg = all >= need;
                m_txtAllNum.text = all.ToString();
                m_txtMoney.text = need.ToString();

                m_goFrom.transform.localPosition = m_vec0;
                m_txtAllNumTip.transform.localPosition = m_vec2;

                m_txtAllNumTip.text = "剩余补给点：";
                m_btnLabel.text = "升级";

                to = behavior.GetAllBuffEffect(nextBuffLine.ShopId - startBuffShopId);

                m_selectIndex = nextBuffLine.ShopId;
            }

            m_txtFrom.text = "+" + from + "%";
            m_txtTo.text = "+" + to + "%";
        }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = false;
#endif
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = true;
#endif
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}