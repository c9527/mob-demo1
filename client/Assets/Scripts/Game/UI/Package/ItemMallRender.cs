﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemMallRender : ItemRender
{
    private CDMallItem _data;
    public ConfigMallLine m_data;
    private Text m_txtName;
    private Text m_txtMoney;
    private Text m_txtMoneyOld;
    private Text m_txtTip;
//    private Text m_txtEquipTip;
    private Image m_imgItem;
//    private Image m_imgCheck;
    private Image m_imgCoin;
    private Image m_imgCoinOld;
    private Image m_imgRedline;
    private GameObject m_goBuyBtn;
//    private GameObject m_goSendBtn;
//    private GameObject m_goTeQuan;
    private Image m_tips_img;
//    private Image m_levelTag;
//    private GameObject m_levelTagObj;
    private Text m_limitBuyTip;
    private Image m_roleTag;
    private Image m_imgBg;
    private Image m_imgBgBar;
    private Text fenqiTip;
    private Image fengqiImg;

    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("txtEquipName").GetComponent<Text>();
        m_txtMoney = tran.Find("txtMoney").GetComponent<Text>();
        m_txtMoneyOld = tran.Find("txtMoneyOld").GetComponent<Text>();
        m_txtTip = tran.Find("txtTip").GetComponent<Text>();
//        m_txtEquipTip = tran.Find("txtEquipTip").GetComponent<Text>();
        m_imgItem = tran.Find("imgEquip").GetComponent<Image>();
//        m_imgCheck = tran.Find("imgCheck").GetComponent<Image>();
        m_imgCoin = tran.Find("imgCoin").GetComponent<Image>();
        m_imgCoinOld = tran.Find("imgCoinOld").GetComponent<Image>();
        m_imgRedline = tran.Find("imgRedline").GetComponent<Image>();
        m_goBuyBtn = tran.Find("Button").gameObject;
//        m_goSendBtn = tran.Find("btnSend").gameObject;
        m_tips_img = tran.Find("tips_img").GetComponent<Image>();
//        m_levelTagObj = tran.Find("levelTag").gameObject;
//        m_levelTag = m_levelTagObj.GetComponent<Image>();
        m_limitBuyTip = tran.Find("limitBuyTip").GetComponent<Text>();
        m_roleTag = tran.Find("roleTag").GetComponent<Image>();
        m_imgBg = tran.Find("imgBg").GetComponent<Image>();
        m_imgBgBar = tran.Find("imgBgBar").GetComponent<Image>();
//        m_goTeQuan = tran.Find("btnTequan").gameObject;
        UGUIClickHandler.Get(m_goBuyBtn).onPointerClick += OnBuyBtnClick;
//        UGUIClickHandler.Get(m_goSendBtn).onPointerClick += onSendBtnClick;
//        UGUIClickHandler.Get(m_goTeQuan).onPointerClick += onTequanBtnClick;
//        m_goTeQuan.TrySetActive(false);
        fenqiTip = tran.Find("fenqitxt").GetComponent<Text>();
        fengqiImg = tran.Find("fengqiTime").GetComponent<Image>();
    }

    private void onSendBtnClick(GameObject target, PointerEventData eventData)
    {
        var level = ConfigMisc.GetInt("send_gift_req_level");
        if (PlayerSystem.roleData.level >= level)
            UIManager.PopPanel<PanelMallSend>(new object[] { m_data}, true,UIManager.GetPanel<PanelMall>());
        else
            UIManager.ShowTipPanel(string.Format("达到{0}级可以赠送道具", level));
    }

    private void onTequanBtnClick(GameObject target, PointerEventData eventData)
    {
        object[] obj = new object[2];
        obj[0] = ConfigManager.GetConfig<ConfigWeaponSacredProp>().GetLine(m_data.ItemId);
        obj[1] = m_data.Name;
        UIManager.PopPanel<PanelTequan>(obj, true);
    }

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
         int index = HeroCardDataManager.MallCardArray.ToList<int>().IndexOf(_data.info.ID);
         if (index != -1)
         {
             if (index + 1 < (int)HeroCardDataManager.getHeroMaxCardType())
             {
                 //不能购买更低等级英雄卡
                 TipsManager.Instance.showTips("不能购买比当前等级低的英雄卡");
                 return;
             }
         }
         if (m_data.ShopType == 8)
         {
             UIManager.PopPanel<PanelBuyInstallment>(new object[] { m_data}, true);
             return;
         }
        if (UIManager.IsOpen<PanelMall>())
        {
            UIManager.GetPanel<PanelMall>().m_selected = m_data;
            if (ItemDataManager.GetItem(m_data.ItemId).Type == "EQUIP")
            {
                UIManager.GetPanel<PanelMall>().m_nowItemHas = !(ItemDataManager.GetDepotItem(m_data.ItemId) == null);
            }
        }
        UIManager.PopPanel<PanelBuy>(new object[] { m_data ,"",_data!=null && _data.black_market}, true);
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDMallItem;
        m_data = _data != null ? _data.info : null;
        var item = ItemDataManager.GetItem((m_data.ItemId));
        if (item == null)
            return;
//        ConfigItemWeaponLine cfgWeapon = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(m_data.ItemId);
//        if (cfgWeapon != null && !string.IsNullOrEmpty(cfgWeapon.LevelTag))
//        {
//            m_levelTagObj.TrySetActive(true);
//            m_levelTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, cfgWeapon.LevelTag));
//            m_levelTag.SetNativeSize();
//            if (cfgWeapon.RareType == 5)
//            {
//                m_goTeQuan.TrySetActive(true);
//            }
//            else
//            {
//                m_goTeQuan.TrySetActive(false);
//            }
//        }
//        else
//        {
//            m_goTeQuan.TrySetActive(false);
//            m_levelTagObj.TrySetActive(false);
//            ConfigItemOtherLine cfgitemother = ConfigManager.GetConfig<ConfigItemOther>().GetLine(m_data.ItemId);
//            if (cfgitemother != null)
//            {
//                if (cfgitemother.RareType == 5)
//                {
//                    m_goTeQuan.TrySetActive(true);
//                }
//                else
//                {
//                    m_goTeQuan.TrySetActive(false);
//                }
//            }
//            else
//            {
//                m_goTeQuan.TrySetActive(false);
//            }
//
//        }

        m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(item.RareType)));
        m_imgBgBar.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, ColorUtil.GetRareColorName(item.RareType) + "_bg"));
        var nonSale = m_data.MoneyType == EnumMoneyType.NON_SALE;
        if (m_data != null && m_data.Type == GameConst.ITEM_TYPE_ROLE)
        {
            if (m_data.SubType == ""||m_data.SubType=="DECORATION")
                m_roleTag.enabled = false;
            else
            {
                m_roleTag.enabled = true;
                m_roleTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, m_data.SubType.ToLower()));
                m_roleTag.SetNativeSize();
            }
        }
        else
        {
            m_roleTag.enabled = false;
        }
        if (_data!=null && _data.locked==false && m_data!=null)
        {
            m_tips_img.enabled = false;
            m_imgCoin.enabled = !nonSale;
            m_imgCoin.SetSprite(ResourceManager.LoadMoneyIcon(m_data.MoneyType));
            m_imgCoin.SetNativeSize();

            if (!string.IsNullOrEmpty(item.Icon))
            {
                m_imgItem.SetSprite(ResourceManager.LoadIcon(item.Icon),item.SubType);
                m_imgItem.SetNativeSize();
                m_imgItem.enabled = true;
            }
            else
                m_imgItem.enabled = false;

            int price = m_data.Price.Length > 0 ? (int)m_data.Price[0].price : 0;
            int day = m_data.Price.Length > 0 ? (int)m_data.Price[0].time : 0;
            var isDiscount = ItemDataManager.IsDiscount(m_data);

            m_txtMoneyOld.enabled = isDiscount && !nonSale;
            m_imgRedline.enabled = isDiscount && !nonSale;
            m_imgCoinOld.enabled = isDiscount && !nonSale;
            if (isDiscount)
            {
                m_imgCoinOld.SetSprite(ResourceManager.LoadMoneyIcon(m_data.MoneyType));
                m_imgCoinOld.SetNativeSize();
            }

            if (isDiscount)
            {
                m_txtMoneyOld.text = price.ToString();
                m_imgRedline.rectTransform.sizeDelta = new Vector2(m_txtMoneyOld.preferredWidth + 8, 2);
            }
            price = ItemDataManager.GetPrice(m_data, price);
            m_txtMoney.enabled = !nonSale;
            int index = HeroCardDataManager.MallCardArray.ToList<int>().IndexOf(_data.info.ID);
            Util.SetGoGrayShader(m_goBuyBtn,false);
            m_goBuyBtn.transform.Find("Text").GetComponent<Text>().text = "购买";
            int cardLv=(int)HeroCardDataManager.getHeroMaxCardType();
            bool canntBuyLowerHeroCard = false;
            if (index != -1)
            {
                if (index + 1 < cardLv)
                {
                    //不能购买更低等级英雄卡
//                    Util.SetGoGrayShader(m_goBuyBtn, true);
                    canntBuyLowerHeroCard = true;
                }
                else
                {
                    //英雄卡
                    price = HeroCardDataManager.getUpCost((EnumHeroCardType)(index + 1), price, day);
                    if (index + 1 != cardLv && cardLv!=0)
                    {
                        m_goBuyBtn.transform.Find("Text").GetComponent<Text>().text = "升级";

                    }
                }
            }
            m_txtMoney.text = price.ToString();
           
            m_txtName.enabled = true;
            m_txtName.color = ColorUtil.GetRareColor(item.RareType);
            if (item.AddRule == 1||item.AddRule == 4)
                m_txtName.text = string.Format("{0}({1})", m_data.Name, day == 0 ? "永久" : day + "天");
            else
                m_txtName.text = string.Format("{0}({1})", m_data.Name, day == 0 ? "无限" : day + "个");

            if (ItemDataManager.ItemIsHeroCard(item))
            {
                m_txtName.text = string.Format("{0}({1})", m_data.Name, day == 0 ? "永久" : day + "天");
            }


            var pitem = ItemDataManager.GetDepotItem(m_data.ItemId);

            /*
            if (pitem != null)
            {
                var datetime = TimeUtil.GetRemainTime(pitem.time_limit);
                if (datetime.TotalDays > 0 || pitem.time_limit == 0)
                {
                    if (ItemDataManager.IsBagHas(pitem.uid))
                    {
//                        m_imgCheck.enabled = true;
                        m_txtEquipTip.enabled = true;
                        m_txtEquipTip.text = "已装备";
                    }
                    else
                    {
//                        m_imgCheck.enabled = true;
                        m_txtEquipTip.enabled = true;
                        m_txtEquipTip.text = "已拥有";
                    }
                }
                else
                {
//                    m_imgCheck.enabled = false;
                    m_txtEquipTip.enabled = false;
                }
            }
            else
            {
//                m_imgCheck.enabled = false;
                m_txtEquipTip.enabled = false;
            }
             * */

            //时间叠加
            var forever = pitem != null && pitem.time_limit == 0 && (item.AddRule == 1);
            var vipUnlock = PlayerSystem.roleData.vip_level >= m_data.UnlockVipLevel;
            var unlock = PlayerSystem.roleData.level >= m_data.UnlockLevel;

            if (m_data.MoneyType == EnumMoneyType.NON_SALE)
            {
                m_goBuyBtn.TrySetActive(false);
                m_txtTip.gameObject.TrySetActive(false);
            }
            else if (forever)
            {
                m_txtTip.text = "永久拥有";
                m_goBuyBtn.TrySetActive(false);
                m_txtTip.gameObject.TrySetActive(true);
            }
            else if (m_data.UnlockVipLevel > 0)
            {
                if (!vipUnlock)
                {
                    if (m_data.UnlockVipLevel == 2)
                        m_txtTip.text = "尊贵会员可买";
                    else if (m_data.UnlockVipLevel == 1)
                        m_txtTip.text = "会员可买";
                }
                m_goBuyBtn.TrySetActive(vipUnlock);
                m_txtTip.gameObject.TrySetActive(!vipUnlock);
            }
            else if (canntBuyLowerHeroCard)
            {
                m_goBuyBtn.TrySetActive(true);
                Util.SetGoGrayShader(m_goBuyBtn);
                m_txtTip.gameObject.TrySetActive(false);
            }
            else
            {
                if (!unlock)
                    m_txtTip.text = m_data.UnlockLevel + "级后解锁";
                m_goBuyBtn.TrySetActive(unlock);
                m_txtTip.gameObject.TrySetActive(!unlock);
            }
//            m_goSendBtn.TrySetActive((m_data.UnlockVipLevel == 0 && _data.black_market==false)&&(m_data.CanSend!=0));

            //限购增加
            if (m_data.LimitAmount > 0)
            {
                var limitTimeStr = "";
                if (0 == m_data.LimitTime)
                    limitTimeStr = "";
                else if (1 == m_data.LimitTime)
                    limitTimeStr = "今日";
                else if (7 == m_data.LimitTime)
                    limitTimeStr = "本周";
                else //这样处理，是因为策划月都当30算，但程序觉得以后会改需求，比如28、29、31的月份，这样以后也可以兼容
                    limitTimeStr = "本月";
                m_limitBuyTip.text = string.Format("{0}已购次数 <color='#00FF00'>{1} / {2}</color>", limitTimeStr, _data.alreadBuyNo, m_data.LimitAmount);
                m_limitBuyTip.gameObject.TrySetActive(true);
            }
            else
            {
                m_limitBuyTip.gameObject.TrySetActive(false);
            }
        }
        else
        {
            m_txtName.enabled = false;
            m_imgRedline.enabled = false;
            m_imgCoinOld.enabled = false;
            m_txtMoneyOld.enabled = false;
            m_imgCoin.enabled = false;
            m_txtMoney.enabled = false;
//            m_txtEquipTip.enabled = false;
//            m_imgCheck.enabled = false;
            m_txtTip.gameObject.TrySetActive(false);
            m_limitBuyTip.gameObject.TrySetActive(false);
            m_goBuyBtn.TrySetActive(false);
//            m_goSendBtn.TrySetActive(false);
            m_imgItem.enabled = true;
            m_imgItem.SetSprite(_data.locked ? ResourceManager.LoadSprite(AtlasName.Strengthen, "train_item_locked") : null);
            m_imgItem.SetNativeSize();
            var tips_str = "";
            if (_data != null && _data.locked)
            {
                if (_data.index == 7)
                {
                    tips_str = "open_tips_0";
                }
                else if (_data.index == 8)
                {
                    tips_str = "open_tips_2";
                }
            }
            if (!string.IsNullOrEmpty(tips_str))
            {
                m_tips_img.enabled = true;
                m_tips_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, tips_str));
                m_tips_img.SetNativeSize();
//                m_levelTagObj.TrySetActive(false);
            }
            else
            {
                m_tips_img.enabled = false;
            }
        }

        if (m_data.ShopType == 8)
        {
            m_txtName.text = string.Format("{0}({1})", m_data.Name, "分期付");
            CheckHeroType();
            CheckBuyFengqi();

        }
        else
        {
            fengqiImg.gameObject.TrySetActive(false);
            fenqiTip.gameObject.TrySetActive(false);
        }
    }

    void CheckHeroType()
    {
        var config = ConfigManager.GetConfig<ConfigPhasedPurchase>().GetLine(m_data.ID);
        var heroType = HeroCardDataManager.getHeroMaxCardType();
        if ((int)heroType >= config.HeroCardType)
        {
            m_goBuyBtn.TrySetActive(true);
            fenqiTip.gameObject.TrySetActive(false);
        }
        else
        {
            m_goBuyBtn.TrySetActive(false);
            fenqiTip.gameObject.TrySetActive(true);
            if (config.HeroCardType == 1)
            {
                fenqiTip.text = "<color=orange>英雄卡玩家</color>可购买";
            }
            else if (config.HeroCardType == 2)
            {
                fenqiTip.text = "<color=orange>超级英雄卡玩家</color>可购买";
            }
            else if (config.HeroCardType == 3)
            {
                fenqiTip.text = "<color=orange>传奇英雄卡玩家</color>可购买";
            }
        }
    }

    void CheckBuyFengqi()
    {
        var itemp = ItemDataManager.GetPhasedInfoByMallId(m_data.ID);
        if (itemp != null)
        {
            fengqiImg.gameObject.TrySetActive(true);
            fengqiImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "fengqi" + itemp.phased_number.ToString()));
        }
        else
        {
            fengqiImg.gameObject.TrySetActive(false);
        }
        
    }
}

public class CDMallItem
{
    public int index;
    public ConfigMallLine info;
    public bool black_market = false;
    public bool locked = false;
    public int alreadBuyNo;

    public CDMallItem(int code=0)
    {
        if (code>0)
        {
            info = ConfigManager.GetConfig<ConfigMall>().GetLine(code);
        }
    }
}
