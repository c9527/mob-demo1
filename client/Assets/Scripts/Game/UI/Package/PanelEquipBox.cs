﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelEquipBox : BasePanel
{
    private ItemInfo _data;
    private string[] _random_names;

    private float _last_time;

    public Image item_img;
    public Text name_txt;
    public Text desc_txt;

    UIEffect _effect_player;    
    UIEffect m_effect_di;
    bool m_isUseGiftBag = false;
    public PanelEquipBox()
    {
        SetPanelPrefabPath("UI/Package/PanelEquipBox");

        AddPreLoadRes("UI/Package/PanelEquipBox", EResType.UI);
    }

    public override void Init()
    {
        m_tran.Find("HitArea").gameObject.AddComponent<SortingOrderRenderer>();
        item_img = m_tran.Find("item_img").GetComponent<Image>();
        name_txt = m_tran.Find("name_txt").GetComponent<Text>();
        desc_txt = m_tran.Find("desc_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("HitArea")).onPointerClick += delegate { if (_last_time == 0) { HidePanel(); } };
    }

    public override void OnShow()
    {
        
        var _code = 0;
        if (HasParams())
        {
            _code = (int)m_params[0];
            var ary = m_params[1] as ItemInfo[];
            _data = ary != null && ary.Length > 0 ? ary[0] : null;
        }
        var source_data = new List<string>();
        var use_info = _code > 0 ? ItemDataManager.GetItem(_code) as ConfigItemOtherLine : null;
        m_isUseGiftBag = use_info.UseType == "use_giftbag";
        if (use_info!=null)
        {
            if(!m_isUseGiftBag)
            {
                var random_data = !string.IsNullOrEmpty(use_info.UseParam) ? use_info.UseParam.Split(';') : null;
                for (int i = 0; random_data != null && i < random_data.Length; i++)
                {
                    var info = ItemDataManager.GetItem(int.Parse(random_data[i].Split('#')[0]));
                    source_data.Insert(UnityEngine.Random.Range(0, source_data.Count + 1), info.DispName);
                }
            }
            else
            {
                for (int i = 0; i < use_info.VirtualItemList.Length; i++)
                {
                    source_data.Add(ItemDataManager.GetItem(use_info.VirtualItemList[i]).DispName);
                }
            }
        }
        _random_names = source_data.ToArray();
        item_img.enabled = false;
        if (item_img.transform.Find("_mergeIcon") != null)
            item_img.transform.Find("_mergeIcon").gameObject.TrySetActive(false);
        desc_txt.text = "";
        if (_effect_player != null)
        {
            _effect_player.Destroy();
            _effect_player = null;
        }
        //UIEffect.ShowEffect(m_tran, EffectConst.UI_EQUIP_BOX_EXPLODE_EFFECT,5f, new Vector2(20, 100));        
        if(m_isUseGiftBag&&use_info.EffectType.Length==2)
        {
            //m_effect_di = UIEffect.ShowEffectAfter(item_img.transform, use_info.EffectType[1], 5f, new Vector2(0, 100));
            if(m_effect_di==null)
            {
                m_effect_di = UIEffect.ShowEffectAfter(item_img.transform, use_info.EffectType[1], new Vector2(0, 60));
            }
            else
            {
                m_effect_di.Play();
            }
            //m_effect_di = UIEffect.ShowEffect(m_tran, use_info.EffectType[1], 5f, new Vector2(0, 100));
            UIEffect.ShowEffect(m_tran, use_info.EffectType[0], 5f, new Vector2(0, 100));        
        }  
        else
        {
            if (m_effect_di != null)
                m_effect_di.Hide();
            UIEffect.ShowEffect(m_tran, EffectConst.UI_EQUIP_BOX_EXPLODE_EFFECT,5f, new Vector2(20, 100));        
        }
        AudioManager.PlayUISound(AudioConst.equipBoxOpen);
        _last_time = Time.realtimeSinceStartup;
    }

    public override void OnHide()
    {
        _last_time = 0;
        if (_effect_player != null)
        {
            _effect_player.Destroy();
            _effect_player = null;
        }
        if(m_effect_di!=null)
        {
            m_effect_di.Destroy();
            m_effect_di = null;
        }
        PanelRewardItemTipsWithEffect.AutoUseGiftBag();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_last_time == 0 || Time.realtimeSinceStartup > (_last_time + 4.5f))
        {
            if (_last_time != 0)
            {
                _last_time = 0;
                if (!m_isUseGiftBag)
                {
                    if (_effect_player == null)
                    {
                        _effect_player = UIEffect.ShowEffectAfter(item_img.transform, EffectConst.UI_EQUIP_BOX_EFFECT, new Vector2(20, 100));
                    }
                    else
                    {
                        _effect_player.Play();
                    }
                }
                if (_data!=null && _data.ID>0)
                {
                    var info = ItemDataManager.GetItem(_data.ID);
                    var day = _data.day;
                    var num = _data.cnt;
                    ConfigItemLine old_info = null;
                    if (info.Type == GameConst.ITEM_TYPE_MATERIAL)//将碎片还原武器
                    {
                        old_info = info;
                        ItemInfo from = ItemDataManager.GetFromItem(_data.ID);
                        //info = ItemDataManager.GetWeaponItemByMaterial(info.ID);
                        info = ItemDataManager.GetItem(from.ID);
                        var sdata = ItemDataManager.GetDepotItem(info.ID);
                        day = sdata != null && sdata.time_limit == 0 ? StrengthenModel.Instance.ConvertMaterialNumToDay(num) : 0;
                    }
                    item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
                    item_img.SetNativeSize();
                    item_img.enabled = true;
                    var name_str = info.DispName;
                    if (info.AddRule == 1 || info.AddRule == 4)
                    {
                        name_str += day == 0 ? "（永久）" : "（" + day + "天）";
                    }
                    name_txt.text = name_str;
                    if (old_info!=null && old_info.ID!=info.ID && info.AddRule != 4)
                    {
//                        desc_txt.text = string.Format("已拥有此枪械，该奖励自动兑换为[{0}x{1}]", old_info.DispName, num);
                        desc_txt.text = ""; 
                    }
                    else
                    {
                        desc_txt.text = ""; 
                    }
                }
                else
                {
                    name_txt.text = "???";
                    desc_txt.text = "";
                }
            }
            return;
        }
        if (_random_names != null && _random_names.Length>0)
        {
            name_txt.text = _random_names[(int)(1f / 0.15f * (Time.realtimeSinceStartup - _last_time) % _random_names.Length)];
        }
    }
}
