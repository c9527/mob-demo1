﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelFix : BasePanel
{
    private Text m_txtName;
    private Text m_txtCost;
    private Image m_imgEquip;
    private int m_uid;

    public PanelFix()
    {
        SetPanelPrefabPath("UI/Package/PanelFix");

        AddPreLoadRes("UI/Package/PanelFix", EResType.UI);
    }

    public override void Init()
    {
        m_txtName = m_tran.Find("background/txtName").GetComponent<Text>();
        m_txtCost = m_tran.Find("background/txtCost").GetComponent<Text>();
        m_imgEquip = m_tran.Find("background/imgEquip").GetComponent<Image>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnFix")).onPointerClick += OnFixBtnClick;
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;
        var pitem = ItemDataManager.GetDepotItemByUid((int)m_params[0]);
        var item = ItemDataManager.GetItemWeapon(pitem.item_id);
        m_uid = pitem.uid;
        m_imgEquip.SetSprite(ResourceManager.LoadIcon(item.Icon),item.SubType);
        m_imgEquip.SetNativeSize();

        m_txtName.text = string.Format("{0} ({1})", item.DispName, TimeUtil.GetRemainTimeString(pitem.time_limit));

        m_txtCost.text = ((ItemDataManager.MAX_DURABILITY - pitem.durability) * item.RepairPrice).ToString();
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void OnFixBtnClick(GameObject target, PointerEventData eventData)
    {
        HidePanel();

        NetLayer.Send(new tos_player_repair_weapons() { weapon_uids = new[] { m_uid } });
    }
}
