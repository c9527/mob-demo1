﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 通用的Item UI render
/// 支持控制Item Frame，Item Image，Item Text,Item Selected（独立于Toggle）
/// </summary>
class UIPartItemRender : ItemRender
{
    CDItemData _data;

    string _frame_atlas_name = AtlasName.Strengthen;
    string _frame_sprite_prefix = "item_frame_";
    int _size_mode;//0：native size，1: fixed size，2：auto fit

    //必选组件
    Image _status_img;
    Image _item_img;

    //可选组件
    Text _name_txt;
    Text _num_txt;
    Image _selected_img;

    public override void Awake()
    {
        _status_img = GetComponent<Image>();
        _item_img = transform.Find("Image").GetComponent<Image>();
        //可选组件,使用前需要判断null
        var trans = transform.Find("Text");
        if (trans != null)
        {
            _name_txt = trans.GetComponent<Text>();
        }
        trans = transform.Find("Num");
        if (trans != null)
        {
            _num_txt = trans.GetComponent<Text>();
        }
        trans = transform.Find("Selected");
        if (trans != null)
        {
            _selected_img = trans.GetComponent<Image>();
        }
    }

    public void SetStyle(string atlas,string prefix,int size_mode=0)
    {
        if (!string.IsNullOrEmpty(atlas) && !string.IsNullOrEmpty(prefix))
        {
            _frame_atlas_name = atlas;
            _frame_sprite_prefix = prefix;
        }
        _size_mode = size_mode;
    }

    public CDItemData data
    {
        get { return _data; }
    }

    public bool selected
    {
        get 
        {
            return _data != null && _data.selected == true;
        }
        set
        {
            if (_data != null)
            {
                _data.selected = value;
                if (_selected_img != null && _selected_img.enabled != _data.selected)
                {
                    _selected_img.enabled = _data.selected;
                }
            }
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDItemData;
        if (_data == null || _data.sdata.item_id <= 0)
            _status_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, _data == null || _data.sdata.item_id == -1 ? "lock_bg" : "question_bg"));
        if (_data != null && _data.info!=null)
        {
            var quality = _data.GetQuality();
            _status_img.SetSprite(quality > 0 ? ResourceManager.LoadSprite(_frame_atlas_name, _frame_sprite_prefix + quality) : null);
            _item_img.enabled = true;
            _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, _data.info.Icon));
            if (_data!= null && _data.info is ConfigItemWeaponLine)
            {
                _item_img.transform.localEulerAngles = new Vector3(0, 0, -15);
            }
            else
            {
                _item_img.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            if (_size_mode == 0)
            {
                _item_img.SetNativeSize();
            }
            else if (_size_mode==2)
            {
                if (_item_img.sprite != null)
                {
                    var scale = Mathf.Min(_item_img.rectTransform.sizeDelta.y / _item_img.sprite.rect.height, 1);
                    _item_img.rectTransform.sizeDelta = new Vector2(_item_img.sprite.rect.width * scale, _item_img.sprite.rect.height * scale);
                }
            }
        }
        else
        {
            _item_img.enabled = false;
            _item_img.SetSprite(null);
        }
        if (_name_txt != null)
        {
            if (_data != null && !string.IsNullOrEmpty(_data.overrideName))
                _name_txt.text = _data.overrideName;
            else if (_data != null && _data.info != null)
                _name_txt.text = _data.info.DispName;
            else
                _name_txt.text = "";
        }
        if (_num_txt != null)
        {
            _num_txt.text = _data != null ? ("X" + _data.sdata.item_cnt) : "";
        }
        if (_selected_img != null)
        {
            _selected_img.enabled = _data!=null && _data.selected;
        }
    }
}
