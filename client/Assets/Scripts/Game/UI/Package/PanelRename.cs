﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelRename : BasePanel
{
    InputField _name_txt;

    public PanelRename()
    {
        SetPanelPrefabPath("UI/Package/PanelRename");

        AddPreLoadRes("UI/Package/PanelRename", EResType.UI);
    }

    public override void Init()
    {
        _name_txt = m_tran.Find("name_txt").GetComponent<InputField>();
        _name_txt.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close_btn")).onPointerClick += (target, event_data) => HidePanel();
        UGUIClickHandler.Get(m_tran.Find("confirm_btn")).onPointerClick += OnConfirmBtnClick;
    }

    private void OnConfirmBtnClick(GameObject target, PointerEventData eventData)
    {
        var name = _name_txt.text!=null ? _name_txt.text.Trim() : null;
        if (string.IsNullOrEmpty(name))
        {
            TipsManager.Instance.showTips("名字不可以为空");
        }
        else
        {
            PlayerSystem.TosRenameRoleName(name);
            HidePanel();
        }
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

