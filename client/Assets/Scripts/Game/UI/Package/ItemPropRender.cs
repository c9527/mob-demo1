﻿using UnityEngine;
using UnityEngine.UI;

public class ItemPropRender : ItemRender
{
    private PropRenderInfo m_data;
    private Text m_txtName;
    private Image m_imgPgbar;
    private Image m_imgValue;
    private Image m_imgValueAdd;
    private Text m_txtValue;
    private float m_valuePosX;         
//    private GameObject m_specialAttr;
//    private Text m_specialAttrName;
//    private DataGrid m_starList;
//    private GameObject m_starItem0;
    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("txtName").GetComponent<Text>();
        m_imgPgbar = tran.Find("pgbar").GetComponent<Image>();
        m_imgValue = tran.Find("pgbar/value").GetComponent<Image>();
        m_imgValueAdd = tran.Find("pgbar/valueAdd").GetComponent<Image>();
        m_txtValue = tran.Find("txtValue").GetComponent<Text>();
        m_valuePosX = m_txtValue.rectTransform.anchoredPosition.x;
//        m_specialAttr = tran.Find("specialAttr").gameObject;
//        m_specialAttrName = tran.Find("specialAttr/name").GetComponent<Text>();
//        m_starList = tran.Find("specialAttr/starList").gameObject.AddComponent<DataGrid>();        
//        m_starItem0 = m_starList.transform.Find("starItem0").gameObject;
//        m_starItem0.TrySetActive(false);
//        m_starList.SetItemRender(m_starItem0, typeof(ItemStarRender));
    }

    protected override void OnSetData(object data)
    {
        m_data = data as PropRenderInfo;
        if (m_data==null || m_data.value==0)
        {
            gameObject.TrySetActive(false);
            return;
        }
//        if(m_data.name=="特殊属性")
//        {
//            if(UIManager.IsOpen<PanelMall>())
//            {
//                m_specialAttr.transform.localPosition = new Vector3(0, 0, 0);
//            }
//            else
//            {
//                m_specialAttr.transform.localPosition = new Vector3(33, 0, 0);
//            }
//            m_imgPgbar.gameObject.TrySetActive(false);
//            m_txtName.gameObject.TrySetActive(false);
//            m_txtValue.gameObject.TrySetActive(false);
//            m_specialAttr.TrySetActive(true);
//            ConfigWeaponSpecialAttrLine config = ConfigManager.GetConfig<ConfigWeaponSpecialAttr>().GetLine(int.Parse(m_data.prop_type));
//            m_specialAttrName.text = config.Name;
//            string[] strs = new string[m_data.value];
//            for (int i = 0; i < strs.Length;i++ )
//            {
//                strs[i] = config.Icon;
//            }
//            m_starList.Data = strs;
//            return;
//        }
//        else
//        {
//            m_specialAttr.TrySetActive(false);
//        }
        m_imgPgbar.gameObject.TrySetActive(!m_data.hidePgBar);
        var pos = m_txtValue.rectTransform.anchoredPosition;
//        m_txtValue.rectTransform.anchorMin = !m_data.hidePgBar ? new Vector2(1, 0.5f) : new Vector2(0, 0.5f); 
//        m_txtValue.rectTransform.anchorMax = !m_data.hidePgBar ? new Vector2(1, 0.5f) : new Vector2(0, 0.5f); 
        m_txtValue.rectTransform.anchoredPosition = !m_data.hidePgBar ? new Vector2(m_valuePosX, pos.y) : new Vector2(112, pos.y);

        var max = m_imgPgbar.rectTransform.sizeDelta.x - 4;
        gameObject.TrySetActive(true);
        m_txtName.text = m_data.name;
        var full_value = m_data.base_value > 0 ? (float)m_data.base_value : 100f;
        m_imgValue.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(m_data.value / full_value, 0, 1) * max, m_imgValue.rectTransform.sizeDelta.y);
        m_imgValueAdd.rectTransform.sizeDelta = new Vector2(Mathf.Clamp((m_data.value + m_data.valueAdd) / full_value, 0, 1) * max, m_imgValueAdd.rectTransform.sizeDelta.y);
        var base_value = m_data.base_value > 0 ? m_data.base_value / 100f : 1f;
        var value_add = (m_data.valueAdd + m_data.partAdd + m_data.suitAdd) / base_value;
        var value_str = "";
        if (m_data.value > 0)
        {
            value_str += (m_data.value / base_value).ToString("0.##") + (base_value > 1 ? "%" : "");
        }
        if (value_add != 0)
        {
            value_str += "<color='#00FF00'>+" + (int)value_add + (base_value > 1 ? "%" : "") + "</color>";
        }
        m_txtValue.text = value_str;
    }
}

public class PropRenderInfo
{
    public string name;
    public string prop_type;
    public int value;
    public float valueAdd;
    public float partAdd;
    public float suitAdd;
    public bool hidePgBar;
    public int base_value;//比例基数，100表示百分比，1000表示千分比，如此类推
}

public class ItemStarRender:ItemRender
{
    Image m_image;
    public override void Awake()
    {
        m_image = transform.GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_image.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, (string)data));
        m_image.SetNativeSize();
    }
}