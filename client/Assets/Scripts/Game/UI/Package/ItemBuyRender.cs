﻿using UnityEngine;
using UnityEngine.UI;

class ItemBuyRender : ItemRender
{
    private Text m_text;
    private Transform m_btn;
    private Toggle m_toggle;

    public override void Awake()
    {
        m_toggle = transform.GetComponent<Toggle>();
        m_text = transform.Find("Text").GetComponent<Text>();
        m_btn = transform.Find("Background");
    }

    public bool enabled
    {
        get { return m_toggle.enabled; }
    }

    protected override void OnSetData(object data)
    {
        m_renderData = data;
        m_toggle.enabled = true;
        m_btn.gameObject.TrySetActive(true);
        m_text.color = Color.white;
        var info = data as ItemBuyInfo;
        if (info.addRule == 1 || info.addRule == 4)
        {
            if (info.number == 0)
            {
                m_text.text = "永久";

                if (info.needUnlock)
                {
                    var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(info.item_id);
                    if ((sdata.state >= 3 && sdata.level >= 9))
                        m_text.color = Color.white;
                    else
                        m_text.color = Color.gray;
                }

//                var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(info.item_id);
//                var item = ItemDataManager.GetItem(sdata.weapon_id);
//                if ((sdata.state>=3 && sdata.level>=9)||item.Type=="ROLE")
//                {
//                    m_text.text = "永久";
//                }
//                else
//                {
//                    m_text.text = "<color='#00EAFF'>(熟练度满9级解锁永久购买)</color>";
//                    m_toggle.enabled = false;
//                    m_btn.gameObject.TrySetActive(false);
//                }
            }
            else
            {
                ItemTime time = ItemDataManager.FloatToDayHourMinute(info.number);
                //m_text.text = info.number + "天";
                m_text.text = time.ToString(false);
            }
        }
        else
        {
            m_text.text = info.number != 0 ? info.number + "个" : "无限";
            if (ItemDataManager.ItemIsHeroCard(info.item_id))
            {
                m_text.text = info.number + "天";
            }
        }
    }
}

public class ItemBuyInfo
{
    public int index;
    public int item_id;
    public int price;
    public EnumMoneyType priceType;
    public bool needUnlock;
    public float number;
    public int addRule;
}

class ItemBuyBuffRender : ItemRender
{
    private Text m_text;
    private GameObject goHadBuy;

    public override void Awake()
    {
        m_text = transform.Find("Background/Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_renderData = data;
        var info = data as ItemBuyInfo;
        //        gameObject.TrySetActive(info.price > 0);

        m_text.text = "+" + info.number + "%";

        if (info.addRule == 1 || info.addRule == 4)
        {
            if (goHadBuy == null)
            {
                goHadBuy = new GameObject("hadbuypic");
                var image = goHadBuy.AddComponent<Image>();
                image.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "hadbuy"));
                image.SetNativeSize();
                goHadBuy.transform.SetParent(transform);
                goHadBuy.transform.localScale = Vector3.one;
                goHadBuy.transform.localPosition = new Vector3(-60, 8, 0);
            }
            goHadBuy.TrySetActive(true);
        }
        else
        {
            if (goHadBuy != null) goHadBuy.TrySetActive(false);
        }
    }
}