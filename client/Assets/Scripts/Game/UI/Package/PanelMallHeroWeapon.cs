﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public partial class PanelMall
{
    private DataGrid m_heroWeaponTabbar;
    private DataGrid m_heroWeaponPropDataGrid;
    private Button m_heroWeaponBtnBuy;
    private Button m_heroWeaponBtnDetail;
    private Button m_heroWeaponBtnPrev;
    private Button m_heroWeaponBtnNext;
    private Image m_heroWeaponImgEquip;
    private Image m_heroWeaponImgIcon;
    private Image m_heroWeaponImgForever;
    private Text m_heroWeaponTxtMoney;
    private RawImage m_heroWeaponModel;
    private GameObject m_heroWeaponDrag;
    private int m_heroWeaponTabbarPage;

    private void InitHeroWeapon()
    {
        m_tranRightHeroWeapon = m_tran.Find("rightHeroWeapon").GetComponent<RectTransform>();
        m_heroWeaponTabbar = m_tran.Find("rightHeroWeapon/tabbar/content").gameObject.AddComponent<DataGrid>();
        m_heroWeaponTabbar.SetItemRender(m_heroWeaponTabbar.transform.Find("item").gameObject, typeof(ItemHeroWeaponTabbarRender));
        m_heroWeaponPropDataGrid = m_tran.Find("rightHeroWeapon/propList/content").gameObject.AddComponent<DataGrid>();
        m_heroWeaponPropDataGrid.SetItemRender(m_heroWeaponPropDataGrid.transform.Find("item").gameObject, typeof(ItemHeroWeaponPropRender));
        m_heroWeaponPropDataGrid.useClickEvent = false;
        m_heroWeaponPropDataGrid.autoSelectFirst = false;
        m_heroWeaponBtnBuy = m_tran.Find("rightHeroWeapon/btnBuy").GetComponent<Button>();
        m_heroWeaponBtnDetail = m_tran.Find("rightHeroWeapon/btnDetail").GetComponent<Button>();
        m_heroWeaponBtnPrev = m_tran.Find("rightHeroWeapon/btnPrev").GetComponent<Button>();
        m_heroWeaponBtnNext = m_tran.Find("rightHeroWeapon/btnNext").GetComponent<Button>();
        m_heroWeaponImgEquip = m_tran.Find("rightHeroWeapon/imgEquip").GetComponent<Image>();
        m_heroWeaponImgIcon = m_tran.Find("rightHeroWeapon/imgIcon").GetComponent<Image>();
        m_heroWeaponImgForever = m_tran.Find("rightHeroWeapon/imgForever").GetComponent<Image>();
        m_heroWeaponTxtMoney = m_tran.Find("rightHeroWeapon/txtMoney").GetComponent<Text>();

        m_heroWeaponModel = m_tran.Find("rightHeroWeapon/weaponModel").GetComponent<RawImage>();
        m_heroWeaponModel.enabled = true;
        m_heroWeaponModel.texture = WeaponDisplay.RenderTexture;
        m_heroWeaponModel.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;
        m_heroWeaponDrag = m_tran.Find("rightHeroWeapon/weaponDrag").gameObject;
    }

    private void InitEventHeroWeapon()
    {
        UGUIClickHandler.Get(m_heroWeaponBtnBuy.gameObject).onPointerClick += OnBuyBtnClick;
        UGUIClickHandler.Get(m_heroWeaponBtnDetail.gameObject).onPointerClick += OnHeroWeaponDetailBtnClick;
        UGUIClickHandler.Get(m_heroWeaponBtnPrev.gameObject).onPointerClick += OnHeroWeaponBtnTurnPageClick;
        UGUIClickHandler.Get(m_heroWeaponBtnNext.gameObject).onPointerClick += OnHeroWeaponBtnTurnPageClick;
        m_heroWeaponTabbar.onItemSelected += OnHeroWeaponTabbarSelected;

        UGUIDragHandler.Get(m_heroWeaponDrag).onDrag += OnDrag;
    }

    private void OnHeroWeaponBtnTurnPageClick(GameObject target, PointerEventData eventData)
    {
        var offset = target == m_heroWeaponBtnPrev.gameObject ? -1 : 1;
        m_heroWeaponTabbarPage += offset;
        m_heroWeaponTabbarPage = Mathf.Clamp(m_heroWeaponTabbarPage, 0, Mathf.CeilToInt(m_heroWeaponTabbar.Data.Length / 5f)-1);
        m_heroWeaponTabbar.ResetScrollPosition(m_heroWeaponTabbarPage*5, true);
    }

    private void OnHeroWeaponDetailBtnClick(GameObject target, PointerEventData eventdata)
    {
        OnItemSelected(m_selectItem);
    }

    private void UpdateModel(object renderData)
    {
        var vo = renderData as CDMallItem;
        if (vo == null || vo.info == null)
            return;
        var titem = ItemDataManager.GetItem(vo.info.ItemId);
        if (titem is ConfigItemWeaponLine && (titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2))
        {
            var weapon = titem as ConfigItemWeaponLine;
            ModelDisplay.ConfigRole = null;
            ModelDisplay.ConfigWeapon = weapon;
            ModelDisplay.UpdateModel();
        }
    }

    private void OnHeroWeaponTabbarSelected(object renderData)
    {
        var vo = renderData as CDMallItem;
        m_selectItem = vo;
        if (vo == null || vo.info == null)
        {
            m_heroWeaponImgEquip.enabled = false;
            m_heroWeaponTxtMoney.text = "";
            m_heroWeaponPropDataGrid.Data = new object[0];
            return;
        }

        UpdateModel();

        var price = vo.info.Price.Length > 0 ? vo.info.Price[vo.info.Price.Length-1].price : 0;
        var nonSale = vo.info.MoneyType == EnumMoneyType.NON_SALE;
        var titem = ItemDataManager.GetItem(vo.info.ItemId);
        if (titem is ConfigItemWeaponLine && 
            (titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 ||
             titem.SubType == GameConst.WEAPON_SUBTYPE_DAGGER || titem.SubType == GameConst.WEAPON_SUBTYPE_GRENADE ||
             titem.SubType == GameConst.WEAPON_SUBTYPE_FLASHBOMB))
        {
            var weapon = titem as ConfigItemWeaponLine;
            m_heroWeaponImgEquip.enabled = false;
            m_heroWeaponModel.enabled = true;
            WeaponDisplay.UpdateMode(weapon);
        }
        else
        {
            m_heroWeaponImgEquip.enabled = true;
            m_heroWeaponModel.enabled = false;
        }

        if (!string.IsNullOrEmpty(titem.Icon))
        {
//            m_heroWeaponImgEquip.enabled = true;
            m_heroWeaponImgEquip.SetSprite(ResourceManager.LoadIcon(titem.Icon), titem.SubType);
            m_heroWeaponImgEquip.SetNativeSize();
        }
        else
        {
            m_heroWeaponImgEquip.enabled = false;
        }

        var pitem = ItemDataManager.GetDepotItem(vo.info.ItemId);
        var forever = pitem != null && pitem.time_limit == 0 && (titem.AddRule == 1);
        var vipUnlock = PlayerSystem.roleData.vip_level >= vo.info.UnlockVipLevel;
        var unlock = PlayerSystem.roleData.level >= vo.info.UnlockLevel;
        m_heroWeaponBtnBuy.gameObject.TrySetActive(true);
        m_heroWeaponBtnBuy.transform.Find("Text").GetComponent<Text>().text = "购买";
        if (nonSale)
        {
            m_heroWeaponBtnBuy.gameObject.TrySetActive(false);
            m_heroWeaponBtnBuy.transform.Find("Text").GetComponent<Text>().text = "获取途径";
        }
        else if (forever)
            m_heroWeaponBtnBuy.gameObject.TrySetActive(false);
        else if (vo.info.UnlockVipLevel > 0)
            m_heroWeaponBtnBuy.gameObject.TrySetActive(vipUnlock);
        else
            m_heroWeaponBtnBuy.gameObject.TrySetActive(unlock);

//        m_heroWeaponBtnBuy.gameObject.TrySetActive(!nonSale);
        m_heroWeaponTxtMoney.gameObject.TrySetActive(!nonSale);
        m_heroWeaponImgIcon.gameObject.TrySetActive(!nonSale);
        m_heroWeaponImgForever.gameObject.TrySetActive(!nonSale);

        m_heroWeaponImgIcon.SetSprite(ResourceManager.LoadMoneyIcon(vo.info.MoneyType));
        m_heroWeaponImgIcon.SetNativeSize();

        m_heroWeaponTxtMoney.text = ItemDataManager.GetPrice(vo.info, price).ToString();

        var prop = ConfigManager.GetConfig<ConfigWeaponSacredProp>().GetLine(vo.info.ItemId);
        m_heroWeaponPropDataGrid.Data = prop != null ? prop.PropDesc : new object[0];
        m_heroWeaponPropDataGrid.ResetScrollPosition();
    }
}