﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：刀锋据点模式UI
//创建者：hwl
//创建日期: 2016/11/03
///////////////////////////////////////////
public class PanelDaggerOW : BasePanel
{

    private DaggerPointBehavior m_behavior;
    private Vector3 m_effectPos = new Vector3(0,-44,0);
    private UIEffect[] m_fightEffectList = new UIEffect[5];

    private GameObject[] m_grayProgressList = new GameObject[5];
    private GameObject[] m_blueProgressList = new GameObject[5];
    private GameObject[] m_redProgressList = new GameObject[5];

    private Image[] m_blueProgressImgList = new Image[5];
    private Image[] m_redProgressImgList = new Image[5];

    private GameObject m_redBarGo;
    private GameObject m_blueBarGo;
    private Image m_redBarImg;
    private Image m_blueBarImg;
    private int m_pointId = -1;

    private int m_totlaScore;
    private p_occupy_info m_curInfo;
    private GameObject m_progressBar;
    private Text m_numText1;
    private Text m_numText2;
    ConfigMapListLine m_mapList = null;
    private const float m_OT = 0.99f;//加时条件
    private bool m_showOTProgress = false;
    private GameObject m_otBarGo;
    private Image m_otImg;
    private float m_cdTime = 0;
    private int m_otBarWidth = 108;
    private UIEffect m_otEffect;
    private int m_lastOwerId = 0;
    private int m_nowOwerId = 0;
    private int m_otTarget = 0;

    public PanelDaggerOW()
    {
        SetPanelPrefabPath("UI/DaggerOW/PanelDaggerOW");

        AddPreLoadRes("UI/DaggerOW/PanelDaggerOW", EResType.UI);

    }

    public override void Init()
    {
        GameObject go = null;
        for (int i = 0; i < 5; i++)
        {
            go = m_tran.Find("progress/progress_bar" + i + "/progressBg0").gameObject;
            m_grayProgressList[i] = go;

            go = m_tran.Find("progress/progress_bar" + i + "/progressBg1").gameObject;
            m_redProgressList[i] = go;
            m_redProgressImgList[i] = m_tran.Find("progress/progress_bar" + i + "/progress1").GetComponent<Image>();

            go = m_tran.Find("progress/progress_bar" + i + "/progressBg2").gameObject;
            m_blueProgressList[i] = go;
            m_blueProgressImgList[i] = m_tran.Find("progress/progress_bar" + i + "/progress2").GetComponent<Image>();

        }
        m_redBarGo = m_tran.Find("progressBar/progressBar1").gameObject;
        m_blueBarGo = m_tran.Find("progressBar/progressBar2").gameObject;
        m_redBarImg = m_redBarGo.GetComponent<Image>();
        m_blueBarImg = m_blueBarGo.GetComponent<Image>();
        m_progressBar = m_tran.Find("progressBar").gameObject;
        m_numText1 = m_tran.Find("progressBar/numText1").GetComponent<Text>();
        m_numText2 = m_tran.Find("progressBar/numText2").GetComponent<Text>();
        m_otBarGo = m_tran.Find("OTprogressBar").gameObject;
        m_otBarGo.TrySetActive(false);

        m_otImg = m_otBarGo.transform.Find("progressBar").GetComponent<Image>();
        createEffect();
    }

    private void createEffect()
    {
        UIEffect effect = null;
        for (int i = 0; i < 5; i++)
        {
            effect = UIEffect.ShowEffect(m_grayProgressList[i].transform.parent, EffectConst.UI_DAGGEROW_ZHENGDUOZHONG, m_effectPos);
            m_fightEffectList[i] = effect;
            effect.Hide();
        }

        m_otEffect = UIEffect.ShowEffectBefore(m_otImg.transform, EffectConst.UI_DAGGEROW_OVERTIME, new Vector3(54, -9, 0));
    }

    public override void InitEvent()
    {
        AddEventListener<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_ENTER_BURST_POINT, OnEnterPoint);
        AddEventListener<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_LEAVE_BURST_POINT, OnLeavePoint);
        AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
    }

    private void OnLeavePoint(GameObject whichPoint, GameObject go, string type, int id)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (go == MainPlayer.singleton.gameObject)
        {
            if (type == "Dagger_ow")
            {
                ReSetBar();
            }
        }
    }

    private void OnEnterPoint(GameObject whichPoint, GameObject go, string type, int id)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (go == MainPlayer.singleton.gameObject)
        {
            if (type == "Dagger_ow")
            {
                showProgressBar(true);
                m_pointId = id;
                m_redBarImg.fillAmount = m_redProgressImgList[id - 1].fillAmount;
                m_blueBarImg.fillAmount = m_blueProgressImgList[id - 1].fillAmount;
            }
        }
    }

    public void ReSetBar()
    {
        showProgressBar(false);
        updateBar(0, 0);
        m_pointId = -1;
    }

    private void OnPlayerRevive(string name, BasePlayer player)
    {
        ReSetBar();
    }

    private void showProgressBar(bool isShow)
    {
        m_blueBarImg.enabled = isShow;
        m_redBarImg.enabled = isShow;
        m_progressBar.TrySetActive(isShow);
    }

    public void UpdateProgress()
    {
        m_curInfo = m_behavior.GetOccupyInfo(m_pointId);
        p_occupy_info[] list = m_behavior.GetOccupyInfoList();
        if (list == null || list.Length == 0)
        {
            Logger.Log("p_occupy_info list is null");
            return;
        }
        p_occupy_info info = null;
        for (int i = 0; i < list.Length; i++)
        {
            info = list[i];
            m_blueProgressList[i].TrySetActive(true);
            m_redProgressList[i].TrySetActive(true);
            int score = 0;
            int score1 = 0;
            score = (info.scores == null || info.scores.Length == 0) ? 0 : info.scores[0];
            if (info.scores != null && info.scores.Length == 2)
            {
                score1 = info.scores[1];
            }
            float percent1 = score * 1.0f / m_totlaScore;
            float percent2 = score1 * 1.0f / m_totlaScore;
            if( info.owner == 0 )
            {
                m_grayProgressList[i].TrySetActive(true);
                m_redProgressList[i].transform.GetComponent<Image>().enabled = false;
                m_blueProgressList[i].transform.GetComponent<Image>().enabled = false;
               
                m_redProgressImgList[i].fillAmount = percent1;
                m_blueProgressImgList[i].fillAmount = percent2;
                
            }
            else if (info.owner == 1)
            {
                m_grayProgressList[i].TrySetActive(false);
                m_grayProgressList[i].transform.GetComponent<Image>().enabled = true;
                m_redProgressList[i].transform.GetComponent<Image>().enabled = true;
                m_blueProgressList[i].transform.GetComponent<Image>().enabled = false;

                m_redProgressImgList[i].fillAmount = 0;
                m_blueProgressImgList[i].fillAmount = percent2;
            }
            else
            {
                m_redProgressList[i].transform.GetComponent<Image>().enabled = false;
                m_blueProgressList[i].transform.GetComponent<Image>().enabled = true;
                m_grayProgressList[i].TrySetActive(false);
                m_redProgressImgList[i].fillAmount = percent1;
                m_blueProgressImgList[i].fillAmount = 0;
                
            }

            if (info.id == m_pointId)
            {
                updateBar(percent1, percent2);
                m_numText1.text = info.occupyMember1.ToString();
                m_numText2.text = info.occupyMember2.ToString();
            }
            showFightEffect(i, info.occupyMember1, info.occupyMember2);
            //Debug.LogError("owner == " + info.owner + "-----------" + percent1 + "______________________" + percent2);
        }
        //加时处理
        if (list.Length == 1 && m_cdTime > 0)
        {
            m_curInfo = m_behavior.GetOccupyInfo(1);
            var resoureList = m_behavior.ResoureList();
            int winParam = m_mapList.OwTarget;
            if (resoureList != null)
            {
                
                for (int i = 0; i < resoureList.Length; i++)
                {
                    m_otTarget = int.Parse((winParam * m_OT).ToString());
                    //Debug.LogError("i = " + i + "  resoureList[i] =   " + resoureList[i] +"  len = "+ resoureList.Length);
                    if (resoureList[i] >= m_otTarget)
                    {
                        //Debug.LogError("加时-----------");
                        m_nowOwerId = m_curInfo.owner;
                        if (m_lastOwerId != m_nowOwerId )
                        {
                            if (m_lastOwerId > 0)
                            {
                                Debug.Log("失去资源点");
                                m_otBarGo.TrySetActive(false);
                                m_lastOwerId = m_nowOwerId;
                                return;
                            }
                            m_lastOwerId = m_nowOwerId;
                        }
                      
                        if (m_curInfo.owner == (int)WorldManager.CAMP_DEFINE.UNION)
                        {
                            if (m_curInfo.occupyMember1 > 0 && m_curInfo.occupyMember2 > 0 && resoureList[1] == m_otTarget && resoureList[0] < m_otTarget)
                            {
                                m_otBarGo.TrySetActive(false);
                                curTime = 0;
                                return;
                            }
                            if (m_curInfo.occupyMember2 > 0 && resoureList[0] < m_otTarget)
                            {
                                m_otBarGo.TrySetActive(false);
                                curTime = 0;
                                return;
                            }
                            if (m_curInfo.occupyMember2 > 0)
                            {
                                //显示加时进度条 但不走
                                m_showOTProgress = false;
                                m_otImg.fillAmount = 1.0f;
                                m_otEffect.m_goEffect.transform.localPosition = new Vector3(m_otBarWidth * 0.5f, -9, 0);
                                if (m_otBarGo != null && finish == false)
                                {
                                    m_otBarGo.TrySetActive(true);
                                    if (m_otEffect.m_hide == true)
                                        m_otEffect.Play();
                                    SortingOrderRenderer.RebuildAll();
                                }
                                    
                            }
                            else if (m_curInfo.occupyMember2 == 0)
                            {
                                //开始倒计时
                                m_showOTProgress = true;
                            }

                        }
                        else
                        {
                            if (m_curInfo.occupyMember1 > 0 && m_curInfo.occupyMember2 > 0 && resoureList[0] == m_otTarget && resoureList[1] < m_otTarget)
                            {
                                m_otBarGo.TrySetActive(false);
                                curTime = 0;
                                return;
                            }
                            if (m_curInfo.occupyMember1 > 0 && resoureList[1] < m_otTarget)
                            {
                                m_otBarGo.TrySetActive(false);
                                curTime = 0;
                                return;
                            }
                            if (m_curInfo.occupyMember1 > 0)
                            {
                                //显示加时进度条 但不走
                                m_showOTProgress = false;
                                m_otImg.fillAmount = 1.0f;
                                m_otEffect.m_goEffect.transform.localPosition = new Vector3(m_otBarWidth * 0.5f, -9, 0);
                                if (m_otBarGo != null && finish == false)
                                {
                                    m_otBarGo.TrySetActive(true);
                                    if (m_otEffect.m_hide == true)
                                        m_otEffect.Play();
                                    SortingOrderRenderer.RebuildAll();
                                }
                                   
                            }
                            else if (m_curInfo.occupyMember1 == 0)
                            {
                                //开始倒计时
                                m_showOTProgress = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private void showFightEffect(int index, int redMember,int blueMember)
    {
        if (redMember > 0 && blueMember > 0)
        {
            if (m_fightEffectList[index].m_hide == true)
            {
                m_fightEffectList[index].Play();
            }
        }
        else
        {
            m_fightEffectList[index].Hide();
        }
    }

    private void updateBar(float value1, float value2)
    {
        m_redBarImg.fillAmount = value1;
        float temp = value2;
        if (value1 > 0 && value2 > 0)
        {
            temp = 1 - value1 < 0 ? 0 : 1 - value1;
        }
        m_blueBarImg.fillAmount = temp;

    }

    public void BindBehavior(DaggerPointBehavior behavior)
    {
        m_behavior = behavior;
    }

    public override void OnShow()
    {
        m_totlaScore = ConfigMisc.GetInt("ow_score_target");
        var mapRule = ConfigManager.GetConfig<ConfigMapRule>().GetRule(SceneManager.singleton.curMapId, WorldManager.singleton.gameRule);
        m_cdTime = mapRule.Overtime+ 0.4f;
        m_progressBar.TrySetActive(false);
        m_numText1.text = "";
        m_numText2.text = "";
        m_mapList = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString());
        if (m_mapList != null && m_mapList.StrongPointEffect != "")
        {
            string[] effectList = m_mapList.StrongPointEffect.Split(';');
            initPointVisible(effectList.Length);
            for (int i = 0; i < effectList.Length; i++)
            {
                if (int.Parse(effectList[i]) == 0)
                {
                    m_blueProgressList[i].TrySetActive(false);
                    m_redProgressList[i].TrySetActive(false);

                }
                else if (int.Parse(effectList[i]) == 1)
                {
                    m_blueProgressList[i].TrySetActive(false);
                    m_redProgressList[i].TrySetActive(true);
                }
                else
                {
                    m_blueProgressList[i].TrySetActive(true);
                    m_redProgressList[i].TrySetActive(false);
                }
            }
        }
    }

    private void initPointVisible(int num)
    {
        for( int i = 0; i < 5; i++ )
        {
            if( num > i )
            {
                m_grayProgressList[i].transform.parent.gameObject.TrySetActive(true);
            }
            else
            {
                m_grayProgressList[i].transform.parent.gameObject.TrySetActive(false);
            }
        }
    }

    float curTime = 0;
    bool finish = false;
    public override void Update()
    {
        if (m_showOTProgress == true && finish == false)
        {
            curTime += Time.deltaTime;
            if (curTime > m_cdTime)
            {
                //Debug.LogError("_____________finish");
                finish = true;
                m_otImg.fillAmount = 1.0f;
                m_otBarGo.TrySetActive(false);
                m_otEffect.m_goEffect.transform.localPosition = new Vector3(m_otBarWidth * 0.5f, -9, 0);
                curTime = 0;
            }
            else
            {
                float curProgress = 1.0f - (curTime / m_cdTime);
                m_otEffect.m_goEffect.transform.localPosition = new Vector3(m_otBarWidth * 0.5f * (2 * curProgress - 1), -9, 0);
                if (m_otImg != null) m_otImg.fillAmount = curProgress;
                
            }
        }
        else
        {
            curTime = 0;
            if( finish == true )
            {
                m_otBarGo.TrySetActive( false);
            }
            
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }
}
