﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：限时礼物品项
//创建者：HWL
//创建时间: 2016/7/01 15:11:53
///////////////////////////
public class LimitGiftItem : ItemRender
{
    private GameObject m_buyBtnGo;
    private Button buyBtn;
    private Text m_itemName;
    private Text m_originalCost;
    private Text m_nowCost;
    private Text m_btnLabel;
    private Image m_itemIcon;
    private string m_RewardId;
    private int giftId = 0;
    private int buy = 0;
    private int needDiamond = 0;
    private GameObject m_iconBg;
    private Image m_bg;
    private DataGrid m_dataGrid;
    public override void Awake()
    {
        m_bg = transform.Find("bg").GetComponent<Image>();
        m_buyBtnGo = transform.Find("buybtn").gameObject;
        buyBtn = m_buyBtnGo.GetComponent<Button>();
        m_btnLabel = transform.Find("buybtn/label/").GetComponent<Text>();
        m_itemName = transform.Find("itemName").GetComponent<Text>();
        m_originalCost = transform.Find("price/priceText").GetComponent<Text>();
        m_nowCost = transform.Find("buybtn/priceText").GetComponent<Text>();
        m_itemIcon = transform.Find("itemIcon").GetComponent<Image>();

        m_iconBg = transform.Find("iconbg/").gameObject;
        m_dataGrid = transform.Find("iconbg/iconList").gameObject.AddMissingComponent<DataGrid>();
        GameObject go = transform.Find("iconbg/iconList/Item").gameObject;
        m_dataGrid.SetItemRender(go, typeof(RewardIcon));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useLoopItems = false;

        UGUIClickHandler.Get(m_buyBtnGo).onPointerClick += OnBuyGiftHandle;
    }

    private void OnBuyGiftHandle(GameObject target, PointerEventData eventData)
    {
        if (buy == 1)
        {
            return;
        }
        if (needDiamond > PlayerSystem.roleData.diamond)
        {
            UIManager.ShowTipPanel("钻石不足是否前往充值？", delegate
            {
                UIManager.GetPanel<PanelLimitGift>().HidePanel();
                if (UIManager.GetPanel<PanelHallBattle>() != null)
                {
                    SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
                }
                if (UIManager.GetPanel<PanelSettle>() != null)
                {
                    UISystem.isShowRecharge = true;
                    UIManager.GetPanel<PanelSettle>().HidePanel();
                }
                
            }, null, true, true, "充值", "取消");
            return;
        }
        UIManager.ShowTipPanel("是否花费" + needDiamond + "钻石购买" + m_itemName.text + "？", delegate
        {
            NetLayer.Send(new tos_player_timelimit_gift_buy() { gift_id = giftId, reward_id = m_RewardId });
            Logger.Log("m_giftId--------" + giftId + "____________" + m_RewardId);
        }, null, true, true, "确定", "取消");
        
    }

    protected override void OnSetData(object data)
    {
        p_timelimit_item item = data as p_timelimit_item;
        buy = item.buy;
        if (buy == 0)
        {
            m_btnLabel.text = "购买";
            Util.SetNormalShader(buyBtn);
        }
        else
        {
            m_btnLabel.text = "已购买";
            Util.SetGrayShader(buyBtn);
        }
        if (UIManager.GetPanel<PanelLimitGift>() != null)
        {
            giftId = UIManager.GetPanel<PanelLimitGift>().m_LimitGiftId;
        }
        m_RewardId = item.reward_id;

        ConfigReward reward = ConfigManager.GetConfig<ConfigReward>();
        ConfigRewardLine rewardLine = reward.GetLine(m_RewardId);
        if( rewardLine == null )
        {
            Logger.Log("reward变数据为null  id = " + m_RewardId);
            return;
        }
        ItemInfo[] itemList = rewardLine.ItemList;
        if (itemList.Length == 0)
        {
            Logger.Log("ItemList 长度为0 id =  " + m_RewardId);
            return;
        }
        int bigIconID = itemList[0].ID;
        ConfigItemWeapon itemWeapon = ConfigManager.GetConfig<ConfigItemWeapon>();
        ConfigItemWeaponLine itemLine = null;
        if( itemWeapon != null )
        {
            itemLine = itemWeapon.GetLine(bigIconID);
        }
        if( itemLine == null )
        {
            Logger.Log("m_itemId == " + bigIconID + "  数据为空");
            return;
        }

        m_itemIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, itemLine.Icon));
        if (itemList.Length == 1)
        {
            m_iconBg.TrySetActive(false);
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.LimitGift, "itembg"));
            m_itemIcon.rectTransform.anchoredPosition = new Vector3(96f, -146f, 0f);
        }
        else
        {
            m_iconBg.TrySetActive(true);
            m_itemIcon.rectTransform.anchoredPosition = new Vector3(96f, -104f, 0f);
            m_dataGrid.Data = itemList.ArrayDelete<ItemInfo>(0);
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.LimitGift, "mItembg"));
        }
        string priceStr = GetItemInfo();
        needDiamond = 0;
        int limitTime = 0;
        m_nowCost.text = item.price.ToString();
        needDiamond = item.price;
        if( priceStr != "" )
        {
            string[] arr = priceStr.Split('|');
            m_originalCost.text = arr[0];
        }
        else
        {
            Logger.Log("配置数据表有错找不到价格" + "m_giftId--------" + giftId + "____________" + m_RewardId);
            m_nowCost.text = "";
            m_originalCost.text = "";
        }
        if( limitTime == 0 )
        {
            m_itemName.text = itemLine.DispName + "(永久)";
        }
        else
        {
            m_itemName.text = itemLine.DispName + "(" + limitTime + "天)";
        }
    }

    private string GetItemInfo()
    {
        ConfigLimitGift limitGift = ConfigManager.GetConfig<ConfigLimitGift>();
        ConfigLimitGiftLine giftLine = null;
        if( limitGift != null )
        {
            giftLine = limitGift.GetLine(giftId.ToString());
        }
        if( giftLine == null )
        {
            return "";
        }
        string[] info = giftLine.GiftList.Split(';');
        string[] item = null;
        for( int i = 0 ; i < info.Length; i++ )
        {
            item = info[i].Split('#');
            for( int j = 0; j < item.Length; j++ )
            {
                if (m_RewardId.Equals(item[0]))
                {
                    return item[1] + "|" + item[2];
                }
            }
        }
        return "";
    }
}

public class RewardIcon : ItemRender
{
    private Image m_icon;
    private Text m_text;
    public override void Awake()
    {
        m_icon = transform.Find("iconItem").GetComponent<Image>();
        m_text = transform.Find("lable").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        ItemInfo item = data as ItemInfo;
        ConfigItemLine itemLine =  ItemDataManager.GetItem(item.ID);
        if (itemLine == null)
        {
            return;
        }
        m_icon.SetSprite(ResourceManager.LoadIcon(item.ID));
        m_icon.SetNativeSize();
        if (itemLine.Type == GameConst.ITEM_TYPE_EQUIP)
        {
            m_text.text = item.day == 0 ? "(永久)" : "(" + item.day + "天)";
        }
        else
        {
            m_text.text =  "(" + item.cnt + "个)";
        }
    }
}