﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：限时礼包
//创建者：HWL
//创建时间: 2016/7/01 14:37:53
///////////////////////////
public class PanelLimitGift : BasePanel
{
    private Text m_timeText;
    private int leftTime;
    private DataGrid m_itemDataGrid;
    public int m_LimitGiftId;
    private Text m_descText;
    private float m_limitGiftLastTime;
    public PanelLimitGift()
    {
        SetPanelPrefabPath("UI/LimitGift/PanelLimitGift");
        AddPreLoadRes("UI/LimitGift/PanelLimitGift", EResType.UI);
        AddPreLoadRes("Atlas/LimitGift", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }
    public override void Init()
    {
        m_itemDataGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddMissingComponent<DataGrid>();
        m_itemDataGrid.SetItemRender(m_tran.Find("ScrollDataGrid/content/item").gameObject, typeof(LimitGiftItem));
        m_timeText = m_tran.Find("time/timeText").GetComponent<Text>();
        m_itemDataGrid.useLoopItems = false;
        m_itemDataGrid.autoSelectFirst = false;

        m_descText = m_tran.Find("desc").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate{ this.OnHide(); };
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            m_LimitGiftId = (int)m_params[0];
            leftTime = int.Parse(m_params[1].ToString());
            
            m_itemDataGrid.Data = m_params[2] as p_timelimit_item[];
            if( m_LimitGiftId == 0 )
            {
                m_descText.text = "";
            }
            else
            {
                m_descText.text = "【三选一，每日限购一次】";
            }
        }
    }

    public void UpdateLeftTime(float time)
    {
        leftTime = (int)time;
        m_timeText.text = TimeUtil.FormatTime(leftTime, 3, false); ;
    }

    public void UpdateView(p_timelimit_item[] list)
    {
        m_itemDataGrid.Data = list;
    }

    public override void OnHide()
    {
        base.HidePanel();
        Debug.Log("hidepanel --");
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if( leftTime <= 0 )
        {
            this.HidePanel();
        }
        if( UIManager.IsOpen<PanelHallBattle>() == false )
        {
            if (Time.realtimeSinceStartup - m_limitGiftLastTime >= 1)
            {
                m_limitGiftLastTime = Time.realtimeSinceStartup;
                leftTime--;
            }
            if (leftTime > 0)
            {
                UpdateLeftTime(leftTime);
            }
        }
    }
}
