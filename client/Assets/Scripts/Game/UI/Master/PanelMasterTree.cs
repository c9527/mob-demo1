﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelMasterTree : BasePanel
{
    private master_tree_toc m_myself;   //> 自己
    private master_tree_toc m_master;   //> 师傅
    private List<master_tree_toc> m_lstMasterBrothers;  //> 师叔伯
    private List<master_tree_toc> m_lstMates;   //>师兄弟
    private List<master_tree_toc> m_lstApprentices; //> 徒弟
    private Dictionary<long, List<master_tree_toc>> m_dicSecondApprentices;    //> 徒孙

    private Transform m_tranMyself;
    private Transform m_tranMaster;
    private Transform m_selTran;
    private List<Transform> m_lstTranMasterBrother;
    private List<Transform> m_lstTranMates;
    private List<Transform> m_lstTranApprentices;
    private Dictionary<long, List<Transform>> m_dicTranSecondApprentices;
    private static float UPDATE_TIME = 300f;
    private float m_fTime;

    private Button m_btnAdd;
    private Button m_btnFollow;
    private Button m_btnLookup;
    private Button m_btnChat;

    private master_tree_toc m_selectNode;
    private Text m_txtUpdateTime;

    private int m_iTick;
    public PanelMasterTree()
    {
        SetPanelPrefabPath("UI/Master/PanelMasterTree");
        AddPreLoadRes("UI/Master/PanelMasterTree", EResType.UI);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
    }

    public override void Init()
    {
        m_lstMasterBrothers = new List<master_tree_toc>();
        m_lstMates = new List<master_tree_toc>();
        m_lstApprentices = new List<master_tree_toc>();
        m_dicSecondApprentices = new Dictionary<long, List<master_tree_toc>>();

        m_lstTranMasterBrother = new List<Transform>();
        m_lstTranMates = new List<Transform>();
        m_lstTranApprentices = new List<Transform>();
        m_dicTranSecondApprentices = new Dictionary<long, List<Transform>>();
        m_fTime = 0.0f;

        m_tranMyself = m_tran.Find("mates/mate2");
        m_tranMaster = m_tran.Find("master_brothers/master2");
        m_lstTranMasterBrother.Add(m_tran.Find("master_brothers/master1"));
        m_lstTranMasterBrother.Add(m_tran.Find("master_brothers/master3"));
        m_lstTranMasterBrother.Add(m_tran.Find("master_brothers/master4"));

        m_lstTranMates.Add(m_tran.Find("mates/mate1"));
        m_lstTranMates.Add(m_tran.Find("mates/mate3"));
        m_lstTranMates.Add(m_tran.Find("mates/mate4"));

        m_lstTranApprentices.Add(m_tran.Find("apprentices/apprentice1"));
        m_lstTranApprentices.Add(m_tran.Find("apprentices/apprentice2"));
        m_lstTranApprentices.Add(m_tran.Find("apprentices/apprentice3"));
        m_lstTranApprentices.Add(m_tran.Find("apprentices/apprentice4"));

        m_dicTranSecondApprentices[0] = new List<Transform>();
        m_dicTranSecondApprentices[0].Add(m_tran.Find("second_apprentices/apprentice1/second_apprentice1"));
        m_dicTranSecondApprentices[0].Add(m_tran.Find("second_apprentices/apprentice1/second_apprentice2"));
        m_dicTranSecondApprentices[0].Add(m_tran.Find("second_apprentices/apprentice1/second_apprentice3"));
        m_dicTranSecondApprentices[0].Add(m_tran.Find("second_apprentices/apprentice1/second_apprentice4"));

        m_dicTranSecondApprentices[1] = new List<Transform>();
        m_dicTranSecondApprentices[1].Add(m_tran.Find("second_apprentices/apprentice2/second_apprentice1"));
        m_dicTranSecondApprentices[1].Add(m_tran.Find("second_apprentices/apprentice2/second_apprentice2"));
        m_dicTranSecondApprentices[1].Add(m_tran.Find("second_apprentices/apprentice2/second_apprentice3"));
        m_dicTranSecondApprentices[1].Add(m_tran.Find("second_apprentices/apprentice2/second_apprentice4"));

        m_dicTranSecondApprentices[2] = new List<Transform>();
        m_dicTranSecondApprentices[2].Add(m_tran.Find("second_apprentices/apprentice3/second_apprentice1"));
        m_dicTranSecondApprentices[2].Add(m_tran.Find("second_apprentices/apprentice3/second_apprentice2"));
        m_dicTranSecondApprentices[2].Add(m_tran.Find("second_apprentices/apprentice3/second_apprentice3"));
        m_dicTranSecondApprentices[2].Add(m_tran.Find("second_apprentices/apprentice3/second_apprentice4"));

        m_dicTranSecondApprentices[3] = new List<Transform>();
        m_dicTranSecondApprentices[3].Add(m_tran.Find("second_apprentices/apprentice4/second_apprentice1"));
        m_dicTranSecondApprentices[3].Add(m_tran.Find("second_apprentices/apprentice4/second_apprentice2"));
        m_dicTranSecondApprentices[3].Add(m_tran.Find("second_apprentices/apprentice4/second_apprentice3"));
        m_dicTranSecondApprentices[3].Add(m_tran.Find("second_apprentices/apprentice4/second_apprentice4"));

        m_btnAdd = m_tran.Find("option/add").GetComponent<Button>();
        m_btnFollow = m_tran.Find("option/follow").GetComponent<Button>();
        m_btnLookup = m_tran.Find("option/look_up").GetComponent<Button>();
        m_btnChat = m_tran.Find("option/chat").GetComponent<Button>();
        m_txtUpdateTime = m_tran.Find("update").GetComponent<Text>();
        m_iTick = 0;
    }

    public override void Update()
    {
       
        if (m_fTime > 0.0f)
            m_fTime -= Time.deltaTime;

        m_iTick++;

        if (IsOpen())
        {
            if(m_iTick % 30 == 0)
            {
                int min = (int)m_fTime / 60;
                int sec = (int)m_fTime % 60;
                string update = "";
                if (min != 0)
                {
                    update += min.ToString();
                    update += ":";
                }

                update += sec.ToString();
                update += "后刷新师门树";
                m_txtUpdateTime.text = update;
                    
            }
            if (m_fTime <= 0.0f)
            {
                NetLayer.Send(new tos_master_tree());
                m_fTime = UPDATE_TIME;
            }
        }
       
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("master_brothers/master1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("master_brothers/master2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("master_brothers/master3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("master_brothers/master4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("mates/mate1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("mates/mate3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("mates/mate4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("apprentices/apprentice1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("apprentices/apprentice2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("apprentices/apprentice3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("apprentices/apprentice4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice1/second_apprentice1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice1/second_apprentice2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice1/second_apprentice3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice1/second_apprentice4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice2/second_apprentice1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice2/second_apprentice2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice2/second_apprentice3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice2/second_apprentice4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice3/second_apprentice1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice3/second_apprentice2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice3/second_apprentice3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice3/second_apprentice4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice4/second_apprentice1").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice4/second_apprentice2").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice4/second_apprentice3").gameObject).onPointerClick += OnClkTreeNode;
        UGUIClickHandler.Get(m_tran.Find("second_apprentices/apprentice4/second_apprentice4").gameObject).onPointerClick += OnClkTreeNode;

        UGUIClickHandler.Get(m_tran.Find("option/add").gameObject).onPointerClick += OnClkAdd;
        UGUIClickHandler.Get(m_tran.Find("option/follow").gameObject).onPointerClick += OnClkFollow;
        UGUIClickHandler.Get(m_tran.Find("option/look_up").gameObject).onPointerClick += OnClkLookup;
        UGUIClickHandler.Get(m_tran.Find("option/chat").gameObject).onPointerClick += OnClkChat;
    }

    public override void OnShow()
    {
        if (m_fTime <= 0f )
        {
            NetLayer.Send(new tos_master_tree());
            m_fTime = UPDATE_TIME;
        }
       
    }

    protected void ResetTrans()
    {
        ResetInfo( m_tranMyself);
        ResetInfo(m_tranMaster);
        
        foreach( Transform tran in m_lstTranMasterBrother )
        {
            ResetInfo(tran);
        }

        foreach( Transform tran in m_lstTranMates )
        {
            ResetInfo(tran);
        }

        foreach( Transform tran in m_lstTranApprentices )
        {
            ResetInfo(tran);
        }

        for( int i = 0 ; i < m_dicTranSecondApprentices.Count; ++i )
        {
            if( m_dicTranSecondApprentices.ContainsKey(i) )
            {
                foreach( Transform tran in m_dicTranSecondApprentices[i] )
                {
                    ResetInfo(tran);
                }
            }
        }
    }

    protected void ResetInfo( Transform tran )
    {
        Toggle togg = tran.GetComponent<Toggle>();
        if(togg != null)
        {
            //togg.enabled = false;
            //bool bFlag = togg.isOn;
            if (togg.isOn)
                m_selTran = tran;

            togg.isOn = false;
            togg.interactable = false;
        }
        tran.FindChild("head").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "master_guest"));
        tran.FindChild("head").GetComponent<Image>().SetNativeSize();
        tran.FindChild("head").localScale = Vector3.one;
        tran.FindChild("name").GetComponent<Text>().text = "暂无";
        tran.FindChild("id").GetComponent<Text>().text = "0";
    }
    protected void UpdateView()
    {
        m_selectNode = null;
        m_selTran = null;

        ResetTrans();

        if (m_myself == null)
            return;

        SetInfo(m_tranMyself, m_myself);
        if (m_master != null)
            SetInfo(m_tranMaster, m_master);

        for(int i =0; i < m_lstMasterBrothers.Count; ++i)
        {
            SetInfo(m_lstTranMasterBrother[i], m_lstMasterBrothers[i]);
        }

        for(int i =0; i < m_lstMates.Count; ++i)
        {
            SetInfo(m_lstTranMates[i], m_lstMates[i]);
        }

        for(int i = 0; i < m_lstApprentices.Count; ++i)
        {
            SetInfo(m_lstTranApprentices[i], m_lstApprentices[i]);
            List<Transform> lstTran = m_dicTranSecondApprentices[i];
            if (m_dicSecondApprentices.ContainsKey(m_lstApprentices[i].apprentice))
            {
                List<master_tree_toc> lstInfo = m_dicSecondApprentices[m_lstApprentices[i].apprentice];
                for( int j = 0; j < lstInfo.Count; ++j)
                {
                    SetInfo(lstTran[j], lstInfo[j]);
                }
            }
        }

    }

    protected void SetInfo( Transform tran,master_tree_toc info )
    {
        if (m_selTran == tran)
            m_selectNode = info;

        tran.FindChild("name").GetComponent<Text>().text = info.name;
        tran.FindChild("head").GetComponent<Image>().SetSprite(ResourceManager.LoadRoleIcon(info.icon));
        tran.FindChild("head").GetComponent<Image>().SetNativeSize();
        Vector3 scale = new Vector3(0.8f, 0.75f, 1f);
        tran.FindChild("head").localScale = scale;
        Toggle togg = tran.GetComponent<Toggle>();
        if(togg != null)
        {
            //togg.enabled = true;
            togg.interactable = true;
        }

        tran.FindChild("id").GetComponent<Text>().text = info.apprentice.ToString();
    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    { }

    protected void OnClose(GameObject target, PointerEventData eventData)
    {
       
    }

    private void Toc_master_tree( toc_master_tree proto )
    {
        m_myself = null;
        m_master = null;
        m_lstMates.Clear();
        m_lstMasterBrothers.Clear();
        m_lstApprentices.Clear();
        m_dicSecondApprentices.Clear();

        for (int i = 0; i < proto.mates.Length; ++i)
        {
            master_tree_toc info = new master_tree_toc();
            info.master = proto.mates[i].master;
            info.apprentice = proto.mates[i].apprentice;
            info.name = proto.mates[i].name;
            info.icon = proto.mates[i].icon;
            info.level = proto.mates[i].level;
            info.sex = proto.mates[i].sex;

            if (info.apprentice == PlayerSystem.roleId)
                m_myself = info;
            else
                m_lstMates.Add(info);
        }

        if (m_myself == null)
        {
            m_myself = new master_tree_toc();
            if(PlayerFriendData.singleton.m_masters.Count > 0 )
            {
                m_myself.master = PlayerFriendData.singleton.m_masters[0].id;
            }
            else
            {
                m_myself.master = 0;
            }
            m_myself.apprentice = PlayerSystem.roleId;
            m_myself.name = PlayerSystem.roleData.name;
            m_myself.icon = PlayerSystem.roleData.icon;
            m_myself.level = PlayerSystem.roleData.level;
            m_myself.sex = PlayerSystem.roleData.sex;
        }
         
        for( int i = 0; i < proto.masters.Length; ++i )
        {
            master_tree_toc info = new master_tree_toc();
            info.master = proto.masters[i].master;
            info.apprentice = proto.masters[i].apprentice;
            info.name = proto.masters[i].name;
            info.icon = proto.masters[i].icon;
            info.level = proto.masters[i].level;
            info.sex = proto.masters[i].sex;

            if (info.apprentice == m_myself.master)
                m_master = info;
            else
                m_lstMasterBrothers.Add(info);
        }

        for (int i = 0; i < proto.apprentice.Length; ++i)
        {
            master_tree_toc info = new master_tree_toc();
            info.master = proto.apprentice[i].master;
            info.apprentice = proto.apprentice[i].apprentice;
            info.name = proto.apprentice[i].name;
            info.icon = proto.apprentice[i].icon;
            info.level = proto.apprentice[i].level;
            info.sex = proto.apprentice[i].sex;

            m_lstApprentices.Add(info);
        }

        for (int i = 0; i < proto.second_apprentice.Length; ++i)
        {
            master_tree_toc info = new master_tree_toc();
            info.master = proto.second_apprentice[i].master;
            info.apprentice = proto.second_apprentice[i].apprentice;
            info.name = proto.second_apprentice[i].name;
            info.icon = proto.second_apprentice[i].icon;
            info.level = proto.second_apprentice[i].level;
            info.sex = proto.second_apprentice[i].sex;

            List<master_tree_toc> lstSecond = null;
            if (m_dicSecondApprentices.ContainsKey(info.master))
                lstSecond = m_dicSecondApprentices[info.master];
            else
            {
                lstSecond = new List<master_tree_toc>();
                m_dicSecondApprentices[info.master] = lstSecond;
            }

            lstSecond.Add(info);
        }

        UpdateView();
    }

    protected master_tree_toc GetTreeNode( long id )
    {
        if (m_master.apprentice == id)
            return m_master;

        foreach (master_tree_toc node in m_lstMasterBrothers)
        {
            if (node.apprentice == id)
                return node;
        }

        foreach (master_tree_toc node in m_lstMates)
        {
            if (node.apprentice == id)
                return node;
        }

        foreach (master_tree_toc node in m_lstApprentices)
        {
            if (node.apprentice == id)
                return node;
        }

        for (int i = 0; i < m_dicSecondApprentices.Count; ++i )
        {
            if( m_dicSecondApprentices.ContainsKey(i) )
            {
                List<master_tree_toc> lstNodes = m_dicSecondApprentices[i];
                foreach( master_tree_toc node in lstNodes )
                {
                    if (node.apprentice == id)
                        return node;
                }
            }
        }
           
        return null;
    }

    protected master_tree_toc GetSwitchOnTreeNode()
    {

        return null;
    }
    protected void OnClkTreeNode(GameObject target, PointerEventData eventData)
    {
        string strId = target.transform.FindChild("id").GetComponent<Text>().text;
        long id = long.Parse(strId);
        if (id == 0)
            return;

        m_selectNode = GetTreeNode(id);
        
        ResetButtons();
    }

    protected void ResetButtons()
    {
        if (m_selectNode == null)
            return;
        if (m_selectNode.apprentice == PlayerSystem.roleId)
            return;

        if (PlayerFriendData.singleton.isFriend(m_selectNode.apprentice))
        {
            Util.SetGrayShader(m_btnAdd);
            Util.SetNormalShader(m_btnFollow);
            
        }
        else
        {
            Util.SetNormalShader(m_btnAdd);
            Util.SetGrayShader(m_btnFollow);
        }
    }

    protected void OnClkAdd(GameObject target, PointerEventData eventData)
    {
        if(m_selectNode == null)
        {
            TipsManager.Instance.showTips("请选择一个对象!");
            return;
        }

        if(PlayerFriendData.singleton.isFriend(m_selectNode.apprentice))
        {
            TipsManager.Instance.showTips(string.Format("{0}已经是你的好友", m_selectNode.name));
            return;
        }

        PlayerFriendData.AddFriend(m_selectNode.apprentice);

    }

    protected void OnClkFollow(GameObject target, PointerEventData eventData)
    {
        if (m_selectNode == null)
        {
            TipsManager.Instance.showTips("请选择一个对象!");
            return;
        }

        RoomModel.FollowFriend(m_selectNode.apprentice, false, false);
    }

    protected void OnClkLookup(GameObject target, PointerEventData eventData)
    {
        if (m_selectNode == null)
        {
            TipsManager.Instance.showTips("请选择一个对象!");
            return;
        }

        if (m_selectNode.apprentice == PlayerSystem.roleId)
            return;

        NetLayer.Send(new tos_player_view_player { friend_id = m_selectNode.apprentice });
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data,UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    protected void OnClkChat(GameObject target, PointerEventData eventData)
    {
        if (m_selectNode == null)
        {
            TipsManager.Instance.showTips("请选择一个对象!");
            return;
        }

        if (m_selectNode.apprentice == PlayerSystem.roleId)
            return;

        bool isFriend = PlayerFriendData.singleton.isFriend(m_selectNode.apprentice);
        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = m_selectNode.apprentice;
        rcd.m_strName = m_selectNode.name;
        rcd.m_iSex = m_selectNode.sex;
        rcd.m_iLevel = m_selectNode.level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = m_selectNode.icon;
        ChatMsgTips msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = { msgTips };
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, this);
    }
}
