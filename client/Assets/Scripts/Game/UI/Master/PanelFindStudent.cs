﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


class PanelFindStudent : BasePanel
{
    public PanelFindStudent()
    {
        SetPanelPrefabPath("UI/Master/PanelFindStudent");
        AddPreLoadRes("UI/Master/PanelFindStudent", EResType.UI);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
    }

    InputField m_msg;
    Text m_sure;
    float m_time;

    public override void Init()
    {
        m_time = -1;
        m_msg = m_tran.Find("InputField").GetComponent<InputField>();
        m_sure = m_tran.Find("send/Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("send").gameObject).onPointerClick += OnMsgSend;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate { this.HidePanel(); };
    }

    private void OnMsgSend(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.roleData.level < 20)
        {
            TipsManager.Instance.showTips("等级不足20级，无法招募学徒");
            return;
        }
        if (m_time != -1)
        {
            int time = 60 - (int)(Time.realtimeSinceStartup - m_time);
            TipsManager.Instance.showTips("请等待" + time.ToString() + "秒后再发送");
            return;
        }

        CHAT_CHANNEL eChannel = CHAT_CHANNEL.CC_CRSSERVER;
        long id = PlayerSystem.roleId;
        if (m_msg.text != "")
        {
            m_msg.text = "," + m_msg.text;
        }
        string msg = "招募学徒{0},<color=#8600FFFF><a=apply_trainee|{1}>我要申请</a></color>";
        NetChatData.SndChatInfo((int)eChannel, 0, string.Format(msg,m_msg.text,id));
        m_time = Time.realtimeSinceStartup;
        this.HidePanel();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnShow()
    {
        m_msg.text = "";
    }

    public override void Update()
    {
        if (m_time == -1)
        {
            m_sure.text = "发送";
        }
        else
        {
            int time = 60 - (int)(Time.realtimeSinceStartup - m_time);
            m_sure.text = "发送(" + time + ")";
        }
        if (Time.realtimeSinceStartup - m_time >= 60)
        {
            m_time = -1;
        }

    }
   



}


