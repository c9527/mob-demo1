﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class PanelStudentApply:BasePanel
{
    public class ItemStudentApply : ItemRender
    {
        Image m_headicon;
        Image m_imgState;
        Image m_armyImg;
        Text m_lvl;
        Text m_name;
        Text m_mvp;
        Text m_ace;
        long m_id;



        public override void Awake()
        {
            var tran = transform;
            m_headicon = tran.Find("Head/icon").GetComponent<Image>();
            m_armyImg = tran.Find("level/imgArmy").GetComponent<Image>();
            m_imgState = tran.Find("Head/State").GetComponent<Image>();
            m_lvl = tran.Find("level/Text").GetComponent<Text>();
            m_mvp = tran.Find("info/mvp/Text").GetComponent<Text>();
            m_name = tran.Find("info/name").GetComponent<Text>();
            m_ace = tran.Find("info/ace/Text").GetComponent<Text>();
            UGUIClickHandler.Get(tran.Find("refuse").gameObject).onPointerClick += refuse;
            UGUIClickHandler.Get(tran.Find("apply").gameObject).onPointerClick += apply;
        }

        void refuse(GameObject target, PointerEventData data)
        {
            NetLayer.Send(new tos_master_agree_apply() { id = m_id, agree = false});
        }

        void apply(GameObject target, PointerEventData data)
        {
            NetLayer.Send(new tos_master_agree_apply() { id = m_id, agree = true});
        }

        protected override void OnSetData(object data)
        {
            var info = data as p_master_base_info_toc;
            if (info == null)
            {
                return;
            }
            m_headicon.SetSprite(ResourceManager.LoadRoleIcon(info.icon));
            m_headicon.SetNativeSize();
            m_id = info.id;
            m_lvl.text = String.Format("{0}级", info.level);
            m_mvp.text = info.mvp.ToString();
            m_ace.text = (info.gold_ace + info.silver_ace).ToString();
            m_name.text = info.name;
            m_armyImg.SetSprite(ResourceManager.LoadArmyIcon(info.level));
            FRIEND_STATE state;
            if (info.handle == 0)
            {
                state = FRIEND_STATE.OFFLINE;
            }
            else
            {
                state = FRIEND_STATE.ONLINE;
            }
            if (info.room != 0)
            {
                state = FRIEND_STATE.INROOM;
            }

            if (info.team != 0)
            {
                state = FRIEND_STATE.INRANK;
            }

            if (info.fight)
            {
                state = FRIEND_STATE.INFIGHT;
            }
            SetStateImg(state);
        }

        public void SetStateImg(FRIEND_STATE m_friendState)
        {
            if (m_friendState == FRIEND_STATE.INROOM)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inroom"));
            }
            else if (m_friendState == FRIEND_STATE.INRANK)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inrank"));
            }
            else if (m_friendState == FRIEND_STATE.INFIGHT)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "fighting"));
            }
            else if (m_friendState == FRIEND_STATE.ONLINE)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "online"));
            }
            else if (m_friendState == FRIEND_STATE.OFFLINE)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "outline"));
            }
            m_imgState.SetNativeSize();
        }
    }


    Text m_stuNum;
    DataGrid m_applylist;
    public PanelStudentApply()
    {
        SetPanelPrefabPath("UI/Master/PanelStudentApply");
        AddPreLoadRes("UI/Master/PanelStudentApply", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);

    }

    public override void Init()
    {
        m_stuNum = m_tran.Find("Text").GetComponent<Text>();
        m_applylist = m_tran.Find("List/content").gameObject.AddComponent<DataGrid>();
        m_applylist.SetItemRender(m_tran.Find("List/content/ItemStudent").gameObject, typeof(ItemStudentApply));
        //m_applylist.Data = new object[20];
        m_applylist.Data = MasterGiftDataManager.singleton.m_applies.ToArray();
    }

    public void ChangeNum()
    {
        m_stuNum.text = "当前学徒数：" + UIManager.GetPanel<PanelSubMasterReward>().m_stuNum.ToString() + "/4";
    }
    

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate { this.HidePanel(); };
        AddEventListener("StudentNumChange", ChangeNum);
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnShow()
    {
        m_applylist.Data = MasterGiftDataManager.singleton.m_applies.ToArray();
        ChangeNum();
    }

    void Toc_master_apply_master_list(toc_master_apply_master_list data)
    {
        m_applylist.Data = data.applies;
        if (data.applies.Length == 0)
        {
            MasterGiftDataManager.singleton.m_hasApply = false;
//            UIManager.PopPanel<PanelSubMasterReward>().UpdateBubble();
        }
        else
        {
            MasterGiftDataManager.singleton.m_hasApply = true;
//            UIManager.PopPanel<PanelSubMasterReward>().UpdateBubble();
        }
        BubbleManager.SetBubble(BubbleConst.StudentApply, MasterGiftDataManager.singleton.m_hasApply);
        //BubbleManager.SetBubble(BubbleConst.ApprenticeGift, MasterGiftDataManager.singleton.m_hasApply || BubbleManager.HasBubble(BubbleConst.ApprenticeGift));
        
    }

    void Toc_master_agree_apply(toc_master_agree_apply data)
    {
        NetLayer.Send(new tos_master_apply_master_list());
        NetLayer.Send(new tos_master_apprentice_info());
        NetLayer.Send(new tos_master_master_welfare());
    }

    public override void Update()
    {
       
    }

   
}

