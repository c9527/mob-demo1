﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/11/5 17:26:21
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelMasterGrade : BasePanel
{
    /// <summary>
    /// 领取按钮
    /// </summary>
    private Transform m_btnDraw;
    /// <summary>
    /// 当前积分
    /// </summary>
    private Text m_curPointTxt;
    /// <summary>
    /// 进阶需要积分
    /// </summary>
    private Text m_nextPointTxt;
    /// <summary>
    /// 段位积分分界
    /// </summary>
    private Transform m_pointSpaceText;
    private Transform m_pointNameText;
    /// <summary>
    /// 我的段位图标
    /// </summary>
    private Image m_mineGradeIconImg;
    /// <summary>
    /// 我的段位名字
    /// </summary>
    private Image m_mineGradeNaemImg;

    /// <summary>
    /// 礼包图片
    /// </summary>
    private Image m_giftImg;
    /// <summary>
    /// 段位Id
    /// </summary>
    private int gradeId;
    /// <summary>
    /// 当前段位积分
    /// </summary>
    private int point;
    /// <summary>
    /// 是否已经领取
    /// </summary>
    private bool isDraw;

    private GameObject m_btnRule;

    private ConfigMasterRankGrade m_configMasterRankGrade;
    private Text m_todayPointTxt;//今日获得积分

    public PanelMasterGrade()
    {
        // 构造器
        SetPanelPrefabPath("UI/Master/PanelMasterGrade");
        AddPreLoadRes("UI/Master/PanelMasterGrade", EResType.UI);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
    }
    /// <summary>
    /// 初始化
    /// </summary>
    public override void Init()
    {
        m_btnDraw = m_tran.Find("leftInfoFrame/BtnDraw");
        //m_btnDraw.GetComponent<Button>().enabled = false;
        //Util.SetGrayShader(m_btnDraw);
        //Util.SetNormalShader(m_btnDraw);
        m_curPointTxt = m_tran.Find("minePointFrame/curPointTxt").GetComponent<Text>();
        m_nextPointTxt = m_tran.Find("minePointFrame/nextPointTxt").GetComponent<Text>();
        m_pointSpaceText = m_tran.Find("gradeInfoFrame/PointTxt");
        m_pointNameText = m_tran.Find("gradeInfoFrame/TextList");
        m_todayPointTxt = m_tran.Find("minePointFrame/todayPointTxt").GetComponent<Text>();
        m_mineGradeIconImg = m_tran.Find("leftInfoFrame/iconImg").GetComponent<Image>();
        m_mineGradeNaemImg = m_tran.Find("leftInfoFrame/nameImg").GetComponent<Image>();
        m_giftImg = m_tran.Find("leftInfoFrame/gifBox").GetComponent<Image>();
        m_configMasterRankGrade = ConfigManager.GetConfig<ConfigMasterRankGrade>();
        m_btnRule = m_tran.Find("rulebutton").gameObject;
        initPointRankGrade();
    }
    /// <summary>
    /// 设置按钮状态
    /// </summary>
    /// <param name="isNormal"></param>
    public void setDrawBtnStatus(bool isNormal, bool isNoGift = false)
    {
        isDraw = !isNormal;
        m_btnDraw.GetComponent<Button>().enabled = isNormal;
        Text text = m_btnDraw.FindChild("Text").GetComponent<Text>();
        if (isNormal)
        {
            Util.SetNormalShader(m_btnDraw);
            text.text = "领取";
        }
        else
        {
            Util.SetGrayShader(m_btnDraw);
            if (isNoGift)
            {
                text.text = "不能领取";
            }
            else
            {
                text.text = "已领取";
            }

        }
    }

    /// <summary>
    /// 显示界面信息
    /// </summary>
    private void showViewInfo(toc_master_grade proto)
    {
        point = proto.point;
        gradeId = proto.grade_id;
        ConfigMasterRankGradeLine config = m_configMasterRankGrade.GetLine(gradeId);
        if (config != null)
        {
            //m_nextPointTxt.text = config.Score - point < 0 ? "0" : config.Score - point + 1 + "分";
            if (gradeId == 6)
            {
                m_nextPointTxt.text = "已满阶";
            }
            else
            {
                m_nextPointTxt.text = (config.Score+1) + "分";
            }

        }
        m_curPointTxt.text = point + "分";
       
        if (gradeId == 1)
        {
            m_mineGradeIconImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "no"));
            m_mineGradeIconImg.SetNativeSize();
        }
        else
        {
            m_mineGradeIconImg.rectTransform.sizeDelta = new Vector2(103, 89);
            m_mineGradeIconImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "masGrade_icon" + gradeId));
        }
        m_mineGradeNaemImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "masGrade_name" + gradeId));
        string gifPath= "5_gift";
        if(gradeId>=3){
            gifPath=(gradeId -2)*5+"_gift";
        }
        m_giftImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "masGrade_name" + gradeId));
        m_giftImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, gifPath));
        //m_mineGradeIconImg.w
        if (gradeId == 1)
            setDrawBtnStatus(false, true);
        //ResourceManager.LoadSprite(AtlasName.Master, "master_guest");
    }

    /// <summary>
    /// 初始化显示阶段  从段位2开始显示  第一个段位不显示
    /// </summary>
    private void initPointRankGrade()
    {
        Transform tempTrans;
        Text tempText;
        int tempInt = 0;
        ConfigMasterRankGradeLine tempConfig;
        tempConfig = m_configMasterRankGrade.GetLine(1);
        tempInt = tempConfig.Score + 1;
        for (int i = 1; i <= 5; i++)
        {
            tempTrans = m_pointSpaceText.FindChild("pointTxt" + i);
            if (tempTrans != null)
            {
                tempText = tempTrans.GetComponent<Text>();
                if (tempText != null)
                {
                    tempConfig = m_configMasterRankGrade.GetLine(i + 1);
                    if (tempConfig != null)
                    {
                        tempText.text = tempInt + "-" + tempConfig.Score;
                        tempInt = tempConfig.Score + 1;
                        m_pointNameText.FindChild("text" + i).GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Master,"name" + tempConfig.Id));
                    }
                }
            }
        }
    }



    public override void Update()
    {
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnDraw).onPointerClick += OnClickBtnDraw;
        UGUIClickHandler.Get(m_btnRule).onPointerClick += onClickBtnRule;
    }

    private void onClickBtnRule(GameObject target, PointerEventData eventData)
    {
        UIManager.PopRulePanel("masterGrade");
    }

    private void OnClickBtnDraw(GameObject target, PointerEventData eventData)
    {
        /* ConfigMasterRankGradeLine config = m_configMasterRankGrade.GetLine(gradeId);
         if (config == null)
         {
             return;
         }*/
        // int itemId = config.Gift;
        if (m_btnDraw.GetComponent<Button>().enabled)
            NetLayer.Send(new tos_master_grade_drawgif() { item_id = gradeId });
    }

    /// <summary>
    /// 礼包领取成功返回
    /// </summary>
    /// <param name="proto"></param>
    private void Toc_master_grade_drawgif(toc_master_grade_drawgif proto)
    {
        //proto.result
        setDrawBtnStatus(false);

        //弹窗显示礼包信息
        //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(null, true, null);
        ConfigMasterRankGradeLine tempConfig = m_configMasterRankGrade.GetLine(gradeId);
        var config2 = ConfigManager.GetConfig<ConfigGiftBag>();
        ConfigGiftBagLine giftLine = config2.GetLine(tempConfig.Gift);
        //panelItemTip.SetRewardItemList(giftLine.GiftList);
        PanelRewardItemTipsWithEffect.DoShow(giftLine.GiftList);
        BubbleManager.SetBubble(BubbleConst.MasterGrade, false);
    }


    public override void OnShow()
    {
        //初始界面显示
        toc_master_grade proto = new toc_master_grade();
        proto.grade_id = 1;
        proto.point = 0;
        showViewInfo(proto);
        NetLayer.Send(new tos_master_grade());
         m_score = ConfigMisc.GetInt("master_score_daily_max");
        
    }

    private int m_score;
    /// <summary>
    /// 今日获取积分
    /// </summary>
    private void updateTodayPoint(int point)
    {
        m_todayPointTxt.text = point + "/" + m_score; 
    }

    private void Toc_master_grade(toc_master_grade proto)
    {
        showViewInfo(proto);
        updateTodayPoint(proto.todayPoint);
        NetLayer.Send(new tos_master_grade_isdraw() { grade_id = proto.grade_id});
    }

    /// <summary>
    /// 是否领取返回
    /// </summary>
    /// <param name="proto"></param>
    private void Toc_master_grade_isdraw(toc_master_grade_isdraw proto)
    {
        if (gradeId == 1)
            setDrawBtnStatus(false, true);
        else
            setDrawBtnStatus(proto.isdraw != 1);
    }


    public override void OnHide()
    {

    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {

    }
}
