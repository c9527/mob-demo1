﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PanelRecoMaster:BasePanel
{

    public class ItemMasterApply:ItemRender
    {
        Image m_headicon;
        Image m_imgState;
        Image m_armyImg;
        Text m_lvl;
        Text m_name;
        Text m_mvp;
        Text m_ace;
        public long m_id;
        GameObject m_apply;
        GameObject m_text;
        int m_goldNum;
        
        public bool m_hasapply
        {
            set
            {
                m_apply.TrySetActive(!value);
            }
        }
        public override void Awake()
        {
            var tran = transform;
            m_headicon = tran.Find("Head/icon").GetComponent<Image>();
            m_armyImg = tran.Find("level/imgArmy").GetComponent<Image>();
            m_imgState = tran.Find("Head/State").GetComponent<Image>();
            m_lvl = tran.Find("level/Text").GetComponent<Text>();
            m_mvp = tran.Find("info/mvp/Text").GetComponent<Text>();
            m_name = tran.Find("info/name").GetComponent<Text>();
            m_ace = tran.Find("info/ace/Text").GetComponent<Text>();
            m_apply = tran.Find("apply").gameObject;
            UGUIClickHandler.Get(tran.Find("apply")).onPointerClick += SendApply;
            m_text = tran.Find("Text").gameObject;
            m_hasapply = false;
        }

        public void changeView()
        {
            m_apply.TrySetActive(false);
            m_text.TrySetActive(true);
        }

        void SendApply(GameObject target, PointerEventData data)
        {
            PanelRecoMaster.m_index = m_id;
            NetLayer.Send(new tos_master_add_master() { id = m_id });
        }



        protected override void OnSetData(object data)
        {
            var info = data as p_master_base_info_toc;
            m_apply.TrySetActive(true);
            m_text.TrySetActive(false);
            if (info == null)
            {
                return;
            }
            m_headicon.SetSprite(ResourceManager.LoadRoleIcon(info.icon));
            m_headicon.SetNativeSize();
            m_armyImg.SetSprite(ResourceManager.LoadArmyIcon(info.level));
            m_id = info.id;
            m_mvp.text = info.mvp.ToString();
            m_ace.text = (info.silver_ace + info.gold_ace).ToString();
            m_name.text = info.name;
            m_lvl.text = String.Format("{0}级", info.level);
            FRIEND_STATE state;
            m_apply.TrySetActive(true);
            if (info.handle == 0)
            {
                state = FRIEND_STATE.OFFLINE;
            }
            else
            {
                state = FRIEND_STATE.ONLINE;
            }
            if (info.room != 0)
            {
                state = FRIEND_STATE.INROOM;
            }
            if (info.team != 0)
            {
                state = FRIEND_STATE.INRANK;
            }

            if (info.fight)
            {
                state = FRIEND_STATE.INFIGHT;
            }
            SetStateImg(state);
        }

        public void SetStateImg(FRIEND_STATE m_friendState)
        {
            if (m_friendState == FRIEND_STATE.INROOM)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inroom"));
            }
            else if (m_friendState == FRIEND_STATE.INRANK)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inrank"));
            }
            else if (m_friendState == FRIEND_STATE.INFIGHT)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "fighting"));
            }
            else if (m_friendState == FRIEND_STATE.ONLINE)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "online"));
            }
            else if (m_friendState == FRIEND_STATE.OFFLINE)
            {
                m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "outline"));
            }
            m_imgState.SetNativeSize();
        }
    }

    Text m_time;
    DataGrid m_masterlist;
    public static long m_index;
    float m_starttime;
    public PanelRecoMaster()
    {
        SetPanelPrefabPath("UI/Master/PanelRecoMaster");
        AddPreLoadRes("UI/Master/PanelRecoMaster", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
        
    }

    public override void Init()
    {
        m_starttime = Time.realtimeSinceStartup;
        m_time = m_tran.Find("Text").GetComponent<Text>();
        m_masterlist = m_tran.Find("List/content").gameObject.AddComponent<DataGrid>();
        m_masterlist.SetItemRender(m_tran.Find("List/content/ItemStudent").gameObject, typeof(ItemMasterApply));
        m_masterlist.Data = MasterGiftDataManager.singleton.m_applies.ToArray();
        NetLayer.Send(new tos_master_rcmnd_master_list());
        //m_masterlist.Data = new object[20];
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {
       
    }

    public override void OnDestroy()
    {
  
    }

    public override void OnHide()
    {
         
    }

    public override void OnShow()
    {
        
    }

    void Toc_master_rcmnd_master_list(toc_master_rcmnd_master_list data)
    {
        var list = data.recommends;
        m_masterlist.Data = list;
    }

    void Toc_master_add_master(CDict data)
    {
        changeView();
    }

    public override void Update()
    {
        if (Time.realtimeSinceStartup - m_starttime > 300)
        {
            NetLayer.Send(new tos_master_rcmnd_master_list());
            m_starttime = Time.realtimeSinceStartup;
        }
        int time = (int)(300 - (Time.realtimeSinceStartup - m_starttime));
        int minute = time / 60;
        int second = time % 60;
        m_time.text = minute + ":" + second + "后刷新";
    }

    void changeView()
    {
        for (int i = 0; i < m_masterlist.Data.Length; i++)
        {
            var info = m_masterlist.ItemRenders[i] as ItemMasterApply;
            if (info.m_id == m_index)
            {
                info.changeView();
            }
        }
    }



}

