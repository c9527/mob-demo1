﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/11/18 10:20:39
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelMasterPutGun : BasePanel
{

    public class ItemPutGun : ItemRender
    {
        /// <summary>
        /// 枪的数据信息
        /// </summary>
        private p_item m_data;

        private Text m_txtName;
        private Image m_imgIcon;
        private GameObject m_hasPut;
        private GameObject m_btnPut;
        private Image m_qualityBg;
        //private float m_tick = 0;
        //private int restTime = 0;
        public override void Awake()
        {
            m_txtName = transform.Find("txtName").GetComponent<Text>();
            m_imgIcon = transform.Find("WeaponIcon").GetComponent<Image>();
            m_qualityBg = transform.gameObject.GetComponent<Image>();
            m_hasPut = transform.Find("hasPut").gameObject;
            m_btnPut = transform.Find("btnPut").gameObject;
            UGUIClickHandler.Get(m_btnPut).onPointerClick += onClickPut;
        }

        protected override void OnSetData(object data)
        {
            m_data = data as p_item;
            var item_vo = new CDItemData(m_data);
            if (item_vo.info == null)
            {
                return;
            }
            //var color_value = item_vo.info.RareType;
            //if (color_value == 0)
            //{
            //    color_value = item_vo.GetQuality();
            //}
            if (!string.IsNullOrEmpty(item_vo.info.Icon))
            {
                m_imgIcon.SetSprite(ResourceManager.LoadIcon(item_vo.info.Icon),item_vo.info.SubType);
                m_imgIcon.SetNativeSize();
                m_imgIcon.enabled = true;
            }
            else
            {
                m_imgIcon.enabled = false;
            }
            ConfigItemWeaponLine cfg = ItemDataManager.GetItem(m_data.item_id) as ConfigItemWeaponLine;
            var colorValue = 0;
            if (cfg != null)
            {
                m_txtName.text = cfg.DispName;
                colorValue = cfg.RareType;
            }
            m_qualityBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            if (hasWeaponPutted(m_data.uid))
            {
                m_btnPut.TrySetActive(false);
                m_hasPut.TrySetActive(true);
            }
            else
            {
                m_btnPut.TrySetActive(true);
                m_hasPut.TrySetActive(false);
            }
        }

        /*void Update()
        {
            m_tick += Time.deltaTime;
            if (m_tick > 1)
            {
                m_tick = 0;
                if (restTime == 0)
                {
                    //更新界面
                }
                else if (restTime > 0)
                {
                    restTime--;

                }
            }
        }*/

        private void onClickPut(GameObject target, PointerEventData eventData)
        {
            UIManager.GetPanel<PanelMasterPutGun>().HidePanel();
            NetLayer.Send(new tos_master_put_weapon_to_arsenal() { uid = m_data.uid});
        }
    }

    private GameObject m_btnClose;
    private GameObject m_btnTabAll;
    private GameObject m_btnTabMain;
    private GameObject m_btnTabSub;

    private const string TABBTN_ALL = "BtnTabAll";
    private const string TABBTN_MAIN = "BtnTabMain";
    private const string TABBTN_SUB = "BtnTabSub";

    private string m_curTabType = "BtnTabAll";

    /// <summary>
    /// 武器列表
    /// </summary>
    private DataGrid m_weaponList;

    /// <summary>
    /// 已放入师徒库的武器
    /// </summary>
    static int[] m_mineWeaponIdAry;

    public PanelMasterPutGun()
    {
        // 构造器
        SetPanelPrefabPath("UI/Master/PanelMasterPutGun");
        AddPreLoadRes("UI/Master/PanelMasterPutGun", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
    }

    public override void Init()
    {
        m_weaponList = m_tran.Find("List/content").gameObject.AddComponent<DataGrid>();
        m_weaponList.SetItemRender(m_tran.Find("List/content/ItemWeapon").gameObject, typeof(ItemPutGun));
        m_btnClose = m_tran.Find("close").gameObject;
        m_btnTabAll = m_tran.Find("TabBtn/BtnTabAll").gameObject;
        m_btnTabMain = m_tran.Find("TabBtn/BtnTabMain").gameObject;
        m_btnTabSub = m_tran.Find("TabBtn/BtnTabSub").gameObject;

        // m_weaponList.Data = new object[4]; 
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnClose;
        UGUIClickHandler.Get(m_btnTabAll).onPointerClick += onClickTabBtn;
        UGUIClickHandler.Get(m_btnTabMain).onPointerClick += onClickTabBtn;
        UGUIClickHandler.Get(m_btnTabSub).onPointerClick += onClickTabBtn;
    }
    /// <summary>
    /// 标签按钮选择控制
    /// </summary>
    /// <param name="tabType"></param>
    private void tabBtnSelectControl(string tabType)
    {
        m_curTabType = tabType;
        int type = 0;
        switch (m_curTabType)
        {
            case TABBTN_MAIN:
                type = 1;
                break;
            case TABBTN_SUB:
                type = 2;
                break;
            default:
                type = 0;
                break;
        }
        m_weaponList.Data = ItemDataManager.GetMasterGunItemByType(type);
    }

    private void onClickBtnClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void onClickTabBtn(GameObject target, PointerEventData eventData)
    {
        if (m_curTabType != target.name)
        {
            tabBtnSelectControl(target.name);
        }
    }

    public override void OnShow()
    {
        if(m_params!=null){
            m_mineWeaponIdAry = m_params[0] as int[];
        }
        tabBtnSelectControl(m_curTabType);
    }
    /// <summary>
    /// 判断是否已经被放入师徒武器库
    /// </summary>
    /// <param name="id">道具uid</param>
    /// <returns></returns>
    public static bool hasWeaponPutted(int id)
    {
        int lngth = m_mineWeaponIdAry.Length;
        if (lngth == 0) return false;
        for (int i = 0; i < lngth; i++)
        {
            if (m_mineWeaponIdAry[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }
    public override void Update()
    {

    }
}
