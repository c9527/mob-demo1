﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

class PanelSubMasterReward : BasePanel
{
    /// <summary>
    /// 徒弟列表数据项
    /// </summary>
    public class ItemStudent : ItemRender
    {
        Text m_name;
        Text m_lvl;
        Text m_goldnum;
        GameObject m_goldicon;
        GameObject m_giftbox;
        Image m_headicon;
        public long m_id;
        GameObject m_noStu;
        GameObject m_hasStu;
        Text m_getInfo;
        public int m_giftid;
        int m_level;
        public int m_gold;
        p_master_base_info_toc m_data;
        Image m_box;
        Text double_num;
        Image m_online;
        public override void Awake()
        {
            m_giftid = 0;
            m_gold = 0;
            var tran = transform;
            m_online = tran.Find("hasStu/online").GetComponent<Image>();
            m_name = tran.Find("hasStu/name").GetComponent<Text>();
            m_lvl = tran.Find("hasStu/lvl").GetComponent<Text>();
            m_goldnum = tran.Find("hasStu/icon/num").GetComponent<Text>();
            m_giftbox = tran.Find("hasStu/gift").gameObject;
            m_goldicon = tran.Find("hasStu/icon").gameObject;
            m_headicon = tran.Find("hasStu/head").GetComponent<Image>();
            m_getInfo = tran.Find("hasStu/getText").GetComponent<Text>();
            m_noStu = tran.Find("noStu").gameObject;
            m_hasStu = tran.Find("hasStu").gameObject;
            m_box = m_giftbox.GetComponent<Image>();
            double_num = tran.Find("hasStu/num").GetComponent<Text>();
            GameObject temp1 = tran.Find("hasStu/kick").gameObject;
            GameObject temp2 = tran.Find("hasStu/get").gameObject;
            UGUIClickHandler.Get(tran.Find("hasStu/kick").gameObject).onPointerClick += OnKickStudentClick;
            UGUIClickHandler.Get(tran.Find("hasStu/get").gameObject).onPointerClick += OnGetGiftClick;
        }

        protected override void OnSetData(object data)
        {
            var info = data as p_master_base_info_toc;
            m_data = info;
            if (info == null)
            {
                m_noStu.TrySetActive(true);
                m_id = 0;
                m_level = 0;
                m_name.text = "";
                m_lvl.text = "";
                m_headicon.gameObject.TrySetActive(false);
                m_hasStu.TrySetActive(false);
                transform.GetComponent<Toggle>().isOn = false;
                return;
            }
            else
            {
                m_headicon.gameObject.TrySetActive(true);
                m_noStu.TrySetActive(false);
                m_hasStu.TrySetActive(true);
            }
            m_id = info.id;
            m_level = info.level;
            m_name.text = info.name;
            m_lvl.text = String.Format("{0}级",  info.level);
            m_headicon.SetSprite(ResourceManager.LoadRoleIconBig(info.icon));
            m_headicon.SetNativeSize();
            m_headicon.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
            double_num.text = "0";
            for (int i = 0; i < PlayerFriendData.singleton.m_double.Count; i++)
            {
                if (PlayerFriendData.singleton.m_double[i].id == info.id)
                {
                    double_num.text = PlayerFriendData.singleton.m_double[i].rest_num.ToString();
                }
            }
            string onlineName = info.handle != 0 ? "online" : "offline";
            m_online.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, onlineName));
        }

        public void OnKickStudentClick(GameObject target, PointerEventData eventData)
        {
            UIManager.PopPanel<PanelBattleKickReason>(new object[] { m_data, false }, true);
        }

        public void OnGetGiftClick(GameObject target, PointerEventData eventData)
        {
            bool gold = true;
            if (m_gold > 0)
            {
                gold = true;
            }
            else
            {
                gold = false;
            }
            if (gold)
            {
                NetLayer.Send(new tos_master_get_master_welfare() { player_id = m_id, welfare_id = 0, is_gold = gold });
            }
            else
            {
                NetLayer.Send(new tos_master_get_master_welfare() { player_id = m_id, welfare_id = m_giftid, is_gold = gold });
            }
            /*todo
             * 收取礼物
             * */
        }
        public void CheckHasGift(bool hasgift)
        {
            if (hasgift)
            {

                m_giftbox.TrySetActive(true);
                m_box.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, (m_giftid * 5).ToString() + "_gift"));
                m_goldicon.TrySetActive(false);
                transform.Find("hasStu/get").gameObject.TrySetActive(true);
                m_getInfo.gameObject.TrySetActive(false);
            }
            else
            {
                m_giftbox.TrySetActive(false);
                m_goldicon.TrySetActive(true);
                m_goldnum.text = m_gold.ToString();
                transform.Find("hasStu/get").gameObject.TrySetActive(true);
                m_getInfo.gameObject.TrySetActive(false);
            }
        }

        public void NoGift()
        {
            m_giftbox.TrySetActive(false);
            m_goldicon.TrySetActive(false);
            transform.Find("hasStu/get").gameObject.TrySetActive(false);
            m_getInfo.gameObject.TrySetActive(true);
            int n = m_level / 5 + 1;
            if (n < 6)
            {
                m_getInfo.text = "无礼包可领";
            }
            else
            {
                m_getInfo.text = "无礼包可领";
            }
        }
    }

    /// <summary>
    /// 徒弟看到的武器库  数据项
    /// </summary>
    public class ItemWeapon : ItemRender
    {
        GameObject m_hasWeapon;
        GameObject m_noWeapon;
        Image m_imgIcon;
        Text m_txtName;
        Text m_txtTime;
        GameObject m_useInfo;
        Text m_txtUseName;
        GameObject m_btnGet;
        weapon_attr m_data;
        bool m_isBorrow;
        private float m_tick = 0;
        private int restTime = 0;

        private ConfigItemLine m_itemCfg;

        public override void Awake()
        {
            m_hasWeapon = transform.Find("hasWeapon").gameObject;
            m_noWeapon = transform.Find("noWeapon").gameObject;
            m_imgIcon = m_hasWeapon.transform.Find("weaponIcon").gameObject.GetComponent<Image>();
            m_txtName = m_hasWeapon.transform.Find("name").gameObject.GetComponent<Text>();
            m_txtTime = m_hasWeapon.transform.Find("timeText").gameObject.GetComponent<Text>();
            m_useInfo = m_hasWeapon.transform.Find("userInfoText").gameObject;
            m_txtUseName = m_useInfo.transform.Find("nameText").gameObject.GetComponent<Text>();
            m_btnGet = m_hasWeapon.transform.Find("BtnGet").gameObject;
            UGUIClickHandler.Get(m_btnGet).onPointerClick += onClickBtnGet;
            MiniTipsManager.get(m_imgIcon.gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg == null) return null;
                return new MiniTipsData() { name=m_itemCfg.DispName,dec=m_itemCfg.Desc};
            };
        }

        private void onClickBtnGet(GameObject target, PointerEventData eventData)
        {
            if (PanelSubMasterReward.isInKickMasterCD())
            {
                TipsManager.Instance.showTips("解除师徒关系后24小时内暂时无法使用");
                return;
            }

            if (m_isBorrow)
            {
                TipsManager.Instance.showTips("您已经借用一把导师武器");
                return;
            }
            NetLayer.Send(new tos_master_borrow_weapon() { id = m_data.id,uid = m_data.uid});
        }

        void Update()
        {
            if (m_data == null)
            {
                return;
            }
            m_tick += Time.deltaTime;
            if (m_tick > 1)
            {
                m_tick = 0;
                if (restTime == 0)
                {
                    //更新界面
                    NetLayer.Send(new tos_master_arsenal() { is_master = true });
                    restTime = -1;
                }
                else if (restTime > 0)
                {
                    restTime--;
                    m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
                }
            }
        }

        public void showHasBrrow(bool isBorrow)
        {
            if (PanelSubMasterReward.isInKickMasterCD()) return;
            m_isBorrow = isBorrow;
            Util.SetGoGrayShader(m_btnGet, isBorrow);
        }

        protected override void OnSetData(object data)
        {
            m_data = data as weapon_attr;
            if (data == null)
            {
                m_hasWeapon.TrySetActive(false);
                m_noWeapon.TrySetActive(true);
            }
            else
            {
                m_hasWeapon.TrySetActive(true);
                m_noWeapon.TrySetActive(false);
                ConfigItemWeaponLine cfg = ItemDataManager.GetItem(m_data.id) as ConfigItemWeaponLine;
                m_itemCfg = cfg;
                m_imgIcon.SetSprite(ResourceManager.LoadIcon(cfg.Icon),cfg.SubType);
                TimeSpan timeSpan = TimeUtil.GetRemainTime((uint)m_data.expire_time);
                restTime = (int)timeSpan.TotalSeconds;
                m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
                m_imgIcon.SetNativeSize();
                m_imgIcon.enabled = true;
                m_txtName.text = cfg.DispName;
                m_txtUseName.text = m_data.apprentice_name;
                m_txtTime.gameObject.TrySetActive(true);
                if (m_data.status == 1)
                {
                    m_useInfo.TrySetActive(true);
                    m_btnGet.TrySetActive(false);
                    if (PlayerSystem.roleData.name == m_data.apprentice_name)
                    {
                        m_txtTime.gameObject.TrySetActive(false);
                    }
                }
                else
                {
                    if (PanelSubMasterReward.isInKickMasterCD())
                    {
                        Util.SetGoGrayShader(m_btnGet, true);
                    }
                    else
                    {
                        Util.SetGoGrayShader(m_btnGet, false);

                    }
                    if (m_data.is_borrowed)
                    {
                        //借过了 不显示借用按钮
                        m_useInfo.TrySetActive(false);
                        m_btnGet.TrySetActive(false);
                    }
                    else
                    {
                        m_useInfo.TrySetActive(false);
                        m_btnGet.TrySetActive(true);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 我的武器库  数据项
    /// </summary>
    public class MyItemWeapon : ItemRender
    {
        GameObject m_hasWeapon;
        GameObject m_noWeapon;
        Image m_imgIcon;
        Text m_txtName;
        Text m_userName;
        Text m_txtTime;
        Image m_stateImg;
        weapon_attr m_data;
        private float m_tick = 0;
        private int restTime = 0;
        
        private ConfigItemLine m_itemCfg;

        public override void Awake()
        {
            m_hasWeapon = transform.Find("hasWeapon").gameObject;
            m_noWeapon = transform.Find("noWeapon").gameObject;
            m_imgIcon = m_hasWeapon.transform.Find("weaponIcon").gameObject.GetComponent<Image>();
            m_txtName = m_hasWeapon.transform.Find("name").gameObject.GetComponent<Text>();
            m_txtTime = m_hasWeapon.transform.Find("timeText/time").gameObject.GetComponent<Text>();
            m_userName = m_hasWeapon.transform.Find("timeText").gameObject.GetComponent<Text>();
            m_stateImg = m_hasWeapon.transform.Find("waitIcon").gameObject.GetComponent<Image>();

            UGUIClickHandler.Get(transform.Find("noWeapon/BtnPutWeapon").gameObject).onPointerClick += onClickBtnPut;
            MiniTipsManager.get(m_imgIcon.gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg == null) return null;
                return new MiniTipsData() { name = m_itemCfg.DispName, dec = m_itemCfg.Desc };
            };
        }

        private void onClickBtnPut(GameObject target, PointerEventData eventData)
        {
            GameDispatcher.Dispatch(GameEvent.MASTER_REWARD_PUT_WEAPON);
        }

        void Update()
        {
            if (restTime != -1)
            {
                m_tick += Time.deltaTime;
                if (m_tick > 1)
                {
                    m_tick = 0;
                    restTime--;
                    m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
                }
                else if (restTime == 0)
                {
                    NetLayer.Send(new tos_master_arsenal() { is_master = false });
                }
            }
        }


        protected override void OnSetData(object data)
        {
            m_data = data as weapon_attr;
            if (data == null)
            {
                m_hasWeapon.TrySetActive(false);
                m_noWeapon.TrySetActive(true);
            }
            else
            {
                m_hasWeapon.TrySetActive(true);
                m_noWeapon.TrySetActive(false);
                restTime = (int)(TimeUtil.GetRemainTime((uint)m_data.expire_time).TotalSeconds);
                ConfigItemWeaponLine cfg = ItemDataManager.GetItem(m_data.id) as ConfigItemWeaponLine;
                m_itemCfg = cfg;
                m_imgIcon.SetSprite(ResourceManager.LoadIcon(cfg.Icon), cfg.SubType);

                if (m_data.status == 1)
                {
                    m_userName.text = "" + m_data.apprentice_name;
                    m_stateImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "borrowed"));
                }
                else
                {
                    m_userName.text = "";
                    m_stateImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "waitIcon"));
                }
                m_stateImg.SetNativeSize();
                m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
                m_txtName.text = m_itemCfg.DispName;
            }
        }
    }


    public int m_stuNum;
    GameObject m_viewMaster;
    GameObject m_viewStudent;
    GameObject m_noMaster;
    GameObject m_hasMaster;
    InputField m_masterId;
    Image m_box;
    Image m_head;
    Text m_name;
    Text m_lvl;
    DataGrid m_studentlist;
    Text m_getInfo;
    long m_mid;
    GameObject m_applybubble;
    GameObject m_masterBubble;
    GameObject m_masterAppBubble;
    GameObject m_masterGradeBubble;
    int m_giftid;
    ConfigMasterWelfareLine m_cmwl;
    ConfigMasterWelfare m_cmw;
    p_master_base_info_toc m_data;
    GameObject m_btnfindStu;
    GameObject m_btnApply;
    Text m_num;

    const string TABBTN_MASTER = "BtnTabMaster";
    const string TABBTN_APPRENTICE = "BtnTabApprentice";
    const string TABBTN_MASTERGRADE = "BtnTabMasterGrade";
    const string TABBTN_MASTERTREE = "BtnTabMasterTree";
    /// <summary>
    /// 当前选择的标签
    /// </summary>
    string m_curTabType = TABBTN_MASTER;

    GameObject m_btnTabMaster;
    GameObject m_btnTabApprentice;
    GameObject m_btnTabMasterGrade;
    GameObject m_btnTabMasterTree;
    /// <summary>
    /// 导师界面
    /// </summary>
    GameObject m_panelMaster;
    /// <summary>
    /// 徒弟界面
    /// </summary>
    GameObject m_panelStudent;
    /// <summary>
    /// 好友交互按钮列表
    /// </summary>
    GameObject m_tabBtn;

    /// <summary>
    /// 导师武器设置当前显示的页
    /// </summary>
    int m_curWeaponPage = 0;
    const int MAX_WEAPONPAGE = 4;
    /// <summary>
    /// 徒弟看到的师傅的武器列表
    /// </summary>
    DataGrid m_weaponDataGrid;
    //我的武器库
    DataGrid m_myWeaponDataGrid;
    /// <summary>
    /// 导师武器库
    /// </summary>
    weapon_attr[] m_masterWeaponAry = new weapon_attr[4];

    /// <summary>
    /// 我的武器库
    /// </summary>
    weapon_attr[] m_mineWeaponAry = new weapon_attr[4];

    private float m_tick = 0;
    private int restTime = 0;

    //GameObject m_goChat;
    //GameObject m_goLookOver;
    //GameObject m_goFollow;
    /// <summary>
    /// 标志是否有徒弟
    /// </summary>
    bool hasStudent = false;
    /// <summary>
    /// 导师数据
    /// </summary>
    p_master_base_info_toc m_masterData;

    /// <summary>
    /// 上次踢师傅的时间
    /// </summary>
    public static int m_kickMasterTime;
    private ConfigItemLine m_itemCfg;
    private ItemDropInfo[] m_dropArray;
    private GameObject m_goPopListPanel;
    public PanelSubMasterReward()
    {
        SetPanelPrefabPath("UI/Master/PanelSubMasterReward");
        AddPreLoadRes("UI/Master/PanelSubMasterReward", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        //AddPreLoadRes("Atlas/Activity", EResType.Atlas);
        AddPreLoadRes("Atlas/Master", EResType.Atlas);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
    }


    public override void Init()
    {
        m_cmw = ConfigManager.GetConfig<ConfigMasterWelfare>();
        m_stuNum = 0;
        m_viewMaster = m_tran.Find("master").gameObject;
        m_viewStudent = m_tran.Find("student").gameObject;
        m_noMaster = m_tran.Find("master/NoMaster").gameObject;
        m_hasMaster = m_tran.Find("master/HasMaster").gameObject;
        m_masterId = m_tran.Find("master/NoMaster/MasterId").GetComponent<InputField>();
        m_masterId.gameObject.AddComponent<InputFieldFix>();
        m_head = m_tran.Find("master/HasMaster/head").GetComponent<Image>();
        m_name = m_tran.Find("master/HasMaster/namebg/Text").GetComponent<Text>();
        m_lvl = m_tran.Find("master/HasMaster/lvl").GetComponent<Text>();
        m_getInfo = m_tran.Find("master/HasMaster/getText").GetComponent<Text>();
        m_studentlist = m_tran.Find("master/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_studentlist.SetItemRender(m_tran.Find("master/ScrollDataGrid/content/ItemStudent").gameObject, typeof(ItemStudent));
        m_applybubble = m_tran.Find("master/studentapply/bubble").gameObject;
        m_masterAppBubble = m_tran.Find("BtnFrame/TabBtn/BtnTabApprentice/Bubble").gameObject;
        m_masterBubble = m_tran.Find("BtnFrame/TabBtn/BtnTabMaster/Bubble").gameObject;
        m_masterGradeBubble = m_tran.Find("BtnFrame/TabBtn/BtnTabMasterGrade/Bubble").gameObject;
        m_btnfindStu = m_tran.Find("master/findstudent").gameObject;
        m_btnApply = m_tran.Find("master/studentapply").gameObject;
        m_box = m_tran.Find("master/HasMaster/gift").GetComponent<Image>(); ;
        m_num = m_tran.Find("master/HasMaster/num").GetComponent<Text>();
        m_studentlist.Data = new object[4];
        m_studentlist.autoSelectFirst = false;
        m_hasMaster.TrySetActive(false);
        m_noMaster.TrySetActive(true);

        m_btnTabMaster = m_tran.Find("BtnFrame/TabBtn/BtnTabMaster").gameObject;
        m_btnTabApprentice = m_tran.Find("BtnFrame/TabBtn/BtnTabApprentice").gameObject;
        m_btnTabMasterGrade = m_tran.Find("BtnFrame/TabBtn/BtnTabMasterGrade").gameObject;
        m_btnTabMasterTree = m_tran.Find("BtnFrame/TabBtn/BtnTabMasterTree").gameObject;
        m_btnTabMasterTree.TrySetActive(false);
        m_panelMaster = m_tran.Find("master").gameObject;
        m_panelStudent = m_tran.Find("student").gameObject;
        m_tabBtn = m_tran.Find("BtnFrame/RightBtn").gameObject;
        m_tabBtn.TrySetActive(false);
        m_weaponDataGrid = m_tran.Find("student/WeaponList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_weaponDataGrid.SetItemRender(m_tran.Find("student/WeaponList/ScrollDataGrid/content/ItemWeapon").gameObject, typeof(ItemWeapon));
        m_weaponDataGrid.Data = new object[4];

        m_myWeaponDataGrid = m_tran.Find("student/WeaponInfo/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_myWeaponDataGrid.SetItemRender(m_tran.Find("student/WeaponInfo/ScrollDataGrid/content/ItemWeapon").gameObject, typeof(MyItemWeapon));
        m_myWeaponDataGrid.Data = new object[4];

        m_studentlist.onItemSelected += onSelectStudentItem;
        //masterWeaponSetPageControl(0);
        //m_goChat = m_tran.Find("BtnFrame/RightBtn/buttonlist/chat").gameObject;
        // m_goFollow = m_tran.Find("BtnFrame/RightBtn/buttonlist/follow").gameObject;
        // m_goLookOver = m_tran.Find("BtnFrame/RightBtn/buttonlist/lookover").gameObject;
        m_dropArray = new[]
        {
            new ItemDropInfo { type = "STUDENT", subtype = "LOOK", label = "查看" }, 
            new ItemDropInfo { type = "STUDENT", subtype = "FOLLOW", label = "跟随" }, 
            new ItemDropInfo { type = "STUDENT", subtype = "TALK", label = "私聊" },
        };
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelSocity>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("master/NoMaster/sure").gameObject).onPointerClick += onClickFindMaster;
        UGUIClickHandler.Get(m_tran.Find("master/NoMaster/recommand").gameObject).onPointerClick += onClickRecommand;
        UGUIClickHandler.Get(m_tran.Find("master/HasMaster/kick").gameObject).onPointerClick += onClickKickMaster;
        UGUIClickHandler.Get(m_tran.Find("master/HasMaster/Button").gameObject).onPointerClick += onClickGetMasterGift;
        UGUIClickHandler.Get(m_tran.Find("master/studentapply").gameObject).onPointerClick += onClickStudentApply;
        UGUIClickHandler.Get(m_tran.Find("master/findstudent").gameObject).onPointerClick += onClickFindStudent;
        UGUIClickHandler.Get(m_tran.Find("rulebutton")).onPointerClick += delegate { UIManager.PopRulePanel("Master"); };
        UGUIClickHandler.Get(m_tran.Find("master/HasMaster/clickarea")).onPointerClick += onClickMasterHead;
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);

        UGUIClickHandler.Get(m_btnTabMaster).onPointerClick += onClickTabBtn;
        UGUIClickHandler.Get(m_btnTabApprentice).onPointerClick += onClickTabBtn;
        UGUIClickHandler.Get(m_btnTabMasterGrade).onPointerClick += onClickTabBtn;
        UGUIClickHandler.Get(m_btnTabMasterTree).onPointerClick += null;
        
        // m_studentlist.onItemSelected += onSelectStudentItem;
       // UGUIClickHandler.Get(m_goChat).onPointerClick += OnClickChat;
        //UGUIClickHandler.Get(m_goLookOver).onPointerClick += OnClickLookOver;
        //UGUIClickHandler.Get(m_goFollow).onPointerClick += OnClickFollow;

        UGUIClickHandler.Get(m_goPopListPanel).onPointerClick += delegate { isClickMaster = false; m_goPopListPanel.TrySetActive(false); UIManager.HideDropList(); }; ;
        AddEventListener(GameEvent.MASTER_REWARD_PUT_WEAPON, putGun);
    }





    void OnClickChat(GameObject target, PointerEventData eventData)
    {
        p_master_base_info_toc data = new p_master_base_info_toc();
        if (m_curTabType == TABBTN_MASTER)
        {
            if( isClickMaster == false )
            {
                data = m_studentlist.SelectedData<p_master_base_info_toc>();
                if (data == null)
                {
                    return;
                }
            }
            else
            {
                if (m_masterData == null)
                {
                    return;
                }
                data = m_masterData;
            }
        }
        //bool isFriend = true;//m_listFriend.IndexOf(m_curSelected) > -1 ? true : false;
        //if (m_curSelected.m_state == FRIEND_STATE.OFFLINE)
        //{
        //    UIManager.ShowTipPanel("对方不在线");
        //     return;
        // }
        bool isFriend = PlayerFriendData.singleton.isFriend(data.id);
        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = data.id;
        rcd.m_strName = data.name;
        rcd.m_iSex = data.sex;
        rcd.m_iLevel = data.level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = data.icon;
        ChatMsgTips msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = { msgTips };
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, this);

    }

    void OnClickFollow(GameObject target, PointerEventData eventData)
    {
        p_master_base_info_toc data = new p_master_base_info_toc();
        if (m_curTabType == TABBTN_MASTER)
        {
            if (isClickMaster == false)
            {
                data = m_studentlist.SelectedData<p_master_base_info_toc>();
                if (data == null)
                {
                    return;
                }
            }
            else
            {
                if (m_masterData == null)
                {
                    return;
                }
                data = m_masterData;
            }
            
        }

        RoomModel.FollowFriend(data.id, false, false);
    }
    void OnClickLookOver(GameObject target, PointerEventData eventData)
    {
        p_master_base_info_toc data = new p_master_base_info_toc();
        if (m_curTabType == TABBTN_MASTER)
        {
            if (isClickMaster == false)
            {
                data = m_studentlist.SelectedData<p_master_base_info_toc>();
                if (data == null)
                {
                    return;
                }
            }
            else
            {
                if (m_masterData == null)
                {
                    return;
                }
                data = m_masterData;
            }
        }
        
        PlayerFriendData.ViewPlayer(data.id);
    }
    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    private bool isClickMaster = false;
    private void onClickMasterHead(GameObject target,PointerEventData data)
    {
        //if(m_masterData != null)
        {
            isClickMaster = true;
            m_goPopListPanel.TrySetActive(true);
            UIManager.ShowDropList(m_dropArray, OnDropListItemSelected, m_tran, target, 132, 45, UIManager.DropListPattern.Pop);
        }
    }

    private void onSelectStudentItem(object renderdata)
    {
        if( isClickMaster == false )
        {
            p_master_base_info_toc data = m_studentlist.SelectedData<p_master_base_info_toc>();
            if (data == null)
            {
                return;
            }
            GameObject target = m_studentlist.TargetGo;
            if (target != null)
            {
                m_goPopListPanel.TrySetActive(true);
                UIManager.ShowDropList(m_dropArray, OnDropListItemSelected, m_tran, target, 132, 45, UIManager.DropListPattern.Pop);
            }
        }
    }

    private void OnDropListItemSelected(object renderData)
    {
        UIManager.HideDropList();
        m_goPopListPanel.TrySetActive(false);
        var data = renderData as ItemDropInfo;
        if (data == null)
            return;
        if (data.subtype == m_dropArray[0].subtype)
        {
            //查看
            OnClickLookOver(null, null);
        }
        else if(data.subtype == m_dropArray[1].subtype)
        {
            //跟随
            OnClickFollow(null, null);
        }
        else if( data.subtype == m_dropArray[2].subtype )
        {
            //私聊
            OnClickChat(null,null);
        }
        isClickMaster = false;
    }

    private void onClickBtnLeftWeapon(GameObject target, PointerEventData eventData)
    {
        if (m_curWeaponPage == 0)
        {
            masterWeaponSetPageControl(MAX_WEAPONPAGE - 1);
        }
        else
        {
            masterWeaponSetPageControl(m_curWeaponPage - 1);
        }
    }

    private void onClickBtnRightWeapon(GameObject target, PointerEventData eventData)
    {
        if (m_curWeaponPage < MAX_WEAPONPAGE - 1)
        {
            masterWeaponSetPageControl(m_curWeaponPage + 1);
        }
        else if (m_curWeaponPage == MAX_WEAPONPAGE - 1)
        {
            masterWeaponSetPageControl(0);
        }
    }
    /// <summary>
    /// 导师武器设置控制翻页
    /// </summary>
    /// <param name="index"></param>
    private void masterWeaponSetPageControl(int index, bool isCallBack = false)
    {
        m_curWeaponPage = index;
        m_itemCfg = null;
        weapon_attr attr = m_mineWeaponAry[index];
		m_itemCfg = null;
        if (attr != null)
        {
            ConfigItemWeaponLine cfg = ItemDataManager.GetItem(attr.id) as ConfigItemWeaponLine;
            m_itemCfg = cfg;
           // m_imgWeaponIcon.SetSprite(ResourceManager.LoadIcon(cfg.Icon),cfg.SubType);
            m_tran.Find("student/WeaponInfo/Weapon/HasWeapon").gameObject.TrySetActive(true);
            m_tran.Find("student/WeaponInfo/Weapon/NoWeapon").gameObject.TrySetActive(false);

            Text infoText = m_tran.Find("student/WeaponInfo/Weapon/HasWeapon/Text").GetComponent<Text>();
            if (attr.status == 1)
            {
                infoText.text = "已借出，" + attr.apprentice_name;
            }
            else
            {
                infoText.text = "剩余借用时间";
            }
            restTime = (int)(TimeUtil.GetRemainTime((uint)attr.expire_time).TotalSeconds);
            //枪械过期更新界面
            if (!isCallBack && restTime <= 0)
            {
                getMasterGunInfo(false);
            }
            //m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
        }
        else
        {
            restTime = -1;
            //m_imgWeaponIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, "master_noWeaponIcon"));
            m_tran.Find("student/WeaponInfo/Weapon/NoWeapon").gameObject.TrySetActive(true);
            m_tran.Find("student/WeaponInfo/Weapon/HasWeapon").gameObject.TrySetActive(false);
        }
    }

    private void onClickTabBtn(GameObject target, PointerEventData eventData)
    {
        if (target.name != m_curTabType)
        {
            tabPageControl(target.name);
        }
    }

    private void UpdateBubble()
    {
        m_applybubble.TrySetActive(MasterGiftDataManager.singleton.m_hasApply);
        m_masterAppBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.MasterGift) );
        m_masterBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.ApprenticeGift) || MasterGiftDataManager.singleton.m_hasApply);
        m_masterGradeBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.MasterGrade));
    }

    void onClickKickMaster(GameObject target, PointerEventData eventData)
    {
        //NetLayer.Send(new tos_master_kick_master(){id=m_mid,kick_master=true,reason=""});
        UIManager.PopPanel<PanelBattleKickReason>(new object[] { m_data, true }, true);

    }

    void onClickRecommand(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelRecoMaster>(null, true, null);
    }

    void onClickFindMaster(GameObject target, PointerEventData eventData)
    {
        if (m_masterId.text == "")
        {
            return;
        }
        NetLayer.Send(new tos_master_add_master() { id = long.Parse(m_masterId.text) });
        m_masterId.text = "";
    }

    void onClickGetMasterGift(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_master_get_master_welfare() { player_id = PlayerSystem.roleId, welfare_id = m_giftid, is_gold = false });
    }

    void onClickStudentApply(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelStudentApply>(null, true, null);
    }

    void onClickFindStudent(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.roleData.level < 20)
        {
            TipsManager.Instance.showTips("等级不足20级，无法招募学徒");
            return;
        }
        UIManager.PopPanel<PanelFindStudent>(null, true, null);
    }

    private void putGun()
    {
        onClickBtnPutGun(null, null);
    }

    private void onClickBtnPutGun(GameObject target, PointerEventData eventData)
    {
        if (hasStudent)
        {
            UIManager.PopPanel<PanelMasterPutGun>(new[] { getMineMasterWeaponID() }, true, null);
        }
        else
        {
            TipsManager.Instance.showTips("没有学徒，不能放入武器");
        }
    }

    /// <summary>
    /// 获得已放入师徒武器库的武器列表
    /// </summary>
    /// <returns></returns>
    public Array getMineMasterWeaponID()
    {
        List<int> list = new List<int>();
        int lngth = m_mineWeaponAry.Length;
        for (int i = 0; i < lngth; i++)
        {
            if (m_mineWeaponAry[i] != null)
            {
                list.Add(m_mineWeaponAry[i].uid);
            }
        }
        return list.ToArray();
    }
    public void ChangeMasterAndStuInfo(toc_master_apprentice_info data)
    {
        object[] obs = new object[4];
        var list = data.apprentices;
        if (data.apprentices.Length >= 4)
        {
            m_btnApply.TrySetActive(false);
            m_btnfindStu.TrySetActive(false);
            obs = new object[data.apprentices.Length];
        }
        else
        {
            m_btnApply.TrySetActive(true);
            m_btnfindStu.TrySetActive(true);
        }
        m_stuNum = list.Length;
        hasStudent = m_stuNum != 0;
        for (int i = 0; i < data.apprentices.Length; i++)
        {
            obs[i] = data.apprentices[i];
        }
        m_studentlist.Data = obs;

        if (data.masters.Length == 0)
        {
            m_masterData = null;
            m_noMaster.TrySetActive(true);
            m_hasMaster.TrySetActive(false);
        }
        else
        {
            m_masterData = data.masters[0];
            m_data = data.masters[0];
            m_noMaster.TrySetActive(false);
            m_hasMaster.TrySetActive(true);
            m_name.text = data.masters[0].name;
            m_lvl.text = String.Format("{0}级", data.masters[0].level);
            m_head.SetSprite(ResourceManager.LoadRoleIconBig(data.masters[0].icon));
            m_head.SetNativeSize();
            m_mid = data.masters[0].id;
        }
        m_num.text = "0";
        if (PlayerFriendData.singleton.m_masters.Count > 0)
        {
            for (int i = 0; i < PlayerFriendData.singleton.m_double.Count; i++)
            {
                if (PlayerFriendData.singleton.m_double[i].id == PlayerFriendData.singleton.m_masters[0].id)
                {
                    m_num.text = PlayerFriendData.singleton.m_double[i].rest_num.ToString();
                }
            }
        }
    }




    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
        if (UIManager.IsOpen<PanelFindStudent>())
        {
            UIManager.GetPanel<PanelFindStudent>().HidePanel();
        }
        if (UIManager.IsOpen<PanelStudentApply>())
        {
            UIManager.GetPanel<PanelStudentApply>().HidePanel();
        }
        if (UIManager.IsOpen<PanelRecoMaster>())
        {
            UIManager.GetPanel<PanelRecoMaster>().HidePanel();
        }
        UIManager.HideDropList();
    }

    public override void OnShow()
    {
        //UpdateBubble();
        //NetLayer.Send(new tos_master_double_exp());
        //NetLayer.Send(new tos_master_apprentice_info());
        //NetLayer.Send(new tos_master_master_welfare());
        tabPageControl(m_curTabType);
        m_goPopListPanel.TrySetActive(false);
        isClickMaster = false;
    }
    /// <summary>
    /// 标签界面控制
    /// </summary>
    /// <param name="type"></param>
    private void tabPageControl(String type)
    {
        UpdateBubble();
        m_curTabType = type;
        switch (type)
        {
            case TABBTN_MASTER:
                m_panelMaster.TrySetActive(true);
                m_panelStudent.TrySetActive(false);
                NetLayer.Send(new tos_master_double_exp());
                NetLayer.Send(new tos_master_apprentice_info());
//                NetLayer.Send(new tos_master_master_welfare());
                if (UIManager.IsOpen<PanelMasterTree>())
                    UIManager.GetPanel<PanelMasterTree>().HidePanel();
                if (UIManager.IsOpen<PanelMasterGrade>())
                    UIManager.GetPanel<PanelMasterGrade>().HidePanel();
                //getMasterGunInfo(true);
                break;
            case TABBTN_APPRENTICE:
                m_panelMaster.TrySetActive(false);
                m_panelStudent.TrySetActive(true);
                m_weaponDataGrid.Data = new object[4];
                NetLayer.Send(new tos_master_double_exp());
                NetLayer.Send(new tos_master_apprentice_info());
                //NetLayer.Send(new tos_master_master_welfare());
                if (UIManager.IsOpen<PanelMasterTree>())
                    UIManager.GetPanel<PanelMasterTree>().HidePanel();
                if (UIManager.IsOpen<PanelMasterGrade>())
                    UIManager.GetPanel<PanelMasterGrade>().HidePanel();
                NetLayer.Send(new tos_master_arsenal() { is_master = true });
                //getMasterGunInfo(false);
                break;
            case TABBTN_MASTERGRADE:
                m_panelMaster.TrySetActive(false);
                m_panelStudent.TrySetActive(false);
                
                if (UIManager.IsOpen<PanelMasterTree>())
                    UIManager.GetPanel<PanelMasterTree>().HidePanel();
                UIManager.PopPanel<PanelMasterGrade>(null, false, this);
                break;
            case TABBTN_MASTERTREE:
                m_panelMaster.TrySetActive(false);
                m_panelStudent.TrySetActive(false);
                if (UIManager.IsOpen<PanelMasterGrade>())
                    UIManager.GetPanel<PanelMasterGrade>().HidePanel();
                UIManager.PopPanel<PanelMasterTree>(null, false, this);
                break;
        }
    }


    void Toc_master_kick_master(toc_master_kick_master data)
    {
        m_weaponDataGrid.Data = new object[4];
        NetLayer.Send(new tos_master_double_exp());
        NetLayer.Send(new tos_master_apprentice_info());
        NetLayer.Send(new tos_master_master_welfare());

    }


    public override void Update()
    {
        /*if (restTime != -1)
        {
            m_tick += Time.deltaTime;
            if (m_tick > 1)
            {
                m_tick = 0;
                restTime--;
                m_txtTime.text = TimeUtil.FormatTime(restTime, 2);
            }
            else if (restTime == 0)
            {
                getMasterGunInfo(false);
            }
        }
*/
    }

    void CheckHasGift(bool hasgift)
    {
        if (hasgift)
        {
            if (m_giftid != 0)
            {
                m_box.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, ((m_giftid - 4) * 5).ToString() + "_gift"));
            }
            m_tran.Find("master/HasMaster/Button").gameObject.TrySetActive(true);
            m_getInfo.gameObject.TrySetActive(false);
        }
        else
        {
            m_tran.Find("master/HasMaster/Button").gameObject.TrySetActive(false);
            m_getInfo.gameObject.TrySetActive(true);
            int n = PlayerSystem.roleData.level / 5 + 1;
            if (n > 5)
            {
                m_getInfo.text = "已全部领取";
            }
            else
            {
                if (n < 2)
                {
                    n = 2;
                }
                m_getInfo.text = "到达" + (n * 5) + "级可领取";
                m_box.SetSprite(ResourceManager.LoadSprite(AtlasName.Master, (n * 5 - 5).ToString() + "_gift"));
            }
        }
    }

    void Toc_master_master_welfare(toc_master_master_welfare data)
    {
        SetGift();
    }




    public void SetGift()
    {
        CheckHasGift(false);
        var gifts = MasterGiftDataManager.singleton.m_gifts;
        var students = m_studentlist.ItemRenders;
        for (int j = 0; j < students.Count; j++)
        {
            var item = students[j] as ItemStudent;
            item.NoGift();
        }

        for (int i = 0; i < gifts.Count; i++)
        {
            if (gifts[i].player_id == PlayerSystem.roleId)
            {
                if (gifts[i].welfare_ids.Length > 0)
                {
                    this.m_giftid = gifts[i].welfare_ids[0];
                    CheckHasGift(true);
                    continue;
                }
            }
            else
            {
                for (int j = 0; j < students.Count; j++)
                {
                    var item = students[j] as ItemStudent;

                    if (gifts[i].player_id == item.m_id)
                    {
                        if (gifts[i].welfare_ids.Length > 0 && gifts[i].gold <= 0)
                        {
                            item.m_gold = 0;
                            item.m_giftid = gifts[i].welfare_ids[0];
                            item.CheckHasGift(true);
                        }
                        else if (gifts[i].gold > 0)
                        {
                            item.m_giftid = 0;
                            item.m_gold = gifts[i].gold;
                            item.CheckHasGift(false);
                        }
                        continue;
                    }
                }
            }
        }
    }
    /// <summary>
    /// 是否在剔除师傅冷却时间中
    /// </summary>
    /// <returns></returns>
    public static bool isInKickMasterCD()
    {
        bool isInCD = false;
        TimeSpan timeSpan = TimeUtil.GetPassTime(m_kickMasterTime);
        if (timeSpan.TotalHours < 24)
        {
            isInCD = true;
        }
        return isInCD;
    }

    void Toc_master_apprentice_info(toc_master_apprentice_info data)
    {
        m_kickMasterTime = data.kick_master_time;
        if (m_curTabType == TABBTN_MASTER)
        {
            if (data.masters == null || data.masters.Length == 0) { }
            else
                getMasterGunInfo(true);
        }
        else if (m_curTabType == TABBTN_APPRENTICE)
        {
            getMasterGunInfo(false);
        }
        if (UIManager.IsOpen<PanelSubMasterReward>())
        {
            ChangeMasterAndStuInfo(data);
        }
        if (UIManager.IsOpen<PanelStudentApply>())
        {
            UIManager.GetPanel<PanelStudentApply>().ChangeNum();
        }
        NetLayer.Send(new tos_master_master_welfare() { });
    }

    void Toc_master_get_master_welfare(toc_master_get_master_welfare data)
    {
        m_cmwl = m_cmw.GetLine(data.welfare_id);
        //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(null, true, null);
        if (data.gold <= 0)
        {
            //panelItemTip.SetRewardItemList(m_cmw.GetLine(data.welfare_id).Reward);
            PanelRewardItemTipsWithEffect.DoShow(m_cmw.GetLine(data.welfare_id).Reward);
        }
        else
        {
            ItemInfo item = new ItemInfo();
            item.ID = 5002;
            item.cnt = data.gold;
            ItemInfo[] items = new ItemInfo[1];
            items[0] = item;
            //panelItemTip.SetRewardItemList(items);
            PanelRewardItemTipsWithEffect.DoShow(items);
        }
        NetLayer.Send(new tos_master_master_welfare());
    }
    /// <summary>
    /// 查询师徒武器库信息
    /// </summary>
    /// <param name="data"></param>
    void Toc_master_arsenal(toc_master_arsenal data)
    {
        if (data.is_master)
        {//导师的武器库
            m_masterWeaponAry = null;
            m_masterWeaponAry = new weapon_attr[4];
            data.arsenal.CopyTo(m_masterWeaponAry, 0);
            m_weaponDataGrid.Data = m_masterWeaponAry;
            //导师的武器如果借过其他的武器的借用按钮置灰
            bool isBorrow = false;
            foreach (var temp in m_masterWeaponAry)
            {
                if (temp != null)
                    if (temp.apprentice_name == PlayerSystem.roleData.name)
                    {
                        isBorrow = true;
                        break;
                    }
            }
            foreach (ItemWeapon temp in m_weaponDataGrid.ItemRenders)
            {
                temp.showHasBrrow(isBorrow);
            }
        }
        else
        {//我的武器库
            m_mineWeaponAry = null;
            m_mineWeaponAry = new weapon_attr[4];
            data.arsenal.CopyTo(m_mineWeaponAry, 0);
            m_myWeaponDataGrid.Data = m_mineWeaponAry;
            //masterWeaponSetPageControl(0, true);
        }
    }

    void getMasterGunInfo(bool isMaster)
    {
        NetLayer.Send(new tos_master_arsenal() { is_master = isMaster });
    }


    void Toc_master_put_weapon_to_arsenal(toc_master_put_weapon_to_arsenal data)
    {
        getMasterGunInfo(false);
    }

    void Toc_master_borrow_weapon(toc_master_borrow_weapon data)
    {
        getMasterGunInfo(true);
    }

    static void Toc_master_has_welfare_apply(toc_master_has_welfare_apply data)
    {
        if (data.has_apprentice || data.has_master)
        {
            NetLayer.Send(new tos_master_master_welfare());
        }
    }

}

