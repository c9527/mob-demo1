﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelBattleBigHead : BasePanel
{
    private GameObject m_progress;
    private GameObject m_progressIcons;
    private Image m_progressBar;
    private GameObject m_becomeHeroTip;
    private GameObject m_becomeBigHeadHeroTip;
    private GameObject m_bigHeadHeroTip;
    private GameObject m_tips;
    private GameObject m_becomeBigHeadHeroBtn;
    private GameObject m_bigHeadEnergyGo;
    private Image m_yellowBar;
    private Image m_blueBar;

    private Text m_tipsText;
    private UIEffect m_progressEffect;
    private float m_progressStageRatio;
    private int m_curKingCount;
    private int m_maxHeroCount;

    private const string HERO_TIP_TEXT = "我方已有大头王:{0}，本局最多可产生大头英雄数:{1}";

    public PanelBattleBigHead()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleBigHead");

        AddPreLoadRes("UI/Battle/PanelBattleBigHead", EResType.UI);
        AddPreLoadRes("Atlas/BigHead", EResType.Atlas);
    }

    public override void Init()
    {
        m_progress = m_tran.Find("progress_bar_bg").gameObject;
        m_progressIcons = m_tran.Find("big_head_icons").gameObject;
        m_progressBar = m_tran.Find("progress_bar_bg/progress_bar").GetComponent<Image>();
        m_becomeHeroTip = m_tran.Find("big_head_hero_now_tip").gameObject;
        m_becomeBigHeadHeroTip = m_tran.Find("become_big_head_hero_tips_bg").gameObject;
        m_bigHeadHeroTip = m_tran.Find("big_head_hero_now_tip").gameObject;
        m_tips = m_tran.Find("tips_bg").gameObject;
        m_becomeBigHeadHeroBtn = m_tran.Find("become_big_head_hero_btn").gameObject;
        m_tipsText = m_tran.Find("tips_bg/tips_txt").GetComponent<Text>();

        m_becomeHeroTip.TrySetActive(false);
        m_becomeBigHeadHeroTip.TrySetActive(false);
        m_bigHeadHeroTip.TrySetActive(false);
        m_becomeBigHeadHeroBtn.TrySetActive(false);

        m_bigHeadEnergyGo = m_tran.Find("become_big_head_energry").gameObject;
        m_yellowBar = m_bigHeadEnergyGo.transform.Find("yellowbar").GetComponent<Image>();
        m_blueBar = m_bigHeadEnergyGo.transform.Find("bluebar").GetComponent<Image>();
        m_yellowBar.fillAmount = 0f;
        m_blueBar.fillAmount = 0f;
        m_progressBar.fillAmount = 0f;
        SetCountTips(m_curKingCount, m_maxHeroCount);
        m_bigHeadEnergyGo.TrySetActive(true);
    }

    public void ResetEnergyBar()
    {
        if (m_yellowBar !=null)
        {
            m_yellowBar.fillAmount = 0f;
        }
        if (m_blueBar)
        {
            m_blueBar.fillAmount = 0f;
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_becomeBigHeadHeroBtn).onPointerClick += OnBecomeHero;
        AddEventListener<BasePlayer,bool>(GameEvent.PLAYER_LEVEL_UP, playLevelUpEffect);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
        {
            if (m_becomeBigHeadHeroBtn.activeSelf)
                OnBecomeHero(null, null);
        });
#endif
    }

    private void playLevelUpEffect(BasePlayer player, bool isLevelUp)
    {
        if (player == null)
        {
            Logger.Log("大头模式升级的英雄为空");
            return;
        }

        if (isLevelUp && player.isRendererVisible)
        {
            if (player.PlayerId == MainPlayer.singleton.PlayerId)
            {
                UIEffect.ShowEffect(m_tran, EffectConst.UI_BIGHEAD_MAINPLAYER_LEVEL_UP);

            }
            else
            {
                Vector3 effectPos = new Vector3(0, 0, 0); ;
                EffectManager.singleton.GetEffect<EffectTimeout>(EffectConst.BIGHEAD_OTHERPLAYER_LEVEL_UP, (effect) =>
                {
                    effect.Attach(effectPos, Vector3.zero, Space.Self, player.transform);
                    effect.SetTimeOut(-1);
                    effect.Play();
                });
            }
        }
    }

    public override void OnShow()
    {
        float percent = 0;
        int campScore1 = 0;
        int campScore2 = 0;
        if (HasParams())
        {
            percent = (float)m_params[0];
            campScore1 = (int)m_params[1];
            campScore2 = (int)m_params[2];
            if (campScore1 == 0 && campScore2 == 0)
            {
                ResetEnergyBar();
            }
            else
            {
                if (percent > 0)
                {
                    SetBigHeadEnergyProgress(percent);
                }
            }
        }
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        if (m_progressEffect != null)
            m_progressEffect.Destroy();
    }

    public override void Update()
    {

    }

    public void SetBigHeadProgress(float stage)
    {
        if (m_progressBar != null)
            m_progressBar.fillAmount = stage * m_progressStageRatio;
    }

    public void SetBigHeadEnergyProgress(float value)
    {
        if(m_yellowBar!=null)
        {
            m_yellowBar.fillAmount = value;
            if(m_blueBar.fillAmount != 1f)
            {
                m_blueBar.fillAmount = 1f;
            }
        }
    }

    public void ShowHeroTip(bool isShow)
    {
        if (m_bigHeadHeroTip != null)
            m_bigHeadHeroTip.TrySetActive(isShow);
    }

    public void ShowBecomeHeroTip(bool isShow)
    {
        if (m_becomeBigHeadHeroTip != null && m_becomeBigHeadHeroBtn != null)
        {
            m_becomeBigHeadHeroTip.TrySetActive(isShow);
            m_becomeBigHeadHeroBtn.TrySetActive(isShow);
            if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web))
            {
                m_tran.Find("become_big_head_hero_tips_bg/become_big_head_hero_tips").gameObject.TrySetActive(false);
                m_tran.Find("become_big_head_hero_tips_bg/PCkeyText").GetComponent<Text>().text = "点击 " + GlobalBattleParams.singleton.GetPcKeyTextByKey(GameEvent.INPUT_KEY_E_DOWN) + " 变身为大头英雄";
            }
            else
            {
                m_tran.Find("become_big_head_hero_tips_bg/become_big_head_hero_tips").gameObject.TrySetActive(true);
                m_tran.Find("become_big_head_hero_tips_bg/PCkeyText").GetComponent<Text>().text = "";
            }
        }
    }

    public void SetProgressStage(int stage)
    {
        m_progressStageRatio = 1f / stage;
    }

    public void ShowProgressEffect(bool isShow)
    {
        if (isShow)
        {
            if (m_progressEffect != null)
                m_progressEffect.Play();
            else
                m_progressEffect = UIEffect.ShowEffect(m_progressBar.transform, EffectConst.UI_BIG_HEAD_PROGRESS_BAR);
        }
        else if (m_progressEffect != null)
        {
            m_progressEffect.Hide();
        }
    }

    public void SetCountTips(int curKingCount, int maxHeroCount)
    {
        if (m_tipsText != null)
            m_tipsText.text = string.Format(HERO_TIP_TEXT, curKingCount, maxHeroCount);
        else
        {
            m_curKingCount = curKingCount;
            m_maxHeroCount = maxHeroCount;
        }
    }

    public void ShowCountTips(bool isShow)
    {
        if (m_tips != null)
            m_tips.TrySetActive(isShow);
    }

    public void ShowProgress(bool isShow)
    {
        if (m_progress != null && m_progressIcons != null)
        {
            m_progress.TrySetActive(isShow);
            m_progressIcons.TrySetActive(isShow);
        }
    }

    public void ShowEnergy(bool isShow)
    {
        if( m_bigHeadEnergyGo != null )
        {
            m_bigHeadEnergyGo.TrySetActive(isShow);
        }
    }

    //变成大头英雄
    private void OnBecomeHero(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive)
            NetLayer.Send(new tos_fight_update_actor_type() { actor_type = GameConst.ACTOR_TYPE_BIG_HEAD_HERO });
    }
}
