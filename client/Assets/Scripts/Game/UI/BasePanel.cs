﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

/// <summary>
/// 说明：
/// 1.每个页面都认为是一个Panel，大厅为常驻底层Panel，上面叠加各种功能Panel
/// 2.BasePanel类是个非Mono类，相当于MVC中M和C，V是BasePanel的属性，这样可以在界面隐藏状态下，依然能够监听处理事件数据，在页面打开的时候再更新界面信息
/// 3.Panel内的部件prefab上尽量不挂脚本，除非他的功能比较独立，尽量在Panel内集中处理
/// 
/// 创建Panel步骤：
/// 1.新建继承BasePanel的子类，添加构造函数、实现IPanel接口函数框架
/// 2.在构造函数中设置Panel的Prefab路径，并添加预加载资源信息。
/// 3.在Init接口中获取对象引用，以及其他初始化操作
/// 4.在InitEvent接口中注册UI事件或全局事件
/// 5.在OnShow、OnHide、OnDestroy、Update函数中写逻辑
/// 
/// Tips：
/// 1.基类BasePanel提供了m_go和m_tran，方便获取Panel的Prefab实例化后的GameoObject及其RectReansform对象
/// 2.执行ShowPanel后，先立即执行构造函数，等待资源加载完毕后，依次同步执行Init()，InitEvent()，OnShow()，在Panel没销毁之前再打开，就只执行OnShow()了
/// 3.对于像房间列表这种一打开UI就需要服务端数据的，可以在构造函数内向后端请求数据并添加监听，在后端消息返回和OnShow函数分别响应处理数据
/// 4.如果需要在页面没打开之前就能监听消息，可以写在UIManager类的RegedistServerMsg方法中
/// </summary>
public abstract class BasePanel : IPanel
{
    protected GameObject m_go;
    protected RectTransform m_tran;
    protected Canvas m_canvas;
    protected CanvasGroup m_canvasGroup;
    private List<ResourceItem> m_resList = new List<ResourceItem>();
    protected bool m_pixelPrefect = false;
    protected object[] m_params;
    private bool m_preloaded;
    private bool m_preloading;
    private bool m_isOpen;
    private bool m_isOpenLogic;
    private bool m_isInited;
    private string m_prefabPath;
    protected bool m_isCreateRayCaster = true;
    public bool DontDestroyOnLoad { get; set; }

    private bool m_tween;
    private Vector2 m_tweenPos1;
    private Vector2 m_tweenPos2;

    private List<BasePanel> m_listChildPanel = new List<BasePanel>();
    private BasePanel m_parentPanel = null;

    protected void SetPanelPrefabPath(string path)
    {
        m_prefabPath = path;
    }

    protected void AddPreLoadRes(string path, EResType type, int tag = 0)
    {
        m_resList.Add(new ResourceItem(path, type, tag));
    }

    public List<ResourceItem> GetPreloadRes()
    {
        return m_resList;
    }

    public void SetParams(object[] @params)
    {
        m_params = @params;
    }

    public void AddChildPanel(BasePanel child)
    {
        if (!m_listChildPanel.Contains(child))
        {
            m_listChildPanel.Add(child);
        }
    }

    public void AddChild(Transform child, bool worldPosStay)
    {
        child.SetParent(m_tran, worldPosStay);
    }

    public void SetParent(BasePanel parent)
    {
        m_parentPanel = parent;
    }

    public Transform transform
    {
        get { return m_tran; }
    }

    public bool HasParams()
    {
        return m_params != null && m_params.Length > 0;
    }

    public void ShowPanel(Transform parent = null, bool addMask = false, Action onShow = null, float maskAlpha = 0.75f)
    {
        if (m_isOpen)
            return;

        m_isOpenLogic = true;
        if (m_preloaded)
        {
            m_isOpen = true;
            NetLayer.AddHandler(this);
            m_go.TrySetActive(true);
            for (int i = 0; i < m_listenerList.Count; i++)
            {
                GameDispatcher.AddEventListener(m_listenerList[i].type, m_listenerList[i].listener);
            }
            SetMask(addMask, m_go, maskAlpha);
            var tran = m_go.GetComponent<RectTransform>();
            tran.SetUILocation(parent, 0, 0);
            tran.SetAsLastSibling();
            SortingOrderRenderer.RebuildAll();
            OnShow();
            TweenShow(false);

            if (onShow != null)
                onShow();

            LuaHook.CheckHook(HookType.UI_Open, 0, this);
        }
        else if (!m_preloading)
        {
            m_preloading = true;
            UIManager.ShowMask("加载中...", Driver.isMobilePlatform ? 8 : 60);
            ResourceManager.LoadMulti(m_resList.ToArray(), null, () =>
            {
                m_preloading = false;

                if (!m_isOpenLogic)
                {
                    UIManager.HideMask();
                    return;
                }
                m_isOpen = true;
                NetLayer.AddHandler(this);
                if (m_go == null)
                {
                    m_go = ResourceManager.LoadUI(m_prefabPath);
                    m_tran = m_go.GetComponent<RectTransform>();
                }

                m_preloaded = true;

                var tran = m_go.GetComponent<RectTransform>();
                tran.SetUILocation(parent, 0, 0);
                tran.localScale = new Vector3(1f, 1f, 1f);
                tran.offsetMin = new Vector2(0f, 0f);
                tran.offsetMax = new Vector2(0f, 0f);
                tran.anchorMin = new Vector2(0f, 0f);
                tran.anchorMax = new Vector2(1f, 1f);
                tran.SetAsLastSibling();
                m_go.TrySetActive(true);

                SetMask(addMask, m_go, maskAlpha);

                m_canvas = m_go.AddMissingComponent<Canvas>();
                m_canvas.overridePixelPerfect = m_pixelPrefect;
                m_canvas.pixelPerfect = m_pixelPrefect;

                if (m_go.GetComponent<SortingOrderRenderer>() == null)
                {
                    var rendererScript = m_go.AddComponent<SortingOrderRenderer>();
                    if (!m_isCreateRayCaster)
                    {
                        rendererScript.RemoveGraphicRaycaster();
                    }
                }
                SortingOrderRenderer.RebuildAll();

                Init();
                m_isInited = true;
                InitEvent();
                UIManager.HideMask();
                OnShow();
                TweenShow(true);

                if (onShow != null)
                    onShow();

                LuaHook.CheckHook(HookType.UI_Open, 0, this);
            });
        }

    }

    private void SetMask(bool addMask, GameObject go, float alpha)
    {
        if (addMask && go.GetComponent<Image>() == null)
        {
            var mask = go.AddComponent<Image>();
            mask.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itemblack"));
            mask.color = new Color(1f, 1f, 1f, alpha);
            mask.type = Image.Type.Sliced;
        }
        else if (!addMask && go.GetComponent<Image>())
        {
            Object.Destroy(go.GetComponent<Image>());
        }
    }

    private void TweenShow(bool delay)
    {
        //新手完成后才加窗口缓动效果
        if (!GuideManager.IsGuideFinish() || GuideManager.IsGuiding())
            return;

        TryTween("infoframe", "equipframe", delay);
        TryTween("pageframe", "optionframe", delay);
        TryTween("bound/left", "bound/right", delay);
    }

    private void TryTween(string frame1Name, string frame2Name, bool delay)
    {
        var frame1 = m_tran.Find(frame1Name);
        var frame2 = m_tran.Find(frame2Name);
        if (frame1 != null && frame2 != null)
        {
            var tranFrame1 = frame1.GetComponent<RectTransform>();
            var tranFrame2 = frame2.GetComponent<RectTransform>();

            if (m_tweenPos1 == Vector2.zero || m_tweenPos1 == Vector2.zero)
            {
                m_tweenPos1 = tranFrame1.anchoredPosition;
                m_tweenPos2 = tranFrame2.anchoredPosition;
            }

            tranFrame1.anchoredPosition = new Vector2(m_tweenPos1.x - tranFrame1.sizeDelta.x - 100, m_tweenPos1.y);
            tranFrame2.anchoredPosition = new Vector2(m_tweenPos2.x + tranFrame2.sizeDelta.x + 100, m_tweenPos2.y);

            TimerManager.SetTimeOut(delay ? 0.1f : 0, () =>
            {
                if (tranFrame1 == null || tranFrame2 == null)
                {
                    return;
                }
                TweenPosition.Begin(tranFrame1.gameObject, 0.2f, m_tweenPos1).method = UITweener.Method.EaseIn;
                TweenPosition.Begin(tranFrame2.gameObject, 0.2f, m_tweenPos2).method = UITweener.Method.EaseIn;
            });

        }
    }

    public void HidePanel()
    {
        m_isOpenLogic = false;
        if (!m_isOpen || m_go == null)
            return;

        m_isOpen = false;
        m_preloading = false;
        NetLayer.RemoveHandler(this);
        m_go.TrySetActive(false);
        for (int i = 0; i < m_listenerList.Count; i++)
        {
            GameDispatcher.DeleteEventListener(m_listenerList[i].type, m_listenerList[i].listener);
        }

        OnHide();

        for (int i = 0; i < m_listChildPanel.Count; ++i)
        {
            m_listChildPanel[i].HidePanel();
        }
        //特殊处理一下玩家升级的特效播放
        if (UIManager.GetPanel<PanelRoleLevelup>() != null && UIManager.GetPanel<PanelRoleLevelup>().IsOpen() == true)
        {
            UIManager.GetPanel<PanelRoleLevelup>().playEffect();
        }
    }

    public bool IsOpen()
    {
        return m_isOpen;
    }

    public bool IsInited()
    {
        return m_isInited;
    }

    public void DestroyPanel()
    {
        m_isOpenLogic = false;
        if (m_go == null)
            return;

        m_isOpen = false;
        m_isInited = false;
        m_preloaded = false;
        m_preloading = false;
        NetLayer.RemoveHandler(this);
        GameObject.Destroy(m_go);
        for (int i = 0; i < m_listenerList.Count; i++)
        {
            GameDispatcher.DeleteEventListener(m_listenerList[i].type, m_listenerList[i].listener);
        }
        m_listenerList.Clear();

        OnDestroy();

        for (int i = 0; i < m_listChildPanel.Count; ++i)
        {
            m_listChildPanel[i].DestroyPanel();
        }
        m_listChildPanel.Clear();
    }

    public float Alpha
    {
        get
        {
            if (m_canvasGroup == null)
            {
                m_canvasGroup = m_go.GetComponent<CanvasGroup>();
                if (m_canvasGroup == null)
                    m_canvasGroup = m_go.AddComponent<CanvasGroup>();
            }
            return m_canvasGroup.alpha;
        }
        set
        {
            if (m_canvasGroup == null)
            {
                m_canvasGroup = m_go.GetComponent<CanvasGroup>();
                if (m_canvasGroup == null)
                    m_canvasGroup = m_go.AddComponent<CanvasGroup>();
            }
            m_canvasGroup.alpha = value;
        }
    }

    public bool EnableMouseEvent
    {
        get
        {
            if (m_canvasGroup == null)
            {
                m_canvasGroup = m_go.GetComponent<CanvasGroup>();
                if (m_canvasGroup == null)
                    m_canvasGroup = m_go.AddComponent<CanvasGroup>();
            }
            return m_canvasGroup.blocksRaycasts;
        }
        set
        {
            if (m_canvasGroup == null)
            {
                m_canvasGroup = m_go.GetComponent<CanvasGroup>();
                if (m_canvasGroup == null)
                    m_canvasGroup = m_go.AddComponent<CanvasGroup>();
            }
            m_canvasGroup.blocksRaycasts = value;
        }
    }


    #region 注册事件侦听器
    private struct ListenerStruct
    {
        public string type;
        public Delegate listener;
    }
    private List<ListenerStruct> m_listenerList = new List<ListenerStruct>();
    public void AddEventListener(string type, EventCallback listener) { GameDispatcher.AddEventListener(type, listener); m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    public void AddEventListener<T1>(string type, EventCallback<T1> listener) { GameDispatcher.AddEventListener<T1>(type, listener); ;m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    public void AddEventListener<T1, T2>(string type, EventCallback<T1, T2> listener) { GameDispatcher.AddEventListener<T1, T2>(type, listener); m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    public void AddEventListener<T1, T2, T3>(string type, EventCallback<T1, T2, T3> listener) { GameDispatcher.AddEventListener<T1, T2, T3>(type, listener); m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    public void AddEventListener<T1, T2, T3, T4>(string type, EventCallback<T1, T2, T3, T4> listener) { GameDispatcher.AddEventListener<T1, T2, T3, T4>(type, listener); m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    public void AddEventListener<T1, T2, T3, T4, T5>(string type, EventCallback<T1, T2, T3, T4, T5> listener) { GameDispatcher.AddEventListener<T1, T2, T3, T4, T5>(type, listener); m_listenerList.Add(new ListenerStruct() { type = type, listener = listener }); }
    #endregion

    public abstract void Init();
    public abstract void InitEvent();
    public abstract void OnShow();
    public abstract void OnHide();
    public abstract void OnBack();
    public abstract void OnDestroy();
    public abstract void Update();
}
