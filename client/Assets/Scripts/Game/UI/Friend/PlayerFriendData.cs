﻿using System.Collections.Generic;
using System.Linq;

public class PlayerFriendData
{

    public const string FRIEND_DATA_UPDATE = "friend_data_update";
    public const string ADD_FRIEND = "add_friend";

    List<FriendData> m_listFriend;
    List<FriendData> m_listReply;
    List<FriendData> m_listBlack;
    List<FriendData> m_listRecommand;
    List<FriendData> m_listFamily;

    public List<p_master_base_info_toc> m_masters;
    public List<p_master_base_info_toc> m_students;

    public List<p_master_double_exp_toc> m_double;
    
    public static PlayerFriendData singleton = null;
    public PlayerFriendData()
    {
        if (singleton != null)
        {
            Logger.Error("PlayerFriendData is a singleton");
        }

        singleton = this;
        Init();
    }

    void Init()
    {
        m_masters = new List<p_master_base_info_toc>();
        m_students = new List<p_master_base_info_toc>();
        m_listFriend = new List<FriendData>();
        m_listReply = new List<FriendData>();
        m_listBlack = new List<FriendData>();
        m_listRecommand = new List<FriendData>();
        m_listFamily = new List<FriendData>();
        GameDispatcher.AddEventListener<toc_login_select>(GameEvent.MAIN_PLAYER_JOIN, OnMainPlayerJoin);
        m_double = new List<p_master_double_exp_toc>();
    }

    void OnMainPlayerJoin(toc_login_select tlogin)
    {
        GetFriendList();
        NetLayer.Send(new tos_player_tag_list());
        
    }

//    public static void SendFriendMsg(string key, object value = null)
//    {
//        CDict proto = new CDict();
//        proto["typ"] = "player";
//        proto["key"] = key;
//        if (value != null)
//            proto["val"] = value;
//        NetLayer.SendPack(proto);
//    }

    static void Toc_player_friend_data(toc_player_friend_data data)
    {
        singleton.FlushList(data.friend_list, singleton.m_listFriend);
        singleton.FlushList(data.fans_list, singleton.m_listReply);
        singleton.FlushList(data.black_list, singleton.m_listBlack);

        BubbleManager.SetBubble(BubbleConst.FriendApply, singleton.m_listReply.Count);
        GameDispatcher.Dispatch(FRIEND_DATA_UPDATE);
    }

    public void FlushList(p_friend_data[] syncList, List<FriendData> oneList)
    {
        oneList.Clear();
        for( int index = 0; index < syncList.Length; ++ index )
        {
            p_friend_data serverfriendData = syncList[index];
            FriendData newFriendData = new FriendData();
            CopyFriendDataFromDict( newFriendData, serverfriendData );

            oneList.Add(newFriendData);
        }
    }

//    public void FlushFamilyList()
//    {
//        m_listFamily.Clear();
//        for (int index = 0; index < m_listFriend.Count; ++index)
//        {
//            if (FamilyDataManager.Instance.IsFamilyMember(m_listFriend[index].m_id))
//            {
//                m_listFamily.Add(m_listFriend[index]);
//            }
//        }
//    }

   
    public bool isFriend(long uid)
    {
        return GetFriend(uid) != null;
    }

    public FriendData GetFriend(long uid)
    {
        if (uid > 0)
        {
            for (int i = 0; i < m_listFriend.Count; i++)
            {
                if (m_listFriend[i].m_id == uid)
                    return m_listFriend[i];
            }
        }
        return null;
    }

    /// <summary>
    /// 检查id对应玩家跟主角的师徒关系
    /// </summary>
    /// <param name="id">玩家的pid</param>
    /// <returns>0：没关系 1：师傅 2：徒弟</returns>
    public int CheckIsMasterOrStudent(long id)
    {
        if (m_masters.Count > 0)
        {
            if (id == m_masters[0].id)
            {
                return 1;
            }
        }
        for (int i = 0; i < m_students.Count; i++)
        {
            if (id == m_students[i].id)
            {
                return 2;
            }
        }
        return 0;
    }


    public bool CheckIsInMaster(int id)
    {
        if (m_masters.Count > 0)
        {
            if (id == m_masters[0].id)
            {
                return true;
            }
        }
        for (int i = 0; i < m_students.Count; i++)
        {
            if (id == m_students[i].id)
            {
                return true;
            }
        }
        return false;
    }


    public List<FriendData> friendList
    {
        get
        {
            return m_listFriend;
        }
    }

    public List<FriendData> replyList
    {
        get
        {
            return m_listReply;
        }
    }

    public List<FriendData> blackList
    {
        get
        {
            return m_listBlack;
        }
    }

    public List<FriendData> recommandList
    {
        get
        {
            return m_listRecommand;
        }
    }

    public List<FriendData> familyList
    {
        get
        {
            m_listFamily = m_listFriend.Where(data => FamilyDataManager.Instance.IsFamilyMember(data.m_id)).ToList();
            return m_listFamily;
        }
    }

    void CopyFriendDataFromDict(FriendData newFriendData, p_friend_data serverfriendData)
    {
        newFriendData.m_id = serverfriendData.id;
        newFriendData.m_name = serverfriendData.name;
        newFriendData.m_sex = serverfriendData.sex;
        newFriendData.m_icon = serverfriendData.icon;
        newFriendData.m_honor = serverfriendData.honor;
        newFriendData.m_vip_level = serverfriendData.vip_level;
        newFriendData.m_level = serverfriendData.level;
        int handle = serverfriendData.handle;
        newFriendData.m_goldace = serverfriendData.gold_ace;
        newFriendData.m_mvp = serverfriendData.mvp;
        newFriendData.m_sliverace = serverfriendData.silver_ace;
        newFriendData.hero_cards = serverfriendData.hero_cards;
        newFriendData.set_titles = serverfriendData.set_titles;
        if( handle == 0 )
        {
            newFriendData.m_state = FRIEND_STATE.OFFLINE;
        }
        else
        {
            newFriendData.m_state = FRIEND_STATE.ONLINE;
        }

        newFriendData.m_channel = serverfriendData.channel;
        newFriendData.m_roomid = serverfriendData.room;
        newFriendData.m_teamid = serverfriendData.team;
        newFriendData.m_in_fight = serverfriendData.fight;
        if( newFriendData.m_roomid != 0 )
        {
            newFriendData.m_state = FRIEND_STATE.INROOM;
        }

        if( newFriendData.m_teamid != 0 )
        {
            newFriendData.m_state = FRIEND_STATE.INRANK;
        }

        if (newFriendData.m_in_fight)
        {
            newFriendData.m_state = FRIEND_STATE.INFIGHT;
        }
        newFriendData.logouttime = serverfriendData.logout_time;
    }

    public void AddNewFriendToFriendList(FriendData newFriend)
    {
        if (m_listFriend.Find(x => x.m_id == newFriend.m_id) != null)
        {
            return;
        }
        m_listFriend.Add(newFriend);
        for (int i = 0; i < m_listReply.Count; i++)
        {
            if (newFriend.m_id==m_listReply[i].m_id)
            {
                m_listReply.RemoveAt(i);
            }
        }
    }

    public void CheckApplyList()
    {
        for (int i = 0; i < m_listReply.Count; i++)
        {
            if (m_listFriend.Contains(m_listReply[i]))
            {
                m_listReply.RemoveAt(i);
            }
        }
    }

    static void Toc_player_friend_msg(toc_player_friend_msg data)
    {
        string type = data.type;
        if( type == "add" )  //好友申请
        {
            FriendData applyData = new FriendData();
            singleton.CopyFriendDataFromDict(applyData, data.friend_data);
            singleton.replyList.Add( applyData );
            GameDispatcher.Dispatch( FRIEND_DATA_UPDATE );
        }
        else if( type == "agree" ) // 对方同意加好友
        {
            FriendData agreeData = new FriendData();
            singleton.CopyFriendDataFromDict(agreeData, data.friend_data);
            singleton.AddNewFriendToFriendList(agreeData);
            GameDispatcher.Dispatch(FRIEND_DATA_UPDATE);
        }
        else if ( type == "refuse" ) // 对方拒绝加好友
        {
        }
        else if (type == "ablack") // 拉黑名单
        {
            List<FriendData> frientList = singleton.friendList;
            var blackId = data.friend_data.id;
            for( int index = 0; index < frientList.Count; ++ index )
            {
                FriendData blackData = frientList[index];
                if ( blackData.m_id == blackId)
                {
                    frientList.Remove(blackData);
                    break;
                }
            }
            GameDispatcher.Dispatch(FRIEND_DATA_UPDATE);
        }
        else if( type == "update" || type == "login" || type == "logout")
        {
            if(type=="login")
            {
                LoginRemindMangager.Ins.ShowTips(1, data.friend_data.id);
            }
            List<FriendData> frientList = singleton.friendList;
            var updateId = data.friend_data.id;
            for (int index = 0; index < frientList.Count; ++index)
            {
                FriendData oneFriend = frientList[index];
                if ( oneFriend.m_id == updateId )
                {
                    singleton.CopyFriendDataFromDict(oneFriend, data.friend_data);
                    break;
                }
            }

            List<FriendData> applyList = singleton.replyList;
            for( int cnt = 0; cnt < applyList.Count; ++ cnt )
            {
                FriendData oneFans = applyList[cnt];
                if ( oneFans.m_id == updateId ) 
                {
                    singleton.CopyFriendDataFromDict(oneFans, data.friend_data);
                    break;
                }  
            }
            GameDispatcher.Dispatch(FRIEND_DATA_UPDATE);
        }

        BubbleManager.SetBubble(BubbleConst.FriendApply, singleton.m_listReply.Count);
    }

    static void Toc_master_apprentice_info(toc_master_apprentice_info data)
    {
        singleton.m_masters.Clear();
        if (data.masters.Length > 0)
        {
            singleton.m_masters.Add(data.masters[0]);
        }
        if (data.apprentices.Length == 4)
        {
            MasterGiftDataManager.singleton.m_hasApply = false;
            BubbleManager.SetBubble(BubbleConst.StudentApply, false);
        }
        singleton.m_students.Clear();
        for (int i = 0; i < data.apprentices.Length; i++)
        {
            singleton.m_students.Add(data.apprentices[i]);
        }
        if (UIManager.IsOpen<PanelSubMasterReward>())
        {
            UIManager.GetPanel<PanelSubMasterReward>().ChangeMasterAndStuInfo(data);
        }
        if (UIManager.IsOpen<PanelStudentApply>())
        {
            UIManager.GetPanel<PanelStudentApply>().ChangeNum();
        }
        GetFriendList();
    }

    static void Toc_master_double_exp(toc_master_double_exp data)
    {
        singleton.m_double = new List<p_master_double_exp_toc>();
        for (int i = 0; i < data.masters.Length; i++)
        {
            singleton.m_double.Add(data.masters[i]);
        }
    }

    static void Toc_player_add_black(toc_player_add_black data)
    {
        var friendid = data.black_id;
        if (friendid == 0)
            return;

        List<FriendData> friendList = singleton.friendList;
        for (int i = 0; i < friendList.Count; ++i)
        {
            FriendData friendData = friendList[i];
            if (friendData.m_id == friendid)
            {
                singleton.blackList.Add(friendData);
                friendList.RemoveAt(i);
                break;
            }
        }

        GameDispatcher.Dispatch(FRIEND_DATA_UPDATE);
    }

    static void Toc_player_agree_fan(toc_player_agree_fan data)
    {
        var friendid = data.fan_id;
        if (friendid == 0)
            return;

        List<FriendData> replyList = singleton.replyList;
        for (int i = 0; i < replyList.Count; ++i)
        {
            FriendData friendData = replyList[i];
            if (friendData.m_id == friendid)
            {
                singleton.AddNewFriendToFriendList(friendData);
                //replyList.RemoveAt(index);
                break;
            }
        }
        //UIManager.GetPanel<PanelHall>().updatebubble();
        BubbleManager.SetBubble(BubbleConst.FriendApply, replyList.Count);
        GameDispatcher.Dispatch(ADD_FRIEND);
    }

    static void Toc_player_del_black(toc_player_del_black data)
    {
        var friendid = data.black_id;
        if (friendid == 0)
            return;

        List<FriendData> blackList = singleton.blackList;
        for (int i = 0; i < blackList.Count; ++i)
        {
            if (blackList[i].m_id == friendid)
            {
                blackList.RemoveAt(i);
                break;
            }
        }

        var blackIdList = NetChatData.m_lstBlack;
        for (int i = 0; i < blackIdList.Count; i++)
        {
            if (blackIdList[i] == friendid)
            {
                blackIdList.RemoveAt(i);
                break;
            }
        }
    }

    static void Toc_player_refuse_fan(toc_player_refuse_fan data)
    {
        var friendid = data.fan_id;
        if (friendid == 0)
            return;

        List<FriendData> replyList = singleton.replyList;
        for (int index = 0; index < replyList.Count; ++index)
        {
            FriendData friendData = replyList[index];
            if (friendData.m_id == friendid)
            {
                replyList.RemoveAt(index);
                break;
            }
        }

        BubbleManager.SetBubble(BubbleConst.FriendApply, replyList.Count);
    }

    static public void GetFriendList()
    {
        NetLayer.Send(new tos_player_friend_data());
    }

    static public void ViewPlayer(long playerId)
    {
        NetLayer.Send(new tos_player_view_player { friend_id = playerId });
    }

    static public void ViewPlayer(string name)
    {
        NetLayer.Send(new tos_player_view_player_by_name {  friend_name=name });
    }

    static public void AddFriend(long playerId)
    {
        if (PlayerFriendData.singleton.friendList.Count<100)
        {
            NetLayer.Send(new tos_player_add_friend { friend_id = playerId });
        }
        else
        {
            TipsManager.Instance.showTips("好友数量已经达到上限");
        }
    }

    static public void AddFriend(string playerName)
    {
        NetLayer.Send(new tos_player_add_friend_by_name { friend_name = playerName });
    }

    static public void DelFriend(long playerId)
    {
        NetLayer.Send(new tos_player_del_friend { friend_id = playerId });
    }

    static public void AddBlack(long playerId)
    {
        NetLayer.Send(new tos_player_add_black() {black_id = playerId});
    }

    static public void DelBlack(long playerId)
    {
        NetLayer.Send(new tos_player_del_black {black_id = playerId});
    }

    static public void RcmndList()
    {
        NetLayer.Send(new tos_player_friend_rcmnd_list());
    }

}


public class FriendData
{
    public long m_id;
    public string m_name;
    public int m_sex;
    public int m_icon;
    public int m_honor;
    public int m_vip_level;
    public int m_level;
    public FRIEND_STATE m_state;
    public int m_channel;
    public int m_roomid;
    public int m_teamid;
    public int m_mvp;
    public int m_goldace;
    public int m_sliverace;
    public bool m_in_fight;
    public int logouttime;
    public p_hero_card_info[] hero_cards;
    public p_title_info[] set_titles;
}
