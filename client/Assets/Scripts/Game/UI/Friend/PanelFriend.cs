﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public enum FRIEND_STATE
{
    OFFLINE,
    ONLINE,
    INROOM,
    INRANK,
    INFIGHT,
}



public class PanelFriend : BasePanel
{
    List<ItemFriend> m_listFriend;
    List<ItemFriend> m_listRecommand;
    List<ItemFriend> m_listRely;
    List<ItemFriend> m_listBlack;
    //List<p_master_base_info_toc> m_masters;
    //List<p_master_base_info_toc> m_students;

    Toggle m_toggleFriend;
    Toggle m_toggleReply;
    Toggle m_toggleRecommand;
    Toggle m_toggleBlack;
    Toggle m_toggleMaster;
    Toggle m_toggleFamily;
    Transform m_tranParentView;

    Transform m_transBtnParent;
    GameObject m_goFindFriend;
    GameObject m_goLookOver;
    GameObject m_goFollow;
    GameObject m_goToBlack;
    GameObject m_goChat;
    GameObject m_goIgnore;
    GameObject m_goAgree;
    GameObject m_goAllAdd;
    GameObject m_goAdd;
    GameObject m_goRemove;
    GameObject m_goDelete;
    //GameObject m_student;
    GameObject m_friend;
    GameObject m_goView;
    //Image m_stuTri;
    Image m_friTri;
    
    GameObject m_list;
    Text m_textOnlineRate;
    Text m_masterOnlineRate;
    FriendData m_curSelected;
    Vector3 m_loc;
    bool m_friendopen;
    bool m_studentopen;
    Text m_friendnum;
    Text m_friendtext;
    Text m_applytext;
    Text m_recotext;
    Text m_blacktext;
    Text m_familytext;
   
    
    GameObject m_bubble;
    bool m_hasapply;
    bool m_hasselect;
    bool m_isblack;
    bool m_isListEmpty;//目前显示的列表是否为空
    FriendTagsType m_currentTagsType;
    DataGrid m_datagrid;
    public GameObject dropTarget;
    public PanelFriend()
    {
        SetPanelPrefabPath("UI/Friend/PanelFriend");
        AddPreLoadRes("UI/Friend/PanelFriend", EResType.UI);

        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }
    public override void Init()
    {
        //m_masters = PlayerFriendData.singleton.m_masters;
        //m_students = PlayerFriendData.singleton.m_students;
        m_isblack = false;
        m_hasselect = false;
        m_listFriend = new List<ItemFriend>();
        m_listRecommand = new List<ItemFriend>();
        m_listBlack = new List<ItemFriend>();
        m_listRely = new List<ItemFriend>();
        m_friendopen = true;
        m_studentopen = false;
        m_hasapply = false;
        m_bubble = m_tran.Find("frame/left/friendreply_bubble").gameObject;
        m_toggleFriend = m_tran.Find("frame/left/tabs/friendlist").GetComponent<Toggle>();
        m_toggleReply = m_tran.Find("frame/left/tabs/friendreply").GetComponent<Toggle>();
        m_toggleRecommand = m_tran.Find("frame/left/tabs/friendrecommand").GetComponent<Toggle>();
        m_toggleBlack = m_tran.Find("frame/left/tabs/blacklist").GetComponent<Toggle>();
        m_toggleMaster = m_tran.Find("frame/left/tabs/masterlist").GetComponent<Toggle>();
        m_toggleFamily = m_tran.Find("frame/left/tabs/familylist").GetComponent<Toggle>();
        m_friendtext = m_tran.Find("frame/left/tabs/friendlist/Label").GetComponent<Text>();
        m_applytext = m_tran.Find("frame/left/tabs/friendreply/Label").GetComponent<Text>();
        m_recotext = m_tran.Find("frame/left/tabs/friendrecommand/Label").GetComponent<Text>(); ;
        m_blacktext = m_tran.Find("frame/left/tabs/blacklist/Label").GetComponent<Text>(); ;
        m_masterOnlineRate = m_tran.Find("frame/left/tabs/masterlist/Label").GetComponent<Text>();

        m_tranParentView = m_tran.Find("frame/left/ScrollRect/content");
        m_list = m_tran.Find("frame/left/ScrollRect").gameObject;
        m_transBtnParent = m_tran.Find("frame/right/buttonlist");
        m_goFindFriend = m_tran.Find("frame/right/findfriend").gameObject;
        m_goLookOver = m_tran.Find("frame/right/lookover").gameObject;
        m_goFollow = m_tran.Find("frame/right/follow").gameObject;
        m_goToBlack = m_tran.Find("frame/right/toblack").gameObject;
        m_goChat = m_tran.Find("frame/right/chat").gameObject;
        m_goIgnore = m_tran.Find("frame/right/ignore").gameObject;
        m_goAgree = m_tran.Find("frame/right/agree").gameObject;
        m_goAllAdd = m_tran.Find("frame/right/alladd").gameObject;
        m_goAdd = m_tran.Find("frame/right/add").gameObject;
        m_goRemove = m_tran.Find("frame/right/remove").gameObject;
        //m_goView = m_tran.Find("frame/right/view").gameObject;
        m_goDelete = m_tran.Find("frame/right/delete").gameObject;

        m_textOnlineRate = m_tran.Find("frame/left/tabs/friendlist/Label").GetComponent<Text>();
        m_familytext = m_tran.Find("frame/left/tabs/familylist/Label").GetComponent<Text>();
        m_datagrid = m_tranParentView.gameObject.AddComponent<DataGrid>();
        m_datagrid.SetItemRender(m_tran.Find("frame/left/ScrollRect/line").gameObject, typeof(ItemFriend));
        m_datagrid.autoSelectFirst = true;
        m_datagrid.useLoopItems = true;
        m_loc = m_list.transform.localPosition;
        m_toggleFriend.isOn = true;
        m_currentTagsType = FriendTagsType.Friend;
        dropTarget = m_tran.Find("dropTarget").gameObject;
        FlushFriendList();
        FriendInit();

    }

    void friendshow(GameObject target, PointerEventData eventData)
    {
        m_friendopen = !m_friendopen;
        RotateTri(m_friTri, m_friendopen);
        m_list.TrySetActive(!m_list.activeSelf);
    }

    void studentshow(GameObject target, PointerEventData eventData)
    {
        m_studentopen = !m_studentopen;
        //RotateTri(m_stuTri, m_studentopen);
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/friendlist")).onPointerClick += OnClickShowFriendList;
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/friendreply")).onPointerClick += OnClickShowReplyList;
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/friendrecommand")).onPointerClick += OnClickShowRecommandList;
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/blacklist")).onPointerClick += OnClickShowBlackList;
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/masterlist")).onPointerClick += OnClickShowMasterList;
        UGUIClickHandler.Get(m_tran.Find("frame/left/tabs/familylist")).onPointerClick += OnClickShowFamilyList;
        //UGUIClickHandler.Get(m_friend).onPointerClick+=friendshow;
        // UGUIClickHandler.Get(m_student).onPointerClick += studentshow; 
        //UGUIClickHandler.Get(m_goFindFriend).onPointerClick += OnClickFindFriend;
        //UGUIClickHandler.Get(m_goLookOver).onPointerClick += OnClickLookOver;
        //UGUIClickHandler.Get(m_goFollow).onPointerClick += OnClickFollow;
        //UGUIClickHandler.Get(m_goToBlack).onPointerClick += OnClickToBlack;
        //UGUIClickHandler.Get(m_goChat).onPointerClick += OnClickChat;
        //UGUIClickHandler.Get(m_goIgnore).onPointerClick += OnClickIgnore;
        //UGUIClickHandler.Get(m_goAgree).onPointerClick += OnClickAgree;
        //UGUIClickHandler.Get(m_goAllAdd).onPointerClick += OnClickAllAdd;
        //UGUIClickHandler.Get(m_goAdd).onPointerClick += OnClickAdd;
        //UGUIClickHandler.Get(m_goRemove).onPointerClick += OnClickRemove;
        //UGUIClickHandler.Get(m_goDelete).onPointerClick += OnClickDelete;
        //UGUIClickHandler.Get(m_goView).onPointerClick += OnClickView;
        UGUIClickHandler.Get(m_tran.Find("findBtn")).onPointerClick += OnClickFindFriend;
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
        m_datagrid.onItemSelected += SetCurItem;
        AddEventListener(PlayerFriendData.FRIEND_DATA_UPDATE, OnFriendDataUpdate);
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    public void UpdateBubble()
    {
        if (m_bubble)
            m_bubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.FriendApply));
    }

    public void SetCurItem(object renderdata)
    {        
        if (renderdata != null)
        {
            var data = renderdata as FriendInfo;
            m_curSelected = data.m_data;
            if (data.IsMaster != 0)
            {
                m_goDelete.gameObject.TrySetActive(false);
                m_goToBlack.gameObject.TrySetActive(false);
            }
            else
            {
                m_goDelete.gameObject.TrySetActive(true);
                m_goToBlack.gameObject.TrySetActive(true);
            }
        }
    }

    public override void OnShow()
    {
        UpdateBubble();

        m_toggleFriend.isOn = true;
        m_currentTagsType = FriendTagsType.Friend;
        FlushFriendList();
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }


    public override void OnHide()
    {

    }
    public override void OnBack()
    {
        UIManager.ShowPanel<PanelSocity>();
    }
    public override void OnDestroy()
    {
    }

    public override void Update()
    {

    }

    public void RotateTri(Image tri, bool bo)
    {
        if (bo)
        {
            tri.transform.Rotate(0, 0, -90);
        }
        else
        {
            tri.transform.Rotate(0, 0, 90);
        }
    }


    
    void FlushFriendList()
    {
        m_isblack = false;
        FlushOnlienFriendNum();
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goChat);
//        AddBtnToButtonList(m_goToBlack);
//        AddBtnToButtonList(m_goFollow);
//        AddBtnToButtonList(m_goLookOver);
//        AddBtnToButtonList(m_goFindFriend);
//        AddBtnToButtonList(m_goDelete);
//        AddBtnToButtonList(m_goView);
        FlushStyle1List(PlayerFriendData.singleton.friendList, m_listFriend);
    }

    void FlushMasterList()
    {
        m_isblack = false;
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goChat);
//        AddBtnToButtonList(m_goFollow);
//        AddBtnToButtonList(m_goLookOver);
//        AddBtnToButtonList(m_goView);
        FlushMasterList(PlayerFriendData.singleton.friendList, m_listFriend);
    }


    void FlushFamilyList()
    {
        m_isblack = false;
        FlushOnlienFriendNum();
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goChat);
//        AddBtnToButtonList(m_goToBlack);
//        AddBtnToButtonList(m_goFollow);
//        AddBtnToButtonList(m_goLookOver);
//        AddBtnToButtonList(m_goFindFriend);
//        AddBtnToButtonList(m_goDelete);
//        AddBtnToButtonList(m_goView);
        FlushStyle1List(PlayerFriendData.singleton.familyList, m_listFriend);
    }


    void FlushRecommandList()
    {
        m_isblack = false;
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goChat);
//        AddBtnToButtonList(m_goAdd);
//        AddBtnToButtonList(m_goAllAdd);
//        AddBtnToButtonList(m_goLookOver);
        m_currentTagsType = FriendTagsType.Recommend;
        FlushStyle1List(PlayerFriendData.singleton.recommandList, m_listRecommand);

    }
    void FlushReplyList()
    {
        m_currentTagsType = FriendTagsType.Apply;
        m_isblack = false;
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goAgree);
//        AddBtnToButtonList(m_goIgnore);
//        AddBtnToButtonList(m_goChat);
//        AddBtnToButtonList(m_goLookOver);
        FlushStyle1List(PlayerFriendData.singleton.replyList, m_listRely, 1);
    }

    void FlushBlackList()
    {
        m_isblack = true;
//        DisactiveButtonList();
//        AddBtnToButtonList(m_goRemove);
//        AddBtnToButtonList(m_goLookOver);
        m_currentTagsType = FriendTagsType.BlackList;
        FlushStyle1List(PlayerFriendData.singleton.blackList, m_listBlack, 2);
    }

    void FlushMasterList(List<FriendData> dataList, List<ItemFriend> itemList, int type = 0)
    {
        var masters = PlayerFriendData.singleton.m_masters;
        var students = PlayerFriendData.singleton.m_students;
        bool style = true;
        if (type != 0)
        {
            style = false;
        }

        List<FriendInfo> fi = new List<FriendInfo>();
        for (int i = 0; i < dataList.Count; i++)
        {
            FriendInfo fri = new FriendInfo();
            fri.m_data = dataList[i];
            fri.m_isStyle = style;
            fri.m_des = type;
            fri.IsMaster = PlayerFriendData.singleton.CheckIsMasterOrStudent(fri.m_data.m_id);
            fri.tags = m_currentTagsType;
            if (fri.IsMaster > 0)
            {
                //fri.m_des = txt;
                fi.Add(fri);
            }
        }
        if (fi.Count != 0)
        {
            MasterSort(masters, fi);
            m_datagrid.Data = fi.ToArray();
        }
        else
        {
            m_datagrid.Data = new object[0];
        }
        m_hasselect = false;
        if (m_datagrid.Data.Length != 0)
        {
            m_datagrid.Select(0);
            var info = m_datagrid.Data[0] as FriendInfo;
            m_curSelected = info.m_data;
        }
        if (dataList.Count == 0)
        {
            m_isListEmpty = true;
        }
        else
        {
            m_isListEmpty = false;
        }


    }


    void MasterSort(List<p_master_base_info_toc> masters, List<FriendInfo> list)
    {
        List<FriendInfo> li = new List<FriendInfo>();        
        if (masters.Count > 0)
        {
            for (int i = 0; i < list.Count; i++)
            {

                if (list[i].m_data.m_id == masters[0].id)
                {
                    li.Add(list[i]);
                    list.RemoveAt(i);
                    break;
                }
            }
        }
        li.AddRange(list);
        list.Clear();
        list.AddRange(li);

    }
    List<FriendInfo> m_friendList;
    void FlushStyle1List(List<FriendData> dataList, List<ItemFriend> itemList, int type = 0)
    {
        bool style = true;
        if (type != 0)
        {
            style = false;
        }
        List<FriendInfo> fi = new List<FriendInfo>();
        for (int i = 0; i < dataList.Count; i++)
        {
            FriendInfo fri = new FriendInfo();
            fri.m_data = dataList[i];
            fri.m_isStyle = style;
            fri.m_des = type;
            fri.tags = m_currentTagsType;
            //fri.m_des = txt;
            fi.Add(fri);
        }
        SortList(fi);
        if (m_currentTagsType != FriendTagsType.Family)
        {
            RemoveMasterAndStudent(fi);
            RemoveFamily(fi);
        }
        if (fi.Count != 0)
        {
            m_datagrid.Data = fi.ToArray();
            m_friendList = fi;
        }
        else
        {
            m_datagrid.Data = new object[0];
        }
        m_hasselect = false;
        if (m_datagrid.Data.Length != 0)
        {
            m_datagrid.Select(0);
            var info = m_datagrid.Data[0] as FriendInfo;
            m_curSelected = info.m_data;
        }
        if (dataList.Count == 0)
        {
            m_isListEmpty = true;
        }
        else
        {
            m_isListEmpty = false;
        }
    }

    void FriendInit()
    {

    }

    void UnFriendInit()
    {
    }

    void OnClickShowFriendList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        m_currentTagsType = FriendTagsType.Friend;
        FriendInit();
        FlushFriendList();
    }

    void OnClickShowReplyList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        m_currentTagsType = FriendTagsType.Apply;
        UnFriendInit();
        FlushReplyList();
    }

    void OnClickShowRecommandList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        UnFriendInit();
        m_currentTagsType = FriendTagsType.Recommend;
        PlayerFriendData.RcmndList();
    }


    void OnClickShowMasterList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        m_currentTagsType = FriendTagsType.Master;
        FlushMasterList();
    }

    void OnClickShowFamilyList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        m_currentTagsType = FriendTagsType.Family;
        FlushFamilyList();
    }

    
    void Toc_player_friend_rcmnd_list(toc_player_friend_rcmnd_list data)
    {
        PlayerFriendData.singleton.FlushList(data.list, PlayerFriendData.singleton.recommandList);
        FlushRecommandList();
    }
    void OnClickShowBlackList(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        UnFriendInit();
        FlushBlackList();
    }

    void OnClickView(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
            return;

        RoomModel.FollowFriend(m_curSelected.m_id, false, true);
    }

    void OnClickFindFriend(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        UIManager.PopPanel<PanelFindFriend>(null, true, this);
    }

    void OnClickLookOver(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
        {
            return;
        }
        PlayerFriendData.ViewPlayer(m_curSelected.m_id);
    }
    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }
    void OnClickDelete(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
        {
            return;
        }
        //fjs>>2015.10.13 删除增加二次确认操作 
        UIManager.ShowTipPanel("您确定删除<Color='#996600'> " + m_curSelected.m_name + " </Color>好友吗？", SureDelete, CancelDelete, false, true, "确定", "取消");
        //  PlayerFriendData.AddBlack(m_curSelected.m_id);
        // PlayerFriendData.DelBlack(m_curSelected.m_id);
        //        PlayerFriendData.SendFriendMsg("add_black", m_curSelected.m_id);
        //        PlayerFriendData.SendFriendMsg("del_black", id);
    }

    void SureDelete()
    {
        //PlayerFriendData.AddBlack(m_curSelected.m_id);
        //PlayerFriendData.DelBlack(m_curSelected.m_id);
        PlayerFriendData.DelFriend(m_curSelected.m_id);
    }

    void CancelDelete()
    {

    }

    void OnClickFollow(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
            return;

        RoomModel.FollowFriend(m_curSelected.m_id, false, false);
    }

    void OnClickToBlack(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
        {
            return;
        }
        //fjs>>2015.10.13 拉黑增加二次确认操作 
        UIManager.ShowTipPanel("您确定<Color='#996600'> " + m_curSelected.m_name + " </Color>好友拉黑吗？", SureBlack, CancelDelete, false, true, "确定", "取消");
        //NetChatData.AddBlack(m_curSelected.m_id);
    }

    void SureBlack()
    {
        NetChatData.AddBlack(m_curSelected.m_id);
    }

    void Toc_player_add_black(toc_player_add_black data)
    {
        FlushFriendList();
    }


    void OnClickChat(GameObject target, PointerEventData eventData)
    {
        if (m_isListEmpty)
        {
            return;
        }
        //bool isFriend = true;//m_listFriend.IndexOf(m_curSelected) > -1 ? true : false;
        //if (m_curSelected.m_state == FRIEND_STATE.OFFLINE)
        //{
        //    UIManager.ShowTipPanel("对方不在线");
        //     return;
        // }
        bool isFriend = PlayerFriendData.singleton.isFriend(m_curSelected.m_id);
        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = m_curSelected.m_id;
        rcd.m_strName = m_curSelected.m_name;
        rcd.m_iSex = m_curSelected.m_sex;
        rcd.m_iLevel = m_curSelected.m_level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = m_curSelected.m_icon;
        ChatMsgTips msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = { msgTips };
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, this);
    }

    void OnClickIgnore(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_refuse_fan { fan_id = m_curSelected.m_id });
    }

    void Toc_player_refuse_fan(toc_player_refuse_fan data)
    {
        FlushReplyList();
    }

    void OnClickAgree(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_agree_fan { fan_id = m_curSelected.m_id });
    }
    void Toc_player_agree_fan(toc_player_agree_fan data)
    {
        FlushOnlienFriendNum();
        FlushReplyList();
    }

    void OnClickAllAdd(GameObject target, PointerEventData eventData)
    {
        List<FriendData> recommandList = PlayerFriendData.singleton.recommandList;
        for (int index = 0; index < recommandList.Count; ++index)
        {
            FriendData friendData = recommandList[index];
            PlayerFriendData.AddFriend(friendData.m_id);
        }
    }

    void OnClickAdd(GameObject target, PointerEventData eventData)
    {
        PlayerFriendData.AddFriend(m_curSelected.m_id);        
    }

    void Toc_player_add_friend(toc_player_add_friend data)
    {
        var friendid = data.friend_id;
        List<FriendData> recommandList = PlayerFriendData.singleton.recommandList;
        recommandList.RemoveAll(x => x.m_id == friendid);
        if (m_toggleRecommand.isOn)
        {
            FlushRecommandList();
        }
    }

    void OnClickRemove(GameObject target, PointerEventData eventData)
    {
        PlayerFriendData.DelBlack(m_curSelected.m_id);
    }

    void Toc_player_del_black(toc_player_del_black data)
    {
        if (m_isblack)
            FlushBlackList();
    }

//    void DisactiveButtonList()
//    {
//
//        for (int index = 0; index < m_transBtnParent.childCount; )
//        {
//            Transform child = m_transBtnParent.GetChild(index);
//            child.SetParent(null, false);
//            child.gameObject.TrySetActive(false);
//            index = 0;
//        }
//    }
//
//    void AddBtnToButtonList(GameObject goBtn)
//    {
//        if (goBtn == null)
//            return;
//        goBtn.TrySetActive(true);
//        goBtn.transform.SetParent(m_transBtnParent, false);
//    }

    public void SetCurSelectedItem(ItemFriend seledItem)
    {
        m_hasselect = true;
    }

    void OnFriendDataUpdate()
    {
        FlushOnlienFriendNum();
        CheckApply();
        if (m_toggleFriend.isOn)
        {
            FlushFriendList();
        }
        if (m_toggleReply.isOn)
        {
            FlushReplyList();
        }
        if (m_toggleRecommand.isOn)
        {
            FlushRecommandList();
        }

        if (m_toggleBlack.isOn)
        {
            FlushBlackList();
        }
        if (m_toggleFamily.isOn)
        {
            FlushFamilyList();
        }
    }

    void FlushOnlienFriendNum()
    {
        var masters = PlayerFriendData.singleton.m_masters;
        var students = PlayerFriendData.singleton.m_students;
        int OnlineNum = 0;
        int MasterOnline = 0;
        var FamilyOnlineNum = 0;
       
        List<FriendData> friendList = PlayerFriendData.singleton.friendList;
        List<FriendData> familyList = PlayerFriendData.singleton.familyList;
        var FriendCount = friendList.Count;
        for (int index = 0; index < friendList.Count; ++index)
        {
            FriendData friendData = friendList[index];
            var isMaster = PlayerFriendData.singleton.CheckIsMasterOrStudent(friendData.m_id) != 0;
            var isFamily = FamilyDataManager.Instance.IsFamilyMember(friendData.m_id);
            if (isMaster || isFamily)
            {
                --FriendCount;
            }
            if (friendData.m_state != FRIEND_STATE.OFFLINE)
            {
                if (isMaster)
                {
                    ++MasterOnline;
                    if (isFamily)
                    {
                        ++FamilyOnlineNum;
                    }
                }
                else if(isFamily)
                {
                    ++FamilyOnlineNum;
                }
                else
                {
                    ++OnlineNum;
                }
            }
        }

        m_textOnlineRate.text = string.Format("好友{0}/{1}", OnlineNum, Mathf.Max(0, FriendCount));
        m_masterOnlineRate.text = string.Format("师徒{0}/{1}", MasterOnline, students.Count+masters.Count);
        m_familytext.text = String.Format("家族{0}/{1}", FamilyOnlineNum,familyList.Count);
        //m_friendnum.text = m_textOnlineRate.text;
    }

    //检查申请中是否有已经成为好友的用户
    public void CheckApply()
    {
        for (int i = 0; i < m_listRely.Count; i++)
        {
            if (m_listFriend.Contains(m_listRely[i]))
            {
                m_listRely.RemoveAt(i);
            }
        }

        if (m_listRely.Count == 0)
        {
            m_bubble.TrySetActive(false);
        }
    }



    //按照在线-不在线的顺序排列
    public void SortList(List<FriendInfo> list)
    {
        List<FriendInfo> onlinelist = new List<FriendInfo>();
        List<FriendInfo> offlinelist = new List<FriendInfo>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].m_data.m_state == FRIEND_STATE.OFFLINE)
            {
                offlinelist.Add(list[i]);
            }
            else
            {
                onlinelist.Add(list[i]);
            }
        }
        list.Clear();
        for (int i = 0; i < onlinelist.Count; i++)
        {
            list.Add(onlinelist[i]);
        }
        for (int i = 0; i < offlinelist.Count; i++)
        {
            list.Add(offlinelist[i]);
        }
    }

    public void SetSelectedItem(FriendInfo info)
    {
        int index = m_friendList.IndexOf(info);
        if (index != -1)
            m_datagrid.Select(index);
        m_datagrid.Select(info);
    }


    public void DoOption(string subType,FriendInfo info)
    {
        FriendData data = info.m_data;
        m_curSelected = data;
        if (subType == FriendDropST.Add)
            OnClickAdd(null, null);
        else if (subType == FriendDropST.Agree)
            OnClickAgree(null, null);
        else if (subType == FriendDropST.Chat)
            OnClickChat(null, null);
        else if (subType == FriendDropST.Defriend)
            OnClickToBlack(null, null);
        else if (subType == FriendDropST.Del)
            OnClickDelete(null, null);
        else if (subType == FriendDropST.Follow)
            OnClickFollow(null, null);
        else if (subType == FriendDropST.Ignore)
            OnClickIgnore(null, null);
        else if (subType == FriendDropST.LookUp)
            OnClickLookOver(null, null);
        else if (subType == FriendDropST.Remove)
            OnClickRemove(null,null);
    }

    //移除好友列表中的师父和徒弟
    public void RemoveMasterAndStudent(List<FriendInfo> list)
    {
        var masters = PlayerFriendData.singleton.m_masters;
        if (masters.Count > 0)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].m_data.m_id == masters[0].id)
                {
                    list.RemoveAt(i);
                    break;
                }
            }
        }
        var students = PlayerFriendData.singleton.m_students;
        for (int j = 0; j < students.Count; j++)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].m_data.m_id == students[j].id)
                {
                    list.RemoveAt(i);
                    break;
                }
            }
        }
    }

    //移除好友列表中的家族成员
    public void RemoveFamily(List<FriendInfo> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (FamilyDataManager.Instance.IsFamilyMember(list[i].m_data.m_id))
            {
                list.RemoveAt(i);
            }
        }
    }

    void Toc_player_del_friend(toc_player_del_friend data)
    {
        long id = data.friend_id;
        var list = PlayerFriendData.singleton.friendList;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].m_id == id)
            {
                list.RemoveAt(i);
                break;
            }
        }
        FlushFriendList();
    }
}


