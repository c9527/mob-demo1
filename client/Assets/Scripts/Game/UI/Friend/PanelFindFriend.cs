﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelFindFriend : BasePanel 
{
    RectTransform m_rectTransInput;

    InputField m_inputTxt;

    public PanelFindFriend()
    {
        SetPanelPrefabPath("UI/Friend/PanelFindFriend");
        AddPreLoadRes("UI/Friend/PanelFindFriend", EResType.UI);

        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
    }

    public override void Init()
    {
        m_inputTxt = m_tran.Find("frame/InputField").GetComponent<InputField>();
        m_inputTxt.gameObject.AddComponent<InputFieldFix>();

    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/InputField")).onPointerClick += OnClickInput;
        UGUIClickHandler.Get(m_tran.Find("frame/close")).onPointerClick += OnClickClose;
        UGUIClickHandler.Get(m_tran.Find("frame/add")).onPointerClick += OnClickAdd;
    }
    public override void OnShow()
    {

    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/InputField")).onPointerClick -= OnClickInput;
    }
    public override void Update()
    {

    }

    void OnClickInput( GameObject target, PointerEventData eventData )
    {
        if( m_rectTransInput == null )
        {
            if (m_tran == null) return;

            m_rectTransInput = m_tran.Find("frame/InputField/InputField Input Caret") as RectTransform;
            Vector2 anchor = new Vector2(0.5f, 0.5f);

            if (m_rectTransInput == null) return;
                
            m_rectTransInput.anchorMin = anchor;
            m_rectTransInput.anchorMax = anchor;
            Vector2 pos = m_rectTransInput.anchoredPosition;
            //pos.y = -15;
            m_rectTransInput.anchoredPosition = pos;
        }
        
    }

    void OnClickClose( GameObject target, PointerEventData eventData )
    {
        HidePanel();
    }

    void OnClickAdd( GameObject target, PointerEventData eventData )
    {
        string nameorId = m_inputTxt.text;

        long isId;
        if (long.TryParse(nameorId, out isId))
            PlayerFriendData.ViewPlayer(isId);
        else
            PlayerFriendData.ViewPlayer(nameorId);
        HidePanel();
    }

    
}
