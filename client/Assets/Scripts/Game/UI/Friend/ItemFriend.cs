﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemFriend : ItemRender
{
    Image m_headicon;
    Image m_headVip;
    Image m_armylvl;
    Text m_lvl;
    Text m_name;
    Text m_viplvl;
    Image m_imgState;
    Text m_mvp;
    Text m_goldace;
    Text m_sliverace;
    Image m_sexImg;
    public FRIEND_STATE m_state;
    GameObject m_record;
    GameObject m_desc;
    Text m_destxt;
    Image m_master;
    Image m_desImage;
    private GameObject m_onlineLogo;
    private Text m_onlineText;
    public long m_id;
    public string m_namestr;
    public int m_level;
    public int m_icon;
    public int m_sex;
    public string m_desctxt;
    UIEffect _headEffect;
    Image m_title1;
    Image m_title2;
    Image m_title3;

    

    /// <summary>
    /// 标签页类型，0~4分别为好友、师徒、好友申请、好友推荐、黑名单
    /// </summary>
    public FriendTagsType type;
    private FriendInfo m_info;
    /// <summary>
    /// 详细功能
    /// </summary>
    private static Dictionary<FriendTagsType, ItemDropInfo[]> _dropListDic;
    private static Dictionary<FriendTagsType, ItemDropInfo[]> m_dropListDic
    {
        get
        {
            if(_dropListDic==null)
            {
                FriendDropTypes types=new FriendDropTypes();
                _dropListDic = new Dictionary<FriendTagsType,ItemDropInfo[]>();
                _dropListDic.Add(FriendTagsType.Friend, new ItemDropInfo[] { types.Chat,types.Follow, types.Del, types.LookUp, types.Defriend }); //好友
                _dropListDic.Add(FriendTagsType.Master, new ItemDropInfo[] { types.Chat, types.Follow, types.LookUp }); //师徒
                _dropListDic.Add(FriendTagsType.Apply, new ItemDropInfo[]{ types.Agree, types.Ignore, types.LookUp, types.Chat }); //好友申请
                _dropListDic.Add(FriendTagsType.Recommend, new ItemDropInfo[]{ types.Chat, types.LookUp, types.Add }); //好友推荐
                _dropListDic.Add(FriendTagsType.BlackList, new ItemDropInfo[] { types.LookUp, types.Remove }); //黑名单
                _dropListDic.Add(FriendTagsType.Family, new ItemDropInfo[] { types.Chat, types.Follow, types.LookUp }); //家族
            }
            return _dropListDic;
        }
    }
    Image m_imgVip;
    GameObject m_goVip;
    GameObject m_goName;
    Vector2 m_anchorName;
    Text m_logtime;
    Text m_familyRelativeName;
    List<Image> m_optionList=new List<Image>();

    public override void Awake()
    {
        var tran = transform;
        m_headicon = tran.Find("headframe/head").GetComponent<Image>();
        m_headVip = tran.Find("headframe/heroVip").GetComponent<Image>();
        m_armylvl = tran.Find("level/imgArmy").GetComponent<Image>();
        m_lvl = tran.Find("level/Text").GetComponent<Text>();
        m_name = tran.Find("namebk/name").GetComponent<Text>();
        m_viplvl = tran.Find("viplvl").GetComponent<Text>();
        m_imgState = tran.Find("state").GetComponent<Image>();
        m_mvp = tran.Find("recordinfo/achieve/Text").GetComponent<Text>();
        m_goldace = tran.Find("recordinfo/goldace/Text").GetComponent<Text>();
        m_sliverace = tran.Find("recordinfo/sliverace/Text").GetComponent<Text>();
        m_record = tran.Find("recordinfo").gameObject;
        m_desc = tran.Find("desc").gameObject;
        m_destxt = tran.Find("desc/Text").GetComponent<Text>();
        m_destxt.gameObject.TrySetActive(false);
        m_desImage = tran.Find("desc/Image").GetComponent<Image>();
        m_sexImg = tran.Find("sex").GetComponent<Image>();
        m_master = tran.Find("master").GetComponent<Image>();
        m_id = 0;
        m_goVip = tran.Find("VIP").gameObject;
        m_imgVip = tran.Find("VIP").GetComponent<Image>();
        m_goName = tran.Find("namebk/name").gameObject;
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition;
        m_logtime = tran.Find("info/Text").GetComponent<Text>();
        m_familyRelativeName = tran.Find("family/Text").GetComponent<Text>();
        Transform options = tran.Find("optionList");
        m_onlineText = tran.Find("online_text").GetComponent<Text>();
        m_onlineLogo = tran.Find("online_logo").gameObject;

        for (int i = 0; i < 3;i++ )
        {
            Transform tr=options.Find("option"+i.ToString());
            m_optionList.Add(tr.GetComponent<Image>());
            UGUIClickHandler.Get(tr).onPointerClick += OnClickHandler;
        }

        m_title1 = tran.Find("recordinfo/goldace").GetComponent<Image>();
        m_title2 = tran.Find("recordinfo/sliverace").GetComponent<Image>();
        m_title3 = tran.Find("recordinfo/achieve").GetComponent<Image>();
    }

    private void OnClickHandler(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        DataRecord data = target.GetComponent<DataRecord>();
        if (data == null)
            return;        
        string subType=data.data[0] as string;        
        UIManager.GetPanel<PanelFriend>().SetSelectedItem(m_info);                    
        if (subType == "Other")
        {
                var list = m_dropListDic[m_info.tags];
                UIManager.GetPanel<PanelFriend>().dropTarget.TrySetActive(true);
                //UIManager.ShowDropList(list, ItemDropCallBack, UIManager.GetPanel<PanelFriend>().transform, UIManager.GetPanel<PanelFriend>().dropTarget, 145, 39, false);
                UIManager.ShowDropList(list, ItemDropCallBack, UIManager.GetPanel<PanelFriend>().transform, UIManager.GetPanel<PanelFriend>().dropTarget.transform, new Vector2(0, 0), UIManager.Direction.Down, 145, 39, UIManager.DropListPattern.Ex);
        }
        else
        {            
            DoOption(subType);
        }
    }

    private void ItemDropCallBack(ItemDropInfo obj)
    {
        DoOption(obj.subtype);
    }


    private void DoOption(string subType)
    {
        PanelFriend panel = UIManager.GetPanel<PanelFriend>();
        if (panel != null)
            panel.DoOption(subType,m_info);
        UIManager.GetPanel<PanelFriend>().dropTarget.TrySetActive(false);
    }
    


    protected override void OnSetData(object data)
    {
        var friendinfo = data as FriendInfo;
        m_info = friendinfo;
        m_logtime.text = TimeUtil.LastLoginString(friendinfo.m_data.logouttime);
        m_headicon.SetSprite(ResourceManager.LoadRoleIcon(friendinfo.m_data.m_icon));
        m_armylvl.SetSprite(ResourceManager.LoadArmyIcon(friendinfo.m_data.m_level));
        m_lvl.text = string.Format("{0}级", friendinfo.m_data.m_level);
        m_name.text = PlayerSystem.GetRoleNameFull(friendinfo.m_data.m_name, friendinfo.m_data.m_id);
        m_viplvl.text = string.Format("VIP {0}", friendinfo.m_data.m_vip_level);
        SetStateImg(friendinfo.m_data);
        var heroType = HeroCardDataManager.getHeroCardTypeByCardInfo(friendinfo.m_data.hero_cards);
        if (m_info.tags == FriendTagsType.Family)
        {
            m_familyRelativeName.transform.parent.gameObject.TrySetActive(true);
            m_familyRelativeName.text = FamilyDataManager.Instance.GetRelativeName(m_info.m_data.m_id);
        }
        else
        {
            m_familyRelativeName.transform.parent.gameObject.TrySetActive(false);
        }
        m_headVip.gameObject.TrySetActive(false);
        if (heroType != (int)EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroType == EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (_headEffect == null)
                {
                    _headEffect = UIEffect.ShowEffect(m_headVip.transform, EffectConst.UI_HEAD_HERO_CARD);
                }
            }
            if(friendinfo.m_data.m_state == FRIEND_STATE.OFFLINE)
            {
                m_headVip.gameObject.TrySetActive(false);
            }
        }

        m_mvp.text = friendinfo.m_data.m_mvp.ToString();
        m_goldace.text = friendinfo.m_data.m_goldace.ToString();
        m_sliverace.text = friendinfo.m_data.m_sliverace.ToString();
        m_id = friendinfo.m_data.m_id;
        m_namestr = friendinfo.m_data.m_name;
        m_level = friendinfo.m_data.m_level;
        m_sex = friendinfo.m_data.m_sex;
        if (m_sex == 0)
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
        }
        else
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
        }
        //m_destxt.text = friendinfo.m_des;
        if (friendinfo.m_isStyle)
        {
            m_desc.TrySetActive(false);
            m_record.TrySetActive(true);

        }
        else
        {
            m_desc.TrySetActive(true);
            m_record.TrySetActive(false);
        }
        if (friendinfo.IsMaster == 0)
        {
            m_master.gameObject.TrySetActive(false);
        }
        else if (friendinfo.IsMaster == 1)
        {
            m_master.gameObject.TrySetActive(true);
            m_master.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "masterIcon"));
        }
        else if (friendinfo.IsMaster == 2)
        {
            m_master.gameObject.TrySetActive(true);
            m_master.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "student"));
        }
        if (friendinfo.m_des == 1)
        {
            m_desImage.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "applytext"));
            m_desImage.SetNativeSize();
        }
        else if (friendinfo.m_des == 2)
        {
            m_desImage.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inblacktext"));
            m_desImage.SetNativeSize();
        }
        ShowOptions();
        if (friendinfo.m_data.set_titles != null)
        {
            if (friendinfo.m_data.set_titles.Length > 0)
            {
                SetTitleSprite(m_title1, friendinfo.m_data.set_titles[0]);
                m_goldace.text = friendinfo.m_data.set_titles[0].cnt.ToString();
                SetTitleSprite(m_title2, friendinfo.m_data.set_titles[1]);
                m_sliverace.text = friendinfo.m_data.set_titles[1].cnt.ToString();
                SetTitleSprite(m_title3, friendinfo.m_data.set_titles[2]);
                m_mvp.text = friendinfo.m_data.set_titles[2].cnt.ToString();
            }
            else
            {
                SetTitleSprite(m_title1, new p_title_info() { title = "gold_ace", type = 1 });
                SetTitleSprite(m_title2, new p_title_info() { title = "silver_ace", type = 1 });
                SetTitleSprite(m_title3, new p_title_info() { title = "mvp", type = 1 });
            }
        }
        else
        {
            SetTitleSprite(m_title1, new p_title_info() { title = "gold_ace" ,type=1});
            SetTitleSprite(m_title2, new p_title_info() { title = "silver_ace" ,type=1});
            SetTitleSprite(m_title3, new p_title_info() { title = "mvp", type = 1 });
        }

    }

    private void ShowOptions()
    {
        var drops = m_dropListDic[m_info.tags];
        if (drops.Length >= 1)
            ShowOptionST(m_optionList[0], drops[0].subtype);
        if (drops.Length >= 2)
            ShowOptionST(m_optionList[1], drops[1].subtype);
        if (drops.Length < 3)
            m_optionList[2].gameObject.TrySetActive(false);
        else if (drops.Length == 3)
            ShowOptionST(m_optionList[2], drops[2].subtype);
        else ShowOptionST(m_optionList[2], "", true);
    }

    void SetTitleSprite(Image image, p_title_info title)
    {
        var config = ConfigManager.GetConfig<ConfigTitle>().GetLine(title.title);
        image.SetSprite(ResourceManager.LoadSprite("PlayerInfo", config.icon[title.type - 1]));
    }


    private void ShowOptionST(Image img, string subType,bool showDetial=false)
    {
        DataRecord data = img.gameObject.AddMissingComponent<DataRecord>();
        if (showDetial)
        {
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "other"));
            data.data = new object[] { "Other" };
        }
        else
        {
            if (subType == FriendDropST.Chat)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "chat"));
            }
            else if (subType == FriendDropST.Follow)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "follow"));
            }
            else if (subType == FriendDropST.LookUp)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "checkout"));
            }
            else if (subType == FriendDropST.Agree)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "agree"));
            }
            else if (subType == FriendDropST.Ignore)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "ignore"));
            }
            else if (subType == FriendDropST.Add)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "add"));
            }
            else if (subType == FriendDropST.Remove)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "remove"));
            }
            data.data = new object[] { subType };
        }
        img.SetNativeSize();        
        img.gameObject.TrySetActive(true);
    }

    private void ClickHander(string p)
    {
        throw new System.NotImplementedException();
    }



    public void SetStateImg(FriendData friendData)
    {
        var friendState = friendData.m_state;

        if (friendState == FRIEND_STATE.INROOM)
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inroom"));
            m_imgState.gameObject.TrySetActive(true);
            m_onlineLogo.TrySetActive(true);
            m_onlineText.text = "<Color=#00FF00>在线</Color>";
            m_logtime.text = "在线";
            Util.SetGoGrayShader(gameObject, false);
        }
        else if (friendState == FRIEND_STATE.INRANK)
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inrank"));
            m_imgState.gameObject.TrySetActive(true);
            m_logtime.text = "在线";
            m_onlineLogo.TrySetActive(true);
            m_onlineText.text = "<Color=#00FF00>在线</Color>";
            Util.SetGoGrayShader(gameObject, false);
        }
        else if (friendState == FRIEND_STATE.INFIGHT)
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "fighting"));
            m_imgState.gameObject.TrySetActive(true);
            m_logtime.text = "在线";
            m_onlineLogo.TrySetActive(true);
            m_onlineText.text = "<Color=#00FF00>在线</Color>";
            Util.SetGoGrayShader(gameObject, false);
        }
        else if (friendState == FRIEND_STATE.ONLINE)
        {
//            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "online"));
            m_imgState.gameObject.TrySetActive(false);
            m_logtime.text = "在线";
            m_onlineLogo.TrySetActive(true);
            m_onlineText.text = "<Color=#00FF00>在线</Color>";
            Util.SetGoGrayShader(gameObject, false);
        }
        else if (friendState == FRIEND_STATE.OFFLINE)
        {
//            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "outline"));
            m_imgState.gameObject.TrySetActive(false);
            m_onlineLogo.TrySetActive(false);
            m_onlineText.text = TimeUtil.LastLoginString(friendData.logouttime);
            Util.SetGoGrayShader(gameObject, true);
        }
        m_imgState.SetNativeSize();
    }

}
public class FriendInfo
{
    public FriendData m_data;
    public bool m_isStyle;
    public int m_des = 0;
    public int IsMaster = 0; //0 无师徒关系,1 导师,2 徒弟

    public FriendTagsType tags; //对应所在的标签页

}


public class DataRecord:MonoBehaviour
{
    public object[] data;
}

public class FriendDropST
{
    public const string Del="删除";
    public const string LookUp="查看";
    public const string Follow="跟随";
    public const string Defriend="拉黑";
    public const string Chat="私聊";
    public const string Ignore="忽略";
    public const string Agree = "同意";
    public const string Add="添加";
    public const string Remove="移出";    
}

public enum FriendTagsType
{
    Friend,
    Master,
    Apply,
    Recommend,
    BlackList,
    Family
}

public class FriendDropTypes
{
    public ItemDropInfo Del = new ItemDropInfo() { subtype = FriendDropST.Del, label = FriendDropST.Del };    
    public ItemDropInfo LookUp = new ItemDropInfo() { subtype = FriendDropST.LookUp, label = FriendDropST.LookUp };
    public ItemDropInfo Follow = new ItemDropInfo() { subtype = FriendDropST.Follow, label = FriendDropST.Follow };
    public ItemDropInfo Defriend = new ItemDropInfo() { subtype = FriendDropST.Defriend, label = FriendDropST.Defriend };
    public ItemDropInfo Chat = new ItemDropInfo() { subtype = FriendDropST.Chat, label = FriendDropST.Chat };
    public ItemDropInfo Ignore = new ItemDropInfo() { subtype = FriendDropST.Ignore, label = FriendDropST.Ignore };
    public ItemDropInfo Agree = new ItemDropInfo() { subtype = FriendDropST.Agree, label = FriendDropST.Agree };
    public ItemDropInfo Add = new ItemDropInfo() { subtype = FriendDropST.Add, label = FriendDropST.Add };
    public ItemDropInfo Remove = new ItemDropInfo() { subtype = FriendDropST.Remove, label = FriendDropST.Remove };    
}


