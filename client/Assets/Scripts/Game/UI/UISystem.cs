﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public static class UISystem
{
    //public static SettleNetProxy m_settleNet = new SettleNetProxy();
    /// <summary>
    /// 房间是否最小化
    /// </summary>
    public static bool isMinimizeRoom = false;
    public static Transform panelRoomParent;
    public static bool isNormalBack = false;
    public static bool isShowRecharge = false;
    public static void Init()
    {
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnLoadBattleScene);
        GameDispatcher.AddEventListener(GameEvent.GAME_HALL_SCENE_LOADED, OnHallSceneLoaded);
        GameDispatcher.AddEventListener(GameEvent.LOGIN_SUCCESS, OnLoginSucess);
        GameDispatcher.AddEventListener(GameEvent.UI_SETTLE_END, OnSettleEnd);
    }    
    private static void OnLoadBattleScene()
    {
        if (WorldManager.singleton.fightEntered)//防止玩家在游戏结束时进入导致的边间错误
        {
            if (!Driver.isMobilePlatform)
            {
                UIManager.fullScreen = true;
            }
            UIManager.DestroyAllPanel();
            ResourceManager.GarbageCollect();
            
            UIManager.ShowPanel<PanelBattle>();
            if( FightVideoManager.isPlayFight == true)
            {
                Logger.Log("FightVideoManager------tos_record_start_watch" );
                NetLayer.Send(new tos_record_start_watch() { by_center = FightVideoManager.ByCenter()});
            }
            AudioManager.StopBgMusic();
            if(PanelBattleSurvivalScore.listBoss.Count==0)
            {
                AudioManager.PlayBgMusic();
            }                            
        }
    }

    private static void OnHallSceneLoaded()
    {
        if (Driver.m_platform==EnumPlatform.web)
        {
            UIManager.fullScreen = false;
        }
        UIManager.DestroyAllPanel();
        ResourceManager.GarbageCollect();
        if( FightVideoManager.isPlayFight == false )
        {
            if (RoomModel.ChannelType == ChannelTypeEnum.none)
                return;
            if (RoomModel.ChannelType == ChannelTypeEnum.survival || RoomModel.ChannelType == ChannelTypeEnum.defend || RoomModel.ChannelType == ChannelTypeEnum.worldboss || RoomModel.ChannelType == ChannelTypeEnum.boss4newer)
                UIManager.ShowPanel<PanelSurvivalSettle>();
            else if (RoomModel.ChannelType == ChannelTypeEnum.stage_survival)
                UIManager.ShowPanel<PanelOldSurvivalSettle>();
            else
                UIManager.ShowPanel<PanelSettle>();
        }
        else
        {
            GameDispatcher.Dispatch(GameEvent.UI_SETTLE_END);
//             if( FightVideoManager.isCloseBySettingPanel == true )
//             {
                FightVideoManager.isPlayFight = false;
            //}
        }
    }

    private static void OnSettleEnd()
    {
        UIManager.DestroyAllPanel();
        UIManager.PopRootPanel<PanelBackground>();
        UIManager.PopRootPanel<PanelModel>();
        UIManager.PopPanel<PanelHall>();

        RoomModel.ShowBattleEndPanel();
        ShowPanelAfterBattleEnd();
        
        AudioManager.SetListener();
        AudioManager.PlayBgMusic("Hall_1");
    }


    private static void ShowPanelAfterBattleEnd()
    {
        if (FamilyDataManager.Instance.ApplyInfo != null)
        {
            UIManager.PopPanel<PanelFamilyApply>();
        }
        if (isShowRecharge)
        {
            SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
            isShowRecharge = false;
        }
       
    }

    private static void OnLoginSucess()
    {        
        UIManager.DestroyAllPanel();
        ResourceManager.GarbageCollect();

        var preloads = new List<ResourceItem>();
        preloads.AddRange(new PanelBackground().GetPreloadRes());
        preloads.AddRange(new PanelHall().GetPreloadRes());
        preloads.AddRange(new PanelHallBattle().GetPreloadRes());
        preloads.AddRange(new PanelModel().GetPreloadRes());
        UIManager.ShowLoadingWithProBar(preloads);
        UIManager.PopRootPanel<PanelBackground>();
        UIManager.PopRootPanel<PanelModel>();
        UIManager.PopPanel<PanelHall>();
        UIManager.ShowPanel<PanelHallBattle>(new object[1] { "login" });
         //if (!Driver.singleton.m_justCreateRole)
         //       UIManager.PopPanel<PanelEvent>(null, true, null);
        //NetLayer.Send(new tos_player_signin_data());                     
        //游戏数据获取
        NetLayer.Send(new tos_player_item_equip_data());
        NetLayer.Send(new tos_master_apprentice_info());
        NetLayer.Send(new tos_master_double_exp());
        FamilyDataManager.Instance.ReqFamilyList();
        PhotoDataManager.Instance.ReqUrlList();
        MicroBlogDataManager.Instance.ReqSelfInfo();
        PlayerSystem.TosGetDefaultRoles();
        PlayerSystem.TosGetBuffData();
        PlayerSystem.TosGetRechargeData();
        StrengthenModel.Instance.Tos_player_weapon_strengthen_data();
        StrengthenModel.Instance.Tos_player_weapon_train_info();
        StrengthenModel.Instance.Tos_player_weapon_accessory_data();

        tos_corps_get_data msg = new tos_corps_get_data();
        NetLayer.Send(msg);
        NetLayer.Send(new tos_master_apprentice_info());
        NetChatData.ResetData();

        PlayerAchieveData.RequestAchievement();
        EventModel.Instance.TosLottery();

        if (GlobalConfig.serverValue.cm_open && SdkManager.cm != 1)
        {
            if (SdkManager.cm == 2)
            {
                UIManager.PopPanel<PanelCM>(null,true);
            }
            //启动防沉迷
            PlayerSystem.WakeupCMSystem(false);
        }
    }

    

    static void Toc_player_err_msg(toc_player_err_msg proto)
    {
        var errMsg = Util.GetErrMsg(proto.err_code, proto.err_msg);

        if (proto.proto_type == "player" && proto.proto_key == "buy_item")
        {
            TipsManager.Instance.showTips(errMsg);
        }
        else if (SceneManager.singleton.battleSceneLoaded)
        {
            WorldManager.ShowTextMsgInPanel(errMsg, Color.blue);
        }
        else
        {
            if (proto.proto_type == "login" && proto.proto_key == "create")
            {
                TipsManager.Instance.showTips(errMsg);
                Driver.SendLogServer(1100, "code:{0} msg:{1}", proto.err_code, errMsg);
            }
            else if (proto.proto_type == "login" && proto.proto_key == "select")
            {
                UIManager.ShowTipPanel(errMsg);
                Driver.SendLogServer(1110, "code:{0} msg:{1}", proto.err_code, errMsg);
            }
            else if (proto.proto_type == "login" && proto.proto_key == "verify")
            {
                if (proto.err_code == 100002)
                {
                    Driver.SendLogServer(1108, "code:{0} msg:{1}", proto.err_code, errMsg);
                    if (Application.isWebPlayer)
                    {
                        UIManager.ShowTipPanel("登录信息过期，请刷新游戏(F5)", () => PlatformAPI.Call("reloadGame", SdkManager.ConvertGameServerID())).DontDestroyOnLoad = true;
                    }
                    else
                    {
                        UIManager.ShowTipPanel("登录信息过期，请重新启动游戏", Application.Quit).DontDestroyOnLoad = true;
                    }
                }
                else
                {
                    if (proto.err_code != 0)
                        Driver.SendLogServer(1107, "code:{0} msg:{1}", proto.err_code, errMsg);
                    var panel = UIManager.ShowTipPanel(errMsg);
                    if (panel != null)
                        panel.DontDestroyOnLoad = true;
                    GameDispatcher.Dispatch(GameEvent.LOGIN_ENABLE_BTN);
                }
            }
            else
            {
                TipsManager.Instance.showTips(errMsg);
                if (UIManager.IsOpen<PanelCorpsMatchRoom>())
                {
                    if (UIManager.GetPanel<PanelMatchingTips>() != null)
                        UIManager.GetPanel<PanelMatchingTips>().HidePanel();
                }
            }
        }
        if (proto.err_code == 100001)//登陆验证失败（Tokey过期）(需要重登的处理)
        {
            Driver.SendLogServer(1108, "code:{0} msg:{1}", proto.err_code, errMsg);
            Logger.Log("Tokey过期，重新登录平台");
            SdkManager.TokeyOverdue = true;
            if (Driver.m_platform == EnumPlatform.pc)
                UIManager.ShowTipPanel("登录信息过期，请退出游戏重新登录账号", Application.Quit, null, false, false, "退出");
            SdkManager.Login();
            GameDispatcher.Dispatch(GameEvent.LOGIN_ENABLE_BTN);
        }

        NetLayer.TryLaunchErrorCallBack(proto);
    }

    static void Toc_player_add_friend( toc_player_add_friend data)
    {
        TipsManager.Instance.showTips("好友申请发送成功");
    }

    static void Toc_player_critical_msg(toc_player_critical_msg proto)
    {
        UIManager.ShowTipPanel(proto.msg);
    }

    static void Toc_fight_join_spectator(toc_fight_join_spectator proto)
    {
        //Logger.Error(proto.name);
        //UIManager.ShowTv(proto.name);
        WorldManager.ShowTextMsgInPanel(proto.spectator.name+"加入观战", Color.blue);
        //TipsManager.Instance.showTips(proto.name);
    }
}
