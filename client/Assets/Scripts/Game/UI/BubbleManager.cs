﻿using System.Collections.Generic;

class BubbleManager
{
    /// <summary>
    /// 如果是活动框架的小红点  直接用  EventList的ID  作为Key值
    /// </summary>
    private static Dictionary<string, int> m_dic = new Dictionary<string, int>();


    public static void SetBubble(string key, int num, bool isDispatchEvent = true)
    {
        if (!m_dic.ContainsKey(key))
            m_dic.Add(key, num);
        else
            m_dic[key] = num;

        if (isDispatchEvent)
        {
            GameDispatcher.Dispatch(GameEvent.UI_BUBBLE_CHANGE);
        }
    }

    public static void SetBubble(string key, bool one, bool isDispatchEvent = true)
    {
        SetBubble(key, one ? 1 : 0, isDispatchEvent);
    }

    public static int GetBubble(string key)
    {
        if (m_dic.ContainsKey(key))
            return m_dic[key];
        return 0;
    }

    public static bool HasBubble(string key)
    {
        return GetBubble(key) > 0;
    }

    //判断小红点数据 有没有活动框架数据
    public static bool isActivityFrameHasBubble()
    {
        string[] keys = new string[m_dic.Keys.Count];
        m_dic.Keys.CopyTo(keys, 0);
        foreach (string str in keys)
        {
            int themId;
            if (int.TryParse(str, out themId))
            {//有活动框架上的数据记录
                if (HasBubble(str))
                {
                    return true;
                }
            }
        }
        return false;
    }
}