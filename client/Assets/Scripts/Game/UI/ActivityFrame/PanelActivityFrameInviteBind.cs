﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 14:46:25
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelActivityFrameInviteBind : BasePanel
{
    private Image m_advertPic;
    private Text m_txtTime;
    private Image m_btnSubmit;
    ConfigEventListLine m_eventList;
    private int m_restTime;//倒计剩余时间
    private float m_timeClick;
    p_player_event serverData = null;
    int eventStatus = 0;
    private InputField m_inputTxt;
    public PanelActivityFrameInviteBind()
    {
        // 构造器
        SetPanelPrefabPath("UI/ActivityFrame/PanelActivityFrameInviteBind");
        AddPreLoadRes("UI/ActivityFrame/PanelActivityFrameInviteBind", EResType.UI);
        AddPreLoadRes("Atlas/ActivityFrame", EResType.Atlas);
    }
    public override void Init()
    {
        m_advertPic = m_tran.Find("AdvertPic").GetComponent<Image>();
        m_txtTime = m_tran.Find("time_txt").GetComponent<Text>();
        m_btnSubmit = m_tran.Find("btn_submit").GetComponent<Image>();
        m_inputTxt = m_tran.Find("InputText").GetComponent<InputField>();
        m_inputTxt.gameObject.AddComponent<InputFieldFix>();
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnSubmit.gameObject).onPointerClick += onClickBtnSumbitHandler;
        AddEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }

    private void UpdateEvent()
    {
        OnShow();
    }
    //提交激活码
    private void onClickBtnSumbitHandler(GameObject target, PointerEventData eventData)
    {
        string str = m_inputTxt.text.Trim();
        if (str != "" && str != null)
        {
            long player_id = 0;
            if (long.TryParse(str, out player_id))
            {
                NetLayer.Send(new tos_player_oldplayer_invite() { player_id = player_id });
            }
            else
            {
                TipsManager.Instance.showTips("好友ID错误");
            }
        }
    }

    public override void OnShow()
    {
        eventStatus = 0;
        m_restTime = -1;
        serverData = null;
        if (m_params != null && m_params.Length != 0)
        {
            ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(m_params[0]) as ConfigEventThemeLine;
            m_advertPic.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, cfg.AdvertPic));
            if (cfg.EndTimeDec == "" || cfg.EndTimeDec == null)
            {
                m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(cfg.EndTime)).TotalSeconds;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
            else
            {
                m_txtTime.text = cfg.EndTimeDec;
            }
            if (cfg.EventList != null && cfg.EventList.Length != 0)
            {
                m_eventList = ConfigManager.GetConfig<ConfigEventList>().GetLine(cfg.EventList[0]) as ConfigEventListLine;
                if (m_eventList != null)
                {
                    // m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(m_eventList.TimeEnd)).TotalSeconds;
                    // m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";

                    if (m_eventList.EventType == "client")
                    {
                        /*if (m_eventList.GoPageImg == null || m_eventList.GoPageImg == "")
                            m_btnGetImage.gameObject.TrySetActive(false);
                        else
                        {
                            m_btnGetImage.gameObject.TrySetActive(true);
                            //客户端显示专用
                            m_btnGetImage.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, m_eventList.GoPageImg));
                        }*/
                    }
                    else
                    {
                        serverData = ActivityFrameDataManager.getEventData(m_eventList.Id);
                        eventStatus = ActivityFrameDataManager.getEventSatus(serverData);
                    }
                }
            }
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }
    public override void Update()
    {
        if (m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
        }
    }
}
