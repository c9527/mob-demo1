﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 14:46:25
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelActivityFrameGo : BasePanel
{
    private Image m_advertPic;
    private Text m_txtTime;
    private Image m_btnGetImage;
    ConfigEventListLine m_eventList;
    private int m_restTime;//倒计剩余时间
    private float m_timeClick;
    p_player_event serverData = null;
    int eventStatus = 0;
    private Text m_txtProcess;
    public PanelActivityFrameGo()
    {
        // 构造器
        SetPanelPrefabPath("UI/ActivityFrame/PanelActivityFrameGo");
        AddPreLoadRes("UI/ActivityFrame/PanelActivityFrameGo", EResType.UI);
        AddPreLoadRes("Atlas/ActivityFrame", EResType.Atlas);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }
    public override void Init()
    {
        m_advertPic = m_tran.Find("AdvertPic").GetComponent<Image>();
        m_txtTime = m_tran.Find("time_txt").GetComponent<Text>();
        m_btnGetImage = m_tran.Find("Btn_goGet").GetComponent<Image>();
        m_txtProcess = m_tran.Find("process_txt").GetComponent<Text>();
        m_txtProcess.gameObject.TrySetActive(true);
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnGetImage.gameObject).onPointerClick += onClickPageGOHandler;
        AddEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }

    private void UpdateEvent()
    {
        OnShow();
    }

    private void onClickPageGOHandler(GameObject target, PointerEventData eventData)
    {
        if (eventStatus == ActivityFrameDataManager.EVENT_STATUS_CAN_DRAW)
        {
            NetLayer.Send(new tos_player_take_event_reward() { event_id = m_eventList.Id });
            //return;
        }
        else if (eventStatus == ActivityFrameDataManager.EVENT_STATUS_NO_FINISH)
        {
            if (m_eventList != null && m_eventList.GoURL != null && m_eventList.GoURL != "")
            {
                if (Driver.m_platform == EnumPlatform.web)
                {
                    PlatformAPI.Call("openURL", m_eventList.GoURL, "_blank");
                }
                else
                {
                    Application.OpenURL(m_eventList.GoURL);
                }
                return;
            }
            if (m_eventList != null && m_eventList.GoPage != null && m_eventList.GoPage != "")
            {
                UIManager.GotoPanel(m_eventList.GoPage);
                PanelEvent parent = UIManager.GetPanel<PanelEvent>();
                if (parent != null)
                    parent.HidePanel();
            }
        }
    }

    public override void OnShow()
    {
        eventStatus = 0;
        m_restTime = -1;
        serverData = null;
        Util.SetNormalShader(m_btnGetImage);
        m_txtProcess.gameObject.TrySetActive(false);
        if (m_params != null && m_params.Length != 0)
        {
            ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(m_params[0]) as ConfigEventThemeLine;
            m_advertPic.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, cfg.AdvertPic));
            if (cfg.EndTimeDec == "" || cfg.EndTimeDec == null)
            {
                m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(cfg.EndTime)).TotalSeconds;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
            else
            {
                m_txtTime.text = cfg.EndTimeDec;
            }
            if (cfg.EventList != null && cfg.EventList.Length != 0)
            {
                m_eventList = ConfigManager.GetConfig<ConfigEventList>().GetLine(cfg.EventList[0]) as ConfigEventListLine;
                if (m_eventList != null)
                {
                    p_player_event pEvent = ActivityFrameDataManager.getEventData(m_eventList.Id);
                    if (pEvent != null)
                    {//显示进度文本

                        m_txtProcess.text = pEvent.progress + "/" + pEvent.total;
                        if (m_eventList.GoPageProTxtPos == null || m_eventList.GoPageProTxtPos.Length <= 1)
                        {
                            //不显示文本
                        }
                        else
                        {
                            m_txtProcess.rectTransform.localPosition = new Vector3(m_eventList.GoPageProTxtPos[0], m_eventList.GoPageProTxtPos[1], 0);
                            m_txtProcess.gameObject.TrySetActive(true);
                        }
                    }
                    // m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(m_eventList.TimeEnd)).TotalSeconds;
                    // m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
                    if (m_eventList.GoPagePos == null || m_eventList.GoPagePos.Length <= 1)
                    {
                        m_btnGetImage.rectTransform.localPosition = new Vector3(97, -195, 0);
                    }
                    else
                        m_btnGetImage.rectTransform.localPosition = new Vector3(m_eventList.GoPagePos[0], m_eventList.GoPagePos[1], 0);
                    if (m_eventList.EventType == "client")
                    {
                        if (m_eventList.GoPageImg == null || m_eventList.GoPageImg == "")
                            m_btnGetImage.gameObject.TrySetActive(false);
                        else
                        {
                            m_btnGetImage.gameObject.TrySetActive(true);
                            //客户端显示专用
                            m_btnGetImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, m_eventList.GoPageImg));
                        }
                    }
                    else
                    {
                        m_btnGetImage.gameObject.TrySetActive(true);
                        serverData = ActivityFrameDataManager.getEventData(m_eventList.Id);
                        eventStatus = ActivityFrameDataManager.getEventSatus(serverData);
                        if (eventStatus == ActivityFrameDataManager.EVENT_STATUS_FINISH_DRAW)
                        {
                            Util.SetGrayShader(m_btnGetImage);
                        }
                        else if (eventStatus == ActivityFrameDataManager.EVENT_STATUS_NO_FINISH)
                        {
                            m_btnGetImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, "actFrame_goGet"));
                        }
                        else
                        {
                            m_btnGetImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, m_eventList.GoPageImg));
                        }
                    }
                }
            }
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }
    public override void Update()
    {
        if (m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
        }
    }
}
