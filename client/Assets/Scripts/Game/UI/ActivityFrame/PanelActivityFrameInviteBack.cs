﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 14:46:25
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelActivityFrameInviteBack : BasePanel
{
    private Image m_advertPic;
    private Text m_txtTime;
    private Image m_btnCopy;
    private Text m_txtCode;
    ConfigEventListLine m_eventList;
    private int m_restTime;//倒计剩余时间
    private float m_timeClick;
    p_player_event serverData = null;
    int eventStatus = 0;
    public PanelActivityFrameInviteBack()
    {
        // 构造器
        SetPanelPrefabPath("UI/ActivityFrame/PanelActivityFrameInviteBack");
        AddPreLoadRes("UI/ActivityFrame/PanelActivityFrameInviteBack", EResType.UI);
        AddPreLoadRes("Atlas/ActivityFrame", EResType.Atlas);
    }
    public override void Init()
    {
        m_advertPic = m_tran.Find("AdvertPic").GetComponent<Image>();
        m_txtTime = m_tran.Find("time_txt").GetComponent<Text>();
        m_btnCopy = m_tran.Find("btn_copy").GetComponent<Image>();
        m_txtCode = m_tran.Find("TxtCode").GetComponent<Text>();
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnCopy.gameObject).onPointerClick += onClickBtnCopyHandler;
        AddEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }

    private void UpdateEvent()
    {
        OnShow();
    }

    private void onClickBtnCopyHandler(GameObject target, PointerEventData eventData)
    {
        SdkManager.CopyToClipBoard(m_txtCode.text);
    }

    public override void OnShow()
    {
        eventStatus = 0;
        m_restTime = -1;
        serverData = null;
        if (m_params != null && m_params.Length != 0)
        {
            ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(m_params[0]) as ConfigEventThemeLine;
            m_advertPic.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, cfg.AdvertPic));
            if (cfg.EndTimeDec == "" || cfg.EndTimeDec == null)
            {
                m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(cfg.EndTime)).TotalSeconds;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
            else
            {
                m_txtTime.text = cfg.EndTimeDec;
            }
            m_txtCode.text = PlayerSystem.roleId.ToString();
            //ConfigEventListLine tempEventList;
            //p_player_event tempEvent;

           // m_tran.Find("TxtProcess1").GetComponent<Text>().text = ActivityFrameDataManager.oldPlayerInfo_inviteGifNum + "/20";
            //m_tran.Find("TxtProcess2").GetComponent<Text>().text = ActivityFrameDataManager.oldPlayerInfo_popGifNum + "/20";

          /* if (cfg.EventList != null && cfg.EventList.Length != 0)
            {
                for (int i = 0; i < cfg.EventList.Length; i++)
                {
                    // tempEventList = ConfigManager.GetConfig<ConfigEventList>().GetLine(cfg.EventList[i]) as ConfigEventListLine;
                    tempEvent = ActivityFrameDataManager.getEventData(cfg.EventList[i]);
                    if (tempEvent != null)
                    {
                        m_tran.Find("TxtProcess" + (i + 1)).GetComponent<Text>().text = tempEvent.progress + "/" + tempEvent.total;
                    }
                    //
                }
            }*/
        }

        NetLayer.Send(new tos_player_get_oldplayer_gift_info() { });
    }

    private void Toc_player_get_oldplayer_gift_info(toc_player_get_oldplayer_gift_info data)
    {
        m_tran.Find("TxtProcess1").GetComponent<Text>().text = data.invite_gift_cnt + "/30";
        m_tran.Find("TxtProcess2").GetComponent<Text>().text = data.pop_gift_cnt + "/10";
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
    }
    public override void Update()
    {
        if (m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
        }
    }
}
