﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

/***************************************************************************
 * Author: zhouxiaogang
 * Create Time: 2015/12/16 14:43:21
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelActivityFrameList : BasePanel
{
    public class ActivityFrameItem : ItemRender
    {
        private Text m_txtCondition;
        private Text m_txtProcess;
        private GameObject m_goItem;
        private GameObject m_goEquip;
        private GameObject m_rewardIcon;
        private GameObject m_btnGet;
        private GameObject m_btnGo;
        private GameObject m_btnGetted;
        private ConfigEventListLine m_dataCfg;
        private p_player_event m_serverData;
        private Vector3 twoPos = new Vector3(-75, 0, 0);
        private Vector3 onePos = new Vector3(-18, 0, 0);

        private ConfigItemLine m_itemCfg1;
        private ConfigItemLine m_itemCfg2;

        public override void Awake()
        {
            m_txtCondition = transform.Find("condition").GetComponent<Text>();
            m_txtProcess = transform.Find("process").GetComponent<Text>();
            m_goItem = transform.Find("item_list").gameObject;
            m_goEquip = transform.Find("equip_list").gameObject;
            m_btnGet = transform.Find("btn_get").gameObject;
            m_btnGo = transform.Find("btn_go").gameObject;
            m_btnGetted = transform.Find("btn_getted").gameObject;
            m_rewardIcon = transform.Find("RewardIcon").gameObject;
            UGUIClickHandler.Get(m_btnGet).onPointerClick += onClickBtnGetHandler;
            UGUIClickHandler.Get(m_btnGo).onPointerClick += onClickBtnGoHandler;
            GameDispatcher.AddEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
            MiniTipsManager.get(m_goEquip.gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem.transform.FindChild("item1").gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem.transform.FindChild("item2").gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg2 == null) return null;
                return new MiniTipsData() { name = m_itemCfg2.DispName, dec = m_itemCfg2.Desc };
            };
        }

        private void UpdateEvent()
        {
            OnSetData(m_dataCfg);
        }

        private void onClickBtnGetHandler(GameObject target, PointerEventData eventData)
        {
            NetLayer.Send(new tos_player_take_event_reward() { event_id = m_dataCfg.Id });
        }
        private void onClickBtnGoHandler(GameObject target, PointerEventData eventData)
        {
            if (m_dataCfg != null && m_dataCfg.GoPage != null && m_dataCfg.GoPage != "")
            {
                UIManager.GotoPanel(m_dataCfg.GoPage);
                PanelEvent parent = UIManager.GetPanel<PanelEvent>();
                if (parent != null)
                    parent.HidePanel();
            }
        }

        void OnDestroy()
        {
            GameDispatcher.RemoveEventListener(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT, UpdateEvent);
        }
        protected override void OnSetData(object data)
        {
            if (data == null) return;
            m_dataCfg = data as ConfigEventListLine;
            m_txtCondition.text = m_dataCfg.ConditionDec;
            int curUtcTime = TimeUtil.GetNowTimeStamp();

            p_player_event eventData = ActivityFrameDataManager.getEventData(m_dataCfg.Id);
            if (eventData != null)
            {
                int status = ActivityFrameDataManager.getEventSatus(eventData);
                m_btnGet.TrySetActive(false);
                m_btnGo.TrySetActive(false);
                m_btnGetted.TrySetActive(false);

                if (curUtcTime < uint.Parse(m_dataCfg.TimeStart))
                {
                    m_txtProcess.text = "活动暂未开始";
                }
                else
                {
                    if (m_dataCfg.ProcessDec.IndexOf('%') != -1)
                    {
                        string[] ary = m_dataCfg.ProcessDec.Split(new char[] { '%' });
                        if (ary.Length == 3)
                        {
                            m_txtProcess.text = ary[0] + eventData.progress + ary[1] + eventData.total + ary[2];
                        }
                    }
                    else
                    {
                        m_txtProcess.text = m_dataCfg.ProcessDec;
                    }
                }

                switch (status)
                {
                    case ActivityFrameDataManager.EVENT_STATUS_NO_FINISH:
                        if (m_dataCfg.GoPage == null || m_dataCfg.GoPage == "") { }
                        else
                            m_btnGo.TrySetActive(true);
                        break;
                    case ActivityFrameDataManager.EVENT_STATUS_CAN_DRAW:
                        m_btnGet.TrySetActive(true);
                        break;
                    case ActivityFrameDataManager.EVENT_STATUS_FINISH_DRAW:
                        m_btnGetted.TrySetActive(true);
                        m_txtProcess.text = "已完成";
                        break;
                    default:
                        break;
                }
            }
            m_rewardIcon.TrySetActive(false);
            if (m_dataCfg.RewardIcon != null && m_dataCfg.RewardIcon != "")
            {
                m_rewardIcon.TrySetActive(true);
                m_rewardIcon.transform.Find("name").GetComponent<Text>().text = m_dataCfg.RewardName;
                m_rewardIcon.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, m_dataCfg.RewardIcon));
                m_rewardIcon.GetComponent<Image>().SetNativeSize();
                m_goEquip.TrySetActive(false);
                m_goItem.TrySetActive(false);
                return;
            }

            ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(m_dataCfg.Reward);
            if (rewardLine != null && rewardLine.ItemList != null && rewardLine.ItemList.Length != 0)
            {

                var info = ItemDataManager.GetItem(rewardLine.ItemList[0].ID);
                m_itemCfg1 = info;
                if (info.Type == GameConst.ITEM_TYPE_EQUIP)
                {
                    m_goEquip.TrySetActive(true);
                    m_goItem.TrySetActive(false);
                    m_goEquip.transform.FindChild("img_equip").GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(rewardLine.ItemList[0].ID), info.SubType);
                    m_goEquip.transform.FindChild("img_equip").GetComponent<Image>().SetNativeSize();
                    // m_goEquip.transform.FindChild("name").GetComponent<Text>().text = info.DispName;
                    m_goEquip.transform.FindChild("name").GetComponent<Text>().text = m_dataCfg.RewardName;
                }
                else
                {
                    m_goEquip.TrySetActive(false);
                    m_goItem.TrySetActive(true);
                    m_goItem.transform.FindChild("name").GetComponent<Text>().text = m_dataCfg.RewardName;
                    int lngth = rewardLine.ItemList.Length;
                    if (lngth > 2) lngth = 2;
                    for (int i = 0; i < lngth; ++i)
                    {
                        string icon = string.Format("item{0}/icon", (i + 1).ToString());
                        string num = string.Format("item{0}/num", (i + 1).ToString());
                        string name = string.Format("item{0}/name", (i + 1).ToString());
                        string discription = string.Format("item{0}/discription", (i + 1).ToString());
                        var itemInfo = ItemDataManager.GetItem(rewardLine.ItemList[i].ID);
                        if (i == 0) 
                            m_itemCfg1 = itemInfo;
                        else
                            m_itemCfg2 = itemInfo;
                        m_goItem.transform.FindChild(icon).GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(rewardLine.ItemList[i].ID), itemInfo != null ? itemInfo.SubType : "");                        
                        if(itemInfo.SubType != "ROLE")
                        {
                            m_goItem.transform.FindChild(icon).GetComponent<Image>().preserveAspect = false;
                            m_goItem.transform.FindChild(icon).GetComponent<Image>().SetNativeSize();
                        }
                        else
                        {
                            m_goItem.transform.FindChild(icon).GetComponent<Image>().preserveAspect = true;
                            m_goItem.transform.FindChild(icon).GetComponent<Image>().rectTransform.sizeDelta = new Vector2(151, 87);
                        }
                        m_goItem.transform.FindChild(num).GetComponent<Text>().text = "X" + rewardLine.ItemList[i].cnt;
                        m_goItem.transform.FindChild(name).GetComponent<Text>().text = itemInfo.DispName;
                        m_goItem.transform.FindChild(discription).GetComponent<Text>().text = itemInfo.Desc;
                    }

                    if (lngth >= 2)
                    {
                        m_goItem.transform.FindChild("add").gameObject.TrySetActive(true);
                        m_goItem.transform.FindChild("item2").gameObject.TrySetActive(true);
                        m_goItem.transform.FindChild("item1").gameObject.GetComponent<RectTransform>().localPosition = twoPos;
                    }
                    else
                    {
                        m_goItem.transform.FindChild("add").gameObject.TrySetActive(false);
                        m_goItem.transform.FindChild("item2").gameObject.TrySetActive(false);
                        m_goItem.transform.FindChild("item1").gameObject.GetComponent<RectTransform>().localPosition = onePos;
                    }
                }
            }
        }
    }


    private Text m_txtTime;
    private Image m_advertPic;

    private GameObject m_gridContent;
    private GameObject m_gridItem;

    private DataGrid m_dataGrid;
    private int m_restTime;//倒计剩余时间
    private float m_timeClick;
    private int m_itemHeight;
    private Scrollbar m_sBar;
    public PanelActivityFrameList()
    {
        // 构造器
        SetPanelPrefabPath("UI/ActivityFrame/PanelActivityFrameList");
        AddPreLoadRes("UI/ActivityFrame/PanelActivityFrameList", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/ActivityFrame", EResType.Atlas);
    }
    public override void Init()
    {
        m_txtTime = m_tran.Find("time_txt").GetComponent<Text>();
        m_advertPic = m_tran.Find("TopAdvert/AdvertPic").GetComponent<Image>();
        m_gridContent = m_tran.Find("ScrollDataGrid/content").gameObject;
        m_sBar = m_tran.Find("VBScrollBar").GetComponent<Scrollbar>();
        m_gridItem = m_gridContent.transform.FindChild("ActivityFrameItem").gameObject;
        m_dataGrid = m_gridContent.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_gridItem, typeof(ActivityFrameItem));
        GridLayoutGroup script = m_gridContent.GetComponent<GridLayoutGroup>();
        m_itemHeight = (int)(script.cellSize.y + script.spacing.y);
    }
    public override void InitEvent()
    {
    }
    public override void OnShow()
    {
        m_restTime = -1;
        if (m_params != null && m_params.Length != 0)
        {
            ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine((int)m_params[0]) as ConfigEventThemeLine;
            if (cfg.EndTimeDec == "" || cfg.EndTimeDec == null)
            {
                m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(cfg.EndTime)).TotalSeconds;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
            else
            {
                m_txtTime.text = cfg.EndTimeDec;
            }

            m_advertPic.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, cfg.AdvertPic));
            List<ConfigEventListLine> eventList = new List<ConfigEventListLine>();
            ConfigEventListLine tempCfg;
            int canDrawId = -1;
            if (cfg.EventList != null)
            {
                int lngth = cfg.EventList.Length;

                for (int i = 0; i < lngth; i++)
                {
                    tempCfg = ConfigManager.GetConfig<ConfigEventList>().GetLine(cfg.EventList[i]) as ConfigEventListLine;
                    if (tempCfg != null)
                    {
                        if(ActivityFrameDataManager.getEventData(tempCfg.Id)!=null)//后端有推过来数据才显示
                        eventList.Add(tempCfg);
                        if (canDrawId == -1 && ActivityFrameDataManager.checkEventSatus(tempCfg.Id) == ActivityFrameDataManager.EVENT_STATUS_CAN_DRAW)
                        {
                            canDrawId = i;
                        }
                    }
                }
                m_dataGrid.Data = eventList.ToArray();
                if (canDrawId == -1)
                    canDrawId = 0;
                m_dataGrid.ShowItemOnTop(canDrawId);
            }
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }
    public override void Update()
    {
        if (m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
        }
    }
}
