﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class RechargeItemRender : ItemRender
{
    TDRecharge _data;

    public Image item_img;
    public Image extra_ico;
    public Image mark_ico;
    public Text name_txt;
    public Text extra_txt;
    public Text cost_txt;

    public override void Awake()
    {
        item_img = transform.Find("item_img").GetComponent<Image>();
        extra_ico = transform.Find("extra_ico").GetComponent<Image>();
        mark_ico = transform.Find("mark_ico").GetComponent<Image>();
        name_txt = transform.Find("name_txt").GetComponent<Text>();
        extra_txt = transform.Find("extra_txt").GetComponent<Text>();
        cost_txt = transform.Find("buy_btn/Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = m_renderData is List<TDRecharge> ? (m_renderData as List<TDRecharge>)[0] as TDRecharge : m_renderData as TDRecharge;
        if (_data == null)
        {
            return;
        }
        mark_ico.enabled = _data.Recommend==1;
        extra_ico.enabled = _data.FirstbuyAdd>0 && PlayerSystem.IsFirstBuy(_data.ID);
        name_txt.text = _data.Name;
        //ConfigItemLine cfg = ItemDataManager.GetItem(_data.ID);
        //item_img.SetSprite(ResourceManager.LoadIcon(_data.Icon),cfg!=null?cfg.SubType:"");
        item_img.SetSprite(ResourceManager.LoadIcon(_data.Icon));
        item_img.SetNativeSize();
        extra_txt.text = _data.AdditionalDiamond > 0 ? "送" + _data.AdditionalDiamond + "钻石" : "";
        cost_txt.text = "￥" + _data.PlatformCost;
    }
}
