﻿using UnityEngine;
using System.Collections;

public class PanelRechargeiOS :BasePanel {

    public const int loginID = -1;

    public PanelRechargeiOS()
    {
        SetPanelPrefabPath("UI/Member/PanelRechargeiOS");

        AddPreLoadRes("Atlas/Member", EResType.Atlas);
        AddPreLoadRes("UI/Member/PanelRechargeiOS", EResType.UI);
    }

    public override void Init()
    {
        
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btn")).onPointerClick += OnClick;        
    }

    private void OnClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }

    public override void OnShow()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}
