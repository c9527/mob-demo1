﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PanelMember : BasePanel
{
    int _quick_index;
    TDRecharge[] _quick_data;

    public ToggleGroup quick_canvas;
    public Text quick_cost_txt;
    private Text m_txtRemainTime;
    private Text m_txtCoinAdd;
    private Text m_txtHonorAdd;
    private Text m_txtExpAdd;
    private Text m_txtDiamondAdd;
    private Text m_txtTrainningAdd;
    private Text m_txtTrainningEffect;
    private static int GIFT_NUM = 3;
    private static int MEMBER_PRIVILEGE_ID = 1;
    private static int MEMBER_RECHARGE_7DAY_ID = 1;
    private static int MEMBER_RECHARGE_30DAY_ID = 2;

    private GameObject[] m_7DayGiftObj = new GameObject[GIFT_NUM];
    private GameObject[] m_30DaysGiftObj = new GameObject[GIFT_NUM];

    private GameObject m_7DayGiftHide;
    private GameObject m_30DayGiftMove;
    //private UIEffect m_effectPlayer;
    public PanelMember()
    {
        SetPanelPrefabPath("UI/Member/PanelMember");
        AddPreLoadRes("UI/Member/PanelMember", EResType.UI);

        //AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Member", EResType.Atlas);

        var ids = new int[] { 1, 2 };
        _quick_data = new TDRecharge[ids.Length];
        for (int i = 0; i < _quick_data.Length; i++)
        {
            _quick_data[i] = ConfigManager.GetConfig<ConfigRecharge>().GetLine(ids[i]);
        }
    }

    public override void Init()
    {
        quick_canvas = m_tran.Find("quick_canvas").GetComponent<ToggleGroup>();
        quick_cost_txt = m_tran.Find("quick_canvas/buy_btn/Text").GetComponent<Text>();
        m_txtRemainTime = m_tran.Find("remain_time/time_value").GetComponent<Text>();
        m_txtCoinAdd = m_tran.Find("privilege/content/coin_add/value").GetComponent<Text>();
        m_txtHonorAdd = m_tran.Find("privilege/content/honor_add/value").GetComponent<Text>();
        m_txtExpAdd = m_tran.Find("privilege/content/exp_add/value").GetComponent<Text>();
        m_txtDiamondAdd = m_tran.Find("privilege/content/diamond_add/value").GetComponent<Text>();
        m_txtTrainningAdd = m_tran.Find("privilege/content/trainning_add/value1").GetComponent<Text>();
        m_txtTrainningEffect = m_tran.Find("privilege/content/trainning_add/value2").GetComponent<Text>();

        if (Driver.m_platform == EnumPlatform.ios)
        {
            m_7DayGiftHide = m_tran.Find("reward/7day_gift").gameObject;
            m_7DayGiftHide.TrySetActive(false);
            m_30DayGiftMove = m_tran.Find("reward/30day_gift").gameObject;
            m_30DayGiftMove.transform.localPosition = new Vector3(0, 55, 0);
        }

        for ( int i = 0; i < GIFT_NUM; ++i )
        {
            string sevenDaysItem = string.Format( "reward/7day_gift/gift_item{0}",(i+1).ToString());
            string thirtyDaysItem = string.Format("reward/30day_gift/gift_item{0}",(i + 1).ToString());
            m_7DayGiftObj[i] = m_tran.Find(sevenDaysItem).gameObject;
            m_30DaysGiftObj[i] = m_tran.Find(thirtyDaysItem).gameObject;
        }

        InitView();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        for (int i = 0; i < 2;i++ )
        {
            UGUIClickHandler.Get(quick_canvas.transform.Find("Toggle_"+i)).onPointerClick += OnToggleClick;
        }
        UGUIClickHandler.Get(m_tran.Find("quick_canvas/buy_btn")).onPointerClick += OnQuickBuyBtnClick;
    }

    public override void OnShow()
    {
        _quick_index = 0;
        var selected_toggle = quick_canvas.transform.Find("Toggle_1").GetComponent<Toggle>();
        selected_toggle.isOn = true;
        OnToggleClick(selected_toggle.gameObject, null);

        //if (m_effectPlayer == null)
        //{
            //m_effectPlayer = UIEffect.ShowEffect(m_tran, EffectConst.UI_RECHARGE_EFFECT, new Vector2(-290, 5));
        //}

        UpdateDays();
    }

    private void OnQuickBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        var tdata = _quick_index < _quick_data.Length ? _quick_data[_quick_index] : null;
        if (tdata != null)
        {
            SdkManager.Pay(tdata.ID);
        }
    }

    private void OnToggleClick(GameObject target, PointerEventData eventData)
    {
        _quick_index = int.Parse(target.name.Substring(target.name.LastIndexOf('_') + 1));
        if (_quick_index < _quick_data.Length)
            quick_cost_txt.text = string.Format("￥{0}  ({1}天)",_quick_data[_quick_index].PlatformCost, _quick_data[_quick_index].Guarantee);
        else
            quick_cost_txt.text = "￥??????";
    }

    public override void OnHide()
    {
        //if (m_effectPlayer != null)
        //{
        //    m_effectPlayer.Destroy();
        //    m_effectPlayer = null;
       // }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    protected void UpdateDays()
    {
        if (MemberManager.IsMember())
        {
            TimeSpan remain = MemberManager.GetMemberRemainTime();
            m_txtRemainTime.text = string.Format("{0}天{1}小时", remain.Days, remain.Hours);
        }
        else
        {
            m_txtRemainTime.text = "0天0小时";
        }
    }
    protected void InitView()
    {
        var configPrivilege = ConfigManager.GetConfig<ConfigMemberPrivilege>();
        ConfigMemberPrivilegeLine privilegeLine = configPrivilege.GetLine(MEMBER_PRIVILEGE_ID);
        m_txtCoinAdd.text = privilegeLine.Coin + "%";
        m_txtHonorAdd.text = privilegeLine.Medal + "%";
        m_txtExpAdd.text = privilegeLine.Exp + "%";
        m_txtDiamondAdd.text = string.Format("每天获得{0}钻石", privilegeLine.Diamond);
        m_txtTrainningAdd.text = string.Format("开启{0}小时训练", privilegeLine.TrainHour.ToString());
        m_txtTrainningEffect.text = string.Format("最大{0}%训练效果", privilegeLine.MaxTrain.ToString());

        var configRrecharge = ConfigManager.GetConfig<ConfigRecharge>();
        TDRecharge sevenDayRecharge = configRrecharge.GetLine(MEMBER_RECHARGE_7DAY_ID);
        TDRecharge thirtyDayRecharge = configRrecharge.GetLine(MEMBER_RECHARGE_30DAY_ID);

        var configGift = ConfigManager.GetConfig<ConfigGiftBag>();
        ConfigGiftBagLine sevenDayGift = configGift.GetLine(sevenDayRecharge.GiftBagId);
        ConfigGiftBagLine thirtyDayGift = configGift.GetLine(thirtyDayRecharge.GiftBagId);

        for( int i = 0; i < GIFT_NUM; ++i )
        {
            ResetGift(m_7DayGiftObj[i],sevenDayGift.GiftList[i],7);
            ResetGift(m_30DaysGiftObj[i],thirtyDayGift.GiftList[i],30);
        }
    }

    protected void ResetGift( GameObject item,ItemInfo info,int day )
    {
        item.transform.FindChild("day").GetComponent<Text>().text = day + "天";
        ConfigItemLine itemLine = ItemDataManager.GetItem(info.ID);
        var img = item.transform.FindChild("item_icon").GetComponent<Image>();
        img.SetSprite(ResourceManager.LoadIcon(itemLine.Icon),itemLine.SubType);
        img.SetNativeSize();
        item.transform.FindChild("name").GetComponent<Text>().text = itemLine.DispName;
    }
}
