﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelMemberEffect : BasePanel
{
    private UIEffect m_effect;

    public PanelMemberEffect()
    {
        SetPanelPrefabPath("UI/Member/PanelMemberEffect");

        AddPreLoadRes("Atlas/Member", EResType.Atlas);
        AddPreLoadRes("UI/Member/PanelMemberEffect", EResType.UI);
    }

    public override void Init()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran).onPointerClick += OnClkScreen;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        if( m_effect != null )
        {
            m_effect.Destroy();
            m_effect = null;
        }

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)m_params[0];
        if(eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            m_effect = UIEffect.ShowEffect(m_tran, EffectConst.UI_BUY_NORMAL_MEMBER, new Vector2(0, 0));
        }
        else if( eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH )
        {
            m_effect = UIEffect.ShowEffect(m_tran, EffectConst.UI_BUY_HIGH_MEMBER, new Vector2(0, 0));
        }
    }

    private void OnSwitchToView(string name,params object[] args)
    {
       
    }

    public override void OnHide()
    {
       
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        if( m_effect != null )
        {
            m_effect.Destroy();
            m_effect = null;
        }
		if(Driver.m_platform == EnumPlatform.ios)
		{
			if (SdkManager.payInfo == null) 
			{
				return;
			}
			if (SdkManager.payInfo.Associator > 0) 
			{
				if(SdkManager.isGoComment())
                {
                    SdkManager.m_logFrom = 2;
					SdkManager.getStarComment();
				}
			}
		}

	}
	
	public override void Update()
    {
    }

    private void OnClkScreen(GameObject target, PointerEventData eventData)
    {
        DestroyPanel();
    }
}
