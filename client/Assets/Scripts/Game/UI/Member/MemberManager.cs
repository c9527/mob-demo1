﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MemberManager 
{
    public enum MEMBER_TYPE
    {
        MEMBER_NONE = 0,
        MEMBER_NORMAL = 1,
        MEMBER_HIGH = 2
    }

    private static List<p_recharge_associator_info> m_lstData = new List<p_recharge_associator_info>();
 
	
   private static void Toc_player_recharge_associator_info( toc_player_recharge_associator_info proto )
   {
       m_lstData.Clear();
       m_lstData.AddRange(proto.recharge_associator_info);
       if (IsHighMember())
       {
           PlayerSystem.roleData.vip_level = 2;
       }
       else if (IsMember())
       {
           PlayerSystem.roleData.vip_level = 1;
       }
       else
       {
           PlayerSystem.roleData.vip_level = 0;
       }

       GameDispatcher.Dispatch(GameEvent.UI_MEMBER_CHANGE);
   }

    public static bool IsMember()
    {
        for( int i = 0; i < m_lstData.Count; ++i )
        {
            if (m_lstData[i].associator_type == (int)MEMBER_TYPE.MEMBER_NORMAL)
            {
                if (TimeUtil.GetNowTimeStamp() < m_lstData[i].associator_guarantee)
                    return true;
            }
        }

        return false;
    }

    public static TimeSpan GetMemberRemainTime()
    {
        for (int i = 0; i < m_lstData.Count; ++i)
        {
            if (m_lstData[i].associator_type == (int)MEMBER_TYPE.MEMBER_NORMAL)
            {
                if (TimeUtil.GetNowTimeStamp() < m_lstData[i].associator_guarantee)
                {
                    return TimeUtil.GetRemainTime((uint)m_lstData[i].associator_guarantee);
                }
            }
        }

        return TimeSpan.Zero;
    }
    public static bool IsHighMember()
    {
        for (int i = 0; i < m_lstData.Count; ++i)
        {
            if (m_lstData[i].associator_type == (int)MEMBER_TYPE.MEMBER_HIGH)
            {
                if (TimeUtil.GetNowTimeStamp() < m_lstData[i].associator_guarantee)
                    return true;
            }
        }

        return false;
    }

    public static TimeSpan GetHighMemberRemainTime()
    {
        for (int i = 0; i < m_lstData.Count; ++i)
        {
            if (m_lstData[i].associator_type == (int)MEMBER_TYPE.MEMBER_HIGH)
            {
                if (TimeUtil.GetNowTimeStamp() < m_lstData[i].associator_guarantee)
                {
                    return TimeUtil.GetRemainTime((uint)m_lstData[i].associator_guarantee);
                }
            }
        }

        return TimeSpan.Zero;
    }
}
