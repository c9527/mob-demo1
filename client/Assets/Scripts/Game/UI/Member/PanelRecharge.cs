﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelRecharge : BasePanel
{
    DataGrid _good_list;
    Transform _recharge_confirm_view;
    Text _recharge_name_txt;
    Text _recharge_cost_txt;
    DataGrid _recharge_mode_list;
    UIEffect _effect_player;

    public PanelRecharge()
    {
        SetPanelPrefabPath("UI/Member/PanelRecharge");

        AddPreLoadRes("Atlas/Icon",EResType.Atlas);
        AddPreLoadRes("Atlas/Member", EResType.Atlas);
        AddPreLoadRes("UI/Member/PanelRecharge", EResType.UI);
        AddPreLoadRes("UI/Package/ItemBuyRender", EResType.UI);
    }

    public override void Init()
    {
        _good_list = m_tran.Find("recharge_view/good_list/content").gameObject.AddComponent<DataGrid>();
        _good_list.autoSelectFirst = false;
        _good_list.SetItemRender(m_tran.Find("recharge_view/good_list/Render").gameObject, typeof(RechargeItemRender));
        _good_list.onItemSelected = OnRechargeConfirmHandler;

        _recharge_confirm_view = m_tran.Find("recharge_confirm_view");
        _recharge_confirm_view.gameObject.AddComponent<SortingOrderRenderer>();

        _recharge_name_txt = _recharge_confirm_view.Find("frame/txtName").GetComponent<Text>();
        _recharge_cost_txt = _recharge_confirm_view.Find("frame/txtMoney").GetComponent<Text>();
        _recharge_mode_list = _recharge_confirm_view.Find("frame/group").gameObject.AddComponent<DataGrid>();
        _recharge_mode_list.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemBuyRender"), typeof(ItemBuyRender));
        _recharge_mode_list.onItemSelected = OnRechargeModeSelected;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_recharge_confirm_view.Find("frame/btnClose")).onPointerClick += delegate { _recharge_confirm_view.gameObject.TrySetActive(false); };
        UGUIClickHandler.Get(_recharge_confirm_view.Find("frame/btnBuy")).onPointerClick += OnRechargeModeConfirm;
    }


    private void OnRechargeModeSelected(object renderData)
    {
        var vo = renderData as ItemBuyInfo;
        var tdata = ConfigManager.GetConfig<ConfigRecharge>().GetLine(vo.index);
        if (tdata != null)
        {
            _recharge_name_txt.text = tdata.Name;
#if LANG_ZH
            _recharge_cost_txt.text = "￥" + tdata.PlatformCost;
#else
            _recharge_cost_txt.text = "$" + tdata.PlatformCost;
#endif

        }
    }

    private void OnRechargeModeConfirm(GameObject target, PointerEventData eventData)
    {
        _recharge_confirm_view.gameObject.TrySetActive(false);
        var vo = _recharge_mode_list.SelectedData<ItemBuyInfo>();
        var tdata = ConfigManager.GetConfig<ConfigRecharge>().GetLine(vo.index);
        if (tdata != null)
        {
            OnRechargeConfirmHandler(tdata);
        }
    }

    private void OnRechargeConfirmHandler(object renderData)
    {
        if (renderData is List<TDRecharge>)
        {
            ShowRechargeConfirmView(renderData as List<TDRecharge>);
        }
        else
        {
            var tdata = renderData as TDRecharge;
            if (tdata == null)
            {
                return;
            }
            SdkManager.Pay(tdata.ID);
        }
    }

    private void ShowRechargeConfirmView(List<TDRecharge> data)
    {
        if (data == null || data.Count==0)
        {
            _recharge_confirm_view.gameObject.TrySetActive(false);
            return;
        }
        _recharge_confirm_view.gameObject.TrySetActive(true);
        SortingOrderRenderer.RebuildAll();
        _recharge_name_txt.text = data[0].Name;
#if LANG_ZH
        _recharge_cost_txt.text = "￥" + data[0].PlatformCost;
#else
        _recharge_cost_txt.text = "$" + data[0].PlatformCost;
#endif
        var source_data = new ItemBuyInfo[data.Count];
        for (int i = 0; i < source_data.Length; i++)
        {
            source_data[i] = new ItemBuyInfo();
            source_data[i].index = data[i].ID;
            source_data[i].addRule = 1;
            source_data[i].number = data[i].Guarantee;
        }
        _recharge_mode_list.Data = source_data;
    }

    public override void OnShow()
    {
        _recharge_confirm_view.gameObject.TrySetActive(false);
        OnUpdateListData();
        if (_effect_player == null)
        {
            _effect_player = UIEffect.ShowEffect(m_tran, EffectConst.UI_RECHARGE_EFFECT,new Vector2(-287, -2));
        }
    }

    public override void OnHide()
    {
        _recharge_confirm_view.gameObject.TrySetActive(false);
        if (_effect_player != null)
        {
            _effect_player.Destroy();
            _effect_player = null;
        }
    }

    public override void OnBack()
    {
        
    }

    private void OnUpdateListData()
    {
        var source_data = new List<object>();
        var temp = ConfigManager.GetConfig<ConfigRecharge>().m_dataArr;
        if(temp!=null)
        {
            var temp_map = new Dictionary<int, List<TDRecharge>>();
            List<TDRecharge> vo_ary = null;
            foreach(var tdata in temp)
            {
                if (!tdata.Sell)
                    continue;

                if (SdkManager.IsSDKIOS)
                {
                    if (!tdata.DisplayInIOS)
                        continue;
                }
                else
                {
                    if (!tdata.Display)
                        continue;
                }
                source_data.Add(tdata);
//                if (tdata.Associator > 0)
//                {
//                    if (temp_map.TryGetValue(tdata.Associator, out vo_ary)==false)
//                    {
//                        temp_map[tdata.Associator] = vo_ary = new List<TDRecharge>();
//                        source_data.Add(vo_ary);
//                    }
//                    vo_ary.Add(tdata);
//                }
//                else
//                {
//                    source_data.Add(tdata);
//                }
            }
        }
        _good_list.Data = source_data.ToArray();
    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {

    }

    //=========================================s to c
    private void Toc_player_recharge_info(toc_player_recharge_info data)
    {
        OnUpdateListData();
		SdkManager.appNSLog ("SdkManager.payCallback:"+SdkManager.payCallback);
		if(Driver.m_platform == EnumPlatform.ios)
		{
			SdkManager.appNSLog ("Toc_player_recharge_welfare_list:SdkManager.payCallback"+SdkManager.payCallback);
			if(SdkManager.payCallback!=null)
			{			
				var msg = SdkManager.payCallback;
				SdkManager.SdkPaySuccess (msg);
			}
		}
    }
}
