﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelRechargeMember : BasePanel
{
    static public string VIEW_RECHARGE = "recharge";
    static public string VIEW_MEMBER = "member";
    static public string VIEW_SPMEMBER = "sp_member";

    public BasePanel current_panel;
    public Image banner_img;
    public Image poster_img;

    public PanelRechargeMember()
    {
        SetPanelPrefabPath("UI/Member/PanelRechargeMember");

        AddPreLoadRes("Atlas/Member", EResType.Atlas);
        AddPreLoadRes("UI/Member/PanelRechargeMember", EResType.UI);
    }

    public override void Init()
    {
        banner_img = m_tran.Find("banner_img").GetComponent<Image>();
        poster_img = m_tran.Find("poster_img").GetComponent<Image>();

#if UNITY_ANDROID
        PlatformAPI.FixRechargeBug();
#endif
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("view_tab/recharge")).onPointerClick += OnChangeView;

        var tabMember = m_tran.Find("view_tab/member");
        var tabSpMember = m_tran.Find("view_tab/sp_member");
        UGUIClickHandler.Get(tabMember).onPointerClick += OnChangeView;
        UGUIClickHandler.Get(tabSpMember).onPointerClick += OnChangeView;

//        if (SdkManager.IsSDKIOS)
//        {
//            tabMember.gameObject.TrySetActive(true);
//            tabSpMember.gameObject.TrySetActive(true);
//        }
//        else
//        {
//            tabMember.gameObject.TrySetActive(false);
//            tabSpMember.gameObject.TrySetActive(false);
//        }
    }

    private void OnChangeView(GameObject target, PointerEventData eventData)
    {
        OnSwitchToView(target.name);
    }

    public override void OnShow()
    {
        var view_name = VIEW_RECHARGE;
        if (HasParams())
        {
            view_name = m_params.Length > 0 ? m_params[0] as string : null;
        }
        m_tran.Find("view_tab").GetComponent<ToggleGroup>().SetAllTogglesOff();
        OnSwitchToView(view_name, m_params);
    }

    private void OnSwitchToView(string name,params object[] args)
    {
        var old_panel = current_panel;
        string post_name = "";
        if (name == VIEW_SPMEMBER)
        {
            m_tran.Find("view_tab/sp_member").GetComponent<Toggle>().isOn = true;
            current_panel = UIManager.PopPanel<PanelHighMember>(args, false, this, LayerType.Content);
            post_name = "poster_2";
        }
        else if (name == VIEW_MEMBER)
        {
            m_tran.Find("view_tab/member").GetComponent<Toggle>().isOn = true;
            current_panel = UIManager.PopPanel<PanelMember>(args, false, this, LayerType.Content);
            post_name = "poster_2";
        }
        else
        {
            m_tran.Find("view_tab/recharge").GetComponent<Toggle>().isOn = true;
            current_panel = UIManager.PopPanel<PanelRecharge>(args, false, this, LayerType.Content);
            post_name = "poster_1";
        }
        poster_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Member, post_name));
        //poster_img.SetNativeSize();
        if (old_panel!=null && old_panel != current_panel)
        {
            old_panel.HidePanel();
        }
    }

    public override void OnHide()
    {
        current_panel = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

