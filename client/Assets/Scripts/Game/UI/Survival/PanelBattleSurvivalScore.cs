﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelBattleSurvivalScore : BasePanel
{

    public DataGrid bossDataGrid;
    public static List<OtherPlayer> listBoss = new List<OtherPlayer>();
    protected Vector2 pickupPos = new Vector2(28, 0);
    protected Vector2 monsterPos = new Vector2(50, 0);

    protected ImageDownWatch m_kDownTimer; 
    protected UIImgNumText m_easyNumTxt;
    protected UIImgNumText m_roundNumTxt;
    protected UIImgNumText m_allNumTxt;
    protected UIImgNumText m_collectedNumTxt;

    protected GameObject m_goSlash;
    protected GameObject m_goBaseInfo;

    protected Text m_revivecoin;
    protected Text m_supplypoint;
    protected Image m_stagelevel;
    protected Image m_monsterOrCollect;

    protected bool m_showTipBtn = false;

    protected float m_curRoundTotalTime;
    protected int m_configRound;
    protected string m_winType; //胜利条件（配表）
    protected string m_taskTip; //任务提示
    protected SurvivalBehavior m_behavior;

    virtual protected string panelPath { get { return "UI/Battle/PanelBattleSurvivalScore"; } }

    public PanelBattleSurvivalScore()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath(panelPath);

        AddPreLoadRes(panelPath, EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_kDownTimer = m_tran.Find("TopBar/time").gameObject.AddMissingComponent<ImageDownWatch>();
        Sprite[] timeSpriteList = { ResourceManager.LoadSprite(AtlasName.Battle, "time0"), ResourceManager.LoadSprite(AtlasName.Battle, "time1"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time2"),ResourceManager.LoadSprite( AtlasName.Battle,"time3"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time4"),ResourceManager.LoadSprite( AtlasName.Battle,"time5"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time6"),ResourceManager.LoadSprite( AtlasName.Battle,"time7"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time8"),ResourceManager.LoadSprite( AtlasName.Battle,"time9")};
        m_kDownTimer.SetNumSpriteList(timeSpriteList);
        m_goSlash = m_tran.Find("TopBar/Collect/Slash").gameObject;

        m_easyNumTxt = new UIImgNumText(m_tran.Find("TopBar/Stage/EasyNum"), "blue", AtlasName.Battle, 0, -3);
        m_roundNumTxt = new UIImgNumText(m_tran.Find("TopBar/RoundNum"), "gray_", AtlasName.Common, 0, -3);
        m_allNumTxt = new UIImgNumText(m_tran.Find("TopBar/Collect/AllNum"), "red", AtlasName.Battle, 0, -3);
        m_collectedNumTxt = new UIImgNumText(m_tran.Find("TopBar/Collect/CollectedNum"), "red", AtlasName.Battle, 0, -3);

        m_revivecoin = m_tran.Find("TopBar/CoinNum").GetComponent<Text>();
        m_supplypoint = m_tran.Find("TopBar/dollarNum").GetComponent<Text>();
        m_stagelevel = m_tran.Find("TopBar/Stage/Easy").GetComponent<Image>();
        m_monsterOrCollect = m_tran.Find("TopBar/Collect/MonsterOrCollect").GetComponent<Image>();

        m_goBaseInfo = m_tran.Find("BaseInfo").gameObject;
        m_goBaseInfo.TrySetActive(false);

        Transform bossRender = m_tran.Find("bossHPGrid/hpBar");
        bossDataGrid = bossRender.transform.parent.gameObject.AddComponent<DataGrid>();
        bossDataGrid.SetItemRender(bossRender.gameObject, typeof(BossHPItemRender));
        
        if (WorldManager.singleton.isDefendModeOpen)
        {
            bossDataGrid.transform.localPosition -= new Vector3(0, 70, 0);
        }
    }

    public override void InitEvent()
    {

    }


    public override void OnShow()
    {
        UpdateRound(PlayerBattleModel.Instance.battle_state.round, m_configRound, m_winType, m_curRoundTotalTime, m_taskTip);        
    }

    public override void OnHide()
    {
        
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {

    }

    public override void Update()
    {
        if (m_kDownTimer != null)
        {
            var run_time = PlayerBattleModel.Instance.battle_info.round_time;
            float left_time = m_curRoundTotalTime - run_time;
            m_kDownTimer.SetTime = left_time;
        }
    }

    virtual public void BindBehavior(SurvivalBehavior behavior)
    {
        m_behavior = behavior;
    }

    virtual public void UpdateInfo(int coint, int point, int totalbox, int remainbox, int round, int stageLv, int monster)
    {
        if (m_revivecoin != null && coint != -1) m_revivecoin.text = coint.ToString();
        if (m_supplypoint != null && point != -1) m_supplypoint.text = point.ToString();
        if (m_winType == SurvivalWinType.KILLALL)
        {
            if (m_stagelevel != null)
            {
                m_stagelevel.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "world_boss"));
                m_stagelevel.SetNativeSize();
                m_stagelevel.transform.localPosition = Vector3.zero;
            }
            if (m_easyNumTxt != null)
                m_easyNumTxt.Show(false);
        }
        else
        {
            if (m_allNumTxt != null && totalbox != -1)
                m_allNumTxt.value = totalbox;

            if (m_stagelevel != null && stageLv != -1)
            {
                m_stagelevel.transform.localPosition = new Vector3(-20f, 0, 0);
                string sprite = "";
                if (stageLv > 100)
                    sprite = "stage_survival";
                else
                    sprite =  "stage_lv" + Mathf.Max(1, stageLv);
                m_stagelevel.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, sprite));
                m_stagelevel.SetNativeSize();
            }
            if (m_easyNumTxt != null && round != -1)
            {
                m_easyNumTxt.Show(true);
                m_easyNumTxt.value = round;
            }
        }
        
        if (m_collectedNumTxt != null && !string.IsNullOrEmpty(m_winType))
        {
            if (m_winType == SurvivalWinType.KILLALL)
            {
                m_collectedNumTxt.Show(false);
                return;
            }

            string msg = "";
            if (m_winType == SurvivalWinType.PICKUP)
            {
                m_collectedNumTxt.transform.parent.localPosition = pickupPos;
                m_collectedNumTxt.value = totalbox - remainbox;
                m_collectedNumTxt.Show(true);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "收集病毒箱: " : m_taskTip) + (totalbox - remainbox) + "/" + totalbox;
            }
            else if (m_winType == SurvivalWinType.KILLCNT)
            {
                m_collectedNumTxt.transform.parent.localPosition = monsterPos;
                m_collectedNumTxt.value = monster;
                m_collectedNumTxt.Show(true);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭怪物: " : m_taskTip) + monster;
            }
            else if (m_winType == SurvivalWinType.PICKKILL)
            {
                m_collectedNumTxt.transform.parent.localPosition = remainbox <= 0 ? monsterPos : pickupPos;
                m_collectedNumTxt.value = remainbox <= 0 ? monster : totalbox - remainbox;
                m_collectedNumTxt.Show(true);
                if(remainbox <= 0)
                {
                    showKillCnt();
                    msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭怪物: " : m_taskTip)  + monster;
                }
                else
                {
                    showPickUp();
                    msg = (string.IsNullOrEmpty(m_taskTip) ? "收集病毒箱: " : m_taskTip)  + (totalbox - remainbox) + "/" + totalbox;
                }
            }
            else if (m_winType == SurvivalWinType.KILLBOSS)
            {
                m_collectedNumTxt.Show(false);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭领主巨石" : m_taskTip);
            }
            else if (!string.IsNullOrEmpty(m_taskTip))
                msg = m_taskTip;

            if (MainPlayer.singleton != null && UIManager.IsOpen<PanelBattle>())
            {
                if (string.IsNullOrEmpty(msg))
                {
                    if (m_showTipBtn)
                    {
                        UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(false, false, msg, 180);
                        m_showTipBtn = false;
                    }
                }
                else
                {
                    if (!m_showTipBtn)//第一次show
                    {
                        UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(true, false, msg, 180);
                        m_showTipBtn = true;
                    }
                    UIManager.GetPanel<PanelBattle>().ShowBattleTipMsg(msg);
                }
            }
        }
    }

    virtual public void UpdateRound(int gameRound, int configRound, string winType, float roundTime, string taskTip)
    {        
        m_curRoundTotalTime = roundTime;
        m_winType = winType;
        m_configRound = configRound;
        m_taskTip = taskTip;
        if (string.IsNullOrEmpty(m_winType)) 
            return;

        if (m_roundNumTxt != null) 
            m_roundNumTxt.value = gameRound;

        if (m_winType == SurvivalWinType.PICKUP  || m_winType == SurvivalWinType.PICKKILL)
        {
            showPickUp();
        }
        else if (m_winType == SurvivalWinType.KILLALL || m_winType == SurvivalWinType.KILLBOSS)
        {
            showKillAll();
        }
        else if (m_winType == SurvivalWinType.KILLCNT)
        {
            showKillCnt();
        }

        if (m_behavior != null)
            UpdateInfo(m_behavior.revivecoin, m_behavior.supplypoint, m_behavior.total_box, m_behavior.remain_box,
                m_behavior.total_round, m_behavior.stage_level, m_behavior.monster);
        if (bossDataGrid != null)
            bossDataGrid.Data = listBoss.ToArray();
        TryPlayBossMusic();
    }

    virtual protected void showPickUp()
    {
        if (m_monsterOrCollect != null) 
        { 
            m_monsterOrCollect.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "collect_num")); 
            m_monsterOrCollect.SetNativeSize();
            m_monsterOrCollect.transform.localPosition = new Vector3(-20f, 0, 0);
        }
        if (m_goSlash != null) m_goSlash.TrySetActive(true);
        if (m_allNumTxt != null) m_allNumTxt.gameObject.TrySetActive(true);
    }

    virtual protected void showKillAll()
    {
        if (m_monsterOrCollect != null) 
        {
            if (listBoss.Count != 0)
            {
                if (listBoss[0].configData.ID == 4035)
                {
                    m_monsterOrCollect.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "kill_jushi"));
                }
                else if (listBoss[0].configData.ID == 4056)
                {
                    m_monsterOrCollect.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "kill_sishen"));
                }
                else
                {
                    m_monsterOrCollect.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "kill_jushi"));
                }
            }
            m_monsterOrCollect.SetNativeSize();  
            m_monsterOrCollect.transform.localPosition = Vector3.zero;
        }
        if (m_goSlash != null) m_goSlash.TrySetActive(false);
        if (m_allNumTxt != null) m_allNumTxt.gameObject.TrySetActive(false);
    }

    virtual protected void showKillCnt()
    {
        if (m_monsterOrCollect != null) 
        { 
            m_monsterOrCollect.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "monster_left")); 
            m_monsterOrCollect.SetNativeSize();
            m_monsterOrCollect.transform.localPosition = new Vector3(-20f, 0, 0);
        }
        if (m_goSlash != null) m_goSlash.TrySetActive(false);
        if (m_allNumTxt != null) m_allNumTxt.gameObject.TrySetActive(false);
    }
    

    public static void TryAddBossHPBar(OtherPlayer player)
    {
        if (player != null && player.isStoryPlayer)
            return;

        TryPlayBossMusic();
        if (player != null && !PanelBattleSurvivalScore.listBoss.Contains(player))
        {
            PanelBattleSurvivalScore.listBoss.Add(player);
        }

        PanelBattleSurvivalScore panel = UIManager.GetPanel<PanelBattleSurvivalScore>();
        if (panel == null)
            panel = UIManager.GetPanel<PanelBattleSurvivalDefendScore>();
        if(panel == null)
            panel = UIManager.GetPanel<PanelBattleSurvivalChallengeScore>();
        
        if (panel != null && panel.IsInited())
        {
            panel.bossDataGrid.Data = listBoss.ToArray();            
        }                       
    }

    public static void TryPlayBossMusic()
    {
        if (UIManager.IsBattle() && AudioManager.GetBGMusicName() != AudioConst.zombieBoss&&listBoss.Count!=0)
        {
            AudioManager.PlayBgMusic(AudioConst.zombieBoss);            
        }
    }

    public static void TryDeleteBossHPBar(OtherPlayer player,bool all=false)
    {
        if (player != null && player.isStoryPlayer)
            return;

        PanelBattleSurvivalScore panel = UIManager.GetPanel<PanelBattleSurvivalScore>();
        if (panel == null)
            panel = UIManager.GetPanel<PanelBattleSurvivalDefendScore>();
        if(all)
        {
            listBoss.Clear();
        }
        else
        {
            if (player != null)
                listBoss.Remove(player);
        }
        if(panel!=null&&panel.bossDataGrid!=null)
        {
            panel.bossDataGrid.Data = listBoss.ToArray();
        }       
    }

    virtual public void BindBasePlayer(BasePlayer basePlayer)
    {

    }
    
}
