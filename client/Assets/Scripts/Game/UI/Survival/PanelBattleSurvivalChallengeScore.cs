﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 挑战模式的面板（基于SurvivalScore添加base血量 显示）
/// </summary>
class PanelBattleSurvivalChallengeScore : PanelBattleSurvivalDefendScore
{
    override protected string panelPath { get { return "UI/Battle/PanelBattleSurvivalChallengeScore"; } }

    public PanelBattleSurvivalChallengeScore()
    {

    }

    override public void UpdateInfo(int coint, int point, int totalbox, int remainbox, int round, int stageLv, int monster)
    {
        if (m_revivecoin != null && coint != -1) m_revivecoin.text = coint.ToString();
        if (m_supplypoint != null && point != -1) m_supplypoint.text = point.ToString();

        if (m_stagelevel != null && stageLv != -1)
        {
            string sprite = "challenge_lv_" + Mathf.Max(1, stageLv);
            m_stagelevel.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, sprite));
            m_stagelevel.SetNativeSize();
        }

        if (m_collectedNumTxt != null && !string.IsNullOrEmpty(m_winType))
        {
            if (m_winType == SurvivalWinType.KILLALL)
            {
                m_collectedNumTxt.Show(false);
                return;
            }

            string msg = "";
            if (m_winType == SurvivalWinType.PICKUP)
            {
                m_collectedNumTxt.transform.parent.localPosition = pickupPos;
                m_collectedNumTxt.value = totalbox - remainbox;
                m_collectedNumTxt.Show(true);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "收集病毒箱: " : m_taskTip) + (totalbox - remainbox) + "/" + totalbox;
            }
            else if (m_winType == SurvivalWinType.KILLCNT)
            {
                m_collectedNumTxt.transform.parent.localPosition = monsterPos;
                m_collectedNumTxt.value = monster;
                m_collectedNumTxt.Show(true);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭怪物: " : m_taskTip) + monster;
            }
            else if (m_winType == SurvivalWinType.PICKKILL)
            {
                m_collectedNumTxt.transform.parent.localPosition = remainbox <= 0 ? monsterPos : pickupPos;
                m_collectedNumTxt.value = remainbox <= 0 ? monster : totalbox - remainbox;
                m_collectedNumTxt.Show(true);
                if (remainbox <= 0)
                {
                    showKillCnt();
                    msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭怪物: " : m_taskTip) + monster;
                }
                else
                {
                    showPickUp();
                    msg = (string.IsNullOrEmpty(m_taskTip) ? "收集病毒箱: " : m_taskTip) + (totalbox - remainbox) + "/" + totalbox;
                }
            }
            else if (m_winType == SurvivalWinType.KILLBOSS)
            {
                m_collectedNumTxt.Show(false);
                msg = (string.IsNullOrEmpty(m_taskTip) ? "消灭领主巨石" : m_taskTip);
            }
            else if (!string.IsNullOrEmpty(m_taskTip))
                msg = m_taskTip;

            if (MainPlayer.singleton != null && UIManager.IsOpen<PanelBattle>())
            {
                if (string.IsNullOrEmpty(msg))
                {
                    if(m_showTipBtn)
                    {
                        UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(false, false, msg, 180);
                        m_showTipBtn = false;
                    }
                }
                else
                {
                    if (!m_showTipBtn)//第一次show
                    {
                        UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(true, false, msg, 180);
                        m_showTipBtn = true;
                    }
                    UIManager.GetPanel<PanelBattle>().ShowBattleTipMsg(msg);
                }
            }
        }
    }

}
