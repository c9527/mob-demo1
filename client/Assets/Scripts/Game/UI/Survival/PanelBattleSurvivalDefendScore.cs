﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 守护模式的面板（基于SurvivalScore添加base血量 显示）
/// </summary>
class PanelBattleSurvivalDefendScore : PanelBattleSurvivalScore
{
    private int m_beingAttackTipsTimer = -1;
    private GameObject m_beingAttackTips;

    private BasePlayer m_basePlayer;
    private Image m_baseHp;
    private Image m_baseIcon;
    private GameObject m_cureBtn;
    private int m_reviveCoinCure;

    private float m_lastTime;

    public PanelBattleSurvivalDefendScore()
    {

    }

    public override void Init()
    {
        base.Init();

        m_baseHp = m_tran.Find("BaseInfo/HpBg/Hp").GetComponent<Image>();
        m_baseIcon = m_tran.Find("BaseInfo/BaseIcon").GetComponent<Image>();
        var tran = m_tran.Find("BaseInfo/CureBtn");
        if (tran != null)
        {
            m_cureBtn = tran.gameObject;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            tran.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "PC_cure_icon"));
#endif
        }
        m_beingAttackTips = m_tran.Find("BaseInfo/BeingAttackedTips").gameObject;
        var tsBeingAttackTips = m_beingAttackTips.AddMissingComponent<TweenScale>();
        tsBeingAttackTips.style = UITweener.Style.PingPong;
        tsBeingAttackTips.from = Vector3.one;
        tsBeingAttackTips.to = new Vector3(1.2f, 1.2f, 1.2f);
        tsBeingAttackTips.duration = 0.5f;
        m_beingAttackTips.TrySetActive(false);

        if (m_basePlayer != null)
            BindBasePlayer(m_basePlayer);
    }

    public override void InitEvent()
    {
        base.InitEvent();
        if (m_cureBtn != null)
            UGUIClickHandler.Get(m_cureBtn).onPointerClick += OnCureBase;
        GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
    }


    public override void OnDestroy()
    {
        base.OnDestroy();

        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);
        m_basePlayer = null;
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
    }

    public override void Update()
    {
        base.Update();

        //显示Hp信息
        if (m_basePlayer != null && m_basePlayer.serverData != null && m_baseHp != null)
        {
            if (Time.time >= (m_lastTime + 0.5f))
            {
                m_lastTime = Time.time;
                m_baseHp.fillAmount = (float)m_basePlayer.serverData.life / m_basePlayer.serverData.maxhp();
            }
        }
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    override public void BindBasePlayer(BasePlayer basePlayer)
    {
        m_basePlayer = basePlayer;

        if (m_goBaseInfo == null)
            return;

        if (m_basePlayer == null)
        {
            m_goBaseInfo.TrySetActive(false);
        }
        else
        {
            m_goBaseInfo.TrySetActive(true);
            SetBaseHpBar();
            SetCureBtn();
        }
    }

    private void OnPlayerBeDamaged(DamageData damageData)
    {
        if (damageData.victim == m_basePlayer)
        {
            PlayBaseBeingAttackedTips();
        }
    }

    private void PlayBaseBeingAttackedTips()
    {
        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);

        m_beingAttackTips.TrySetActive(true);

        m_beingAttackTipsTimer = TimerManager.SetTimeOut(2f, () =>
        {
            m_beingAttackTips.TrySetActive(false);
            m_beingAttackTipsTimer = -1;
        });
    }

    public void SetBaseHpBar()
    {
        if (m_baseIcon == null || m_basePlayer == null || m_basePlayer.configData == null)
            return;

        if (!string.IsNullOrEmpty(m_basePlayer.configData.SmallIcon))
        {
            Sprite sprite = ResourceManager.LoadSprite(AtlasName.Survival, m_basePlayer.configData.SmallIcon);
            m_baseIcon.SetSprite(sprite);
            m_baseIcon.SetNativeSize();
            m_baseIcon.enabled = true;
        }
        else
            m_baseIcon.enabled = false;
    }

    public void SetCureBtn()
    {
        if (m_cureBtn == null || m_behavior == null)
            return;
        m_cureBtn.TrySetActive(m_behavior.GetReviveCoinCure(m_basePlayer.Pid) > 0);
    }

    private void OnCureBase(GameObject target, PointerEventData eventData)
    {
        if (m_behavior == null)
            return;
        m_behavior.OnClickCuteBtn();
    }

}
