﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossHPItemRender:ItemRender  {

    private OtherPlayer m_boss;
    private Image m_icon;
    private Image m_fg;
    private Image m_hpBg;
    private Text m_count;
    private int m_leftBarCount;
    private bool needSetGray=false;
    private float m_singleHpCount;
    const string greenHp = "greenhpFG";
    const string yelloHp = "yellowhpFG";
    const string redHp = "redhpFG";
    private int m_hpBarCount = 3; //血条数量
    public override void Awake()
    {
        m_icon = transform.Find("icon").gameObject.GetComponent<Image>();
        m_fg = transform.Find("fg").gameObject.GetComponent<Image>();
        m_hpBg = transform.Find("bg").gameObject.GetComponent<Image>();
        m_count = transform.Find("count").GetComponent<Text>();      
    }
    
    protected override void OnSetData(object data)
    {
        //if (RoomModel.ChannelType==ChannelTypeEnum.worldboss)
        if(WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_WORLD_BOSS))
        {
            m_hpBarCount = 30;            
            m_count.gameObject.TrySetActive(true);            
        }
        //else if(RoomModel.ChannelType==ChannelTypeEnum.boss4newer)
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_BOSS_NEWER))
        {
            m_hpBarCount = 10;            
            m_count.gameObject.TrySetActive(true);            
        }
        else
        {
            m_hpBarCount = 3;
            m_count.gameObject.TrySetActive(false);
        }
        m_leftBarCount = m_hpBarCount;
        m_singleHpCount = 1f / m_hpBarCount;
        m_boss = data as OtherPlayer;
        m_hpBg.fillAmount = 1.0f;
        m_fg.fillAmount = 0f;
        if(m_boss==null)
        {
            Util.SetGoGrayShader(m_icon.gameObject,true);
        }
        else
        {
            Util.SetGoGrayShader(m_icon.gameObject, false);
            m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, m_boss.configData.SmallIcon));
        }
        
    }    
     void Update()
    {        
         float life;
         float realLife;
         if (m_boss == null || m_boss.MaxHp == 0)
         {
             life = 0;
             realLife = 3;
             m_fg.fillAmount = 0f;
             m_hpBg.fillAmount = 0f;
             return;
         }
         else
         {
             life = m_boss.CurHp * 1.0f / (m_boss.MaxHp * 1.0f);
             realLife = life;
             m_leftBarCount = Mathf.CeilToInt(life / m_singleHpCount);
             life = (life % (1.0f / (m_hpBarCount / 3f)))*(m_hpBarCount / 3f); //血量重映射   
         }         
        if(life<1f/3f)
        {
            if(m_fg.sprite.name!=redHp)
            {
                m_fg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, redHp));
            }           
            if(realLife<((1.0f/m_hpBarCount*3f)*1f/3f)) //最后一管血
            {
                m_hpBg.fillAmount = 0;                
            }
            else
            {
                m_hpBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, greenHp));
            }
            life = life / (1f / 3f);
        }
        else if(life<2f/3f)
        {
            if(m_fg.sprite.name!=yelloHp)
            {
                m_fg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, yelloHp));
            }
            if(m_hpBg.sprite.name!=redHp)
            {
                m_hpBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, redHp));
            }            
            life = (life - 1f/3f) / (1f/3f);
        }
         else
        {
            if(m_fg.sprite.name!=greenHp)
            {
                m_fg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, greenHp));
            }
            if(m_hpBg.sprite.name!=yelloHp)
            {
                m_hpBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, yelloHp));
            }            
            life = (life - 2f/3f) / (1f/3f);
        }
        //m_fg.SetNativeSize();
        //m_hpBg.SetNativeSize();
        m_fg.fillAmount = life;
        m_count.text = "x" + m_leftBarCount.ToString();
    }
}
