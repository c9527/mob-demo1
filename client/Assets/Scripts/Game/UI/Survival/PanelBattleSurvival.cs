﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelBattleSurvival : BasePanel
{
    private const int MIN_SHOW_SLIDER_PLAYER_COUNT = 5;
    private const int MAX_PLAYER_COUNT = 10;
    private int frame = 0;
    private List<int> idList = new List<int>(MAX_PLAYER_COUNT);
    private List<GameObject> itemList = new List<GameObject>(MAX_PLAYER_COUNT);
    private List<GameObject> btnList = new List<GameObject>(MAX_PLAYER_COUNT);
    private List<Text> nameTextList = new List<Text>(MAX_PLAYER_COUNT);
    private List<Text> killNumTextList = new List<Text>(MAX_PLAYER_COUNT);
    private List<Text> helperNameTextList = new List<Text>(MAX_PLAYER_COUNT);
    private List<Image> processList = new List<Image>(MAX_PLAYER_COUNT);
    private List<GameObject> helpGoList = new List<GameObject>(MAX_PLAYER_COUNT);
    private List<GameObject> rescueIconGoList = new List<GameObject>(MAX_PLAYER_COUNT);
    private List<GameObject> rebornCoinIconGoList = new List<GameObject>(MAX_PLAYER_COUNT);

    private RectTransform m_survivalItemList;
    private GameObject m_goVBScrollBar;
    private ScrollRect m_scrollContent;
    private float m_spacing;

    private float m_tick;

    private SurvivalBehavior m_behavior;
    /// <summary>
    /// 当前显示的页码
    /// </summary>
    private int m_curPage = 1;
    private const int PAGE_ITEMNUM = 5;

    private GameObject m_hideBtnGo;
    private GameObject m_pcTips;
    private ConfigChannelLine m_channel;

    public PanelBattleSurvival()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleSurvival");

        AddPreLoadRes("UI/Battle/PanelBattleSurvival", EResType.UI);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_survivalItemList = m_tran.Find("ScrollContent/SurvivalItemList").GetComponent<RectTransform>();
        m_spacing = m_survivalItemList.GetComponent<VerticalLayoutGroup>().spacing;
        m_goVBScrollBar = m_tran.Find("ScrollContent/VBScrollBar").gameObject;
        m_scrollContent = m_tran.Find("ScrollContent").GetComponent<ScrollRect>();
        m_pcTips = m_tran.Find("PCTips").gameObject;
        m_pcTips.TrySetActive(false);
        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        {
            Transform trans = m_tran.Find(string.Concat("ScrollContent/SurvivalItemList/SurvivalItem", i));
            Text text0 = trans.Find("NameText").GetComponent<Text>();
            Text text1 = trans.Find("KillNumText").GetComponent<Text>();
            Text text2 = trans.Find("HelperText").GetComponent<Text>();
            Image img = trans.Find("HpProcess/up").GetComponent<Image>();
            GameObject goHelp = trans.Find("HelpIcon").gameObject;
            GameObject goRescueIcon = trans.Find("RescueIcon").gameObject;
            GameObject goRebornCoinIcon = trans.Find("RebornCoinIcon").gameObject;
            trans.gameObject.TrySetActive(false);
            addTweenPos(goHelp);
            helpGoList.Add(goHelp);
            rescueIconGoList.Add(goRescueIcon);
            rebornCoinIconGoList.Add(goRebornCoinIcon);
            btnList.Add(trans.Find("ReviveBtn").gameObject);
            itemList.Add(trans.gameObject);
            nameTextList.Add(text0);
            killNumTextList.Add(text1);
            helperNameTextList.Add(text2);
            processList.Add(img);
            idList.Add(0);

            UnShowRescueInfo(i);
            if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
            {
                trans.Find("ReviveBtn/CoinIcon").gameObject.TrySetActive(false);
                trans.Find("ReviveBtn/CoinText").gameObject.TrySetActive(false);
                if ((i+1)%5==0)
                {
                    trans.Find("ReviveBtn/ReviveText").GetComponent<Text>().text = "复活 Num5";
                }
                else
                    trans.Find("ReviveBtn/ReviveText").GetComponent<Text>().text = "复活 Num" + (i + 1) % PAGE_ITEMNUM;
            }
        }
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            m_pcTips.TrySetActive(true);
            m_scrollContent.enabled = false;
            m_goVBScrollBar.TrySetActive(false);
        }

        m_channel = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        m_hideBtnGo = m_tran.Find("hideBtn").gameObject;
    }

    /// <summary>
    /// 滚动列表
    /// </summary>
    /// <param name="index"></param>
    private void scrollAtIndex(int index, int allNum)
    {
        float m_itemSpace = m_survivalItemList.gameObject.GetComponent<VerticalLayoutGroup>().spacing;
        float ViewSpace = m_scrollContent.gameObject.GetComponent<RectTransform>().sizeDelta.y;
        var value = (index * m_itemSpace) / (Mathf.Max(ViewSpace, allNum * m_itemSpace - ViewSpace));
        value = Mathf.Clamp01(value);
        m_scrollContent.verticalNormalizedPosition = 1 - value;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_hideBtnGo).onPointerClick += OnHideBtnClick;
        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        {
            UGUIClickHandler.Get(btnList[i]).onPointerClick += OnReviveBtnClick;
        }
        //GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        GameDispatcher.AddEventListener<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, OnRescuePlayer);
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            AddEventListener<int>(GameEvent.INPUT_KEY_PAD_NUM_DOWN, (num) =>
            {
                if (num == 0 || num > PAGE_ITEMNUM) return;

                int index = num + (m_curPage - 1) * PAGE_ITEMNUM;
                if (index <= btnList.Count)
                {
                    OnReviveBtnClick(btnList[index - 1], null);
                }
            });
            AddEventListener(GameEvent.INPUT_KEY_PLUS_DOWN, () =>
            {
                

                int playerNum = getPlayerNum();
                if (playerNum > PAGE_ITEMNUM)
                {
                    int maxPage = (int)Mathf.Ceil((float)playerNum / PAGE_ITEMNUM);
                    if (m_curPage < maxPage)
                    {
                        m_curPage++;
                    }
                    else
                        m_curPage = 1;

                    Vector3 vec = m_survivalItemList.anchoredPosition;
                     m_survivalItemList.anchoredPosition = new Vector3(vec.x,180*(m_curPage-1),vec.z);
                    //scrollAtIndex((m_curPage - 1) * PAGE_ITEMNUM + 1, playerNum);
                }
            });
        }
    }

    private void OnHideBtnClick(GameObject target, PointerEventData eventData)
    {
        bool isShow = m_scrollContent.gameObject.activeSelf;
        m_scrollContent.gameObject.TrySetActive(!isShow);
        if (isShow == true)
        {
            m_hideBtnGo.GetRectTransform().anchoredPosition = new Vector3(-24, -62, 0);
            m_hideBtnGo.GetRectTransform().localScale = new Vector3(-1, -1, -1);
        }
        else
        {
            m_hideBtnGo.GetRectTransform().anchoredPosition = new Vector3(-175, -62, 0);
            m_hideBtnGo.GetRectTransform().localScale = new Vector3(1, 1, 1);
        }
    }


    private int getPlayerNum()
    {
        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null) { return 0; }
        int camp = curPlayer.Camp;
        int playerNum = 0;
        List<KillNumRankData> dataList = PlayerBattleModel.Instance.GetKillNumRankDataList(camp);
        if (dataList == null) return 0;
        BasePlayer player;
        for (int i = 0; i < dataList.Count; i++)
        {
            player = WorldManager.singleton.GetPlayerById(dataList[i].id);
            if (player == null || player.serverData == null || player.Pid <= 0) continue;
            playerNum++;
        }
        return playerNum;
    }

    public void BindBehavior(SurvivalBehavior behavior)
    {
        m_behavior = behavior;
    }

    private void addTweenPos(GameObject go)
    {
        if (go == null) return;
        TweenPosition m_tweenPos = TweenPosition.Begin(go, 1f, new Vector3(-110, -26, 0));
        m_tweenPos.style = UITweener.Style.PingPong;
        m_tweenPos.method = UITweener.Method.Linear;
        m_tweenPos.enabled = true;
    }

    private void OnReviveBtnClick(GameObject target, PointerEventData eventData)
    {
        if (m_behavior == null) return;
        GameObject parent = target.transform.parent.gameObject;
        int index = itemList.IndexOf(parent);
        if (index == -1 || index >= idList.Count) return;
        int id = idList[index];
        m_behavior.OnClickReviveBtn(id);
    }

    public override void OnShow()
    {
        OnUpdate();
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        {
            UGUIClickHandler.Get(btnList[i]).RemoveAllHandler(); ;
        }
        GameDispatcher.RemoveEventListener<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, OnRescuePlayer);
    }

    void OnRescuePlayer(toc_fight_actor_rescued data)
    {
        BasePlayer from = WorldManager.singleton.GetPlayerById(data.rescuer);
        BasePlayer to = WorldManager.singleton.GetPlayerById(data.actor);
        if (from == null || to == null) return;
        if (!idList.Contains(data.rescuer) || !idList.Contains(data.actor)) return;
        int index = idList.IndexOf(data.actor);
        helperNameTextList[index].text = string.Format("<color='{0}'>{1}</color>", "#ff8a00", from.PlayerName);
        rescueIconGoList[index].TrySetActive(true);
        rebornCoinIconGoList[index].TrySetActive(false);
        TimerManager.SetTimeOut(3f, () => { UnShowRescueInfo(index); });
    }

    public void OnUseRebornCoinRebornPlayer(toc_fight_survival_use_revivecoin data)
    {
        BasePlayer from = WorldManager.singleton.GetPlayerById(data.from);
        BasePlayer to = WorldManager.singleton.GetPlayerById(data.actor);
        if (from == null || to == null) return;
        if (from == to) return;
        if (!idList.Contains(data.from) || !idList.Contains(data.actor)) return;
        int index = idList.IndexOf(data.actor);
        helperNameTextList[index].text = string.Format("<color='{0}'>{1}</color>", "#ff8a00", from.PlayerName);
        rescueIconGoList[index].TrySetActive(false);
        rebornCoinIconGoList[index].TrySetActive(true);
        TimerManager.SetTimeOut(3f, () => { UnShowRescueInfo(index); });
    }

    void UnShowRescueInfo(int index)
    {
        rescueIconGoList[index].TrySetActive(false);
        rebornCoinIconGoList[index].TrySetActive(false);
        helperNameTextList[index].text = "";
    }

    void HideAllItem()
    {
        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        {
           itemList[i].TrySetActive(false);
        }
        m_scrollContent.enabled = false;
        m_goVBScrollBar.TrySetActive(false);
    }

    public override void Update()
    {
        m_tick += Time.deltaTime;
        if (m_tick >= 1.0f)
        {
            m_tick = 0;
            OnUpdate();
        }
    }

    public void OnUpdate()
    {
        if (!IsInited() || IsOpen() == false)
            return;

        //{
        //frame = 0;
        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null) { HideAllItem(); return; }
        int camp = curPlayer.Camp;
        List<KillNumRankData> dataList = PlayerBattleModel.Instance.GetKillNumRankDataList(camp);
        if (dataList == null || dataList.Count == 0) { HideAllItem(); return; }
        if (WorldManager.singleton.isWorldBossModeOpen || WorldManager.singleton.isChallengeModeOpen) //世界boss以伤害量排名或者生存挑战模式
            dataList.Sort((a, b) => { return b.score != a.score ? b.score - a.score : a.killNum - b.killNum; });
        else
            dataList.Sort((a, b) => { return b.killNum != a.killNum ? b.killNum - a.killNum : a.id - b.id; });
        BasePlayer player;
        int len = dataList.Count;
        int j = 0;
        len = len > MAX_PLAYER_COUNT ? MAX_PLAYER_COUNT : len;
        for (int i = 0; i < len; i++)
        {
            player = WorldManager.singleton.GetPlayerById(dataList[i].id);
            if (player == null || player.serverData == null)
                continue;
            if (player.Pid <= 0 && (m_channel.Type != ChannelTypeEnum.boss4newer || player.Camp != 1))
                continue;
            j++;
            string color = player is MainPlayer ? ColorUtil.ColorToHex(ColorUtil.ColorEnum.Green, true) : "#ffffff";
            string name = string.Format("<color='{0}'>{1}</color>", color, dataList[i].name);
            int temp = WorldManager.singleton.isChallengeModeOpen ? dataList[i].score / 100 : dataList[i].score;
            string killNum = string.Format("<color='{0}'>{1}</color>", color, "/" + ((WorldManager.singleton.isWorldBossModeOpen || WorldManager.singleton.isChallengeModeOpen) ? temp : dataList[i].killNum));
            itemList[j - 1].TrySetActive(true);
            idList[j - 1] = dataList[i].id;
            nameTextList[j - 1].text = name;
            killNumTextList[j - 1].text = killNum;
            processList[j - 1].fillAmount = dataList[i].curHpPercent;
            btnList[j - 1].TrySetActive(!WorldManager.singleton.isViewBattleModel && player.isDead && !(player is MainPlayer));
            helpGoList[j - 1].TrySetActive(player.isDying);
            if (player.isDying)
            {
                UnShowRescueInfo(j - 1);
            }
        }

        for (int k = j; k < MAX_PLAYER_COUNT; k++)
        {
            itemList[k].TrySetActive(false);
        }

        m_survivalItemList.sizeDelta = new Vector2(m_survivalItemList.sizeDelta.x, m_spacing * len);

        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            int playerNum = getPlayerNum();
            if (playerNum > PAGE_ITEMNUM)
            {
                m_pcTips.TrySetActive(true);
            }
            else
            {
                m_pcTips.TrySetActive(false);
            }
        }
        else
        {
            //设置滚动
            if (len > MIN_SHOW_SLIDER_PLAYER_COUNT)
            {
                if (!m_scrollContent.enabled)
                {
                    m_scrollContent.enabled = true;
                    m_goVBScrollBar.TrySetActive(true);
                }
            }
            else
            {
                if (m_scrollContent.enabled)
                {
                    m_scrollContent.enabled = false;
                    m_goVBScrollBar.TrySetActive(false);
                }
            }
        }
       
    }
}
