﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/30 15:12:38
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelTaskNewHand : BasePanel
{
    public class ItemTaskNewHand : ItemRender
    {
        private Text m_textDec;
        private GameObject m_btnGet;
        private GameObject m_btnGo;
        private GameObject m_goItem;
        private ConfigMissionLine m_cfgData;
        private Image m_icon;
        private GameObject m_imgGot;
        private ConfigItemLine m_itemCfg1;
        private ConfigItemLine m_itemCfg2;
        public override void Awake()
        {
            m_textDec = transform.Find("TextDec").GetComponent<Text>();
            m_btnGet = transform.Find("btn_get").gameObject;
            m_btnGo = transform.Find("btn_go").gameObject;
            m_goItem = transform.Find("item_list").gameObject;
            m_icon = transform.Find("Icon").GetComponent<Image>();
            m_imgGot = transform.Find("ImgGot").gameObject;
            UGUIClickHandler.Get(m_btnGet).onPointerClick += onClickBtnGetHandler;
            UGUIClickHandler.Get(m_btnGo).onPointerClick += onClickBtnGoHandler;
            MiniTipsManager.get(m_goItem.transform.FindChild("item1").gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem.transform.FindChild("item2").gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg2 == null) return null;
                return new MiniTipsData() { name = m_itemCfg2.DispName, dec = m_itemCfg2.Desc };
            };
        }

        private void onClickBtnGetHandler(GameObject target, PointerEventData eventData)
        {
            PlayerMissionData.getMissionReward(m_cfgData.ID);
        }

        private void onClickBtnGoHandler(GameObject target, PointerEventData eventData)
        {
            PanelTaskNewHand panel = UIManager.GetPanel<PanelTaskNewHand>();
            panel.HidePanel();
            UIManager.GotoPanel(m_cfgData.GoSystem);
        }

        protected override void OnSetData(object data)
        {
            m_cfgData = data as ConfigMissionLine;
            if(m_cfgData!=null)
                transform.name = m_cfgData.ID.ToString();
            if (m_cfgData == null) return;
            SyncMission serverData = PlayerMissionData.singleton.GetMission(m_cfgData.ID);
            if (serverData == null) return;
            m_textDec.text = m_cfgData.Desc;
            //ConfigItemLine cfg = ItemDataManager.GetItem(m_cfgData.ID);
            //m_icon.SetSprite(ResourceManager.LoadIcon(m_cfgData.Icon), cfg!=null?cfg.SubType:"");
            m_icon.SetSprite(ResourceManager.LoadIcon(m_cfgData.Icon));
            //显示奖励
            ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(m_cfgData.RewardId);
            if (rewardLine != null && rewardLine.ItemList != null && rewardLine.ItemList.Length != 0)
            {
                var info = ItemDataManager.GetItem(rewardLine.ItemList[0].ID);
                if (info.Type == GameConst.ITEM_TYPE_EQUIP)
                {

                }
                else
                {
                    m_goItem.TrySetActive(true);

                    ConfigItemLine cfgItem;
                    int lngth = rewardLine.ItemList.Length;
                    for (int i = 0; i < lngth; ++i)
                    {
                        if (i > 1)
                        {
                            break;
                        }
                        string icon = string.Format("item{0}/icon", (i + 1).ToString());
                        string num = string.Format("item{0}/num", (i + 1).ToString());
                        string name = string.Format("item{0}/name", (i + 1).ToString());
                        string discription = string.Format("item{0}/discription", (i + 1).ToString());
                        cfgItem = ItemDataManager.GetItem(rewardLine.ItemList[i].ID);
                        if (i == 0) m_itemCfg1 = cfgItem;
                        else m_itemCfg2 = cfgItem;
                        m_goItem.transform.FindChild(icon).GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(rewardLine.ItemList[i].ID), cfgItem != null ? cfgItem.SubType : "");
                        //m_goItem.transform.FindChild(icon).GetComponent<Image>().SetNativeSize();
                        m_goItem.transform.FindChild(num).GetComponent<Text>().text = "X" + rewardLine.ItemList[i].cnt;
                    }

                    if (lngth >= 2)
                    {
                        m_goItem.transform.FindChild("item2").gameObject.TrySetActive(true);
                    }
                    else
                    {
                        m_goItem.transform.FindChild("item2").gameObject.TrySetActive(false);
                    }
                }
            }
            m_imgGot.TrySetActive(false);
            if (serverData.m_status == SyncMission.MISSION_STATE.ACCEPT || serverData.m_status == SyncMission.MISSION_STATE.DOING)
            {
                m_btnGet.TrySetActive(false);
                m_btnGo.TrySetActive(true);
            }
            else if (serverData.m_status == SyncMission.MISSION_STATE.DONE)
            {
                m_btnGet.TrySetActive(true);
                m_btnGo.TrySetActive(false);
            }
            else
            {
                m_btnGet.TrySetActive(false);
                m_btnGo.TrySetActive(false);
                m_imgGot.TrySetActive(true);
            }
        }
    }


    private GameObject m_itemGO;
    private DataGrid m_dataGrid;
    private Text m_textDec;
    private GameObject m_btnGet;
    private GameObject m_btnGo;
    private GameObject m_btnClose;

    private GameObject m_goRenderTexture;
    //private Camera m_weaponCamera;
    //private Transform m_tranWeapon;
    private Image m_imgEquip;
    //private string m_lastWeaponModeName;
    private float m_rotationCenter = 3000.12f; //模型旋转的中心
    private int m_rotateDir = 1;
    private bool m_userDraging;     //用户正在拖拽模型

    private int weaponId;
    private ConfigMissionLine levelTask;
    private Text m_weaponName;

    private ConfigItemLine m_itemCfg;
    private GameObject m_weaponDrag;
    private RawImage m_weaponModel;

    public PanelTaskNewHand()
    {
        SetPanelPrefabPath("UI/TaskNewHand/PanelTaskNewHand");

        AddPreLoadRes("UI/TaskNewHand/PanelTaskNewHand", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        //AddPreLoadRes("UI/Strengthen/WeaponTexture", EResType.RenderTexture);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON, EResType.Material);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);

    }
    public override void Init()
    {
        m_itemGO = m_tran.Find("ListFrame/Content/ItemTask").gameObject;
        m_dataGrid = m_tran.Find("ListFrame/Content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_itemGO, typeof(ItemTaskNewHand));
        m_textDec = m_tran.Find("WeaponFrame/TaskDec").GetComponent<Text>();
        m_btnGet = m_tran.Find("WeaponFrame/btn_get").gameObject;
        m_btnGo = m_tran.Find("WeaponFrame/btn_go").gameObject;
        m_btnClose = m_tran.Find("bg/close").gameObject;

        m_weaponDrag = m_tran.Find("WeaponFrame/weaponDrag").gameObject;
        m_weaponDrag.AddComponent<SortingOrderRenderer>();

        m_goRenderTexture = m_tran.Find("WeaponFrame/weaponImage").gameObject;
        //m_goRenderTexture.GetComponent<RawImage>().texture = WeaponDisplay.RenderTexture;
        m_imgEquip = m_tran.Find("WeaponFrame/imgEquip").GetComponent<Image>();
        m_weaponName = m_tran.Find("WeaponFrame/weaponName").GetComponent<Text>();
        //m_weaponCamera = new GameObject("WeaponCamera").AddComponent<Camera>();
        //m_weaponCamera.orthographic = false;
        //m_weaponCamera.orthographicSize = 0.51f;
        //m_weaponCamera.fieldOfView = 35;
        //m_weaponCamera.farClipPlane = 5;
        //m_weaponCamera.targetTexture = ResourceManager.LoadRenderTexture("UI/Strengthen/WeaponTexture");
        //m_weaponCamera.transform.SetLocation(null, m_rotationCenter, 0f, 1.4f);
        //m_weaponCamera.transform.localRotation = Quaternion.Euler(0, 180f, 0);

        m_weaponModel = m_goRenderTexture.GetComponent<RawImage>();
        //m_goRenderTexture.AddComponent<CanvasGroup>().blocksRaycasts = false;
        m_weaponModel.enabled = true;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnGet).onPointerClick += onClickBtnGetHandler;
        UGUIClickHandler.Get(m_btnGo).onPointerClick += onClickBtnGoHandler;
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnCloseHandler;
        UGUIDragHandler.Get(m_weaponDrag).onDrag += OnDrag;
        UGUIUpDownHandler.Get(m_goRenderTexture).onPointerDown += delegate { m_userDraging = true; };
        UGUIUpDownHandler.Get(m_goRenderTexture).onPointerUp += delegate { m_userDraging = false; };
        //AddEventListener(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED, () => { UpdateView(); });
        MiniTipsManager.get(m_tran.Find("WeaponFrame").gameObject).miniTipsDataFunc = (go) =>
        {
            if (m_itemCfg == null) return null;
            return new MiniTipsData() { name = m_itemCfg.DispName, dec = m_itemCfg.Desc };
        };
    }

    private void onClickBtnGetHandler(GameObject target, PointerEventData eventData)
    {
        SyncMission serverData = PlayerMissionData.singleton.GetMission(levelTask.ID);
        if (serverData == null) return;
        if (serverData.m_status == SyncMission.MISSION_STATE.DONE)
        {
            PlayerMissionData.getMissionReward(levelTask.ID);            
        }         
        else
            TipsManager.Instance.showTips("等级不足，请升级后再来领取");
    }

    private void onClickBtnGoHandler(GameObject target, PointerEventData eventData)
    {
        UIManager.GotoPanel(levelTask.GoSystem);
        HidePanel();
    }

    private void onClickBtnCloseHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnDrag(GameObject target, PointerEventData eventData)
    {
        WeaponDisplay.Rotation(eventData.delta.x);
    }

    //private void UpdateMode(int weaponId)
    //{
    //    var _data = ItemDataManager.GetItemWeapon(weaponId);
    //    if (_data != null)
    //    {
    //        if (m_lastWeaponModeName != _data.MPModel)
    //        {
    //            m_lastWeaponModeName = _data.MPModel;
    //            ResourceManager.LoadModel("Weapons/" + m_lastWeaponModeName, (go) =>
    //            {
    //                if (!m_go)
    //                {
    //                    Object.Destroy(go);
    //                    return;
    //                }

    //                if (m_tranWeapon)
    //                    Object.DestroyImmediate(m_tranWeapon.gameObject);

    //                m_goRenderTexture.TrySetActive(true);
    //                m_tranWeapon = go.transform;
    //                m_tranWeapon.localPosition = new Vector3(3000, 0, 0);
    //                m_tranWeapon.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));

    //                // 排除特效节点
    //                ExtendFuncHelper.CallBackBool<GameObject> funVisit = (GameObject child) =>
    //                {
    //                    string name = child.name.ToLower();
    //                    if (name.ToLower().StartsWith(ModelConst.WEAPON_EFFECT))
    //                        return false;

    //                    Renderer render = child.GetComponent<Renderer>();
    //                    if (render != null)
    //                    {
    //                        var texture = render.sharedMaterial.mainTexture;
    //                        if (m_tranWeapon.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
    //                            render.material = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON_CRYSTAL);
    //                        else
    //                            render.material = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON);
    //                        render.material.mainTexture = texture;
    //                    }
    //                    return true;
    //                };

    //                m_tranWeapon.VisitChild(funVisit, true);

    //            }, true);
    //        }
    //        else
    //        {
    //            if (m_goRenderTexture)
    //                m_goRenderTexture.TrySetActive(true);
    //        }

    //    }
    //}



    public override void OnShow()
    {
        UpdateView();
        //if (m_weaponCamera)
        //    m_weaponCamera.gameObject.TrySetActive(true);
        //if (m_tranWeapon)
        //    m_tranWeapon.gameObject.TrySetActive(true);
    }

    public void UpdateView()
    {
        List<ConfigMissionLine> dataList = new List<ConfigMissionLine>();
        List<int> list = PlayerMissionData.singleton.GetNewHandMissionList();
        ConfigMissionLine cfg;
        int lngth = list.Count;
        levelTask = null;
        for (int i = 0; i < lngth; i++)
        {
            cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(list[i]) as ConfigMissionLine;
            if (cfg != null)
            {
                if (cfg.Type == (int)PlayerMissionData.MISSION_TYPE.NEWHAND_LEVEL)
                {
                    levelTask = cfg;
                }
                else
                {
                    dataList.Add(cfg);
                }
            }
        }
        dataList.Sort((a, b) =>
        {
            if (a.Index > b.Index)
            {
                return 1;
            }
            else if (a.Index < b.Index)
            {
                return -1;
            }
            return 0;
        });
        m_dataGrid.Data = dataList.ToArray();
        if (levelTask == null)
        {
            HidePanel();
            return;
        }
        if (levelTask != null)
        {
            m_textDec.text = levelTask.Desc;
            ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(levelTask.RewardId);
            if (rewardLine != null && rewardLine.ItemList != null && rewardLine.ItemList.Length != 0)
            {
                var info = ItemDataManager.GetItem(rewardLine.ItemList[0].ID);
                m_itemCfg = info;
                if (info.Type == GameConst.ITEM_TYPE_EQUIP)
                {
                    m_weaponModel.texture = WeaponDisplay.RenderTextureBig;
                    m_weaponModel.SetNativeSize();
                    WeaponDisplay.Visible = true;
                    //UpdateMode(rewardLine.ItemList[0].ID);
                    ConfigItemWeaponLine weaponLine = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(rewardLine.ItemList[0].ID);
                    if (weaponLine.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || weaponLine.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2)
                    {
                        WeaponDisplay.UpdateMode(weaponLine);
                    }
                    m_weaponName.text = rewardLine.RewardName;
                    m_goRenderTexture.TrySetActive(true);
                    m_imgEquip.gameObject.TrySetActive(false);
                }
                else
                {
                    m_weaponName.text = rewardLine.RewardName;
                    m_goRenderTexture.TrySetActive(false);
                    m_imgEquip.gameObject.TrySetActive(true);
                }
            }
            SyncMission serverData = PlayerMissionData.singleton.GetMission(levelTask.ID);
            if (serverData == null) return;
            if (serverData.m_status == SyncMission.MISSION_STATE.GOTREWARD) return;
            Util.SetGoGrayShader(m_btnGet, false);
            if (serverData.m_status == SyncMission.MISSION_STATE.ACCEPT || serverData.m_status == SyncMission.MISSION_STATE.DOING)
            {
                if (levelTask.GoSystem == "" || levelTask.GoSystem == null)
                {

                    Util.SetGoGrayShader(m_btnGet, true);
                    //TimerManager.SetTimeOut(0.001f, ()=>Util.SetGoGrayShader(m_btnGet, true));
                    m_btnGet.TrySetActive(true);
                    m_btnGo.TrySetActive(false);
                }
                else
                {
                    m_btnGet.TrySetActive(false);
                    m_btnGo.TrySetActive(true);
                }
            }
            else if (serverData.m_status == SyncMission.MISSION_STATE.DONE)
            {
                m_btnGet.TrySetActive(true);
                m_btnGo.TrySetActive(false);
            }
            else
            {
                m_btnGet.TrySetActive(false);
                m_btnGo.TrySetActive(false);
            }
        }
    }

    public override void OnHide()
    {
        //if (m_weaponCamera)
        //    m_weaponCamera.gameObject.TrySetActive(false);
        //if (m_tranWeapon)
        //    m_tranWeapon.gameObject.TrySetActive(false);
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        //if (m_weaponCamera)
        //    Object.Destroy(m_weaponCamera.gameObject);
        //if (m_tranWeapon)
        //    Object.Destroy(m_tranWeapon.gameObject);
    }
    public override void Update()
    {
        //if (m_tranWeapon != null)
        //{
        //    var r = m_tranWeapon.localRotation.eulerAngles.y;
        //    if (r > 120 || r < 60)
        //        m_rotateDir *= -1;
        //    m_tranWeapon.RotateAround(new Vector3(m_rotationCenter, 0, 0), Vector3.up * m_rotateDir, Time.deltaTime * 5);
        //}
        if (!m_userDraging && IsOpen())
            WeaponDisplay.Rotation(-Time.deltaTime * 25);
    }


    void Toc_player_get_mission_reward(toc_player_get_mission_reward data)
    {
        var cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(data.mission_id);
        if(cfg!=null&&!(cfg.Type==3||cfg.Type==2))
        {
            return;
        }
        int iMissionId = data.mission_id;
        ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(PlayerMissionData.singleton.GetMissionRewardId(iMissionId));
        if (rewardLine != null)
        {
            //PanelRewardItemTip tip = UIManager.PopPanel<PanelRewardItemTip>(null, true, this);
            //tip.SetRewardItemList(rewardLine.ItemList);
            PanelRewardItemTipsWithEffect.DoShow(rewardLine.ItemList);
        }
        UpdateView();
    }
}
