﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/2 16:05:34
 * Description:主界面快速开始
 * Update History:
 *
 **************************************************************************/

public class PanelQuickStart : BasePanel
{


    private GameObject m_btnClose;

    private GameObject m_btnQuickStart;

    private GameObject m_itemBtnTab;
    private GameObject m_contentTab;

    private DataGrid m_tabGrid;

    private GameObject m_itemIcon;
    private GameObject m_contentIcon;

    private DataGrid m_iconGrid;

    private const int RANDOM_ID = 0;


    /// <summary>
    /// 图片列表
    /// </summary>
    public class QuickStartIconItem : ItemRender
    {
        private Image m_icon;
        private ConfigQuickStartListLine m_cfg;
        public override void Awake()
        {
            m_icon = transform.Find("Icon").GetComponent<Image>();
        }
        protected override void OnSetData(object data)
        {
            m_cfg = data as ConfigQuickStartListLine;
            if (m_cfg != null)
            {
                m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.QuickStart, m_cfg.Icon));
            }
        }
    }

    /// <summary>
    /// 列表项
    /// </summary>
    public class QuickStartTabItem : ItemRender
    {

        Text m_txtName;
        Toggle m_toggle;
        public override void Awake()
        {
            m_txtName = transform.Find("Text").GetComponent<Text>();
            m_toggle = transform.GetComponent<Toggle>();
            m_toggle.onValueChanged.AddListener(onToggleChange);
        }

        private void onToggleChange(bool selected)
        {
            if (selected)
            {
                m_txtName.color = new Color(1f, 1f, 1f);
            }
            else
            {
                m_txtName.color = new Color(41 / 255f, 48 / 255f, 48 / 255f);
            }
        }

        protected override void OnSetData(object data)
        {
            ConfigQuickStartTypeLine cfg = data as ConfigQuickStartTypeLine;
            if (cfg == null) return;
            m_txtName.text = cfg.Name;
        }
    }



    public PanelQuickStart()
    {
        // 构造器
        SetPanelPrefabPath("UI/QuickStart/PanelQuickStart");

        AddPreLoadRes("UI/QuickStart/PanelQuickStart", EResType.UI);
        AddPreLoadRes("Atlas/QuickStart", EResType.Atlas);
    }

    public override void Init()
    {
        m_btnClose = m_tran.Find("BtnClose").gameObject;
        m_btnQuickStart = m_tran.Find("btnQuickStart").gameObject;

        m_itemBtnTab = m_tran.Find("TabList/ItemBtnTab").gameObject;
        m_itemBtnTab.TrySetActive(false);
        m_contentTab = m_tran.Find("TabList/Content").gameObject;

        m_tabGrid = m_contentTab.AddComponent<DataGrid>();
        m_tabGrid.SetItemRender(m_itemBtnTab, typeof(QuickStartTabItem));


        m_itemIcon = m_tran.Find("ScrollDataGrid/ItemIcon").gameObject;
        m_itemIcon.TrySetActive(false);
        m_contentIcon = m_tran.Find("ScrollDataGrid/Content").gameObject;
        m_iconGrid = m_contentIcon.AddComponent<DataGrid>();
        m_iconGrid.SetItemRender(m_itemIcon, typeof(QuickStartIconItem));
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnCloseHandler;
        m_tabGrid.onItemSelected = onSelectTabGridHandler;
        UGUIClickHandler.Get(m_btnQuickStart).onPointerClick += onClickBtnQuickStart;
    }

    private void onClickBtnQuickStart(GameObject target, PointerEventData eventData)
    {
        ConfigQuickStartListLine cfg = m_iconGrid.SelectedData<ConfigQuickStartListLine>();
        if (cfg == null) return;
        if (cfg.Id == RANDOM_ID)
        {
            //随机选择一个模式
            int random = UnityEngine.Random.Range(1, m_iconGrid.Data.Length);
            cfg = m_iconGrid.Data[random] as ConfigQuickStartListLine;
        }
        if (cfg == null) return;
        RoomModel.m_channel = RoomModel.FindChannelByType(cfg.ChannelType, cfg.ChannelSubType);
        PanelRoomBase.RoomMinimizeCheck(() =>
            {
                NetLayer.Send(new tos_joinroom_quick_join() { channel = RoomModel.m_channel, rule = cfg.GameRule, source = 1});
                HidePanel();
            });
                
    }

    private void onSelectTabGridHandler(object renderData)
    {
        ConfigQuickStartTypeLine cfg = renderData as ConfigQuickStartTypeLine;
        if (cfg == null) return;
        List<ConfigQuickStartListLine> list = new List<ConfigQuickStartListLine>();
        ConfigQuickStartList listCfg = ConfigManager.GetConfig<ConfigQuickStartList>();
        if (listCfg.m_dataArr != null && listCfg.m_dataArr.Length != 0)
        {
            foreach (ConfigQuickStartListLine temp in listCfg.m_dataArr)
            {
                if (cfg.Type == 0)
                {
                    if (temp.IsHot == 1)
                        list.Add(temp);
                }
                else
                {
                    if (temp.Type == cfg.Type)
                        list.Add(temp);
                }
            }
            if (list.Count != 0)
            {
                ConfigQuickStartListLine randomCfg = listCfg.GetLine(RANDOM_ID);
                list.Insert(0, randomCfg);
            }
        }
        m_iconGrid.Data = list.ToArray();
    }

    private void onClickBtnCloseHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    public override void OnShow()
    {
        ConfigQuickStartType typeCfg = ConfigManager.GetConfig<ConfigQuickStartType>();
        if (typeCfg != null)
        {
            m_tabGrid.Data = typeCfg.m_dataArr;
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }
    public override void Update()
    {

    }
}
