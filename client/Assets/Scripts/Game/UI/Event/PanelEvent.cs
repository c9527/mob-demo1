﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class EventTitleData
{
    public int id;
    public string name;
    public string title;
    public int state;
}

class PanelEvent : BasePanel
{
    static public string VIEW_TODAY_FIRST_RECHARGE = "first_recharge";
    static public string VIEW_ACCUMULATIVE_RECHARGE = "accumulative_recharge";
    static public string VIEW_NOTICE = "bulitin";
    static public string VIEW_RECHARGE_DOUBLE_RETURN = "recharge_double_return";
    static public string VIEW_ARSENAL_FAMOUS_GUN = "arsenal_famous_gun";
    static public string VIEW_ITEM_DROP_DOUBLE = "item_drop_double";
    static public string VIEW_INTEGRAL_TIME = "integral_time";
    static public string VIEW_RED_STAR = "redstar";
    static public string VIEW_CARNIVAL = "item_icon_double";
    static public string VIEW_LOGIN_7_DAY = "login_day";
    static public string VIEW_7_HAPPY_DAY = "integral_time2";
    static public string HALLO_WEEN = "halloween";
    static public string HALLO_WEEN1 = "halloween1";
    static public string VIEW_GUN_PROMOTE = "gun_promote";

    public BasePanel current_view;
    private string m_strViewName;

    private GameObject m_goTodayRechargeBubble;
    private GameObject m_goAccumultiveRechargeBubble;
    private Text m_txtAccumulative;
    private List<EventTitleData> m_lstEventTitleData;
    private DataGrid m_eventTitleDataGrid;
    public enum ANNOUNCE_TYPE
    {
        UPDATE_BULITIN = 1,
        RECHARGE_DOUBLE_RETURN = 2,
        ARSENAL_FAMOUSE_GUN = 3
    }
    public Dictionary<string, int> themeSatus = new Dictionary<string, int>() { { "none", 0 }, { "hot", 3 }, { "time", 2 }, { "new", 1 } };

    public PanelEvent()
    {
        SetPanelPrefabPath("UI/Event/PanelEvent");

        AddPreLoadRes("Atlas/RechargeGift", EResType.Atlas);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
        AddPreLoadRes("UI/Event/PanelEvent", EResType.UI);
        AddPreLoadRes("UI/Event/ItemEventTitle", EResType.UI);

    }

    public override void Init()
    {
        m_lstEventTitleData = new List<EventTitleData>();
        m_eventTitleDataGrid = m_tran.Find("title/group/content").gameObject.AddComponent<DataGrid>();
        m_eventTitleDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Event/ItemEventTitle"), typeof(ItemEventTitle));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("bg/close")).onPointerClick += OnClkClose;
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
    }

    private void UpdateBubble()
    {
        if (HasChildPanel(VIEW_TODAY_FIRST_RECHARGE))
            m_goTodayRechargeBubble.TrySetActive(RechargeGiftManager.HasTodayFirstRechargeGift());
        if (HasChildPanel(VIEW_ACCUMULATIVE_RECHARGE))
            m_goAccumultiveRechargeBubble.TrySetActive(RechargeGiftManager.HasAccummultiveRechargeGift());

        List<ItemRender> itemTitles = m_eventTitleDataGrid.ItemRenders;
        foreach (var item in itemTitles)
        {
            ItemEventTitle title = item as ItemEventTitle;
            title.ResetBubble();
        }
    }

    public override void OnShow()
    {
        //var view_name = VIEW_NOTICE;
        //if (HasParams())
        //{
        //    m_strViewName = m_params[0] as string;
        //}
        PanelSevenDayReward.firstOpen = false;
        current_view = null;
        m_lstEventTitleData.Clear();
        int curUtcTime = TimeUtil.GetNowTimeStamp();


        var themeConfig = ConfigManager.GetConfig<ConfigEventTheme>();
        ConfigEventThemeLine themeLine;
        ConfigEventListLine eventListLine;
        for (int k = 0; k < themeConfig.m_dataArr.Length; k++)
        {
            themeLine = themeConfig.m_dataArr[k];
            if (themeLine.EventList != null && themeLine.EventList.Length != 0)
            {
                bool isShow = false;
                int lngth = themeLine.EventList.Length;
                for (int i = 0; i < lngth; i++)
                {
                    isShow = false;
                    eventListLine = ConfigManager.GetConfig<ConfigEventList>().GetLine(themeLine.EventList[i]) as ConfigEventListLine;
                    if (eventListLine != null)
                    {
                        if (curUtcTime >= uint.Parse(eventListLine.VisibleStart) && curUtcTime <= uint.Parse(eventListLine.VisibleEnd))
                        {
                            if (themeLine.ThemeId == 124 || themeLine.ThemeId == 125)
                            {
                                //这两个活动只有老玩家可以看到
                                if (!ActivityFrameDataManager.oldPlayerInfo_isOld)
                                {
                                    continue;
                                }
                            }
                            else if (themeLine.ThemeId == 126)
                            {//如果玩家已经绑定过邀请码 则不显示
                                if (ActivityFrameDataManager.oldPlayerInfo_isInvite || !ActivityFrameDataManager.oldPlayerInfo_isOld)
                                {
                                    continue;
                                }
                            }
                            else if (themeLine.ThemeId == 154 || themeLine.ThemeId == 155)
                            {//PC端和安卓端登陆活动 则判断对应的活动是否开启
                                if (!ActivityFrameDataManager.isEventOpen(eventListLine.Id))
                                {
                                    continue;
                                }
                            }
                            else if (themeLine.ThemeId == 298 || themeLine.ThemeId == 299 || themeLine.ThemeId == 300)
                            {
                                if (!ActivityFrameDataManager.newPlayerInfo_isNew)
                                {
                                    continue;
                                }
                            }
                            if (eventListLine.EventType != "client")
                            {//不是
                                if (!ActivityFrameDataManager.isHasEvent(eventListLine.Id))
                                {
                                    continue;
                                }
                            }
                            if (eventListLine.ServerLimit!=null&&eventListLine.ServerLimit.Length != 0)
                            {
                                //当期服务器没在服务器列表中则不显示
                                if (eventListLine.ServerLimit.ToList<int>().IndexOf(PlayerSystem.serverId) == -1)
                                {
                                    continue;
                                }
                            }
                            isShow = true;
                            break;
                        }
                    }
                }

                //按渠道号屏蔽活动
                if (themeLine.SkipChannel.Length == 0 && string.IsNullOrEmpty(themeLine.Device))
                    isShow = isShow;
                else if ((themeLine.SkipChannel.Length == 0 || Array.IndexOf(themeLine.SkipChannel, SdkManager.PlatformForSeparate) != -1) &&
                    (string.IsNullOrEmpty(themeLine.Device) || themeLine.Device == Driver.m_platform.ToString()))
                    isShow = false;

                if (isShow)
                {
                    EventTitleData data = new EventTitleData();
                    data.id = themeLine.ThemeId;
                    data.name = themeLine.ThemeId + "";
                    data.title = themeLine.Name;
                    data.state = themeSatus[themeLine.Status.ToLower()];
                    m_lstEventTitleData.Add(data);
                }
            }
        }



        var config = ConfigManager.GetConfig<ConfigEvent>();

        for (int i = 0; i < config.m_dataArr.Length; ++i)
        {
            ConfigEventLine line = config.m_dataArr[i];
            int idx = m_lstEventTitleData.FindIndex(x => x.title == line.Title);//Name有特殊作用，改成用Title分组
            if (idx > -1)
                continue;


            if (curUtcTime >= line.ViewStartTime && curUtcTime <= line.ViewEndTime)
            {
                if (line.Name == VIEW_ACCUMULATIVE_RECHARGE)
                {
                    DateTime deadTime = new DateTime(2015, 12, 3, 23, 59, 59);
                    int deadTimeStamp = TimeUtil.GetTimeStamp(deadTime);
                    DateTime aliveTime = new DateTime(2015, 12, 9, 23, 59, 59);
                    int aliveTimeStamp = TimeUtil.GetTimeStamp(aliveTime);

                    int nowTimeStamp = TimeUtil.GetNowTimeStamp();
                    if (nowTimeStamp > deadTimeStamp && nowTimeStamp < aliveTimeStamp)
                    {
                        if (RechargeGiftManager.m_accumultiveRecharge != null && RechargeGiftManager.m_accumultiveRecharge.stage != PanelAccumulativeRecharge.STAGE_NEW)
                            continue;
                    }
                }

                EventTitleData data = new EventTitleData();
                data.id = line.Id;
                data.name = line.Name;
                data.title = line.Title;
                data.state = line.State;
                m_lstEventTitleData.Add(data);
            }
        }
        if (m_lstEventTitleData.Count == 0)
            return;

        m_strViewName = m_lstEventTitleData[0].name;
        m_eventTitleDataGrid.Data = m_lstEventTitleData.ToArray();

        if (HasChildPanel(VIEW_TODAY_FIRST_RECHARGE))
            m_goTodayRechargeBubble = m_tran.Find("title/group/content/first_recharge/bubble_tip").gameObject;

        if (HasChildPanel(VIEW_ACCUMULATIVE_RECHARGE))
        {
            m_goAccumultiveRechargeBubble = m_tran.Find("title/group/content/accumulative_recharge/bubble_tip").gameObject;
            m_txtAccumulative = m_tran.Find("title/group/content/accumulative_recharge/label").GetComponent<Text>();
        }
        m_eventTitleDataGrid.Select(0);
        OnSwitchToView(m_lstEventTitleData[0].name, m_eventTitleDataGrid.ItemRenders[0] as ItemEventTitle, m_lstEventTitleData[0].id);

        if (HasChildPanel(VIEW_TODAY_FIRST_RECHARGE))
            m_goTodayRechargeBubble.TrySetActive(RechargeGiftManager.HasTodayFirstRechargeGift());

        if (HasChildPanel(VIEW_ACCUMULATIVE_RECHARGE))
        {
            m_goAccumultiveRechargeBubble.TrySetActive(RechargeGiftManager.HasAccummultiveRechargeGift());
            if (RechargeGiftManager.IsInAccumulativeNew())
                m_txtAccumulative.text = "新手累计充值";
            else
                m_txtAccumulative.text = "累计充值";
        }

        UpdateBubble();
    }

    private void Toc_player_is_oldplayer(toc_player_is_oldplayer data)
    {
        if (data.is_invited==1)
        {
            for (int i = 0; i < m_lstEventTitleData.Count; i++)
            {
                if (m_lstEventTitleData[i].id == 997)
                {
                    m_lstEventTitleData.RemoveAt(i);
                    break;
                }
            }
        }
        if (m_lstEventTitleData.Count == 0)
            return;

        m_strViewName = m_lstEventTitleData[0].name;
        m_eventTitleDataGrid.Data = m_lstEventTitleData.ToArray();
        m_eventTitleDataGrid.Select(0);
        OnSwitchToView(m_lstEventTitleData[0].name, m_eventTitleDataGrid.ItemRenders[0] as ItemEventTitle, m_lstEventTitleData[0].id);
    }


    public void OnGetTodayFirstGift()
    {
        m_goTodayRechargeBubble.TrySetActive(false);
    }

    public void SwitchToView(string name)
    {
        for (int i = 0; i < m_lstEventTitleData.Count; ++i)
        {
            if (m_lstEventTitleData[i].name == name)
            {
                m_eventTitleDataGrid.Select(i);
                //m_eventTitleDataGrid.ResetScrollPosition(i);
                OnSwitchToView(m_lstEventTitleData[i].name,
                                m_eventTitleDataGrid.ItemRenders[i] as ItemEventTitle, m_lstEventTitleData[i].id);
                return;
            }
        }
    }
    public void OnSwitchToView(string name, ItemEventTitle title, int id)
    {
        var old_view = current_view;
        int themeId = 0;
        if (int.TryParse(name, out themeId))
        {
            ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(themeId) as ConfigEventThemeLine;
            if (cfg.PageType == ConfigEventTheme.PAGETYPE_LIST)
            {
                current_view = UIManager.PopPanel<PanelActivityFrameList>(new object[] { themeId }, false, this);
            }
            else if (cfg.PageType == ConfigEventTheme.PAGETYPE_EXCHANGE)
            {
                current_view = UIManager.PopPanel<PanelExchange>(new object[] { themeId }, false, this);
            }
            else if (cfg.PageType == ConfigEventTheme.PAGETYPE_INVITEBACK)
            {
                current_view = UIManager.PopPanel<PanelActivityFrameInviteBack>(new object[] { themeId }, false, this);
            }
            else if (cfg.PageType == ConfigEventTheme.PAGETYPE_INVITEBIND)
            {
                current_view = UIManager.PopPanel<PanelActivityFrameInviteBind>(new object[] { themeId }, false, this);
            }
            else
            {
                current_view = UIManager.PopPanel<PanelActivityFrameGo>(new object[] { themeId }, false, this);
            }
            if (old_view == current_view)
            {
                current_view.OnShow();
            }
        }
        else if (name == VIEW_NOTICE)
        {
            current_view = UIManager.PopPanel<PanelUpdateBulitin>(null, false, this);
        }
        else if (name == VIEW_ACCUMULATIVE_RECHARGE)
        {
            current_view = UIManager.PopPanel<PanelAccumulativeRecharge>(new object[] { RechargeGiftManager.m_accumultiveRecharge }, false, this);
        }
        else if (name == VIEW_TODAY_FIRST_RECHARGE)
        {
            current_view = UIManager.PopPanel<PanelTodayFirstRechargeGift>(new object[] { RechargeGiftManager.m_todayFirstRecharge  }, false, this);
           // current_view = UIManager.PopPanel<PanelActivityFrameList>(new object[] { 101 }, false, this);
            //current_view = UIManager.PopPanel<PanelActivityFrameGo>(new object[] { 102 }, false, this);
        }
        else if (name == VIEW_RECHARGE_DOUBLE_RETURN)
        {
            current_view = UIManager.PopPanel<PanelRechargeDoubleReturn>(null, false, this);
        }
        else if (name == VIEW_ARSENAL_FAMOUS_GUN)
        {
            current_view = UIManager.PopPanel<PanelArsenalFamousGun>(null, false, this);
        }
        else if (name == VIEW_ITEM_DROP_DOUBLE)
        {
            current_view = UIManager.PopPanel<PanelItemDropDouble>(new object[] { title, id }, false, this);
        }
        else if (name == VIEW_INTEGRAL_TIME)
        {
            current_view = UIManager.PopPanel<PanelSendGunInTime>(new object[] { title, id }, false, this);
        }
        else if (name == VIEW_RED_STAR)
        {
            current_view = UIManager.PopPanel<PanelRedstar>(new object[] { title, id }, false, this);
        }
        else if (name == VIEW_CARNIVAL)
        {
            current_view = UIManager.PopPanel<PanelCarnival>(new object[] { title, id }, false, this);
        }
        else if (name == VIEW_LOGIN_7_DAY)
        {
            current_view = UIManager.PopPanel<PanelLoginSevenDay>(new object[] { title, id }, false, this);
        }
        else if (name == VIEW_7_HAPPY_DAY)
        {
            current_view = UIManager.PopPanel<Panel7HappyDay>(new object[] { title, id }, false, this);
        }
        else if (name == "redstargun")
        {
            current_view = UIManager.PopPanel<PanelRedStarGun>(new object[] { title, id }, false, this);
        }
        else if (name == "colourgun")
        {
            current_view = UIManager.PopPanel<PanelEssenceGun>(new object[] { title, id }, false, this);
        }
        else if (name == HALLO_WEEN)
        {
            current_view = UIManager.PopPanel<PanelHalloween>(null, false, this);
        }
        else if (name == HALLO_WEEN1)
        {
            current_view = UIManager.PopPanel<PanelHalloween1>(null, false, this);
        }
        else if (name == VIEW_GUN_PROMOTE)
        {
            if (current_view is PanelEventGunPromote)
            {
                current_view.HidePanel();
            }
            current_view = UIManager.PopPanel<PanelEventGunPromote>(new object[] { id }, false, this);
        }
        else if (name == "daily_double")
        {
            current_view = UIManager.PopPanel<PanelGetDailyDoubleBuffer>(new object[] { title, id }, false, this);
        }

        m_strViewName = name;
        if (old_view != null && old_view != current_view)
        {
            old_view.HidePanel();
        }
    }

    private bool HasChildPanel(string name)
    {
        for (int i = 0; i < m_lstEventTitleData.Count; ++i)
        {
            if (m_lstEventTitleData[i].name == name)
                return true;
        }

        return false;
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }
}
