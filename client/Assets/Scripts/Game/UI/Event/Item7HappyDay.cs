﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Item7HappyDay : ItemRender
{
    private SevenHappyDayData m_data;
    private GameObject m_goBtn;
    private Text m_txtBtn;
    private Image m_imgItem;
    private Text m_txtItem;
    private Text m_txtOnline;
    ConfigEventWelfareLine m_welLine;
    public override void Awake()
    {
        m_goBtn = transform.Find("btn_get").gameObject;
        m_txtBtn = transform.Find("btn_get/Label").GetComponent<Text>();
        m_imgItem = transform.Find("item_img").GetComponent<Image>();
        m_txtItem = transform.Find("item_num").GetComponent<Text>();
        m_txtOnline = transform.Find("online").GetComponent<Text>();
        UGUIClickHandler.Get(m_goBtn).onPointerClick += OnClickGet;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as SevenHappyDayData;

        var config = ConfigManager.GetConfig<ConfigEventWelfare>();
        m_welLine = config.GetLine(m_data.welId);

        ItemInfo itemInfo = m_welLine.Item[0];
        ConfigItemLine itemLine = ItemDataManager.GetItem(itemInfo.ID);
        m_imgItem.SetSprite(ResourceManager.LoadIcon(itemLine.Icon),itemLine.SubType);
        m_imgItem.SetNativeSize();
        m_txtItem.text = itemLine.DispName + " " + "X" + itemInfo.cnt + " ";
        if (itemInfo.day == 0)
        {
            m_txtItem.text += "(永久)";
        }
        else if (itemInfo.day > 0)
        {
            m_txtItem.text += string.Format("({0}天)", itemInfo.day);
        }

        var info = ConfigManager.GetConfig<ConfigEvent>().GetLine(m_data.id);
        m_txtOnline.text = info != null && info.Params != null && info.Params.Length > 1 ? info.Params[1] : "";
        //m_txtOnline.text = string.Format("12月{0}号 15:00在线", m_data.day.ToString() );
        int status = EventManager.GetWelfareStatus(m_data.welId);
        if (status == (int)EventManager.Event_Status.S_RECEIVED)
        {
            m_txtBtn.text = "已领取";
            Util.SetGrayShader(m_goBtn.GetComponent<Image>());
        }
        else if( status == (int)EventManager.Event_Status.S_ALLOW_RECEIVE )
        {
            m_txtBtn.text = "领取";
            Util.SetNormalShader(m_goBtn.GetComponent<Image>());
        }
        else if( status == (int)EventManager.Event_Status.S_UN_GET )
        {
            m_txtBtn.text = "未达成";
            Util.SetNormalShader(m_goBtn.GetComponent<Image>());
        }
    }

    private void OnClickGet(GameObject target, PointerEventData eventData)
    {
        int status = EventManager.GetWelfareStatus(m_data.welId);
        if (status != (int)EventManager.Event_Status.S_ALLOW_RECEIVE)
            return;

        tos_player_get_event msg = new tos_player_get_event();
        msg.id = m_data.welId;
        NetLayer.Send(msg);

        m_txtBtn.text = "已领取";
        Util.SetGrayShader(m_goBtn.GetComponent<Image>());
        EventManager.SetWelfareStatus(m_data.welId, (int)EventManager.Event_Status.S_RECEIVED);

        var config = ConfigManager.GetConfig<ConfigEventWelfare>();
        ConfigEventWelfareLine line = config.GetLine(m_data.welId);
        if (line != null)
        {
            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] { true }, true, null);
            //panelItemTip.SetRewardItemList(line.Item);
            PanelRewardItemTipsWithEffect.DoShow(line.Item);
        }
    }

}
