﻿using System;
using System.Collections;
using System.Collections.Generic;
using DockingModule;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;

class PanelShare : BasePanel
{
    private Transform _current_view;
    private int _qcode_avatar_index;
    private string[] _qcode_avatar_ary;
    private TextEditor _editor;
    private Dictionary<int, int> _mallAmoutDic;

    public Transform main_frame;
    public Text desc_txt;
    public Text value_txt;
    public Text total_value_txt;
    public Text sharelink_txt;
    public Image qcode_img;
    public Text cash_txt;
    public Button share_btn;

    public Transform shop_frame;
    public DataGrid item_list;

    public Transform pop_frame;
    public Transform supplement_win;
    public InputField name_txt;

    public Dictionary<int, int> MallAmoutDic
    {
        get { return _mallAmoutDic; }
    }

    public PanelShare()
    {
        SetPanelPrefabPath("UI/Activity/PanelShare");
        
        AddPreLoadRes("Atlas/ShareAvatar", EResType.Atlas);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
        AddPreLoadRes("UI/Activity/PanelShare", EResType.UI);
    }

    public override void Init()
    {
        _editor = new TextEditor();
        _qcode_avatar_ary = ConfigManager.GetConfig<ConfigMisc>().GetLine("qcode_share_avatar").ValueStr.Split(';');

        cash_txt = m_tran.Find("cash_txt/Text").GetComponent<Text>();

        _mallAmoutDic = new Dictionary<int, int>();

        main_frame = m_tran.Find("main_frame");
        desc_txt = main_frame.Find("desc_txt").GetComponent<Text>();
        value_txt = main_frame.Find("value_txt").GetComponent<Text>();
        total_value_txt = main_frame.Find("total_value_txt").GetComponent<Text>();
        sharelink_txt = main_frame.Find("sharelink_txt").GetComponent<Text>();
        qcode_img = main_frame.Find("qcode_img").GetComponent<Image>();
        share_btn = main_frame.Find("share_btn").GetComponent<Button>();

        //if (Driver.m_platform == EnumPlatform.ios)
        //    extend_btn.gameObject.TrySetActive(true);

        shop_frame = m_tran.Find("shop_frame");
        item_list = shop_frame.Find("item_list/content").gameObject.AddComponent<DataGrid>();
        item_list.autoSelectFirst = false;
        item_list.SetItemRender(shop_frame.Find("item_list/Render").gameObject,typeof(ShareShopItemRender));

        pop_frame = m_tran.Find("pop_frame");
        supplement_win = pop_frame.Find("supplement_win");
        name_txt = supplement_win.Find("name_txt").GetComponent<InputField>();
        name_txt.gameObject.AddComponent<InputFieldFix>();
        share_btn.gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("tab_bar/btn_0")).onPointerClick += OnSwitchView;
        UGUIClickHandler.Get(m_tran.Find("tab_bar/btn_1")).onPointerClick += OnSwitchView;

        UGUIClickHandler.Get(share_btn.gameObject).onPointerClick += OnShareCash;
        UGUIClickHandler.Get(main_frame.Find("link_btn")).onPointerClick += (target, evt) =>
        {
            SdkManager.CopyToClipBoard(sharelink_txt.text.Trim());  
            /* _editor.content.text = sharelink_txt.text;
            _editor.OnFocus();
            _editor.Copy();
#if UNITY_ANDROID
            SdkManager.CopyToClipBoard(sharelink_txt.text.Trim());  
#endif*/
        };
        UGUIClickHandler.Get(main_frame.Find("pre_btn")).onPointerClick += OnChangeQCodeAvatar;
        UGUIClickHandler.Get(main_frame.Find("next_btn")).onPointerClick += OnChangeQCodeAvatar;
        UGUIClickHandler.Get(main_frame.Find("save_btn")).onPointerClick += OnSaveQCodeImg;

        item_list.onItemSelected = OnShareShopItemClick;

        UGUIClickHandler.Get(supplement_win.Find("ok_btn")).onPointerClick += OnSupplementShareReward;
        UGUIClickHandler.Get(supplement_win.Find("btnClose")).onPointerClick += (target,evt)=>pop_frame.gameObject.TrySetActive(false);

        AddEventListener(GameEvent.PROXY_EVENT_SHARE_DATA_UPDATED, OnShareDataUpdated);
    }

    private void OnUpdatePlayerInfo()
    {
        cash_txt.text = PlayerSystem.roleData.cash.ToString();
    }

    private void OnShareDataUpdated()
    {
        sharelink_txt.text = EventModel.Instance.share_url;
        if (Driver.isMobilePlatform)
            Driver.singleton.StartCoroutine(OnUpdateQCodeImage());
    }

    private void OnSwitchView(GameObject target, PointerEventData eventData)
    {
        var old_view = _current_view;
        if (target != null && target.name == "btn_1")
        {
            _current_view = shop_frame;
        }
        else
        {
            _current_view = main_frame;
        }
        if (_current_view!=old_view)
        {
            pop_frame.gameObject.TrySetActive(false);
            if (old_view != null)
            {
                old_view.gameObject.TrySetActive(false);
            }
            _current_view.gameObject.TrySetActive(true);
            cash_txt.text = PlayerSystem.roleData.cash.ToString();
            if (_current_view == shop_frame)
            {
                item_list.Data = ItemDataManager.GetMallItemsByShopType(1);
            }
        }
    }

    private void OnSaveQCodeImg(GameObject target, PointerEventData eventData)
    {
        if (qcode_img.sprite != null)
        {
            var sprite = qcode_img.sprite;
            var texture = new Texture2D((int)sprite.rect.width,(int)sprite.rect.height);
            texture.SetPixels32(sprite.texture.GetPixels32());
            sprite = qcode_img.transform.Find("avatar_img").GetComponent<Image>().sprite;
            var avatar_scale = 1f;// Mathf.Min(sprite.rect.height > sprite.rect.width ? 100 / sprite.rect.height : 100 / sprite.rect.width, 1);
            var avatar_roate = Quaternion.Euler(Vector3.forward * 0);
            var offset = new Vector4(texture.width / 2, texture.height / 2);
            for (int i = 0; i < sprite.rect.width; i++)
            {
                for (int j = 0; j < sprite.rect.height; j++)
                {
                    var color = sprite.texture.GetPixel(i, j);
                    if (color.a > 0.2)
                    {
                        var target_point = Matrix4x4.TRS(Vector3.zero, avatar_roate, Vector3.one * avatar_scale) * (new Vector2(i, j) - sprite.rect.size / 2) + offset;
                        var p_i = Mathf.RoundToInt(target_point.x);
                        var p_j = Mathf.RoundToInt(target_point.y);
                        texture.SetPixel(p_i, p_j, color);
                    }
                }
            }
            var file_data = texture.EncodeToPNG();
            FileStream file = File.Open(UpdateWorker.SdRootPath + "custom_share_qcode.png", FileMode.Create);
            file.Write(file_data, 0, file_data.Length);
            file.Flush();
            file.Close();
            SdkManager.SaveFileToPhotoLib(UpdateWorker.SdRootPath + "custom_share_qcode.png");
        }
        else
        {
            UIManager.ShowTipPanel("二维码图片未加载完成");
        }
    }

    private void OnChangeQCodeAvatar(GameObject target, PointerEventData eventData)
    {
        if (_qcode_avatar_ary != null && _qcode_avatar_ary.Length > 0)
        {
            var step = target != null ? (target.name == "pre_btn" ? -1 : 1) : 0;
            _qcode_avatar_index += step;
            if (_qcode_avatar_index < 0)
            {
                _qcode_avatar_index += _qcode_avatar_ary.Length;
            }
            else
            {
                _qcode_avatar_index %= _qcode_avatar_ary.Length;
            }
            //ResourceManager.LoadTexture(_qcode_avatar_ary[_qcode_avatar_index],SetupQCodeAvatar);
            qcode_img.transform.Find("avatar_img").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.ShareAvatar,_qcode_avatar_ary[_qcode_avatar_index]));
            qcode_img.transform.Find("avatar_img").GetComponent<Image>().SetNativeSize();
        }
        else
        {
            qcode_img.transform.Find("avatar_img").GetComponent<Image>().SetSprite(null);
            qcode_img.transform.Find("avatar_img").GetComponent<Image>().SetNativeSize();
        }
    }

    private void SetupQCodeAvatar(Texture2D img)
    {
        var name = _qcode_avatar_ary != null && _qcode_avatar_ary.Length > 0 ? _qcode_avatar_ary[_qcode_avatar_index] : null;
        if (string.IsNullOrEmpty(name) || name!=img.name)
        {
            return;
        }
        var avatar_img = qcode_img.transform.Find("avatar_img").GetComponent<Image>();
        avatar_img.SetSprite(Sprite.Create(img, new Rect(0, 0, img.width, img.height), Vector2.one / 2));
        //avatar_img.transform.localScale = new Vector3(0.4f, 0.4f, 1);
        //avatar_img.transform.eulerAngles = new Vector3(0,0,-22);
        avatar_img.SetNativeSize();
    }

    private void OnShareCash(GameObject target, PointerEventData eventData)
    {
        SdkManager.GetShareData("mutiple");
    }

    private void OnShareShopItemClick(object renderData)
    {
        var shop_data = renderData as ConfigMallLine;
        if (shop_data != null)
        {

            var itemRender = item_list.TargetGo.GetComponent<ShareShopItemRender>();

            if (itemRender != null && itemRender.CanBuy())
            {
                UIManager.PopPanel<PanelBuy>(new object[] { shop_data }, true, this);
            }
        }
    }

    private void OnSupplementShareReward(GameObject target, PointerEventData eventData)
    {
        pop_frame.gameObject.TrySetActive(false);
        var name = name_txt.text;
        //////////////////////////////////////////////////////////////////////////
    }

    public override void OnShow()
    {
        _current_view = null;
        _qcode_avatar_index = 0;
        m_tran.Find("tab_bar").GetComponent<ToggleGroup>().SetAllTogglesOff();
        m_tran.Find("tab_bar/btn_0").GetComponent<Toggle>().isOn = true;
        main_frame.gameObject.TrySetActive(false);
        shop_frame.gameObject.TrySetActive(false);
        pop_frame.gameObject.TrySetActive(false);
        main_frame.Find("save_btn").gameObject.TrySetActive(Driver.isMobilePlatform);

        NetLayer.Send(new tos_player_mall_record());

        if (GlobalConfig.serverValue.shareCash_open)
        {
            if (Driver.m_platform == EnumPlatform.android)
            {
                share_btn.gameObject.TrySetActive(true);
            }
        }
        else
        {
            share_btn.gameObject.TrySetActive(false);
        }
        if (HasParams() && m_params[0].ToString() == "shop")
        {
            OnSwitchView(m_tran.Find("tab_bar/btn_1").gameObject, null);
            var btn1 = m_tran.Find("tab_bar/btn_1").GetComponent<Toggle>();
            btn1.isOn = true;
            m_tran.Find("tab_bar").GetComponent<ToggleGroup>().NotifyToggleOn(btn1);
        }
        else
        {
            OnSwitchView(null, null);
        }
        
        OnChangeQCodeAvatar(null, null);
        EventModel.Instance.TosGetShareData();
//        if (SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE_DATA))
//        {
//            SdkManager.GetShareData();
//        }
//        else
//        {
//            UIManager.ShowNoThisFunc();
//        }

        SdkManager.GetShareData("");
    }

    public override void OnHide()
    {
    }

    private void Toc_player_mall_record(toc_player_mall_record tocData)
    {
        _mallAmoutDic.Clear();
        foreach (var rec in tocData.p_record)
        {
            _mallAmoutDic[rec.mall_id] = rec.amount;
        }
    }

    private IEnumerator OnUpdateQCodeImage()
    {
        if (!IsOpen())
        {
            yield return null;
        }
        WWW loader = new WWW(EventModel.Instance.share_qcode_url);
        yield return loader;

        if (string.IsNullOrEmpty(loader.error))
        {
            qcode_img.SetSprite(Sprite.Create(loader.texture, new Rect(0, 0, loader.texture.width, loader.texture.height), Vector2.one / 2));
            qcode_img.SetNativeSize();
            loader.Dispose();
        }
    }

    public void SetQRCode(byte[] bytes)
    {
        var tex = new Texture2D(1, 1);
        tex.LoadImage(bytes);
        qcode_img.SetSprite(Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one / 2));
        qcode_img.SetNativeSize();
    }

    public void SetShareLink(string url)
    {
        sharelink_txt.text = url;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    //=========================s to c
    private void Toc_player_attribute(CDict data)
    {
        OnUpdatePlayerInfo();
    }

    private void Toc_player_buy_item(toc_player_buy_item data)
    {
        if (_current_view == shop_frame)
        {
            item_list.Data = ItemDataManager.GetMallItemsByShopType(1);
        }
    }

    private void Toc_player_cash_gain_info(toc_player_cash_gain_info data)
    {
        value_txt.text = "今日获得    " + (data.total_gain - data.before_gain);
        total_value_txt.text = "累计获得    " + data.total_gain;
    }
}

