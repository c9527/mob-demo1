﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelUpdateBulitin : BasePanel
{
    private Text m_txtBulitinContent;
    private Text m_txtTitle;
    public PanelUpdateBulitin()
    {
        SetPanelPrefabPath("UI/Event/PanelUpdateBulitin");
        AddPreLoadRes("UI/Event/PanelUpdateBulitin", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtTitle = m_tran.Find("title").GetComponent<Text>();
        m_txtBulitinContent = m_tran.Find("scroll/content").GetComponent<Text>();
    }

   
    public override void Update()
    {
    }

    public override void InitEvent()
    {
        //UGUIClickHandler.Get(m_tran.Find("btn_get_gift")).onPointerClick += OnClkGetGift;
    }

    public override void OnShow()
    {
        InitContent();
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    protected void InitContent()
    {
        var config = ConfigManager.GetConfig<ConfigAnnouncement>();
        ConfigAnnouncementLine line = config.GetLine( (int) PanelEvent.ANNOUNCE_TYPE.UPDATE_BULITIN );
        m_txtTitle.text = line.Name;

        string str = "";
        string strNew = "";
        str = line.Content;
        strNew = str.Replace("\\n", "\n");
        m_txtBulitinContent.text = strNew;
    }
}
