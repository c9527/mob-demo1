﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelLottery : BasePanel
{
    private toc_player_lottery _data;
    private Queue<string> _rank_data = new Queue<string>();
    private Queue<string> _message_data = new Queue<string>();
    private int _exchange_points;

    private int _timer;
    private int _lottery_step;//0初始阶段，1抽奖阶段，2抽奖锁定阶段，3完成阶段
    private int _lottery_type;
    private int _tab_type;
    private int[] _lottery_cost;
    private LotteryItemRender _selected_item;
    private int _next_free_timestamp;

    public Text top_info_txt;
    public ToggleGroup tab_bar;
    public DataGrid message_list;
    public Text point_txt;
    public LotteryItemRender[] item_list;
    public Transform item_img;
    public UIEffect[] effect_list;
    public Text tips_txt;
    public Button lottery_btn;
    public Button cont_lottery_btn;
    private UIEffect _award_effect;
    private UIEffect _lb_effect;
    private UIEffect _clb_effect;
    public Text award_txt;

    public PanelLottery()
    {
        SetPanelPrefabPath("UI/Event/PanelLottery");

        AddPreLoadRes("UI/Event/PanelLottery", EResType.UI);
        AddPreLoadRes("UI/Common/PanelGetItem", EResType.UI);
        AddPreLoadRes("Effect/" + EffectConst.UI_LOTTERY_CARD_COVER, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_GETITEM_GETWEAPON, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_GETITEM_ITEM, EResType.UIEffect);
    }

    public override void Init()
    {
        item_img = m_tran.Find("item_img");
        item_img.gameObject.AddComponent<SortingOrderRenderer>();
        _lottery_cost = new[] {ConfigMisc.GetInt("lottery_one_diamond"), ConfigMisc.GetInt("lottery_ten_diamond")};
        award_txt = m_tran.Find("info_frame/award_txt").GetComponent<Text>();
        top_info_txt = m_tran.Find("info_frame/info_txt").GetComponent<Text>();
        tab_bar = m_tran.Find("info_frame/tab_bar").GetComponent<ToggleGroup>();
        message_list = m_tran.Find("info_frame/message_txt/content").gameObject.AddComponent<DataGrid>();
        message_list.autoSelectFirst = false;
        message_list.SetItemRender(message_list.transform.parent.Find("Render").gameObject,typeof(LotteryMessageRender));
        point_txt = m_tran.Find("info_frame/point_txt/Text").GetComponent<Text>();
        var prototype = m_tran.Find("lottery_frame/item_list/lottery_item_0").gameObject;
        var start_pos = prototype.transform.localPosition;
        item_list = new LotteryItemRender[12];
        item_list[0] = prototype.AddComponent<LotteryItemRender>();
        for (int i = 1; i < item_list.Length; i++)
        {
            item_list[i] = (UIHelper.Instantiate(prototype) as GameObject).GetComponent<LotteryItemRender>();
            item_list[i].name = "lottery_item_" + i;
            item_list[i].transform.SetLocation(prototype.transform.parent,start_pos.x + i % 4 * 150,start_pos.y +(int)(i / 4) * -125);
            item_list[i].transform.localScale = Vector3.one;
        }
        effect_list = new UIEffect[item_list.Length];
        lottery_btn = m_tran.Find("lottery_frame/lottery_btn").GetComponent<Button>();
        lottery_btn.transform.Find("cost_txt").GetComponent<Text>().text = _lottery_cost[0].ToString();
        cont_lottery_btn = m_tran.Find("lottery_frame/cont_lottery_btn").GetComponent<Button>();
        cont_lottery_btn.transform.Find("cost_txt").GetComponent<Text>().text = _lottery_cost[1].ToString();
        tips_txt = m_tran.Find("lottery_frame/tips_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        var close_btn = m_tran.Find("close_btn");
        if (close_btn != null)
        {
            UGUIClickHandler.Get(close_btn).onPointerClick += delegate { HidePanel(); UIManager.ShowPanel<PanelHallBattle>();};
        }
        UGUIClickHandler.Get(tab_bar.transform.Find("btn_0")).onPointerClick += OnTabBtnClick;
        UGUIClickHandler.Get(tab_bar.transform.Find("btn_1")).onPointerClick += OnTabBtnClick;
        UGUIClickHandler.Get(m_tran.Find("info_frame/exchange_btn")).onPointerClick += (target, evt) => { UIManager.PopPanel<PanelExchangePoints>(new object[]{ _exchange_points },true,this); };
        UGUIClickHandler.Get(lottery_btn.gameObject).onPointerClick += OnLotteryBtnClick;
        UGUIClickHandler.Get(cont_lottery_btn.gameObject).onPointerClick += OnLotteryBtnClick;
        foreach (var render in item_list)
        {
            UGUIClickHandler.Get(render.gameObject).onPointerClick += OnLotteryItemClick;
        }
    }

    private void OnLotteryItemClick(GameObject target, PointerEventData eventData)
    {
        if (_lottery_step != 1 || _lottery_type==0)
        {
            return;
        }
        _lottery_step = 2;
        _selected_item = target.GetComponent<LotteryItemRender>();
        EventModel.Instance.TosLottery(_lottery_type);
    }

    private void OnLotteryBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_lottery_step != 0 || target.GetComponent<Button>().interactable==false)
        {
            return;
        }
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        _lottery_type = target!=null && target.name == "cont_lottery_btn" ? 2 : 1;
        if ((_lottery_type == 1 && _data != null && _data.free_times > 0) || PlayerSystem.CheckMoney(_lottery_cost[_lottery_type - 1], EnumMoneyType.DIAMOND, null) == true)
        {
            _lottery_step = 1;
            //设置准备抽奖UI
            lottery_btn.interactable = false;
            cont_lottery_btn.interactable = false;
            Util.SetGrayShader(lottery_btn);
            Util.SetGrayShader(cont_lottery_btn);
            if (_lb_effect != null)
            {
                _lb_effect.Destroy();
                _lb_effect = null;
            }
            if (_clb_effect != null)
            {
                _clb_effect.Destroy();
                _clb_effect = null;
            }
            foreach (var render in item_list)
            {
                render.TurnItem(false,true);
            }
        }
    }

    private void OnTabBtnClick(GameObject target=null, PointerEventData eventData=null)
    {
        var old_type = _tab_type;
        _tab_type = target != null && target.name == "btn_1" ? 1 : 0;
        if (old_type != _tab_type)
        {
            OnUpdateMessageView();
        }
    }

    public override void OnShow()
    {
        _lottery_step = 0;
        _tab_type = 0;
        _next_free_timestamp = 0;
        tab_bar.SetAllTogglesOff();
        tab_bar.transform.Find("btn_0").GetComponent<Toggle>().isOn = true;
        //OnUpdateMessageView();
        //OnUpdateLotteryView();
        EventModel.Instance.TosLottery();
        EventModel.Instance.TosGetLotteryRank();
        _timer = TimerManager.SetTimeOut(60f, EventModel.Instance.TosGetLotteryRank);
        if (_award_effect == null)
        {
            _award_effect = UIEffect.ShowEffect(award_txt.transform, EffectConst.UI_LOTTERY_AWARD,new Vector2(-5,5));
        }
        if (_lb_effect != null)
        {
            _lb_effect.Destroy();
            _lb_effect = null;
        }
        if (_clb_effect != null)
        {
            _clb_effect.Destroy();
            _clb_effect = null;
        }

        for (int i = 0; i < item_list.Length; i++)
        {
            if (!(i == 0 || i == 2 || i == 5 || i == 7 || i == 8 || i == 10))
                continue;
            if (effect_list[i] == null)
                effect_list[i] = UIEffect.ShowEffectBefore(item_list[i].transform, EffectConst.UI_LOTTERY_LITTLE_STAR, item_list[i].transform.localPosition);
        }
    }

    public override void OnHide()
    {
        _lottery_step = 0;
        _tab_type = 0;
        _rank_data.Clear();
        //_message_data.Clear();
        if (_timer >0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        foreach (var render in item_list)
        {
            render.TurnItem(false, false, false);
        }
        if (_award_effect != null)
        {
            _award_effect.Destroy();
            _award_effect = null;
        }
        for (int i = 0; i < effect_list.Length; i++)
        {
            if (effect_list[i] != null)
                effect_list[i].Destroy();
            effect_list[i] = null;
        }
    }

    private void OnUpdateLotteryResult()
    {
        _lottery_step = 3;
        var lottery_config = ConfigManager.GetConfig<ConfigLottery>();
        var tdata = lottery_config.GetLine(_data.reward_id_list[0]);
        var temp = new List<CDItemData>();
        foreach (var code in _data.reward_id_list)
        {
            temp.Add(lottery_config.GetLine(code).GetMainItem());
        }
        //送枪魂特殊处理
        var sp_reward = ConfigManager.GetConfig<ConfigMisc>().GetLine("lottery_gun_soul");
        if(sp_reward!=null && sp_reward.ValueInt>0)
        {
            var vo = new CDItemData(sp_reward.ValueInt);
            vo.sdata.item_cnt = _lottery_type==2 ? 10 : 1;
            temp.Add(vo);
        }
        if (_selected_item != null)
        {
            
            _selected_item.SetData(tdata);
            _selected_item.gameObject.TrySetActive(false);
            UIEffect.ShowEffect(m_tran, tdata.IsRarity == 1 ? EffectConst.UI_LOTTERY_SP_HIT : EffectConst.UI_LOTTERY_HIT,1.5f);
            AudioManager.PlayUISound(AudioConst.lotteryTurnCard);
        } 
        item_img.gameObject.TrySetActive(true);
        var avatar = item_img.Find("Image").GetComponent<Image>();
        avatar.SetSprite(ResourceManager.LoadIcon(tdata.GetMainItem().info.Icon),tdata.GetMainItem().info.SubType);
        avatar.SetNativeSize();
        avatar.transform.localScale = Vector3.one * 1.5f;
        avatar.gameObject.TrySetActive(false);
        _timer = TimerManager.SetTimeOut(1f, () =>
        {
            avatar.gameObject.TrySetActive(true);
            avatar.transform.localScale = Vector3.one * 3f;
            TweenScale.Begin(avatar.gameObject, 0.25f, Vector3.one * 1.5f).method = UITweener.Method.BounceIn;
            _timer = TimerManager.SetTimeOut(0.5f,()=>
            {
                _lottery_step = 0;
                item_img.gameObject.TrySetActive(false);
                OnUpdateInfoView();
                Action hide_action = ()=>OnUpdateLotteryView(_selected_item);
                Action fun_action = () => OnLotteryBtnClick(_lottery_type == 2 ? cont_lottery_btn.gameObject : lottery_btn.gameObject, null);
                UIManager.PopPanel<PanelGetItem>(new object[] { temp.ToArray(), hide_action, fun_action, _lottery_type, ItemDataManager.item_added_temp_record.value.ToArray(),_data.free_times }, true, this);
                EventModel.Instance.TosGetLotteryRank();
                _timer = TimerManager.SetTimeOut(60f, EventModel.Instance.TosGetLotteryRank);
            });
        });
    }

    private void OnUpdateLotteryView(LotteryItemRender target=null)
    {
        if (_data == null || _lottery_step==1 || _lottery_step==2)
        {
            return;
        }
        item_img.gameObject.TrySetActive(false);
        var filter_map = new Dictionary<int,bool>();
        if (target != null && _data.reward_id_list!=null && _data.reward_id_list.Length>0)
        {
            foreach (var filter_code in _data.reward_id_list)
            {
                filter_map[filter_code] = true;
            }
        }
        var source_data = new List<TDLottery>();
        var lottery_config = ConfigManager.GetConfig<ConfigLottery>();
        if (lottery_config.m_dataArr != null)
        {
            var now_stamp = TimeUtil.GetNowTimeStamp();
            for (int i = 0; i < lottery_config.m_dataArr.Length; i++)
            {
                var do_filter = false;
                var tdata = lottery_config.m_dataArr[i];
                filter_map.TryGetValue(tdata.ID, out do_filter);
                do_filter = do_filter || (target == null && tdata.IsRarity == 0);
                if (do_filter == false && tdata.InTime(now_stamp))
                {
                    source_data.Insert(UnityEngine.Random.Range(0, source_data.Count + 1), tdata);
                }
            }
        }
        for (int j = 0; j < item_list.Length; j++)
        {
            var render = item_list[j];
            if (render.gameObject.activeSelf == false)
            {
                render.gameObject.TrySetActive(true);
            }
            if (target == null || render != target)
            {
                render.SetData(source_data.Count>0 && j<source_data.Count ? source_data[j] : null);
                render.TurnItem(true);
            }
            else if (target == render)
            {
                render.TurnItem(true, false, true);
            }
        }
        if (_lottery_step == 0 && lottery_btn.interactable == false)
        {
            lottery_btn.interactable = true;
            cont_lottery_btn.interactable = true;
            Util.SetNormalShader(lottery_btn);
            Util.SetNormalShader(cont_lottery_btn);
        }
        //if (lottery_btn.interactable && _lb_effect == null)
        //{
        //    _lb_effect = UIEffect.ShowEffect(lottery_btn.transform, EffectConst.UI_LOTTERY_BUTTON,new Vector2(-7.5f,3.5f));
        //}
        //if (cont_lottery_btn.interactable && _clb_effect == null)
        //{
        //    _clb_effect = UIEffect.ShowEffect(cont_lottery_btn.transform, EffectConst.UI_LOTTERY_BUTTON, new Vector2(-7.5f, 3.5f));
        //}
    }

    private void OnUpdateInfoView()
    {
        if (_data == null)
        {
            return;
        }
        if (_data.top_player!=null && _data.top_player.uid != 0)
        {
            top_info_txt.text = string.Format("{0}\n共计{1}钻石", _data.top_player.name, _data.top_player.award_amount);
        }
        else
        {
            top_info_txt.text = "";
        }
        award_txt.text = _data.award_amount.ToString();
        lottery_btn.transform.Find("Text").GetComponent<Text>().text = _data.free_times > 0 ? "免费一次" : "购买一次";
        lottery_btn.transform.Find("cost_txt").GetComponent<Text>().text = _data.free_times > 0 ? "免费" : _lottery_cost[0].ToString();
    }

    private void OnUpdateMessageView(int type=-1)
    {
        if (type!=-1 && type != _tab_type)
        {
            return;
        }
        var list_data = _tab_type == 1 ? _message_data : _rank_data;
        var source_data = list_data.ToArray();
        Array.Reverse(source_data);
        message_list.Data = list_data.Count > 0 ? source_data : new object[0];
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_next_free_timestamp <= 0 || TimeUtil.GetNowTimeStamp() > (_next_free_timestamp + 3))
        {
            if (_next_free_timestamp > 0)
            {
                _next_free_timestamp = 0;
                Logger.Log("===================next_free_timestamp timeout：EventModel.Instance.TosLottery()");
                EventModel.Instance.TosLottery();
            }
            tips_txt.text = "每次购买必得一个枪魂";
            return;
        }
        var cd = _next_free_timestamp - TimeUtil.GetNowTimeStamp();
        tips_txt.text = string.Format("{0}后免费购买1次，每次购买必得一个枪魂", TimeUtil.FormatTime(Math.Max(cd,0), 2)); 
    }

    //===================================s to c
    private void Toc_player_exchange_points(toc_player_exchange_points data)
    {
        _exchange_points = data.points;
        point_txt.text = "兑换券：" + _exchange_points;
    }

    private void Toc_player_lottery(toc_player_lottery data)
    {
        _data = data;
        Logger.Log("===================free times：" + _data.free_times + " free cd：" + _data.free_cd);
        _next_free_timestamp = 0;
        if (_data.free_cd > 0)
        {
            _next_free_timestamp = TimeUtil.GetNowTimeStamp() + _data.free_cd;
        }
        if (data.reward_id_list != null && data.reward_id_list.Length > 0)
        {
            OnUpdateLotteryResult();
        }
        else
        {
            OnUpdateInfoView();
            OnUpdateLotteryView();
        }
    }

    private void Toc_player_get_lottery_rank(toc_player_get_lottery_rank data)
    {
        _rank_data.Clear();
        for(var i=0;i<data.player_list.Length;i++)
        {
            var info = ConfigManager.GetConfig<ConfigLottery>().GetLine(data.player_list[i].reward_id);
            _rank_data.Enqueue(string.Format("<size='19'><color='#ffc970'>{0}</color></size>\n{1}",data.player_list[i].name,(info!=null ? info.ItemName : data.player_list[i].reward_id.ToString())));
        }
        OnUpdateMessageView(0);
    }

    private void Toc_player_notice(toc_player_notice data)
    {
        if (data.msg.IndexOf("type")== 0)
        {
            //type:1, name:精彩的苹果, item_id:9
            var msg = data.msg.Split(',');
            var reward_id = int.Parse(msg[2].Split(':')[1]);
            var player_name = msg[1].Split(':')[1];
            var info = ConfigManager.GetConfig<ConfigLottery>().GetLine(reward_id);
            _message_data.Enqueue(string.Format("<size='19'><color='#ffc970'>{0}</color></size>\n{1}", player_name, (info != null ? info.ItemName : reward_id.ToString())));
            if (_message_data.Count > 50)
            {
                _message_data.Dequeue();
            }
            OnUpdateMessageView(1);
        }
    }
}
