﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LotteryItemRender : ItemRender
{
    private TDLottery _data;

    private int _timer;
    private const float EASE_TIME = 1f;
    //private int _ease_step;
    private float _tween_start_time;

    public GameObject back_layer;
    public GameObject face_layer;
    public Image item_img;
    public Text name_txt;
    UIEffect _hit_effect;

    public override void Awake()
    {
        back_layer = transform.Find("back").gameObject;
        face_layer = transform.Find("face").gameObject;
        item_img = transform.Find("face/item_img").GetComponent<Image>();
        name_txt = transform.Find("face/name_txt").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _tween_start_time = 0;
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        _data = data as TDLottery;
        if (_data == null)
        {
            return;
        }
        item_img.SetSprite(ResourceManager.LoadIcon(_data.GetMainIcon()), _data.GetMainItem().info.SubType);
        if (_data.GetMainItem() != null && _data.GetMainItem().info is ConfigItemWeaponLine)
        {
            item_img.transform.localEulerAngles = new Vector3(0,0,-15);
        }
        else
        {
            item_img.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        item_img.SetNativeSize();
        name_txt.text = _data.ItemName;
    }

    //private void Update()
    //{
    //    if (_tween_start_time == 0 || (Time.time - _tween_start_time) >= EASE_TIME)
    //    {
    //        _tween_start_time = 0;
    //        return;
    //    }
    //    var dt = Time.time - _tween_start_time;
    //    var target_layer = back_layer.activeSelf ? back_layer : face_layer;
    //    if (_ease_step == 0 && dt >= EASE_TIME / 2)
    //    {
    //        Logger.Log(target_layer.name + " rotate complete");
    //        _ease_step = 1;
    //        target_layer.gameObject.TrySetActive(false);
    //        target_layer = target_layer == back_layer ? face_layer : back_layer;
    //        target_layer.gameObject.TrySetActive(true);
    //        Logger.Log(target_layer.name + " rotate start");
    //    }
    //    var ry = EaseMethod.UpdateByEase(dt, EASE_TIME, -180, 0);
    //    target_layer.transform.eulerAngles = new Vector3(0,_ease_step==1 ? 180 + ry : ry,0);
    //}

    public void TurnItem(bool open=false,bool tween=false,bool play_effect=false)
    {
        _tween_start_time = 0;
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        if (_hit_effect != null)
        {
            _hit_effect.Destroy();
            _hit_effect = null;
        }
        if (tween == true)
        {
            if (open)
            {
                UIEffect.ShowEffect(transform, _data != null && _data.IsRarity == 1 ? EffectConst.UI_LOTTERY_SP_HIT : EffectConst.UI_LOTTERY_HIT, 0.95f, new Vector3(0,0,-100));
            }
            else
            {
                UIEffect.ShowEffect(transform, EffectConst.UI_LOTTERY_CARD_COVER, 1f, new Vector3(0, 0, -100));
            }
            //_ease_step = 0;
            _tween_start_time = Time.realtimeSinceStartup;
            //back_layer.transform.eulerAngles = Vector3.zero;
            //face_layer.transform.eulerAngles = Vector3.zero;
            back_layer.TrySetActive(false);
            face_layer.TrySetActive(false);
            _timer = TimerManager.SetTimeOut(EASE_TIME, () => SetupItemViewStatus(open, play_effect));
        }
        else
        {
            SetupItemViewStatus(open, play_effect);
        }
    }

    private void SetupItemViewStatus(bool open=false, bool play_effect=false)
    {
        _tween_start_time = 0;
        //back_layer.transform.eulerAngles = Vector3.zero;
        //face_layer.transform.eulerAngles = Vector3.zero;
        back_layer.TrySetActive(!open);
        face_layer.TrySetActive(open);
        if (play_effect == true && _hit_effect == null)
        {
            _hit_effect = UIEffect.ShowEffect(transform, EffectConst.UI_LOTTERY_HIGHLIGHT);
        }
        else if (play_effect == false && _hit_effect != null)
        {
            _hit_effect.Destroy();
            _hit_effect = null;
        }
    }

    public bool IsOpen()
    {
        return face_layer.activeSelf;
    }
}
