﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SevenHappyDayData
{
    public int id;
    public int welId;
    public int day;
}

public class Panel7HappyDay : BasePanel
{
    private ItemEventTitle m_eventTitle;
    private int m_activityId;
    private DataGrid m_childDataGrid;
    private List<SevenHappyDayData> m_lst7HappyDay;

    public Text desc_txt;    

    public Panel7HappyDay()
    {
        SetPanelPrefabPath("UI/Event/Panel7HappyDay");
        AddPreLoadRes("UI/Event/Panel7HappyDay", EResType.UI);
        AddPreLoadRes("UI/Event/Item7HappyDay", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_childDataGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_childDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Event/Item7HappyDay"), typeof(Item7HappyDay));
        m_lst7HappyDay = new List<SevenHappyDayData>();
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        AddEventListener(GameEvent.UI_EVENT_DATA_CHANGE, ResetChild);
    }

    private void ResetChild()
    {
        m_childDataGrid.Data = m_lst7HappyDay.ToArray();
    }
    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_eventTitle = m_params[0] as ItemEventTitle;
        m_activityId = (int)m_params[1];

        desc_txt.text = "";
        m_lst7HappyDay.Clear();
        var config = ConfigManager.GetConfig<ConfigEvent>();
        for( int i = 0; i < 3;++i )
        {
            ConfigEventLine line = config.GetLine(m_activityId + i);
            if (line == null || line.Welfare == null || line.Welfare.Length == 0)
                continue;
            if (line.Id == m_activityId && line.Params!=null && line.Params.Length>0)
            {
                desc_txt.text = line.Params[0];
            }
            SevenHappyDayData data = new SevenHappyDayData();
            data.id = line.Id;
            data.welId = line.Welfare[0];
            data.day = i + 1;
            m_lst7HappyDay.Add(data);
        }

        m_childDataGrid.Data = m_lst7HappyDay.ToArray();

        DateTime dtNow = TimeUtil.GetNowTime();
        int day = dtNow.Day;
        int month = dtNow.Month;
        if( month == 12 )
        {
            int idx = (day - 1) % 3;
            m_childDataGrid.ResetScrollPosition(idx);
        }
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

}
