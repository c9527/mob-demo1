﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class LotteryMessageRender : ItemRender
{
    public Text content_txt;

    public override void Awake()
    {
        content_txt = transform.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        content_txt.text = data != null ? data.ToString() : "???\n???";
    }
}
