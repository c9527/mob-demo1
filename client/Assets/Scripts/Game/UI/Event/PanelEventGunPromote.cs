﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelEventGunPromote : BasePanel
{
    public Image bg_img;
    public Image icon_img;
    public Text desc_txt;
    public Transform action_btn;

    public PanelEventGunPromote()
    {
        SetPanelPrefabPath("UI/Event/PanelEventGunPromote");

        AddPreLoadRes("Atlas/EventDynamic", EResType.Atlas);
        AddPreLoadRes("UI/Event/PanelEventGunPromote", EResType.UI);
    }

    public override void Init()
    {
        bg_img = transform.Find("frame/Image").GetComponent<Image>();
        icon_img = transform.Find("frame/Icon").GetComponent<Image>();
        desc_txt = transform.Find("frame/desc_txt").GetComponent<Text>();
        action_btn = transform.Find("frame/btn_go");
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(action_btn.gameObject).onPointerClick += OnActionBtnClick;
    }

    private void OnActionBtnClick(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if (parent != null && parent.IsOpen())
        {
            parent.HidePanel();
        }
        UIManager.ShowPanel<PanelMall>();
    }

    public override void OnShow()
    {
        var info = HasParams() ? ConfigManager.GetConfig<ConfigEvent>().GetLine(m_params[0]) : null;
        if (info == null)
        {
            return;
        }
        var data = info.Params != null && info.Params.Length > 0 ? info.Params : null;
        if (data != null && data.Length > 0)
        {
            desc_txt.text = data[0];
        }
        else
        {
            desc_txt.text = "";
        }
        if (data != null && data.Length > 1)
        {
            bg_img.SetSprite(ResourceManager.LoadSprite(AtlasName.EventDynamic, data[1]));
            bg_img.SetNativeSize();
        }
        if (data != null && data.Length > 2)
        {
            icon_img.gameObject.TrySetActive(true);
            icon_img.SetSprite(ResourceManager.LoadSprite(AtlasName.EventDynamic, data[2]));
            icon_img.SetNativeSize();
        }
        else
        {
            icon_img.gameObject.TrySetActive(false);
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
