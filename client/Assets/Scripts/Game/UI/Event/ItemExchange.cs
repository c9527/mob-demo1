﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemExchange : ItemRender
{
    private ExchangeData m_data;
    private GameObject m_goLmt;
    private GameObject m_goOutOfTime;
    private GameObject m_goGet;
    private GameObject m_goGetted;
    private GameObject m_goHasForever;
    
    //private Image m_imgItem;
    //private Text m_txtItem;
    private Text m_txtCondition;
    //private string m_strItemName;
    private Image m_imgExchangeIcon;
    //private Transform m_itemsTran;
    private GameObject m_itemGo;
    public override void Awake()
    {
        m_goLmt = transform.Find("limit").gameObject;
        m_goOutOfTime = transform.Find("out_of_print").gameObject;
        m_goGet = transform.Find("btn_get").gameObject;
        m_goGetted = transform.Find("btn_getted").gameObject;
        m_goHasForever = transform.Find("btn_has_forever").gameObject;
        //m_imgItem = transform.Find("item_img").GetComponent<Image>();
        //m_txtItem = transform.Find("item_num").GetComponent<Text>();
        m_txtCondition = transform.Find("condition").GetComponent<Text>();
        m_imgExchangeIcon = transform.Find("exchange_icon").GetComponent<Image>();
        //m_itemsTran = transform.Find("exchange_items").transform;
        m_itemGo = transform.Find("item").gameObject;
        UGUIClickHandler.Get(m_goGet).onPointerClick += OnClickGet;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ExchangeData;

        var cfgExchange = ConfigManager.GetConfig<ConfigExchange>();
        ConfigExchangeLine exchangeLine = cfgExchange.GetLine(m_data.Id);
        if (exchangeLine == null)
            return;

        if( exchangeLine.Times == 0 )
            m_goLmt.TrySetActive(false);
        else
            m_goLmt.TrySetActive(true);

        if (exchangeLine.Times == 0)
            m_goLmt.TrySetActive(false);
        else
            m_goLmt.TrySetActive(true);

        
        m_imgExchangeIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, exchangeLine.ExchangeIcon));
        m_txtCondition.text = exchangeLine.Condition.ToString();
        if (exchangeLine.Item.Length == 0)
            return;

        Text itemNameTxt = m_itemGo.transform.FindChild("item_num").GetComponent<Text>();
        Image itemImg = m_itemGo.transform.FindChild("item_img").GetComponent<Image>();
        ItemInfo itemInfo = exchangeLine.Item[0];
        ConfigItemLine itemLine = ItemDataManager.GetItem(itemInfo.ID);
        itemImg.SetSprite(ResourceManager.LoadIcon(itemLine.Icon),itemLine.SubType);
        itemImg.SetNativeSize();

        string itemName = itemLine.DispName + " ";                      

        if (itemInfo.cnt > 1)
        {
            itemName += "X" + itemInfo.cnt + " ";
        }

        if (itemInfo.day == 0)
        {
            itemName += " (永久)";
        }
        else if (itemInfo.day > 0)
        {
            ItemTime itemTime = ItemDataManager.FloatToDayHourMinute(itemInfo.day);         
            itemName += itemTime.ToString();
        }
        itemNameTxt.text = itemName;

        var pitem = ItemDataManager.GetDepotItem(itemInfo.ID);

        if (pitem != null && pitem.time_limit == 0)
        {
            m_data.State = Exchange_State.ES_HAS_FOREVER;
        }

        if (m_data.State == Exchange_State.ES_HAS_FOREVER)
        {
            m_goHasForever.TrySetActive(true);
            m_goGet.TrySetActive(false);
            m_goGetted.TrySetActive(false);
        }
        else if (m_data.State == Exchange_State.ES_EXCHANGE)
        {
            m_goHasForever.TrySetActive(false);
            m_goGet.TrySetActive(true);
            m_goGetted.TrySetActive(false);
        }
        else
        {
            m_goHasForever.TrySetActive(false);
            m_goGet.TrySetActive(false);
            m_goGetted.TrySetActive(true);
        }

        /*
        for( int i = 0; i < exchangeLine.Item.Length; ++i )
        {
            ItemInfo itemInfo = exchangeLine.Item[i];
            ConfigItemLine itemLine = ItemDataManager.GetItem(itemInfo.ID);
            GameObject itemGo = GameObject.Instantiate(m_itemProtoGo) as GameObject;
            itemGo.transform.SetParent(m_itemsTran, false);
            itemGo.TrySetActive(true);
            Text itemNameTxt = itemGo.transform.FindChild("item_num").GetComponent<Text>();
            Image itemImg = itemGo.transform.FindChild("item_img").GetComponent<Image>();
            itemImg.SetSprite(ResourceManager.LoadIcon(itemLine.Icon));
            itemImg.SetNativeSize();

            m_strItemName = itemLine.DispName + " ";
            if (itemInfo.cnt > 1)
            {
                m_strItemName += "X" + itemInfo.cnt + " ";
            }

            if (itemInfo.day == 0)
            {
                m_strItemName += " (永久)";
            }
            else if (itemInfo.day > 0)
            {
                m_strItemName += string.Format(" ({0}天)", itemInfo.day);
            }
            
            itemNameTxt.text = m_strItemName;      
        }
        */
        /*
        ItemInfo itemInfo = m_welLine.Item[0];
        ConfigItemLine itemLine = ItemDataManager.GetItem(itemInfo.ID);
        m_imgItem.SetSprite(ResourceManager.LoadIcon(itemLine.Icon));
        m_imgItem.SetNativeSize();
        m_strItemName = itemLine.DispName + " ";
        if (itemInfo.cnt > 1)
        {
            m_strItemName += "X" + itemInfo.cnt + " ";
        }

        if (itemInfo.day == 0)
        {
            m_strItemName += " (永久)";
        }
        else if (itemInfo.day > 0)
        {
            m_strItemName += string.Format(" ({0}天)", itemInfo.day);
        }

        m_txtItem.text = m_strItemName;
        m_txtStar.text = m_welLine.Condition[1].ToString();
        */
        
    }

    private void OnClickGet(GameObject target, PointerEventData eventData)
    {
        var cfgExchange = ConfigManager.GetConfig<ConfigExchange>();
        ConfigExchangeLine exchangeLine = cfgExchange.GetLine(m_data.Id);
        if (exchangeLine == null)
            return;

        NetLayer.Send(new tos_player_exchange() { type = exchangeLine.Type, id = exchangeLine.Id});

        /*
        string strDesc = string.Format("确定花{0}红星兑换: {1}?", m_welLine.Condition[1], m_strItemName);
        UIManager.ShowTipPanel(strDesc, () =>
        {
            NetLayer.Send(new tos_player_get_event() { id = m_data.welId });
            var config = ConfigManager.GetConfig<ConfigEventWelfare>();
            ConfigEventWelfareLine line = config.GetLine(m_data.welId);
            if (line != null)
            {
                var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] { true }, true, null);
                panelItemTip.SetRewardItemList(line.Item);
            }
        }, null, true, true, "确定", "取消");
         */
    }
        

}
