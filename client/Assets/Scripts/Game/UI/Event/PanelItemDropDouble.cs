﻿using UnityEngine;
using System.Collections;

public class PanelItemDropDouble : BasePanel
{
    public PanelItemDropDouble()
    {
        SetPanelPrefabPath("UI/Event/PanelItemDropDouble");
        AddPreLoadRes("UI/Event/PanelItemDropDouble", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        //UGUIClickHandler.Get(m_tran.Find("btn_get_gift")).onPointerClick += OnClkGetGift;
    }

    public override void OnShow()
    {
       
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }
}
