﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemRedstar : ItemRender
{
    private RedstarData m_data;
    private GameObject m_goLmt;
    private GameObject m_goOutOfTime;
    private GameObject m_goBtn;
    private Text m_txtBtn;
    private Image m_imgItem;
    private Text m_txtItem;
    private Text m_txtStar;
    ConfigEventWelfareLine m_welLine;
    private string m_strItemName;
    public override void Awake()
    {
        m_goLmt = transform.Find("limit").gameObject;
        m_goOutOfTime = transform.Find("out_of_print").gameObject;
        m_goBtn = transform.Find("btn_get").gameObject;
        m_txtBtn = transform.Find("btn_get/Label").GetComponent<Text>();
        m_imgItem = transform.Find("item_img").GetComponent<Image>();
        m_txtItem = transform.Find("item_num").GetComponent<Text>();
        m_txtStar = transform.Find("star_num").GetComponent<Text>();
        UGUIClickHandler.Get(m_goBtn).onPointerClick += OnClickGet;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as RedstarData;
        var config = ConfigManager.GetConfig<ConfigEventWelfare>();
        m_welLine = config.GetLine(m_data.welId);
        if (m_welLine.Times == 0)
            m_goLmt.TrySetActive(false);
        else
            m_goLmt.TrySetActive(true);

        if (m_welLine.Sign == 0)
            m_goOutOfTime.TrySetActive(false);
        else
            m_goOutOfTime.TrySetActive(true);

        ItemInfo itemInfo = m_welLine.Item[0];
        ConfigItemLine itemLine = ItemDataManager.GetItem(itemInfo.ID);
        m_imgItem.SetSprite(ResourceManager.LoadIcon(itemLine.Icon),itemLine.SubType);
        m_imgItem.SetNativeSize();
        m_strItemName = itemLine.DispName + " ";
        if (itemInfo.cnt > 1)
        {
            m_strItemName += "X" + itemInfo.cnt + " ";
        }
        
        if(itemInfo.day == 0)
        {
            m_strItemName += " (永久)";
        }
        else if(itemInfo.day > 0)
        {
            m_strItemName += string.Format(" ({0}天)", itemInfo.day);
        }

        m_txtItem.text = m_strItemName;
        m_txtStar.text = m_welLine.Condition[1].ToString();

        int status = EventManager.GetWelfareStatus(m_data.welId);
        if( status == (int)EventManager.Event_Status.S_RECEIVED )
        {
            m_txtBtn.text = "已兑换";
            Util.SetGrayShader(m_goBtn.GetComponent<Image>());
        }
        else
        {
            m_txtBtn.text = "兑换";
            Util.SetNormalShader(m_goBtn.GetComponent<Image>());
        }
    }

     private void OnClickGet(GameObject target, PointerEventData eventData)
    {
        int status = EventManager.GetWelfareStatus(m_data.welId);
        if (status == (int)EventManager.Event_Status.S_RECEIVED)
            return;

         if( status == (int)EventManager.Event_Status.S_UN_GET )
         {
             TipsManager.Instance.showTips("红星不足，或不在活动时间，无法兑换");
             return;
         }

         string strDesc = string.Format("确定花{0}红星兑换: {1}?", m_welLine.Condition[1], m_strItemName);
         UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_player_get_event() { id = m_data.welId });
                                                 var config = ConfigManager.GetConfig<ConfigEventWelfare>();
                                                 ConfigEventWelfareLine line = config.GetLine(m_data.welId);
                                                 if (line != null)
                                                 {
                                                    //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] { true }, true, null);
                                                    //panelItemTip.SetRewardItemList(line.Item);
                                                     PanelRewardItemTipsWithEffect.DoShow(line.Item);
                                                 }
                                                }, null, true, true, "确定", "取消");  

    }
}
