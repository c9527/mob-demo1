﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PanelSendGunInTime : BasePanel 
{
    private ItemEventTitle m_eventTitle;
    private int m_id;
    private GameObject m_goGet;
    private Text m_txtGet;
    private int m_iStatus;
    private int m_iWelId;
    public PanelSendGunInTime()
    {
        SetPanelPrefabPath("UI/Event/PanelSendGunInTime");
        AddPreLoadRes("UI/Event/PanelSendGunInTime", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
       m_goGet = m_tran.Find("btn_get").gameObject;
       m_txtGet = m_tran.Find("btn_get/Label").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_goGet).onPointerClick += OnClkGet;
        AddEventListener(GameEvent.UI_EVENT_DATA_CHANGE, ResetWelfare);
    }

    private void OnClkGet(GameObject target, PointerEventData eventData)
    {
        if (m_iStatus != (int)EventManager.Event_Status.S_ALLOW_RECEIVE)
            return;

        tos_player_get_event msg = new tos_player_get_event();
        msg.id = m_iWelId;
        NetLayer.Send(msg);
        EventManager.SetWelfareStatus(m_iWelId, (int)EventManager.Event_Status.S_RECEIVED);

        if (m_eventTitle != null)
            m_eventTitle.ResetBubble();

        
        var config = ConfigManager.GetConfig<ConfigEventWelfare>();
        ConfigEventWelfareLine line = config.GetLine(m_iWelId);
        if (line != null)
        {
            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] {true}, true, null);
            //panelItemTip.SetRewardItemList(line.Item);
            PanelRewardItemTipsWithEffect.DoShow(line.Item);
        }
        
    }
    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_eventTitle = m_params[0] as ItemEventTitle;
        m_id = (int)m_params[1];

        var config = ConfigManager.GetConfig<ConfigEvent>();
        ConfigEventLine line = config.GetLine(m_id);
        if (line == null || line.Welfare == null || line.Welfare.Length == 0)
            return;

        m_iWelId = line.Welfare[0];
        ResetWelfare();
    }

    private void ResetWelfare()
    {
        m_iStatus = EventManager.GetWelfareStatus(m_iWelId);
        if (m_iStatus == (int)EventManager.Event_Status.S_UN_GET)
        {
            m_txtGet.text = "未达成";
            Util.SetNormalShader(m_goGet.GetComponent<Image>());
        }
        else if (m_iStatus == (int)EventManager.Event_Status.S_ALLOW_RECEIVE)
        {
            m_txtGet.text = "领取";
            Util.SetNormalShader(m_goGet.GetComponent<Image>());
        }
        else if (m_iStatus == (int)EventManager.Event_Status.S_RECEIVED)
        {
            m_txtGet.text = "已领取";
            Util.SetGrayShader(m_goGet.GetComponent<Image>());
        }

    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }
}
