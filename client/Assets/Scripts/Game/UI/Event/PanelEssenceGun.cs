﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelEssenceGun : BasePanel
{
    public Text desc_txt;

    public PanelEssenceGun()
    {
        SetPanelPrefabPath("UI/Event/PanelEssenceGun");
        AddPreLoadRes("UI/Event/PanelEssenceGun", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btn_go").gameObject).onPointerClick += OnClkGo;
    }

    public override void OnShow()
    {
       var info = HasParams() && m_params.Length>1 ? ConfigManager.GetConfig<ConfigEvent>().GetLine(m_params[1]) : null;
       if (info == null)
       {
           return;
       }
       if (info.Params != null && info.Params.Length > 0)
       {
           desc_txt.text = info.Params[0];
       }
       else
       {
           desc_txt.text = "";
       }
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkGo(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if( parent != null && parent.IsOpen())
        {
            //parent.SwitchToView(PanelEvent.VIEW_7_HAPPY_DAY);
            parent.HidePanel();
        }
        UIManager.ShowPanel<PanelMall>();//中国城礼包活动
    }
}
