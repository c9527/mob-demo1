﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System.IO;
using System.Threading;
using System.Collections;

class PanelLottoryShare : BasePanel
{
    Image m_logo;
    Image m_weaponImage;
    Image m_erweima;

    Text m_weaponName;    

    Button m_captureShot;
    Button m_shareToFriend;
    Button m_shareToFriendLine;
    Button m_back;

    static toc_fight_game_result share_data = PanelSettle.game_result;

    private Vector3 captureLeft;
    private Vector3 captureRight;
    protected static int timeOutUid;

    public PanelLottoryShare()
    {
        m_pixelPrefect = false;

        SetPanelPrefabPath("UI/Event/PanelLottoryShare");
        AddPreLoadRes("UI/Event/PanelLottoryShare", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Login", EResType.Atlas);
        AddPreLoadRes("Atlas/WeixinShare", EResType.Atlas);
    }

    public override void Init()
    {
        m_logo = m_tran.Find("logo").GetComponent<Image>();
        m_erweima = m_tran.Find("erweima").GetComponent<Image>();
        m_weaponImage = m_tran.Find("weapon_bg/weapon_img").GetComponent<Image>();
        
        m_weaponName = m_tran.Find("weapon_bg/weapon_name").GetComponent<Text>();

        m_captureShot = m_tran.Find("button/capture_btn").GetComponent<Button>();
        m_shareToFriend = m_tran.Find("button/share_friend_btn").GetComponent<Button>();
        m_shareToFriendLine = m_tran.Find("button/share_line_btn").GetComponent<Button>();
        m_back = m_tran.Find("button/back_btn").GetComponent<Button>();

        captureLeft = m_tran.Find("background/captureLeft").position;
        captureRight = m_tran.Find("background/captureRight").position;
        InitData();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_back.gameObject).onPointerClick += onClose;
        UGUIClickHandler.Get(m_shareToFriend.gameObject).onPointerClick += onShareToFriend;
        UGUIClickHandler.Get(m_shareToFriendLine.gameObject).onPointerClick += onShareToFriendLine;
        UGUIClickHandler.Get(m_captureShot.gameObject).onPointerClick += onCaptureShot;
    }

    public void InitData()
    {
        var logoName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.LOGO);
        var qrName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.GAME_SHARE_QRCODE);
        m_logo.enabled = !string.IsNullOrEmpty(logoName);
        m_erweima.enabled = !string.IsNullOrEmpty(qrName);

        m_logo.SetSprite(ResourceManager.LoadSprite(AtlasName.Login, logoName));
        m_erweima.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, qrName));
    }

    private void onClose(GameObject target, PointerEventData eventData)
    {
        OnHide();
        base.HidePanel();
    }

    private void onShareToFriend(GameObject target, PointerEventData eventData)
    {
        //Logger.Log("shareToFriend---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 1;
            if (Driver.m_platform == EnumPlatform.ios)
            {
                SdkManager.WeixinShare("1", imagePath);//1 friend 2 friend line	
                //SdkManager.SaveCapture(imagePath);
            }
            else
            {
                PanelWeixinShare.handleShareToWeixin("wechat_friends", imagePath);
            }
        }));
    }

    private void onShareToFriendLine(GameObject target, PointerEventData eventData)
    {
        //Logger.Log("shareToFriendLine---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 1;
            if (Driver.m_platform == EnumPlatform.ios)
            {
                SdkManager.WeixinShare("2", imagePath);//1 friend 2 friend line
                //SdkManager.SaveCapture(imagePath);
            }
            else
            {
                PanelWeixinShare.handleShareToWeixin("wechat_moments", imagePath);
            }
        }));
    }

    private void onCaptureShot(GameObject target, PointerEventData eventData)
    {
        //Logger.Log("onCaptureShot---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.SaveCapture(imagePath);
        }));
    }
    public string GetCaptureShotPath()
    {
        Vector2 left = RectTransformUtility.WorldToScreenPoint(UIManager.m_uiCamera, captureLeft);
        Vector2 right = RectTransformUtility.WorldToScreenPoint(UIManager.m_uiCamera, captureRight);
        string path = null;
        path = PanelWeixinShare.DoGetCapture((float)left.x, (float)right.y, Mathf.Abs(right.x - left.x), Mathf.Abs(right.y - left.y));
        timeOutUid = TimerManager.SetTimeOut(1.0f, () =>
        {
            m_back.gameObject.TrySetActive(true);
            m_back.enabled = true;
        });
        return path;
    }

    IEnumerator CaptureCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(1f);
        action();
    }

    public override void OnShow()
    {
        if (m_params.Length == 0) return;
        CDItemData tempData = m_params[0] as CDItemData;
        if (tempData == null) return;
        m_weaponImage.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, tempData.info.Icon));
        m_weaponName.text = tempData.info.DispName;
    }

    public override void OnHide()
    {
        if (timeOutUid > 0)
            TimerManager.RemoveTimeOut(timeOutUid);
    }

    public override void OnBack()
    {
        HidePanel();
    }

    public override void OnDestroy()
    {
        if (timeOutUid > 0)
            TimerManager.RemoveTimeOut(timeOutUid);
    }

    public override void Update()
    {
    }
}

