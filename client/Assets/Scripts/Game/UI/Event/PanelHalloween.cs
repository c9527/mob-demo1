﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelHalloween : BasePanel
{
    public PanelHalloween()
    {
        SetPanelPrefabPath("UI/Event/PanelHalloween");
        AddPreLoadRes("UI/Event/PanelHalloween", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btn_get").gameObject).onPointerClick += OnClkGet;
    }

    public override void OnShow()
    {
       
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkGet(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if (parent != null)
            parent.HidePanel();

        UIManager.ShowPanel<PanelSocity>( new object[]{"corps"});
    }
}
