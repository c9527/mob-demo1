﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelHalloween1 : BasePanel
{
    public PanelHalloween1()
    {
        SetPanelPrefabPath("UI/Event/PanelHalloween1");
        AddPreLoadRes("UI/Event/PanelHalloween1", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btn_buy").gameObject).onPointerClick += OnClkBuy;
    }

    public override void OnShow()
    {
       
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkBuy(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if (parent != null)
            parent.HidePanel();

        UIManager.ShowPanel<PanelMall>();
    }
}
