﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class RedstarData
{
    public int welId;
}
public class PanelRedstar : BasePanel
{
    private DataGrid m_childDataGrid;
    private ItemEventTitle m_eventTitle;
    private int m_id;
    private List<RedstarData> m_lstRedstarData;
    private Text m_txtStarNum;
    public static int STAR_ID = 5012;
    public PanelRedstar()
    {
        SetPanelPrefabPath("UI/Event/PanelRedstar");
        AddPreLoadRes("UI/Event/PanelRedstar", EResType.UI);
        AddPreLoadRes("UI/Event/ItemRedstar", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_lstRedstarData = new List<RedstarData>();
        m_childDataGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_childDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Event/ItemRedstar"), typeof(ItemRedstar));
        m_txtStarNum = m_tran.Find("star_sum").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        AddEventListener(GameEvent.UI_EVENT_DATA_CHANGE, ResetStarNum);
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_eventTitle = m_params[0] as ItemEventTitle;
        m_id = (int)m_params[1];

        var config = ConfigManager.GetConfig<ConfigEvent>();
        ConfigEventLine line = config.GetLine(m_id);
        if (line == null || line.Welfare == null || line.Welfare.Length == 0)
            return;

        m_lstRedstarData.Clear();
        for( int i = 0; i < line.Welfare.Length; ++i )
        {
            RedstarData data = new RedstarData();
            data.welId = line.Welfare[i];
            m_lstRedstarData.Add(data);
        }

        ResetStarNum();
    } 

    private void ResetStarNum()
    {
        m_childDataGrid.Data = m_lstRedstarData.ToArray();
        m_txtStarNum.text = EventManager.GetEventItemNum(STAR_ID).ToString();
    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

}
