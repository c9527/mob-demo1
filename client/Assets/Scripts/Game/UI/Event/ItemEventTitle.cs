﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemEventTitle : ItemRender
{
    private EventTitleData m_data;
    private Text m_txtTitle;
    private Image m_imgState;
    private GameObject m_goState;
    private GameObject m_goBubble;
    private enum STATE
    {
        S_NONE = 0,
        S_NEW = 1,
        S_TIME = 2,
        S_HOT = 3
    }

    public override void Awake()
    {
        m_txtTitle = transform.Find("label").GetComponent<Text>();
        m_imgState = transform.Find("state").GetComponent<Image>();
        m_goState = transform.Find("state").gameObject;
        m_goBubble = transform.Find("bubble_tip").gameObject;
        UGUIClickHandler.Get(transform).onPointerClick += OnTabBtnClick;
    }

    protected override void OnSetData(object data)
    {
        //data.GetType().type
        m_data = data as EventTitleData;
        transform.gameObject.name = m_data.name;
        m_txtTitle.text = m_data.title;
        m_goState.TrySetActive(true);
        if (m_data.state == (int)STATE.S_NONE)
        {
            m_goState.TrySetActive(false);
        }
        else if( m_data.state == (int)STATE.S_NEW )
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "new"));
        }
        else if( m_data.state == (int)STATE.S_TIME )
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "time"));
        }
        else if( m_data.state == (int)STATE.S_HOT )
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "hot"));
        }

        ResetBubble();
    }

    private void OnTabBtnClick(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>() ;
        parent.OnSwitchToView(target.name,this,m_data.id);
    }

    public void ResetBubble()
    {
        int themeId = 0;
        if (int.TryParse(m_data.name, out themeId))
        {
            //活动框架小红点处理
            m_goBubble.TrySetActive(ActivityFrameDataManager.activityThemeHasBubble(themeId));
            return;
        }

        if( m_data.name == PanelEvent.VIEW_RED_STAR )
        {
            //> 对红星照我去战斗做特殊处理,不显示气泡
            m_goBubble.TrySetActive(false);
            return;
        }

        var config = ConfigManager.GetConfig<ConfigEvent>();

        if( m_data.name == PanelEvent.VIEW_7_HAPPY_DAY )
        {
            for( int i = 0; i < 7; ++i )
            {
                ConfigEventLine happyLine = config.GetLine(m_data.id + i);
                if (happyLine == null || happyLine.EventId == 0 || happyLine.Welfare == null || happyLine.Welfare.Length == 0)
                    continue;

                if (EventManager.HasWelfare(happyLine.Welfare[0]))
                {
                    m_goBubble.TrySetActive(true);
                    return;
                }
            }
        }

       
        ConfigEventLine line = config.GetLine(m_data.id);
        if (line == null || line.EventId == 0 || line.Welfare == null || line.Welfare.Length == 0)
            return;
        
         for(int i =0; i < line.Welfare.Length; ++i)
         {
             if(EventManager.HasWelfare( line.Welfare[i]))
             {
                 m_goBubble.TrySetActive(true);
                 return;
             }
         }

         m_goBubble.TrySetActive(false);
    }
}
