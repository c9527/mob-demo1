﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PanelCarnival : BasePanel
{
    public Text desc_txt;

    public PanelCarnival()
    {
        SetPanelPrefabPath("UI/Event/PanelCarnival");
        AddPreLoadRes("UI/Event/PanelCarnival", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("goto_battle")).onPointerClick += OnClkGotoBattle;
    }

    public override void OnShow()
    {
        var info = HasParams() && m_params.Length > 1 ? ConfigManager.GetConfig<ConfigEvent>().GetLine(m_params[1]) : null;
        if (info == null)
        {
            return;
        }
        if (info.Params != null && info.Params.Length > 0)
        {
            desc_txt.text = info.Params[0];
        }
        else
        {
            desc_txt.text = "";
        }
    } 

    private void ResetStarNum()
    {
        
    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkGotoBattle(GameObject target, PointerEventData eventData)
    {
        DestroyPanel();
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if (parent != null)
            parent.HidePanel();

        //任务达人
        UIManager.ShowPanel<PanelDailyMission>();
    }
}
