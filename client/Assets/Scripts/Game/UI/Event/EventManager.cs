﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EventManager
{
    public enum Event_Status
    {
        S_UN_GET = 1,               //> 未获得
        S_ALLOW_RECEIVE = 2,        //> 未领取
        S_RECEIVED = 3              //> 已领取
    }
    private static Dictionary<int, p_event> m_dicWelfareStatus = new Dictionary<int, p_event>();
    private static List<ItemInfo> m_lstEventItemInfo = new List<ItemInfo>();

    public static bool HasWelfare()
    {
        foreach (var item in m_dicWelfareStatus )
        {
            //> 特殊处理一些整点未领取的礼包id
            if ( item.Value.status == (int)Event_Status.S_ALLOW_RECEIVE && ( item.Value.id < 23 || item.Value.id > 32 ) )
                return true;
        }
        return false;
    }

    public static bool HasWelfare( int welId )
    {
        foreach (var item in m_dicWelfareStatus)
        {
            if (item.Key == welId  &&  item.Value.status == (int)Event_Status.S_ALLOW_RECEIVE)
                return true;
        }
        return false;
    }

    public static int GetWelfareStatus( int welId )
    {
        foreach (var item in m_dicWelfareStatus)
        {
            if (item.Key == welId )
                return item.Value.status;
        }
        return 1;
    }

    public static p_event GetWelfareEvent( int welId )
    {
        foreach (var item in m_dicWelfareStatus)
        {
            if (item.Key == welId)
                return item.Value;
        }
        return null;
    }
    public static void SetWelfareStatus( int welId,int status )
    {
        m_dicWelfareStatus[welId].status = status;
        GameDispatcher.Dispatch(GameEvent.UI_BUBBLE_CHANGE);
    }
    private static void Toc_player_event_list(toc_player_event_list proto)
    {
        m_dicWelfareStatus.Clear();
        m_lstEventItemInfo.Clear();
        for(int i = 0; i < proto.events.Length; ++i )
        {
            m_dicWelfareStatus[proto.events[i].id] = proto.events[i];
        }

        m_lstEventItemInfo.AddRange(proto.items);
        GameDispatcher.Dispatch(GameEvent.UI_BUBBLE_CHANGE);
    }

    public static ItemInfo GetEventItemInfo( int id )
    {
        for( int i = 0; i < m_lstEventItemInfo.Count; ++i)
        {
            if (m_lstEventItemInfo[i].ID == id)
                return m_lstEventItemInfo[i];
        }

        return null;
    }

    public static int GetEventItemNum( int id )
    {
        for (int i = 0; i < m_lstEventItemInfo.Count; ++i)
        {
            if (m_lstEventItemInfo[i].ID == id)
                return m_lstEventItemInfo[i].cnt;
        }

        return 0;
    }

    public static void  SetEventItemNum( int id,int num )
    {
        for (int i = 0; i < m_lstEventItemInfo.Count; ++i)
        {
            if (m_lstEventItemInfo[i].ID == id)
                m_lstEventItemInfo[i].cnt = num;
        }
    }
}
