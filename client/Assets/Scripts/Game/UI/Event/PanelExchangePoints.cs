﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelExchangePoints : BasePanel
{
    private TDLotteryExchange _data;

    public Image item_img;
    public Text name_txt;
    public Text desc_txt;
    public Text point_txt;

    public PanelExchangePoints()
    {
        SetPanelPrefabPath("UI/Event/PanelExchangePoints");

        AddPreLoadRes("UI/Event/PanelExchangePoints", EResType.UI);
    }

    public override void Init()
    {
        item_img = m_tran.Find("frame/item_img").GetComponent<Image>();
        name_txt = m_tran.Find("frame/name_txt").GetComponent<Text>();
        desc_txt = m_tran.Find("frame/desc_txt").GetComponent<Text>();
        point_txt = m_tran.Find("frame/point_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("frame/fun_btn")).onPointerClick += OnFunBtnClick;
    }

    private void OnFunBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data!=null && _data.ExchangePoints>0)
        {
            EventModel.Instance.TosExchangePoints(_data.Id);
        }
        else
        {
            UIManager.ShowTipPanel("此物品不能兑换");
        }
    }

    public override void OnShow()
    {
        _data = ConfigManager.GetConfig<ConfigLotteryExchange>().m_dataArr[0];
        var point = 0;
        if (HasParams())
        {
            point = (int)m_params[0];
        }
        if(_data!=null)
        {
             var info = ItemDataManager.GetItem(_data.Id);
            item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            item_img.SetNativeSize();
            name_txt.text = info.DispName;
            desc_txt.text = info.Desc;
        }
        point_txt.text = string.Format("兑换券：<color='{2}'>{0}</color>/{1}", point,_data!=null ? _data.ExchangePoints : 0,_data!=null && point>=_data.ExchangePoints ? "green":"red");
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    //===================================s to c
    private void Toc_player_exchange_points(toc_player_exchange_points data)
    {
        if (data.op_code > 100)
        {
            TipsManager.Instance.showTips("兑换成功！");
        }
        point_txt.text = string.Format("兑换券：<color='{2}'>{0}</color>/{1}", data.points, _data != null ? _data.ExchangePoints : 0, _data != null && data.points >= _data.ExchangePoints ? "green" : "red");
    }
}

