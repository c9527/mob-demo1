﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum Exchange_State
{
    ES_EXCHANGE = 1,          //> 可以兑换
    ES_EXCHANGED = 2,        //> 已经兑换
    ES_HAS_FOREVER = 3,     //> 已经拥有永久的
}
public class ExchangeData
{
    public int Id;
    public Exchange_State State;

}
public class PanelExchange : BasePanel
{
    private int m_themeId;
    private DataGrid m_exchangeData;
    private Image m_imgNotice;
    private Text m_txtExchangeNum;
    private List<ExchangeData> m_lstExchangeData;
    private string m_exchangeType;
    private Text m_txtTime;
    private int m_restTime;//倒计剩余时间
    private float m_timeClick;
    public PanelExchange()
    {
        SetPanelPrefabPath("UI/Event/PanelExchange");
        AddPreLoadRes("UI/Event/PanelExchange", EResType.UI);
        AddPreLoadRes("UI/Event/ItemExchange", EResType.UI);

        AddPreLoadRes("Atlas/Event", EResType.Atlas);
        AddPreLoadRes("Atlas/ActivityFrame", EResType.Atlas);
    }

    public override void Init()
    {
        m_exchangeData = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_exchangeData.SetItemRender(ResourceManager.LoadUIRef("UI/Event/ItemExchange"), typeof(ItemExchange));
        m_txtExchangeNum = m_tran.Find("exchange_num").GetComponent<Text>();
        m_imgNotice = m_tran.Find("notice").GetComponent<Image>();
        m_txtTime = m_tran.Find("time_txt").GetComponent<Text>();
        m_lstExchangeData = new List<ExchangeData>();
    }


    public override void Update()
    {
        if (m_restTime > 0)
        {
            m_timeClick += Time.deltaTime;
            if (m_timeClick >= 1)
            {
                m_timeClick = 0;
                m_restTime--;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
        }
    }

    public override void InitEvent()
    {
        
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_restTime = -1;
        m_themeId = (int)m_params[0];
        var cfgTheme = ConfigManager.GetConfig<ConfigEventTheme>();
        ConfigEventThemeLine themeLine = cfgTheme.GetLine(m_themeId);
        int themeId = 0;
        if( themeLine != null )
        {
            themeId = themeLine.EventList[0];
            m_imgNotice.SetSprite(ResourceManager.LoadSprite(AtlasName.ActivityFrame, themeLine.AdvertPic));
            if (themeLine.EndTimeDec == "" || themeLine.EndTimeDec == null)
            {
                m_restTime = (int)TimeUtil.GetRemainTime(uint.Parse(themeLine.EndTime)).TotalSeconds;
                m_txtTime.text = "活动倒计时：" + TimeUtil.FormatTime(m_restTime, 4, true) + "结束";
            }
            else
            {
                m_txtTime.text = themeLine.EndTimeDec;
            }
        }

        var cfgEventList = ConfigManager.GetConfig<ConfigEventList>();
        ConfigEventListLine eventListLine = cfgEventList.GetLine(themeLine.EventList[0]);
        if (eventListLine != null)
        {
            m_exchangeType = eventListLine.EventParam;
            NetLayer.Send(new tos_player_exchange_list() { type = m_exchangeType });
            NetLayer.Send(new tos_player_exchange_num() { type = m_exchangeType });
        }
        
    } 

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    private void Toc_player_exchange_list( toc_player_exchange_list proto )
    {
        if (m_exchangeType == null || m_exchangeType == "")
            return;

        m_lstExchangeData.Clear();
        var cfgExchange = ConfigManager.GetConfig<ConfigExchange>();
        for( int i = 0; i < cfgExchange.m_dataArr.Length; ++i )
        {
            ConfigExchangeLine exchangeLine = cfgExchange.m_dataArr[i];
            if (exchangeLine.Type != m_exchangeType)
                continue;

            ExchangeData exchange = new ExchangeData();
            exchange.Id = exchangeLine.Id;
            exchange.State = Exchange_State.ES_EXCHANGE;
            for( int j = 0; j < proto.list.Length; ++j )
            {
                if (exchange.Id == proto.list[j])
                {
                    exchange.State = Exchange_State.ES_EXCHANGED;
                    break;
                }    
            }

            m_lstExchangeData.Add(exchange);
        }

        m_exchangeData.Data = m_lstExchangeData.ToArray();
    }
    

    private void Toc_player_exchange( toc_player_exchange proto )
    {
        var cfgExchange = ConfigManager.GetConfig<ConfigExchange>();
        ConfigExchangeLine exchangeLine = cfgExchange.GetLine(proto.id);
        if (exchangeLine == null)
            return;

        //UIManager.PopPanel<PanelRewardItemTip>(new object[] { }, true, this).SetRewardItemList(exchangeLine.Item);
        PanelRewardItemTipsWithEffect.DoShow(exchangeLine.Item);
        NetLayer.Send(new tos_player_exchange_list() { type = proto.type });
    }

    private void Toc_player_exchange_num( toc_player_exchange_num proto )
    {
        m_txtExchangeNum.text = proto.exchange_num.ToString();
    }
    

    protected void OnClkGotoBattle(GameObject target, PointerEventData eventData)
    {
       
    }
}
