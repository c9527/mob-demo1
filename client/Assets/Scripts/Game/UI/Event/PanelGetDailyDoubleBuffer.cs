﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelGetDailyDoubleBuffer : BasePanel
{
    private GameObject m_goGet;
    private GameObject m_goGetted;
    private ItemEventTitle m_eventTitle;
    private int m_activityId;
    private ConfigEventLine m_cfgEventLine;

    public PanelGetDailyDoubleBuffer()
    {
        SetPanelPrefabPath("UI/Event/PanelGetDailyDoubleBuffer");
        AddPreLoadRes("UI/Event/PanelGetDailyDoubleBuffer", EResType.UI);
        
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_goGet = m_tran.Find("btn_get").gameObject;
        m_goGetted = m_tran.Find("btn_getted").gameObject;
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_goGet).onPointerClick += OnClickGet;
    }
    
    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_eventTitle = m_params[0] as ItemEventTitle;
        m_activityId = (int)m_params[1];

        var config = ConfigManager.GetConfig<ConfigEvent>();
        m_cfgEventLine = config.GetLine(m_activityId);
        if (m_cfgEventLine == null)
            return;

        int status = EventManager.GetWelfareStatus(m_cfgEventLine.Welfare[0]);
        if (status == (int)EventManager.Event_Status.S_RECEIVED)
        {
            m_goGet.TrySetActive(false);
            m_goGetted.TrySetActive(true);
            Util.SetGrayShader(m_goGetted.GetComponent<Image>());
        }
        else 
        {
            m_goGet.TrySetActive(true);
            m_goGetted.TrySetActive(false);
        }
        
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    private void OnClickGet(GameObject target, PointerEventData eventData)
    {
         if (m_cfgEventLine == null)
            return;

        int status = EventManager.GetWelfareStatus(m_cfgEventLine.Welfare[0]);
        if (status == (int)EventManager.Event_Status.S_RECEIVED)
        {
            TipsManager.Instance.showTips("奖励已领取!");
            return;
        }

        tos_player_get_event msg = new tos_player_get_event();
        msg.id = m_cfgEventLine.Welfare[0];
        NetLayer.Send(msg);

        EventManager.SetWelfareStatus(m_cfgEventLine.Welfare[0], (int)EventManager.Event_Status.S_RECEIVED);
        m_goGet.TrySetActive(false);
        m_goGetted.TrySetActive(true);
        Util.SetGrayShader(m_goGetted.GetComponent<Image>());

        //var config = ConfigManager.GetConfig<ConfigEventWelfare>();
        //ConfigEventWelfareLine line = config.GetLine(m_cfgEventLine.Welfare[0]);
        //if (line != null)
        //{
            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] { true }, true, null);
            //panelItemTip.SetRewardItemList(line.Item);
        //}
    }
}
