﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class ShareShopItemRender : ItemRender
{
    private ConfigMallLine _data;

    public Image item_img;
    public Text item_txt;
    public Image mark_img;
    public Text cost_txt;
    public Text status_txt;

    public override void Awake()
    {
        item_img = transform.Find("item_img").GetComponent<Image>();
        mark_img = transform.Find("mark_img").GetComponent<Image>();
        item_txt = transform.Find("item_txt").GetComponent<Text>();
        cost_txt = transform.Find("cost_txt").GetComponent<Text>();
        status_txt = transform.Find("status_txt").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as ConfigMallLine;
        if (_data == null)
        {
            return;
        }
        var info = ItemDataManager.GetItem(_data.ItemId);
        if (info != null && (info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || info.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 || info.SubType == GameConst.WEAPON_SUBTYPE_DAGGER))
        {
            item_img.rectTransform.localScale = new Vector3(0.7f,0.7f,0.7f);
        }
        else
        {
            item_img.rectTransform.localScale = Vector3.one;
        }
        item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
        item_img.SetNativeSize();
        item_txt.text = _data.Name;
        ItemMallPrice sell_info = _data.Price!=null ? _data.Price[0] : null;
        if (sell_info != null)
        {
            cost_txt.text = sell_info.price.ToString();
            mark_img.gameObject.TrySetActive(true);
            if (sell_info.time == 7)
            {
                mark_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "time_limit"));
            }
            else if (sell_info.time==0)
            {
                mark_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "time_forever"));
            }
            else
            {
                mark_img.gameObject.TrySetActive(false);
            }
        }
        else
        {
            cost_txt.text = "0";
            mark_img.gameObject.TrySetActive(false);
        }

        if (IsPermanentLimitBuy())
        {
            cost_txt.gameObject.TrySetActive(false);
            status_txt.text = "已拥有";
            status_txt.gameObject.TrySetActive(true);
        }
        else
        {
            //限购
            var isLimitBuy = IsReachLimitAmount();
            cost_txt.gameObject.TrySetActive(!isLimitBuy);
            status_txt.gameObject.TrySetActive(isLimitBuy);
            if (isLimitBuy)
            {
                status_txt.text = "今日已购:" + _data.LimitAmount + "/" + _data.LimitAmount;
            }
            
        }
    }

    private bool IsPermanentLimitBuy()
    {
        var sitem = ItemDataManager.GetDepotItem(_data.ItemId);
        var itemData = ItemDataManager.GetItem((_data.ItemId));
        return (itemData.Type == GameConst.ITEM_TYPE_EQUIP || itemData.Type == GameConst.ITEM_TYPE_ROLE) &&
               itemData.AddRule != 4 && sitem != null && TimeUtil.GetRemainTimeType(sitem.time_limit) == 0;
    }

    public bool CanBuy()
    {
        return !IsReachLimitAmount() && !IsPermanentLimitBuy();
    }

    private bool IsReachLimitAmount()
    {
        var panel = UIManager.GetPanel<PanelShare>();
        return panel != null && _data.LimitAmount != 0 && panel.MallAmoutDic.ContainsKey(_data.ID) &&
            panel.MallAmoutDic[_data.ID] >= _data.LimitAmount;
    }
}
