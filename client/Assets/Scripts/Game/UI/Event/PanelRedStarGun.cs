﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelRedStarGun : BasePanel
{
    public PanelRedStarGun()
    {
        SetPanelPrefabPath("UI/Event/PanelRedStarGun");
        AddPreLoadRes("UI/Event/PanelRedStarGun", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btn_go").gameObject).onPointerClick += OnClkGo;
    }

    public override void OnShow()
    {
       
    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkGo(GameObject target, PointerEventData eventData)
    {
        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if( parent != null )
        {
            parent.SwitchToView(PanelEvent.VIEW_RED_STAR);
        }
    }
}
