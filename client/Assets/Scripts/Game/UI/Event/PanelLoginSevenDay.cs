﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelLoginSevenDay : BasePanel
{
    private GameObject m_goBtn;
    private Text m_txtBtn;
    private Text m_txtLoginNum;

    private ItemEventTitle m_eventTitle;
    private int m_activityId;
    private int m_welId;
    public PanelLoginSevenDay()
    {
        SetPanelPrefabPath("UI/Event/PanelLoginSevenDay");
        AddPreLoadRes("UI/Event/PanelLoginSevenDay", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_goBtn = m_tran.Find("btn_get").gameObject;
        m_txtBtn = m_tran.Find("btn_get/Label").GetComponent<Text>();
        m_txtLoginNum = m_tran.Find("login_num").GetComponent<Text>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_goBtn).onPointerClick += OnClkGetGift;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_eventTitle = m_params[0] as ItemEventTitle;
        m_activityId = (int)m_params[1];

        var config = ConfigManager.GetConfig<ConfigEvent>();
        ConfigEventLine line = config.GetLine(m_activityId);
        if (line == null || line.Welfare == null || line.Welfare.Length == 0)
            return;

        m_welId = line.Welfare[0];
        p_event welfareEvent = EventManager.GetWelfareEvent(m_welId);
        if (welfareEvent == null)
            return;

        int days = welfareEvent.extend >= 7 ? 7 : welfareEvent.extend;
        m_txtLoginNum.text = "(" + days + "/7)";
        if( welfareEvent.status == (int)EventManager.Event_Status.S_ALLOW_RECEIVE )
        {
            m_txtBtn.text = "领取";
            Util.SetNormalShader(m_goBtn.GetComponent<Image>());
        }
        else if( welfareEvent.status == (int)EventManager.Event_Status.S_RECEIVED )
        {
            m_txtBtn.text = "已领取";
            Util.SetGrayShader(m_goBtn.GetComponent<Image>());
        }
        else if( welfareEvent.status == (int)EventManager.Event_Status.S_UN_GET )
        {
            m_txtBtn.text = "未达成";
            Util.SetNormalShader(m_goBtn.GetComponent<Image>());
        }

    } 
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkGetGift(GameObject target, PointerEventData eventData)
    {
        p_event welfareEvent = EventManager.GetWelfareEvent(m_welId);
        if (welfareEvent == null)
            return;

        if (welfareEvent.status == (int)EventManager.Event_Status.S_ALLOW_RECEIVE)
        {
            tos_player_get_event msg = new tos_player_get_event();
            msg.id = m_welId;
            NetLayer.Send(msg);

            EventManager.SetWelfareStatus(m_welId, (int)EventManager.Event_Status.S_RECEIVED);
            m_txtBtn.text = "已领取";
            Util.SetGrayShader(m_goBtn.GetComponent<Image>());

            var config = ConfigManager.GetConfig<ConfigEventWelfare>();
            ConfigEventWelfareLine line = config.GetLine(m_welId);
            if (line != null)
            {
                //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[] { true }, true, null);
                //panelItemTip.SetRewardItemList(line.Item);
                PanelRewardItemTipsWithEffect.DoShow(line.Item);
            }
        }
        else if (welfareEvent.status == (int)EventManager.Event_Status.S_RECEIVED)
        {
            TipsManager.Instance.showTips("已领取"); ;
        }
        else if (welfareEvent.status == (int)EventManager.Event_Status.S_UN_GET)
        {
            TipsManager.Instance.showTips("条件未达成");
        }
    }
}
