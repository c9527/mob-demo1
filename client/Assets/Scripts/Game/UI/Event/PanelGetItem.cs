﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelGetItem : BasePanel
{
    private CDItemData[] _data;
    private CDItemData[] _update_record;
    private int _free_times;

    private int _update_timestamp;
    private int _timer;
    private Action _on_fun_callback;
    private Action _on_hide_callback;

    public GameObject main_frame;
    public GameObject sp_frame;
    private float sp_frame_click_time_delay;
    public Transform item_layer;
    public UIItemRender[] item_list;
    public Button fun_btn;
    public Button close_btn;
    public Button share_btn;
    public Button show_next_btn;
    //public Text continue_txt;
    public GameObject hitArea;
    public Image item_img;
    public Text desc_txt;
	public static bool isRareComment = false;
    public static bool isRareShare = false;
    UIEffect _sp_efffect;

    private CDItemData m_curPopShowData;

    public PanelGetItem()
    {
        SetPanelPrefabPath("UI/Common/PanelGetItem");

        AddPreLoadRes("UI/Common/PanelGetItem", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        main_frame = m_tran.Find("main_frame").gameObject;
        sp_frame = m_tran.Find("sp_frame").gameObject;
        m_tran.Find("HitArea").gameObject.AddComponent<SortingOrderRenderer>();
        item_img = m_tran.Find("sp_frame/item_img").GetComponent<Image>();
        desc_txt = m_tran.Find("sp_frame/desc_txt").GetComponent<Text>();
        item_layer = m_tran.Find("main_frame/items");
        var prototype = m_tran.Find("main_frame/items/item_0").gameObject;
        item_list = new UIItemRender[11];
        item_list[0] = prototype.AddComponent<UIItemRender>();
        item_list[0].SetStyle(null,null,2);
        for(var i=1;i<item_list.Length;i++)
        {
            item_list[i] = (UIHelper.Instantiate(prototype) as GameObject).GetComponent<UIItemRender>();
            item_list[i].name = "item_0" + i;
            item_list[i].SetStyle(null, null, 2);
        }
        close_btn = m_tran.Find("HitArea/close_btn").GetComponent<Button>();
        //continue_txt = m_tran.Find("HitArea/Text").GetComponent<Text>();
        //hitArea = m_tran.Find("HitArea").gameObject;
        share_btn = m_tran.Find("HitArea/share_btn").GetComponent<Button>();
        share_btn.gameObject.TrySetActive(false);
        show_next_btn = m_tran.Find("HitArea/show_next_btn").GetComponent<Button>();
        //show_next_btn.gameObject.TrySetActive(false);
        fun_btn = m_tran.Find("HitArea/fun_btn").GetComponent<Button>();
        fun_btn.gameObject.TrySetActive(false);
        close_btn.gameObject.TrySetActive(false);
        
        //continue_txt.gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(close_btn.gameObject).onPointerClick += OnClickPanel;
        //UGUIClickHandler.Get(hitArea).onPointerClick += OnClickPanel;
        UGUIClickHandler.Get(share_btn.gameObject).onPointerClick += OnShareBtnClick;
        UGUIClickHandler.Get(show_next_btn.gameObject).onPointerClick += OnClickPanel;
//        UGUIClickHandler.Get(sp_frame).onPointerClick += OnClickPanel;
        UGUIClickHandler.Get(fun_btn.gameObject).onPointerClick += OnFunButtonClick;
    }

    private void OnFunButtonClick(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        if (_on_fun_callback != null)
        {
            _on_fun_callback();
            _on_fun_callback = null;
        }
    }

    private void OnClickPanel(GameObject target, PointerEventData eventData)
    {
        if (target == hitArea && !sp_frame.gameObject.activeSelf)
            return;
        if (target == hitArea && Time.realtimeSinceStartup - sp_frame_click_time_delay < 1)
            return;
        if (sp_frame.activeSelf)
        {
            sp_frame.TrySetActive(false);
            PlayNextItem();
        }
        else
        {
            //isShowShareBtn(false);
            //show_next_btn.gameObject.TrySetActive(false);
            //show_next_btn.enabled = false;
			HidePanel();
            SdkManager.appNSLog("isRareComment:" + isRareComment);
            if (Driver.m_platform == EnumPlatform.ios)
            {
                if (isRareComment)
                {
                    if (SdkManager.isGoComment())
                    {
                        SdkManager.m_logFrom = 1;
                        SdkManager.getStarComment();
                    }
                    isRareComment = false;
                }
            }
        }
        //isShowShareBtn(false);
    }

    private void OnShareBtnClick(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelLottoryShare>(new object[] { m_curPopShowData });
    }

    public override void OnShow()
    {
        _update_timestamp = TimeUtil.GetNowTimeStamp();
        _on_fun_callback = null;
        _on_hide_callback = null;
        _free_times = 0;
        if (HasParams())
        {
            _data = m_params[0] as CDItemData[];
            var lottery_type = 1;
            if (m_params.Length > 1)
            {
                _on_hide_callback = (Action)m_params[1];
            }
            if (m_params.Length > 2)
            {
                _on_fun_callback = (Action)m_params[2];
            }
            if (m_params.Length > 3)
            {
                lottery_type = (int)m_params[3];
                fun_btn.transform.Find("Text").GetComponent<Text>().text = lottery_type == 2 ? "再买十次" : "再买一次";
            }
            if (m_params.Length > 4)
            {
                var update_list = m_params[4] as ItemInfo[];
                var temp = new List<CDItemData>();
                for (int i = 0; i < update_list.Length; i++)
                {
                    var update_vo = new CDItemData(update_list[i].ID);
                    if (update_vo.info.Type == GameConst.ITEM_TYPE_MATERIAL)
                    {
                        update_vo.extend = ItemDataManager.GetWeaponItemByMaterial(update_vo.sdata.item_id);
                    }
                    temp.Add(update_vo);
                }
                _update_record = temp.ToArray();
            }
            if (m_params.Length > 5)
            {
                _free_times = (int)m_params[5];
            }
            if (lottery_type == 1 && _free_times > 0)
            {
                fun_btn.transform.Find("Image").GetComponent<Image>().gameObject.TrySetActive(false);
                fun_btn.transform.Find("Text").GetComponent<Text>().text = "再来\n免费一次";
            }
            else
            {
                fun_btn.transform.Find("Image").GetComponent<Image>().gameObject.TrySetActive(true);
                fun_btn.transform.Find("txtPrice").GetComponent<Text>().text = (int)m_params[3] == 2 ? ConfigMisc.GetInt("lottery_ten_diamond").ToString() : ConfigMisc.GetInt("lottery_one_diamond").ToString();
            }
        }
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        fun_btn.gameObject.TrySetActive(false);
        close_btn.gameObject.TrySetActive(false);
        //continue_txt.gameObject.TrySetActive(true);
        sp_frame.TrySetActive(false);
        for (var i = 0; i < item_list.Length; i++)
        {
            item_list[i].transform.SetParent(null);
        }
        PlayNextItem();
    }

    private void PlayNextItem()
    {
        var index = item_layer.transform.childCount;
        if (_data == null || index>=_data.Length || index >= item_list.Length)
        {
            fun_btn.gameObject.TrySetActive(false);
            //close_btn.gameObject.TrySetActive(true);
            //continue_txt.gameObject.TrySetActive(false);
            return;
        }
        var vo = _data[index];
        var render = item_list[index];
        render.transform.SetParent(item_layer);
        render.transform.ResetLocal();
        render.SetData(vo);
        if (vo.info != null && vo.info.Type == GameConst.ITEM_TYPE_EQUIP)
        {
            PopSpFrame(vo,index);
        }
        else
        {
           // UIEffect.ShowEffect(render.transform, EffectConst.UI_GETITEM_ITEM);
            var delay = 0.5f;
            if(vo.info!=null && vo.info.ID==5008)//至尊大奖
            {
                //isRareComment = true;
                delay = 2f;
                UIEffect.ShowEffect(render.transform, EffectConst.UI_LOTTERY_SP_REWARD,2f);
            }
            _timer = TimerManager.SetTimeOut(delay, PlayNextItem);
        }
    }

    private void PopSpFrame(CDItemData value,int index)
    {
        m_curPopShowData = value;
        if (_sp_efffect == null)
        {
            _sp_efffect = UIEffect.ShowEffectAfter(item_img.transform, EffectConst.UI_GETITEM_GETWEAPON);
        }
        AudioManager.PlayUISound(AudioConst.lotteryGetNbWeapon, false);
        sp_frame.TrySetActive(true);
        sp_frame_click_time_delay = Time.realtimeSinceStartup;
        item_img.SetSprite(ResourceManager.LoadIcon(value.info.Icon),value.info.SubType);
        item_img.SetNativeSize();
        //item_img.transform.localScale = Vector3.one * 3f;
        //TweenScale.Begin(item_img.gameObject, 0.25f, Vector3.one * 1.5f).method = UITweener.Method.BounceIn;
        var desc_str = string.IsNullOrEmpty(value.overrideName) ? value.info.DispName : value.overrideName;
        desc_txt.text = desc_str;
        if (value.info.Type==GameConst.ITEM_TYPE_EQUIP && _update_record != null)
        {
            CDItemData record_vo = null;
            for (int i = 0; i < _update_record.Length;i++ )
            {
                if (_update_record[i] != null)
                {
                    if (_update_record[i].info.ID == value.sdata.item_id)
                    {
                        _update_record[i] = null;
                        break;
                    }
                    else if (_update_record[i].extend is ConfigItemWeaponLine && (_update_record[i].extend as ConfigItemWeaponLine).ID==value.sdata.item_id)
                    {
                        record_vo = _update_record[i];
                        _update_record[i] = null;
                        break;
                    }
                }
            }

           //新军火库特殊处理
            if(Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
            {
                if(GlobalConfig.serverValue.weixinShare_open)
                {
                    if (value.extend is ConfigNewLotteryLine)
                    {
                        if ((value.extend as ConfigNewLotteryLine).HighGrade == 2)
                        {
                            isRareComment = true;
                            //isShowShareBtn(true);
                        }
                        /*if((value.extend as ConfigNewLotteryLine).RareType >= 3)
                        {
                            isRareShare = true;
                            isShowShareBtn(true);
                        }*/
                        //else
                        //{
                            //isShowShareBtn(false);
                        //}
                    }
                }
            }

            if (value.info.Type == GameConst.ITEM_TYPE_EQUIP)
            {
                ConfigWeaponStrengthenStateLine ssCfg = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(value.sdata.item_id, 1) as ConfigWeaponStrengthenStateLine;
                if (ssCfg == null) return;
                ConfigItemLine matrilCfg = ItemDataManager.GetItem(ssCfg.DisplayStuff);
                if (matrilCfg == null) return;
                if (value.extend is ConfigNewLotteryLine)
                {
                    if (value.selected)
                    {
                        int day = 0;
                        ConfigNewLotteryLine cfg = (value.extend as ConfigNewLotteryLine);
                        if (cfg != null && cfg.ItemId.Length == 3)
                            day = (int)cfg.ItemId[2];
//                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", matrilCfg.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(day));
                    }
                    //itemId=(value.extend as ConfigNewLotteryLine)
                }
                else
                {
                    p_item pItem = ItemDataManager.GetDepotItem(value.sdata.item_id);
                    if (pItem == null) return;
                    if (pItem.time_limit == 0)
                    {
                        var reward_info = ConfigManager.GetConfig<ConfigReward>().GetLine(value.extend);
//                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", matrilCfg.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(reward_info.ItemList[0].day));
                    }
                }
               /* p_item pItem = ItemDataManager.GetDepotItem(value.sdata.item_id);
                if (pItem == null) return;
                if (pItem.time_limit == 0)
                {//背包中有永久武器

                    ConfigWeaponStrengthenStateLine ssCfg = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(pItem.item_id, 1) as ConfigWeaponStrengthenStateLine;
                    if (ssCfg == null) return;
                    ConfigItemLine matrilCfg = ItemDataManager.GetItem(ssCfg.DisplayStuff);
                    if (matrilCfg == null) return;
                    if (value.extend is ConfigNewLotteryLine)
                    {
                        //itemId=(value.extend as ConfigNewLotteryLine)
                        int day = 0;
                        ConfigNewLotteryLine cfg = (value.extend as ConfigNewLotteryLine);
                        if (cfg != null && cfg.ItemId.Length == 3)
                            day =(int)cfg.ItemId[2];
                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", matrilCfg.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(day));
                    }
                    else
                    {
                        var reward_info = ConfigManager.GetConfig<ConfigReward>().GetLine(value.extend);
                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", matrilCfg.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(reward_info.ItemList[0].day));
                    }
                }*/
            }

            /*if (record_vo != null)
            {
                var tinfo = ItemDataManager.GetWeaponItemByMaterial(record_vo.sdata.item_id);
                if (tinfo != null && tinfo.ID == value.sdata.item_id)
                {

                    if (value.extend is ConfigNewLotteryLine)
                    {
                        //itemId=(value.extend as ConfigNewLotteryLine)
                        int day = 0;
                        ConfigNewLotteryLine cfg = (value.extend as ConfigNewLotteryLine);
                        if (cfg != null && cfg.ItemId.Length == 3)
                            day = cfg.ItemId[2];
                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", record_vo.info.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(day));
                    }
                    else
                    {
                        var reward_info = ConfigManager.GetConfig<ConfigReward>().GetLine(value.extend);
                        desc_str += string.Format("\n\n已拥有此枪械，该奖励自动兑换为[{0}x{1}]", record_vo.info.DispName, StrengthenModel.Instance.ConvertDayToMaterialNum(reward_info.ItemList[0].day));
                    }
                }
            }*/

        }
        desc_txt.text = desc_str;
    }

    private void isShowShareBtn(bool isShow)
    {
        share_btn.gameObject.TrySetActive(isShow);
        share_btn.enabled = isShow;
        if (isShow)
        {
            show_next_btn.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(80, -210);
        }
        else
        {
            show_next_btn.gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(1, -210);
        }
    }

    public override void OnHide()
    {
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        if (_sp_efffect != null)
        {
            _sp_efffect.Destroy();
            _sp_efffect = null;
        }
        if (_on_hide_callback != null)
        {
            _on_hide_callback();
            _on_hide_callback = null;
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
