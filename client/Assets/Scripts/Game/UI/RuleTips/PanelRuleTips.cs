﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelRuleTips : BasePanel {
    private Text m_content;
    private DataGrid m_TabList;
    private GameObject m_tabContent;
    private GameObject m_item0;

    public PanelRuleTips()
    {
        SetPanelPrefabPath("UI/Common/PanelRuleTips");
        AddPreLoadRes("UI/Common/PanelRuleTips", EResType.UI);
        AddPreLoadRes("Atlas/QuickStart", EResType.Atlas);
    }

    public override void Init()
    {
        m_content = m_tran.Find("contentScroll/content").GetComponent<Text>();
        m_tabContent = m_tran.Find("TabList/Content").gameObject;
        m_item0 = m_tran.Find("TabList/Content/ItemBtnTab").gameObject;
        m_TabList = m_tabContent.AddComponent<DataGrid>();
        m_TabList.useLoopItems = true;
        m_TabList.SetItemRender(m_item0, typeof(RuleTabRender));
        m_TabList.onItemSelected += OnItemSelected;
    }

    private void OnItemSelected(object renderData)
    {
        ConfigRuleTipeLine cfg = renderData as ConfigRuleTipeLine;       
        if (cfg != null)
            SetContent(cfg.ruleTips);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("BtnClose")).onPointerClick += ClickClose;
    }

    private void ClickClose(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }

    public override void OnShow()
    {
        ConfigRuleTipeLine[] rules = ConfigManager.GetConfig<ConfigRuleTips>().m_dataArr;
        List<ConfigRuleTipeLine> list = new List<ConfigRuleTipeLine>();
        for (int i = 0; i < rules.Length;i++ )
        {
            if(rules[i].channel==RoomModel.ChannelType)
            {
                list.Add(rules[i]);
            }
        }
        m_TabList.Data = list.ToArray();
        m_TabList.Select(0);
    }

    public override void OnHide()
    {
        
    }

    public static void SetContent(string content)
    {
        PanelRuleTips panel = UIManager.GetPanel<PanelRuleTips>();
        if(panel!=null)
        {
            panel.m_content.text = content;
        }
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}
