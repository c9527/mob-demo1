﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RuleTabRender :ItemRender {
    private Toggle m_toggle;
    public ConfigRuleTipeLine m_configData;
    private Text m_text;
    public override void Awake()
    {
        m_toggle = transform.GetComponent<Toggle>();
        m_text = transform.Find("Text").GetComponent<Text>();
    }

    protected  override void OnSetData(object data)
    {
        //m_toggle.isOn = true;
        m_configData = data as ConfigRuleTipeLine;
        if (m_configData == null)
            return;
        m_text.text = m_configData.ruleName;
        //PanelRuleTips.SetContent(m_configData.ruleTips);
    }

}
