﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

enum ITEM_STATE
{
    NORMAL = 0,
    HOLD = 1
}

class ItemData
{
    public GameObject m_goItem;
    public p_player_info m_data;
    public ITEM_STATE m_state;
}

 class PanelRoomWorldBoss : PanelRoomBase {
    const int TEAM_ITEM_SIZE=5;
    GameObject m_goTeamList;    
    GameObject m_goTeamOperation;
    ItemData[] m_listTeamItem;
    GameObject m_goBtnInvite;
    GameObject m_guestTxt;
    GameObject m_goBtnStart;

    Image _title_img;
    Image _title_name_img;
    Image _title_name_img_en;
    Text m_time;
    GameObject _poplist_mask;

    p_player_info m_selected_data;
    bool m_isEnd = false;
    bool m_bIsOwner=false;
    ItemDropInfo[] m_hostOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             //new ItemDropInfo { type = "OPERATION", subtype = "CHANGE", label = "转让房主"},
             new ItemDropInfo { type = "OPERATION", subtype = "KICK", label = "踢出房间"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };

    ItemDropInfo[] m_guestOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };
    toc_room_info m_tempTeamListData;
    public PanelRoomWorldBoss()
    {
        SetRoomFunc(new []{RoomFunc.InviteFriend,RoomFunc.RankMatching }
            );
        SetPanelPrefabPath("UI/Team/PanelTeam");
        AddPreLoadRes("UI/Team/PanelTeam", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();        
        m_time = m_tran.Find("optionframe/operation/time").GetComponent<Text>();
        m_goTeamList = m_tran.Find("pageframe/team/teamlist").gameObject;
        m_goTeamOperation = m_tran.Find("optionframe").gameObject;
        m_goBtnInvite = m_goTeamOperation.transform.Find("btnInviteFriend").gameObject;
        m_goBtnStart = m_goTeamOperation.transform.Find("btnStart").gameObject;
        m_guestTxt = m_tran.Find("optionframe/WaitText").gameObject;
        m_guestTxt.TrySetActive(false);
        m_listTeamItem = new ItemData[TEAM_ITEM_SIZE];
        for(int i=0;i<TEAM_ITEM_SIZE;i++)
        {
            GameObject item = m_goTeamList.transform.Find("teammate" + i.ToString()).gameObject;
            GameObject btn = item.transform.Find("ask_tip").gameObject;
            UGUIClickHandler.Get(btn).onPointerClick += OnClickInviteFriend;
            UGUIClickHandler.Get(item).onPointerClick += ShowInfoMenu;
            ItemData newItem = new ItemData();
            newItem.m_goItem = item;
            newItem.m_data = null;
            newItem.m_state = ITEM_STATE.NORMAL;
            m_listTeamItem[i] = newItem;
            InitTeamItem(newItem);
        }

    }

    private void ShowInfoMenu(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            ItemData item = m_listTeamItem[index];
            if (item.m_goItem.gameObject == target)
            {
                m_selected_data = item.m_data;
                if (item.m_state == ITEM_STATE.HOLD)
                {
                    if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                    {
                        if (m_bIsOwner)
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                        else
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                    }
                }
                break;
            }
        }
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        _poplist_mask.TrySetActive(false);
        if (m_selected_data == null)
        {
            return;
        }
        var uid = m_selected_data.id;
        if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(uid);
        }
        else if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(uid);
        }
        else if (data.subtype == "CHAT")
        {
            PlayerRecord rcd = new PlayerRecord();
            rcd.m_iPlayerId = uid;
            rcd.m_strName = PlayerSystem.GetRoleNameFull(m_selected_data.name, m_selected_data.id);
            rcd.m_iLevel = m_selected_data.level;
            rcd.m_bOnline = true;
            rcd.m_isFriend = PlayerFriendData.singleton.isFriend(uid);
            rcd.m_iHead = m_selected_data.icon;
            ChatMsgTips msgTips = new ChatMsgTips();
            msgTips.m_rcd = rcd;
            msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
            UIManager.PopChatPanel<PanelChatSmall>(new object[] { msgTips });
        }
        else if (data.subtype == "KICK")
        {
            NetLayer.Send(new tos_room_kick_player { id = uid });
        }
    }

    private void OnInviteClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        m_selected_data = null;
        if (m_goBtnInvite != target)
        {
            for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
            {
                ItemData item = m_listTeamItem[index];
                if (item.m_goItem.transform.Find("ask_tip").gameObject == target)
                {
                    m_selected_data = item.m_data;
                    if (item.m_state == ITEM_STATE.HOLD)
                    {
                        if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                        {
                            if (m_bIsOwner)
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                            else
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                        }
                    }
                    else
                    {
                        NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
                    }
                    break;
                }
            }
        }
        else
        {
            NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
        }
    }

    void InitTeamItem(ItemData item)
    {
        item.m_goItem.transform.Find("head").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("armylvl").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("inner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("owner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("goldace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("silverace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("acheive").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("vip").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("databg").gameObject.TrySetActive(false);        
        item.m_data = null;
        item.m_state = ITEM_STATE.NORMAL;

    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(_poplist_mask).onPointerClick += OnShowMenuHandler;
    }

    private void OnShowMenuHandler(GameObject target = null, PointerEventData eventData = null)
    {
        UIManager.HideDropList();
        _poplist_mask.TrySetActive(false);
    }
    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            UGUIClickHandler.Get(m_listTeamItem[index].m_goItem.transform.Find("ask_tip").gameObject).onPointerClick -= OnClickInviteFriend;
        }
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnShow()
    {
        base.OnShow();
        if (!RoomModel.IsInRoom)
            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.match), stage = 0 });
        m_tempTeamListData = RoomModel.RoomInfo;
        m_tempTeamListData = RoomModel.RoomInfo;
        if (m_tempTeamListData != null)
        {
            OnUpdateTeamList(m_tempTeamListData);
            m_tempTeamListData = null;
        }
    }

    void OnUpdateTeamList(toc_room_info teamData)
    {

        m_goTeamList.TrySetActive(true);
        m_goTeamOperation.TrySetActive(true);

        p_player_info[] playerList = teamData.players;
        var leaderId = teamData.leader;

        if (leaderId == PlayerSystem.roleId)
        {
            m_bIsOwner = true;
        }
        else
        {
            m_bIsOwner = false;
        }
        for (int n = 0; n < TEAM_ITEM_SIZE; ++n)
        {
            InitTeamItem(m_listTeamItem[n]);
        }

        bool bFindLeader = false;

        for (int index = 0; index < playerList.Length; ++index)
        {
            var teamerData = playerList[index];
            var playerid = teamerData.id;

            if (playerid == leaderId)
            {
                bFindLeader = true;

                ItemData item = m_listTeamItem[0];

                HoldTeamListSlot(item, teamerData);
            }
            else
            {
                if (!bFindLeader)
                {
                    if (index + 1 < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index + 1];

                        HoldTeamListSlot(item, teamerData);
                    }

                }
                else
                {
                    if (index < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index];

                        HoldTeamListSlot(item, teamerData);
                    }
                }
            }
        }

        ItemData ownerItem = m_listTeamItem[0];
        if (ownerItem.m_state == ITEM_STATE.HOLD)
        {
            ownerItem.m_goItem.transform.Find("owner").gameObject.TrySetActive(true);
        }


        ShowOperationPri();
    }

    void HoldTeamListSlot(ItemData item, p_player_info data)
    {
        if (item.m_state == ITEM_STATE.HOLD)
            return;

        item.m_state = ITEM_STATE.HOLD;
        item.m_data = data;
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("databg").gameObject.TrySetActive(true);
        GameObject goHead = item.m_goItem.transform.Find("head").gameObject;
        goHead.TrySetActive(true);
        Image headicon = goHead.transform.Find("icon").GetComponent<Image>();
        headicon.SetSprite(ResourceManager.LoadRoleIconBig(data.icon));
        headicon.SetNativeSize();
        headicon.transform.localScale = new Vector3(0.8f, 0.8f, 1);

        GameObject goArmyLvl = item.m_goItem.transform.Find("armylvl").gameObject;
        goArmyLvl.TrySetActive(true);
        Image armylvl = goArmyLvl.transform.Find("army").GetComponent<Image>();
        armylvl.SetSprite(ResourceManager.LoadArmyIcon(data.level));

        Text txtArmyLvl = goArmyLvl.transform.Find("level").GetComponent<Text>();
        txtArmyLvl.text = string.Format("{0}级", data.level);

        GameObject goInner = item.m_goItem.transform.Find("inner").gameObject;
        goInner.TrySetActive(true);

        Text name = goInner.transform.Find("name").GetComponent<Text>();
        name.text = PlayerSystem.GetRoleNameFull(data.name, data.id);

        GameObject goName = goInner.transform.Find("name").gameObject;
        GameObject goVip = item.m_goItem.transform.Find("vip").gameObject;
        Image imgVip = item.m_goItem.transform.Find("vip").GetComponent<Image>();

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            goVip.TrySetActive(false);

            Vector2 vipSizeData = goVip.GetComponent<RectTransform>().sizeDelta;
            //goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            imgVip.SetNativeSize();
        }

        //item.m_goItem.transform.Find("member_icon").gameObject.TrySetActive(true);

        GameObject goVipName = goInner.transform.Find("vipname").gameObject;
        goVipName.TrySetActive(false);
        //        Text txtVipName = goVipName.GetComponent<Text>();

        var gold_ace = item.m_goItem.transform.Find("goldace");
        gold_ace.gameObject.TrySetActive(true);
        gold_ace.Find("num").GetComponent<Text>().text = data.gold_ace.ToString();
        var silver_ace = item.m_goItem.transform.Find("silverace");
        silver_ace.gameObject.TrySetActive(true);
        silver_ace.Find("num").GetComponent<Text>().text = data.silver_ace.ToString();
        var mvp = item.m_goItem.transform.Find("acheive");
        mvp.gameObject.TrySetActive(true);
        mvp.Find("num").GetComponent<Text>().text = data.mvp.ToString();
        foreach (p_title_info info in data.titles)
        {
            if (info.title == "mvp")
            {
                mvp.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "gold_ace")
            {
                gold_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "silver_ace")
            {
                silver_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
        }
    }

    void ShowOperationPri()
    {

        if (m_bIsOwner)
        {
            m_goBtnInvite.TrySetActive(true);
            m_guestTxt.TrySetActive(false);
            m_goBtnStart.TrySetActive(true);
        }
        else
        {
            m_goBtnInvite.TrySetActive(false);
            m_guestTxt.TrySetActive(true);
            m_goBtnStart.TrySetActive(false);
        }
    }

    public override void Update()
    {
        base.Update();
        UpdateTime();
    }


     void UpdateTime()
    {
        if (m_isEnd)
            return;
        DateTime date = TimeUtil.GetNowTime();
        if (m_time  == null)
            return;
        DateTime endShowTime = new DateTime(date.Year, date.Month, date.Day, 13, 0, 0);

        if (date < endShowTime)
        {            
            TimeSpan sp = endShowTime - date;
            m_time.text = string.Format("{0}:{1}", sp.Minutes, sp.Seconds);
            m_isEnd = false;
        }
        else
        {
            m_isEnd = true;
            m_time.text = "0:0";
        }
    }

     void Toc_room_info(toc_room_info data)
     {
         OnUpdateTeamList(RoomModel.RoomInfo);
     }

     void Toc_player_view_player(toc_player_view_player data)
     {
         if (!UIManager.IsOpen<PanelMainPlayerInfo>())
         {
             UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
         }
     }

     void Toc_room_leave_team(CDict data)
     {
         UIManager.ShowPanel<PanelHallBattle>();
     }
}
