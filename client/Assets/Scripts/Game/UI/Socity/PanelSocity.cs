﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;


public class PanelSocity : BasePanel
{
    private Button m_toggleFriend;
    private Button m_toggleCorps;
    private Button m_toggleLegion;
    private Button m_toggleMaster;

    public static BasePanel m_childPanel;
    public GameObject m_friendBubble;
    public GameObject m_corpsBubble;
    public GameObject m_legionBubble;
    public GameObject m_legionLock;
    public GameObject m_masterBubble;

    public PanelSocity()
    {
        SetPanelPrefabPath("UI/Socity/PanelSocity");
        AddPreLoadRes("UI/Socity/PanelSocity", EResType.UI);
    }

    public override void Init()
    {
        m_toggleFriend = m_tran.Find("ToggleFriend").GetComponent<Button>();
        m_toggleCorps = m_tran.Find("ToggleCorps").GetComponent<Button>();
        m_toggleLegion = m_tran.Find("ToggleLegion").GetComponent<Button>();
        m_toggleLegion.enabled = false;
        m_toggleMaster = m_tran.Find("ToggleMaster").GetComponent<Button>();
        m_friendBubble = m_tran.Find("ToggleFriend/Bubble").gameObject;
        m_corpsBubble = m_tran.Find("ToggleCorps/Bubble").gameObject;
        m_legionBubble = m_tran.Find("ToggleLegion/Bubble").gameObject;
        m_masterBubble = m_tran.Find("ToggleMaster/Bubble").gameObject;
        m_legionLock = m_tran.Find("ToggleLegion/lock").gameObject;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ToggleFriend")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("ToggleCorps")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("ToggleLegion")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("ToggleMaster")).onPointerClick += OnClickToggle;
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
    }

    void UpdateBubble()
    {
        m_friendBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.FriendApply));
        m_corpsBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.CorpsApply) || BubbleManager.HasBubble(BubbleConst.CorpsTaskReward) || BubbleManager.HasBubble(BubbleConst.CorpsGuardRunning));
        m_masterBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.ApprenticeGift) || BubbleManager.HasBubble(BubbleConst.MasterGift) || BubbleManager.HasBubble(BubbleConst.StudentApply) || BubbleManager.HasBubble(BubbleConst.MasterGrade));
    }

    public override void OnShow()
    {
        ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.11f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
        var hall = UIManager.GetPanel<PanelHall>();
        if (hall != null && hall.IsOpen())
        {
            hall.CheckChosen(hall.m_btgSocial);
        }

        var familyOpenLvl = ConfigMisc.GetInt("family_level");
        if (PlayerSystem.roleData.level >= familyOpenLvl)
        {
            m_legionLock.TrySetActive(false);
            Util.SetNormalShader(m_toggleLegion.transform.Find("Background").GetComponent<Image>());
        }
        else
        {
            m_legionLock.TrySetActive(true);
            Util.SetGrayShader(m_toggleLegion.transform.Find("Background").GetComponent<Image>());
        }
        if (HasParams())
        {
            string option = (string)m_params[0];
            if (option == "friend")
            {
                m_childPanel = UIManager.ShowPanel<PanelFriend>();
            }
            else if (option == "corps")
            {
                m_childPanel = UIManager.ShowPanel<PanelCorps>(m_params.Length > 1 ? m_params.ArrayDelete(0) : null);
                if (m_params.Length == 1)
                {
                    CorpsDataManager.ReLoadCorpsList();
                }
            }
            else if (option == "legion")
            {
                m_childPanel = UIManager.ShowPanel<PanelLegion>();
            }
            else if (option == "master")
            {
                m_childPanel = UIManager.ShowPanel<PanelSubMasterReward>();
      
            }

        }

        //NetLayer.Send(new tos_corpsmatch_is_registered());
        UpdateBubble();
        if (UIManager.GetPanel<PanelPvpRoomList>() != null)
        {
            UIManager.GetPanel<PanelPvpRoomList>().m_filterRule = "";
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {

    }

    void OnClickToggle(GameObject target, PointerEventData eventData)
    {
        if (m_childPanel != null)
            m_childPanel.HidePanel();

        if (target.name == "ToggleFriend")
        {
            m_childPanel = UIManager.ShowPanel<PanelFriend>();
        }
        else if (target.name == "ToggleCorps")
        {
            //CorpsDataManager.ReloadCorpsData();
            CorpsDataManager.ReLoadCorpsList();
            //NetLayer.Send(new tos_corpsmatch_is_registered());

        }
        else if (target.name == "ToggleLegion")
        {
            var familyOpenLvl = ConfigMisc.GetInt("family_level");
            if (PlayerSystem.roleData.level < familyOpenLvl)
            {
                UIManager.ShowTipPanel(familyOpenLvl + "级开启家族系统");
                return;
            }
            m_childPanel = UIManager.ShowPanel<PanelFamily>();
        }
        else if (target.name == "ToggleMaster")
        {
            // m_childPanel = UIManager.PopPanel<PanelMasterTree>(null, false, this);
            // m_childPanel = UIManager.PopPanel<PanelMasterGrade>(null, false, this);
            m_childPanel = UIManager.ShowPanel<PanelSubMasterReward>();

        }
    }

    void Toc_legionwar_data(toc_legionwar_data data)
    {
        UIManager.PopPanel<PanelLegionMap>();
    }


}
