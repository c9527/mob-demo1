﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/16 16:32:04
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelLotteryExchange : BasePanel
{
    public class LotteryExchangeItem : ItemRender
    {
        private Text m_txtCost;
        private Image m_icon;
        private ConfigNewLotteryExchangeLine m_cfg;
        public override void Awake()
        {
            m_txtCost = transform.Find("Text").GetComponent<Text>();
            m_icon = transform.Find("Icon").GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            m_cfg = data as ConfigNewLotteryExchangeLine;
            if (m_cfg == null) return;
            m_txtCost.text = m_cfg.ToAmount + "";
            var cfg = ItemDataManager.GetItem(m_cfg.ToMoneyId);
            m_icon.SetSprite(ResourceManager.LoadIcon(m_cfg.ToMoneyId),cfg!=null?cfg.SubType:"");
        }
    }


    private GameObject m_btnBuy;
    private GameObject m_btnClose;
    private GameObject m_itemBuy;

    private GameObject m_content;
    private DataGrid m_dataGrid;


    //private Image m_titleImg;
    private Image m_imgMoneyIcon;
    private Text m_txtTitle;
    private Text m_txtTips;

    /// <summary>
    /// 兑换的货币ID
    /// </summary>
    private int m_moneyId;

    private Text m_txtMoney;
    private ConfigNewLotteryExchangeLine m_selCfg;

    public PanelLotteryExchange()
    {
        // 构造器
        SetPanelPrefabPath("UI/LotteryNew/PanelLotteryExchange");
        //AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("UI/LotteryNew/PanelLotteryExchange", EResType.UI);
    }

    public override void Init()
    {
        m_imgMoneyIcon = m_tran.Find("background/imgCoin").GetComponent<Image>();
        m_itemBuy = m_tran.Find("background/BuyGrid/ItemBuy").gameObject;
        m_itemBuy.TrySetActive(false);
        m_btnClose = m_tran.Find("background/btnClose").gameObject;
        m_content = m_tran.Find("background/BuyGrid").gameObject;
        m_dataGrid = m_content.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_itemBuy, typeof(LotteryExchangeItem));
        m_txtTips = m_tran.Find("background/txtName").GetComponent<Text>();
        m_txtMoney = m_tran.Find("background/txtMoney").GetComponent<Text>();
        m_btnBuy = m_tran.Find("background/btnBuy").gameObject;
        //m_titleImg = m_tran.Find("background/TitleImg").GetComponent<Image>();
        m_txtTitle = m_tran.Find("background/Title").GetComponent<Text>();
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnCloseHandler;
        m_dataGrid.onItemSelected = OnDataGridItemSelect;
        UGUIClickHandler.Get(m_btnBuy).onPointerClick += onClickBtnBuyHandler;
    }

    private void OnDataGridItemSelect(object renderData)
    {
        m_selCfg = renderData as ConfigNewLotteryExchangeLine;
        m_txtMoney.text = m_selCfg.FromAmount + "";
        m_imgMoneyIcon.SetSprite(ResourceManager.LoadMoneyIcon((EnumMoneyType)(Enum.Parse(typeof(EnumMoneyType), m_selCfg.FromMoney))));
    }

    private void onClickBtnBuyHandler(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_new_exchange_lottery() { exchange_code = m_selCfg.ID });
    }

    private void Toc_player_new_exchange_lottery(toc_player_new_exchange_lottery data)
    {
        ConfigItemOtherLine cfg = ConfigManager.GetConfig<ConfigItemOther>().GetLine(m_moneyId);
        TipsManager.Instance.showTips("兑换成功 " + cfg.DispName + " ×" + m_selCfg.ToAmount);
        HidePanel();
    }

    private void onClickBtnCloseHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    public override void OnShow()
    {
        if (m_params.Length == 0) return;
        m_moneyId = (int)m_params[0];
        m_txtTips.text = "选择要兑换的紫晶数量";
        if (m_moneyId == 5040)
        {
            m_txtTips.text = "选择要兑换的白晶数量";
            //tleImg.SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, "lotteryNew_txtWhite"));
            m_txtTitle.text = "白晶兑换";
        }
        if (m_moneyId == 5041)
        {
            m_txtTips.text = "选择要兑换的蓝晶数量";
            //titleImg.SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, "lotteryNew_txtBlue"));
            m_txtTitle.text = "蓝晶兑换";
        }
        else
        {
            //m_titleImg.SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, "lotteryNew_txtPurple"));
            m_txtTitle.text = "紫晶兑换";
        }
        ConfigNewLotteryExchange cfg = ConfigManager.GetConfig<ConfigNewLotteryExchange>();
        if (cfg.m_dataArr == null) return;
        List<ConfigNewLotteryExchangeLine> tempList = new List<ConfigNewLotteryExchangeLine>();
        foreach (var temp in cfg.m_dataArr)
        {
            if (temp.ToMoneyId == m_moneyId)
            {
                tempList.Add(temp);
            }
        }
        m_dataGrid.Data = tempList.ToArray();
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }
    public override void Update()
    {

    }
}
