﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/5/11 14:03:48
 * Description:
 * Update History:
 *
 **************************************************************************/

public class LotteryNewClock
{
    private GameObject m_go;
    private GameObject m_bubbleTip;
    private int m_freeInterval;
    private static int m_time = -1;
    public LotteryNewClock()
    {
        // 构造器
        m_freeInterval = ConfigManager.GetConfig<ConfigMisc>().GetLine("lottery_free_interval").ValueInt * 3600;
    }
    public void setClockGO(GameObject go)
    {
        m_go = go;
        Transform tran=m_go.transform.Find("bubble_tip");
        if (tran != null)
        {
            m_bubbleTip = tran.gameObject;
            m_bubbleTip.TrySetActive(false);
            NetLayer.Send(new tos_player_new_lottery() { lottery_type = 1, time = 0 });
        }
    }

     private float clickTime=0;

     public  void Update()
     {
         if (clickTime > 1)
         {
             clickTime = 0;
             if (m_time != -1)
             {
                 TimeSpan timeSpan = TimeUtil.GetRemainTime((uint)(m_time + m_freeInterval));
                 if (timeSpan.TotalSeconds <= 0)
                 {
                     // "免费";
                     if (m_bubbleTip!=null)
                     m_bubbleTip.TrySetActive(true);

                 }
                 else
                 {
                     if (m_bubbleTip != null)
                         m_bubbleTip.TrySetActive(false);
                 }
             }
         }
         else
         {
             clickTime += Time.deltaTime;
         }
     }

    private static void Toc_player_new_lottery(toc_player_new_lottery data)
    {
        if (data.lottery_type == 1)
            m_time = data.free_time;
    }
}
