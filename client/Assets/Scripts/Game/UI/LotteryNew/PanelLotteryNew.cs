﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/15 16:29:37
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelLotteryNew : BasePanel
{
    /// <summary>
    /// 奖励图标信息
    /// </summary>
    public class LotteryNewItem : ItemRender
    {
        private Image m_iconBg;
        private Image m_icon;
        private Text m_text;

        private GameObject m_sign;
        public ConfigNewLotteryLine m_cfg;
        private GameObject m_hasgot;
        private GameObject m_effectBg;
        private ConfigItemLine m_iconItem;

        public override void Awake()
        {
            m_icon = transform.Find("Icon").GetComponent<Image>();
            m_iconBg = transform.Find("IconBg").GetComponent<Image>();
            m_text = transform.Find("Text").GetComponent<Text>();
            m_sign = transform.Find("Sign").gameObject;
            m_hasgot = transform.Find("HasGot").gameObject;
            m_hasgot.TrySetActive(false);
            m_effectBg = transform.Find("EffectBg").gameObject;
            m_effectBg.TrySetActive(false);
            MiniTipsManager.get(m_icon.gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_iconItem == null) return null;
                return new MiniTipsData() { name = m_iconItem.DispName, 
                    props = m_iconItem is ConfigItemWeaponLine ? ItemDataManager.GetPropRenderInfos(m_iconItem as ConfigItemWeaponLine) : new object[0]};
            };
        }

        public bool isFitItemId(int itemId)
        {
            if (m_cfg == null) return false;
            return m_cfg.ItemId[0] == itemId;
        }


        /// <summary>
        /// 设置已获得
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool setHasGot(int id)
        {
            if (m_cfg == null)
                return false;
            if (m_cfg.ID == id)
            {
                m_hasgot.TrySetActive(true);
                m_iconBg.SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, "lotteryNew_Item1"));
            }
            return m_cfg.ID == id;
        }


        public void setEffectBg(bool isShow)
        {
            m_effectBg.TrySetActive(isShow);
        }

        protected override void OnSetData(object data)
        {
            m_effectBg.TrySetActive(false);
            m_hasgot.TrySetActive(false);
            m_cfg = data as ConfigNewLotteryLine;
            if (m_cfg == null) return;
            if (m_cfg.IsLimit == (int)ConfigNewLotteryLine.TYPE_LIMIT.LIMIT ||
                m_cfg.IsLimit == (int)ConfigNewLotteryLine.TYPE_LIMIT.BIG_PRIZE)
            {
                m_sign.TrySetActive(true);
                m_sign.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, m_cfg.IsLimit == (int)ConfigNewLotteryLine.TYPE_LIMIT.LIMIT ? "lotteryNew_sprizeImg" : "lotteryNew_limitImg"));
            }
            else
            {
                m_sign.TrySetActive(false);
            }
            ConfigItemLine itemLine = ItemDataManager.GetItem((int)m_cfg.ItemId[0]);
            if (itemLine == null) return;
            m_iconItem = itemLine;
            m_icon.SetSprite(ResourceManager.LoadIcon(itemLine.Icon), itemLine.SubType);
            m_icon.SetNativeSize();
            if (m_cfg.ItemId.Length == 3)
            {
                string str = "永久";
                if (m_cfg.ItemId[2] >= 0)
                {
                    //str = m_cfg.ItemId[2] + "天";
                    ItemTime time = ItemDataManager.FloatToDayHourMinute(m_cfg.ItemId[2]);
                    str = time.ToString();
                }
                m_text.text = itemLine.DispName + str;
            }
            else if (m_cfg.ItemId.Length == 2)
            {
                ConfigItemLine cfgItem = ItemDataManager.GetItem((int)m_cfg.ItemId[0]);
                if (cfgItem == null) return;
                if (cfgItem.Type == GameConst.ITEM_TYPE_ITEMS || cfgItem.Type == GameConst.ITEM_TYPE_MONEY || cfgItem.Type == GameConst.ITEM_TYPE_MATERIAL)
                {
                    m_text.text = itemLine.DispName + "X" + m_cfg.ItemId[1] + "";
                }
                else if (cfgItem.Type == GameConst.ITEM_TYPE_EQUIP)
                {
                    m_text.text = itemLine.DispName + "(永久)";
                }
                else
                {
                    m_text.text = itemLine.DispName + "(" + m_cfg.ItemId[1] + "天)";
                }
            }
            else
            {
                m_text.text = itemLine.DispName;
            }
            switch (m_cfg.HighGrade)
            {
                case (int)ConfigNewLotteryLine.TYPE_HIGHGRADE.level0:
                    m_text.color = new Color(239 / 255f, 239 / 255f, 239 / 255f);
                    break;
                case (int)ConfigNewLotteryLine.TYPE_HIGHGRADE.level1:
                    m_text.color = new Color(177 / 255f, 243 / 255f, 1f);
                    break;
                case (int)ConfigNewLotteryLine.TYPE_HIGHGRADE.level2:
                    m_text.color = new Color(1f, 249 / 255f, 219 / 255f);
                    break;
            }
            m_iconBg.SetSprite(ResourceManager.LoadSprite(AtlasName.LotteryNew, "lotteryNew_Item" + (m_cfg.HighGrade + 1)));
        }
    }


    public class LotteryNewNoticeItem : ItemRender
    {
        public Text m_text;
        public override void Awake()
        {
            m_text = transform.GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            //throw new NotImplementedException();
            m_text.text = data as string;
        }
    }


    private List<GameObject> m_btnTabList;
    private int m_curBtnTabIndex = -1;
    private List<GameObject> m_sprizeArray;


    private GameObject m_itemSprize;
    private GameObject m_goGridH;
    private DataGrid m_gridH;
    private GameObject m_goGridV;
    private DataGrid m_gridV;

    private bool m_inEvent;

    /// <summary>
    /// 抽奖消耗对应的伙伴类型
    /// </summary>
    private int[] MONEYTYPE = { 5040, 5041, 5042 };
    private string[] MONEYTYPESTRING = { "WHITE_STONE", "BLUE_STONE", "PURPLE_STONE" };
    private int[] LotteryType = new int[3];
    /// <summary>
    /// 存储协议数据
    /// </summary>
    private toc_player_new_lottery[] m_protoData = new toc_player_new_lottery[] { null, null, null };

    /// <summary>
    /// 奖励列表
    /// </summary>
    private Dictionary<int, List<int>> m_sprizeListDic;
    /// <summary>
    /// 奖励配置
    /// </summary>
    private ConfigNewLottery m_newLotteryCfg;

    private Dictionary<int, string[]> m_noticeLucky;
    private Dictionary<int, string[]> m_noticeNotice;

    private Text txt_blueStoneNum;
    private Text txt_purpleStoneNum;
    private Text txt_whiteStoneNum;


    //private Text txt_cionSprizeNum;
    //private Text txt_blueSprizeNum;

    // private Text txt_pointSprizeNum2;
    // private Text txt_pointSprizeNum3;

    private Text txt_pointExchange2;
    private Text txt_pointExchange3;



    private GameObject m_btnCoinOne;
    private GameObject m_btnCoinTen;
    private GameObject m_btnBlueOne;
    private GameObject m_btnBlueTen;
    private GameObject m_btnPurpleOne;
    private GameObject m_btnPurpleTen;


    private GameObject m_btnPointExchange2;
    private GameObject m_btnPointExchange3;


    private Text txt_pointInfoTips1;
    private Text txt_pointInfoTips2;
    private Text txt_pointInfoTips3;


    /// <summary>
    /// 抽奖展示
    /// </summary>
    public Transform item_img;
    private int _timer;

    private GameObject m_panelLucky;
    private GameObject m_panelNotice;

    private DataGrid m_gridLucky;
    private DataGrid m_gridNotice;
    private GameObject m_noticeItem;

    private int m_curPage;
    private const int PAGE_NUM = 6;

    private Text m_txtPage;
    private GameObject m_btnLeft;
    private GameObject m_btnRight;

    private GameObject m_signFree;
    private Text m_txtTime;
    private int costNum;
    private Text m_txtCost;
    private int freeInterval = 0;
    private GameObject m_btnBubble;
    private GameObject m_tabBubble;

    private Text m_dailyLimitText1;
    private Text m_dailyLimitText2;
    private Text m_dailyLimitText3;

    private Text m_discountBuy1;
    private Text m_discountBuy2;
    private Text m_discountBuy3;

    private Text m_discountBuyTen1;
    private Text m_discountBuyTen2;
    private Text m_discountBuyTen3;

    private GameObject m_discount1;
    private GameObject m_discount2;
    private GameObject m_discount3;

    private Text m_txtCost1;
    private Text m_txtCost2;
    private Text m_txtCost3;

    private Text m_txtTencost1;
    private Text m_txtTencost2;
    private Text m_txtTencost3;

    private GameObject m_dis1;
    private GameObject m_dis2;
    private GameObject m_dis3;

    private GameObject m_tendis1;
    private GameObject m_tendis2;
    private GameObject m_tendis3;

    private int[] disCount;
    private int[] disTenCount;

    private Text m_disNum1;
    private Text m_disNum2;
    private Text m_disNum3;

    private GameObject ImgDis1;
    private GameObject ImgDis2;
    private GameObject ImgDis3;

    private Text disrange1;
    private Text disrange2;
    private Text disrange3;

    private GameObject m_lock;
    private bool m_purpleOpen;



    public PanelLotteryNew()
    {
        // 构造器
        // 构造器
        SetPanelPrefabPath("UI/LotteryNew/PanelLotteryNew");
        AddPreLoadRes("Atlas/LotteryNew", EResType.Atlas);
        AddPreLoadRes("UI/LotteryNew/PanelLotteryNew", EResType.UI);
        AddPreLoadRes("Effect/" + EffectConst.UI_JUNHUOKU_XULI, EResType.UIEffect);

        AddPreLoadRes("UI/Common/PanelGetItem", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        freeInterval = ConfigManager.GetConfig<ConfigMisc>().GetLine("lottery_free_interval").ValueInt * 3600;
        m_btnTabList = new List<GameObject>();
        m_sprizeArray = new List<GameObject>();
        for (int i = 1; i <= 3; i++)
        {
            m_btnTabList.Add(m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_" + i).gameObject);
            m_sprizeArray.Add(m_tran.Find("frame/FrameGrid/TopSprize/Sprize_" + i).gameObject);
        }
        m_inEvent = false;
        m_itemSprize = m_tran.Find("frame/FrameGrid/ItemSprize").gameObject;
        m_itemSprize.TrySetActive(false);
        m_goGridH = m_tran.Find("frame/FrameGrid/Grid_H/Grid_content").gameObject;
        m_goGridV = m_tran.Find("frame/FrameGrid/Grid_V/Grid_content").gameObject;
        m_gridH = m_goGridH.AddComponent<DataGrid>();
        m_gridV = m_goGridV.AddComponent<DataGrid>();
        m_gridH.SetItemRender(m_itemSprize, typeof(LotteryNewItem));
        m_gridV.SetItemRender(m_itemSprize, typeof(LotteryNewItem));
        m_lock = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_3/lock").gameObject;
        m_purpleOpen = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_3/toggle").gameObject.activeInHierarchy;

        txt_whiteStoneNum = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_1/CoinInfo/Text").GetComponent<Text>();
        txt_blueStoneNum = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_2/CoinInfo/Text").GetComponent<Text>();
        txt_purpleStoneNum = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_3/CoinInfo/Text").GetComponent<Text>();
        //txt_pointSprizeNum2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/PointInfo/TextNum").GetComponent<Text>();
        // txt_pointSprizeNum3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/PointInfo/TextNum").GetComponent<Text>();
        txt_pointExchange2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BoxExchange/Text").GetComponent<Text>();
        txt_pointExchange3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BoxExchange/Text").GetComponent<Text>();
        m_dailyLimitText1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/buy_cnt").GetComponent<Text>();
        m_dailyLimitText2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/buy_cnt").GetComponent<Text>();
        m_dailyLimitText3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/buy_cnt").GetComponent<Text>();


        // txt_cionSprizeNum = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/TxtNum").GetComponent<Text>();
        //txt_blueSprizeNum = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/TxtNum").GetComponent<Text>();
        m_btnCoinOne = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyOne").gameObject;
        m_btnCoinTen = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyTen").gameObject;
        m_btnBlueOne = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyOne").gameObject;
        m_btnBlueTen = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyTen").gameObject;

        m_btnPointExchange2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BoxExchange").gameObject;
        m_btnPointExchange3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BoxExchange").gameObject;

        txt_pointInfoTips1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/PointInfo/Text").GetComponent<Text>();
        txt_pointInfoTips2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/PointInfo/Text").GetComponent<Text>();
        txt_pointInfoTips3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/PointInfo/Text").GetComponent<Text>();


        m_btnPurpleOne = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyOne").gameObject;
        m_btnPurpleTen = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyTen").gameObject;
        item_img = m_tran.Find("item_img");
        item_img.gameObject.AddComponent<SortingOrderRenderer>();
        item_img.gameObject.TrySetActive(false);

        m_noticeItem = m_tran.Find("frame/FrameMsg/Item").gameObject;
        m_noticeItem.TrySetActive(false);
        m_panelLucky = m_tran.Find("frame/FrameMsg/PanelLucky/ScrollRect/Grid").gameObject;
        m_panelNotice = m_tran.Find("frame/FrameMsg/PanelNotice/ScrollRect/Grid").gameObject;
        m_gridLucky = m_panelLucky.AddComponent<DataGrid>();
        m_gridNotice = m_panelNotice.AddComponent<DataGrid>();
        m_gridLucky.SetItemRender(m_noticeItem, typeof(LotteryNewNoticeItem));
        m_gridNotice.SetItemRender(m_noticeItem, typeof(LotteryNewNoticeItem));
        initSprizeDic();
        initLotteryType();
        m_noticeNotice = new Dictionary<int, string[]>();
        m_noticeLucky = new Dictionary<int, string[]>();
        m_txtPage = m_tran.Find("frame/FrameMsg/PanelLucky/PageCtrl/Text").GetComponent<Text>();
        m_btnLeft = m_tran.Find("frame/FrameMsg/PanelLucky/PageCtrl/BtnLeft").gameObject;
        m_btnRight = m_tran.Find("frame/FrameMsg/PanelLucky/PageCtrl/BtnRight").gameObject;

        m_signFree = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_1/CoinInfo/Free").gameObject;
        //Util.SetGoGrayShader(m_signFree, true);
        m_txtTime = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_1/CoinInfo/TextTime").GetComponent<Text>();
        m_txtTime.text = "";
        m_txtCost = m_btnCoinOne.transform.Find("TxtCost").GetComponent<Text>();
        costNum = getTypeCostByTime(LotteryType[0], 1);
        m_btnBubble = m_btnCoinOne.transform.Find("bubble_tip").gameObject;
        m_btnBubble.TrySetActive(false);
        m_tabBubble = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_1/bubble_tip").gameObject;
        m_tabBubble.TrySetActive(false);

        m_discountBuy1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyOne/TxtDis/TxtDis").GetComponent<Text>();
        m_discountBuy2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyOne/TxtDis/TxtDis").GetComponent<Text>();
        m_discountBuy3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyOne/TxtDis/TxtDis").GetComponent<Text>();

        m_discountBuyTen1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyTen/TxtDis/TxtDis").GetComponent<Text>();
        m_discountBuyTen2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyTen/TxtDis/TxtDis").GetComponent<Text>();
        m_discountBuyTen3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyTen/TxtDis/TxtDis").GetComponent<Text>();

        m_discount1 = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_1/discount").gameObject;
        m_discount2 = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_2/discount").gameObject;
        m_discount3 = m_tran.Find("frame/TabBtn/Grid_tab/BtnTab_3/discount").gameObject;

        m_txtCost1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyOne/TxtCost").GetComponent<Text>();
        m_txtCost2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyOne/TxtCost").GetComponent<Text>();
        m_txtCost3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyOne/TxtCost").GetComponent<Text>();

        m_txtTencost1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyTen/TxtCost").GetComponent<Text>();
        m_txtTencost2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyTen/TxtCost").GetComponent<Text>();
        m_txtTencost3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyTen/TxtCost").GetComponent<Text>();

        m_dis1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyOne/TxtDis").gameObject;
        m_dis2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyOne/TxtDis").gameObject;
        m_dis3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyOne/TxtDis").gameObject;

        m_tendis1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/BtnBuyTen/TxtDis").gameObject;
        m_tendis2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/BtnBuyTen/TxtDis").gameObject;
        m_tendis3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/BtnBuyTen/TxtDis").gameObject;

        disCount = new int[3] { 0, 0, 0 };
        disTenCount = new int[3] { 0, 0, 0 };

        m_disNum1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/discount").GetComponent<Text>();
        m_disNum2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/discount").GetComponent<Text>();
        m_disNum3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/discount").GetComponent<Text>();

        ImgDis1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/Img/Image").gameObject;
        ImgDis2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/Img/Image").gameObject;
        ImgDis3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/Img/Image").gameObject;

        disrange1 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_1/discount/timerange").GetComponent<Text>();
        disrange2 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_2/discount/timerange").GetComponent<Text>();
        disrange3 = m_tran.Find("frame/FrameGrid/TopSprize/Sprize_3/discount/timerange").GetComponent<Text>();

    }
    /// <summary>
    /// 显示货币信息
    /// </summary>
    private void showStoneNumInfo()
    {
        txt_whiteStoneNum.text = PlayerSystem.roleData.white_stone + "";
        txt_blueStoneNum.text = PlayerSystem.roleData.blue_stone + "";
        txt_purpleStoneNum.text = PlayerSystem.roleData.purple_stone + "";
    }

    public bool IsInEvent()
    {
        var config = ConfigManager.GetConfig<ConfigEventList>().GetLine(1661);
        if (config == null)
        {
            return false;
        }
        var time = TimeUtil.GetNowTimeStamp();
        if (time >= int.Parse(config.TimeStart) && time <= int.Parse(config.TimeEnd))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// 获取可抽奖的类型
    /// </summary>
    private void initLotteryType()
    {
        for (int i = 0; i < 3; i++)
        {
            LotteryType[i] = getTypeByMoneyTypeString(i);
        }
    }
    /// <summary>
    /// 获取抽奖类型
    /// </summary>
    private int getTypeByMoneyTypeString(int index)
    {
        int type = -1;
        ConfigNewLotteryCost cfg = ConfigManager.GetConfig<ConfigNewLotteryCost>();
        if (cfg == null) return type;
        string typestr = MONEYTYPESTRING[index];
        foreach (var temp in cfg.m_dataArr)
        {
            if (temp.MoneyType == typestr)
            {
                type = temp.Type;
                break;
            }
        }
        return type;
    }

    /// <summary>
    /// 获得附加的枪魂道具
    /// </summary>
    /// <param name="type"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private ConfigNewLotteryCostLine addItemByTime(int type, int time)
    {
        ConfigNewLotteryCostLine tempcfg = null;
        ConfigNewLotteryCost cfg = ConfigManager.GetConfig<ConfigNewLotteryCost>();
        if (cfg == null) return tempcfg;
        foreach (var temp in cfg.m_dataArr)
        {
            if (temp.Type == type && temp.Time == time)
            {
                tempcfg = temp;
                break;
            }
        }
        return tempcfg;
    }

    /// <summary>
    /// 获得消耗数
    /// </summary>
    /// <param name="type"></param>
    /// <param name="time"></param>
    /// <param name="subTime"></param>
    /// <returns></returns>
    private int getTypeCostByTime(int type, int time, int subTime = 0)
    {
        int cost = 0;
        bool discountIn = false;
        ConfigNewLotteryCost cfg = ConfigManager.GetConfig<ConfigNewLotteryCost>();
        if (type == 1 && disTenCount != null && cfg.GetbyCountAndType(10, "WHITE_STONE") != null)
        {
            discountIn = disTenCount[0] < cfg.GetbyCountAndType(10, "WHITE_STONE").DiscountTime;
        }
        else if (type == 2 && disTenCount != null && cfg.GetbyCountAndType(10, "BLUE_STONE") != null)
        {
            discountIn = disTenCount[1] < cfg.GetbyCountAndType(10, "BLUE_STONE").DiscountTime;
        }
        else if (type == 3 && disTenCount != null && cfg.GetbyCountAndType(10, "PURPLE_STONE") != null)
        {
            discountIn = disTenCount[2] < cfg.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime;
        }

        if (cfg == null) return type;
        foreach (var temp in cfg.m_dataArr)
        {
            if (temp.Type == type && temp.Time == time)
            {
                if (subTime < temp.Cost.Length)
                {
                    cost = temp.Cost[subTime];
                }
                else
                {
                    cost = temp.Cost[temp.Cost.Length - 1];
                }
                if (m_inEvent && temp.DiscountCost != 0&&discountIn)
                {
                    cost = temp.DiscountCost;
                }
                break;
            }
        }
        return cost;
    }



    /// <summary>
    /// 更新显示已获得状态
    /// </summary>
    private void updateHasGot()
    {
        return;
        if (m_protoData[2] != null)
        {
            if (m_protoData[2].his_items.Length == 0) return;
            bool isDeal = false;
            foreach (int id in m_protoData[2].his_items)
            {
                isDeal = false;
                foreach (LotteryNewItem temp in m_gridV.ItemRenders)
                {
                    if (temp.setHasGot(id))
                    {
                        isDeal = true;
                        break;
                    }
                }
                if (!isDeal)
                {
                    foreach (LotteryNewItem temp in m_gridH.ItemRenders)
                    {
                        if (temp.setHasGot(id))
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// 初始化奖励列表
    /// </summary>
    private void initSprizeDic()
    {
        m_sprizeListDic = new Dictionary<int, List<int>>();
        m_newLotteryCfg = ConfigManager.GetConfig<ConfigNewLottery>();

        if (m_newLotteryCfg.m_dataArr == null) return;
        foreach (var temp in m_newLotteryCfg.m_dataArr)
        {
            if (temp.ShowWeight > 0)
                if (m_sprizeListDic.ContainsKey(temp.MoneyType))
                {
                    m_sprizeListDic[temp.MoneyType].Add(temp.ID);
                }
                else
                {
                    m_sprizeListDic.Add(temp.MoneyType, new List<int>() { temp.ID });
                }
        }
    }
    /// <summary>
    /// 获取奖励物品列表
    /// </summary>
    /// <param name="moneyType"></param>
    /// <returns></returns>
    private List<ConfigNewLotteryLine> getSprizeListByMoneyType(int moneyType)
    {
        List<ConfigNewLotteryLine> tempList = new List<ConfigNewLotteryLine>();
        int[] idAry = new int[12];
        if (m_sprizeListDic.ContainsKey(moneyType))
        {
            int count = m_sprizeListDic[moneyType].Count;
            int random = 0;
            int temp;
            for (int i = 0; i < 12; i++)
            {
                random = UnityEngine.Random.Range(0, count);
                if (random < count && random != i)
                {
                    temp = m_sprizeListDic[moneyType][i];
                    m_sprizeListDic[moneyType][i] = m_sprizeListDic[moneyType][random];
                    m_sprizeListDic[moneyType][random] = temp;
                }
            }
            m_sprizeListDic[moneyType].CopyTo(0, idAry, 0, 12);
        }
        int lngth = idAry.Length;
        for (int i = 0; i < lngth; i++)
        {
            if (idAry[i] != 0)
            {
                tempList.Add(m_newLotteryCfg.GetLine(idAry[i]));
            }
        }
        return tempList;
    }


    public override void InitEvent()
    {
        for(int i = 0; i < m_btnTabList.Count; i++)
        {
            if (i != 2 || m_purpleOpen)
            {
                UGUIClickHandler.Get(m_btnTabList[i]).onPointerClick += onClickBtnTabHandler;
            }
        }
        //UGUIClickHandler.Get(m_btnTabList[0].transform.Find("SelectView/Btn_change").gameObject).onPointerClick += onClickBtnTab1Change;
        UGUIClickHandler.Get(m_btnTabList[1].transform.Find("Btn_change").gameObject).onPointerClick += onClickBtnTab2Change;
        if (m_purpleOpen)
        {
            UGUIClickHandler.Get(m_btnTabList[2].transform.Find("Btn_change").gameObject).onPointerClick += onClickBtnTab3Change;
        }
        else
        {
            m_btnTabList[2].transform.Find("Btn_change").gameObject.TrySetActive(false);
            m_btnTabList[2].transform.Find("maintain").gameObject.TrySetActive(true);
        }
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_INFO_CHANGE, showStoneNumInfo);
        UGUIClickHandler.Get(m_btnCoinOne).onPointerClick += onClickBtnCoinOneHandler;
        UGUIClickHandler.Get(m_btnCoinTen).onPointerClick += onClickBtnCoinTenHandler;
        UGUIClickHandler.Get(m_btnBlueOne).onPointerClick += onClickBtnBlueOneHandler;
        UGUIClickHandler.Get(m_btnBlueTen).onPointerClick += onClickBtnBlueTenHandler;
        UGUIClickHandler.Get(m_btnPurpleOne).onPointerClick += onClickBtnPurpleOneHandler;
        UGUIClickHandler.Get(m_btnPurpleTen).onPointerClick += onClickBtnPurpleTenHandler;

        UGUIClickHandler.Get(m_btnPointExchange2).onPointerClick += onClickBtnPointExchange2Handler;
        UGUIClickHandler.Get(m_btnPointExchange3).onPointerClick += onClickBtnPointExchange3Handler;

        for (int i = 1; i <= 3; i++)
        {
            UGUIClickHandler.Get(m_tran.Find("frame/FrameGrid/TopSprize/Sprize_" + i + "/BtnRule_" + i).gameObject).onPointerClick += onClickBtnRuleHandler;
        }

        UGUIClickHandler.Get(m_btnLeft).onPointerClick += onClickBtnLeftHandler;
        UGUIClickHandler.Get(m_btnRight).onPointerClick += onClickBtnRightHandler;

    }

    private void onClickBtnLeftHandler(GameObject target, PointerEventData eventData)
    {
        showLuckyPageList(m_curPage - 1);
    }

    private void onClickBtnRightHandler(GameObject target, PointerEventData eventData)
    {
        showLuckyPageList(m_curPage + 1);
    }

    private void onClickBtnPointExchange2Handler(GameObject target, PointerEventData eventData)
    {
        TDLotteryExchange cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(2) as TDLotteryExchange;
        if (cfg != null)
        {
            if (cfg.ExchangePoints > m_protoData[1].point)
            {
                TipsManager.Instance.showTips("点券不足");
            }
            else
            {
                NetLayer.Send(new tos_player_exchange_points() { op_code = 2 });
            }
        }
    }
    private void onClickBtnPointExchange3Handler(GameObject target, PointerEventData eventData)
    {
        TDLotteryExchange cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(3) as TDLotteryExchange;
        if (cfg != null)
        {
            if (cfg.ExchangePoints > m_protoData[2].point)
            {
                TipsManager.Instance.showTips("点券不足");
            }
            else
            {
                NetLayer.Send(new tos_player_exchange_points() { op_code = 3 });
            }
        }
    }
    private void onClickBtnRuleHandler(GameObject target, PointerEventData eventData)
    {
        int index = int.Parse(target.name.Split('_')[1]);
        UIManager.PopRulePanel("LotteryNew" + index);
    }

    private void onClickBtnCoinOneHandler(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        if (m_protoData[0] != null)
        {
            DateTime lastTime = TimeUtil.GetTime(m_protoData[0].free_time);
            TimeSpan lastSpan = TimeUtil.GetPassTime(m_protoData[0].free_time);
            TimeSpan timeSpan = TimeUtil.GetRemainTime((uint)(m_protoData[0].free_time + freeInterval));
            if (timeSpan.TotalSeconds <= 0)
            {
                sendLotteryMsg(LotteryType[0], 1);
                return;
            }
        }
        if (getTypeCostByTime(LotteryType[0], 1) > PlayerSystem.roleData.white_stone)
        {
            TipsManager.Instance.showTips("白晶不足，完成每日活动任务可获得");
        }
        else
        {
            sendLotteryMsg(LotteryType[0], 1);
        }
    }
    private void onClickBtnCoinTenHandler(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        if (getTypeCostByTime(LotteryType[0], 10) > PlayerSystem.roleData.white_stone)
        {
            TipsManager.Instance.showTips("白晶不足，完成每日活动任务可获得");
        }
        else
        {
            sendLotteryMsg(LotteryType[0], 10);

        }
    }
    private void onClickBtnBlueOneHandler(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        if (getTypeCostByTime(LotteryType[1], 1) > PlayerSystem.roleData.blue_stone)
        {
            TipsManager.Instance.showTips("蓝晶不足");
            UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[1] }, true);
        }
        else
        {
            sendLotteryMsg(LotteryType[1], 1);
        }
    }
    private void onClickBtnBlueTenHandler(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        if (getTypeCostByTime(LotteryType[1], 10) > PlayerSystem.roleData.blue_stone)
        {
            TipsManager.Instance.showTips("蓝晶不足");
            UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[1] }, true);
        }
        else
        {
            sendLotteryMsg(LotteryType[1], 10);
        }
    }

    private void onClickBtnPurpleTenHandler(GameObject target, PointerEventData eventData)
    {
        if (getTypeCostByTime(LotteryType[2], 10) > PlayerSystem.roleData.purple_stone)
        {
            TipsManager.Instance.showTips("紫晶不足");
            UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[2] }, true);
        }
        else
        {
            sendLotteryMsg(LotteryType[2], 10);

        }
    }

    private void onClickBtnPurpleOneHandler(GameObject target, PointerEventData eventData)
    {
        /*if (turning) return;
        if (m_protoData[2] == null) return;
        if (m_protoData[2].his_items.Length == 12)
        {
            TipsManager.Instance.showTips("不能再抽取，请等待重置");
            return;
        }*/
        if (getTypeCostByTime(LotteryType[2], 1) > PlayerSystem.roleData.purple_stone)
        {
            TipsManager.Instance.showTips("紫晶不足");
            UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[2] }, true);
        }
        else
        {
            sendLotteryMsg(LotteryType[2], 1);
        }
    }
    private void onClickBtnTab1Change(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[0] }, true);
    }

    private void onClickBtnTab2Change(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[1] }, true);
    }

    private void onClickBtnTab3Change(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        UIManager.PopPanel<PanelLotteryExchange>(new object[] { MONEYTYPE[2] }, true);
    }
    private void onClickBtnTabHandler(GameObject target, PointerEventData eventData)
    {
        if (turning) return;
        int index = int.Parse(target.name.Split('_')[1]);
        selectBtnTabByIndex(index - 1);
    }

    /// <summary>
    /// 选择显示的奖项
    /// </summary>
    /// <param name="index"></param> index 从0 开始
    private void selectSprizeView(int index)
    {
        int count = m_sprizeArray.Count;
        for (int i = 0; i < count; i++)
        {
            m_sprizeArray[i].TrySetActive(i == index);
        }
        showSprizeItemList(index);
        if (m_protoData[index] == null)
        {
            sendLotteryMsg(LotteryType[index]);
        }
        else if (index == 2)
        {
            updateHasGot();
        }
    }

    /// <summary>
    /// 显示奖项列表
    /// </summary>
    /// <param name="index"></param>
    private void showSprizeItemList(int index)
    {
        if (index >= MONEYTYPE.Length) return;
        List<ConfigNewLotteryLine> tempList = getSprizeListByMoneyType(MONEYTYPE[index]);
        if (tempList.Count < 12) return;
        ConfigNewLotteryLine[] eightAry = new ConfigNewLotteryLine[8];
        ConfigNewLotteryLine[] fourAry = new ConfigNewLotteryLine[4];
        tempList.CopyTo(0, eightAry, 0, 8);
        tempList.CopyTo(8, fourAry, 0, 4);
        m_gridH.Data = fourAry;
        m_gridV.Data = eightAry;
    }

    /// <summary>
    /// 选择标签页
    /// </summary>
    /// <param name="index"></param>
    private void selectBtnTabByIndex(int index = 0)
    {
        if (m_curBtnTabIndex == index) return;
        m_curBtnTabIndex = index;
        int count = m_btnTabList.Count;
        for (int i = 0; i < count; i++)
        {
            setBtnTabSelect(m_btnTabList[i], i == index);
        }
        selectSprizeView(index);
        updateNoticeMsg(true);
    }

    /// <summary>
    /// 设置标签按钮显示
    /// </summary>
    private void setBtnTabSelect(GameObject target, bool isSelect = false)
    {
        if (target == null) return;
        Transform temp = target.transform.Find("SelectView");
        if (temp == null) return;
        temp.gameObject.TrySetActive(isSelect);
        temp = target.transform.Find("Coin");
        if (temp == null) return;
        temp.gameObject.TrySetActive(!isSelect);
    }

    public override void OnShow()
    {
        m_protoData = new toc_player_new_lottery[] { null, null, null };
        selectBtnTabByIndex();
        showStoneNumInfo();
        ConfigNewLotteryCostLine cfg = addItemByTime(LotteryType[1], 1);
        if (cfg != null)
            txt_pointInfoTips2.text = "每次开启额外赠送" + cfg.Items[0].cnt + "个枪魂";
        cfg = addItemByTime(LotteryType[2], 1);
        if (cfg != null)
            txt_pointInfoTips3.text = "星座武器箱不开出重复武器；每次开启送     ×" + cfg.Items[0].cnt;
        cfg = addItemByTime(LotteryType[0], 1);
        if (cfg != null)
            txt_pointInfoTips1.text = "每次开启额外赠送" + cfg.Items[0].cnt + "个枪魂";
        if (HasParams() && m_params.Length == 2)
        {
            string subPanle = m_params[1] as string;
            switch (subPanle)
            {
                case "baijing":
                    onClickBtnTabHandler(m_btnTabList[0], null);
                    break;
                case "lanjing":
                    onClickBtnTabHandler(m_btnTabList[1], null);
                    break;
                case "zijing":
                    onClickBtnTabHandler(m_btnTabList[2], null);
                    break;
            }
        }
        UIManager.HidePopPanelExcept(new List<string>() { typeof(PanelLotteryNew).Name, typeof(PanelHall).Name });
        UIManager.GetPanel<PanelHall>().HideButton();
        m_inEvent = ConfigManager.GetConfig<ConfigEventList>().IsInLotteryEvent();
        InitUIByEvent(m_inEvent);
        var config = ConfigManager.GetConfig<ConfigNewLotteryCost>();

        //紫晶屏蔽
        m_lock.TrySetActive(!GlobalConfig.serverValue.open_purple_lottery);


    }

    void InitUIByEvent(bool isevent)
    {
        var config = ConfigManager.GetConfig<ConfigNewLotteryCost>();
        if (config.GetbyCountAndType(1, "WHITE_STONE") != null && isevent && disCount[0] < config.GetbyCountAndType(1, "WHITE_STONE").DiscountTime)
        {
            m_discountBuy1.text = config.GetbyCountAndType(1, "WHITE_STONE").DiscountCost.ToString();
            m_dis1.TrySetActive(true);
            m_txtCost.gameObject.TrySetActive(false);
        }
        else
        {
            m_dis1.TrySetActive(false);
            m_txtCost.gameObject.TrySetActive(true);
        }
        if (config.GetbyCountAndType(10, "WHITE_STONE") != null && isevent && disTenCount[0] < config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime)
        {
            m_discount1.TrySetActive(true);
            m_discountBuyTen1.text = config.GetbyCountAndType(10, "WHITE_STONE").DiscountCost.ToString();
            m_tendis1.TrySetActive(true);
            m_txtTencost1.gameObject.TrySetActive(false);
            m_disNum1.gameObject.TrySetActive(config.GetbyCountAndType(10, "WHITE_STONE") != null && isevent);
            m_disNum1.text = "折扣次数:" + disTenCount[0] * 10 + "/" + config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime * 10;
            ImgDis1.TrySetActive(true);
            disrange1.gameObject.TrySetActive(true);
        }
        else
        {
            m_discount1.TrySetActive(false);
            m_tendis1.TrySetActive(false);
            m_txtTencost1.gameObject.TrySetActive(true);
            m_disNum1.gameObject.TrySetActive(config.GetbyCountAndType(10, "WHITE_STONE") != null && isevent);
            ImgDis1.TrySetActive(false);
            disrange1.gameObject.TrySetActive(false);

        }

        if (config.GetbyCountAndType(1, "BLUE_STONE") != null && isevent && disCount[1] < config.GetbyCountAndType(1, "BLUE_STONE").DiscountTime)
        {

            m_discountBuy2.text = config.GetbyCountAndType(1, "BLUE_STONE").DiscountCost.ToString();
            m_dis2.TrySetActive(true);
            m_txtCost2.gameObject.TrySetActive(false);
        }
        else
        {

            m_dis2.TrySetActive(false);
            m_txtCost2.gameObject.TrySetActive(true);
        }
        if (config.GetbyCountAndType(10, "BLUE_STONE") != null && isevent&&disTenCount[1] < config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime)
        {
            m_discount2.TrySetActive(true);
            m_discountBuyTen2.text = config.GetbyCountAndType(10, "BLUE_STONE").DiscountCost.ToString();
            m_tendis2.TrySetActive(true);
            m_txtTencost2.gameObject.TrySetActive(false);
            m_disNum2.gameObject.TrySetActive(config.GetbyCountAndType(10, "BLUE_STONE") != null&&isevent);
            m_disNum2.text = "折扣次数:" + (disTenCount[1] * 10).ToString() + "/" + config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime * 10;
            ImgDis2.TrySetActive(true);
            disrange2.gameObject.TrySetActive(true);
        }
        else
        {
            m_discount2.TrySetActive(false);
            m_tendis2.TrySetActive(false);
            m_txtTencost2.gameObject.TrySetActive(true);
            m_disNum2.gameObject.TrySetActive(config.GetbyCountAndType(10, "BLUE_STONE") != null && isevent);
            ImgDis2.TrySetActive(false);
            disrange2.gameObject.TrySetActive(false);
        }

        if (config.GetbyCountAndType(1, "PURPLE_STONE") != null && isevent && disCount[2] < config.GetbyCountAndType(1, "PURPLE_STONE").DiscountTime)
        {

            m_discountBuy3.text = config.GetbyCountAndType(1, "PURPLE_STONE").DiscountCost.ToString();
            m_dis3.TrySetActive(true);
            m_txtCost3.gameObject.TrySetActive(false);
        }
        else
        {

            m_dis3.TrySetActive(false);
            m_txtCost3.gameObject.TrySetActive(true);
        }
        if (config.GetbyCountAndType(10, "PURPLE_STONE") != null && isevent && disTenCount[2] < config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime)
        {
            m_discount3.TrySetActive(true);
            m_discountBuyTen3.text = config.GetbyCountAndType(10, "PURPLE_STONE").DiscountCost.ToString();
            m_tendis3.TrySetActive(true);
            m_txtTencost3.gameObject.TrySetActive(false);
            m_disNum3.gameObject.TrySetActive(config.GetbyCountAndType(10, "PURPLE_STONE") != null && isevent);
            m_disNum3.text ="折扣次数:"+ disTenCount[2]*10 + "/" + config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime*10;
            ImgDis3.TrySetActive(true);
            disrange3.gameObject.TrySetActive(true);
        }
        else
        {
            m_discount3.TrySetActive(false);
            m_tendis3.TrySetActive(false);
            m_txtTencost3.gameObject.TrySetActive(true);
            m_disNum3.gameObject.TrySetActive(config.GetbyCountAndType(10, "PURPLE_STONE") != null && isevent);
            ImgDis3.TrySetActive(false);
            disrange3.gameObject.TrySetActive(false);
        }
        if (disTenCount != null)
        {
            if (config.GetbyCountAndType(10, "WHITE_STONE") != null&&isevent)
            {
                ImgDis1.TrySetActive(true);
                disrange1.text = config.GetbyCountAndType(10, "WHITE_STONE").DiscountTimeRange;
                disrange1.gameObject.TrySetActive(true);
                if (disTenCount[0] <= config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime)
                {
                    m_disNum1.text = "折扣次数:" + disTenCount[0] * 10 + "/" + config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime * 10;
                }
                else
                {
                    m_disNum1.text = "折扣次数:" + config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime * 10 + "/" + config.GetbyCountAndType(10, "WHITE_STONE").DiscountTime * 10;
                }
            }
            if (config.GetbyCountAndType(10, "BLUE_STONE") != null && isevent)
            {
                ImgDis2.TrySetActive(true);
                disrange2.text = config.GetbyCountAndType(10, "BLUE_STONE").DiscountTimeRange;
                disrange2.gameObject.TrySetActive(true);
                if (disTenCount[1] <= config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime)
                {
                    m_disNum2.text = "折扣次数:" + disTenCount[1] * 10 + "/" + config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime * 10;
                }
                else
                {
                    m_disNum2.text = "折扣次数:" + config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime * 10 + "/" + config.GetbyCountAndType(10, "BLUE_STONE").DiscountTime * 10;
                }
            }
            if (config.GetbyCountAndType(10, "PURPLE_STONE") != null && isevent)
            {
                ImgDis3.TrySetActive(true);
                disrange3.text = config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTimeRange;
                disrange3.gameObject.TrySetActive(true);
                if (disTenCount[2] <= config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime)
                {
                    m_disNum3.text = "折扣次数:" + disTenCount[2] * 10 + "/" + config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime * 10;
                }
                else
                {
                    m_disNum3.text = "折扣次数:" + config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime * 10 + "/" + config.GetbyCountAndType(10, "PURPLE_STONE").DiscountTime * 10;
                }
            }
        }

        
    }

    public override void OnHide()
    {
        if (_timer > 0)
        {
            TimerManager.RemoveTimeOut(_timer);
            _timer = 0;
        }
        UIManager.GetPanel<PanelHall>().ShowButton();
    }
    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_INFO_CHANGE, showStoneNumInfo);
    }

    private float clickTime = 0;
    private int updateSpace = 0;
    public override void Update()
    {
        if (!UIManager.IsOpen<PanelLotteryNew>()) return;
        if (clickTime > 1)
        {
            updateSpace++;
            if (updateSpace > 10)
            {
                updateSpace = 0;
                NetLayer.Send(new tos_player_new_lottery_show() { type = LotteryType[m_curBtnTabIndex] });
            }
            clickTime = 0;
            if (m_protoData[0] != null)
            {
                TimeSpan timeSpan = TimeUtil.GetRemainTime((uint)(m_protoData[0].free_time + freeInterval));
                if (timeSpan.TotalSeconds <= 0)
                {
                    m_txtTime.text = "";
                    //Util.SetGoGrayShader(m_signFree, false);
                    m_signFree.TrySetActive(true);
                    m_txtCost.text = "免费";
                    m_tabBubble.TrySetActive(true);
                    m_btnBubble.TrySetActive(true);
                    m_txtCost.gameObject.TrySetActive(false);
                }
                else
                {
                    m_tabBubble.TrySetActive(false);
                    m_btnBubble.TrySetActive(false);
                    //Util.SetGoGrayShader(m_signFree, true);
                    m_signFree.TrySetActive(false);
                    int hour = timeSpan.Hours + 24 * timeSpan.Days;
                    int min = timeSpan.Minutes;
                    int sec = timeSpan.Seconds;
                    m_txtCost.gameObject.TrySetActive(true);
                    m_txtTime.text = (hour < 10 ? "0" + hour : hour + "") + ":" + (min < 10 ? "0" + min : min + "") + ":" + (sec < 10 ? "0" + sec : sec + "");
                    m_txtCost.text = costNum + "";
                }
            }
            //if(m_protoData[0].free_time)
        }
        else
        {
            clickTime += Time.deltaTime;
        }

        if (turning)
        {
            turnClick++;
            if (turnClick == turnSpace)
            {
                turnClick = 0;

                turnCount++;

                if (turnCount == 12 + turnIndex + 1)
                {
                    //转一圈开始减速
                    turnSlow = true;
                }
                else if (turnCount == 24 + turnIndex + 1)
                {
                    turnSpace += 10;
                }
                else if (turnCount == 24 + turnIndex + 2)
                {//停止转圈
                    turnEndDeal();
                }
                if (turning)
                {
                    int index = (turnCount - 1) % 12;
                    int lastIndex = index - 1;
                    if (index - 1 < 0)
                    {
                        lastIndex = 11;
                    }
                    turnItems[index].setEffectBg(true);
                    turnItems[lastIndex].setEffectBg(false);
                }
                if (turnSlow)
                {
                    turnSpace++;
                }
            }
        }
    }





    private void OnUpdateLotteryResult(toc_player_new_lottery data, bool isShowEffect = false)
    {
        if (isShowEffect)
        {
            UIEffect.ShowEffect(item_img, EffectConst.UI_JUNHUOKU_XULI, 2f, (obj) =>
            {
                //OnUpdateLotteryResult(data);
            }, null);
        }
        //var tdata = lottery_config.GetLine(_data.reward_id_list[0]);
        var temp = new List<CDItemData>();


        ConfigItemLine itemCfg;
        int lngth = data.items.Length;
        for (int i = 0; i < lngth; i++)
        {
            var itemInfo = data.items[i];
            var vo = new CDItemData(itemInfo.ID);
            vo.sdata.item_cnt = itemInfo.cnt;
            vo.extend = itemInfo.ID;
            if (i < data.exists.Length)
                vo.selected = data.exists[i];
            if (vo.info.Type == GameConst.ITEM_TYPE_EQUIP)
            {
                vo.extend = getNewLotteryCfgByReward(data.lottery_type, itemInfo.ID, itemInfo.day);
            }
            ///vo.extend = itemInfo.ID;
            itemCfg = ItemDataManager.GetItem(itemInfo.ID);
            if (itemCfg == null) continue;
            if (itemCfg.Type == GameConst.ITEM_TYPE_MATERIAL || itemCfg.Type == GameConst.ITEM_TYPE_MONEY || itemCfg.Type == GameConst.ITEM_TYPE_ITEMS)
                vo.overrideName = itemCfg.DispName;
            else if (itemInfo.day >= 0)
            {
                ItemTime time = ItemDataManager.FloatToDayHourMinute(itemInfo.day);
                vo.overrideName = itemCfg.DispName + time.ToString();
            }
            else
            {
                vo.overrideName = itemCfg.DispName + "(永久)";
            }
            temp.Add(vo);
        }
        //送枪魂特殊处理
        ConfigNewLotteryCostLine addCfg = addItemByTime(data.lottery_type, data.time);
        if (addCfg != null && addCfg.Items[0].ID > 0)
        {
            var vo = new CDItemData(addCfg.Items[0].ID);
            vo.sdata.item_cnt = addCfg.Items[0].cnt;
            temp.Add(vo);
        }
        //送枪魂特殊处理
        /* var sp_reward = ConfigManager.GetConfig<ConfigMisc>().GetLine("lottery_gun_soul");
         if (sp_reward != null && sp_reward.ValueInt > 0)
         {
             var vo = new CDItemData(sp_reward.ValueInt);
             vo.sdata.item_cnt = _lottery_type == 2 ? 10 : 1;
             temp.Add(vo);
         }*/

        item_img.gameObject.TrySetActive(true);
        var avatar = item_img.Find("Image").GetComponent<Image>();
        var cfg = ItemDataManager.GetItem(data.items[0].ID);
        avatar.SetSprite(ResourceManager.LoadIcon(data.items[0].ID), cfg != null ? cfg.SubType : "");
        avatar.SetNativeSize();
        avatar.transform.localScale = Vector3.one * 1.5f;
        avatar.gameObject.TrySetActive(false);
        float _waitTime = 0.2f;
        if (isShowEffect)
        {
            _waitTime = 1.2f;
        }
        _timer = TimerManager.SetTimeOut(_waitTime, () =>
        {
            avatar.gameObject.TrySetActive(true);
            if (isShowEffect)
            {
                avatar.transform.localScale = Vector3.one * 0.5f;
                TweenScale.Begin(avatar.gameObject, 0.25f, Vector3.one * 1.5f).method = UITweener.Method.BounceOut;
            }
            else
            {
                avatar.transform.localScale = Vector3.one * 3f;
                TweenScale.Begin(avatar.gameObject, 0.25f, Vector3.one * 1.5f).method = UITweener.Method.BounceIn;
            }

            _timer = TimerManager.SetTimeOut(0.5f, () =>
            {
                item_img.gameObject.TrySetActive(false);
                //OnUpdateInfoView();
                Action hide_action = () => { };
                Action fun_action = () => { };
                UIManager.PopPanel<PanelGetItem>(new object[] { temp.ToArray(), hide_action, fun_action, m_curBtnTabIndex, ItemDataManager.item_added_temp_record.value.ToArray(), 0 }, true, this);
                EventModel.Instance.TosGetLotteryRank();
                //_timer = TimerManager.SetTimeOut(60f, EventModel.Instance.TosGetLotteryRank);
                _timer = TimerManager.SetTimeOut(60f, () => { });
            });
        });
    }

    /// <summary>
    /// 奖励物品获取配置
    /// </summary>
    /// <param name="type"></param>
    /// <param name="itemId"></param>
    /// <returns></returns>
    private ConfigNewLotteryLine getNewLotteryCfgByReward(int type, int itemId, float day)
    {
        ConfigNewLotteryLine tempCfg = null;
        ConfigNewLottery newlottery = ConfigManager.GetConfig<ConfigNewLottery>();
        int moneyId = MONEYTYPE[type - 1];
        bool isFit = false;
        foreach (ConfigNewLotteryLine temp in newlottery.m_dataArr)
        {
            isFit = false;
            if (temp.MoneyType == moneyId && temp.ItemId[0] == itemId)
            {
                if (day > 0)
                {
                    if (temp.ItemId.Length == 3 && temp.ItemId[2] == day)
                        isFit = true;
                }
                else
                    isFit = true;

                if (isFit)
                {
                    tempCfg = temp;
                    break;
                }
            }
        }
        return tempCfg;
    }

    /// <summary>
    /// 显示跑马灯效果
    /// </summary>

    private bool turning = false;
    private int turnIndex = -1;
    private bool turnSlow = false;
    private LotteryNewItem[] turnItems;
    private int turnSpace = 2;
    private int turnClick = 0;
    private int turnCount = 0;
    private void showItemTurning(int itemId)
    {
        turnItems = getTurnItemList();
        int lngth = turnItems.Length;
        for (int i = 0; i < lngth; i++)
        {
            if (turnItems[i].isFitItemId(itemId))
            {
                turnIndex = i;
            }
        }
        turnSlow = false;
        turnSpace = 3;
        turnClick = 0;
        turning = true;
        turnCount = 0;
    }

    /// <summary>
    /// 跑马灯结算处理
    /// </summary>
    private void turnEndDeal()
    {
        turnItems[turnIndex].setEffectBg(false);
        turnIndex = -1;
        turning = false;
        turnItems = null;
        OnUpdateLotteryResult(m_protoData[2]);
        updateHasGot();
    }



    /// <summary>
    /// 获得转圈的列表
    /// </summary>
    /// <returns></returns>
    private LotteryNewItem[] getTurnItemList()
    {
        LotteryNewItem[] tempAry = new LotteryNewItem[12];
        m_gridH.ItemRenders.CopyTo(0, tempAry, 0, 2);
        m_gridV.ItemRenders.CopyTo(0, tempAry, 2, 4);
        tempAry[6] = m_gridH.ItemRenders[3] as LotteryNewItem;
        tempAry[7] = m_gridH.ItemRenders[2] as LotteryNewItem;
        tempAry[8] = m_gridV.ItemRenders[7] as LotteryNewItem;
        tempAry[9] = m_gridV.ItemRenders[6] as LotteryNewItem;
        tempAry[10] = m_gridV.ItemRenders[5] as LotteryNewItem;
        tempAry[11] = m_gridV.ItemRenders[4] as LotteryNewItem;
        return tempAry;
    }


    /// <summary>
    ///添加新的公告信息
    /// </summary>
    private void addNoticeInfo(int type, string[] msgLuck, string[] msgNotice)
    {
        if (msgLuck != null && msgLuck.Length != 0)
        {
            if (!m_noticeLucky.ContainsKey(type))
                m_noticeLucky.Add(type, msgLuck);
            else
            {
                List<string> sList = m_noticeLucky[type].ToList<string>();
                sList.InsertRange(0, msgLuck.ToList<string>());
                m_noticeLucky[type] = sList.ToArray();
            }
        }

        if (msgNotice != null && msgNotice.Length != 0)
        {
            if (!m_noticeNotice.ContainsKey(type))
                m_noticeNotice.Add(type, msgNotice);
            else
            {
                List<string> sList = m_noticeNotice[type].ToList<string>();
                sList.InsertRange(0, msgNotice.ToList<string>());
                m_noticeNotice[type] = sList.ToArray();
            }
        }
        updateNoticeMsg(false);
    }

    /// <summary>
    /// 更新信息面板
    /// </summary>
    /// <param name="type"></param>
    /// <param name="msgLuck"></param>
    /// <param name="msgNotice"></param>
    private void updateNoticeInfo(int type, string[] msgLuck, string[] msgNotice)
    {
        Array.Reverse(msgLuck);
        Array.Reverse(msgNotice);
        if (msgLuck != null && msgLuck.Length != 0)
        {
            if (!m_noticeLucky.ContainsKey(type))
                m_noticeLucky.Add(type, msgLuck);
            else
                m_noticeLucky[type] = msgLuck;
        }
        else
            m_noticeLucky[type] = new string[] { };
        if (msgNotice != null && msgNotice.Length != 0)
        {
            if (!m_noticeNotice.ContainsKey(type))
                m_noticeNotice.Add(type, msgNotice);
            else
                m_noticeNotice[type] = msgNotice;
        }
        else
            m_noticeNotice[type] = new string[] { };
        updateNoticeMsg(true);
    }

    /// <summary>
    /// 显示实时公告面板
    /// </summary>
    private void updateNoticeMsg(bool isReset)
    {
        int type = LotteryType[m_curBtnTabIndex];
        /*if (m_noticeLucky.ContainsKey(type))
            m_gridLucky.Data = m_noticeLucky[type];
        else
            m_gridLucky.Data = new object[0];*/
        showLuckyPageList(isReset ? 1 : m_curPage);
        if (m_noticeNotice.ContainsKey(type))
            m_gridNotice.Data = m_noticeNotice[type];
        else
            m_gridNotice.Data = new object[0];
    }

    /// <summary>
    /// 显示页面列表
    /// </summary>
    /// <param name="index"></param>
    private void showLuckyPageList(int index)
    {
        if (index < 1) index = 1;
        int type = LotteryType[m_curBtnTabIndex];
        List<string> temp;
        if (m_noticeLucky.ContainsKey(type))
            temp = m_noticeLucky[type].ToList<string>();
        else
            temp = new List<string>();
        int maxPage = (int)Math.Ceiling(temp.Count / (float)PAGE_NUM);
        if (maxPage == 0)
            index = 1;
        else if (index > maxPage)
            index = maxPage;
        int startId = (index - 1) * PAGE_NUM;
        int copyNum = PAGE_NUM;
        m_curPage = index;
        if (startId < temp.Count)
        {
            if (startId + copyNum >= temp.Count)
            {
                copyNum = temp.Count - startId;
            }
            string[] copyAry = new string[copyNum];
            temp.CopyTo(startId, copyAry, 0, copyNum);
            m_gridLucky.Data = copyAry;
            m_txtPage.text = m_curPage + "/" + maxPage;
        }
        else
        {
            m_gridLucky.Data = new object[0];
            m_txtPage.text = "1/1";
        }

    }


    /// <summary>
    /// 请求抽奖信息
    /// </summary>
    /// <param name="type"></param>
    /// <param name="time"></param>
    private void sendLotteryMsg(int type, int time = 0)
    {
        ItemDataManager.ResetTempAddedRecord(ItemDataManager.UPDATE_REASON_NEWLOTTERY);
        NetLayer.Send(new tos_player_new_lottery() { lottery_type = type, time = time });
        if (time == 0)
            NetLayer.Send(new tos_player_new_lottery_show() { type = type });
    }

    private void Toc_player_new_lottery_show(toc_player_new_lottery_show data)
    {
        updateNoticeInfo(data.type, data.lucky_list, data.recent_list);
    }

    private void Toc_player_exchange_points(toc_player_exchange_points data)
    {
        TDLotteryExchange cfg = new TDLotteryExchange();
        if (data.op_code == 2)
        {
            m_protoData[1].point = data.points;
            // txt_pointSprizeNum2.text = data.points + "";
            cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(2) as TDLotteryExchange;
            if (cfg != null)
            {
                txt_pointExchange2.text = data.points + "/" + cfg.ExchangePoints + "兑换";
            }

        }
        else if (data.op_code == 3)
        {
            m_protoData[2].point = data.points;
            // txt_pointSprizeNum3.text = data.points + "";
            cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(3) as TDLotteryExchange;
            if (cfg != null)
            {
                txt_pointExchange3.text = data.points + "/" + cfg.ExchangePoints + "兑换";
            }
        }

        if (cfg != null)
        {
            var temp = new List<CDItemData>();
            var vo = new CDItemData(cfg.ITEM);
            vo.sdata.item_cnt = 1;
            vo.extend = cfg.ITEM;
            temp.Add(vo);

            _timer = TimerManager.SetTimeOut(0.5f, () =>
            {
                item_img.gameObject.TrySetActive(false);
                Action hide_action = () => { };
                Action fun_action = () => { };
                UIManager.PopPanel<PanelGetItem>(new object[] { temp.ToArray(), hide_action, fun_action, m_curBtnTabIndex, ItemDataManager.item_added_temp_record.value.ToArray(), 0 }, true, this);
                EventModel.Instance.TosGetLotteryRank();
                //_timer = TimerManager.SetTimeOut(60f, EventModel.Instance.TosGetLotteryRank);
                _timer = TimerManager.SetTimeOut(60f, () => { });
            });

        }
    }

    private void Toc_player_new_lottery(toc_player_new_lottery data)
    {
        addNoticeInfo(data.lottery_type, data.lucky_list, data.recent_list);
        disCount[data.lottery_type - 1] = data.discount_one_count;
        disTenCount[data.lottery_type - 1] = data.discount_ten_count;
        InitUIByEvent(m_inEvent);
        var daily_buy_cnt = data.buy_cnt + "/99";
        if (data.lottery_type == LotteryType[0])
        {//金币
            m_protoData[0] = data;
            //txt_cionSprizeNum.text = data.award_amount + "";
            m_btnCoinOne.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[0], 1) + "";
            m_btnCoinTen.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[0], 10) + "";
            m_dailyLimitText1.text = daily_buy_cnt;
            if (data.time != 0)
            {
                //抽奖展示获得
                OnUpdateLotteryResult(data, true);

                /*UIEffect.ShowEffect(m_sprizeArray[0].transform, EffectConst.UI_JUNHUOKU_XULI, 2f, (obj) =>
                {
                    OnUpdateLotteryResult(data);
                }, null);*/
            }
        }
        else if (data.lottery_type == LotteryType[1])
        {//蓝晶
            m_protoData[1] = data;
            //txt_pointSprizeNum2.text = data.point + "";

            TDLotteryExchange cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(2) as TDLotteryExchange;
            if (cfg != null)
            {
                txt_pointExchange2.text = data.point + "/" + cfg.ExchangePoints + "兑换";
                var cfgs = ItemDataManager.GetItem(cfg.ITEM);

            }
            //txt_blueSprizeNum.text = data.award_amount + "";
            m_btnBlueOne.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[1], 1) + "";
            m_btnBlueTen.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[1], 10) + "";
            m_dailyLimitText2.text = daily_buy_cnt;
            if (data.time != 0)
            {
                //抽奖展示获得
                // OnUpdateLotteryResult(data);
                OnUpdateLotteryResult(data, true);
                /*UIEffect.ShowEffect(m_sprizeArray[1].transform, EffectConst.UI_JUNHUOKU_XULI, 2f, (obj) =>
                {
                    OnUpdateLotteryResult(data);
                }, null);*/
            }
        }
        else if (data.lottery_type == LotteryType[2])
        {//紫晶
            m_protoData[2] = data;
            //txt_pointSprizeNum3.text = data.point + "";
            TDLotteryExchange cfg = ConfigManager.GetConfig<ConfigLotteryExchange>().GetLine(3) as TDLotteryExchange;
            if (cfg != null)
            {
                txt_pointExchange3.text = data.point + "/" + cfg.ExchangePoints + "兑换";
                var cfgs = ItemDataManager.GetItem(cfg.ITEM);
            }
            m_btnPurpleOne.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[2], 1) + "";
            m_btnPurpleTen.transform.Find("TxtCost").GetComponent<Text>().text = getTypeCostByTime(LotteryType[2], 10) + "";
            m_dailyLimitText3.text = daily_buy_cnt;
            //Util.SetGoGrayShader(m_btnPurpleOne, data.his_items.Length == 12);
            if (data.time != 0)
            {
                //抽奖展示获得
                OnUpdateLotteryResult(data, true);
//                showItemTurning(data.items[0].ID);
            }
            else
            {
                updateHasGot();
            }
        }
        

    }


}
