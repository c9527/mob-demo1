﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetExchangeCodeData {

    public static void SndExchangeCode(string exchange_code)
    {
        tos_player_use_exchange_code proto = new tos_player_use_exchange_code();
        proto.exchange_code = exchange_code;
        NetLayer.Send(proto);
    }
	
}
