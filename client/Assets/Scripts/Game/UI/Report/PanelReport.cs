﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：举报界面
//创建者：hwl
//创建日期: 2016/3/02
///////////////////////////////////////////
public class PanelReport : BasePanel
{
    private Text m_playerNameText;
    private Button m_reportTypeBtn;
    private ItemDropInfo[] m_report_type_data;
    private Text m_reportTypeText;
    private Text m_contentText;
    private InputField m_InputField;
    private int m_reportType = 0;
    private long m_uid;


    public PanelReport()
    {
        SetPanelPrefabPath("UI/Report/PanelReport");
        AddPreLoadRes("UI/Report/PanelReport", EResType.UI);
    }

   
    public override void Init()
    {
        m_report_type_data = new ItemDropInfo[] {
            new ItemDropInfo { type = "REPORT", subtype = "1", label = "谩骂玩家" }, 
            new ItemDropInfo { type = "REPORT", subtype = "2", label = "使用非法程序" },
            new ItemDropInfo { type = "REPORT", subtype = "3", label = "恶意利用游戏漏洞" }, 
            new ItemDropInfo { type = "REPORT", subtype = "4", label = "挂机消极对战" }
        };
        m_reportTypeBtn = m_tran.Find("ReportTypeBtn").GetComponent<Button>();
        m_playerNameText = m_tran.Find("TextInfo/PlayerNameText").GetComponent<Text>();
        m_reportTypeText = m_reportTypeBtn.transform.Find("Text").GetComponent<Text>();
        m_contentText = m_tran.transform.Find("InputField/Text").GetComponent<Text>();
        m_InputField = m_tran.transform.Find("InputField").GetComponent<InputField>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Send")).onPointerClick += SendReport;
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("ReportTypeBtn")).onPointerClick += showDropList;
    }

    /// <summary>
    /// 发送报告
    /// </summary>
    /// <param name="target"></param>
    /// <param name="data"></param>
    private void SendReport( GameObject target,PointerEventData data )
    {
        if (m_reportTypeText.text == "选择举报类型：")
        {
            TipsManager.Instance.showTips("请选择举报的类型");
            return;
        }
        if (m_uid != 0)
            NetLayer.Send(new tos_player_report() { be_report_id = m_uid, report_type = m_reportType, content = m_contentText.text, be_report_name = m_playerNameText.text, proof = "", proof2 = "", proof3 = "" });
        UIManager.ShowTipPanel("举报已发往议事大厅。谢谢");
        this.HidePanel();
        
    }

    /// <summary>
    /// 点击选择举报类型
    /// </summary>
    /// <param name="target"></param>
    /// <param name="data"></param>
    private void showDropList(GameObject target, PointerEventData data)
    {
        UIManager.ShowDropList(m_report_type_data, OnSelectReportType, m_tran, target, 180, 50);
    }

    private void OnSelectReportType(ItemDropInfo vo = null)
    {
        m_reportTypeText.text = vo.label;
        m_reportType = int.Parse(vo.subtype);
    }

    public override void OnShow()
    {
        m_InputField.text = "";
        m_reportTypeText.text = "选择举报类型：";
        if(HasParams())
        {
            m_playerNameText.text = m_params[0].ToString();
            m_uid = (long)m_params[1];
        }
    }

    public override void Update()
    {

    }

    public override void OnHide()
    {
        m_playerNameText.text = "";
        m_contentText.text = "";
        m_reportType = 0;
        base.HidePanel();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }
}
