﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;


    public class OnlineClock
    {
        public enum ClockType
        {
            CLK_TEXT = 1,           //> 文字时钟
            CLK_IMAGE = 2,          //> 图像时钟
        }
        private GameObject m_goClock;
        private Image m_minute1;
        private Image m_minute2;
        private Image m_second1;
        private Image m_second2;
        private Image m_hour1;
        private Image m_point;
        private int m_minute;
        private int m_second;
        private int m_hour;
        private float m_starttime;
        private int m_resttime;
        private Image m_midepoint;
        private bool m_isHour;
        private int m_needtime;
        private bool m_send;

        private Text m_txtClock;
        private ClockType m_clkType = ClockType.CLK_TEXT;
        private GameObject m_bubble;
        public void SetClockObj( GameObject go ,ClockType cType )
        {
            m_goClock = go;

            m_isHour = false;
            m_starttime = -1f;
            m_resttime = -1;
            m_minute = 0;
            m_second = 0;
            m_hour = 0;
            m_needtime = 0;
            m_clkType = cType;

            if( m_clkType == ClockType.CLK_TEXT )
            {
                m_txtClock = m_goClock.transform.Find("time/clock").GetComponent<Text>();
            }
            else
            {
                m_minute1 = m_goClock.transform.Find("minute1").GetComponent<Image>();
                m_minute2 = m_goClock.transform.Find("minute2").GetComponent<Image>();
                m_second1 = m_goClock.transform.Find("second1").GetComponent<Image>();
                m_second2 = m_goClock.transform.Find("second2").GetComponent<Image>();
                m_hour1 = m_goClock.transform.Find("hour").GetComponent<Image>();
                m_point = m_goClock.transform.Find("point2").GetComponent<Image>();
                m_midepoint = m_goClock.transform.Find("point").GetComponent<Image>();
            }
            Transform b = m_goClock.transform.Find("bubble_tip");
            m_bubble = b != null ? b.gameObject : null;
            m_send = true;
        }

        public void Update()
        {
            if (m_starttime < 0)
            {
                return;
            }
            else
            {
                int i = Convert.ToInt32(Time.realtimeSinceStartup - m_starttime);
                int time = m_resttime - i;
                if (time == 0 && m_send)
                {
                    m_send = false;
                    TimerManager.SetTimeOut(1, () => NetLayer.Send(new tos_player_welfare_data()));
                }
                if (time < 0)
                {
                    return;
                }
                m_hour = time / 3600;
                m_minute = time / 60 - m_hour * 60;
                m_second = time % 60;
                SetTime();
            }
        }

        public bool UpdateView( p_welfare_data_toc data )
        {
            m_starttime = Time.realtimeSinceStartup;
            //var topShowIndex = -1;
            var welfareData = data.welfare_list;
            bool bAllGained = true;

            var configWelfareReward = ConfigManager.GetConfig<ConfigWelfareReward>();
            m_needtime = -1;
            for (int i = 0; i < welfareData.Length; i++)
            {
                var cfg = configWelfareReward.GetLine(i + 1);
                if (cfg == null || cfg.RewardCond != "online")
                    continue;
                if ((PanelSubWelfare.WelfareState)welfareData[i] != PanelSubWelfare.WelfareState.Gained)
                    bAllGained = false;

                switch ((PanelSubWelfare.WelfareState)welfareData[i])
                {
                    case PanelSubWelfare.WelfareState.Uncomplete:
                        if (m_needtime < 0)
                            m_needtime = cfg.CondParam;
                        break;
                    case PanelSubWelfare.WelfareState.Complete:
                        m_needtime = 0;
                        break;
                    }
            }

            int time = data.total_online_time - data.last_online_time;
            time = m_needtime - time;
            if (time <= 0)
            {
                time = 0;
            }
            m_resttime = time;
            m_hour = time / 3600;
            m_minute = time / 60 - m_hour  * 60;
            m_second = time % 60;
            SetTime();

            if(bAllGained)
                return false;
            else
                return true;
        }

        public void SetTime()
        {
            if (m_hour > 0)
            {
                m_isHour = true;
            }
            else
            {
                m_isHour = false;
            }
            if( m_clkType == ClockType.CLK_TEXT )
            {
                string time = "";
                if (m_hour > 0)
                    time = time + m_hour.ToString() + ":";

                if (m_minute >= 10)
                    time = time + m_minute.ToString() + ":";
                else
                    time = time + "0" + m_minute.ToString() + ":";

                if(m_second >= 10)
                    time = time + m_second.ToString();
                else
                    time = time + "0" + m_second.ToString();

                if (m_hour == 0 && m_minute == 0 && m_second == 0)
                {
                    m_txtClock.text = "可领取";
                    if (m_bubble != null)
                        m_bubble.TrySetActive(true);
                }
                else
                {
                    m_txtClock.text = time;
                    //if (m_bubble != null)
                    //    m_bubble.TrySetActive(false);
                }
                    
            }
            else
            {
                SetTimeUI();
                m_hour1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_hour).ToString()));
                m_minute1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_minute / 10).ToString()));
                m_minute2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_minute % 10).ToString()));
                m_second1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_second / 10).ToString()));
                m_second2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_second % 10).ToString()));
            }
           
        }

        private void SetTimeUI()
        {
            if (m_isHour)
            {
                m_hour1.gameObject.TrySetActive(true);
                m_point.gameObject.TrySetActive(true);
                if (m_midepoint.transform.localPosition.x == 0)
                {
                    m_second1.transform.localPosition += new Vector3(24, 0, 0);
                    m_second2.transform.localPosition += new Vector3(24, 0, 0);
                    m_minute1.transform.localPosition += new Vector3(24, 0, 0);
                    m_minute2.transform.localPosition += new Vector3(24, 0, 0);
                    m_midepoint.transform.localPosition += new Vector3(24, 0, 0);
                }
            }
            else
            {
                m_hour1.gameObject.TrySetActive(false);
                m_point.gameObject.TrySetActive(false);
                if (m_midepoint.transform.localPosition.x == 0)
                {

                }
                else
                {
                    m_second1.transform.localPosition -= new Vector3(24, 0, 0);
                    m_second2.transform.localPosition -= new Vector3(24, 0, 0);
                    m_minute1.transform.localPosition -= new Vector3(24, 0, 0);
                    m_minute2.transform.localPosition -= new Vector3(24, 0, 0);
                    m_midepoint.transform.localPosition -= new Vector3(24, 0, 0);
                }
            }
        }

    }
public class PanelOnlineReward : BasePanel
{
    private DataGrid m_dataGrid;
    private OnlineClock m_onlineClock;
    public PanelOnlineReward()
    {
        SetPanelPrefabPath("UI/Hall/PanelOnlineReward");
        AddPreLoadRes("UI/Hall/PanelOnlineReward", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    public override void Init()
    {
        m_dataGrid = m_tran.Find("frame/itemlist/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("frame/itemlist/content/item").gameObject, typeof(PanelSubWelfare.ItemWelfareRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useClickEvent = false;
        m_dataGrid.useLoopItems = true;

        m_onlineClock = new OnlineClock();
        m_onlineClock.SetClockObj(m_tran.Find("frame/clock").gameObject,OnlineClock.ClockType.CLK_IMAGE);
    }
   
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += OnClkClose;
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_player_welfare_data());
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {
        m_onlineClock.Update();
    }

    public void UpdateView(p_welfare_data_toc data)
    {
        //m_starttime = Time.time;
        var topShowIndex = -1;
        var welfareData = data.welfare_list;
        var dataInfos = new List<PanelSubWelfare.ItemWelfareInfo>();

        //获取当前Tag类型的配置数据
        var configWelfare = ConfigManager.GetConfig<ConfigWelfareReward>();
        for (int i = 0; i < configWelfare.m_dataArr.Length; ++i)
        {
            var configLine = configWelfare.m_dataArr[i];
            if (configLine.Tag == PanelSubWelfare.WelfareTag.Online)
            {
                var info = new PanelSubWelfare.ItemWelfareInfo();
                info.m_config = configLine;
                info.m_state = (PanelSubWelfare.WelfareState)welfareData[i];
                dataInfos.Add(info);

                if (topShowIndex == -1 && (info.m_state == PanelSubWelfare.WelfareState.Complete || info.m_state == PanelSubWelfare.WelfareState.Uncomplete))
                    topShowIndex = dataInfos.Count - 1;
            }
        }

        m_dataGrid.Data = dataInfos.ToArray();
        m_dataGrid.ShowItemOnTop(topShowIndex);
        if (!m_onlineClock.UpdateView(data))
            HidePanel();
    }

    private void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }
}
