﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using Random = UnityEngine.Random;


class PanelChannelList : BasePanel
{
    public class ItemChannel : ItemRender
    {
        public Text m_name;
        public Image m_bar;
        public float m_per;
        public Image m_state;
        ConfigChannel m_cc;
        Text m_statetxt;
        Image m_bg;
        public override void Awake()
        {
            m_name = transform.Find("name").GetComponent<Text>();
            m_bar = transform.Find("processbar/bar").GetComponent<Image>();
            m_state = transform.Find("state").GetComponent<Image>();
            m_statetxt = transform.Find("state/Text").GetComponent<Text>();
            m_bg = transform.Find("").GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            m_cc = ConfigManager.GetConfig<ConfigChannel>();
            var info = data as p_channel_info;
            m_name.text = m_cc.GetLine(info.id).Name;


            if (m_cc.GetLine(info.id).IsPlatformEnable())
                Util.SetNormalShader(m_bg);
            else
                Util.SetGrayShader(m_bg);

            m_bar.fillAmount = 0f;
            //m_bar.fillAmount = 0.8f;
            m_per = (float)(info.player_cnt) / 1000f;
            if (info.player_cnt < 800)
            {
                m_bar.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "normalbar"));
                m_state.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "normalpoint"));
                m_statetxt.text = "流畅";
                m_statetxt.color = new Color(0, 1, 0);
            }
            else if (info.player_cnt >= 800 && info.player_cnt < 1000)
            {
                m_bar.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "yongjipoint"));
                m_state.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "yongjipoint"));
                m_statetxt.text = "拥挤";
                m_statetxt.color = new Color(255f / 255f, 111f / 255f, 33f / 255f);
            }
            else
            {
                m_bar.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "baomanbar"));
                m_state.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "baomanpoint"));
                m_statetxt.text = "爆满";
                m_statetxt.color = new Color(255f / 255f, 0f, 0f);
            }
        }

        public void SetBar(float per)
        {
            m_bar.fillAmount = per;
        }
    }

    class ItemPageChannel : ItemRender
    {
        Image m_img;
        int m_pageid;
        public override void Awake()
        {
            m_img = transform.GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            
        }

        public void SetPageId(int id)
        {
            m_pageid = id;
        }

        public void IsPageNow(int id)
        {
            if (id == m_pageid)
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "pagenow"));
            }
            else
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "pagetip"));
            }
        }
    }

    DataGrid m_channellist;
    GameObject m_roller;
    GameObject m_pagecontent;
    int m_channelnum;
    ConfigChannel m_cc;
    int m_pagenum;
    GameObject m_itempage;
    List<ItemPageChannel> m_pages;
    public PanelChannelList()
    {
        SetPanelPrefabPath("UI/Room/PanelChannelList");
        AddPreLoadRes("UI/Room/PanelChannelList", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    public override void Init()
    {
        m_pagenum = 0;
        m_roller = m_tran.Find("channellist").gameObject;
        m_channellist = m_tran.Find("channellist/content").gameObject.AddComponent<DataGrid>();
        m_channellist.SetItemRender(m_tran.Find("ItemChannel").gameObject, typeof(ItemChannel));
        m_channellist.autoSelectFirst = false;
        m_cc = ConfigManager.GetConfig<ConfigChannel>();
        m_pagecontent = m_tran.Find("pagetip").gameObject;
        m_itempage = m_tran.Find("pagetip/ItemPage").gameObject;
        m_pages = new List<ItemPageChannel>();
    }

    public override void InitEvent()
    {
        m_channellist.onItemSelected += goToRoomList;
        UGUIClickHandler.Get(m_tran.Find("next").gameObject).onPointerClick += Next;
        UGUIClickHandler.Get(m_tran.Find("pre").gameObject).onPointerClick += Pre;
    }


    void Next(GameObject target, PointerEventData data)
    {
        //if (m_channellist.horizonPos + 1f / m_channelnum < 1)
        //{
        //    m_channellist.horizonPos += 1f / (float)m_channelnum;
        //}
        //else
        //{
        //    m_channellist.horizonPos = 1;
        //}
    }

    void Pre(GameObject target, PointerEventData data)
    {
        //if (m_channellist.horizonPos - 1f / m_channelnum > 0)
        //{
        //    m_channellist.horizonPos -= 1f /(float)m_channelnum;
        //}
        //else
        //{
        //    m_channellist.horizonPos = 0;
        //}
    }
    void goToRoomList(object renderData)
    {
        var channel = renderData as p_channel_info;
        if (!m_cc.GetLine(channel.id).IsPlatformEnable())
        {
            TipsManager.Instance.showTips("该频道不允许当前运行平台进入");
            return;
        }
        if (channel.player_cnt >= channel.player_max)
        {
            TipsManager.Instance.showTips("该频道已满，请选择其他频道");
            return;
        }

        if (Driver.IsNewServer() || m_cc.GetLine(channel.id).LevelMin <= PlayerSystem.roleData.level && m_cc.GetLine(channel.id).LevelMax >= PlayerSystem.roleData.level)
        {
            RoomModel.m_channel = channel.id;
            this.HidePanel();
            if (m_cc.GetLine(channel.id).Type == ChannelTypeEnum.pvp)
            {
                UIManager.ShowPanel<PanelPvpRoomList>();
            }
            else if (m_cc.GetLine(channel.id).Type == ChannelTypeEnum.survival)
            {
                UIManager.ShowPanel<PanelSurvivalRoomList>();
            }
        }
        else 
        {
            TipsManager.Instance.showTips("你的等级不能进入该频道");
            return;
        }
    }

    public override void OnBack()
    {
        this.HidePanel();
        UIManager.ShowPanel<PanelPvpRoomList>();
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnShow()
    {
        for (int i = 0; i < m_pages.Count;i++ )
        {
            GameObject.Destroy(m_pages[i].gameObject);
        }
        m_pages.Clear();


        if (m_params != null)
        {
            m_channelnum = 0;
            var channelList = new List<p_channel_info>();
            for (int i = 0; i < m_params.Length; i++)
            {
                var pChannel = m_params[i] as p_channel_info;
                var config = m_cc.GetLine(pChannel.id);
                if (config != null && config.Display)
                {
                    channelList.Add(pChannel);
                    m_channelnum++;
                }
            }

            channelList.Sort((a, b) =>
            {
                var aLine = m_cc.GetLine(a.id);
                var bLine = m_cc.GetLine(b.id);
                var aEnable = aLine.IsPlatformEnable();
                var bEnable = bLine.IsPlatformEnable();
                if (aEnable && !bEnable)
                    return -1;
                else if (!aEnable && bEnable)
                    return 1;
                else if (aLine.SortIndex != bLine.SortIndex)
                    return aLine.SortIndex - bLine.SortIndex;
                return a.id - b.id;
            });




            m_channellist.Data = CheckChannelList(channelList).ToArray();
            m_roller.transform.localScale = new Vector3(1.7f, 1.7f, 1);
            m_channelnum = m_channellist.Data.Length;
            if (m_channelnum % 8 != 0)
            {
                m_pagenum = m_channelnum / 8 + 1;
            }
            else
            {
                m_pagenum = m_channelnum / 8;
            }
            float startx = 0;
            if(m_pagenum%2==0)
            {
                startx = 0 - 56 * m_pagenum / 2;
            }
            else
            {
                startx = 0 - 56 * m_pagenum / 2 - 28;
            }
            for (int i = 0; i < m_pagenum; i++)
            {
                GameObject go = UIHelper.Instantiate(m_itempage) as GameObject;
                go.transform.SetParent(m_pagecontent.transform);
                m_pages.Add(go.AddComponent<ItemPageChannel>());
                go.TrySetActive(true);
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(startx+56f*(float)i,0,0);
                m_pages[i].SetPageId(i);
            }
            m_pages[0].IsPageNow(0);
            m_channellist.horizonPos = 0;
        }
    }

    public override void Update()
    {
        if (m_roller != null)
        {
            if (m_roller.transform.localScale.x > 1) 
            {
                m_roller.transform.localScale -= new Vector3(0.1f, 0.1f, 1f);
                return;
            }
        }
        if (m_channellist.Data!=null)
        {
            List<ItemRender> items = new List<ItemRender>();
            items = m_channellist.ItemRenders;
            for (int i = 0; i < items.Count; i++)
            {
                var channel = items[i] as ItemChannel;
                if (channel.m_bar.fillAmount < channel.m_per)
                {
                    channel.SetBar(channel.m_bar.fillAmount + 0.02f);
                }
                else
                {
                    channel.SetBar(channel.m_per);
                }
            }
        }
        for (int i = 0; i < m_pages.Count; i++)
        {
            m_pages[i].IsPageNow(GetPageNow());
        }

    }

    int GetPageNow()
    {
        float a  = 8f/(float)m_channelnum;
        for (int i = 0; i < m_pagenum; i++)
        {
            if (m_channellist.horizonPos >= 1)
            {
                return m_pagenum - 1;
            }
            if (m_channellist.horizonPos > 0 + a * i && m_channellist.horizonPos < 0 + a * (i + 1))
            {
                return i;
            }
        }
        return 0;
    }


    List<p_channel_info> CheckChannelList(List<p_channel_info> list)
    {
        List<p_channel_info> newlist = new List<p_channel_info>();
        List<p_channel_info> advList = new List<p_channel_info>();
        List<p_channel_info> freeList = new List<p_channel_info>();
        List<p_channel_info> surList = new List<p_channel_info>();
        List<p_channel_info> baseList = new List<p_channel_info>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].subtyp == "new")
            {
                newlist.Add(list[i]);
            }
            else if (list[i].subtyp == "adv")
            {
                advList.Add(list[i]);
            }
            else if (list[i].subtyp == "free")
            {
                freeList.Add(list[i]);
            }
            else if (list[i].typ == "survival")
            {
                surList.Add(list[i]);
            }
            var config = ConfigManager.GetConfig<ConfigChannel>().GetLine(list[i].id);
            if (config.isBase)
            {
                baseList.Add(list[i]);
            }

        }
        if (PlayerSystem.roleData.level < 15)
        {
            List<p_channel_info> showlist = new List<p_channel_info>();
            showlist.Add(advList[0]);
            showlist.AddRange(CheckChannelPlayer(newlist));
            showlist.AddRange(CheckChannelPlayer(freeList));
            showlist.Add(surList[0]);
            for (int i = 0; i < baseList.Count; i++)
            {
                if(!showlist.Contains(baseList[i]))
                {
                    showlist.Add(baseList[i]);
                }
            }
            return showlist;
        }
        else
        {
            List<p_channel_info> showlist = new List<p_channel_info>();
            showlist.AddRange(CheckChannelPlayer(advList));
            showlist.Add(newlist[0]);
            showlist.AddRange(CheckChannelPlayer(freeList));
            showlist.Add(surList[0]);
            for (int i = 0; i < baseList.Count; i++)
            {
                if (!showlist.Contains(baseList[i]))
                {
                    showlist.Add(baseList[i]);
                }
            }
            return showlist;
        }


    }

    List<p_channel_info> CheckChannelPlayer(List<p_channel_info> list)
    {
        List<p_channel_info> showlist = new List<p_channel_info>();
        bool hasempty = false;
        p_channel_info empty = new p_channel_info();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].player_cnt == 0&&!hasempty)
            {
                empty = list[i];
                hasempty = true;
            }
            if (list[i].player_cnt > 0)
            {
                showlist.Add(list[i]);
            }
        }

        bool isallfull = true;
        for (int i = 0; i < showlist.Count; i++)
        {
            if (showlist[i].player_max > showlist[i].player_cnt)
            {
                isallfull = false;
            }
        }

        if (isallfull)
        {
            showlist.Add(empty);
        }



        return showlist;
    }


}

