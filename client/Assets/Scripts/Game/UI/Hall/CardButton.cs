﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;


enum location
    {
        middle = 0,
        left = 1,
        right = 2,
        back = 3
    };
class CardButton
{
    public Vector2[] m_position;
    private location m_loc;
    private Button m_button;
    public Transform m_tran;
    public Image m_img;
    private Image m_imgRole;
    private Image m_imgWord;
    private Image m_imgLock;
    private string m_name;
    private bool m_canMove;
    public location m_lastloc;
    public RectTransform m_rtf;
    public Rect m_rect;
    private UIEffect m_effectCard;
    //private UIEffect m_effectRole;

    private bool m_enable = true;
    public bool Enable
    {
        set
        {
            m_enable = value;
            if (m_enable)
            {
                if (m_loc == location.middle)
                    ShowEffect();
                if (m_img)
                    Util.SetNormalShader(m_img);
                if (m_imgLock)
                    m_imgLock.gameObject.TrySetActive(false);
            }
            else
            {
                if (m_effectCard != null)
                    m_effectCard.Hide();
                if (m_img)
                    Util.SetGrayShader(m_img);
                if (m_imgLock)
                    m_imgLock.gameObject.TrySetActive(true);
            }
        }
        get { return m_enable; }
    }

    public CardButton()
    {
        m_button = null;
        m_loc = location.middle;
        m_position = new Vector2[4];
        m_position[0] = new Vector2(-235, 148);
        m_position[1] = new Vector2(-383, 203);
        m_position[2] = new Vector2(-90, 215);
        m_position[3] = new Vector2(-235, 250);
        m_canMove = false;
    }


    public void Init(Button btn,string name)
    {
        m_button = btn;
        m_tran = btn.transform;
        m_tran.gameObject.AddComponent<SortingOrderRenderer>();
        m_img = btn.GetComponent<Image>();
        m_imgRole = m_tran.Find("role").GetComponent<Image>();
        m_imgWord = m_tran.Find("word").GetComponent<Image>();
        m_imgLock = m_tran.Find("lock").GetComponent<Image>();
        SetName(name);

        if (Enable)
            Util.SetNormalShader(m_img);
        else
            Util.SetGrayShader(m_img);

        m_imgLock.gameObject.TrySetActive(!Enable);
    }

    public void SetName(string name)
    {
        m_name = name;
    }

    public string GetName()
    {
        return m_name;
    }

    public void SetLocation(location loc)
    {
        m_loc = loc;
        m_lastloc = loc;
        if (loc != location.middle)
        {
            SetColor(0.5f);
        }
        m_tran.GetComponent<RectTransform>().anchoredPosition = m_position[CheckLoc()];
    }

    private void SetColor(float alpha)
    {
        m_img.color = new Color(1.0f, 1.0f, 1.0f, alpha);
        m_imgRole.color = new Color(1.0f, 1.0f, 1.0f, alpha);
        m_imgWord.color = new Color(1.0f, 1.0f, 1.0f, alpha);
    }

    public location GetLocation()
    {
        return m_loc;
    }

    public void SetButtonByLoc()
    {
        m_lastloc = m_loc;
        if (CheckLoc()!= -1)
        {
            m_rtf.anchoredPosition = m_position[CheckLoc()];
        }
        if (CheckLoc() == 0)
        {
            m_rtf.SetAsLastSibling();
            SetColor(1.0f);
        }
        else
        {
            SetColor(0.5f);
        }

        if (m_loc != location.middle)
        {
//            m_img.SetSprite(ResourceManager.LoadSprite("Hall",m_name+"behind"));
            if (m_effectCard != null)
                m_effectCard.Hide();
            //if (m_effectRole != null)
            //    m_effectRole.Hide();
        }
        else
        {
//            m_img.SetSprite(ResourceManager.LoadSprite("Hall", m_name + "forward"));
//            UIManager.GetPanel<PanelHallBattle>().StartModeEffect();

            ShowEffect();
        }
        m_img.SetNativeSize();
        m_tran.localScale = new Vector3(1.0f, 1.0f, 1.0f);       
    }

    public void ShowEffect()
    {
        if (!Enable)
            return;
        if (m_effectCard == null)
        {
            var effectNameCard = "";
            var effectPosCard = new Vector2();
            if (m_name == "pvp")
            {
                effectNameCard = EffectConst.UI_HALLBATTLE_PVP_NEW;
                effectPosCard = new Vector2(2, -171);
            }
            else if (m_name == "pve")
            {
                effectNameCard = EffectConst.UI_HALLBATTLE_PVE_NEW;
                effectPosCard = new Vector2(1, -177);
            }
            else if (m_name == "crops")
            {
                effectNameCard = EffectConst.UI_HALLBATTLE_CORPSMATCH_NEW;
                effectPosCard = new Vector2(1, -177);
            }
            else
            {
                effectNameCard = EffectConst.UI_HALLBATTLE_RANK_New;
                effectPosCard = new Vector2(-3, -171);
            }
            m_effectCard = UIEffect.ShowEffectAfter(m_tran.Find("role"), effectNameCard, effectPosCard);
        }
        m_effectCard.Play();
        SortingOrderRenderer.RebuildAll();
    }

    public int CheckLoc()
    {
        if (m_loc == location.left)
        {
            return 1;
        }
        else if (m_loc == location.right)
        {
            return 2;
        }
        else if (m_loc == location.middle)
        {
            return 0;
        }
        else if (m_loc == location.back)
        {
            return 3;
        }
        return -1;
    }

    public int checkLastLoc()
    {
        if (m_lastloc == location.left)
        {
            return 1;
        }
        else if (m_lastloc == location.right)
        {
            return 2;
        }
        else if (m_lastloc == location.middle)
        {
            return 0;
        }
        else if (m_lastloc == location.back)
        {
            return 3;
        }
        return -1;
    }

    public void ChangeLoc(location loc)
    {
        if (loc == m_loc)
        {
            m_canMove = false;
        }
        else
        {
            m_lastloc = m_loc;
            m_loc = loc;
            m_canMove = true;
        }
    }

    public void TurnLeft()
    {
        m_canMove = true;
        if (m_loc == location.left)
        {
            m_lastloc = location.left;
            m_loc = location.back;
            return;
        }

        if (m_loc == location.right)
        {
            m_lastloc = location.right;
            m_loc = location.middle;
            return;
        }

        if (m_loc == location.middle)
        {
            m_lastloc = location.middle;
            m_loc = location.left;
            return;
        }
        if (m_loc == location.back)
        {
            m_lastloc = location.back;
            m_loc = location.right;
            return;
        }
       
    }

    public void TurnRight()
    {
        m_canMove = true;
        if (m_loc == location.left)
        {
            m_lastloc = location.left;
            m_loc = location.middle;
            return;
        }

        if (m_loc == location.right)
        {
            m_lastloc = location.right;
            m_loc = location.back;
            return;
        }

        if (m_loc == location.middle)
        {
            m_lastloc = location.middle;
            m_loc = location.right;
            return;
        }
        if (m_loc == location.back)
        {
            m_lastloc = location.back;
            m_loc = location.left;
            return;
        }
        
    }

    public bool CheckCanMove()
    {
        if (m_loc == m_lastloc)
        {
            return false;
        }
        if (m_loc == location.middle && m_lastloc == location.left)
        {
            if (m_rtf.anchoredPosition.x >= m_position[0].x && m_rtf.anchoredPosition.y <= m_position[0].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.middle && m_lastloc == location.right)
        {
            if (m_rtf.anchoredPosition.x <= m_position[0].x && m_rtf.anchoredPosition.y <= m_position[0].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.left && m_lastloc == location.middle)
        {
            if (m_rtf.anchoredPosition.x <= m_position[1].x && m_rtf.anchoredPosition.y >= m_position[1].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.left && m_lastloc == location.back)
        {
            if (m_rtf.anchoredPosition.x <= m_position[1].x && m_rtf.anchoredPosition.y <= m_position[1].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.back && m_lastloc == location.left)
        {
            if (m_rtf.anchoredPosition.x >= m_position[3].x && m_rtf.anchoredPosition.y >= m_position[3].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }

        if (m_loc == location.back && m_lastloc == location.right)
        {
            if (m_rtf.anchoredPosition.x <= m_position[3].x && m_rtf.anchoredPosition.y >= m_position[3].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.right && m_lastloc == location.middle)
        {
            if (m_rtf.anchoredPosition.x >= m_position[2].x && m_rtf.anchoredPosition.y >= m_position[2].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }
        if (m_loc == location.right && m_lastloc == location.back)
        {
            if (m_rtf.anchoredPosition.x >= m_position[2].x && m_rtf.anchoredPosition.y <= m_position[2].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }

        /*if (m_loc == location.right)
        {
            if (m_rtf.anchoredPosition.x >= m_position[2].x && m_rtf.anchoredPosition.y >= m_position[2].y)
            {
                m_canMove = false;
                SetButtonByLoc();
                m_lastloc = m_loc;
            }
            return m_canMove;
        }*/
        return m_canMove;
    }

    public void Dispose()
    {
        if (m_effectCard != null)
            m_effectCard.Destroy();
        m_effectCard = null;

        //if (m_effectRole != null)
        //    m_effectRole.Destroy();
        //m_effectRole = null;
    }
}

