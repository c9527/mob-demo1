﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

class CardMenu
{
    public CardButton m_pvpBtn;
    public CardButton m_pveBtn;
    public CardButton m_rankBtn;
    public CardButton m_cropBtn;
    Vector2[] m_dirMoveLeft;
    Vector2[] m_dirMoveRight;
    public int m_subIndex;
    public CardMenu(){
        m_pvpBtn = new CardButton();
        m_pveBtn = new CardButton();
        m_rankBtn = new CardButton();
        m_cropBtn = new CardButton();
        m_dirMoveLeft = new Vector2[4];
        m_dirMoveRight = new Vector2[4];
    }

    public void InitRect(RectTransform rtf1, RectTransform rtf2, RectTransform rtf3, RectTransform rtf4)
    {
        m_pvpBtn.m_rtf = rtf1;
        m_pveBtn.m_rtf = rtf2;
        m_rankBtn.m_rtf = rtf3;
        m_cropBtn.m_rtf = rtf4;
      
        m_pvpBtn.m_rect = rtf1.rect;
        m_pveBtn.m_rect = rtf2.rect;
        m_rankBtn.m_rect = rtf3.rect;
        m_cropBtn.m_rect = rtf4.rect;
        rtf1.SetAsLastSibling();
        //下面的是个大坑不要打开
       // m_cropBtn.m_tran.SetAsFirstSibling();
    }
    public void InitMenu(Button pvp=null,Button pve=null,Button mission=null,Button crops = null)
    {
        m_pvpBtn.Init(pvp,"pvp");
        m_pveBtn.Init(pve,"pve");
        m_rankBtn.Init(mission, "mission");
        m_cropBtn.Init(crops, "crops");
        m_pvpBtn.SetLocation(location.middle);
        m_pvpBtn.ShowEffect();
        m_pveBtn.SetLocation(location.left);
        m_rankBtn.SetLocation(location.right);
        m_cropBtn.SetLocation(location.back);
//        m_pvpBtn.SetPositions(m_pvpBtn.m_tran.localPosition,m_pveBtn.m_tran.localPosition,
//                              m_missionBtn.m_tran.localPosition);
//        m_pveBtn.SetPositions(m_pvpBtn.m_tran.localPosition,m_pveBtn.m_tran.localPosition,
//                              m_missionBtn.m_tran.localPosition);
//        m_missionBtn.SetPositions(m_pvpBtn.m_tran.localPosition, m_pveBtn.m_tran.localPosition,
//                              m_missionBtn.m_tran.localPosition);
//        m_dirMoveLeft[0] = m_pvpBtn.m_position[1] - m_pvpBtn.m_position[0] + new Vector2(5, 10);
        m_dirMoveLeft[0] = m_pvpBtn.m_position[1] - m_pvpBtn.m_position[0];
        m_dirMoveLeft[1] = m_pvpBtn.m_position[3] - m_pvpBtn.m_position[1];
       
//        m_dirMoveLeft[2] = m_pvpBtn.m_position[0] - m_pvpBtn.m_position[2] - new Vector2(17, 15); 
        m_dirMoveLeft[2] = m_pvpBtn.m_position[0] - m_pvpBtn.m_position[2];
        m_dirMoveLeft[3] = m_pvpBtn.m_position[2] - m_pvpBtn.m_position[3];

        m_dirMoveRight[0] = m_pvpBtn.m_position[2] - m_pvpBtn.m_position[0];
//        m_dirMoveRight[1] = m_pvpBtn.m_position[0] - m_pvpBtn.m_position[1] - new Vector2(20, 15);
        m_dirMoveRight[1] = m_pvpBtn.m_position[0] - m_pvpBtn.m_position[1];
        m_dirMoveRight[2] = m_pvpBtn.m_position[3] - m_pvpBtn.m_position[2];
        m_dirMoveRight[3] = m_pvpBtn.m_position[1] - m_pvpBtn.m_position[3];
    }

    public void TurnLeft()
    {
        m_pveBtn.TurnLeft();
        m_pvpBtn.TurnLeft();
        m_rankBtn.TurnLeft();
        m_cropBtn.TurnLeft();
        CheckMove(false);
    }

    public void TurnRight()
    {
        m_pveBtn.TurnRight();
        m_pvpBtn.TurnRight();
        m_rankBtn.TurnRight();
        m_cropBtn.TurnRight();
        CheckMove(true);
    }

    public void MoveRight(float time)
    {
        if (m_pvpBtn.CheckCanMove())
        {
            TurnScale(m_pvpBtn);
            m_pvpBtn.m_tran.Translate(m_dirMoveRight[m_pvpBtn.checkLastLoc()] * time/10.0f);
            m_pvpBtn.CheckCanMove();
        }
        if (m_pveBtn.CheckCanMove())
        {
            TurnScale(m_pveBtn);
            m_pveBtn.m_tran.Translate(m_dirMoveRight[m_pveBtn.checkLastLoc()] * time / 10.0f);
            m_pveBtn.CheckCanMove();
        }
        if (m_rankBtn.CheckCanMove())
        {
            TurnScale(m_rankBtn);
            m_rankBtn.m_tran.Translate(m_dirMoveRight[m_rankBtn.checkLastLoc()] * time / 10.0f);
            m_rankBtn.CheckCanMove();
        }
        if (m_cropBtn.CheckCanMove())
        {
            TurnScale(m_cropBtn);
            m_cropBtn.m_tran.Translate(m_dirMoveRight[m_cropBtn.checkLastLoc()] * time / 10.0f);
            m_cropBtn.CheckCanMove();
        }
    }

    public void TurnScale(CardButton button)
    {
//        if (button.checkLastLoc() == 0)
//        {
//            button.m_tran.localScale = new Vector3(button.m_tran.localScale.x - 0.02f,
//                                                   button.m_tran.localScale.y - 0.009f, 1);
//        }
//        if (button.CheckLoc() == 0)
//        {
//            button.m_tran.localScale = new Vector3(button.m_tran.localScale.x + 0.02f,
//                                                   button.m_tran.localScale.y + 0.015f, 1);
//        }
//        if (button.CheckLoc() == 2)
//        {
//            float a = button.m_img.color.a;
//            button.m_img.color = new Color(1.0f, 1.0f, 1.0f, a - 0.05f);
//        }
//        if (button.checkLastLoc() == 2)
//        {
//            float a = button.m_img.color.a;
//            button.m_img.color = new Color(1.0f, 1.0f, 1.0f, a + 0.05f);
//        }
    }

    public void MoveLeft(float time)
    {
        if (m_pvpBtn.CheckCanMove())
        {
            TurnScale(m_pvpBtn);
            m_pvpBtn.m_tran.Translate(m_dirMoveLeft[m_pvpBtn.checkLastLoc()] * time/10.0f);
            m_pvpBtn.CheckCanMove();
        }
        if (m_pveBtn.CheckCanMove())
        {
            TurnScale(m_pveBtn);
            m_pveBtn.m_tran.Translate(m_dirMoveLeft[m_pveBtn.checkLastLoc()] * time/10.0f);
            m_pveBtn.CheckCanMove();
        }
        if (m_rankBtn.CheckCanMove())
        {
            TurnScale(m_rankBtn);
            m_rankBtn.m_tran.Translate(m_dirMoveLeft[m_rankBtn.checkLastLoc()] * time / 10.0f);
            m_rankBtn.CheckCanMove();
        }
        if (m_cropBtn.CheckCanMove())
        {
            TurnScale(m_cropBtn);
            m_cropBtn.m_tran.Translate(m_dirMoveLeft[m_cropBtn.checkLastLoc()] * time / 10.0f);
            m_cropBtn.CheckCanMove();
        }
    }

    public bool CheckStop()
    {
        if (m_rankBtn.CheckCanMove() || m_pveBtn.CheckCanMove() || m_pvpBtn.CheckCanMove() || m_cropBtn.CheckCanMove())
        {
            return true;
        }
        return false;
    }

    public void CheckMove(bool right)
    {
        CardButton[] cb = new CardButton[4];
        cb[m_pvpBtn.checkLastLoc()] = m_pvpBtn;
        cb[m_pveBtn.checkLastLoc()] = m_pveBtn;
        cb[m_rankBtn.checkLastLoc()] = m_rankBtn;
        cb[m_cropBtn.checkLastLoc()] = m_cropBtn;
        if (right)
        {
            CheckOrderToRight(cb[0], cb[1], cb[2],cb[3]);
        }
        else
        {
            CheckOrderToLeft(cb[0], cb[1], cb[2], cb[3]);
        }
        SortingOrderRenderer.RebuildAll();
    }

    public void CheckOrderToRight(CardButton middle,CardButton left,CardButton right,CardButton back)
    {
        back.m_rtf.SetAsFirstSibling();
        right.m_rtf.SetAsFirstSibling();
        left.m_rtf.SetAsLastSibling();
        m_subIndex = left.m_rtf.GetSiblingIndex();

    }

    public void CheckOrderToLeft(CardButton middle, CardButton left, CardButton right,CardButton back)
    {
        back.m_rtf.SetAsFirstSibling();
        left.m_rtf.SetAsFirstSibling();
        right.m_rtf.SetAsLastSibling();
        m_subIndex = right.m_rtf.GetSiblingIndex();
    }

    public void Dispose()
    {
        if (m_pveBtn != null)
            m_pveBtn.Dispose();
        if (m_pvpBtn != null)
            m_pvpBtn.Dispose();
        if (m_cropBtn != null)
            m_cropBtn.Dispose();
        if (m_rankBtn != null)
            m_rankBtn.Dispose();
    }
}

