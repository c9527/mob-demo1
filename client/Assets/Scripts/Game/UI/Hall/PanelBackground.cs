﻿using UnityEngine.UI;

public class PanelBackground : BasePanel
{
    private string _sprite_name;
    private bool _style_updated;

    private Image m_imgBackground;
    private UIEffect m_effectBg;
    private UIEffect m_effectLightBg;

    public PanelBackground()
    {
        m_isCreateRayCaster = false;

        SetPanelPrefabPath("UI/Hall/PanelBackground");
        AddPreLoadRes("UI/Hall/PanelBackground", EResType.UI);
        AddPreLoadRes("Atlas/Background", EResType.Atlas);
        AddPreLoadRes("Effect/"+EffectConst.UI_HALL_BG_CLOUD, EResType.UIEffect);
        AddPreLoadRes("Effect/"+EffectConst.UI_HALL_BG_LIGHT, EResType.UIEffect);
    }

    public override void Init()
    {
        m_imgBackground = m_tran.Find("imgBackground").GetComponent<Image>();
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        if (m_effectBg == null)
        {
            m_effectBg = UIEffect.ShowEffect(m_tran, EffectConst.UI_ROOM_BG_FOG, true);
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        if (m_effectBg != null)
            m_effectBg.Destroy();
        m_effectBg = null;
    }

    public override void Update()
    {
        if (_style_updated)
        {
            _style_updated = false;
            if (_sprite_name == "hallBg")
            {
                m_effectBg = UIEffect.ShowEffect(m_tran, EffectConst.UI_ROOM_BG_FOG, true);
            }
            else
            {
                m_effectBg = UIEffect.ShowEffect(m_tran, EffectConst.UI_HALL_BG_CLOUD, true);
                //m_effectLightBg = UIEffect.ShowEffect(m_tran, EffectConst.UI_HALL_BG_LIGHT, true);
            }
            m_imgBackground.SetSprite(ResourceManager.LoadSprite(AtlasName.Background, _sprite_name));
        }
        if (ItemDataManager.CheckReNewList())
        {
            if (!UIManager.IsOpen<PanelRenew>())
            {
                if (GuideManager.IsGuideFinish())
                {
                    UIManager.PopChatPanel<PanelRenew>(null, true);
                }
            }
            else
            {
                GameDispatcher.Dispatch("new_renew_item");
            }
        }
        
    }

    public void setupBG(string name = null)
    {
        if (name == null)
            name = "hallBg";
        if (_sprite_name == name)
            return;
        _sprite_name = name;
        _style_updated = true;
        if (m_effectBg != null)
        {
            m_effectBg.Destroy();
        }
        m_effectBg = null;
        if (m_effectLightBg != null)
        {
            m_effectLightBg.Destroy();
        }
        m_effectLightBg = null;
    }
}
