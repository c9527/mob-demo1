﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelModel : BasePanel
{
    private bool m_dragging;
    private RectTransform m_tranDrag;

    public PanelModel()
    {
        SetPanelPrefabPath("UI/Hall/PanelModel");
        AddPreLoadRes("UI/Hall/PanelModel", EResType.UI);
        AddPreLoadRes("Materials/" + GameConst.MAT_HALL_PLAYER, EResType.Material);
        AddPreLoadRes("Materials/" + GameConst.MAT_HALL_WEAPON, EResType.Material);
        AddPreLoadRes("Materials/" + GameConst.MAT_HALL_WEAPON_CRYSTAL, EResType.Material);

        //预加载大厅人物与武器
        var info = PlayerSystem.getEquipRoleInfo((int)WorldManager.CAMP_DEFINE.UNION);
        var roleName = info != null ? info.OPModel + "Hall" : "yazhounan1Hall";
        AddPreLoadRes("Roles/" + roleName, EResType.Battle);
        if (ItemDataManager.m_bags != null && ItemDataManager.m_bags.Length > 0)
        {
            var weapon_info = ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON1) as ConfigItemWeaponLine;
            weapon_info = weapon_info == null ? ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON2) as ConfigItemWeaponLine : weapon_info;
            if (weapon_info != null)
            {
                if (weapon_info.HallModelCheck == 0)
                {
                    AddPreLoadRes("Weapons/" + weapon_info.OPModel, EResType.Battle);
                }
                else
                {
                    AddPreLoadRes("Weapons/" + weapon_info.MPModel, EResType.Battle);
                }
            }
        }
    }

    public override void Init()
    {
        m_parentLayer = LayerType.Root;
        ModelDisplay.Init();

        var roleImage = m_tran.Find("roleImage").GetComponent<RawImage>();
        roleImage.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;
        roleImage.texture = ModelDisplay.RenderTexture;
        roleImage.enabled = true;

        m_tranDrag = m_tran.Find("dragArea").GetComponent<RectTransform>();

        ModelDisplay.Visible = false;
        ModelDisplay.UpdateModel();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tranDrag).onPointerClick += OnRoleClick;
        UGUIDragHandler.Get(m_tranDrag).onDrag += OnRoleDrag;
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        ModelDisplay.Destroy();
        WeaponDisplay.Destroy();
    }

    private LayerType m_parentLayer = LayerType.Root;
    public override void Update()
    {
        var layer = UIManager.IsOpen<PanelHallBattle>() ? LayerType.Content : LayerType.Root;

        if (m_parentLayer != layer)
        {
            m_parentLayer = layer;
            transform.SetParent(UIManager.GetLayer(layer));
            SortingOrderRenderer.RebuildAll();

            m_tranDrag.anchoredPosition = new Vector2(layer == LayerType.Content ? 20 : 0, 0);
            m_tranDrag.sizeDelta = new Vector2(layer == LayerType.Content ? -338 : 0, 0);
        }
    }

    private void OnRoleClick(GameObject target, PointerEventData eventdata)
    {
        if (!ModelDisplay.Visible)
            return;
        if (m_dragging)
        {
            m_dragging = false;
            return;
        }
        ModelDisplay.ChangeCameraPosition();
    }

    private void OnRoleDrag(GameObject target, PointerEventData eventdata)
    {
        if (!ModelDisplay.Visible)
            return;
        m_dragging = true;
        ModelDisplay.Rotation(eventdata.delta.x);
    }
}
