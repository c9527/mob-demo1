﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;


public class PanelHallBattle : BasePanel
{
    public static bool showSevenDay = true;
    private GameObject m_btnQuickStart;
    //private CardMenu m_cardMenu;
    private Vector2 m_downpos;
    private bool m_moveLeft;
    private bool m_moveRight;

    private Toggle m_toggleCampWatcher;
    private Toggle m_toggleCampPatriot;
    

    GameObject m_LoginAwardBubble;
    //GameObject m_SuperArmorBubble;
    private GameObject m_eventBubble;
    private GameObject m_lotteryBubble;
    private GameObject m_enhanceBubble;
    private GameObject m_missionBubble;
    private GameObject m_btnEvent;

    private UIEffect m_effectReward1;
    private UIEffect m_effectReward2;
    //private UIEffect m_effectShouchong;
    private UIEffect m_effect_player;
    private UIEffect m_effect_corps;
    private UIEffect m_effect_legion;
    //private GameObject m_bubbleSevenDay;
    private GameObject m_bubbleNewHand;
    public GameObject imgSevenDay;
    GameObject m_bubbleSevenDay;
    private Transform m_imgRecharge;
    private GameObject m_imgWorldBoss;
    public static GameObject m_imgNewHandWorldBoss;
    private Text m_worldBossText;
    private bool m_worldBossEventOpen = false;
    private UIEffect m_worldBossEffect;
    public static UIEffect m_newHandWorldBossEffect;
    //    UIEffect m_lastUIEffectOfLoginReward;
    //    private UIEffect m_effectActivity;
    //    private UIEffect m_effectRank;
    //    private UIEffect m_effectHasReward;
    //    public int m_sublingIndex;
    //    public bool m_haswelfareReward;
    //    public bool m_signReward;
    //    public bool m_hassign;
    //    public bool m_haslvl;
    //    public bool m_hasonline;

    //private GameObject m_goFirstRecharge;

    private GameObject m_goNewHand;
    private OnlineClock m_onlineClock;
    private GameObject m_goLoginAward;
    private GameObject m_btnPVP;
    private GameObject m_btnPVE;
    private GameObject m_btnRank;
    private GameObject m_btnCorps;
    private GameObject m_btnLegion;
    private GameObject m_lockRank;
    private GameObject m_lockLegion;
    private GameObject m_lockCorps;
    private GameObject m_buddleTipGo;
    private GameObject m_legionPoint;
    private GameObject m_corpsPoint;
    private static GameObject m_limitGiftGo;
    private GameObject m_fightVideoGo;
    public static GameObject m_liveVideoGo;
    private Text m_limitGiftTimeText;
    private Image m_limitGiftIcon;
    public static int m_finishTime;
    public static int m_limitGiftLeftTime = 0;
    public static int m_limitGiftId;
    public static p_timelimit_item[] m_limitGiftArr = new p_timelimit_item[] { };
    bool m_isTime;
    bool m_corpsIstime;
    bool m_legionIstime;

    Image m_corpsIsBattle;
    Image m_legionIsBattle;
    int m_corpStartTime;
    int m_corpEndTime;
    int m_legionStartTime;
    int m_legionEndTime;
    int[] m_legionWeek;
    int[] m_corpsWeek;
    DateTime m_nowTime;
    DateTime m_legionStart;
    DateTime m_legionEnd;
    DateTime m_corpStart;
    DateTime m_corpEnd;
    LotteryNewClock m_lotteryNewClock;
    GameObject m_rewardtime;

    GameObject m_discountTime;
    ConfigEventListLine lotteryEvent;
    public PanelHallBattle()
    {
        SetPanelPrefabPath("UI/Hall/PanelHallBattle");
        AddPreLoadRes("UI/Hall/PanelHallBattle", EResType.UI);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALLBATTLE_PVP_CARD, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALLBATTLE_PVP_ROLE, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALLBATTLE_LEGION_CORPS, EResType.UIEffect);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }


    public override void Init()
    {
        //        m_sublingIndex = 0;
        m_moveRight = false;
        m_moveLeft = false;
        //        m_haslvl = false;
        //        m_hasonline = false;
        m_toggleCampWatcher = m_tran.Find("campBtn/btnCampWatcher").GetComponent<Toggle>();
        m_toggleCampPatriot = m_tran.Find("campBtn/btnCampPatriot").GetComponent<Toggle>();
        //        m_haswelfareReward = false;
        //        m_signReward = false;
        //m_cardMenu = new CardMenu();
        //m_cardMenu.InitMenu(m_tran.Find("gameModeMask/btnPVP").GetComponent<Button>(),
        //                    m_tran.Find("gameModeMask/btnPVE").GetComponent<Button>(),
        //                    m_tran.Find("gameModeMask/btnRank").GetComponent<Button>(),
        //                    m_tran.Find("gameModeMask/btnCrop").GetComponent<Button>());
        //m_cardMenu.InitRect(m_tran.Find("gameModeMask/btnPVP").gameObject.GetRectTransform(),
        //                    m_tran.Find("gameModeMask/btnPVE").gameObject.GetRectTransform(),
        //                    m_tran.Find("gameModeMask/btnRank").gameObject.GetRectTransform(),
        //                     m_tran.Find("gameModeMask/btnCrop").gameObject.GetRectTransform());

        m_btnEvent = m_tran.Find("icon/imgEvent").gameObject;
        m_LoginAwardBubble = m_tran.Find("icon/imgLoginAward/bubble_tip").gameObject;
        //m_SuperArmorBubble = m_tran.Find("icon/imgSuperArmor/bubble_tip").gameObject;
        m_eventBubble = m_tran.Find("icon/imgEvent/bubble_tip").gameObject;
        m_lotteryBubble = m_tran.Find("icon/imgLottery/bubble_tip").gameObject;
        m_enhanceBubble = m_tran.Find("icon/imgEnhance/bubble_tip").gameObject;
        //m_missionBubble = m_tran.Find("icon/imgMission/bubble_tip").gameObject;
        //m_bubbleSevenDay = m_tran.Find("icon/imgSevenDay/bubble_tip").gameObject;
        m_bubbleNewHand = m_tran.Find("imgNewHand/bubble_tip").gameObject;
        imgSevenDay = m_tran.Find("imgSevenDay").gameObject;
        m_bubbleSevenDay = imgSevenDay.transform.Find("bubble_tip").gameObject;
        
        m_imgWorldBoss = m_tran.Find("imgWorldBoss").gameObject;
        m_imgNewHandWorldBoss = m_tran.Find("imgNewHandWorldBoss").gameObject;
        m_worldBossText = m_tran.Find("imgWorldBoss/time/clock").GetComponent<Text>();
        m_imgWorldBoss.TrySetActive(false);
        m_imgNewHandWorldBoss.TrySetActive(false);
        m_worldBossEffect = UIEffect.ShowEffect(m_imgWorldBoss.transform, "UI_boss", new Vector3(-3.28f, 3.55f, 0));
        m_newHandWorldBossEffect = UIEffect.ShowEffect(m_imgNewHandWorldBoss.transform, "UI_boss", new Vector3(-3.28f, 3.55f, 0));
        imgSevenDay.TrySetActive(showSevenDay);
        //m_goFirstRecharge = m_tran.Find("icon/imgFirstRecharge").gameObject;

        m_imgRecharge = m_tran.Find("icon/imgRecharge");
        m_goNewHand = m_tran.Find("imgNewHand").gameObject;
        m_goNewHand.TrySetActive(PlayerMissionData.singleton.isHasNewHandMission());
        //        m_effectHasReward = UIEffect.ShowEffect(m_tran.Find("icon/imgLoginAward"), EffectConst.UI_HALLBATTLE_HASREWARD, new Vector2(0, 0));
        //        m_effectHasReward.Destroy();
        //        m_lastUIEffectOfLoginReward = UIEffect.ShowEffect(m_tran.Find("icon/imgLoginAward"), EffectConst.UI_HALLBATTLE_REWARD, new Vector2(-2, 6));

        //        //强化指引
        //        var level = PlayerSystem.roleData.level;
        //        var mission = PlayerMissionData.singleton.GetMission(10001);
        //        if (level > 1 && level < 10 && mission != null && mission.m_status != SyncMission.MISSION_STATE.DONE)
        //        {
        //            GuideManager.StartChapterName("WeaponDetail");
        //        }

        m_goLoginAward = m_tran.Find("icon/imgLoginAward").gameObject;
        m_onlineClock = new OnlineClock();
        m_onlineClock.SetClockObj(m_goLoginAward, OnlineClock.ClockType.CLK_TEXT);
        m_btnQuickStart = m_tran.Find("btnQuickStart").gameObject;
        m_btnQuickStart.AddComponent<SortingOrderRenderer>().ForceRebuildAll = true;
        m_fightVideoGo = m_tran.Find("btnFightVideo").gameObject;
        m_fightVideoGo.TrySetActive(true);
        m_liveVideoGo = m_tran.Find("btnLiveVideo").gameObject;

        m_limitGiftGo = m_tran.Find("imgLimitGift").gameObject;
        m_limitGiftGo.AddMissingComponent<SortingOrderRenderer>();
        SortingOrderRenderer.RebuildAll();
        m_limitGiftTimeText = m_limitGiftGo.transform.Find("time/clock").GetComponent<Text>();
        m_limitGiftIcon = m_limitGiftGo.transform.Find("icon").GetComponent<Image>();
        
        m_btnPVP = m_tran.Find("btnPVP").gameObject;
        m_btnPVE = m_tran.Find("btnPVE").gameObject;
        m_btnRank = m_tran.Find("btnRank").gameObject;
        m_btnCorps = m_tran.Find("btnCorps").gameObject;
        m_btnLegion = m_tran.Find("btnLegion").gameObject;
        m_corpsPoint = m_tran.Find("btnCorps/effect").gameObject;
        m_legionPoint = m_tran.Find("btnLegion/effect").gameObject;        

        m_lockRank = m_tran.Find("btnRank/lockRank").gameObject;
        m_lockLegion = m_tran.Find("btnLegion/lockRank").gameObject;
        m_lockCorps = m_tran.Find("btnCorps/lockRank").gameObject;
        m_buddleTipGo = m_tran.Find("btnLegion/bubble_tip").gameObject;
        UIEffect.ShowEffect(m_btnQuickStart.transform, EffectConst.UI_KUAISU_KAISHI);
        m_corpsIsBattle = m_tran.Find("btnCorps/IsBattle").GetComponent<Image>();
        m_legionIsBattle = m_tran.Find("btnLegion/IsBattle").GetComponent<Image>();
        m_isTime = false;
        m_corpsIstime = false;
        m_legionIstime = false;

        var cel = ConfigManager.GetConfig<ConfigEventList>();
        var legion = cel.GetLine(1200);
        m_legionWeek = legion.WeekLimit;
        m_legionStartTime = int.Parse(legion.TimeLimit.Split('#')[0]);
        m_legionEndTime = int.Parse(legion.TimeLimit.Split('#')[1]);

        var corps = cel.GetLine(1112);
        m_corpsWeek = new int[7];
        for (int i = 0; i < 7; i++)
        {
            m_corpsWeek[i] = i + 1;
        }
        m_corpStartTime = int.Parse(corps.TimeLimit.Split('#')[0]);
        m_corpEndTime = int.Parse(corps.TimeLimit.Split('#')[1]);
        m_rewardtime = m_tran.Find("icon/imgLoginAward/time").gameObject;


        m_lotteryNewClock = new LotteryNewClock();
        m_lotteryNewClock.setClockGO(m_tran.Find("icon/imgLottery").gameObject);
        CorpsDataManager.IsOpenByHallBattlePanel = true;
        NetLayer.Send(new tos_corpsmatch_score_info());
        var instance = WelfareDataManger.singleton;
        SdkManager.IsSupportChuShouTV();
        m_discountTime = m_tran.Find("icon/imgLottery/discount").gameObject;
    }

    public bool checkIsCorpsTime()
    {
        m_nowTime = TimeUtil.GetNowTime();
        int week = (int)m_nowTime.DayOfWeek;
        if (week == 0)
        {
            week = 7;
        }
        for (int i = 0; i < m_corpsWeek.Length; i++)
        {
            if (week == m_corpsWeek[i])
            {
                m_corpStart = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, m_corpStartTime / 100, m_corpStartTime % 100, 0);
                m_corpEnd = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, m_corpEndTime / 100, m_corpEndTime % 100, 0);
                if (m_nowTime <= m_corpEnd && m_nowTime >= m_corpStart)
                {
                    if (m_effect_corps == null)
                    {
                        m_effect_corps = UIEffect.ShowEffectBefore(m_corpsPoint.transform, EffectConst.UI_HALLBATTLE_LEGION_CORPS, new Vector3(-85, 25, 0));
                    }
                    return true;
                }
            }
        }
        if (m_effect_corps != null)
        {
            m_effect_corps.Destroy();
            m_effect_corps = null;
        }
        return false;
    }

    public bool checkIsLegionTime()
    {
        m_nowTime = TimeUtil.GetNowTime();
        int week = (int)m_nowTime.DayOfWeek;
        if (week == 0)
        {
            week = 7;
        }
        
        for (int i = 0; i < m_legionWeek.Length; i++)
        {
            if (week == m_corpsWeek[i])
            {
                m_legionStart = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, m_legionStartTime / 100, m_legionStartTime % 100, 0);
                m_legionEnd = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, m_legionEndTime / 100, m_legionEndTime % 100, 0);
                if (m_nowTime <= m_legionEnd && m_nowTime >= m_legionStart)
                {
                    if (m_effect_legion == null)
                    {
                        m_effect_legion = UIEffect.ShowEffectBefore(m_legionPoint.transform, EffectConst.UI_HALLBATTLE_LEGION_CORPS, new Vector3(-85, 25, 0));
                    }
                    return true;
                }
            }
        }
        if (m_effect_legion != null)
        {
            m_effect_legion.Destroy();
            m_effect_legion = null;
        }

        return false;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnPVP")).onPointerClick += pointUp;
        UGUIClickHandler.Get(m_tran.Find("btnPVE")).onPointerClick += pointUp;
        UGUIClickHandler.Get(m_tran.Find("btnRank")).onPointerClick += pointUp;
        UGUIClickHandler.Get(m_tran.Find("btnCorps")).onPointerClick += pointUp;
        UGUIClickHandler.Get(m_tran.Find("btnLegion")).onPointerClick += pointUp;

        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnPVP")).onPointerDown += pointDown;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnPVE")).onPointerUp += pointUp;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnPVE")).onPointerDown += pointDown;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnRank")).onPointerUp += pointUp;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnRank")).onPointerDown += pointDown;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnCrop")).onPointerUp += pointUp;
        //UGUIUpDownHandler.Get(m_tran.Find("gameModeMask/btnCrop")).onPointerDown += pointDown;


        UGUIClickHandler.Get(m_tran.Find("icon/imgLoginAward"), AudioConst.activityBtnClick).onPointerClick += OnClickActList;
        //UGUIClickHandler.Get(m_tran.Find("icon/imgSuperArmor"), AudioConst.activityBtnClick).onPointerClick += null;
        UGUIClickHandler.Get(m_tran.Find("icon/imgRankList")).onPointerClick += OnClickRankList;
        UGUIClickHandler.Get(m_tran.Find("icon/imgRecharge")).onPointerClick += OnClickRechargeBtn;
        UGUIClickHandler.Get(m_tran.Find("icon/imgFirstRecharge")).onPointerClick += OnClkFirstRechargeGift;
        //UGUIClickHandler.Get(m_tran.Find("icon/imgAccumulativeRecharge")).onPointerClick += OnClkAccumulativeRechargeGift;
        UGUIClickHandler.Get(m_tran.Find("icon/imgEvent")).onPointerClick += OnClickEvent;
        UGUIClickHandler.Get(m_tran.Find("icon/imgLottery"),AudioConst.lotteryOpen).onPointerClick += OnClickLottery;
        UGUIClickHandler.Get(m_tran.Find("icon/imgEnhance")).onPointerClick += OnClickEnhance;
        //UGUIClickHandler.Get(m_tran.Find("icon/imgMission")).onPointerClick += OnClickMission;
        UGUIClickHandler.Get(m_toggleCampWatcher.gameObject).onPointerClick += OnCampBtnClick;
        UGUIClickHandler.Get(m_toggleCampPatriot.gameObject).onPointerClick += OnCampBtnClick;
        UGUIClickHandler.Get(imgSevenDay).onPointerClick += OnSevenDayClick;
        UGUIClickHandler.Get(m_goNewHand).onPointerClick += OnClickNewHand;
        //UGUIClickHandler.Get(m_tran.Find("icon/imgOnlineReward")).onPointerClick += OnClickOnline;
        UGUIClickHandler.Get(m_tran.Find("imgWorldBoss")).onPointerClick += OnClickWorldBoss;
        UGUIClickHandler.Get(m_tran.Find("imgNewHandWorldBoss")).onPointerClick += OnClickNewHandWorldBoss;
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
        AddEventListener(GameEvent.UI_FIRST_PAY, OnFirstPay);
        AddEventListener(GameEvent.PROXY_PLAYER_DEFAULT_ROLE_UPDATED, ModelDisplay.UpdateModel);
        AddEventListener<int, int>(GameEvent.MAINPLAYER_LEVEL_UP, OnRoleLevelUp);
        UGUIClickHandler.Get(m_limitGiftGo).onPointerClick += OpenLimitGiftPanel;
        m_limitGiftGo.TrySetActive(false);
        UGUIClickHandler.Get(m_fightVideoGo).onPointerClick += OpenFightVideoPanel;
        UGUIClickHandler.Get(m_liveVideoGo).onPointerClick += OpenLiveVideoTV;
        int temp = (int)TimeUtil.GetRemainTime((uint)m_finishTime).TotalSeconds;
        //m_limitGiftLeftTime = temp;
        Logger.Log("______________" + temp);
        if (temp <= 0)
        {
            NetLayer.Send(new tos_player_timelimit_gift_time());
        }

        NetLayer.Send(new tos_player_signin_data());
        //NetLayer.Send(new tos_player_welfare_data());

        NetLayer.Send(new tos_player_first_recharge_data());
        NetLayer.Send(new tos_player_recharge_welfare_list());
        NetLayer.Send(new tos_player_event_list());
        AddEventListener(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED, () =>
        {
            m_goNewHand.TrySetActive(PlayerMissionData.singleton.isHasNewHandMission());
        });

        UGUIClickHandler.Get(m_btnQuickStart).onPointerClick += OnBtnQuickStartClick;
        UGUIClickHandler.Get(m_btnLegion).onPointerClick += LegionWar;
        GameDispatcher.AddEventListener(GameEvent.LEGION_BUBBLE_TIP, updateLegionButtle);
    }



    private void updateLegionButtle()
    {
        if (m_buddleTipGo != null)
        {
            m_buddleTipGo.TrySetActive(LegionDataManager.have_reward);
        }
    }

    void LegionWar(GameObject target, PointerEventData eventData)
    {
        var dt = TimeUtil.GetNowTime();
        if (dt.Year == 2016 && dt.Month <= 4 && dt.Day < 6)
        {
            TipsManager.Instance.showTips("世界争霸将在4月8日开放");
            return;
        }

        if (m_isTime)
        {
            NetLayer.Send(new tos_legionwar_data() { });
            NetLayer.Send(new tos_legionwar_my_info() { });
        }
    }

    private void OnBtnQuickStartClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_quick_join_action());
        UIManager.PopPanel<PanelQuickStart>(null, true);
    }

    private void OnClickWorldBoss(GameObject target, PointerEventData eventData)
    {
        if (m_worldBossEventOpen && PlayerSystem.roleData.level > 8)
        {            
            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.worldboss), stage = 0 });
        }
        else
        {
            DateTime show, open, end;
            PanelWorldBoss.GetWorldBossTime(TimeUtil.GetNowTime(), out show, out open, out end);
            TipsManager.Instance.showTips(string.Format("领主将于{0}：{1}来袭",open.Hour,open.Minute));
        }
    }

    private void OnClickNewHandWorldBoss(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.boss4newer), stage = 0 });
    }

    private void OnRoleLevelUp(int oldLevel, int level)
    {
        //if (!m_cardMenu.m_rankBtn.Enable && PlayerSystem.IsRankMatchOpen())
        //    m_cardMenu.m_rankBtn.Enable = true;
    }

    private void OnSevenDayClick(GameObject target, PointerEventData eventData)
    {
        PanelSevenDayReward.firstOpen = false;
        NetLayer.Send(new tos_player_signin_data());
        UIManager.PopPanel<PanelSevenDayReward>(null, true);
    }

    private void OnClickLottery(GameObject target, PointerEventData eventData)
    {
        //UIManager.ShowPanel<PanelLottery>(null);
        UIManager.ShowPanel<PanelLotteryNew>();
    }

    private void OnClickEnhance(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelStrengthen>();
    }

    //private void OnClickMission(GameObject target, PointerEventData eventData)
    //{
    //    UIManager.ShowPanel<PanelDailyMission>();
    //}

    private void OnClickEvent(GameObject target, PointerEventData eventData)
    {
        //UIManager.ShowPanel<PanelEvent>();
        UIManager.PopPanel<PanelEvent>(null, true, this);
        ///UIManager.PopPanel<PanelMemberEffect>(new object[] { 2 }, true, null);
    }

    private void OnClickNewHand(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelTaskNewHand>(null, true, this);
    }

    private void OnClkFirstRechargeGift(GameObject target, PointerEventData eventData)
    {
        //UIManager.PopPanel<PanelFirstRecharge>(null, true, null);
    }
    private void OnClickRechargeBtn(GameObject target, PointerEventData eventData)
    {
        SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
    }

    private void UpdateBubble()
    {
        var level = BubbleManager.HasBubble(BubbleConst.WelfareLevelReward);
        var online = BubbleManager.HasBubble(BubbleConst.WelfareOnlineReward);
        var sign = BubbleManager.HasBubble(BubbleConst.WelfareSignReward) || BubbleManager.HasBubble(BubbleConst.WelfareLeijiReward);
        var heroCard = BubbleManager.HasBubble(BubbleConst.HeroCardReward);
        var studentapply = BubbleManager.HasBubble(BubbleConst.StudentApply);
        var investment = ConfigManager.GetConfig<ConfigFoundationReward>().GetAllBubble();
        //var mastergift = BubbleManager.HasBubble(BubbleConst.MasterGift);

        if (m_LoginAwardBubble)
            m_LoginAwardBubble.TrySetActive(level || online || sign || heroCard||investment);
        //fjs:主界面福利按钮 小红点剔除师徒礼包
        //m_LoginAwardBubble.TrySetActive(level || online || sign || studentapply || mastergift);

        if (m_eventBubble)
            m_eventBubble.TrySetActive(RechargeGiftManager.HasAccummultiveRechargeGift()
                                || RechargeGiftManager.HasTodayFirstRechargeGift()
                                || EventManager.HasWelfare() || BubbleManager.isActivityFrameHasBubble());
        if (m_lotteryBubble)
        {
            m_lotteryBubble.TrySetActive(false);
            //m_lotteryBubble.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.LotteryFree));
        }
        m_enhanceBubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.StrengthenTrain) || BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade) || BubbleManager.HasBubble(BubbleConst.StrengthenAwaken));
        m_bubbleNewHand.TrySetActive(BubbleManager.HasBubble(BubbleConst.NewHandTask));
        m_bubbleSevenDay.TrySetActive(BubbleManager.HasBubble(BubbleConst.SevenDayReward));
    }

    //    public void SetRewardBubble()
    //    {
    //        
    //        _loginAward_bubble_tip.gameObject.TrySetActive(m_signReward|m_haswelfareReward);
    //        if (m_lastUIEffectOfLoginReward != null)
    //        {
    //            m_lastUIEffectOfLoginReward.Destroy();
    //            m_effectHasReward.Destroy();
    //        }
    //        
    //        if (m_signReward | m_haswelfareReward)
    //        {
    //            m_lastUIEffectOfLoginReward = UIEffect.ShowEffectAfter(m_tran.Find("icon/imgLoginAward"), "UI_denglujiangli01", new Vector2(-16, 10));
    //            m_effectHasReward = UIEffect.ShowEffect(m_tran.Find("icon/imgLoginAward"), "UI_denglujiangli02", new Vector2(0, 0));
    //        }
    //        else
    //        {
    //           
    //            m_lastUIEffectOfLoginReward = UIEffect.ShowEffect(m_tran.Find("icon/imgLoginAward"), EffectConst.UI_HALLBATTLE_REWARD, new Vector2(-2, 6));
    //        }
    //    }

    private void OnCampBtnClick(GameObject target, PointerEventData eventdata)
    {
        if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.UNION)
            ModelDisplay.ModelDefaultCamp = WorldManager.CAMP_DEFINE.REBEL;
        else if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.REBEL)
            ModelDisplay.ModelDefaultCamp = WorldManager.CAMP_DEFINE.UNION;

        UpdateCampIcon();
        ModelDisplay.UpdateModel();
    }

    private void UpdateCampIcon()
    {
        if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.REBEL)
        {
            m_toggleCampWatcher.gameObject.TrySetActive(false);
            m_toggleCampPatriot.gameObject.TrySetActive(true);
        }
        else if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.UNION)
        {
            m_toggleCampWatcher.gameObject.TrySetActive(true);
            m_toggleCampPatriot.gameObject.TrySetActive(false);
        }
    }

    void ToMiddle(CardButton cbtn)
    {
        if (cbtn.GetLocation() == location.left)
        {
            TurnRight();
        }
        else if (cbtn.GetLocation() == location.right)
        {
            TurnLeft();
        }

    }

    void TurnRight()
    {
        if (m_moveLeft != true)
        {
            m_moveRight = true;
            //m_cardMenu.TurnRight();
            //            StopModeEffect();
        }
    }


    void TurnLeft()
    {
        if (m_moveRight != true)
        {
            m_moveLeft = true;
            //m_cardMenu.TurnLeft();
            //            StopModeEffect();
        }
    }

    private void playFunClickEffect(Transform tran)
    {
        if (m_effect_player != null)
        {
            if (m_effect_player.m_goEffect != null && m_effect_player.m_goEffect.transform.parent == tran)
            {
                return;//忽略连续快速点击
            }
            m_effect_player.Destroy();
            m_effect_player = null;
        }
        m_effect_player = UIEffect.ShowEffect(tran, EffectConst.UI_HALL_CLICK, 1f, new Vector2(0, -150));
    }

    void pointUp(GameObject target, PointerEventData eventData)
    {
        if (target.name == "btnPVP")
        {
            AudioManager.PlayUISound(AudioConst.btnClick);
            //NetLayer.Send(new tos_player_channel_list());     
            if (UISystem.isMinimizeRoom)
            {
                UISystem.isMinimizeRoom = false;
                PanelRoomBase.ShowMinimizeRoom();
            }
            else
            {
                 UIManager.GotoPanel("battle");
            }
            
        }
        if (target.name == "btnPVE")
        {
            AudioManager.PlayUISound(AudioConst.btnClick);
            PanelRoomBase.RoomMinimizeCheck(() =>
            {
                UIManager.ShowPanel<PanelPVE>();
            });
        }
        if (target.name == "btnRank")
        {
            if (!PlayerSystem.IsRankMatchOpen())
            {
                TipsManager.Instance.showTips("15级开启排位竞技");
            }
            else
            {
                AudioManager.PlayUISound(AudioConst.btnClick);
                PanelRoomBase.RoomMinimizeCheck(() =>
                {
                    UIManager.GotoPanel("rank");
                });
            }
        }
        if (target.name == "btnCorps")
        {
            AudioManager.PlayUISound(AudioConst.btnClick);
            if (!PlayerSystem.IsCorpsMatchOpen())
            {
                int lvl = ConfigManager.GetConfig<ConfigMisc>().GetLine("join_corps_limit_level").ValueInt;
                TipsManager.Instance.showTips(lvl.ToString()+"级开启战队联赛");
            }
            else
            {
                PanelRoomBase.RoomMinimizeCheck(() =>
                {
                    UIManager.ShowPanel<PanelCorpsEnter>();
                });
            }
        }
        if (target.name == "Legion")
        {
            AudioManager.PlayUISound(AudioConst.btnClick);
            //PanelRoomBase.RoomMinimizeCheck(() =>
            //{

            //});
        }
    }

    void pointDown(GameObject target, PointerEventData eventData)
    {
        m_downpos = eventData.position;
    }

    public override void OnShow()
    {
        ModelDisplay.Visible = true;
        ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.2f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f)});
        //        if (m_effectActivity == null)
        //            m_effectActivity = UIEffect.ShowEffect(m_tran.Find("icon/imgSuperArmor"), EffectConst.UI_HALLBATTLE_ACTIVITY, new Vector2(0f, 5f));
        //        if (m_effectRank != null)
        //            m_effectRank = UIEffect.ShowEffect(m_tran.Find("icon/imgRankList"), EffectConst.UI_HALLBATTLE_RANK, new Vector2(-3f, 6f));

        if (m_effectReward1 == null)
            //m_effectReward1 = UIEffect.ShowEffectAfter(m_tran.Find("icon/imgLoginAward"), "UI_denglujiangli01", new Vector2(-16, 10));
            if (m_effectReward2 == null)
            {
                //m_effectReward2 = UIEffect.ShowEffect(m_tran.Find("icon/imgLoginAward"), "UI_denglujiangli02", new Vector2(0, 0));
            }

        //if (m_effectShouchong == null)
        //    m_effectShouchong = UIEffect.ShowEffect(m_tran.Find("icon/imgFirstRecharge/Image"), EffectConst.UI_FIRST_RECHARGE_ZHUYE, new Vector3(-6, 9));
        //var rankEnable = m_cardMenu.m_rankBtn.Enable;        
        Util.SetGoGrayShader(m_btnRank, !PlayerSystem.IsRankMatchOpen());
        Util.SetGoGrayShader(m_btnCorps, !PlayerSystem.IsCorpsMatchOpen());
        m_lockCorps.TrySetActive(!PlayerSystem.IsCorpsMatchOpen());
        m_lockRank.TrySetActive(!PlayerSystem.IsRankMatchOpen());
        //if (rankEnable && !isRankOpen)
        //    m_cardMenu.m_rankBtn.Enable = false;
        //else if (!rankEnable && isRankOpen)
        //    m_cardMenu.m_rankBtn.Enable = true;

        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
        {
            panelHall.CheckChosen(panelHall.m_btgBattle);
            panelHall.SetNameVisible(true);
        }
        if (UIManager.HasPanel<PanelBackground>())
            UIManager.GetPanel<PanelBackground>().setupBG("hallBg2");
        if (ModelDisplay.ModelDefaultCamp != WorldManager.CAMP_DEFINE.REBEL && ModelDisplay.ModelDefaultCamp != WorldManager.CAMP_DEFINE.UNION)
            ModelDisplay.ModelDefaultCamp = WorldManager.CAMP_DEFINE.UNION;
        UpdateCampIcon();
        ModelDisplay.UpdateModel();
        m_eventBubble.TrySetActive(RechargeGiftManager.HasAccummultiveRechargeGift() || RechargeGiftManager.HasTodayFirstRechargeGift());
        //if (PlayerSystem.roleData.first_pay_time != 0 || RechargeGiftManager.m_bFirstPay)
        //{
        //    m_goFirstRecharge.TrySetActive(false);
        //    m_imgRecharge.gameObject.TrySetActive(true);
        //}
        //else
        //{
        //    m_goFirstRecharge.TrySetActive(true);
        //    m_imgRecharge.gameObject.TrySetActive(false);
        //}
        UpdateBubble();


       
        NetLayer.Send(new tos_player_welfare_data());

        NetLayer.Send(new tos_player_signin_data());
        if (UIManager.GetPanel<PanelPvpRoomList>() != null)
        {
            UIManager.GetPanel<PanelPvpRoomList>().m_filterRule = "";
        }
        if (AntiAddictionManager.Instance.isFirstLoading)
        {
            if (Driver.m_platform == EnumPlatform.web && ConfigMisc.GetInt("anti_addiction_open") == 1)
                UIManager.ShowTipPanelInLayer("抵制不良游戏 拒绝盗版游戏\n注意自身保护 谨防受骗上当\n适度游戏益脑 沉迷游戏伤身\n合理安排时间 享受健康生活", UIManager.GetLayer(LayerType.AnitAddiction));

            if (AntiAddictionManager.Instance.state == AntiAddictionState.NotCheck)
                UIManager.PopPanel<PanelAntiAddiction>(null, true, null, LayerType.AnitAddiction);
        }
        AntiAddictionManager.Instance.isFirstLoading = false;
        if (Driver.m_platform == EnumPlatform.ios)
        {
            if (!GlobalConfig.serverValue.activity_open_ios)
            {
                m_btnEvent.SetActive(false);
            }
            if (!GlobalConfig.serverValue.legionWar_open)
            {
                Util.SetGrayShader(m_btnLegion.GetComponent<Image>());
                m_lockLegion.TrySetActive(true);
                UGUIClickHandler.Get(m_btnLegion).onPointerClick -= LegionWar;
                UGUIClickHandler.Get(m_btnLegion).onPointerClick += null;
            }
            else
            {
                m_lockLegion.TrySetActive(false);
                Util.SetNormalShader(m_btnLegion.GetComponent<Image>());
            }
        }

        var week = ConfigManager.GetConfig<ConfigEventList>();
        var wls = week.GetLine(1200);
        var wl = wls.WeekLimit;
        var dt = TimeUtil.GetNowTime().DayOfWeek;
        var dts = TimeUtil.GetNowTime();
        m_isTime = true;
        //for (int i = 0; i < wl.Length; i++)
        //{
        //    if (wl[i] == (int)dt)
        //    {
        //        if (dts.Hour >= 19 && dts.Hour <= 21)
        //        {
        //            m_isTime = true;
        //        }
        //        break;
        //    }
        //}

        if (dts.Year == 2016 && dts.Month <= 4 && dts.Day < 6)
        {
            m_isTime = false;
        }

        if (m_isTime)
        {
            m_lockLegion.TrySetActive(false);
            Util.SetNormalShader(m_btnLegion.GetComponent<Image>());
        }
        else
        {
            Util.SetGrayShader(m_btnLegion.GetComponent<Image>());
            m_lockLegion.TrySetActive(true);
        }

        //军团战领取奖励红点
        NetLayer.Send(new tos_legionwar_my_info() { });
        m_legionIsBattle.gameObject.TrySetActive(checkIsLegionTime());
        m_corpsIsBattle.gameObject.TrySetActive(checkIsCorpsTime());

        if (!AudioManager.IsBgMusicPlaying())
            AudioManager.PlayBgMusic("Hall_1");
        if (!GlobalConfig.clientValue.newHandWordBoss_enter && PlayerSystem.roleData.level < 8)
        {
            m_imgNewHandWorldBoss.TrySetActive(true);
            m_newHandWorldBossEffect.Play();
        }
        lotteryEvent = ConfigManager.GetConfig<ConfigEventList>().GetLine(1661);
        m_discountTime.TrySetActive(ConfigManager.GetConfig<ConfigEventList>().IsInLotteryEvent());
        if (HasParams())
        {
            if (m_params.Length > 0)
            {
                if (m_params[0] == "login")
                {
                    SdkManager.LogEnterGame(PlayerSystem.roleId.ToString(), PlayerSystem.roleData.name, PlayerSystem.roleData.level.ToString(), SdkManager.m_serverId, SdkManager.m_serverName);
                }
            }
        }
    }

    public bool IsInLotteryEvent()
    {

        if (lotteryEvent == null)
        {
            return false;
        }
        var time = TimeUtil.GetNowTimeStamp();
        if (time >= int.Parse(lotteryEvent.TimeStart) && time <= int.Parse(lotteryEvent.TimeEnd))
        {
            return true;
        }
        return false;
    }

    private void OnFirstPay()
    {
        //m_goFirstRecharge.TrySetActive(false);
    }
    public override void OnHide()
    {
        if (m_effect_player != null)
        {
            m_effect_player.Destroy();
            m_effect_player = null;
        }
        if (m_effectReward1 != null)
            m_effectReward1.Destroy();
        m_effectReward1 = null;
        if (m_effectReward2 != null)
            m_effectReward2.Destroy();
        m_effectReward2 = null;
        if (m_effect_legion != null)
        {
            m_effect_legion.Destroy();
            m_effect_legion = null;
        }
        if (m_effect_corps != null)
        {
            m_effect_corps.Destroy();
            m_effect_corps = null;
        }
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.SetNameVisible(false);
        CorpsDataManager.IsOpenByHallBattlePanel = false;
        //if (m_effectShouchong != null)
        //    m_effectShouchong.Destroy();
        //m_effectShouchong = null;
//        if (UIManager.HasPanel<PanelBackground>())
//            UIManager.GetPanel<PanelBackground>().setupBG();
    }

    public override void OnBack()
    {
        //        if (SdkManager.IsSupportFunction("showPlatformExitDialog"))
        //            SdkManager.ShowExitDialog();
        //        else
        //            UIManager.ShowTipPanel("是否要退出游戏？", Application.Quit, UIManager.HideTipPanel);
    }

    public override void OnDestroy()
    {
        //if (m_cardMenu != null)
        //    m_cardMenu.Dispose();
        //m_cardMenu = null;
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_BUBBLE_TIP, updateLegionButtle);
    }

    public static void ClearLimitGift()
    {
        m_limitGiftLeftTime = 0;
        m_limitGiftLastTime = 0;
        m_limitGiftId = -1;
        m_finishTime = 0;
    }

    public override void Update()
    {
        DateTime date = TimeUtil.GetNowTime();
        //if (CorpsDataManager.IsJoinCorps())
        //{

        //}
        UpdateCorpsState();
        UpdateLegionState();
        UpdateWorldBoss(date);
        UpdateLimitGiftTime();
        m_discountTime.TrySetActive(ConfigManager.GetConfig<ConfigEventList>().IsInLotteryEvent());
        if (!IsOpen())
            return;
        //if (!m_cardMenu.CheckStop())
        //{
        //    m_moveLeft = false;
        //    m_moveRight = false;
        //    //            m_sublingIndex = m_cardMenu.m_subIndex;
        //}
        //if (m_moveRight)
        //{
        //    m_cardMenu.MoveRight(Time.smoothDeltaTime);
        //    return;
        //}
        //if (m_moveLeft)
        //{
        //    m_cardMenu.MoveLeft(Time.smoothDeltaTime);

        //}

        m_onlineClock.Update();
        m_lotteryNewClock.Update();
    }

    void OnClickActList(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelLoginReward>();
    }

    void OnClickRankList(GameObject target, PointerEventData eventData)
    {
        /*if (PlayerSystem.roleData.level < 10)
        {
            UIManager.ShowTipPanel("排行榜10级开放");
            return;
        }*/
        PanelRankList.OpenPanel();
    }
    float m_lastTime = 0f;
    private void UpdateWorldBoss(DateTime date)
    {
        if (GlobalConfig.clientValue.newHandWordBoss_enter || PlayerSystem.roleData.level >= 8)
        {
            m_imgNewHandWorldBoss.TrySetActive(false);
            m_newHandWorldBossEffect.Hide();
        }
        if (m_imgWorldBoss == null||(Time.realtimeSinceStartup-m_lastTime)<0.5f)
            return;
        m_lastTime = Time.realtimeSinceStartup;
        DateTime startShowTime;
        DateTime openTime;
        DateTime endShowTime;
        PanelWorldBoss.GetWorldBossTime(date, out startShowTime, out openTime, out endShowTime);
        bool showImg = false;
        if (date < startShowTime)
        {
            showImg = false;
            m_worldBossEventOpen = false;
        }
        else if (date < openTime)
        {
            showImg = true;
            TimeSpan sp = openTime - date;
            m_worldBossText.text = string.Format("{0:D2}:{1:D2}", sp.Minutes, sp.Seconds);
            m_worldBossEventOpen = false;
        }
        else if (date < endShowTime)
        {
            showImg = true;
            TimeSpan sp = endShowTime - date;
            m_worldBossText.text = string.Format("{0:D2}:{1:D2}", sp.Minutes, sp.Seconds);
            if (!m_worldBossEventOpen)
            {
                m_worldBossEffect.Play();
                SortingOrderRenderer.RebuildAll();
            }
            m_worldBossEventOpen = true;
        }
        else
        {
            showImg = false;
            m_worldBossEventOpen = false;
        }
        if (PlayerSystem.roleData.level <= 8)
            showImg = false;
        m_imgWorldBoss.TrySetActive(showImg);
        if (!m_worldBossEventOpen && PlayerSystem.roleData.level > 8)
            m_worldBossEffect.Hide();
    }

  

    void Toc_player_item_equip_data(toc_player_item_equip_data data)
    {
        ModelDisplay.UpdateModel();
    }

    void Toc_player_equip_item(toc_player_equip_item data)
    {
        TimerManager.SetTimeOut(0.05f, () => { ModelDisplay.UpdateModel(); });
    }
    public void UpdateOnlineReward(p_welfare_data_toc data)
    {
        if (m_onlineClock != null)
        {
            if (!m_onlineClock.UpdateView(data))
            {
                m_rewardtime.TrySetActive(false);
            }
                //m_onlineClock.TrySetActive(false);
        }

    }

    /// <summary>
    /// 更新显示限时礼包入口
    /// </summary>
    public static void UpdateLimitGift(bool isShow)
    {
        if (m_limitGiftGo !=null)
        {
            m_limitGiftGo.TrySetActive(isShow);
        }
    }

    private static float m_limitGiftLastTime;
    private void UpdateLimitGiftTime()
    {
        
        if (Time.realtimeSinceStartup - m_limitGiftLastTime >= 1)
        {
            m_limitGiftLastTime = Time.realtimeSinceStartup;
            m_limitGiftLeftTime--;
        }
        if (m_limitGiftLeftTime > 0)
        {
            m_limitGiftTimeText.text = TimeUtil.FormatTime(m_limitGiftLeftTime, 3, false);
            if( UIManager.IsOpen<PanelLimitGift>() == true )
            {
                UIManager.GetPanel<PanelLimitGift>().UpdateLeftTime(m_limitGiftLeftTime);
            }
        }
        else
        {
            UpdateLimitGift(false);
            if( UIManager.IsOpen<PanelLimitGift>() == true )
            {
               UIManager.GetPanel<PanelLimitGift>().HidePanel();
            }
        }
    }


    private void OpenLimitGiftPanel(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelLimitGift>(new object[] { m_limitGiftId, m_limitGiftLeftTime, m_limitGiftArr }, true);
    }

    private void OpenFightVideoPanel(GameObject target, PointerEventData eventData)
    {
         UIManager.ShowPanel<PanelFightVideo>(new object[] { m_limitGiftId, m_limitGiftLeftTime, m_limitGiftArr });
    }

    private void OpenLiveVideoTV(GameObject target, PointerEventData evenData)
    {
        Logger.Log("ClickChuShouTV、、、、");
        if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_CHUSHOUTV))
        {
            UIManager.ShowNoThisFunc();
            return;
        }
        else
            SdkManager.ShowChuShouTV();
    }

    private void Toc_player_timelimit_gift_time(toc_player_timelimit_gift_time data)
    {
        int timeStamp = TimeUtil.GetNowTimeStamp() - data.timestamp;
        m_finishTime = data.timestamp;
        Logger.Log("timeStamp  = " + timeStamp + "   m_finishTime = " + m_finishTime);
        PlayerPrefs.SetInt("settle_opne_limitGift_panel", 0);
    }

    /// <summary>
    /// 限时礼包协议
    /// </summary>
    /// <param name="limitGift"></param>
    private static void Toc_player_timelimit_gift(toc_player_timelimit_gift limitGift)
    {
        int temp = (int)TimeUtil.GetRemainTime((uint)limitGift.second_left).TotalSeconds;
        m_limitGiftLeftTime = temp;
        m_limitGiftId = limitGift.gift_id;
        m_limitGiftArr = limitGift.item_list;
        if (m_limitGiftLeftTime > 0 && isShowLimitIcon() == true)
        {
            UpdateLimitGift(true);
        }
        else
        {
            UpdateLimitGift(false);
        }
        if (UIManager.IsOpen<PanelSettle>() == true && PlayerPrefs.GetInt("settle_opne_limitGift_panel") == 0)
        {
            Debug.Log("00000000000000000000000000000 == " + m_limitGiftLeftTime);
            UIManager.PopPanel<PanelLimitGift>(new object[] { m_limitGiftId, m_limitGiftLeftTime, m_limitGiftArr }, true);
            PlayerPrefs.SetInt("settle_opne_limitGift_panel", 1);
        }
    }

    private static bool isShowLimitIcon()
    {
        bool isShow = true;
        int buyNum = 0;
        for( int i = 0; i < m_limitGiftArr.Length;i++ )
        {
            if( m_limitGiftId == 0)
            {
                if( m_limitGiftArr[i].buy == 1 )
                {
                    buyNum = buyNum + 1;
                    if( buyNum == 3 )
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (m_limitGiftArr[i].buy == 1)
                {
                    return false;
                }
            }
        }
        return isShow;
    }

    private static void Toc_player_timelimit_gift_buy(toc_player_timelimit_gift_buy data)
    {
        if (data.reward_id != "")
        {
            for( int i = 0 ; i < m_limitGiftArr.Length; i++ )
            {
                if (data.reward_id == m_limitGiftArr[i].reward_id)
                {
                    m_limitGiftArr[i].buy = 1;
                }
            }
            bool isShow = isShowLimitIcon();
            UpdateLimitGift(isShow);
            if (UIManager.IsOpen<PanelLimitGift>() == true )
            {
                if( isShow == false )
                {
                    UIManager.GetPanel<PanelLimitGift>().HidePanel();
                }
                else
                {
                    UIManager.GetPanel<PanelLimitGift>().UpdateView(m_limitGiftArr);
                }
                
            }
        }
    }
    /*private void OnClickOnline(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelOnlineReward>(null, true, null);
    }*/

    private void Toc_player_signin_data(toc_player_signin_data data)
    {
        if (data.day_signin.signined_times >= ConfigManager.GetConfig<ConfigSevenDaySignin>().m_dataArr.Length||data.day_signin.signined_times<0)
        {
            imgSevenDay.TrySetActive(false);
            showSevenDay = false;
        }
        else
        {
            imgSevenDay.TrySetActive(true);
            showSevenDay = true;
        }
    }

    void  Toc_legionwar_data(toc_legionwar_data data)
    {
        UIManager.PopPanel<PanelLegionMap>();
        bool m_has = false;
        bool m_is = false;
        if (PlayerSystem.m_tagList != null)
        {
            for (int i = 0; i < PlayerSystem.m_tagList.Count; i++)
            {
                if (PlayerSystem.m_tagList[i].tag_id == (int)EnumTag.LEGION_WAR_FIRST)
                {
                    m_has = true;
                    if (PlayerSystem.m_tagList[i].value == 1)
                    {
                        m_is = true;
                    }
                }
            }
        }
        if (!m_has || !m_is)
        {
            UIManager.PopPanel<PanelLegionFirst>(null, true);
        }
    }

    void UpdateLegionState()
    {
        m_nowTime = DateTime.Now;
        if (m_nowTime == m_legionStart)
        {
            if(m_legionIsBattle.gameObject.activeSelf !=true)
            {
                m_legionIsBattle.gameObject.TrySetActive(true);
                if (m_effect_legion == null)
                {
                    m_effect_legion = UIEffect.ShowEffectBefore(m_legionPoint.transform, EffectConst.UI_HALLBATTLE_LEGION_CORPS, new Vector3(-85, 25, 0));
                }
            }
        }
        if (m_nowTime == m_legionEnd)
        {
            if (m_legionIsBattle.gameObject.activeSelf != false)
            {
                m_legionIsBattle.gameObject.TrySetActive(false);
                if (m_effect_legion != null)
                {
                    m_effect_legion.Destroy();
                    m_effect_legion = null;
                }
            }
        }
    }

    void UpdateCorpsState()
    {
        m_nowTime = DateTime.Now;
        if (m_nowTime == m_corpStart)
        {
            if (m_corpsIsBattle.gameObject.activeSelf != true)
            {
                m_corpsIsBattle.gameObject.TrySetActive(true);
                if (m_effect_corps == null)
                {
                    m_effect_corps = UIEffect.ShowEffectBefore(m_corpsPoint.transform, EffectConst.UI_HALLBATTLE_LEGION_CORPS, new Vector3(-85, 25, 0));
                }
            }
        }
        if (m_nowTime == m_corpEnd)
        {
            if (m_corpsIsBattle.gameObject.activeSelf != false)
            {
                m_corpsIsBattle.gameObject.TrySetActive(false);
                if (m_effect_corps != null)
                {
                    m_effect_corps.Destroy();
                    m_effect_corps = null;
                }
            }
        }
    }

}
