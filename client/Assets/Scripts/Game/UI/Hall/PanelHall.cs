﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using Random = UnityEngine.Random;

class PanelHall : BasePanel
{
    private Image m_imgHead;
    private Image m_imgArmy;
    private Text m_textPlayerName;
    private GameObject m_buff_go;
    private GameObject m_buffListGo;
    private List<Image> m_buff_list;
    public ButtonToggle m_btnMall;
    public ButtonToggle m_btgBattle;
    public ButtonToggle m_btgDepot;
    public ButtonToggle m_btgEnhance;
    public ButtonToggle m_btgSocial;
    public ButtonToggle m_btgMission;
    private Button m_btnBack;
//    private Image m_imgLvPgbar;
    private Text m_txtLv;
    private Text m_txtDiamond;
    private Text m_txtCoin;
    private Text m_txtMedal;
    private Text m_txtVoucher;

    private GameObject m_goChatTips;
    private Image m_imgChannel;
    private Text m_txtChannel;
    private Text m_txtChatName;
    private Image m_imgChatVoice;
    private Text m_txtChatMsg;
    private MotionText m_motionText;
    private RectTransform m_transChatName;
    private RectTransform m_transChatMsg;
    private UIEffect m_effect_player; 
    private UIEffect _headEffect;
    

    Stack<BasePanel> m_showedPanelList;

    private ButtonToggle[] m_btgs;

    private List<UIEffect> m_effects;

    private ChatMsgTips m_chatMsgTips;
    private float _update_time = -1;

    private GameObject m_goVip;
    private GameObject m_goPlayerName;
    private Image m_imgVip;
    private RectTransform m_effectPoint;
//    private Vector2 m_nameAnchorPos;

    public static bool m_firstTimeLoad = true;

    private GameObject m_antiAddiction;
    private Image m_money;
    private Image m_exp;
    private Image m_medal;
    private Image m_headVip;
    private GameObject m_backRoom;
    private GameObject m_back;

    GameObject m_topbargo;
    GameObject m_mainplayergo;
    GameObject m_moneygo;
    GameObject m_toprightgo;
    GameObject m_chatgo;
    GameObject m_bottomgo;
    
    public PanelHall()
    {
        SetPanelPrefabPath("UI/Hall/PanelHall");
        AddPreLoadRes("UI/Hall/PanelHall", EResType.UI);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
        AddPreLoadRes("UI/Mail/ItemMail", EResType.UI);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALL_COIN, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALL_MEDAL, EResType.UIEffect);
        AddPreLoadRes("Effect/" + EffectConst.UI_HALL_DIAMOND, EResType.UIEffect);

        if (!Application.isEditor || UIGmPanel.m_editorGuide)
            BeginGuide();
    }


    public override void Init()
    {
        m_buff_go = m_tran.Find("mainplayer/buff_list/buff_render").gameObject;
        m_buffListGo = m_tran.Find("mainplayer/buff_list").gameObject;
        m_buff_list = new List<Image>();

        m_showedPanelList = new Stack<BasePanel>();
        m_textPlayerName = m_tran.Find("imgNameBg/playername").GetComponent<Text>();
        m_goVip = m_tran.Find("mainplayer/VIP").gameObject;
        m_goPlayerName = m_tran.Find("imgNameBg").gameObject;
        m_imgVip = m_tran.Find("mainplayer/VIP").GetComponent<Image>();        
//        m_nameAnchorPos = m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;
        m_imgHead = m_tran.Find("mainplayer/headframe/head").GetComponent<Image>();
        m_headVip = m_tran.Find("mainplayer/headVip").GetComponent<Image>();
        m_effectPoint = m_tran.Find("mainplayer/headVip/effect").GetComponent<RectTransform>();
        m_imgArmy = m_tran.Find("mainplayer/army").GetComponent<Image>();

//        m_imgLvPgbar = m_tran.Find("lvPgbar/background/value").GetComponent<Image>();
        m_txtLv = m_tran.Find("mainplayer/lvl/Text").GetComponent<Text>();
        m_txtDiamond = m_tran.Find("money/diamond/Text").GetComponent<Text>();
        m_txtVoucher = m_tran.Find("money/voucher/Text").GetComponent<Text>();
        m_txtCoin = m_tran.Find("money/coin/Text").GetComponent<Text>();
        m_txtMedal = m_tran.Find("money/medal/Text").GetComponent<Text>();

        m_goChatTips = m_tran.Find("chatframe/tips").gameObject;
        m_imgChannel = m_tran.Find("chatframe/tips/channel").GetComponent<Image>();
        m_txtChannel = m_tran.Find("chatframe/tips/channel/Text").GetComponent<Text>();
        m_txtChatName = m_tran.Find("chatframe/tips/name").GetComponent<Text>();
        m_imgChatVoice = m_tran.Find("chatframe/tips/audio").GetComponent<Image>();
        m_transChatName = m_tran.Find("chatframe/tips/name").GetComponent<RectTransform>();
        m_transChatMsg = m_tran.Find("chatframe/tips/line").GetComponent<RectTransform>();
        //m_goChatTips = m_tran.Find("chatframe/tips/line").gameObject;
        m_txtChatMsg = m_tran.Find("chatframe/tips/line").GetComponent<Text>();        

        var rect = m_txtChatMsg.rectTransform.rect;
        m_txtChatMsg.rectTransform.anchorMin = new Vector2(0, 1);
        m_txtChatMsg.rectTransform.anchorMax = new Vector2(0, 1);
        m_txtChatMsg.rectTransform.sizeDelta = rect.size;
        m_motionText = m_txtChatMsg.gameObject.AddComponent<MotionText>();
        m_motionText.MaxImageSize = 22;

        m_btgBattle = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnBattle"), "battle");
        m_btgDepot = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnDepot"), "depot");
        //m_btgEnhance = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnEnhance"), "enhance");
        m_btgSocial = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnSocial"), "social");
        m_btgMission = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnMission"), "mission");
        m_btnMall = new ButtonToggle(m_tran.Find("bottomframe/btnBar/btnMall"), "mall");


        m_btgBattle.SetChosen();

        m_btnBack = m_tran.Find("topRight/back").GetComponent<Button>();

        m_antiAddiction = m_tran.Find("mainplayer/antiAddiction").gameObject;
        m_money = m_antiAddiction.transform.Find("money").GetComponent<Image>();
        m_exp = m_antiAddiction.transform.Find("exp").GetComponent<Image>();
        m_medal = m_antiAddiction.transform.Find("madel").GetComponent<Image>();

        m_backRoom = m_tran.Find("topRight/backRoom").gameObject;
        m_back = m_tran.Find("topRight/back").gameObject;
        m_btgs = new ButtonToggle[5];
        m_btgs[0] = m_btgBattle;
        m_btgs[1] = m_btgDepot;
        //m_btgs[2] = m_btgEnhance;
        m_btgs[2] = m_btgSocial;
        m_btgs[3] = m_btgMission;
        m_btgs[4] = m_btnMall;

        m_effects = new List<UIEffect>();
        m_effects.Add(UIEffect.ShowEffect(m_tran.Find("money/coin/icon"), EffectConst.UI_HALL_COIN, new Vector2(1, 1)));
        m_effects.Add(UIEffect.ShowEffect(m_tran.Find("money/medal/icon"), EffectConst.UI_HALL_MEDAL, new Vector2(0, -1)));
        m_effects.Add(UIEffect.ShowEffect(m_tran.Find("money/diamond/icon"), EffectConst.UI_HALL_DIAMOND, new Vector2(0f, 3f)));
        //        m_effects.Add(UIEffect.ShowEffect(m_btnMall.transform, EffectConst.UI_HALL_BTN_MALL, 0, new Vector2(-3f, -4.5f)));
        m_headVip.gameObject.TrySetActive(false);

        m_topbargo = m_tran.Find("imgTopBarBg").gameObject;
        m_mainplayergo = m_tran.Find("mainplayer").gameObject;
        m_moneygo = m_tran.Find("money").gameObject;
        m_toprightgo = m_tran.Find("topRight").gameObject;
        m_chatgo = m_tran.Find("chatframe").gameObject;
        m_bottomgo = m_tran.Find("bottomframe").gameObject;
        
    }

    public void HideHall()
    {
        m_topbargo.TrySetActive(false);
        m_mainplayergo.TrySetActive(false);
        m_moneygo.TrySetActive(false);
        m_toprightgo.TrySetActive(false);
        m_chatgo.TrySetActive(false);
        m_bottomgo.TrySetActive(false);
    }

    public void ShowHall()
    {
        m_topbargo.TrySetActive(true);
        m_mainplayergo.TrySetActive(true);
        m_moneygo.TrySetActive(true);
        m_toprightgo.TrySetActive(true);
        m_chatgo.TrySetActive(true);
        m_bottomgo.TrySetActive(true);
    }

    public void SetNameVisible(bool visible)
    {
        m_goPlayerName.TrySetActive(visible);
    }

    public void HideButton()
    {
        m_chatgo.TrySetActive(false);
        m_bottomgo.TrySetActive(false);
    }

    public void ShowButton()
    {
        m_chatgo.TrySetActive(true);
        m_bottomgo.TrySetActive(true);
    }

    public static void BeginGuide()
    {
        if (Driver.m_platform == EnumPlatform.web)
            Logger.Error("m_firstTimeLoad:" + m_firstTimeLoad + ">>>GlobalConfig.clientValue.guideChapterName:" + GlobalConfig.clientValue.guideChapterName);

        if (m_firstTimeLoad && !GuideManager.IsGuideFinish())
        {
            var chapterName = GlobalConfig.clientValue.guideChapterName;
            if (chapterName == "")
            {
                if (Driver.isMobilePlatform)
                    UIManager.PopPanel<PanelGuideAsk>(null, true, null, LayerType.Guide, 0.91f);
                else
                {
                    GuideManager.StartChapterName("Guide");
                    GlobalConfig.clientValue.guideChapterName = "Guide";
                    GlobalConfig.SaveClientValue("guideChapterName");
                    Logger.Log("保存指引进度：" + GlobalConfig.clientValue.guideChapterName);
                }
            }
            else
                GuideManager.StartChapterName(chapterName);
        }
    }

    public void CheckChosen(ButtonToggle btg)
    {
        for (int i = 0; i < 5; i++)
        {
            if (btg == m_btgs[i])
                m_btgs[i].SetChosen();
            else
                m_btgs[i].SetUnChosen();
        }
    }

    public override void InitEvent()
    {
        NetLayer.Send(new tos_player_mail_list() { });
        NetLayer.Send(new tos_player_phased_purchase_data() { });
        UGUIClickHandler.Get(m_tran.Find("topRight/back"), null).onPointerClick += OnBackBtnClick;
        UGUIClickHandler.Get(m_tran.Find("topRight/setting")).onPointerClick += OnClickHallSetting;
        UGUIClickHandler.Get(m_tran.Find("topRight/mail")).onPointerClick += OnMailClick;
        UGUIClickHandler.Get(m_tran.Find("chaticon")).onPointerClick += OnClickChat;
        UGUIClickHandler.Get(m_tran.Find("chatframe")).onPointerClick += OnClickChat;
        UGUIClickHandler.Get(m_btgBattle.Toggle.gameObject).onPointerClick += OnbtgBattleClick;
        UGUIClickHandler.Get(m_btgDepot.Toggle.gameObject).onPointerClick += OnbtgDepotClick;
        //UGUIClickHandler.Get(m_btgEnhance.Toggle.gameObject).onPointerClick += OnbtgEnhanceClick;

        UGUIClickHandler.Get(m_btgSocial.Toggle.gameObject).onPointerClick += OnSocialToggleClick;
        UGUIClickHandler.Get(m_btgMission.Toggle.gameObject).onPointerClick += OnMissionToggleClick;
        UGUIClickHandler.Get(m_backRoom).onPointerClick += OnBackRoomClick;
        UGUIClickHandler.Get(m_btnMall.Toggle.gameObject).onPointerClick += OnBtnMallClick;

        UGUIClickHandler.Get(m_tran.Find("mainplayer")).onPointerClick += OnAskShowMainPlayerInfo;
        UGUIClickHandler.Get(m_tran.Find("money/coin")).onPointerClick += ClickCoinAdd;
        UGUIClickHandler.Get(m_tran.Find("money/medal")).onPointerClick += ClickMedalAdd;
        UGUIClickHandler.Get(m_tran.Find("money/diamond")).onPointerClick += ClickDiamondAdd;
        UGUIClickHandler.Get(m_tran.Find("money/voucher")).onPointerClick += ClickVoucherAdd;
//        UGUIClickHandler.Get(m_tran.Find("mainplayer/headframe")).onPointerClick += delegate
//        {
//            UIManager.PopPanel<PanelHeadIcon>(null, true, this);
//        };
 
        AddEventListener(GameEvent.MAINPLAYER_INFO_CHANGE, UpdateView);
        AddEventListener<ChatMsgTips>(GameEvent.UI_GET_CHAT_MSG, OnNewChatMsg);

        AddEventListener(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED, () => { m_btgMission.notice_num = PlayerMissionData.singleton.reward_num + PlayerAchieveData.m_rewardNum; });
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
        AddEventListener(GameEvent.UI_MEMBER_CHANGE, OnChangeMember);
        AddEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, UpdateView);
        AddEventListener(GameEvent.UI_SELF_ICON_URL_RETRIVED, UpdateView);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener(GameEvent.INPUT_KEY_ENTER_UP, () =>
        {
            if (RoomModel.IsInRoom) return;
            if (!(UIManager.IsOpen<PanelChatBig>() || UIManager.IsOpen<PanelChatSmall>()))
            {
                OnClickChat(null, null);
            }
        });
#endif

        if (m_firstTimeLoad)
        {
            m_firstTimeLoad = false;
            NetLayer.Send(new tos_player_load_done() { step = 0 });
        }
    }

    private void OnBtnMallClick(GameObject target, PointerEventData eventData)
    {                
        playFunClickEffect(m_btnMall.Toggle.transform);
        EnsureLeaveRoom(() =>
        {
//            var panel = UIManager.GetPanel<PanelMall>();
//            if (panel != null && panel.IsOpen() && panel.FilterMode)
//                panel.HidePanel();
            UIManager.ShowPanel<PanelMall>();
        }, target);
        CheckChosen(m_btnMall);     
    }

    /*private void OnbtgEnhanceClick(GameObject target, PointerEventData eventData)
    {
        playFunClickEffect(m_btgEnhance.Toggle.transform); 
        EnsureLeaveRoom(() => UIManager.ShowPanel<PanelStrengthenMain>(),target);
        CheckChosen(m_btgEnhance); 
    }*/

    private void OnbtgDepotClick(GameObject target, PointerEventData eventData)
    {
        playFunClickEffect(m_btgDepot.Toggle.transform);
        EnsureLeaveRoom(() => UIManager.ShowPanel<PanelDepot>(),target);
        CheckChosen(m_btgDepot); 
    }

    private void OnbtgBattleClick(GameObject target, PointerEventData eventData)
    {
        playFunClickEffect(m_btgBattle.Toggle.transform);
        EnsureLeaveRoom(() => UIManager.ShowPanel<PanelHallBattle>(),target);
        CheckChosen(m_btgBattle); 
    }

    private void OnBackRoomClick(GameObject target, PointerEventData eventData)
    {
        ShowPanelRoom();
    }

    public void OnSocialToggleClick(GameObject target, PointerEventData eventData)
    {
        playFunClickEffect(m_btgSocial.Toggle.transform);
        EnsureLeaveRoom(() => UIManager.ShowPanel<PanelSocity>(),target); 
        CheckChosen(m_btgSocial);
    }

    public void OnMissionToggleClick(GameObject target, PointerEventData eventData)
    {
        playFunClickEffect(m_btgMission.Toggle.transform);
        EnsureLeaveRoom(() => UIManager.ShowPanel<PanelDailyMission>(),target); 
        CheckChosen(m_btgMission); 
    }


    private void UpdateBubble()
    {
        if (m_btgSocial == null || m_btnMall == null)
            return;
        //m_btgEnhance.notice_num = BubbleManager.HasBubble(BubbleConst.StrengthenTrain) || BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade) ? 1 : 0;
        m_btgSocial.notice_num = BubbleManager.GetBubble(BubbleConst.FriendApply) +
                                 BubbleManager.GetBubble(BubbleConst.CorpsApply) +
                                 BubbleManager.GetBubble(BubbleConst.LegionApply) +
                                 BubbleManager.GetBubble(BubbleConst.CorpsTaskReward) +
                                 BubbleManager.GetBubble(BubbleConst.CorpsGuardRunning) +
                                 BubbleManager.GetBubble(BubbleConst.MasterGift) + BubbleManager.GetBubble(BubbleConst.MasterGrade) + BubbleManager.GetBubble(BubbleConst.StudentApply) + BubbleManager.GetBubble(BubbleConst.ApprenticeGift);
        var mallBubble = m_tran.Find("bottomframe/btnBar/btnMall/bubble_tip");
        if(mallBubble)
        {
            mallBubble.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.MallSPNotice));
        }

        var mailBubble = m_tran.Find("topRight/mail/bubble_tip");
        if (mailBubble)
        {
            mailBubble.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.MailUnread));
        }
        var chat_bubble = m_tran.Find("chaticon/bubble");
        if (chat_bubble != null)
        {
            chat_bubble.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.ChatOfflineMsg));
        }
        var battleBubble = m_tran.Find("bottomframe/btnBar/btnBattle/bubble_tip");
        if (battleBubble != null)
        {
            battleBubble.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.NewHandTask));
        }
        var hallChatTips = m_tran.Find("chatframe/bubble_tip");
        if (hallChatTips != null)
        {
            hallChatTips.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.HallChatCorps) || BubbleManager.HasBubble(BubbleConst.HallChatLegion) ||
                BubbleManager.HasBubble(BubbleConst.HallChatFriend) || BubbleManager.HasBubble(BubbleConst.HallChatPrivate));
        }
    }

    private void playFunClickEffect(Transform tran)
    {
        if (m_effect_player != null)
        {
            if (m_effect_player.m_goEffect != null && m_effect_player.m_goEffect.transform.parent == tran)
            {
                return;//忽略连续快速点击
            }
            m_effect_player.Destroy();
            m_effect_player = null;
        }
        m_effect_player = UIEffect.ShowEffect(tran, EffectConst.UI_HALL_CLICK, 1f);
    }

    private void Toc_player_buff_list(toc_player_buff_list data)
    {
        OnUpdateBuffHandler();
    }

    private void OnUpdateBuffHandler()
    {
        _update_time = -1;
        var buff_data = PlayerSystem.buff_data;
        var config = ConfigManager.GetConfig<ConfigBuff>();
        var buff_value = 0;
        if (buff_data != null && buff_data.Length > 0)
        {
            foreach (var sbuff in buff_data)
            {
                if (_update_time == -1 || sbuff.time_limit < _update_time)
                {
                    _update_time = sbuff.time_limit;
                }
                var tdata = config.GetLine(sbuff.buff_id);
                if (tdata != null)
                {
                    buff_value |= tdata.IconValue;
                }
            }
        }
        var i = 0;
        var icon_value = 1;
        Image render = null;
        while (buff_value > 0)
        {
            if ((buff_value & icon_value) > 0)
            {
                if (i < m_buff_list.Count)
                {
                    render = m_buff_list[i];
                }
                else
                {
                    render = ((GameObject)UIHelper.Instantiate(m_buff_go)).GetComponent<Image>();
                    render.transform.SetParent(m_buff_go.transform.parent);
                    render.transform.localScale = Vector3.one;
                    m_buff_list.Add(render);
                    UGUIClickHandler.Get(render.gameObject).onPointerClick += OnBuffItemClick;
                }
                render.gameObject.TrySetActive(true);
                render.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "buff_icon_" + icon_value));
                i++;
            }
            buff_value |= icon_value;
            buff_value ^= icon_value;
            icon_value *= 2;
        }
        while (i < m_buff_list.Count)
        {
            m_buff_list[i].gameObject.TrySetActive(false);
            i++;
        }
    }

    private void OnClickChat(GameObject target, PointerEventData eventData)
    {
        if (m_chatMsgTips == null)
        {
            UIManager.PopChatPanel<PanelChatSmall>();
        }
        else
        {
            if (/*m_chatMsgTips.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER ||m_chatMsgTips.m_eChannel==CHAT_CHANNEL.CC_CRSSERVER|| */m_chatMsgTips.m_eChannel == CHAT_CHANNEL.CC_LABA)
            {
                UIManager.PopChatPanel<PanelChatSmall>();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatSmall>(new object[] { m_chatMsgTips });
                m_chatMsgTips = null;
            }
        }

    }
    private void OnBackBtnClick(GameObject target, PointerEventData eventData)
    {
        
        if (m_btnBack.interactable)
        {
            if (UIManager.IsOpen<PanelLotteryNew>())
            {
                UIManager.GetPanel<PanelLotteryNew>().HidePanel();
            }
            UISystem.isNormalBack = true;
            AudioManager.PlayUISound(AudioConst.backBtnClick);
            UIManager.BackPanel();
        }
    }


    private void OnMailClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_mail_list() { });
        UIManager.PopPanel<PanelMail>(null, true);
    }
    void EnsureLeaveRoom(Action callBack,GameObject obj)
    {
        //callBack();        
        var cfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        bool minimize = (cfg != null && cfg.Minimize == 1);
        if (PanelRoomBase.IsCurrentRoomOpen()&&!minimize)
        {
            UIManager.ShowTipPanel("继续操作将离开房间，确定吗？", callBack, () => CheckChosen(m_btgBattle), false, true);
        }
        else
            callBack();
    }
    /// <summary>
    /// 最小化房间
    /// </summary>
    public void MinimizePanelRoom()
    {
        UISystem.isMinimizeRoom = true;
        m_backRoom.TrySetActive(true);
        m_back.TrySetActive(false);
    }

    public void SetNormalBack()
    {
         m_back.TrySetActive(true);
         m_backRoom.TrySetActive(false);
         UISystem.isMinimizeRoom = false;         
    }
    public void ShowPanelRoom()
    {
        m_back.TrySetActive(true);
        m_backRoom.TrySetActive(false);
        UISystem.isMinimizeRoom = false;
        PanelRoomBase.ShowMinimizeRoom();
        if (UIManager.IsOpen<PanelLotteryNew>())
            UIManager.HidePanel<PanelLotteryNew>();
    }

    void OnAskShowMainPlayerInfo(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_view_player() { friend_id = PlayerSystem.roleId });
        
    }

    void Toc_player_view_player(toc_player_view_player data)
    {
        m_showedPanelList.Push(UIManager.CurContentPanel);
        UIManager.PopPanel<PanelMainPlayerInfo>(new object[2] { data,UIManager.CurContentPanel });
    }

    /// <summary>
    /// 检查当前Panel类型是否和房间有关
    /// </summary>
    /// <returns></returns>
    public static bool PanelCheck(Type type )
    {
        string typeStr = type.ToString();
        string subTypeStr = type.BaseType.ToString();
        if (subTypeStr.Equals(typeof( PanelRoomBase).ToString()))
            return true;
        if (subTypeStr.Equals(typeof(PanelRoomListBase).ToString()))
            return true;
        if (typeStr.Equals(typeof(PanelCorpsEnter).ToString()) ||
            typeStr.Equals(typeof(PanelPVE).ToString()) || 
            typeStr.Equals(typeof( PanelLvlPass).ToString()) ||
            typeStr.Equals( typeof( PanelZombiePass).ToString()) ||
            typeStr.Equals(typeof(PanelCorpsNew).ToString())
            )
            return true;
        //if(panel is)
        return false;
    }

    private void ClickDiamondAdd(GameObject target, PointerEventData eventData)
    {
        SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);        
        //ItemInfo info = new ItemInfo();
        //info.ID = 5003;
        //info.cnt = PlayerSystem.roleData.diamond;
        //UIManager.PopPanel<PanelItemGuide>(new object[1] { info}, true);
    }


    private void ClickVoucherAdd(GameObject target, PointerEventData eventData)
    {
        /*string content = "可以在商城的<color=#00ffffff>【其他】标签</color>中兑换点券";
        object obj = UIManager.CurContentPanel;
        if (PanelCheck(obj.GetType()) || UISystem.isMinimizeRoom)
        {
            TipsManager.Instance.showTips(content);
            return;
        }
        UIManager.ShowTipPanel(content, () =>
        {
            OnBtnMallClick(null,null);
        }, null, true, false, "立刻前往", null);
        */
        ConfigMallLine cfg = ConfigManager.GetConfig<ConfigMall>().GetLine(GameConst.ITEM_ID_VOUCHER) as ConfigMallLine;
        if (cfg != null)
            UIManager.PopPanel<PanelBuyVoucher>(new object[] { cfg, "", false,"<size=18>其他获得途径</size>:\n1.任务  2.战队任务 3.福利在线奖励 4.福利-签到" }, true);
    }
    private void ClickMedalAdd(GameObject target, PointerEventData eventData)
    {
        string content = "参与【战斗】可获得大量勋章\n<color=#00ffffff>加入战队，领取【战队军资】可得双倍勋章增益</color>";
        object obj = UIManager.CurContentPanel;        
        if(PanelCheck(obj.GetType())||UISystem.isMinimizeRoom)
        {
            TipsManager.Instance.showTips(content);
            return;
        }
        UIManager.ShowTipPanel(content, () => 
            {
                openPanelPvpRoomList();
            }, null, true, false, "立刻前往", null);
        //if (PanelRoomBase.IsCurrentRoomOpen())
        //{
        //    isNormalBack = false;
        //}
    }

    private void ClickCoinAdd(GameObject target, PointerEventData eventData)
    {
            string content = "参与【战斗】可获得大量金币\n<color=#00ffffff>加入战队，领取【战队军资】可得双倍金币增益</color>";
            object obj = UIManager.CurContentPanel;        
            if(PanelCheck(obj.GetType())||UISystem.isMinimizeRoom)
            {
                TipsManager.Instance.showTips(content);
                return;
            }

            UIManager.ShowTipPanel(content, () =>
            {
                openPanelPvpRoomList();
            }, null, true, false, "立刻前往", null);                   
       
    }

    private void openPanelPvpRoomList()
    {
        if (UIManager.GetPanel<PanelPvpRoomList>() != null)
        {
            UIManager.GetPanel<PanelPvpRoomList>().m_filterRule = "";
            UIManager.GetPanel<PanelPvpRoomList>().UpdateRule();
            UIManager.GetPanel<PanelPvpRoomList>().UpdateRoomList();
        }
        UIManager.GotoPanel("battle");
    }

    private void OnBuffItemClick(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelPlayerBuffInfo>(new object[] { PlayerSystem.buff_data }, true, this);
    }

    public override void OnShow()
    {
        if (HasParams())
        {
//            if (GlobalConfig.serverValue.cm_open)
//                UIManager.ShowTipPanel((string)m_params[0]);
//            NetChatData.MockChatMsg((string)m_params[0]);
        }
        UpdateView();
        OnUpdateBuffHandler();
        UpdateAntiAddiction();
    }

    public static void TryUpdateAntiAddiction()
    {
        if(UIManager.IsOpen<PanelHall>())
        {
            UIManager.GetPanel<PanelHall>().UpdateAntiAddiction();
        }
    }

    public void UpdateAntiAddiction()
    {
        if(!IsInited())
            return;
        var instance = AntiAddictionManager.Instance;
        if (instance.rewardState == RewardState.Full)
        {
            m_antiAddiction.TrySetActive(false);
            m_buffListGo.TrySetActive(true);
        }            
        else if(instance.rewardState==RewardState.Half)
        {
            m_antiAddiction.TrySetActive(true);
            m_buffListGo.TrySetActive(false);
            m_money.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "halfMoney"));
            m_exp.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "halfExp"));
            m_medal.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "halfMedal"));
        }
        else
        {
            m_antiAddiction.TrySetActive(true);
            m_buffListGo.TrySetActive(false);
            m_money.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "zeroMoney"));
            m_exp.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "zeroExp"));
            m_medal.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "zeroMedal"));
        }
    }

    public int GetApplyNum()
    {
        return m_btgSocial.notice_num;
    }

    private void UpdateView()
    {
        var roleData = PlayerSystem.roleData;
        m_imgHead.SetRoleIconOrSelfIcon(roleData.icon, PhotoDataManager.IconUrl);
        if (String.IsNullOrEmpty(PhotoDataManager.IconUrl))
        {
           m_imgHead.SetNativeSize();
        }
        var heroType = HeroCardDataManager.getHeroMaxCardType();
        if(heroType != EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroType == EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (_headEffect == null)
                {
                    _headEffect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD,new Vector2(0, -3));
                }
            }
        }

        m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(roleData.level));
        m_textPlayerName.text = roleData.name;
        SetVipMember();
        m_txtLv.text = String.Format("{0}级",  roleData.level);
        m_txtDiamond.text = roleData.diamond.ToString();
        m_txtVoucher.text = roleData.eventcoin_002.ToString();
        m_txtCoin.text = roleData.coin.ToString();
        m_txtMedal.text = roleData.medal.ToString();
        var config = ConfigManager.GetConfig<ConfigRoleLevel>().GetLine(roleData.level + 1);
//        m_imgLvPgbar.fillAmount = config == null ? 1 :(float)roleData.exp / config.NeedExp;
        m_btgSocial.notice_num = PlayerFriendData.singleton.replyList.Count;
        m_btgMission.notice_num = PlayerMissionData.singleton.reward_num + PlayerAchieveData.m_rewardNum;
        UpdateBubble();
    }
    private void SetVipMember()
    {
//        m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = m_nameAnchorPos;
        if (MemberManager.IsHighMember())
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (MemberManager.IsMember())
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else
        {
            m_goVip.TrySetActive(false);
//            Vector2 vipSizeData = m_goVip.GetComponent<RectTransform>().sizeDelta;
//            m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_nameAnchorPos.x - vipSizeData.x / 2, m_nameAnchorPos.y);
        }
    }
    private void OnChangeMember()
    {
        SetVipMember();
    }
    public override void OnHide()
    {
        if (m_effect_player != null)
        {
            m_effect_player.Destroy();
            m_effect_player = null;
        }
        if (_headEffect != null)
        {
            _headEffect.Destroy();
            _headEffect = null;
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        if (m_effects != null)
        {
            for (int i = 0; i < m_effects.Count; i++)
            {
                m_effects[i].Destroy();
            }
            m_effects.Clear();
        }
    }

    public override void Update()
    {
        if (UIManager.CurContentPanel is PanelHallBattle)
        {
            if (UIManager.IsOpen<PanelLotteryNew>())//军火库使用返回按钮
                m_btnBack.interactable = true;
            else if (m_btnBack.IsInteractable())
                    m_btnBack.interactable = false;
        }
        else if (!m_btnBack.IsInteractable())
            m_btnBack.interactable = true;

        if (_update_time >= 0 && TimeUtil.GetNowTimeStamp() >= _update_time)
        {
            PlayerSystem.MockTocPlayerBuff();
            OnUpdateBuffHandler();
        }
        CorpsDataManager.CalcCorpsMatchStart();
        LegionDataManager.CheckLegionWarStart();
    }

    public string RandomName()
    {
        var config = ConfigManager.GetConfig<ConfigRoleName>();
        var first = config.m_dataArr[Random.Range(0, config.m_dataArr.Length)].firstName;
        var second = config.m_dataArr[Random.Range(0, config.m_dataArr.Length)].secondName;
        return string.Format("<color='#FF0000'>{0}{1}</color>", first, second);
    }

    public void ShowCurModelPanel()
    {
        if (m_showedPanelList.Count > 0)
        {
            UIManager.ShowPanel(m_showedPanelList.Pop());
        }
    }

    void OnClickHallSetting(GameObject target, PointerEventData eventData)
    {
        m_showedPanelList.Push(UIManager.CurContentPanel);
        UIManager.PopPanel<PanelSetting>(null, true);
        //object[] setBk = { true };
       //UIManager.PopPanel<PanelParamSetting>(setBk, true);
        //UIManager.PopPanel<PanelPCKeySetting>(null, true);
    }

    void Toc_player_notice(toc_player_notice data)
    {
        if (data.type == 2)
        {
            UIManager.ShowTv(data.msg);
        }
    }

    void OnNewChatMsg(ChatMsgTips tips)
    {
        m_goChatTips.TrySetActive(true);
        if (tips.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
            m_txtChannel.text = NetChatData.WorldChannelCrsSerOpen ? "本服" : "世界";
        }
        else if(tips.m_eChannel == CHAT_CHANNEL.CC_CRSSERVER)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
            m_txtChannel.text = NetChatData.WorldChannelCrsSerOpen ? "世界" : "跨服";
        }
        else if (tips.m_eChannel == CHAT_CHANNEL.CC_LABA)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
            m_txtChannel.text = "喇叭";
        }
        else if (tips.m_eChannel == CHAT_CHANNEL.CC_CORPS)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "team_channel"));
            m_txtChannel.text = "战队";
        }
        else if (tips.m_eChannel == CHAT_CHANNEL.CC_FRIEND)
        {
            if ((tips.m_rcd != null && FamilyDataManager.Instance.IsFamilyMember(tips.m_rcd.m_iPlayerId)))
            {
                var str = FamilyDataManager.Instance.GetRelativeName(tips.m_rcd.m_iPlayerId);
                m_imgChannel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,20 * str.Length);
                m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "friend_channel"));
                m_txtChannel.text = str;
            }
            else
            {
                m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "friend_channel"));
                m_imgChannel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 40);
                m_txtChannel.text = "好友";
            }
            
        }
        else if (tips.m_eChannel == CHAT_CHANNEL.CC_PRIVATE)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "friend_channel"));
            m_txtChannel.text = "私聊";
        }
        else if (tips.m_eChannel == CHAT_CHANNEL.CC_LEGION)
        {
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "legion_channel"));
            m_txtChannel.text = "军团";
        }
        int chineseNum = 0;
        int englishNum = 0;

        for (int i = 0; i < tips.m_strName.Length; i++)
        {
            if ((int)tips.m_strName[i] > 127)
                chineseNum++;
            else
                englishNum++;
        }
        var anchoredPosName = m_imgChannel.rectTransform.anchoredPosition;
        anchoredPosName.x += m_imgChannel.rectTransform.sizeDelta.x;
        m_transChatName.anchoredPosition = anchoredPosName;
        Vector2 sizeName = m_transChatName.sizeDelta;
        sizeName.x = 20f * chineseNum + 10f * englishNum + 20;
        m_transChatName.sizeDelta = sizeName;

        m_txtChatName.text = "[" + tips.m_strName + "]";
        if (tips.m_is_audio == true)
        {
            m_imgChatVoice.gameObject.TrySetActive(true);
            var pos = m_imgChatVoice.rectTransform.anchoredPosition;
            pos.x = anchoredPosName.x + sizeName.x;
            m_imgChatVoice.rectTransform.anchoredPosition = pos;
            anchoredPosName += m_imgChatVoice.rectTransform.sizeDelta;
        }
        else
        {
            m_imgChatVoice.gameObject.TrySetActive(false);
        }
        Vector2 anchoredPosMsg = m_transChatMsg.anchoredPosition;
        anchoredPosMsg.x = anchoredPosName.x + sizeName.x;
//        m_transChatMsg.anchoredPosition = anchoredPosMsg;

        string userName = null;
        if (tips.m_heroType == (int)EnumHeroCardType.super)
            userName = RightManager.USER_HERO_CARD_SUPER;
        else if (tips.m_heroType == (int)EnumHeroCardType.legend)
            userName = RightManager.USER_HERO_CARD_LEGEND;

        //当有换行符的情况下，截取第一行显示(xing)
        var line_index = !string.IsNullOrEmpty(tips.m_strChatMsg) ? tips.m_strChatMsg.IndexOf('\n') : -1;
        //string areaStr = (tips.m_area == "" || tips.m_area == null)   ?"" : "(" + tips.m_area + ")";
        var count = chineseNum*2 + englishNum;
        var msg = "<color='#00000000'>-----" + "</color>".PadLeft(8+count, '-') + (line_index != -1 ? tips.m_strChatMsg.Substring(0, line_index) : tips.m_strChatMsg);
        m_txtChatMsg.text = msg;
        m_motionText.Create(null, userName);
        m_chatMsgTips = tips;
    }

    static void Toc_player_send_mail_gift(toc_player_send_mail_gift data)
    {
        //TipsManager.Instance.showTips(data.msg);
        TipsManager.Instance.showTips("赠送成功！");
        return;
    }

    void Toc_player_set_icon(toc_player_set_icon data)
    {
        if (m_imgHead != null)
            m_imgHead.SetRoleIcon(PlayerSystem.roleData.icon);
    }


    static void Toc_player_new_mail(toc_player_new_mail data)
    {
        BubbleManager.SetBubble(BubbleConst.MailUnread, true);
    }
    //static void Toc_player_mail_list(toc_player_mail_list data)
    //{
    //    if (data.list.Length == 0)
    //    {
    //        m_hasNewMail = false;
    //    }
    //    else
    //    {
    //        m_hasNewMail = true;
    //    }

    //}


}
