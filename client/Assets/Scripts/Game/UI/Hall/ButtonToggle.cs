﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

class ButtonToggle
{
    private int _notice_num;
    private Transform _bubble_tip;

//    private Image m_uplight;
//    private Image m_downlight;
    private bool m_isChosen;
    private Toggle m_tog;
    private string m_name;

    public ButtonToggle(Transform tranBtn, string name)
    {
        _bubble_tip = tranBtn.Find("bubble_tip");
//        m_uplight = tranBtn.Find("Checkmark/uplight").GetComponent<Image>();
//        m_downlight = tranBtn.Find("Checkmark/downlight").GetComponent<Image>(); 
        m_isChosen = false;
        m_tog = tranBtn.GetComponent<Toggle>();
        m_name = name;
        SetUnChosen();
    }

    public int notice_num
    {
        set 
        { 
            _notice_num = value;
            if (_bubble_tip != null)
            {
                _bubble_tip.gameObject.TrySetActive(_notice_num > 0);
                if (_bubble_tip.gameObject.activeSelf)
                {
                    _bubble_tip.Find("Text").GetComponent<Text>().text = _notice_num.ToString();
                }
            }
        }
        get { return _notice_num; }
    }

    public Toggle Toggle
    {
        get { return m_tog; }
    }

    public void SetChosen()
    {
        m_isChosen = true;
//        m_uplight.enabled = true;
//        m_downlight.enabled = true;
        m_tog.isOn = true;
    }

    public void SetUnChosen()
    {
        m_isChosen = false;
//        m_uplight.enabled = false;
//        m_downlight.enabled = false;
    }





    





}

