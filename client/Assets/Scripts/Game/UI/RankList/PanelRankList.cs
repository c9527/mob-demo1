﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/*
public enum RANK_TYPE
{
    RT_SCORE = 0,
    RT_KILL = 1,
    RT_MVP = 2,
    RT_ACE = 3,
    RT_MILITARY = 4,
    RT_ACHEIVE = 5,
    RT_CORPS = 6,
    RT_LEGION = 7,
    RT_EQUIP = 8
}
*/

public class ItemRankData
{
    public long id;                  //> 玩家id
    public string name;             //> 玩家姓名
    public string corps_name;       //> 战队名
    public int val;                 //> 分值
    public int ext;                 //> 扩展值,暂时专指ACE榜的白银
    public int queue;               //> 排名
}


public class RankOptionType
{
    public const string LOOK_UP = "1";
    public const string ADD_FRIEND = "2";
}

public class ItemRankTitleData
{
    public int rank_type;
}
public class PanelRankList : BasePanel
{
    private static int[] m_iCurPageNum = {  0, 0, 0, 0, 0, 0,0,0,0 };
    private static int[] m_iCurFndPageNum = { 0, 0, 0, 0, 0, 0,0,0,0 };
    private static int NUM_OF_PAGE = 30;
    private static int PAGE_MAX = 10;
    private static List<ItemRankTitleData> m_lstRankTitle = new List<ItemRankTitleData>();

    private DataGrid m_titleDataGrid;
    private int page;//显示排行榜的列数

    class ItemList
    {
        public List<ItemRankData> m_lstItem;
        public float m_fTime;
    }

    private ItemDropInfo[] _optionList;

    public ItemDropInfo[] OptionList
    {
        get { return _optionList; }
    }


    private static Dictionary<int, Dictionary<int, ItemList>> m_dicRankData = new Dictionary<int, Dictionary<int, ItemList >>();
    public static int m_curRankType = 0;

    private static List<ConfigRanklistLine> m_lstRankListLine = new List<ConfigRanklistLine>();
    private Dictionary<string,List<int>> m_rewardStartRank;
    private Dictionary<string, List<ConfigRanklistRewardLine>> m_rewardData;
    private static List<string> m_lstRankName = new List<string>();
    private static Dictionary<int,ItemRankData> m_dicMyRankData = new Dictionary<int,ItemRankData>();
    private List<ItemRankData> m_lstViewData;
    private DataGrid m_dataGrid;
    private Text m_txtPageHit;
    private Text m_txtRoleName;
    private Text m_txtMyScore;
    private Text m_txtMyQueue;
    private Text m_txtValueName;
    private Text m_txtRankName;
    private Text m_txtFriendOnly;
    private static bool m_bFriendOnly = false;
    public GameObject dropTarget;
    public static Action OnBackCallBack;

    private static Dictionary<int, ItemList> m_dicFriendRankData = new Dictionary<int, ItemList>();
    private static int[] m_arrPageNumMax = { 0, 0, 0, 0, 0, 0 ,0,0,0};
    private static int[] m_arrFriendPageNumMax = { 0, 0, 0, 0, 0, 0,0,0,0 };
    //private static string[] m_arrValueName = { "积分", "消灭数", "MVP数", "黄金/白银", "军衔等级", "成就点数","战队周贡献","军团周贡献","武器等级"};
    private static float UPDATE_TIME = 60f;
//    private static Image m_titlerank;
    private GameObject m_goFriendOnly;
    private bool m_setMyPosition = false;
    private GameObject m_armyrank;
    private GameObject m_goCorpsRank;
    private Vector2 m_origCorpsAnchorPos;
    private Text m_txtAdd;
    private Text m_txtMy;
    private Text m_txtShortInfo;
    private Image m_rewardPic;
    private Text m_isGetReward;

    public const int NEW_RANKLIST = 0x1;
    public const int NORMAL_RANKLIST = 0x2;
    public const int CORPS_RANKLIST = 0x4;
    public const int ENTERTAIN_RANKLIST = 0x08;
    public PanelRankList()
    {
        SetPanelPrefabPath("UI/RankList/PanelRankList");
        AddPreLoadRes("UI/RankList/PanelRankList", EResType.UI);
        AddPreLoadRes("UI/RankList/ItemRankListInfo", EResType.UI);
        AddPreLoadRes("UI/RankList/ItemRankTitle", EResType.UI);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
    }

    public static ConfigRanklistLine GetRanklistLine( int eType )
    {
        if (m_lstRankListLine.Count > eType)
        {
            return m_lstRankListLine[eType];
        }
        return null;
    }
    public override void Init()
    {
        m_lstViewData = new List<ItemRankData>();
        m_rewardData = new Dictionary<string, List<ConfigRanklistRewardLine>>();
        m_rewardStartRank = new Dictionary<string, List<int>>();

        
        dropTarget = m_tran.Find("dropTarget").gameObject;
        m_dataGrid = m_tran.Find("Frame/Right/MiddleList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/RankList/ItemRankListInfo"), typeof(ItemRankListInfo));
        m_dataGrid.useLoopItems = true;

        m_titleDataGrid = m_tran.Find("Frame/Left/RankTitle/content").gameObject.AddComponent<DataGrid>();
        m_titleDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/RankList/ItemRankTitle"), typeof(ItemRankTitle));
        m_titleDataGrid.onItemSelected = OnSelectRankTitle;

        m_txtPageHit = m_tran.Find("Frame/Right/BottomPageHit/cur_page").GetComponent<Text>();
        m_txtMyScore = m_tran.Find("Frame/Right/Score/MyScore/Value").GetComponent<Text>();
        m_txtMyQueue = m_tran.Find("Frame/Right/Score/MyRank/Text").GetComponent<Text>();
        m_txtValueName = m_tran.Find("Frame/Right/MiddleList/Title/Score/Text").GetComponent<Text>();
        m_txtRankName = m_tran.Find("Frame/Right/MiddleList/Title/Name/Text").GetComponent<Text>();
        m_txtFriendOnly = m_tran.Find("Frame/Right/Operate/FriendOnly/Text").GetComponent<Text>();
        m_goFriendOnly = m_tran.Find("Frame/Right/Operate/FriendOnly").gameObject;

        m_txtAdd = m_tran.Find("Frame/Right/Operate/AddFriend/Text").GetComponent<Text>();
        m_txtMy = m_tran.Find("Frame/Right/Operate/MyPosition/Text").GetComponent<Text>();
        m_txtShortInfo = m_tran.Find("Frame/Right/Reward/text/rule").GetComponent<Text>();
        m_rewardPic = m_tran.Find("Frame/Right/Reward/item_pic").GetComponent<Image>();
        m_isGetReward = m_tran.Find("Frame/Right/Reward/text/can_get_reward").GetComponent<Text>();


        _optionList = new ItemDropInfo[]
        {
            new ItemDropInfo { type = "RankList", subtype = RankOptionType.LOOK_UP, label = "查看信息" }, 
            new ItemDropInfo { type = "RankList", subtype = RankOptionType.ADD_FRIEND, label = "加为好友" },
        };

        page = 0;

        var rewardDataList = ConfigManager.GetConfig<ConfigRanklistReward>();
        foreach (var rankRewardLine in rewardDataList.m_dataArr)
        {
            if (!m_rewardStartRank.ContainsKey(rankRewardLine.Name))
            {
               m_rewardStartRank[rankRewardLine.Name] = new List<int>();
            }
            m_rewardStartRank[rankRewardLine.Name].Add(rankRewardLine.BeginRank);
            if (!m_rewardData.ContainsKey(rankRewardLine.Name))
            {
                m_rewardData[rankRewardLine.Name] = new List<ConfigRanklistRewardLine>();
            }
            m_rewardData[rankRewardLine.Name].Add(rankRewardLine);
        }

    }
    
    public override void Update()
    {
        foreach ( var dic in m_dicRankData )
        {
            foreach( var item in dic.Value )
            {
                var itemList = item.Value;
                itemList.m_fTime -= Time.deltaTime;
            }
        }

        foreach( var dic in m_dicFriendRankData )
        {
            var itemList = dic.Value;
            itemList.m_fTime -= Time.deltaTime;
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/BottomPageHit/pre_btn").gameObject).onPointerClick += OnClkPageup;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/BottomPageHit/next_btn").gameObject).onPointerClick += OnClkPagedown;

        UGUIClickHandler.Get(m_tran.Find("Frame/Right/Operate/LookUp").gameObject).onPointerClick += OnLookUp;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/Operate/LookNo1").gameObject).onPointerClick += OnSelectFirst;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/Operate/MyPosition").gameObject).onPointerClick += OnMyPosition;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/Operate/AddFriend").gameObject).onPointerClick += OnAddFriend;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/Operate/FriendOnly").gameObject).onPointerClick += OnFriendOnly;
        UGUIClickHandler.Get(m_tran.Find("Frame/Right/LongInfo").gameObject).onPointerClick += OnExplain;
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
    }

    public override void OnShow()
    {
        m_curRankType = 0;
        dropTarget.TrySetActive(false);
        m_bFriendOnly = false;
        ResetFriendOnly();

        InitRankInfo();

        m_titleDataGrid.Data = m_lstRankTitle.ToArray();
        UpdateView();
    }

    public override void OnHide()
    {
        m_bFriendOnly = false;
        ResetFriendOnly();

        m_dicRankData.Clear();
        m_dicMyRankData.Clear();
        m_dicFriendRankData.Clear();
        m_lstRankName.Clear();
        m_lstRankListLine.Clear();
        m_lstRankTitle.Clear();

    }

    public void DoOption(string subType, ItemRankData m_data)
    {
        if (subType == RankOptionType.ADD_FRIEND)
        {
            OnAddFriend(null, null);
        }
        else if (subType == RankOptionType.LOOK_UP)
        {
            OnLookUp(null, null);
        }
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    public override void OnBack()
    {

        if (OnBackCallBack != null)
        {
            OnBackCallBack();
            OnBackCallBack = null;
        }
        else
        {
            UIManager.ShowPanel<PanelHallBattle>();
        }
    }

    public override void OnDestroy()
    {
    }

    protected void UpdateView()
    {
        int pageNum = m_iCurPageNum[(int)m_curRankType];
        if( !m_bFriendOnly )
        {
            if (!HasRankData(m_curRankType, pageNum))
            {
                m_lstViewData.Clear();
            }
            else
            {
                if (!m_dicRankData.ContainsKey(m_curRankType))
                    return;
                Dictionary<int, ItemList> dicPageRankData = m_dicRankData[m_curRankType];
                List<ItemRankData> lstRankData = dicPageRankData[pageNum].m_lstItem;
                m_lstViewData = lstRankData;
            }
        }
        else
        {
            if (!HasRankData(m_curRankType, pageNum))
            {
                m_lstViewData.Clear();
            }
            else
            {
                if (!m_dicFriendRankData.ContainsKey(m_curRankType))
                    return;
                m_lstViewData = m_dicFriendRankData[m_curRankType].m_lstItem;
            }
        }

        ConfigRanklistLine line = GetRanklistLine(m_curRankType);
        m_txtValueName.text = line.DataName;
        ItemRankData myInfo = null;
        if (m_dicMyRankData.ContainsKey(m_curRankType))
        {
            myInfo = m_dicMyRankData[m_curRankType];
        }
        else
        {
            myInfo = new ItemRankData();
        }
           
        if (myInfo.queue == 0)
        {
            m_txtMyQueue.text = "未上榜";
        }
        else
        {
            m_txtMyQueue.text = "榜上第" + myInfo.queue.ToString() + "名";
        }
        if (line.Name == "ace")
        {
            m_txtMyScore.text = "我的" + m_txtValueName.text + ": " + myInfo.val.ToString() + "/" + myInfo.ext.ToString();
        }
        else
        {
            m_txtMyScore.text = "我的" + m_txtValueName.text + ": "+ myInfo.val.ToString();
        }
        SetRewardTextAndImage(line, myInfo.queue);
        string page = GetCurPage(m_curRankType).ToString() + "/" + GetPageMax(m_curRankType).ToString();
        m_dataGrid.Data = m_lstViewData.ToArray();
        m_txtPageHit.text = page;

        if (line.Name == "corps")
        {
            m_txtRankName.text = "战队名";
            m_txtAdd.text = "申请加入";
            m_txtMy.text = "我的战队";
            m_goFriendOnly.TrySetActive(false);
        }
        else
        {
            m_txtRankName.text = "玩家名";
            m_txtAdd.text = "加为好友";
            m_txtMy.text = "我的位置";
            m_goFriendOnly.TrySetActive(true);
        }

        int idx = (int)m_curRankType;
        ConfigRanklistLine rankListLine = m_lstRankListLine[(int)m_curRankType];
        if (rankListLine != null)
            m_txtShortInfo.text = rankListLine.ShortInfo;
        else
            m_txtShortInfo.text = "";
        
        if (!m_bFriendOnly)
        {
            if (m_setMyPosition)
            {
                OnMyPosition(null, null);
            }
        }
    }

    private void SetRewardTextAndImage(ConfigRanklistLine line, int queue)
    {
        var hasReward = m_rewardStartRank.ContainsKey(line.Name);
        if (queue != 0 && hasReward)
        {
            m_isGetReward.text = "当前排名可获奖励";
            var startRankList = m_rewardStartRank[line.Name];
            int index = -1;
            for (int i = startRankList.Count - 1; i >= 0; i--)
            {
                if (queue >= startRankList[i])
                {
                    index = i;
                    break;
                }
            }
            if (index == -1)
            {
                return;
            }
            var rankRewardList = m_rewardData[line.Name];
            var items = rankRewardList[index].RewardList;
            var itemData = ItemDataManager.GetItem(items[0].ID);
            m_rewardPic.SetSprite(ResourceManager.LoadIcon(itemData.Icon), itemData.SubType);
            m_rewardPic.gameObject.TrySetActive(true);
        }
        else
        {
            m_rewardPic.gameObject.TrySetActive(false);
            m_isGetReward.text = hasReward ? "当前排名无奖励" : "此排行榜无奖励";
        }
    }

    private void OnSelectRankTitle(object renderData)
    {
        var data = renderData as ItemRankTitleData;
        m_curRankType = data.rank_type;
        m_bFriendOnly = false;
        ResetFriendOnly();
        RequestData(m_curRankType, 1);
    }

    protected void OnClkRankTitleLeft(GameObject target, PointerEventData eventData)
    {
        page = page - 1;
        if (page == 0)
        {
            m_titleDataGrid.Select(0);
            m_titleDataGrid.ResetScrollPosition(0);
        }
        else
        {
            m_titleDataGrid.Select(5*page);
            m_titleDataGrid.ResetScrollPosition(5 * page);
        }
    }

    protected void OnClkRankTitleRight(GameObject target, PointerEventData eventData)
    {
        page = page + 1;
        if (page == m_lstRankListLine.Count / 5)
        {
            m_titleDataGrid.Select(m_lstRankTitle.Count - m_lstRankListLine.Count % 5);
            if (m_lstRankListLine.Count % 5 == 0)
            {
                m_titleDataGrid.ResetScrollPosition(m_lstRankTitle.Count - 1 - m_lstRankListLine.Count % 5);
            }
            else
            {
                m_titleDataGrid.horizonPos = (float)((page + 1) * 5) / ((float)m_lstRankListLine.Count - 1);
            }
        }
        else
        {
            m_titleDataGrid.Select(5 * page);
            m_titleDataGrid.ResetScrollPosition(5 * page);
        }
        
    }
   
    protected void OnClkPageup(GameObject target, PointerEventData eventData)
    {
        int iPage = GetCurPage( m_curRankType );
        if (iPage <= 1)
            return;
        iPage -= 1;
        RequestData(m_curRankType, iPage);
    }

    protected void OnClkPagedown(GameObject target, PointerEventData eventData)
    {
        int iPage = GetCurPage(m_curRankType); ;
        int iPageMax = GetPageMax(m_curRankType);
        if (iPage >= iPageMax)
            return;

        iPage += 1;
        RequestData(m_curRankType, iPage);

    }


    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }


    protected void OnMyPosition(GameObject target, PointerEventData eventData)
    {
        if (m_bFriendOnly)
            return;

        if (m_dicMyRankData.ContainsKey(m_curRankType))
        {
            ItemRankData myInfo = m_dicMyRankData[m_curRankType];
            if (myInfo != null)
            {
                if (myInfo.queue == 0)
                {
                    UIManager.ShowTipPanel("很遗憾你未上榜");
                }
                else
                {
                    int iPage = myInfo.queue / NUM_OF_PAGE;
                    int j = myInfo.queue % NUM_OF_PAGE;
                    if (j > 0)
                        iPage++;

                    if (HasRankData(m_curRankType, iPage))
                    {
                        if (m_iCurFndPageNum[(int)m_curRankType] != iPage)
                        {
                            m_setMyPosition = false;
                            SetCurPage(m_curRankType, iPage);
                            ShowRankList();
                        }

                        int idx = myInfo.queue % NUM_OF_PAGE;
                        if (idx == 0)
                            idx = NUM_OF_PAGE;

                        m_dataGrid.Select(idx - 1);
                        m_dataGrid.ResetScrollPosition(idx - 1);
                        
                    }
                    else
                    {
                        SendMsgToGetData(m_curRankType, iPage);
                        m_setMyPosition = true;
                    }                 
                }
            }
        }
    }

    protected void OnSelectFirst(GameObject target, PointerEventData eventData)
    {
        if (m_bFriendOnly)
            return;

        int iPage = GetCurPage(m_curRankType);
        if(iPage == 1)
        {
            m_dataGrid.ResetScrollPosition();
            m_dataGrid.Select(0);
            return;
        }
        if (!HasRankData(m_curRankType, 1))
        {
            SendMsgToGetData(m_curRankType, 1);
        }
        else
        {
            SetCurPage(m_curRankType, 1);
            ShowRankList();
            m_dataGrid.ResetScrollPosition();
        }

      
    }

    protected void OnLookUp(GameObject target, PointerEventData eventData)
    {
        var item = m_dataGrid.SelectedData<ItemRankData>();
        ConfigRanklistLine line = GetRanklistLine(m_curRankType);
        if (line.Name == "corps")
        {
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择战队");
                return;
            }

            tos_corps_view_info msg = new tos_corps_view_info();
            msg.corps_id = item.id;
            NetLayer.Send(msg);
        }
        else
        {
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择一个玩家");
                return;
            }

            if (item.id == PlayerSystem.roleId)
            {
                return;
            }

            PlayerFriendData.ViewPlayer(item.id);
        }
    }

    private void Toc_corps_view_info(toc_corps_view_info proto)
    {
        UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, this);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

     private void OnExplain(GameObject target, PointerEventData eventData)
     {
         //string str = "每日00:00将排名奖励发至邮箱，并清零本日榜单数据\n\n奖励：\n 第1名：水晶AK47 \n 第2名：黄金MP5 \n 第3名：qbz95 \n 第4~1200名：大量奖章";

         string str = "";
         string strNew = "";
         ConfigRanklistLine rankListLine = m_lstRankListLine[(int)m_curRankType];
         if (rankListLine != null)
             str = rankListLine.LongInfo;
        strNew = str.Replace( "\\n","\n");
        UIManager.ShowTipMulTextPanel(strNew);
     }

     protected void OnAddFriend(GameObject target, PointerEventData eventData)
     {
         var item = m_dataGrid.SelectedData<ItemRankData>();
         ConfigRanklistLine line = GetRanklistLine(m_curRankType);

         if (line.Name == "corps" )
         {
             if (item == null)
             {
                 TipsManager.Instance.showTips("请选择一个战队");
                 return;
             }

             if( CorpsDataManager.IsJoinCorps() )
             {
                 TipsManager.Instance.showTips("请先退出战队");
                 return;
             }

             tos_corps_apply_enter msg = new tos_corps_apply_enter();
             msg.corps_id = item.id;
             NetLayer.Send(msg);

             TipsManager.Instance.showTips("申请已发送！");
         }
         else
         {
             if (item == null)
             {
                 UIManager.ShowTipPanel("请选择一个玩家");
                 return;
             }

             if (item.id == PlayerSystem.roleId)
             {
                 return;
             }

             if (m_bFriendOnly)
             {
                 UIManager.ShowTipPanel("该玩家已经是你的好友了!");
                 return;
             }
             PlayerFriendData.AddFriend(item.id);
             //TipsManager.Instance.showTips("你已经发送了好友申请！");
         }
       
     }

    protected void ResetFriendOnly()
    {
         if (m_bFriendOnly)
         {
             m_goFriendOnly.GetComponent<Toggle>().isOn = true;
             m_txtFriendOnly.text = "查看全部";
         }
         else
         {
             m_goFriendOnly.GetComponent<Toggle>().isOn = false;
             m_txtFriendOnly.text = "只看好友";
         }
     }
     protected void OnFriendOnly(GameObject target, PointerEventData eventData)
     {
         m_bFriendOnly = !m_bFriendOnly;
         ResetFriendOnly();
         RequestData(m_curRankType, 1);
     }

    public static void RequestData( int eType,int iPage )
    {

//        if( m_lstRankName.Count == 0 )
//        {
//            ConfigRanklist cfgRankList = ConfigManager.GetConfig<ConfigRanklist>();
//            for (int i = 0; i < cfgRankList.m_dataArr.Length; i++)
//            {
//                if ((cfgRankList.m_dataArr[i].Special & rank_mask) != 0 && cfgRankList.m_dataArr[i].Special!=4)
//                    m_lstRankName.Add(cfgRankList.m_dataArr[i].Name);
//            }
//            m_arrPageNumMax = new int[m_lstRankName.Count];
//            m_arrFriendPageNumMax = new int[m_lstRankName.Count];
//            m_iCurPageNum = new int[m_lstRankName.Count];
//            m_iCurFndPageNum = new int[m_lstRankName.Count];
//        }
//        if( m_dicMyRankData.Count == 0 )
//        {
//            tos_player_my_ranks msg = new tos_player_my_ranks();
//            NetLayer.Send(msg);
//        }

        if(HasRankData( eType,iPage ) )
        {
            SetCurPage(eType, iPage);
            ShowRankList();
        }
        else
        {
            SendMsgToGetData(eType, iPage);
        }
    }

    private static void InitRankInfo(int rankMask = 0)
    {
        var roleData = PlayerSystem.roleData;
        if (rankMask == 0)
        {
            rankMask = roleData.level <= 15 ? NEW_RANKLIST : NORMAL_RANKLIST;
        }
        if (m_lstRankTitle.Count == 0)
        {
            ConfigRanklist cfgRankList = ConfigManager.GetConfig<ConfigRanklist>();
            int j = 0;
            for (int i = 0; i < cfgRankList.m_dataArr.Length; i++)
            {
                if ((cfgRankList.m_dataArr[i].Special & rankMask) != 0)
                {
                    m_lstRankName.Add(cfgRankList.m_dataArr[i].Name);
                    m_lstRankListLine.Add(cfgRankList.m_dataArr[i]);
                    m_lstRankTitle.Add(new ItemRankTitleData() { rank_type = j });
                    j++;
                }
            }
            m_arrPageNumMax = new int[m_lstRankName.Count];
            m_arrFriendPageNumMax = new int[m_lstRankName.Count];
            m_iCurPageNum = new int[m_lstRankName.Count];
            m_iCurFndPageNum = new int[m_lstRankName.Count];
        }
        if (m_dicMyRankData.Count == 0)
        {
            tos_player_my_ranks msg = new tos_player_my_ranks();
            NetLayer.Send(msg);
        }
    }

    public static void OpenPanel(int rankMask = 0)
    {
        InitRankInfo(rankMask);
        SendMsgToGetData(0, 1);
    }

    private static void SetCurPage( int eType,int iPage )
    {
        if( !m_bFriendOnly )
        {
            m_iCurPageNum[(int)eType] = iPage;
        }
        else
        {
            m_iCurFndPageNum[(int)eType] = iPage;
        }
    }

    private static int GetCurPage( int eType )
    {
        if (!m_bFriendOnly)
        {
            return m_iCurPageNum[eType];
        }
        else
        {
            return m_iCurFndPageNum[eType] ;
        }
    }

    private static int GetPageMax( int eType )
    {
        if (!m_bFriendOnly)
        {
            return m_arrPageNumMax[eType];
        }
        else
        {
            return m_arrFriendPageNumMax[eType];
        }
    }
    private static void SendMsgToGetData( int eType,int iPage )
    {
        if( !m_bFriendOnly )
        {
            tos_player_rank_list msg = new tos_player_rank_list();
            msg.name = m_lstRankName[eType];
            msg.page = iPage;
            NetLayer.Send(msg);
        }
        else
        {
            tos_player_friend_ranklist msg = new tos_player_friend_ranklist();
            msg.name = m_lstRankName[eType];
            NetLayer.Send(msg);
        }
    }
    public static bool HasRankData( int eType,int iPage )
    {
        if( !m_bFriendOnly )
        {
            if (!m_dicRankData.ContainsKey(eType))
                return false;

            Dictionary<int, ItemList> dicPageRankData = m_dicRankData[eType];
            if (!dicPageRankData.ContainsKey(iPage))
                return false;

            ItemList itemList = dicPageRankData[iPage];
            if (itemList.m_fTime <= 0f)
                return false;

            //List<ItemRankData> lstRankData = itemList.m_lstItem;
            if (itemList.m_lstItem.Count <= 0)
                return false;
        }
        else
        {
            if (!m_dicFriendRankData.ContainsKey(eType))
                return false;
            ItemList itemList = m_dicFriendRankData[eType];
            if (itemList.m_fTime <= 0f)
                return false;

            List<ItemRankData> lstFndRankData = itemList.m_lstItem;
            if (lstFndRankData.Count == 0)
                return false;
        }

        return true;
    }

    private static void Toc_player_rank_list(toc_player_rank_list proto)
    {
        int idx = m_lstRankName.IndexOf(proto.name);
        if (idx <= -1)
            return;

        int iPageMax = proto.size / NUM_OF_PAGE;
        if( proto.size % NUM_OF_PAGE > 0 )
        {
            iPageMax++;
        }
        m_arrPageNumMax[idx] = iPageMax;

        int iPage = proto.page;
        int eType = idx;
        Dictionary<int, ItemList> dicPageRankData;
        List<ItemRankData> lstRankData = null;
        if( m_dicRankData.ContainsKey( eType ) )
        {
            dicPageRankData = m_dicRankData[eType];
            if( dicPageRankData.ContainsKey(iPage) )
            {
                lstRankData = dicPageRankData[iPage].m_lstItem;
            }
            else
            {
                lstRankData = new List<ItemRankData>();
                dicPageRankData[iPage] = new ItemList();
                dicPageRankData[iPage].m_lstItem = lstRankData;
            }
            dicPageRankData[iPage].m_fTime = UPDATE_TIME;
        }
        else
        {
            dicPageRankData = new Dictionary<int, ItemList>();
            m_dicRankData[eType] = dicPageRankData;
            dicPageRankData[iPage] = new ItemList();

            lstRankData = new List<ItemRankData>();
            dicPageRankData[iPage].m_lstItem = lstRankData;
            dicPageRankData[iPage].m_fTime = UPDATE_TIME;
        }
        lstRankData.Clear();
        int iOrgLenth = (iPage -1) * NUM_OF_PAGE;

        for( int i = 0; i < proto.list.Length ; ++i )
        {
            ItemRankData data = new ItemRankData();
            data.id = proto.list[i].id;
            data.name = proto.list[i].name;
            data.corps_name = proto.list[i].corps_name;
            data.val = proto.list[i].val;
            data.ext = proto.list[i].ext;
            data.queue = iOrgLenth + i + 1;
            lstRankData.Add(data);
        }
        if (lstRankData.Count > 0)
        {
            SetCurPage(m_curRankType, iPage);
        }
       
        ShowRankList();

       
    }

    private static void Toc_player_friend_ranklist( toc_player_friend_ranklist proto )
    {
        int idx = m_lstRankName.IndexOf(proto.name);
        if (idx <= -1)
            return;

        int iPageMax = proto.list.Length / NUM_OF_PAGE;
        if (proto.list.Length % NUM_OF_PAGE > 0)
        {
            iPageMax++;
        }
        m_arrFriendPageNumMax[idx] = iPageMax;

        List<ItemRankData> lstRankData = null;
        if( m_dicFriendRankData.ContainsKey(m_curRankType) )
        {
            lstRankData = m_dicFriendRankData[m_curRankType].m_lstItem;
        }
        else
        {
            lstRankData = new List<ItemRankData>();
            ItemList itemList = new ItemList();
            m_dicFriendRankData[m_curRankType] = itemList;
            itemList.m_lstItem = lstRankData;
            itemList.m_fTime = UPDATE_TIME;
        }

        lstRankData.Clear();

        for (int i = 0; i < proto.list.Length; ++i)
        {
            ItemRankData data = new ItemRankData();
            data.id = proto.list[i].id;
            data.name = proto.list[i].name;
            data.corps_name = proto.list[i].corps_name;
            data.val = proto.list[i].val;
            data.ext = proto.list[i].ext;
            data.queue = proto.list[i].rank;
            lstRankData.Add(data);
        }

        lstRankData.Sort((x, y) => x.queue - y.queue);
        if (lstRankData.Count > 0 )
        {
            SetCurPage(m_curRankType, 1);
        }
       
        ShowRankList();
    }
    private static void Toc_player_my_ranks(toc_player_my_ranks proto)
    {
        for( int i = 0; i < proto.ranks.Length; ++i )
        {
            int idx = m_lstRankName.IndexOf(proto.ranks[i].name);
            if (idx <= -1)
                continue;
            ItemRankData rankData = new ItemRankData();
            rankData.queue = proto.ranks[i].rank;
            rankData.val = proto.ranks[i].val;
            rankData.ext = proto.ranks[i].ext;
            m_dicMyRankData[idx] = rankData;
        }
    }
    private static void ShowRankList()
    {
        if (UIManager.IsOpen<PanelRankList>())
        {
            UIManager.GetPanel<PanelRankList>().UpdateView();
        }
        else if (CanShowRankList())
        {
            UIManager.ShowPanel<PanelRankList>();
        }
    }

    private static bool CanShowRankList()
    {
        return UIManager.IsOpen<PanelHallBattle>() || UIManager.IsOpen<PanelTeam>();
    }


}
