﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemRankTitle : ItemRender
{
    private ItemRankTitleData m_data;
    private Text m_txtName;
    public override void Awake()
    {
        m_txtName = transform.Find("Label").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ItemRankTitleData;
        //m_txtName.text = m_data.name;
        ConfigRanklistLine line = PanelRankList.GetRanklistLine(m_data.rank_type);
        if (line != null)
            m_txtName.text = line.DispName;
    }
}
