﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemRankListInfo : ItemRender
{
    private ItemRankData m_data;

    private Text m_txtQueue;
    private Text m_txtName;
    // private Text m_txtCorps;
    private Text m_txtScore;
    public override void Awake()
    {
        m_txtQueue = transform.Find("queue").GetComponent<Text>();
        m_txtName = transform.Find("name").GetComponent<Text>();
        //m_txtCorps = transform.Find("corps").GetComponent<Text>();
        m_txtScore = transform.Find("score").GetComponent<Text>();
        UGUIClickHandler.Get(transform).onPointerClick += OnShowDropList;
    }

    private void OnShowDropList(GameObject target, PointerEventData eventData)
    {
        UIManager.GetPanel<PanelRankList>().dropTarget.TrySetActive(true);
        UIManager.ShowDropList(UIManager.GetPanel<PanelRankList>().OptionList, ItemDropCallBack, UIManager.GetPanel<PanelRankList>().transform, target.transform, new Vector2(0, 0), UIManager.Direction.Down, 132, 45, UIManager.DropListPattern.Pop);
    }

    private void ItemDropCallBack(ItemDropInfo info)
    {
        var panel = UIManager.GetPanel<PanelRankList>();
        if (panel != null)
            panel.DoOption(info.subtype, m_data);
        UIManager.GetPanel<PanelRankList>().dropTarget.TrySetActive(false);
    }

    protected void OnSetFontColor(GameObject target, PointerEventData eventData)
    {
        transform.Find("name").GetComponent<Text>().color= new Color(255, 240, 203);
        transform.Find("score").GetComponent<Text>().color= new Color(255, 240, 203);
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ItemRankData;
        int queue = m_data.queue;

        m_txtQueue.text = "";
        if (queue > 99)
        {
            m_txtQueue.text = queue.ToString();
            transform.Find("queue/icon").gameObject.TrySetActive(false);
            transform.Find("queue/Num").gameObject.TrySetActive(false);
        }
        else if (queue > 9)
        {
            transform.Find("queue/icon").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue / 10)));
            transform.Find("queue/icon").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/icon").gameObject.TrySetActive(true);
            transform.Find("queue/Num").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue % 10)));
            transform.Find("queue/Num").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/Num").gameObject.TrySetActive(true);
            
        }
        else
        {
            transform.Find("queue/icon").gameObject.TrySetActive(false);
            transform.Find("queue/Num").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, queue < 4 ? "rank" + queue : "rank_" + queue));
            transform.Find("queue/Num").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/Num").gameObject.TrySetActive(true);
        }
        m_txtName.text = m_data.name;
        ConfigRanklistLine line = PanelRankList.GetRanklistLine(PanelRankList.m_curRankType);

        if (line != null && line.Name == "ace")
        {
            m_txtScore.text = m_data.val.ToString() + "/" + m_data.ext.ToString();
        }
        else
        {
            m_txtScore.text = m_data.val.ToString();
        }
        
    }
}
