﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PanelMatchingTips : BasePanel
{
    RectTransform m_rectTxtMask;
    float m_fCostTime = 0;
    float m_fGuessTime = 0;
    Text m_txtCostTime;
    Text m_txtPreTime;
    bool m_isNation;
    bool m_canGoNation;
    public static uint m_sid;

    public PanelMatchingTips()
    {
        SetPanelPrefabPath("UI/Team/PanelMatchingTips");
        AddPreLoadRes("UI/Team/PanelMatchingTips", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);

    }
    public override void Init()
    {
        m_rectTxtMask = m_tran.Find("frame/dot").gameObject.AddMissingComponent<RectTransform>();
        Vector2 size = m_rectTxtMask.sizeDelta;

        m_txtCostTime = m_tran.Find("frame/costtime/Text").GetComponent<Text>();
        //m_txtPreTime = m_tran.Find("").GetComponent<Text>();
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/Button")).onPointerClick += OnCloseMatching;
    }
    public override void OnShow()
    {
        UIManager.HidePanel<PanelMainPlayerInfo>();
        UIManager.HidePanel<PanelSetting>();
        UIManager.HidePanel<PanelMail>();
        UIManager.HidePanel<PanelInviteFriend>();
        m_fCostTime = 0;
        m_isNation = false;
        m_canGoNation = true;
        if(RoomModel.RoomInfo != null)
        {
            int channelTemp = RoomModel.RoomInfo.channel;
            if (channelTemp == (int)EnumChannelNation.NATION_PHONE || channelTemp == (int)EnumChannelNation.NATION_PC_WEB)
                m_isNation = true;
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/Button")).onPointerClick -= OnCloseMatching;
    }
    public override void Update()
    {
        m_fCostTime += Time.deltaTime;
        int hour =(int)m_fCostTime / 3600;
        int minute = ((int)m_fCostTime - hour * 3600)/60;
        int second = (int)m_fCostTime % 60;
        m_txtCostTime.text = string.Format(GetTimeStr(hour) + ":" + GetTimeStr(minute) + ":" + GetTimeStr(second));
        if (!GlobalConfig.serverValue.nation_channel_default)
        {
            if (RoomModel.RoomInfo != null)
            {
                if (RoomModel.RoomInfo.players.Length == 1 && !m_isNation && m_canGoNation)
                {
                    if (m_fCostTime > 60)
                    {
                        UIManager.ShowTipPanel("是否切换至国际频道进行匹配？", () =>
                        {
                            var channelTemp = 0;
                            if (Driver.m_platform == EnumPlatform.android || Driver.m_platform == EnumPlatform.ios)
                                channelTemp = (int)EnumChannelNation.NATION_PHONE;
                            else if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
                                channelTemp = (int)EnumChannelNation.NATION_PC_WEB;
                            tos_base proto = new tos_room_match_change_channel() { channel = channelTemp };
                            NetLayer.Send(proto);
                            m_sid = proto.sid;
                            HidePanel();
                            m_isNation = true;
                            TipsManager.Instance.showTips("已切换至国际频道");
                        }, () => { m_canGoNation = false; }, true, true, "确定", "取消");
                    }
                }
            }
        }         
    }

    string GetTimeStr(int time)
    {
        if (time < 10)
        {
            return "0" + time.ToString();
        }
        else
        {
            return time.ToString();
        }
    }

    void OnCloseMatching( GameObject target, PointerEventData eventData )
    {
        //if( (bool)m_params[0] == true )
        //{
        //    SendTeamMsg("stop_match");
        //}
        //else
        //{
        //    SendTeamMsg("leave_team");
        //}
        NetLayer.Send(new tos_room_cancel_match());
        HidePanel();
    }


    
}
