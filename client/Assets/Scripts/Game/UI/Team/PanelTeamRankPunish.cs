﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;

public class PanelTeamRankPunish : BasePanel
{
    private Text m_txtContent;
    private Text m_txtTitle;
    private Text m_txtTimeRule;
    private Text m_txtReleaseTime;
    private Button m_btnClose;
    private roomPunish m_punish;

    public PanelTeamRankPunish()
    {
        SetPanelPrefabPath("UI/Common/PanelTeamRankPunish");
        AddPreLoadRes("UI/Common/PanelTeamRankPunish", EResType.UI);
    }

    public override void Init()
    {
        m_txtContent = m_tran.Find("background/txtContent").GetComponent<Text>();
        m_txtTitle = m_tran.Find("background/txtTitle").GetComponent<Text>();
        m_txtTimeRule = m_tran.Find("background/txtReleaseTime").GetComponent<Text>();
        m_txtReleaseTime = m_tran.Find("background/txtReleaseTime/Time").GetComponent<Text>();
        m_btnClose = m_tran.Find("background/btnClose").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            m_punish = m_params[0] as roomPunish;
            m_txtTitle.text = "禁赛惩罚警告";
            m_txtTimeRule.text = "禁赛剩余时间:";            
        }
    }

    private float m_clickTime = 0;

    public override void Update()
    {
        m_clickTime += Time.deltaTime;
        if (m_clickTime > 1)
        {
            setPunishTime();
            m_clickTime = 0;
        }
    }

    private void setPunishTime()
    {
        if (m_punish != null)
        {
            if (TimeUtil.GetRemainTimeType((uint)m_punish.release_time) != -1)
            {
                m_txtReleaseTime.text = TimeUtil.FormatTime((int)TimeUtil.GetRemainTime((uint)m_punish.release_time).TotalSeconds, 4, true);
                string name = m_punish.player_id == PlayerSystem.roleId ? "您" : "您队伍中的" + m_punish.name;
                m_txtContent.text = string.Format("尊敬的玩家，由于{0}近期在排位战斗中存在中途退出和挂机等行为，现已禁止你进行排位匹配", name);
            }   
        }     
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        
    }
}