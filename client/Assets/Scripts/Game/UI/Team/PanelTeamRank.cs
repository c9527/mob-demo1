﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelTeamRank : BasePanel
{
    private Image _titleIcon;
    private Image _titleRankImg;
    private Image _titleNameImg;
    private Text _rankTxt;
    private Text _rankPoint;
    private Text _winTxt;
    private DataGrid _titleGrid;
    private DataGrid _rankGrid;
    private Text _rankPage;

    private Dictionary<int, int> _pageDic;
    private List<ItemRankData> _itemRankDataList;
    private List<ItemRankData> _itemRankDataPool;

    private static string QUALIFYING_SCORE_RANK_NAME = "qualifying_score";
    private static string QUALIFYING_SCORE_JOKE_RANK_NAME = "qualifying_score_joke";
    private static string NEWER_QUALIFYING_SCORE_RANK_NAME = "newer_qualifying_score";
    private static string JOKE_PREFIX = "joke_";
    private static string QUALIFYING_SCORE_PREFIX = "qualifying_";

    private static string[] QUALIFYING_SCORE_RANK_NAME_ARRAY =
    {
        "",
        "score_yellow_metal",
        "score_white_silver",
        "score_gold",
        "score_platinum",
        "score_diamond",
        "score_god"
    };
    private int _bigType; //区分娱乐模式还是排位竞技模式
    private string _bigTypePrefix; //区分娱乐模式还是排位竞技模式排行榜前缀

    public int CurrentTitle;
    private const float PAGE_SIZE = 8f;
    private int _pageMax;

    public PanelTeamRank()
    {
        SetPanelPrefabPath("UI/Team/PanelTeamRank");
        AddPreLoadRes("UI/Team/PanelTeamRank", EResType.UI);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);
    }

    public override void Init()
    {
        
        _itemRankDataList = new List<ItemRankData>();
        _itemRankDataPool =new List<ItemRankData>();
        _pageDic = new Dictionary<int, int>();

        _titleIcon = m_tran.Find("rightDetail/titleframe/titleicon").GetComponent<Image>();
        _titleRankImg = m_tran.Find("rightDetail/titleframe/titleicon/rank").GetComponent<Image>();
        _titleNameImg = m_tran.Find("rightDetail/titleframe/titlename").GetComponent<Image>();

        _rankPoint = m_tran.Find("rightDetail/rankinfo/rankPoint/count").GetComponent<Text>();
        _rankTxt = m_tran.Find("rightDetail/titleframe/rank/count").GetComponent<Text>();
        _winTxt = m_tran.Find("rightDetail/rankinfo/win/count").GetComponent<Text>();
        _rankPage = m_tran.Find("middleRankList/BottomPageHit/cur_page/Text").GetComponent<Text>();

        _titleGrid = m_tran.Find("leftRankTab/content").gameObject.AddComponent<DataGrid>();
        _titleGrid.SetItemRender(m_tran.Find("leftRankTab/item").gameObject, typeof(ItemTeamRankTitle));
        _titleGrid.useLoopItems = true;

        _rankGrid = m_tran.Find("middleRankList/ScrollRect/content").gameObject.AddComponent<DataGrid>();
        _rankGrid.SetItemRender(m_tran.Find("middleRankList/ScrollRect/item").gameObject, typeof(ItemTeamRankListInfo));
        _rankGrid.useLoopItems = true;

        //获取所有段位
        var cfg = ConfigManager.GetConfig<ConfigScoreTitle>();
        var titleList = cfg.GetAllScoreTitleList();
        // 段位最后有小等级,要取子字符串
        for (int i = 0; i < titleList.Count; i++)
        {
            var name = titleList[i].Name;
            if (name.Length > 4)
            {
                titleList[i].Name = name.Substring(0, name.Length - 1);
            }
        }
        
        _titleGrid.onItemSelected += delegate(object renderData)
        {
            var data = renderData as TDScoreTitle;
            CurrentTitle = data.range;
            NetLayer.Send(new tos_player_rank_list() { name = TitleIntToStr(data.range), page = GetPage(data.range) });
        };
        var titleArray = titleList.ToArray();
        Array.Reverse(titleArray);
        _titleGrid.Data = titleArray;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("BtnReward").gameObject).onPointerClick += OnExplain;
        UGUIClickHandler.Get(m_tran.Find("middleRankList/BottomPageHit/pre_btn").gameObject).onPointerClick += OnClickBtnPagePrev;
        UGUIClickHandler.Get(m_tran.Find("middleRankList/BottomPageHit/next_btn").gameObject).onPointerClick += OnClickBtnPageNext;

    }

    private void OnExplain(GameObject target, PointerEventData eventData)
    {
        string str = "";
        string strNew = "";
        var cfgRankList = ConfigManager.GetConfig<ConfigRanklist>();
        ConfigRanklistLine rankListLine = cfgRankList.GetLine(TitleIntToStr(CurrentTitle));
        if (rankListLine != null)
            str = rankListLine.LongInfo;
        strNew = str.Replace("\\n", "\n");
        UIManager.ShowTipMulTextPanel(strNew);
    }


    private void OnClickBtnPagePrev(GameObject target, PointerEventData eventData)
    {
        var page = GetPage(CurrentTitle);
        var prevPage = Mathf.Max(1, page -1);
        if (page != prevPage)
        {
            _pageDic[CurrentTitle] = prevPage;

            NetLayer.Send(new tos_player_rank_list() { name = TitleIntToStr(CurrentTitle), page = prevPage });
        }
    }

    private void OnClickBtnPageNext(GameObject target, PointerEventData eventData)
    {
        var page = GetPage(CurrentTitle);
        var nextPage = Mathf.Min(_pageMax, page + 1);
        if (page != nextPage)
        {
            _pageDic[CurrentTitle] = nextPage;
            NetLayer.Send(new tos_player_rank_list() { name = TitleIntToStr(CurrentTitle), page = nextPage });
        }
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _bigType = (int)m_params[0];
            _bigTypePrefix = _bigType == 0 ?QUALIFYING_SCORE_PREFIX : JOKE_PREFIX;
        }
        _titleGrid.Select(0);
//        NetLayer.Send(new tos_player_rank_list() { name = TitleIntToStr(CurrentTitle), page = GetPage(CurrentTitle) });
        var score = PlayerSystem.roleData.honor;
        var info = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(score);
        if (info != null)
        {
            _titleIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, info.IconKey));
            _titleRankImg.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, string.Format("rank{0}", info.Level)));
            _titleRankImg.SetNativeSize();
            _titleRankImg.gameObject.TrySetActive(info.IconKey != "kingtitle");
            _titleNameImg.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, info.IconKey+"word"));
        }
        _rankPoint.text = score.ToString();
        _winTxt.text = "0";
        _rankTxt.text = "未上榜";
        NetLayer.Send(new tos_player_my_ranks());
    }

    private void Toc_player_my_ranks(toc_player_my_ranks data)
    {
        string rankName = "";
        rankName = _bigType == 1 ?QUALIFYING_SCORE_JOKE_RANK_NAME:PlayerSystem.roleData.level >= 15 ? QUALIFYING_SCORE_RANK_NAME : NEWER_QUALIFYING_SCORE_RANK_NAME;
        for (int i = 0; i < data.ranks.Length; i++)
        {
            if (data.ranks[i].name.StartsWith(_bigTypePrefix) && data.ranks[i].ext != 0)
            {
                _winTxt.text = data.ranks[i].ext.ToString();
            }
            if (data.ranks[i].name == rankName)
            {
                if (data.ranks[i].rank == 0)
                {
                    _rankTxt.text = "未上榜";
                }
                else
                {
                    _rankTxt.text = data.ranks[i].rank.ToString();
                }
                
                _rankPoint.text = data.ranks[i].val.ToString();
            }
        }
    }


    private void Toc_player_rank_list(toc_player_rank_list proto)
    {
        _pageMax = Mathf.Max(1,Mathf.CeilToInt(proto.size / PAGE_SIZE));
        var currentPage = GetPage(CurrentTitle);
        _rankPage.text = currentPage + "/" + _pageMax;

        _itemRankDataList.Clear();
        int iOrgLenth = Convert.ToInt32((currentPage - 1) * PAGE_SIZE);

        for (int i = 0; i < proto.list.Length; ++i)
        {
            ItemRankData data = GetPoolItem(i);
            data.id = proto.list[i].id;
            data.name = proto.list[i].name;
            data.corps_name = proto.list[i].corps_name;
            data.val = proto.list[i].val;
            data.ext = proto.list[i].ext;
            data.queue = iOrgLenth + i + 1;
            _itemRankDataList.Add(data);
        }
        _rankGrid.Data = _itemRankDataList.ToArray();
    }

    private ItemRankData GetPoolItem(int index)
    {
        if (index >= _itemRankDataPool.Count)
        {
            var item = new ItemRankData();
            _itemRankDataPool.Add(item);
            return item;
        }
        return _itemRankDataPool[index];
    }

    public int GetPage(int range)
    {
        if (!_pageDic.ContainsKey(CurrentTitle))
        {
            _pageDic[CurrentTitle] = 1;
        }
        return _pageDic[CurrentTitle];
    }

    public string TitleIntToStr(int range)
    {
        if (range > 0 && range < QUALIFYING_SCORE_RANK_NAME_ARRAY.Length)
        {
            return _bigTypePrefix + QUALIFYING_SCORE_RANK_NAME_ARRAY[range];
        }
        return "";
    }

    public override void OnHide()
    {
        _itemRankDataList.Clear();
    }

    public override void OnBack()
    {
        RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.match);
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.m_channel, stage = 0 });
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}


class ItemTeamRankTitle : ItemRender
{
    private Text _titleName;
    private Image _titleIcon;
    private PanelTeamRank _parentPanel;
    

    public override void Awake()
    {
        _parentPanel = UIManager.GetPanel<PanelTeamRank>();
        _titleIcon = transform.Find("titleicon").GetComponent<Image>();
        _titleName = transform.Find(("grade")).GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        TDScoreTitle _data = data as TDScoreTitle;
        _titleIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, _data.IconKey));
        _titleName.text = _data.Name;
    }
}


class ItemTeamRankListInfo : ItemRender
{
    private Text _name;
    private Text _win;
    private Text _rank;
    private Text _credit;
    private Image _rankIcon;
    private Image _rankNum;
    private ItemRankData _data;

    public override void Awake()
    {
        _name = transform.Find("name").GetComponent<Text>();
        _win = transform.Find("win").GetComponent<Text>();
        _rank = transform.Find("rank").GetComponent<Text>();
        _credit = transform.Find("point").GetComponent<Text>();
        _rankIcon = transform.Find("rank/icon").GetComponent<Image>();
        _rankNum = transform.Find("rank/Num").GetComponent<Image>();

    }

    protected override void OnSetData(object data)
    {
        _data = data as ItemRankData;
        var queue = _data.queue;
        _rank.text = "";
        if (queue > 99)
        {
            _rank.text = queue.ToString();
            _rankIcon.gameObject.TrySetActive(false);
            _rankNum.gameObject.TrySetActive(false);
        }
        else if (queue > 9)
        {
            _rankIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue / 10)));
            _rankIcon.SetNativeSize();
            _rankIcon.gameObject.TrySetActive(true);
            _rankNum.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue % 10)));
            _rankNum.SetNativeSize();
            _rankNum.gameObject.TrySetActive(true);
            
        }
        else
        {
            _rankIcon.gameObject.TrySetActive(false);
            _rankNum.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, queue < 4 ? "rank" + queue : "rank_" + queue));
            _rankNum.SetNativeSize();
            _rankNum.gameObject.TrySetActive(true);
        }
        _name.text = _data.name;
        _credit.text = _data.val.ToString();
        _win.text = _data.ext.ToString();
    }
}