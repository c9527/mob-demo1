﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;


class PanelRankMode : BasePanel
{

    class ItemRankMode : ItemRender
    {
        GameObject m_checkmark;
        Text m_modename;
        GameObject m_hot;
        public bool select;
        public string name;
        public ConfigJokeModeGroupLine m_data;
        public override void Awake()
        {
            m_checkmark = transform.Find("Background/Checkmark").gameObject;
            m_modename = transform.Find("Background/Text").GetComponent<Text>();

        }

        protected override void OnSetData(object data)
        {
            var info = data as ConfigJokeModeGroupLine;
            m_modename.text = info.GroupName;
            m_checkmark.TrySetActive(false);
            select = false;
            name = info.GroupName;
            m_data = info;
        }

        public void Select()
        {
            select = true;
            m_checkmark.TrySetActive(select);
        }

        public void UnSelect()
        {
            select = false;
            m_checkmark.TrySetActive(select);
        }

    }

    bool isAllSelect;
    GameObject m_ok;
    DataGrid m_ruleList;
    List<ConfigJokeModeGroupLine> modes;
    GameObject m_allSelectButton;
    GameObject m_checkall;
    Text m_text;
    Toggle m_allmode;
    public PanelRankMode()
    {

        SetPanelPrefabPath("UI/Team/PanelRankMode");
        AddPreLoadRes("UI/Team/PanelRankMode", EResType.UI);


    }
    public override void Init()
    {
        isAllSelect = false;

        m_ok = m_tran.Find("frame/ButtonSure").gameObject;
        m_ruleList = m_tran.Find("frame/normalgridlayout").gameObject.AddComponent<DataGrid>();
        m_ruleList.SetItemRender(m_tran.Find("frame/copeitem").gameObject, typeof(ItemRankMode));
        m_allSelectButton = m_tran.Find("frame/ALLselect").gameObject;
        m_checkall = m_tran.Find("frame/ALLselect/Checkmark").gameObject;
        m_text = m_tran.Find("frame/Text").GetComponent<Text>();
        int limit = ConfigManager.GetConfig<ConfigMisc>().GetLine("jokematch_group_num").ValueInt;
        m_text.text = "最少选择" + limit + "组匹配模式，筛选项会在本次登陆期间保存.";
        modes = new List<ConfigJokeModeGroupLine>();
        var cf = ConfigManager.GetConfig<ConfigJokeModeGroup>();

        for (int i = 0; i < cf.m_dataArr.Length; i++)
        {
                modes.Add(cf.m_dataArr[i]);
        }

        m_ruleList.Data = modes.ToArray();
        if (RoomModel.m_rankmodeSelect == null)
        {
            RoomModel.m_rankmodeSelect = new List<ConfigJokeModeGroupLine>();
        }
        m_allmode = m_tran.Find("frame/allmode").GetComponent<Toggle>();
        if (RoomModel.m_rankmodeSelect.Count == 0)
        {
            RoomModel.m_rankmodeSelect.Clear();
            for (int i = 0; i < modes.Count; i++)
            {
                RoomModel.m_rankmodeSelect.Add(modes[i]);
            }
            for (int i = 0; i < m_ruleList.ItemRenders.Count; i++)
            {
                var item = m_ruleList.ItemRenders[i] as ItemRankMode;
                item.Select();
            }
        }
        isAllSelect = true;
        m_allmode.isOn = isAllSelect;
    }

    public override void InitEvent()
    {
        m_ruleList.onItemSelected += onItemSelect;
        UGUIClickHandler.Get(m_ok).onPointerClick += OnOkClick;
        UGUIClickHandler.Get(m_allSelectButton).onPointerClick += OnAllSelect;
        UGUIClickHandler.Get(m_tran.Find("frame/allmode")).onPointerClick += OnAllSelect;
       
    }

    void OnAllSelect(GameObject target, PointerEventData data)
    {
        if (!isAllSelect)
        {
            RoomModel.m_rankmodeSelect.Clear();
            for (int i = 0; i < modes.Count; i++)
            {
                RoomModel.m_rankmodeSelect.Add(modes[i]);
            }
            for (int i = 0; i < m_ruleList.ItemRenders.Count; i++)
            {
                var item = m_ruleList.ItemRenders[i] as ItemRankMode;
                item.Select();
            }
            isAllSelect = !isAllSelect;

        }
        else
        {
            RoomModel.m_rankmodeSelect.Clear();
            for (int i = 0; i < m_ruleList.ItemRenders.Count; i++)
            {
                var item = m_ruleList.ItemRenders[i] as ItemRankMode;
                item.UnSelect();
            }
            isAllSelect = !isAllSelect;
        }
        m_checkall.TrySetActive(isAllSelect);
        m_allmode.isOn = isAllSelect;
    }



    void OnOkClick(GameObject target, PointerEventData data)
    {
        int limit = ConfigManager.GetConfig<ConfigMisc>().GetLine("jokematch_group_num").ValueInt;
        if (RoomModel.m_rankmodeSelect.Count < limit)
        {
            TipsManager.Instance.showTips("至少选择"+limit.ToString()+"个模式组");
            return;
        }
        List<string> rules = new List<string>();
        for (int i = 0; i < RoomModel.m_rankmodeSelect.Count; i++)
        {
            for (int j = 0; j < RoomModel.m_rankmodeSelect[i].ModeGroup.Length; j++)
            {
                rules.Add(RoomModel.m_rankmodeSelect[i].ModeGroup[j]);
            }
        }
        //Debug.LogError(rules.Count);
       NetLayer.Send(new tos_room_rule() { rule_list = rules.ToArray(), rule = PanelTeam.rule});
       HidePanel();
    }

    void onItemSelect(object itemRender)
    {

        var info = itemRender as ConfigJokeModeGroupLine;
        if (!RoomModel.m_rankmodeSelect.Contains(info))
        {
            RoomModel.m_rankmodeSelect.Add(info);
        }
        else
        {
            RoomModel.m_rankmodeSelect.Remove(info);
        }
        for (int i = 0; i < m_ruleList.ItemRenders.Count; i++)
        {
            var item = m_ruleList.ItemRenders[i] as ItemRankMode;
            if (item.m_data == info)
            {
                item.select = !item.select;
                if (item.select)
                {
                    item.Select();
                }
                else
                {
                    item.UnSelect();
                }
            }
        }
        return;
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        m_checkall.TrySetActive(isAllSelect);
        
    }

    public override void Update()
    {
        for (int i = 0; i < m_ruleList.ItemRenders.Count; i++)
        {
            var item = m_ruleList.ItemRenders[i] as ItemRankMode;
            for (int j = 0; j < RoomModel.m_rankmodeSelect.Count; j++)
            {
                if (item.m_data.GroupId == RoomModel.m_rankmodeSelect[j].GroupId)
                {
                    item.Select();
                }
            }
        }
    }
}

