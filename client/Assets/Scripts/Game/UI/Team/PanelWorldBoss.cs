﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Text;

class PanelWorldBoss : PanelRoomBase
{
    enum ITEM_STATE
    {
        NORMAL = 0,
        HOLD = 1
    }



    /// <summary>
    /// 世界boss是否已经开启
    /// </summary>
    /// <returns>0：已开启，1：未开启</returns>
    public static int IsOpenEvent()
    {
        int result = 0;
        var date = TimeUtil.GetNowTime();

        DateTime startTime;
        DateTime endShowTime;
        DateTime startShowTime;
        PanelWorldBoss.GetWorldBossTime(date, out startShowTime, out startTime, out endShowTime);
        result= date > startTime && date < endShowTime ? 0 : 1;
        return result;
    }

    const int TEAM_ITEM_SIZE = 5;
    class ItemData
    {
        public GameObject m_goItem;
        public p_player_info m_data;
        public ITEM_STATE m_state;
    }

    GameObject m_goTeamList;
    GameObject m_goTeamOperation;

    ItemData[] m_listTeamItem;

        
    GameObject m_goBtnStart;
    GameObject m_inviteFriend;
    Image _title_img;
    Image _title_name_img;
    Image _title_name_img_en;        
    GameObject _poplist_mask;
    //GameObject m_matchingTips;
    Text m_costtime;
    Text m_guesttime;
    Text m_leftTime;
    GameObject m_cancelBtn;
    GameObject m_wait;
    Image m_bossImg;
    Image m_titlecn;
    Image m_titleen;

    p_player_info m_selected_data;
    bool m_bIsOwner = false;
    bool m_matching = false;
    public bool is4newer = false;
    public static bool m_isOver = false;
    ItemDropInfo[] m_hostOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             //new ItemDropInfo { type = "OPERATION", subtype = "CHANGE", label = "转让房主"},
             new ItemDropInfo { type = "OPERATION", subtype = "KICK", label = "踢出房间"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };

    ItemDropInfo[] m_guestOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };    

    toc_room_info m_tempTeamListData;

    private Vector2 m_anchorName;

    public PanelWorldBoss()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.InviteFriend, 
            RoomFunc.WorldBoss,
            //RoomFunc.RankMatching
        });
        SetPanelPrefabPath("UI/Team/PanelWorldBoss");
        AddPreLoadRes("UI/Team/PanelWorldBoss", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);       
    }

    public override void Init()
    {
        base.Init();
        m_goTeamList = m_tran.Find("pageframe/team/teamlist").gameObject;        
        m_goTeamOperation = m_tran.Find("optionframe").gameObject;        
        m_goBtnStart = m_goTeamOperation.transform.Find("btnStart").gameObject;                            
        m_listTeamItem = new ItemData[TEAM_ITEM_SIZE];
        for (int cnt = 1; cnt <= TEAM_ITEM_SIZE; ++cnt)
        {
            GameObject item = m_goTeamList.transform.Find("teammate" + cnt.ToString()).gameObject;
            GameObject btn = item.transform.Find("ask_tip").gameObject;
            UGUIClickHandler.Get(btn).onPointerClick += OnClickInviteFriend;
            UGUIClickHandler.Get(item).onPointerClick += ShowInfoMenu;

            ItemData newItem = new ItemData();
            newItem.m_goItem = item;
            newItem.m_data = null;
            newItem.m_state = ITEM_STATE.NORMAL;

            m_listTeamItem[cnt - 1] = newItem;

            InitTeamItem(newItem);
        }
        m_goTeamList.TrySetActive(false);
        m_goTeamOperation.TrySetActive(false);
        _title_img = m_tran.Find("optionframe/titleframe/titleicon").GetComponent<Image>();
        _title_name_img = m_tran.Find("optionframe/titleframe/titlename").GetComponent<Image>();
        _title_name_img_en = m_tran.Find("optionframe/titleframe/titlenameen").GetComponent<Image>();        
        _poplist_mask = m_tran.Find("poplistpanel").gameObject;
        m_inviteFriend = m_tran.Find("optionframe/btnInviteFriend").gameObject;
        m_leftTime = m_tran.Find("optionframe/lefttime").GetComponent<Text>();
        //m_matchingTips = m_tran.Find("MatchingTips").gameObject;
        //m_costtime = m_matchingTips.transform.Find("frame/costtime").GetComponent<Text>();
        //m_guesttime = m_matchingTips.transform.Find("frame/guesttime").GetComponent<Text>();
        //m_cancelBtn = m_matchingTips.transform.Find("frame/Button").gameObject;
        m_wait = m_tran.Find("optionframe/WaitText").gameObject;
        //m_matchingTips.TrySetActive(false);
        _poplist_mask.TrySetActive(false);
        m_bossImg = m_tran.Find("optionframe/titleframe/titleicon").GetComponent<Image>();
        m_titlecn = m_tran.Find("optionframe/titleframe/titlename").GetComponent<Image>();
        m_titleen = m_tran.Find("optionframe/titleframe/titlenameen").GetComponent<Image>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_poplist_mask).onPointerClick += OnShowMenuHandler;
        UGUIClickHandler.Get(m_tran.Find("pageframe/ruleintro").gameObject).onPointerClick += delegate { UIManager.PopRulePanel("Ranking"); };
        UGUIClickHandler.Get(m_inviteFriend).onPointerClick += OnClickInviteFriend;
        UGUIClickHandler.Get(m_goBtnStart).onPointerClick += OnClickStart;
        //UGUIClickHandler.Get(m_cancelBtn).onPointerClick += OnCancelClick;
    }

    private void OnCancelClick(GameObject target, PointerEventData eventData)
    {
        
    }

    private void OnClickStart(GameObject target, PointerEventData eventData)
    {
        //Logger.Log("start");
        //m_matchingTips.TrySetActive(true);
    }


    private void OnShowMenuHandler(GameObject target = null, PointerEventData eventData = null)
    {
        UIManager.HideDropList();
        _poplist_mask.TrySetActive(false);
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            is4newer = true;
            m_isOver = false;
            m_leftTime.text = "不限时";                  
        }        
        else
        {
            is4newer = false;
        }
        base.OnShow();
        if (!RoomModel.IsInRoom)
        {            
                NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.worldboss), stage = 0 });           
        }
        m_tempTeamListData = RoomModel.RoomInfo;
        if (m_tempTeamListData != null)
        {
            OnUpdateTeamList(m_tempTeamListData);
            m_tempTeamListData = null;
        }
        PanelBattleSurvivalScore.TryDeleteBossHPBar(null, true);
        PanelBattleDotaScore.TryDeleteBossHPBar(null, true);
        if (TimeUtil.GetNowTime().Hour > 14)
        {
            m_bossImg.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "boss"));
            m_titlecn.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "bossJushi"));
//            m_titleen.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "bossEnglishg"));

        }
        else
        {
            m_bossImg.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "boss"));
            m_titlecn.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "bossJushi"));
//            m_titleen.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, "bossEnglishg"));
        }
        m_bossImg.SetNativeSize();
        m_titleen.SetNativeSize();
        m_titlecn.SetNativeSize();
    }


    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            UGUIClickHandler.Get(m_listTeamItem[index].m_goItem.transform.Find("ask_tip").gameObject).onPointerClick -= OnClickInviteFriend;
        }
    }
    public override void Update()
    {
        base.Update();
        if(!is4newer)
            UpdateTime();
    }

    public static void  GetWorldBossTime(DateTime date,out DateTime showTime,out DateTime openTime,out DateTime endTime)
    {        
        int dis = int.MaxValue;        
        string time = ConfigManager.GetConfig<ConfigEventList>().GetLine(1110).TimeLimit;
        string[] strsTimes = time.Split(';'); //活动时间段分组
        DateTime tShow= date;
        DateTime tOpen = date;
        DateTime tClose = date;
        for (int i = 0; i < strsTimes.Length;i++ ) 
        {
            string[] strsTime=strsTimes[i].Split('#');
            string open = strsTime[0]; //开启时间
            string close = strsTime[1];  //关闭时间
            DateTime o = GetTime(date, open);            
            int dis2 = Mathf.Abs((int)(date - o).TotalSeconds);
            if(dis2<dis)
            {
                dis = dis2;
                tOpen = o;
                tShow = o.AddMinutes(-30);
                tClose = GetTime(date, close); 
            }           
        }
        showTime = tShow;
        openTime = tOpen;
        endTime = tClose;
    }

    private static DateTime GetTime(DateTime now, string str)
    {
        string strHour = str.Substring(0, 2);
        string strMin = str.Substring(2, 2);
        DateTime time = new DateTime(now.Year, now.Month, now.Day, int.Parse(strHour), int.Parse(strMin),0);
        return time;
    }


    float m_lastUpdateTime;
    private void UpdateTime()
    {
        if ((Time.time - m_lastUpdateTime) < 1.0f)
            return;
        m_lastUpdateTime = Time.time;
        
        DateTime date = TimeUtil.GetNowTime();
        DateTime startShowTime, openTime, endShowTime;
        PanelWorldBoss.GetWorldBossTime(date,out startShowTime,out openTime,out endShowTime);       
        if (date < endShowTime)
        {
            TimeSpan sp = endShowTime - date;
            m_leftTime.text = string.Format("剩余时间   {0:D2}:{1:D2}", sp.Minutes, sp.Seconds);
        }
        else
        {
            m_leftTime.text = "00:00";
            m_isOver = true;
        }
    }

    void InitTeamItem(ItemData item)
    {
        item.m_goItem.transform.Find("head").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("armylvl").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("inner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("owner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("goldace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("silverace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("acheive").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("vip").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("databg").gameObject.TrySetActive(false);
        m_anchorName = item.m_goItem.transform.Find("inner/name").GetComponent<RectTransform>().anchoredPosition;
        item.m_data = null;
        item.m_state = ITEM_STATE.NORMAL;

    }

    void ShowInfoMenu(GameObject target, PointerEventData eventData)
    {
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            ItemData item = m_listTeamItem[index];
            if (item.m_goItem.gameObject == target)
            {
                m_selected_data = item.m_data;
                if (item.m_state == ITEM_STATE.HOLD)
                {
                    if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                    {
                        if (m_bIsOwner)
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                        else
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                    }
                }
                break;
            }
        }
    }

    void OnClickInviteFriend(GameObject target, PointerEventData eventData)
    {
        m_selected_data = null;
        if (m_inviteFriend != target)
        {
            for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
            {
                ItemData item = m_listTeamItem[index];
                if (item.m_goItem.transform.Find("ask_tip").gameObject == target)
                {
                    m_selected_data = item.m_data;
                    if (item.m_state == ITEM_STATE.HOLD)
                    {
                        if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                        {
                            if (m_bIsOwner)
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                            else
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                        }
                    }
                    else
                    {
                        NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
                    }
                    break;
                }
            }
        }
        else
        {
            NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
        }
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        _poplist_mask.TrySetActive(false);
        if (m_selected_data == null)
        {
            return;
        }
        var uid = m_selected_data.id;
        if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(uid);
        }
        else if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(uid);
        }
        else if (data.subtype == "CHAT")
        {
            PlayerRecord rcd = new PlayerRecord();
            rcd.m_iPlayerId = uid;
            rcd.m_strName = PlayerSystem.GetRoleNameFull(m_selected_data.name, m_selected_data.id);
            rcd.m_iLevel = m_selected_data.level;
            rcd.m_bOnline = true;
            rcd.m_isFriend = PlayerFriendData.singleton.isFriend(uid);
            rcd.m_iHead = m_selected_data.icon;
            ChatMsgTips msgTips = new ChatMsgTips();
            msgTips.m_rcd = rcd;
            msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
            UIManager.PopChatPanel<PanelChatSmall>(new object[] { msgTips });
        }
        else if (data.subtype == "KICK")
        {
            NetLayer.Send(new tos_room_kick_player { id = uid });
        }
    }

    void OnUpdateTeamList(toc_room_info teamData)
    {

        m_goTeamList.TrySetActive(true);
        m_goTeamOperation.TrySetActive(true);

        p_player_info[] playerList = teamData.players;
        var leaderId = teamData.leader;

        if (leaderId == PlayerSystem.roleId)
        {
            m_bIsOwner = true;           
        }
        else
        {
            m_bIsOwner = false;
        }
        m_inviteFriend.TrySetActive(m_bIsOwner);
        m_goBtnStart.TrySetActive(m_bIsOwner);
        m_wait.TrySetActive(!m_bIsOwner);
        for (int n = 0; n < TEAM_ITEM_SIZE; ++n)
        {
            InitTeamItem(m_listTeamItem[n]);
        }

        bool bFindLeader = false;

        for (int index = 0; index < playerList.Length; ++index)
        {
            var teamerData = playerList[index];
            var playerid = teamerData.id;

            if (playerid == leaderId)
            {
                bFindLeader = true;

                ItemData item = m_listTeamItem[0];

                HoldTeamListSlot(item, teamerData);
            }
            else
            {
                if (!bFindLeader)
                {
                    if (index + 1 < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index + 1];

                        HoldTeamListSlot(item, teamerData);
                    }

                }
                else
                {
                    if (index < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index];

                        HoldTeamListSlot(item, teamerData);
                    }
                }
            }
        }

        ItemData ownerItem = m_listTeamItem[0];
        if (ownerItem.m_state == ITEM_STATE.HOLD)
        {
            ownerItem.m_goItem.transform.Find("owner").gameObject.TrySetActive(true);
        }


        ShowOperationPri();
    }

    void HoldTeamListSlot(ItemData item, p_player_info data)
    {
        if (item.m_state == ITEM_STATE.HOLD)
            return;

        item.m_state = ITEM_STATE.HOLD;
        item.m_data = data;
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("databg").gameObject.TrySetActive(true);
        GameObject goHead = item.m_goItem.transform.Find("head").gameObject;
        goHead.TrySetActive(true);
        Image headicon = goHead.transform.Find("icon").GetComponent<Image>();
        headicon.SetSprite(ResourceManager.LoadRoleIconBig(data.icon));
        headicon.SetNativeSize();
        headicon.transform.localScale = new Vector3(0.8f, 0.8f, 1);

        GameObject goArmyLvl = item.m_goItem.transform.Find("armylvl").gameObject;
        goArmyLvl.TrySetActive(true);
        Image armylvl = goArmyLvl.transform.Find("army").GetComponent<Image>();
        armylvl.SetSprite(ResourceManager.LoadArmyIcon(data.level));

        Text txtArmyLvl = goArmyLvl.transform.Find("level").GetComponent<Text>();
        txtArmyLvl.text = string.Format("{0}级", data.level);

        GameObject goInner = item.m_goItem.transform.Find("inner").gameObject;
        goInner.TrySetActive(true);

        Text name = goInner.transform.Find("name").GetComponent<Text>();
        name.text = PlayerSystem.GetRoleNameFull(data.name, data.id);

        GameObject goName = goInner.transform.Find("name").gameObject;
        GameObject goVip = item.m_goItem.transform.Find("vip").gameObject;
        Image imgVip = item.m_goItem.transform.Find("vip").GetComponent<Image>();

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            goVip.TrySetActive(false);

            Vector2 vipSizeData = goVip.GetComponent<RectTransform>().sizeDelta;
            //goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            imgVip.SetNativeSize();
        }

        //item.m_goItem.transform.Find("member_icon").gameObject.TrySetActive(true);

        GameObject goVipName = goInner.transform.Find("vipname").gameObject;
        goVipName.TrySetActive(false);
        //        Text txtVipName = goVipName.GetComponent<Text>();

        var gold_ace = item.m_goItem.transform.Find("goldace");
        gold_ace.gameObject.TrySetActive(true);
        gold_ace.Find("num").GetComponent<Text>().text = data.gold_ace.ToString();
        var silver_ace = item.m_goItem.transform.Find("silverace");
        silver_ace.gameObject.TrySetActive(true);
        silver_ace.Find("num").GetComponent<Text>().text = data.silver_ace.ToString();
        var mvp = item.m_goItem.transform.Find("acheive");
        mvp.gameObject.TrySetActive(true);
        mvp.Find("num").GetComponent<Text>().text = data.mvp.ToString();
        foreach (p_title_info info in data.titles)
        {
            if (info.title == "mvp")
            {
                mvp.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "gold_ace")
            {
                gold_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "silver_ace")
            {
                silver_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
        }
    }

    void ShowOperationPri()
    {

        if (m_bIsOwner)
        {
            m_inviteFriend.TrySetActive(true);            
            m_goBtnStart.TrySetActive(true);
        }
        else
        {
            m_inviteFriend.TrySetActive(false);            
            m_goBtnStart.TrySetActive(false);
        }
    }



    void SetTeamListData(toc_room_info data)
    {
        m_tempTeamListData = data;
    }

    void Toc_room_info(toc_room_info data)
    {
        OnUpdateTeamList(RoomModel.RoomInfo);        
    }

    void Toc_room_leave_team(CDict data)
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data,UIManager.CurContentPanel  }, true, this, LayerType.Chat);
        }
    }
    
}
