﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;


class PanelTeamMode : BasePanel
{
    public PanelTeamMode()
    {
        SetPanelPrefabPath("UI/Team/PanelTeamMode");
        AddPreLoadRes("UI/Team/PanelTeamMode", EResType.UI);
    }
    int type = 0;
    Button m_normal;
    Button m_yule;
    public override void Init()
    {
        m_normal = m_tran.Find("BtnMatcnNormal").GetComponent<Button>();
        m_yule = m_tran.Find("BtnMatcnYule").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("yule1")).onPointerClick += delegate { selectMode(1); };
        UGUIClickHandler.Get(m_tran.Find("yule2")).onPointerClick += delegate { selectMode(1); };
        UGUIClickHandler.Get(m_tran.Find("normal1")).onPointerClick += delegate { selectMode(0); };
        UGUIClickHandler.Get(m_tran.Find("normal2")).onPointerClick += delegate { selectMode(0); };
    }


    void selectMode(int i)
    {
        RoomModel.matchType = i;
        RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.match);
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.m_channel, stage = 0 });
        //UIManager.ShowPanel<PanelTeam>(new object[]{null,i});
    }




    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
       
    }

    public override void OnHide()
    {

    }



    public override void OnShow()
    {

    }

    public override void Update()
    {

    }
   
}

