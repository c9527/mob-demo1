﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;

class PanelTeam : PanelRoomBase
{
    enum ITEM_STATE
    {
        NORMAL = 0,
        HOLD = 1
    }


    const int TEAM_ITEM_SIZE = 5;
    class ItemData
    {
        public GameObject m_goItem;
        public p_player_info m_data;
        public ITEM_STATE m_state;
    }

    GameObject m_goTeamList;
    GameObject m_goTeamOperation;

    ItemData[] m_listTeamItem;

    
    GameObject m_goBtnChangeMode;
    GameObject m_guestTxt;
    GameObject m_goBtnStart;
    private GameObject m_goBtnModeRank;
    Image _title_img;
    Image _title_name_img;
    Image _title_name_img_en;
    Image m_rankImg;
    Text _score_txt;
    Text m_rank;

    GameObject _poplist_mask;    
    //Text m_matchModeText;
    Text m_ruleName;

    List<ConfigSpecialRuleListLine> m_clt;
    ConfigSpecialRuleListLine ruleSel;
    int ruleType;
    int bigType;
    p_player_info m_selected_data;
    bool m_bIsOwner = false;
    static public string rule="all";
    GameObject m_btnMode;
    GameObject m_modetri;
    Text m_modeName;


    ItemDropInfo[] m_hostOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             //new ItemDropInfo { type = "OPERATION", subtype = "CHANGE", label = "转让房主"},
             new ItemDropInfo { type = "OPERATION", subtype = "KICK", label = "踢出房间"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };

    ItemDropInfo[] m_guestOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };

    toc_room_info m_tempTeamListData;

    private Vector2 m_anchorName;
    ItemDropInfo[] modes;
	public PanelTeam()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.InviteFriend, 
            RoomFunc.RankMatching
        });
        SetPanelPrefabPath("UI/Team/PanelTeam");
        AddPreLoadRes("UI/Team/PanelTeam", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);

    }

    public override void Init()
    {
        base.Init();
        ruleType = 0;
        m_goTeamList = m_tran.Find("pageframe/team/teamlist").gameObject;
        m_rank = m_tran.Find("optionframe/myrank/rank").gameObject.GetComponent<Text>();
        m_goTeamOperation = m_tran.Find("optionframe").gameObject;
        //m_goBtnChangeMode = m_goTeamOperation.transform.Find("btnChangeMode").gameObject;
        m_goBtnStart = m_goTeamOperation.transform.Find("btnStart").gameObject;
        m_guestTxt = m_tran.Find("optionframe/WaitText").gameObject;        
        //m_matchModeText = m_tran.Find("optionframe/btnChangeMode/Text").GetComponent<Text>();
        m_ruleName = m_tran.Find("optionframe/WaitText/modename/Text").GetComponent<Text>();
        m_goBtnModeRank = m_tran.Find("pageframe/btnModeRankList").gameObject;
        m_guestTxt.TrySetActive(false);        
        m_listTeamItem = new ItemData[TEAM_ITEM_SIZE];
        for (int cnt = 1; cnt <= TEAM_ITEM_SIZE; ++cnt)
        {
            GameObject item = m_goTeamList.transform.Find( "teammate" + cnt.ToString() ).gameObject;
            GameObject btn = item.transform.Find("ask_tip").gameObject;
            UGUIClickHandler.Get(btn).onPointerClick += OnClickInviteFriend;
            UGUIClickHandler.Get(item).onPointerClick += ShowInfoMenu;

            ItemData newItem = new ItemData();
            newItem.m_goItem = item;
            newItem.m_data = null;
            newItem.m_state = ITEM_STATE.NORMAL;

            m_listTeamItem[cnt - 1] = newItem;

            InitTeamItem(newItem);
        }
        m_goTeamList.TrySetActive(false);
        m_goTeamOperation.TrySetActive(false);
        _title_img = m_tran.Find("optionframe/titleframe/titleicon").GetComponent<Image>();
        m_rankImg = m_tran.Find("optionframe/titleframe/titleicon/rank").GetComponent<Image>();
        _title_name_img = m_tran.Find("optionframe/titleframe/titlename").GetComponent<Image>();
        _title_name_img_en = m_tran.Find("optionframe/titleframe/titlenameen").GetComponent<Image>();
        _score_txt = m_tran.Find("optionframe/operation/score").GetComponent<Text>();
        _poplist_mask = m_tran.Find("poplistpanel").gameObject;
        _poplist_mask.TrySetActive(false);

        m_btnMode = m_tran.Find("optionframe/modebtn").gameObject;
        m_modeName = m_tran.Find("optionframe/modebtn/Text").GetComponent<Text>();
        m_modetri = m_tran.Find("optionframe/modebtn/Image").gameObject;



        ruleSel = null;
        bigType = 0;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_poplist_mask).onPointerClick += OnShowMenuHandler;
        UGUIClickHandler.Get(m_tran.Find("pageframe/ruleintro").gameObject).onPointerClick += delegate { UIManager.PopRulePanel("Ranking"); };

        UGUIClickHandler.Get(m_tran.Find("optionframe/btnRankList").gameObject).onPointerClick += OnClickRankBtn;
        UGUIClickHandler.Get(m_goBtnModeRank).onPointerClick += OnClickModeRankBtn;
        UGUIClickHandler.Get(m_btnMode).onPointerClick += OnChangeModeClick;
    }

    void OnClickRankBtn(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelTeamRank>(new object[] { bigType });
    }

    private void OnClickModeRankBtn(GameObject target, PointerEventData eventData)
    {
        PanelRankList.OnBackCallBack = () =>
            {
                RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.match);
                NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.m_channel, stage = 0 });
            };
            PanelRankList.OpenPanel(PanelRankList.ENTERTAIN_RANKLIST);
    }

    void showRuleDesc()
    {
        if (bigType == 1)
        {
            UIManager.PopRulePanel("JokeMatch");
        }
        else
        {
            UIManager.PopRulePanel("Ranking");
        }
    }


    private void OnChangeModeClick(GameObject target, PointerEventData eventData)
    {
        if (bigType!=0)
        {
            UIManager.PopPanel<PanelRankMode>(null, true);
        }
    }

    private void ModeSelect(ItemDropInfo item)
    {
        rule = item.subtype;
        tos_room_rule roomRule = new tos_room_rule();
        roomRule.rule = item.subtype;
        roomRule.rule_list = new string[0];
        NetLayer.Send(new tos_room_rule() { rule = item.subtype, rule_list =new string[0]});        
    }

    private void OnShowMenuHandler(GameObject target=null, PointerEventData eventData=null)
    {
        UIManager.HideDropList();
        _poplist_mask.TrySetActive(false);
    }

    public override void OnShow()
    {
        base.OnShow();
        if (!RoomModel.IsInRoom)
            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.match), stage = 0 });
        m_tempTeamListData = RoomModel.RoomInfo;
        
        if( m_tempTeamListData != null )
        {
            OnUpdateTeamList(m_tempTeamListData);
            m_tempTeamListData = null;
        }
        bigType = RoomModel.matchType;
        if (HasParams())
        {
            if (m_params.Length >= 2)
            {
                bigType = (int)m_params[1];
                
            }
            else
            {
                bigType = 0;
            }
        }
        updateScoreTitle();
        var config = ConfigManager.GetConfig<ConfigSpecialRuleList>().GetLine(rule);
        if (config != null)
        {
            if (bigType == 0)
            {
                rule = "all";
                m_modeName.text = "普通模式";
                m_modetri.TrySetActive(false);
                m_goBtnModeRank.TrySetActive(false);
            }
            else
            {
                rule = "joke";
                m_modeName.text = "娱乐模式";
                m_modetri.TrySetActive(true);
                m_goBtnModeRank.TrySetActive(true);
            }
        }
        NetLayer.Send(new tos_player_my_ranks());
        NetLayer.Send(new tos_room_rule() { rule = PanelTeam.rule, rule_list = new string[0] });
        //var config = ConfigManager.GetConfig<ConfigSpecialRuleList>().GetLine(rule);
        UpdateModeName();
        

        //if (config == null)
        //    m_ruleName.text = "普通模式";
        //else
        //    m_ruleName.text = config.RuleName;
        //Logger.Log("sendRule:" + PanelTeam.rule);

        List<string> rules = new List<string>();
        for (int i = 0; i < RoomModel.m_rankmodeSelect.Count; i++)
        {
            for (int j = 0; j < RoomModel.m_rankmodeSelect[i].ModeGroup.Length; j++)
            {
                rules.Add(RoomModel.m_rankmodeSelect[i].ModeGroup[j]);
            }
        }
        //Debug.LogError(rules.Count);
        NetLayer.Send(new tos_room_rule() { rule_list = rules.ToArray(), rule = PanelTeam.rule });
    }

    void UpdateModeName()
    {
        var config = ConfigManager.GetConfig<ConfigSpecialRuleList>();
        m_clt = new List<ConfigSpecialRuleListLine>();
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            if (config.m_dataArr[i].RuleType == bigType)
            {
                m_clt.Add(config.m_dataArr[i]);
            }
        }

    }




    private void updateScoreTitle()
    {
        
        var score = PlayerSystem.roleData.honor;
        if (bigType == 1)
        {
            score = PlayerSystem.roleData.honor_joke;
        }
        var info = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(score);        
        if (info != null)
        {
            _title_img.enabled = true;
            m_rankImg.enabled = true;
            _title_name_img.enabled = true;
            _title_name_img_en.enabled = true;
            _title_img.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, info.IconKey));
            _title_img.SetNativeSize();
            Sprite sp=ResourceManager.LoadSprite(AtlasName.RankList, string.Format("rank{0}", info.Level));
            if (sp != null)
            {
                m_rankImg.SetSprite(sp);
                m_rankImg.SetNativeSize();
            }
            else
                m_rankImg.enabled = false;
            _title_name_img.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, info.IconKey+"word"));
            _title_name_img_en.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, info.IconKey + "worden"));
            _title_name_img.SetNativeSize();
            _title_name_img_en.SetNativeSize();
        }
        else
        {
            _title_img.enabled = false;
            m_rankImg.enabled = false;
            _title_name_img.enabled = false;
            _title_name_img_en.enabled = false;
        }
        _score_txt.text = score.ToString();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelTeamMode>();
    }

    public override void OnDestroy()
    {
        for( int index = 0; index < TEAM_ITEM_SIZE;++ index  )
        {
            UGUIClickHandler.Get(m_listTeamItem[index].m_goItem.transform.Find("ask_tip").gameObject).onPointerClick -= OnClickInviteFriend;
        }
    }
    public override void Update()
    {
        base.Update();
    }

    void InitTeamItem( ItemData item )
    {
        item.m_goItem.transform.Find("head").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("armylvl").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("inner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("owner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("goldace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("silverace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("acheive").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive( true );
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("vip").gameObject.TrySetActive(false);
        m_anchorName = item.m_goItem.transform.Find("inner/name").GetComponent<RectTransform>().anchoredPosition;
        item.m_data = null;
        item.m_state = ITEM_STATE.NORMAL;
        
    }

    void ShowInfoMenu(GameObject target, PointerEventData eventData)
    {
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            ItemData item = m_listTeamItem[index];
            if (item.m_goItem.gameObject == target)
            {
                m_selected_data = item.m_data;
                if (item.m_state == ITEM_STATE.HOLD)
                {
                    if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                    {
                        if (m_bIsOwner)
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                        else
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                    }
                }
                break;
            }
        }
    }

    void OnClickInviteFriend( GameObject target, PointerEventData eventData )
    {
        m_selected_data = null;
        if ( m_goBtnChangeMode != target)
        {
            for( int index = 0; index < TEAM_ITEM_SIZE; ++ index )
            {
                ItemData item = m_listTeamItem[index];
                if( item.m_goItem.transform.Find("ask_tip").gameObject == target )
                {
                    m_selected_data = item.m_data;
                    if( item.m_state == ITEM_STATE.HOLD )
                    {
                        if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                        {
                            if (m_bIsOwner)
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                            else
                            {
                                _poplist_mask.TrySetActive(true);
                                UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                            }
                        }
                    }
                    else
                    {
                        NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
                    }
                    break;
                }
            }
        }
        else
        {
            NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" });
        }
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        _poplist_mask.TrySetActive(false);
        if (m_selected_data == null)
        {
            return;
        }
        var uid = m_selected_data.id;
        if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(uid);
        }
        else if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(uid);
        }
        else if (data.subtype == "CHAT")
        {
            PlayerRecord rcd = new PlayerRecord();
            rcd.m_iPlayerId = uid;
            rcd.m_strName = PlayerSystem.GetRoleNameFull(m_selected_data.name, m_selected_data.id);
            rcd.m_iLevel = m_selected_data.level;
            rcd.m_bOnline = true;
            rcd.m_isFriend = PlayerFriendData.singleton.isFriend(uid);
            rcd.m_iHead = m_selected_data.icon;
            ChatMsgTips msgTips = new ChatMsgTips();
            msgTips.m_rcd = rcd;
            msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
            UIManager.PopChatPanel<PanelChatSmall>(new object[]{msgTips});
        }
        else if (data.subtype == "KICK")
        {
            NetLayer.Send(new tos_room_kick_player { id = uid });
        }
    }

    void OnUpdateTeamList(toc_room_info teamData )
    {
        
        m_goTeamList.TrySetActive(true);
        m_goTeamOperation.TrySetActive(true);

        p_player_info[] playerList = teamData.players;
        var leaderId = teamData.leader;

        if( leaderId == PlayerSystem.roleId )
        {
            m_bIsOwner = true;
        }
        else
        {
            m_bIsOwner = false;
        }
        for( int n = 0; n < TEAM_ITEM_SIZE; ++ n )
        {
            InitTeamItem(m_listTeamItem[n]);
        }

        bool bFindLeader = false;
        
        for( int index = 0; index < playerList.Length; ++ index )
        {
            var teamerData = playerList[index];
            var playerid = teamerData.id;

            if ( playerid == leaderId)
            {
                bFindLeader = true;

                ItemData item = m_listTeamItem[0];

                HoldTeamListSlot(item, teamerData);
            }
            else
            {
                if( !bFindLeader )
                {
                    if( index + 1 < TEAM_ITEM_SIZE )
                    {
                        ItemData item = m_listTeamItem[index + 1];

                        HoldTeamListSlot( item, teamerData);
                    }
                    
                }
                else
                {
                    if (index < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[ index ];

                        HoldTeamListSlot(item, teamerData);
                    }
                }
            }
        }

        ItemData ownerItem = m_listTeamItem[0];
        if( ownerItem.m_state == ITEM_STATE.HOLD )
        {
            ownerItem.m_goItem.transform.Find("owner").gameObject.TrySetActive( true );
        }


        ShowOperationPri();
    }

    void HoldTeamListSlot( ItemData item,p_player_info data )
    {
        if (item.m_state == ITEM_STATE.HOLD)
            return;

        item.m_state = ITEM_STATE.HOLD;
        item.m_data = data;
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive( false );
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(false);
        //item.m_goItem.transform.Find("databg").gameObject.TrySetActive(true);
        GameObject goHead = item.m_goItem.transform.Find("head").gameObject;
        goHead.TrySetActive(true);
        Image headicon = goHead.transform.Find("icon").GetComponent<Image>();
        headicon.SetSprite(ResourceManager.LoadRoleIconBig(data.icon));
        headicon.SetNativeSize();
        headicon.transform.localScale = new Vector3(0.8f, 0.8f, 1);
       
        GameObject goArmyLvl = item.m_goItem.transform.Find("armylvl").gameObject;
        goArmyLvl.TrySetActive( true );
        Image armylvl = goArmyLvl.transform.Find("army").GetComponent<Image>();
        armylvl.SetSprite(ResourceManager.LoadArmyIcon(data.level));

        Text txtArmyLvl = goArmyLvl.transform.Find("level").GetComponent<Text>();
        txtArmyLvl.text = string.Format("{0}级", data.level);

        GameObject goInner = item.m_goItem.transform.Find("inner").gameObject;
        goInner.TrySetActive(true);

        Text name = goInner.transform.Find("name").GetComponent<Text>();
        name.text = data.name;
        Text server = goInner.transform.Find("serverName").GetComponent<Text>();
        server.text = "[" + PlayerSystem.GetRoleSeverCNName(data.id) + "]";

        GameObject goName = goInner.transform.Find("name").gameObject;
        GameObject goVip = item.m_goItem.transform.Find("vip").gameObject;
        Image imgVip = item.m_goItem.transform.Find("vip").GetComponent<Image>();

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            goVip.TrySetActive(false);

            Vector2 vipSizeData = goVip.GetComponent<RectTransform>().sizeDelta;
            //goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            imgVip.SetNativeSize();
        }

        //item.m_goItem.transform.Find("member_icon").gameObject.TrySetActive(true);

        GameObject goVipName = goInner.transform.Find("vipname").gameObject;
        goVipName.TrySetActive(false);
//        Text txtVipName = goVipName.GetComponent<Text>();

        var gold_ace = item.m_goItem.transform.Find("goldace");
        gold_ace.gameObject.TrySetActive(true);
        gold_ace.Find("num").GetComponent<Text>().text = data.titles[1].cnt.ToString();
        gold_ace.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[1].title, data.titles[1].type)));
        var silver_ace = item.m_goItem.transform.Find("silverace");
        silver_ace.gameObject.TrySetActive(true);
        silver_ace.Find("num").GetComponent<Text>().text = data.titles[2].cnt.ToString();
        silver_ace.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[2].title, data.titles[2].type)));
        var mvp = item.m_goItem.transform.Find("acheive");
        mvp.gameObject.TrySetActive(true);
        mvp.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[0].title, data.titles[0].type)));
        mvp.Find("num").GetComponent<Text>().text = data.titles[0].cnt.ToString();

        //段位积分显示
        ConfigChannelLine channelCfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(3);
        Image bgImage = item.m_goItem.GetComponent<Image>();
        TDScoreTitle stCfg = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(data.honor);
        
        if (data.level >= channelCfg.LevelMin&&stCfg != null && stCfg.RankRoomBG!="")
        {
            bgImage.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, stCfg.RankRoomBG));
        }
        else
        {
            bgImage.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList,"rolebg"));
        }
        foreach (p_title_info info in data.titles)
        {
            if (info.title == "mvp")
            {
                mvp.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "gold_ace")
            {
                gold_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "silver_ace")
            {
                silver_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
        }
    }

    void ShowOperationPri()
    {
        
        if( m_bIsOwner )
        {
            //m_goBtnChangeMode.TrySetActive(true);
            m_btnMode.TrySetActive(true);
            m_guestTxt.TrySetActive(false);
            m_goBtnStart.TrySetActive(true);
        }
        else
        {
            m_btnMode.TrySetActive(false);
            //m_goBtnChangeMode.TrySetActive(false);
            m_guestTxt.TrySetActive(true);
            m_goBtnStart.TrySetActive(false);
        }
    }

    void SetTeamListData( toc_room_info data )
    {
        m_tempTeamListData = data;
    }

    void Toc_room_info( toc_room_info data )
    {
        OnUpdateTeamList( RoomModel.RoomInfo );
        //Logger.Log("RecvRule:" + data.rule);
        var config = ConfigManager.GetConfig<ConfigSpecialRuleList>().GetLine(data.rule);
        //if (config!=null)
        //{
        //    bigType = config.RuleType;
        //}
        UpdateModeName();
        if (data.sid != 0 && PanelMatchingTips.m_sid != 0 && data.sid == PanelMatchingTips.m_sid)
            NetLayer.Send(new tos_room_start_match());
        ruleSel = config;
        if (ruleSel == null)
        {
            //m_mode1.GetComponent<Toggle>().isOn = true;
            //ruleSel = m_clt[0];
        }
        else
        {

        }
    }

    void Toc_room_leave_team( CDict data )
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    void Toc_player_my_ranks(toc_player_my_ranks data)
    {
        for (int i = 0; i < data.ranks.Length; i++)
        {
            if (PlayerSystem.roleData.level >= 15)
            {
                if (data.ranks[i].name == "qualifying_score")
                {

                    if (data.ranks[i].rank == 0)
                    {
                        m_rank.text = "未上榜";
                    }
                    else
                    {
                        m_rank.text = data.ranks[i].rank.ToString();
                    }
                }
            }
            else
            {
                if (data.ranks[i].name == "newer_qualifying_score")
                {

                    if (data.ranks[i].rank == 0)
                    {
                        m_rank.text = "未上榜";
                    }
                    else
                    {
                        m_rank.text = data.ranks[i].rank.ToString();
                    }
                }
            }
        }
        return;
    }
}
