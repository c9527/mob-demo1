﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PanelGuideTrain : PanelGuide
{
    public PanelGuideTrain()
    {
        SetPanelPrefabPath("UI/Guide/PanelGuideTrain");
        AddPreLoadRes("UI/Guide/PanelGuideTrain", EResType.UI);
    }

    protected override void OnNextBtnClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_trigger_guide());
        base.OnNextBtnClick(target, eventData);
    }
}