﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelGuideAsk : BasePanel
{
    private Button m_btnNew;
    private Button m_btnOld;

    public PanelGuideAsk()
    {
        SetPanelPrefabPath("UI/Guide/PanelGuideAsk");

        AddPreLoadRes("UI/Guide/PanelGuideAsk", EResType.UI);
    }

    public override void Init()
    {
        m_btnNew = m_tran.Find("btnNew").GetComponent<Button>();
        m_btnOld = m_tran.Find("btnOld").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnNew.gameObject).onPointerClick += OnClickNew;
        UGUIClickHandler.Get(m_btnOld.gameObject).onPointerClick += OnClickOld;
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void OnClickOld(GameObject target, PointerEventData eventdata)
    {
        HidePanel();
        GuideManager.StartChapterName("Battle");
        GlobalConfig.clientValue.guideChapterName = "Battle";
    }

    private void OnClickNew(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        GuideManager.StartGuideId(3);//进入新手关卡
        GlobalConfig.clientValue.guideChapterName = "Battle";
    }
}