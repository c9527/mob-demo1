﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelGuide : BasePanel
{
    private Text m_txtName;
    private Text m_txtContent;
    private Button m_btnSkip;
    private Button m_btnNext;
    private GameObject m_goAward;

    private BaseGuide m_guide;

    public PanelGuide()
    {
        SetPanelPrefabPath("UI/Guide/PanelGuide");
        AddPreLoadRes("UI/Guide/PanelGuide", EResType.UI);
    }

    public override void Init()
    {
        m_txtName = m_tran.Find("txtName").GetComponent<Text>();
        m_txtContent = m_tran.Find("txtContent").GetComponent<Text>();
        m_btnSkip = m_tran.Find("btnSkip").GetComponent<Button>();
        m_btnNext = m_tran.Find("btnNext").GetComponent<Button>();
        m_goAward = m_tran.Find("award").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnNext.gameObject).onPointerClick += OnNextBtnClick;
        UGUIClickHandler.Get(m_btnSkip.gameObject).onPointerClick += OnSkipBtnClick;
    }

    protected virtual void OnNextBtnClick(GameObject target, PointerEventData eventData)
    {
        m_guide.CompleteGuide();
    }

    protected virtual void OnSkipBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!UIManager.IsBattle())
            HidePanel();
        else
            //PanelRoomList.SendMsg("leave_room");
            NetLayer.Send(new tos_room_leave());

        var jumpToChapter = m_guide.Config.SkipToChapterName;
        if (!string.IsNullOrEmpty(jumpToChapter))
        {
            GlobalConfig.clientValue.guideChapterName = jumpToChapter;
            GlobalConfig.SaveClientValue("guideChapterName");
            Logger.Log("保存指引进度：" + GlobalConfig.clientValue.guideChapterName);
            GuideManager.StartChapterName(jumpToChapter);
        }
        else
            GuideManager.DestroyGuide();
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;
        m_guide = m_params[0] as BaseGuide;
        if (m_guide == null)
            return;
        m_txtName.text = m_guide.Config.Title;
        m_txtContent.text = m_guide.Config.Content;

        m_goAward.TrySetActive(m_guide.Config.ShowAward);
        if (m_guide.Config.ShowAward)
            m_txtContent.alignment = TextAnchor.UpperLeft;
        else
            m_txtContent.alignment = TextAnchor.MiddleLeft;
        m_btnSkip.gameObject.TrySetActive(m_guide.Config.ShowSkipButton);
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}