﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelPlayerBuffInfo : BasePanel
{
    private toc_player_buff_list.buff[] _data;
    public DataGrid item_list;

    public PanelPlayerBuffInfo()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelPlayerBuffInfo");

        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("UI/PlayerInfo/PanelPlayerBuffInfo", EResType.UI);
    }

    public override void Init()
    {
        item_list = m_tran.Find("frame/item_list/content").gameObject.AddComponent<DataGrid>();
        item_list.autoSelectFirst = false;
        item_list.SetItemRender(m_tran.Find("frame/item_list/Render").gameObject, typeof(BuffListItemRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/action_btn")).onPointerClick += (target, evt) => HidePanel();
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _data = m_params[0] as toc_player_buff_list.buff[];
        }
        item_list.Data = _data;
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

class BuffListItemRender : ItemRender
{
    private toc_player_buff_list.buff _data;

    public Image item_img;
    public Text name_txt;
    public Text time_txt;
    public Text desc_txt;

    public override void Awake()
    {
        item_img = transform.Find("item_img").GetComponent<Image>();
        name_txt = transform.Find("name_txt").GetComponent<Text>();
        time_txt = transform.Find("time_txt").GetComponent<Text>();
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as toc_player_buff_list.buff;
        if (_data == null)
        {
            return;
        }
        var info = ConfigManager.GetConfig<ConfigBuff>().GetLine(_data.buff_id);
        if (info != null)
        {
            item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Hall, "buff_icon_" + info.IconValue));
            item_img.SetNativeSize();
            name_txt.text = info.Name;
            desc_txt.text = info.Desc;
        }
        else
        {
            item_img.SetSprite(null);
            item_img.SetNativeSize();
            name_txt.text = "未配置名字";
            desc_txt.text = "未配置描述";
        }
        WakeupTimer();
    }

    private void WakeupTimer()
    {
        if (!isActiveAndEnabled)
        {
            CancelInvoke();
            return;
        }
        var cd = _data != null ? _data.time_limit - TimeUtil.GetNowTimeStamp() : 0;
        if (IsInvoking() && cd <= 0)
        {
            CancelInvoke("WakeupTimer");
        }
        else if(!IsInvoking() && cd>0)
        {
            InvokeRepeating("WakeupTimer", 1, 1f);
        }
        time_txt.text = TimeUtil.FormatTime(cd,0);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
}

