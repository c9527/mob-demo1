﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
public class PanelOtherPlayerInfo : BasePanel
{

    private Transform m_tabbarParent;
    private Transform m_imgLockParent;
    private RectTransform m_effectPoint;
    private ToggleGroup m_toggleGroup;

    private OtherBagRenderInfo[] _page_data;
    private OtherItemPackageRender[] _weapon_list;

    private int m_openedPageCount = 0;
    private int m_selectedGroupId = 1;

    private toc_player_view_player.EquipGroup[] m_equipsList;


    ///componts
    private Image m_headIcon;

    private Image m_armyLvl;
    private Text m_txtplayerName;
    //private Text m_txtVip;
    private Text m_txtMvp;
    private Text m_txtGoldAce;
    private Text m_txtSilverAce;

    private Text m_txtArmyLvlName;
    private Text m_txtRankScore;
    private Text m_txtKillCnt;
    private Text m_txtTeamName;
    private Text m_txtArmyName;
    private Text m_txtTotalWinCount;
    private Text m_txtRankLevel;
    private Text m_txtRoleId;
    private Text m_txtAllWinRate;
    private Text m_txtShotHead;
    private Text m_txtKillContinue;
    private Button m_btnAddFriend;

    private Image m_imgVip;
    private Image m_headVip;
    private GameObject m_goVip;
    private GameObject m_goName;
    private Vector2 m_anchorName;
    private UIEffect m_effect;

    private GameObject m_btnPlayerZone;

    private long m_playerId;
    private Button m_btnViewBadge;
    private List<int> m_lstBadges;
    public PanelOtherPlayerInfo()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelOtherPlayerInfo");
        AddPreLoadRes("UI/PlayerInfo/PanelOtherPlayerInfo", EResType.UI);

        AddPreLoadRes("UI/Package/ItemPageIcon", EResType.UI);
        AddPreLoadRes("UI/Package/ItemLockIcon", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPackageRender", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
    }

    public override void Init()
    {
        m_tabbarParent = m_tran.Find("frame/baginfo/pageTabbar");
        m_imgLockParent = m_tran.Find("frame/baginfo/pageLock");

        m_headIcon = m_tran.Find("frame/playerinfo/headframe/icon").GetComponent<Image>();
        m_armyLvl = m_tran.Find("frame/playerinfo/headframe/imgArmy").GetComponent<Image>();
        m_txtArmyLvlName = m_tran.Find("frame/playerinfo/headframe/armylvlname").GetComponent<Text>();
        m_txtplayerName = m_tran.Find("frame/playerinfo/playername").GetComponent<Text>();
        m_txtMvp = m_tran.Find("frame/playerinfo/mvp/value").GetComponent<Text>();
        m_txtGoldAce = m_tran.Find("frame/playerinfo/goldAce/value").GetComponent<Text>();
        m_txtSilverAce = m_tran.Find("frame/playerinfo/silverAce/value").GetComponent<Text>();
        //m_txtVip = m_tran.Find("frame/playerinfo/viplvl").GetComponent<Text>();
        m_imgVip = m_tran.Find("frame/playerinfo/vip").GetComponent<Image>();
        m_headVip = m_tran.Find("frame/playerinfo/headframe/headVip").GetComponent<Image>();
        m_effectPoint = m_tran.Find("frame/playerinfo/headframe/headVip/effect").GetComponent<RectTransform>();
        m_goVip = m_tran.Find("frame/playerinfo/vip").gameObject;
        m_goName = m_tran.Find("frame/playerinfo/playername").gameObject;
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition;

        m_txtRankScore = m_tran.Find("frame/playerinfo/rankvalue/value").GetComponent<Text>();
        m_txtKillCnt = m_tran.Find("frame/playerinfo/allkill/value").GetComponent<Text>();
        m_txtArmyName = m_tran.Find("frame/playerinfo/armyvalue/value").GetComponent<Text>();
        m_txtTeamName = m_tran.Find("frame/playerinfo/teamname/value").GetComponent<Text>();
        m_txtTotalWinCount = m_tran.Find("frame/playerinfo/totalwin/value").GetComponent<Text>();
        m_txtRankLevel = m_tran.Find("frame/playerinfo/ranklevel/value").GetComponent<Text>();
        m_txtRoleId = m_tran.Find("frame/playerinfo/roleid/value").GetComponent<Text>();
        m_txtAllWinRate = m_tran.Find("frame/playerinfo/winrate/value").GetComponent<Text>();
        m_txtShotHead = m_tran.Find("frame/playerinfo/shotheadnum/value").GetComponent<Text>();
        m_txtKillContinue = m_tran.Find("frame/playerinfo/killcontinue/value").GetComponent<Text>();
        m_btnAddFriend = m_tran.Find("frame/baginfo/btnAddFriend").GetComponent<Button>();
        m_btnPlayerZone = m_tran.Find("tabBtn/TabZone").gameObject;
        m_btnViewBadge = m_tran.Find("frame/baginfo/btnViewBadge").GetComponent<Button>();
        //Util.SetGrayShader(m_btnViewBadge);
        m_lstBadges = new List<int>();
        _page_data = new OtherBagRenderInfo[]
        {
            new OtherBagRenderInfo() {defaultName = "          ", subType = "WEAPON1", itemId = 0, width = 240, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "     ", subType = "WEAPON2", itemId = 0, width = 240, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "     ", subType = "DAGGER", itemId = 0, width = 166, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "闪光弹", subType = "FLASHBOMB", itemId = 0, width = 166, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "手雷", subType = "GRENADE", itemId = 0, width = 166, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "靴子", subType = "BOOTS", itemId = 0, width = 166, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "防弹衣", subType = "ARMOR", itemId = 0, width = 166, icon = "", group = 0},
            new OtherBagRenderInfo() {defaultName = "头盔", subType = "HELMET", itemId = 0, width = 166, icon = "", group = 0},
        };
        _weapon_list = new OtherItemPackageRender[_page_data.Length];
        for (int i = 0; i < _weapon_list.Length; i++)
        {
            var tran = m_tran.Find("frame/baginfo/weapon_list/item_" + i);
            if (tran != null)
            {
                var render = tran.GetComponent<OtherItemPackageRender>();
                if (render == null)
                {
                    render = tran.gameObject.AddComponent<OtherItemPackageRender>();
                }
                _weapon_list[i] = render;
                render.SetData(_page_data[i]);
            }
        }

        m_toggleGroup = m_tabbarParent.GetComponent<ToggleGroup>();
        SetPageCount(1);


    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_btnAddFriend.gameObject).onPointerClick += OnAddFriend;
        UGUIClickHandler.Get(m_btnPlayerZone).onPointerClick += onClickBtnPlayerZone;
        UGUIClickHandler.Get(m_tran.Find("frame/baginfo/btnViewBadge").gameObject).onPointerClick += OnClkBadge;

    }

    private void onClickBtnPlayerZone(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        UIManager.PopPanel<PanelPlayerZone>(new object[] { m_playerId }, true, this, LayerType.Chat);
    }

    private void OnAddFriend(GameObject target, PointerEventData eventData)
    {
        PlayerFriendData.AddFriend(((toc_player_view_player)m_params[0]).id);
    }

    private void OnItemSelected(object itemRender)
    {

    }

    public override void OnShow()
    {
        m_tran.Find("tabBtn/TabFile").GetComponent<Toggle>().isOn = true;
        UpdateView((toc_player_view_player)m_params[0]);
    }

    public override void OnHide()
    {
        if (m_effect != null)
        {
            m_effect.Destroy();
            m_effect = null;
        }
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {

    }

    private void SetPageCount(int openCount)
    {
        var maxCount = ConfigManager.GetConfig<ConfigEquipBag>().m_dataArr.Length;
        for (int i = m_openedPageCount; i < openCount; i++)
        {
            var page_item = ResourceManager.LoadUI("UI/Package/ItemPageIcon");
            var page = i + 1;
            page_item.name = page.ToString();
            var label_0 = page_item.transform.Find("Background/Label").GetComponent<Image>();
            label_0.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "num_b" + page));
            var toggle = page_item.GetComponent<Toggle>();
            toggle.group = m_toggleGroup;
            toggle.isOn = i == 0;
            page_item.transform.SetLocation(m_tabbarParent);
            page_item.TrySetActive(true);
            UGUIClickHandler.Get(page_item).onPointerClick += OnPageTabbarClick;
        }
        for (int i = openCount; i < m_openedPageCount; i++)
        {
            GameObject.DestroyImmediate(m_tabbarParent.GetChild(m_tabbarParent.childCount - 1).gameObject);
        }

        for (int i = m_imgLockParent.childCount; i < maxCount - openCount; i++)
        {
            var itemLock = ResourceManager.LoadUI("UI/Package/ItemLockIcon");
            ;
            itemLock.transform.SetLocation(m_imgLockParent);
            itemLock.TrySetActive(true);
        }
        var lockItemCount = m_imgLockParent.childCount;
        for (int i = maxCount - openCount; i < lockItemCount; i++)
        {
            GameObject.DestroyImmediate(m_imgLockParent.GetChild(0).gameObject);
        }
        m_openedPageCount = openCount;
    }

    private void OnPageTabbarClick(GameObject target, PointerEventData eventData)
    {
        SelectPage(int.Parse(target.name));
    }

    private void SelectPage(int groupId)
    {
        m_selectedGroupId = groupId;

        for (int j = 0; j < _page_data.Length; j++)
        {
            _page_data[j].itemId = 0;
            _page_data[j].group = m_selectedGroupId;
        }
        var bags = m_equipsList;
        for (int i = 0; i < bags.Length; i++)
        {
            if (bags[i].group_id == groupId)
            {
                var bag = bags[i];
                _page_data[0].itemId = bag.WEAPON1.item_id;
                _page_data[1].itemId = bag.WEAPON2.item_id;
                _page_data[2].itemId = bag.DAGGER.item_id;
                _page_data[3].itemId = bag.FLASHBOMB.item_id;
                _page_data[4].itemId = bag.GRENADE.item_id;
                _page_data[5].itemId = bag.BOOTS.item_id;
                _page_data[6].itemId = bag.ARMOR.item_id;
                _page_data[7].itemId = bag.HELMET.item_id;

                _page_data[0].stage = bag.WEAPON1.stage;
                _page_data[1].stage = bag.WEAPON2.stage;
                _page_data[2].stage = bag.DAGGER.stage;
                _page_data[3].stage = bag.FLASHBOMB.stage;
                _page_data[4].stage = bag.GRENADE.stage;
                _page_data[5].stage = bag.BOOTS.stage;
                _page_data[6].stage = bag.ARMOR.stage;
                _page_data[7].stage = bag.HELMET.stage;

                _page_data[0].time = bag.WEAPON1.time_limit;
                _page_data[1].time = bag.WEAPON2.time_limit;
                _page_data[2].time = bag.DAGGER.time_limit;
                _page_data[3].time = bag.FLASHBOMB.time_limit;
                _page_data[4].time = bag.GRENADE.time_limit;
                _page_data[5].time = bag.BOOTS.time_limit;
                _page_data[6].time = bag.ARMOR.time_limit;
                _page_data[7].time = bag.HELMET.time_limit;

                _page_data[0].part_data = StrengthenModel.Instance.GenWeaponPartCDData(bag.WEAPON1.item_id, bag.WEAPON1.accessory_list, bag.WEAPON1.stage);
                _page_data[1].part_data = StrengthenModel.Instance.GenWeaponPartCDData(bag.WEAPON2.item_id, bag.WEAPON2.accessory_list, bag.WEAPON2.stage);
                break;
            }
        }
        for (int k = 0; k < _weapon_list.Length; k++)
        {
            _weapon_list[k].SetData(_page_data[k]);
        }
    }

    private void UpdateView(toc_player_view_player data)
    {
        if (data == null)
            return;

        m_btnAddFriend.gameObject.TrySetActive(!PlayerFriendData.singleton.isFriend(data.id));

        m_equipsList = data.equip_groups;
        m_playerId = data.id;
        var roleLine = ConfigManager.GetConfig<ConfigRoleLevel>().GetLine(data.level);

        m_headIcon.SetSprite(ResourceManager.LoadRoleIcon(data.icon));
        m_headIcon.SetNativeSize();
        m_txtplayerName.text = data.name;
        //m_txtVip.text = string.Format("VIP{0}", data.vip_level);
        //m_txtVip.gameObject.TrySetActive(data.vip_level != 0);
        m_goName.GetComponent<RectTransform>().anchoredPosition = m_anchorName;
        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            m_goVip.TrySetActive(false);
            Vector2 vipSizeData = m_goVip.GetComponent<RectTransform>().sizeDelta;
            m_goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }

        m_txtArmyLvlName.text = string.Format("{0}级" , data.level);
        m_armyLvl.SetSprite(ResourceManager.LoadArmyIcon(data.level));
        m_txtMvp.text = data.fight_stat.mvp_cnt.ToString();
        m_txtGoldAce.text = data.fight_stat.gold_ace.ToString();
        m_txtSilverAce.text = data.fight_stat.silver_ace.ToString();

        m_txtRoleId.text = data.id.ToString();
        m_txtArmyName.text = roleLine.Title;
        m_txtTeamName.text = data.corps_name;
        m_txtKillCnt.text = data.fight_stat.kill_cnt.ToString();
        m_txtTotalWinCount.text = data.fight_stat.win_cnt.ToString();
        m_txtAllWinRate.text = string.Format("{0}%", data.fight_stat.fight_cnt > 0 ? (int)(((float)data.fight_stat.win_cnt / data.fight_stat.fight_cnt) * 100) : 0);
        m_txtShotHead.text = string.Format("{0}%", data.fight_stat.kill_cnt > 0 ? (int)(((float)data.fight_stat.headshoot / data.fight_stat.kill_cnt) * 100) : 0);
        m_txtKillContinue.text = data.fight_stat.max_multi_kill.ToString();

        m_txtRankScore.text = data.rank_val.ToString();
        var config = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(data.rank_val);
        m_txtRankLevel.text = config != null ? config.Name : "";

        SetPageCount(data.equip_groups.Length);
        SelectPage(1);
        m_lstBadges.Clear();
        m_lstBadges.AddRange(data.badge_list);

        m_headVip.gameObject.TrySetActive(false);
        if (data.hero_type != (int)EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (data.hero_type == (int)EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (data.hero_type == (int)(EnumHeroCardType.super))
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (data.hero_type == (int)EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (m_effect == null)
                {
                    m_effect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD, new Vector3(-3, -2, 0));
                }
            }
        }
    }

    private void OnClkBadge(GameObject target, PointerEventData eventData)
    {
        UIManager.PopChatPanel<PanelViewBadge>(new object[] { m_lstBadges }, false, this);
    }
}