﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelMicroBlogMain : BasePanel
{
    private InputField m_input;
    private Button m_motion_btn;
    private Image m_headIcon;
    private DataGrid m_microBlogDataGrid;
    private List<MicroBlogData> m_microBlogDataList;
    public GameObject dropTarget;


    public PanelMicroBlogMain()
    {
        SetPanelPrefabPath("UI/PlayerInfo/MicroBlog/PanelMicroBlogMain");
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/PanelMicroBlogMain", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/ItemMicroBlog", EResType.UI);
        AddPreLoadRes("Atlas/MicroBlog", EResType.Atlas);

    }

    public static ItemDropInfo[] DropInfos = new ItemDropInfo[]
    {
        new ItemDropInfo {type = "Blog", subtype = "LOOKUP", label = "查看信息"},
        new ItemDropInfo {type = "Blog", subtype = "BLACK", label = "拉黑"}
    };

    

    public override void Init()
    {
        m_motion_btn = m_tran.Find("up/btn_emoji").GetComponent<Button>();
        m_headIcon = m_tran.Find("up/headframe/head").GetComponent<Image>();
        m_input = m_tran.Find("up/InputField").GetComponent<InputField>();
        m_input.gameObject.AddComponent<InputFieldFix>();
        dropTarget = m_tran.Find("dropTarget").gameObject;

        m_microBlogDataGrid = m_tran.Find("middle/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_microBlogDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/PlayerInfo/MicroBlog/ItemMicroBlog"), typeof(ItemMicroBlog));
        
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += OnCloseClick;
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
        UGUIClickHandler.Get(m_motion_btn.gameObject).onPointerClick += (target, evt) =>
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(60, -30)}, false, this).MotionSelected = OnSelectedMotion;
            }
        };
        UGUIClickHandler.Get(m_tran.Find("up/btn_comment")).onPointerClick += OnClickSendBtn;
    }

    private void OnSelectedMotion(string key)
    {
        if (m_input.text.Length + key.Length > m_input.characterLimit)
            return;
        m_input.text += key;
    }


    private void OnClickSendBtn(GameObject target, PointerEventData data)
    {
        string inputText = WordFiterManager.Fiter(m_input.text.Trim());
        if (!String.IsNullOrEmpty(inputText))
        {
            m_input.text = String.Empty;
            NetLayer.Send(new tos_microblog_add_blog() { content = inputText });
        }
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
    }

    public override void OnShow()
    {
        m_headIcon.SetRoleIconOrSelfIcon(PlayerSystem.roleData.icon, PhotoDataManager.IconUrl);
        dropTarget.TrySetActive(false);
        NetLayer.Send(new tos_microblog_feed_list(){last_id = 0, refresh = true});
    }

    private void Toc_microblog_feed_list(toc_microblog_feed_list data)
    {
        var feedList = MicroBlogDataManager.ConvertToMicroBlogDataList(data.author_list, data.list);
        m_microBlogDataList = feedList;
        m_microBlogDataGrid.Data = feedList.ToArray();

    }

    private void Toc_microblog_add_blog(toc_microblog_add_blog data)
    {
        var selfAuthorInfo = MicroBlogDataManager.GetSelfMicroBlogAuthorInfo();
        var blogData = new MicroBlogData();
        blogData.AuthorInfo = selfAuthorInfo;
        blogData.Content = data.content;
        m_microBlogDataList.Insert(0, blogData);
        m_microBlogDataGrid.Data = m_microBlogDataList.ToArray();
    }

    private void Toc_microblog_praise_blog(toc_microblog_praise_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(true, data.praise_cnt);
        }
        
    }

    private void Toc_microblog_cancel_praise(toc_microblog_cancel_praise data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(false, data.praise_cnt);
        }
    }

    private void Toc_microblog_comment_blog(toc_microblog_comment_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private void Toc_microblog_del_comment(toc_microblog_del_comment data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private void Toc_microblog_del_blog(toc_microblog_del_blog data)
    {
        for (int i = 0; i < m_microBlogDataList.Count; i++)
        {
            if (m_microBlogDataList[i].Content.id == data.blog_id)
            {
                m_microBlogDataList.RemoveAt(i);
                break;
            }
        }
        m_microBlogDataGrid.Data = m_microBlogDataList.ToArray();
    }


    private ItemMicroBlog GetMicroBlog(long blogId)
    {
        var items = m_microBlogDataGrid.ItemRenders;
        foreach (var item in items)
        {
            var microBlogData = item.m_renderData as MicroBlogData;
            if (microBlogData.Content.id == blogId)
            {
                return item as ItemMicroBlog;
            }
        }
        return null;
    }

    void OnCloseClick(GameObject target, PointerEventData data)
    {
        if (UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
        }
        if (UIManager.IsOpen<PanelPlayerZone>())
        {
            UIManager.GetPanel<PanelPlayerZone>().HidePanel();
        }
        HidePanel();
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            HidePanel();
//            UIManager.GetPanel<PanelMainPlayerInfo>().curTab = "TabFile";
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] {data, UIManager.CurContentPanel}, true, this);
        }
        else
        {
//            UIManager.GetPanel<PanelMainPlayerInfo>().curTab = "TabFile";
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this);
        }
    }
}


public class ItemMicroBlog : ItemRender
{
    public float m_itemHeight;
    private MicroBlogData m_data;
    private Text m_contentTxt;
    private Image m_sexImg;
    private Image m_headIcon;
    private Image m_likeImg;
    private Image m_contentBg;
    
    private Text m_name;
    private Text m_date;
    private Text m_likeCnt;
    private Text m_commentCnt;
    private GameObject m_opt;
    private GameObject m_del;
    private GameObject m_like;
    private GameObject m_comment;
    private bool m_likeFlag;
    private LayoutElement m_layoutElement;
    private GameObject m_dropTarget;

    public override void Awake()
    {
        m_contentTxt = transform.Find("content/bg/Text").GetComponent<Text>();
        m_opt = transform.Find("opt").gameObject;
        m_del = transform.Find("opt/del").gameObject;
        m_like = transform.Find("opt/like").gameObject;
        m_comment = transform.Find("opt/comment").gameObject;
        m_date = transform.Find("date").GetComponent<Text>();
        m_likeCnt = transform.Find("opt/like/count").GetComponent<Text>();
        m_commentCnt = transform.Find("opt/comment/count").GetComponent<Text>();
        m_headIcon = transform.Find("headframe/head").GetComponent<Image>();
        m_likeImg = transform.Find("opt/like/icon").GetComponent<Image>();
        m_sexImg = transform.Find("sex").GetComponent<Image>();
        m_name = transform.Find("name").GetComponent<Text>();
        m_contentBg = transform.Find("content/bg").GetComponent<Image>();
        m_layoutElement = GetComponent<LayoutElement>();

//        imgChatBg.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(txtChat.preferredWidth, minChatBgWidth, txtChat.rectTransform.sizeDelta.x) + 18, txtChat.preferredHeight + 7);

        UGUIClickHandler.Get(m_del).onPointerClick +=
            delegate
            {
                UIManager.ShowTipPanel("是否删除这条好友圈信息?", () => NetLayer.Send(new tos_microblog_del_blog() {blog_id = m_data.Content.id}));
            };
        UGUIClickHandler.Get(m_like).onPointerClick += delegate
        {
            if (m_data.Content.is_praised)
            {
                NetLayer.Send(new tos_microblog_cancel_praise() { blog_id = m_data.Content.id });
                
            }
            else
            {
                NetLayer.Send(new tos_microblog_praise_blog() { blog_id = m_data.Content.id });
            }
        };
        UGUIClickHandler.Get(m_comment).onPointerClick += delegate
        {
            var layerType = UIManager.IsOpen<PanelChatSmall>()? LayerType.Chat:LayerType.POP;
            UIManager.PopPanel<PanelMicroBlogComment>(new object[] { m_data }, true, null, layerType);
        };
        UGUIClickHandler.Get(transform.Find("headframe")).onPointerClick += OnClickHeadIcon;
    }

    public void UpdatePraise(int praise_cnt)
    {
        m_likeCnt.text = Util.ToThousandStr(praise_cnt);
        m_data.Content.praise_cnt = praise_cnt;
    }

    public void UpdatePraise(bool isPraise, int praise_cnt)
    {
        m_likeCnt.text = Util.ToThousandStr(praise_cnt);
        m_data.Content.praise_cnt = praise_cnt;
        m_data.Content.is_praised = isPraise;

        if (isPraise)
        {
            m_likeImg.SetSprite(ResourceManager.LoadSprite(AtlasName.MicroBlog, "heart"));
        }
        else
        {
            m_likeImg.SetSprite(ResourceManager.LoadSprite(AtlasName.MicroBlog, "thumbup"));
        }
    }

    public void UpdateCommentCnt(int commentCnt)
    {
        m_commentCnt.text = commentCnt.ToString();
        m_data.Content.comment_cnt = commentCnt;
    }


    protected override void OnSetData(object data)
    {
        m_data = data as MicroBlogData;
        if (m_data != null)
        {
            var tran = GetComponent<RectTransform>();
            if (!m_opt.activeInHierarchy)
            {
                m_del.TrySetActive(false);
            }
            else
            {
                m_del.TrySetActive(m_data.AuthorInfo.player_id == PlayerSystem.roleId);
            }

            if (m_data.AuthorInfo.sex == 0)
            {
                m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
            }
            else
            {
                m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
            }

            m_headIcon.SetRoleIconOrSelfIcon(m_data.AuthorInfo.icon, m_data.AuthorInfo.icon_url);
            m_contentTxt.text = m_data.Content.msg;
            m_contentTxt.gameObject.AddMissingComponent<MotionText>().Create(NetChatData.HandleLinkAction);
            m_contentBg.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(m_contentTxt.preferredWidth, 0,
                m_contentTxt.rectTransform.sizeDelta.x) + 18, m_contentTxt.preferredHeight + 7);
            m_date.text = TimeUtil.GetRelativeTimeStr(m_data.Content.time);
            m_name.text = m_data.AuthorInfo.name;
            m_itemHeight = 67 + m_contentTxt.preferredHeight + 7;
            m_layoutElement.preferredHeight = m_itemHeight;
            tran.sizeDelta = new Vector2(tran.sizeDelta.x, m_itemHeight);
            UpdateCommentCnt(m_data.Content.comment_cnt);
            UpdatePraise(m_data.Content.is_praised, m_data.Content.praise_cnt);
        }
       
    }


    public static void DoOption(string subtype, p_microblog_author_info authorInfo)
    {
        if (subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(authorInfo.player_id);
        }
        else if (subtype == "BLACK")
        {
            UIManager.ShowTipPanel("您确定<Color='#996600'> " + authorInfo.name + " </Color>好友拉黑吗？", delegate { NetChatData.AddBlack(authorInfo.player_id); }, null, false, true, "确定", "取消");
        }
    }


    private void OnClickHeadIcon(GameObject go, PointerEventData eventData)
    {
        if (m_data.AuthorInfo.player_id != PlayerSystem.roleId && !UIManager.IsOpen<PanelPlayerZone>())
        {
            m_dropTarget = transform.parent.parent.parent.parent.Find("dropTarget").gameObject;
            Transform tran = null;
            if (UIManager.IsOpen<PanelMicroBlogMain>())
            {
                tran = UIManager.GetPanel<PanelMicroBlogMain>().transform;
            }
            else if (UIManager.IsOpen<PanelChatSmall>())
            {
                tran = UIManager.GetPanel<PanelChatSmall>().transform;
            }
            UIManager.ShowDropList(PanelMicroBlogMain.DropInfos, ItemDropCallBack, tran, go.transform,
            new Vector2(0, 0), UIManager.Direction.Down, 132, 45, UIManager.DropListPattern.Pop);
            m_dropTarget.TrySetActive(true);
        }
    }

    private void ItemDropCallBack(ItemDropInfo dropInfo)
    {
        DoOption(dropInfo.subtype, m_data.AuthorInfo);
        m_dropTarget.TrySetActive(false);
    }
}