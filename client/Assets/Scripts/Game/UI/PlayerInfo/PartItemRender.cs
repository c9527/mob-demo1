﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PartItemRender : ItemRender {
    Image m_item_img;
    GameObject m_checkMark;
    GameObject m_actionBtn;
    Text m_textName;
    Text m_textTime;
    CDItemData m_item;
    Text m_btnTex;
    string m_time;
	// Use this for initialization
	public override  void Awake()
    {
        m_item_img = transform.Find("item_img").GetComponent<Image>();
        m_checkMark = transform.Find("Checkmark").gameObject;
        m_actionBtn = transform.Find("action_btn").gameObject;
        m_textName = transform.Find("textName").GetComponent<Text>();
        m_textTime = transform.Find("textTime").GetComponent<Text>();
        m_btnTex = transform.Find("action_btn/Text").GetComponent<Text>();
        UGUIClickHandler.Get(m_actionBtn.transform).onPointerClick += OnActonClick;
    }

    private void OnActonClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if(!string.IsNullOrEmpty(m_time))
        {
            if(m_time=="已过期")
            {
                UIManager.ShowPanel<PanelBuy>(new object[] { m_item.info, "continue" });
            }
            else
            {
                //NetLayer.Send(new tos_player_default_fight_roles_equip() { itemID = m_item.info.ID, roleID = UIManager.GetPanel<PanelRole>().selectedData.ID });
            }
        }
    }
		
    
    protected override void OnSetData(object data)
    {        
        CDItemData cdItem = new CDItemData(data as p_item);
        m_item = cdItem;
        m_item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, cdItem.info.Icon));
        m_item_img.SetNativeSize();
        m_textName.text = cdItem.info.DispName;
        m_time = TimeUtil.GetRemainTimeString(cdItem.sdata.time_limit);
        m_textTime.text = m_time;
        if(m_time=="已过期")
        {
            m_btnTex.text = "续费";
        }
    }
  
}
