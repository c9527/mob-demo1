﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;


public class PanelChangeText:BasePanel
{
    private InputField m_word;
    private toc_player_get_zone_info data;
    public PanelChangeText()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelChangeText");
        AddPreLoadRes("UI/PlayerInfo/PanelChangeText", EResType.UI);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }


    public override void Init()
    {
        m_word = m_tran.Find("background/Input").GetComponent<InputField>();
        m_word.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnOk")).onPointerClick += OnOkClick;
        UGUIClickHandler.Get(m_tran.Find("background/btnCancel")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    private void OnOkClick(GameObject target, PointerEventData eventData)
    {
        tos_player_edit_zone newdata = new tos_player_edit_zone();
        newdata.p_zone_info = data.p_zone_info;
        for (int i = 0; i < newdata.p_zone_info.Length; i++)
        {
            if (newdata.p_zone_info[i].key == "description")
            {
                newdata.p_zone_info[i].value = m_word.text;
            }
        }

        NetLayer.Send(new tos_player_edit_zone() { p_zone_info = newdata.p_zone_info });
        HidePanel();

        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            data = m_params[0] as toc_player_get_zone_info;
        }
    }

    public override void Update()
    {
        
    }



}

