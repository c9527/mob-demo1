﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;


class PanelRole : BasePanel
{
    int _selected_camp;
    ConfigItemRoleLine _selected_data;
    ConfigItemWeaponLine _default_weapon_data;

    Camera _role_camera;
    GameObject _role_model;
    GameObject _weapon_model;

    Text _role_name_txt;
    Image[] _part_list;
    DataGrid _role_list;

    public PanelRole()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelRole");

        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("UI/PlayerInfo/PanelRole", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/RoleItemRender", EResType.UI);
        AddPreLoadRes("UI/Hall/RoleTexture", EResType.RenderTexture);
    }

    public override void Init()
    {
        _role_camera = new GameObject("panel_role_camera").AddComponent<Camera>();
        _role_camera.orthographic = true;
        _role_camera.orthographicSize = 0.53f;
        _role_camera.farClipPlane = 4;
        _role_camera.targetTexture = ResourceManager.LoadRenderTexture("UI/Hall/RoleTexture");
        _role_camera.transform.SetLocation(null, 100, 1.3f, 2);
        _role_camera.transform.localRotation = Quaternion.Euler(new Vector3(0,180,0));

        _role_name_txt = m_tran.Find("name_txt").GetComponent<Text>();

        _part_list = new Image[4];
        for (int i = 0; i < 4; i++)
        {
            _part_list[i] = m_tran.Find("part_list/item_" + i).GetComponent<Image>();
        }

        _role_list = m_tran.Find("role_list/content").gameObject.AddComponent<DataGrid>();
        _role_list.autoSelectFirst = false;
        _role_list.SetItemRender(ResourceManager.LoadUIRef("UI/PlayerInfo/RoleItemRender"), typeof(RoleItemRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnRole")).onPointerClick += (target,data)=> UIManager.ShowPanel<PanelRole>();
        UGUIClickHandler.Get(m_tran.Find("btnPackage")).onPointerClick += (target, data) => UIManager.ShowPanel<PanelPackage>();
        UGUIClickHandler.Get(m_tran.Find("btnDepot")).onPointerClick += (target, data) => UIManager.ShowPanel<PanelDepot>();

        UGUIDragHandler.Get(m_tran.Find("role_drag_layer")).onDrag += OnRoleDrag;

        UGUIClickHandler.Get(m_tran.Find("role_tabar/item_0")).onPointerClick += onRoleTypeChanged;
        UGUIClickHandler.Get(m_tran.Find("role_tabar/item_1")).onPointerClick += onRoleTypeChanged;
        UGUIClickHandler.Get(m_tran.Find("role_tabar/item_2")).onPointerClick += onRoleTypeChanged;

        UGUIClickHandler.Get(m_tran.Find("desc_btn")).onPointerClick += OnDescBtnClick;
        UGUIClickHandler.Get(m_tran.Find("buy_btn")).onPointerClick += OnBuyBtnClick;

        _role_list.onItemSelected = onSelectedRoleChanged;

        AddEventListener<object>(GameEvent.UI_ROLE_ITEM_SELECTED, onSelectRoleHandler);
        AddEventListener(GameEvent.PROXY_PLAYER_DEFAULT_ROLE_UPDATED, delegate { _role_list.Data = _role_list.Data; });
    }

    private void onSelectRoleHandler(object arg1)
    {
        _role_list.Select(arg1);
    }

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_selected_data == null)
        {
            return;
        }
        var sitem = ItemDataManager.GetDepotItem(_selected_data.ID);
        if (sitem != null && sitem.time_limit == 0)
        {
            UIManager.ShowTipPanel("您已经拥有此角色了");
        }
        else
        {
            var shop_info = ConfigManager.GetConfig<ConfigMall>().GetMallLine(_selected_data.ID);
            if (shop_info != null)
            {
                UIManager.PopPanel<PanelBuy>(new object[] { shop_info });
            }
            else
            {
                UIManager.ShowTipPanel("此角色无法购买");
            }
        }
    }

    private void OnDescBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_selected_data == null)
        {
            return;
        }
        UIManager.PopPanel<PanelRoleDesc>(new object[]{_selected_data.Introduction},true,this);
    }

    private void onSelectedRoleChanged(object renderData)
    {
        _selected_data = renderData as ConfigItemRoleLine;
        if (_selected_data == null)
        {
            return;
        }
        _default_weapon_data = null;

        if (_selected_data.Weapon != null && _selected_data.Weapon.Length > 0) //如果配置表里有配置武器则显示这把武器
        {
            _default_weapon_data = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(_selected_data.Weapon);
        }
        else if (ItemDataManager.m_bags.Length != 0)
        {
            _default_weapon_data = ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON1) as ConfigItemWeaponLine;
            _default_weapon_data = _default_weapon_data == null ? ItemDataManager.GetItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON2) as ConfigItemWeaponLine : _default_weapon_data;
        }
        _role_name_txt.text = _selected_data.DispName;
        for (int i = 0; i < _part_list.Length; i++)
        {
            Image part_render = _part_list[i];
            part_render.SetSprite(UnityEngine.Random.Range(0, 2) == 1 ? ResourceManager.LoadIcon("helmet") : null);
            part_render.SetNativeSize();
        }
        updateRoleModel(_selected_data);
    }

    private void updateRoleModel(ConfigItemRoleLine data)
    {
        if (data == null)
            return;
        var role_name = data.RoleCamp != 3 ? data.OPModel+"Hall" : data.OPModel;
        if(_role_model == null || _role_model.name != role_name)
        {
            if (_role_model != null)//加载前先清理
            {
                GameObject.Destroy(_role_model);
                _role_model = null;
            }
            ResourceManager.LoadModelRef("Roles/" + role_name, model =>
                {
                    if (m_go == null || _selected_data == null || model.name != role_name || (_role_model != null && _role_model.name == model.name))
                    {
                        return;
                    }
                    if (_role_model != null)//由于加载延时所以不是每次加载前一定能够清理，因此在这里再清理一次
                    {
                        GameObject.Destroy(_role_model);
                        _role_model = null;
                    }
                    _role_model = GameObject.Instantiate(model) as GameObject;
                    _role_model.name = role_name;
                    _role_model.transform.localPosition = new Vector3(100, 0, 0);
                    _role_model.transform.localEulerAngles = new Vector3(0, 0, 0);
                    var arr = _role_model.transform.GetComponentsInChildren<Renderer>();
                    for (int i = 0; i < arr.Length; i++)
                    {
                        var texture = arr[i].sharedMaterial.mainTexture;
                        arr[i].material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_PLAYER);
                        arr[i].material.mainTexture = texture;
                        if(arr[i] is SkinnedMeshRenderer)
                        {
                            SkinnedMeshRenderer smr = arr[i] as SkinnedMeshRenderer;
                            smr.quality = SkinQuality.Bone4;
                        }
#if UNITY_EDITOR
                        arr[i].sharedMaterial.shader = Shader.Find(arr[i].sharedMaterial.shader.name);
#endif
                    }

                    //if (_default_weapon_data == null || (data != null && data.RoleCamp == (int)WorldManager.CAMP_DEFINE.ZOMBIE))
                    if (_default_weapon_data == null || _default_weapon_data.Class == WeaponManager.WEAPON_TYPE_PAW)
                    {
                        return;
                    }

//                    var acName = _role_model.GetComponent<Animator>().runtimeAnimatorController.name;
//                    var acArr = acName.Split('_');
//                    ResourceManager.LoadAnimatorController(acArr[0] + "Hall_" + acArr[1],
//                        controller =>
//                        {
//                            if (m_go == null || _selected_data == null || (_role_model != null && _role_model.name == model.name))
//                                return;
//                            _role_model.GetComponent<Animator>().runtimeAnimatorController = controller;
//                        });

                    if (_weapon_model != null)
                    {
                        GameObject.Destroy(_weapon_model);
                        _weapon_model = null;
                    }
                    string modelname;
                    if (_default_weapon_data.HallModelCheck == 0)
                    {
                        modelname = _default_weapon_data.OPModel;
                    }
                    else
                    {
                        modelname = _default_weapon_data.MPModel;
                    }
                    ResourceManager.LoadModel("Weapons/" +modelname, (weapon_model) =>
                    {
                        if (m_go == null || (_weapon_model != null && _weapon_model.name == weapon_model.name))
                        {
                            GameObject.Destroy(weapon_model);
                            return;
                        }
                        _weapon_model = weapon_model;
                        _weapon_model.name = modelname;
                        var arr2 = _weapon_model.transform.GetComponentsInChildren<Renderer>();
                        foreach (var weapon_mat in arr2)
                        {
                            var texture = weapon_mat.sharedMaterial.mainTexture;
                            if (weapon_mat.sharedMaterial.shader.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
                                weapon_mat.material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_WEAPON_CRYSTAL);
                            else
                                weapon_mat.material = ResourceManager.LoadMaterial(GameConst.MAT_HALL_WEAPON);
                            weapon_mat.material.mainTexture = texture;
#if UNITY_EDITOR
                            weapon_mat.sharedMaterial.shader = Shader.Find(weapon_mat.sharedMaterial.shader.name);
#endif
                        }
                        var ani = _role_model.GetComponent<Animator>();
                        //if (_default_weapon_data != null && !string.IsNullOrEmpty(_default_weapon_data.HallAnimation) && _selected_data.RoleCamp != (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                        if (_default_weapon_data != null)
                        {
                            var roleAnim = _default_weapon_data.HallAnimation[0];
                            if (data.ModelSex - 1 < _default_weapon_data.HallAnimation.Length)
                                roleAnim = _default_weapon_data.HallAnimation[data.ModelSex - 1];
                            if (!string.IsNullOrEmpty(roleAnim))
                                ani.Play(roleAnim);
                            else
                                ani.Play("stand");

                            if (_weapon_model != null)
                            {
                                bool isRightHand = _default_weapon_data.SlotType != WeaponBase.WEAPON_SLOTTYPE_LEFTHAND;
                                var modeSexIsMale = data == null || data.ModelSex == 1;
                                Transform transBipRightHandCenter = GameObjectHelper.FindChildByTraverse(_role_model.transform, isRightHand ? ModelConst.BIP_NAME_RIGHT_HAND_CENTER : ModelConst.BIP_NAME_LEFT_HAND_CENTER);
                                var transRightHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipRightHandCenter, isRightHand ? ModelConst.PLAYER_SLOT_RIGHT_HAND : ModelConst.PLAYER_SLOT_LEFT_HAND).transform;
                                transRightHandSlot.ResetLocal();
                                _weapon_model.transform.SetParent(transRightHandSlot, false);
                                _weapon_model.transform.localPosition = modeSexIsMale ? _default_weapon_data.HallPos[0] : _default_weapon_data.HallGrilPos[0];
                                _weapon_model.transform.localRotation = Quaternion.Euler(modeSexIsMale ? _default_weapon_data.HallRot[0] : _default_weapon_data.HallGrilRot[0]);
                                _weapon_model.transform.localScale *= _default_weapon_data.HallModelScale;
                            }
                        }
                        else
                            ani.Play("stand");

//                        ani.SetLayerWeight(1, 0);
//                        ani.SetLayerWeight(2, 0);
                    }, false);
                }, false);
        }
    }

    private void OnRoleDrag(GameObject target, PointerEventData eventdata)
    {
        if (_role_model == null)
        {
            return;
        }
        _role_model.transform.localEulerAngles += new Vector3(0, -eventdata.delta.x * 0.5f, 0);
    }

    private void onRoleTypeChanged(GameObject target,PointerEventData eventData)
    {       
        if (target != null)
        {
            int.TryParse(target.name.Substring(target.name.LastIndexOf("_") + 1), out _selected_camp);
            _selected_camp += 1;
        }
        else
        {           
            m_tran.Find("role_tabar/item_0").GetComponent<Toggle>().isOn = true;
            _selected_camp = 1;
        }
        _role_list.Data = ConfigManager.GetConfig<ConfigItemRole>().getRoleData(_selected_camp);
        _role_list.Select(_role_list.Data!=null ? _role_list.Data[0] : null);
    }

    public override void OnShow()
    {
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.CheckChosen(panelHall.m_btgDepot);
        _selected_camp = 1;
        _selected_data = null;
        _default_weapon_data = null;
        if (_role_camera!=null)
        {
            _role_camera.gameObject.TrySetActive(true);
        }
        onRoleTypeChanged(null, null);
        onSelectedRoleChanged(_role_list.Data!=null && _role_list.Data.Length>0 ? _role_list.Data[0] : null);
    }

    public override void OnHide()
    {
        if (_role_camera!=null)
        {
            _role_camera.gameObject.TrySetActive(false);
        }
        if (_role_model != null)
        {
            GameObject.Destroy(_role_model);
            _role_model = null;
        }
        if (_weapon_model != null)
        {
            GameObject.Destroy(_weapon_model);
            _weapon_model = null;
        }
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelPackage>();
    }

    public override void OnDestroy()
    {
        if (_role_camera!=null)
        {
            GameObject.Destroy(_role_camera);
            _role_camera = null;
        }
        if (_role_model != null)
        {
            GameObject.Destroy(_role_model);
            _role_model = null;
        }
        if (_weapon_model != null)
        {
            GameObject.Destroy(_weapon_model);
            _weapon_model = null;
        }
    }

    public override void Update()
    {
        
    }

    void Toc_item_new(toc_item_new data)
    {
        _role_list.Data = _role_list.Data;
    }

    void Toc_item_update(toc_item_update data)
    {
        _role_list.Data = _role_list.Data;
    }
}

