﻿using UnityEngine;
using UnityEngine.UI;


class RoleItemRender : ItemRender
{
    ConfigItemRoleLine _data;
    p_item _sdata;

    Image _status_img;
    Text _status_ico;
    Image _item_img;
    Text _name_txt;
    Text _desc_txt;
    Text _effect_txt;
    Button _action_btn;

    public override void Awake()
    {
        _status_img = transform.GetComponent<Image>();
        _status_ico = transform.Find("status_ico").GetComponent<Text>();
        _item_img = transform.Find("item_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _desc_txt = transform.Find("desc_txt").GetComponent<Text>();
        _effect_txt = transform.Find("effect_txt").GetComponent<Text>();
        _action_btn = transform.Find("action_btn").GetComponent<Button>();

        UGUIClickHandler.Get(_action_btn.gameObject).onPointerClick += onActionBtnClick;
    }

    private void onActionBtnClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (_data == null)
        {
            return;
        }
        if (_sdata != null)
        {
            GameDispatcher.Dispatch<object>(GameEvent.UI_ROLE_ITEM_SELECTED, _data);
            PlayerSystem.TosSetDefaultRole(_sdata.uid, _data.RoleCamp);
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as ConfigItemRoleLine;
        if (_data == null)
        {
            _sdata = null;
            return;
        }
        _sdata = ItemDataManager.GetDepotItem(_data.ID);
        var is_equip = _sdata!=null ? PlayerSystem.isEquipRole(_sdata.uid) : false;
        _status_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, is_equip ? "role_item_selected_bg" : "role_item_bg"));
        if(is_equip)
        {
            _status_ico.text = "使用中";
        }
        else if(_sdata==null)
        {
            _status_ico.text = "未拥有";
        }
        _action_btn.gameObject.TrySetActive(_sdata != null && (_sdata.time_limit==0 || _sdata.time_limit>TimeUtil.GetNowTimeStamp()) && !is_equip);
        _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, _data.Icon));
        _item_img.SetNativeSize();
        _name_txt.text = _data.DispName;
        _desc_txt.text = _sdata!=null ? TimeUtil.GetRemainTimeString(_sdata.time_limit) : "";
        _effect_txt.text = _data.MaxHP > 100 ? "生命值+" + (_data.MaxHP - 100) : "";
        if (_data.RoleCamp == 3)
        {
            _action_btn.gameObject.TrySetActive(false);
            if (_sdata != null)
            {
                _status_ico.gameObject.TrySetActive(false);
            }
        }
    }
}

