﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


class PanelChangeRoleIcon : BasePanel
{
    private int m_selected;
    private string m_selectedSelfIconUrl;
    private GameObject m_selfTip;
    private GameObject m_regularIconList;
    private GameObject m_selfIconList;
    private int _tabIndex = 0;
    private DataGrid m_regularIconGrid;
    private DataGrid m_selfIconGrid;
    private List<HeadIconData> _regularHeadIconDataList;
    private List<HeadIconData> _honorHeadIconDataList;
    private List<SelfHeadIconData> _selfHeadIconDataList;
    private Toggle _regularHeadIconToggle;
    private Toggle _honorHeadIconToggle;
    private Toggle _selfHeadIconToggle;
    private static int SELF_ICON_MAX_NUM;

    public PanelChangeRoleIcon()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelChangeRoleIcon");
        AddPreLoadRes("UI/PlayerInfo/PanelChangeRoleIcon", EResType.UI);
        AddPreLoadRes("Atlas/HeadIcon", EResType.Atlas);
    }

    public override void Init()
    {
		SELF_ICON_MAX_NUM = ConfigMisc.GetInt("max_icon_url_cnt");
//        m_content = m_tran.Find("background/icon_list/content");
//        m_renderSource = m_tran.Find("background/icon_list/itemRender").gameObject;
        m_selfTip = m_tran.Find("background/self_tip").gameObject;
        m_regularIconList = m_tran.Find("background/icon_list").gameObject;
        m_selfIconList = m_tran.Find("background/self_icon_list").gameObject;

        _regularHeadIconToggle = m_tran.Find("background/tab_list/basic").GetComponent<Toggle>();
        _honorHeadIconToggle = m_tran.Find("background/tab_list/honor").GetComponent<Toggle>();
        _selfHeadIconToggle = m_tran.Find("background/tab_list/self").GetComponent<Toggle>();

        m_regularIconGrid = m_tran.Find("background/icon_list/content").gameObject.AddComponent<DataGrid>();
        m_regularIconGrid.SetItemRender(m_tran.Find("background/icon_list/itemRender").gameObject, typeof(ItemHeadIcon));
        m_regularIconGrid.onItemSelected += OnItemClick;
        m_regularIconGrid.autoSelectFirst = false;

        m_selfIconGrid = m_tran.Find("background/self_icon_list/content").gameObject.AddComponent<DataGrid>();
        m_selfIconGrid.SetItemRender(m_tran.Find("background/self_icon_list/itemRender").gameObject, typeof(ItemSelfHeadIcon));
        m_selfIconGrid.onItemSelected += OnSelfIconClick; 
        m_selfIconGrid.autoSelectFirst = false;


        var icon_data = ConfigManager.GetConfig<ConfigRoleIcon>().m_dataArr;
        _regularHeadIconDataList = new List<HeadIconData>();
        _honorHeadIconDataList = new List<HeadIconData>();
        _selfHeadIconDataList =  new List<SelfHeadIconData>();
        if (icon_data != null)
        {
            for (int i = 0; i < icon_data.Length; i++)
            {
                var item = new HeadIconData();
                item.Data = icon_data[i];
                if (item.Data.Source != 0)
                {
                    _honorHeadIconDataList.Add(item);
                }
                else
                {
                    _regularHeadIconDataList.Add(item);
                }
            }
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnOk")).onPointerClick += OnBtnOkClick;
//        UGUIClickHandler.Get(m_tran.Find("background/btnCancel")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("background/btnClose")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("background/tab_list/basic")).onPointerClick += delegate{_tabIndex = 0; UpdateView(); };
        UGUIClickHandler.Get(m_tran.Find("background/tab_list/honor")).onPointerClick += delegate { _tabIndex = 1; UpdateView(); };
        UGUIClickHandler.Get(m_tran.Find("background/tab_list/self")).onPointerClick += delegate { _tabIndex = 2; UpdateView(); };
        AddEventListener(GameEvent.UI_SELF_ICON_URL_RETRIVED, UpdateView);

    }

    private void OnBtnOkClick(GameObject target, PointerEventData eventData)
    {
        if (_tabIndex != 2)
        {
            NetLayer.Send(new tos_player_set_icon() { icon = m_selected });
        }
        else
        {
            var iconData = m_selfIconGrid.SelectedData<SelfHeadIconData>();
            if (iconData != null)
            {
                if (iconData.IsValidate)
                {
                    PhotoDataManager.Instance.SetUrl(m_selfIconGrid.SeletedIndex);
                }
                else
                {
                    var tipsStr = ((iconData.ValidateCode == -1)) ? "审核失败" : "正在审核中";
                    TipsManager.Instance.showTips(tipsStr);
                    return;
                }
            }
        }
        HidePanel();
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _tabIndex = (int) m_params[0];
        }
        else
        {
            _tabIndex = 0;
        }
        UpdateView();
    }


    public void UpdateView()
    {
        var showSelfList = _tabIndex == 2;
        m_selfTip.TrySetActive(showSelfList);
        m_selfIconList.TrySetActive(showSelfList);
        m_regularIconList.TrySetActive(!showSelfList);
        _selfHeadIconToggle.gameObject.TrySetActive(showSelfList);
        _regularHeadIconToggle.gameObject.TrySetActive(!showSelfList);
        _honorHeadIconToggle.gameObject.TrySetActive(!showSelfList);

        if (showSelfList)
        {
            UpdateSelfIconView();
        }
        else
        {
            UpdateRegularIconView();
        }
    }

    private void UpdateSelfIconView()
    {
        _selfHeadIconToggle.isOn = true;
        var selfIconUrlList = PhotoDataManager.Instance.UrlList;
        m_selfIconGrid.Reset();
        _selfHeadIconDataList.Clear();
        for (int i = 0; i < selfIconUrlList.Count; i++)
        {
            var item = new SelfHeadIconData();
            item.Url = selfIconUrlList[i];
            item.Index = i;
            _selfHeadIconDataList.Add(item);
        }

        if (selfIconUrlList.Count < SELF_ICON_MAX_NUM)
        {
            _selfHeadIconDataList.Add(new SelfHeadIconData() { Index = -1 });
        }
        m_selfIconGrid.Data = _selfHeadIconDataList.ToArray();
        m_selfIconGrid.Select(PhotoDataManager.Instance.SelectedIconIndex);

    }


    private void UpdateRegularIconView()
    {
        m_regularIconGrid.Reset();
        m_selected = PlayerSystem.roleData.icon;
        List<HeadIconData> iconDataList;
        if (_tabIndex == 0)
        {
            _regularHeadIconToggle.isOn = true;
            iconDataList = _regularHeadIconDataList;
        }
        else
        {
            _honorHeadIconToggle.isOn = true;
            iconDataList = _honorHeadIconDataList;
        }
        HeadIconData selectedData = null;
        for (int i = 0; i < iconDataList.Count; i++)
        {
            if (iconDataList[i].Data.ID == m_selected)
            {
                selectedData = iconDataList[i];
            }
            iconDataList[i].Unlock = IsUnLock(iconDataList[i].Data)?1:0;
        }
        var iconSortedArray = iconDataList.OrderByDescending(i => i.Unlock).ToArray();
        m_regularIconGrid.Data =  iconSortedArray;
//        for (int i = 0; i < iconSortedArray.Length; i++)
//        {
//            m_regularIconGrid.ItemRenders[i].GetComponent<Toggle>().interactable = iconSortedArray[i].Unlock > 0;
//        }
        if (selectedData != null)
        {
            m_regularIconGrid.Select(selectedData);
        }
    }

    private void OnItemClick(object data)
    {
        HeadIconData iconData = data as HeadIconData;
        if (iconData != null && iconData.Unlock > 0)
        {
            m_selected = iconData.Data.ID;
        }
    }

    private void OnSelfIconClick(object data)
    {
        SelfHeadIconData iconData = data as SelfHeadIconData;
        if (iconData != null )
        {
            if (iconData.Index < 0)
            {
                UIManager.PopPanel<PanelHeadIcon>(null, true, this);
            }
        }
    }
    
    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    public bool IsUnLock(ConfigRoleIconLine data)
    {
        if (data.Source > 0)     //> 分享头像
        {
            if (ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(data.Source) != null)
            {
                var sitem = ItemDataManager.GetDepoUnlimitItem(data.Source);
                return sitem != null && TimeUtil.GetRemainTimeType(sitem.time_limit) == 0;
            }
            else
            {
                var sitem = ItemDataManager.GetDepotItem(data.Source);
                return sitem != null && TimeUtil.GetRemainTimeType(sitem.time_limit) != -1;
            }
        }
        if (data.Source < 0)    //> 成就头像
        {
            return PlayerAchieveData.m_lstHeadIcon.IndexOf(data.ID) > -1;
        }
        return true;
    }
}

public class HeadIconData
{
    public ConfigRoleIconLine Data;
    // 排序用:1为解锁, 0为未解锁
    public int Unlock;
}

public class SelfHeadIconData
{
    public string Url;
    public string PicPath;
    public bool IsValidate = false;
    public int Index;
    public int ValidateCode;
}

public class ItemHeadIcon : ItemRender
{
    private HeadIconData _data;
    private Image _icon;
    public bool Unlock;
    private Toggle _toggle;

    public override void Awake()
    {
        _icon = transform.Find("icon").GetComponent<Image>();
        _toggle = GetComponent<Toggle>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as HeadIconData;

        var cfg = _data.Data;

        _icon.SetSprite(ResourceManager.LoadRoleIcon(cfg.ID));
        _icon.SetNativeSize();

        Unlock = _data.Unlock > 0;

        if (Unlock)
        {
            Util.SetNormalShader(_icon);
            MiniTipsManager.Remove(gameObject);
            _toggle.interactable = true;
        }
        else
        {
            Util.SetGrayShader(_icon);
            var txt = cfg.IconGetDesc;
            _toggle.interactable = false;
            MiniTipsManager.get(gameObject).miniTipsDataFunc = (go) =>
            {
                if (txt != null && txt != "")
                {
                    return new MiniTipsData() { name = "获得途径", dec = txt };
                }
                return null;
            };
        }
    }
}

public class ItemSelfHeadIcon : ItemRender
{
    private SelfHeadIconData _data;
    private Image _icon;
    private GameObject _imageGo;
    private Toggle _toggle;
    private GameObject _statusGo;
    private Image _statusImg;
    private GameObject _addGo;
    private GameObject _delGo;
    private bool _validatePass;
    private int _validateCode;

    public override void Awake()
    {
        _imageGo = transform.Find("icon").gameObject;
        _icon = transform.Find("icon").GetComponent<Image>();
        _toggle = GetComponent<Toggle>();
        _addGo = transform.Find("add").gameObject;
        _statusGo = transform.Find("status").gameObject;
        _statusImg = _statusGo.GetComponent<Image>();
        _delGo = transform.Find("del").gameObject;

        UGUIClickHandler.Get(_delGo).onPointerClick += (target, data) =>
        { UIManager.ShowTipPanel("是否删除该头像?", () => { PhotoDataManager.Instance.DelUrl(_data.Index); }); };

        _toggle.onValueChanged.AddListener(OnClickToggle);
    }

    private void OnClickToggle(bool value)
    {
        _delGo.TrySetActive(value);
       
    }


    protected override void OnSetData(object data)
    {
        _data = data as SelfHeadIconData;
//        _toggle.interactable = false;
        _validatePass = false;
        _data.IsValidate = false;
        if (_data == null || _data.Index < 0)
        {
            _statusGo.TrySetActive(false);
            _addGo.TrySetActive(true);
            _imageGo.TrySetActive(false);
            _delGo.TrySetActive(false);
            _toggle.interactable = false;
        }
        else
        {
            var url = _data.Url;
            
            SdkManager.ValidateUploadPic(url, (code) =>
            {
                if (code == 1)
                {
                    _statusGo.TrySetActive(false);
                    _validatePass = true;
                    _data.IsValidate = true;
                    _data.ValidateCode = 0;
                }
                else
                {
                    var spriteName = code == -1 ? "fail_review" : "in_reviewing";
                    _data.ValidateCode = code;
                    _statusImg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeadIcon, spriteName));
                    _statusGo.TrySetActive(true);
                }
            });
            _toggle.interactable = true;
            _icon.SetRoleIconBig(url);
            _imageGo.TrySetActive(true);
            _addGo.TrySetActive(false);
            _delGo.TrySetActive(false);
        }

    }
}