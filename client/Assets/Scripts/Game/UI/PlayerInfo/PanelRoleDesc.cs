﻿using UnityEngine;
using UnityEngine.UI;

class PanelRoleDesc : BasePanel
{
    Text _content_txt;
    Text m_titleText;

    public PanelRoleDesc()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelRoleDesc");

        AddPreLoadRes("UI/PlayerInfo/PanelRoleDesc", EResType.UI);
    }

    public override void Init()
    {
        _content_txt = m_tran.Find("content_txt").GetComponent<Text>();
        m_titleText = m_tran.Find("Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok_btn")).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            var str = m_params[0] as string;
            if (!string.IsNullOrEmpty(str))
            {
                str = str.Replace("<br/>", "\n");
            }
            _content_txt.text = str;
            m_titleText.text = m_params.Length >= 2 ? m_params[1] as string : "";
        }
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}

