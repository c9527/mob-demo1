﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelMicroBlogComment:BasePanel
{
    private InputField m_input;
    private DataGrid m_commentGrid;
    private List<MicroBlogCommentData> m_commentData;
    private ItemMicroBlog m_blog;
    private MicroBlogData m_blogData;
    private Image m_roleIcon;
    private RectTransform m_middleRectTran;
    private const int INPUT_CHARACTER_LIMIT = 20;
    public GameObject dropTarget;

    public PanelMicroBlogComment()
    {
        SetPanelPrefabPath("UI/PlayerInfo/MicroBlog/PanelMicroBlogComment");
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/PanelMicroBlogComment", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/ItemMicroBlogComment", EResType.UI);
        AddPreLoadRes("Atlas/MicroBlog", EResType.Atlas);
    }

    public override void Init()
    {
        m_blog = m_tran.Find("up").gameObject.AddComponent<ItemMicroBlog>();
        m_commentGrid = m_tran.Find("middle/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_commentGrid.SetItemRender(ResourceManager.LoadUIRef("UI/PlayerInfo/MicroBlog/ItemMicroBlogComment"), typeof(ItemMicroBlogComment));
        m_input = m_tran.Find("buttom/InputField").GetComponent<InputField>();
        m_tran.Find("buttom/InputField").gameObject.AddComponent<InputFieldFix>();
        m_roleIcon = m_tran.Find("buttom/headframe/head").GetComponent<Image>();
        m_middleRectTran = m_tran.Find("middle").GetComponent<RectTransform>();
        dropTarget = m_tran.Find("dropTarget").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
        UGUIClickHandler.Get(m_tran.Find("buttom/btn_comment")).onPointerClick += OnClickSendBtn;
        UGUIClickHandler.Get(m_tran.Find("buttom/btn_emoji")).onPointerClick += (target, evt) =>
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(60, 240) }, false, this).MotionSelected = OnSelectedMotion;
            }
        };
        AddEventListener<string>(GameEvent.UI_MICROBLOG_COMMENT_REPLY, OnReply);
    }

    private void OnSelectedMotion(string key)
    {
        if (m_input.text.Length + key.Length > m_input.characterLimit)
            return;
        m_input.text += key;
    }

    private void OnClickSendBtn(GameObject target, PointerEventData data)
    {
        string inputText = WordFiterManager.Fiter(m_input.text.Trim());
        if (!String.IsNullOrEmpty(inputText))
        {
            m_input.text = String.Empty;
            NetLayer.Send(new tos_microblog_comment_blog() { blog_id = m_blogData.Content.id, comment = inputText });
        }
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        m_input.characterLimit = INPUT_CHARACTER_LIMIT;
    }

    private void OnReply(string name)
    {
        var inputStr = m_input.text;
        var match = Regex.Match(inputStr, @"^\s*回复.*?:");
        string replyStr = String.Empty;
        string replyPrefix = String.Format("回复{0}:", name);
        if (match.Success)
        {
            replyStr = inputStr.Replace(match.Value, replyPrefix);

        }
        else
        {
            replyStr = (replyPrefix + inputStr);
           
        }
        m_input.characterLimit = replyPrefix.Length + INPUT_CHARACTER_LIMIT;
        m_input.text = replyStr;

    }

    public override void OnShow()
    {
        m_blogData = m_params[0] as MicroBlogData;
        m_blog.SetData(m_blogData);
        m_input.characterLimit = INPUT_CHARACTER_LIMIT;
        dropTarget.TrySetActive(false);

        m_middleRectTran.sizeDelta = new Vector2(m_middleRectTran.sizeDelta.x, 362f-m_blog.m_itemHeight);
        m_roleIcon.SetRoleIconOrSelfIcon(PlayerSystem.roleData.icon, PhotoDataManager.IconUrl);
        NetLayer.Send(new tos_microblog_comment_list() { blog_id = m_blogData.Content.id });

    }

    private void Toc_microblog_comment_list(toc_microblog_comment_list data)
    {
        if (data.blog_id == m_blogData.Content.id)
        {
            m_commentData = MicroBlogDataManager.ConvertToMicroBlogCommentDataList(data.blog_id, m_blogData.AuthorInfo.player_id, data.author_list, data.comment_list);
            m_commentGrid.Data = m_commentData.ToArray();
        }
    }

    private void Toc_microblog_del_comment(toc_microblog_del_comment data)
    {
        for (int i = 0; i < m_commentData.Count; i++)
        {
            if (m_commentData[i].CommentInfo.id == data.comment_id)
            {
                m_commentData.RemoveAt(i);
                break;
            }
        }
        m_commentGrid.Data = m_commentData.ToArray();
    }

    private void Toc_microblog_comment_blog(toc_microblog_comment_blog data)
    {
//        var blogData = new MicroBlogData();
//        blogData.AuthorInfo = selfAuthorInfo;
//        blogData.Content = data.content;
//        m_commentData.Insert(0, blogData);
//        m_commentGrid.Data = m_commentData.ToArray();
        NetLayer.Send(new tos_microblog_comment_list() { blog_id = m_blogData.Content.id });
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            HidePanel();
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] {data, UIManager.CurContentPanel}, true, this);
        }
        else
        {
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this);
        }
    }
}




public class ItemMicroBlogComment : ItemRender
{
    private MicroBlogCommentData m_data;
    private Image m_sexImg;
    private Text m_name;
    private Text m_content;
    private GameObject m_del;
    private RectTransform m_contentTran;
    public float m_itemHeight;
    private Image m_contentBg;
    private LayoutElement m_layoutElement;

    public override void Awake()
    {
        m_sexImg = transform.Find("sex").GetComponent<Image>();
        m_name = transform.Find("name").GetComponent<Text>();
        m_content = transform.Find("content/bg/Text").GetComponent<Text>();
        m_layoutElement = GetComponent<LayoutElement>();
        m_contentBg = transform.Find("content/bg").GetComponent<Image>();
        m_contentTran = transform.Find("content").GetComponent<RectTransform>();
        m_del = transform.Find("opt/del").gameObject;

        UGUIClickHandler.Get(transform.Find("opt/talk")).onPointerClick += OnReply;
        UGUIClickHandler.Get(transform.Find("opt/del")).onPointerClick += delegate
        {
            UIManager.ShowTipPanel("是否删除这条评论?", () => NetLayer.Send(
                new tos_microblog_del_comment(){blog_id = m_data.BlogId,comment_id = m_data.CommentInfo.id}));
        };
        UGUIClickHandler.Get(m_name.gameObject).onPointerClick += OnClickHeadName;
    }

    private void OnReply(GameObject target, PointerEventData eventData)
    {
        GameDispatcher.Dispatch(GameEvent.UI_MICROBLOG_COMMENT_REPLY, m_data.AuthorInfo.name);
    }

    protected override void OnSetData(object data)
    {
        var tran = GetComponent<RectTransform>();
        m_data = data as MicroBlogCommentData;
        m_del.TrySetActive(m_data.BlogOwnerId == PlayerSystem.roleId || m_data.AuthorInfo.player_id == PlayerSystem.roleId);
        m_content.text = m_data.CommentInfo.comment;
        m_name.text = m_data.AuthorInfo.name;

        if (m_data.AuthorInfo.sex == 0)
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
        }
        else
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
        }
        m_contentTran.anchoredPosition = new Vector2(82.03f + m_name.preferredWidth, m_contentTran.anchoredPosition.y);
        m_content.gameObject.AddMissingComponent<MotionText>().Create(NetChatData.HandleLinkAction);
        m_contentBg.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(m_content.preferredWidth, 0,
            m_content.rectTransform.sizeDelta.x) + 18, m_content.preferredHeight + 7);
        m_itemHeight = m_content.preferredHeight + 17;
        m_layoutElement.preferredHeight = m_itemHeight;
        tran.sizeDelta = new Vector2(tran.sizeDelta.x, m_itemHeight);

    }

    public static void DoOption(string subtype, p_microblog_author_info authorInfo)
    {
        if (subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(authorInfo.player_id);
        }
        else if (subtype == "BLACK")
        {
            UIManager.ShowTipPanel("您确定<Color='#996600'> " + authorInfo.name + " </Color>好友拉黑吗？", delegate { NetChatData.AddBlack(authorInfo.player_id); }, null, false, true, "确定", "取消");
        }
    }


    private void OnClickHeadName(GameObject go, PointerEventData eventData)
    {
        if (m_data.AuthorInfo.player_id != PlayerSystem.roleId)
        {
            UIManager.ShowDropList(PanelMicroBlogMain.DropInfos, ItemDropCallBack, UIManager.GetPanel<PanelMicroBlogComment>().transform, go.transform,
            new Vector2(15, 0), UIManager.Direction.Down, 132, 45, UIManager.DropListPattern.Pop);
            UIManager.GetPanel<PanelMicroBlogComment>().dropTarget.TrySetActive(true);
        }
        
        

    }

    private void ItemDropCallBack(ItemDropInfo dropInfo)
    {
        DoOption(dropInfo.subtype, m_data.AuthorInfo);
        UIManager.GetPanel<PanelMicroBlogComment>().dropTarget.TrySetActive(false);

    }
}