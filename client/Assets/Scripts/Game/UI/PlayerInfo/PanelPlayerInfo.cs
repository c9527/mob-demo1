﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PanelMainPlayerInfo : BasePanel
{
    toc_player_view_player m_data;
    //    Image m_imageHead;
    Text m_txtPlayerName;
    Text m_txtVipLvl;
    Image m_imgArmy;
    Text m_txtArmyLvl;
    Image m_imgExp;
    Image m_imgHead;
    Image m_headVip;
    Text m_txtExp;
    Image m_microBlogLock;
    Image m_microBlogBg;
    //Button m_btnVip;

    ConfigRoleLevel m_configRoleLevel;

    GameObject m_rightPanelObj;

    Text m_txtArmyLvTitle;
    Text m_txtTeam;
    Text m_txtRoleId;
    Text m_txtMvpAce;
    Text m_txtWinRate;
    Text m_txtKillCount;
    Text m_txtHeadShot;
    Text m_txtKillContinue;
    Text m_txtCombatInfo;
    //    Text m_txtGrenateKill;
    //    Text m_txtKnifeKill;
    Text m_txtRankScore;
    Image m_imgRankIcon;
    Image m_imgRankWord;
    //Image m_imgHeroCardTBg;

    Dictionary<string, GameObject> m_dicNormalStat;
    Dictionary<string, GameObject> m_dicQualifyingStat;

    GameObject m_goItemProto;
    GameObject m_goContentList;

    ConfigGameRule m_configGameRule;
    GameObject m_goNomarl;
    GameObject m_goRank;

    Toggle m_toggleNomarl;
    Toggle m_toggleRank;

    float m_lastUpdateTime = 0.0f;

    Image m_imgVip;
    GameObject m_goVip;

    GameObject m_btnTabZone;
    GameObject m_btnTabFile;
    GameObject m_btnTabCareer;
    GameObject m_btnTabMicroBlogMain;

    Toggle m_toggleTabZone;
    Toggle m_toggleTabFile;
    Toggle m_toggleTabCareer;
    Toggle m_toggleTabMicroBlogMain;

    public string curTab = "TabFile";
    Image m_imgChenghao;
    GameObject m_goChenghao;
    RectTransform m_effectPoint;
    UIEffect m_effect;

    Text m_mvp;
    Text m_goldace;
    Text m_sliverace;

    DataGrid m_equipDataGrid;
    DataGrid m_bagDataGrid;
    DataGrid m_weaponDataGrid;

    BagRenderInfo[] m_bags;

    private Image m_imgNextBagPage;

    GameObject m_chat;
    GameObject m_delete;
    GameObject m_family_invite;
    GameObject m_follow;
    GameObject m_add;


    GameObject m_friend;
    GameObject m_changeHead;
    BasePanel m_paneloback;

    Image m_campIcon;

    private Toggle m_toggleCampWatcher;
    private Toggle m_toggleCampPatriot;

    private Text m_chatCon;
    private GameObject m_edit;
    RawImage m_roleImage;

    Text m_serverName;

    toc_player_get_zone_info zone_data;
    public PanelMainPlayerInfo()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelMainPlayerInfo");
        AddPreLoadRes("UI/PlayerInfo/PanelMainPlayerInfo", EResType.UI);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/RankList", EResType.Atlas);
        AddPreLoadRes("Atlas/Achievement", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Atlas/Background", EResType.Atlas);
    }


    public override void Init()
    {
        m_dicNormalStat = new Dictionary<string, GameObject>();
        m_dicQualifyingStat = new Dictionary<string, GameObject>();

        m_configGameRule = ConfigManager.GetConfig<ConfigGameRule>();

        m_imgExp = m_tran.Find("left/frame/exp_value_pb/value_img").GetComponent<Image>();
        m_txtExp = m_tran.Find("left/frame/exp_value_pb/Text").GetComponent<Text>();
        //m_btnVip = m_tran.Find("left/frame/topframe/btnVip").GetComponent<Button>();
        //m_btnVip.interactable = false;
        //Util.SetGrayShader(m_btnVip);

        Transform leftTransform = m_tran.Find("left/frame/leftwireframe");
        //        m_imageHead = leftTransform.Find("head").GetComponent<Image>();
        m_txtPlayerName = leftTransform.Find("playername").GetComponent<Text>();
        m_txtVipLvl = leftTransform.Find("viplvl").GetComponent<Text>();
        m_imgVip = leftTransform.Find("VIP").GetComponent<Image>();
        m_goVip = leftTransform.Find("VIP").gameObject;
        m_imgArmy = leftTransform.Find("imgArmy").GetComponent<Image>();
        m_txtArmyLvl = leftTransform.Find("armylvlname").GetComponent<Text>();
        m_txtArmyLvTitle = leftTransform.Find("slipArmyLv/value").GetComponent<Text>();
        m_txtTeam = leftTransform.Find("slipTeam/value").GetComponent<Text>();
        m_txtRoleId = leftTransform.Find("slipRoleId/value").GetComponent<Text>();
        m_imgHead = leftTransform.Find("head/icon").GetComponent<Image>();
        m_headVip = leftTransform.Find("headVip").GetComponent<Image>();
        m_effectPoint = leftTransform.Find("headVip/effect").GetComponent<RectTransform>();
        m_txtRankScore = leftTransform.Find("txtRankScore").GetComponent<Text>();
        m_imgRankIcon = m_tran.Find("Right/imgRankIcon").GetComponent<Image>();
        m_imgRankWord = m_tran.Find("Right/imgRankWord").GetComponent<Image>();
        m_microBlogLock = m_tran.Find("left/tabBtn/TabMicroBlogMain/lock").GetComponent<Image>();
        m_microBlogBg = m_tran.Find("left/tabBtn/TabMicroBlogMain/Background").GetComponent<Image>();

        m_configRoleLevel = ConfigManager.GetConfig<ConfigRoleLevel>();

        m_rightPanelObj = m_tran.Find("left/frame/right").gameObject;

        m_txtMvpAce = m_rightPanelObj.transform.Find("righttop/slip1/value").GetComponent<Text>();
        m_txtWinRate = m_rightPanelObj.transform.Find("righttop/slip2/value").GetComponent<Text>();
        m_txtKillCount = m_rightPanelObj.transform.Find("righttop/slip3/value").GetComponent<Text>();

        m_txtHeadShot = m_rightPanelObj.transform.Find("righttop/slip6/value").GetComponent<Text>();
        m_txtKillContinue = m_rightPanelObj.transform.Find("righttop/slip7/value").GetComponent<Text>();
        m_txtCombatInfo = m_rightPanelObj.transform.Find("righttop/slip5/value").GetComponent<Text>();

        //        m_txtGrenateKill = m_rightPanelObj.transform.Find("righttop/slip3value2/info").GetComponent<Text>();
        //        m_txtKnifeKill = m_rightPanelObj.transform.Find("righttop/slip3value3/info").GetComponent<Text>();

        m_goItemProto = m_rightPanelObj.transform.Find("rightbottom/ScrollRect/row").gameObject;
        m_goContentList = m_rightPanelObj.transform.Find("rightbottom/ScrollRect/content").gameObject;

        m_goNomarl = m_rightPanelObj.transform.Find("rightbottom/group/normal").gameObject;
        m_goRank = m_rightPanelObj.transform.Find("rightbottom/group/rank").gameObject;
        m_toggleNomarl = m_goNomarl.GetComponent<Toggle>();
        m_toggleRank = m_goRank.GetComponent<Toggle>();

        PreCreateFightStat();
        m_btnTabFile = m_tran.Find("left/tabBtn/TabFile").gameObject;
        m_btnTabZone = m_tran.Find("left/tabBtn/TabZone").gameObject;
        m_btnTabCareer = m_tran.Find("left/tabBtn/TabCareer").gameObject;
        m_btnTabMicroBlogMain = m_tran.Find("left/tabBtn/TabMicroBlogMain").gameObject;

        m_toggleTabFile = m_tran.Find("left/tabBtn/TabFile").GetComponent<Toggle>();
        m_toggleTabZone = m_tran.Find("left/tabBtn/TabZone").GetComponent<Toggle>();
        m_toggleTabCareer = m_tran.Find("left/tabBtn/TabCareer").GetComponent<Toggle>();
        m_toggleTabMicroBlogMain = m_tran.Find("left/tabBtn/TabMicroBlogMain").GetComponent<Toggle>();

       

        m_mvp = m_tran.Find("left/frame/mvp/Text").GetComponent<Text>();
        m_goldace = m_tran.Find("left/frame/goldace/Text").GetComponent<Text>();
        m_sliverace = m_tran.Find("left/frame/sliverace/Text").GetComponent<Text>();
        m_imgChenghao = m_tran.Find("left/frame/leftwireframe/chenghao").GetComponent<Image>();
        m_goChenghao = m_tran.Find("left/frame/leftwireframe/btnChangeChenghao").gameObject;

        //m_imgHeroCardTBg = m_tran.Find("left/frame/topframe").GetComponent<Image>();     

        m_equipDataGrid = m_tran.Find("Right/weapon/leftTop/equipSlot").gameObject.AddComponent<DataGrid>();
        m_equipDataGrid.SetItemRender(m_tran.Find("Right/weapon/leftTop/equipSlot/item").gameObject, typeof(ItemEquipSlotRenderByItem));

        m_bagDataGrid = m_tran.Find("Right/weapon/leftBottom/bagBar/content").gameObject.AddComponent<DataGrid>();
        m_bagDataGrid.SetItemRender(m_tran.Find("Right/weapon/leftBottom/bagBar/content/bag").gameObject, typeof(ItemBagRender));
        m_bagDataGrid.autoSelectFirst = true;
        m_bagDataGrid.onItemSelected += OnBagItemSelected;

        m_imgNextBagPage = m_tran.Find("Right/weapon/leftBottom/btnNextPage").GetComponent<Image>();

        m_weaponDataGrid = m_tran.Find("Right/weapon/leftBottom/weaponSlot").gameObject.AddComponent<DataGrid>();
        m_weaponDataGrid.SetItemRender(m_tran.Find("Right/weapon/leftBottom/weaponSlot/item").gameObject, typeof(ItemEquipSlotRenderByItem));

        m_friend = m_tran.Find("Right/friendbtnGroup").gameObject;
        m_chat = m_tran.Find("Right/friendbtnGroup/chat").gameObject;
        m_delete = m_tran.Find("Right/friendbtnGroup/delete").gameObject;
        m_family_invite = m_tran.Find("Right/friendbtnGroup/family_invite").gameObject;
        
        m_follow = m_tran.Find("Right/friendbtnGroup/follow").gameObject;
        m_add = m_tran.Find("Right/friendbtnGroup/add").gameObject;

        m_changeHead = m_tran.Find("left/frame/leftwireframe/btnChangeIcon").gameObject;

        m_toggleCampWatcher = m_tran.Find("Right/campBtn/btnCampWatcher").GetComponent<Toggle>();
        m_toggleCampPatriot = m_tran.Find("Right/campBtn/btnCampPatriot").GetComponent<Toggle>();

        m_chatCon = m_tran.Find("Right/chat/Text").GetComponent<Text>();
        m_edit = m_tran.Find("Right/chat/btnEdit").gameObject;
        m_roleImage = m_tran.Find("roleImage").GetComponent<RawImage>();
        m_roleImage.enabled = true;
        m_roleImage.texture = ModelDisplay.RenderTexture;

        m_serverName = m_tran.Find("left/frame/leftwireframe/ServerName").GetComponent<Text>();
    }
    public override void InitEvent()
    {
        //AddEventListener<CDict>(GameEvent.PROXY_PLAYER_STAT_OF_FIGHT, OnFightStatUpdate);
        UGUIClickHandler.Get(m_tran.Find("left/frame/leftwireframe/btnChangeIcon")).onPointerClick += delegate { UIManager.PopPanel<PanelChangeRoleIcon>(); };
        UGUIClickHandler.Get(m_goNomarl).onPointerClick += OnClickShowNormal;
        UGUIClickHandler.Get(m_goRank).onPointerClick += OnClickShowRank;
        UGUIClickHandler.Get(m_tran.Find("left/btnClose")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_btnTabFile).onPointerClick += OnClickBtnTab;
        UGUIClickHandler.Get(m_btnTabZone).onPointerClick += OnClickBtnTab;
        UGUIClickHandler.Get(m_btnTabCareer).onPointerClick += OnClickBtnTab;
        UGUIClickHandler.Get(m_btnTabMicroBlogMain).onPointerClick += OnClickBtnTab;
        UGUIClickHandler.Get(m_imgNextBagPage.gameObject).onPointerClick += OnBagPageClick;
        UGUIClickHandler.Get(m_chat).onPointerClick += OnClickChat;
        UGUIClickHandler.Get(m_delete).onPointerClick += OnClickDelete;
        UGUIClickHandler.Get(m_family_invite).onPointerClick += OnClickFamilyInvite;
        UGUIClickHandler.Get(m_follow).onPointerClick += delegate { RoomModel.FollowFriend(m_data.id, false, false); };
        UGUIDragHandler.Get(m_tran.Find("Right/dragArea")).onDrag += OnRoleDrag;
        UGUIClickHandler.Get(m_toggleCampWatcher.gameObject).onPointerClick += OnCampBtnClick;
        UGUIClickHandler.Get(m_toggleCampPatriot.gameObject).onPointerClick += OnCampBtnClick;
        UGUIClickHandler.Get(m_goChenghao).onPointerClick += OnClickChenghao;
        UGUIClickHandler.Get(m_edit).onPointerClick += delegate { UIManager.PopPanel<PanelChangeText>(new object[1] { zone_data }); };
        UGUIClickHandler.Get(m_add).onPointerClick += delegate { PlayerFriendData.AddFriend(m_data.id); };
        //UGUIClickHandler.Get(m_tran.Find("left/frame/leftwireframe/btnViewBadge").gameObject).onPointerClick += OnClickBadge;
    }

    private void OnCampBtnClick(GameObject target, PointerEventData eventdata)
    {
        if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.UNION)
        {
            ModelDisplay.ModelDefaultCamp = WorldManager.CAMP_DEFINE.REBEL;
            
        }
        else if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.REBEL)
        {
            ModelDisplay.ModelDefaultCamp = WorldManager.CAMP_DEFINE.UNION;
        }
        for (int i = 0; i < m_data.roles.Length; i++)
        {
            if (m_data.roles[i].camp == (int)ModelDisplay.ModelDefaultCamp)
            {
                ModelDisplay.ConfigRole = ConfigManager.GetConfig<ConfigItemRole>().GetLine(m_data.roles[i].item_id);
            }
        }

        UpdateCampIcon();
        ModelDisplay.UpdateModel();
    }

    private void UpdateCampIcon()
    {
        if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.REBEL)
        {
            m_toggleCampWatcher.gameObject.TrySetActive(false);
            m_toggleCampPatriot.gameObject.TrySetActive(true);
        }
        else if (ModelDisplay.ModelDefaultCamp == WorldManager.CAMP_DEFINE.UNION)
        {
            m_toggleCampWatcher.gameObject.TrySetActive(true);
            m_toggleCampPatriot.gameObject.TrySetActive(false);
        }
    }

    private void OnRoleDrag(GameObject target, PointerEventData eventdata)
    {
        ModelDisplay.Rotation(eventdata.delta.x);
    }
    void OnClickDelete(GameObject target, PointerEventData eventData)
    {
        //fjs>>2015.10.13 删除增加二次确认操作 
        UIManager.ShowTipPanel("您确定删除<Color='#996600'> " + m_data.name + " </Color>好友吗？", SureDelete, CancelDelete, false, true, "确定", "取消");
        //  PlayerFriendData.AddBlack(m_curSelected.m_id);
        // PlayerFriendData.DelBlack(m_curSelected.m_id);
        //        PlayerFriendData.SendFriendMsg("add_black", m_curSelected.m_id);
        //        PlayerFriendData.SendFriendMsg("del_black", id);
    }

    void OnClickFamilyInvite(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelFamilyAdd>(new object[] {m_data.id, m_data.name}, true, this);
    }

    

    void SureDelete()
    {
        //PlayerFriendData.AddBlack(m_curSelected.m_id);
        //PlayerFriendData.DelBlack(m_curSelected.m_id);
        PlayerFriendData.DelFriend(m_data.id);
        m_friend.TrySetActive(false);
    }

    void CancelDelete()
    {

    }

    void OnClickChat(GameObject target, PointerEventData eventData)
    {
        bool isFriend = PlayerFriendData.singleton.isFriend(m_data.id);
        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = m_data.id;
        rcd.m_strName = m_data.name;
        rcd.m_iSex = m_data.sex;
        rcd.m_iLevel = m_data.level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = m_data.icon;
        ChatMsgTips msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = { msgTips };
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, this);
    }

    public void UpdataBag()
    {
        var equip_group = m_data.equip_groups;
        int num = equip_group.Length;
        m_bags = new BagRenderInfo[8];
        for (int i = 0; i < m_bags.Length; i++)
        {
            m_bags[i] = new BagRenderInfo();
            m_bags[i].bagId = i + 1;
            m_bags[i].label = (i + 1).ToString(); ;
            var data = m_bags[i];
            data.isLock = i >= num;
            data.isCanBuy = false;
        }

        m_bagDataGrid.UpdateData = m_bags;

    }


    private void OnClickBtnTab(GameObject target, PointerEventData eventData)
    {
        if (target.name == curTab)
        {
            return;
        }
         tabBtnPageControl(target.name);
    }

  

    private void tabBtnPageControl(string tabName)
    {
        curTab = tabName;
        if (tabName == "TabFile")
        {
            //显示档案信息
            
            m_tran.Find("left/btnClose").gameObject.TrySetActive(true);
            m_tran.Find("left/frame").gameObject.TrySetActive(true);
            if (UIManager.IsOpen<PanelPlayerZone>())
            {
                UIManager.GetPanel<PanelPlayerZone>().HidePanel();
            }
            if (UIManager.IsOpen<PanelCareer>())
            {
                UIManager.GetPanel<PanelCareer>().HidePanel();
            }
            if (UIManager.IsOpen<PanelMicroBlogMain>())
            {
                UIManager.GetPanel<PanelMicroBlogMain>().HidePanel();
            }
            m_toggleTabFile.isOn = true;
        }
        else if (tabName == "TabZone")
        {//个人空间信息
            m_tran.Find("left/btnClose").gameObject.TrySetActive(false);
            m_tran.Find("left/frame").gameObject.TrySetActive(false);
            UIManager.PopPanel<PanelPlayerZone>(new object[1] { m_data.id }, false, this);
            if (UIManager.IsOpen<PanelCareer>())
            {
                UIManager.GetPanel<PanelCareer>().HidePanel();
            }
            if (UIManager.IsOpen<PanelMicroBlogMain>())
            {
                UIManager.GetPanel<PanelMicroBlogMain>().HidePanel();
            }
            m_toggleTabZone.isOn = true;
        }
        else if (tabName == "TabCareer")
        {
            m_tran.Find("left/btnClose").gameObject.TrySetActive(false);
            m_tran.Find("left/frame").gameObject.TrySetActive(false);
            UIManager.PopPanel<PanelCareer>(new object[1] { m_data }, false);
            if (UIManager.IsOpen<PanelPlayerZone>())
            {
                UIManager.GetPanel<PanelPlayerZone>().HidePanel();
            }
            if (UIManager.IsOpen<PanelMicroBlogMain>())
            {
                UIManager.GetPanel<PanelMicroBlogMain>().HidePanel();
            }
            m_toggleTabCareer.isOn = true;
        }
        else if (tabName == "TabMicroBlogMain")
        {
            m_tran.Find("left/btnClose").gameObject.TrySetActive(false);
            m_tran.Find("left/frame").gameObject.TrySetActive(false);
            UIManager.PopPanel<PanelMicroBlogMain>(new object[1] { m_data }, false);
            if (UIManager.IsOpen<PanelPlayerZone>())
            {
                UIManager.GetPanel<PanelPlayerZone>().HidePanel();
            }
            if (UIManager.IsOpen<PanelCareer>())
            {
                UIManager.GetPanel<PanelCareer>().HidePanel();
            }
            m_toggleTabMicroBlogMain.isOn = true;
        }
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        //UIManager.PopPanel<PanelCareer>(new object[1] { data }, false);
    }

    private void OnBagItemSelected(object renderData)
    {
        var vo = renderData as BagRenderInfo;
        if (vo == null)
            return;
        if (vo.isLock)
        {
            return;
        }
        SlotRenderInfoByItem[] m_equipInfos = new SlotRenderInfoByItem[5];
        m_equipInfos[0] = new SlotRenderInfoByItem();
        m_equipInfos[1] = new SlotRenderInfoByItem();
        m_equipInfos[2] = new SlotRenderInfoByItem();
        m_equipInfos[3] = new SlotRenderInfoByItem();
        m_equipInfos[4] = new SlotRenderInfoByItem();
        var bag = m_data.equip_groups[vo.bagId - 1];
        m_equipInfos[0].itemid = bag.HELMET.item_id;
        m_equipInfos[1].itemid = bag.ARMOR.item_id;
        m_equipInfos[2].itemid = bag.BOOTS.item_id;
        m_equipInfos[3].itemid = bag.GRENADE.item_id;
        m_equipInfos[4].itemid = bag.FLASHBOMB.item_id;
        m_equipDataGrid.Data = m_equipInfos;


        SlotRenderInfoByItem[] m_weapons = new SlotRenderInfoByItem[3];
        m_weapons[0] = new SlotRenderInfoByItem();
        m_weapons[1] = new SlotRenderInfoByItem();
        m_weapons[2] = new SlotRenderInfoByItem();
        m_weapons[0].itemid = bag.WEAPON1.item_id;
        m_weapons[1].itemid = bag.WEAPON2.item_id;
        m_weapons[2].itemid = bag.DAGGER.item_id ;
        m_weaponDataGrid.Data = m_weapons;
    }

   

    public override void OnShow()
    {
        if (HasParams())
        {
            m_data = m_params[0] as toc_player_view_player;
            UpdataBag();
            m_bagDataGrid.Select(0);
            //UIManager.HidePanel<PanelHall>();
            if (PlayerFriendData.singleton.isFriend(m_data.id))
            {
                m_add.TrySetActive(false);
                m_follow.TrySetActive(true);
                m_delete.TrySetActive(false);
                m_family_invite.TrySetActive(true);
                m_chat.TrySetActive(true);
            }
            else
            {
                m_add.TrySetActive(true);
                m_follow.TrySetActive(false);
                m_delete.TrySetActive(false);
                m_family_invite.TrySetActive(true);
                m_chat.TrySetActive(true);
            }
            ModelDisplay.SetCameraPosArr(new Vector3[] { new Vector3(1000.239f, 1.61f, 3.67f) });
            ModelDisplay.Visible = true;
            if (m_data.id == PlayerSystem.roleId)
            {
                m_changeHead.TrySetActive(true);
                m_goChenghao.TrySetActive(true);
                m_imgChenghao.enabled = true;
                m_edit.TrySetActive(true);
                m_add.TrySetActive(false);
                m_follow.TrySetActive(false);
                m_delete.TrySetActive(false);
                m_family_invite.TrySetActive(false);
                m_chat.TrySetActive(false);
                m_btnTabMicroBlogMain.TrySetActive(true);
                if (PlayerSystem.roleData.level >= ConfigMisc.GetInt("microblog_open_level"))
                {
                    Util.SetNormalShader(m_microBlogBg);
                    m_microBlogLock.gameObject.TrySetActive(false);
                }
                else
                {
                    Util.SetGrayShader(m_microBlogBg);
                    m_microBlogLock.gameObject.TrySetActive(true);
                }
            }
            else
            {
                m_changeHead.TrySetActive(false);
                m_goChenghao.TrySetActive(false);
                m_imgChenghao.enabled = false;
                m_edit.TrySetActive(false);
                m_btnTabMicroBlogMain.TrySetActive(false);

            }

            

            OnFightStatUpdate();
            if (UIManager.GetPanel<PanelHall>()!=null)
            {
                UIManager.GetPanel<PanelHall>().HideHall();
            }
            if (m_params.Length > 1)
            {
                m_paneloback = m_params[1] as BasePanel;
            }
            else
            {
                m_paneloback = null;
            }

            for (int i = 0; i < m_data.roles.Length; i++)
            {
                if (m_data.roles[i].camp == (int)ModelDisplay.ModelDefaultCamp)
                {
                    ModelDisplay.ConfigRole = ConfigManager.GetConfig<ConfigItemRole>().GetLine(m_data.roles[i].item_id);
                }
            }
            ModelDisplay.ConfigWeapon = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(m_data.equip_groups[0].WEAPON1.item_id);
            UpdateCampIcon();
            ConfigItemDecorationLine[] list = new ConfigItemDecorationLine[5];
            for (int i = 0; i < m_data.equip_groups[0].decorations.Length; i++)
            {
                var config = ItemDataManager.GetItem(m_data.equip_groups[0].decorations[i].item_id) as ConfigItemDecorationLine;
                list[int.Parse(config.Part) - 1] = config;
            }
            ModelDisplay.ConfigDecoration = list;
            ModelDisplay.UpdateModel();
            NetLayer.Send(new tos_player_get_zone_info() { owner_id = m_data.id });
        }
        tabBtnPageControl(curTab);
//        if (curTab == "TabZone")
//        {
//            tabBtnPageControl("TabZone");
//        }
//        if (curTab == "TabCareer")
//        {
//            tabBtnPageControl("TabCareer");
//        }
//        if (curTab == "TabMicroBlogMain")
//        {
//            tabBtnPageControl("TabMicroBlogMain");
//        }
        ShowPlayerBaseInfo();
        if (m_lastUpdateTime == 0.0f)
        {
            m_rightPanelObj.TrySetActive(false);
            SendMsg("fight_stat");
        }
        else
        {
            if (Time.realtimeSinceStartup - m_lastUpdateTime >= 30.0f)
            {
                SendMsg("fight_stat");
            }
        }
        

    }
    public override void OnHide()
    {
        ModelDisplay.ConfigRole = null;
        ModelDisplay.Visible = false;
        ModelDisplay.ConfigWeapon = null;
        ModelDisplay.ConfigDecoration = null;
        ModelDisplay.UpdateModel();
        if (UIManager.GetPanel<PanelHall>() != null)
        {
            UIManager.GetPanel<PanelHall>().ShowHall();
            
            if (m_paneloback != null)
            {
                if (m_paneloback == UIManager.GetPanel<PanelHallBattle>())
                {
                    ModelDisplay.Visible = true;
                    ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.2f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
                }
                if (m_paneloback == UIManager.GetPanel<PanelMall>())
                {
                    ModelDisplay.Visible = true;
                    ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.11f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
                }
                if (m_paneloback == UIManager.GetPanel<PanelSocity>())
                {
                    ModelDisplay.Visible = true;
                    ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.11f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
                }
                if (m_paneloback == UIManager.GetPanel<PanelDepot>())
                {
                    ModelDisplay.Visible = true;
                    ModelDisplay.SetCameraPosArr(new[] { new Vector3(999.2f, 1.6f, 3.83f), new Vector3(998.68f, 1.47f, 6f) });
                }
                if (m_paneloback == UIManager.GetPanel<PanelRoom>()||m_paneloback==UIManager.GetPanel<PanelRoom8v8>()||
                    m_paneloback==UIManager.GetPanel<PanelRoomWorldBoss>()||m_paneloback==UIManager.GetPanel<PanelTeam>()||
                    m_paneloback==UIManager.GetPanel<PanelSurvivalRoom>())
                {
                    if (RoomModel.RoomInfo == null)
                    {
                        return;
                    }
                }
                if (m_paneloback == UIManager.GetPanel<PanelLotteryNew>())
                {
                    UIManager.GetPanel<PanelHall>().HideButton();
                }
                UIManager.ShowPanel(m_paneloback);
            }
        }

        if (UIManager.IsOpen<PanelPlayerZone>())
        {
            UIManager.GetPanel<PanelPlayerZone>().HidePanel();
        }
        if (UIManager.IsOpen<PanelCareer>())
        {
            UIManager.GetPanel<PanelCareer>().HidePanel();
        }
        if (UIManager.IsOpen<PanelMicroBlogMain>())
        {
            UIManager.GetPanel<PanelMicroBlogMain>().HidePanel();
        }
        curTab = "TabFile";
    }


    void Toc_player_get_zone_info(toc_player_get_zone_info data)
    {
        zone_data = data;
        for (int i = 0; i < zone_data.p_zone_info.Length; i++)
        {
            if (zone_data.p_zone_info[i].key == "description")
            {
                m_chatCon.text = zone_data.p_zone_info[i].value;
            }
        }

    }
    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {

    }

    void ShowPlayerBaseInfo()
    {
        //var roleData = PlayerSystem.roleData;
        m_txtPlayerName.text = m_data.name;
        //PlayerSystem.GetRealServerId()
        m_serverName.text = "[" + PlayerSystem.GetRoleSeverCNName(m_data.id) + "]";
        var heroTpye = HeroCardDataManager.getHeroMaxCardType();
        if(heroTpye == EnumHeroCardType.normal)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_NORMAL);
        }
        else if (heroTpye == EnumHeroCardType.super)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_SUPER);
        }
        else if (heroTpye == EnumHeroCardType.legend)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_LEGEND);
        }

        if (m_data.vip_level > 0)
        {
            string strVip = string.Format("VIP{0}", m_data.vip_level);
            m_txtVipLvl.text = strVip;
        }
        else
        {
            m_txtVipLvl.text = "";
        }

        if (MemberManager.IsHighMember())
        {
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (MemberManager.IsMember())
        {
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else
        {
            m_goVip.TrySetActive(false);
        }
        int curLevel = m_data.level;
        string strArmyLvl = string.Format("{0}级", curLevel);
        m_txtArmyLvl.text = strArmyLvl;

        m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(curLevel));

        var curLine = m_configRoleLevel.GetLine(curLevel);
        m_txtArmyLvTitle.text = curLine != null ? curLine.Title : "";

        m_txtTeam.text = m_data.corps_name;
        m_txtRoleId.text = m_data.id.ToString();

        m_imgHead.SetRoleIcon(m_data.icon);
        m_imgHead.SetNativeSize();

        ConfigRoleLevelLine nextLine = m_configRoleLevel.GetLine(curLevel + 1);
        if (nextLine != null)
        {
            int nextLvlNeedExp = nextLine.NeedExp;
            long curExp = m_data.exp;

            float fProcess = (float)((float)curExp / (float)nextLvlNeedExp);
            if (fProcess == 1.0)
            {
                fProcess = 0.0f;
            }

            m_imgExp.fillAmount = fProcess;
            m_txtExp.text = curExp + "/" + nextLvlNeedExp;
        }
        else
        {
            m_imgExp.fillAmount = 1.0f;
            m_txtExp.text = "Max";
        }
        if (m_data.id == PlayerSystem.roleId)
        {
            if (PlayerSystem.roleData.chenghao == 0)
            {
                if (PlayerAchieveData.m_lstChenghao.Count > 0)
                {
                    PlayerSystem.roleData.chenghao = PlayerAchieveData.m_lstChenghao[0];
                    NetLayer.Send(new tos_player_set_chenghao() { chenghao = PlayerAchieveData.m_lstChenghao[0] });
                }
            }
            if (PlayerSystem.roleData.chenghao > 0)
            {
                m_goChenghao.TrySetActive(true);
                var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
                ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(PlayerSystem.roleData.chenghao);
                if (lineChenghao != null)
                {
                    m_imgChenghao.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
                    m_imgChenghao.SetNativeSize();
                }
            }
            else
            {
                m_goChenghao.TrySetActive(false);
                m_imgChenghao.gameObject.TrySetActive(false);
            }
        }

        var heroType = EnumHeroCardType.none;
        m_headVip.gameObject.TrySetActive(false);
        if (m_data.id == PlayerSystem.roleId)
        {
            heroTpye = HeroCardDataManager.getHeroMaxCardType();
        }
        else
        {
            heroTpye = (EnumHeroCardType)m_data.hero_type;
        }
        if (heroType != (int)EnumHeroCardType.none)
        {
            
            m_headVip.gameObject.TrySetActive(true);
            if (heroTpye == EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroTpye == EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroTpye == EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (m_effect == null)
                {
                    m_effect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD, new Vector3(-3, 0, 0));
                }
            }
        }

    }

    void ChangeHeroCardBg(string topBgName)
    {
        //m_imgHeroCardTBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, topBgName));
    }

    public static void SendMsg(string key, object value = null)
    {
        CDict proto = new CDict();
        proto["typ"] = "player";
        proto["key"] = key;
        if (value != null)
            proto["val"] = value;
        NetLayer.SendPack(proto);
    }

    void OnFightStatUpdate()
    {
        m_lastUpdateTime = Time.realtimeSinceStartup;
        m_rightPanelObj.TrySetActive(true);

        UpdateGlobelFightStat();

        UpdateFightStat(0, m_dicNormalStat);
        UpdateFightStat(1, m_dicQualifyingStat);

        if (m_toggleNomarl.isOn)
        {
            ShowList(m_dicNormalStat);
            m_txtKillContinue.text = m_data.mode_stats[0].max_multi_kill.ToString();
        }
        else if (m_toggleRank.isOn)
        {
            ShowList(m_dicQualifyingStat);
            m_txtKillContinue.text = m_data.mode_stats[1].max_multi_kill.ToString();
        }
    }

    void UpdateGlobelFightStat()
    {
        int kill_cnt = m_data.fight_stat.kill_cnt;
        int death_cnt = m_data.fight_stat.death_cnt;
        int win_cnt = m_data.fight_stat.win_cnt;
        int fight_cnt = m_data.fight_stat.fight_cnt;
        int headshoot = m_data.fight_stat.headshoot;
        int gold_ace = m_data.fight_stat.gold_ace;
        int silver_ace = m_data.fight_stat.silver_ace;
        int mvp_cnt = m_data.fight_stat.mvp_cnt;
        int max_multi_kill = m_data.fight_stat.max_multi_kill; 

        m_mvp.text = m_data.fight_stat.mvp_cnt.ToString();
        m_goldace.text = m_data.fight_stat.gold_ace.ToString();
        m_sliverace.text = m_data.fight_stat.silver_ace.ToString();
        m_txtWinRate.text = string.Format("{0}%", fight_cnt > 0 ? (int)(((float)win_cnt / (float)fight_cnt) * 100) : 0);
        m_txtKillCount.text = kill_cnt.ToString();

        m_txtKillContinue.text = max_multi_kill.ToString();
        m_txtCombatInfo.text = win_cnt.ToString();
        m_txtMvpAce.text = mvp_cnt + "/" + gold_ace;

        m_txtHeadShot.text = string.Format("{0}%", kill_cnt > 0 ? (int)(((float)headshoot / (float)kill_cnt) * 100) : 0);

        m_txtRankScore.text = "排位积分：" + m_data.rank_val;
        var config = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(m_data.rank_val);
        if (config != null)
        {
            m_imgRankIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, config.IconKey));
            m_imgRankWord.SetSprite(ResourceManager.LoadSprite(AtlasName.RankList, config.IconKey + "word"));
            //m_imgRankIcon.SetNativeSize();
            //m_imgRankWord.SetNativeSize();
        }
        m_imgRankIcon.enabled = config != null;
        m_imgRankWord.enabled = config != null;
    }

    void UpdateFightStat(int type, Dictionary<string, GameObject> set)
    {
        //if (statData == null)
        //    return;
        var data = m_data.mode_stats[type].game_stats;
        foreach (var log in data)
        {
            
            if (log != null)
            {
                ConfigGameRuleLine line = m_configGameRule.GetLineByGameRule(log.game_type);
                if (line != null)
                {
                    GameObject item;
                    set.TryGetValue(log.game_type, out item);
                    if (item == null)
                    {
                        item = set[line.GameType] = UIHelper.Instantiate(m_goItemProto) as GameObject;
                    }
                    Text modeName = item.transform.Find("slip1/info").GetComponent<Text>();
                    Text killinfo = item.transform.Find("killvalue1/info").GetComponent<Text>();
                    Text wininfo = item.transform.Find("winvalue1/info").GetComponent<Text>();
                    modeName.text = line != null ? line.Mode_Name : "NA";
                    wininfo.text = string.Format("{0}/{1}", log.win_cnt, log.fight_cnt);
                    killinfo.text = string.Format("{0}/{1}", log.kill_cnt, log.death_cnt);
                }
            }
        }
    }

    void ShowList(Dictionary<string, GameObject> set)
    {
        Transform parentTransform = m_goContentList.transform;

        for (int index = 0; index < parentTransform.childCount; )
        {
            Transform child = parentTransform.GetChild(index);
            child.SetParent(null);
            child.gameObject.TrySetActive(false);
            index = 0;
        }

        foreach (GameObject go in set.Values)
        {
            go.TrySetActive(true);
            go.transform.SetParent(m_goContentList.transform, false);
            go.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        }

    }

    void OnClickShowNormal(GameObject target, PointerEventData eventData)
    {
        ShowList(m_dicNormalStat);
        m_txtKillContinue.text = m_data.mode_stats[0].max_multi_kill.ToString();
    }

    void OnClickShowRank(GameObject target, PointerEventData eventData)
    {
        ShowList(m_dicQualifyingStat);
        m_txtKillContinue.text = m_data.mode_stats[1].max_multi_kill.ToString();
    }

    void PreCreateFightStat()
    {
        for (int index = 0; index < m_configGameRule.m_dataArr.Length; ++index)
        {
            ConfigGameRuleLine line = m_configGameRule.m_dataArr[index];
            if (!line.IsShow("PanelPlayerInfo"))
                continue;
            GameObject item;
            if (!m_dicNormalStat.TryGetValue(line.Type, out item))
            {
                item = UIHelper.Instantiate(m_goItemProto) as GameObject;
                m_dicNormalStat[line.GameType] = item;
            }
            Text modeName = item.transform.Find("slip1/info").GetComponent<Text>();
            Text killinfo = item.transform.Find("killvalue1/info").GetComponent<Text>();
            Text wininfo = item.transform.Find("winvalue1/info").GetComponent<Text>();
            modeName.text = line != null ? line.Mode_Name : "NA";
            wininfo.text = string.Format("{0}/{1}", 0, 0);
            killinfo.text = string.Format("{0}/{1}", 0, 0);
        }
    }

    void Toc_player_set_icon(toc_player_set_icon data)
    {
        if (m_imgHead != null)
        {
            m_imgHead.SetRoleIcon(PlayerSystem.roleData.icon);
        }
    }

    void OnClickChenghao(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelViewChenghao>(new object [] {PlayerSystem.roleData.chenghao}, false, this);
    }

    void Toc_player_set_chenghao( toc_player_set_chenghao proto )
    {
        var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(proto.chenghao);
        if (lineChenghao != null)
        {
            m_imgChenghao.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
            m_imgChenghao.SetNativeSize();
            PlayerSystem.roleData.chenghao = proto.chenghao;
        }
    }

    void OnClickBadge(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelViewBadge>(null, false, this);
    }

    private void OnBagPageClick(GameObject target, PointerEventData eventData)
    {
        if (Mathf.Abs(m_bagDataGrid.horizonPos) < 0.1f)
        {
            m_imgNextBagPage.rectTransform.localScale = new Vector3(-1, 1, 1);
            m_bagDataGrid.ResetScrollPosition(6, true);
        }
        else
        {
            m_imgNextBagPage.rectTransform.localScale = Vector3.one;
            m_bagDataGrid.ResetScrollPosition(0, true);
        }
    }
}
