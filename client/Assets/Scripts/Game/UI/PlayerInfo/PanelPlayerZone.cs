﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/11/25 9:44:46
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelPlayerZone : BasePanel
{
    GameObject m_frameLeft;
    GameObject m_frameLeftEdit;
    GameObject m_btnEdit;
    GameObject m_btnEditEnd;
    GameObject m_btnClose;
    GameObject m_addBlogGo;
    toc_player_get_zone_info m_data;
    private toc_microblog_view_player m_microBlogData;
    const string KEY_PLAYERId = "player_id";
    const string KEY_ICON = "icon";
    const string KEY_VIPLEVEL = "vip_level";
    const string KEY_NAME = "name";
    const string KEY_DESCRIPTION = "description";
    const string KEY_REGION = "region";
    const string KEY_GENDER = "gender";
    const string KEY_ONLINETIME = "online_time";
    const string KEY_BIRTHDAY = "birthday";
    const string KEY_QQ = "qq";
    const string KEY_ICON_URL = "icon_url";

    Dictionary<string, string> m_dataDic;

    InputField m_inputMonth;
    InputField m_inputDay;
    Text m_textStar;
    GameObject m_tabBtn;
    GameObject m_btnLookOther;
    private GameObject m_iconBtn;
    private GameObject m_iconBtnReplaceText;
    private Text m_iconBtnText;
    private Image m_imgSex;
    private Text m_fansCount;
    private DataGrid m_historyDataGrid;

    private InputField m_input;
    private Button m_motion_btn;
    private Image m_headIcon;
    private Image m_icon;
    private long m_otherPlayerId;


    public PanelPlayerZone()
    {
        // 构造器
        SetPanelPrefabPath("UI/PlayerInfo/PanelPlayerZone");
        AddPreLoadRes("UI/PlayerInfo/PanelPlayerZone", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/ItemMicroBlog", EResType.UI);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
        AddPreLoadRes("Atlas/MicroBlog", EResType.Atlas);
        
    }
    public override void Init()
    {
        m_frameLeft = m_tran.Find("layout/edit/frame/frameLeft").gameObject;
        m_frameLeftEdit = m_tran.Find("layout/edit/frame/frameLeftEdit").gameObject;
        m_btnEdit = m_frameLeft.transform.Find("btnEdit").gameObject;
        m_btnEdit.TrySetActive(false);
        m_btnEditEnd = m_frameLeftEdit.transform.Find("btnEditEnd").gameObject;
        m_btnClose = m_tran.Find("btnClose").gameObject;
        m_frameLeft.TrySetActive(true);
        m_frameLeftEdit.TrySetActive(false);
        m_iconBtn = m_tran.Find("layout/info/Button").gameObject;
        m_iconBtnText = m_iconBtn.transform.Find("Text").GetComponent<Text>();
        m_iconBtnReplaceText = m_tran.Find("layout/info/desc").gameObject;
        m_icon = m_tran.Find("layout/info/icon/Image").GetComponent<Image>();
        m_fansCount = m_tran.Find("layout/info/fans/Text").GetComponent<Text>();
        m_historyDataGrid = m_tran.Find("layout/history/content/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_historyDataGrid.autoSelectFirst = false;
        m_historyDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/PlayerInfo/MicroBlog/ItemMicroBlog"), typeof(ItemMicroBlog));
        m_imgSex = m_tran.Find("layout/info/sex").GetComponent<Image>();
        m_addBlogGo = m_tran.Find("layout/history/up").gameObject;

        m_inputMonth = m_frameLeftEdit.transform.Find("txtList/txtMonthEdit").GetComponent<InputField>();
        m_inputDay = m_frameLeftEdit.transform.Find("txtList/txtDayEdit").GetComponent<InputField>();
        m_textStar = m_frameLeftEdit.transform.Find("txtList/txtMonthEdit/TextStar").GetComponent<Text>();
        m_btnLookOther = m_tran.Find("layout/edit/tabBtn/TabFile").gameObject;
        m_tabBtn = m_tran.Find("layout/edit/tabBtn").gameObject;


        m_frameLeftEdit.transform.Find("txtInfoEdit").gameObject.AddComponent<InputFieldFix>();
        m_frameLeftEdit.transform.Find("txtList/txtAreaEdit").gameObject.AddComponent<InputFieldFix>();
        m_frameLeftEdit.transform.Find("txtList/txtOnlineEdit").gameObject.AddComponent<InputFieldFix>();
        m_inputMonth.gameObject.AddComponent<InputFieldFix>();
        m_inputDay.gameObject.AddComponent<InputFieldFix>();
        m_frameLeftEdit.transform.Find("txtList/txtQQEdit").gameObject.AddComponent<InputFieldFix>();

        m_motion_btn = m_tran.Find("layout/history/up/btn_emoji").GetComponent<Button>();
        m_headIcon = m_tran.Find("layout/history/up/headframe/head").GetComponent<Image>();
        m_input = m_tran.Find("layout/history/up/InputField").GetComponent<InputField>();
        m_input.gameObject.AddComponent<InputFieldFix>();

        //m_frameLeftEdit.transform.Find("txtInfoEdit").gameObject.AddComponent<InputFieldFix>().onEndEdit;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnEdit).onPointerClick += onClickBtnEdit;
        UGUIClickHandler.Get(m_btnEditEnd).onPointerClick += onClickBtnEndEdit;
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnClose;
        m_inputMonth.GetComponent<InputFieldFix>().onEndEdit+=onMonthEdit;
        m_inputDay.GetComponent<InputFieldFix>().onEndEdit += onDayEdit;
        UGUIClickHandler.Get(m_btnLookOther).onPointerClick += onClickBtnLookOther;
        UGUIClickHandler.Get(m_iconBtn).onPointerClick += onClickBtnChangeIcon;
        AddEventListener(GameEvent.UI_SELF_ICON_URL_RETRIVED, () =>
        {
            if((m_otherPlayerId == -1))
            {
                m_icon.SetRoleBigIconOrSelfIcon(PlayerSystem.roleData.icon, PhotoDataManager.IconUrl);
            }
        });
        AddEventListener(GameEvent.UI_MICROBLOG_PLAYER_ZONE, UpdateBlogView);
        AddEventListener(GameEvent.UI_MICROBLOG_FOLLOW, UpdateFollowBtnState);

        UGUIClickHandler.Get(m_motion_btn.gameObject).onPointerClick += (target, evt) =>
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(60, -30) }, false, this).MotionSelected = OnSelectedMotion;
            }
        };
        UGUIClickHandler.Get(m_tran.Find("layout/history/up/btn_comment")).onPointerClick += OnClickSendBtn;
    }

    private void OnSelectedMotion(string key)
    {
        if (m_input.text.Length + key.Length > m_input.characterLimit)
            return;
        m_input.text += key;
    }


    private void OnClickSendBtn(GameObject target, PointerEventData data)
    {
        string inputText = WordFiterManager.Fiter(m_input.text.Trim());
        if (!String.IsNullOrEmpty(inputText))
        {
            m_input.text = String.Empty;
            NetLayer.Send(new tos_microblog_add_blog() { content = inputText });
        }
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
    }

    private void onMonthEdit(string text)
    {
        int month = int.Parse(text);
        if (month < 1)
        {
            month = 1;
        }
        else if (month > 12)
        {
            month = 12;
        }
        m_inputMonth.text = month + "";
        m_textStar.text = getStarName(int.Parse(m_inputMonth.text), int.Parse(m_inputDay.text));
    }
    private void onDayEdit(string text)
    {
        int day = int.Parse(text);
        if (day < 1)
        {
            day = 1;
        }
        else if (day > 31)
        {
            day = 31;
        }
       m_inputDay.text = day + "";
       m_textStar.text = getStarName(int.Parse(m_inputMonth.text),int.Parse(m_inputDay.text));
        //m_frameLeftEdit.transform.Find("txtList/txtMonthEdit/TextStar").GetComponent<Text>().text = month + "";
    }



    /// <summary>
    /// 获取星座名字
    /// </summary>
    /// <returns></returns>
    private string getStarName(int month, int day)
    {
        string starName = "";
        string monthStr = month + "";
        string dayStr = day + "";
        if (day < 10)
        {
            dayStr = "0" + dayStr;
        }
        int num = int.Parse(monthStr + dayStr);
        if (num <= 119)
        {
            starName = "魔蝎座";
        }
        else if (num <= 218)
        {
            starName = "水瓶座";
        }
        else if (num <= 320)
        {
            starName = "双鱼座";
        }
        else if (num <= 419)
        {
            starName = "白羊座";
        }
        else if (num <= 520)
        {
            starName = "金牛座";
        }
        else if (num <= 621)
        {
            starName = "双子座";

        }
        else if (num <= 722)
        {
            starName = "巨蟹座";
        }
        else if (num <= 822)
        {
            starName = "狮子座";
        }
        else if (num <= 922)
        {
            starName = "处女座";
        }
        else if (num <= 1023)
        {
            starName = "天平座";
        }
        else if (num <= 1121)
        {
            starName = "天蝎座";
        }
        else if (num <= 1221)
        {
            starName = "射手座";
        }
        else
        {
            starName = "魔蝎座";
        }
        return starName;

    }

    private void onClickBtnLookOther(GameObject target, PointerEventData eventData)
    {
        PlayerFriendData.ViewPlayer(m_otherPlayerId);
    }

    private void onClickBtnChangeIcon(GameObject target, PointerEventData eventData)
    {
        object[] param = null;
        if (m_otherPlayerId == -1)
        {
#if UNITY_ANDROID || UNITY_IPHONE
            if (PlayerSystem.roleData.level >= ConfigMisc.GetInt("set_icon_url_lvl_min"))
            {
                param = new object[] {2};
                UIManager.PopPanel<PanelChangeRoleIcon>(param, true, this);
            }
            else
            {
                TipsManager.Instance.showTips("30级开放自定义头像");
            }
#endif
        }
        else
        {
            DealWithFollow(m_otherPlayerId);
        }
    }


    private void DealWithFollow(long playerId)
    {
        if (MicroBlogDataManager.Instance.IsFollow(playerId))
        {
            MicroBlogDataManager.Instance.DelFollow(playerId);
        }
        else
        {
            MicroBlogDataManager.Instance.AddFollow(playerId);
        }
    }
      
    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            HidePanel();
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    private void onClickBtnClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        if (UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
        }
    }

    private void onClickBtnEdit(GameObject target, PointerEventData eventData)
    {
        showPlayerInfoEditView();
    }

    private void onClickBtnEndEdit(GameObject target, PointerEventData eventData)
    {
        // showPlayerInfoView(null);
        NetLayer.Send(new tos_player_edit_zone() { p_zone_info = getEditData() });
    }

    private p_zone_info_item[] getEditData()
    {
        p_zone_info_item[] editData = new p_zone_info_item[] { };
        string monthStr = m_frameLeftEdit.transform.Find("txtList/txtMonthEdit").GetComponent<InputField>().text;
        string dayStr = m_frameLeftEdit.transform.Find("txtList/txtDayEdit").GetComponent<InputField>().text;
        p_zone_info_item tempItem = new p_zone_info_item() { key = KEY_BIRTHDAY, value = monthStr + "月" + dayStr + "日" };
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        tempItem = new p_zone_info_item() { key = KEY_DESCRIPTION, value = m_frameLeftEdit.transform.Find("txtInfoEdit").GetComponent<InputField>().text };
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        //性别
        string sex = "0";
        if (m_frameLeftEdit.transform.Find("txtList/txtSexEdit/btnGirl").GetComponent<Toggle>().isOn)
        {
            sex = "1";
        }
        tempItem = new p_zone_info_item() { key = KEY_GENDER, value = sex };
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        tempItem = new p_zone_info_item() { key = KEY_ONLINETIME, value = m_frameLeftEdit.transform.Find("txtList/txtOnlineEdit").GetComponent<InputField>().text };
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        tempItem = new p_zone_info_item() { key = KEY_QQ, value = m_frameLeftEdit.transform.Find("txtList/txtQQEdit").GetComponent<InputField>().text };
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        //地区
        tempItem = new p_zone_info_item() { key = KEY_REGION, value = m_frameLeftEdit.transform.Find("txtList/txtAreaEdit").GetComponent<InputField>().text };
        tempItem.value = WordFiterManager.Fiter(tempItem.value);
        editData = editData.ArrayConcat<p_zone_info_item>(tempItem);
        return editData;
    }



    void Toc_player_edit_zone(toc_player_edit_zone data)
    {
        if (data.result == 0)
        {
            getPlayerInfo();
        }
        else if (data.result == 1)
        {
            //TipsManager.Instance.showTips("描述含有敏感字");
        }
    }


    void Toc_player_get_zone_info(toc_player_get_zone_info data)
    {
        showPlayerInfoView(data);
    }
    /// <summary>
    /// 显示角色界面信息
    /// </summary>
    /// <param name="data"></param>
    void showPlayerInfoView(toc_player_get_zone_info data)
    {
        m_frameLeft.TrySetActive(true);
        m_frameLeftEdit.TrySetActive(false);
        //查看别人信息不能编辑
        if (m_otherPlayerId != -1)
        {
            m_btnEdit.TrySetActive(false);
        }
        else
        {
            m_btnEdit.TrySetActive(true);
        }
        if (data == null) return;
        p_zone_info_item[] infoAry = data.p_zone_info;
        int lngth = infoAry.Length;
        m_dataDic = new Dictionary<string, string>();
        for (int i = 0; i < lngth; i++)
        {
            m_dataDic.Add(infoAry[i].key, infoAry[i].value);
        }

        m_tran.Find("layout/info/name").GetComponent<Text>().text = getValueByKey(KEY_NAME);
        m_frameLeft.transform.Find("txtInfo").GetComponent<Text>().text = getValueByKey(KEY_DESCRIPTION);
        m_frameLeft.transform.Find("txtGoodNum").GetComponent<Text>().text = "暂未开放";
        m_frameLeft.transform.Find("txtList/txtArea").GetComponent<Text>().text = getValueByKey(KEY_REGION);
        m_frameLeft.transform.Find("txtList/txtSex").GetComponent<Text>().text = getValueByKey(KEY_GENDER) == "0" ? "男" : "女";
        m_frameLeft.transform.Find("txtList/txtOnline").GetComponent<Text>().text = getValueByKey(KEY_ONLINETIME);

        if (getValueByKey(KEY_GENDER) == "0")
        {
            m_imgSex.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
        }
        else
        {
            m_imgSex.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
        }   
        string birthdayStr = getValueByKey(KEY_BIRTHDAY);
        string monthStr = "1";
        string dayStr = "1";
        string[] splitAry;
        if (birthdayStr != "")
        {
            birthdayStr = birthdayStr.Replace("月", ",").Replace("日", "");
            splitAry = birthdayStr.Split(new char[] { ',' });
            monthStr = splitAry[0];
            dayStr = splitAry[1];
            m_frameLeft.transform.Find("txtList/txtBirthday").GetComponent<Text>().text = getValueByKey(KEY_BIRTHDAY) + "     " + getStarName(int.Parse(monthStr), int.Parse(dayStr));
        }
        else
        {
            m_frameLeft.transform.Find("txtList/txtBirthday").GetComponent<Text>().text = getValueByKey(KEY_BIRTHDAY);
        }
        m_frameLeft.transform.Find("txtList/txtQQ").GetComponent<Text>().text = getValueByKey(KEY_QQ);
        var iconUrl = getValueByKey(KEY_ICON_URL);
        var regularIcon = getValueByKey(KEY_ICON);
        m_icon.SetRoleBigIconOrSelfIcon(int.Parse(regularIcon), iconUrl);
//        string icon = getValueByKey(KEY_ICON);
//        if (icon != "")
//            m_frameLeft.transform.Find("Icon").GetComponent<Image>().SetSprite(ResourceManager.LoadRoleIconBig(int.Parse(icon)));

    }

    private string getValueByKey(string key)
    {
        string value = "";
        if (m_dataDic == null)
        {

        }
        else if (m_dataDic.ContainsKey(key))
        {
            value = m_dataDic[key];
        }
        return value;
    }

    void showPlayerInfoEditView()
    {
        m_frameLeft.TrySetActive(false);
        m_frameLeftEdit.TrySetActive(true);

        m_frameLeftEdit.transform.Find("txtName").GetComponent<Text>().text = getValueByKey(KEY_NAME);
        m_frameLeftEdit.transform.Find("txtInfoEdit").GetComponent<InputField>().text = getValueByKey(KEY_DESCRIPTION);
        m_frameLeftEdit.transform.Find("txtGoodNum").GetComponent<Text>().text = "暂未开放";
        m_frameLeftEdit.transform.Find("txtList/txtAreaEdit").GetComponent<InputField>().text = getValueByKey(KEY_REGION);

        if (getValueByKey(KEY_GENDER) == "0")
        {
            m_frameLeftEdit.transform.Find("txtList/txtSexEdit/btnBoy").GetComponent<Toggle>().isOn = true;
        }
        else
        {
            m_frameLeftEdit.transform.Find("txtList/txtSexEdit/btnGirl").GetComponent<Toggle>().isOn = true;
        }
        // m_frameLeftEdit.transform.Find("txtList/txtSex").GetComponent<Text>().text = getValueByKey(KEY_GENDER) == "0" ? "男" : "女";

        m_frameLeftEdit.transform.Find("txtList/txtOnlineEdit").GetComponent<InputField>().text = getValueByKey(KEY_ONLINETIME);
        string birthdayStr = getValueByKey(KEY_BIRTHDAY);
        string monthStr = "1";
        string dayStr = "1";
        string[] splitAry;
        if (birthdayStr != "")
        {
            birthdayStr = birthdayStr.Replace("月", ",").Replace("日", "");
            splitAry = birthdayStr.Split(new char[] { ',' });
            monthStr = splitAry[0];
            dayStr = splitAry[1];
        }
        m_inputMonth.text = monthStr;
        m_inputDay.text = dayStr;
        m_textStar.text = getStarName(int.Parse(m_inputMonth.text), int.Parse(m_inputDay.text));
        m_frameLeftEdit.transform.Find("txtList/txtQQEdit").GetComponent<InputField>().text = getValueByKey(KEY_QQ);
        string icon = getValueByKey(KEY_ICON);
        if (icon != "")
            m_frameLeftEdit.transform.Find("Icon").GetComponent<Image>().SetSprite(ResourceManager.LoadRoleIconBig(int.Parse(icon)));
    }

    private void getPlayerInfo()
    {
        long playerId=PlayerSystem.roleId ;
        if(m_otherPlayerId!=-1){
            playerId = m_otherPlayerId;
        }
        NetLayer.Send(new tos_player_get_zone_info() { owner_id = playerId });
    }


    private void Toc_microblog_view_player(toc_microblog_view_player data)
    {
        m_microBlogData = data;
        UpdateBlogView();
    }

    private void GetMicroBlogInfo()
    {
        if (m_otherPlayerId != -1)
        {
            MicroBlogDataManager.Instance.ReqPlayerInfo(m_otherPlayerId);
        }
        else
        {
            MicroBlogDataManager.Instance.ReqSelfInfo();
        }
    }

    private void UpdateBlogView()
    {
        if (m_otherPlayerId != -1)
        {
            var authorInfo = new p_microblog_author_info();
            authorInfo.player_id = m_microBlogData.player_id;
            authorInfo.icon = m_microBlogData.icon;
            authorInfo.icon_url = m_microBlogData.icon_url;
            authorInfo.level = m_microBlogData.level;
            authorInfo.name = m_microBlogData.name;
            m_addBlogGo.TrySetActive(false);
            var blogDataList = MicroBlogDataManager.ConvertToMicroBlogDataList(authorInfo, m_microBlogData.blog_list);
            UpdateBlogView(blogDataList, m_microBlogData.fans_cnt);
        }
        else
        {
            m_addBlogGo.TrySetActive(true);
            m_headIcon.SetRoleIconOrSelfIcon(PlayerSystem.roleData.icon, PhotoDataManager.IconUrl);
            UpdateBlogView(MicroBlogDataManager.Instance.SelfMicroBlog, MicroBlogDataManager.Instance.FansCount);
        }
    }

    private void UpdateBlogView(List<MicroBlogData> blogList, int fansCount)
    {
        ShowFollowBtnState();
        m_fansCount.text = Util.ToThousandStr(fansCount);
        m_historyDataGrid.Data = blogList.ToArray();
    }


    private void ShowFollowBtnState()
    {
        if (m_otherPlayerId != -1)
        {
            if (MicroBlogDataManager.Instance.IsFollow(m_otherPlayerId))
            {
                m_iconBtnText.text = "取消关注";
            }
            else
            {
                m_iconBtnText.text = "关注";
            }
            m_iconBtn.TrySetActive(true);
        }
        else
        {
            ShowChangeIconBtn();
        }
    }

    private void ShowChangeIconBtn()
    {
        m_iconBtnText.text = "更改";
#if UNITY_ANDROID || UNITY_IPHONE
        m_iconBtn.TrySetActive(true);
        m_iconBtnReplaceText.TrySetActive(false);
#else
        m_iconBtn.TrySetActive(false);
        m_iconBtnReplaceText.TrySetActive(true);
#endif
    }

    private void UpdateFollowBtnState()
    {
        ShowFollowBtnState();
        if (m_otherPlayerId != -1)
        {
            if (MicroBlogDataManager.Instance.IsFollow(m_otherPlayerId))
            {
                m_fansCount.text = Util.ToThousandStr((++m_microBlogData.fans_cnt));
            }
            else
            {
                m_fansCount.text = Util.ToThousandStr(--m_microBlogData.fans_cnt);
            }
        }
    }

    private void Toc_microblog_praise_blog(toc_microblog_praise_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(true, data.praise_cnt);
        }

    }

    private void Toc_microblog_cancel_praise(toc_microblog_cancel_praise data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(false, data.praise_cnt);
        }
    }

    private void Toc_microblog_comment_blog(toc_microblog_comment_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private void Toc_microblog_del_comment(toc_microblog_del_comment data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private ItemMicroBlog GetMicroBlog(long blogId)
    {
        var items = m_historyDataGrid.ItemRenders;
        foreach (var item in items)
        {
            var microBlogData = item.m_renderData as MicroBlogData;
            if (microBlogData.Content.id == blogId)
            {
                return item as ItemMicroBlog;
            }
        }
        return null;
    }

    private void Toc_microblog_add_blog(toc_microblog_add_blog data)
    {
        GetMicroBlogInfo();
    }

    public override void OnShow()
    {
        m_btnEdit.TrySetActive(false);
        if ((m_params != null && m_params.Length != 0))
        {
            
            m_otherPlayerId = (long)m_params[0];
            if (m_otherPlayerId != PlayerSystem.roleId)
            {
                m_addBlogGo.TrySetActive(false);
                m_tabBtn.TrySetActive(false);
                m_tran.Find("layout/edit/tabBtn/TabZone").GetComponent<Toggle>().isOn = true;
            }
            else
            {

                m_otherPlayerId = -1;
                m_tabBtn.TrySetActive(false);
            }
        }
        else
        {
            m_otherPlayerId = -1;
            m_tabBtn.TrySetActive(false);
        }
        ShowFollowBtnState();
        getPlayerInfo();
        GetMicroBlogInfo();
    }


    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {

    }


}
