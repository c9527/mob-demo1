﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PanelRoleLevelup : BasePanel
{
    public Image cur_img;
    public Text cur_txt;
    private Text curLevelText;
    private List<Transform> m_itemList = new List<Transform>();
    private const int ITEM_NUM = 3;
    private Transform m_listTran;

    private UIEffect _effect_player;
    private UIEffect m_effect_suipian;
    private UIEffect m_effect_di;
    private List<UIEffect> m_itemEffectList = new List<UIEffect>();
    private int m_timeId;
    private int m_itemTimeId;

    public PanelRoleLevelup()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelRoleLevelup");

        AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("UI/PlayerInfo/PanelRoleLevelup", EResType.UI);
    }

    public override void Init()
    {
        transform.Find("frame/cur_img").gameObject.AddMissingComponent<SortingOrderRenderer>();
        cur_img = transform.Find("frame/cur_img").GetComponent<Image>();
        cur_txt = transform.Find("frame/cur_txt").GetComponent<Text>();
        curLevelText = transform.Find("frame/curLlevel_txt").GetComponent<Text>();
        for (int i = 0; i < ITEM_NUM; ++i)
        {
            Transform tran = transform.Find("frame/itemList/item" + i);
            m_itemList.Add(tran);
        }

        m_listTran = transform.Find("frame/itemList");
        cur_img.gameObject.TrySetActive(false);
        curLevelText.text = "";
        cur_txt.text = "";
        m_listTran.gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(transform.Find("frame/ok_btn")).onPointerClick += (target,evt) => HidePanel();
    }

    public override void OnShow()
    {
        WelfareDataManger.singleton.UpdateData();
        m_listTran.gameObject.TrySetActive(false);
        var pre_value = HasParams() && m_params.Length > 0 ? (int)m_params[0] : 0;
        var cur_value = HasParams() && m_params.Length > 1 ? (int)m_params[1] : 0;
        var info = ConfigManager.GetConfig<ConfigRoleLevel>().GetLine(cur_value);
        
        AudioManager.PlayUISound(AudioConst.levelUp);
        m_timeId = TimerManager.SetTimeOut(0.3f, () =>
        {

            if (m_effect_di == null)
            {
                m_effect_di = UIEffect.ShowEffectAfter(transform.Find("bg").transform, EffectConst.ROLE_LEVELUP_DIGUANG, new Vector3(0, 160, 0));
            }
            else
            {
                m_effect_di.Play();
            }
            if (m_effect_suipian == null)
            {
                m_effect_suipian = UIEffect.ShowEffectAfter(transform.Find("frame").transform, EffectConst.ROLE_LEVELUP_PICE, new Vector3(0, 70, 0));
            }
            else
            {
                m_effect_suipian.Play();
            }
            cur_img.gameObject.TrySetActive(true);
            cur_img.SetSprite(ResourceManager.LoadArmyIcon(cur_value));
            cur_img.SetNativeSize();
            curLevelText.text = String.Format("{0}级",  cur_value);
            cur_txt.text = info != null ? info.Title : "";
        });
        
        if (_effect_player == null)
        {
            OnSetupEffect();
        }
        else
        {
            _effect_player.Play();
        }

        m_itemTimeId = TimerManager.SetTimeOut(0.4f, () => { showItemList(info); });
        SortingOrderRenderer.RebuildAll();
    }

    public void playEffect()
    {
        if (_effect_player != null)
        {
             _effect_player.Play();
        }
    }

    private void showItemList(ConfigRoleLevelLine info)
    {
        if( info == null ||  info.GetOpenFunList()== null )
        {
            m_listTran.gameObject.TrySetActive(false);
        }
        else
        {
            m_listTran.gameObject.TrySetActive(true);
            string[] list = info == null ? null : info.GetOpenFunList();
            string icon = "";
            string desc = "";
            int len = list == null ? 0 : list.Length;
            Image img;
            int openFunNum = 0;
            for (int i = 0; i < m_itemList.Count; ++i)
            {
                string[] item = null;
                if (i < len)
                {
                    item = list[i] == null ? null : list[i].Split(';');
                }
                icon = item == null ? "" : item[0];
                desc = item == null ? "" : item[1];
                img = m_itemList[i].Find("icon").GetComponent<Image>();
                if (icon == "")
                {
                    m_itemList[i].gameObject.TrySetActive(false);
                }
                else
                {
                    openFunNum = openFunNum + 1;
                    m_itemList[i].gameObject.TrySetActive(true);
                    img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, icon));
                    UIEffect itemEffect = UIEffect.ShowEffect(m_itemList[i], EffectConst.ROLE_LEVELUP_ITEM_EFFECT,new Vector3(-207,38,0));
                    m_itemEffectList.Add(itemEffect);
                    img.SetNativeSize();
                }
                m_itemList[i].Find("desc").GetComponent<Text>().text = desc;
            }
            changePostion(openFunNum);
        }
    }

    private void changePostion(int openFunNum)
    {
        switch( openFunNum )
        {
            case 1:
                m_listTran.localPosition = new Vector3(251,-118,0);
                break;
            case 2:
                m_listTran.localPosition = new Vector3(137, -118, 0);
                break;
            case 3:
                m_listTran.localPosition = new Vector3(23, -118, 0);
                break;
        }
    }

    private void OnSetupEffect()
    {
        if(!IsOpen())
        {
            return;
        }
        if (_effect_player == null)
        {
            _effect_player = UIEffect.ShowEffectAfter(transform.Find("frame").transform, EffectConst.ROLE_LEVELUP_TITLE, new Vector3(0, 10,0));
            
        }
    }

    public override void OnHide()
    {
        if (_effect_player != null)
        {
            _effect_player.Hide();
        }
        if (m_effect_di != null)
        {
            m_effect_di.Hide();
        }
        if (m_effect_suipian != null)
        {
            m_effect_suipian.Hide();
        }
        for( int i = 0; i < m_itemEffectList.Count;i++ )
        {
            m_itemEffectList[i].Hide();
            m_itemEffectList[i].Destroy();
        }
        m_itemEffectList.Clear();
        cur_img.gameObject.TrySetActive(false);
        curLevelText.text = "";
        cur_txt.text = "";
        if (m_timeId != -1)
        {
            TimerManager.RemoveTimeOut(m_timeId);
        }

        if (m_itemTimeId != -1)
        {
            TimerManager.RemoveTimeOut(m_itemTimeId);
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
