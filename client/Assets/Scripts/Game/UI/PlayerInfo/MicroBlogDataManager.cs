﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MicroBlogData
{
    public p_microblog_author_info AuthorInfo;
    public p_microblog_content Content;
}

public class MicroBlogCommentData
{
    public long BlogId;
    public long BlogOwnerId;
    public p_microblog_author_info AuthorInfo;
    public p_microblog_comment_info CommentInfo;
}

class MicroBlogDataManager : Singleton<MicroBlogDataManager>
{
    private List<long> m_followList;
    private List<long> m_fanList;
    private List<long> m_blackList;

    public int FansCount
    {
        get { return Instance.m_fanList.Count; }
    }

    private List<MicroBlogData> m_microBlogPool;
    private List<MicroBlogData> m_selfMicroBlog;

    private List<MicroBlogCommentData> m_microBlogCommentDataPool; 

    public List<MicroBlogData> SelfMicroBlog
    {
        get { return m_selfMicroBlog; }
    }

    public MicroBlogDataManager()
    {
        m_followList = new List<long>();
        m_fanList = new List<long>();
        m_blackList = new List<long>();

        m_microBlogPool = new List<MicroBlogData>();
        m_selfMicroBlog= new List<MicroBlogData>();

        m_microBlogCommentDataPool = new List<MicroBlogCommentData>();

    }

    private MicroBlogCommentData GetPoolCommentData(int index)
    {
        if (index >= m_microBlogCommentDataPool.Count)
        {
            var item = new MicroBlogCommentData();
            m_microBlogCommentDataPool.Add(item);
            return item;
        }
        return m_microBlogCommentDataPool[index];
    }


    private static void Toc_microblog_my_data(toc_microblog_my_data data)
    {
        Instance.m_followList = data.follow_list.ToList();
        Instance.m_fanList = data.fans_list.ToList();
        Instance.m_blackList = data.black_list.ToList();


        var selfAuthorInfo = GetSelfMicroBlogAuthorInfo();

        Instance.m_selfMicroBlog = ConvertToMicroBlogDataList(selfAuthorInfo, data.blog_list);

        GameDispatcher.Dispatch(GameEvent.UI_MICROBLOG_PLAYER_ZONE);
    }


    public static p_microblog_author_info GetSelfMicroBlogAuthorInfo()
    {
        var selfAuthorInfo = new p_microblog_author_info();
        var playerData = PlayerSystem.roleData;
        selfAuthorInfo.icon = playerData.icon;
        selfAuthorInfo.name = playerData.name;
        selfAuthorInfo.icon_url = PhotoDataManager.IconUrl;
        selfAuthorInfo.level = playerData.level;
        return selfAuthorInfo;
    }

    public static List<MicroBlogData> ConvertToMicroBlogDataList(p_microblog_author_info authorInfo, p_microblog_content[] contents)
    {
        var microBlogPool = Instance.m_microBlogPool;
        var blogDataList = new List<MicroBlogData>();
        MicroBlogData tmpBlogData;
        for (int i = 0; i < contents.Length; i++)
        {
            contents[i].msg = WordFiterManager.Fiter(contents[i].msg);
            if (i < microBlogPool.Count)
            {
                tmpBlogData = microBlogPool[i];
            }
            else
            {
                tmpBlogData = new MicroBlogData();
                microBlogPool.Add(tmpBlogData);
            }
            tmpBlogData.AuthorInfo = authorInfo;
            tmpBlogData.Content = contents[i];
            blogDataList.Add(tmpBlogData);
        }
        return blogDataList;
    }


    public static List<MicroBlogData> ConvertToMicroBlogDataList(p_microblog_author_info[] authorInfos, p_microblog_content[] contents)
    {
//        if (authorInfos.Length != contents.Length)
//        {
//            return null;
//        }
        var authorDic = authorInfos.ToDictionary(i => i.player_id);
        var microBlogPool = Instance.m_microBlogPool;
        var blogDataList = new List<MicroBlogData>();
        MicroBlogData tmpBlogData;
        for (int i = 0; i < contents.Length; i++)
        {
            contents[i].msg = WordFiterManager.Fiter(contents[i].msg);
            if (i < microBlogPool.Count)
            {
                tmpBlogData = microBlogPool[i];
            }
            else
            {
                tmpBlogData = new MicroBlogData();
                microBlogPool.Add(tmpBlogData);
            }
            tmpBlogData.AuthorInfo = authorDic[contents[i].player_id];
            tmpBlogData.Content = contents[i];
            blogDataList.Add(tmpBlogData);
        }
        return blogDataList.OrderByDescending(i => i.Content.time).ToList();
    }


    public static List<MicroBlogCommentData> ConvertToMicroBlogCommentDataList(long blog_id, long ownerId, p_microblog_author_info[] authorInfos, p_microblog_comment_info[] contents)
    {
        var authorDic = authorInfos.ToDictionary(i => i.player_id);
        var microBlogPool = Instance.m_microBlogPool;
        var blogCommentDataList = new List<MicroBlogCommentData>();
        MicroBlogCommentData tmpCommentData;
        for (int i = 0; i < contents.Length; i++)
        {
            contents[i].comment = WordFiterManager.Fiter(contents[i].comment);
            tmpCommentData = Instance.GetPoolCommentData(i);
            tmpCommentData.BlogId = blog_id;
            tmpCommentData.BlogOwnerId = ownerId;
            tmpCommentData.AuthorInfo = authorDic[contents[i].pid];
            tmpCommentData.CommentInfo = contents[i];
            blogCommentDataList.Add(tmpCommentData);
        }
        return blogCommentDataList;
    }

    public void ReqSelfInfo()
    {
        NetLayer.Send(new tos_microblog_my_data());
    }

    public void ReqPlayerInfo(long playerId)
    {
        NetLayer.Send(new tos_microblog_view_player(){view_pid = playerId});
    }

    public bool IsFollow(long playerId)
    {
        return m_followList.Contains(playerId);
    }

    public void AddFollow(long playerId)
    {
        NetLayer.Send(new tos_microblog_add_follow { follow_id = playerId });
    }

    private static void Toc_microblog_add_follow(toc_microblog_add_follow data)
    {
        if (!Instance.m_followList.Contains(data.follow_id))
        {
            Instance.m_followList.Add(data.follow_id);
            GameDispatcher.Dispatch(GameEvent.UI_MICROBLOG_FOLLOW);
        }
    }

    private static void Toc_microblog_del_blog(toc_microblog_del_blog data)
    {
        for (int i = 0; i < Instance.m_selfMicroBlog.Count; i++)
        {
            if (Instance.m_selfMicroBlog[i].Content.id == data.blog_id)
            {
                Instance.m_selfMicroBlog.RemoveAt(i);
                GameDispatcher.Dispatch(GameEvent.UI_MICROBLOG_PLAYER_ZONE);
                break;
            }
        }
    }

    public void DelFollow(long playerId)
    {
        NetLayer.Send(new tos_microblog_del_follow { follow_id = playerId });
    }

    private static void Toc_microblog_del_follow(toc_microblog_del_follow data)
    {
        if (Instance.m_followList.Contains(data.follow_id))
        {
            Instance.m_followList.Remove(data.follow_id);
            GameDispatcher.Dispatch(GameEvent.UI_MICROBLOG_FOLLOW);

        }
    }

    public void AddBlack(long playerId)
    {
        NetLayer.Send(new tos_microblog_add_black { black_id = playerId });
    }

    private static void Toc_microblog_add_black(toc_microblog_add_black data)
    {
        if (!Instance.m_blackList.Contains(data.black_id))
        {
            Instance.m_blackList.Add(data.black_id);
        }
    }

    public void DelBlack(long playerId)
    {
        NetLayer.Send(new tos_microblog_add_black { black_id = playerId });
    }

    private static void Toc_microblog_del_black(toc_microblog_del_black data)
    {
        if (Instance.m_blackList.Contains(data.black_id))
        {
            Instance.m_blackList.Remove(data.black_id);
        }
    }
}