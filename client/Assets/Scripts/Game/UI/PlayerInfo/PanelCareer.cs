﻿ using System;
 using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;


public class PanelCareer:BasePanel
{
    Text m_name;
    Image m_head;
    Text m_lvl;
    Image m_army;
    Image m_icon1;
    Image m_headVip;
    //Image m_imgHeroCardTBg;
    Text m_num1;
    Image m_icon2;
    Text m_num2;
    Image m_icon3;
    Text m_num3;
    p_title_info[] general_titles;
    p_title_info[] qualifying_titles;
    List<career_info> info_list;
    DataGrid m_data;
    string curTab;
    toc_player_view_player m_info;
    Image mask;
    bool isChangeMode;
    p_title_info[] chosenTitle;
    int click_num;//表示需要替换哪一个title；
    Toggle m_matchtab;
    Text m_btnTxt;

    RectTransform m_effectPoint;
    UIEffect m_effect;
    GameObject m_changebtn; 

    Toggle m_pvptab;
    int type;
    class ItemCareer : ItemRender
    {
        Image icon;
        GameObject showing;
        Text num;
        Text name;
        GameObject desc;
        GameObject backg;
        GameObject select;
       

        public override void Awake()
        {
            icon = transform.Find("Icon").GetComponent<Image>();
            showing = transform.Find("Image").gameObject;
            num = transform.Find("Text").GetComponent<Text>();
            name = transform.Find("name").GetComponent<Text>();
            desc = transform.Find("desc").gameObject;
            backg = transform.Find("Background").gameObject;
            select = transform.Find("Background/Checkmark").gameObject;
            
        }

        protected override void OnSetData(object data)
        {
            var info = data as career_info;
            var config = ConfigManager.GetConfig<ConfigTitle>().GetLine(info.name);
            icon.SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(info.name, info.type)));
            //icon.SetSprite(ResourceManager.LoadSprite("PlayerInfo", info.name));
            num.text = info.num.ToString();
            showing.TrySetActive(false);
            name.text = info.showname;
            UGUIClickHandler.Get(desc).onPointerClick += delegate { UIManager.ShowTipPanel(info.desc); };
            showing.TrySetActive(info.isShowing);
            if (UIManager.GetPanel<PanelCareer>().isChangeMode)
            {
                desc.TrySetActive(false);
                backg.TrySetActive(true);
                select.TrySetActive(info.isShowing);
            }
            else
            {
                desc.TrySetActive(true);
                backg.TrySetActive(false);
            }
            
        }
    }

    public class career_info
    {
        public int num = 0;
        public string name;
        public string showname;
        public string desc;
        public bool isShowing;
        public int type;

    }

    public PanelCareer()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelCareer");
        AddPreLoadRes("UI/PlayerInfo/PanelCareer", EResType.UI);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        m_head = m_tran.Find("frame/Head/head/icon").GetComponent<Image>();
        m_headVip = m_tran.Find("frame/Head/headVip").GetComponent<Image>();
        m_name = m_tran.Find("frame/Head/playername").GetComponent<Text>();
        m_lvl = m_tran.Find("frame/Head/armylvlname").GetComponent<Text>();
        m_army = m_tran.Find("frame/Head/imgArmy").GetComponent<Image>();
        m_icon1 = m_tran.Find("frame/Head/icon1").GetComponent<Image>();
        m_num1 = m_tran.Find("frame/Head/icon1/Text").GetComponent<Text>();
        m_icon2 = m_tran.Find("frame/Head/icon2").GetComponent<Image>();
        m_num2 = m_tran.Find("frame/Head/icon2/Text").GetComponent<Text>();
        m_icon3 = m_tran.Find("frame/Head/icon3").GetComponent<Image>();
        m_num3 = m_tran.Find("frame/Head/icon3/Text").GetComponent<Text>();
        m_data = m_tran.Find("frame/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_data.autoSelectFirst = false;
        m_data.SetItemRender(m_tran.Find("frame/ItemCareer").gameObject, typeof(ItemCareer));
        mask = m_tran.Find("frame/mask").GetComponent<Image>();
        isChangeMode = false;
        chosenTitle = new p_title_info[3];
        click_num = 0;
        m_matchtab = m_tran.Find("frame/tabBtn/Tabmatch").GetComponent<Toggle>();
        m_btnTxt = m_tran.Find("frame/Head/Change/Text").GetComponent<Text>();
        m_effectPoint = m_tran.Find("frame/Head/headVip/effect").GetComponent<RectTransform>();
        m_changebtn = m_tran.Find("frame/Head/Change").gameObject;
        //m_imgHeroCardTBg = m_tran.Find("frame/topframe").GetComponent<Image>();
        m_pvptab = m_tran.Find("frame/tabBtn/Tabpvp").GetComponent<Toggle>();
        type = 1;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/Head/Close").gameObject).onPointerClick += OnCloseClick;
        UGUIClickHandler.Get(m_tran.Find("frame/Head/Change")).onPointerClick += OnChangeClick;
        UGUIClickHandler.Get(m_tran.Find("frame/tabBtn/Tabmatch")).onPointerClick += OnTabClick;
        UGUIClickHandler.Get(m_tran.Find("frame/tabBtn/Tabpvp")).onPointerClick += OnTabClick;
    }

    void OnChangeClick(GameObject target, PointerEventData data)
    {
        mask.gameObject.TrySetActive(!mask.gameObject.activeSelf);
        isChangeMode = !isChangeMode;

        if (!isChangeMode)
        {
            click_num = 0;
            SendNewTitleShow();
            m_btnTxt.text = "更换";
            m_data.UpdateView();
        }
        else
        {
            //OnTabClick(m_matchtab.gameObject, null);
            //m_matchtab.isOn = true;
            m_btnTxt.text = "确定";
            m_data.UpdateView();
        }
        if (isChangeMode)
        {
            m_data.onItemSelected += ChooseTitle;
        }
        else
        {
            m_data.onItemSelected -= ChooseTitle;
        }

    }

    void ChooseTitle(object renderdata)
    {
        var temp = GetTitleInfoByType((renderdata as career_info).name, type);
        if (temp == null)
        {
            return;
        }
        for (int i = 0; i < chosenTitle.Length; i++)
        {
            if (temp.title == chosenTitle[i].title)
            {
                if (temp.type == chosenTitle[i].type)
                {
                    return;
                }
            }
        }
            //if (temp.title == chosenTitle[0].title || temp.title == chosenTitle[1].title || temp.title == chosenTitle[2].title)
            //{
            //    return;
            //}
        chosenTitle[click_num % 3] = new p_title_info() { title = temp.title, type = type };
        click_num++;
        UpdateShowTitle();
        return;
    }

    void SendNewTitleShow()
    {
        NetLayer.Send(new tos_player_set_titles() { titles = new p_title_info[3] { chosenTitle[0], chosenTitle[1], chosenTitle[2] } });
    }

    void Toc_player_set_titles(toc_player_set_titles data)
    {
        m_info.set_titles = data.titles;
    }

    void UpdateShowTitle()
    {
        SetTitleSprite(m_icon1, chosenTitle[0]) ;
        SetTitleSprite(m_icon2, chosenTitle[1]);
        SetTitleSprite(m_icon3, chosenTitle[2]);
        m_num1.text = GetTitleInfoByType(chosenTitle[0].title, chosenTitle[0].type).cnt.ToString();
        m_num2.text = GetTitleInfoByType(chosenTitle[1].title, chosenTitle[1].type).cnt.ToString();
        m_num3.text = GetTitleInfoByType(chosenTitle[2].title, chosenTitle[2].type).cnt.ToString();
        for (int i = 0; i < m_data.Data.Length; i++)
        {
            var data = m_data.Data[i] as career_info;
            data.isShowing = false;
            for (int j = 0; j < 3; j++)
            {
                if (data.name == chosenTitle[j].title)
                {
                    if (data.type == chosenTitle[j].type)
                    {
                        data.isShowing = true;
                    }
                }
            }
        }
        m_data.UpdateView();
    }

    void OnCloseClick(GameObject target, PointerEventData data)
    {
        if (UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
        }
        if (UIManager.IsOpen<PanelPlayerZone>())
        {
            UIManager.GetPanel<PanelPlayerZone>().HidePanel();
        }
        HidePanel();

    }

    void OnTabClick(GameObject target,PointerEventData data)
    {
        if (curTab == target.name)
        {
            return;
        }
        else
        {
            curTab = target.name;
            if (curTab == "Tabpvp")
            {
                type = 1;
                CheckTitle(general_titles,1);
                m_data.Data = info_list.ToArray();
                UpdateShowTitle();
                return;
            }
            if (curTab == "Tabmatch")
            {
                type = 2;
                CheckTitle(qualifying_titles,2);
                m_data.Data = info_list.ToArray();
                UpdateShowTitle();
                return;
            }
        }
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        if (!HasParams())
        {
            return;
        }
        var heroTpye = HeroCardDataManager.getHeroMaxCardType();
        if (heroTpye == EnumHeroCardType.normal)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_NORMAL);
        }
        else if (heroTpye == EnumHeroCardType.super)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_SUPER);
        }
        else if (heroTpye == EnumHeroCardType.legend)
        {
            ChangeHeroCardBg(GameConst.INFO_LINE_LEGEND);
        }

        
        m_info = m_params[0] as toc_player_view_player;
     
        m_changebtn.TrySetActive(m_info.id == PlayerSystem.roleId);
        

        m_name.text = m_info.name;
        m_head.SetSprite(ResourceManager.LoadRoleIcon(m_info.icon));
        m_lvl.text = String.Format("{0}级",  m_info.level);
        m_army.SetSprite(ResourceManager.LoadArmyIcon(m_info.level));
        qualifying_titles = m_info.qualifying_titles;
        general_titles = m_info.general_titles;
        m_icon1.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "mvp"));
        m_icon2.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "gold_ace"));
        m_icon3.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "silver_ace"));
        m_pvptab.isOn = true;
        curTab = "Tabpvp";
        if (m_info.set_titles != null)
        {
            SetTitleSprite(m_icon1, m_info.set_titles[0]);
            SetTitleSprite(m_icon2, m_info.set_titles[1]);
            SetTitleSprite(m_icon3, m_info.set_titles[2]);
            m_num1.text = GetTitleInfoByType(m_info.set_titles[0].title, m_info.set_titles[0].type).cnt.ToString();
            m_num2.text = GetTitleInfoByType(m_info.set_titles[1].title, m_info.set_titles[1].type).cnt.ToString();
            m_num3.text = GetTitleInfoByType(m_info.set_titles[2].title, m_info.set_titles[2].type).cnt.ToString();
            chosenTitle[0] = m_info.set_titles[0];
            chosenTitle[1] = m_info.set_titles[1];
            chosenTitle[2] = m_info.set_titles[2];
        }

        CheckTitle(general_titles, 1);
        m_data.Data = info_list.ToArray();
        var heroType = HeroCardDataManager.getHeroMaxCardType();


        m_headVip.gameObject.TrySetActive(false);        

        m_headVip.gameObject.TrySetActive(false);
        if (heroTpye != (int)EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroTpye == EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroTpye == EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroTpye == EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (m_effect == null)
                {
                    m_effect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD, new Vector3(0, -5, 0));
                }
            }
        }

        if (isChangeMode)
        {
            click_num = 0;
            m_btnTxt.text = "更换";
            isChangeMode = false;
            m_data.onItemSelected -= ChooseTitle;
        }
        m_data.UpdateView();

        
    }

    void ChangeHeroCardBg(string topBgName)
    {
        //m_imgHeroCardTBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, topBgName));
    }

    public override void Update()
    {

    }

    void CheckTitle(p_title_info[] list,int type)
    {
        var cc = ConfigManager.GetConfig<ConfigTitle>().m_dataArr;
        info_list = new List<career_info>();
        for (int i = 0; i < cc.Length; i++)
        {
            for (int j = 0; j < cc[i].ShowType.Length; j++)
            {
                if (cc[i].ShowType[j] == type)
                {
                    career_info car = new career_info();
                    car.name = cc[i].Type;
                    car.showname = cc[i].Name;
                    car.num = 0;
                    car.desc = cc[i].tips;
                    car.isShowing = false;
                    info_list.Add(car);
                    car.type = type;
                }
            }
        }
        m_num1.text = "0";
        m_num2.text = "0";
        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < info_list.Count; j++)
            {
                if (list[i].title == info_list[j].name)
                {
                    info_list[j].num = list[i].cnt;
                }
            }
        }

        if (m_info.set_titles != null)
        {
            SetTitleSprite(m_icon1, chosenTitle[0]);
            SetTitleSprite(m_icon2, chosenTitle[1]);
            SetTitleSprite(m_icon3, chosenTitle[2]);
            m_num1.text = GetTitleInfoByType(chosenTitle[0].title, chosenTitle[0].type).cnt.ToString();
            m_num2.text = GetTitleInfoByType(chosenTitle[1].title, chosenTitle[1].type).cnt.ToString();
            m_num3.text = GetTitleInfoByType(chosenTitle[2].title, chosenTitle[2].type).cnt.ToString();
        }
        //UpdateShowTitle();
        
        //if (type == 2)
        //{
        //    //if (m_info.qualifying_set_titles == null || m_info.qualifying_set_titles.Length == 0)
        //    //{
        //        m_icon1.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "mvp"));
        //        m_icon2.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "gold_ace"));
        //        m_icon3.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "silver_ace"));
        //        m_num1.text = GetTitleInfoByType("mvp", 2).cnt.ToString();
        //        m_num2.text = GetTitleInfoByType("gold_ace", 2).cnt.ToString();
        //        m_num3.text = GetTitleInfoByType("silver_ace", 2).cnt.ToString();
        //        chosenTitle[0] = GetTitleInfoByType("mvp", 2);
        //        chosenTitle[1] = GetTitleInfoByType("gold_ace", 2);
        //        chosenTitle[2] = GetTitleInfoByType("silver_ace", 2);
        //    //}
        //}
        //else
        //{
        //    //if (m_info.qualifying_set_titles == null || m_info.qualifying_set_titles.Length == 0)
        //    //{
        //        m_icon1.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "mvp"));
        //        m_icon2.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "gold_ace"));
        //        m_icon3.SetSprite(ResourceManager.LoadSprite("PlayerInfo", "silver_ace"));
        //        if (GetTitleInfoByType("mvp", 1) != null)
        //        {
        //            m_num1.text = GetTitleInfoByType("mvp", 1).cnt.ToString();
        //            chosenTitle[0] = GetTitleInfoByType("mvp", 1);
        //        }
        //        else
        //        {
        //            m_num1.text = 0.ToString();
        //        }
        //        if (GetTitleInfoByType("gold_ace", 1) != null)
        //        {
        //            m_num2.text = GetTitleInfoByType("gold_ace", 1).cnt.ToString();
        //            chosenTitle[1] = GetTitleInfoByType("gold_ace", 1);
        //        }
        //        else
        //        {
        //            m_num2.text = 0.ToString();
        //        }
        //        if (GetTitleInfoByType("silver_ace", 1) != null)
        //        {
        //            m_num3.text = GetTitleInfoByType("silver_ace", 1).cnt.ToString();
        //            chosenTitle[2] = GetTitleInfoByType("silver_ace", 1);
        //        }
        //        else
        //        {
        //            m_num3.text = 0.ToString();
        //        }
                
        //    //}
        //}
        //    //else
        //    //{
        //    //    m_icon1.SetSprite(ResourceManager.LoadSprite("PlayerInfo", m_info.qualifying_set_titles[0]));
        //    //    m_num1.text = GetTitleInfoByType(m_info.qualifying_set_titles[0], 2).cnt.ToString();
        //    //    chosenTitle[0] = GetTitleInfoByType(m_info.qualifying_set_titles[0], 2);
        //    //    if (m_info.qualifying_set_titles.Length >= 2)
        //    //    {
        //    //        m_icon2.SetSprite(ResourceManager.LoadSprite("PlayerInfo", m_info.qualifying_set_titles[1]));
        //    //        m_num2.text = GetTitleInfoByType(m_info.qualifying_set_titles[1], 2).cnt.ToString();
        //    //        chosenTitle[1] = GetTitleInfoByType(m_info.qualifying_set_titles[1], 2);
        //    //    }
        //    //}






            for (int i = 0; i < info_list.Count; i++)
            {
                for (int j = 0; j < m_info.set_titles.Length; j++)
                {
                    if (info_list[i].name == m_info.set_titles[j].title)
                    {
                        if (info_list[i].type == m_info.set_titles[j].type)
                        info_list[i].isShowing = true;
                    }
                }
            }
        



    }


    void SetTitleSprite(Image image, p_title_info title)
    {
        var config = ConfigManager.GetConfig<ConfigTitle>().GetLine(title.title);
        image.SetSprite(ResourceManager.LoadSprite("PlayerInfo", config.icon[title.type-1]));
    }
    p_title_info GetTitleInfoByType(string name, int type)
    {
        if (type == 1)
        {
            return GetTitleInfoFromGeneral(name);
        }
        else
        {
            return GetTitleInfoFromQualifying(name);
        }
    }

    p_title_info GetTitleInfoFromGeneral(string name)
    {
        for (int i = 0; i < m_info.general_titles.Length; i++)
        {
            if (name == m_info.general_titles[i].title)
            {
                return m_info.general_titles[i];
            }
        }
        var cc = ConfigManager.GetConfig<ConfigTitle>();
        var info = cc.GetLine(name);
        for (int i = 0; i < info.ShowType.Length; i++)
        {
            if (info.ShowType[i] == 1)
            {
                return new p_title_info() { title = name, cnt = 0,type=1 };
            }
        }
        return null;
    }

    p_title_info GetTitleInfoFromQualifying(string name)
    {
        for (int i = 0; i < m_info.qualifying_titles.Length; i++)
        {
            if (name == m_info.qualifying_titles[i].title)
            {
                return m_info.qualifying_titles[i];
            }
        }
        var cc = ConfigManager.GetConfig<ConfigTitle>();
        var info = cc.GetLine(name);
        for (int i = 0; i < info.ShowType.Length; i++)
        {
            if (info.ShowType[i] == 2)
            {
                return new p_title_info() { title = name, cnt = 0 ,type=2};
            }
        }
        return null;
    }
}

