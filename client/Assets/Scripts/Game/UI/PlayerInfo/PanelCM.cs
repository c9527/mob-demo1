﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

class PanelCM : BasePanel
{
    public InputField name_txt;
    public InputField card_txt;

    public PanelCM()
    {
        SetPanelPrefabPath("UI/PlayerInfo/PanelCM");

        AddPreLoadRes("UI/PlayerInfo/PanelCM", EResType.UI);
    }

    public override void Init()
    {
        name_txt = transform.Find("frame/name_txt").GetComponent<InputField>();
        name_txt.gameObject.AddComponent<InputFieldFix>();
        card_txt = transform.Find("frame/card_txt").GetComponent<InputField>();
        card_txt.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(transform.Find("frame/cancel_btn")).onPointerClick += (target,evt) => HidePanel();
        UGUIClickHandler.Get(transform.Find("frame/submit_btn")).onPointerClick += OnSubmitBtnClick;
    }

    private void OnSubmitBtnClick(GameObject target, PointerEventData eventData)
    {
        var name = name_txt.text;
        var card = card_txt.text;
        if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(card))
        {
            Driver.singleton.StartCoroutine(OnSubmitInfo(name,card));
        }
        else
        {
            TipsManager.Instance.showTips("请填写完整姓名和身份证");
        }
    }

    private IEnumerator OnSubmitInfo(string name,string card)
    {
        UIManager.ShowMask("正在验证中......");
        var data = new string[] {"account="+SdkManager.m_userName,"card="+card,"truename="+name,"sign="+"key" };
        var rmi = new WWW(SdkManager.gameCMUrl+"?"+string.Join("&",data));
        yield return rmi;
        while (!rmi.isDone && string.IsNullOrEmpty(rmi.error))
        {
            yield return null;
        }
        UIManager.HideMask();
        Logger.Log("================================" + rmi.error);
        if (string.IsNullOrEmpty(rmi.error))
        {
            Logger.Log("==========平台防沉迷注册信息返回结果：" + rmi.text);
            var result = 0;
            int.TryParse(rmi.text, out result);
            if (result == 1)
            {
                SdkManager.cm = 1;
                UIManager.ShowTipPanel("您已通过防沉迷系统", () => HidePanel(),null, false);
            }
            else if (result == 2)
            {
                SdkManager.cm = 0;
                UIManager.ShowTipPanel("您的账号是未成年账号，请合理安排游戏时间",()=>HidePanel(),null,false);
            }
            else
            {
                UIManager.ShowTipPanel("您填写的信息无效，请确认后再填写");
            }
        }
        else
        {
            UIManager.ShowTipPanel(rmi.error, () => HidePanel(), null, false);
        }
    }

    public override void OnShow()
    {
        name_txt.text = "";
        card_txt.text = "";
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
