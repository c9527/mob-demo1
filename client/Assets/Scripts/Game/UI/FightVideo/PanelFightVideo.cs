﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：战斗回放
//创建者：hwl
//创建日期: 2016/8/20
///////////////////////////////////////////
public class PanelFightVideo : BasePanel
{
    private Toggle m_ToggleRank;
    private Toggle m_ToggleRankJoke;
    private Toggle m_ToggleCorpsMatch;
    private RectTransform m_rect;
    private DataGrid m_dataGrid;
    private GameObject m_honorTitleGo;
    private Text m_honorName;
    private GameObject m_honorPrev;
    private GameObject m_honorNext;
    private int m_curHonorId;

    private GameObject m_pagePrev;
    private GameObject m_pageNext;
    private Text m_page;
    private int m_curPage = 1;
    private int m_totalPage = 1;
    private string m_fightType;
    private int SCORE_TITLE_LEN = 6;

    private RectTransform m_VBScrollBar;
    private ItemDropInfo[] m_platformArr;
    private GameObject m_btnplatformType;
    private Text m_platformTypeLabel;
    private string m_platformStr = "all";

    public PanelFightVideo()
    {
        SetPanelPrefabPath("UI/FightVideo/PanelFightVideo");
        AddPreLoadRes("UI/FightVideo/PanelFightVideo", EResType.UI);
        AddPreLoadRes("Atlas/FightVideo", EResType.Atlas);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }
    public override void Init()
    {
        m_ToggleRank = m_tran.Find("ToggleGroup/ToggleRank").GetComponent<Toggle>();
        m_ToggleRankJoke = m_tran.Find("ToggleGroup/ToggleRankJoke").GetComponent<Toggle>();
        m_ToggleCorpsMatch = m_tran.Find("ToggleGroup/ToggleCorpsMatch").GetComponent<Toggle>();
        m_dataGrid = m_tran.Find("frame/RankItemList/ScrollView/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_dataGrid.transform.Find("VideoItem").gameObject, typeof(FightVideoItem));
        m_rect = m_dataGrid.transform.parent.GetComponent<RectTransform>();
        m_honorTitleGo = m_tran.Find("titlebg").gameObject;
        m_honorName = m_honorTitleGo.transform.Find("honorName").GetComponent<Text>();
        m_honorPrev = m_honorTitleGo.transform.Find("prev").gameObject;
        m_honorNext = m_honorTitleGo.transform.Find("next").gameObject;

        m_pagePrev = m_tran.Find("page/prev").gameObject;
        m_pageNext = m_tran.Find("page/next").gameObject;
        m_page = m_tran.Find("page/page").GetComponent<Text>();
        m_VBScrollBar = m_tran.Find("frame/RankItemList/VBScrollBar").GetComponent<RectTransform>();
        m_btnplatformType = m_tran.Find("btnType").gameObject;
        m_platformTypeLabel = m_btnplatformType.transform.Find( "Text" ).GetComponent<Text>();
        m_platformArr = new[]
        {
            new ItemDropInfo { type = "PLATFORM", subtype = "ALL", label = "全部"},
            new ItemDropInfo { type = "PLATFORM", subtype = "PHONE", label = "手机端"},
            new ItemDropInfo { type = "PLATFORM", subtype = "PC", label = "电脑端"},
        };
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_ToggleRank.gameObject).onPointerClick += onToggleClick;
        UGUIClickHandler.Get(m_ToggleRankJoke.gameObject).onPointerClick += onToggleClick;
        UGUIClickHandler.Get(m_ToggleCorpsMatch.gameObject).onPointerClick += onToggleClick;
        
        UGUIClickHandler.Get(m_honorPrev).onPointerClick += honorChange;
        UGUIClickHandler.Get(m_honorNext).onPointerClick += honorChange;

        UGUIClickHandler.Get(m_pagePrev).onPointerClick += onPageChange;
        UGUIClickHandler.Get(m_pageNext).onPointerClick += onPageChange;
        UGUIClickHandler.Get(m_btnplatformType).onPointerClick += OnBtnPlatformTypeClick;
    }

    private void OnBtnPlatformTypeClick(GameObject target, PointerEventData eventdata)
    {
        UIManager.ShowDropList(m_platformArr, OnDropListItemSelected, m_tran, target, 240);
    }

    private void OnDropListItemSelected(ItemDropInfo data)
    {
        
        if (data.subtype == "ALL")
        {
            m_platformTypeLabel.text = m_platformArr[0].label;
            m_platformStr = "all";
        }
        else if (data.subtype == "PHONE")
        {
            m_platformTypeLabel.text = m_platformArr[1].label;
            m_platformStr = "android";
        }
            
        else if (data.subtype == "PC")
        {
            m_platformTypeLabel.text = m_platformArr[2].label;
            m_platformStr = "pc";
        }
        NetLayer.Send(new tos_record_history_list() { game_type = m_fightType, level = m_curHonorId, page = m_curPage, by_center = ByCenter(), platform = m_platformStr });
    }

    private void onPageChange(GameObject target, PointerEventData eventData)
    {
        if (target.name == m_honorPrev.name)
        {
            if (m_curPage > 1)
                m_curPage--;
        }
        else
        {
            if (m_curPage < m_totalPage)
                m_curPage++;
        }
        NetLayer.Send(new tos_record_history_list() { game_type = m_fightType, level = m_curHonorId, page = m_curPage, by_center = ByCenter(), platform = m_platformStr });
    }

    private void honorChange(GameObject target, PointerEventData eventData)
    {
        if (target.name == m_honorPrev.name)
        {
            if (m_curHonorId > 1)
                m_curHonorId--;
        }
        else
        {
            if (m_curHonorId < SCORE_TITLE_LEN)
                m_curHonorId++;
        }

        NetLayer.Send(new tos_record_history_list() { game_type = m_fightType, level = m_curHonorId, page = m_curPage, by_center = ByCenter(), platform = m_platformStr });
    }

    private bool ByCenter()
    {
        if (m_fightType == ChannelTypeEnum.match.ToString() || m_fightType == ChannelTypeEnum.match.ToString() + "_joke")
        {
            return true;
        }
        return false;
    }

    private void onToggleClick(GameObject target, PointerEventData eventData)
    {
        m_curPage = 1;
        if(target.name == m_ToggleRank.name)
        {
            resetRandGrid();
            m_fightType = ChannelTypeEnum.match.ToString();
            m_curHonorId = FightVideoManager.GetHonorConfig().range;
        }
        else if (target.name == m_ToggleRankJoke.name)
        {
            resetRandGrid();
            m_fightType = ChannelTypeEnum.match.ToString() + "_joke";
            m_curHonorId = FightVideoManager.GetHonorConfig().range;
        }
        else
        {
            initGrid();
            m_fightType = ChannelTypeEnum.corpsmatch.ToString();
        }
        NetLayer.Send(new tos_record_history_list() { game_type = m_fightType, level = m_curHonorId, page = m_curPage, by_center = ByCenter(), platform = m_platformStr });
    }

    private void initGrid()
    {
        m_honorTitleGo.TrySetActive(false);
        m_rect.anchoredPosition = new Vector3(136, -11, 0);
        m_rect.sizeDelta = new Vector2(932f, 362f);
        m_VBScrollBar.anchoredPosition = new Vector3(598, -12, 0);
        m_VBScrollBar.sizeDelta = new Vector2(6, 362);
        
        m_curHonorId = 0;
    }

    private void resetRandGrid() 
    {
        m_honorTitleGo.TrySetActive(true);
        m_rect.anchoredPosition = new Vector3(136, -33, 0);
        m_rect.sizeDelta = new Vector2(932f, 317f);
        m_VBScrollBar.anchoredPosition = new Vector3(598, -35, 0);
        m_VBScrollBar.sizeDelta = new Vector2(6, 317);
    }

    public override void OnShow()
    {
        m_platformStr = "all";
        m_platformTypeLabel.text = "全部";
        m_ToggleRank.isOn = true;
        m_page.text = "1/1";
        m_honorName.text = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleName(0);
        m_curHonorId = FightVideoManager.GetHonorConfig().range;
        m_fightType = ChannelTypeEnum.match.ToString();
        resetRandGrid();
        NetLayer.Send(new tos_record_history_list() { game_type = m_fightType, level = m_curHonorId, page = m_curPage, by_center = ByCenter(), platform = m_platformStr });
        
    }

    public void UpdateViewInfo()
    {
        var record = FightVideoManager.RecordInfo;
        m_fightType = record.game_type;
        m_curPage = record.page;
        m_totalPage = record.total_page > 0 ? record.total_page:1;
        m_curHonorId = record.level ;
        m_page.text = m_curPage + "/" + m_totalPage;
        if (m_curHonorId > 0)
        {
            m_honorName.text = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleName(m_curHonorId);
        }
        Logger.Log("_____________________巅峰对决 list len = " + record.list.Length);
        m_dataGrid.Data = record.list;
        
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
        FightVideoManager.isPlayFight = false;
    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {
    }
}

