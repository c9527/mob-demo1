﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TeamMemberItem : ItemRender {

    Text m_playerNameText;
    Text m_corpsNameText;
    Text m_levelText;
    Image m_headIcon;
    Image m_levelIcon;
    Image m_bg;
    public override void Awake()
    {
        m_playerNameText = transform.Find("name").GetComponent<Text>();
        m_corpsNameText = transform.Find("corpName").GetComponent<Text>();
        m_levelText = transform.Find("level/Text").GetComponent<Text>();

        m_headIcon = transform.Find("Head/icon").GetComponent<Image>();
        m_levelIcon = transform.Find("level/imgArmy").GetComponent<Image>();
        m_bg = transform.GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        if( data == null )
        {
            m_headIcon.enabled = false;
            m_corpsNameText.text = "";
            m_playerNameText.text = "";
            m_levelIcon.enabled = false;
            m_levelText.text = "";
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.FightVideo, "honor_0"));
            return;
        }
        m_headIcon.enabled = true;
        m_levelIcon.enabled = true;
        p_team_role roleData = data as p_team_role;
        m_headIcon.SetSprite(ResourceManager.LoadRoleIcon(roleData.icon));
        m_headIcon.SetNativeSize();
        m_corpsNameText.text = roleData.corps_name;
        m_playerNameText.text = roleData.name;
        m_levelIcon.SetSprite(ResourceManager.LoadArmyIcon(roleData.level));
        m_levelText.text = String.Format("{0}级",  roleData.level);
        int range  =FightVideoManager.GetHonorConfig() == null ? 0 : FightVideoManager.GetHonorConfig().range;
        m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.FightVideo, "honor_" + range));
    }
}
