﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：弹幕输入界面
//创建者：hwl
//创建日期: 2016/9/08
///////////////////////////////////////////
public class FightVideoSendMsg : BasePanel {
    private InputField m_msgInput;

    public FightVideoSendMsg()
    {
        SetPanelPrefabPath("UI/FightVideo/PanelFightVideoMsg");
        AddPreLoadRes("UI/FightVideo/PanelFightVideoMsg", EResType.UI);
    }
    public override void Init()
    {
        m_msgInput = m_tran.Find("InputField").GetComponent<InputField>();
        m_msgInput.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        
        UGUIClickHandler.Get(m_tran.Find("Send")).onPointerClick += SendMsg;
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };
    }

    private void SendMsg(GameObject target, PointerEventData eventData)
    {
        if (m_msgInput.text == "")
        {
            TipsManager.Instance.showTips("内容不能为空");
        }
        else
        {
            NetLayer.Send(new tos_record_send_message() { msg = WordFiterManager.Fiter(m_msgInput.text),by_center = FightVideoManager.ByCenter() });
            m_msgInput.text = "";
            this.HidePanel();
        }
    }

    public override void OnShow()
    {
        FightVideoManager.PlaySpeed = 0;
        NetLayer.Send(new tos_record_set_speed { speed = FightVideoManager.PlaySpeed, by_center = FightVideoManager.ByCenter() });
    }

    public override void OnHide()
    {
        FightVideoManager.PlaySpeed = 1;
        NetLayer.Send(new tos_record_set_speed { speed = FightVideoManager.PlaySpeed, by_center = FightVideoManager.ByCenter() });
        
    }

    public override void OnBack()
    {
         
    }

    public override void OnDestroy()
    {
         
    }

    public override void Update()
    {
         
    }
}
