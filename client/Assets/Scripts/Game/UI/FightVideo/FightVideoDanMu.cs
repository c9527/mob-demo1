﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：弹幕输入界面
//创建者：hwl
//创建日期: 2016/9/08
///////////////////////////////////////////
public class FightVideoDanMu : BasePanel 
{
    private List<p_record_message> m_msgList = new List<p_record_message>();
    private List<GameObject> m_textList = new List<GameObject>();
    private GameObject m_itemGo;
    public FightVideoDanMu()
    {
        SetPanelPrefabPath("UI/FightVideo/FightVideoDanMu");
        AddPreLoadRes("UI/FightVideo/FightVideoDanMu", EResType.UI);
    }
    public override void Init()
    {
        m_itemGo = m_tran.Find("mask").gameObject;
        EnableMouseEvent = false;
        GameObject go;
        for( int i = 0;i < 6;i++ )
        {
            go = GameObject.Instantiate(m_itemGo) as GameObject;
            go.transform.parent = transform;
            go.transform.localScale = Vector3.one;
            m_textList.Add(go);
        }
    }

    public override void InitEvent()
    {

    }

    public override void OnShow()
    {
         
    }

    public override void OnHide()
    {
         
    }

    public override void OnBack()
    {
         
    }

    public override void OnDestroy()
    {
         
    }

    public override void Update()
    {
        Text label;
        float speed = 0;
        for (int i = 0; i < m_textList.Count; i++)
        {
            var rt = m_textList[i].transform.Find("img").gameObject.GetRectTransform();
            label = rt.Find("Text").gameObject.GetComponent<Text>();
            speed = label.text.Length * 0.03f;
            if (label.text != "")
                rt.anchoredPosition += new Vector2(-Time.deltaTime * 100 * FightVideoManager.PlaySpeed * speed, rt.anchoredPosition.y);
            if (rt.anchoredPosition.x <= -560 - label.preferredWidth)
            {
                rt.anchoredPosition = new Vector2(560, 0);
                rt.transform.Find("Text").GetComponent<Text>().text = "";
                rt.sizeDelta = new Vector2(10,26);
            }
        }
    }

    void Toc_record_message(toc_record_message data)
    {
        m_msgList.Clear();
        m_msgList.AddRange(data.messages.ToList());
        GameObject rt = null;
        p_record_message message = null;
        for (int i = 0; i < m_msgList.Count; i++)
        {
            if(i >= m_textList.Count)
            {
                rt = GameObject.Instantiate(m_itemGo) as GameObject;
                rt.GetRectTransform();
                rt.transform.parent = transform;
                m_textList.Add(rt);
            }
            else
            {
                bool isCanUse = false;
                foreach (GameObject go in m_textList)
                {
                    rt = go;
                    if (rt.transform.Find("img/Text").GetComponent<Text>().text == "")
                    {
                        isCanUse = true;
                        break;
                    }
                }
                if( isCanUse == false )
                {
                    rt = GameObject.Instantiate(m_itemGo) as GameObject;
                    rt.transform.parent = transform;
                    m_textList.Add(rt);
                }
            }
            float y = new System.Random().Next(-260,-100);
            y = y + 30;
            rt.GetRectTransform().anchoredPosition = new Vector2(0, y);
            rt.GetRectTransform().localScale = Vector3.one;
            var rt1 = rt.transform.Find("img/Text").gameObject.GetRectTransform();
            message = data.messages[i];
            rt1.GetComponent<Text>().text = getMsgWithColor(message.msg, message.hero_type);
            var img = rt.transform.Find("img").GetComponent<Image>();
            if (message.player_id == WorldManager.singleton.fightId)
            {
                img.gameObject.GetRectTransform().sizeDelta = new Vector2(rt1.GetComponent<Text>().preferredWidth + 2, 26);
                img.enabled = true;
            }
            else
            {
                img.enabled = false;
            }
            rt1.localScale = Vector3.one;
        }
    }

    private string getMsgWithColor(string msg,int type)
    {
        string str = msg;
        uint index = 1;
        uint m_size = 22;
        EnumHeroCardType hero_type = (EnumHeroCardType)(type);
        switch (hero_type)
        {
            case EnumHeroCardType.none:
                index = 1;
                break;
            case EnumHeroCardType.normal:
                index = 4;
                break;
            case EnumHeroCardType.super:
                index = 7;
                m_size = 24;
                break;
            case EnumHeroCardType.legend:
                index = 8;
                m_size = 24;
                break;
        }
        string color = ColorUtil.ColorToHex(ColorUtil.GetColor(index));
        str = string.Format("<size='{0}'><color='#{1}'>{2}</color></size>", m_size, color, str);
        return str;
    }
}
