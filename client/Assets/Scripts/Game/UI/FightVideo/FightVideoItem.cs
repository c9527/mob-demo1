﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：战斗回放
//创建者：hwl
//创建日期: 2016/8/20
///////////////////////////////////////////
public class FightVideoItem : ItemRender {

    private GameObject m_playedText;
    private Text m_modeText;
    private Text m_timeText;
    private Text m_playedCountText;
    private GameObject m_lookBtn;
    private GameObject m_playBtn;

    private TeamItem[] TeamItemList = new TeamItem[2];
    private p_record_history m_data;
    private string m_modeName;
    private GameObject m_corpsMatchStage;
    private Text m_corpsMatchStageText;

    public override void Awake()
    {
        m_playedText = transform.Find("playedText").gameObject;
        m_modeText = transform.Find("modeText").GetComponent<Text>();
        m_timeText = transform.Find("timeText").GetComponent<Text>();
        m_playedCountText = transform.Find("playedCountText").GetComponent<Text>();

        m_corpsMatchStage = transform.Find("corpsMatchFlag").gameObject;
        m_corpsMatchStageText = transform.Find("corpsMatchFlag/Text").GetComponent<Text>();
        m_lookBtn = transform.Find("lookIcon").gameObject;
        m_playBtn = transform.Find("playbtn").gameObject;
        UGUIClickHandler.Get(m_lookBtn).onPointerClick += OpenPanelTeamInfo;
        UGUIClickHandler.Get(m_playBtn).onPointerClick += OnPlayVideo;
        
        for( int i = 1; i <= 2; i++)
        {
            TeamItem item = new TeamItem(transform.Find("item" + i));
            TeamItemList[i-1] = item;
        }
    }

    private void OpenPanelTeamInfo(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelFightVideoTeamInfo>(new object[] { m_modeName, m_data.map, m_data.id, m_data.teams,m_data.game_type },true);
    }

    private void OnPlayVideo(GameObject target, PointerEventData eventData)
    {
        int isPlay = PlayerPrefs.GetInt("fightVideo_" + m_data.id);
        if( isPlay == 0 )
        {
            PlayerPrefs.SetInt("fightVideo_" + m_data.id,1);
        }

        FightVideoManager.isPlayFight = true;
        FightVideoManager.m_curFightId = m_data.id;
        bool byCenter = false;
        if (FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString() || FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString()+"_joke")
        {
            byCenter = true;
        }
        NetLayer.Send(new tos_record_watch_record() { id = m_data.id,by_center = byCenter});
        
       
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_record_history;
        if (m_data == null)
        {
            return;
        }

        p_team_info[] list = new p_team_info[2];
        if (m_data.teams.Length < 2 || m_data.teams[1].roles.Length == 0)
        {
            for (int j = 0; j < 2; j++)
            {
                p_team_info info = new p_team_info();
                if (m_data.teams[0].roles.Length < 1)
                {
                    info.corps_name = "";
                    info.icon = 1;
                    info.name = "";
                }
                else
                {
                    info.corps_name = (j >= m_data.teams[0].roles.Length  )? "" : m_data.teams[0].roles[j].corps_name;
                    info.icon = (j >= m_data.teams[0].roles.Length) ? 1 : m_data.teams[0].roles[j].icon;
                    info.name = (j >= m_data.teams[0].roles.Length) ? "" : m_data.teams[0].roles[j].name;
                    
                }
                list[j] = info;
            }
        }
        else
        {
            list = m_data.teams;
        }

        for (int i = 0; i < list.Length; i++)
        {
            TeamItemList[i].UpdateTeamInfo(list[i], m_data.win_camp, m_data.id);
        }

        int isPlay = PlayerPrefs.GetInt("fightVideo_" + m_data.id);
        m_playedText.TrySetActive(isPlay == 1);

        m_modeName = ConfigManager.GetConfig<ConfigGameRule>().GetLineByGameRule(m_data.game_type).Mode_Name;
        m_modeText.text = m_modeName;
        m_playedCountText.text = m_data.watch_times + "观看";
        int time = (int)TimeUtil.GetPassTime(m_data.time).TotalSeconds;
        if( time /(24*3600) >= 1)
        {
            int index = TimeUtil.FormatTime(time, 4, true).IndexOf("时");
            m_timeText.text = TimeUtil.FormatTime(time, 4, true).Remove(index+1) + "前";
        }
        else
        {
            m_timeText.text = TimeUtil.FormatTime(time, 2, false) + "前";
        }
        if (FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString() || FightVideoManager.m_fightType == (ChannelTypeEnum.match.ToString()+"_joke"))
        {
            m_corpsMatchStage.TrySetActive(false);
        }
        else
        {
            m_corpsMatchStage.TrySetActive(true);
            m_corpsMatchStageText.text = FightVideoManager.GetMatchStage(m_data.schedule);
        }
    }

}
class TeamItem
{
    private Image m_headIcon;
    private Image m_armIcon;
    private Image m_resultIcon;
    private Text m_playerNameText;
    private Text m_levelText;
    private Text m_corpsNameText;
    private GameObject m_headGo;
    private GameObject m_corpsIconGo;

    private Image m_corpsBg;
    private Image m_corpsIcon;
    public TeamItem(Transform tran)
    {
        m_resultIcon = tran.Find("result").GetComponent<Image>();
        m_headIcon = tran.Find("head/imgIcon").GetComponent<Image>();
        m_playerNameText = tran.Find("playerName").GetComponent<Text>();
        m_corpsNameText = tran.Find("corpName").GetComponent<Text>();
        m_corpsBg = tran.Find("logo/flag").GetComponent<Image>();
        m_corpsIcon = tran.Find("logo/biaozhi").GetComponent<Image>();
        m_headGo = tran.Find("head").gameObject;
        m_corpsIconGo = tran.Find("logo").gameObject;
    }

    public void UpdateTeamInfo(p_team_info info, int winCamp, long id)
    {
        if (FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString() || FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString()+"_joke")
        {
            m_headGo.TrySetActive(true);
            m_corpsIconGo.TrySetActive(false);
            m_headIcon.SetSprite(ResourceManager.LoadRoleIcon(info.icon));
            m_headIcon.SetNativeSize();
        }
        else
        {
            m_headGo.TrySetActive(false);
            m_corpsIconGo.TrySetActive(true);
            string flag = "f" + info.logo_pannel.ToString() + "_" + "c" + info.logo_color.ToString();
            m_corpsBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            string biaozhi = "biaozhi_" + info.logo_icon.ToString();
            m_corpsIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        }
        
        m_corpsNameText.text = info.corps_name;
        m_playerNameText.text = info.name;
        string result = "";
        int value = PlayerPrefs.GetInt("fightVideo_" + id);
        if (value == 1)
        {
            m_resultIcon.gameObject.TrySetActive(true);
            if (info.camp == winCamp)
            {
                result = "win";
            }
            else
            {
                result = "lose";
            }
            m_resultIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.FightVideo, result));
        }
        else
        {
            m_resultIcon.gameObject.TrySetActive(false);
        }
       
    }
}
