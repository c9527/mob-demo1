﻿using UnityEngine;
using System.Collections;

///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：战斗回放数据管理
//创建者：hwl
//创建日期: 2016/8/20
///////////////////////////////////////////
static class FightVideoManager 
{
    private static toc_record_history_list m_record;
    public static bool isPlayFight = false;
    public static string m_fightType;
    public static long m_curFightId;
    public static bool isCloseBySettingPanel = false;
    public static int PlaySpeed = 1;
    public static toc_record_history_list RecordInfo { get { return m_record; } }
    public static TDScoreTitle GetHonorConfig()
    {
        var score = PlayerSystem.roleData.honor;
        var info = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(score);
        return info;
    }

    public static string GetHonorName()
    {
        string honorName = "";
        var score = PlayerSystem.roleData.honor;
        var info = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(score);
        if (info != null)
        {
            honorName = info.Name;
        }
        return honorName;
    }

    public static bool ByCenter()
    {
        if (m_fightType == ChannelTypeEnum.match.ToString() || m_fightType == ChannelTypeEnum.match.ToString() + "_joke")
        {
            return true;
        }
        return false;
    }

    public static string GetMatchStage(int value)
    {
        switch(value)
        {
            case 1:
                return "十六强赛";

            case 2:
                return "八强赛";

            case 3:
                return "4强赛";

            case 4:
                return "四进二";

            case 5:
                return "季军赛";

            case 6:
                return "冠军赛";
        }
        return "";
    }

    static void Toc_record_history_list(toc_record_history_list recordData)
    {
        m_record = recordData;
        m_fightType = m_record.game_type;
        PanelFightVideo panel = UIManager.GetPanel<PanelFightVideo>();
        if (panel != null &&panel.IsOpen() == true)
        {
            panel.UpdateViewInfo();
        }
    }
}
public class record_history
{
    public int CurPage = 1;
    public int TotalPage = 1;
    public int FightType;
    public int HonorId;
    internal p_record_history[] recordList;
}



