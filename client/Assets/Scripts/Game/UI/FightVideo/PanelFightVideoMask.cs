﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PanelFightVideoMask : BasePanel
{
    WorldManager.FightExitCallBack _callback;

    public PanelFightVideoMask()
    {
        SetPanelPrefabPath("UI/FightVideo/PanelFightVideoMask");
        AddPreLoadRes("UI/FightVideo/PanelFightVideoMask", EResType.UI);
        AddPreLoadRes("Atlas/FightVideo", EResType.Atlas);
    }

    public override void Init()
    {
        if(HasParams())
        {
            _callback = m_params[0] as WorldManager.FightExitCallBack;
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("replay").gameObject).onPointerClick += onReplayHandle;
        UGUIClickHandler.Get(m_tran.Find("exit").gameObject).onPointerClick += onExitHandle;
    }

    public void onReplayHandle(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_room_leave());
        bool byCenter = false;
        if (FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString() || FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString()+"_joke")
        {
            byCenter = true;
        }
        NetLayer.Send(new tos_record_watch_record() { id = FightVideoManager.m_curFightId, by_center = byCenter });
        FightVideoManager.isPlayFight = true;
        HidePanel();
    }

    public void onExitHandle(GameObject target,PointerEventData eventData)
    {
        if(_callback != null)
        {
            _callback();
        }
        HidePanel();
    }

    public override void OnShow()
    {

    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
