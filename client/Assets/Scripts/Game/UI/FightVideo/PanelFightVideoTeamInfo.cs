﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
///////////////////////////////////////////
    //Copyright (C): 4399 FPS studio
    //All rights reserved
    //文件描述：战斗回放队伍信息
    //创建者：hwl
    //创建日期: 2016/8/20
///////////////////////////////////////////
public class PanelFightVideoTeamInfo : BasePanel
{
    private DataGrid m_dataGrid1;
    private DataGrid m_dataGrid2;
    private Text m_mapNameText;
    private Text m_modeNameText;
    private long m_id;
    private string chanelType;

    public PanelFightVideoTeamInfo()
    {
        SetPanelPrefabPath("UI/FightVideo/PanelFightVideoTeamInfo");
        AddPreLoadRes("UI/FightVideo/PanelFightVideoTeamInfo", EResType.UI);
    }

    public override void Init()
    {
        m_dataGrid1 = m_tran.Find("LeftList/content").gameObject.AddMissingComponent<DataGrid>();
        m_dataGrid1.SetItemRender(m_tran.Find("LeftList/content/ItemTeamMember").gameObject, typeof(TeamMemberItem));
        m_dataGrid1.Data = new object[5];

        m_dataGrid2 = m_tran.Find("RightList/content").gameObject.AddMissingComponent<DataGrid>();
        m_dataGrid2.SetItemRender(m_tran.Find("RightList/content/ItemTeamMember").gameObject, typeof(TeamMemberItem));
        m_dataGrid2.Data = new object[5];
        m_modeNameText = m_tran.Find("modeName").GetComponent<Text>();
        m_mapNameText = m_tran.Find("mapName").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += delegate { base.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("playbtn").gameObject).onPointerClick += onPlayVideo;
    }

    private void onPlayVideo(GameObject target,PointerEventData eventData)
    {
        int isPlay = PlayerPrefs.GetInt("fightVideo_" + m_id);
        if (isPlay == 0)
        {
            PlayerPrefs.SetInt("fightVideo_" + m_id, 1);
        }
        FightVideoManager.isPlayFight = true;
        bool byCenter = false;
        if (FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString() || FightVideoManager.m_fightType == ChannelTypeEnum.match.ToString()+"_joke")
        {
            byCenter = true;
        }
        NetLayer.Send(new tos_record_watch_record() { id = m_id,by_center = byCenter });
        
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            m_modeNameText.text = m_params[0].ToString();
            int mapID = Convert.ToInt32(m_params[1]);
            m_id = (long)m_params[2];
            chanelType = m_params[3].ToString();
            p_team_info[] list = m_params[3] as p_team_info[];

            p_team_role[] roles1 = new p_team_role[5];
            p_team_role[] roles2 = new p_team_role[5];

            
            if (list[1].roles.Length == 0)
            {
                int len = list[0].roles.Length/2;
                for (int i = 0; i < len; i++)
                {
                    roles1[i] = list[0].roles[i];
                    roles2[i] = list[0].roles[len + i];
                }

            }else
            {
                for (int i = 0; i < list[0].roles.Length; i++)
                {
                    roles1[i] = list[0].roles[i];
                }
                for (int i = 0; i < list[1].roles.Length; i++)
                {
                    roles2[i] = list[1].roles[i];
                }
            }
            
            m_dataGrid1.Data = roles1;
            m_dataGrid2.Data = roles2;
            showMapID(mapID);
        }
    }

    private void showMapID(int mapid)
    {
        var configs = ConfigManager.GetConfig<ConfigMapList>();
        ConfigMapListLine mapLine = null;
        if (configs != null)
        {
            mapLine = configs.GetLine(mapid.ToString());
        }
        m_mapNameText.text = mapLine == null ? "" : mapLine.MapName;
    }

    public override void OnHide()
    {
        FightVideoManager.isPlayFight = false;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
