﻿using UnityEngine;
using System.Collections;

public class PanelBattleReady : BasePanel {

    private GameObject m_ready;
    private GameObject m_start;
    private bool m_needShowStart = true;
    private int m_timerId = -1;

    public PanelBattleReady()
    {
        SetPanelPrefabPath("UI/Battle/PanelBattleReady");
        AddPreLoadRes("UI/Battle/PanelBattleReady", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }


    public override void Init()
    {
        m_ready = m_tran.Find("ready").gameObject;
        m_start = m_tran.Find("start").gameObject;
        m_ready.TrySetActive(true);
        m_start.TrySetActive(false);
    }

    public override void InitEvent()
    {
        
    }

    public override void Update()
    {
        if (!IsOpen())
            return;
        if(PlayerBattleModel.Instance.canStart)
        {
            if(m_needShowStart)
            {
                PlayerBattleModel.Instance.ClearBattleRecord();
                m_ready.TrySetActive(false);
                var panel = UIManager.GetPanel<PanelBattle>();
                if (panel != null)
                    panel.ClearKillMsg();
                m_needShowStart = false;                
                
                //if(!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
                //{
                    m_start.TrySetActive(true);
                    m_timerId = TimerManager.SetTimeOut(5f, () =>
                    {
                        if (m_start != null)
                            m_start.TrySetActive(false);
                        HidePanel();
                        m_timerId = -1;
                    }, true);
                //}                
            }
        }
    }

    public override void OnShow()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        if (m_timerId != -1)
        {
            TimerManager.RemoveTimeOut(m_timerId);
            m_timerId = -1;
        }
    }
}
