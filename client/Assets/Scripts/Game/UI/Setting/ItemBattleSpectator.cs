﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemBattleSpectator : ItemRender
{
    Battle_Spectator m_data;
    //0死亡 1警察 2匪徒
    int _style = 0;

    Image _item_bg;

    Image _head_bg;
    Image _head_icon;
    Image _lv_ico;
    Text _lv_txt;
    Text _name_txt;
    Text _desc_txt;
    //Text _score_txt;
    //Image _status_ico;

    //GameObject _goVoteBtn;
    Image _imgVip;
    GameObject _goVip;
    GameObject _goName;
    //public void ShowVoteBtn(bool isShow = false)
    //{
    //    if (_goVoteBtn != null) _goVoteBtn.TrySetActive(isShow);
    //}

    public override void Awake()
    {
        _item_bg = transform.Find("item_bg").GetComponent<Image>();
        _head_bg = transform.Find("head_bg").GetComponent<Image>();
        _head_icon = transform.Find("head_icon").GetComponent<Image>();
        _lv_ico = transform.Find("junxian/Icon").GetComponent<Image>();
        _lv_txt = transform.Find("junxian/Text").GetComponent<Text>();
        _name_txt = transform.Find("name/name_txt").GetComponent<Text>();
        _desc_txt = transform.Find("desc_txt").GetComponent<Text>();
        //_score_txt = transform.Find("score_txt").GetComponent<Text>();
        //_status_ico = transform.Find("status_ico").GetComponent<Image>();
        //_goVoteBtn = transform.Find("vote_btn").gameObject;
        //_goVoteBtn.TrySetActive(false);
        _imgVip = transform.Find("name/vip").GetComponent<Image>();
        _goVip = transform.Find("name/vip").gameObject;
        _goName = transform.Find("name/name_txt").gameObject;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as Battle_Spectator;
        if (m_data == null)
        {
            return;
        }
        updateStyles(0);
        if (m_data.m_bMainRole)
        {
            _name_txt.color = new Color(0, 1, 0);
            _desc_txt.color = new Color(0, 1, 0);
            //_score_txt.color = new Color(0, 1, 0);
        }
        else if (_style == 1)
        {
            _name_txt.color = new Color(1, 0.86f, 0.73f);
            _desc_txt.color = new Color(1, 0.86f, 0.73f);
            //_score_txt.color = new Color(1, 0.86f, 0.73f);
        }
        else if (_style == 2)
        {
            _name_txt.color = new Color(0.82f, 0.91f, 1);
            _desc_txt.color = new Color(0.82f, 0.91f, 1);
            //_score_txt.color = new Color(0.82f, 0.91f, 1);
        }
        else
        {
            _name_txt.color = new Color(0.87f, 0.87f, 0.87f);
            _desc_txt.color = new Color(0.87f, 0.87f, 0.87f);
            //_score_txt.color = new Color(0.87f, 0.87f, 0.87f);
        }

        _head_icon.SetSprite(ResourceManager.LoadRoleIcon(m_data.m_icon, 0));
        _lv_ico.SetSprite(ResourceManager.LoadArmyIcon(m_data.m_level));
        _lv_txt.text = String.Format("{0}级",  m_data.m_level);
        //_status_ico.gameObject.TrySetActive(false);
        _name_txt.text = PlayerSystem.GetRoleNameFull(m_data.m_strName, m_data.m_iPlayerId);
        _desc_txt.text = m_data.m_strCampName;
        //_score_txt.text = "";

        _goVip.TrySetActive(false);
    }

    private void updateStyles(int value = -1)
    {
        if (_style == value)
        {
            return;
        }
        _style = value;
        if (_style == 1)
        {
            _item_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Setting, "record_item_bg_1"));

            _head_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Setting, "record_avatar_bg_1"));
        }
        else if (_style == 2)
        {
            _item_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Setting, "record_item_bg_2"));

            _head_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Setting, "record_avatar_bg_1"));
        }
        else
        {
            _item_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Setting, "record_item_bg_0"));
        }
    }
}
