﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class Battle_Record
{
    public int m_iPlayerId;
    public string m_strName;
    public int m_iKillNum;
    public int m_iDeathNum;
    public int m_score;
    public int m_level;
    public bool m_bAlive;
    public int m_iCamp;
    public bool m_bMainRole;
    public int m_icon;
    public MemberManager.MEMBER_TYPE m_eType;
    public int m_honor;//排位赛积分
}

/// <summary>
/// 观战者信息
/// </summary>
public class Battle_Spectator
{
    public int m_iPlayerId;
    public string m_strName;
    public bool m_bMainRole;
    public int m_icon;
    public int m_level;
    public string m_strCampName;
}

public class BattleRecordType
{
    public const int Empty = -1;
    public const int Player = 0;
    public const int Spectator = 1;
}

public class PanelBattleRecord : BasePanel
{
    private static DataGrid m_jingDataGrid;
    private static DataGrid m_feiDataGrid;
    private static DataGrid m_soloDataGrid;
    private static DataGrid m_spectatorGrid;
    private GameObject m_goJing;
    private GameObject m_goFei;
    private GameObject m_goSolo;
    private GameObject m_goSpectartor;
    private GameObject m_goKick;
    private Text m_txtTitle;
    private GameObject m_goSpectatorInfo;
    private Text m_spectatorInfoText;
    private GameObject exit_btn;

    List<ItemRender> renderArray = new List<ItemRender>();

    private float m_tick;

    private int m_recordType = BattleRecordType.Empty;

    public PanelBattleRecord()
    {
        SetPanelPrefabPath("UI/Setting/PanelBattleRecord");

        AddPreLoadRes("UI/Setting/PanelBattleRecord", EResType.UI);
        AddPreLoadRes("UI/Setting/ItemBattleRecord", EResType.UI);
        AddPreLoadRes("UI/Setting/ItemBattleSpectator", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public void ShowVoteBtn(bool isShow = false)
    {
        if (isShow && m_recordType != BattleRecordType.Player)
            return;

        object[] dataArray = null;
        renderArray.Clear();
        if (MainPlayer.singleton.Camp == 0)
        {
            dataArray = m_soloDataGrid.Data;
            renderArray = m_soloDataGrid.ItemRenders;
        }
        else
        {
            dataArray = m_jingDataGrid.Data.ArrayConcat(m_feiDataGrid.Data);
            renderArray.AddRange(m_jingDataGrid.ItemRenders);
            renderArray.AddRange(m_feiDataGrid.ItemRenders);
        }

        if (dataArray != null && dataArray.Length > 0)
        {
            int count = dataArray.Length;
            //Logger.Error("ShowVoteBtn, count: "+ count);
            for (int i = 0; i < count; i++)
            {
                Battle_Record data = dataArray[i] as Battle_Record;
                ItemBattleRecord render = renderArray[i] as ItemBattleRecord;
                render.ShowVoteBtn(isShow && data.m_iCamp == MainPlayer.singleton.Camp && data.m_iPlayerId != MainPlayer.singleton.PlayerId);
                //Logger.Error("camp: " + data.m_iCamp + ", player: " + data.m_iPlayerId + ", my camp: " + MainPlayer.singleton.Camp + ", myId: " + MainPlayer.singleton.PlayerId);
            }
        }
    }

    private void OnGetData()
    {
        if (IsOpen() == false)
            return;

        // 调用：窗口点开时；窗口点开数据更新时
        // Camp PlayerCamp Change Event
        if (m_recordType == BattleRecordType.Player)
        {
             OnGetBattleRecordData();
        }
    }

    private void OnSpectatorJoin(long pid)
    {
        if (IsOpen() == false)
            return;

        if (m_recordType == BattleRecordType.Spectator)
        {
            OnGetBattleSpectatorData();
        }
    }

    private void OnSpectatorLeave(long pid)
    {
        if (IsOpen() == false)
            return;

        if (m_recordType == BattleRecordType.Spectator)
        {
            OnGetBattleSpectatorData();
        }
    }

    private void OnGetBattleSpectatorData()
    {
        m_goSpectartor.TrySetActive(true);
        m_goJing.TrySetActive(false);
        m_goFei.TrySetActive(false);
        m_goSolo.TrySetActive(false);


        List<SBattleSpectator> spectators = PlayerBattleModel.Instance.battle_info.spectators;

        int spectatorCount = spectators.Count;

        m_spectatorInfoText.text = string.Format("{0}/50", spectatorCount);

        var lstRec = new List<Battle_Spectator>();

        for (int i = 0; i < spectatorCount; i++)
        {
            SBattleSpectator spectator = spectators[i];

            Battle_Spectator sp = new Battle_Spectator();

            sp.m_iPlayerId = (int)spectator.id;
            sp.m_strName = spectator.name;
            sp.m_icon = spectator.icon;
            sp.m_level = spectator.level;
            sp.m_strCampName = spectator.campName;
            sp.m_bMainRole = sp.m_iPlayerId == WorldManager.singleton.myid ? true : false;
            lstRec.Add(sp);
        }

        DataGrid dataGrid = m_spectatorGrid;

        dataGrid.autoSelectFirst = false;
        dataGrid.Data = lstRec.ToArray();
    }

    private void OnGetBattleRecordData()
    {
        m_goSpectartor.TrySetActive(false);

        ConfigGameRuleLine line = WorldManager.singleton.gameRuleLine;
        if (line != null)
            m_txtTitle.text = line.Mode_Name;

        List<SBattleCamp> camps = PlayerBattleModel.Instance.battle_info.camps;

        for (int i = 0; i < camps.Count; i++)
        {
            SBattleCamp camp = camps[i];

            int cp = camp.camp;
            DataGrid dataGrid = null;
            if (cp == 0)
            {
                dataGrid = m_soloDataGrid;
                m_goJing.TrySetActive(false);
                m_goFei.TrySetActive(false);
                m_goSolo.TrySetActive(true);
            }
            else
            {
                if (cp == 1)
                    dataGrid = m_jingDataGrid;
                else
                    dataGrid = m_feiDataGrid;

                m_goJing.TrySetActive(true);
                m_goFei.TrySetActive(true);
                m_goSolo.TrySetActive(false);
            }

            var lstRec = new List<Battle_Record>();
            List<SBattlePlayer> players = camp.players;
            for (int j = 0; j < players.Count; j++)
            {
                SBattlePlayer pl = players[j];

                //DOTA模式，非人类不显示排行榜
                if (null != WorldManager.singleton && WorldManager.singleton.isDotaModeOpen)
                {
                    if (pl.actor_type != GameConst.ACTOR_TYPE_PERSON)
                    {
                        continue;
                    }
                }

                Battle_Record newRcd = new Battle_Record();
                newRcd.m_iPlayerId = pl.actor_id;
                newRcd.m_strName = pl.name;
                newRcd.m_iKillNum = pl.kill;
                newRcd.m_iDeathNum = pl.death_num;
                newRcd.m_bAlive = pl.alive;
                newRcd.m_level = pl.level;
                newRcd.m_score = pl.score;
                newRcd.m_icon = pl.icon;
                newRcd.m_iCamp = cp;
                newRcd.m_bMainRole = newRcd.m_iPlayerId == WorldManager.singleton.myid ? true : false;
                newRcd.m_eType = (MemberManager.MEMBER_TYPE)pl.vip_level;
                newRcd.m_honor = pl.honor;
                lstRec.Add(newRcd);
            }
            dataGrid.autoSelectFirst = false;
            dataGrid.Data = lstRec.ToArray();
            if (dataGrid.onItemSelected == null) dataGrid.onItemSelected = OnItemSelected;
        }
    }



    public override void Init()
    {
        m_txtTitle = m_tran.Find("title").GetComponentInChildren<Text>();
        exit_btn = m_tran.Find("exit_btn").gameObject;
        m_goJing = m_tran.Find("frame_jing").gameObject;
        m_goFei = m_tran.Find("frame_fei").gameObject;
        m_goSolo = m_tran.Find("frame_solo").gameObject;
        m_goSpectartor = m_tran.Find("frame_spectator").gameObject;
        
        m_goKick = m_tran.Find("kickout_btn").gameObject;
        m_goSpectatorInfo = m_tran.Find("spectator_info").gameObject;
        m_spectatorInfoText = m_tran.Find("spectator_info/spectator_info_text").GetComponent<Text>();
        
        m_jingDataGrid = m_tran.Find("frame_jing/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_jingDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Setting/ItemBattleRecord"), typeof(ItemBattleRecord));

        m_feiDataGrid = m_tran.Find("frame_fei/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_feiDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Setting/ItemBattleRecord"), typeof(ItemBattleRecord));

        m_soloDataGrid = m_tran.Find("frame_solo/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_soloDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Setting/ItemBattleRecord"), typeof(ItemBattleRecord));

        m_spectatorGrid = m_tran.Find("frame_spectator/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_spectatorGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Setting/ItemBattleSpectator"), typeof(ItemBattleSpectator));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("exit_btn")).onPointerClick += delegate
        {
            string tipstr = PlayerBattleModel.Instance.battle_info.channel_type == "match" ? "中途退出游戏，将扣除双倍积分，是否确认退出？" : "中途退出游戏，将无法获得奖励，是否确认退出？";
            string confirm = "强制退出";
            string cancel = "继续战斗";
            if (WorldManager.singleton.isViewBattleModel || PlayerBattleModel.Instance.battle_state == null || PlayerBattleModel.Instance.battle_state.state == GameConst.Fight_State_GameInit)
            {
                tipstr = "是否退出";
                confirm = "退出";
                cancel = "取消";
            }
            UIManager.ShowTipPanel(tipstr,
                () =>
                {
                    if (RoomModel.IsGuideStage)
                        GuideManager.DestroyGuide();
                    HidePanel();
                    //PanelRoomList.SendMsg("leave_fight");
                    NetLayer.Send(new tos_room_leave());
                }, HidePanel,
                false, true, confirm, cancel);
       };
       UGUIClickHandler.Get(m_tran.Find("return_btn")).onPointerClick += delegate { HidePanel(); };
       UGUIClickHandler.Get(m_tran.Find("kickout_btn")).onPointerClick += delegate { OnKickOutSb(); };

       GameDispatcher.AddEventListener<long>(GameEvent.SPECTATOR_JOIN, OnSpectatorJoin);
       GameDispatcher.AddEventListener<long>(GameEvent.SPECTATOR_LEAVE, OnSpectatorLeave);
       // UGUIClickHandler.Get(m_tran.Find("kickout_btn")).onPointerClick += delegate { TipsManager.Instance.showTips("该功能正在升级中"); };
    }

    private void OnKickOutSb()
    {
        NetLayer.Send(new tos_fight_can_vote_kick() { });
    }

    private void OnItemSelected(object data)
    {
        if (PlayerBattleModel.Instance.CanVoteKick)
        {
            Battle_Record rec = data as Battle_Record;
            if (rec.m_iCamp == MainPlayer.singleton.Camp && rec.m_iPlayerId != MainPlayer.singleton.PlayerId)
            {
                HidePanel();
                UIManager.PopPanel<PanelBattleKickReason>(new object[] { data });
            }
        }
    }

    public override void OnShow()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        Screen.lockCursor = false;
#endif
        if (m_params != null && m_params.Length > 0)
        {
            SwitchRecord((int)m_params[0]);
        }
        exit_btn.TrySetActive(!FightVideoManager.isPlayFight);
        m_goKick.TrySetActive(!WorldManager.singleton.isViewBattleModel && m_recordType == BattleRecordType.Player);
        //WorldManager.singleton.ShowInfo(true);
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        Screen.lockCursor = true;
#endif
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<long>(GameEvent.SPECTATOR_JOIN, OnSpectatorJoin);
        GameDispatcher.RemoveEventListener<long>(GameEvent.SPECTATOR_LEAVE, OnSpectatorLeave);
    }
    public override void Update()
    {
        m_tick += Time.deltaTime;
        if (m_tick >= 1.5f)
        {
            m_tick = 0;
            OnGetData();
        }
    }

    public void SwitchRecord(int type)
    {
        m_recordType = type;

        if (!IsInited())
            return;

        switch (m_recordType)
        {
            case BattleRecordType.Player:
                m_goSpectatorInfo.TrySetActive(false);
                OnGetBattleRecordData();
                break;
            case BattleRecordType.Spectator:
                m_goSpectatorInfo.TrySetActive(true);
                OnGetBattleSpectatorData();
                break;
        }
    }
}
