﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
#if UNITY_WEBPLAYER
using DockingModule;
#endif
public class PanelParamSetting : BasePanel
{
    private ConfigBattleParamSetting m_defaultParamSetting;
    private ConfigBattleParamSetting m_userParamSetting;

    const string SAVE_BATTLE_PARAM_KEY = "user_set_battle_params";

    Slider m_sliderSensity;
    Slider m_sliderAim;
    Slider m_sliderAimAcc;
    Slider m_sliderMusic;
    Slider m_sliderSound;
    Slider m_sliderFightSound;

    Toggle m_driftFire;
    Toggle m_stockFire;

    Toggle m_clickSquat;
    Toggle m_pressSquat;

    Toggle m_doubleJump;
    Toggle m_rightJump;

    Toggle m_grenadeSwitchOn;
    Toggle m_grenadeSwitchOff;

    Toggle m_lightGrenadeSwitchOn;
    Toggle m_lightGrenadeSwitchOff;

    Toggle m_weaponSwitchOn;
    Toggle m_weaponSwitchOff;

    Toggle m_battleAutoAudioSwitchOn;
    Toggle m_battleAutoAudioSwitchOff;

    Toggle m_FightDirectionTopToggle;
    Toggle m_FightDirectionBottomToggle;
    Toggle m_FightDirectionHideToggle;

    Toggle m_showCityToggle;
    Toggle m_hideCityToggle;

    Toggle m_BecomeHeroHand;
    Toggle m_BecomeHeroAuto;

    Toggle m_ChangeMatrixHand;
    Toggle m_ChangeMatrixAuto;

    Toggle m_leftHandToggle;
    Toggle m_rightHandToggle;

    Toggle m_crossPointToggle;//带红点的准星
    Toggle m_crossToggle;//不带红点的准星
    Toggle m_circleCrossToggle;//圆圈红星

    Toggle m_JoystickToggle1;
    Toggle m_JoystickToggle2;
    Toggle m_JoystickToggle3;

    Toggle m_JoystickMoveToggle1;
    Toggle m_JoystickMoveToggle2;

    Toggle m_NormalKnifeToggle;
    Toggle m_ChangeHeavyKnifeToggle;

    Toggle m_KnifeAutoAimOpenToggle;
    Toggle m_KnifeAutoAimCloseToggle;

    Toggle m_ZombieAutoAimOpenToggle;
    Toggle m_ZombieAutoAimCloseToggle;

    Text m_sensityValue;
    Text m_aimValue;
    Text m_aimAccValue;
    Text m_musicValue;
    Text m_soundValue;
    Text m_fightSoundValue;

    Dictionary<string, int> m_dicDefaultKeyToIndex;
    Dictionary<string, int> m_dicUserKeyToIndex;

    bool m_beAddBkPanel = false;
    private Button m_btnClose;
    private Button m_btnExit;
    private Button m_btnSend;
    private Button m_btnLogout;
    private Button m_btnScreen;

    private GameObject m_btnQuit;
    private GameObject m_btnReturn;
    private GameObject m_baseScrollDataGridGo;
    private GameObject m_btnScrollDataGridGo;
    private GameObject m_shotGroup;

    private GameObject m_moveShop;
    private GameObject m_nomoveShop;

    private GameObject m_nomoveCheck;
    private GameObject m_moveCheck;

    private GameObject m_moveImage;
    int time = 300;
    int movingTime = 0;
    Vector3 initpos;
    int upordown = -1;
    public PanelParamSetting()
    {
        SetPanelPrefabPath("UI/Setting/PanelParamSetting");
        AddPreLoadRes("UI/Setting/PanelParamSetting", EResType.UI);

        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }
    public override void Init()
    {
        m_defaultParamSetting = ConfigManager.GetConfig<ConfigBattleParamSetting>();
        m_userParamSetting = ScriptableObject.CreateInstance<ConfigBattleParamSetting>();
        ReadUserBattleParamSetting();
        m_baseScrollDataGridGo = m_tran.Find("frame/ScrollDataGrid").gameObject;
        m_btnScrollDataGridGo = m_tran.Find("frame/btnScrollDataGrid").gameObject;

        m_sliderSensity = m_tran.Find("frame/ScrollDataGrid/content/offen/Sensity/SensitySlider").gameObject.GetComponent<Slider>();
        m_sliderAim = m_tran.Find("frame/ScrollDataGrid/content/offen/Aim/AimSlider").gameObject.GetComponent<Slider>();
        m_sliderAimAcc = m_tran.Find("frame/ScrollDataGrid/content/offen/AimAcc/AimAccSlider").gameObject.GetComponent<Slider>();
        m_sliderMusic = m_tran.Find("frame/ScrollDataGrid/content/offen/Music/MusicSlider").gameObject.GetComponent<Slider>();
        m_sliderSound = m_tran.Find("frame/ScrollDataGrid/content/offen/Sound/SoundSlider").gameObject.GetComponent<Slider>();
        m_sliderFightSound = m_tran.Find("frame/ScrollDataGrid/content/offen/FightSound/SoundSlider").gameObject.GetComponent<Slider>();

        m_driftFire = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/FireGroup/drift").gameObject.GetComponent<Toggle>();
        m_stockFire = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/FireGroup/stable").gameObject.GetComponent<Toggle>();

        m_clickSquat = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/CrouchGroup/click").gameObject.GetComponent<Toggle>();
        m_pressSquat = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/CrouchGroup/press").gameObject.GetComponent<Toggle>();

        m_doubleJump = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/JumpGroup/double").gameObject.GetComponent<Toggle>();
        m_rightJump = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/JumpGroup/right").gameObject.GetComponent<Toggle>();

        m_grenadeSwitchOn = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/GrenadeGroup/GrenadeOn").gameObject.GetComponent<Toggle>();
        m_grenadeSwitchOff = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/GrenadeGroup/GrenadeOff").gameObject.GetComponent<Toggle>();

        //m_lightGrenadeSwitchOn = m_tran.Find("frame/buttonsetting/LightGrenadeGroup/LightOn").gameObject.GetComponent<Toggle>();
        //m_lightGrenadeSwitchOff = m_tran.Find("frame/buttonsetting/LightGrenadeGroup/LightOff").gameObject.GetComponent<Toggle>();

        m_weaponSwitchOn = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/WeaponSwitchGroup/SwitchOn").gameObject.GetComponent<Toggle>();
        m_weaponSwitchOff = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/WeaponSwitchGroup/SwitchOff").gameObject.GetComponent<Toggle>();

        m_battleAutoAudioSwitchOn = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/BattleAutoAudioGroup/AudioOn").gameObject.GetComponent<Toggle>();
        m_battleAutoAudioSwitchOff = m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/BattleAutoAudioGroup/AudioOff").gameObject.GetComponent<Toggle>();

        m_crossPointToggle = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetCrosshairs/crosshairsPoint").gameObject.GetComponent<Toggle>();
        m_crossToggle = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetCrosshairs/crosshairs").gameObject.GetComponent<Toggle>();
        m_circleCrossToggle = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetCrosshairs/circle").gameObject.GetComponent<Toggle>();

        m_JoystickToggle1 = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetJoystick/Joystick1").gameObject.GetComponent<Toggle>();
        m_JoystickToggle2 = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetJoystick/Joystick2").gameObject.GetComponent<Toggle>();
        m_JoystickToggle3 = m_tran.Find("frame/btnScrollDataGrid/content/operate/SetJoystick/Joystick3").gameObject.GetComponent<Toggle>();

        m_JoystickMoveToggle1 = m_tran.Find("frame/btnScrollDataGrid/content/operate2/SetJoystickMove/JoystickMove1").gameObject.GetComponent<Toggle>();
        m_JoystickMoveToggle2 = m_tran.Find("frame/btnScrollDataGrid/content/operate2/SetJoystickMove/JoystickMove2").gameObject.GetComponent<Toggle>();

        m_NormalKnifeToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/HeavyKnife/normalKnife").gameObject.GetComponent<Toggle>();
        m_ChangeHeavyKnifeToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/HeavyKnife/changeHeavy").gameObject.GetComponent<Toggle>();

        m_KnifeAutoAimOpenToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/KnifeAutoAim/OpenKnifeAutoAim").gameObject.GetComponent<Toggle>();
        m_KnifeAutoAimCloseToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/KnifeAutoAim/CloseKnifeAutoAim").gameObject.GetComponent<Toggle>();

        m_ZombieAutoAimOpenToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ZombieAutoAim/OpenZombieAutoAim").gameObject.GetComponent<Toggle>();
        m_ZombieAutoAimCloseToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ZombieAutoAim/CloseZombieAutoAim").gameObject.GetComponent<Toggle>();

#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/KnifeAutoAim").gameObject.TrySetActive(false);
        m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ZombieAutoAim").gameObject.TrySetActive(false);
#endif
        m_FightDirectionTopToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ShowFightDirection/topLeft").GetComponent<Toggle>();
        m_FightDirectionBottomToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ShowFightDirection/bottomRight").GetComponent<Toggle>();
        m_FightDirectionHideToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ShowFightDirection/hide").GetComponent<Toggle>();
        
        m_showCityToggle = m_tran.Find("frame/ScrollDataGrid/content/otherSetting/ShowCity/show").GetComponent<Toggle>();
        m_hideCityToggle = m_tran.Find("frame/ScrollDataGrid/content/otherSetting/ShowCity/hide").GetComponent<Toggle>();

        m_BecomeHeroHand = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/BecomeHero/hand").GetComponent<Toggle>();
        m_BecomeHeroAuto = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/BecomeHero/auto").GetComponent<Toggle>();

        m_ChangeMatrixHand = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ChangeMatrix/handChangeMatrix").GetComponent<Toggle>();
        m_ChangeMatrixAuto = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ChangeMatrix/autoChangeMatrix").GetComponent<Toggle>();

        m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/ChangeMatrix").gameObject.TrySetActive(false);

        m_leftHandToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/HandTypeBtn/leftHand").GetComponent<Toggle>();
        m_rightHandToggle = m_tran.Find("frame/ScrollDataGrid/content/BasicSetting/HandTypeBtn/rightHand").GetComponent<Toggle>();

        m_sensityValue = m_tran.Find("frame/ScrollDataGrid/content/offen/Sensity/Text").gameObject.GetComponent<Text>();
        m_aimValue = m_tran.Find("frame/ScrollDataGrid/content/offen/Aim/Text").gameObject.GetComponent<Text>();
        m_aimAccValue = m_tran.Find("frame/ScrollDataGrid/content/offen/AimAcc/Text").gameObject.GetComponent<Text>();
        m_musicValue = m_tran.Find("frame/ScrollDataGrid/content/offen/Music/Text").gameObject.GetComponent<Text>();
        m_soundValue = m_tran.Find("frame/ScrollDataGrid/content/offen/Sound/Text").gameObject.GetComponent<Text>();
        m_fightSoundValue = m_tran.Find("frame/ScrollDataGrid/content/offen/FightSound/Text").gameObject.GetComponent<Text>();

        m_btnClose = m_tran.Find("btnClose").GetComponent<Button>();
        m_btnExit = m_tran.Find("frame/btnExit").GetComponent<Button>();
        m_btnSend = m_tran.Find("frame/btnAdvice").GetComponent<Button>();
        m_btnLogout = m_tran.Find("frame/btnLogout").GetComponent<Button>();
        m_btnScreen = m_tran.Find("frame/btnScreen").GetComponent<Button>();
        m_btnQuit = m_tran.Find("frame/quit").gameObject;
        m_btnReturn = m_tran.Find("frame/return").gameObject;

        m_dicDefaultKeyToIndex = new Dictionary<string, int>();
        m_dicUserKeyToIndex = new Dictionary<string, int>();

        BuildKeyToIndex(m_userParamSetting, m_dicUserKeyToIndex);
        BuildKeyToIndex(m_defaultParamSetting, m_dicDefaultKeyToIndex);

       /* if (m_params != null && m_params.Length > 0)
        {
            m_beAddBkPanel = (bool)m_params[0];
            if (m_beAddBkPanel)
            {
                GameObject frame = m_tran.Find("frame").gameObject;
                Image bk = frame.AddMissingComponent<Image>();
                bk.type = Image.Type.Sliced;
                bk.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "view_bg_5"));
            }
        }*/

        m_shotGroup = m_tran.Find("frame/shotSetGroup").gameObject;
        m_nomoveShop = m_tran.Find("frame/shotSetGroup/noMovingshot").gameObject;
        m_moveShop = m_tran.Find("frame/shotSetGroup/Movingshot").gameObject;

        m_moveCheck = m_tran.Find("frame/shotSetGroup/Movingshot/Image").gameObject;
        m_nomoveCheck = m_tran.Find("frame/shotSetGroup/noMovingshot/Image").gameObject;
        m_moveImage = m_tran.Find("frame/shotSetGroup/Movingshot/image/right/Image").gameObject;
        initpos = m_moveImage.transform.localPosition;
        

#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_tran.Find("frame/UserSetting").gameObject.TrySetActive(false);
        m_tran.Find("frame/ScrollDataGrid/content/offen/Aim/AimName").GetComponent<Text>().text = "鼠标灵敏度";
        m_tran.Find("frame/ScrollDataGrid/content/offen/AimAcc/AimAccName").GetComponent<Text>().text = "开镜灵敏度";
#endif

    }
    public override void InitEvent()
    {
        m_sliderSensity.onValueChanged.AddListener(OnSensitySliderValueChange);
        m_sliderAim.onValueChanged.AddListener(OnAimSliderValueChange);
        m_sliderAimAcc.onValueChanged.AddListener(OnAimAccSliderValueChange);
        m_sliderMusic.onValueChanged.AddListener(OnMusicSliderValueChange);
        m_sliderSound.onValueChanged.AddListener(OnSoundSliderValueChange);
        m_sliderFightSound.onValueChanged.AddListener(OnFightSoundSliderValueChange);

        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/FireGroup/drift").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/FireGroup/stable").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/CrouchGroup/click").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/CrouchGroup/press").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/JumpGroup/double").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/JumpGroup/right").gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/GrenadeGroup/GrenadeOn").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/GrenadeGroup/GrenadeOff").gameObject).onPointerClick += OnClickToggle;

        //UGUIClickHandler.Get(m_tran.Find("frame/buttonsetting/LightGrenadeGroup/LightOn").gameObject).onPointerClick += OnClickToggle;
        //UGUIClickHandler.Get(m_tran.Find("frame/buttonsetting/LightGrenadeGroup/LightOff").gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/WeaponSwitchGroup/SwitchOn").gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("frame/btnScrollDataGrid/content/buttonsetting/WeaponSwitchGroup/SwitchOff").gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_BecomeHeroAuto.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_BecomeHeroHand.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_ChangeMatrixHand.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_ChangeMatrixAuto.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_leftHandToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_rightHandToggle.gameObject).onPointerClick += OnClickToggle;


        UGUIClickHandler.Get(m_battleAutoAudioSwitchOn.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_battleAutoAudioSwitchOff.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_crossPointToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_crossToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_circleCrossToggle.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_JoystickToggle1.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_JoystickToggle2.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_JoystickToggle3.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_JoystickMoveToggle1.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_JoystickMoveToggle2.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_NormalKnifeToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_ChangeHeavyKnifeToggle.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_KnifeAutoAimOpenToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_KnifeAutoAimCloseToggle.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_ZombieAutoAimOpenToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_ZombieAutoAimCloseToggle.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_FightDirectionTopToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_FightDirectionBottomToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_FightDirectionHideToggle.gameObject).onPointerClick += OnClickToggle;

        UGUIClickHandler.Get(m_tran.Find("frame/RestoreDefault").gameObject).onPointerClick += OnClickRestoreDefaultSetting;
        UGUIClickHandler.Get(m_tran.Find("frame/UserSetting").gameObject).onPointerClick += OnClickUserSetting;

        UGUIClickHandler.Get(m_showCityToggle.gameObject).onPointerClick += OnClickVisibleCityToggle;
        UGUIClickHandler.Get(m_hideCityToggle.gameObject).onPointerClick += OnClickVisibleCityToggle;


        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += delegate
        {
            HidePanel();
            PanelSetting panel = UIManager.GetPanel<PanelSetting>();
            if (panel != null)
                panel.HidePanel();
        };

        UGUIClickHandler.Get(m_btnExit.gameObject).onPointerClick += delegate
        {
            if (SdkManager.IsSupportFunction(SdkFuncConst.SHOW_PLATFORM_EXIT_DIALOG)||SdkManager.InterNationalVersion())
                SdkManager.ShowExitDialog();
            else
            {
                var tips = "您是否要退出游戏？\n"+ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.EXIT_TIP);
                UIManager.ShowTipPanel(tips, Quit, UIManager.HideTipPanel);
            }
        };

        UGUIClickHandler.Get(m_tran.Find("frame/btnAdvice")).onPointerClick += delegate {
            if (SdkManager.SdkName == "RusSDK")
            {
                SdkManager.openCustomerServices();                
            }
            else if (SdkManager.SdkName == "4399iosRuSdk")
            {
                IosRuSdk.openCustomerService();
            }
            else
            {
                UIManager.PopPanel<PanelAdvice>(null, true);
            }
        };

        UGUIClickHandler.Get(m_btnLogout.gameObject).onPointerClick += delegate
        {
            if (SdkManager.IsSupportFunction(SdkFuncConst.LOGOUT)||SdkManager.InterNationalVersion())
                SdkManager.Logout();
        };
        UGUIClickHandler.Get(m_btnScreen.gameObject).onPointerClick += (target, evt) => UIManager.fullScreen = !UIManager.fullScreen;

        UGUIClickHandler.Get(m_btnQuit).onPointerClick += OnClickQuitBattle;
        UGUIClickHandler.Get(m_btnReturn).onPointerClick += OnClickBackBattle;

        UGUIClickHandler.Get(m_moveShop).onPointerClick += OnClickShotTog;
        UGUIClickHandler.Get(m_nomoveShop).onPointerClick += OnClickShotTog;
        UGUIClickHandler.Get(m_tran.Find("frame/shotSetGroup/noMovingshot/Button")).onPointerClick += delegate { OnClickShotTog(m_nomoveShop, null); };
        UGUIClickHandler.Get(m_tran.Find("frame/shotSetGroup/Movingshot/Button")).onPointerClick += delegate { OnClickShotTog(m_moveShop, null); };
    }

    /// <summary>
    /// 显示的是哪个页签下的内容
    /// ishow = true 显示第一个页签的东西 
    /// ishow = false 显示第二个页签的东西
    /// </summary>
    /// <param name="isShow"> </param>
    public void ShowTabContend(bool isShow = true)
    {
        m_baseScrollDataGridGo.TrySetActive(isShow);
        m_btnScrollDataGridGo.TrySetActive(!isShow);
        m_shotGroup.TrySetActive(false);
    }

    public void ShowShotTab()
    {
        m_baseScrollDataGridGo.TrySetActive(false);
        m_btnScrollDataGridGo.TrySetActive(false);
        m_shotGroup.TrySetActive(true);
    }

    void OnClickShotTog(GameObject target, PointerEventData eventData)
    {
        if (target.name == "Movingshot")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 0.0f);
            m_nomoveShop.GetComponent<Toggle>().isOn = false;
            m_moveShop.GetComponent<Toggle>().isOn = true;
            m_nomoveCheck.TrySetActive(false);
            m_moveCheck.TrySetActive(true);
        }
        else if (target.name == "noMovingshot")
        {
            if (UIManager.IsBattle())
            {
                GameDispatcher.Dispatch(GameEvent.UI_CLICK_STABLE);
            }
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 1.0f);
            m_nomoveShop.GetComponent<Toggle>().isOn = true;
            m_moveShop.GetComponent<Toggle>().isOn = false;
            m_nomoveCheck.TrySetActive(true);
            m_moveCheck.TrySetActive(false);
        }
    }

    void OnClickQuitBattle(GameObject target, PointerEventData eventData)
    {
        string tipstr = (PlayerBattleModel.Instance.battle_info != null &&PlayerBattleModel.Instance.battle_info.channel_type == "match") ? "中途退出战斗，将扣除双倍积分，是否确认退出？" : "中途退出战斗，将无法获得奖励，是否确认退出？";
        string confirm = "强制退出";
        string cancel = "继续战斗";
        if (WorldManager.singleton.isViewBattleModel || PlayerBattleModel.Instance.battle_state == null || PlayerBattleModel.Instance.battle_state.state == GameConst.Fight_State_GameInit)
        {
            tipstr = "是否退出";
            confirm = "退出";
            cancel = "取消";
        }
        UIManager.ShowTipPanel(tipstr,
            delegate
            {
                if (RoomModel.IsGuideStage)
                    GuideManager.DestroyGuide();
                NetLayer.Send(new tos_room_leave());
                RoomModel.SetCustomRoomLeaveType(RoomLeaveType.Force);
                HidePanel();
                UIManager.GetPanel<PanelBattle>().ShowPanel();
                PanelSetting paraSetPanel = UIManager.GetPanel<PanelSetting>();
                if (FightVideoManager.isPlayFight == true)
                {
                    FightVideoManager.isCloseBySettingPanel = true;
                    NetLayer.Send(new tos_record_stop_watch() { by_center = FightVideoManager.ByCenter()});
                }
               
                if (paraSetPanel != null)
                    paraSetPanel.HidePanel();
            },
            delegate { OnClickBackBattle(target, eventData); },
            false, true, confirm, cancel);
    }


    public void OnClickBackBattle(GameObject target, PointerEventData eventData)
    {
        PanelSetting paraSetPanel = UIManager.GetPanel<PanelSetting>();
        if (paraSetPanel != null)
            paraSetPanel.HidePanel();
        HidePanel();
        UIManager.GetPanel<PanelBattle>().ShowPanel();

        if (Driver.m_platform == EnumPlatform.web)
        {
            UIManager.fullScreen = true;
        }
    }




    public override void OnShow()
    {
        bool isShow = true;
        if (HasParams()&&m_params.Length==1)
        {
            isShow = (bool)m_params[0];
        }
        ShowTabContend(isShow);
        if (m_params.Length > 1)
        {
            ShowShotTab();
        }
        if (Driver.m_platform == EnumPlatform.ios)
            m_btnLogout.gameObject.TrySetActive(false);
        else
            m_btnLogout.gameObject.TrySetActive(!UIManager.IsBattle() && SdkManager.IsSupportFunction(SdkFuncConst.LOGOUT));
        if (SdkManager.SdkName == "RusSDK")
        {
            m_btnLogout.gameObject.TrySetActive(true);
        }
        m_btnScreen.gameObject.TrySetActive(Driver.m_platform == EnumPlatform.pc);

        UpdateParamsValue(m_userParamSetting);

        //var showCloseBtn = HasParams() && (bool)m_params[0];
        bool showCloseBtn = !UIManager.IsBattle();
        m_btnClose.gameObject.TrySetActive(showCloseBtn);
        var configIsShow = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent("ShowExitButton").ToLower() == "true";
        m_btnExit.gameObject.TrySetActive((showCloseBtn && configIsShow) || (showCloseBtn && SdkManager.SdkName == "RusSDK"));
        m_btnSend.gameObject.TrySetActive(showCloseBtn);
        m_btnQuit.TrySetActive(!showCloseBtn);
        m_btnReturn.TrySetActive(!showCloseBtn);
        if( WorldManager.singleton.GetParamSettingStatus(GameConst.PARAMSETTING_AREA_VISIBLE) == 1 )
        {
            m_showCityToggle.isOn = true;
            m_hideCityToggle.isOn = false;
        }
        else
        {
            m_showCityToggle.isOn = false;
            m_hideCityToggle.isOn = true;
        }
        if (FightVideoManager.isPlayFight == true)
        {
            FightVideoManager.PlaySpeed = 0;
            NetLayer.Send(new tos_record_set_speed { speed = FightVideoManager.PlaySpeed, by_center = FightVideoManager.ByCenter() });
        }
    }
    public override void OnHide()
    {
        SaveUserBattleParamSetting();
        if (FightVideoManager.isPlayFight == true)
        {
            FightVideoManager.PlaySpeed = 1;
            NetLayer.Send(new tos_record_set_speed { speed = FightVideoManager.PlaySpeed, by_center = FightVideoManager.ByCenter() });
        }
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        SaveUserBattleParamSetting();

        m_sliderSensity.onValueChanged.RemoveListener(OnSensitySliderValueChange);
        m_sliderAim.onValueChanged.RemoveListener(OnAimSliderValueChange);
        m_sliderAimAcc.onValueChanged.RemoveListener(OnAimAccSliderValueChange);
        m_sliderMusic.onValueChanged.RemoveListener(OnMusicSliderValueChange);
        m_sliderSound.onValueChanged.RemoveListener(OnSoundSliderValueChange);
        m_sliderFightSound.onValueChanged.RemoveListener(OnFightSoundSliderValueChange);
    }
    public override void Update()
    {
        if (movingTime >= 300)
        {
            movingTime = 0;
            upordown *= -1;
        }
        movingTime++;
        m_moveImage.transform.localPosition += new Vector3(0.15f, 0.15f, 0) * upordown;
    }


    void ReadUserBattleParamSetting()
    {
        m_userParamSetting.Load(m_defaultParamSetting);
        m_userParamSetting.m_dataArr = GlobalBattleParams.singleton.SaveArray();
    }

    void SaveUserBattleParamSetting()
    {

        string strSaveInfo = m_userParamSetting.Export();
        PlayerPrefs.SetString(SAVE_BATTLE_PARAM_KEY, WWW.EscapeURL(strSaveInfo));
        GameDispatcher.Dispatch(GameEvent.UI_SETTING_UPDATE);
    }


    void OnSensitySliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_SENSITY, curValue);
        int iValue = (int)(curValue * 100);
        m_sensityValue.text = iValue.ToString();
    }

    void OnAimSliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_AIM, curValue);
        int iValue = (int)(curValue * 100);
        m_aimValue.text = iValue.ToString();
    }

    void OnAimAccSliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_AIMACC, curValue);
        int iValue = (int)(curValue * 100);
        m_aimAccValue.text = iValue.ToString();
    }

    void OnMusicSliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_MUSIC, curValue);
        int iValue = (int)(curValue * 100);
        m_musicValue.text = iValue.ToString();
        AudioManager.MusicVolume = curValue;
    }

    void OnSoundSliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_SOUND, curValue);
        int iValue = (int)(curValue * 100);
        m_soundValue.text = iValue.ToString();
        AudioManager.SoundVolume = curValue;
    }

    void OnFightSoundSliderValueChange(float curValue)
    {
        SetUserParamValueByKey(GlobalBattleParams.KEY_FIGHT_SOUND, curValue);
        int iValue = (int)(curValue * 100);
        m_fightSoundValue.text = iValue.ToString();
        AudioManager.FightSoundVolume = curValue;
    }

    void OnClickToggle(GameObject target, PointerEventData eventData)
    {
        if (target.name == "drift")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 0.0f);
        }
        else if (target.name == "stable")
        {
            if (UIManager.IsBattle())
            {
                GameDispatcher.Dispatch(GameEvent.UI_CLICK_STABLE);
            }
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 1.0f);
        }

        if (target.name == "click")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_SQUAT, 0.0f);
        }
        else if (target.name == "press")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_SQUAT, 1.0f);
        }

        if (target.name == "double")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JUMP, 0.0f);

        }
        else if (target.name == "right")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JUMP, 1.0f);
        }

        if (target.name == "GrenadeOn")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_GRENADE, 1.0f);
        }
        else if (target.name == "GrenadeOff")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_GRENADE, 0.0f);
        }

        if (target.name == "LightOn")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_LIGHTGRENADE, 1.0f);
        }
        else if (target.name == "LightOff")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_LIGHTGRENADE, 0.0f);
        }

        if (target.name == "SwitchOn")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_WEAPON_SWITCH, 1.0f);
        }
        else if (target.name == "SwitchOff")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_WEAPON_SWITCH, 0.0f);
        }

        if (target.name == "AudioOn")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_BATTLEAUTOAUDIO, 1.0f);
        }
        else if (target.name == "AudioOff")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_BATTLEAUTOAUDIO, 0.0f);
        }

        if (target.name == "hand")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_BECOME_HERO, 0.0f);
        }
        else if (target.name == "auto")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_BECOME_HERO, 1.0f);
        }

        if (target.name == "leftHand")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_HOLD_HAND, 0.0f);
            GameDispatcher.Dispatch<int>(GameEvent.UI_BATTLEPARAM_SETTING_HAND_TYPE, GameConst.PLAYER_wEAPON_HOLD_HAND_LEFT);
        }
        else if (target.name == "rightHand")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_HOLD_HAND, 1.0f);
            GameDispatcher.Dispatch<int>(GameEvent.UI_BATTLEPARAM_SETTING_HAND_TYPE, GameConst.PLAYER_WEAPON_HOLD_HADN_RIGHT);
        }

        if (target.name == "handChangeMatrix")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_CHANGE_MATRIX, 0.0f);
        }
        else if (target.name == "autoChangeMatrix")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_CHANGE_MATRIX, 1.0f);
        }

        if (target.name == "crosshairsPoint")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_CROSS_TYPE, GameConst.CROSS_POINT);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_CROSS_TYPE, GameConst.CROSS_POINT);
        }
        else if (target.name == "crosshairs")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_CROSS_TYPE, GameConst.CROSS_NO_POION);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_CROSS_TYPE, GameConst.CROSS_NO_POION);
        }
        else if (target.name == "circle")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_CROSS_TYPE, GameConst.CROSS_CIRCLE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_CROSS_TYPE, GameConst.CROSS_CIRCLE);
        }

        if (target.name == "Joystick1")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JOYSTICK_TYPE, GameConst.JOYSTICK1);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE, GameConst.JOYSTICK1);
        }
        else if (target.name == "Joystick2")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JOYSTICK_TYPE, GameConst.JOYSTICK2);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE, GameConst.JOYSTICK2);
        }
        else if (target.name == "Joystick3")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JOYSTICK_TYPE, GameConst.JOYSTICK3);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE, GameConst.JOYSTICK3);
        }
       //设置摇杆移动类型
        if (target.name == "JoystickMove1")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE, GameConst.JOYSTICK_MOVE1);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE, GameConst.JOYSTICK_MOVE1);
        }
        else if (target.name == "JoystickMove2")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE, GameConst.JOYSTICK_MOVE2);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE, GameConst.JOYSTICK_MOVE2);
        }
        //设置重刀
        if (target.name == "normalKnife")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_HEAVYKNIFE, GameConst.NORMAL_HEAVYKNIFE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE, GameConst.NORMAL_HEAVYKNIFE);
        }
        else if (target.name == "changeHeavy")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_HEAVYKNIFE, GameConst.CHANGE_HEAVY_KNIFE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE, GameConst.CHANGE_HEAVY_KNIFE);
        }

        if (target.name == "OpenKnifeAutoAim")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_AUTOAIM, GameConst.AUTOAIM_OPEN);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_AUTOAIM, GameConst.AUTOAIM_OPEN);
            if (WorldManager.singleton != null && RoomModel.RoomInfo != null)
            {
                AutoAim.Enable = RoomModel.RoomInfo.auto_aim && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
            }
        }
        else if (target.name == "CloseKnifeAutoAim")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_AUTOAIM, GameConst.AUTOAIM_CLOSE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_AUTOAIM, GameConst.AUTOAIM_CLOSE);
            if (WorldManager.singleton != null && RoomModel.RoomInfo != null)
            {
                AutoAim.Enable = RoomModel.RoomInfo.auto_aim && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
            }
        }

        if (target.name == "OpenZombieAutoAim")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_ZOMBIE_AUTOAIM, GameConst.ZOMBIE_AUTOAIM_OPEN);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_ZOMBIE_AUTOAIM, GameConst.ZOMBIE_AUTOAIM_OPEN);
        }
        else if (target.name == "CloseZombieAutoAim")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_ZOMBIE_AUTOAIM, GameConst.ZOMBIE_AUTOAIM_CLOSE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_ZOMBIE_AUTOAIM, GameConst.ZOMBIE_AUTOAIM_CLOSE);
        }
        //设置作战指示
        if (target.name == m_FightDirectionTopToggle.name)
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIGHT_DIRECTION, GameConst.FIGHTDIRECTIONBTN_TOP);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION, GameConst.FIGHTDIRECTIONBTN_TOP);
        }
        else if (target.name == m_FightDirectionBottomToggle.name)
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIGHT_DIRECTION, GameConst.FIGHTDIRECTIONBTN_BOTTOM);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION, GameConst.FIGHTDIRECTIONBTN_BOTTOM);
        }
        else if (target.name == m_FightDirectionHideToggle.name)
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIGHT_DIRECTION, GameConst.FIGHTDIRECTIONBTN_HIDE);
            GameDispatcher.Dispatch<float>(GameEvent.UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION, GameConst.FIGHTDIRECTIONBTN_HIDE);
        }
        return;
    }

    private void OnClickVisibleCityToggle(GameObject target, PointerEventData eventData)
    {
        if (target.name == "show")
        {
            NetLayer.Send(new tos_player_set_option() { option_type = GameConst.PARAMSETTING_AREA_VISIBLE, option_val = 1 });
        }
        else if (target.name == "hide")
        {
            NetLayer.Send(new tos_player_set_option() { option_type = GameConst.PARAMSETTING_AREA_VISIBLE, option_val = 0 });
        }
    }


    void OnClickRestoreDefaultSetting(GameObject target, PointerEventData eventData)
    {
        m_userParamSetting.Load(m_defaultParamSetting);
        UpdateParamsValue(m_userParamSetting);
        return;
    }

    void OnClickUserSetting(GameObject target, PointerEventData eventData)
    {
        //临时的恶心处理方案（打开PanelUserDefine需要先初始化PanelBattle里面的摇杆）
        if (UIManager.HasPanel<PanelBattle>())
        {
            UIManager.PopChatPanel<PanelUserDefine>();
        }
        else
        {
            UIManager.PopChatPanel<PanelBattle>(new object[] { "link_to_PanelUserDefine" });
        }
    }



    void BuildKeyToIndex(ConfigBattleParamSetting source, Dictionary<string, int> to)
    {
        for (int index = 0; index < source.m_dataArr.Length; ++index)
        {
            to[source.m_dataArr[index].Key] = index;
        }
    }

    ConfigBattleParamSettingLine GetParamByKey(ConfigBattleParamSetting source, string key)
    {
        int index = -1;
        if (source == m_userParamSetting)
        {
            index = m_dicUserKeyToIndex[key];
        }
        else if (source == m_defaultParamSetting)
        {
            index = m_dicDefaultKeyToIndex[key];
        }

        if (index >= 0 && index < m_userParamSetting.m_dataArr.Length)
        {
            return source.m_dataArr[index];
        }

        return null;
    }

    void SetUserParamValueByKey(string key, float newValue)
    {
        int index = m_dicUserKeyToIndex[key];

        if (index >= m_userParamSetting.m_dataArr.Length)
        {
            return;
        }

        ConfigBattleParamSettingLine lineInfo = m_userParamSetting.m_dataArr[index];
        lineInfo.value = newValue;

        GlobalBattleParams.singleton.SetBattleParam(key, newValue);
    }

    void UpdateParamsValue(ConfigBattleParamSetting source)
    {
        m_sliderSensity.value = GetParamByKey(source, GlobalBattleParams.KEY_SENSITY).value;
        int iTemp = (int)(m_sliderSensity.value * 100);
        m_sensityValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_SENSITY, m_sliderSensity.value);

        m_sliderAim.value = GetParamByKey(source, GlobalBattleParams.KEY_AIM).value;
        iTemp = (int)(m_sliderAim.value * 100);
        m_aimValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AIM, m_sliderAim.value);

        m_sliderAimAcc.value = GetParamByKey(source, GlobalBattleParams.KEY_AIMACC).value;
        iTemp = (int)(m_sliderAimAcc.value * 100);
        m_aimAccValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AIMACC, m_sliderAimAcc.value);

        m_sliderMusic.value = GetParamByKey(source, GlobalBattleParams.KEY_MUSIC).value;
        iTemp = (int)(m_sliderMusic.value * 100);
        m_musicValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_MUSIC, m_sliderMusic.value);

        m_sliderSound.value = GetParamByKey(source, GlobalBattleParams.KEY_SOUND).value;
        iTemp = (int)(m_sliderSound.value * 100);
        m_soundValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_SOUND, m_sliderSound.value);

        m_sliderFightSound.value = GetParamByKey(source, GlobalBattleParams.KEY_FIGHT_SOUND).value;
        iTemp = (int)(m_sliderFightSound.value * 100);
        m_fightSoundValue.text = iTemp.ToString();
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_FIGHT_SOUND, m_sliderFightSound.value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_FIRE).value == 0.0f)
        {
            m_driftFire.isOn = true;
            m_stockFire.isOn = false;
            m_moveCheck.TrySetActive(true);
            m_moveShop.GetComponent<Toggle>().isOn = true;
            m_nomoveCheck.TrySetActive(false);
            m_nomoveShop.GetComponent<Toggle>().isOn = false;
        }
        else
        {
            m_stockFire.isOn = true;
            m_driftFire.isOn = false;
            m_moveCheck.TrySetActive(false);
            m_moveShop.GetComponent<Toggle>().isOn = false;
            m_nomoveCheck.TrySetActive(true);
            m_nomoveShop.GetComponent<Toggle>().isOn = true;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_FIRE, GetParamByKey(source, GlobalBattleParams.KEY_FIRE).value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_SQUAT).value == 0.0f)
        {
            m_pressSquat.isOn = false;
            m_clickSquat.isOn = true;
        }
        else
        {
            m_pressSquat.isOn = true;
            m_clickSquat.isOn = false;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_SQUAT, GetParamByKey(source, GlobalBattleParams.KEY_SQUAT).value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_JUMP).value == 0.0f)
        {
            m_doubleJump.isOn = true;
            m_rightJump.isOn = false;
        }
        else
        {
            m_rightJump.isOn = true;
            m_doubleJump.isOn = false;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_JUMP, GetParamByKey(source, GlobalBattleParams.KEY_JUMP).value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_GRENADE).value == 0.0f)
        {
            m_grenadeSwitchOff.isOn = true;
            m_grenadeSwitchOn.isOn = false;
        }
        else
        {
            m_grenadeSwitchOn.isOn = true;
            m_grenadeSwitchOff.isOn = false;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_GRENADE, GetParamByKey(source, GlobalBattleParams.KEY_GRENADE).value);

        //if (GetParamByKey(source, GlobalBattleParams.KEY_LIGHTGRENADE).value == 0.0f)
        //{
        //    m_lightGrenadeSwitchOff.isOn = true;

        //}
        //else
        //{
        //    m_lightGrenadeSwitchOn.isOn = true;
        //}
        //GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_LIGHTGRENADE, GetParamByKey(source, GlobalBattleParams.KEY_LIGHTGRENADE).value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_WEAPON_SWITCH).value == 0.0f)
        {
            m_weaponSwitchOff.isOn = true;
            m_weaponSwitchOn.isOn = false;
        }
        else
        {
            m_weaponSwitchOn.isOn = true;
            m_weaponSwitchOff.isOn = false;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_WEAPON_SWITCH, GetParamByKey(source, GlobalBattleParams.KEY_WEAPON_SWITCH).value);

        if (GetParamByKey(source, GlobalBattleParams.KEY_BATTLEAUTOAUDIO).value == 0.0f)
        {
            m_battleAutoAudioSwitchOff.isOn = true;
            m_battleAutoAudioSwitchOn.isOn = false;
        }
        else
        {
            m_battleAutoAudioSwitchOn.isOn = true;
            m_battleAutoAudioSwitchOff.isOn = false;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_BATTLEAUTOAUDIO, GetParamByKey(source, GlobalBattleParams.KEY_BATTLEAUTOAUDIO).value);

        //设置变身英雄
        if (GetParamByKey(source, GlobalBattleParams.KEY_BECOME_HERO).value == 0.0f)
        {
            m_BecomeHeroHand.isOn = true;
            m_BecomeHeroAuto.isOn = false;

        }
        else
        {
            m_BecomeHeroAuto.isOn = true;
            m_BecomeHeroHand.isOn = false;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_BECOME_HERO, GetParamByKey(source, GlobalBattleParams.KEY_BECOME_HERO).value);

        //设置持枪手
        if (GetParamByKey(source, GlobalBattleParams.KEY_HOLD_HAND).value == 0.0f)
        {
            m_leftHandToggle.isOn = true;
            m_rightHandToggle.isOn = false;

        }
        else
        {
            m_rightHandToggle.isOn = true;
            m_leftHandToggle.isOn = false;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_HOLD_HAND, GetParamByKey(source, GlobalBattleParams.KEY_HOLD_HAND).value);

        //设置变身母体
        if (GetParamByKey(source, GlobalBattleParams.KEY_CHANGE_MATRIX).value == 0.0f)
        {
            m_ChangeMatrixHand.isOn = true;
            m_ChangeMatrixAuto.isOn = false;
        }
        else
        {
            m_ChangeMatrixHand.isOn = false;
            m_ChangeMatrixAuto.isOn = true;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_CHANGE_MATRIX, GetParamByKey(source, GlobalBattleParams.KEY_CHANGE_MATRIX).value);

        //设置准星类型
        if (GetParamByKey(source, GlobalBattleParams.KEY_CROSS_TYPE).value == GameConst.CROSS_POINT)
        {
            m_crossPointToggle.isOn = true;
            m_crossToggle.isOn = false;
            m_circleCrossToggle.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_CROSS_TYPE).value == GameConst.CROSS_NO_POION)
        {
          
            m_crossPointToggle.isOn = false;
            m_crossToggle.isOn = true;
            m_circleCrossToggle.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_CROSS_TYPE).value == GameConst.CROSS_CIRCLE)
        {
            m_circleCrossToggle.isOn = true;
            m_crossPointToggle.isOn = false;
            m_crossToggle.isOn = false;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_CROSS_TYPE, GetParamByKey(source, GlobalBattleParams.KEY_CROSS_TYPE).value);

        //设置摇杆类型
        if (GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_TYPE).value == GameConst.JOYSTICK1)
        {
            m_JoystickToggle1.isOn = true;
            m_JoystickToggle2.isOn = false;
            m_JoystickToggle3.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_TYPE).value == GameConst.JOYSTICK2)
        {
            m_JoystickToggle1.isOn = false;
            m_JoystickToggle2.isOn = true;
            m_JoystickToggle3.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_TYPE).value == GameConst.JOYSTICK3)
        {
            m_JoystickToggle1.isOn = false;
            m_JoystickToggle2.isOn = false;
            m_JoystickToggle3.isOn = true;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_JOYSTICK_TYPE, GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_TYPE).value);

        //设置摇杆移动类型
        if (GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE).value == GameConst.JOYSTICK_MOVE1)
        {
            m_JoystickMoveToggle1.isOn = true;
            m_JoystickMoveToggle2.isOn = false;

        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE).value == GameConst.JOYSTICK_MOVE2)
        {
            m_JoystickMoveToggle1.isOn = false;
            m_JoystickMoveToggle2.isOn = true;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE, GetParamByKey(source, GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE).value);

        //设置重刀
        if (GetParamByKey(source, GlobalBattleParams.KEY_HEAVYKNIFE).value == GameConst.NORMAL_HEAVYKNIFE)
        {
            m_NormalKnifeToggle.isOn = true;
            m_ChangeHeavyKnifeToggle.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_HEAVYKNIFE).value == GameConst.CHANGE_HEAVY_KNIFE)
        {
            m_NormalKnifeToggle.isOn = false;
            m_ChangeHeavyKnifeToggle.isOn = true;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_HEAVYKNIFE, GetParamByKey(source, GlobalBattleParams.KEY_HEAVYKNIFE).value);

        //设自瞄
        if (GetParamByKey(source, GlobalBattleParams.KEY_AUTOAIM).value == GameConst.AUTOAIM_OPEN)
        {
            m_KnifeAutoAimOpenToggle.isOn = true;
            m_KnifeAutoAimCloseToggle.isOn = false;

        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_AUTOAIM).value == GameConst.AUTOAIM_CLOSE)
        {
            m_KnifeAutoAimCloseToggle.isOn = true;
            m_KnifeAutoAimOpenToggle.isOn = false;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AUTOAIM, GetParamByKey(source, GlobalBattleParams.KEY_AUTOAIM).value);

        //设置僵尸自瞄
        if (GetParamByKey(source, GlobalBattleParams.KEY_ZOMBIE_AUTOAIM).value == GameConst.ZOMBIE_AUTOAIM_OPEN)
        {
            m_ZombieAutoAimOpenToggle.isOn = true;
            m_ZombieAutoAimCloseToggle.isOn = false;

        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_ZOMBIE_AUTOAIM).value == GameConst.ZOMBIE_AUTOAIM_CLOSE)
        {
            m_ZombieAutoAimCloseToggle.isOn = true;
            m_ZombieAutoAimOpenToggle.isOn = false;
        }

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_ZOMBIE_AUTOAIM, GetParamByKey(source, GlobalBattleParams.KEY_ZOMBIE_AUTOAIM).value);

        //设置作战指示按钮位置
        if (GetParamByKey(source, GlobalBattleParams.KEY_FIGHT_DIRECTION).value == GameConst.FIGHTDIRECTIONBTN_TOP)
        {
            m_FightDirectionTopToggle.isOn = true;
            m_FightDirectionBottomToggle.isOn = false;
            m_FightDirectionHideToggle.isOn = false;

        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_FIGHT_DIRECTION).value == GameConst.FIGHTDIRECTIONBTN_BOTTOM)
        {
            m_FightDirectionTopToggle.isOn = false;
            m_FightDirectionBottomToggle.isOn = true;
            m_FightDirectionHideToggle.isOn = false;
        }
        else if (GetParamByKey(source, GlobalBattleParams.KEY_FIGHT_DIRECTION).value == GameConst.FIGHTDIRECTIONBTN_HIDE)
        {
            m_FightDirectionTopToggle.isOn = false;
            m_FightDirectionBottomToggle.isOn = false;
            m_FightDirectionHideToggle.isOn = true;
        }
        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_FIGHT_DIRECTION, GetParamByKey(source, GlobalBattleParams.KEY_FIGHT_DIRECTION).value);
    
    }

    void Quit()
    {
        Application.Quit();
    }
}
