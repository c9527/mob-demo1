﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelTransparentTip : BasePanel
{
    private Text title;
    private Text text0;
    private Text text1;
    private Text text2;

    private Text textBtnL;
    private Text textBtnR;

    private GameObject goBtnL;
    private GameObject goBtnR;

    private GameObject goGBG;
    private GameObject goRBG;

    private int type;//1:进行投票 2:投票结果

    private float _end_time = -1;
    private float _last_time = 0;

    public PanelTransparentTip()
    {
        SetPanelPrefabPath("UI/Setting/PanelTransparentTip");

        AddPreLoadRes("UI/Setting/PanelTransparentTip", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public override void Init()
    {
        title = m_tran.Find("Title").GetComponent<Text>();
        text0 = m_tran.Find("Text0").GetComponent<Text>();
        text1 = m_tran.Find("Text1").GetComponent<Text>();
        text2 = m_tran.Find("Text2").GetComponent<Text>();

        goBtnL = m_tran.Find("ButtonL").gameObject;
        goBtnR = m_tran.Find("ButtonR").gameObject;

        goGBG = m_tran.Find("GBG").gameObject;
        goRBG = m_tran.Find("RBG").gameObject;

        textBtnL = m_tran.Find("ButtonL/Text").GetComponent<Text>();
        textBtnR = m_tran.Find("ButtonR/Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(goBtnL.transform).onPointerClick += OnLeftBtnClick;
        UGUIClickHandler.Get(goBtnR.transform).onPointerClick += OnRightBtnClick;
    }

    private void OnLeftBtnClick(GameObject target, PointerEventData eventData)
    {
        if (type == 1)
        {
            NetLayer.Send(new tos_fight_vote_kick() { pro_or_con = 1 });
        }
        HidePanel();
    }

    private void OnRightBtnClick(GameObject target, PointerEventData eventData)
    {
        if (type == 1)
        {
            NetLayer.Send(new tos_fight_vote_kick() { pro_or_con = 2 });
        }
        HidePanel();
    }
    
    public override void OnShow()
    {
        if (HasParams())
        {
            type = (int)m_params[0];
            if (type == 1)//开始投票
            {
                goGBG.TrySetActive(false);
                goRBG.TrySetActive(false);
                goBtnL.TrySetActive(true);

                text1.text = string.Empty;
                text2.text = string.Empty;
                text0.text = (string)m_params[1];

                textBtnL.text = "同意";
                textBtnR.text = "反对";
            }
            else if(type == 2)//投票结果
            {
                goGBG.TrySetActive(true);
                goRBG.TrySetActive(true);
                goBtnL.TrySetActive(false);

                text0.text = (string)m_params[1];
                text1.text = (string)m_params[2];

                text2.text = string.Empty;
                textBtnL.text = string.Empty;
                textBtnR.text = "确定";

                //setCD(15);
            }
        }
    }

    public override void OnHide()
    {
        
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {
        //if (_end_time == -1)
        //{
        //    return;
        //}
        //if ((Time.time - _last_time) >= 1)
        //{
        //    _last_time = Time.time;
        //    text2.text = string.Format("{0}秒 后自动退出游戏", _end_time > 0 ? TimeUtil.FormatTime((int)(_end_time - Time.time), 0) : "0");
        //    if (_last_time >= _end_time)
        //    {
        //        _end_time = -1;
        //        _last_time = 0;
        //    }
        //}
    }

    public void setCD(int value)
    {
        _end_time = value < 0 ? -1 : Time.realtimeSinceStartup + value;
        _last_time = 0;
    }
}
