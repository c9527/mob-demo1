﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelBattleKickReason : BasePanel
{
    public string[] m_battlereason;
    public string[] m_masterreason;
    public Text[] m_text;
    public int m_mode;
    public long m_id;//被踢的人的id
    public bool m_isMaster;
    public Text m_title;
    public PanelBattleKickReason()
    {
        SetPanelPrefabPath("UI/Setting/PanelBattleKickReason");

        AddPreLoadRes("UI/Setting/PanelBattleKickReason", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public override void Init()
    {
        m_title = m_tran.Find("Text").GetComponent<Text>();
        m_battlereason = new string[4];
        m_battlereason[0] = "使用非法程序";
        m_battlereason[1] = "辱骂玩家";
        m_battlereason[2] = "长时间不操作";
        m_battlereason[3] = "恶意利用游戏漏洞";
        m_masterreason = new string[4];
        m_masterreason[0] = "长时间不登陆";
        m_masterreason[1] = "师徒关系不好";
        m_masterreason[2] = "我心另有所属";
        m_masterreason[3] = "不想说什么";
        m_text = new Text[4];
        for (int i = 0; i < 4; i++)
        {
            m_text[i] = m_tran.Find(("Button" + i + "/Text")).GetComponent<Text>();
        }

    }

    //战斗内踢人模式 0
    //师徒踢人模式 1
    public void ChangeMode(int mode)
    {
        m_mode = mode;
        if (mode == 0)
        {
            m_title.text = "请选择强制踢人的理由";
            for (int i = 0; i < 4; i++)
            {
                m_text[i].text = m_battlereason[i];
            }
        }
        else if (mode == 1)
        {
            m_title.text = "请选择解除关系的理由";
            for (int i = 0; i < 4; i++)
            {
                m_text[i].text = m_masterreason[i];
            }
        }
    }


    public override void InitEvent()
    {
        for (int i = 0; i < 4; i++)
        {
            UGUIClickHandler.Get(m_tran.Find(("Button" + i))).onPointerClick += OnKickOutVote;
        }
        UGUIClickHandler.Get(m_tran.Find("Close")).onPointerClick += delegate { HidePanel(); };
    }

    private void OnKickOutVote(GameObject target, PointerEventData eventData)
    {
        if (m_mode == 0)
        {
            if (target == null) return;
            string text = target.GetComponentInChildren<Text>().text;
            //Logger.Error("OnKickOutSb, name; "+target.name+", text: "+text);
            if (HasParams())
            {
                Battle_Record rec = m_params[0] as Battle_Record;
                NetLayer.Send(new tos_fight_begin_vote_kick() { kicked_player_id = rec.m_iPlayerId, reason = text });
                UIManager.GetPanel<PanelBattleRecord>().ShowVoteBtn(false);
                HidePanel();
            }
        }
        else if (m_mode == 1)
        {
            if (target == null) return;
            string text = target.GetComponentInChildren<Text>().text;
            NetLayer.Send(new tos_master_kick_master() { id = m_id, kick_master = m_isMaster, reason = text });
            HidePanel();
        }
    }
    //修改显示模式，true显示对战模式，false 显示师徒模式

    public override void OnShow()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        Screen.lockCursor = false;
#endif
        if (HasParams())
        {
            Battle_Record rec = m_params[0] as Battle_Record;
            if (rec != null)
            {
                m_mode = 0;
                ChangeMode(m_mode);
            }
            else
            {
                m_mode = 1;
                var a = m_params[0] as p_master_base_info_toc;
                m_isMaster = (bool)m_params[1];
                ChangeMode(m_mode);
                m_id = a.id;
            }
        }
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (UIManager.IsBattle())
            Screen.lockCursor = true;
#endif
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {

    }
}
