﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;



public class PanelAdvice : BasePanel
{
    private InputField m_adviceInput;
    private Text QQText;
    private int m_type;

    public PanelAdvice()
    {
        SetPanelPrefabPath("UI/Setting/PanelAdvice");
        AddPreLoadRes("UI/Setting/PanelAdvice", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public override void Init()
    {
        m_adviceInput = m_tran.Find("InputField").GetComponent<InputField>();
        m_adviceInput.gameObject.AddComponent<InputFieldFix>();
        m_type = 11;
        QQText = m_tran.Find("QQgroup").GetComponent<Text>();
        QQText.text = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.CUSTOMER_SERVICE);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("typeGroup/bug")).onPointerClick += delegate { m_type = 11; };
        UGUIClickHandler.Get(m_tran.Find("typeGroup/advice")).onPointerClick += delegate { m_type = 13; };
        UGUIClickHandler.Get(m_tran.Find("typeGroup/other")).onPointerClick += delegate { m_type = 10; };
        UGUIClickHandler.Get(m_tran.Find("Send")).onPointerClick += SendText;
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };

    }

    void SendText( GameObject target,PointerEventData data)
    {
        if (m_adviceInput.text == "")
        {
            TipsManager.Instance.showTips("反馈内容不能为空");
        }
        else
        {
            NetLayer.Send(new tos_player_complain() { complain_type = m_type, content = m_adviceInput.text });
            TipsManager.Instance.showTips("反馈成功，感谢您的参与");
            m_adviceInput.text = "";
        }
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {

    }

    public override void Update()
    {

    }
}

