﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
public class PanelSetting : BasePanel
{

    GameObject m_goQuit;
    GameObject m_goReturn;
    private GameObject m_btnBasic;
    private GameObject m_BtnSet;
    private GameObject m_btnKeySet;
    private GameObject m_btnShot;
    public PanelSetting()
    {
        SetPanelPrefabPath("UI/Setting/PanelSetting");
        AddPreLoadRes("UI/Setting/PanelSetting", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }
    public override void Init()
    {
        // m_goQuit = m_tran.Find("frame/quit").gameObject;
        // m_goReturn = m_tran.Find("frame/return").gameObject;
        m_btnBasic = m_tran.Find("tabBtn/TabBasic").gameObject;
        m_BtnSet = m_tran.Find("tabBtn/TabBtnSet").gameObject;
        m_btnKeySet = m_tran.Find("tabBtn/TabKeySet").gameObject;
        m_btnShot = m_tran.Find("tabBtn/TabShotSet").gameObject;
        m_btnKeySet.TrySetActive(false);
        
       

    }
    public override void InitEvent()
    {
        //UGUIClickHandler.Get(m_goQuit).onPointerClick += OnClickQuitBattle;
        //UGUIClickHandler.Get(m_goReturn).onPointerClick += OnClickBackBattle;
        UGUIClickHandler.Get(m_btnBasic).onPointerClick += onClickBtnBasicHandler;
        UGUIClickHandler.Get(m_BtnSet).onPointerClick += onClickBtnSetHandler;
        UGUIClickHandler.Get(m_btnKeySet).onPointerClick += onClickBtnKeySetHandler;
        UGUIClickHandler.Get(m_btnShot).onPointerClick += onClickBtnShotSetHandler;
    }

    private void onClickBtnShotSetHandler(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelPCKeySetting>())
        {
            UIManager.GetPanel<PanelPCKeySetting>().HidePanel();
        }
        if (UIManager.IsOpen<PanelParamSetting>())
        {
            UIManager.GetPanel<PanelParamSetting>().ShowShotTab();
        }
        else
        {
            UIManager.PopPanel<PanelParamSetting>(new object[] { true,1 });
        }
    }

    private void onClickBtnBasicHandler(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelPCKeySetting>())
        {
            UIManager.GetPanel<PanelPCKeySetting>().HidePanel();
        }
        if (UIManager.IsOpen<PanelParamSetting>())
        {
            UIManager.GetPanel<PanelParamSetting>().ShowTabContend(true);
        }
        else
        {
            UIManager.PopPanel<PanelParamSetting>(new object[] { true });
        }
    }


    private void onClickBtnSetHandler(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelPCKeySetting>())
        {
            UIManager.GetPanel<PanelPCKeySetting>().HidePanel();
        }
        if (UIManager.IsOpen<PanelParamSetting>())
        {
            UIManager.GetPanel<PanelParamSetting>().ShowTabContend(false);
        }
        else
        {
            UIManager.PopPanel<PanelParamSetting>(new object[] { false });
        }
        
    }
    private void onClickBtnKeySetHandler(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.roleData.level < PanelGuidePC.SHOW_LEVEL)
        {
            TipsManager.Instance.showTips(PanelGuidePC.SHOW_LEVEL + "级开启按键自定义功能");
            return;
        }
        if (UIManager.IsOpen<PanelParamSetting>())
        {
            UIManager.GetPanel<PanelParamSetting>().HidePanel();
        }
        UIManager.PopPanel<PanelPCKeySetting>();
    }

    public override void OnShow()
    {
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            m_btnKeySet.TrySetActive(true);
            if (PlayerSystem.roleData.level < PanelGuidePC.SHOW_LEVEL)
            {
                Util.SetGoGrayShader(m_btnKeySet, true);
                m_btnKeySet.GetComponent<Toggle>().enabled = false;
            }
            else
            {
                Util.SetGoGrayShader(m_btnKeySet, false);
                m_btnKeySet.GetComponent<Toggle>().enabled = true;
            }
        }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        Screen.lockCursor = false;
#endif
        //ShowSetting(true);
        m_btnBasic.GetComponent<Toggle>().isOn = true;
        UIManager.PopPanel<PanelParamSetting>(new object[]{true});
        GameDispatcher.Dispatch(GameEvent.UI_SETTING_OPEN);
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (UIManager.IsBattle())
            Screen.lockCursor = true;
#endif
        GameDispatcher.Dispatch(GameEvent.UI_SEETING_CLOSE);
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        GameDispatcher.Dispatch(GameEvent.UI_SEETING_CLOSE);

        // UGUIClickHandler.Get(m_goQuit).onPointerClick -= OnClickQuitBattle;
        //UGUIClickHandler.Get(m_goReturn).onPointerClick -= OnClickBackBattle;
    }
    public override void Update()
    {

    }



    void OnClickQuitBattle(GameObject target, PointerEventData eventData)
    {
        string tipstr = PlayerBattleModel.Instance.battle_info.channel_type == "match" ? "中途退出战斗，将扣除双倍积分，是否确认退出？" : "中途退出战斗，将无法获得奖励，是否确认退出？";
        string confirm = "强制退出";
        string cancel = "继续战斗";
        if (WorldManager.singleton.isViewBattleModel || PlayerBattleModel.Instance.battle_state == null || PlayerBattleModel.Instance.battle_state.state == GameConst.Fight_State_GameInit)
        {
            tipstr = "是否退出";
            confirm = "退出";
            cancel = "取消";
        }
        UIManager.ShowTipPanel(tipstr,
            delegate
            {
                if (RoomModel.IsGuideStage)
                    GuideManager.DestroyGuide();
                NetLayer.Send(new tos_room_leave());
                RoomModel.SetCustomRoomLeaveType(RoomLeaveType.Force);
                HidePanel();
                HideParamSetting();
                UIManager.GetPanel<PanelBattle>().ShowPanel();
            },
            delegate { OnClickBackBattle(target, eventData); },
            false, true, confirm, cancel);
    }


    public void OnClickBackBattle(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        HideParamSetting();


        UIManager.GetPanel<PanelBattle>().ShowPanel();

        if (!Driver.isMobilePlatform)
        {
            UIManager.fullScreen = true;
        }

        //PanelOperation panelOp = UIManager.GetPanel<PanelOperation>();
        //if( panelOp != null )
        //{
        //    panelOp.ShowPanel();
        //}
        //else
        //{
        //    UIManager.PopPanel<PanelOperation>();
        //}
    }



    void ShowSetting(bool bShow)
    {
        if (bShow)
        {
            UIManager.PopPanel<PanelParamSetting>();
            ShowPanel();
        }
        else
        {
            HideParamSetting();
        }
    }

    void HideParamSetting()
    {
        PanelParamSetting paraSetPanel = UIManager.GetPanel<PanelParamSetting>();
        if (paraSetPanel != null)
            paraSetPanel.HidePanel();
    }

}
