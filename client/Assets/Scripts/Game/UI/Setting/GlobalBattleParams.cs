﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class GlobalBattleParams
{
    static public GlobalBattleParams singleton;
    const string SAVE_BATTLE_PARAM_KEY = "user_set_battle_params";
    const string SAVE_BATTLE_PC_PARAM_KEY = "user_set_battle_pc_params";

    public const string KEY_SENSITY = "Sensity";
    public const string KEY_AIM = "AimSensity";
    public const string KEY_AIMACC = "AimAccTimes";
    public const string KEY_MUSIC = "Music";
    public const string KEY_SOUND = "Sound";
    public const string KEY_FIGHT_SOUND = "FightSound";
    public const string KEY_FIRE = "Fire";
    public const string KEY_SQUAT = "Squat";
    public const string KEY_JUMP = "Jump";
    public const string KEY_GRENADE = "Grenade";
    public const string KEY_LIGHTGRENADE = "LightGrenade";
    public const string KEY_WEAPON_SWITCH = "WeaponSwitch";
    public const string KEY_BATTLEAUTOAUDIO = "BattleAutoAudio";
    public const string KEY_MIC_SPEED = "MicSpeed";
    public const string KEY_ACCELTOR = "Acceltor";
    public const string KEY_MIC_XISHU = "MicXishu";
    public const string KEY_YC = "Yanchi";
    public const string KEY_SD = "Sudu";
    public const string KEY_JSD = "Jiasudu";
    //public const string KEY_SURVIVAL_AUTOFIRE = "Survial_auto_fire";
    public const string KEY_AUTO_FIRE = "AutoFire";
    public const string KEY_BECOME_HERO = "BecomeHero";
    public const string KEY_HOLD_HAND = "HoldHand";
    public const string KEY_CHANGE_MATRIX = "ChangeMatrix";//变身母体
    public const string KEY_CROSS_TYPE = "CrossType";//准星的形状
    public const string KEY_JOYSTICK_TYPE = "JoystickType";//摇杆的形状
    public const string KEY_JOYSTICK_MOVE_TYPE = "JoystickMoveType";//摇杆移动类型
    public const string KEY_HEAVYKNIFE = "HeavyKnife";//重刀类型
    public const string KEY_KNIFE_AUTOAIM = "KnifeAutoAim";   //刀战自瞄
    public const string KEY_ZOMBIE_AUTOAIM = "ZombieAutoAim"; //僵尸自瞄
    public const string KEY_AUTOAIM = "AutoAim";
    public const string KEY_FIGHT_DIRECTION = "FightDirection"; //作战指示

    Dictionary<string, float> m_dicBattleParams;

    public delegate void NotifyParamChange(string paramName, float newValue);

    public event NotifyParamChange NotifyEvent;

    /// <summary>
    /// PC版快捷键设置
    /// </summary>
    Dictionary<string, string> m_dicPCParams;

    public GlobalBattleParams()
    {
        if (singleton != null)
            Logger.Error("GlobalBattleParams is a singleton,but instance multi");
        singleton = this;

        Init();
    }

    void Init()
    {
        initPcKey();
        m_dicBattleParams = new Dictionary<string, float>();

        ConfigBattleParamSetting defaultParamSetting = ConfigManager.GetConfig<ConfigBattleParamSetting>();
        foreach (var line in defaultParamSetting.m_dataArr)
        {
            //Logger.Error("key: "+line.Key+", val: "+line.value);
            m_dicBattleParams[line.Key] = line.value;
        }

        ConfigBattleParamSetting userParamSetting = ScriptableObject.CreateInstance<ConfigBattleParamSetting>(); ;
        string strUIInfo = PlayerPrefs.GetString(SAVE_BATTLE_PARAM_KEY, "");
        if (strUIInfo == "")
            return;

        try
        {
            userParamSetting.Load(WWW.UnEscapeURL(strUIInfo));
        }
        catch
        {
            PlayerPrefs.DeleteKey(SAVE_BATTLE_PARAM_KEY);
            return;
        }

        foreach (var line in userParamSetting.m_dataArr)
        {
            if (m_dicBattleParams.ContainsKey(line.Key))
                m_dicBattleParams[line.Key] = line.value;
        }
    }


    private void initPcKey()
    {
        //pc web版本快捷键设置

        m_dicPCParams = new Dictionary<string, string>();
        ConfigBattlePcKeySetting defaultPCParamSetting = ConfigManager.GetConfig<ConfigBattlePcKeySetting>();
        foreach (var line in defaultPCParamSetting.m_dataArr)
        {
            //Logger.Error("key: "+line.Key+", val: "+line.value);
            m_dicPCParams[line.Key] = line.Value;
        }

        ConfigBattlePcKeySetting userPcParamSetting = ScriptableObject.CreateInstance<ConfigBattlePcKeySetting>(); ;
        string strPcUIInfo = PlayerPrefs.GetString(SAVE_BATTLE_PC_PARAM_KEY, "");
        if (strPcUIInfo == "")
            return;

        try
        {
            userPcParamSetting.Load(WWW.UnEscapeURL(strPcUIInfo));
        }
        catch
        {
            PlayerPrefs.DeleteKey(SAVE_BATTLE_PC_PARAM_KEY);
            return;
        }

        foreach (var line in userPcParamSetting.m_dataArr)
        {
            if (m_dicPCParams.ContainsKey(line.Key))
                m_dicPCParams[line.Key] = line.Value;
        }
    }

    /// <summary>
    /// 保存快捷键设置
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public void savePcKeyValue(string key, string value)
    {
        if (m_dicPCParams.ContainsKey(key))
        {
            m_dicPCParams[key] = value;
        }
    }

    /// <summary>
    /// 保存PC版的快捷键设置
    /// </summary>
    /// <returns></returns>
    public ConfigBattlePcKeySettingLine[] SavePCArray()
    {
        List<ConfigBattlePcKeySettingLine> list = new List<ConfigBattlePcKeySettingLine>();
        foreach (var pair in m_dicPCParams)
        {
            list.Add(new ConfigBattlePcKeySettingLine()
            {
                Key = pair.Key,
                Value = pair.Value,
            });
        }
        return list.ToArray();
    }

    /// <summary>
    /// 重置PC版的参数设置
    /// </summary>
    public void resetPcKeyParam()
    {
        ConfigBattlePcKeySetting defaultPCParamSetting = ConfigManager.GetConfig<ConfigBattlePcKeySetting>();
        if (defaultPCParamSetting != null)
            foreach (var line in defaultPCParamSetting.m_dataArr)
            {
                m_dicPCParams[line.Key] = line.Value;
            }
        savePcKeyToPlayerPrefs();
    }

    /// <summary>
    /// 保存PC版本设置
    /// </summary>
    public void savePcKeyToPlayerPrefs()
    {
        ConfigBattlePcKeySetting defaultPCParamSetting = ConfigManager.GetConfig<ConfigBattlePcKeySetting>();
        ConfigBattlePcKeySetting cfg = ScriptableObject.CreateInstance<ConfigBattlePcKeySetting>();
        cfg.Load(defaultPCParamSetting);
        cfg.m_dataArr = SavePCArray();
        string strSaveInfo = cfg.Export();
        PlayerPrefs.SetString(SAVE_BATTLE_PC_PARAM_KEY, WWW.EscapeURL(strSaveInfo));
    }

    /// <summary>
    /// 获取用自定义的键值
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string GetPcKeyParam(string key)
    {
        string param = "";
        if (m_dicPCParams.ContainsKey(key))
            param = m_dicPCParams[key];
        return param;
    }

    /// <summary>
    /// 获取PC快捷键信息
    /// </summary>
    /// <returns></returns>
    public string GetPcKeyTextByKey(string key)
    {
        return PcKeyToText(GetPcKeyParam(key));
    }

    /// <summary>
    /// 玩家是否有自定义PC快捷键
    /// </summary>
    /// <returns></returns>
    public bool IsPcKeyAutoDefine(string key)
    {
        ConfigBattlePcKeySettingLine pcl = ConfigManager.GetConfig<ConfigBattlePcKeySetting>().GetLine(key) as ConfigBattlePcKeySettingLine;
        if (pcl == null) return false;
        return pcl.Value == GetPcKeyParam(key);
    }

    /// <summary>
    /// 将配置中的字符串  转化显示为文本
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string PcKeyToText(string keyCode)
    {
        string str = keyCode;
        if (keyCode == "") return str;
        if (keyCode.IndexOf("Alpha") != -1)
        {
            str = keyCode.Substring(5);
        }
        else
        {
            if (str.Length == 1)
                str = keyCode.ToUpper();
            else
            {
                KeyCode kc = (KeyCode)Enum.Parse(typeof(KeyCode), keyCode);
                if (kc != null)
                    switch (kc)
                    {
                        case KeyCode.Minus:
                            str = "-";
                            break;
                        case KeyCode.Plus:
                            str = "=";
                            break;
                        case KeyCode.Backslash:
                            str = "\\";
                            break;
                        case KeyCode.LeftParen:
                            str = "[";
                            break;
                        case KeyCode.RightParen:
                            str = "]";
                            break;
                        case KeyCode.Semicolon:
                            str = ";";
                            break;
                        case KeyCode.Quote:
                            str = "'";
                            break;
                        case KeyCode.Comma:
                            str = ",";
                            break;
                        case KeyCode.Period:
                            str = ".";
                            break;
                        case KeyCode.Slash:
                            str = "/";
                            break;
                        case KeyCode.Mouse2:
                            str = "滚轮";
                            break;
                        case KeyCode.BackQuote:
                            str = "`";
                            break;
                        default:
                            break;
                    }
            }
        }
        return str;
    }

    /// <summary>
    /// 将字符串转化为配置的字符串
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string ToPcKey(string key)
    {
        string str = key;
        int num;
        if (str.Length == 1)
        {
            if (int.TryParse(str, out num))
            {
                str = "Alpha" + str;
            }
            else
            {
                switch (str)
                {
                    case "-":
                        str = KeyCode.Minus.ToString();
                        break;
                    case "=":
                        str = KeyCode.Plus.ToString();
                        break;
                    case "\\":
                        str = KeyCode.Backslash.ToString();
                        break;
                    case "[":
                        str = KeyCode.LeftParen.ToString();
                        break;
                    case "]":
                        str = KeyCode.RightParen.ToString();
                        break;
                    case ";":
                        str = KeyCode.Semicolon.ToString();
                        break;
                    case "'":
                        str = KeyCode.Quote.ToString();
                        break;
                    case ",":
                        str = KeyCode.Comma.ToString();
                        break;
                    case ".":
                        str = KeyCode.Period.ToString();
                        break;
                    case "/":
                        str = KeyCode.Slash.ToString();
                        break;
                    case "`":
                        str = KeyCode.BackQuote.ToString();
                        break;
                    default:
                        str = str.ToUpper();
                        break;
                }
            }
        }
        return str;
    }



    public float GetBattleParam(string key)
    {
        return m_dicBattleParams[key];
    }

    public void SetBattleParam(string key, float newValue)
    {
        if (key == KEY_MIC_SPEED || key == KEY_ACCELTOR || key == KEY_MIC_XISHU)
        {
            m_dicBattleParams[key] = newValue;
            return;
        }
        if (newValue < 0.0f || newValue > 1.0f)
            return;

        m_dicBattleParams[key] = newValue;

        if (NotifyEvent != null)
        {
            NotifyEvent(key, newValue);
        }
    }

    public ConfigBattleParamSettingLine[] SaveArray()
    {
        List<ConfigBattleParamSettingLine> list = new List<ConfigBattleParamSettingLine>();
        foreach (var pair in m_dicBattleParams)
        {
            list.Add(new ConfigBattleParamSettingLine()
            {
                Key = pair.Key,
                value = pair.Value,
            });
        }
        return list.ToArray();
    }
}

