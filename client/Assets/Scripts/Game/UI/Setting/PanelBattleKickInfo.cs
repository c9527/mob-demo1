﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelBattleKickInfo : BasePanel
{
    private GameObject goProBtn;
    private GameObject goConBtn;

    private Text KickedNameTxt;
    private Text KickedReasonTxt;
    private Text SponsorNameTxt;
    private Text ProTxt;
    private Text ConTxt;
    private Text SecondTxt;

    private Text ProBtnTxt;
    private Text ConBtnTxt;

    private float _end_time = -1;
    private float _last_time = 0;

    public PanelBattleKickInfo()
    {
        SetPanelPrefabPath("UI/Setting/PanelBattleKickInfo");

        AddPreLoadRes("UI/Setting/PanelBattleKickInfo", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public override void Init()
    {
        goProBtn = m_tran.Find("ProButton").gameObject;
        goConBtn = m_tran.Find("ConButton").gameObject;
        ProBtnTxt = goProBtn.transform.Find("Text").GetComponent<Text>();
        ConBtnTxt = goConBtn.transform.Find("Text").GetComponent<Text>();
        KickedNameTxt = m_tran.Find("Offset/KickedNameTxt").GetComponent<Text>();
        KickedReasonTxt = m_tran.Find("Offset/KickedReasonTxt").GetComponent<Text>();
        SponsorNameTxt = m_tran.Find("Offset/SponsorNameTxt").GetComponent<Text>();
        ProTxt = m_tran.Find("Offset/ProTxt").GetComponent<Text>();
        ConTxt = m_tran.Find("Offset/ConTxt").GetComponent<Text>();
        SecondTxt = m_tran.Find("Offset/SecondTxt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(goProBtn.transform).onPointerClick += OnPro;
        UGUIClickHandler.Get(goConBtn.transform).onPointerClick += OnCon;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener(GameEvent.INPUT_KEY_Y_DOWN, () =>
        {
            if (goProBtn.activeSelf)
            {
                OnPro(null, null);
            }
        });
        AddEventListener(GameEvent.INPUT_KEY_N_DOWN, () =>
        {
            if (goConBtn.activeSelf)
            {
                OnCon(null, null);
            }
        });
#endif
    }

    public override void OnShow()
    {
        bool showBtn = (bool)m_params[6];
        ShowBtn(showBtn);
        refresh();
    }

    public void refresh(object[] @params = null)
    {
        if (@params != null) SetParams(@params);
        KickedNameTxt.text = string.Concat("对象：", (string)m_params[0]);
        KickedReasonTxt.text = string.Concat("原因：", (string)m_params[1]);
        SponsorNameTxt.text = string.Concat("发起人：", (string)m_params[2]);

        ProTxt.text = string.Concat("同意", m_params[3]);
        ConTxt.text = string.Concat("反对", m_params[4]);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        ProBtnTxt.text = "同意Y";
        ConBtnTxt.text = "反对N";
#endif
        SecondTxt.text = string.Concat(m_params[5], "秒");
        setCD((int)m_params[5]);
    }

    private void OnPro(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_fight_vote_kick() { pro_or_con = 1 });
        ShowBtn(false);
    }

    private void OnCon(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_fight_vote_kick() { pro_or_con = 2 });
        ShowBtn(false);
    }

    private void ShowBtn(bool isShow)
    {
        goProBtn.TrySetActive(isShow);
        goConBtn.TrySetActive(isShow);
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {
        if (_end_time == -1)
        {
            return;
        }
        if ((Time.realtimeSinceStartup - _last_time) >= 1)
        {
            _last_time = Time.realtimeSinceStartup;
            SecondTxt.text = string.Format("{0}秒", _end_time > 0 ? TimeUtil.FormatTime((int)(_end_time - Time.realtimeSinceStartup), 0) : "0");
            if (_last_time >= _end_time)
            {
                _end_time = -1;
                _last_time = 0;
                HidePanel();
            }
        }
    }
    public void setCD(int value)
    {
        _end_time = value < 0 ? -1 : Time.realtimeSinceStartup + value;
        _last_time = 0;
    }
}
