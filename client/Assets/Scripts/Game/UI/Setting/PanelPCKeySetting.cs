﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/2/14 15:36:20
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelPCKeySetting : BasePanel
{
    public class PCKeySettingItem : ItemRender
    {
        public const string EVENT_SELECT_ITEM = "pckeysettingitem_selcect";
        private Text m_txtName;
        private Text m_txtKey;
        private Toggle m_toggle;
        private ConfigBattlePcKeySettingLine cfg;
        public string keyCode;
        public override void Awake()
        {
            m_txtKey = transform.Find("Btn_Set/Text").GetComponent<Text>();
            m_txtName = transform.Find("SettingName").GetComponent<Text>();
            m_toggle = transform.Find("Btn_Set").GetComponent<Toggle>();
            initEvent();
        }

        private void initEvent()
        {
            m_toggle.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnValueChanged(bool selected)
        {
            if (selected)
            {
                m_txtKey.text = "请设置";
                GameDispatcher.Dispatch<PCKeySettingItem>(EVENT_SELECT_ITEM, this);
            }
            else
            {
                m_txtKey.text = getKeyText();
            }
        }
        protected override void OnSetData(object data)
        {
            cfg = data as ConfigBattlePcKeySettingLine;
            if (cfg == null)
                return;
            m_txtName.text = cfg.Name;
            keyCode = GlobalBattleParams.singleton.GetPcKeyParam(cfg.Key);
            m_txtKey.text = getKeyText();
        }

        public void clearSelect()
        {
            if (m_toggle.isOn)
                m_toggle.isOn = false;
            m_txtKey.text = getKeyText();
        }

        private string getKeyText()
        {
            return GlobalBattleParams.singleton.PcKeyToText(keyCode);
        }

        public void setKey(string keyCodestr)
        {
            if (m_toggle.isOn)
                m_toggle.isOn = false;
            keyCode = keyCodestr;
            m_txtKey.text = getKeyText();
        }
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="str"></param>
        public void saveKey()
        {
            GlobalBattleParams.singleton.savePcKeyValue(cfg.Key, keyCode);
        }
    }


    private GameObject m_btnClose;
    private GameObject m_basicPanel;
    private GameObject m_item;
    private DataGrid m_basicGrid;

    private GameObject m_battlePanel;
    private DataGrid m_battleGrid;


    private GameObject m_scrollContent;
    private Image m_battleBg;

    private PCKeySettingItem m_curSelectItem;

    private bool m_anyKeyDown;

    private GameObject m_btnSave;
    private GameObject m_btnCancel;
    private GameObject m_btnDefault;

    private List<KeyCode> SPECIAL_KEY = new List<KeyCode>
    {
        KeyCode.Minus,KeyCode.Plus,KeyCode.Backslash,KeyCode.LeftParen,
        KeyCode.RightParen,KeyCode.Semicolon,KeyCode.Quote,KeyCode.Comma,
        KeyCode.Period,KeyCode.Slash,KeyCode.Mouse2,KeyCode.BackQuote,KeyCode.Space,KeyCode.Tab
    };


    public PanelPCKeySetting()
    {
        // 构造器
        SetPanelPrefabPath("UI/Setting/PanelPCKeySetting");
        AddPreLoadRes("UI/Setting/PanelPCKeySetting", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }
    public override void Init()
    {
        m_btnClose = m_tran.Find("btnClose").gameObject;
        m_scrollContent = m_tran.Find("frame/ScrollDataGrid/content").gameObject;
        m_basicPanel = m_tran.Find("frame/ScrollDataGrid/content/basic/Panel").gameObject;
        m_item = m_tran.Find("frame/ScrollDataGrid/Item").gameObject;
        m_basicGrid = m_basicPanel.AddComponent<DataGrid>();
        m_basicGrid.SetItemRender(m_item, typeof(PCKeySettingItem));

        m_battlePanel = m_tran.Find("frame/ScrollDataGrid/content/battle/Panel").gameObject;
        m_battleGrid = m_battlePanel.AddComponent<DataGrid>();
        m_battleGrid.SetItemRender(m_item, typeof(PCKeySettingItem));

        m_battleBg = m_tran.Find("frame/ScrollDataGrid/content/battle").GetComponent<Image>();

        m_btnSave = m_tran.Find("frame/BtnSave").gameObject;
        m_btnDefault = m_tran.Find("frame/BtnDefault").gameObject;
        m_btnCancel = m_tran.Find("frame/BtnCancel").gameObject;


    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += onClickBtnCloseHandler;
        AddEventListener<PCKeySettingItem>(PCKeySettingItem.EVENT_SELECT_ITEM, (item) =>
        {
            m_curSelectItem = item;
        });
        UGUIClickHandler.Get(m_go).onPointerClick += onClickPanelHandler;
        UGUIClickHandler.Get(m_btnSave).onPointerClick += onClickBtnSaveHandler;
        UGUIClickHandler.Get(m_btnDefault).onPointerClick += onClickBtnDefaultHandler;
        UGUIClickHandler.Get(m_btnCancel).onPointerClick += onClickBtnCancelHandler;

    }

    private void onClickBtnSaveHandler(GameObject target, PointerEventData eventData)
    {
        savePcKeyToDic();
        TipsManager.Instance.showTips("成功保存设置");
    }
    private void onClickBtnDefaultHandler(GameObject target, PointerEventData eventData)
    {
        GlobalBattleParams.singleton.resetPcKeyParam();
        showView();
    }
    private void onClickBtnCancelHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();

        if (UIManager.IsOpen<PanelSetting>())
        {
            UIManager.GetPanel<PanelSetting>().OnShow();
        }
        //UIManager.PopPanel<PanelParamSetting>();
    }

    private void onClickPanelHandler(GameObject target, PointerEventData eventData)
    {
        if (m_curSelectItem != null)
        {
            m_curSelectItem.clearSelect();
        }
    }

    private void onClickBtnCloseHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        PanelSetting paraSetPanel = UIManager.GetPanel<PanelSetting>();
        if (paraSetPanel != null)
            paraSetPanel.HidePanel();
    }

    public override void OnShow()
    {
        showView();
        m_btnClose.TrySetActive(!UIManager.IsBattle());
    }
    /// <summary>
    /// 将修改值保存到参数池里面
    /// </summary>
    private void savePcKeyToDic()
    {
        List<ItemRender> list = m_basicGrid.ItemRenders;
        PCKeySettingItem item;
        foreach (ItemRender temp in list)
        {
            item = temp as PCKeySettingItem;
            item.saveKey();
        }
        list = m_battleGrid.ItemRenders;
        foreach (ItemRender temp in list)
        {
            item = temp as PCKeySettingItem;
            item.saveKey();
        }
        GlobalBattleParams.singleton.savePcKeyToPlayerPrefs();
    }
    /// <summary>
    /// 判断当前快捷键是否被占用
    /// </summary>
    /// <param name="pcKey"></param>
    private PCKeySettingItem checkRepeatKey(string pcKey)
    {
        List<ItemRender> list = m_basicGrid.ItemRenders;
        PCKeySettingItem item;
        PCKeySettingItem aimItem = null;
        foreach (ItemRender temp in list)
        {
            item = temp as PCKeySettingItem;
            if (item.keyCode == pcKey)
            {
                aimItem = item;
                break;
            }
        }
        if (aimItem != null) return aimItem;
        list = m_battleGrid.ItemRenders;
        foreach (ItemRender temp in list)
        {
            item = temp as PCKeySettingItem;
            if (item.keyCode == pcKey)
            {
                aimItem = item;
                break;
            }
        }
        return aimItem;
    }

    private void setKeyAction(PCKeySettingItem item, KeyCode pcKey)
    {
        PCKeySettingItem checkItem = checkRepeatKey(pcKey.ToString());
        if (checkItem == null || checkItem == item)
        {
            item.setKey(pcKey.ToString());
        }
        else
        {
            string txtKey = GlobalBattleParams.singleton.PcKeyToText(pcKey.ToString());
            UIManager.ShowTipPanel(txtKey + "键已被占用，改键将清空已使用的键，是否继续？", () => { item.setKey(pcKey.ToString()); checkItem.setKey(""); }, () => item.clearSelect(), false,
                  true, "确定", "取消");
        }
        m_curSelectItem = null;
    }


    /// <summary>
    /// 显示面板
    /// </summary>
    private void showView()
    {
        ConfigBattlePcKeySetting pcKeyCfg = ConfigManager.GetConfig<ConfigBattlePcKeySetting>();
        List<ConfigBattlePcKeySettingLine> basicList = new List<ConfigBattlePcKeySettingLine>();
        List<ConfigBattlePcKeySettingLine> battleList = new List<ConfigBattlePcKeySettingLine>();
        if (pcKeyCfg.m_dataArr != null)
        {
            foreach (ConfigBattlePcKeySettingLine temp in pcKeyCfg.m_dataArr)
            {
                if (temp.Type == 0)
                {
                    basicList.Add(temp);
                }
                else
                {
                    battleList.Add(temp);
                }
            }
            m_basicGrid.Data = basicList.ToArray();
            m_battleGrid.Data = battleList.ToArray();
            int hight = 80 + (int)(48 * Mathf.Ceil(battleList.Count / 2f));
            if (hight < 188) hight = 188;
            m_battleBg.rectTransform.sizeDelta = new Vector2(747, (float)hight);
            m_scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(780, (float)hight + 188 + 5);
        }
    }





    public override void OnHide()
    {
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {
        if (m_curSelectItem != null)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                setKeyAction(m_curSelectItem, KeyCode.Mouse2);
            }
            else
                if (Input.anyKeyDown)
                {
                    m_anyKeyDown = true;
                    if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Mouse2))
                    {
                        m_anyKeyDown = false;
                    }
                    else if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
                    {
                        setKeyAction(m_curSelectItem, KeyCode.LeftShift);
                        m_anyKeyDown = false;
                    }
                    else if (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.RightAlt))
                    {
                        setKeyAction(m_curSelectItem, KeyCode.LeftAlt);
                        m_anyKeyDown = false;
                    }
                    else
                    {
                        KeyCode kc;
                        for (int num = 48; num <= 57; num++)
                        {
                            kc = (KeyCode)num;
                            if (kc != null)
                            {
                                if (Input.GetKeyDown(kc))
                                {
                                    setKeyAction(m_curSelectItem, kc);
                                    m_anyKeyDown = false;
                                    break;
                                }
                            }
                        }
                        //先判断字母是否按下
                        if (m_anyKeyDown)
                            for (int i = 97; i <= 122; i++)
                            {
                                kc = (KeyCode)i;
                                if (kc != null)
                                {
                                    if (Input.GetKeyDown(kc))
                                    {
                                        setKeyAction(m_curSelectItem, kc);
                                        m_anyKeyDown = false;
                                        break;
                                    }
                                }
                            }
                        //判断特殊键是否按下
                        if (m_anyKeyDown)
                            foreach (KeyCode temp in SPECIAL_KEY)
                            {
                                if (Input.GetKeyDown(temp))
                                {
                                    setKeyAction(m_curSelectItem, temp);
                                    m_anyKeyDown = false;
                                    break;
                                }
                            }
                    }

                    if (m_anyKeyDown)
                    {
                        TipsManager.Instance.showTips("无法使用该键位");
                        m_curSelectItem.clearSelect();
                        m_curSelectItem = null;
                    }
                }
        }
    }
}
