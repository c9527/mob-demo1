﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PanelUserDefine : BasePanel
{

    private ConfigBattleUIControlPos m_defaultControlPosInfo;
    const string SAVE_BATTLE_UI_POS_KEY = "user_set_battle_ui_pos_03";

    static Dictionary<string, GameObject> m_dragCloneObjs = new Dictionary<string, GameObject>();
    
    private ConfigBattleUIControlPos m_userSetControlPosInfo;

    private GameObject m_btnConfirm;
    private GameObject m_btnReset;

    public PanelUserDefine()
    {
        SetPanelPrefabPath("UI/Setting/PanelUserDefine");
        AddPreLoadRes("UI/Setting/PanelUserDefine", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Weapon", EResType.Atlas);
    }


    public override void Init()
    {
        m_btnConfirm = m_tran.Find("comfirm").gameObject;
        m_btnReset = m_tran.Find("reset").gameObject;

        m_defaultControlPosInfo = ConfigManager.GetConfig<ConfigBattleUIControlPos>();
        m_userSetControlPosInfo = ScriptableObject.CreateInstance<ConfigBattleUIControlPos>();

        ReadFromPlayerPrefs();


    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnConfirm).onPointerClick += OnClickConfirm;
        UGUIClickHandler.Get(m_btnReset).onPointerClick += OnClickReset;
    }
    public override void OnShow()
    {
        DrawEditorBk();
        UpdateCloneSetUIPos();
        //SpecialCloneSprite();
        
    }
    public override void OnHide()
    {
        
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_btnConfirm).onPointerClick -= OnClickConfirm;
        UGUIClickHandler.Get(m_btnReset).onPointerClick -= OnClickReset;
    }
    public override void Update()
    {

    }


    public static void AddCanDragElements(string name, GameObject go)
    {
        if (m_dragCloneObjs.ContainsKey(name) && m_dragCloneObjs[name] != null)
            return;
        GameObject cloneObj = UIHelper.Instantiate(go) as GameObject;

        UIDragable dragable = cloneObj.AddComponent<UIDragable>();

        ConfigBattleUIControlPos configControlPos = ConfigManager.GetConfig<ConfigBattleUIControlPos>();
        ConfigBattleUIControlPosLine line = configControlPos.GetLine(name);
        dragable.minLocalScale = line.minLocalScale;
        dragable.maxLocalScale = line.maxLocalScale;

        cloneObj.TrySetActive(false);
        m_dragCloneObjs[name] = cloneObj;

        UGUIClickHandler.Get(cloneObj).onPointerClick += OnCanDragObjSelect;
    }

    static void OnCanDragObjSelect( GameObject target, PointerEventData eventData )
    {
        UIDragable dragableCompment = target.GetComponent<UIDragable>();
        dragableCompment.selected = true;
        foreach( GameObject obj in m_dragCloneObjs.Values )
        {
            if( obj != target )
            {
                obj.GetComponent<UIDragable>().selected = false;
            }
        }
        
    }

    void ReadFromPlayerPrefs()
    {
        string strUIInfo = PlayerPrefs.GetString(SAVE_BATTLE_UI_POS_KEY, "");
        bool hasError = false;
        if (strUIInfo != "")
        {
            try
            {
                m_userSetControlPosInfo.Load(WWW.UnEscapeURL(strUIInfo));
            }
            catch
            {
                hasError = true;
                PlayerPrefs.DeleteKey(SAVE_BATTLE_UI_POS_KEY);
            }
        }

        if (hasError || strUIInfo == "")
        {
            m_userSetControlPosInfo.Load(m_defaultControlPosInfo);
        }

    }


    void UpdateCloneSetUIPos()
    {
     
        foreach (ConfigBattleUIControlPosLine kObjPosInfo in m_defaultControlPosInfo.m_dataArr)
        {
            GameObject control = m_dragCloneObjs[kObjPosInfo.key];
            control.TrySetActive(true);
            control.transform.SetParent(m_go.transform, false);
            ConfigBattleUIControlPosLine userLine = m_userSetControlPosInfo.GetLine(kObjPosInfo.key);
            if (userLine != null)
            {
                SetUIControlPos(control, userLine);
            }
            else
            {
                SetUIControlPos(control, kObjPosInfo);
            }
                
        }
        
    }

    void SetUIControlPos(GameObject obj, ConfigBattleUIControlPosLine info)
    {
        RectTransform goRectTransform = obj.GetComponent<RectTransform>();
        Vector2 anchor = goRectTransform.anchorMin;
        anchor.x = info.fAnchorMinX;
        anchor.y = info.fAnchorMinY;
        goRectTransform.anchorMin = anchor;

        anchor = goRectTransform.anchorMax;
        anchor.x = info.fAnchorMaxX;
        anchor.y = info.fAnchorMaxY;
        goRectTransform.anchorMax = anchor;

        Vector2 sizeData = goRectTransform.sizeDelta;
        sizeData.x = info.fWidth;
        sizeData.y = info.fHeight;
        goRectTransform.sizeDelta = sizeData;

        Vector3 ScaleData = goRectTransform.localScale;
        ScaleData.x = info.fLocalScale;
        ScaleData.y = info.fLocalScale;
        ScaleData.z = info.fLocalScale;
        goRectTransform.localScale = ScaleData;

        Vector2 anchorPos = goRectTransform.anchoredPosition;
        anchorPos.x = info.anchorX;
        anchorPos.y = info.anchorY;
        goRectTransform.anchoredPosition = anchorPos;

        return;
    }


    void DrawEditorBk()
    {
        Transform editorBkTransform = m_go.transform.Find("editor_bk_obj");
        if (editorBkTransform == null)
        {
            GameObject editorBkObj = new GameObject("editor_bk_obj");


            editorBkObj.transform.SetParent( m_go.transform );

            editorBkTransform = editorBkObj.transform;
            RectTransform editorBkRectTransform = editorBkObj.AddComponent<RectTransform>();
            editorBkRectTransform.localScale = new Vector3(1, 1, 1);
            editorBkRectTransform.anchorMin = new Vector2(0, 0);
            editorBkRectTransform.anchorMax = new Vector2(1, 1);
            editorBkRectTransform.anchoredPosition = new Vector2(0, 0);
            editorBkRectTransform.sizeDelta = new Vector2(0, 0);
            editorBkObj.name = "editor_bk_obj";

            Image bk = editorBkObj.AddComponent<Image>();
            bk.color = new Color32(39, 39, 39, 255);
//            bk.type = Image.Type.Tiled;
//            bk.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "main_ui_bk"));



            GameObject hObj = new GameObject("hsplitor");

            hObj.transform.SetParent(editorBkObj.transform);


            Image horSpliter = hObj.AddComponent<Image>();
            horSpliter.type = Image.Type.Sliced;
            Sprite splitorSprite = ResourceManager.LoadSprite(AtlasName.Battle, "splitor" );
            horSpliter.SetSprite(splitorSprite);


            RectTransform hRectTransform = hObj.transform as RectTransform;

            hRectTransform.localScale = new Vector3(1, 1, 1);
            hRectTransform.anchorMin = new Vector2(0, 0.5f);
            hRectTransform.anchorMax = new Vector2(1, 0.5f);
            hRectTransform.pivot = new Vector2(0.5f, 0.5f);
            hRectTransform.sizeDelta = new Vector2(0, splitorSprite.rect.height);
            hRectTransform.anchoredPosition = new Vector2(0, 0);

            GameObject vObj = UIHelper.Instantiate(hObj) as GameObject;
            vObj.transform.SetParent(hObj.transform.parent);

            RectTransform vRectTransform = vObj.transform as RectTransform;
            vRectTransform.localScale = hRectTransform.localScale;
            vRectTransform.anchorMin = hRectTransform.anchorMin;
            vRectTransform.anchorMax = hRectTransform.anchorMax;
            vRectTransform.sizeDelta = hRectTransform.sizeDelta;
            vRectTransform.anchoredPosition = hRectTransform.anchoredPosition;

            vObj.transform.Rotate(0, 0, 90, Space.Self);
            
        }

        editorBkTransform = m_go.transform.Find("editor_bk_obj");
        GameObject editorBk = editorBkTransform.gameObject;
      
        editorBk.transform.SetAsFirstSibling();
        editorBk.TrySetActive(true);
        
    }

    void SpecialCloneSprite()
    {
        GameObject gunObj = m_dragCloneObjs["GunList"];


        Image gunBk = gunObj.AddMissingComponent<Image>();
        gunBk.type = Image.Type.Sliced;

        gunBk.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "weaponSprite"));

        RectTransform parentRectTransform = gunObj.transform as RectTransform;

        GameObject gunSprite = null;
        Transform tempTransform = gunObj.transform.Find("gun");
        if (tempTransform == null)
        {
            gunSprite = new GameObject("gun");
        }
        else
        {
            gunSprite = tempTransform.gameObject;
        }

        gunSprite.transform.SetParent(gunObj.transform);

        RectTransform gunRectTransform = gunSprite.AddMissingComponent<RectTransform>();
        gunRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        gunRectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        gunRectTransform.anchoredPosition = new Vector2(0, 0);
        gunRectTransform.sizeDelta = parentRectTransform.sizeDelta;
        gunRectTransform.localScale = parentRectTransform.localScale * 1.5f;


        
        gunSprite.AddMissingComponent<Image>().SetSprite(ResourceManager.LoadWeaponIcon( 1001 ));



        return;
    }

    /*
    void ShowConfirmBtns( )
    {
        if (m_btnConfirm == null)
        {
            GameObject confirmsetObj = new GameObject("confirmsetObj");
            m_btnConfirm = confirmsetObj;

            confirmsetObj.transform.SetParent( m_go.transform );
            Button confirmBt = confirmsetObj.AddMissingComponent<Button>();

            Sprite kSprite = ResourceManager.LoadSprite(AtlasName.Common, "WhiteBtn_upSkin");
            
            Image confirmImage = confirmsetObj.AddMissingComponent<Image>();
            confirmImage.SetSprite(kSprite);
            confirmImage.SetNativeSize();

            RectTransform confirmRectTransform = confirmsetObj.GetComponent<RectTransform>();
            confirmRectTransform.pivot = new Vector2(1, 1);
            confirmRectTransform.anchorMin = new Vector2(1, 1);
            confirmRectTransform.anchorMax = new Vector2(1, 1);
            confirmRectTransform.anchoredPosition = new Vector2(0, 0);
            confirmRectTransform.localScale = new Vector3(1f, 1f, 1f);
            



            Sprite subSprite = ResourceManager.LoadSprite(AtlasName.Battle, "confirmbtn");
            
            GameObject subGo = new GameObject();
            subGo.transform.SetParent(confirmsetObj.transform);
            RectTransform subRectTransform = subGo.AddMissingComponent<RectTransform>();
            subRectTransform.anchoredPosition = new Vector2(6.18f, -6.37f);
            subRectTransform.sizeDelta = new Vector2(subSprite.rect.width, subSprite.rect.height);
            subRectTransform.localScale = new Vector3(1, 1, 1);
            Image subImage = subGo.AddMissingComponent<Image>();
            subImage.SetSprite(subSprite);

            confirmBt.onClick.AddListener(OnClickConfirm);
        }

        if (m_btnReset == null)
        {
            m_btnReset = new GameObject("ResetsetObj");

            m_btnReset.transform.SetParent( m_go.transform );
            Button resetBt = m_btnReset.AddMissingComponent<Button>();

            Sprite kSprite = ResourceManager.LoadSprite(AtlasName.Common, "WhiteBtn_upSkin");
            
            Image resetImage = m_btnReset.AddMissingComponent<Image>();
            resetImage.SetSprite(kSprite);
            resetImage.SetNativeSize();

            RectTransform resetRectTransform = m_btnReset.GetComponent<RectTransform>();
            resetRectTransform.pivot = new Vector2(1, 1);
            resetRectTransform.anchorMin = new Vector2(1, 1);
            resetRectTransform.anchorMax = new Vector2(1, 1);

            resetRectTransform.localScale = new Vector3(1, 1, 1f);
            resetRectTransform.anchoredPosition = new Vector2(-(kSprite.rect.width * resetRectTransform.localScale.x) - 5, 0);
            
            

            Sprite subSprite = ResourceManager.LoadSprite(AtlasName.Battle, "reset" );
            
            GameObject subGo = new GameObject();
            subGo.transform.SetParent(m_btnReset.transform);
            RectTransform subRectTransform = subGo.AddMissingComponent<RectTransform>();
            subRectTransform.anchoredPosition = new Vector2(6.18f, -6.37f);
            subRectTransform.sizeDelta = new Vector2(subSprite.rect.width, subSprite.rect.height);
            subRectTransform.localScale = new Vector3(1, 1, 1);
            Image subImage = subGo.AddMissingComponent<Image>();
            subImage.SetSprite(subSprite);
           

            resetBt.onClick.AddListener(OnClickReset);
        }


    }
    */
    void OnClickConfirm( GameObject target, PointerEventData eventData )
    {
        if (m_defaultControlPosInfo.m_dataArr.Length != m_userSetControlPosInfo.m_dataArr.Length )
        {
            m_userSetControlPosInfo.Load( m_defaultControlPosInfo );
        }

        
        foreach (ConfigBattleUIControlPosLine kObjPosInfo in m_userSetControlPosInfo.m_dataArr)
        {
            GameObject go = m_dragCloneObjs[kObjPosInfo.key];
            RectTransform goRectTransform = go.GetComponent<RectTransform>();
            kObjPosInfo.anchorX = goRectTransform.anchoredPosition.x;
            kObjPosInfo.anchorY = goRectTransform.anchoredPosition.y;
            kObjPosInfo.fAnchorMinX = goRectTransform.anchorMin.x;
            kObjPosInfo.fAnchorMinY = goRectTransform.anchorMin.y;
            kObjPosInfo.fAnchorMaxX = goRectTransform.anchorMax.x;
            kObjPosInfo.fAnchorMaxY = goRectTransform.anchorMax.y;
            kObjPosInfo.fWidth = goRectTransform.sizeDelta.x;
            kObjPosInfo.fHeight = goRectTransform.sizeDelta.y;
            kObjPosInfo.fLocalScale = goRectTransform.localScale.x;

        }

        SaveToPlayerPrefs();

        GameDispatcher.Dispatch<ConfigBattleUIControlPos>(GameEvent.UI_USER_DEFINE_CONTROL_POS, m_userSetControlPosInfo);

        

        HidePanel();
        UIManager.GetPanel<PanelParamSetting>().ShowPanel();

    }

    void OnClickReset( GameObject target, PointerEventData eventData )
    {
        m_userSetControlPosInfo.Load(m_defaultControlPosInfo);
        SaveToPlayerPrefs();

        GameDispatcher.Dispatch<ConfigBattleUIControlPos>(GameEvent.UI_USER_DEFINE_CONTROL_POS, m_defaultControlPosInfo);

        

        HidePanel();
        UIManager.GetPanel<PanelParamSetting>().ShowPanel();
    }

    void SaveToPlayerPrefs()
    {
        string strSaveInfo = m_userSetControlPosInfo.Export();
        PlayerPrefs.SetString(SAVE_BATTLE_UI_POS_KEY, WWW.EscapeURL(strSaveInfo));
        PlayerPrefs.Save();
    }
}
