﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
///////////////////////////////////////////
//Copyright (C): 4399 YiDao studio
//All rights reserved
//文件描述：战队联赛积分排名项
//创建者：hwl
//创建日期: 2015/12/29
///////////////////////////////////////////
public class ItemCorpsRankInfo : ItemRender
{

    private BaseCorpsData m_data;
    private Text m_txtRank;
    private Text m_txtName;
    private Text m_txtScore;
    private long m_id;


    public override void Awake()
    {
        m_txtRank = transform.Find("Rank").GetComponent<Text>();
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_txtScore = transform.Find("score").GetComponent<Text>();
        UGUIClickHandler.Get(gameObject).onPointerClick += OnSelect;
    }

    protected override void OnSetData(object data)
    {
        ItemCorpsRankData itemData = data as ItemCorpsRankData;
        m_id = itemData.id;
        m_txtRank.text = itemData.rank.ToString();
        m_txtName.text = itemData.name.ToString();
        m_txtScore.text = itemData.score.ToString();
        
    }

    protected void OnSelect(GameObject target, PointerEventData eventData)
    {
    }
}


public class ItemCorpsRankData
{
    public long id;
    public int rank;                  //> 玩家排名
    public string name;             //> 玩家姓名
    public int score;
}

