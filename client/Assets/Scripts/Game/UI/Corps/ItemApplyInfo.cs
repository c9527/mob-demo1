﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemApplyInfo : ItemRender
{
    private p_corps_apply_toc m_data;
    private Text m_txtName;
    private Text m_txtLvl;
    private Image m_imgHead;
    private Image m_imgArmy;
    private Image m_imgVip;
    private GameObject m_goVip;
    private GameObject m_goName;
    private Vector2 m_anchorName;
    public override void Awake()
    {
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_txtLvl = transform.Find("imgLv/Text").GetComponent<Text>();
        m_imgHead = transform.Find("imgHead/head").GetComponent<Image>();
        m_imgArmy = transform.Find("imgArmy").GetComponent<Image>();

        m_goVip = transform.Find("vip").gameObject;
        m_imgVip = transform.Find("vip").GetComponent<Image>();
        m_goName = transform.Find("name").gameObject;
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition; 
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_corps_apply_toc;
        m_txtName.text = m_data.name;
        m_txtLvl.text =String.Format("{0}级", m_data.level);
        m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(m_data.icon));
        m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(m_data.level));
        //m_txtName.text = m_data.name;
        //m_txtLvl.text = "Lv" + m_data.level.ToString();

        m_goName.GetComponent<RectTransform>().anchoredPosition = m_anchorName;
        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)m_data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            m_goVip.TrySetActive(false);
            Vector2 vipSizeData = m_goVip.GetComponent<RectTransform>().sizeDelta;
            m_goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }
    }

}
