﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemMemberInfo : ItemRender
{

    private p_corps_member_toc m_data;
    private Text m_txtName;
    private GameObject m_online;
    private Text m_txtPosition;
    private Image m_sexImg;
    private Image m_headicon;
    private Image m_positionBg;
    private Image m_armylvl;
    private Image m_headVip;
    private Text m_txtLevel;
    private Text m_txtContribution;
    private Text m_txtLastLogin;

    public override void Awake()
    {
        m_txtName = transform.Find("namebk/name").GetComponent<Text>();
        m_txtPosition = transform.Find("info/Text").GetComponent<Text>();
        m_positionBg = transform.Find("info").GetComponent<Image>();
        m_headVip = transform.Find("headframe/heroVip").GetComponent<Image>();
        m_headicon = transform.Find("headframe/head").GetComponent<Image>();
        m_armylvl = transform.Find("level/imgArmy").GetComponent<Image>();
        m_sexImg = transform.Find("sex").GetComponent<Image>();
        m_txtLevel = transform.Find("level/Text").GetComponent<Text>();
        m_txtContribution = transform.Find("credit").GetComponent<Text>();
        m_txtLastLogin = transform.Find("online_text").GetComponent<Text>();
        m_online = transform.Find("online_logo").gameObject;
        UGUIClickHandler.Get(transform).onPointerClick += OnClickMemberInfo;
    }

    private void OnClickMemberInfo(GameObject go, PointerEventData eventData)
    {
        
        if (m_data.id != PlayerSystem.roleId)
        {
            UIManager.GetPanel<PanelCorps>().dropTarget.TrySetActive(true);
            UIManager.ShowDropList(UIManager.GetPanel<PanelCorps>().ClickMemberOptions, ItemDropCallBack, UIManager.GetPanel<PanelCorps>().transform, go.transform, new Vector2(0, 0), UIManager.Direction.Down, 132, 45, UIManager.DropListPattern.Pop);
        }
    }

    private void ItemDropCallBack(ItemDropInfo info)
    {
        var panel = UIManager.GetPanel<PanelCorps>();
        if (panel != null)
            panel.DoOption(info.subtype, m_data);
        UIManager.GetPanel<PanelCorps>().dropTarget.TrySetActive(false);
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_corps_member_toc;
        m_txtName.text = m_data.name;
        m_txtPosition.text = PanelCorps.m_strPosition[m_data.member_type - 1];  
        m_txtLevel.text = m_data.level + "级";
        m_txtContribution.text = m_data.week_fighting.ToString();
        m_headicon.SetSprite(ResourceManager.LoadRoleIcon(m_data.icon));
        m_armylvl.SetSprite(ResourceManager.LoadArmyIcon(m_data.level));
        m_txtLastLogin.text = TimeUtil.LastLoginString(m_data.logout_time);
        string nameBgSpriteName;
        if (m_data.member_type > (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER &&
            m_data.member_type < (int)CORPS_MEMBER_TYPE.TYPE_MEMBER)
        {
            nameBgSpriteName = "name_bg_3";
        }
        else
        {
            nameBgSpriteName = "name_bg_" + (m_data.member_type - 1);
        }
        m_positionBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, nameBgSpriteName));
        m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, m_data.sex == 0?"male":"female"));

        if( m_data.handle == 0 )
        {
            m_online.TrySetActive(false);
        }
        else
        {
            m_txtLastLogin.text = "<color='#00ff00'>在线</color>";
        }
        var heroType = m_data.hero_type;
         m_headVip.gameObject.TrySetActive(false);
        if (heroType != (int) EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroType == (int)EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == (int)EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == (int)EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
            }
        }
    }
}
