﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：战队联赛上届战绩
//创建者：HWL
//创建时间: 2016/4/20 17:59:30
///////////////////////////

class PanelCorpsLastMatch : BasePanel
{
    public class ItemCorp
    {
        public Text name;
        public Image icon;
    }

    ItemCorp[] corps8;
    ItemCorp[] corps4;
    ItemCorp[] corps1;

    private Text m_TimeText;
    private Text m_TipsText;
    public PanelCorpsLastMatch()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsLastMatch");
        AddPreLoadRes("UI/Corps/PanelCorpsLastMatch", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }
    public override void Init()
    {
        corps8 = new ItemCorp[8];
        corps4 = new ItemCorp[4];
        corps1 = new ItemCorp[1];
        for (int i = 1; i <= 8; i++)
        {
            corps8[(i - 1)] = new ItemCorp();
            corps8[(i - 1)].name = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup1/ItemTeam{0}/name", i)).GetComponent<Text>();
            corps8[(i - 1)].icon = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup1/ItemTeam{0}/flag/biaozhi", i)).GetComponent<Image>();
        }
        for (int i = 1; i <= 4; i++)
        {
            corps4[(i - 1)] = new ItemCorp();
            corps4[(i - 1)].name = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup2/ItemTeam{0}/name", i)).GetComponent<Text>();
            corps4[(i - 1)].icon = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup2/ItemTeam{0}/flag/biaozhi", i)).GetComponent<Image>();
        }
        for (int i = 1; i <= 1; i++)
        {
            corps1[(i - 1)] = new ItemCorp();
            corps1[(i - 1)].name = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup3/ItemTeam{0}/name", i)).GetComponent<Text>();
            corps1[(i - 1)].icon = m_tran.Find(string.Format("LastMatchtable/MatchTable/TeamGroup3/ItemTeam{0}/flag/biaozhi", i)).GetComponent<Image>();
        }

        m_TimeText = m_tran.Find("LastMatchtable/timeText").GetComponent<Text>();
        m_TipsText = m_tran.Find("LastMatchtable/timeTips").GetComponent<Text>();
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        DateTime time = TimeUtil.GetNowTime();
        int temp = CorpsDataManager.m_matchWeek;
        NetLayer.Send(new tos_corpsmatch_last_record() { is_last = true });
        int days = 0;
        if (temp == 7)
        {
            if( CorpsDataManager.m_matchStage == 1 )
            {
                days =7+ 1 + 1;
            }
            else
            {
                days = 1 + 1;
            }
            
        }
        else
        {
            days = (7 - temp + 1 + 1);
        }
        m_TimeText.text = days + "天";

        if (days<=7)
        {
            m_TimeText.enabled = false;
            m_TipsText.text = "联赛混战区每日19:00~21:00开启";
        }
        else
        {
            m_TimeText.enabled = false;
            m_TipsText.text = "联赛混战区每日19:00~21:00开启";
        }
    }

    void Toc_corpsmatch_last_record(toc_corpsmatch_last_record data)
    {
        clearInfo();
        p_corpsmatch_team team;

        for (int i = 0; i < data.records.Length; i++)
        {
            
            if (data.records[i].day == 3)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    team = data.records[i].teams[j];
                    if (team != null)
                    {
                        Logger.Log("i=" + i + " day=" + data.records[i].day + "  pos=" + (team.pos - 1) + "  name=" + team.name + "\n");
                        corps8[team.pos-1].name.text = team.name;
                        string biaozhi = "biaozhi_" + team.icon.ToString();
                        corps8[team.pos-1].icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
                        corps8[team.pos-1].icon.gameObject.TrySetActive(true);
                    }
                }
            }
            if (data.records[i].day == 4)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    team = data.records[i].teams[j];
                    if (team != null)
                    {
                        Logger.Log("i=" + i + " day=" + data.records[i].day + "  pos=" + (team.pos-1) + "  name=" + team.name + "\n");
                    }
                    else
                    {
                        Logger.Log("team is Null");
                    }
                    
                    for (int e = 0; e < corps8.Length; e++)
                    {
                        if (corps8[e].name.text == team.name)
                        {
                            corps4[e/2].name.text = team.name;
                            string biaozhi = "biaozhi_" + team.icon.ToString();
                            corps4[e/2].icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
                            corps4[e/2].icon.gameObject.TrySetActive(true);
                        }
                    }
                }
            }
            if (data.records[i].day == 7)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    team = data.records[i].teams[j];
                    for (int e = 0; e < corps4.Length; e++)
                    {
                        if (corps4[e].name.text == team.name)
                        {
                            corps1[j].name.text = team.name;
                            string biaozhi = "biaozhi_" + team.icon.ToString();
                            corps1[j].icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
                            corps1[j].icon.gameObject.TrySetActive(true);
                        }
                    }
                }
            }
        }
    }

    private void clearInfo()
    {
        for (int i = 0; i < corps8.Length; i++)
        {
            corps8[i].name.text = "";
            corps8[i].icon.gameObject.TrySetActive(false);
        }
        for (int i = 0; i < corps4.Length; i++)
        {
            corps4[i].name.text = "";
            corps4[i].icon.gameObject.TrySetActive(false);
        }
        for (int i = 0; i < corps1.Length; i++)
        {
            corps1[i].name.text = "";
            corps1[i].icon.gameObject.TrySetActive(false);
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorpsEnter>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
