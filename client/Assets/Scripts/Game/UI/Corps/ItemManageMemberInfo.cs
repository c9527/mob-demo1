﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI.Corps
{
    public class ItemManagerMemberInfo : ItemRender
    {
        private p_corps_member_toc m_data;
        private Text m_txtName;

        private Text m_txtPosition;
        private Image m_sexImg;
        private Image m_headicon;
        private Image m_positionBg;
        private Image m_armylvl;
        private Text m_txtLevel;
        private Text m_txtContribution;
        private Text m_txtAllContribution;
        private Text m_txtLastLogin;

        public override void Awake()
        {
            m_txtName = transform.Find("namebk/name").GetComponent<Text>();
            m_txtPosition = transform.Find("info/Text").GetComponent<Text>();
            m_positionBg = transform.Find("info").GetComponent<Image>();
//            m_headicon = transform.Find("headframe/head").GetComponent<Image>();
//            m_armylvl = transform.Find("level/imgArmy").GetComponent<Image>();
//            m_sexImg = transform.Find("sex").GetComponent<Image>();
            m_txtLevel = transform.Find("lvl").GetComponent<Text>();
            m_txtContribution = transform.Find("credit").GetComponent<Text>();
            m_txtAllContribution = transform.Find("allcredit").GetComponent<Text>();
            m_txtLastLogin = transform.Find("online_text").GetComponent<Text>();
            UGUIClickHandler.Get(transform).onPointerClick += OnClickMemberInfo;
        }

        protected override void OnSetData(object data)
        {
            m_data = data as p_corps_member_toc;
            m_txtName.text = m_data.name;
            m_txtPosition.text = PanelCorps.m_strPosition[m_data.member_type - 1];
            m_txtLevel.text = m_data.level.ToString();
            m_txtAllContribution.text = m_data.fighting.ToString();
            m_txtContribution.text = m_data.week_fighting.ToString();
//            m_headicon.SetSprite(ResourceManager.LoadRoleIcon(m_data.icon));
//            m_armylvl.SetSprite(ResourceManager.LoadArmyIcon(m_data.level));
            m_txtLastLogin.text = TimeUtil.LastLoginString(m_data.logout_time);
            string nameBgSpriteName;
            if (m_data.member_type > (int) CORPS_MEMBER_TYPE.TYPE_VICELEADER &&
                m_data.member_type < (int) CORPS_MEMBER_TYPE.TYPE_MEMBER)
            {
                nameBgSpriteName = "name_bg_3";
            }
            else
            {
                nameBgSpriteName = "name_bg_" + (m_data.member_type - 1);
            }
            m_positionBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, nameBgSpriteName));
            if (m_data.handle != 0)
            {
                m_txtLastLogin.text = "<color='#00ff00'>在线</color>";
            }
        }

        private void OnClickMemberInfo(GameObject go, PointerEventData eventData)
        {
            if (m_data.id != PlayerSystem.roleId)
            {
                var mySelf = CorpsDataManager.GetSelf();
                UIManager.GetPanel<PanelCorpsManage>().dropTarget.TrySetActive(true);
                ItemDropInfo[] options;
                if (mySelf == null || mySelf.member_type >= (int)CORPS_MEMBER_TYPE.TYPE_MEMBER)
                {
                    options = UIManager.GetPanel<PanelCorpsManage>().ClickMemberOptions;
                }
                else if (mySelf.member_type >= (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER)
                {
                    options = UIManager.GetPanel<PanelCorpsManage>().ClickViceLeaderOptions;
                }
                else
                {
                    options = UIManager.GetPanel<PanelCorpsManage>().ClickLeaderOptions;
                }
                UIManager.ShowDropList(options, ItemDropCallBack, UIManager.GetPanel<PanelCorpsManage>().transform, go.transform, new Vector2(0, 0), UIManager.Direction.Down, 132, 45, UIManager.DropListPattern.Pop);

            }
        }

        private void ItemDropCallBack(ItemDropInfo info)
        {
            var panel = UIManager.GetPanel<PanelCorpsManage>();
            if (panel != null)
                panel.DoOption(info.subtype, m_data);
            UIManager.GetPanel<PanelCorpsManage>().dropTarget.TrySetActive(false);
        }

    }
}
