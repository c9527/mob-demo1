﻿using System.Text;
using Assets.Scripts.Game.UI.Corps;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;


public class PanelCorps : BasePanel
{
    public class ItemMatchList : ItemRender
    {
        public Text m_team1;
        public Text m_team2;
        public Image m_icon1;
        public Image m_icon2;
        public int id;
        public bool is_myTeam;

        public override void Awake()
        {
            id = 0;
            is_myTeam = false;
            m_team1 = transform.Find("Team1/name").GetComponent<Text>();
            m_team2 = transform.Find("Team2/name").GetComponent<Text>();
            m_icon1 = transform.Find("Team1/Image").GetComponent<Image>();
            m_icon2 = transform.Find("Team2/Image").GetComponent<Image>();

        }

        protected override void OnSetData(object data)
        {
            var info = data as p_corpsmatch_room;
            id = info.room_id;
            m_team1.text = info.team1;
            m_team2.text = info.team2;
            if (info.team1 == CorpsDataManager.GetCorpsName() || info.team2 == CorpsDataManager.GetCorpsName())
            {
                is_myTeam = true;
                if (info.team1 == CorpsDataManager.GetCorpsName())
                {
                    m_team1.color = new Color32(134, 253, 131, 255);
                }
                else
                {
                    m_team2.color = new Color32(134, 253, 131, 255);
                }
            }
            string biaozhi = "biaozhi_" + info.icon1.ToString();
            m_icon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            biaozhi = "biaozhi_" + info.icon2.ToString();
            m_icon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        }
    }


    private CDCorpsActive[] _active_data;

    private DataGrid _active_list;

//    private Toggle m_toggleCorpsInfo;
//    private Toggle m_toggleCorpsMembers;
    private Toggle m_toggleCorpsHomepage;
    private Toggle m_toggleCorpsMatch;
    private Toggle m_toggleCorpsActive;
//    private Toggle m_toggleCorpsRecommend;

    public static string search_key;
//    private GameObject m_goCorpsList;
//    private GameObject m_goCorpsMembers;
    private GameObject m_goCorpsHomepage;
    private GameObject m_goCorpsActive;
    private GameObject m_goCorpsMatchResult;
    private GameObject curCorpsMatch;    
    private DataGrid m_corpsListDataGrid;
    private DataGrid m_memberListDataGrid;
    private DataGrid m_recommendDataGrid;
    private List<string> m_logStrList;
    private GameObject m_applyBubble;
    private Text m_applyNum;
    private static int BTN_HEIGHT = 45;
    private static int BTN_SPACE = 15;
    private GameObject m_goRecruit;
    private GameObject m_goManage;
    private GameObject m_goApply;
    private GameObject m_goAuthorrize;
    private GameObject m_goKickout;
    private GameObject m_goChangeLeader;
    private GameObject m_goNoticeEdit;
    private GameObject m_goUpgrade;
    private GameObject m_goSetting;    
    private GameObject m_goMyInfo;
    private GameObject m_goMatchInfo;
    private GameObject m_goBottomCommonBtns;
    private GameObject m_goBottomMatchBtns;
//    private GameObject m_goRecommendList;
    private int m_roomid;

    private long m_team1id;
    private long m_team2id;
    private bool m_matchteaminfo;
    public CorpsData m_matchData;
    public GameObject dropTarget;

    //联赛结果
    private Image m_flag1;
    private Image m_flag2;
    private Image m_flag3;
    private Image m_icon1;
    private Image m_icon2;
    private Image m_icon3;
    private Text m_name1;
    private Text m_name2;
    private Text m_name3;

    private Text m_corpsScoreText;
    private Text m_corpsRankText;
    private Text m_myScoreRankText;
    private Text m_myScoreText;
    private Text m_corpMatchRemain;
    private ItemDropInfo[] _clickMemberOptions;

    public ItemDropInfo[] ClickMemberOptions
    {
        get { return _clickMemberOptions; }
    }
    DateTime m_curTime = TimeUtil.GetNowTime();
    DateTime m_signupTime = new DateTime(TimeUtil.GetNowTime().Year, TimeUtil.GetNowTime().Month, 10, 23, 59, 59);
    

    public class ItemTeam
    {
        public Text name;
        public Image icon;
    }

    protected enum FRAME_TYPE
    {
        TYPE_HOMEPAGE = 1,
        TYPE_MEMBER = 2,
        TYPE_CORPS = 3,
        TYPE_ACTIVE = 5,
        TYPE_MATCH = 6,
        TYPE_RECOMMEDN=7,
    }

    public class MemberOption
    {
        public const string LOOK_UP = "1";
        public const string FOLLOW = "2";
        public const string CHAT = "3";
    }

    

    public static string[] m_strPosition = { "队长", "副队长", "指挥官", "考核官", "外交官", "秘书长", "普通成员" };

    private Dictionary<CORPS_MEMBER_TYPE, List<p_corps_member_toc>> m_dicAuthers;
    private FRAME_TYPE m_curFrame = FRAME_TYPE.TYPE_HOMEPAGE;

    private int CREATE_CORPS_LVL;
    public PanelCorps()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorps");
        AddPreLoadRes("UI/Corps/PanelCorps", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemCorpsMember", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void Init()
    {
        _clickMemberOptions = new ItemDropInfo[] {
            new ItemDropInfo { type = "CorpMember", subtype = MemberOption.LOOK_UP, label = "查看信息" }, 
            new ItemDropInfo { type = "CorpMember", subtype = MemberOption.FOLLOW, label = "跟随" },
            new ItemDropInfo { type = "CorpMember", subtype = MemberOption.CHAT, label = "私聊" }, 
        };
        _active_data = new CDCorpsActive[]{
            new CDCorpsActive(){ Uid=ECorpsActiveUID.LOTTERY, Icon="corps_lottery", Desc="战前准备，抽取每日军资", Progress=0, Total=0, Status=0},
            new CDCorpsActive(){ Uid=ECorpsActiveUID.SHOP, Icon="corps_shop", Desc="只此一家，欢迎抢购", Progress=0, Total=0, Status=0},
            new CDCorpsActive(){ Uid=ECorpsActiveUID.COOPERATE_TASK, Icon="cooperate_task", Desc="全员拿奖，争做合作任务", Progress=0, Total=0, Status=0},
            new CDCorpsActive(){ Uid=ECorpsActiveUID.GUARD_TASK, Icon="guard_task", Desc="每日20:30开启，通关可获得荣誉", Progress=0, Total=0, Status=0},
        };

        m_logStrList = new List<string>();
        CREATE_CORPS_LVL = ConfigMisc.GetInt("create_corps_reqlv");
        m_toggleCorpsHomepage = m_tran.Find("left/top_tab/grid/ToggleCorpsHomepage").GetComponent<Toggle>();
        m_toggleCorpsActive = m_tran.Find("left/top_tab/grid/ToggleCorpsActive").GetComponent<Toggle>();
        m_toggleCorpsMatch = m_tran.Find("left/top_tab/grid/ToggleCorpsMatch").GetComponent<Toggle>();
        m_goCorpsHomepage = m_tran.Find("left/middle_info/corps_info").gameObject;
        m_goCorpsActive = m_tran.Find("left/middle_info/corps_active").gameObject;               
        m_goCorpsMatchResult = m_tran.Find("left/middle_info/corps_match").gameObject;
        dropTarget = m_tran.Find("dropTarget").gameObject;
        curCorpsMatch = m_goCorpsMatchResult;

        _active_list = m_goCorpsActive.transform.Find("item_list/content").gameObject.AddComponent<DataGrid>();
        _active_list.autoSelectFirst = false;
        _active_list.SetItemRender(m_goCorpsActive.transform.Find("item_list/Render").gameObject, typeof(CorpsActiveItemRender));

        m_goRecruit = m_tran.Find("right/top_info/recruit_btn").gameObject;
        m_goManage = m_tran.Find("right/top_info/manage_btn").gameObject;
        m_goNoticeEdit = m_tran.Find("left/middle_info/corps_info/annouce/edit_btn").gameObject;
        m_goUpgrade = m_tran.Find("left/middle_info/corps_info/upgrade_btn").gameObject;

        m_goMyInfo = m_tran.Find("left/middle_info/myinfo").gameObject;
        m_goMatchInfo = m_tran.Find("left/middle_info/match_info").gameObject;

        m_goBottomCommonBtns = m_tran.Find("left/bottom_btns/common_btns").gameObject;
        m_goBottomMatchBtns = m_tran.Find("left/bottom_btns/match_btns").gameObject;

        m_applyBubble = m_tran.Find("right/top_info/manage_btn/bubble_tip").gameObject;
        m_applyNum = m_tran.Find("right/top_info/manage_btn/bubble_tip/Text").GetComponent<Text>();

        m_tran.Find("right/ScrollRect/content").gameObject.AddComponent<DataGrid>();
        m_memberListDataGrid = m_tran.Find("right/ScrollRect/content").GetComponent<DataGrid>();
        m_memberListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemCorpsMember"), typeof(ItemMemberInfo));
        m_memberListDataGrid.useLoopItems = true;
        m_dicAuthers = new Dictionary<CORPS_MEMBER_TYPE, List<p_corps_member_toc>>();

        m_team1id = 0;
        m_team2id = 0;

        m_roomid = 0;
        m_matchteaminfo = false;
        m_matchData = new CorpsData();

        m_curTime = TimeUtil.GetNowTime();

        CorpsDataManager.m_roomdata[0] = new corpsRoomData();
        CorpsDataManager.m_roomdata[1] = new corpsRoomData();

        //联赛结果
        m_flag1 = m_tran.Find("left/middle_info/corps_match/team3rd/team1").GetComponent<Image>();
        m_flag2 = m_tran.Find("left/middle_info/corps_match/team3rd/team2").GetComponent<Image>();
        m_flag3 = m_tran.Find("left/middle_info/corps_match/team3rd/team3").GetComponent<Image>();

        m_icon1 = m_tran.Find("left/middle_info/corps_match/team3rd/team1/Image").GetComponent<Image>();
        m_icon2 = m_tran.Find("left/middle_info/corps_match/team3rd/team2/Image").GetComponent<Image>();
        m_icon3 = m_tran.Find("left/middle_info/corps_match/team3rd/team3/Image").GetComponent<Image>();

        m_name1 = m_tran.Find("left/middle_info/corps_match/team3rd/team1/name").GetComponent<Text>();
        m_name2 = m_tran.Find("left/middle_info/corps_match/team3rd/team2/name").GetComponent<Text>();
        m_name3 = m_tran.Find("left/middle_info/corps_match/team3rd/team3/name").GetComponent<Text>();

        m_corpsScoreText = m_tran.Find("left/middle_info/match_info/scoreText/corpsScoreText").GetComponent<Text>();
        m_corpsRankText = m_tran.Find("left/middle_info/match_info/scoreText/corpsRankText").GetComponent<Text>();
        m_myScoreText = m_tran.Find("left/middle_info/match_info/scoreText/myScoreText").GetComponent<Text>();
        m_myScoreRankText = m_tran.Find("left/middle_info/match_info/scoreText/myScoreRankText").GetComponent<Text>();
        m_corpMatchRemain = m_tran.Find("left/middle_info/match_info/remain/count").GetComponent<Text>();

    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("left/top_tab/grid/ToggleCorpsHomepage").gameObject).onPointerClick += OnClkCorpHomepage;
        UGUIClickHandler.Get(m_tran.Find("left/top_tab/grid/ToggleCorpsActive").gameObject).onPointerClick += OnClkCorpActive;
        UGUIClickHandler.Get(m_tran.Find("left/top_tab/grid/ToggleCorpsMatch").gameObject).onPointerClick += OnClkCorpMatch;
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;

        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/common_btns/log_btn").gameObject).onPointerClick += OnClkCorpsLog;
        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/common_btns/rank_btn").gameObject).onPointerClick += OnClkCorpsRank;
        UGUIClickHandler.Get(m_tran.Find("left/middle_info/corps_info/upgrade_btn").gameObject).onPointerClick += OnClkCorpsUpgrade;
        UGUIClickHandler.Get(m_tran.Find("right/top_info/recruit_btn").gameObject).onPointerClick += OnClkRecruit;
        UGUIClickHandler.Get(m_tran.Find("right/top_info/manage_btn").gameObject).onPointerClick += OnClkManage;
        UGUIClickHandler.Get(m_tran.Find("left/middle_info/corps_info/annouce/edit_btn").gameObject).onPointerClick += OnClkBulitin;


        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/common_btns/donate_btn").gameObject).onPointerClick += OnPopUpDonatePanel;
        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/match_btns/JoinMatch").gameObject).onPointerClick += OnOpenCorpsRoomPanel;

        InitButtonListEvent(m_goCorpsMatchResult);

        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateUIBubble);
        AddEventListener(GameEvent.UI_CHANGE_CORPS, UpdateView);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener<BaseCorpsData>(GameEvent.UI_DOUBLE_CLICK_CORPSLIST, OnDoubleClickCorpsList);
#endif
    }

    private void OnClickRecommend(GameObject target, PointerEventData eventData)
    {
        SwitchFrame(FRAME_TYPE.TYPE_RECOMMEDN);
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    private void OnClickCorpsRefresh(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_corps_recmd_list());
    }


    private void OnDoubleClickCorpsList(BaseCorpsData corps )
    {
        if (CorpsDataManager.m_corpsData.corps_id != 0)
        {
            UIManager.ShowTipPanel("请先退出战队");
            return;
        }
        tos_corps_apply_enter msg = new tos_corps_apply_enter();
        msg.corps_id = corps.corps_id;
        NetLayer.Send(msg);
    }

    private void UpdateUIBubble()
    {
        m_toggleCorpsActive.transform.Find("Bubble").gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.CorpsTaskReward) || BubbleManager.HasBubble(BubbleConst.CorpsGuardRunning));
    }

    private void OnClkRecruit(GameObject target, PointerEventData eventData)
    {
        p_corps_member_toc self = CorpsDataManager.GetSelf();
        if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
        {
            return;
        }
        if (self.member_type >= (int)CORPS_MEMBER_TYPE.TYPE_MEMBER)
        {
            TipsManager.Instance.showTips("无权限招募成员");
            return;
        }
        UIManager.PopPanel<PanelCorpsRecruit>(null, true, this);
    }

    private void OnClkManage(GameObject target, PointerEventData eventData)
    {
        p_corps_member_toc self = CorpsDataManager.GetSelf();
        if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
        {
            return;
        }
        UIManager.ShowPanel<PanelCorpsManage>(null);
    }


    private void OnUpdatePlayerData()
    {
        if (m_goCorpsHomepage.activeSelf|| m_goCorpsActive.activeSelf)
        {
            m_tran.FindChild("left/middle_info/myinfo/mycorpscredit/num").GetComponent<Text>().text = PlayerSystem.roleData.corps_coin.ToString();
        }
    }

    /// <summary>
    /// 打开混战界面
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnOpenCorpsRoomPanel(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelCorpsEnter>();
    }

    private void OnPopUpDonatePanel(GameObject target, PointerEventData eventData)
    {
        //弹出捐献界面
        UIManager.PopPanel<PanelCorpsDonate>(null, true, this);
    }


    private void OnClkCorpActive(GameObject target, PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
        {
            m_toggleCorpsActive.isOn = false;
            UIManager.ShowPanel<PanelCorpsList>();
            TipsManager.Instance.showTips("你还未加入战队!");
            return;
        }
        SwitchFrame(FRAME_TYPE.TYPE_ACTIVE);
    }

    public override void OnShow()
    {
        CorpsDataManager.m_bGetCorpsList = true;
        if(!CorpsDataManager.IsJoinCorps())
        {
            NetLayer.Send(new tos_corps_recmd_list());            
            m_curFrame = FRAME_TYPE.TYPE_RECOMMEDN;
        }
            
        UpdateView();
        UpdateUIBubble();
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
        if (m_curFrame != FRAME_TYPE.TYPE_MATCH)
        {
            curCorpsMatch.TrySetActive(false);
        }
        //请求上期前三
        m_flag1.gameObject.TrySetActive(false);
        m_flag2.gameObject.TrySetActive(false);
        m_flag3.gameObject.TrySetActive(false);
        NetLayer.Send(new tos_corpsmatch_result()); 
        if (HasParams())
        {
            string option = (string)m_params[0];
            if (option == "corpsActivity" && CorpsDataManager.IsJoinCorps())
            {
                m_toggleCorpsActive.isOn = true;
                SwitchFrame(FRAME_TYPE.TYPE_ACTIVE);
                if (m_params.Length > 1)
                {
                    if ((string) m_params[1] == "corpsCoperateTask")
                        UIManager.ShowPanel<PanelCorpsTask>();
                }
            }
        }
        CorpsDataManager.IsOpenByCorpsPanel = true;
        NetLayer.Send(new tos_player_my_ranks());//排名
        //请求我的积分，我的战队积分、剩余次数信息
        NetLayer.Send(new tos_corpsmatch_score_info());
    }

    public override void OnHide()
    {
        CorpsDataManager.m_bGetCorpsList = false;
        PanelCorps.search_key = null;
        CorpsDataManager.IsOpenByCorpsPanel = false;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelSocity>();
    }

    public override void OnDestroy()
    {
    }

    protected void SwitchFrame(FRAME_TYPE eType)
    {
        m_curFrame = eType;

        m_toggleCorpsHomepage.isOn = false;
        m_toggleCorpsActive.isOn = false;
        m_toggleCorpsMatch.isOn = false;
        m_goCorpsHomepage.TrySetActive(false);
        m_goCorpsActive.TrySetActive(false);
        curCorpsMatch.TrySetActive(false);
        m_goBottomCommonBtns.TrySetActive(true);
        m_goBottomMatchBtns.TrySetActive(false);
        m_goMyInfo.TrySetActive(true);
        m_goMatchInfo.TrySetActive(false);

        if (m_curFrame == FRAME_TYPE.TYPE_HOMEPAGE)
        {
            m_toggleCorpsHomepage.isOn = true;
            m_goCorpsHomepage.TrySetActive(true);
            UpdateHomePage();
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_CORPS)
        {
            UIManager.ShowPanel<PanelCorpsList>(null);
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_ACTIVE)
        {
            m_toggleCorpsActive.isOn = true;
            m_goCorpsActive.TrySetActive(true);
            UpdateCorpsActive();
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
        {
            m_toggleCorpsMatch.isOn = true;
            curCorpsMatch.TrySetActive(true);
            m_goBottomCommonBtns.TrySetActive(false);
            m_goBottomMatchBtns.TrySetActive(true);
            m_goMyInfo.TrySetActive(false);
            m_goMatchInfo.TrySetActive(true);
            UpdateCorpsMatch();
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_RECOMMEDN)
        {
            UIManager.ShowPanel<PanelCorpsList>();
        }
    }

    private void RefreshActiveList(ECorpsActiveUID uid)
    {
        List<ItemRender> renders = null;
        if (m_goCorpsActive.activeSelf)
        {
            renders = _active_list.ItemRenders;
            foreach (CorpsActiveItemRender render_0 in renders)
            {
                if (render_0 != null && render_0.data != null && render_0.data.Uid == uid)
                {
                    render_0.RefreshView();
                    break;
                }
            }
        }
    }

    private void UpdateCorpsActive()
    {
        _active_list.Data = _active_data;
//        OnUpdatePlayerData();
        CorpsModel.Instance.TosGetLotteryData();
        CorpsModel.Instance.TosGetShopData();
    }

    public void UpdateHomePage()
    {
        Transform m_transHomepage = m_goCorpsHomepage.transform;

        m_tran.FindChild("left/middle_info/corps_info/logo/name").GetComponent<Text>().text = CorpsDataManager.m_corpsData.name;
        string flag = "f" + CorpsDataManager.m_corpsData.logo_pannel.ToString() + "_" + "c" + CorpsDataManager.m_corpsData.logo_color.ToString();
        m_tran.FindChild("left/middle_info/corps_info/logo/flag").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        string biaozhi = "biaozhi_" + CorpsDataManager.m_corpsData.logo_icon.ToString();
        m_tran.FindChild("left/middle_info/corps_info/logo/biaozhi").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        m_tran.FindChild("left/middle_info/corps_info/lvl").GetComponent<Text>().text = "等级:"+ CorpsDataManager.m_corpsData.level;

        var config = ConfigManager.GetConfig<ConfigCorpsLevel>();
        ConfigCorpsLevelLine upLine = config.GetLine(CorpsDataManager.m_corpsData.level + 1);
        if (upLine == null)
        {
            upLine = config.GetLine(CorpsDataManager.m_corpsData.level);
        }

        m_tran.FindChild("left/middle_info/corps_info/contribute/content").GetComponent<Text>().text = CorpsDataManager.m_corpsData.fighting.ToString() + "/" + upLine.Activeness.ToString();
        m_tran.FindChild("left/middle_info/corps_info/contribute/slider").GetComponent<Slider>().value =(float)CorpsDataManager.m_corpsData.fighting/upLine.Activeness;
        m_tran.FindChild("left/middle_info/corps_info/construct/content").GetComponent<Text>().text = CorpsDataManager.m_corpsData.tribute.ToString();
        m_tran.FindChild("left/middle_info/corps_info/id/content").GetComponent<Text>().text = CorpsDataManager.m_corpsData.corps_id.ToString();
        m_tran.FindChild("right/top_info/member_count").GetComponent<Text>().text = CorpsDataManager.m_lstMember.Count.ToString() + "/" + CorpsDataManager.m_corpsData.max_member.ToString();
        m_tran.FindChild("left/middle_info/myinfo/mycredit/num").GetComponent<Text>().text = CorpsDataManager.GetSelf().fighting.ToString();
        m_tran.FindChild("left/middle_info/myinfo/mycorpscredit/num").GetComponent<Text>().text = PlayerSystem.roleData.corps_coin.ToString();

        if (CorpsDataManager.m_corpsData.notice != "")
        {
            m_transHomepage.FindChild("annouce/content").GetComponent<Text>().text = CorpsDataManager.m_corpsData.notice;
        }

        m_dicAuthers.Clear();
        for (int i = 0; i < CorpsDataManager.m_lstMember.Count; i++)
        {
            p_corps_member_toc mbr = CorpsDataManager.m_lstMember[i];
            CORPS_MEMBER_TYPE mbrType = (CORPS_MEMBER_TYPE)mbr.member_type;
            if (mbrType == CORPS_MEMBER_TYPE.TYPE_MEMBER)
                continue;

            List<p_corps_member_toc> lstMbrs;
            if (m_dicAuthers.ContainsKey(mbrType))
            {
                lstMbrs = m_dicAuthers[mbrType];
            }
            else
            {
                lstMbrs = new List<p_corps_member_toc>();
                m_dicAuthers[mbrType] = lstMbrs;
            }

            lstMbrs.Add(mbr);
        }
        p_corps_member_toc mySelf = CorpsDataManager.GetSelf();

        if (!CorpsDataManager.CanSeeApplyList())
        {
            m_goRecruit.TrySetActive(false);

        }
        else
        {
            if (CorpsDataManager.GetApplyNum() > 0)
            {
                m_applyBubble.TrySetActive(true);
                m_applyNum.GetComponent<Text>().text = CorpsDataManager.GetApplyNum().ToString();
            }
            else
            {
                m_applyBubble.TrySetActive(false);
            }
            m_goRecruit.TrySetActive(true);
        }

        if (mySelf == null || mySelf.member_type > (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER)
        {
            m_goUpgrade.TrySetActive(false);
            m_goNoticeEdit.TrySetActive(false);
        }
        else
        {
            m_goUpgrade.TrySetActive(true);
            m_goNoticeEdit.TrySetActive(true);
        }
        m_memberListDataGrid.Data = CorpsDataManager.m_lstMember.ToArray();
    }


    public void UpdateView( )
    {
        if (!CorpsDataManager.IsJoinCorps())
        {          
            if (!(m_curFrame==FRAME_TYPE.TYPE_RECOMMEDN||m_curFrame==FRAME_TYPE.TYPE_CORPS))
            {
                m_curFrame = FRAME_TYPE.TYPE_RECOMMEDN;
            }           
            if(string.IsNullOrEmpty(PanelCorps.search_key))
            {
                SwitchFrame(m_curFrame);
            }
            else
            {
                SwitchFrame(FRAME_TYPE.TYPE_CORPS);
            }
        }
        else
        {
            if (m_curFrame == FRAME_TYPE.TYPE_RECOMMEDN)
                m_curFrame =FRAME_TYPE.TYPE_HOMEPAGE;
            SwitchFrame(m_curFrame);
        }
    }

    public void UpdateCorpsMatch()
    {
        CheckCorpsData();
        ShowCorpsContent();
    }

    void InitButtonListEvent(GameObject go)
    {
        UGUIClickHandler.Get(m_tran.Find("left/middle_info/corps_match/ButtonList/Award")).onPointerClick += delegate
        {
            UIManager.PopRulePanel("CorpsMatchReward");
        };
        UGUIClickHandler.Get(m_tran.Find("left/middle_info/corps_match/ButtonList/Rule")).onPointerClick += delegate { UIManager.PopRulePanel("CorpsMatch"); };
        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/match_btns/CorpsMatchRank")).onPointerClick += delegate { UIManager.PopPanel<PanelCorpsMatchRank>(new object[] { "corpsmatch" }, true); };
        UGUIClickHandler.Get(m_tran.Find("left/bottom_btns/match_btns/CorpsMatchMyRank")).onPointerClick += delegate { UIManager.PopPanel<PanelCorpsMatchRank>(new object[] { "corpsmatch_win_rounds" }, true); };
    }

    void Toc_corpsmatch_match_info(toc_corpsmatch_match_info data)
    {
        m_team1id = data.my_id;
        m_team2id = data.other_id;
        m_matchteaminfo = true;
        CorpsDataManager.m_roomdata = new corpsRoomData[2];
        CorpsDataManager.m_corpsrule = data.mode;
        if (m_team2id != 0)
        {
            NetLayer.Send(new tos_corps_view_info() { corps_id = m_team2id });
        }
        updateTeamView();
    }

    void updateTeamView()
    {
        string flag = "f" + CorpsDataManager.m_corpsData.logo_pannel.ToString() + "_" + "c" + CorpsDataManager.m_corpsData.logo_color.ToString();
        string biaozhi = "biaozhi_" + CorpsDataManager.m_corpsData.logo_icon.ToString();
        CorpsDataManager.m_roomdata[0] = new corpsRoomData();
        CorpsDataManager.m_roomdata[0].corpsicon = CorpsDataManager.m_corpsData.logo_icon;
        CorpsDataManager.m_roomdata[0].corpsname = CorpsDataManager.m_corpsData.name;
        CorpsDataManager.m_roomdata[0].corpsid = CorpsDataManager.m_corpsData.corps_id;
        CorpsDataManager.m_roomdata[0].corpscolor = CorpsDataManager.m_corpsData.logo_color;
        CorpsDataManager.m_roomdata[0].corpsflag = CorpsDataManager.m_corpsData.logo_pannel;

        var noTeam2 = m_team2id == 0;
        m_tran.Find("frame_corps_vote/modeChoose").gameObject.TrySetActive(!noTeam2);
        m_tran.Find("frame_corps_vote/CheckVote").gameObject.TrySetActive(!noTeam2);
    }

    protected void OnClkCorpHomepage(GameObject target, PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
        {
            UIManager.ShowPanel<PanelCorpsList>();
            TipsManager.Instance.showTips("你还未加入战队!");
            return;
        }

        SwitchFrame(FRAME_TYPE.TYPE_HOMEPAGE);
    }

    protected void OnClkCorpMatch(GameObject target, PointerEventData eventData)
    {

        if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
        {
            UIManager.ShowPanel<PanelCorpsList>();
            TipsManager.Instance.showTips("你还未加入战队!");
            return;
        }
        m_toggleCorpsHomepage.isOn = false;
        m_toggleCorpsActive.isOn = false;
        m_toggleCorpsMatch.isOn = false;
        SwitchFrame(FRAME_TYPE.TYPE_MATCH);
    }

    public void DoOption(string subType, p_corps_member_toc m_data)
    {
        if (subType == MemberOption.CHAT)
            OnClickChat(m_data);
        else if (subType == MemberOption.FOLLOW)
            OnClickFollow(null, null);
        else if (subType == MemberOption.LOOK_UP)
            OnLookUp(null, null);
    }
   

    void OnClickChat(p_corps_member_toc m_data)
    {
        bool isFriend = PlayerFriendData.singleton.isFriend(m_data.id);
        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = m_data.id;
        rcd.m_strName = m_data.name;
        rcd.m_iSex = 1;
        rcd.m_iLevel = m_data.level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = m_data.icon;
        ChatMsgTips msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = { msgTips };
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, this);
    }

    void OnClickFollow(GameObject target, PointerEventData eventData)
    {
        var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择跟随的人");
            return;
        }

        if (item.id == PlayerSystem.roleId)
        {
            return;
        }
        RoomModel.FollowFriend(item.id, false, false);
    }

    protected void OnClkCorpsLog(GameObject target, PointerEventData eventData)
    {
        m_logStrList.Clear();
        for (int i = 0; i < CorpsDataManager.m_lstLogs.Count; i++)
        {
            m_logStrList.Add(FormatLogStr(CorpsDataManager.m_lstLogs[i]));
        }

        string logStr = String.Join("\n", m_logStrList.ToArray());
        if (String.IsNullOrEmpty(logStr))
        {
            logStr = "\n";
        }
        UIManager.ShowTipMulTextPanel(logStr);
    }

    private string FormatLogStr(p_corps_log_toc m_data)
    {
        string strLog = "";
        string strTime = TimeUtil.GetTime(m_data.log_time).ToString("M月d号 HH:mm");
        strLog += strTime;
        strLog += " ";
        if (m_data.log_type == "agree_apply")
        {
            strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
            strLog += m_data.opr_name;
            strLog += "批准";
            strLog += m_data.mbr_name;
            strLog += "加入战队";
        }
        else if (m_data.log_type == "kick")
        {
            strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
            strLog += m_data.opr_name;
            strLog += "将";
            strLog += m_data.mbr_name;
            strLog += "踢出战队";
        }
        else if (m_data.log_type == "quit")
        {
            strLog += m_data.opr_name;
            strLog += "退出战队";
        }
        else if (m_data.log_type == "change_member_type")
        {
            strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
            strLog += m_data.opr_name;
            strLog += "修改了";
            strLog += m_data.mbr_name;
            strLog += "的职位";
        }
        else if (m_data.log_type == "update_notice")
        {
            strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
            strLog += m_data.opr_name;
            strLog += "更新了战队公告";
        }
        else if (m_data.log_type == "change_icon")
        {
            strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
            strLog += m_data.opr_name;
            strLog += "更新了战队图标";
        }
        else if (m_data.log_type == "set_leader")
        {
            strLog += "队长变更为";
            strLog += m_data.mbr_name;
        }
        else if (m_data.log_type == "impeach")
        {
            strLog += "由";
            strLog += m_data.mbr_name;
            strLog += "接任";
            strLog += m_data.opr_name;
            strLog += "成为新的队长";
        }
        else
        {
            return "";
        }
        return strLog;
    }

    protected void OnClkCorpsRank(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelCorpsList>();
    }
    

    protected void OnClkCorpsUpgrade(GameObject target, PointerEventData eventData)
    {
        p_corps_member_toc self = CorpsDataManager.GetSelf();
        if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
            return;


        if (self.member_type > (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER)
        {
            TipsManager.Instance.showTips("无权限升级战队!");
            return;
        }

        var config = ConfigManager.GetConfig<ConfigCorpsLevel>();
        ConfigCorpsLevelLine upLine = config.GetLine(CorpsDataManager.m_corpsData.level + 1);
        if (upLine == null)
        {
            TipsManager.Instance.showTips("战队已到最高级!");
            return;
        }
        if (CorpsDataManager.m_corpsData.fighting < upLine.Activeness)
        {

            UIManager.ShowTipPanel(string.Format("当前战队贡献值{0}/{1},贡献值不足,无法升级\n参加战斗可获得战队贡献值",
                                                    CorpsDataManager.m_corpsData.fighting.ToString(), upLine.Activeness.ToString())
                                                    );
            return;
        }

        string info = string.Format("当前战队建设值: {0}\n升级消耗建设值: {1}\n\n是否消耗建设值,将战队由{2}级提升至{3}级",
                                     CorpsDataManager.m_corpsData.tribute.ToString(), upLine.Tribute, CorpsDataManager.m_corpsData.level.ToString(),
                                     (CorpsDataManager.m_corpsData.level + 1).ToString());

        UIManager.ShowTipPanel(info, () =>
        {
            if (CorpsDataManager.m_corpsData.tribute < upLine.Tribute)
            {
                TipsManager.Instance.showTips("战队建设值不足!");
                return;
            }
            NetLayer.Send(new tos_corps_upgrade_level());
        }, null, true, true, "确定", "取消");
    }
    private void Toc_corps_upgrade_level(toc_corps_upgrade_level proto)
    {
        TipsManager.Instance.showTips("战队升级成功!");
    }

    protected void OnLookUp(GameObject target, PointerEventData eventData)
    {
        var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择查看信息的人");
            return;
        }

        if (item.id == PlayerSystem.roleId)
        {
            UIManager.ShowTipPanel("不能查看自己的信息");
            return;
        }

        PlayerFriendData.ViewPlayer(item.id);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    protected void OnClkBulitin(GameObject target, PointerEventData eventData)
    {
        p_corps_member_toc self = CorpsDataManager.GetSelf();
        if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
            return;

        if (self.member_type > (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER)
        {
            TipsManager.Instance.showTips("无权限修改公告");
            return;
        }

        UIManager.PopPanel<PanelBulitin>(null, true, this);
    }

    public void UpdateNotice(string notice)
    {
        Transform m_transHomepage = m_goCorpsHomepage.transform;
        CorpsDataManager.m_corpsData.notice = notice;
        m_transHomepage.FindChild("notice").GetComponent<Text>().text = CorpsDataManager.m_corpsData.notice;
    }


    void Toc_corpsmatch_result(toc_corpsmatch_result data)
    {
        
        if (data.team.Length >= 1)
        {
            if (data.team[0].name == "" || data.team[0].name == null) return;
            m_name1.text = data.team[0].name;
            string biaozhi = "biaozhi_" + data.team[0].icon;
            m_icon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[0].panel + "_" + "c" + data.team[0].color.ToString();
            m_flag1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            m_flag1.gameObject.TrySetActive(true);
        }
        if (data.team.Length >= 2)
        {
            if (data.team[1].name == "" || data.team[1].name == null) return;
            m_name2.text = data.team[1].name;
            string biaozhi = "biaozhi_" + data.team[1].icon;
            m_icon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[1].panel + "_" + "c" + data.team[1].color.ToString();
            m_flag2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            m_flag2.gameObject.TrySetActive(true);
        }

        if (data.team.Length >= 3)
        {
            if (data.team[2].name == "" || data.team[2].name == null) return;
            m_name3.text = data.team[2].name;
            string biaozhi = "biaozhi_" + data.team[2].icon;
            m_icon3.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[2].panel + "_" + "c" + data.team[2].color.ToString();
            m_flag3.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            m_flag3.gameObject.TrySetActive(true);
        }
    }

    void ShowCorpsContent()
    {
        curCorpsMatch = m_goCorpsMatchResult;
    }

    //刷新数据
    void CheckCorpsData()
    {
       
    }

    void Toc_player_attribute(CDict data)
    {
        OnUpdatePlayerData();
    }

    void Toc_corps_mission_data(toc_corps_mission_data data)
    {
        RefreshActiveList(ECorpsActiveUID.COOPERATE_TASK);
    }

    void Toc_corps_notify_mission(toc_corps_notify_mission data)
    {
        RefreshActiveList(ECorpsActiveUID.COOPERATE_TASK);
    }

    void Toc_corps_get_activeness_reward(toc_corps_get_activeness_reward data)
    {
        RefreshActiveList(ECorpsActiveUID.COOPERATE_TASK);
    }

    void Toc_corps_getlotterycounts(toc_corps_getlotterycounts data)
    {
        RefreshActiveList(ECorpsActiveUID.LOTTERY);
    }

    void Toc_corps_enter_mall(toc_corps_enter_mall data)
    {
        RefreshActiveList(ECorpsActiveUID.SHOP);
    }

    /// <summary>
    /// 积分、胜场信息
    /// </summary>
    /// <param name="scoreInfo"></param>
    void Toc_corpsmatch_score_info(toc_corpsmatch_score_info scoreInfo)
    {
        if (scoreInfo == null)
        {
            Logger.Log("战队联赛积分、胜场服务器信息为null");
            return;
        }
        m_corpsScoreText.text = "我队积分："  + scoreInfo.corps_score;
        m_myScoreText.text = "我的积分：" + scoreInfo.player_score;
        m_corpMatchRemain.text = scoreInfo.remain_rounds.ToString();
    }

    /// <summary>
    /// 积分、胜场排名
    /// </summary>
    /// <param name="data"></param>
    void Toc_player_my_ranks(toc_player_my_ranks data)
    {
        for (int i = 0; i < data.ranks.Length; i++)
        {
            if (data.ranks[i].name == "corpsmatch")
            {
                if (data.ranks[i].rank == 0)
                {
                    m_corpsRankText.text = "我队排名：未上榜";
                }
                else
                {
                    m_corpsRankText.text = "我队排名：" + data.ranks[i].rank.ToString();
                }
            }

            if (data.ranks[i].name == "corpsmatch_win_rounds")
            {
                if (data.ranks[i].rank == 0)
                {
                    m_myScoreRankText.text = "胜场排名：未上榜";
                }
                else
                {
                    m_myScoreRankText.text = "胜场排名：" + data.ranks[i].rank.ToString();
                }
            }
        }
    }
}
