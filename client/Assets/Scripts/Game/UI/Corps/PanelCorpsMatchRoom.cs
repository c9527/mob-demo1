﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
///////////////////////////////////////////
//Copyright (C): 4399 YiDao studio
//All rights reserved
//文件描述：战队联赛匹配界面
//创建者：hwl
//创建日期: 2015/12/29
///////////////////////////////////////////
 class PanelCorpsMatchRoom : PanelRoomBase
{
    enum ITEM_STATE
    {
        NORMAL = 0,
        HOLD = 1
    }

    ItemDropInfo[] m_hostOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             new ItemDropInfo { type = "OPERATION", subtype = "KICK", label = "踢出房间"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };

    ItemDropInfo[] m_guestOpArr = new[]
        {
             new ItemDropInfo { type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
             new ItemDropInfo { type = "OPERATION", subtype =  "ADDFRIEND", label = "加为好友"},
             new ItemDropInfo { type = "OPERATION", subtype = "CHAT", label = "私聊"},
        };
    
    class ItemData
    {
        public GameObject m_goItem;
        public p_player_info m_data;
        public ITEM_STATE m_state;
    }

    private Button m_btnMyCorp;
    private Text m_btnMyCorpText;
    private Text m_myRankText;
    private Text m_rankTitleText;
    private GameObject m_waitTextGo;

    private Toggle m_CorpRankToggle;
    private Toggle m_MyRankToggle;

    private const int TEAM_ITEM_SIZE = 5;
    private DataGrid m_corpsRankListDataGrid;
    private GameObject m_goTeamList;
    private ItemData[] m_listTeamItem;
    private p_player_info m_selected_data;
    private bool m_bIsOwner = false;
    private GameObject m_btnStartGo;
    private Text m_btnStartText;
    private GameObject m_CorpsRankListGo;
    private GameObject m_CorpsTop16;
    private int m_curPage = 1;
    private const int MAX_NUM = 5;
    private Text m_pageText;
    private int m_totalPage = 1;
    private List<ItemCorpsRankData> m_rankDataList = new List<ItemCorpsRankData>();
    private Text m_closeTipsText;
    private static Text m_leftTimesText;
    private static Text m_myScoreText;
    private static Text m_myCorpScoreText;
    private toc_room_info m_tempTeamListData;
    GameObject _poplist_mask;
    private List<Text> m_Top16NameTextList16 = new List<Text>();
    private Text m_TitleText;
    private Text m_nameTitleText;
    private static Dictionary<string, ItemRankData> m_dicMyRankData = new Dictionary<string, ItemRankData>();
    private string m_curRankTypeName = "";
    private static toc_corpsmatch_score_info m_scoreInfo;

    public PanelCorpsMatchRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.RankMatching
        });
        SetPanelPrefabPath("UI/Corps/PanelCorpsMatchRoom");
        AddPreLoadRes("UI/Corps/ItemCorpsRank", EResType.UI);
        AddPreLoadRes("UI/Corps/PanelCorpsMatchRoom", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/PanelRoleDesc", EResType.UI);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    private float m_startCheckTime = 0;
    private bool isCheck = true;
    bool isOpen = false;
    public override void Update()
    {
        base.Update();
        if(IsOpen() == false)
        {
            return;
        }
        if (Time.realtimeSinceStartup - m_startCheckTime > 10)
        {
            m_startCheckTime = Time.realtimeSinceStartup;
            isCheck = true;
        }
        if (isCheck == true)
        {
            DateTime m_curTime = TimeUtil.GetNowTime();
            if ((m_curTime.Hour >= 19) && (m_curTime.Hour < 20 || (m_curTime.Hour == 20 && m_curTime.Minute == 0 && m_curTime.Second == 0)))
            {
                isOpen = true;
            }
            if (isOpen == true)
            {
                m_btnStartText.text = "开始匹配";
                Util.SetGoGrayShader(m_btnStartGo, false);
            }
            else
            {
                m_btnStartText.text = "19:00~20:00开战";
                Util.SetGoGrayShader(m_btnStartGo, true);
            }
            showInviteBtn(isOpen);
            isCheck = false;
        }
    }

    protected override void OnStartClick(GameObject target, PointerEventData eventdata)
    {
        if(isOpen == false)
        {
            TipsManager.Instance.showTips("战队联赛19:00~20:00开启！！");
            return;
        }
        base.OnStartClick(target, eventdata);
    }

    private void showInviteBtn(bool isOpen)
    {
        for( int i = 0; i < m_listTeamItem.Length; i++ )
        {
            if (m_listTeamItem[i] != null)
            {
                if (m_listTeamItem[i].m_data != null)
                {
                    m_listTeamItem[i].m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(false);
                }
                else
                {
                    m_listTeamItem[i].m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(isOpen);
                }
            }
        }
    }

    public override void Init()
    {
        base.Init();
        _poplist_mask = m_tran.Find("poplistpanel").gameObject;
        _poplist_mask.TrySetActive(false);
        m_tran.Find("optionframe/CorpsRankList/RankGo/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_corpsRankListDataGrid = m_tran.Find("optionframe/CorpsRankList/RankGo/ScrollDataGrid/content").GetComponent<DataGrid>();
        m_corpsRankListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemCorpsRank"), typeof(ItemCorpsRankInfo));

        m_pageText = m_tran.Find("optionframe/CorpsRankList/RankGo/page/pageText").GetComponent<Text>();
        m_pageText.text = "1/1";

        m_CorpsRankListGo = m_tran.Find("optionframe/CorpsRankList").gameObject;
        m_CorpsTop16 = m_tran.Find("optionframe/CorpsTop16").gameObject;
        for (int i = 0; i < 16; i++ )
        {
            Text txt = m_CorpsTop16.transform.Find("operation/Top16Name" + i).GetComponent<Text>();
            m_Top16NameTextList16.Add(txt);
        }
            m_listTeamItem = new ItemData[TEAM_ITEM_SIZE];
        m_goTeamList = m_tran.Find("pageframe/team/teamlist").gameObject;
        m_goTeamList.TrySetActive(false);

        m_btnMyCorp = m_tran.Find("optionframe/CorpsRankList/btnMyCorp").GetComponent<Button>();
        m_btnMyCorpText = m_btnMyCorp.transform.Find("Text").GetComponent<Text>();
        m_rankTitleText = m_tran.Find("optionframe/CorpsRankList/myrank").GetComponent<Text>();
        m_myRankText = m_tran.Find("optionframe/CorpsRankList/myrank/rank").GetComponent<Text>();
        m_nameTitleText = m_tran.Find("optionframe/CorpsRankList/RankGo/Title/Text2").GetComponent<Text>();
        m_TitleText = m_tran.Find("optionframe/CorpsRankList/RankGo/Title/Text3").GetComponent<Text>();
        m_waitTextGo = m_tran.Find("optionframe/WaitText").gameObject;
        initRoomMateList();
        InitRankToggle();
        initInfoText();
        InitStart();
    }

    private void initInfoText()
    {
        m_closeTipsText = m_tran.Find("pageframe/myInfoText/TipsText").GetComponent<Text>();
        m_leftTimesText = m_tran.Find("pageframe/myInfoText/leftTimesText").GetComponent<Text>();
        m_myCorpScoreText = m_tran.Find("pageframe/myInfoText/myCorpScoreText").GetComponent<Text>();
        m_myScoreText = m_tran.Find("pageframe/myInfoText/myScoreText").GetComponent<Text>();
    }

    private void InitRankToggle()
    {
        m_CorpRankToggle = m_tran.Find("optionframe/CorpsRankList/tabbar/tabCorpsRank").GetComponent<Toggle>();
        m_MyRankToggle = m_tran.Find("optionframe/CorpsRankList/tabbar/tabMyRank").GetComponent<Toggle>();
        m_CorpRankToggle.isOn = true;
        m_MyRankToggle.isOn = false;
    }

    private void InitStart()
    {
        m_btnStartGo = m_tran.Find("optionframe/btnStart").gameObject;
        m_btnStartText = m_btnStartGo.transform.Find("Text").GetComponent<Text>();
    }


    private void initRoomMateList()
    {
        for (int i = 1; i <= TEAM_ITEM_SIZE; ++i)
        {
            GameObject item = m_goTeamList.transform.Find("teammate" + i.ToString()).gameObject;
            GameObject btn = item.transform.Find("ask_tip").gameObject;
            UGUIClickHandler.Get(btn).onPointerClick += OnClickInviteCorpMate;
            UGUIClickHandler.Get(item).onPointerClick += ShowInfoMenu;

            ItemData newItem = new ItemData();
            newItem.m_goItem = item;
            newItem.m_data = null;
            newItem.m_state = ITEM_STATE.NORMAL;

            m_listTeamItem[i - 1] = newItem;

            InitTeamItem(newItem);
        }
    }

    private void InitTeamItem(ItemData item)
    {
        item.m_goItem.transform.Find("head").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("armylvl").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("inner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("owner").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("goldace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("silverace").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("acheive").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(true);
        item.m_goItem.transform.Find("vip").gameObject.TrySetActive(false);
        item.m_data = null;
        item.m_state = ITEM_STATE.NORMAL;

    }

    void Toc_room_info(toc_room_info data)
    {
        OnUpdateTeamList(RoomModel.RoomInfo);
    }

    void OnUpdateTeamList(toc_room_info teamData)
    {
        m_goTeamList.TrySetActive(true);
        if(teamData == null)
        {
            return;
        }
        p_player_info[] playerList = teamData.players;
        var leaderId = teamData.leader;

        if (leaderId == PlayerSystem.roleId)
        {
            m_bIsOwner = true;
        }
        else
        {
            m_bIsOwner = false;
        }
        for (int n = 0; n < TEAM_ITEM_SIZE; ++n)
        {
            InitTeamItem(m_listTeamItem[n]);
        }

        bool bFindLeader = false;

        for (int index = 0; index < playerList.Length; ++index)
        {
            var teamerData = playerList[index];
            var playerid = teamerData.id;

            if (playerid == leaderId)
            {
                bFindLeader = true;

                ItemData item = m_listTeamItem[0];

                HoldTeamListSlot(item, teamerData);
            }
            else
            {
                if (!bFindLeader)
                {
                    if (index + 1 < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index + 1];

                        HoldTeamListSlot(item, teamerData);
                    }

                }
                else
                {
                    if (index < TEAM_ITEM_SIZE)
                    {
                        ItemData item = m_listTeamItem[index];

                        HoldTeamListSlot(item, teamerData);
                    }
                }
            }
        }

        ItemData ownerItem = m_listTeamItem[0];
        if (ownerItem.m_state == ITEM_STATE.HOLD)
        {
            ownerItem.m_goItem.transform.Find("owner").gameObject.TrySetActive(true);
        }

        ShowOperationPri();
    }

    private void ShowOperationPri()
    {

        if (m_bIsOwner)
        {
            m_btnStartGo.TrySetActive(true);
            m_waitTextGo.TrySetActive(false);
        }
        else
        {
            m_btnStartGo.TrySetActive(false);
            m_waitTextGo.TrySetActive(true);
        }
    }

    void HoldTeamListSlot(ItemData item, p_player_info data)
    {
        if (item.m_state == ITEM_STATE.HOLD)
            return;

        item.m_state = ITEM_STATE.HOLD;
        item.m_data = data;
        item.m_goItem.transform.Find("ask").gameObject.TrySetActive(false);
        item.m_goItem.transform.Find("ask_tip").gameObject.TrySetActive(false);
        GameObject goHead = item.m_goItem.transform.Find("head").gameObject;
        goHead.TrySetActive(true);
        Image headicon = goHead.transform.Find("icon").GetComponent<Image>();
        headicon.SetSprite(ResourceManager.LoadRoleIconBig(data.icon));
        headicon.SetNativeSize();
        headicon.transform.localScale = new Vector3(0.8f, 0.8f, 1);

        GameObject goArmyLvl = item.m_goItem.transform.Find("armylvl").gameObject;
        goArmyLvl.TrySetActive(true);
        Image armylvl = goArmyLvl.transform.Find("army").GetComponent<Image>();
        armylvl.SetSprite(ResourceManager.LoadArmyIcon(data.level));

        Text txtArmyLvl = goArmyLvl.transform.Find("level").GetComponent<Text>();
        txtArmyLvl.text = string.Format("{0}级", data.level);

        GameObject goInner = item.m_goItem.transform.Find("inner").gameObject;
        goInner.TrySetActive(true);

        Text name = goInner.transform.Find("name").GetComponent<Text>();
        name.text = PlayerSystem.GetRoleNameFull(data.name, data.id);

        GameObject goName = goInner.transform.Find("name").gameObject;
        GameObject goVip = item.m_goItem.transform.Find("vip").gameObject;
        Image imgVip = item.m_goItem.transform.Find("vip").GetComponent<Image>();

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)data.vip_level;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            goVip.TrySetActive(false);
            Vector2 vipSizeData = goVip.GetComponent<RectTransform>().sizeDelta;
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            goVip.TrySetActive(true);
            imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            imgVip.SetNativeSize();
        }

        GameObject goVipName = goInner.transform.Find("vipname").gameObject;
        goVipName.TrySetActive(false);
        var gold_ace = item.m_goItem.transform.Find("goldace");
        gold_ace.gameObject.TrySetActive(true);
        gold_ace.Find("num").GetComponent<Text>().text = data.titles[1].cnt.ToString();
        gold_ace.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[1].title, data.titles[1].type)));
        var silver_ace = item.m_goItem.transform.Find("silverace");
        silver_ace.gameObject.TrySetActive(true);
        silver_ace.Find("num").GetComponent<Text>().text = data.titles[2].cnt.ToString();
        silver_ace.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[2].title, data.titles[2].type)));
        var mvp = item.m_goItem.transform.Find("acheive");
        mvp.gameObject.TrySetActive(true);
        mvp.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(data.titles[0].title, data.titles[0].type)));
        mvp.Find("num").GetComponent<Text>().text = data.titles[0].cnt.ToString();
        foreach( p_title_info info in data.titles)
        {
            if (info.title == "mvp")
            {
                mvp.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "gold_ace")
            {
                gold_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
            else if (info.title == "silver_ace")
            {
                silver_ace.Find("num").GetComponent<Text>().text = info.cnt.ToString();
            }
        }
    }

    //邀请战队人员
    private void OnClickInviteCorpMate(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_room_invitelist { is_friend = false, stage = 0, stage_type="" });
    }

    private void ShowInfoMenu(GameObject target, PointerEventData eventData)
    {
        for (int index = 0; index < TEAM_ITEM_SIZE; ++index)
        {
            ItemData item = m_listTeamItem[index];
            if (item.m_goItem.gameObject == target)
            {
                m_selected_data = item.m_data;
                if (item.m_state == ITEM_STATE.HOLD)
                {
                    if (m_selected_data != null && m_selected_data.id != PlayerSystem.roleId)
                    {
                        if (m_bIsOwner)
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_hostOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                        else
                        {
                            _poplist_mask.TrySetActive(true);
                            UIManager.ShowDropList(m_guestOpArr, OnOpListItemSelected, m_tran, item.m_goItem, 220, 30, UIManager.DropListPattern.Ex);
                        }
                    }
                }
                break;
            }
        }
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        _poplist_mask.TrySetActive(false);
        if (m_selected_data == null)
        {
            return;
        }
        var uid = m_selected_data.id;
        if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(uid);
        }
        else if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(uid);
        }
        else if (data.subtype == "CHAT")
        {
            PlayerRecord rcd = new PlayerRecord();
            rcd.m_iPlayerId = uid;
            rcd.m_strName = PlayerSystem.GetRoleNameFull(m_selected_data.name, m_selected_data.id);
            rcd.m_iLevel = m_selected_data.level;
            rcd.m_bOnline = true;
            rcd.m_isFriend = PlayerFriendData.singleton.isFriend(uid);
            rcd.m_iHead = m_selected_data.icon;
            ChatMsgTips msgTips = new ChatMsgTips();
            msgTips.m_rcd = rcd;
            msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
            UIManager.PopChatPanel<PanelChatSmall>(new object[] { msgTips });
        }
        else if (data.subtype == "KICK")
        {
            NetLayer.Send(new tos_room_kick_player { id = uid });
        }
    }

    /// <summary>
    /// 显示16强名单还是战队排行
    /// </summary>
    private void showCorpsRankList()
    {
        bool isShow = false;
        m_CorpsRankListGo.TrySetActive(isShow);
        m_CorpsTop16.TrySetActive(!isShow);
    }

    private void setCorpsRankData(List<ItemCorpsRankData> dataList)
    {
        List<ItemCorpsRankData> tempList = new List<ItemCorpsRankData>();
        m_rankDataList = dataList;
        for (int i = 0; i < MAX_NUM; i++ )
        {
            if (i <= dataList.Count - 1)
            {
                tempList.Add(dataList[i]);
            }
            
        }
        m_corpsRankListDataGrid.Data = tempList.ToArray();
        if (dataList.Count == 0)
        {
            m_curPage = 1;
        }
       
        m_pageText.text = m_curPage + "/" + m_totalPage;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_poplist_mask).onPointerClick += OnShowMenuHandler;
        UGUIClickHandler.Get(m_CorpRankToggle.gameObject).onPointerClick += OnRankTabClick;
        UGUIClickHandler.Get(m_MyRankToggle.gameObject).onPointerClick += OnRankTabClick;
        UGUIClickHandler.Get(m_tran.Find("optionframe/CorpsRankList/RankGo/page/left").gameObject).onPointerClick += OnClkPageup;
        UGUIClickHandler.Get(m_tran.Find("optionframe/CorpsRankList/RankGo/page/right").gameObject).onPointerClick += OnClkPagedown;
        UGUIClickHandler.Get(m_tran.Find("pageframe/ruleintro")).onPointerClick += delegate { UIManager.PopRulePanel("CorpsMatch"); };
        UGUIClickHandler.Get(m_btnMyCorp.gameObject).onPointerClick += onMyCorp;
    }

    private void OnShowMenuHandler(GameObject target = null, PointerEventData eventData = null)
    {
        UIManager.HideDropList();
        _poplist_mask.TrySetActive(false);
    }

     /// <summary>
     /// 我的排名
     /// </summary>
     /// <param name="target"></param>
     /// <param name="eventData"></param>
     private void onMyCorp(GameObject target, PointerEventData eventData)
    {
        if (m_dicMyRankData.ContainsKey(m_curRankTypeName))
        {
            ItemRankData myInfo = m_dicMyRankData[m_curRankTypeName];
            if (myInfo != null)
            {
                if (myInfo.queue == 0)
                {
                    UIManager.ShowTipPanel("很遗憾你未上榜");
                }
                else
                {
                    int idx = myInfo.queue % MAX_NUM;
                    if (idx == 0)
                        idx = myInfo.queue / MAX_NUM;
                    else
                        idx = myInfo.queue / MAX_NUM + 1;
                    m_curPage = idx;
                    NetLayer.Send(new tos_player_rank_list() { name = m_curRankTypeName, page = m_curPage });
                   /* int iPage = myInfo.queue / MAX_NUM;
                    int j = myInfo.queue % MAX_NUM;
                    if (j > 0)
                        iPage++;

                    if (PanelRankList.HasRankData(m_curRankType, iPage))
                    {
                        int idx = myInfo.queue % MAX_NUM;
                        if (idx == 0)
                            idx = myInfo.queue % MAX_NUM + 1;
                        m_curPage = idx;
                        NetLayer.Send(new tos_player_rank_list() { name = m_curRankTypeName, page = m_curPage });
                    }
                    else
                    {
                        PanelRankList.SendMsgToGetData(m_curRankType, iPage);
                    }*/
                }
            }
        }
    }

   
    /// <summary>
    /// 排行类型
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnRankTabClick(GameObject target, PointerEventData eventData)
    {
        m_curPage = 1;
        
        if (target.name == "tabCorpsRank")
        {
            m_rankTitleText.text = "战队排名：";
            m_curRankTypeName = "corpsmatch";
            m_TitleText.text = "积分";
            m_nameTitleText.text = "战队";
            m_btnMyCorpText.text = "我的战队";
        }
        else if (target.name == "tabMyRank")
        {
            m_TitleText.text = "胜场";
            m_nameTitleText.text = "名字";
            m_rankTitleText.text = "我的排名：";
            m_curRankTypeName = "corpsmatch_win_rounds";
            m_btnMyCorpText.text = "我的胜场";
        }
        NetLayer.Send(new tos_player_rank_list() { name = m_curRankTypeName, page = m_curPage });
        NetLayer.Send(new tos_player_my_ranks());//我的排名
    }

   
    private void OnClkPageup(GameObject target, PointerEventData eventData)
    {
        if(m_curPage <= 1)
        {
            return;
        }
        m_curPage -= 1;
        NetLayer.Send(new tos_player_rank_list() { name = getRankTypeName(), page = m_curPage });
    }

    private void OnClkPagedown(GameObject target, PointerEventData eventData)
    {
        if (m_curPage >= m_totalPage)
        {
            return;
        }
        m_curPage += 1;
        NetLayer.Send(new tos_player_rank_list() { name = getRankTypeName(), page = m_curPage });
    }

    private string getRankTypeName()
    {
        if(m_MyRankToggle.isOn == true)
        {
            return "corpsmatch_win_rounds";
        }
        else
        {
            return "corpsmatch";
        }
    }

    /// <summary>
    /// 是否显示预选赛结束提示
    /// </summary>
    /// <param name="isShow"></param>
    private void showCloseTips(bool isShow)
    {
        m_closeTipsText.gameObject.TrySetActive(isShow);
        m_myScoreText.gameObject.TrySetActive(!isShow);
        m_myCorpScoreText.gameObject.TrySetActive(!isShow);
    }

    public override void OnShow()
    {
        base.OnShow();
        m_startCheckTime = 0;
       if (!RoomModel.IsInRoom)
            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.corpsmatch_group), stage = 0 });
        m_tempTeamListData = RoomModel.RoomInfo;
        if (m_tempTeamListData != null)
        {
            OnUpdateTeamList(m_tempTeamListData);
            m_tempTeamListData = null;
        }
        //请求我的积分，我的战队积分、剩余次数信息
        NetLayer.Send(new tos_corpsmatch_score_info());
        //请求战队联赛排行榜数据
        NetLayer.Send(new tos_player_rank_list() { name = "corpsmatch", page = 1});
        m_rankTitleText.text = "战队排名：";
        m_btnMyCorpText.text = "我的战队";
        m_CorpRankToggle.isOn = true;
        m_MyRankToggle.isOn = false;
        m_curRankTypeName = getRankTypeName();
        if (CorpsDataManager.m_matchStage == 1 )
        {
            //NetLayer.Send(new tos_corpsmatch_top16());
            m_CorpsRankListGo.TrySetActive(false);
            m_CorpsTop16.TrySetActive(true);
            showCorpsmatchTop16();
        }
        NetLayer.Send(new tos_player_my_ranks());//我的排名
        onShowScore();
        m_curPage = 1;
        showCloseTips(CorpsDataManager.m_matchStage == 1);
    }

    public override void OnHide()
    {
        base.OnHide();
        m_dicMyRankData.Clear();
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorpsEnter>();
    } 

    public override void OnDestroy()
    {
    }

    /// <summary>
    /// 战队联赛战队积分排行榜
    /// </summary>
    /// <param name="data"></param>
    void Toc_player_rank_list(toc_player_rank_list data)
    {
        List<ItemCorpsRankData> items = new List<ItemCorpsRankData>();
        for (int i = 0; i < data.list.Length; i++)
        {
            ItemCorpsRankData item = new ItemCorpsRankData();
            item.id = data.list[i].id;
            if (data.name == "corpsmatch")
            {
                item.name = data.list[i].corps_name;
            }
            else
            {
                item.name = data.list[i].name;
            }
            item.rank = data.start + i;
            item.score = data.list[i].val;
            items.Add(item);
        }
        m_totalPage = data.size % MAX_NUM == 0 ? data.size / MAX_NUM : (data.size / MAX_NUM + 1);
        m_totalPage = m_totalPage == 0 ? 1 : m_totalPage;
        setCorpsRankData(items);
        
    }

    /// <summary>
    /// 剩余次数信息
    /// </summary>
    /// <param name="scoreInfo"></param>
    static void Toc_corpsmatch_score_info(toc_corpsmatch_score_info scoreInfo)
    {
        CorpsDataManager.m_CorpsMatcheftTimes = scoreInfo.remain_rounds;
        if (CorpsDataManager.IsOpenByCorpsPanel == true || CorpsDataManager.IsOpenByHallBattlePanel == true)
        {
            //社交界面里请求的，不监听
            return;
        }
        m_scoreInfo = scoreInfo;
        if (UIManager.GetPanel<PanelCorpsMatchRoom>() != null)
        {
            onShowScore();
        }
        
        if (scoreInfo.remain_rounds > 0)
        {
            if (UIManager.IsOpen<PanelCorpsMatchRoom>() == false && UIManager.IsOpen<PanelCorpsEnter>() == false)
                UIManager.ShowPanel<PanelCorpsMatchRoom>();
        }
        else
        {
            var curTime = TimeUtil.GetNowTime();
            if (curTime.Hour >= 19 && curTime.Hour < 21)
            {
                TipsManager.Instance.showTips("今日参赛次数已经用完!");
            }
            NetLayer.Send(new tos_room_leave());
            if (UIManager.GetPanel<PanelCorpsEnter>() == null)
            {
                UIManager.ShowPanel<PanelCorpsEnter>();
            }
        }
    }

    private static void onShowScore()
    {
        if (m_scoreInfo != null && UIManager.IsOpen<PanelCorpsMatchRoom>())
        {
            m_leftTimesText.text = "<color='#B5B5B5'>剩余次数： </color>" + "<color='#B5B5B5'>" + m_scoreInfo.remain_rounds + "</color>";
            m_myCorpScoreText.text = "<color='#B5B5B5'>我队积分： </color>" + "<color='#B5B5B5'>" + m_scoreInfo.corps_score + "</color>";
            m_myScoreText.text = "<color='#B5B5B5'>我的积分： </color>" + "<color='#B5B5B5'>" + m_scoreInfo.player_score + "</color>";
        }
    }

     /// <summary>
     /// 显示16强名单
     /// </summary>
     /// <param name="data"></param>
    void showCorpsmatchTop16()
    {
        for (int i = 0; i < 16;i++ )
        {
            m_Top16NameTextList16[i].text = "";
        }
        p_corpsmatch_member[] top16NameList16 = CorpsDataManager.m_Top16NameList16;
        if (top16NameList16 == null) 
        {
            return;
        }
            
        for (int i = 0; i < top16NameList16.Length; i++)
        {
            m_Top16NameTextList16[i].text = top16NameList16[i].name;
        }
    }


     /// <summary>
     /// 我的排名、战队排名
     /// </summary>
     /// <param name="data"></param>
     void Toc_player_my_ranks(toc_player_my_ranks data)
     {
         for (int i = 0; i < data.ranks.Length; i++)
         {

             if (m_CorpRankToggle.isOn == true)
             {
                 if (data.ranks[i].name == "corpsmatch")
                 {
                     ItemRankData rankData = new ItemRankData();
                     rankData.queue = data.ranks[i].rank;
                     rankData.val = data.ranks[i].val;
                     rankData.ext = data.ranks[i].ext;
                     m_dicMyRankData[data.ranks[i].name] = rankData;

                     if (data.ranks[i].rank == 0)
                     {
                         m_myRankText.text = "未上榜";
                     }
                     else
                     {
                         m_myRankText.text = data.ranks[i].rank.ToString();
                     }
                     break;
                 }
             }

             if (m_MyRankToggle.isOn == true)
             {
                 if (data.ranks[i].name == "corpsmatch_win_rounds")
                 {
                     ItemRankData rankData = new ItemRankData();
                     rankData.queue = data.ranks[i].rank;
                     rankData.val = data.ranks[i].val;
                     rankData.ext = data.ranks[i].ext;
                     m_dicMyRankData[data.ranks[i].name] = rankData;

                     if (data.ranks[i].rank == 0)
                     {
                         m_myRankText.text = "未上榜";
                     }
                     else
                     {
                         m_myRankText.text = data.ranks[i].rank.ToString();
                     }
                     break;
                 }
             }
         }
    }
}
