﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI.Corps
{
    public class PanelCorpsList : BasePanel
    {
        private GameObject m_goCorpsList;
        private DataGrid m_corpsListDataGrid;
        private GameObject m_pageBtns;
        private int CREATE_CORPS_LVL;
        private InputField _search_txt;
        private GameObject m_toggleRecommend;
        private GameObject m_toggleRank;
        private GameObject m_searchBtn;


        public PanelCorpsList()
        {
            SetPanelPrefabPath("UI/Corps/PanelCorpsList");
            AddPreLoadRes("UI/Corps/PanelCorpsList", EResType.UI);
            AddPreLoadRes("UI/Corps/ItemCorps", EResType.UI);
            AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        }

        public override void Init()
        {
            CREATE_CORPS_LVL = ConfigMisc.GetInt("create_corps_reqlv");
            m_goCorpsList = m_tran.Find("frame_corps_list").gameObject;
            m_pageBtns = m_tran.Find("frame_corps_list/operation/page").gameObject;
            _search_txt = m_tran.Find("frame_corps_list/operation/search").GetComponent<InputField>();
            m_toggleRecommend = m_tran.Find("frame_corps_list/title/ToggleRecommend").gameObject;
            m_toggleRank = m_tran.Find("frame_corps_list/title/ToggleRank").gameObject;
            m_searchBtn = m_tran.Find("frame_corps_list/operation/search").gameObject;

            m_tran.Find("frame_corps_list/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
            m_corpsListDataGrid = m_tran.Find("frame_corps_list/ScrollDataGrid/content").GetComponent<DataGrid>();
            m_corpsListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemCorps"), typeof (ItemCorpsInfo));
            m_corpsListDataGrid.useLoopItems = true;
        }

        public override void InitEvent()
        {
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/title/ToggleRecommend").gameObject).onPointerClick +=
                OnToggleRecommendClick;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/title/ToggleRank").gameObject).onPointerClick +=
                OnToggleRankClick;

            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/search/search_btn").gameObject).onPointerClick
                += OnSearchBtnClick;
                    UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/look_up").gameObject).onPointerClick += OnClkCorpsLookup;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/my_corps").gameObject).onPointerClick +=
                OnClkCorpsMy;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/add_in").gameObject).onPointerClick +=
                OnClkCorpsAdd;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/page/left").gameObject).onPointerClick +=
                OnClkCorpsPagedown;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_list/operation/page/right").gameObject).onPointerClick +=
                OnClkCorpsPageup;
            AddEventListener(GameEvent.UI_CHANGE_CORPS, UpdateView);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener<BaseCorpsData>(GameEvent.UI_DOUBLE_CLICK_CORPSLIST, OnDoubleClickCorpsList);
#endif
        }

        private void OnDoubleClickCorpsList(BaseCorpsData corps)
        {
            if (CorpsDataManager.m_corpsData.corps_id != 0)
            {
                UIManager.ShowTipPanel("请先退出战队");
                return;
            }
            tos_corps_apply_enter msg = new tos_corps_apply_enter();
            msg.corps_id = corps.corps_id;
            NetLayer.Send(msg);
        }


        private void OnToggleRecommendClick(GameObject go, PointerEventData eventData)
        {
            m_pageBtns.SetActive(false);
            m_searchBtn.TrySetActive(false);
            UpdateCorspRecommendList();
        }

        private void OnToggleRankClick(GameObject go, PointerEventData eventData)
        {
            m_pageBtns.SetActive(true);
            m_searchBtn.TrySetActive(true);
            UpdateCorpsList();
        }

        public override void OnShow()
        {
            CorpsDataManager.m_bGetCorpsList = true;
            
            if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
            {
                if (CorpsDataManager.recommendList.Count == 0)
                {
                    NetLayer.Send(new tos_corps_recmd_list());
                }   
                m_toggleRecommend.TrySetActive(true);
                m_toggleRank.TrySetActive(true);
                m_toggleRecommend.GetComponent<Toggle>().isOn = true;
                m_pageBtns.TrySetActive(false);
                m_searchBtn.TrySetActive(false);
                UpdateCorspRecommendList();
            }
            else
            {
                m_toggleRecommend.TrySetActive(false);
                m_toggleRank.TrySetActive(true);
                m_toggleRank.GetComponent<Toggle>().isOn = true;
                m_pageBtns.TrySetActive(true);
                m_searchBtn.TrySetActive(true);
                UpdateCorpsList();
            }
        }

        public void UpdateView()
        {
            if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
            {
                m_toggleRecommend.TrySetActive(true);
            }
            else
            {
                m_toggleRecommend.TrySetActive(false);
            }

            if (m_toggleRank.GetComponent<Toggle>().isOn)
            {
                UpdateCorpsList();
            }
        }

        public void UpdateCorspRecommendList()
        {
            if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
            {
                m_goCorpsList.transform.FindChild("operation/my_corps/Text").GetComponent<Text>().text = "创建战队";
                m_corpsListDataGrid.Data = CorpsDataManager.recommendList.ToArray();
            }
           
        }

        protected void OnClkCorpsLookup(GameObject target, PointerEventData eventData)
        {
            BaseCorpsData item;
            item = m_corpsListDataGrid.SelectedData<BaseCorpsData>();
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择战队");
                return;
            }

            tos_corps_view_info msg = new tos_corps_view_info();
            msg.corps_id = item.corps_id;
            NetLayer.Send(msg);
        }

        public void UpdateCorpsList()
        {
            if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
            {
                m_goCorpsList.transform.FindChild("operation/my_corps/Text").GetComponent<Text>().text = "创建战队";
            }
            else
            {
                m_goCorpsList.transform.FindChild("operation/my_corps/Text").GetComponent<Text>().text = "我的战队";
            }

            if (CorpsDataManager.m_dicCorps.ContainsKey(CorpsDataManager.m_iCurCorpsPage))
            {
                ResetCorpsListPage();
                List<BaseCorpsData> lstCorps = CorpsDataManager.m_dicCorps[CorpsDataManager.m_iCurCorpsPage];
                m_corpsListDataGrid.Data = lstCorps.ToArray();
                if (!string.IsNullOrEmpty(PanelCorps.search_key))
                {
                    string key = PanelCorps.search_key;
                    PanelCorps.search_key = null;
                    int index_for_id = -1;
                    int index_for_name = -1;
                    for (int i = 0; i < lstCorps.Count; i++)
                    {
                        if (lstCorps[i].corps_id.ToString() == key)
                        {
                            index_for_id = i;
                            break;
                        }
                        if (lstCorps[i].corps_name == key)
                        {
                            index_for_name = i;
                        }
                    }
                    int index = index_for_id != -1 ? index_for_id : index_for_name;
                    if (index != -1)
                    {
                        m_corpsListDataGrid.ShowItemOnTop(index);
                    }
                }
            }
        }

        protected void ResetCorpsListPage()
        {
            m_tran.Find("frame_corps_list/operation/page/page").GetComponent<Text>().text =
                CorpsDataManager.m_iCurCorpsPage + "/" + CorpsDataManager.m_iPageNum;
        }

        protected void OnClkCorpsPageup(GameObject target, PointerEventData eventData)
        {
            if (CorpsDataManager.m_iCurCorpsPage >= CorpsDataManager.m_iPageNum)
            {
                TipsManager.Instance.showTips("已经是最后一页了");
                return;
            }

            CorpsDataManager.m_iCurCorpsPage++;
            if (CorpsDataManager.m_dicCorps.ContainsKey(CorpsDataManager.m_iCurCorpsPage))
            {
                UpdateCorpsList();
            }
            else
            {
                var msgList = new tos_corps_get_list();
                msgList.page = CorpsDataManager.m_iCurCorpsPage;
                NetLayer.Send(msgList);
            }
        }

        protected void OnClkCorpsPagedown(GameObject target, PointerEventData eventData)
        {
            if (CorpsDataManager.m_iCurCorpsPage <= 1)
            {
                TipsManager.Instance.showTips("已经是第一页了");
                return;
            }
            CorpsDataManager.m_iCurCorpsPage--;
            if (CorpsDataManager.m_dicCorps.ContainsKey(CorpsDataManager.m_iCurCorpsPage))
            {
                UpdateCorpsList();
            }
            else
            {
                var msgList = new tos_corps_get_list();
                msgList.page = CorpsDataManager.m_iCurCorpsPage;
                NetLayer.Send(msgList);
            }
        }

        private void OnSearchBtnClick(GameObject target, PointerEventData eventData)
        {
            if (!string.IsNullOrEmpty(_search_txt.text))
            {
                PanelCorps.search_key = _search_txt.text.Trim();
                NetLayer.Send(new tos_corps_search_name {corps_name = PanelCorps.search_key});
//                HidePanel();
            }
            else
            {
                TipsManager.Instance.showTips("请输入正确的战队名称或者ID");
            }
        }

        protected void OnClkCorpsMy(GameObject target, PointerEventData eventData)
        {
            if (CorpsDataManager.m_corpsData.corps_id == 0)
            {
                if (PlayerSystem.roleData.level < CREATE_CORPS_LVL)
                {
                    TipsManager.Instance.showTips(string.Format("创建战队需要{0}级", CREATE_CORPS_LVL));
                    return;
                }
                UIManager.PopPanel<PanelCorpsCreate>(null, true, this);
            }
            else
            {
                PanelCorps.search_key = CorpsDataManager.m_corpsData.corps_id.ToString();
                NetLayer.Send(new tos_corps_search_name {corps_name = PanelCorps.search_key});
            }
        }

        private void Toc_corps_view_info(toc_corps_view_info proto)
        {
            UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, this);
        }

        protected void OnClkCorpsAdd(GameObject target, PointerEventData eventData)
        {
            if (CorpsDataManager.m_corpsData.corps_id != 0)
            {
                UIManager.ShowTipPanel("请先退出战队");
                return;
            }
            BaseCorpsData item;
            item = m_corpsListDataGrid.SelectedData<BaseCorpsData>();
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择战队");
                return;
            }

            var msg = new tos_corps_apply_enter();
            msg.corps_id = item.corps_id;
            NetLayer.Send(msg);
        }

        public override void OnHide()
        {
            CorpsDataManager.m_bGetCorpsList = false;
        }

        public override void OnBack()
        {
            if (CorpsDataManager.m_corpsData == null || CorpsDataManager.m_corpsData.corps_id == 0)
            {
                UIManager.ShowPanel<PanelSocity>();
            }
            else
            {
                UIManager.ShowPanel<PanelCorps>();
            }
        }

        public override void OnDestroy()
        {
        }

        public override void Update()
        {
        }
    }
}
