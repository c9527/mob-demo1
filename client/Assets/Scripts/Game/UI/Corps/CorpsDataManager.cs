﻿using System.Linq;
using Assets.Scripts.Game.UI.Corps;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BaseCorpsData
{
    public int m_iQueue;
    public long corps_id;
    public string corps_name;
    public long leader_id;
    public string leader_name;
    public int member_cnt;
    public int fighting;    // 战斗力
    public int member_max;
    public int enter_level;
    public int logo_pannel; // 战队旗面
    public int logo_icon;   // 战队标志
    public int logo_color;  // 旗面颜色
}

public class CorpsData
{
    public long corps_id;
    public long legion_id;
    public string name;
    public string notice;
    public int create_time;
    public long creator_id;
    public string creator_name;
    public long leader_id;
    public string leader_name;
    //public member[] member_list;// 成员列表
    //public apply[] apply_list;  // 申请列表
    public int max_member;  // 战队成员容量
    public int fighting;    // 战队贡献值
    public int logo_pannel; // 战队旗面
    public int logo_icon;   // 战队标志
    public int logo_color;  // 旗面颜色
    public int level;       //> 等级
    public int tribute;     //> 建设值
    //public corps_log[] log_list;    // 日志列表
    public int donate_coin;      //> 剩余能够捐献的金币数
    public int donate_diamond;   //> 剩余能够捐献的钻石数
    public int next_diamond_id;  //  下一个可捐献的id
    public int enter_level;//战队申请等级限制
}

public enum CORPS_MEMBER_TYPE
{
    TYPE_LEADER = 1,// -- 队长
    TYPE_VICELEADER = 2,// -- 副队长
    TYPE_COMMANDER = 3,// -- 指挥官
    TYPE_EXAMINER = 4,// -- 考核官
    TYPE_DIPLOMAT = 5,// -- 外交官
    TYPE_SECRETARY = 6,// -- 秘书
    TYPE_MEMBER = 7, // -- 普通成员
    TYPE_MAX
}

public enum CORPS_SORT_ORDER
{
    DESC = 0,
    ASC = 1
}

public enum CORPS_MEMBER_SORT_COLS
{
    DEFAULT = 0,
    LVL_DESC = 2,
    LVL_ASC = 3,
    WEEK_FIGHTING_DESC = 4,
    WEEK_FIGHTING_ASC,
    FIGHTING_DESC,
    FIGHTING_ASC,
    ONLINE_DESC,
    ONLINE_ASC
}

public class corpsRoomData
{
    public string corpsname;
    public int corpsicon;
    public long corpsid;
    public int corpsflag;
    public int corpscolor;
}

public class CorpsDataManager
{
    //public static p_corps_data_toc m_corpsData;
    public static CorpsData m_corpsData = new CorpsData();
    public static Dictionary<int, List<BaseCorpsData>> m_dicCorps = new Dictionary<int, List<BaseCorpsData>>();
    public static bool m_bGetCorpsList = false;

    internal static List<p_corps_member_toc> m_lstMember = new List<p_corps_member_toc>();
    internal static List<p_corps_member_toc> m_lstMemberOnline = new List<p_corps_member_toc>();
    internal static List<p_corps_member_toc> m_lstMemberOffline = new List<p_corps_member_toc>();
    internal static Dictionary<long, p_corps_member_toc> m_memberDic = new Dictionary<long, p_corps_member_toc>();    
    private static List<PlayerRecord> m_lstMemberData = new List<PlayerRecord>();

    internal static List<p_corps_apply_toc> m_lstApply = new List<p_corps_apply_toc>();
    internal static List<p_corps_log_toc> m_lstLogs = new List<p_corps_log_toc>();
    public static int m_iCurCorpsPage = 1;
    public static int m_iPageNum;
    public static int NUM_OF_PAGE = 20;
    public static int CORPS_PLAYER_NUM = 20;
    public static CORPS_MEMBER_SORT_COLS SortFlag = 0;
    //战队联赛数据

    public static bool is_signed = false;
    private static bool is_showTop16Start = false;
    public static long myid;
    public static long otherid;
    public static int[] m_corpsrule = new int[8];

    public static corpsRoomData[] m_roomdata = new corpsRoomData[2];
    public static int corpsmatch_roomid = 0;
    public static p_corpsmatch_room[] match_list = null;
    public static p_corpsmatch_view[] match_result_16 = null;
    public static p_corpsmatch_resultteam[] result = null;

    private static int _guard_start_timestamp;
    private static int _guard_end_timestamp;                              

    public static int m_iMaxLegion = 1;
    public static int m_matchStage = -1;//0 ： 混战  1： 16强以后
    public static int m_matchWeek;//周几
    public static int m_corpsMatchScore = 0;//混赛积分
    public static int m_CorpsMatcheftTimes = 0;
    public static bool IsOpenByCorpsPanel = false;
    public static bool IsOpenByHallBattlePanel = false;

    public static List<BaseCorpsData> recommendList = new List<BaseCorpsData>();
    public static p_corpsmatch_member[] m_Top16NameList16 = null;

    /// <summary>
    /// type: 0-到结束时间的倒计时，1-到开始时间的倒计时
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static int GedtGuardCD(int type = 0)
    {
        if (type == 1)
        {
            return Mathf.Max(_guard_start_timestamp - TimeUtil.GetNowTimeStamp(), 0);
        }
        else
        {
            return Mathf.Max(_guard_end_timestamp - TimeUtil.GetNowTimeStamp(), 0);
        }
    }

    /// <summary>
    /// return: 1-未开启，2-开启中，3-今日已关闭
    /// </summary>
    /// <returns></returns>
    public static int GetGuardStatus()
    {
        var result = 1;
        var now_stamp = TimeUtil.GetNowTimeStamp();
        if (now_stamp < _guard_start_timestamp)
        {
            result = 1;
        }
        else if (now_stamp <= _guard_end_timestamp)
        {
            result = 2;
        }
        else
        {
            result = 3;
            if (TimeUtil.GetTime(now_stamp).Day != TimeUtil.GetTime(_guard_end_timestamp).Day)
            {
                result = 1;
            }
        }
        return result;
    }

    public static void UpdateGuardData(int timestamp=0)
    {
        var time_data = ConfigManager.GetConfig<ConfigMisc>().GetLine("open_instance_time").ValueStr.Split(';');
        var booking_time = timestamp > 0 ? TimeUtil.GetTime(timestamp) : TimeUtil.GetNowTime();
        //time_data[0] = "14:21";
        //time_data[1] = "60";
        var timeStrArray = time_data[0].Split(':');
        var hour = Convert.ToInt32(timeStrArray[0]);
        var minute = Convert.ToInt32(timeStrArray[1]);
        //        DateTime.TryParse(temp.ToString("yyyy-MM-dd") + " " + time_data[0], out temp);
        _guard_start_timestamp = TimeUtil.GetTimeStamp(booking_time.Date) + hour * 3600 + minute * 60;
        _guard_end_timestamp = _guard_start_timestamp + int.Parse(time_data[1]);
        //设置提醒定时任务
        WakeupGuardDataTimer();
    }

    private static void WakeupGuardDataTimer()
    {
        TimerManager.RemoveTimeOut(WakeupGuardDataTimer);
        var status = GetGuardStatus();
        var bubble = BubbleManager.HasBubble(BubbleConst.CorpsGuardRunning);
        if (status==0 || status==3 || IsJoinCorps()==false)
        {
            if (bubble)
            {
                BubbleManager.SetBubble(BubbleConst.CorpsGuardRunning, false);
            }
            return;
        }
        if (status == 1)
        {
            var msg = "";
            var cd = GedtGuardCD(1);
            if (cd > 15 * 60)
            {
                cd -= 15 * 60;
            }
            else if (cd > 5 * 60)//15分钟提醒
            {
                cd -= 5 * 60;
                msg = "<color=purple>距离【守护时光机】开启还有15分钟请战队成员做好准备</color>";
            }
            else if (cd > 60)//5分钟提醒
            {
                cd -= 60;
                msg = "<color=purple>距离【守护时光机】开启还有5分钟请战队成员做好准备</color>";
            }
            else//一分钟提醒
            {
                cd += 1;
                msg = "<color=purple>僵尸将在60秒后到达战场，请迅速前往【守护时光机】</color>";
            }
            TimerManager.SetTimeOut(cd, WakeupGuardDataTimer);
            if (!string.IsNullOrEmpty(msg))
            {
                NetChatData.MockChatMsg(msg,CHAT_CHANNEL.CC_CORPS);
            }
        }
        else if (status == 2)
        {
            if (!bubble)
            {
                TimerManager.SetTimeOut(GedtGuardCD() + 0.5f, WakeupGuardDataTimer);
                BubbleManager.SetBubble(BubbleConst.CorpsGuardRunning, true);
            }
        }
    }

    private static float lastTime;

    private static int _corpsmatchTips = -10000;
    private static int m_corpsmatchTips
    {
        get
        {
            if (_corpsmatchTips == -10000)
                _corpsmatchTips = PlayerPrefs.GetInt("corpsmatchTips");
            return _corpsmatchTips;
        }
        set
        {
            if(_corpsmatchTips!=value)
            {
                _corpsmatchTips = value;
                PlayerPrefs.SetInt("corpsmatchTips", value);
                PlayerPrefs.Save();
            }
        }
    }
    /// <summary>
    /// 倒计时弹战队赛开始提示框
    /// </summary>
    public static void CalcCorpsMatchStart()
    {
        var now_time = TimeUtil.GetNowTime();
      
        var temp1 = new DateTime(now_time.Year, now_time.Month, now_time.Day, 18, 50, 0);
        var temp2 = new DateTime(now_time.Year, now_time.Month, now_time.Day, 20, 0, 0);
        if (now_time.Hour >= temp1.Hour && now_time.Minute >= temp1.Minute && (TimeUtil.GetTimeStamp(temp2) - TimeUtil.GetNowTimeStamp() > 0))
        {
            if (IsJoinCorps() )
           {
               int tips = m_corpsmatchTips;
               //int tips = PlayerPrefs.GetInt("corpsmatchTips");
                if (tips == 0)
                {
                    if (m_matchStage == 0)
                    {
                        UIManager.ShowTipPanel("战队联赛开始了", delegate { UIManager.ShowPanel<PanelCorpsEnter>(); }, null,
                    true, true, "前往参加", "取消");
                    }
                    
                    if (now_time.Hour >= 19 && now_time.Hour < 21)
                    {
                        NetChatData.MockChatMsg("战队联赛已开启，请务必前往战场！", CHAT_CHANNEL.CC_CORPS);
                    }
                    else
                    {
                        NetChatData.MockChatMsg("战队联赛10分钟后开启，请务必前往战场！", CHAT_CHANNEL.CC_CORPS);
                    }
                }
                //PlayerPrefs.SetInt("corpsmatchTips", 1);
               // PlayerPrefs.Save();
                m_corpsmatchTips = 1;
            }
        }
        else
        {
            //PlayerPrefs.SetInt("corpsmatchTips", 0);
            //PlayerPrefs.Save();
            m_corpsmatchTips = 0;
        }
        if ((now_time.Hour == 20 && now_time.Minute >= 50) && is_showTop16Start == false)
        {
            if (Time.realtimeSinceStartup - lastTime >= 300.0f)
            {
                lastTime = Time.realtimeSinceStartup;
                NetLayer.Send(new tos_corpsmatch_is_registered());
                NetLayer.Send(new tos_corpsmatch_stage());
            }
            if (is_signed == true && m_matchStage == 1)
            {
                is_showTop16Start = true;
                NetChatData.MockChatMsg("战队联赛21点开启，请务必前往战场！", CHAT_CHANNEL.CC_CORPS);
            }
        }
    }

    static void Toc_corpsmatch_match_info(toc_corpsmatch_match_info data)
    {
        if (data.mode == null)
        {
            m_corpsrule = null;
            return;
        }
        m_corpsrule = new int[data.mode.Length];
        for (int i = 0; i < data.mode.Length; i++)
        {
            m_corpsrule[i] = data.mode[i];
        }
        myid = data.my_id;
        otherid = data.other_id;
    }

    static void Toc_corpsmatch_room(toc_corpsmatch_room data)
    {
        corpsmatch_roomid = data.id;
    }

    static void Toc_corpsmatch_match_list(toc_corpsmatch_match_list data)
    {
        match_list = new p_corpsmatch_room[data.list.Length];
        for (int i = 0; i < data.list.Length; i++)
        {
            match_list[i] = data.list[i];
        }
    }



    static void Toc_coprsmatch_last_record(toc_corpsmatch_last_record data)
    {
        match_result_16 = new p_corpsmatch_view[data.records.Length];
        for (int i = 0; i < data.records.Length; i++)
        {
            match_result_16[i] = data.records[i];
        }
    }

    static void Toc_corpsmatch_result(toc_corpsmatch_result data)
    {
        result = new p_corpsmatch_resultteam[data.team.Length];
        for (int i = 0; i < data.team.Length; i++)
        {
            result[i] = data.team[i];
        }
    }
    private static void Toc_corps_data(toc_corps_data proto)
    {
        OnGetCorpsData(proto.data);
    }

    public static p_corps_member_toc GetMemberById(long id)
    {
        if (m_memberDic.ContainsKey(id))
            return m_memberDic[id];
        else return null;
    }

    internal static void OnGetCorpsData(p_corps_data_toc data)
    {
        var prevCorpsId = m_corpsData.corps_id;
        ResetCorpsData(data);

        m_lstMember.Clear();
        m_lstMemberOnline.Clear();
        m_lstMemberOffline.Clear();
        m_memberDic.Clear();
        for (int i = 0; i < data.member_list.Length; ++i)
        {
            List<p_corps_member_toc> lstMember = null;
            p_corps_member_toc member = data.member_list[i];

            if (member.handle != 0)
                lstMember = m_lstMemberOnline;
            else
                lstMember = m_lstMemberOffline;
            if (!m_memberDic.ContainsKey(member.id))
                m_memberDic.Add(member.id, member);
            lstMember.Add(member);
        }
        m_lstMember.AddRange(m_lstMemberOnline);
        m_lstMember.AddRange(m_lstMemberOffline);
        SortMemberList();

        m_lstApply.Clear();
        m_lstApply.AddRange(data.apply_list);
        BubbleManager.SetBubble(BubbleConst.CorpsApply, CanSeeApplyList() ? m_lstApply.Count : 0);

        m_lstLogs.Clear();
        m_lstLogs.AddRange(data.log_list);

        ShowPanelCorps(prevCorpsId != m_corpsData.corps_id);
    }


    public static void SortMemberList()
    {
        switch (SortFlag)
        {
            case CORPS_MEMBER_SORT_COLS.DEFAULT:
                m_lstMember.Sort((a, b) => (b.handle - a.handle) * 100 + (a.member_type - b.member_type));
                break;
            case CORPS_MEMBER_SORT_COLS.LVL_DESC:
                m_lstMember.Sort((a, b) => b.level - a.level);
                break;
            case CORPS_MEMBER_SORT_COLS.LVL_ASC:
                m_lstMember.Sort((a, b) => a.level - b.level);
                break;
            case CORPS_MEMBER_SORT_COLS.WEEK_FIGHTING_DESC:
                m_lstMember.Sort((a, b) => b.week_fighting - a.week_fighting);
                break;
            case CORPS_MEMBER_SORT_COLS.WEEK_FIGHTING_ASC:
                m_lstMember.Sort((a, b) => a.week_fighting - b.week_fighting);
                break;
            case CORPS_MEMBER_SORT_COLS.FIGHTING_DESC:
                m_lstMember.Sort((a, b) => b.fighting - a.fighting);
                break;
            case CORPS_MEMBER_SORT_COLS.FIGHTING_ASC:
                 m_lstMember.Sort((a, b) => a.fighting - b.fighting);
                break;
            case CORPS_MEMBER_SORT_COLS.ONLINE_DESC:
                m_lstMember.Sort((a, b) => b.logout_time - a.logout_time);
                break;
            case CORPS_MEMBER_SORT_COLS.ONLINE_ASC:
                m_lstMember.Sort((a, b) => a.logout_time - b.logout_time);
                break;
            default:
                m_lstMember.Sort((a,b) => (b.handle - a.handle)*100+(a.member_type - b.member_type));
                break;
        }
    }

    public static void OnLeaveCorps()
    {
        //if (m_corpsData.legion_id != 0)
       // {
       //     LegionManager.OnLeaveLegion();
       // }
        m_corpsData = new CorpsData();
        m_lstMemberOnline.Clear();
        m_lstMemberOffline.Clear();
        m_lstMember.Clear();
        m_lstApply.Clear();
        m_lstLogs.Clear();
        ShowPanelCorps();
        BubbleManager.SetBubble(BubbleConst.CorpsTaskReward, false);
        BubbleManager.SetBubble(BubbleConst.CorpsGuardRunning, false);
    }

    private static void Toc_corps_get_list(toc_corps_get_list proto)
    {
        if (!string.IsNullOrEmpty(PanelCorps.search_key) && proto.list.Length == 0)
        {
            PanelCorps.search_key = null;
            UIManager.ShowTipPanel("找不到该战队的信息");
            return;
        }

        m_iPageNum = Mathf.Max(proto.pages,1);
        m_iCurCorpsPage = proto.page;
        m_bGetCorpsList = true;
        List<BaseCorpsData> lstCorps = null;
        if (m_dicCorps.ContainsKey(m_iCurCorpsPage))
        {
            lstCorps = m_dicCorps[m_iCurCorpsPage];
        }
        else
        {
            lstCorps = new List<BaseCorpsData>();
            m_dicCorps[m_iCurCorpsPage] = lstCorps;
        }

        lstCorps.Clear();
        for (int i = 0; i < proto.list.Length; i++)
        {
            BaseCorpsData newData = new BaseCorpsData();
            newData.m_iQueue = proto.start + i;
            newData.corps_id = proto.list[i].corps_id;
            newData.corps_name = proto.list[i].corps_name;
            newData.leader_id = proto.list[i].leader_id;
            newData.leader_name = proto.list[i].leader_name;
            newData.member_cnt = proto.list[i].member_cnt;
            newData.fighting = proto.list[i].fighting;
            newData.member_max = proto.list[i].max_member;
            newData.enter_level = proto.list[i].enter_level;
            newData.logo_color = proto.list[i].logo_color;
            newData.logo_icon = proto.list[i].logo_icon;
            newData.logo_pannel = proto.list[i].logo_pannel;
            lstCorps.Add(newData);
        }
        ShowPanelCorps();
    }

    private static void ResetCorpsData(p_corps_data_toc data)
    {
        m_corpsData.corps_id = data.corps_id;
        m_corpsData.legion_id = data.legion_id;

        //LegionManager.OnLeaveLegion();  //> 清空军团的数据
        //if (m_corpsData.legion_id != 0)
        //    LegionManager.RequestLegionData();

        m_corpsData.create_time = data.create_time;
        m_corpsData.creator_id = data.creator_id;
        m_corpsData.creator_name = data.creator_name;
        m_corpsData.fighting = data.fighting;
        m_corpsData.leader_id = data.leader_id;
        m_corpsData.leader_name = data.leader_name;
        m_corpsData.logo_color = data.logo_color;
        m_corpsData.logo_icon = data.logo_icon;
        m_corpsData.logo_pannel = data.logo_pannel;
        m_corpsData.max_member = data.max_member;
        m_corpsData.name = data.name;
        m_corpsData.notice = data.notice;
        m_corpsData.level = data.level;
        m_corpsData.tribute = data.tribute;
        m_corpsData.enter_level = data.enter_level;
        UpdateGuardData();
        if (m_corpsData.corps_id > 0)
        {
            CorpsModel.Instance.TosGetTaskData();
        }
        NetLayer.Send(new tos_corps_donate_info());
    }


    public static void ShowPanelCorps(bool changeCorps = false)
    {
        if (m_corpsData != null && m_bGetCorpsList)
        {
            if (!IsJoinCorps()||!String.IsNullOrEmpty(PanelCorps.search_key))
            {
                if (UIManager.IsOpen<PanelCorpsList>())
                {
                    UIManager.GetPanel<PanelCorpsList>().UpdateView();
                }
                else
                {
                    UIManager.ShowPanel<PanelCorpsList>();
                }
            }
            else
            {
                if (UIManager.IsOpen<PanelCorps>())
                {
                    UIManager.GetPanel<PanelCorps>().UpdateView();
                   
                }
                else if (UIManager.IsOpen<PanelCorpsList>())
                {
                    UIManager.GetPanel<PanelCorpsList>().UpdateView();
                    if (changeCorps)
                    {
                        UIManager.ShowPanel<PanelCorps>();
                    }
                }
                else
                {
                    UIManager.ShowPanel<PanelCorps>();
                }
            }
        }
    }

    public static bool IsJoinCorps()
    {
        if (m_corpsData == null || m_corpsData.corps_id == 0)
        {
            return false;
        }

        return true;
    }

    public static string GetCorpsName()
    {
        if (m_corpsData == null || m_corpsData.corps_id == 0)
        {
            return "";
        }

        return m_corpsData.name;
    }

    //public static void  ReloadCorpsData()
    //{
    //    m_corpsData = new CorpsData();
    //    tos_corps_get_data msg = new tos_corps_get_data();
    //     NetLayer.Send(msg);
    // }
    public static void ReLoadCorpsList()
    {
        m_dicCorps.Clear();
        tos_corps_get_list msgList = new tos_corps_get_list();
        msgList.page = 1;
        NetLayer.Send(msgList);
    }
    public static List<PlayerRecord> GetCorpsMember()
    {
        m_lstMemberData.Clear();
        for (int i = 0; i < m_lstMember.Count; ++i)
        {
            PlayerRecord rcd = new PlayerRecord();
            rcd.m_strName = m_lstMember[i].name;
            rcd.m_iPlayerId = m_lstMember[i].id;
            rcd.m_iSex = 0;
            rcd.m_iLevel = m_lstMember[i].level;
            rcd.m_iHead = m_lstMember[i].icon;
            rcd.m_isFriend = false;
            rcd.m_bOnline = m_lstMember[i].handle != 0 ? true : false;
            m_lstMemberData.Add(rcd);
        }
        return m_lstMemberData;
    }

    public static int GetApplyNum()
    {
        return m_lstApply.Count;
    }

    public static bool IsLeader()
    {
        if (m_corpsData == null)
            return false;

        return m_corpsData.leader_id == PlayerSystem.roleId;
    }

    public static int GetFightting()
    {
        if (m_corpsData == null)
            return 0;

        return m_corpsData.fighting;
    }


    private static void Toc_corps_donate_info(toc_corps_donate_info proto)
    {
        CorpsDataManager.m_corpsData.donate_coin = proto.donated_coin;
        CorpsDataManager.m_corpsData.donate_diamond = proto.donated_diamond;
        CorpsDataManager.m_corpsData.next_diamond_id = proto.next_diamond_id;
    }

    private static void Toc_corps_create_corps(toc_corps_create_corps proto)
    {
        if (proto.data.corps_id == 0)
        {
            UIManager.ShowTipPanel("你的战队创建失败!");
            return;
        }
        if (!UIManager.IsOpen<PanelCorps>())
        {
            UIManager.ShowPanel<PanelCorps>();
        }
        UIManager.ShowTipPanel("你的战队创建成功!");
        CorpsDataManager.OnGetCorpsData(proto.data);
        CorpsDataManager.ReLoadCorpsList();
    }


    internal static p_corps_apply_toc GetApply(long id)
    {
        for (int i = 0; i < CorpsDataManager.m_lstApply.Count; ++i)
        {
            if (CorpsDataManager.m_lstApply[i].id == id)
            {
                return CorpsDataManager.m_lstApply[i];
            }
        }

        return null;
    }

    internal static void AddApply(p_corps_apply_toc apply)
    {
        p_corps_apply_toc corps_appy = GetApply(apply.id);
        if (corps_appy != null)
            return;

        CorpsDataManager.m_lstApply.Add(apply);
    }

    public static void RemoveInApply(long id)
    {
        for (int i = 0; i < CorpsDataManager.m_lstApply.Count; ++i)
        {
            if (CorpsDataManager.m_lstApply[i].id == id)
            {
                CorpsDataManager.m_lstApply.RemoveAt(i);
                break;
            }
        }
    }

    internal static void UpdateCorpsView()
    {
        if (UIManager.IsOpen<PanelCorps>())
        {
            UIManager.GetPanel<PanelCorps>().UpdateView();
        }
        else if (UIManager.IsOpen<PanelCorpsList>())
        {
            UIManager.GetPanel<PanelCorpsList>().UpdateView();
        }
        else if (UIManager.IsOpen<PanelCorpsManage>())
        {
            UIManager.GetPanel<PanelCorpsManage>().OnShow();
        }
    }
    internal static bool IsLeader(long id)
    {
        if (CorpsDataManager.m_corpsData == null || id == 0)
            return false;

        if (CorpsDataManager.m_corpsData.leader_id == id)
            return true;

        return false;
    }

    internal static bool HasPosition(CORPS_MEMBER_TYPE eType)
    {
        if (eType == CORPS_MEMBER_TYPE.TYPE_MEMBER)
            return true;
        if (eType == CORPS_MEMBER_TYPE.TYPE_LEADER)
            return false;

        int cur_num = 0;
        int max_num = 0;
        for (int i = 0; i < m_lstMember.Count; ++i)
        {
            if (m_lstMember[i].member_type == (int)eType)
                cur_num++;
        }

        if (eType == CORPS_MEMBER_TYPE.TYPE_VICELEADER)
            max_num = 2;
        else
            max_num = 1;

        if (cur_num >= max_num)
            return false;

        return true;
    }

    /// <summary>
    /// 是否可以看见申请列表
    /// </summary>
    /// <returns></returns>
    internal static bool CanSeeApplyList()
    {
        var mySelf = GetSelf();
        return mySelf != null && mySelf.member_type < (int)CORPS_MEMBER_TYPE.TYPE_MEMBER;
    }

    internal static p_corps_member_toc GetSelf()
    {
        return GetMember(PlayerSystem.roleId);
    }

    internal static p_corps_member_toc GetMember(long id)
    {
        for (int i = 0; i < m_lstMember.Count; ++i)
        {
            if (m_lstMember[i].id == id)
                return m_lstMember[i];
        }

        return null;
    }

    internal static void RemoveMember(long id)
    {
        List<p_corps_member_toc>[] memberArr = { m_lstMemberOffline, m_lstMemberOnline, m_lstMember };
        for (int idx = 0; idx < memberArr.Length; ++idx)
        {
            List<p_corps_member_toc> lstMember = memberArr[idx];
            for (int i = 0; i < lstMember.Count; ++i)
            {
                if (lstMember[i].id == id)
                {
                    lstMember.RemoveAt(i);
                    break;
                }
            }
        }

    }

    private static void SetMemberOnlineInfo(long id, int handle, int logout_time)
    {
        List<p_corps_member_toc> lstMemberAdd = null;
        List<p_corps_member_toc> lstMemberDel = null;
        if (handle == 0)
        {
            lstMemberAdd = m_lstMemberOffline;
            lstMemberDel = m_lstMemberOnline;
        }
        else
        {
            lstMemberAdd = m_lstMemberOnline;
            lstMemberDel = m_lstMemberOffline;
        }

        p_corps_member_toc memberModify = null;
        for (int i = 0; i < lstMemberDel.Count; ++i)
        {
            if (lstMemberDel[i].id == id)
            {
                memberModify = lstMemberDel[i];
                lstMemberDel.RemoveAt(i);
                break;
            }
        }
        if (memberModify == null)
            return;

        memberModify.handle = handle;
        memberModify.logout_time = logout_time;
        lstMemberAdd.Add(memberModify);
        m_lstMember.Clear();
        m_lstMember.AddRange(CorpsDataManager.m_lstMemberOnline);
        m_lstMember.AddRange(CorpsDataManager.m_lstMemberOffline);
        SortMemberList();
    }

    private static void AddMember(p_corps_member_toc mbr)
    {
        for (int i = 0; i < m_lstMember.Count; ++i)
        {
            if (mbr.id == m_lstMember[i].id)
                return;
        }

        List<p_corps_member_toc> lstMemberAdd = null;
        if (mbr.handle != 0)
            lstMemberAdd = m_lstMemberOnline;
        else
            lstMemberAdd = m_lstMemberOffline;

        lstMemberAdd.Add(mbr);
        m_lstMember.Clear();
        m_lstMember.AddRange(CorpsDataManager.m_lstMemberOnline);
        m_lstMember.AddRange(CorpsDataManager.m_lstMemberOffline);
        SortMemberList();
        if (!m_memberDic.ContainsKey(mbr.id))
            m_memberDic.Add(mbr.id, mbr);
    }

    private static void ChangeMbrType(p_corps_member_toc mbr)
    {
        RemoveMember(mbr.id);
        AddMember(mbr);
    }
    private static void Toc_corps_message(toc_corps_message proto)
    {
        if (proto.type == "apply_enter")
        {
        }
        else if (proto.type == "agree_apply")
        {
            p_corps_apply_toc apply = GetApply(proto.pid);
            if (apply == null)
               // NetLayer.Send(new tos_corpsmatch_is_registered());
            return;

            string info = string.Format("{0}被同意加入战队", apply.name);
            TipsManager.Instance.showTips(info);
        }
        else if (proto.type == "refuse_apply")
        {
            p_corps_apply_toc apply = GetApply(proto.pid);
            if (apply == null)
                return;

            string info = string.Format("{0}被拒绝加入战队", apply.name);
            TipsManager.Instance.showTips(info);
        }
        else if (proto.type == "quit")
        {
            if (proto.pid == PlayerSystem.roleId)
            {
                OnLeaveCorps();
            }
            else
            {
                p_corps_member_toc member = GetMember(proto.pid);
                if (member == null)
                    return;
                string info = string.Format("{0}退出了战队", member.name);
                TipsManager.Instance.showTips(info);
            }

        }
        else if (proto.type == "dismiss")
        {
            OnLeaveCorps();
            ReLoadCorpsList();
        }
        else if (proto.type == "kick")
        {
            if (proto.pid == PlayerSystem.roleId)
            {
                TipsManager.Instance.showTips("你被队长踢出了战队!");
                OnLeaveCorps();
            }
            else
            {
                p_corps_member_toc member = GetMember(proto.pid);
                if (member == null)
                    return;

                string info = string.Format("{0}被队长踢出了战队", member.name);
                TipsManager.Instance.showTips(info);

            }
        }
        else if (proto.type == "change_member_type")
        {
            p_corps_member_toc member = GetMember(proto.pid);
            if (member == null)
                return;

            string info = string.Format("{0}的职务从{1}变成了{2}", member.name, PanelCorps.m_strPosition[member.member_type - 1], proto.msg);
            TipsManager.Instance.showTips(info);

        }
        else if (proto.type == "set_leader")
        {
            p_corps_member_toc member = GetMember(proto.pid);
            if (member == null)
                return;

            m_corpsData.leader_id = member.id;
            m_corpsData.leader_name = member.name;
            string info = string.Format("{0}成了新队长", member.name);
            TipsManager.Instance.showTips(info);

        }
        else if (proto.type == "update_notice")
        {
            TipsManager.Instance.showTips("队长修改了公告!");
        }
    }

    private static void Toc_corps_notify_apply(toc_corps_notify_apply proto)
    {
        if (proto.type == "add")
        {
            AddApply(proto.apply);
        }
        else if (proto.type == "del")
        {
            RemoveInApply(proto.apply.id);
        }

        BubbleManager.SetBubble(BubbleConst.CorpsApply, CanSeeApplyList() ? m_lstApply.Count : 0);

        UpdateCorpsView();
    }

    private static void Toc_corps_notify_members(toc_corps_notify_members proto)
    {
        for (int i = 0; i < proto.list.Length; ++i)
        {
            toc_corps_notify_members.notify_member ntf_mbr = proto.list[i];
            if (ntf_mbr.type == "login")
            {
                p_corps_member_toc member = GetMember(ntf_mbr.member.id);
                if (member == null)
                    return;
                //string info = string.Format("{0}刚刚登录了", member.name);
                //TipsManager.Instance.showTips(info);
                LoginRemindMangager.Ins.ShowTips(2, member.id);
                SetMemberOnlineInfo(ntf_mbr.member.id, 1, ntf_mbr.member.logout_time);
            }
            else if (ntf_mbr.type == "logout")
            {
                p_corps_member_toc member = GetMember(ntf_mbr.member.id);
                if (member == null)
                    return;
                //string info = string.Format("{0}刚刚下线了", member.name);
                //TipsManager.Instance.showTips(info);

                SetMemberOnlineInfo(ntf_mbr.member.id, 0, ntf_mbr.member.logout_time);
            }
            else if (ntf_mbr.type == "add")
            {
                AddMember(ntf_mbr.member);
            }
            else if (ntf_mbr.type == "del")
            {
                RemoveMember(ntf_mbr.member.id);
            }
            else if (ntf_mbr.type == "change_type")
            {
                ChangeMbrType(ntf_mbr.member);
            }
            else if (ntf_mbr.type == "update")
            {
                p_corps_member_toc member = GetMember(ntf_mbr.member.id);
                if (member == null)
                    return;

                member.level = ntf_mbr.member.level;
                member.fighting = ntf_mbr.member.fighting;
                member.qualifying_win = ntf_mbr.member.qualifying_win;
            }
        }

        UpdateCorpsView();
    }

    private static void Toc_corps_notify_infos(toc_corps_notify_infos proto)
    {
        for (int i = 0; i < proto.list.Length; ++i)
        {
            toc_corps_notify_infos.notify_info ntf_info = proto.list[i];
            if (ntf_info.key == "fighting")
            {
                m_corpsData.fighting = ntf_info.int_val;
            }
            else if (ntf_info.key == "notice")
            {
                m_corpsData.notice = ntf_info.str_val;
            }
            else if (ntf_info.key == "logo_pannel")
            {
                m_corpsData.logo_pannel = ntf_info.int_val;
            }
            else if (ntf_info.key == "logo_icon")
            {
                m_corpsData.logo_icon = ntf_info.int_val;
            }
            else if (ntf_info.key == "level")
            {
                m_corpsData.level = ntf_info.int_val;
            }
            else if (ntf_info.key == "tribute")
            {
                m_corpsData.tribute = ntf_info.int_val;
            }
            else if (ntf_info.key == "max_member")
            {
                m_corpsData.max_member = ntf_info.int_val;
            }
            else if (ntf_info.key == "enter_level")
            {
                m_corpsData.enter_level = ntf_info.int_val;
            }
        }

        UpdateCorpsView();
    }

    private static void Toc_corps_notify_logs(toc_corps_notify_logs proto)
    {
        m_lstLogs.Clear();
        m_lstLogs.AddRange(proto.list);
        UpdateCorpsView();
    }

    static void Toc_corpsmatch_is_registered(toc_corpsmatch_is_registered data)
    {
        is_signed = data.is_registered;
    }

    /// <summary>
    /// 战队联赛状态
    /// </summary>
    /// <param name="data"></param>
    static void Toc_corpsmatch_stage(toc_corpsmatch_stage data)
    {
        m_matchStage = data.stage;
        m_matchWeek = data.week;
//         if (UIManager.IsOpen<PanelCorpsEnter>() == false && m_matchStage == 1)
//         {
//             UIManager.ShowPanel<PanelCorpsEnter>();
//         }
    }

    /// <summary>
    /// 每场混赛获得积分
    /// </summary>
    /// <param name="data"></param>
    static void Toc_corpsmatch_group_score(toc_corpsmatch_group_score data)
    {
        m_corpsMatchScore = data.score;
    }

    static void Toc_corps_recmd_list(toc_corps_recmd_list tocData)
    {
        recommendList.Clear();        
        for(int i=0;i<tocData.list.Length;i++)
        {
            p_corps_info_toc info = tocData.list[i];
            BaseCorpsData data = new BaseCorpsData();
            data.m_iQueue = i + 1;
            data.corps_id = info.corps_id;
            data.corps_name = info.corps_name;
            data.leader_id = info.leader_id;
            data.leader_name = info.leader_name;
            data.member_cnt = info.member_cnt;
            data.enter_level = info.level;
            data.member_max = info.max_member;
            data.fighting = info.fighting;
            data.enter_level = info.enter_level;
            data.logo_color = info.logo_color;
            data.logo_icon = info.logo_icon;
            data.logo_pannel = info.logo_pannel;
            recommendList.Add(data);
        }
        var panel = UIManager.GetPanel<PanelCorpsList>();
        PanelCorpsRecommend panel2 = UIManager.GetPanel<PanelCorpsRecommend>();
        if (panel!=null&&panel.IsOpen())
            panel.UpdateCorspRecommendList();
        else if (panel2!=null&&panel2.IsOpen())
            panel2.UpdateCorspRecommendList();
    }

    /// <summary>
    /// 16强名单
    /// </summary>
    /// <param name="data"></param>
    static void Toc_corpsmatch_top16(toc_corpsmatch_top16 data)
    {
        m_Top16NameList16 = data.members;
    }

}
