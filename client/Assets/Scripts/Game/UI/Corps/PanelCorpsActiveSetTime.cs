﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelCorpsActiveSetTime : BasePanel
{
    public object _data;

    public Text name_txt;
    public Text desc_txt;
    public Text value_txt;
    public Text cost_txt;

    public PanelCorpsActiveSetTime()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsActiveSetTime");

        AddPreLoadRes("UI/Corps/PanelCorpsActiveSetTime", EResType.UI);
    }

    public override void Init()
    {
        name_txt = m_tran.Find("frame/name_txt").GetComponent<Text>();
        desc_txt = m_tran.Find("frame/desc_txt").GetComponent<Text>();
        value_txt = m_tran.Find("frame/value_txt").GetComponent<Text>();
        cost_txt = m_tran.Find("frame/cost_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/set_btn")).onPointerClick += OnSetupTimeValue;
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target,evt)=>HidePanel();
    }

    private void OnSetupTimeValue(GameObject target, PointerEventData eventData)
    {
        //CorpsModel.Instance.TosGuardTaskAppointment();
        HidePanel();
    }

    public override void OnShow()
    {
        value_txt.text = "剩余建设值" + CorpsDataManager.m_corpsData.tribute.ToString();
        cost_txt.text = "建设值" + ConfigManager.GetConfig<ConfigMisc>().GetLine("open_instance_tribute").ValueInt.ToString();
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

