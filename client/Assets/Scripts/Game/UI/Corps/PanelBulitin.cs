﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelBulitin : BasePanel
{
    private InputField m_inputField;

    public PanelBulitin()
    {
        SetPanelPrefabPath("UI/Corps/PanelBulitin");
        AddPreLoadRes("UI/Corps/PanelBulitin", EResType.UI);
        
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_inputField = m_tran.Find("InputField").GetComponent<InputField>();
        m_inputField.gameObject.AddComponent<InputFieldFix>().onEndEdit += OnSubmitBulitin;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("send").gameObject).onPointerClick += OnClkSend;
    }

    public override void OnShow()
    {   
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    protected void OnClkSend(GameObject target, PointerEventData eventData)
    {
        if (m_inputField.text == "")
        {
            UIManager.ShowTipPanel("请输入公告");
        }
        else
        {
            string text = WordFiterManager.Fiter(m_inputField.text);
            tos_corps_update_notice msg = new tos_corps_update_notice();
            msg.notice = text;
            NetLayer.Send(msg);
            HidePanel();
        }
        
    }

    protected void OnSubmitBulitin(string text)
    {
        
    }

    private static void Toc_corps_update_notice( toc_corps_update_notice proto )
    {
        UIManager.ShowTipPanel("公告更新完毕!");
    }
}
