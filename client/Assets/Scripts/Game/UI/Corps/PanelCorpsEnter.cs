﻿using Assets.Scripts.Game.UI.Corps;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
///////////////////////////////////////////
//Copyright (C): 4399 YiDao studio
//All rights reserved
//文件描述：战队联赛入口
//创建者：hwl
//创建日期: 2015/12/26
///////////////////////////////////////////
public class PanelCorpsEnter : BasePanel 
{
    private Image m_lockImg;
    private Image m_fightingImg;
    DateTime m_curTime = TimeUtil.GetNowTime();
    private int m_stage;//0 : 小组混赛   1： 十六强之后的比赛
    private int m_week;//周几
    private GameObject m_topCard;//精英赛卡片
    private Text m_TimeText;
    public PanelCorpsEnter()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsEnter");
        AddPreLoadRes("UI/Corps/PanelCorps", EResType.UI);
        AddPreLoadRes("UI/Corps/PanelCorpsEnter", EResType.UI);  
    }

    public override void Init()
    {
        m_lockImg = m_tran.Find("frame_Corps16/lockImg").GetComponent<Image>();
        m_lockImg.gameObject.TrySetActive(false);
        m_fightingImg = m_tran.Find("frame_Corps16/fightImg").GetComponent<Image>();
        m_topCard = m_tran.Find("frame_Corps16/user_define").gameObject;
        m_TimeText = m_tran.Find("frame_Corps16/user_define/Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame_battlefield/lvl")).onPointerClick += OnEnterBattelfield;
        UGUIClickHandler.Get(m_tran.Find("frame_Corps16/user_define")).onPointerClick += OnEnterMatch16;
    }

    /// <summary>
    /// 是否显示锁
    /// </summary>
    /// <param name="isLock"></param>
    private void ShowLockIcon(bool isLock)
    {
       // m_lockImg.gameObject.TrySetActive(isLock);
        m_fightingImg.gameObject.TrySetActive(!isLock);
       // Util.SetGoGrayShader(m_topCard, isLock);
    }

    /// <summary>
    /// 进入混战区
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnEnterBattelfield(GameObject target, PointerEventData eventData)
    {
        if( CorpsDataManager.IsJoinCorps() == false )
        {
            TipsManager.Instance.showTips("你还未加入战队!");
            UIManager.ShowPanel<PanelCorpsList>();
            return;
        }

        if (CorpsDataManager.m_CorpsMatcheftTimes <= 0)
        {
            TipsManager.Instance.showTips("今日参赛次数已经用完");
            return;
        }
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.corpsmatch_group), stage = 0 });
    }

    /// <summary>
    /// 进入精英区
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnEnterMatch16(GameObject target, PointerEventData eventData)
    {
        
        if (CorpsDataManager.IsJoinCorps() == false)
        {
            TipsManager.Instance.showTips("你还未加入战队!");
            //UIManager.GotoPanel("corps");
            UIManager.ShowPanel<PanelCorpsList>();
            return;
        }
        if(m_stage == 1)
        {
            if (CorpsDataManager.m_matchWeek == 7)
            {
                UIManager.ShowPanel<PanelCorpsLastMatch>();
            }
            else
            {
                UIManager.ShowPanel<PanelCorpsNew>();
            }
        }
        else
        {
            UIManager.ShowPanel<PanelCorpsLastMatch>();
        }
    }

    private void showStartTime()
    {
        DateTime time = TimeUtil.GetNowTime();
        int temp = CorpsDataManager.m_matchWeek;
        int days = 0;
        if (temp == 7)
        {
            days = 1 + 1;
        }
        else
        {
            days = (7 - temp + 1 + 1);
        }
        m_TimeText.text = days + "天后开赛";
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corpsmatch_stage());
        //请求我的积分，我的战队积分、剩余次数信息
        NetLayer.Send(new tos_corpsmatch_score_info());
        
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void Update()
    {
    }

    public override void OnDestroy()
    {
    }

    /// <summary>
    /// 混战 、16强以后开启状态
    /// </summary>
    /// <param name="data"></param>
    void Toc_corpsmatch_stage(toc_corpsmatch_stage data)
    {
        m_week = data.week;
        m_stage = data.stage;
        ShowLockIcon(m_stage != 1);
        if (m_stage == 1)
        {
            NetLayer.Send(new tos_corpsmatch_top16());
            m_TimeText.text = "21:00开赛";
        }
        else
        {
            showStartTime();
        }
    }
}
