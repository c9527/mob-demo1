﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;


public class PanelCorpsMatchTable:BasePanel
{
    public class DateTable
    {
        public int match_num;
        public int day;
        public string month;
    }

    public class ItemDateTable:ItemRender{
        Text Date;
        Text MatchNum;
        Text Month;

        public override void Awake()
        {
            Date = transform.Find("Date").GetComponent<Text>();
            Month = transform.Find("Month").GetComponent<Text>();
            MatchNum = transform.Find("MatchNum").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            var info = data as DateTable;
            Date.text = info.day.ToString();
            Month.text = info.month;
            MatchNum.text = info.match_num.ToString() + "场比赛";
        }
    }



    GameObject m_signupframe;
    GameObject m_matchframe;
    Text m_signNum;
    DataGrid m_table;
    Image m_bar;
    float[] bar;
    int m_num;

    public PanelCorpsMatchTable()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsMatchTable");
        AddPreLoadRes("UI/Corps/PanelCorpsMatchTable", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corpsmatch_register_num());
        DateTime dt = new DateTime();
        dt = TimeUtil.GetNowTime();
        m_signupframe.TrySetActive(false);
        m_matchframe.TrySetActive(true);
        if(dt.Day<=10)
        {
            m_signupframe.TrySetActive(true);
            m_matchframe.TrySetActive(false);
            m_bar.fillAmount = bar[0];
        }
        if (dt.Day > 10 && dt.Day < 21)
        {
            m_bar.fillAmount = bar[1];
        }
        if (dt.Day > 20 && dt.Day < 25)
        {
            m_bar.fillAmount = bar[2];
        }
        if (dt.Day == 25)
        {
            m_bar.fillAmount = bar[3];
        }
        if (dt.Day >= 26)
        {
            m_bar.fillAmount = bar[4];
        }
    }

    public override void Init()
    {
        m_matchframe = m_tran.Find("bgMatch").gameObject;
        m_matchframe.TrySetActive(false);
        m_signupframe = m_tran.Find("bgSignUp").gameObject;
        m_signupframe.TrySetActive(true);
        m_signNum = m_tran.Find("bgSignUp/Num").GetComponent<Text>();
        m_table = m_tran.Find("bgMatch/datelist/content").gameObject.AddComponent<DataGrid>();
        m_table.SetItemRender(m_tran.Find("bgMatch/datelist/content/ItemDate").gameObject, typeof(ItemDateTable));
        m_bar = m_tran.Find("aprograssbar/bar").GetComponent<Image>();
        bar = new float[5];
        bar[0] = 0.034f;
        bar[1] = 0.198f;
        bar[2] = 0.411f;
        bar[3] = 0.737f;
        bar[4] = 1f;
        m_num = 0;
    }

    void Toc_corpsmatch_register_num(toc_corpsmatch_register_num data)
    {
        m_signNum.text = data.register_num.ToString();
        m_num = data.register_num;
        ShowList();
    }

    void ShowList()
    {
        DateTime dt = TimeUtil.GetNowTime();
        DateTable[] dtb = new DateTable[7];
        for (int i = 0; i < 7; i++)
        {
            dtb[i] = new DateTable();
            dtb[i].day = dt.Day - 3 + i;
            if (dtb[i].day >= 11 && dtb[i].day <= 20)
            {
                dtb[i].match_num = m_num / 2;
            }
            else if (dtb[i].day >= 21 && dtb[i].day < 24)
            {
                if (m_num > 16)
                {
                    dtb[i].match_num = (int)(16 / Math.Pow(2, dt.Day - 20));
                }
                else if (dtb[i].day == 21)
                {
                    if (m_num >= 8)
                    {
                        dtb[i].match_num = 8;
                    }
                    else
                    {
                        dtb[i].match_num = m_num;
                    }
                }
                else if (dtb[i].day == 22)
                {
                    if (m_num >= 4)
                    {
                        dtb[i].match_num = 4;
                    }
                    else
                    {
                        dtb[i].match_num = m_num;
                    }
                }
                else if (dtb[i].day == 23)
                {
                    if (m_num >= 2)
                    {
                        dtb[i].match_num = 2;
                    }
                    else
                    {
                        dtb[i].match_num = m_num;
                    }
                }
            }
            else if (dtb[i].day == 24)
            {
                dtb[i].match_num = 1;
            }
            else if (dtb[i].day == 25)
            {
                dtb[i].match_num = 1;
            }
            else if (dtb[i].day > 25 || dt.Day <= 10)
            {
                dtb[i].match_num = 0;
            }
            dtb[i].month = GetMonth(dt.Month);
        }
        m_table.Data = dtb;
    }

    string GetMonth(int month)
    {
        switch (month)
        {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
        }
        return "";
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void Update()
    {
       
    }


}

