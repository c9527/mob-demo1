﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


class PanelFindCorps : BasePanel
{
    InputField _search_txt;

    public PanelFindCorps()
    {
        SetPanelPrefabPath("UI/Corps/PanelFindCorps");

        AddPreLoadRes("UI/Corps/PanelFindCorps", EResType.UI);
    }

    public override void Init()
    {
        _search_txt = m_tran.Find("frame/InputField").GetComponent<InputField>();
        _search_txt.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target,event_data) => { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("frame/search_btn")).onPointerClick += OnSearchBtnClick;
    }

    private void OnSearchBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!string.IsNullOrEmpty(_search_txt.text))
        {
            PanelCorps.search_key = _search_txt.text.Trim();
            NetLayer.Send(new tos_corps_search_name() { corps_name = PanelCorps.search_key });
            HidePanel();
        }
        else
        {
            TipsManager.Instance.showTips("请输入正确的战队名称或者ID");
        }
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

