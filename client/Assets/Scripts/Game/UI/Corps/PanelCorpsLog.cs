﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelCorpsLog : BasePanel
{
    private DataGrid m_logDataGrid;
    ///private p_corps_data_toc.corps_log[] m_lstLog;
    private List<p_corps_log_toc> m_lstLog;
    public PanelCorpsLog()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsLog");
        AddPreLoadRes("UI/Corps/PanelCorpsLog", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemLogInfo", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_logDataGrid = m_tran.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
        m_logDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemLogInfo"), typeof(ItemLogInfo));
        m_logDataGrid.useLoopItems = true;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
    }

    public override void OnShow()
    {
        if (m_params.Length < 1)
            return;

        m_lstLog = m_params[0] as List<p_corps_log_toc>;
        m_logDataGrid.Data = m_lstLog.ToArray();
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick -= OnClkClose;
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }
}
