﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelCorpsRecruit : BasePanel
{
    private float _cd;

    public Text title_txt;
    public InputField content_txt;
    public Button action_btn;

    public PanelCorpsRecruit()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsRecruit");

        AddPreLoadRes("UI/Corps/PanelCorpsRecruit", EResType.UI);
    }

    public override void Init()
    {
        title_txt = m_tran.Find("frame/title_txt").GetComponent<Text>();
        content_txt = m_tran.Find("frame/content_txt").GetComponent<InputField>();
        content_txt.gameObject.AddComponent<InputFieldFix>();
        action_btn = m_tran.Find("frame/btnOk").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/btnClose")).onPointerClick += (target, evt) => HidePanel();
        UGUIClickHandler.Get(m_tran.Find("frame/btnOk")).onPointerClick += OnActionBtnClick;
    }

    private void OnActionBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_cd>0)
        {
            TipsManager.Instance.showTips("战队招募冷却中");
        }
        else
        {
            var content = content_txt.text != null ? content_txt.text.Trim() : "";
            var link_str = "战队{2}招募成员,{0}<color=#8600FFFF><a=apply_corps|{1}>我要申请</a></color>";
            CorpsModel.Instance.last_recruit_broadcast_time = Time.realtimeSinceStartup;
            NetChatData.SndChatInfo((int)CHAT_CHANNEL.CC_LOCALSERVER, 0, string.Format(link_str, !string.IsNullOrEmpty(content) ? content + "，" : "", CorpsDataManager.m_corpsData.corps_id, CorpsDataManager.m_corpsData.name));
            HidePanel();
        }
    }

    public override void OnShow()
    {
        OnUpdateData();
    }

    private void OnUpdateData()
    {
        _cd = CorpsModel.Instance.last_recruit_broadcast_time > 0 ? CorpsModel.Instance.last_recruit_broadcast_time + CorpsModel.Instance.recruit_broadcast_min_interval - Time.realtimeSinceStartup : 0;
        _cd = Mathf.Max(_cd, 0);
        if (_cd > 0)
        {
            action_btn.transform.Find("Text").GetComponent<Text>().text = string.Format("发送（{0}）",(int)_cd);
        }
        else
        {
            action_btn.transform.Find("Text").GetComponent<Text>().text = "发送";
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_cd == 0)
        {
            return;
        }
        OnUpdateData();
    }
}
