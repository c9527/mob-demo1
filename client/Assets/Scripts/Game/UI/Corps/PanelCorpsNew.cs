﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
///////////////////////////////////////////
//Copyright (C): 4399 YiDao studio
//All rights reserved
//文件描述：新战队联赛界面
//创建者：hwl
//创建日期: 2015/12/29
///////////////////////////////////////////
public class PanelCorpsNew : BasePanel {
     public class ItemMatchList : ItemRender
    {
        public Text m_team1;
        public Text m_team2;
        public Image m_icon1;
        public Image m_icon2;
        public int id;
        public bool is_myTeam;

        public override void Awake()
        {
            id = 0;
            is_myTeam = false;
            m_team1 = transform.Find("Team1/name").GetComponent<Text>();
            m_team2 = transform.Find("Team2/name").GetComponent<Text>();
            m_icon1 = transform.Find("Team1/Image").GetComponent<Image>();
            m_icon2 = transform.Find("Team2/Image").GetComponent<Image>();

        }

        protected override void OnSetData(object data)
        {
            var info = data as p_corpsmatch_room;
            id = info.room_id;
            m_team1.text = info.team1;
            m_team2.text = info.team2;
            if (info.team1 == CorpsDataManager.GetCorpsName() || info.team2 == CorpsDataManager.GetCorpsName())
            {
                is_myTeam = true;
                if (info.team1 == CorpsDataManager.GetCorpsName())
                {
                    m_team1.color = new Color32(134, 253, 131, 255);
                }
                else
                {
                    m_team2.color = new Color32(134, 253, 131, 255);
                }
            }
            m_icon1.gameObject.TrySetActive(info.icon1 != 0);
            m_icon2.gameObject.TrySetActive(info.icon2 != 0);
            string biaozhi = "biaozhi_" + info.icon1.ToString();
            m_icon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            biaozhi = "biaozhi_" + info.icon2.ToString();
            m_icon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        }
    }

    public static string search_key;
   // private GameObject m_goCorpsVote;//投票的视图
    private GameObject m_goCorpsMatchBegin;
    private GameObject m_goCorpsMatchTable4;
    private GameObject m_goCorpsMatchTable8;
    private GameObject m_goCorpsMatchTable16;
    private GameObject m_goCorpsMatchTableAll;
    private GameObject curCorpsMatch;

    private static int BTN_HEIGHT = 45;
    private static int BTN_SPACE = 15;
    private GameObject m_goRecruit;
    private GameObject m_goApply;
    private GameObject m_goAuthorrize;
    private GameObject m_goKickout;
    private GameObject m_goChangeLeader;
    private GameObject m_goNoticeEdit;
    private GameObject m_goUpgrade;
    private GameObject m_goSetting;

    //战队联赛报名
    private DateTime m_signDeadLine;
    private DateTime m_voteDeadLine;

    //战队联赛战场
    private Image m_team1;
    private Image m_team2;
    private Image m_team1flag;
    private Image m_team2flag;
    private Text m_team1name;
    private Text m_team2name;
    private Text m_matchtime;
    private Text m_matchTimedown;

    //战队联赛战场
    private int m_roomid;
    private DateTime m_matchDeadLine;

    private long m_team1id;
    private long m_team2id;
    private bool m_matchteaminfo;
    public CorpsData m_matchData;

    private DataGrid m_matchlist;
    private GameObject m_btnJoinGame;
    private Text m_noGameTip;
    //private Text m_noVoteTip;

    DateTime m_curTime = TimeUtil.GetNowTime();
    DateTime m_signupTime = new DateTime(TimeUtil.GetNowTime().Year, TimeUtil.GetNowTime().Month, 10, 23, 59, 59);

    bool Is_signTime;
    bool Is_prepareTime;
    bool Is_voteTime;
    bool Is_matchTime;

    Image m_round4;
    Image m_matchsign;
    Image m_round4final;
    Text m_roundText;

    public class ItemTeam
    {
        public Text name;
        public Image icon;
    }

    ItemTeam[] items;
    GameObject[] round1;
    GameObject[] round2;
    GameObject[] round3;


    Image m_win1;
    Image m_win2;
    protected enum FRAME_TYPE
    {
        TYPE_HOMEPAGE = 1,
        TYPE_MEMBER = 2,
        TYPE_CORPS = 3,
        TYPE_ACTIVE = 5,
        TYPE_MATCH = 6
    }

    private Dictionary<CORPS_MEMBER_TYPE, List<p_corps_member_toc>> m_dicAuthers;
    private static int AUTHER_NUM = 3;
    private FRAME_TYPE m_curFrame = FRAME_TYPE.TYPE_HOMEPAGE;
    private int m_weekOfDay = -1;
    private GameObject m_EnterBattle;
    private GameObject m_matchTips;
    private GameObject m_Tipsimg;

    private int CREATE_CORPS_LVL;
    public PanelCorpsNew()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsNew");
        AddPreLoadRes("UI/Corps/PanelCorpsNew", EResType.UI);
        //AddPreLoadRes("UI/Corps/ItemCorps", EResType.UI);
        //AddPreLoadRes("UI/Corps/ItemCorpsMember", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void Init()
    {
        Is_signTime = false;
        Is_voteTime = false;
        Is_matchTime = false;

        CREATE_CORPS_LVL = ConfigMisc.GetInt("create_corps_reqlv");
        //m_goCorpsVote = m_tran.Find("frame_corps_vote").gameObject;
        m_goCorpsMatchBegin = m_tran.Find("frame_corps_matchbegin").gameObject;
        m_goCorpsMatchTable4 = m_tran.Find("frame_corps_matchtable_4").gameObject;
        m_goCorpsMatchTable8 = m_tran.Find("frame_corps_matchtable_8").gameObject;
        m_goCorpsMatchTable16 = m_tran.Find("frame_corps_matchtable_16").gameObject;
        m_goCorpsMatchTableAll = m_tran.Find("frame_corps_matchtable_all").gameObject;

        m_tran.Find("frame_corps_matchtable_4/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_tran.Find("frame_corps_matchtable_8/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_tran.Find("frame_corps_matchtable_16/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_roundText = m_goCorpsMatchTable4.transform.Find("round/Image").GetComponent<Text>();
        m_dicAuthers = new Dictionary<CORPS_MEMBER_TYPE, List<p_corps_member_toc>>();

        m_team1id = 0;
        m_team2id = 0;

        m_noGameTip = m_tran.Find("frame_corps_matchbegin/Text").GetComponent<Text>();

        //联赛战场入口
        m_team1 = m_tran.Find("frame_corps_matchbegin/Team1/Image").GetComponent<Image>();
        m_team2 = m_tran.Find("frame_corps_matchbegin/Team2/Image").GetComponent<Image>();

        m_team1flag = m_tran.Find("frame_corps_matchbegin/Team1").GetComponent<Image>();
        m_team2flag = m_tran.Find("frame_corps_matchbegin/Team2").GetComponent<Image>();

        m_team1name = m_tran.Find("frame_corps_matchbegin/Team1/NameBg/Text").GetComponent<Text>();
        m_team2name = m_tran.Find("frame_corps_matchbegin/Team2/NameBg/Text").GetComponent<Text>();

        m_matchtime = m_tran.Find("frame_corps_matchbegin/time/time").GetComponent<Text>();
        m_matchTimedown = m_tran.Find("frame_corps_matchbegin/time/time").GetComponent<Text>();

        m_roomid = 0;
        m_matchteaminfo = false;
        m_matchData = new CorpsData();

        m_curTime = TimeUtil.GetNowTime();

        CorpsDataManager.m_roomdata[0] = new corpsRoomData();
        CorpsDataManager.m_roomdata[1] = new corpsRoomData();
        m_btnJoinGame = m_tran.Find("frame_corps_matchbegin/JoinMatch").gameObject;

        m_voteDeadLine = new DateTime(m_curTime.Year, m_curTime.Month, m_curTime.Day, 19, 0, 10);
        m_matchDeadLine = new DateTime(m_curTime.Year, m_curTime.Month, m_curTime.Day, 20, 0, 10);
        m_signDeadLine = new DateTime(m_curTime.Year, m_curTime.Month, 11, 0, 0, 10);
        //16强视图
        items = new ItemTeam[16];
        round1 = new GameObject[16];
        round2 = new GameObject[8];
        round3 = new GameObject[4];


        for (int i = 1; i <= 2; i++)
        {
            for (int j = 1; j <= 8; j++)
            {
                items[(i - 1) * 8 + j - 1] = new ItemTeam();
                items[(i - 1) * 8 + j - 1].name = m_tran.Find(string.Format("frame_corps_matchtable_all/MatchTable/TeamGroup{0}/ItemTeam{1}/name", i, j)).GetComponent<Text>();
                items[(i - 1) * 8 + j - 1].icon = m_tran.Find(string.Format("frame_corps_matchtable_all/MatchTable/TeamGroup{0}/ItemTeam{1}/flag/biaozhi", i, j)).GetComponent<Image>();
            }
        }
        for (int i = 0; i < 16; i++)
        {
            round1[i] = m_tran.Find(string.Format("frame_corps_matchtable_all/MatchTable/winGroup{0}/roundOne/win{1}", i / 8 + 1, i % 8 + 1)).gameObject;
        }

        for (int i = 0; i < 8; i++)
        {
            round2[i] = m_tran.Find(string.Format("frame_corps_matchtable_all/MatchTable/winGroup{0}/roundTwo/win{1}", i / 4 + 1, i % 4 + 1)).gameObject;
        }

        for (int i = 0; i < 4; i++)
        {
            round3[i] = m_tran.Find(string.Format("frame_corps_matchtable_all/MatchTable/winGroup{0}/roundThree/win{1}", i / 2 + 1, i % 2 + 1)).gameObject;
        }

        m_matchsign = m_tran.Find("frame_corps_matchbegin/final").GetComponent<Image>();
        m_round4 = m_tran.Find("frame_corps_matchtable_4/round").GetComponent<Image>();
        m_round4final = m_tran.Find("frame_corps_matchtable_4/final").GetComponent<Image>();

        m_win1 = m_tran.Find("frame_corps_matchbegin/Team1/win").GetComponent<Image>();
        m_win2 = m_tran.Find("frame_corps_matchbegin/Team2/win").GetComponent<Image>();
        m_EnterBattle = m_tran.Find("frame_corps_matchtable_all/EnterBattle").gameObject;
        m_matchTips = m_tran.Find("frame_corps_matchtable_all/TipsText").gameObject;
        m_Tipsimg = m_tran.Find("frame_corps_matchtable_all/Tipsimg").gameObject;
    }

    public override void Update()
    {
        DateTime cur = TimeUtil.GetNowTime();
        if( m_weekOfDay == 1 )
        {
            m_EnterBattle.TrySetActive(false);
            m_matchTips.TrySetActive(true);
            m_matchTips.GetComponent<Text>().text = "";
            m_Tipsimg.TrySetActive(true);
        }
        else
        {
            if ((cur.Hour == 20 && cur.Minute >= 30) || cur.Hour > 20)
            {
                m_EnterBattle.TrySetActive(true);
                m_matchTips.TrySetActive(false);
            }
            else
            {
                m_EnterBattle.TrySetActive(false);
                m_matchTips.TrySetActive(true);
                m_matchTips.GetComponent<Text>().text = "20:30后可进入战场";
            }
            m_Tipsimg.TrySetActive(false);
        }
        /*DateTime cur = TimeUtil.GetNowTime();
        if (cur.Day == 1)
        {
            if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 0)
            {
                NetLayer.Send(new tos_corpsmatch_is_registered());
            }

            if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 5)
            {

                curCorpsMatch.TrySetActive(false);
                curCorpsMatch = m_goCorpsMatch;
                if (CorpsDataManager.is_signed)
                {
                    m_btnSignUp.TrySetActive(false);
                }
                else
                {
                    m_btnSignUp.TrySetActive(true);
                }
                if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
                {
                    curCorpsMatch.TrySetActive(true);
                }
            }
        }

        if (cur.Day == 26)
        {
            if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 5)
            {
                curCorpsMatch.TrySetActive(false);
                curCorpsMatch = m_goCorpsMatchResult;
                NetLayer.Send(new tos_corpsmatch_result());
                if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
                {
                    curCorpsMatch.TrySetActive(true);
                }
            }
        }

        if (cur.Day >= 21 && cur.Day < 26)
        {
            if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 2)
            {
                NetLayer.Send(new tos_corpsmatch_last_record() { is_last = false });
            }
            if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 5)
            {
                curCorpsMatch.TrySetActive(false);
                curCorpsMatch = m_goCorpsMatchTableAll;
                if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
                {
                    curCorpsMatch.TrySetActive(true);
                }
            }
        }
        if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 0)
        {
            m_matchDeadLine = m_matchDeadLine.AddDays(1);
            m_voteDeadLine = m_voteDeadLine.AddDays(1);
        }



        if (curCorpsMatch == m_goCorpsVote)
        {
            TimeSpan time = TimeUtil.GetRemainTime((uint)(TimeUtil.GetTimeStamp(m_voteDeadLine)));
            int hour = time.Hours;
            int munite = time.Minutes;
            int second = time.Seconds;
            string h;
            string m;
            string s;

            if (hour > 0)
            {
                if (hour > 10)
                {
                    h = hour.ToString() + ":";
                }
                else
                {
                    h = "0" + hour.ToString() + ":";
                }
            }
            else
            {
                h = "00:";
            }
            if (munite < 10)
            {
                m = "0" + munite.ToString() + ":";
            }
            else
            {
                m = munite.ToString() + ":";
            }
            if (second < 10)
            {
                s = "0" + second.ToString();
            }
            else
            {
                s = second.ToString();
            }
            m_voteTimeDown.text = h + m + s;
            if (hour == 0 && munite == 0 && second == 0)
            {
                NetLayer.Send(new tos_corpsmatch_room());
            }
            if (hour <= 0 && munite <= 0 && second <= 0)
            {
                m_voteTimeDown.text = "即将开始";
            }
            if (hour == 0 && munite == 0 && second == -5)
            {
                if (CorpsDataManager.is_signed)
                {
                    curCorpsMatch.TrySetActive(false);
                    curCorpsMatch = m_goCorpsMatchBegin;
                    if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
                    {
                        curCorpsMatch.TrySetActive(true);
                    }
                }
            }
        }*/
        if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 0)
        {
            m_matchDeadLine = m_matchDeadLine.AddDays(1);
        }

        if (curCorpsMatch == m_goCorpsMatchBegin)
        {
            TimeSpan time = TimeUtil.GetRemainTime((uint)(TimeUtil.GetTimeStamp(m_matchDeadLine)));
            int hour = time.Hours;
            int munite = time.Minutes;
            int second = time.Seconds;

            string h;
            string m;
            string s;

            if (hour > 0)
            {
                h = hour.ToString() + ":";
            }
            else
            {
                h = "";
            }
            if (munite < 10)
            {
                m = "0" + munite.ToString() + ":";
            }
            else
            {
                m = munite.ToString() + ":";
            }
            if (second < 10)
            {
                s = "0" + second.ToString();
            }
            else
            {
                s = second.ToString();
            }
            m_matchTimedown.text = h + m + s;
            if (hour <= 0 && munite <= 0 && second <= 0)
            {
                m_matchTimedown.text = "比赛进行中";
            }
//             if (cur.Hour == 0 && cur.Minute == 0 && cur.Second == 0)
//             {
//                 if (cur.Day <= 20 && cur.Day >= 11)
//                 {
//                     NetLayer.Send(new tos_corpsmatch_match_info());
//                     curCorpsMatch.TrySetActive(false);
//                     curCorpsMatch = m_goCorpsVote;
//                     if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
//                     {
//                         curCorpsMatch.TrySetActive(true);
//                     }
//                 }
//             }
        }

       /* if (curCorpsMatch == m_goCorpsMatch)
        {
            TimeSpan time = TimeUtil.GetRemainTime((uint)(TimeUtil.GetTimeStamp(m_signDeadLine)));
            int day = time.Days;
            int hour = time.Hours;
            int munite = time.Minutes;
            int second = time.Seconds;
            string d;
            string h;
            string m;
            string s;

            if (day > 0)
            {
                d = day.ToString() + "天";
            }
            else
            {
                d = "";
            }

            if (hour > 0)
            {
                h = hour.ToString() + ":";
            }
            else
            {
                h = "00:";
            }
            if (munite < 10)
            {
                m = "0" + munite.ToString() + ":";
            }
            else
            {
                m = munite.ToString() + ":";
            }
            if (second < 10)
            {
                s = "0" + second.ToString();
            }
            else
            {
                s = second.ToString();
            }
            m_signTimeDown.text = d + h + m + s;
            if (day == 0 && hour == 0 && munite == 0 && second == 0)
            {
                if (CorpsDataManager.is_signed)
                {
                    NetLayer.Send(new tos_corpsmatch_match_info());
                }
                else
                {
                    m_signTimeDown.text = "小组赛进行中";
                }
            }

            if (day == 0 && hour == 0 && munite == 0 && second == -5)
            {
                if (CorpsDataManager.is_signed)
                {
                    curCorpsMatch.TrySetActive(false);
                    curCorpsMatch = m_goCorpsVote;
                    if (m_curFrame == FRAME_TYPE.TYPE_MATCH)
                    {
                        curCorpsMatch.TrySetActive(true);
                    }
                }
                else
                {
                    m_signTip.gameObject.TrySetActive(false);
                    m_signTip.text = "小组赛火热进行中";
                    m_btnSignUp.TrySetActive(false);
                    m_signTimeDown.gameObject.TrySetActive(false);
                    m_SignGold.gameObject.TrySetActive(false);
                    m_time.TrySetActive(false);
                    m_coin.gameObject.TrySetActive(false);
                    m_noSignTips.TrySetActive(true);
                }
            }
            if (day <= 0 && hour <= 0 && munite <= 0 && second <= 0)
            {
                m_signTimeDown.text = "00:00:00";
            }
        }*/
    }

    public override void InitEvent()
    {
        //战队战场入口
        UGUIClickHandler.Get(m_tran.Find("frame_corps_matchbegin/JoinMatch").gameObject).onPointerClick += JoinMatchRoom;
        //16强对阵图
        UGUIClickHandler.Get(m_tran.Find("frame_corps_matchtable_all/EnterBattle").gameObject).onPointerClick += delegate { Show16List(); };
        UGUIClickHandler.Get(m_tran.Find("frame_corps_matchtable_16/corps16Btn").gameObject).onPointerClick += delegate { UIManager.PopPanel<PanelLastMatch>(null,true); };
        UGUIClickHandler.Get(m_tran.Find("frame_corps_matchtable_8/corps16Btn").gameObject).onPointerClick += delegate { UIManager.PopPanel<PanelLastMatch>(null, true); };
        UGUIClickHandler.Get(m_tran.Find("frame_corps_matchtable_4/corps16Btn").gameObject).onPointerClick += delegate { UIManager.PopPanel<PanelLastMatch>(null, true); };
    }


    public override void OnShow()
    {
        m_round4.gameObject.TrySetActive(true);
        m_round4final.gameObject.TrySetActive(true);
        m_EnterBattle.TrySetActive(false);
        m_matchTips.TrySetActive(true);
        m_win1.gameObject.TrySetActive(false);
        m_win2.gameObject.TrySetActive(false);
        if (curCorpsMatch != null)
        {
            curCorpsMatch.TrySetActive(false);
        }
        curCorpsMatch = m_goCorpsMatchTableAll;
        UpdateView();
        m_curTime = TimeUtil.GetNowTime();
        m_weekOfDay = CorpsDataManager.m_matchWeek;
       /* m_weekOfDay = Convert.ToInt32(m_curTime.DayOfWeek);
        NetLayer.Send(new tos_corpsmatch_last_record() { is_last = false });
        if (CorpsDataManager.m_matchStage == 1 && CorpsDataManager.m_matchWeek >= 2 && m_curTime.Hour >= 21)
        {
            NetLayer.Send(new tos_corpsmatch_record());
        }*/
    }

    public override void OnHide()
    {
        CorpsDataManager.m_bGetCorpsList = false;
        PanelCorps.search_key = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorpsEnter>();
    }

    public override void OnDestroy()
    {
    }

    void Show16List()
    {
        m_round4final.gameObject.TrySetActive(false);
        m_round4.gameObject.TrySetActive(true);
        curCorpsMatch.TrySetActive(false);
        if (m_weekOfDay == 2)
        {
            curCorpsMatch = m_goCorpsMatchTable16;
        }
        else if (m_weekOfDay == 3)
        {
            curCorpsMatch = m_goCorpsMatchTable8;
        }
        else if (m_weekOfDay == 4)
        {
            curCorpsMatch = m_goCorpsMatchTable4;
            m_roundText.text = "半决赛";
        }
        else if (m_weekOfDay == 5)
        {
            curCorpsMatch = m_goCorpsMatchTable4;
            m_round4.gameObject.TrySetActive(false);
            m_round4final.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "3rd"));
            m_round4final.gameObject.TrySetActive(true);
            m_roundText.text = "季军赛";
            //NetLayer.Send(new tos_corpsmatch_match_info());
        }
        else if (m_weekOfDay == 6)
        {
            curCorpsMatch = m_goCorpsMatchTable4;
            m_round4.gameObject.TrySetActive(false);
            m_round4final.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "chanp"));
            m_round4final.gameObject.TrySetActive(true);
            m_roundText.text = "冠军赛";
           // NetLayer.Send(new tos_corpsmatch_match_info());
        }
        if (curCorpsMatch.transform.Find("ScrollDataGrid/content") != null)
        {
            m_matchlist = curCorpsMatch.transform.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
        }
        if (m_matchlist != null)
        {
            m_matchlist.SetItemRender(m_matchlist.gameObject.transform.Find("ItemMatch").gameObject, typeof(ItemMatchList));
            m_matchlist.onItemSelected -= JoinRoom;
        }
        NetLayer.Send(new tos_corpsmatch_match_list());
        curCorpsMatch.TrySetActive(true);
    }

    void Toc_corpsmatch_match_list(toc_corpsmatch_match_list data)
    {
        m_curTime = TimeUtil.GetNowTime();
        curCorpsMatch.TrySetActive(false);
        GameObject go = curCorpsMatch;
        m_round4final.gameObject.TrySetActive(false);
        m_round4.gameObject.TrySetActive(true);
        if (m_curTime.Hour < 20 ||( m_curTime.Hour == 20 && m_curTime.Minute < 30))
        {
            go = m_goCorpsMatchTableAll;
            curCorpsMatch = go;
            curCorpsMatch.TrySetActive(true);
        }
        else
        {
            if (m_weekOfDay == 2)
            {
                go = m_goCorpsMatchTable16;
                m_EnterBattle.TrySetActive(true);
                m_matchTips.TrySetActive(false);
            }
            else if (m_weekOfDay == 3)
            {
                go = m_goCorpsMatchTable8;
            }
            else if (m_weekOfDay == 4)
            {
                go = m_goCorpsMatchTable4;
                m_roundText.text = "半决赛";
            }
            else if (m_weekOfDay == 5)
            {
                go = m_goCorpsMatchTable4;
                m_round4.gameObject.TrySetActive(false);
                m_round4final.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "3rd"));
                m_round4final.gameObject.TrySetActive(true);
                m_roundText.text = "季军赛";

            }
            else if (m_weekOfDay == 6)
            {
                go = m_goCorpsMatchTable4;
                m_roundText.text = "冠军赛";
                m_round4.gameObject.TrySetActive(false);
                m_round4final.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "chanp"));
                m_round4final.gameObject.TrySetActive(true);
            }
            curCorpsMatch = go;
            curCorpsMatch.TrySetActive(true);
            if (go.transform.Find("ScrollDataGrid/content") != null)
            {
                m_matchlist = go.transform.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
            }
            
            if (m_matchlist != null)
            {
                m_matchlist.SetItemRender(m_matchlist.gameObject.transform.Find("ItemMatch").gameObject, typeof(ItemMatchList));
                if (m_weekOfDay == 1)
                {
                    m_matchlist.Data = new object[]{};
                    return;
                }
                m_matchlist.onItemSelected -= JoinRoom;
                m_matchlist.Data = data.list;
                m_matchlist.autoSelectFirst = false;
                m_matchlist.onItemSelected += JoinRoom;
            }

//             if (m_curTime.Hour >= 21)
//             {
//                 if (m_weekOfDay == 5)
//                 {
//                     m_matchsign.gameObject.TrySetActive(true);
//                     m_matchsign.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "3rd"));
//                     NetLayer.Send(new tos_corpsmatch_match_info());
//                 }
//                 if (m_weekOfDay == 6)
//                 {
//                     m_matchsign.gameObject.TrySetActive(true);
//                     m_matchsign.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "chanp"));
//                     NetLayer.Send(new tos_corpsmatch_match_info());
//                 }
//                 
            //}
            //NetLayer.Send(new tos_corpsmatch_room());
        }
    }

    private void JoinMatchRoom(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_joinroom_join() { channel = 8, room = m_roomid, spectator = false, password = "" });
    }

    void JoinRoom(object renderdata)
    {
        m_curTime = TimeUtil.GetNowTime();
        var info = renderdata as p_corpsmatch_room;
        if (info.team1 == CorpsDataManager.GetCorpsName() || info.team2 == CorpsDataManager.GetCorpsName())
        {

            m_roomid = info.room_id;
            curCorpsMatch.TrySetActive(false);
            NetLayer.Send(new tos_corpsmatch_match_info());
            m_round4final.gameObject.TrySetActive(false);
            m_round4.gameObject.TrySetActive(true);
            if (m_curTime.Hour >= 21)
            {
                curCorpsMatch = m_goCorpsMatchBegin;
                Is_matchTime = true;
                Is_signTime = false;
                Is_voteTime = false;
                if (m_weekOfDay == 5)
                {
                    m_matchsign.gameObject.TrySetActive(true);
                    m_matchsign.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "3rd"));
                }
                if (m_weekOfDay == 6)
                {
                    m_matchsign.gameObject.TrySetActive(true);
                    m_matchsign.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "chanp"));
                }
                //NetLayer.Send(new tos_corpsmatch_room());
            }
            /*else
            {
                curCorpsMatch = m_goCorpsVote;
            }*/

            curCorpsMatch.TrySetActive(true);
        }
        else
        {
            NetLayer.Send(new tos_joinroom_join() { channel = 8, room = info.room_id, spectator = true, password = "" });
            if (info.team1 != null)
            {
                CorpsDataManager.m_roomdata[0].corpsname = info.team1;
                CorpsDataManager.m_roomdata[0].corpsicon = info.icon1;
            }
            if (info.team2 != null)
            {
                CorpsDataManager.m_roomdata[1] = new corpsRoomData();
                CorpsDataManager.m_roomdata[1].corpsname = info.team2;
                CorpsDataManager.m_roomdata[1].corpsicon = info.icon2;
            }
        }
    }
    public void UpdateView()
    {
        CheckCorpsData();
    }

    void Toc_corpsmatch_room(toc_corpsmatch_room data)
    {
        m_curTime = TimeUtil.GetNowTime();
        m_roomid = CorpsDataManager.corpsmatch_roomid;
        if (CorpsDataManager.m_matchStage == 1 && CorpsDataManager.m_matchWeek >= 2 && CorpsDataManager.m_matchWeek <= 6)
        {
            if (m_roomid == 0)
            {
                m_tran.Find("frame_corps_matchbegin/JoinMatch").gameObject.TrySetActive(false);
                if (m_curTime.Hour >= 21)
                {
                    if (!m_noGameTip.IsActive())
                    {
                        m_noGameTip.text = "今日比赛已结束";
                        m_noGameTip.gameObject.TrySetActive(true);
                    }
                }
            }
        }
    }

    void Toc_corpsmatch_match_info(toc_corpsmatch_match_info data)
    {
        curCorpsMatch.TrySetActive(false);
        curCorpsMatch = m_goCorpsMatchBegin;
        curCorpsMatch.TrySetActive(true);
        m_team1id = data.my_id;
        m_team2id = data.other_id;
        m_matchteaminfo = true;
        CorpsDataManager.m_roomdata = new corpsRoomData[2];
        CorpsDataManager.m_corpsrule = data.mode;
        if (m_team2id != 0)
        {
            NetLayer.Send(new tos_corps_view_info() { corps_id = m_team2id });
        }
        updateTeamView();
    }

    void updateTeamView()
    {
        m_team1name.text = CorpsDataManager.m_corpsData.name;
       // m_voteName1.text = CorpsDataManager.m_corpsData.name;
        string flag = "f" + CorpsDataManager.m_corpsData.logo_pannel.ToString() + "_" + "c" + CorpsDataManager.m_corpsData.logo_color.ToString();
        m_team1flag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
       // m_voteflag1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        string biaozhi = "biaozhi_" + CorpsDataManager.m_corpsData.logo_icon.ToString();
        m_team1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
       // m_voteteam1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
       
        CorpsDataManager.m_roomdata[0] = new corpsRoomData();
        CorpsDataManager.m_roomdata[0].corpsicon = CorpsDataManager.m_corpsData.logo_icon;
        CorpsDataManager.m_roomdata[0].corpsname = CorpsDataManager.m_corpsData.name;
        CorpsDataManager.m_roomdata[0].corpsid = CorpsDataManager.m_corpsData.corps_id;
        CorpsDataManager.m_roomdata[0].corpscolor = CorpsDataManager.m_corpsData.logo_color;
        CorpsDataManager.m_roomdata[0].corpsflag = CorpsDataManager.m_corpsData.logo_pannel;

        var noTeam2 = m_team2id == 0;

        m_team2name.gameObject.TrySetActive(!noTeam2);
        m_team2flag.gameObject.TrySetActive(!noTeam2);
        m_team2.gameObject.TrySetActive(!noTeam2);

        if (noTeam2)
            m_noGameTip.text = "今日轮空，没有比赛";
        m_noGameTip.gameObject.TrySetActive(noTeam2);
        m_btnJoinGame.TrySetActive(!noTeam2);


        if (m_team2id == 0)
        {
            m_team2name.gameObject.TrySetActive(false);
            m_team2flag.gameObject.TrySetActive(false);
            m_team2.gameObject.TrySetActive(false);
            m_noGameTip.text = "今日轮空，没有比赛";
            m_noGameTip.gameObject.TrySetActive(true);
        }
        else
        {
            m_team2name.gameObject.TrySetActive(true);
            m_team2flag.gameObject.TrySetActive(true);
            m_team2.gameObject.TrySetActive(true);
            m_noGameTip.gameObject.TrySetActive(false);
        }
    }

    private void Toc_corps_view_info(toc_corps_view_info proto)
    {
        if (!m_matchteaminfo)
        {
            UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, this);
        }
        else
        {

            m_team2name.text = proto.corps_name;
            string flag = "f" + proto.logo_pannel.ToString() + "_" + "c" + proto.logo_color.ToString();
            m_team2flag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            string biaozhi = "biaozhi_" + proto.logo_icon.ToString();
            m_team2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));

            CorpsDataManager.m_roomdata[1] = new corpsRoomData();
            CorpsDataManager.m_roomdata[1].corpsicon = proto.logo_icon;
            CorpsDataManager.m_roomdata[1].corpsname = proto.corps_name;
            CorpsDataManager.m_roomdata[1].corpsid = proto.corps_id;
            CorpsDataManager.m_roomdata[1].corpsflag = proto.logo_pannel;
            CorpsDataManager.m_roomdata[1].corpscolor = proto.logo_color;
        }
        m_matchteaminfo = false;
    }

   
    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }
   

    void Toc_corpsmatch_last_record(toc_corpsmatch_last_record data)
    {
        if (UIManager.IsOpen<PanelLastMatch>())
        {
            return;
        }

        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 5)
            {
                var temp = data.records[i];
                data.records[i] = data.records[data.records.Length - 1];
                data.records[data.records.Length - 1] = temp;
            }
        }

        for (int i = 0; i < items.Length; i++)
        {
            items[i].name.text = "";
            items[i].icon.gameObject.TrySetActive(false);
        }

        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 2)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    items[data.records[i].teams[j].pos - 1].name.text = data.records[i].teams[j].name;
                    string biaozhi = "biaozhi_" + data.records[i].teams[j].icon.ToString();
                    items[data.records[i].teams[j].pos - 1].icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
                    items[data.records[i].teams[j].pos - 1].icon.gameObject.TrySetActive(true);
                    if (data.records[i].teams[j].name == CorpsDataManager.GetCorpsName())
                    {
                        items[data.records[i].teams[j].pos - 1].name.color = new Color32(134, 253, 131, 255);
                    }
                }
            }
        }
        for (int i = 0; i < round1.Length; i++)
        {
            round1[i].TrySetActive(false);
        }
        for (int i = 0; i < round2.Length; i++)
        {
            round2[i].TrySetActive(false);
        }
        for (int i = 0; i < round3.Length; i++)
        {
            round3[i].TrySetActive(false);
        }
        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 3)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round1[e].gameObject.TrySetActive(true);
                        }
                    }
                }
            }
            if (data.records[i].day == 4)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round2[e / 2].gameObject.TrySetActive(true);
                        }
                    }
                }
            }
            if (data.records[i].day == 5)
            {
                for (int t = 0; t < 4; t++)
                {
                    if (round2[t * 2].gameObject.activeSelf || round2[t * 2 + 1].gameObject.activeSelf)
                    {
                        round3[t].gameObject.TrySetActive(true);
                    }
                }
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round3[e / 4].gameObject.TrySetActive(false);
                        }
                    }
                }
            }
        }
    }

    /*void Toc_corpsmatch_result(toc_corpsmatch_result data)
    {
        if (data.team.Length >= 1)
        {
            m_name1.text = data.team[0].name;
            string biaozhi = "biaozhi_" + data.team[0].icon;
            m_icon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[0].panel + "_" + "c" + data.team[0].color.ToString();
            m_flag1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        }
        if (data.team.Length >= 2)
        {
            m_name2.text = data.team[1].name;
            string biaozhi = "biaozhi_" + data.team[1].icon;
            m_icon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[1].panel + "_" + "c" + data.team[1].color.ToString();
            m_flag2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        }

        if (data.team.Length >= 3)
        {
            m_name3.text = data.team[2].name;
            string biaozhi = "biaozhi_" + data.team[2].icon;
            m_icon3.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            string flag = "f" + data.team[2].panel + "_" + "c" + data.team[2].color.ToString();
            m_flag3.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        }
    }*/

    void Toc_corpsmatch_full(toc_corpsmatch_full data)
    {
        string info = "房间已满，确定要进入观战席吗？";
        Action a = () => { NetLayer.Send(new tos_joinroom_join() { channel = 8, password = "", room = m_roomid, spectator = true }); };
        UIManager.ShowTipPanel(info, a, null, true, true, "确定", "取消");
    }

    //刷新数据
    void CheckCorpsData()
    {
        //进入16强
        if (CorpsDataManager.m_matchWeek >= 1 && CorpsDataManager.m_matchWeek <= 6)
        {
            NetLayer.Send(new tos_corpsmatch_match_list());
            NetLayer.Send(new tos_corpsmatch_last_record() { is_last = false });
        }
        /*if (CorpsDataManager.m_matchWeek == 7)
        {
            NetLayer.Send(new tos_corpsmatch_result()); 
        }*/
    }

    void Toc_corpsmatch_record(toc_corpsmatch_record data)
    {
        m_curTime = TimeUtil.GetNowTime();
        for (int i = 0; i < data.records.Length; i++)
        {
            DateTime datetime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long ltime = long.Parse(data.records[i].day.ToString() + "0000000");
            TimeSpan tonow = new TimeSpan(ltime);
            DateTime d = datetime.Add(tonow);
            if (d.Month == m_curTime.Month && d.Day == m_curTime.Day)
            {
                m_win1.gameObject.TrySetActive(true);
                m_win2.gameObject.TrySetActive(true);
                m_btnJoinGame.TrySetActive(false);
                m_noGameTip.gameObject.TrySetActive(true);
                if (data.records[i].win == 1)
                {
                    m_win1.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "winsettle"));
                    m_win2.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
                }
                else if (data.records[i].win == 2)
                {
                    m_win1.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
                    m_win2.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "winsettle"));
                }
                else if (data.records[i].win == 0)
                {
                    m_win1.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
                    m_win2.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
                }

            }
        }
    }
}
