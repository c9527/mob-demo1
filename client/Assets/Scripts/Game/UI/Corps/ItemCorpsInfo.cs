﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemCorpsInfo : ItemRender
{
    private BaseCorpsData m_data;
    private Text m_txtQueue;
    private Text m_txtName;
    private Text m_txtFight;
    private Text m_txtCaptain;
    private Text m_txtCapacity;
    private Text m_txtEnterlimit;


    public override void Awake()
    {
        m_txtQueue = transform.Find("queue").GetComponent<Text>();
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_txtFight = transform.Find("fight").GetComponent<Text>();
        m_txtCaptain = transform.Find("captain").GetComponent<Text>();
        m_txtCapacity = transform.Find("capacity").GetComponent<Text>();
        m_txtEnterlimit = transform.Find("enterlimit").GetComponent<Text>();
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        UGUIClickHandler.Get(gameObject).onDoublePointerClick += onDoubleClickCropsItem;
#endif
    }

    private void onDoubleClickCropsItem(GameObject target, PointerEventData eventData)
    {
        GameDispatcher.Dispatch<BaseCorpsData>(GameEvent.UI_DOUBLE_CLICK_CORPSLIST, m_data);
    }

    protected override void OnSetData(object data)
    {
        m_data = data as BaseCorpsData;
        var queue = m_data.m_iQueue;
        m_txtQueue.text = "";
        if (queue > 99)
        {
            m_txtQueue.text = queue.ToString();
            transform.Find("queue/icon").gameObject.TrySetActive(false);
            transform.Find("queue/Num").gameObject.TrySetActive(false);
        }
        else if (queue > 9)
        {
            transform.Find("queue/icon").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue / 10)));
            transform.Find("queue/icon").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/icon").gameObject.TrySetActive(true);
            transform.Find("queue/Num").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (queue % 10)));
            transform.Find("queue/Num").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/Num").gameObject.TrySetActive(true);

        }
        else
        {
            transform.Find("queue/icon").gameObject.TrySetActive(false);
            transform.Find("queue/Num").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, queue < 4 ? "rank" + queue : "rank_" + queue));
            transform.Find("queue/Num").GetComponent<Image>().SetNativeSize();
            transform.Find("queue/Num").gameObject.TrySetActive(true);
        }
        m_txtName.text = m_data.corps_name;
        m_txtFight.text = m_data.fighting.ToString();
        m_txtCaptain.text = m_data.leader_name;
        m_txtCapacity.text = m_data.member_cnt.ToString() + "/" + m_data.member_max.ToString();
        if (m_data.enter_level == -1)
        {
            m_txtEnterlimit.text = "拒绝";
        }
        else if (m_data.enter_level == 0)
        {
            m_txtEnterlimit.text = "验证";
        }
        else
        {
            m_txtEnterlimit.text = "等级:" + m_data.enter_level;
        }

        string flag = "f" + m_data.logo_pannel.ToString() + "_" + "c" + m_data.logo_color.ToString();
        transform.FindChild("logo/flag").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        string biaozhi = "biaozhi_" + m_data.logo_icon.ToString();
        transform.FindChild("logo/biaozhi").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));

        UGUIClickHandler.Get(gameObject).onPointerClick += OnSelect;
    }

    protected void OnSelect(GameObject target, PointerEventData eventData)
    {
    }

}
