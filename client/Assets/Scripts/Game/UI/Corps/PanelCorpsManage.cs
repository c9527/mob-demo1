﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI.Corps
{
    internal class PanelCorpsManage : BasePanel
    {
        private static int BTN_WIDTH = 132;
        private static int BTN_SPACE = 8;
        private GameObject m_goApply;
        private GameObject m_goCorpsMembers;
        private GameObject m_goQuit;
        private GameObject m_goSetting;
        private Text m_memberCount;
        private DataGrid m_memberListDataGrid;
        private Button[] m_sortBtns;
        public GameObject dropTarget;
        private GameObject m_applyBubble;
        private Text m_applyNum;

        private ItemDropInfo[] _clickMemberOptions;
        public ItemDropInfo[] ClickMemberOptions
        {
            get { return _clickMemberOptions; }
        }
        private ItemDropInfo[] _clickViceLeaderOptions;
        public ItemDropInfo[] ClickViceLeaderOptions
        {
            get { return _clickViceLeaderOptions; }
        }
        private ItemDropInfo[] _clickLeaderOptions;
        public ItemDropInfo[] ClickLeaderOptions
        {
            get { return _clickLeaderOptions; }
        }

        private static Color GREY_COLOR = new Color(129 / 255f, 129 / 255f, 129 / 255f);
        private static Color WHITE_COLOR = new Color(1f,1f,1f);

        public PanelCorpsManage()
        {
            SetPanelPrefabPath("UI/Corps/PanelCorpsManage");
            AddPreLoadRes("UI/Corps/PanelCorpsManage", EResType.UI);
            AddPreLoadRes("UI/Corps/ItemCorpsManageMember", EResType.UI);
            AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        }

        public class MemberManageOption
        {
            public const string LOOK_UP = "1";
            public const string KICK_OUT = "2";
            public const string AUTHORIZE = "3";
            public const string CHANGE_LEADER = "4";
        }

        public override void Init()
        {
            m_goCorpsMembers = m_tran.Find("frame_corps_members").gameObject;
            m_goApply = m_tran.Find("frame_corps_members/operation/apply").gameObject;
            m_goQuit = m_tran.Find("frame_corps_members/operation/quit").gameObject;
            m_goSetting = m_tran.Find("frame_corps_members/operation/setting").gameObject;
            m_memberCount = m_tran.Find("member/count").GetComponent<Text>();
            dropTarget = m_tran.Find("dropTarget").gameObject;
            m_applyBubble = m_tran.Find("frame_corps_members/operation/apply/bubble_tip").gameObject;
            m_applyNum = m_tran.Find("frame_corps_members/operation/apply/bubble_tip/Text").GetComponent<Text>();

            m_tran.Find("frame_corps_members/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
            m_memberListDataGrid = m_tran.Find("frame_corps_members/ScrollDataGrid/content").GetComponent<DataGrid>();
            m_memberListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemCorpsManageMember"),
                typeof (ItemManagerMemberInfo));
            m_memberListDataGrid.useLoopItems = true;

            m_sortBtns = new Button[]
            {
                m_tran.Find("frame_corps_members/title/order_1").GetComponent<Button>(),
                m_tran.Find("frame_corps_members/title/order_2").GetComponent<Button>(),
                m_tran.Find("frame_corps_members/title/order_3").GetComponent<Button>(),
                m_tran.Find("frame_corps_members/title/order_4").GetComponent<Button>(),
            };

            for (int i = 0; i < m_sortBtns.Length; i++)
            {
                var image = m_sortBtns[i].transform.Find("content/up").GetComponent<Image>();
                Util.SetGrayShader(image);
                image.color = GREY_COLOR;
                image = m_sortBtns[i].transform.Find("content/down").GetComponent<Image>();
                Util.SetGrayShader(image);
                image.color = GREY_COLOR;
            }

            _clickMemberOptions = new ItemDropInfo[]
            {
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.LOOK_UP, label = "查看信息" }, 
            };

            _clickViceLeaderOptions = new ItemDropInfo[]
            {
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.LOOK_UP, label = "查看信息" }, 
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.KICK_OUT, label = "踢出战队" },
            };

            _clickLeaderOptions = new ItemDropInfo[]
            {
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.LOOK_UP, label = "查看信息" }, 
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.KICK_OUT, label = "踢出战队" },
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.AUTHORIZE, label = "授予职务" },
                 new ItemDropInfo { type = "CorpManageMember", subtype = MemberManageOption.CHANGE_LEADER, label = "转让队长" },
            };
        }

        public override void InitEvent()
        {
            UGUIClickHandler.Get(m_tran.Find("frame_corps_members/operation/apply").gameObject).onPointerClick +=
                OnClkCorpsApply;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_members/operation/setting").gameObject).onPointerClick +=
                OnClkCorpsSetting;
            UGUIClickHandler.Get(m_tran.Find("frame_corps_members/operation/quit").gameObject).onPointerClick +=
                OnClkCorpsQuit;
            UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;

            for (int i = 0; i < m_sortBtns.Length; i++)
            {
                UGUIClickHandler.Get(m_sortBtns[i].gameObject).onPointerClick += OnClkCorpsSort;
            }
        }

        public override void OnShow()
        {
            CorpsDataManager.m_bGetCorpsList = true;
            dropTarget.TrySetActive(false);
            Transform m_transCorpsMember = m_goCorpsMembers.transform;
            p_corps_member_toc mySelf = CorpsDataManager.GetSelf();
            int hideBtnNum = 0;
            int sumBtnNum = 3;
            int sumBtnWidth = sumBtnNum*BTN_WIDTH + (sumBtnNum - 1)*BTN_SPACE;

            if (mySelf == null || !CorpsDataManager.CanSeeApplyList())
            {
                m_goApply.TrySetActive(false);
                hideBtnNum++;
            }
            else
            {
                m_goApply.TrySetActive(true);
                if (CorpsDataManager.GetApplyNum() > 0)
                {
                    m_applyBubble.TrySetActive(true);
                    m_applyNum.GetComponent<Text>().text = CorpsDataManager.GetApplyNum().ToString();
                }
                else
                {
                    m_applyBubble.TrySetActive(false);
                }
            }

            if (mySelf == null || mySelf.member_type > (int) CORPS_MEMBER_TYPE.TYPE_VICELEADER)
            {
                m_goSetting.TrySetActive(false);
                hideBtnNum++;
            }
            else
            {
                m_goSetting.TrySetActive(true);
            }
            m_goQuit.TrySetActive(true);
            Vector2 sizeData = m_transCorpsMember.FindChild("operation").GetComponent<RectTransform>().sizeDelta;
            sizeData.x = sumBtnWidth - (BTN_WIDTH + BTN_SPACE)*hideBtnNum;
            m_transCorpsMember.FindChild("operation").GetComponent<RectTransform>().sizeDelta = sizeData;

                      
            m_memberListDataGrid.Data = CorpsDataManager.m_lstMember.ToArray();
            m_memberCount.text = CorpsDataManager.m_lstMember.Count.ToString() + "/" + CorpsDataManager.m_corpsData.max_member.ToString();
        }


        public void DoOption(string subType, p_corps_member_toc m_data)
        {
            if (subType == MemberManageOption.KICK_OUT)
                OnClkMemberKickout(null, null);
            else if (subType == MemberManageOption.LOOK_UP)
                OnLookUp(null, null);
            else if (subType == MemberManageOption.AUTHORIZE)
            {
                OnClkMemberAuthorize(null, null);
            }
            else if (subType == MemberManageOption.CHANGE_LEADER)
            {
                OnClkCorpsChangeLeader(null, null);
            }
        }

        public void UpdateView()
        {
            m_memberListDataGrid.Data = CorpsDataManager.m_lstMember.ToArray();
        }

        public override void OnHide()
        {
        }

        public override void OnBack()
        {
            UIManager.ShowPanel<PanelCorps>();
        }

        public override void OnDestroy()
        {
        }

        public override void Update()
        {
            
        }

        protected void OnClkCorpsApply(GameObject target, PointerEventData eventData)
        {
            p_corps_member_toc self = CorpsDataManager.GetSelf();
            if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
                return;

            if (self.member_type >= (int) CORPS_MEMBER_TYPE.TYPE_MEMBER)
            {
                TipsManager.Instance.showTips("无权限同意申请");
                return;
            }
            UIManager.PopPanel<PanelCorpsApply>(new object[] {CorpsDataManager.m_lstApply}, true, this);
        }

        private void OnClkCorpsSetting(GameObject target, PointerEventData eventData)
        {
            p_corps_member_toc self = CorpsDataManager.GetSelf();
            if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
                return;
            if (self.member_type >= (int) CORPS_MEMBER_TYPE.TYPE_MEMBER)
            {
                TipsManager.Instance.showTips("无权限设置申请");
                return;
            }
            UIManager.PopPanel<PanelCorpsSetting>(new object[] {CorpsDataManager.m_corpsData.enter_level}, true, this);
        }
        /// <summary>
        /// 战队成员排序,列index从1开始,以2取模,0是降序,1是升序
        /// </summary>
        /// <param name="target"></param>
        /// <param name="eventData"></param>
        private void OnClkCorpsSort(GameObject target, PointerEventData eventData)
        {
            int sortFlag = (int)CorpsDataManager.SortFlag;
            var clickIndex = Convert.ToInt32(target.name.Substring(6));
            var lastSortArrowIndex = sortFlag / 2;
            var sortOrder = sortFlag % 2;
            Image selectedImage, deselectedImage;
            string childObjName, deselectChildObjName;
            if (sortOrder == (int) CORPS_SORT_ORDER.DESC)
            {
                childObjName = "content/down";
                deselectChildObjName = "content/up";
            }
            else
            {
                childObjName = "content/up";
                deselectChildObjName = "content/down";
            }
            
           
            if (clickIndex == lastSortArrowIndex)
            {
                if (sortOrder == (int)CORPS_SORT_ORDER.ASC)
                {
                    selectedImage = m_sortBtns[lastSortArrowIndex - 1].transform.Find(childObjName).GetComponent<Image>();
                    Util.SetGrayShader(selectedImage);
                    selectedImage.color = GREY_COLOR;
                    selectedImage.gameObject.TrySetActive(true);
                    deselectedImage = m_sortBtns[lastSortArrowIndex - 1].transform.Find(deselectChildObjName).GetComponent<Image>();
                    Util.SetGrayShader(deselectedImage);
                    deselectedImage.color = GREY_COLOR;
                    deselectedImage.gameObject.TrySetActive(true);
                    CorpsDataManager.SortFlag = CORPS_MEMBER_SORT_COLS.DEFAULT;
                }
                else
                {
                    CorpsDataManager.SortFlag = (CORPS_MEMBER_SORT_COLS)(++sortFlag);
                    sortOrder++;
                    if (sortOrder == (int)CORPS_SORT_ORDER.DESC)
                    {
                        childObjName = "content/down";
                        deselectChildObjName = "content/up";
                    }
                    else
                    {
                        childObjName = "content/up";
                        deselectChildObjName = "content/down";
                    }
                    selectedImage = m_sortBtns[clickIndex - 1].transform.Find(childObjName).GetComponent<Image>();
                    Util.SetNormalShader(selectedImage);
                    selectedImage.color = WHITE_COLOR;
                    selectedImage.gameObject.TrySetActive(true);
                    m_sortBtns[clickIndex - 1].transform.Find(deselectChildObjName).gameObject.TrySetActive(false);
                }
            }
            else
            {
                if (sortFlag != (int)CORPS_MEMBER_SORT_COLS.DEFAULT)
                {
                    selectedImage = m_sortBtns[lastSortArrowIndex - 1].transform.Find(childObjName).GetComponent<Image>();
                    Util.SetGrayShader(selectedImage);
                    selectedImage.color = GREY_COLOR;
                    selectedImage.gameObject.TrySetActive(true);
                    deselectedImage = m_sortBtns[lastSortArrowIndex - 1].transform.Find(deselectChildObjName).GetComponent<Image>();
                    Util.SetGrayShader(deselectedImage);
                    deselectedImage.color = GREY_COLOR;
                    deselectedImage.gameObject.TrySetActive(true);
                }
                CorpsDataManager.SortFlag = (CORPS_MEMBER_SORT_COLS)(clickIndex * 2);
                selectedImage = m_sortBtns[clickIndex - 1].transform.Find("content/down").GetComponent<Image>();
                Util.SetNormalShader(selectedImage);
                selectedImage.color = WHITE_COLOR;
                selectedImage.gameObject.TrySetActive(true);
                m_sortBtns[clickIndex - 1].transform.Find("content/up").gameObject.TrySetActive(false);
            }
            CorpsDataManager.SortMemberList();
            UpdateView();
        }

        protected void OnClkCorpsQuit(GameObject target, PointerEventData eventData)
        {
            if (CorpsDataManager.m_corpsData == null)
                return;
            if (CorpsDataManager.m_corpsData.leader_id == PlayerSystem.roleId)
            {
                if (CorpsDataManager.m_lstMember.Count <= 1)
                {
                    string info = "你确定要退出并解散该战队吗？战队解散后贡献清0，无法恢复，是否决定退出？";
                    UIManager.ShowTipPanel(info, () => { NetLayer.Send(new tos_corps_dismiss()); }, null, true, true,
                        "确定", "取消");
                }
                else
                {
                    UIManager.ShowTipPanel("队长必须先移交队长职务，才能退出战队");
                }
                return;
            }
            string strDesc = "是否退出战队";
            UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_corps_quit()); }, null, true, true, "确定", "取消");
        }


        protected void OnLookUp(GameObject target, PointerEventData eventData)
        {
            var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择查看信息的人");
                return;
            }

            if (item.id == PlayerSystem.roleId)
            {
                return;
            }

            PlayerFriendData.ViewPlayer(item.id);
        }

        private void Toc_player_view_player(toc_player_view_player data)
        {
            if (!UIManager.IsOpen<PanelMainPlayerInfo>())
            {
                UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
            }
        }

        protected void OnClkCorpsChangeLeader(GameObject target, PointerEventData eventData)
        {
            if (PlayerSystem.roleId != CorpsDataManager.m_corpsData.leader_id)
            {
                TipsManager.Instance.showTips("队长才有权利授予职位");
                return;
            }

            var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择转让队长的成员");
                return;
            }

            if (item.id == PlayerSystem.roleId)
            {
                return;
            }

            string strDesc = string.Format("是否转让队长给{0}", item.name);
            UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_corps_set_leader {leader_id = item.id}); },
                null, true, true, "确定", "取消");
        }

        protected void OnClkMemberAuthorize(GameObject target, PointerEventData eventData)
        {
            p_corps_member_toc self = CorpsDataManager.GetSelf();
            if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
                return;

            var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
            if (item == null)
            {
                UIManager.ShowTipPanel("请选择分派职位的人!");
                return;
            }

            if (item.id == PlayerSystem.roleId)
            {
                return;
            }

            if (self.member_type > (int) CORPS_MEMBER_TYPE.TYPE_LEADER)
            {
                TipsManager.Instance.showTips("队长才有权利授予职位!");
                return;
            }
            UIManager.PopPanel<PanelAuther>(new object[] {item.id, item.member_type}, true, this);
        }

        protected void OnClkMemberKickout(GameObject target, PointerEventData eventData)
        {
            p_corps_member_toc self = CorpsDataManager.GetSelf();
            if (self == null || CorpsDataManager.m_corpsData.corps_id == 0)
                return;

            var item = m_memberListDataGrid.SelectedData<p_corps_member_toc>();
            if (item == null)
            {
                TipsManager.Instance.showTips("请选择踢出的人");
                return;
            }

            if (item.id == PlayerSystem.roleId)
            {
                TipsManager.Instance.showTips("不能踢出自己");
                return;
            }

            if (self.member_type >= (int) CORPS_MEMBER_TYPE.TYPE_MEMBER)
            {
                TipsManager.Instance.showTips("队员无权利踢人!");
                return;
            }

            string strDesc = "";
            if (item.level <= 15)
            {
                strDesc = "踢出等级较低的成员，会清除该成员在战队中的贡献值，确认将其踢出吗";
            }
            else
            {
                strDesc = string.Format("是否将{0}请出战队!", item.name);
            }
            UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_corps_kick_member {member_id = item.id}); },
                null, true, true, "确定", "取消");
        }

        private void OnDropClick(GameObject target, PointerEventData eventData)
        {
            UIManager.HideDropList();
            dropTarget.TrySetActive(false);
        }
    }
}