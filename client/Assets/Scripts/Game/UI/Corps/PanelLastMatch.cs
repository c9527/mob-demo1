﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
///////////////////////////////////////////
//Copyright (C): 4399 YiDao studio
//All rights reserved
//文件描述：现在是本届的16强
//创建者：hwl
//创建日期: 2015/12/29
///////////////////////////////////////////

public class PanelLastMatch : BasePanel
{
    public class ItemTeam
    {
        public Text name;
        public Image icon;
    }

    ItemTeam[] items;
    GameObject[] round1;
    GameObject[] round2;
    GameObject[] round3;

    public PanelLastMatch()
    {
        SetPanelPrefabPath("UI/Corps/PanelLastMatch");
        AddPreLoadRes("UI/Corps/PanelLastMatch", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corpsmatch_last_record() { is_last = false });
    }
    
    public override void Init()
    {
        items = new ItemTeam[16];
        round1 = new GameObject[16];
        round2 = new GameObject[8];
        round3 = new GameObject[4];
     

        for (int i = 1; i <= 2; i++)
        {
            for (int j = 1; j <= 8; j++)
            {
                items[(i - 1) * 8 + j - 1] = new ItemTeam();
                items[(i - 1) * 8 + j - 1].name = m_tran.Find(string.Format("TeamGroup{0}/ItemTeam{1}/name", i, j)).GetComponent<Text>();
                items[(i - 1) * 8 + j - 1].icon = m_tran.Find(string.Format("TeamGroup{0}/ItemTeam{1}/flag/biaozhi", i, j)).GetComponent<Image>();
            }
        }
        for (int i = 0; i < 16; i++)
        {
            round1[i] = m_tran.Find(string.Format("winGroup{0}/roundOne/win{1}", i / 8 + 1, i % 8 + 1)).gameObject;
        }

        for (int i = 0; i < 8; i++)
        {
            round2[i] = m_tran.Find(string.Format("winGroup{0}/roundTwo/win{1}", i / 4 + 1, i % 4 + 1)).gameObject;
        }

        for (int i = 0; i < 4; i++)
        {
            round3[i] = m_tran.Find(string.Format("winGroup{0}/roundThree/win{1}", i / 2 + 1, i % 2 + 1)).gameObject;
        }
        for (int i = 0; i < items.Length; i++)
        {
            items[i].name.text = "";
            items[i].icon.gameObject.TrySetActive(false);
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };

    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void Update()
    {

    }

    void Toc_corpsmatch_last_record(toc_corpsmatch_last_record data)
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].name.text = "";
            items[i].icon.gameObject.TrySetActive(false);
        }
        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 5)
            {
                var temp = data.records[i];
                data.records[i] = data.records[data.records.Length - 1];
                data.records[data.records.Length - 1] = temp;
            }
        }

        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 2)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    items[data.records[i].teams[j].pos-1].name.text = data.records[i].teams[j].name;
                    string biaozhi = "biaozhi_" + data.records[i].teams[j].icon.ToString();
                    items[data.records[i].teams[j].pos - 1].icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
                    items[data.records[i].teams[j].pos - 1].icon.gameObject.TrySetActive(true);
                }
            }
        }
        for (int i = 0; i < round1.Length; i++)
        {
            round1[i].TrySetActive(false);
        }
        for (int i = 0; i < round2.Length; i++)
        {
            round2[i].TrySetActive(false);
        }
        for (int i = 0; i < round3.Length; i++)
        {
            round3[i].TrySetActive(false);
        }
        for (int i = 0; i < data.records.Length; i++)
        {
            if (data.records[i].day == 3)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round1[e].gameObject.TrySetActive(true);
                        }
                    }
                }
            }
            if (data.records[i].day == 4)
            {
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round2[e/2].gameObject.TrySetActive(true);
                        }
                    }
                }
            }
            if (data.records[i].day == 5)
            {
                for (int t = 0; t < 4; t++)
                {
                    if (round2[t * 2].gameObject.activeSelf || round2[t * 2 + 1].gameObject.activeSelf)
                    {
                        round3[t].gameObject.TrySetActive(true);
                    }
                }
                for (int j = 0; j < data.records[i].teams.Length; j++)
                {
                    for (int e = 0; e < items.Length; e++)
                    {
                        if (items[e].name.text == data.records[i].teams[j].name)
                        {
                            round3[e / 4].gameObject.TrySetActive(false);
                        }
                    }
                }
            }
        }
    }
}
