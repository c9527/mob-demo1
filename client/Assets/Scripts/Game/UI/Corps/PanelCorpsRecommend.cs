﻿using UnityEngine;
using System.Collections;

public class PanelCorpsRecommend : BasePanel {
    private DataGrid m_recommendDataGrid;
    private int CREATE_CORPS_LVL;
	public PanelCorpsRecommend()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsRecommend");
        AddPreLoadRes("UI/Corps/PanelCorpsRecommend", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemCorps", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemCorpsMember", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void Init()
    {
        m_recommendDataGrid = m_tran.Find("frame_corps_recommendList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_recommendDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemCorps"), typeof(ItemCorpsInfo));
        m_recommendDataGrid.useLoopItems = true;
        CREATE_CORPS_LVL = ConfigMisc.GetInt("create_corps_reqlv");
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame_corps_recommendList/operation/grid/refreshBtn")).onPointerClick += OnClickCorpsRefresh;
        UGUIClickHandler.Get(m_tran.Find("frame_corps_recommendList/operation/grid/search_btn").gameObject).onPointerClick += OnClkCorpsSearch;
        UGUIClickHandler.Get(m_tran.Find("frame_corps_recommendList/operation/grid/look_up").gameObject).onPointerClick += OnClkCorpsLookup;
        UGUIClickHandler.Get(m_tran.Find("frame_corps_recommendList/operation/grid/my_corps").gameObject).onPointerClick += OnClkCorpsMy;
        UGUIClickHandler.Get(m_tran.Find("frame_corps_recommendList/operation/grid/add_in").gameObject).onPointerClick += OnClkCorpsAdd;
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += ClickClose;
    }

    private void ClickClose(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnClkCorpsAdd(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData.corps_id != 0)
        {
            UIManager.ShowTipPanel("请先退出战队");
            return;
        }
        BaseCorpsData item;
        item = m_recommendDataGrid.SelectedData<BaseCorpsData>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择战队");
            return;
        }
        tos_corps_apply_enter msg = new tos_corps_apply_enter();
        msg.corps_id = item.corps_id;
        NetLayer.Send(msg);
    }

    private void OnClkCorpsMy(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData.corps_id == 0)
        {
            if (PlayerSystem.roleData.level < CREATE_CORPS_LVL)
            {
                TipsManager.Instance.showTips(string.Format("创建战队需要{0}级", CREATE_CORPS_LVL.ToString()));
                return;
            }
            UIManager.PopPanel<PanelCorpsCreate>(null, true, this);
        }
        else
        {
            PanelCorps.search_key = CorpsDataManager.m_corpsData.corps_id.ToString();
            NetLayer.Send(new tos_corps_search_name() { corps_name = PanelCorps.search_key });
        }
    }

    public void UpdateCorspRecommendList()
    {
        m_recommendDataGrid.Data = CorpsDataManager.recommendList.ToArray();
    }

    private void OnClkCorpsLookup(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        BaseCorpsData item;
            item = m_recommendDataGrid.SelectedData<BaseCorpsData>();        
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择战队");
            return;
        }

        tos_corps_view_info msg = new tos_corps_view_info();
        msg.corps_id = item.corps_id;
        NetLayer.Send(msg);
    }


    private void Toc_corps_view_info(toc_corps_view_info proto)
    {
            UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, this);
        
    }

    private void OnClkCorpsSearch(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        UIManager.PopPanel<PanelFindCorps>(null, true, this);
    }

    private void OnClickCorpsRefresh(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        //Logger.Log("tos_corps_recmd_list");
        NetLayer.Send(new tos_corps_recmd_list());
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corps_recmd_list());
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}
