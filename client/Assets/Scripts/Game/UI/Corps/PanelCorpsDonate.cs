﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelCorpsDonate : BasePanel
{
    private Text m_txtCorpsTribute;
    private Text m_txtCorpsCoin;
    private Text m_txtRemainDonateGold;
    private Text m_txtRemainDonateDiamond;
    private DataGrid m_dataGridDonate;
    private List<ConfigCorpsDonateLine> m_lstDonateData;
    private int m_remainDonateCoin;
    private int m_remainDonateDiamond;
    private int m_totalDonateDiamond;
    private int m_corps_coin;
    private int m_corps_tribute;

    public int RemainDonateCoin
    {
        get { return m_remainDonateCoin; }
    }

    public int RemainDonateDiamond
    {
        get { return m_remainDonateDiamond; }
    }

    public PanelCorpsDonate()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsDonate");
        AddPreLoadRes("UI/Corps/PanelCorpsDonate", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemDonate", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtCorpsTribute = m_tran.Find("donate_num/tribute/num").GetComponent<Text>();
        m_txtCorpsCoin = m_tran.Find("donate_num/corps_coin/num").GetComponent<Text>();
        m_txtRemainDonateGold = m_tran.Find("donate_num/gold/num").GetComponent<Text>();
        m_txtRemainDonateDiamond = m_tran.Find("donate_num/diamond/num").GetComponent<Text>();

        m_dataGridDonate = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGridDonate.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemDonate"), typeof(ItemDonate));
        m_dataGridDonate.useLoopItems = true;

        m_lstDonateData = new List<ConfigCorpsDonateLine>();
        var config = ConfigManager.GetConfig<ConfigCorpsDonate>();
        m_lstDonateData.AddRange(config.m_dataArr);

        for (int i = 0; i < m_lstDonateData.Count; ++i)
        {
            m_totalDonateDiamond += m_lstDonateData[i].Diamond;
        }

        m_remainDonateCoin = ConfigMisc.GetInt("corps_donate_max_coin") - CorpsDataManager.m_corpsData.donate_coin;
        m_remainDonateDiamond = m_totalDonateDiamond - CorpsDataManager.m_corpsData.donate_diamond;
        m_totalDonateDiamond = 0;
        m_corps_coin = PlayerSystem.roleData.corps_coin;
        m_corps_tribute = CorpsDataManager.m_corpsData.tribute;
    }

    public override void Update()
    {

    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
    }

    public override void OnShow()
    {
        UpdateDonateNum();
        List<ConfigCorpsDonateLine> list = new List<ConfigCorpsDonateLine>();

        for (int i = 0; i < m_lstDonateData.Count; i++)
        {
            if (m_lstDonateData[i].Coin > 0 || CorpsDataManager.m_corpsData.next_diamond_id == i+1)
            {
                list.Add(m_lstDonateData[i]);
            }
        }
        m_dataGridDonate.Data = list.ToArray();
        //m_dataGridDonate.Data = m_lstDonateData.ToArray();
    }

    public void UpdateDonateNum()
    {
        m_txtCorpsTribute.text = m_corps_tribute.ToString();
        m_txtCorpsCoin.text = m_corps_coin.ToString();
        m_txtRemainDonateGold.text = m_remainDonateCoin.ToString();
        m_txtRemainDonateDiamond.text = m_remainDonateDiamond.ToString();
    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

   

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        DestroyPanel();
    }

    public ToggleGroup GetItemGroup()
    {
        return m_tran.Find("ScrollDataGrid/content").GetComponent<ToggleGroup>();
    }

    private void Toc_corps_donate_tribute(toc_corps_donate_tribute proto)
    {
        TipsManager.Instance.showTips("捐献成功!");
        AudioManager.PlayUISound(AudioConst.levelUp);
        var config = ConfigManager.GetConfig<ConfigCorpsDonate>();
        if (config == null)
            return;

        ConfigCorpsDonateLine donateInfo = config.GetLine(proto.donate_id);
        if (donateInfo == null)
            return;

        m_remainDonateCoin -= donateInfo.Coin;
        m_remainDonateDiamond -= donateInfo.Diamond;
        m_corps_coin += donateInfo.CorpsCoin;
        m_corps_tribute += donateInfo.Tribute;

        UpdateDonateNum();
        OnShow();

        //for (int i = 0; i < m_dataGridDonate.ItemRenders.Count; ++i)
        //{
        //    ItemDonate item = m_dataGridDonate.ItemRenders[i] as ItemDonate;
        //    item.ResetSelect();
        //}
    }

}
