﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelAuther : BasePanel
{
    private List<CORPS_MEMBER_TYPE> m_lstCanAutherType;
    private CORPS_MEMBER_TYPE m_selType;
    private CORPS_MEMBER_TYPE m_orgType;
    public PanelAuther()
    {
        SetPanelPrefabPath("UI/Corps/PanelAuthorize");
        AddPreLoadRes("UI/Corps/PanelAuthorize", EResType.UI);

        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_lstCanAutherType = new List<CORPS_MEMBER_TYPE>();
        m_selType = CORPS_MEMBER_TYPE.TYPE_MAX;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok").gameObject).onPointerClick += OnClkOK;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        //UGUIClickHandler.Get(m_tran.Find("captain").gameObject).onPointerClick += OnSelCaptain;
        UGUIClickHandler.Get(m_tran.Find("vice_captain").gameObject).onPointerClick += OnSelPos;
        UGUIClickHandler.Get(m_tran.Find("commander").gameObject).onPointerClick += OnSelPos;
        UGUIClickHandler.Get(m_tran.Find("examer").gameObject).onPointerClick += OnSelPos;
        UGUIClickHandler.Get(m_tran.Find("diplomat").gameObject).onPointerClick += OnSelPos;
        UGUIClickHandler.Get(m_tran.Find("secretary").gameObject).onPointerClick += OnSelPos;
        UGUIClickHandler.Get(m_tran.Find("member").gameObject).onPointerClick += OnSelPos;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        m_orgType = (CORPS_MEMBER_TYPE)m_params[1];
        /*
        if (m_params.Length < 2 )
            return;

        m_lstCanAutherType = m_params[0] as List<PanelCorps.MEMBER_TYPE>;
        for( PanelCorps.MEMBER_TYPE mbr_type = PanelCorps.MEMBER_TYPE.TYPE_VICELEADER; mbr_type < PanelCorps.MEMBER_TYPE.TYPE_MEMBER; mbr_type++ )
        {
            int iType = (int)mbr_type;
            string goName = iType.ToString();
            if( m_lstCanAutherType.IndexOf( mbr_type ) > -1 )
            {
                UGUIClickHandler.Get(m_tran.FindChild(goName).gameObject).onPointerClick += OnClkAuther;
                string goBack = goName + "/back";
                m_tran.FindChild(goBack).gameObject.TrySetActive(false);
                string goBack2 = goName + "/back2";
                m_tran.FindChild(goBack2).gameObject.TrySetActive(true);
            }
            else
            {
                m_tran.FindChild(goName).GetComponent<Toggle>().enabled = false;
            }
        }
         */
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }


    protected void OnSelPos( GameObject target, PointerEventData eventData )
    {
        if( target.name == "vice_captain" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_VICELEADER;
        }
        else if( target.name == "commander" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_COMMANDER;
        }
        else if( target.name == "examer" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_EXAMINER;
        }
        else if( target.name == "diplomat" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_DIPLOMAT;
        }
        else if( target.name == "secretary" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_SECRETARY;
        }
        else if( target.name == "member" )
        {
            m_selType = CORPS_MEMBER_TYPE.TYPE_MEMBER;
        }
    }

    /*
    protected void OnSelViceCaptain( GameObject target, PointerEventData eventData )
    {
        m_selType = CORPS_MEMBER_TYPE.TYPE_VICELEADER;
    }

    protected void OnSelMember( GameObject target, PointerEventData eventData )
    {
        m_selType = CORPS_MEMBER_TYPE.TYPE_MEMBER;
    }
    protected void OnClkAuther(GameObject target, PointerEventData eventData)
    {
        int iType = int.Parse(target.name);
        m_selType = (CORPS_MEMBER_TYPE)iType;
    }
    */

    protected void OnClkOK(GameObject target, PointerEventData eventData)
    {
        if( m_selType == CORPS_MEMBER_TYPE.TYPE_MAX )
        {
            TipsManager.Instance.showTips("请选择职位!");
            return;
        }
        if( m_selType == m_orgType )
        {
            TipsManager.Instance.showTips(string.Format("该成员已经是{0}",PanelCorps.m_strPosition[(int)m_orgType -1]) );
            return;
        }

        if( !CorpsDataManager.HasPosition( m_selType ) )
        {
            TipsManager.Instance.showTips(string.Format("{0}职位无空缺!", PanelCorps.m_strPosition[(int)m_selType - 1]));
            return;
        }
        tos_corps_change_member_type msg = new tos_corps_change_member_type();
        msg.member_type = (int)m_selType;
        msg.member_id = (long)m_params[0];
        NetLayer.Send(msg);
      
        HidePanel();
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        DestroyPanel();
    }
}
