﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PanelCorpsRoomRank:BasePanel
{
    private DataGrid _rankDataGrid;
    private Text _myRankTxt;
    private Text _myDmgTxt;
    private List<ItemRankData> _rankDataList;
    private List<ItemRankData> _rankDataPool; 

    private static string RANK_NAME = "defend_damage";

    public PanelCorpsRoomRank()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsRoomRank");
        AddPreLoadRes("UI/Corps/PanelCorpsRoomRank", EResType.UI);
        AddPreLoadRes("UI/RankList/ItemRankListInfo", EResType.UI);
    }

    public override void Init()
    {
        _rankDataList = new List<ItemRankData>();
        _rankDataPool = new List<ItemRankData>();

        _myRankTxt = m_tran.Find("myinfo/myRank").GetComponent<Text>();
        _myDmgTxt = m_tran.Find("myinfo/myDmg").GetComponent<Text>();
        _rankDataGrid = m_tran.Find("MiddleList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        _rankDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/RankList/ItemRankListInfo"), typeof(ItemRankListInfo));
        _rankDataGrid.useLoopItems = true;
    }

    

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel();};
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corps_get_hurt_list());
    }

    private void Toc_corps_get_hurt_list(toc_corps_get_hurt_list proto)
    {
        ItemRankData selfData = null;
        _rankDataList.Clear();
        for (int i = 0; i < proto.list.Length; ++i)
        {
            ItemRankData data = GetPoolItem(i);
            var playerId = proto.list[i].player_id;

            data.id = playerId;
            data.name = CorpsDataManager.GetMemberById(playerId).name;
            data.corps_name = CorpsDataManager.GetCorpsName();
            data.val = proto.list[i].hurt;
            data.queue = i + 1;
            _rankDataList.Add(data);
            if (playerId == PlayerSystem.roleId && selfData == null)
            {
                selfData = data;
            }
        }
        if (selfData != null)
        {
            _myRankTxt.text = "我的排名:" + selfData.queue;
            _myDmgTxt.text = "我的伤害:" + selfData.val;
        }
        else
        {
            _myRankTxt.text = "我的排名:未上榜";
            _myDmgTxt.text = "我的伤害:0";
        }
       
        _rankDataGrid.Data = _rankDataList.ToArray();

        var items = _rankDataGrid.ItemRenders;
        for (int i = 0; i < items.Count; i++)
        {
            UGUIClickHandler.Get(items[i].transform).RemoveAllHandler();
        }

    }

    public override void OnHide()
    {
        _rankDataList.Clear();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private ItemRankData GetPoolItem(int index)
    {
        if (index >= _rankDataPool.Count)
        {
            var item = new ItemRankData();
            _rankDataPool.Add(item);
            return item;
        }
        return _rankDataPool[index];
    }

}