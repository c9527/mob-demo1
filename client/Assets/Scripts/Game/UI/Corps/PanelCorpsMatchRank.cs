﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PanelCorpsMatchRank : BasePanel
{
    public class ItemCorpsMatchRank : ItemRender
    {
        Text rank;
        Text team;
        //Text leader;
        Text score;
        Image rankIcon1;
        Image rankIcon2;


        public override void Awake()
        {
            rank = transform.Find("Rank").GetComponent<Text>();
            team = transform.Find("name").GetComponent<Text>();
            //leader = transform.Find("leader").GetComponent<Text>();
            score = transform.Find("score").GetComponent<Text>();
            rankIcon1 = transform.Find("rankIcon1").GetComponent<Image>();
            rankIcon2 = transform.Find("rankIcon2").GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            var info = data as corpsMatchData;
            rank.text = "";
            rankIcon1.gameObject.TrySetActive(true);
            rankIcon2.gameObject.TrySetActive(true);

            if (info.rank > 99)
            {
                rank.text = info.rank.ToString();
                rankIcon2.gameObject.TrySetActive(false);
                rankIcon1.gameObject.TrySetActive(false);
            }
            else if (info.rank > 9)
            {
                rankIcon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (info.rank / 10)));
                rankIcon1.SetNativeSize();
                rankIcon1.gameObject.TrySetActive(true);
                rankIcon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "rank_" + (info.rank % 10)));
                rankIcon2.SetNativeSize();
                rankIcon2.gameObject.TrySetActive(true);
                rankIcon1.rectTransform.anchoredPosition = new Vector2(-274,0);
            }
            else
            {
                rankIcon2.gameObject.TrySetActive(false);
                rankIcon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, info.rank < 4 ? "rank" + info.rank : "rank_" + info.rank));
                rankIcon1.SetNativeSize();
                rankIcon1.gameObject.TrySetActive(true);
                rankIcon1.rectTransform.anchoredPosition = new Vector2(-256, 0);
            }
            team.text = info.name;
            score.text = info.score.ToString();
            //leader.text = info.name;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        }
    }

    public class corpsMatchData
    {
        public int rank;
        public int score;
        public string name;
        public string corpsname;
    }

    public PanelCorpsMatchRank()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsMatchRank");
        AddPreLoadRes("UI/Corps/PanelCorpsMatchRank", EResType.UI);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    DataGrid m_list;
    Text m_page;
    int page;
    int maxPage;
    private const int MAX_NUM = 5;
    private string m_typeName = "corpsmatch";
    private Text m_title1;
    private Text m_title2;
    Text m_myRankText;
    Text m_scoreText;
    Text m_titleName;
    public override void Init()
    {
        m_titleName = transform.Find("bg/title/title").GetComponent<Text>();
        m_scoreText = transform.Find("scoreText").GetComponent<Text>();
        m_myRankText = transform.Find("RankText").GetComponent<Text>();
        m_title1 = m_tran.Find("Title/Text1").GetComponent<Text>();
        m_title2 = m_tran.Find("Title/Text2").GetComponent<Text>();
        m_list = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_list.SetItemRender(m_tran.Find("ScrollDataGrid/content/ItemCorpsRank").gameObject, typeof(ItemCorpsMatchRank));
        m_page = m_tran.Find("page/page").GetComponent<Text>();
        page = 0;
        maxPage = 1;

    }


    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("page/left")).onPointerClick += delegate { SendMsg(false); };
        UGUIClickHandler.Get(m_tran.Find("page/right")).onPointerClick += delegate { SendMsg(true); };
        UGUIClickHandler.Get(m_tran.Find("Close")).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {

    }

    public void SendMsg(bool add)
    {
        if (add)
        {
            if (page >= maxPage)
            {
                return;
            }
            else
            {
                page++;
            }
        }
        else
        {
            if (page <= 1)
            {
                return;
            }
            else
            {
                page--;
            }
        }
        NetLayer.Send(new tos_player_rank_list() { name = m_typeName, page = page });

    }


    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        if (HasParams())
        {
            var str = m_params[0] as string;
            if (!string.IsNullOrEmpty(str))
            {
                m_typeName = str;
            }
        }
        NetLayer.Send(new tos_player_rank_list() { name = m_typeName, page = 1 });
        NetLayer.Send(new tos_player_my_ranks());//我的排名
        m_page.text = "1/1";
        page = maxPage = 1;
        showTitle();

    }

    private void showTitle()
    {
        if (m_typeName == "corpsmatch")//战队积分
        {
            m_title1.text = "战队";
            m_title2.text = "积分";
            m_titleName.text = "战队积分榜";
        }
        else
        {
            m_title1.text = "名字";
            m_title2.text = "胜场";
            m_titleName.text = "我的胜场榜";
        }
    }

    public override void Update()
    {
    }

    void Toc_player_rank_list(toc_player_rank_list data)
    {
        corpsMatchData[] items = new corpsMatchData[data.list.Length];
        for (int i = 0; i < data.list.Length; i++)
        {
            items[i] = new corpsMatchData();
            if (data.name == "corpsmatch")
            {
                items[i].name = data.list[i].corps_name;
            }
            else
            {
                items[i].name = data.list[i].name;
            }
            items[i].rank = data.start + i;
            items[i].score = data.list[i].val;
            //items[i].corpsname = data.list[i].corps_name;
        }
        m_list.Data = items;

        if (data.size != 0)
        {
            maxPage = data.size % MAX_NUM == 0 ? data.size / MAX_NUM : (data.size / MAX_NUM + 1);
            maxPage = maxPage == 0 ? 1 : maxPage;
        }
        m_page.text = data.page + "/" + maxPage;
    }


    /// <summary>
    /// 我的排名、战队排名
    /// </summary>
    /// <param name="data"></param>
    void Toc_player_my_ranks(toc_player_my_ranks data)
    {
        for (int i = 0; i < data.ranks.Length; i++)
        {
            if (m_typeName == "corpsmatch")
            {
                if (data.ranks[i].name == "corpsmatch")
                {
                    ItemRankData rankData = new ItemRankData();
                    rankData.queue = data.ranks[i].rank;
                    rankData.val = data.ranks[i].val;
                    rankData.ext = data.ranks[i].ext;

                    if (data.ranks[i].rank == 0)
                    {
                        m_myRankText.text = "未上榜";
                    }
                    else
                    {
                        m_myRankText.text = "第 " + data.ranks[i].rank.ToString() + " 名";
                    }
                    m_scoreText.text = "本队积分：" + data.ranks[i].val;
                    break;
                }
            }

            if (m_typeName == "corpsmatch_win_rounds")
            {
                if (data.ranks[i].name == "corpsmatch_win_rounds")
                {
                    ItemRankData rankData = new ItemRankData();
                    rankData.queue = data.ranks[i].rank;
                    rankData.val = data.ranks[i].val;
                    rankData.ext = data.ranks[i].ext;
                    if (data.ranks[i].rank == 0)
                    {
                        m_myRankText.text = "未上榜";
                    }
                    else
                    {
                        m_myRankText.text = "第 " + data.ranks[i].rank.ToString() + "名";
                    }
                    m_scoreText.text = "我的胜场：" + data.ranks[i].val;
                    break;
                }
            }
        }
    }
}
