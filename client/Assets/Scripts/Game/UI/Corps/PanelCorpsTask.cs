﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelCorpsTask : BasePanel
{
    private toc_corps_mission_data _data;

    private int _refresh_timestamp;
    private float _last_time;

    public Image item_img;
    public Text name_txt;
    public Text time_txt;
    public DataGrid task_list;
    public RectTransform active_pb;
    private Queue<UIEffect> _checkpoint_effects = new Queue<UIEffect>();

    public PanelCorpsTask()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsTask");

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("UI/Corps/PanelCorpsTask", EResType.UI);
    }

    public override void Init()
    {
        item_img = m_tran.Find("frame/item_img").GetComponent<Image>();
        name_txt = m_tran.Find("frame/name_txt").GetComponent<Text>();
        time_txt = m_tran.Find("frame/time_txt").GetComponent<Text>();
        task_list = m_tran.Find("frame/task_list/content").gameObject.AddComponent<DataGrid>();
        task_list.autoSelectFirst = false;
        task_list.SetItemRender(m_tran.Find("frame/task_list/Render").gameObject, typeof(CorpsTaskRender));
        active_pb = m_tran.Find("frame/active_pb") as RectTransform;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target, evt) => HidePanel();
    }

    public override void OnShow()
    {
        _data = CorpsModel.Instance.task_data;
        if (_data == null)
        {
            CorpsModel.Instance.TosGetTaskData();
            return;
        }
        OnUpdateTaskData();
    }

    private void OnUpdateTaskData(toc_corps_mission_data data=null)
    {
        var now_date = TimeUtil.GetNowTime();
        _refresh_timestamp = TimeUtil.GetTimeStamp(new DateTime(now_date.Year, now_date.Month, now_date.Day).AddMinutes(24 * 60));
        _last_time = 0;
        if (data != null)
        {
            _data = data;
        }
        if (_data.last_star.icon > 0)
        {
            item_img.gameObject.TrySetActive(true);
            item_img.SetSprite(ResourceManager.LoadRoleIconBig(_data.last_star.icon));
            item_img.SetNativeSize();
        }
        else
        {
            item_img.gameObject.TrySetActive(true);
            item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, "no"));
            item_img.SetNativeSize();
        }
        
        name_txt.text = _data.last_star.name;
        task_list.Data = _data.mission_list;
        OnUpdateActiveness();
    }

    private void OnUpdateActiveness()
    {
        if (_data == null)
        {
            return;
        }
        //_data.activiness = 30;
        var size = active_pb.sizeDelta;
        var start_pos = new Vector3(0 - size.x * active_pb.pivot.x, 0);
        //进度条文字
        active_pb.Find("value_txt").gameObject.TrySetActive(false);
//        var pb_value_txt = active_pb.Find("value_txt");
//        start_pos.y = pb_value_txt.localPosition.y;
//        pb_value_txt.localPosition = start_pos + new Vector3(size.x * _data.activeness / 100f, 0);
//        pb_value_txt.Find("Text").GetComponent<Text>().text = _data.activeness.ToString();
        //进度条
        var pb_value = active_pb.Find("value").GetComponent<Image>();
        pb_value.fillAmount = Mathf.Min(_data.activeness / 100f, 1f);
        //checkpoint
        var checkpoint_list = active_pb.Find("checkpoints");
        start_pos.y = 0;
        var source_data = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().m_dataArr;
        
        Transform item = null;
        while (_checkpoint_effects.Count > 0)
        {
            _checkpoint_effects.Dequeue().Destroy();
        }
        for (var i = 0; i < source_data.Length; i++)
        {
            var vo = source_data[i];
            if (i < checkpoint_list.childCount)
            {
                item = checkpoint_list.GetChild(i);
            }
            else
            {
                item = (UIHelper.Instantiate(active_pb.Find("Render").gameObject) as GameObject).transform;
                UGUIClickHandler.Get(item).onPointerClick += OnCheckPointClick;
                item.SetParent(checkpoint_list);
                item.localScale = Vector3.one;
            }
            item.gameObject.TrySetActive(true);
            item.Find("name").GetComponent<Text>().text = vo.Name;
            item.name = "checkpoint_" + (int)vo.ID;
            item.localPosition = start_pos + new Vector3(size.x * vo.Activeness / 100f, 0);
            item.Find("Text").GetComponent<Text>().text = (int)vo.Activeness + "活跃度";
            var img = item.Find("item_img/Image").GetComponent<Image>();
            var titem = ItemDataManager.GetItem(ConfigManager.GetConfig<ConfigReward>().GetLine(vo.Reward).ItemList[0].ID);
            img.SetSprite(ResourceManager.LoadIcon(titem.Icon),titem.SubType);
            img.SetNativeSize();
            if (titem.Type == GameConst.ITEM_TYPE_EQUIP)
            {
                img.transform.localScale = Vector3.one * 0.3f;
                img.transform.eulerAngles = titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2 ? new Vector3(0, 0, -10) : Vector3.zero;
            }
            else
            {
                img.transform.localScale = Vector3.one / 2;
                img.transform.eulerAngles = Vector3.zero;
            }
            var status_img = item.Find("status_img");
            status_img.gameObject.TrySetActive(false);
            if (_data.activeness >= vo.Activeness)
            {
                item.Find("Checkmark").gameObject.TrySetActive(true);
                var has_reward = true;
                for (int j = 0; _data.rewarded_list != null && j < _data.rewarded_list.Length; j++)
                {
                    if (_data.rewarded_list[j] == vo.ID)
                    {
                        has_reward = false;
                        break;
                    }
                }
                if (has_reward)
                {
                    _checkpoint_effects.Enqueue(UIEffect.ShowEffect(item.Find("item_img"), EffectConst.UI_CORPS_TASK_ACTIVENESS_REWARD));
                }
                else
                {
                    status_img.gameObject.TrySetActive(true);
                }
            }
            else
            {
                item.Find("Checkmark").gameObject.TrySetActive(false);
            }
            MiniTipsManager.get(item.gameObject).miniTipsDataFunc = (go) =>
            {
                return new MiniTipsData() { name = titem.DispName, dec = titem.Desc };
            };
        }
        var res_count = checkpoint_list.childCount - source_data.Length;
        while (res_count-- > 0)
        {
            checkpoint_list.GetChild(source_data.Length + res_count).gameObject.TrySetActive(false);
        }
    }

    private void OnCheckPointClick(GameObject target, PointerEventData eventData)
    {
        if (_data == null)
        {
            return;
        }
        var info = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().GetLine(int.Parse(target.name.Substring(target.name.LastIndexOf("_")+1)));
        if (info != null && _data.activeness >= info.Activeness)
        {
            var has_reward = true;
            foreach (var code in _data.rewarded_list)
            {
                if (code == info.ID)
                {
                    has_reward = false;
                    break;
                }
            }
            //TipsManager.Instance.showTips("点击领奖: "+info.ID);
            if (has_reward)
            {
                CorpsModel.Instance.TosGetActivenessReward(info.ID);
                //TipsManager.Instance.showTips("发送领奖请求: "+info.ID);
            }
        }
    }

    public override void OnHide()
    {
        _data = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorps>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_data == null || _last_time == -1 || Time.realtimeSinceStartup < (_last_time + 1))
        {
            return;
        }
        _last_time = Time.realtimeSinceStartup;
        var cd = _refresh_timestamp - TimeUtil.GetNowTimeStamp();
        if (cd <= 0 && cd > -10)
        {
            _last_time = -1;
            CorpsModel.Instance.TosGetTaskData();
        }
        time_txt.text = "任务刷新倒计时：" + TimeUtil.FormatTime(cd);  
    }

    //===========================================s to c
    private void Toc_corps_mission_data(toc_corps_mission_data data)
    {
        OnUpdateTaskData(data);
    }

    private void Toc_corps_notify_mission(toc_corps_notify_mission data)
    {
        if (data.mission.status == 2)
        {
            CorpsModel.Instance.TosGetTaskData();
        }
        else if (CorpsModel.Instance.task_data!=null)
        {
            _data = CorpsModel.Instance.task_data;
            task_list.Data = _data.mission_list;
            OnUpdateActiveness();
        }
    }

    private void Toc_corps_get_activeness_reward(toc_corps_get_activeness_reward data)
    {
        if (CorpsModel.Instance.task_data == null)
        {
            return;
        }
        var cfg = ConfigManager.GetConfig<ConfigCorpsActivenessReward>().GetLine(data.reward_id);
        if (cfg != null)
        {
            var cfgReward = ConfigManager.GetConfig<ConfigReward>().GetLine(cfg.Reward);
            PanelRewardItemTipsWithEffect.DoShow(cfgReward.ItemList);
        }
        _data = CorpsModel.Instance.task_data;
        OnUpdateActiveness();
        TipsManager.Instance.showTips("领取成功");       
    }
}

class CorpsTaskRender : ItemRender
{
    private p_corps_mission _data;

    public Image item_img;
    public Image type_img;
    public Text desc_txt;
    public Text reward_txt;
    public Text info_txt;
    public Image status_img;
    public Text status_txt;
    public Button fun_btn;

    public override void Awake()
    {
        item_img = transform.Find("item_img").GetComponent<Image>();
        type_img = transform.Find("type_img").GetComponent<Image>();
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
        reward_txt = transform.Find("reward_txt").GetComponent<Text>();
        info_txt = transform.Find("info_txt").GetComponent<Text>();
        status_img = transform.Find("status_img").GetComponent<Image>();
        status_txt = transform.Find("status_txt").GetComponent<Text>();
        fun_btn = transform.Find("fun_btn").GetComponent<Button>();

        UGUIClickHandler.Get(fun_btn.gameObject).onPointerClick += OnFunBtnClick;
    }

    private void OnFunBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data == null)
        {
            UIManager.ShowTipPanel("数据错误");
            return;
        }
        var info = ConfigManager.GetConfig<ConfigCorpsMission>().GetLine(_data.id);
        if (info != null)
        {            
            UIManager.GotoPanel(info.GoSystem);                        
        }
            
    }

    protected override void OnSetData(object data)
    {
        _data = data as p_corps_mission;
        if (_data == null)
            return;

        var info = ConfigManager.GetConfig<ConfigCorpsMission>().GetLine(_data.id);
        fun_btn.gameObject.TrySetActive(!string.IsNullOrEmpty(info.GoSystem));
        //ConfigItemLine cfg = ItemDataManager.GetItem(info.ID);
        //item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),cfg!=null?cfg.SubType:"");
        item_img.SetSprite(ResourceManager.LoadIcon(info.Icon));
        item_img.SetNativeSize();
        type_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, _data.type == 2 ? "multiple_ico" : "single_ico"));
        type_img.SetNativeSize();
        desc_txt.text = info.Desc;
        reward_txt.text = (info.CorpsCoin > 0 ? " 战队币+" + info.CorpsCoin : "") + (info.Activeness>0 ? " 活跃度+"+info.Activeness : "");
        int progress = _data.type == 1 && _data.status != 2 ? _data.partake : _data.progress;   // 单人任务在未完成时，进度显示为玩家的参与度
        
        if (_data.status == 2)
        {
            info_txt.text = _data.type == 1 ? _data.finisher : "";
            status_img.gameObject.TrySetActive(true);
            fun_btn.gameObject.TrySetActive(false);
        }
        else
        {
            info_txt.text = "总进度：" + progress + "/" + info.Count;
            status_img.gameObject.TrySetActive(false);
            fun_btn.gameObject.TrySetActive(true);
            fun_btn.transform.Find("Text").GetComponent<Text>().text = string.Format(_data.type == 2 ? "前往（{0}/{1}）" : "前往", _data.partake, info.Limits);
        }
    }
}
