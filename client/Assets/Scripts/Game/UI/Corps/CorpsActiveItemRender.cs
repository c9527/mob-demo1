﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class CorpsActiveItemRender : ItemRender
{
    private CDCorpsActive _data;

    private int _time_limit;

    public Image item_img;
    public Image name_txt;
    public Image title_txt;
    public Image status_img;
    public Text desc_txt;
    public Text fun_txt;
    public Image bubble_img;
    public Button fun_btn;
    public GameObject notAvailableImage;

    public override void Awake()
    {
        item_img = transform.Find("item_img").GetComponent<Image>();
        status_img = transform.Find("status_img").GetComponent<Image>();
        name_txt = transform.Find("name_txt").GetComponent<Image>();
        title_txt = transform.Find("title_txt").GetComponent<Image>();
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
        fun_txt = transform.Find("fun_txt").GetComponent<Text>();
        bubble_img = transform.Find("Bubble").GetComponent<Image>();
        bubble_img.gameObject.TrySetActive(false);
        fun_btn = transform.Find("fun_btn").GetComponent<Button>();
        notAvailableImage = transform.Find("not_available").gameObject;

        UGUIClickHandler.Get(fun_btn.gameObject).onPointerClick += OnFunBtnClick;
    }

    public CDCorpsActive data
    {
        get { return _data; }
    }

    public void RefreshView()
    {
        if (_data == null || _data.Status==-1)
        {
            return;
        }
        if (_data.Uid == ECorpsActiveUID.COOPERATE_TASK)
        {
            OnUpdateTaskData();
        }
        else if (_data.Uid == ECorpsActiveUID.GUARD_TASK)
        {
            OnUpdateGuardData();
        }
        else if (_data.Uid == ECorpsActiveUID.LOTTERY)
        {
            OnUpdateLotteryData();
        }
        else if (_data.Uid == ECorpsActiveUID.SHOP)
        {
            OnUpdateShopData();
        }
    }

    private void OnFunBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data.Status == -1)
        {
            return;
        }
        if (_data.Uid == ECorpsActiveUID.GUARD_TASK)
        {
            PanelRoomBase.RoomMinimizeCheck(() =>
                {
                    CorpsModel.Instance.TosEnterGuardTask();
                });            
        }
        else if (_data.Uid == ECorpsActiveUID.COOPERATE_TASK)
        {
            UIManager.ShowPanel<PanelCorpsTask>();
        }
        else if (_data.Uid == ECorpsActiveUID.LOTTERY)
        {
            UIManager.ShowPanel<PanelCorpsLottery>();
        }
        else if (_data.Uid == ECorpsActiveUID.SHOP)
        {
            UIManager.ShowPanel<PanelCorpsShop>();
        }
        else if (_data.Uid == ECorpsActiveUID.SALARY)
        {
            UIManager.PopPanel<PanelCorpsSalary>(null, true, UIManager.GetPanel<PanelCorps>());
        }
        else
        {
            UIManager.ShowTipPanel("未设定功能！");
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDCorpsActive;
        if (_data == null)
        {
            return;
        }
        item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, _data.Icon));
        item_img.SetNativeSize();
        if (_data.style == 1)
        {
//            item_img.transform.localPosition = new Vector3(-213, 0, 0);
            name_txt.enabled = false;
            title_txt.enabled = false;
        }
        else
        {
//            item_img.transform.localPosition = new Vector3(-265, 0, 0);
            name_txt.enabled = true;
            title_txt.enabled = true;
            name_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, _data.Icon + "_title"));
            name_txt.SetNativeSize();
            title_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, _data.Icon + "_txt"));
            title_txt.SetNativeSize();
        }
        desc_txt.text = _data.Desc;
        status_img.gameObject.TrySetActive(false);
        if (_data.Status == -1)
        {
            fun_btn.gameObject.TrySetActive(false);
            notAvailableImage.TrySetActive(true);
        }
        else
        {
            fun_btn.gameObject.TrySetActive(true);
            notAvailableImage.TrySetActive(false);
        }
        fun_txt.text = "";
        OnDisable();
        RefreshView();
    }

 
    private void WakeupTimer()
    {
        if (_data == null)
        {
            return;
        }
        if (_data.Uid == ECorpsActiveUID.GUARD_TASK)
        {
            var status = CorpsDataManager.GetGuardStatus();
            if (status == 1)
            {
                fun_txt.text = "开启倒计时: " + TimeUtil.FormatTime(CorpsDataManager.GedtGuardCD(1));
            }
            else if(status==2 || status==3)
            {
                OnUpdateGuardData();
            }
        }
    }

    private void OnUpdateGuardData()
    {
        CancelInvoke("WakeupTimer");
        if (!isActiveAndEnabled)
        {
            return;
        }
        var status = CorpsDataManager.GetGuardStatus();
        bubble_img.gameObject.TrySetActive(status == 2);
        status_img.gameObject.TrySetActive(false);
        if(status==2)
        {
//            status_img.gameObject.TrySetActive(true);
            fun_btn.gameObject.TrySetActive(true);
            notAvailableImage.TrySetActive(false);
            Util.SetNormalShader(fun_btn);
            var cd = CorpsDataManager.GedtGuardCD();
            if (cd > 0)
            {
                Invoke("WakeupTimer",cd+1.5f);
            }
        }
        else if (status == 3)
        {
            fun_btn.gameObject.TrySetActive(false);
            notAvailableImage.TrySetActive(true);
        }
        else
        {
            fun_btn.gameObject.TrySetActive(false);
            notAvailableImage.TrySetActive(true);

            var cd = CorpsDataManager.GedtGuardCD(1);
            if (cd > 0)
            {
                InvokeRepeating("WakeupTimer", 0, 1);
            }
        }
    }

    private void OnUpdateTaskData()
    {
        if (!isActiveAndEnabled)
        {
            return;
        }
        bubble_img.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.CorpsTaskReward));
        var task_data = CorpsModel.Instance.task_data;
        fun_txt.text = "战队之星：" + (task_data!=null && task_data.last_star != null ? task_data.last_star.name : "");
    }

    private void OnUpdateShopData()
    {
        var shop_data = CorpsModel.Instance.shop_data;
        _time_limit = shop_data != null ? shop_data.next_flush_timestamp : 0;
        WakeupShopTimer();
    }

    private void OnUpdateLotteryData()
    {
        var lottery_data = CorpsModel.Instance.lottery_data;
        if (lottery_data != null)
        {
            fun_txt.text = string.Format("剩余次数：{0}", lottery_data.total_counts - lottery_data.cur_counts);
        }
        else
        {
            fun_txt.text = "";
        }
    }

    private void WakeupShopTimer()
    {
        if (!isActiveAndEnabled)
        {
            CancelInvoke();
            return;
        }
        var cd = _data != null ? _time_limit - TimeUtil.GetNowTimeStamp() : 0;
        if (IsInvoking() && cd <= 0)
        {
            CancelInvoke("WakeupShopTimer");
            TimerManager.SetTimeOut(3, CorpsModel.Instance.TosGetShopData);
        }
        else if (!IsInvoking() && cd > 0)
        {
            InvokeRepeating("WakeupShopTimer", 1, 1f);
        }
        fun_txt.text = string.Format("刷新倒计时：{0}", TimeUtil.FormatTime(Mathf.Max(cd, 0)));
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
}

public class CDCorpsActive
{
    public ECorpsActiveUID Uid;
    public string Icon;
    public string Desc;
    public int Progress;
    public int Total;
    public int Status;
    public int style;
}

public enum ECorpsActiveUID
{
    LOTTERY,
    SHOP,
    GUARD_TASK,
    COOPERATE_TASK,
    SALARY,
}
