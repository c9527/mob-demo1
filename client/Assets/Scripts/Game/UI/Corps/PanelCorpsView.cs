﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
public class PanelCorpsView : BasePanel
{
    private Text m_txtCorpsName;
    //private Text m_txtCreateDate;
    //private Text m_txtCaptain;
   // private Text m_txtViceCaptainLeft;
   // private Text m_txtViceCaptainRight;
    private Image m_imgFlag;
    private Image m_imgBiaozhi;
    private Text m_txtLvl;
    private Text m_txtNotice;
//    private Text m_txtLeaderName;
//    private Text m_txtViceLeaderNameLeft;
//    private Text m_txtViceLeaderNameRight;
//    private Text m_txtCommanderName;
//    private Text m_txtExamerName;
//    private Text m_txtDiplomatName;
//    private Text m_txtSecretaryName;
    private Text m_txtCorpsId;
    private Text m_txtCapacity;
    private Slider m_sliderFight;
    private Text m_txtFight;
    private Text m_txtConstruct;
    private DataGrid m_memberListDataGrid;

    private toc_corps_view_info m_data;
    public PanelCorpsView()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsView");
        AddPreLoadRes("UI/Corps/PanelCorpsView", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        
        m_txtCorpsName = m_tran.Find("icon_kuang/name").GetComponent<Text>();
        //m_txtFighting = m_tran.Find("corps_power/power_num").GetComponent<Text>();
        //m_txtCorpsId = m_tran.Find("corps_id/value").GetComponent<Text>();
        //m_txtCreateDate = m_tran.Find("create_date/value").GetComponent<Text>();
        //m_txtCapacity = m_tran.Find("corps_capacity/value").GetComponent<Text>();
        //m_txtCaptain = m_tran.Find("official/captain/name").GetComponent<Text>();
        //m_txtViceCaptainLeft = m_tran.Find("official/vice_captain_left/name").GetComponent<Text>();
        //m_txtViceCaptainRight = m_tran.Find("official/vice_captain_right/name").GetComponent<Text>();
        m_imgFlag  = m_tran.Find("icon_kuang/flag").GetComponent<Image>();
        m_imgBiaozhi = m_tran.Find("icon_kuang/biaozhi").GetComponent<Image>();
        m_txtLvl = m_tran.Find("lvl").GetComponent<Text>();
        m_txtNotice = m_tran.Find("notice/content").GetComponent<Text>();
//        m_txtLeaderName = m_tran.Find("official/captain/name").GetComponent<Text>();
//        m_txtViceLeaderNameLeft = m_tran.Find("official/vice_captain_left/name").GetComponent<Text>();
//        m_txtViceLeaderNameRight = m_tran.Find("official/vice_captain_right/name").GetComponent<Text>();
//        m_txtCommanderName = m_tran.Find("official/commander/name").GetComponent<Text>();
//        m_txtExamerName = m_tran.Find("official/examer/name").GetComponent<Text>();
//        m_txtDiplomatName = m_tran.Find("official/diplomat/name").GetComponent<Text>();
//        m_txtSecretaryName = m_tran.Find("official/secretary/name").GetComponent<Text>();
        m_txtCorpsId = m_tran.Find("corps_id/value").GetComponent<Text>();
        m_txtCapacity = m_tran.Find("corps_capacity/value").GetComponent<Text>();
        m_sliderFight = m_tran.Find("contribute/slider").GetComponent<Slider>();
        m_txtFight = m_tran.Find("contribute/content").GetComponent<Text>();
        m_txtConstruct = m_tran.Find("construct/content").GetComponent<Text>();

        m_tran.Find("ScrollRect/content").gameObject.AddComponent<DataGrid>();
        m_memberListDataGrid = m_tran.Find("ScrollRect/content").GetComponent<DataGrid>();
        m_memberListDataGrid.SetItemRender(m_tran.Find("ScrollRect/line").gameObject, typeof(ItemViewMemberInfo));
        m_memberListDataGrid.useLoopItems = true;

    }

    public override void Update()
    {
    }

     public override void InitEvent()
     {
         UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClose;
     }

     public override void OnShow()
     {
        if( m_params.Length <= 0 )
         return;

        m_data = m_params[0] as toc_corps_view_info;
        InitView();
     }

    protected void InitView()
    {
        var config = ConfigManager.GetConfig<ConfigCorpsLevel>();
        ConfigCorpsLevelLine upLine = config.GetLine(m_data.level + 1);
        if (upLine == null)
        {
            upLine = config.GetLine(m_data.level);
        }

        m_tran.FindChild("contribute/content").GetComponent<Text>().text = m_data.fighting.ToString() + "/" + upLine.Activeness.ToString();
        m_tran.FindChild("contribute/slider").GetComponent<Slider>().value = (float)m_data.fighting / upLine.Activeness;
        m_tran.FindChild("construct/content").GetComponent<Text>().text = m_data.tribute.ToString();
        m_txtCorpsName.text = m_data.corps_name;
        string flag = "f" + m_data.logo_pannel.ToString() + "_" + "c" + m_data.logo_color.ToString();
        m_imgFlag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        string biaozhi = "biaozhi_" + m_data.logo_icon.ToString();
        m_imgBiaozhi.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        m_txtLvl.text = String.Format("等级:{0}", m_data.level);
        m_txtCorpsId.text = m_data.corps_id.ToString();
        m_txtCapacity.text = m_data.member_cnt.ToString() + "/" + m_data.max_member.ToString();
        m_txtNotice.text = m_data.notice;

        m_memberListDataGrid.Data = m_data.managers;
        
     }

     public override void OnHide()
     {

     }

     public override void OnBack()
     {
     }

     public override void OnDestroy()
     { }

     protected void OnClose(GameObject target, PointerEventData eventData)
     {
         HidePanel();
     }
}


public class ItemViewMemberInfo : ItemRender
{
    private toc_corps_view_info.manager m_data;
    private Text m_txtName;
    private Text m_txtPosition;
    private Image m_sexImg;
    private Image m_headicon;
    private Image m_positionBg;
    private Image m_armylvl;
    private Image m_headVip;
    private Text m_txtLevel;

    public override void Awake()
    {
        m_txtName = transform.Find("namebk/name").GetComponent<Text>();
        m_txtPosition = transform.Find("info/Text").GetComponent<Text>();
        m_positionBg = transform.Find("info").GetComponent<Image>();
        m_headVip = transform.Find("headframe/heroVip").GetComponent<Image>();
        m_headicon = transform.Find("headframe/head").GetComponent<Image>();
        m_armylvl = transform.Find("level/imgArmy").GetComponent<Image>();
        m_sexImg = transform.Find("sex").GetComponent<Image>();
        m_txtLevel = transform.Find("level/Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as toc_corps_view_info.manager;
        m_txtName.text = m_data.member_name;
        m_txtPosition.text = PanelCorps.m_strPosition[m_data.member_type - 1];
        m_txtLevel.text = String.Format("{0}级", m_data.level);
        m_headicon.SetSprite(ResourceManager.LoadRoleIcon(m_data.icon));
        m_armylvl.SetSprite(ResourceManager.LoadArmyIcon(m_data.level));
        string nameBgSpriteName;
        if (m_data.member_type > (int)CORPS_MEMBER_TYPE.TYPE_VICELEADER &&
            m_data.member_type < (int)CORPS_MEMBER_TYPE.TYPE_MEMBER)
        {
            nameBgSpriteName = "name_bg_3";
        }
        else
        {
            nameBgSpriteName = "name_bg_" + (m_data.member_type - 1);
        }
        m_positionBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, nameBgSpriteName));
        m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, m_data.sex == 0 ? "male" : "female"));

        var heroType = m_data.hero_type;
        m_headVip.gameObject.TrySetActive(false);
        if (heroType != (int)EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroType == (int)EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == (int)EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == (int)EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
            }
        }
    }
}