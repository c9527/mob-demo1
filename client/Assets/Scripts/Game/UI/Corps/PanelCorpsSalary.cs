﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelCorpsSalary : BasePanel
{
    public Image player_img;
    public Text name_txt;
    public Text last_salary_txt;
    public Text contribute_txt;
    public Text percent_txt;
    public Text corps_contribute_txt;
    public Text corps_salary_txt;
    public Text tips_txt;
    public Image item_img;
    public Button get_btn;

    public PanelCorpsSalary()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsSalary");

        AddPreLoadRes("UI/Corps/PanelCorpsSalary", EResType.UI);
    }

    public override void Init()
    {
        player_img = transform.Find("frame/player_img").GetComponent<Image>();
        name_txt = transform.Find("frame/name_txt").GetComponent<Text>();
        last_salary_txt = transform.Find("frame/last_salary_txt").GetComponent<Text>();
        contribute_txt = transform.Find("frame/contribute_txt").GetComponent<Text>();
        percent_txt = transform.Find("frame/percent_txt").GetComponent<Text>();
        corps_contribute_txt = transform.Find("frame/corps_contribute_txt").GetComponent<Text>();
        corps_salary_txt = transform.Find("frame/corps_salary_txt").GetComponent<Text>();
        tips_txt = transform.Find("frame/tips_txt").GetComponent<Text>();
        item_img = transform.Find("frame/item_img").GetComponent<Image>();
        get_btn = transform.Find("frame/get_btn").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(transform.Find("frame/close_btn")).onPointerClick += (target, evt) => HidePanel();
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
