﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
/***************************************************************************
 * Author: Fujiangshan
 * Create Time: 2015/11/11 15:41:56
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelCorpsSetting : BasePanel
{

    private int curType = 1;
    private int curLimit = -1;
    public static string[] SETTING_NAME = new string[] { "", "无验证加入", "需要战队长官验证", "拒绝任何人加入" };
    /// <summary>
    /// 当前限制类型
    /// </summary>
    private Text typeText;

    private Text limitInfoText;
    /// <summary>
    /// 等级限制输入文本
    /// </summary>
    private InputField limitText;
    /// <summary>
    /// 进入等级
    /// </summary>
    private int enter_level;


    private Image activeBg;//可输入背景
    private Image deActiveBg;//不可输入背景


    /// <summary>
    /// 设置信息
    /// </summary>
    private GameObject settingGo;
    /// <summary>
    /// 战队设置类型
    /// </summary>
    public enum SETTING_TYPE
    {
        NONE = 1,
        CHECK,
        DISALLOW
    }
    /// <summary>
    /// 设置类型
    /// </summary>
    private ItemDropInfo[] m_settingClassifyArr;

    public PanelCorpsSetting()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsSetting");
        AddPreLoadRes("UI/Corps/PanelCorpsSetting", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }
    public override void Init()
    {
        m_settingClassifyArr = new[]
        {
            new ItemDropInfo { type = "1", subtype = "", label = SETTING_NAME[1]},
            new ItemDropInfo { type = "2", subtype = "", label = SETTING_NAME[2]},
            new ItemDropInfo { type = "3", subtype = "", label = SETTING_NAME[3]}
        };

        limitText = m_tran.Find("InputText").GetComponent<InputField>();
        m_tran.Find("InputText").gameObject.AddComponent<InputFieldFix>();
        typeText = m_tran.Find("Condition/Text").GetComponent<Text>();
        settingGo = m_tran.Find("Condition").gameObject;
        limitInfoText = m_tran.Find("LimitInfoText").GetComponent<Text>();
        deActiveBg = m_tran.Find("DisActiveBg").GetComponent<Image>();
        activeBg = m_tran.Find("ActiveBg").GetComponent<Image>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("agree").gameObject).onPointerClick += OnClkAgree;
        UGUIClickHandler.Get(settingGo).onPointerClick += OnClkSetting;
        limitText.GetComponent<InputFieldFix>().onEndEdit += OnEndEdit;
    }

    private void OnEndEdit(string text)
    {
        if (limitText.text == "" || limitText.text == "-")
        {
            limitText.text = "10";
        }
        int level = int.Parse(limitText.text);
        if (level > 100)
        {
            level = 100;
            limitText.text = level + "";
        }
        else if (level < 10)
        {
            level = 10;
            limitText.text = level + "";
        }
    }

    public override void OnShow()
    {
        if (m_params.Length < 1)
            return;
        enter_level = (int)m_params[0];
        switch (enter_level)
        {
            case -1:
                curType = (int)SETTING_TYPE.DISALLOW;
                curLimit = -1;
                break;
            case 0:
                curType = (int)SETTING_TYPE.CHECK;
                curLimit = -1;
                break;
            default:
                curType = (int)SETTING_TYPE.NONE;
                curLimit = enter_level;
                break;
        }
        showViewInfo();
    }

    private void showViewInfo()
    {
        typeText.text = SETTING_NAME[curType];
        if (curLimit == -1)
        {
            limitText.text = "";
        }
        else
        {
            limitText.text = curLimit + "";
        }

        if (curType == (int)SETTING_TYPE.NONE)
        {
            limitInfoText.color = new Color(126 / 255f, 197 / 255f, 255 / 255f);
            deActiveBg.enabled = false;
            activeBg.enabled = true;
            limitText.textComponent.color = new Color(1f, 1f, 1f);
            limitText.enabled = true;
        }
        else
        {
            limitInfoText.color = new Color(132 / 255f, 132 / 255f, 132 / 255f);
            limitText.textComponent.color = new Color(113 / 255f, 113 / 255f, 113 / 255f);
            deActiveBg.enabled = true;
            activeBg.enabled = false;
            limitText.enabled = false;
        }
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    
    }




    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    protected void OnClkAgree(GameObject target, PointerEventData eventData)
    {
        int setLevel = 0;
        switch (curType)
        {
            case (int)SETTING_TYPE.NONE:
                setLevel = int.Parse(limitText.text);
                if (setLevel < 1)
                {
                    setLevel = 1;
                }
                break;
            case (int)SETTING_TYPE.CHECK:
                setLevel = 0;
                break;
            case (int)SETTING_TYPE.DISALLOW:
                setLevel = -1;
                break;
        }
        NetLayer.Send(new tos_corps_set_enter_level() { level = setLevel });
        HidePanel();
    }

    protected void OnClkSetting(GameObject target, PointerEventData evetData)
    {
        UIManager.ShowDropList(m_settingClassifyArr, OnDropListItemSelected, m_tran, target, 400);
    }

    private void OnDropListItemSelected(ItemDropInfo data)
    {
        UIManager.HideDropList();
        curType = int.Parse(data.type);
        showViewInfo();
        //typeText.text=SETTING_NAME[curType];
        //typeText.color=new Color()
    }
}
