﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelCorpsCreate : BasePanel
{
    private InputField m_inputName;
    private int m_iPanel = 1;
    private int m_iColor = 1;
    private int m_iBiaozhi = 1;

    private static int PANEL_MAX = 3;
    private static int COLOR_MAX = 5;
    private static int BIAOZHI_MAX = 20;

    private Image m_imgFlag;
    private Image m_imgBiaozhi;

    private Text m_txtPanelName;
    private Text m_txtColorName;
    private Text m_txtBiaozhiName;

    public PanelCorpsCreate()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsCreate");
        AddPreLoadRes("UI/Corps/PanelCorpsCreate", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_inputName = m_tran.Find("name_input").GetComponent<InputField>(); ;
        m_inputName.gameObject.AddComponent<InputFieldFix>();
        m_imgFlag = m_tran.Find("flag").GetComponent<Image>();
        m_imgBiaozhi = m_tran.Find("flag/biaozhi").GetComponent<Image>();

        m_txtPanelName = m_tran.Find("panel_sel/name").GetComponent<Text>();
        m_txtColorName = m_tran.Find("color_sel/name").GetComponent<Text>();
        m_txtBiaozhiName = m_tran.Find("biaozhi_sel/name").GetComponent<Text>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("coin_create/create").gameObject).onPointerClick += OnClkCoinCreate;
        UGUIClickHandler.Get(m_tran.Find("diamond_create/create").gameObject).onPointerClick += OnClkDiamondCreate;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClose;

        UGUIClickHandler.Get(m_tran.Find("panel_sel/panel_left").gameObject).onPointerClick += OnPanelLeft;
        UGUIClickHandler.Get(m_tran.Find("panel_sel/panel_right").gameObject).onPointerClick += OnPanelRight;
        UGUIClickHandler.Get(m_tran.Find("biaozhi_sel/biaozhi_left").gameObject).onPointerClick += OnBiaozhiLeft;
        UGUIClickHandler.Get(m_tran.Find("biaozhi_sel/biaozhi_right").gameObject).onPointerClick += OnBiaozhiRight;
        UGUIClickHandler.Get(m_tran.Find("color_sel/color_left").gameObject).onPointerClick += OnColorLeft;
        UGUIClickHandler.Get(m_tran.Find("color_sel/color_right").gameObject).onPointerClick += OnColorRight;
    }

      public override void OnShow()
      {
          m_tran.Find("coin_create/num").GetComponent<Text>().text = ConfigManager.GetConfig<ConfigMisc>().GetLine("create_corps_reqcoin").ValueInt.ToString();
          m_tran.Find("diamond_create/num").GetComponent<Text>().text = ConfigManager.GetConfig<ConfigMisc>().GetLine("create_corps_reqdiamond").ValueInt.ToString();
      }

      public override void OnHide()
      {

      }

      public override void OnBack()
      {
          
      }

      public override void OnDestroy()
      {
         
      }

      protected void OnPanelLeft(GameObject target, PointerEventData eventData)
      {
          if (m_iPanel <= 1)
              return;
          m_iPanel--;
          ResetFlag();
      }
      
      protected void OnPanelRight( GameObject target, PointerEventData eventData)
      {
          if (m_iPanel >= PANEL_MAX)
              return;
          m_iPanel++;
          ResetFlag();
      }

      protected void OnBiaozhiLeft(GameObject target, PointerEventData eventData )
    {
        if (m_iBiaozhi <= 1)
            return;
        m_iBiaozhi--;
        ResetBiaozhi();
    }

      protected void OnBiaozhiRight(GameObject target, PointerEventData eventData )
    {
        if (m_iBiaozhi >= BIAOZHI_MAX)
            return;
        m_iBiaozhi++;
        ResetBiaozhi();
    }

      protected void OnColorLeft(GameObject target, PointerEventData eventData )
    {
        if (m_iColor <= 1)
            return;
        m_iColor--;
        ResetFlag();
    }

      protected void OnColorRight(GameObject target, PointerEventData eventData )
    {
        if (m_iColor >= COLOR_MAX)
            return;

        m_iColor++;
        ResetFlag();
    }

    protected void ResetFlag()
    {
        string name = "f" + m_iPanel.ToString() + "_" + "c" + m_iColor.ToString();
        m_imgFlag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, name));

        m_txtPanelName.text = "旗面" + m_iPanel.ToString();
        m_txtColorName.text = "颜色" + m_iColor.ToString();
    }

    protected void ResetBiaozhi()
    {
        string name = "biaozhi_" + m_iBiaozhi.ToString();
        m_imgBiaozhi.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, name));

        m_txtBiaozhiName.text = "标志" + m_iBiaozhi.ToString();
    }

     protected void OnClkCoinCreate(GameObject target, PointerEventData eventData)
     {
          if (PlayerSystem.CheckMoney(40000, EnumMoneyType.COIN, null) == false)
          {
              HidePanel();
              return;
          }

          if( m_inputName.text == "" )
          {
              UIManager.ShowTipPanel("还未取名字");
              return;
          }
          tos_corps_create_corps msg = new tos_corps_create_corps();
          msg.icon = m_iBiaozhi;
          msg.corps_name = m_inputName.text;
          msg.money_type = 0;
          msg.pannel = m_iPanel;
          msg.color = m_iColor;
          NetLayer.Send(msg);
          HidePanel();
      }

      protected void OnClose( GameObject target, PointerEventData eventData )
      {
          HidePanel();
      }
      protected void OnClkDiamondCreate(GameObject target, PointerEventData eventData)
      {
          if(PlayerSystem.CheckMoney(400,EnumMoneyType.DIAMOND,null)==false)
          {
              HidePanel();
              return;
          }

          if (m_inputName.text == "")
          {
              UIManager.ShowTipPanel("还未取名字");
              return;
          }

          tos_corps_create_corps msg = new tos_corps_create_corps();
          msg.icon = m_iBiaozhi;
          msg.corps_name = m_inputName.text;
          msg.money_type = 1;
          msg.pannel = m_iPanel;
          msg.color = m_iColor;

          NetLayer.Send(msg);
          HidePanel();
      }
}
