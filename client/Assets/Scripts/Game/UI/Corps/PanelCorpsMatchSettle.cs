﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class PanelCorpsMatchSettle : BasePanel 
{
    public PanelCorpsMatchSettle()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsMatchSettle");
        AddPreLoadRes("UI/Corps/PanelCorpsMatchSettle", EResType.UI);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    Text m_teamname1;
    Image m_flag1;
    Image m_icon1;
    
    
    Text m_teamname2;
    Image m_flag2;
    Image m_icon2;

    GameObject m_win1;
    GameObject m_win2;

    GameObject m_team1;
    GameObject m_team2;
    float m_targetX1;
    float m_targetX2;

    bool start;
    Text m_txt;
    public override void Init()
    {
        m_teamname1 = m_tran.Find("team1bg/name").GetComponent<Text>();
        m_flag1 = m_tran.Find("team1bg/flag").GetComponent<Image>();
        m_icon1 = m_tran.Find("team1bg/flag/icon").GetComponent<Image>();

        m_teamname2 = m_tran.Find("team2bg/name").GetComponent<Text>();
        m_flag2 = m_tran.Find("team2bg/flag").GetComponent<Image>();
        m_icon2 = m_tran.Find("team2bg/flag/icon").GetComponent<Image>();

        m_win1 = m_tran.Find("team1bg/result").gameObject;
        m_win2 = m_tran.Find("team2bg/result").gameObject;

        m_team1 = m_tran.Find("team1bg").gameObject;
        m_team2 = m_tran.Find("team2bg").gameObject;


        m_targetX1 = -252f;
        m_txt = m_tran.Find("Text").GetComponent<Text>();

        start = false;

    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("")).onPointerClick += delegate
        {
            if (m_txt.gameObject.activeSelf)
            {
                HidePanel();
                UIManager.PopPanel<PanelHallBattle>();
            }
        };
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        m_txt.gameObject.TrySetActive(false);
        m_team1.transform.localPosition = new Vector3(-669f, 94f, 0f);
        m_team2.transform.localPosition = new Vector3(676f, -69, 0f);
        m_win1.TrySetActive(false);
        m_win2.TrySetActive(false);
        m_win1.transform.localScale = new Vector3(1.5f, 1.5f);
        m_win2.transform.localScale = new Vector3(1.5f, 1.5f);
        start = true;
        m_teamname1.text = CorpsDataManager.m_roomdata[0].corpsname;
        m_teamname2.text = CorpsDataManager.m_roomdata[1].corpsname;
        string flag = "f" + CorpsDataManager.m_roomdata[0].corpsflag.ToString() + "_" + "c" + CorpsDataManager.m_roomdata[0].corpscolor.ToString();
        m_flag1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        flag = "f" + CorpsDataManager.m_roomdata[1].corpsflag.ToString() + "_" + "c" + CorpsDataManager.m_roomdata[1].corpscolor.ToString();
        m_flag2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        m_icon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, CorpsDataManager.m_roomdata[0].corpsicon.ToString()));
        m_icon2.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, CorpsDataManager.m_roomdata[1].corpsicon.ToString()));
        if (m_params != null)
        {
            result win = m_params[0] as result;
            if (win.win)
            {
                m_win1.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("CorpsMatch", "winsettle"));
                m_win2.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
            }
            else
            {
                m_win2.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("CorpsMatch", "winsettle"));
                m_win1.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite("CorpsMatch", "losesettle"));
            }
        }

    }

    public override void Update()
    {
        if (start)
        {
            m_team1.transform.localPosition += new Vector3(10, 0, 0);
            m_team2.transform.localPosition -= new Vector3(10, 0, 0);
        }
        if (m_team1.transform.localPosition.x >= m_targetX1)
        {
            start = false;
            m_win1.TrySetActive(true);
            m_win2.TrySetActive(true);
        }

        if (m_win1.activeSelf && m_win2.activeSelf)
        {
            if (m_win1.transform.localScale.x <= 1)
            {
                m_txt.gameObject.TrySetActive(true);
                return;
            }
            m_win1.transform.localScale -= new Vector3(0.02f, 0.02f);
            m_win2.transform.localScale -= new Vector3(0.02f, 0.02f);
        }

        

    }
}

