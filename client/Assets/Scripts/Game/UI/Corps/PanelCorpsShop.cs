﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelCorpsShop : BasePanel
{
    private toc_corps_enter_mall _data;

    private float _last_time;
    private UIEffect _halo_effect;

    private p_corps_mallbox _bid_data;

    public Image item_img;
    public Text name_txt;
    public Text tips_txt;
    public Text value_txt;
    public DataGrid prop_list;
    public DataGrid item_list;

    public Transform bid_frame;
    public Text bid_tips_txt;
    public Text cur_txt;
    public InputField bid_txt;

    public PanelCorpsShop()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsShop");

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("UI/Corps/PanelCorpsShop", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPropRender", EResType.UI);
    }

    public override void Init()
    {
        item_img = m_tran.Find("frame/item_img").GetComponent<Image>();
        name_txt = m_tran.Find("frame/item_img/name_txt").GetComponent<Text>();
        prop_list = m_tran.Find("frame/prop_list").gameObject.AddComponent<DataGrid>();
        prop_list.useClickEvent = false;
        prop_list.autoSelectFirst = false;
        prop_list.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemPropRender"), typeof(ItemPropRender));
        tips_txt = m_tran.Find("frame/tips_txt").GetComponent<Text>();
        value_txt = m_tran.Find("frame/value_txt").GetComponent<Text>();
        item_list = m_tran.Find("frame/item_list/content").gameObject.AddComponent<DataGrid>();
        item_list.autoSelectFirst = true;
        item_list.SetItemRender(m_tran.Find("frame/item_list/Render").gameObject, typeof(CorpsShopItemRender));
        item_list.onItemSelected = OnShopItemClick;

        bid_frame = m_tran.Find("bid_frame");
        bid_frame.gameObject.AddComponent<SortingOrderRenderer>();
        bid_tips_txt = bid_frame.Find("view/tips_txt").GetComponent<Text>();
        cur_txt = bid_frame.Find("view/cur_txt").GetComponent<Text>();
        bid_txt = bid_frame.Find("view/bid_txt").GetComponent<InputField>();
        bid_txt.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
//        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target, evt) => HidePanel();
        UGUIClickHandler.Get(bid_frame.Find("view/btnClose")).onPointerClick += CloseBidPanel;
        UGUIClickHandler.Get(bid_frame.Find("view/btnOk")).onPointerClick += OnBidItemHandler;
    }

    private void OnBidItemHandler(GameObject target, PointerEventData eventData)
    {
        if (_bid_data != null && _bid_data.can_use==true)
        {
            var bid_value = 0;
            if (!int.TryParse(bid_txt.text, out bid_value))
            {
                TipsManager.Instance.showTips("竞价输入错误，无法参与竞价！");
                return;
            }
            if (bid_value <= 0)
            {
                TipsManager.Instance.showTips("竞价太低，无法参与竞价！");
            }
            else if (!PlayerSystem.CheckMoney(bid_value, EnumMoneyType.CORPS_COIN))
            {
                TipsManager.Instance.showTips("战队币不足！");
            }
            else
            {
                CorpsModel.Instance.TosShopBidItem(_bid_data.box_id,_bid_data.mall_id,bid_value);
            }
        }
        else
        {
            UIManager.ShowTipPanel("竞价数据错误");
        }
    }

    private void CloseBidPanel(GameObject target, PointerEventData eventData)
    {
        _bid_data = null;
        bid_frame.gameObject.TrySetActive(false);
    }

    public void OpenBidPanel(p_corps_mallbox data)
    {
        _bid_data = data;
        bid_frame.gameObject.TrySetActive(true);
        SortingOrderRenderer.RebuildAll();
        cur_txt.text = "当前价格：     " + _bid_data.highest_bid;
    }

    private void OnShopItemClick(object renderData)
    {
        var sdata = renderData as p_corps_mallbox;
        if (sdata == null || sdata.box_id==0)
        {
            return;
        }
        if (sdata.can_use == false)
        {
            if (CorpsDataManager.m_corpsData.leader_id != PlayerSystem.roleId)
            {
                UIManager.ShowTipPanel("只有队长拥有解锁权限");
                return;
            }
            var info = ConfigManager.GetConfig<ConfigCorpsMallBox>().GetLine(sdata.box_id);
            if (info != null)
            {
                if (CorpsDataManager.m_corpsData.level < info.CorpsLevel)
                {
                    UIManager.ShowTipPanel("战队等级不足！");
                }
                else
                {
                    UIManager.ShowTipPanel(string.Format("当前战队建设值：{0}\n解锁物品栏消耗建设值：{1}\n是否消耗建设值，解锁新的战队商店物品栏？", CorpsDataManager.m_corpsData.tribute, info.Tribute), () =>
                    {
                        CorpsModel.Instance.TosUnlockShopItem(info.ID);
                    });
                }
            }
            else
            {
                UIManager.ShowTipPanel("配置数据错误");
            }
        }
        else
        {
            OnUpdateCurrentItem(sdata);
        }
    }

    private void OnUpdateCurrentItem(p_corps_mallbox data)
    {
        if (data == null)
        {
            return;
        }
        var mall_info = data.mall_id>0 ? ConfigManager.GetConfig<ConfigMall>().GetLine(data.mall_id) : null;
        ConfigItemLine titem = null;
        if(mall_info!=null)
        {
            item_img.gameObject.TrySetActive(true);
            name_txt.gameObject.TrySetActive(true);
            titem = ItemDataManager.GetItem(mall_info.ItemId);
            item_img.SetSprite(ResourceManager.LoadIcon(titem.Icon),titem.SubType);
            item_img.SetNativeSize();
            name_txt.text = mall_info.Name;
        }
        else
        {
            item_img.gameObject.TrySetActive(false);
            name_txt.gameObject.TrySetActive(false);
        }
        prop_list.Data = titem is ConfigItemWeaponLine ? ItemDataManager.GetPropRenderInfos(titem as ConfigItemWeaponLine) : new object[0];
    }

    private void OnUpdatePlayerData()
    {
        value_txt.text = "我的战队币：           " + PlayerSystem.roleData.corps_coin.ToString();
    }

    private void OnUpdateShopData(toc_corps_enter_mall data)
    {
        _data = data;
        _last_time = 0;
        OnUpdatePlayerData();
        OnUpdateView();
    }

    public override void OnShow()
    {
//        if (_halo_effect == null)
//        {
//            _halo_effect = UIEffect.ShowEffectAfter(item_img.transform, EffectConst.UI_MALL_HALO, new Vector2(-324,117));
//        }
        _data = CorpsModel.Instance.shop_data;
        if (_data == null)
        {
            CorpsModel.Instance.TosGetShopData();
            return;
        }
        _last_time = 0;
        OnUpdatePlayerData();
        OnUpdateView();
    }

    private void OnUpdateView()
    {
        if (_data == null)
        {
            return;
        }
        var list = new List<p_corps_mallbox>();
        foreach(var sdata in _data.list)
        {
            list.Add(sdata);
            if (sdata.can_use == false)
            {
                break;
            }
        }
        item_list.Data = list.ToArray();
    }

    public override void OnHide()
    {
        _data = null;
        _last_time = -1;
//        if (_halo_effect != null)
//        {
//            _halo_effect.Destroy();
//            _halo_effect = null;
//        }
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorps>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_data == null || _last_time == -1 || Time.realtimeSinceStartup < (_last_time + 1))
        {
            return;
        }
        _last_time = Time.realtimeSinceStartup;
        var cd = _data.next_flush_timestamp - TimeUtil.GetNowTimeStamp();
        if (cd <= 0 && cd>-10)
        {
            _last_time = -1;
            CorpsModel.Instance.TosGetShopData();
        }
        tips_txt.text = "下次刷新剩余时间：" + TimeUtil.FormatTime(Math.Max(cd,0));  
    }

    //==================================== s to c
    private void Toc_player_attribute(CDict data)
    {
        OnUpdatePlayerData();
    }

    private void Toc_corps_enter_mall(toc_corps_enter_mall data)
    {
        OnUpdateShopData(data);
    }

    private void Toc_corps_buymallbox(toc_corps_buymallbox data)
    {
        if (data.result == 1)
        {
            TipsManager.Instance.showTips("等级不够");
        }
        else if (data.result == 2)
        {
            TipsManager.Instance.showTips("战队币不足");
        }
        else
        {
            TipsManager.Instance.showTips("成功解锁一个商店物品");
        }
    }

    private void Toc_corps_mallbid(toc_corps_mallbid data)
    {
        if (data.result == 1)
        {
            TipsManager.Instance.showTips("竞价过低");
        }
        else if (data.result == 2)
        {
            TipsManager.Instance.showTips("战队币不足");
        }
        else if (data.result == 3)
        {
            TipsManager.Instance.showTips("竞价失败");
        }
        else
        {
            TipsManager.Instance.showTips("竞价成功");
            CloseBidPanel(null, null);
        }
    }
}

class CorpsShopItemRender : ItemRender
{
    private p_corps_mallbox _data;

    private Toggle _toggle;

    public Image bg_img;
    public Image item_img;
    public Transform money_txt;
    public Text name_txt;
    public Text info_txt;
    public Text desc_txt;
    public Button bid_btn;
    private ConfigItemLine m_itemCfg;

    public override void Awake()
    {
        _toggle = transform.GetComponent<Toggle>();
        bg_img = transform.GetComponent<Image>();
        item_img = transform.Find("item_img").GetComponent<Image>();
        money_txt = transform.Find("money_txt");
        name_txt = transform.Find("name_txt").GetComponent<Text>();
        info_txt = transform.Find("info_txt").GetComponent<Text>();
        desc_txt = transform.Find("desc_txt").GetComponent<Text>();
        bid_btn = transform.Find("bid_btn").GetComponent<Button>();

        UGUIClickHandler.Get(transform.Find("bid_btn")).onPointerClick += OnBidBtnClick;
//        MiniTipsManager.get(item_img.gameObject).miniTipsDataFunc = (go) =>
//        {
//            if (m_itemCfg == null) return null;
//            return new MiniTipsData() { name=m_itemCfg.DispName,dec=m_itemCfg.Desc};
//        };
    }

    private void OnBidBtnClick(GameObject target, PointerEventData eventData)
    {
        var shop_view = UIManager.GetPanel<PanelCorpsShop>();
        if (shop_view != null && shop_view.IsOpen())
        {
            shop_view.OpenBidPanel(_data);
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as p_corps_mallbox;
        if (_data == null)
        {
            return;
        }
        _toggle.interactable = _data.can_use;
        if (_toggle.interactable == false)
        {
            _toggle.interactable = false;
        }
        if (_data.mall_id > 0)
        {
            money_txt.gameObject.TrySetActive(true);
            info_txt.gameObject.TrySetActive(true);
            bid_btn.gameObject.TrySetActive(true);
            desc_txt.gameObject.TrySetActive(true);
            var mall_info = ConfigManager.GetConfig<ConfigMall>().GetLine(_data.mall_id);
            var cfg = ItemDataManager.GetItem(mall_info.ItemId);
            item_img.SetSprite(ResourceManager.LoadIcon(mall_info.ItemId),cfg!=null?cfg.SubType:"");
            m_itemCfg = cfg;
            money_txt.GetComponent<Text>().text = _data.highest_bid.ToString();
            var icon = money_txt.Find("Image").GetComponent<Image>();
            icon.SetSprite(ResourceManager.LoadMoneyIcon(mall_info.MoneyType));
            icon.SetNativeSize();
            name_txt.text = mall_info.Name;
            info_txt.text = _data.highest_bidder_name;
            desc_txt.text = "";
        }
        else
        {
            money_txt.gameObject.TrySetActive(false);
            info_txt.gameObject.TrySetActive(false);
            bid_btn.gameObject.TrySetActive(false);
            desc_txt.text = string.Format("战队等级{0}级开启", ConfigManager.GetConfig<ConfigCorpsMallBox>().GetLine(_data.box_id).CorpsLevel);
            item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, "not_available"));
            name_txt.text = "";
        }
        item_img.SetNativeSize();
    }
}
