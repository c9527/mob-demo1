﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class PanelMyTeamRecord : BasePanel
{
    public class ItemMatchRecord : ItemRender
    {
        Text team1;
        Text team2;
        Image team1win;
        Image team2win;
        Text mode;
        Image team1bg;
        Image team2bg;
        Image team1flag;
        Image team2flag;
        Text date;

        public override void Awake()
        {
            team1 = transform.Find("Team1/name").GetComponent<Text>();
            team2 = transform.Find("Team2/name").GetComponent<Text>();
            team1win = transform.Find("Team1/win").GetComponent<Image>();
            team2win = transform.Find("Team2/win").GetComponent<Image>();
            team1flag = transform.Find("Team1").GetComponent<Image>();
            team2flag = transform.Find("Team2").GetComponent<Image>();
            date = transform.Find("Date").GetComponent<Text>();

        }

        protected override void OnSetData(object data)
        {
            var info = data as p_corpsmatch_record;
            team1.text = CorpsDataManager.GetCorpsName();
            team2.text = info.name;
            if (info.win == 1)
            {
                team1win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "win"));
                team2win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "lose"));
            }
            else if (info.win == 2)
            {
                team2win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "win"));
                team1win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "lose"));
            }
            else if (info.win == 0)
            {
                team1win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "lose"));
                team2win.SetSprite(ResourceManager.LoadSprite("CorpsMatch", "lose"));
            }
            string biaozhi = "biaozhi_" + CorpsDataManager.m_corpsData.logo_icon.ToString();
            team1flag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            biaozhi = "biaozhi_" + info.icon.ToString();
            team2flag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
            DateTime datetime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long ltime = long.Parse(info.day.ToString() + "0000000");
            TimeSpan tonow = new TimeSpan(ltime);
            DateTime d = datetime.Add(tonow);
            date.text = d.Month.ToString() + "月" + d.Day.ToString() + "日";
        }
    }


    DataGrid m_recordList;

    public PanelMyTeamRecord()
    {
        SetPanelPrefabPath("UI/Corps/PanelMyTeamRecord");
        AddPreLoadRes("UI/Corps/PanelMyTeamRecord", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_corpsmatch_record());
    }

    public override void Init()
    {
        m_recordList = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_recordList.SetItemRender(m_tran.Find("ScrollDataGrid/content/ItemRecord").gameObject, typeof(ItemMatchRecord));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void Update()
    {

    }

    void Toc_corpsmatch_record(toc_corpsmatch_record data) 
    {
        m_recordList.Data = data.records;
    }
    


}
