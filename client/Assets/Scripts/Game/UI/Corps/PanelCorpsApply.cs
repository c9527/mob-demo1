﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelCorpsApply : BasePanel
{
    private DataGrid m_applyDataGrid;
    private List<p_corps_apply_toc> m_lstApply;
    //private p_corps_data_toc.apply[] m_lstApply;
    public PanelCorpsApply()
    {
        SetPanelPrefabPath("UI/Corps/PanelApply");
        AddPreLoadRes("UI/Corps/PanelApply", EResType.UI);
        AddPreLoadRes("UI/Corps/ItemApplyInfo", EResType.UI);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }
    public override void Init()
    {
        m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_applyDataGrid = m_tran.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
        m_applyDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Corps/ItemApplyInfo"), typeof(ItemApplyInfo));
        m_applyDataGrid.useLoopItems = true;

        m_lstApply = new List<p_corps_apply_toc>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("agree").gameObject).onPointerClick += OnClkAgree;
        UGUIClickHandler.Get(m_tran.Find("ignore").gameObject).onPointerClick += OnClkIgnore;
    }

    public override void OnShow()
    {
        if (m_params.Length < 1)
            return;
        m_lstApply = m_params[0] as List<p_corps_apply_toc>;
        m_applyDataGrid.Data = m_lstApply.ToArray();
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick -= OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("agree").gameObject).onPointerClick -= OnClkAgree;
        UGUIClickHandler.Get(m_tran.Find("ignore").gameObject).onPointerClick -= OnClkIgnore;
    }

    protected void OnClkClose( GameObject target, PointerEventData eventData )
    {
        HidePanel();
    }

    protected void OnClkAgree( GameObject target, PointerEventData eventData )
    {
        var item = m_applyDataGrid.SelectedData<p_corps_apply_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择一个申请");
            return;
        }

        tos_corps_agree_apply msg = new tos_corps_agree_apply();
        msg.apply_id = item.id;
        NetLayer.Send(msg);
        RemoveApply(item.id);
        //HidePanel();
    }

    protected void OnClkIgnore( GameObject target, PointerEventData eventData )
    {
        var item = m_applyDataGrid.SelectedData<p_corps_apply_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择一个申请");
            return;
        }

        tos_corps_refuse_apply msg = new tos_corps_refuse_apply();
        msg.apply_id = item.id;
        NetLayer.Send(msg);
        RemoveApply(item.id);
        //HidePanel();
    }

    protected void RemoveApply( long id )
    {
        for( int i = 0; i < m_lstApply.Count; ++i )
        {
            if( m_lstApply[i].id == id )
            {
                m_lstApply.RemoveAt(i);
                break;
            }
        }

        m_applyDataGrid.Data = m_lstApply.ToArray();
    }
}
