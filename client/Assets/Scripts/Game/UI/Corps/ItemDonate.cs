﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemDonate : ItemRender 
{
    private ConfigCorpsDonateLine m_data;
    private Text m_txtContentFirst;
    private Text m_txtContentMid;
    private Text m_txtContentLast;
    private Button m_btnDonate;
    private Image m_costImg;
    private Image m_gainImg;
    public override void Awake()
    {
        m_txtContentFirst = transform.Find("content/first").GetComponent<Text>();
        m_txtContentMid = transform.Find("content/mid").GetComponent<Text>();
        m_txtContentLast = transform.Find("content/last").GetComponent<Text>();
        m_btnDonate = transform.Find("btn_donate").GetComponent<Button>();
        m_costImg = transform.Find("content/cost").GetComponent<Image>();
        m_gainImg = transform.Find("content/gain").GetComponent<Image>();

        UGUIClickHandler.Get(m_btnDonate.gameObject).onPointerClick += OnClickDonate;
    }
    protected override void OnSetData(object data)
    {
        m_data = data as ConfigCorpsDonateLine;

        m_txtContentFirst.text = "捐献";
        if( m_data.Coin != 0 )
        {
            m_costImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "gold"));
            m_txtContentMid.text = m_data.Coin + ",获得";
        }
        else if (m_data.Diamond != 0 )
        {
            m_costImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "diamond"));
            m_txtContentMid.text = m_data.Diamond + ",获得";
        }
        else
        {
            return;
        }
        m_gainImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, "corps_credit"));
        m_txtContentLast.text = string.Format("{0},{1}点建设值", m_data.CorpsCoin.ToString(), m_data.Tribute.ToString());
    }

    private void OnClickDonate(GameObject go, PointerEventData eventData)
    {
        var donatePanel = UIManager.GetPanel<PanelCorpsDonate>();
        int donateId = m_data.ID;
        var config = ConfigManager.GetConfig<ConfigCorpsDonate>();
        if (config == null)
            return;

        ConfigCorpsDonateLine donateInfo = config.GetLine(donateId);
        if (donateInfo == null)
            return;

        if( donateInfo.Coin != 0 )
        {
            if (donatePanel.RemainDonateCoin < donateInfo.Coin)
            {
                TipsManager.Instance.showTips("剩余可捐献的金币不足!");
                return;
            }
        }
        else
        {
            if (donatePanel.RemainDonateDiamond < donateInfo.Diamond)
            {
                TipsManager.Instance.showTips("剩余可捐献的钻石不足!");
                return;
            }
        }

        tos_corps_donate_tribute msg = new tos_corps_donate_tribute();
        msg.donate_id = donateId;
        NetLayer.Send(msg);
    }


}
