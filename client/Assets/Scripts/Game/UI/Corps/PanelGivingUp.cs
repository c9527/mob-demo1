﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelGivingUp : BasePanel
{
    Image m1;
    Image m2;
    Image s1;
    Image s2;
    float time;
    float startTime;
    float restTime;
    Text m_text;

    public PanelGivingUp()
    {
        SetPanelPrefabPath("UI/Corps/PanelGivingUp");
        AddPreLoadRes("UI/Corps/PanelGivingUp", EResType.UI);
        AddPreLoadRes("Atlas/CorpsMatch", EResType.Atlas);
    }

    public override void Init()
    {
        time = 303;
        startTime = -1;
        m1 = m_tran.Find("TimeDown/m1").GetComponent<Image>();
        m2 = m_tran.Find("TimeDown/m2").GetComponent<Image>();
        s1 = m_tran.Find("TimeDown/s1").GetComponent<Image>();
        s2 = m_tran.Find("TimeDown/s2").GetComponent<Image>();
        m_text = m_tran.Find("Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Close")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("Button")).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        if (m_params != null)
        {
            string str = m_params[0] as string;
            m_text.text = str + "无人准备，弃权倒计时中...";
            startTime = Time.realtimeSinceStartup;
            restTime = ConfigMisc.GetInt("corpsmatch_abstain_time") * 60;
            time = restTime;
        }
    }

    public override void Update()
    {
        if (startTime < 0)
        {
            return;
        }
        restTime = time + 10 - (Time.time - startTime);
        if (restTime < 0)
        {
            startTime = -1;
            this.HidePanel();
        }
        int m = (int)restTime / 60;
        int s = (int)restTime % 60;
        m1.SetSprite(ResourceManager.LoadSprite("CorpsMatch", (m / 10).ToString()));
        m2.SetSprite(ResourceManager.LoadSprite("CorpsMatch", (m % 10).ToString()));
        s1.SetSprite(ResourceManager.LoadSprite("CorpsMatch", (s / 10).ToString()));
        s2.SetSprite(ResourceManager.LoadSprite("CorpsMatch", (s % 10).ToString()));
    }

    



}