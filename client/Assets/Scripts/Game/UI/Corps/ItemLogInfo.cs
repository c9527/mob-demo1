﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemLogInfo : ItemRender
{
    private p_corps_log_toc m_data;
    private Text m_txtLog; 
	// Use this for initialization
	public override void Awake()
    {
        m_txtLog = transform.Find("log").GetComponent<Text>();
    }
	
	// Update is called once per frame
     protected override void OnSetData(object data)
     {
         m_data = data as p_corps_log_toc;
         string strLog = "";
         string strTime = TimeUtil.GetTime(m_data.log_time).ToString("M月d号 HH:mm");
         strLog += strTime;
         strLog += " ";
         if( m_data.log_type == "agree_apply")
         {
             strLog += PanelCorps.m_strPosition[m_data.opr_type -1];
             strLog += m_data.opr_name;
             strLog += "批准";
             strLog += m_data.mbr_name;
             strLog += "加入战队";
         }
         else if( m_data.log_type == "kick")
         {
             strLog += PanelCorps.m_strPosition[m_data.opr_type -1];
             strLog += m_data.opr_name;
             strLog += "将";
             strLog += m_data.mbr_name;
             strLog += "踢出战队";
         }
         else if( m_data.log_type == "quit" )
         {
             strLog += m_data.opr_name;
             strLog += "退出战队";
         }
         else if( m_data.log_type == "change_member_type" )
         {
             strLog += PanelCorps.m_strPosition[m_data.opr_type -1];
             strLog += m_data.opr_name;
             strLog += "修改了";
             strLog += m_data.mbr_name;
             strLog += "的职位";
         }
         else if (m_data.log_type == "update_notice")
         {
             strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
             strLog += m_data.opr_name;
             strLog += "更新了战队公告";
         }
         else if (m_data.log_type == "change_icon")
         {
             strLog += PanelCorps.m_strPosition[m_data.opr_type - 1];
             strLog += m_data.opr_name;
             strLog += "更新了战队图标";
         }
         else if( m_data.log_type == "set_leader" )
         {
             strLog += "队长变更为";
             strLog += m_data.mbr_name;
         }
         else if (m_data.log_type == "impeach")
         {
             strLog += "由";
             strLog += m_data.mbr_name;
             strLog += "接任";
             strLog += m_data.opr_name;
             strLog += "成为新的队长";
         }
         else
         {
             return;
         }
         m_txtLog.text = strLog;

     }
}
