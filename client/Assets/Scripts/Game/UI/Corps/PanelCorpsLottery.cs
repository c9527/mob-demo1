﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelCorpsLottery : BasePanel
{
    private toc_corps_getlotterycounts _times_data;

    private int _lottery_cost;
    private float _play_time;
    private CorpsLotteryItemRender _target_render;
    private CorpsLotteryItemRender[] _tween_list;

    public Image hit_img;
    public Button action_btn;
    public Text tips_txt;
    public Text info_txt;
    public Text m_num;
    public CorpsLotteryItemRender[] item_list;
    
    public PanelCorpsLottery()
    {
        SetPanelPrefabPath("UI/Corps/PanelCorpsLottery");

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("UI/Corps/PanelCorpsLottery", EResType.UI);
    }

    public override void Init()
    {
        _lottery_cost = ConfigManager.GetConfig<ConfigMisc>().GetLine("corps_lettery_consume").ValueInt;

        hit_img = m_tran.Find("frame/hit_img").GetComponent<Image>();
        action_btn = m_tran.Find("frame/action_btn").GetComponent<Button>();
        tips_txt = m_tran.Find("frame/tips_txt").GetComponent<Text>();
        info_txt = m_tran.Find("frame/info_txt").GetComponent<Text>();
        m_num = m_tran.Find("frame/action_btn/Text").GetComponent<Text>();
        item_list = new CorpsLotteryItemRender[12];
        for (int i = 0; i < item_list.Length; i++)
        {
            item_list[i] = m_tran.Find("frame/item_" + i).gameObject.AddComponent<CorpsLotteryItemRender>();
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target, evt) => HidePanel();
        UGUIClickHandler.Get(action_btn.gameObject).onPointerClick += OnActionBtnClick;
        foreach (var render in item_list)
        {
            UGUIClickHandler.Get(render.gameObject).onPointerClick += OnLotteryItemClick;
        }
    }

    private void OnUpdatePlayerData()
    {
        tips_txt.text = "每次领取消耗" + _lottery_cost + "个战队币";
        info_txt.text = "我的战队币：    " + PlayerSystem.roleData.corps_coin;
    }

    private void OnLotteryItemClick(GameObject target, PointerEventData eventData)
    {
        if (_play_time>0)
        {
            TipsManager.Instance.showTips("别急，还在抽奖呢！");
            return;
        }
        var render = target.GetComponent<CorpsLotteryItemRender>();
        if (render != null && render.m_renderData is CDCorpLotteryItem)
        {
            var vo = render.m_renderData as CDCorpLotteryItem;
            if (vo == null || vo.info==null || vo.unlocked == true)
            {
                return;
            }
            if (CorpsDataManager.m_corpsData.level < vo.info.CorpsLevel)
            {
                UIManager.ShowTipPanel("战队等级不足");
            }
            else if(CorpsDataManager.m_corpsData.tribute < vo.info.Tribute)
            {
                int a = CorpsDataManager.m_corpsData.tribute;
                int b = vo.info.Tribute;
                UIManager.ShowTipPanel(string.Format("战队建设值不足,当前建设值{0}，需要建设值{1}", a.ToString(), b.ToString()));
            }
            else
            {
                int a = CorpsDataManager.m_corpsData.tribute;
                int b = vo.info.Tribute;
                string str = "当前战队建设值：" + a.ToString() + "\n" + "解锁消耗建设值：" + b.ToString() + "\n\n" + "是否消耗建设值,解锁格子";
                Action onok = () => CorpsModel.Instance.UnlockLotteryItem(vo.info.ID);
                UIManager.ShowTipPanel(str, onok, null, true, true);
            }

        }
    }

    private void OnActionBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_play_time>0)
        {
            TipsManager.Instance.showTips("别急，还在抽奖呢！");
            return;
        }
        if (_times_data == null || _times_data.cur_counts>=_times_data.total_counts)
        {
            TipsManager.Instance.showTips("抽奖次数已用完！");
            return;
        }
        if (PlayerSystem.CheckMoney(_lottery_cost,EnumMoneyType.CORPS_COIN,""))
        {
            _play_time = Time.realtimeSinceStartup;
            _target_render = null;
            hit_img.gameObject.TrySetActive(true);
            TimerManager.SetTimeOut(3f, CorpsModel.Instance.DoLottery);
        }
    }

    public override void OnShow()
    {
        _times_data = null;
        OnUpdatePlayerData();
        CorpsModel.Instance.TosGetLotteryData(true);
    }

    public override void OnHide()
    {
        _play_time = 0;
        _target_render = null;
        _times_data = null;
        TimerManager.RemoveTimeOut(CorpsModel.Instance.DoLottery);
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelCorps>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (_play_time == 0 || _tween_list==null || _tween_list.Length==0)
        {
            _play_time = 0;
            _target_render = null;
            return;
        }
        var pass = Time.realtimeSinceStartup - _play_time;
        var render = _tween_list[(int)(pass * _tween_list.Length / 1) % _tween_list.Length];
        hit_img.transform.localPosition = render.transform.localPosition;
        if ((_target_render!=null && _target_render == render) || pass>15)
        {
            _play_time = 0;
            _target_render = null;
            if(render!=null && render.Data!=null && render.Data.item_info!=null)
            {
                ItemInfo[] reward_data = null;
                if (render.Data.item_info.Type == "buff")
                {
                    reward_data = new ItemInfo[] { new BuffInfo() { ID = int.Parse(render.Data.item_info.RewardId), buff_name=render.Data.item_info.ItemName, icon = render.Data.item_info.Icon } };
                }
                else
                {
                    reward_data = ConfigManager.GetConfig<ConfigReward>().GetLine(render.Data.item_info.RewardId).ItemList;
                }
                if (reward_data != null)
                {
                    //UIManager.PopPanel<PanelRewardItemTip>(new object[] { }, true, this).SetRewardItemList(reward_data);
                   TimerManager.SetTimeOut(0.5f,()=> PanelRewardItemTipsWithEffect.DoShow(reward_data));
                }
            }
            else
            {
                UIManager.ShowTipPanel("请求超时");
            }
        }
    }

    //==========================s to c
    private void Toc_player_attribute(CDict data)
    {
        OnUpdatePlayerData();
    }

    private void Toc_corps_openlottery(toc_corps_openlottery rmo)
    {
        var lottery_data = rmo.list;
        var list = new List<CorpsLotteryItemRender>();
        for (int i = 0; i < item_list.Length; i++)
        {
            var render = item_list[i];
            if (lottery_data != null && i < lottery_data.Length)
            {
                render.gameObject.TrySetActive(true);
                render.SetData(new CDCorpLotteryItem(lottery_data[i]));
                if (render.Data.unlocked)
                {
                    list.Add(render);
                }
            }
            else
            {
                render.gameObject.TrySetActive(false);
            }
        }
        hit_img.gameObject.TrySetActive(false);
        _tween_list = list.ToArray();
    }

    private void Toc_corps_getlotterycounts(toc_corps_getlotterycounts rmo)
    {
        _times_data = rmo;
        m_num.text = "(" + (rmo.total_counts - rmo.cur_counts).ToString() + "次)";
    }

    private void Toc_corps_uselottery(toc_corps_uselottery rmo)
    {
        _target_render = null;
        if (rmo.result != 0)
        {
            _play_time = 0;
            UIManager.ShowTipPanel(rmo.result == 1 ? "抽奖次数不够" : "战队币不足，战队捐献可以获得战队币");
            return;
        }
        var code = rmo.box_id;
        for (int i = 0; i < item_list.Length; i++)
        {
            var render = item_list[i];
            if (render.Data != null && render.Data.info.ID == code)
            {
                _target_render = render;
                break;
            }
        }
        CorpsModel.Instance.TosGetLotteryData();
    }

    private void Toc_corps_buylotterybox(toc_corps_buylotterybox rmo)
    {
        if (rmo.result != 0)
        {
            UIManager.ShowTipPanel(rmo.result == 1 ? "战队等级不足" : "战队币不足，战队捐献可以获得战队币");
            return;
        }
        TipsManager.Instance.showTips("解锁成功");
    }
}

class CorpsLotteryItemRender : ItemRender
{
    private CDCorpLotteryItem _data;

    public Image ItemImg;
    public Text ItemNametxt;
    public Text ItemNumTxt;

    public CDCorpLotteryItem Data
    {
        get { return m_renderData as CDCorpLotteryItem; }
    }

    public override void Awake()
    {
        ItemImg = transform.Find("item_img").GetComponent<Image>();
        ItemNametxt = transform.Find("num_txt").GetComponent<Text>();
        ItemNumTxt = transform.Find("num").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDCorpLotteryItem;
        if (_data == null || _data.info==null)
        {
            return;
        }
        if (_data.item_info!=null)
        {
            if (string.IsNullOrEmpty(_data.item_info.Icon))
            {
                var reward_info = ConfigManager.GetConfig<ConfigReward>().GetLine(_data.item_info.RewardId);
                var titem = reward_info!=null && reward_info.ItemList!=null && reward_info.ItemList.Length>0 ? ItemDataManager.GetItem(reward_info.ItemList[0].ID) : null;
                ItemImg.SetSprite(titem!=null ? ResourceManager.LoadIcon(titem.Icon) : null,titem.SubType);
                if (titem != null && (titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 || titem.SubType == GameConst.WEAPON_SUBTYPE_WEAPON2))
                {
                    ItemImg.transform.localScale = new Vector3(.55f, .55f);
                    ItemImg.transform.eulerAngles = new Vector3(0, 0, -25);
                }
                else
                {
                    ItemImg.transform.localScale = Vector3.one;
                    ItemImg.transform.eulerAngles = Vector3.zero;
                }
            }
            else
            {
                ItemImg.SetSprite(ResourceManager.LoadIcon(_data.item_info.Icon));
                ItemImg.transform.localScale = Vector3.one;
                ItemImg.transform.eulerAngles = Vector3.zero;
            }
            ItemImg.SetNativeSize();
            var itemFullName = _data.item_info.ItemName;
            string nameStr="",numStr = "";
            var xIndex = itemFullName.IndexOf('x');
            if (xIndex != -1)
            {
                nameStr = itemFullName.Substring(0, xIndex);
                numStr = itemFullName.Substring(xIndex);
            }
            else
            {
                var reg = new Regex(@"^([^0-9]*)(.*)$");
                var matches = reg.Match(itemFullName);
                if (matches.Success)
                {
                    nameStr = matches.Groups[1].Value;
                    numStr = matches.Groups[2].Value;
                }
                else
                {
                    Logger.Error("奖励名称有点奇怪,请联系前端");
                }
            }

            ItemNametxt.text = nameStr;
            ItemNumTxt.text = numStr;
            ItemNumTxt.fontSize = 24;
        }
        else
        {
            ItemImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps,"lock"));
            ItemImg.transform.localScale = Vector3.one;
            ItemImg.transform.eulerAngles = Vector3.zero;
            ItemImg.SetNativeSize();
            ItemNametxt.text = "战队" + _data.info.CorpsLevel + "级";
            ItemNumTxt.text = "<color='FFFFFF'>解锁</color>";
            ItemNumTxt.fontSize = 20;
        }
    }
}

class CDCorpLotteryItem
{
    public TDCorpsLotteryBox info;
    public TDCorpsLottery item_info;
    public bool unlocked;

    public CDCorpLotteryItem(p_corps_lotterybox sdata)
    {
        if (sdata != null)
        {
            info = ConfigManager.GetConfig<ConfigCorpsLotteryBox>().GetLine(sdata.box_id);
            item_info = ConfigManager.GetConfig<ConfigCorpsLottery>().GetLine(sdata.lottery_id);
            unlocked = sdata.can_use;
        }
    }
}

