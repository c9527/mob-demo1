﻿using System.Net;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Text;

public class PanelServerList : BasePanel
{
    TDServerListData _data;
//    TDServerListData[] _recent_data;
//    TDServerListData[] _server_data;

    DataGrid _recent_list;
    DataGrid _server_list;

    public PanelServerList()
    {
        SetPanelPrefabPath("UI/Login/PanelServerList");

        AddPreLoadRes("UI/Login/PanelServerList", EResType.UI);
        AddPreLoadRes("UI/Login/ServerListItemRender", EResType.UI);
    }

    public override void Init()
    {
        _recent_list = m_tran.Find("small_board/server_list/content").gameObject.AddComponent<DataGrid>();
        _recent_list.autoSelectFirst = false;
        _recent_list.SetItemRender(ResourceManager.LoadUIRef("UI/Login/ServerListItemRender"), typeof(ServerListItemRender));
        _server_list = m_tran.Find("big_board/server_list/content").gameObject.AddComponent<DataGrid>();
        _server_list.autoSelectFirst = false;
        _server_list.SetItemRender(ResourceManager.LoadUIRef("UI/Login/ServerListItemRender"), typeof(ServerListItemRender));
    }

    public override void InitEvent()
    {
        _recent_list.onItemSelected = OnSelectedServer;
        _server_list.onItemSelected = OnSelectedServer;
        UGUIClickHandler.Get(m_tran.Find("close_btn")).onPointerClick += delegate { HidePanel(); };
    }


    public override void OnShow()
    {
        if (_data == null)
            _data = m_params[0] as TDServerListData;
        Driver.singleton.StartCoroutine(PanelLogin.LoadServerList(UpdateView));
    }

    private void UpdateView(TDServerListData data = null)
    {
        var recentServerList = new List<TDServerListData>();
        var serverList = (TDServerListData[]) GlobalConfig.serverList;
        for (int i = 0; i < GlobalConfig.recentServerIdList.Length; i++)
        {
            var recentServerId = int.Parse(GlobalConfig.recentServerIdList[i]);
            for (int j = 0; j < serverList.Length; j++)
            {
                if (serverList[j].ServerId == recentServerId)
                {
                    recentServerList.Add(serverList[j]);
                    break;
                }
            }
        }

        _recent_list.Data = recentServerList.ToArray();
        _server_list.Data = GlobalConfig.serverList;
        //最近服务器
        bool found = false;
        var render_list = _recent_list.GetComponentsInChildren<ServerListItemRender>();
        for (int i = 0;render_list!=null && i < render_list.Length; i++)
        {
            if (_data != null && (render_list[i].m_renderData is TDServerListData) && ((TDServerListData)render_list[i].m_renderData).ServerId == _data.ServerId)
            {
                found = true;
                render_list[i].GetComponent<Toggle>().isOn = true;
                break;
            }
        }
        if (found == false)
        {
            _recent_list.GetComponent<ToggleGroup>().SetAllTogglesOff();
        }

        //服务器列表
        found = false;
        render_list = _server_list.GetComponentsInChildren<ServerListItemRender>();
        for (int j = 0; render_list != null && j < render_list.Length; j++)
        {
            if (_data != null && (render_list[j].m_renderData is TDServerListData) && ((TDServerListData)render_list[j].m_renderData).ServerId == _data.ServerId)
            {
                found = true;
                render_list[j].GetComponent<Toggle>().isOn = true;
                break;
            }
        }
        if (found == false)
        {
            _server_list.GetComponent<ToggleGroup>().SetAllTogglesOff();
        }
    }

    private void OnSelectedServer(object renderData)
    {
        if (renderData == null)
            return;
        _data = renderData as TDServerListData;
        HidePanel();
        GameDispatcher.Dispatch(GameEvent.UI_LOGIN_SERVER_CHANGE, _data);
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {

    }
}