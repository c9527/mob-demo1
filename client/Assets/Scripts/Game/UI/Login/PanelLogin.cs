﻿using System.Collections;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DockingModule;

public class TDServerListData
{
    public int ServerId;
    public string ServerName;
    public string ShortServerName;
    public string IP;
    public int Port = 43991;
    public int[] PlatformList;  //开放的渠道平台id
    public int Tag;//0畅通，1维护，2拥挤，3爆满
    public int Value;//0普通，1推荐，2火热
    public string ErrorMsg;
}

public class ConfigServerListData : ConfigBase<TDServerListData>
{
    public ConfigServerListData() : base()
    {
    }
}

public class ConfigAddressMappingLine
{
    public string SrcAddress = "";
    public string DestAddress = "";
}

public class ConfigAddressMapping : ConfigBase<ConfigAddressMappingLine>
{
    public ConfigAddressMapping() : base("SrcAddress")
    {
    }
}

public class PanelLogin : BasePanel
{
    private TDServerListData m_selectedData;
    UIEffect _bg_effect1;
    UIEffect _bg_effect2;
    UIEffect _bg_effect3;
    UIEffect _bg_effect4;

    private Image m_imgBg;
    private Image m_imgRole;
    InputField _account_txt;
    Image _status_ico;
    Text _status_txt;
    Text m_txtVersion;
    Text _server_name_txt;
    Button _enter_btn;
    Button _change_server_btn;
    private Image m_imgLogo;
    private static string m_serverListText;
    GameObject m_text1;
    GameObject m_text2;
    Text republicInfo;
    GameObject m_switchuser;
    public PanelLogin()
    {
        SetPanelPrefabPath("UI/Login/PanelLogin");

        AddPreLoadRes("UI/Login/PanelLogin", EResType.UI);
        AddPreLoadRes("Atlas/Login", EResType.Atlas);
    }

    public override void Init()
    {
        if (SdkManager.Enable)
        {
            //SdkManager.LoadStartBeforeLoginLog();
        }
        _enter_btn = m_tran.Find("enter_btn").GetComponent<Button>();
        _change_server_btn = m_tran.Find("change_btn").GetComponent<Button>();
        _account_txt = m_tran.Find("inputAccount").GetComponent<InputField>();
        _account_txt.gameObject.AddComponent<InputFieldFix>();
        _status_ico = m_tran.Find("stat_icon").GetComponent<Image>();
        _status_txt = m_tran.Find("stat_txt").GetComponent<Text>();
        _server_name_txt = m_tran.Find("server_name").GetComponent<Text>();
        m_txtVersion = m_tran.Find("version_txt").GetComponent<Text>();
        m_txtVersion.gameObject.AddComponent<SortingOrderRenderer>();
        m_imgLogo = m_tran.Find("Logo").GetComponent<Image>();
        m_imgBg = m_tran.Find("bg").GetComponent<Image>();
        m_imgRole = m_tran.Find("role").GetComponent<Image>();
        m_text1 = m_tran.Find("Text1").gameObject;
        m_text2 = m_tran.Find("Text2").gameObject;
        Debug.LogWarning("Game Init Ram Info : " + Util.GetMemoryInfo());
        m_text1.AddComponent<SortingOrderRenderer>();
        m_text2.AddComponent<SortingOrderRenderer>();
        republicInfo = m_text2.GetComponent<Text>();
        m_switchuser = m_tran.Find("switch_sdk_btn").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("enter_btn"), null).onPointerClick += OnEnterGameBtnClick;
        UGUIClickHandler.Get(m_tran.Find("change_btn")).onPointerClick += OnChangeSrv;
        UGUIClickHandler.Get(m_switchuser).onPointerClick += OnChangeUser;

        AddEventListener(GameEvent.SDK_CHECK_UPDATE_COMPLETE, CheckUpdateCompleteCallback);
        AddEventListener<TDServerListData>(GameEvent.UI_LOGIN_SERVER_CHANGE, UpdateView);
        AddEventListener(GameEvent.LOGIN_VERIFY, EnterGameHandler);
        AddEventListener(GameEvent.LOGIN_ENABLE_BTN, EnableBtn);
        AddEventListener(GameEvent.LOGIN_PLATFORM_COMPLETE, OnLoginPlatformComplete);

    }




    private void OnLoginPlatformComplete()
    {
        //平台登陆后重新刷新一下serverlist，因为serverlist会根据平台id做过滤
        GlobalConfig.serverList = ReloadServerList();
        UpdateView();
        if (SdkManager.isNewTempUser)
        {
            OnEnterGameBtnClick(null, null);
        }
    }

    private IEnumerator LoadAddressMapping()
    {
        var wwwMap = new WWW(GetAddressMappingUrl());
        yield return wwwMap;
        Debug.Log("映射列表地址：" + wwwMap.url);
        if (String.IsNullOrEmpty(wwwMap.error))
        {
            try
            {
                GlobalConfig.addressMapping.Load(wwwMap.text);
            }
            catch (Exception e)
            {
                Driver.SendLogServer(1103, e.Message);
            }
        }
        else
            Driver.SendLogServer(1103, wwwMap.error);
        wwwMap.Dispose();
    }

    /// <summary>
    /// 加载服务器列表、最近登录服务器id
    /// </summary>
    /// <returns></returns>
    public static IEnumerator LoadServerList(Action<TDServerListData> updateView, Action callBack = null, bool showMask = true)
    {
        if (showMask)
            UIManager.ShowMask("加载服务器列表...");
        var www = new WWW(GetServerListUrl());
        yield return www;
        Debug.Log("服务器列表地址：" + www.url);

        if (showMask)
            UIManager.HideMask();

        if (String.IsNullOrEmpty(www.error))
        {
            //解析资源服务器上的服务器列表
            GlobalConfig.serverList = ReloadServerList(www.text);

            //本地存储的最近登录服务器id
            var recentServerIdList = PlayerPrefs.GetString(RecentServerIdListKey);
            if (!string.IsNullOrEmpty(recentServerIdList))
            {
                GlobalConfig.recentServerIdList = recentServerIdList.Split(',');
            }
            else
            {
                //如果本地没存，则使用服务器列表的最后一个服务器作为上一次登录
                GlobalConfig.recentServerIdList = new string[] { GlobalConfig.serverList[GlobalConfig.serverList.Length - 1].ServerId.ToString() };
            }
            updateView(null);
            if (callBack != null)
                callBack();
        }
        else
        {
            Logger.Error(www.error);
            UIManager.ShowTipPanel("服务器列表加载失败", () => Driver.singleton.StartCoroutine(LoadServerList(updateView)), null, false, false, "重试");
            try
            {
                var host = new Uri(GetServerListUrl()).Host;
                var ips = Dns.GetHostAddresses(host);
                var ipArr = new string[ips.Length];
                for (int j = 0; j < ipArr.Length; j++)
                {
                    ipArr[j] = ips[j].ToString();
                }
                Driver.SendLogServer(1104, string.Format("服务器列表加载失败{0}，域名{1}解析为{2}，{3}", GetServerListUrl(), host, string.Join(";", ipArr), www.error));
            }
            catch (Exception e)
            {
                Driver.SendLogServer(1104, "解析地址{0}出错:\n{1}", GetServerListUrl(), StackTraceUtility.ExtractStringFromException(e));
            }
        }
        www.Dispose();
    }

    private static TDServerListData[] ReloadServerList(string text = null)
    {
        if (text != null)
            m_serverListText = text;
        if (string.IsNullOrEmpty(m_serverListText))
            return new TDServerListData[0];

        var config = ScriptableObject.CreateInstance<ConfigServerListData>();
        config.Load(m_serverListText);
        var list = new List<TDServerListData>();
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            var open = false;
            var platforms = config.m_dataArr[i].PlatformList;
            if (platforms == null || platforms.Length == 0)
                open = true;
            else
            {
                for (int j = 0; j < platforms.Length; j++)
                {
                    if (platforms[j] == SdkManager.PlatformForLogin)
                    {
                        open = true;
                        break;
                    }
                }
            }

            //对应用宝-枪战英雄的服务器列表进行特殊处理，不显示老服，从联运1服开始显示
            var serverid = config.m_dataArr[i].ServerId;
            if (Driver.m_platform == EnumPlatform.android && SdkManager.m_fngid == "1464169648608640" && serverid >= 1000 && serverid < 1200)
                continue;

            if (open)
                list.Add(config.m_dataArr[i]);
        }
        UnityEngine.Object.Destroy(config);
        return list.ToArray();
    }

    public override void OnShow()
    {

        GlobalConfig.ResetConfig();
        TimerManager.Clear();
        PanelHall.m_firstTimeLoad = true;
        PanelSevenDayReward.firstOpen = true;
        GlobalConfig.clientValue.guideChapterName = "";
        //应用宝平台，更换logo
        var spriteName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.LOGO);
        m_imgLogo.enabled = !string.IsNullOrEmpty(spriteName);
        m_imgLogo.SetSprite(ResourceManager.LoadSprite(AtlasName.Login, spriteName));

		UIManager.m_uiCamera.clearFlags = CameraClearFlags.SolidColor;
        if (!String.IsNullOrEmpty(Global.currentVersion))
            m_txtVersion.text = "当前版本：" + Global.currentVersion;
        if (SdkManager.Enable)
            _account_txt.gameObject.TrySetActive(false);
        else
            _account_txt.text = PlayerPrefs.GetString(AccountKey);

//      SdkManager.CheckUpdate();
//        SdkManager.BeforLogin();
        if (SdkManager.Enable && !SdkManager.IsSdkLoginSuccess())
        {
            if (Driver.m_platform == EnumPlatform.pc)
                UIManager.ShowTipPanel("请退出游戏重新登录账号\nSDK:" + SdkManager.Enable, Application.Quit, null, false, false, "退出");
            SdkManager.Login();
        }
		if(Driver.m_platform == EnumPlatform.ios && GlobalConfig.serverValue.localNotification_open)
		{
			if(Driver.singleton != null)
				Driver.singleton.gameObject.AddComponent<iOSNotificationManager>();
		}
        AudioManager.PlayBgMusic("Hall_1");
        if (_bg_effect1 == null)
            _bg_effect1 = UIEffect.ShowEffectAfter(m_imgRole.rectTransform, EffectConst.UI_LOGIN_HOU, true);
        if (_bg_effect2 == null)
            _bg_effect2 = UIEffect.ShowEffectBefore(m_imgRole.rectTransform, EffectConst.UI_LOGIN_QIAN, true);
        if (_bg_effect3 == null)
            _bg_effect3 = UIEffect.ShowEffect(m_imgRole.rectTransform, EffectConst.UI_LOGIN_QIANG, new Vector3(-62, -227, -20));
//        if (_bg_effect4 == null)
//            _bg_effect4 = UIEffect.ShowEffect(m_tran.Find("kuang"), EffectConst.UI_LOGIN_XIAN, new Vector3(-253, -25));

        Driver.singleton.StartCoroutine(LoadAddressMapping());
        Driver.singleton.StartCoroutine(LoadServerList(UpdateView));

        republicInfo.text = Util.TransStrInLine(ConfigManager.GetConfig<ConfigPlatformContent>().GetContent("ShowRepublicInfo"));
        if (SdkManager.SdkName == "RusSDK")
        {
            m_switchuser.TrySetActive(true);
        }
        else if (SdkManager.SdkName == "4399iosEnSdk" || SdkManager.SdkName == "4399iosRuSdk")
        {
            m_switchuser.TrySetActive(true);
        }
        else
        {
            m_switchuser.TrySetActive(false);
        }


        UpdateView();
        if (SdkManager.Enable)
        {
            SdkManager.LoadEndBeforeLoginLog();
        }
    }

    private TDServerListData GetLastLoginServerConfig()
    {
        var configs = (TDServerListData[])GlobalConfig.serverList;
        if (configs==null || configs.Length==0)
            return null;
        //增加支持对外部传入的server_id进行优先选择
        TDServerListData result = null;
        if (!String.IsNullOrEmpty(SdkManager.m_platform_server_id) || (GlobalConfig.recentServerIdList != null && GlobalConfig.recentServerIdList.Length > 0))
        {
            var plat_form_server_id = SdkManager.ConvertPlatformServerID();
            if (Driver.m_platform == EnumPlatform.pc)
                plat_form_server_id = 0;    //pc不使用传入的服务器参数
            var recent_server_id = GlobalConfig.recentServerIdList!=null && GlobalConfig.recentServerIdList.Length>0 ? Int32.Parse(GlobalConfig.recentServerIdList[0]) : 0;
            for (int i = 0; i < configs.Length; i++)
            {
                var info = configs[i];
                if (info.ServerId == plat_form_server_id)
                {
                    result = info;
                    break;
                }
            }
            if (result == null)
            {
                for (int i = 0; i < configs.Length; i++)
                {
                    var info = configs[i];
                    if (info.ServerId == recent_server_id)
                    {
                        result = info;
                        break;
                    }
                }
            }
        }
        return result ?? configs[configs.Length - 1];
    }

    private void UpdateCurrentServerConfig(ref TDServerListData config)
    {
        if (GlobalConfig.serverList == null || GlobalConfig.serverList.Length == 0)
            return ;
        var configs = (TDServerListData[])GlobalConfig.serverList;
        for (int i = 0; i < configs.Length; i++)
        {
            if (configs[i].ServerId == config.ServerId)
            {
                config = configs[i];
                return ;
            }
        }
    }

    private void UpdateView(TDServerListData data = null)
    {
        if (GlobalConfig.serverList == null || GlobalConfig.serverList.Length == 0)
            return;
        //显示当前选择的服务器状态
        if (data != null)
            m_selectedData = data;
        else if (m_selectedData == null)
            m_selectedData = GetLastLoginServerConfig();

        //若当前选择的并不在serverlist中，则重新选择
        var hasServer = false;
        for (int i = 0; i < GlobalConfig.serverList.Length; i++)
        {
            if (GlobalConfig.serverList[i].ServerId == m_selectedData.ServerId)
            {
                hasServer = true;
                break;
            }
        }
        if (!hasServer)
            m_selectedData = GetLastLoginServerConfig();

        UpdateCurrentServerConfig(ref m_selectedData);
        UpdateSelectServerView(m_selectedData);
    }

    private void UpdateSelectServerView(TDServerListData config)
    {
        if (config != null && _status_ico != null&&UIManager.IsOpen<PanelLogin>())
        {
            _server_name_txt.text = config.ServerName;
            if (config.Tag == 1)
            {
                _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "wh"));
                _status_txt.text = "维护";
            }
            else if (config.Tag == 2)
            {
                _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "yj"));
                _status_txt.text = "拥挤";
            }
            else if (config.Tag == 3)
            {
                _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "bm"));
                _status_txt.text = "爆满";
            }
            else
            {
                _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "ct"));
                _status_txt.text = "畅通";
            }
        }
        else
        {
            _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "wh"));
            _status_txt.text = "维护";
            _server_name_txt.text = "未选择服务器";
        }
    }

    private void CheckUpdateCompleteCallback()
    {
//        SdkManager.Login();
    }

    public override void OnHide()
    {
        if (_bg_effect1 != null)
        {
            _bg_effect1.Destroy();
            _bg_effect1 = null;
        }
        if (_bg_effect2 != null)
        {
            _bg_effect2.Destroy();
            _bg_effect2 = null;
        }
        if (_bg_effect3 != null)
        {
            _bg_effect3.Destroy();
            _bg_effect3 = null;
        }
        if (_bg_effect4 != null)
        {
            _bg_effect4.Destroy();
            _bg_effect4 = null;
        }
		UIManager.m_uiCamera.clearFlags = CameraClearFlags.Depth;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        Driver.singleton.StopCoroutine("LoadServerList");
        ResourceManager.ClearAtlas("Login");
		UIManager.m_uiCamera.clearFlags = CameraClearFlags.Depth;
    }

    public override void Update()
    {
        if (m_imgBg)
        {
            m_imgBg.rectTransform.localScale = Vector3.one*(1.05f + 0.02f*Mathf.Sin(Time.time*0.5f));
            m_imgBg.rectTransform.anchoredPosition = new Vector2(6f * Mathf.Sin(Time.time+Mathf.PI*0.5f), 0);
        }

        if (m_imgRole)
        {
//            m_imgRole.rectTransform.localScale = Vector3.one * (1.005f + 0.02f * Mathf.Sin(Time.time));
            m_imgRole.rectTransform.anchoredPosition = new Vector2(365 + 6f * 0.5f * Mathf.Sin(Time.time + Mathf.PI * 0.5f), 0);
        }
    }

    void OnEnterGameBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_enter_btn.interactable)
        {
            if (SdkManager.Enable)
            {
                SdkManager.ClickEnterGameLog();
            }
            UIEffect.ShowEffect(_enter_btn.GetComponent<Transform>(), EffectConst.UI_LOGIN_BTN_EFFECT, 1, Vector3.zero);
            AudioManager.PlayUISound(AudioConst.btnLogin);
            TimerManager.SetTimeOut(0.8f, EnterGameHandler);
            _enter_btn.interactable = false;
            TimerManager.RemoveTimeOut(EnableBtn);
            TimerManager.SetTimeOut(10, EnableBtn);
        }
    }

    void EnableBtn()
    {
        if (UIManager.HasPanel<PanelLogin>()) 
            _enter_btn.interactable = true;
    }

    void EnterGameHandler()
    {
        Driver.singleton.StartCoroutine(LoadServerList(UpdateView, Login, false));
    }

    void Login()
    {
        if (!SdkManager.Enable)
        {
            //PlayerPrefs.DeleteKey(AccountKey);
            SdkManager.m_userName = PlayerPrefs.GetString(AccountKey);
            if (_account_txt.text != "")
            {
                SdkManager.m_userName = _account_txt.text;
            }
            if (String.IsNullOrEmpty(SdkManager.m_userName))
            {
                SdkManager.m_userName = Guid.NewGuid().ToString();
            }
        }

        var config = m_selectedData;
        if (config == null || String.IsNullOrEmpty(config.IP))
        {
            UIManager.ShowTipPanel("服务器地址为空，请尝试重新选择服务器", () => Driver.singleton.StartCoroutine(LoadServerList(UpdateView)));
            _enter_btn.interactable = true;
            return;
        }
        if (SdkManager.Enable && !SdkManager.IsSdkLoginSuccess())
        {
            if (Driver.m_platform == EnumPlatform.pc)
                UIManager.ShowTipPanel("请退出游戏重新登录账号\nSDK:" + SdkManager.Enable, Application.Quit, null, false, false, "退出");
            Logger.Error(SdkManager.m_ext+"开始登录了");
            SdkManager.Login();
            _enter_btn.interactable = true;
            return;
        }

        //优先判断是否已配置关闭，同步连接Socket
        var result = config.Tag == 1 ? 4 : ConnectToServer.ConnectServer(config);
        if (result == 0)
        {
            SdkManager.m_serverId = config.ServerId.ToString();
            SdkManager.m_serverName = config.ServerName;
            //异步验证账号
            ConnectToServer.LoginVerify();
            PlayerPrefs.SetString(AccountKey, SdkManager.m_userName);
            var list = new List<string>();
            var recentServerIdList = PlayerPrefs.GetString(RecentServerIdListKey);
            if (!String.IsNullOrEmpty(recentServerIdList))
            {
                var arr = recentServerIdList.Split(',');
                list = new List<string>(arr);
            }
            //登录成功后，把服务器id写到最近登录列表的首位
            list.Remove(config.ServerId.ToString());
            list.Insert(0, config.ServerId.ToString());
            GlobalConfig.recentServerIdList = list.ToArray();
            PlayerPrefs.SetString(RecentServerIdListKey, String.Join(",", GlobalConfig.recentServerIdList));
            PlayerPrefs.Save();
        }
        else if (result == 1 || result == 5)   //服务器主动断开
        {
            if (result == 1)
                Driver.SendLogServer(1106, "响应包超时或未收到 Host:{0} Port:{1}", config.IP, config.Port);
            else if (result == 5)
                Driver.SendLogServer(1106, "响应包被拒绝 Host:{0} Port:{1}", config.IP, config.Port);
            Driver.singleton.ShowQueueMask();
            _enter_btn.interactable = true;
        }
        else if (result == 4)   //主动关服
        {
            UIManager.ShowTipPanel(String.IsNullOrEmpty(config.ErrorMsg) ? "服务器维护中" : config.ErrorMsg);
            _enter_btn.interactable = true;
            if (Driver.singleton.m_showQueueMask)
            {
                Driver.singleton.m_showQueueMask = false;
                UIManager.HideMask();
            }
        }
        else
        {
            
            _enter_btn.interactable = true;
            if (Driver.singleton.m_showQueueMask)
            {
                Driver.singleton.m_showQueueMask = false;
                UIManager.HideMask();
            }

            if (result == 2) //网络连接失败
            {
                UIManager.ShowTipPanel("连接失败，请检查网络，重新登录");
                if (NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.SocketConnectFail)
                    Driver.SendLogServer(1105, "创建Socket失败 Host:{0} Port:{1}", config.IP, config.Port);
                else if (NetLayer.LastConnectErrorType == NetLayer.ConnectErrorType.SocketTimeout)
                    Driver.SendLogServer(1105, "连接Socket超时 Host:{0} Port:{1}", config.IP, config.Port);
            }
            else if (result == 3)
                UIManager.ShowTipPanel("服务器地址为空，请尝试重新选择服务器",
                    () => Driver.singleton.StartCoroutine(LoadServerList(UpdateView)));
            else
                UIManager.ShowTipPanel(String.IsNullOrEmpty(config.ErrorMsg) ? "连接服务器失败" : config.ErrorMsg);
        }
    }

    void OnChangeSrv( GameObject go,BaseEventData eventData )
    {
        UIManager.PopPanel<PanelServerList>(new object[] { m_selectedData });
    }

    void OnChangeUser(GameObject go, BaseEventData eventData)
    {
        if(SdkManager.SdkName == "RusSDK")
        {
            SdkManager.SwitchUser();
        }
        else
        {
            SdkManager.SwitchUserAccount();
        }
        
    }

    /// <summary>
    /// 获取服务器列表地址
    /// </summary>
    /// <returns></returns>
    public static string GetServerListUrl()
    {
        var serverlistUrl = Global.gameDefineServerlistUrl;

        if (Global.useDebugServerlistUrl)
            serverlistUrl += "Test";
        else
        {
            if (Driver.m_hasInnerServerlist)
                return UpdateWorker.AppRootPathWww + "serverlist";//使用包内serverlist

            Dictionary<string, string> versionCheckServerlistDic = null;
            if (Global.Dic.ContainsKey("VersionCheckServerlistDic"))
                versionCheckServerlistDic = Global.Dic["VersionCheckServerlistDic"] as Dictionary<string, string>;

            if (Global.appVersion.CompareTo(Global.currentVersion) > 0)
            {
                if (versionCheckServerlistDic != null && versionCheckServerlistDic.ContainsKey(Global.appVersion) && !string.IsNullOrEmpty(versionCheckServerlistDic[Global.appVersion]))
                    serverlistUrl += versionCheckServerlistDic[Global.appVersion];
                else
                    serverlistUrl += "Check";
            }
            else
            {
                Dictionary<string, string> versionCheckDic = null;

                if (Global.Dic.ContainsKey("VersionCheckDic"))
                    versionCheckDic = Global.Dic["VersionCheckDic"] as Dictionary<string, string>;
                if (versionCheckDic != null && versionCheckDic.ContainsKey(Global.appVersion))
                {
                    if (versionCheckServerlistDic != null && versionCheckServerlistDic.ContainsKey(Global.appVersion) && !string.IsNullOrEmpty(versionCheckServerlistDic[Global.appVersion]))
                        serverlistUrl += versionCheckServerlistDic[Global.appVersion];
                    else
                        serverlistUrl += "Check";
                }
            }
        }

        if (Application.isEditor)
            serverlistUrl = "file:////172.16.10.158/zjqz/serverlistInner";

        serverlistUrl += "?id=" + (DateTime.Now.Ticks / 10000000);
        return serverlistUrl;
    }

    /// <summary>
    /// 获取映射列表的地址
    /// </summary>
    /// <returns></returns>
    public static string GetAddressMappingUrl()
    {
        var url = ResourceManager.UpdateUrl + "addressMapping";

        if (Application.isEditor)
            url = "file:////172.16.10.158/zjqz/addressMapping";

        url += "?id=" + (DateTime.Now.Ticks/10000000);
        return url;
    }

    public static string GetVersionCheckListUrl()
    {
        var url = ResourceManager.UpdateUrl + "versionCheckList";

        url += "?id=" + (DateTime.Now.Ticks / 10000000);
        return url;
    }

    public static string AccountKey
    {
        get
        {
            if (Application.isEditor)
                return "account_" + Directory.GetCurrentDirectory();
            return "account";
        }
    }

    public static string RecentServerIdListKey
    {
        get
        {
            if (Application.isEditor)
                return "recentServerIdList_" + Directory.GetCurrentDirectory();
            return "recentServerIdList";
        }
    }
}