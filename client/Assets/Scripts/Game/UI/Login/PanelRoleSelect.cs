﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if UNITY_WEBPLAYER
using DockingWeb;
using DockingModule;
#endif

public class PanelRoleSelect : BasePanel
{
    private GameObject m_goEnter;
    private GameObject m_goInput;
    private GameObject m_randomName;
    private Transform m_man;
    private Transform m_woman;
    private int m_selectedSex = 0;
    private ConfigRoleName m_configRoleName;
    public PanelRoleSelect()
    {
        SetPanelPrefabPath("UI/Login/PanelRoleSelect");
        AddPreLoadRes("UI/Login/PanelRoleSelect", EResType.UI);
        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
    }

    public override void Init()
    {
        m_configRoleName = ConfigManager.GetConfig<ConfigRoleName>();
        m_man = m_tran.FindChild("man");
        m_woman = m_tran.FindChild("woman");
        m_goEnter = m_tran.FindChild("enter").gameObject;
        m_goInput = m_tran.FindChild("namewhite/InputField").gameObject;
        m_goInput.AddComponent<InputFieldFix>().onEndEdit += OnSubmit;
        m_randomName = m_tran.FindChild("RandomName").gameObject;
        InputField field = m_goInput.GetComponent<InputField>();
        field.keyboardType = TouchScreenKeyboardType.Default;
    }

    private void OnSubmit(string text)
    {
        //fjs:1102  手机上输入完成后，不立即创建角色，需等待玩家点击进入游戏才创建角色
        //OnEnterGame(null, null);
    }

    public override void InitEvent()
    {
//        UGUIClickHandler.Get(m_tran.Find("man/btn")).onPointerClick += OnClickSex;
        UGUIClickHandler.Get(m_tran.Find("man/role")).onPointerClick += OnClickSex;
//        UGUIClickHandler.Get(m_tran.Find("woman/btn")).onPointerClick += OnClickSex;
        UGUIClickHandler.Get(m_tran.Find("woman/role")).onPointerClick += OnClickSex;
        UGUIClickHandler.Get(m_goEnter).onPointerClick += OnEnterGame;
        UGUIClickHandler.Get(m_randomName).onPointerClick += RandomName;
    }

    private void OnClickSex(GameObject target, PointerEventData eventdata)
    {
        var isMan = target.transform.parent == m_man;

        m_selectedSex = isMan ? 0 : 1;
        TweenScale.Begin(m_man.gameObject, 0.2f, isMan ? Vector3.one : new Vector3(0.9f, 0.9f, 1));
        TweenScale.Begin(m_woman.gameObject, 0.2f, isMan ? new Vector3(0.9f, 0.9f, 1) : Vector3.one);

        var manColor = isMan ? Color.white : (Color)new Color32(100, 100, 100, 128);
        var womanColor = isMan ? (Color)new Color32(100, 100, 100, 128) : Color.white;
//        m_tran.Find("man/btn").GetComponent<Image>().color = manColor;
        m_tran.Find("man/role").GetComponent<Image>().color = manColor;
//        m_tran.Find("man/shadow").GetComponent<Image>().color = manColor;
//        m_tran.Find("woman/btn").GetComponent<Image>().color = womanColor;
        m_tran.Find("woman/role").GetComponent<Image>().color = womanColor;
//        m_tran.Find("woman/shadow").GetComponent<Image>().color = womanColor;
        
    }

    public override void OnShow()
    {
        //RandomNameFunc();
        NetLayer.Send(new tos_login_random_name() { sex = 0 });       
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
    
    void OnEnterGame(GameObject go,BaseEventData eventData)
    {
        string roleName = m_goInput.GetComponent<InputField>().textComponent.text;
        NetLayer.Send(new tos_login_create() { name = roleName, sex = m_selectedSex });
        m_goInput.GetComponent<InputField>().textComponent.text = "";
//        if(SdkManager.Enable && Driver.m_platform==EnumPlatform.web)
//        {
//            Driver.SendLogServer(1008, (!string.IsNullOrEmpty(SdkManager.m_userName) ? SdkManager.m_userName : "unknow") + ": web创建角色");
//        }
//        if (SdkManager.Enable && Driver.m_platform == EnumPlatform.pc)
//        {
//            Driver.SendLogServer(1009, (!string.IsNullOrEmpty(SdkManager.m_userName) ? SdkManager.m_userName : "unknow") + ": pc创建角色");
//        }
    }

    void OnReturn( GameObject go,BaseEventData eventData )
    {
        UIManager.ShowPanel<PanelLogin>();
    }

    void RandomName(GameObject go, BaseEventData eventData)
    {
        //RandomNameFunc();
        NetLayer.Send(new tos_login_random_name() { sex = m_selectedSex});
    }

    void RandomNameFunc()
    {
        string firstName = "";
        while (string.IsNullOrEmpty(firstName))
        {
            firstName = m_configRoleName.m_dataArr[Random.Range(0, m_configRoleName.m_dataArr.Length)].firstName;
        }
        string secondName = "";
        while (string.IsNullOrEmpty(secondName))
        {
            secondName = m_configRoleName.m_dataArr[Random.Range(0, m_configRoleName.m_dataArr.Length)].secondName;
        }
        string thirdName = "";
        while (string.IsNullOrEmpty(thirdName))
        {
            thirdName = m_configRoleName.m_dataArr[Random.Range(0, m_configRoleName.m_dataArr.Length)].thirdName;
        }
        string username = firstName + secondName + thirdName;
        m_goInput.GetComponent<InputField>().text = username;
    }

    void Toc_login_random_name(toc_login_random_name data)
    {
        m_goInput.GetComponent<InputField>().text = data.name;
    }
}