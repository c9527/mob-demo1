﻿using System;
using UnityEngine;
#if UNITY_WEBPLAYER
using DockingWeb;
#endif
using DockingModule;

public static class ConnectToServer
{
    private static bool m_LoginVerifySuccess; //登陆验证成功
    private static int m_reconnectCount = 0;

    public static bool LoginVerifySuccess{get { return m_LoginVerifySuccess; }}

    /// <summary>
    /// 连接服务器Socket
    /// </summary>
    /// <param name="serverData"></param>
    /// <returns></returns>
    public static int ConnectServer(TDServerListData serverData)
    {
        var server = serverData.IP;
        var port = serverData.Port;

        m_LoginVerifySuccess = false;
        Logger.Log(string.Format("尝试连接服务器[{0}:{1}]", server, port));

        if (string.IsNullOrEmpty(server))
            return 3;

        var result = NetLayer.Connect(server, port);
        if (result == 2)
        {
            //连续连接失败两次，则启用转发服务
            m_reconnectCount++;
            if (m_reconnectCount >= 2)
                NetLayer.EnableForwarding = true;
        }
        else
            m_reconnectCount = 0;
        return result;
    }

    /// <summary>
    /// 验证账号，若返回Toc_login_verify则说明验证成功
    /// </summary>
    /// <returns></returns>
    public static void LoginVerify()
    {
        Logger.Log(
            string.Format(
                "登录验证：[{0}({1})],platform:{2},plat_uid:{3},time:{4},platform server:{5},cm:{6},flag:{7},login server: {8}, fngid:{9}, fnpid:{10}, platformId:{11}",
                SdkManager.m_userName, SdkManager.m_uid, SdkManager.PlatformForLogin, SdkManager.m_uid, SdkManager.time,
                SdkManager.m_platform_server_id, SdkManager.cm, SdkManager.flag, SdkManager.m_serverId, SdkManager.m_fngid, SdkManager.m_pid, SdkManager.m_platform_id));
        string sdkp = SdkManager.Enable ? "fn" : "none";
		int platformp = 0;
        platformp = SdkManager.PlatformForLogin;
        if (SdkManager.SdkName == "RusSDK")
        {
            sdkp = "russdk";
			platformp = SdkManager.PlatformForLogin;
        }
        if (SdkManager.SdkName == "EnSDK")
        {
            sdkp = "ensdk";
        }
		if (Driver.m_platform == EnumPlatform.ios) 
		{
			if (SdkManager.SdkName == "4399iosRuSdk")
			{
				sdkp = "iosRuSdk";
				platformp = 0;
			}
			else if (SdkManager.SdkName == "4399iosEnSdk")
			{
				sdkp = "iosEnSdk";
				platformp = 0;
			}
			else
			{
				sdkp = SdkManager.Enable ? "ios" : "none";
				platformp = SdkManager.PlatformForLogin;
			}				
		}

        NetLayer.Send(new tos_login_verify()
        {
            platform = platformp,
            account = SdkManager.m_userName,
#if UNITY_WEBPLAYER
            sdkplat = SdkManager.Enable ? "4399web" : "none",
#elif UNITY_STANDALONE
            sdkplat = SdkManager.Enable ? "4399pc" : "none",
#else
            sdkplat = sdkp,
#endif
            plat_uid = SdkManager.m_uid,
            ext_info = SdkManager.m_ext,
            other_info = GetOtherInfo(),

        });
    }

    /// <summary>
    /// 验证成功
    /// </summary>
    /// <param name="proto"></param>
    private static void Toc_login_verify(toc_login_verify proto)
    {
        m_LoginVerifySuccess = true;
        if (Driver.singleton.m_showQueueMask)
        {
            Driver.singleton.m_showQueueMask = false;
            UIManager.HideMask();
        }

        SdkManager.m_uid = proto.uid;
        SdkManager.m_oauthInfo = proto.oauth_info;
        SdkManager.LogLoginFinish(proto.uid);
        SdkManager.SetOauthInfo(proto.oauth_info);

        if (proto.list.Length > 0)
        {
            var role = proto.list[0];
            Logger.Log("验证成功，选择角色：" + role.name+"，id="+role.id);
            NetLayer.Send(new tos_login_select() { id = role.id });
            SdkManager.LogSelectServer(role.level.ToString(), role.name, SdkManager.m_serverId);
        }
        else
        {
            Logger.Log("验证成功，准备创建角色");
            SdkManager.LogSelectServer("0", "", SdkManager.m_serverId);
            UIManager.ShowPanel<PanelRoleSelect>();
        }
        GameDispatcher.Dispatch(GameEvent.LOGIN_ENABLE_BTN);
    }

    private static void Toc_login_create(toc_login_create proto)
    {
        Logger.Log("创建角色成功，并选择：" + proto.name + "，id=" + proto.id);
        if (Driver.m_platform == EnumPlatform.web || Driver.m_platform == EnumPlatform.pc)
            DockingService.SendCreateRoleLog(proto.name);
        NetLayer.Send(new tos_login_select() { id = proto.id });
        string roleName = proto.name;
        SdkManager.LogCreateRole(proto.id.ToString(), proto.name, SdkManager.m_serverId, SdkManager.m_serverName);
        PlayerPrefs.SetString("role", roleName);
        PlayerPrefs.Save();
        //SdkManager.RoleLoginlog(proto.name);
    }

    private static void Toc_login_select(toc_login_select proto)
    {
        SdkManager.RoleLoginlog(proto.data.name);
        var time = TimeUtil.GetNowTimeStamp();
        TimeUtil.updateTime(proto.times + 1 - time);    // 加1秒，取整误差
        SdkManager.InitBugly(SdkManager.PlatformForLogin.ToString(), Global.currentVersion, string.Format("{0}({1})", proto.data.name, proto.id));
        Logger.Log("【{0}】登入游戏，服务端时间({1})：{2}，客户端时间：{3}，时间修正：{4}=>{5}", proto.data.name, proto.times, TimeUtil.GetTime(proto.times), TimeUtil.GetTime(time), time, TimeUtil.GetNowTime());
        SdkManager.TP2OnUserLogin(PlatformAPI.TP2_ENTRYID_OTHERS, 1, proto.id.ToString(), "");
        //SdkManager.TP2SDKSetPopupOptions(PlatformAPI.SUSPICIOUS_PROCESS | PlatformAPI.SUSPICIOUS_MODULE | PlatformAPI.TAMPERED_INSTRUCTION | PlatformAPI.MUTABLE_SPEED);
        GameDispatcher.Dispatch(GameEvent.MAIN_PLAYER_JOIN, proto);
        GameDispatcher.Dispatch(GameEvent.LOGIN_SUCCESS);
        if (Driver.m_platform == EnumPlatform.web || Driver.m_platform == EnumPlatform.pc)
        {
            DockingService.SendSelectServerLog();
            DockingService.SendShareEnterLog();
        }
		if (Driver.m_platform == EnumPlatform.ios)
		{
			SdkManager.IOSShareLogin();
		}
        Driver.singleton.SetStatus(string.Format("acc:{0} id:{1} name:{2} ver:{3}", SdkManager.m_userName, proto.id, proto.data.name, Global.currentVersion));
        
    }

    private static CDict GetOtherInfo()
    {
        var info = new CDict();
        info["login_server"] = SdkManager.m_serverId;
		if (SdkManager.SdkName == "4399iosRuSdk" || SdkManager.SdkName == "4399iosEnSdk")
		{
            info["time"] = SdkManager.timestamp;
        }
        else
        {
            info["time"] = SdkManager.time;
        }
        info["server"] = !string.IsNullOrEmpty(SdkManager.m_platform_server_id) ? SdkManager.m_platform_server_id : SdkManager.m_serverId;
        info["cm"] = SdkManager.cm;
        info["flag"] = SdkManager.flag ?? "";
        info["device_id"] = SystemInfo.deviceUniqueIdentifier ?? "";
        info["pack_ver"] = Global.appVersion ?? "";
        info["version"] = Global.currentVersion ?? "";
        string[] strs = SystemInfo.operatingSystem.Split(' ');
        if (strs.Length > 2)
        {
            info["os"] = strs[0].Replace("iPhone", "IOS");
            info["os_version"] = strs[2];
        }
        info["device_name"] = SystemInfo.deviceModel ?? "";
        info["screen"] = Screen.width + "*" + Screen.height;
        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            info["network"] = "WIFI";
        }
        else
        {
            info["network"] = "CarrierData";
        }
        info["push_cid"] = SdkManager.m_getuiClientId ?? "";
        info["game_id"] = SdkManager.m_fngid ?? "";
		if (Driver.m_platform == EnumPlatform.ios)
		{
			info["client_id"] = SdkManager.m_getClientId;
			info["device_token"] = SdkManager.m_getDeviceToken;
		}

        //专门给平台记日志用的
        switch (Driver.m_platform)
        {
            case EnumPlatform.android:info["device"] = "android";break;
            case EnumPlatform.ios:info["device"] = "ios";break;
            case EnumPlatform.web:info["device"] = "web";break;
            case EnumPlatform.pc:info["device"] = "pc";break;
            case EnumPlatform.other:info["device"] = "ios";break;
            default:throw new ArgumentOutOfRangeException();
        }

        if (Application.isEditor)
        {
            info["device_type"] = "Editor";
        }
        else
        {
            switch (Driver.m_platform)
            {
                case EnumPlatform.android: info["device_type"] = "Android"; break;
                case EnumPlatform.ios:
                    if (SystemInfo.deviceModel.Contains("iPhone"))
                        info["device_type"] = "iPhone";
                    else if (SystemInfo.deviceModel.Contains("iPad"))
                        info["device_type"] = "iPad";
                    else if (SystemInfo.deviceModel.Contains("iPod"))
                        info["device_type"] = "iPod";
                    else if (SystemInfo.deviceModel.Contains("iTouch"))
                        info["device_type"] = "iTouch";
                    else
                        info["device_type"] = "Other IOS";
                    break;
                case EnumPlatform.web: info["device_type"] = "WebPlayer"; break;
                case EnumPlatform.pc: info["device_type"] = "PC"; break;
                case EnumPlatform.other: info["device"] = "ios"; break;
                default: throw new ArgumentOutOfRangeException();
            }
        }
        return info;
    }
}
