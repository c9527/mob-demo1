﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Text;

public class PanelLoadingWithProBar : BasePanel {

    private Text m_content;
    private bool m_isLoading = false;
    private static int m_loadingCount = 0;
    /// <summary>
    /// 预加载时进度条最大值
    /// </summary>
    private const float m_loadingLimit = 0.6f;
    private const float m_loadingLimitMax = 0.95f;

    /// <summary>
    /// loading开始时间
    /// </summary>
    private float m_startTime;
    private Action m_callBack;
    private RectTransform m_sliderBar;
    private RectTransform m_fg;
    private Image m_bg;
    private float m_value;    
    public float sliderValue
    {
        get
        {
            return m_value;
        }
        set
        {
            if (value > 1.0f)
                value = 1.0f;
            if (value < 0.00001f)
                value = 0f;
            Vector3 pos = m_fg.localPosition;
            pos.x = m_sliderBar.sizeDelta.x * (value-1);
            m_fg.localPosition = pos;
            m_value = value;
        }
    }
    private Text m_cm_txt;
     
    public PanelLoadingWithProBar()
    {
        m_isCreateRayCaster = false;
        SetPanelPrefabPath("UI/Common/PanelLoadingWithProBar");
        AddPreLoadRes("UI/Common/PanelLoadingWithProBar", EResType.UI);
        AddPreLoadRes("Atlas/Loading", EResType.Atlas);
        AddPreLoadRes("Atlas/Login", EResType.Atlas);
        //AddPreLoadRes("Effect/UI_jindutiao_huoyan", EResType.UIEffect);
    }

    public override void Init()
    {
        m_content = m_tran.Find("tips").GetComponent<Text>();
        m_sliderBar = m_tran.Find("SliderBar").GetComponent<RectTransform>();
        m_fg = m_sliderBar.Find("fg").GetComponent<RectTransform>();
        m_bg = m_tran.Find("bg").GetComponent<Image>();
        m_cm_txt = m_tran.Find("cm_txt").GetComponent<Text>();
        m_cm_txt.gameObject.TrySetActive(Application.isWebPlayer);
        //UIEffect effect = UIEffect.ShowEffect(m_fg, "UI_jindutiao_huoyan", new Vector3(455, 0, 0));
        //effect.Play();
    }

    public override void InitEvent()
    {
    }


    public override void OnShow()
    {
        if (!HasParams())
            return;
        ConfigTipsLine config=ConfigTips.GetRandomTips();
        if(config==null)
        {
            Logger.Log("loading图配置未找到");
            config = ConfigManager.GetConfig<ConfigTips>().m_dataArr[0];
        }
        if (SdkManager.m_pid==166) //应用宝平台
        {
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Loading,GetIconByPlatform(config.Icon2)));
        }
        else
        {
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Loading, GetIconByPlatform(config.Icon1)));
        }        
        sliderValue = 0f;
        m_isLoading = true;
        m_startTime = Time.realtimeSinceStartup;        
        m_callBack = (Action) m_params[1];
        string[] list = config.Content.Split('#');
        int mapid = SceneManager.singleton.curMapId;
        var configs = ConfigManager.GetConfig<ConfigMapList>();
        ConfigMapListLine mapLine = null;
        if (configs != null)
        {
            mapLine = configs.GetLine(mapid.ToString());
        }
        StringBuilder sb = new StringBuilder();
        string mapName = mapLine == null ? "" : ("[<color=#00ff00>" + mapLine.MapName + "</color>]");
        sb.Append(mapName);
        int index = new System.Random().Next(0,list.Length);
        m_content.text = sb.Append(list[index]).ToString(); 
        m_loadingCount++;

        Logger.RecordTime("预加载资源");
        ResourceManager.LoadMulti((ResourceItem[])m_params[0], null, () =>
        {
            m_loadingCount--;
            if (m_loadingCount <= 0)
            {
                m_isLoading = false;
                Logger.PrintTime("预加载资源");
            }
            //if (callBack != null)
            //    callBack();
        });
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (m_isLoading)
        {
            if (Driver.m_platform == EnumPlatform.web || Driver.m_platform == EnumPlatform.pc)
            {
                //考虑PC版和WEB版 需要网络加载资源  将加载的进度条显示速度推慢一倍
                if (sliderValue < m_loadingLimit)
                {
                    sliderValue = (Time.realtimeSinceStartup - m_startTime) * 0.2f;
                }
                else if (sliderValue < m_loadingLimitMax)
                {
                    sliderValue = m_loadingLimit + (Time.realtimeSinceStartup - m_startTime - m_loadingLimit / 0.4f) * 0.02f;
                }
            }
            else
            {
                if (sliderValue < m_loadingLimit)
                {
                    sliderValue = (Time.realtimeSinceStartup - m_startTime) * 0.4f;
                }
                else if (sliderValue < m_loadingLimitMax)
                {
                    sliderValue = m_loadingLimit + (Time.realtimeSinceStartup - m_startTime - m_loadingLimit / 0.4f) * 0.04f;
                }
            }
        }
        else
        {
            if (sliderValue < 1.0f)
            {
                sliderValue += 0.03f;
            }
            else
            {
                if (m_callBack != null)
                {
                    m_callBack();
                    m_callBack = null;
                }
                HidePanel();
            }
        }
    }

    private string GetIconByPlatform(string[] strs)
    {
        if(strs==null||strs.Length!=2)
        {
           Logger.Error("配置错误");
            return "";
        }
        string str = "";
        string[] list;
        int index = 0;
        if (Driver.isMobilePlatform)
        {
            list = strs[1].Split('#');
            index = new System.Random().Next(0, list.Length);
            str = list[index];
            return str;
        }
        else
        {
            list = strs[0].Split('#');
            index = new System.Random().Next(0, list.Length);
            str = list[index];
            return str;
        }
    }
}
