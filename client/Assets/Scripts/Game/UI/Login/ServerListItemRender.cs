﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class ServerListItemRender : ItemRender
{
    TDServerListData _data;

    Text _name_txt;
    Image _status_ico;
    Image _value_ico;

    public override void Awake()
    {
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _status_ico = transform.Find("status_ico").GetComponent<Image>();
        _value_ico = transform.Find("value_ico").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as TDServerListData;
        if (_data == null)
        {
            return;
        }
        _name_txt.text = _data.ServerName;
        if (_data.Tag == 1)
        {
            _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "wh"));
        }
        else if (_data.Tag == 2)
        {
            _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "yj"));
        }
        else if (_data.Tag == 3)
        {
            _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "bm"));
        }
        else
        {
            _status_ico.SetSprite(ResourceManager.LoadSprite("Login", "ct"));
        }
        if (_data.Value>0)
        {
            _value_ico.gameObject.TrySetActive(true);
            _value_ico.SetSprite(ResourceManager.LoadSprite("Login", _data.Value == 1 ? "recommend" : "hot"));
            _value_ico.SetNativeSize();
        }
        else
        {
            _value_ico.gameObject.TrySetActive(false);
        }
    }
}

