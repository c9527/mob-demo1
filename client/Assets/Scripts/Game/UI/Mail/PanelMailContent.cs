﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;


class PanelMailContent:BasePanel
{
    private Button m_receive;
    private Button m_cancel;
    private Text m_tip;
    private Text m_content;
    private MotionText m_richtext;
    private Text m_time;
    private DataGrid m_items;
    private int m_id;
    toc_player_mail_list.mail m_mail;
    bool m_had;

    public PanelMailContent()
    {
        SetPanelPrefabPath("UI/Mail/PanelMailContent");
        AddPreLoadRes("UI/Mail/PanelMailContent", EResType.UI);
        AddPreLoadRes("UI/Mail/ItemGift", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_id = 0;
        m_receive=m_tran.Find("receive").GetComponent<Button>();
        m_tip=m_tran.Find("MailText").GetComponent<Text>();
        m_cancel = m_tran.Find("cancel").GetComponent<Button>();
        m_content = m_tran.Find("contentScorll/content").GetComponent<Text>();
        m_richtext = m_content.gameObject.AddComponent<MotionText>();
        m_time = m_tran.Find("time").GetComponent<Text>();
        m_items = m_tran.Find("ItemGrid/content").gameObject.AddComponent<DataGrid>();
        //var mail = ResourceManager.LoadUI("UI/Mail/ItemGift").AddComponent<ItemGift>();
        m_items.SetItemRender(ResourceManager.LoadUIRef("UI/Mail/ItemGift"), typeof(ItemGift));
        m_had = true;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_receive.gameObject).onPointerClick += ReceieveMail;
        UGUIClickHandler.Get(m_cancel.gameObject).onPointerClick += CancelBack;
    }

    public void ReceieveMail(GameObject target, PointerEventData eventData)
    {
        bool hasEquip = false;
        ConfigItemLine item;
        for (int i = 0; i < m_mail.attachment.Length; i++)
        {
            ConfigItemLine itemline = ItemDataManager.GetItem(m_mail.attachment[i].item_id);
            if (itemline.Type == "EQUIP")
            {
                if (ItemDataManager.GetDepotItem(itemline.ID)==null) 
                {
                    m_had = itemline.SubType == "BULLET";
                    hasEquip = true;
                    item = itemline;
                }
            }
        }
        NetLayer.Send(new tos_player_read_mail() {mail_id=m_id });
        NetLayer.Send(new tos_player_mail_list() {});
        if (!hasEquip)
        {
            this.HidePanel();
        }
    }


    void Toc_item_new(toc_item_new data)
    {
        //var item = data.item;
        //if (!m_had)
        //{
        //    //PanelChooseGroup.DoShow(new object[] { item.uid }, true);
        //    ItemInfo info = new ItemInfo();
        //    info.ID = data.item.item_id;
        //    info.cnt = data.item.item_cnt;
        //    info.day = ((int)data.item.time_limit) / (3600 * 24);
        //    PanelRewardItemTipsWithEffect.DoShow(new ItemInfo[] { info }, true);
        //}
        this.HidePanel();
    }

    public void CancelBack(GameObject target, PointerEventData eventData)
    {
        this.HidePanel();
    }

    public override void OnShow()
    {
        if (HasParams() && m_params[0] is toc_player_mail_list.mail)
        {
            UpdateView(m_params[0] as toc_player_mail_list.mail);
        }
    }

    public override void OnDestroy()
    {

    }

    public override void OnBack()
    {
       
    }

    public override void OnHide()
    {

    }

    public override void Update()
    {

    }

    public void UpdateView(toc_player_mail_list.mail mail)
    {
        m_tip.text = mail.title;
        m_content.text = Regex.Replace(mail.content, @"\\?&([a-zA-Z]+?|#\d+?);", new MatchEvaluator(DecodeHtml));
        TimerManager.SetTimeOut(0.011f, ()=>m_richtext.Create(NetChatData.HandleLinkAction, RightManager.USER_SYSTEM));
        //TimeSpan ts= TimeUtil.GetPassTime(mail.create_time);
        //if (ts.Days >= 30)
        //{
        //    m_time.text = "一个月之前";
        //}
        //else if (ts.Days >= 7)
        //{
        //    m_time.text = "一周之前";
        //}
        //else if (ts.Days >= 1)
        //{
        //    m_time.text = ts.Days.ToString()+ "天之前";
        //}
        //else if (ts.Hours >= 1)
        //{
        //    m_time.text = ts.Hours.ToString() + "小时之前";
        //}
        //else if (ts.Minutes >= 3)
        //{
        //    m_time.text = ts.Minutes.ToString() + "分钟之前";
        //}
        //else
        //{
        //    m_time.text = "刚刚";
        //}
        m_time.text = mail.from_name;
        m_items.Data = mail.attachment;
        if (m_items.Data.Length == 0)
        {
            m_receive.transform.Find("Text").GetComponent<Text>().text = "删除";
            m_cancel.transform.Find("Text").GetComponent<Text>().text = "关闭";
        }
        else
        {
            m_receive.transform.Find("Text").GetComponent<Text>().text = "收取";
            m_cancel.transform.Find("Text").GetComponent<Text>().text = "取消";
        }
        m_id = mail.mail_id;
        m_mail = mail;
    }

    private string DecodeHtml(Match match)
    {
        var result = match.Value;
        var key = match.Groups[1].Value;
        if (key == "nbsp" || key=="#160")
        {
            result = " ";
        }
        else if (key == "amp" || key == "#38")
        {
            result = "&";
        }
        else if (key == "lt" || key == "#60")
        {
            result = "<";
        }
        else if (key == "gt" || key == "#62")
        {
            result = ">";
        }
        return result;
    }
}

