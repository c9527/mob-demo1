﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class ItemMail:ItemRender
{
    private Image m_gift;
    private Text m_sender;
    private Text m_tip;
    private Text m_time;
    private int m_id;

    public override void Awake()
    {
        var tran = transform;
        m_gift = tran.Find("gift").GetComponent<Image>();
        m_sender = tran.Find("Sender").GetComponent<Text>();
        m_time = tran.Find("Time").GetComponent<Text>();
        m_tip = tran.Find("tip").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        var info = data as toc_player_mail_list.mail;
        m_sender.text = info.from_name;
        m_tip.text = info.title;
        m_id = info.mail_id;
        if(info.attachment.Length == 0)
        {
            m_gift.gameObject.TrySetActive(false);
        }
        TimeSpan ts = TimeUtil.GetPassTime(info.create_time);
        if (ts.Days >= 30)
        {
            m_time.text = "一个月前";
        }
        else if (ts.Days >= 7)
        {
            m_time.text = "一周前";
        }
        else if (ts.Days >= 1)
        {
            m_time.text = ts.Days.ToString() + "天前";
        }
        else if (ts.Hours >= 1)
        {
            m_time.text = ts.Hours.ToString() + "小时前";
        }
        else if (ts.Minutes >= 3)
        {
            m_time.text = ts.Minutes.ToString() + "分钟前";
        }
        else
        {
            m_time.text = "刚刚";
        }
        //for (int i = 0; i < info.attachment.Length; i++)
        //{
        //   ConfigItemLine gift = ItemDataManager.GetItem(info.attachment[i].item_id);
        //   if (gift.SubType == "WEAPON1" || gift.SubType == "WEAPON2" || gift.SubType == "DAGGER")
        //   {
        //       m_gift.SetSprite(ResourceManager.LoadSprite("Mail", "weapon"));
        //       return;
        //   }
        //   if (gift.SubType == "ARMOR")
        //   {
        //       m_gift.SetSprite(ResourceManager.LoadSprite("Mail", "armor"));
        //       return;
        //   }
        //   if (gift.SubType == "Role")
        //   {
        //       m_gift.SetSprite(ResourceManager.LoadSprite("Mail", "role"));
        //       return;
        //   }

        //}
    }


    public void SetGift(Sprite gift)
    {
        m_gift.SetSprite(gift);
    }

    public void SetTip(string tip)
    {
        m_tip.text = tip;
    }

    public void SetSender(string sendername)
    {
        m_sender.text = sendername;
    }

    public void SetTime(string time)
    {
        m_time.text = time;
    }

}

