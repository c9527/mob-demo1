﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

class ItemGift:ItemRender
{
    Image m_gift;
    Text m_count;
    Image m_imgItemIcon;
    Text m_txtNum;
    Image m_imgItemIconEx;
    Text m_txtNumEx;
    Text m_txtName;
    Image m_bound;

    public override void Awake()
    {
        var tran = transform;
        m_imgItemIcon = tran.Find("image").GetComponent<Image>();
        m_txtNum = tran.Find("num").GetComponent<Text>();
        m_imgItemIconEx = tran.Find("imageEx").GetComponent<Image>();
        m_txtNumEx = tran.Find("numEx").GetComponent<Text>();
        m_txtName = tran.Find("name").GetComponent<Text>();
        m_bound = tran.Find("bound").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var info = data as toc_player_mail_list.attachment;
        ConfigItemLine itemLine = ItemDataManager.GetItem(info.item_id);
        Sprite icon = ResourceManager.LoadIcon(itemLine.Icon);
        if (itemLine.Type != "EQUIP")
        {
            m_imgItemIcon.gameObject.TrySetActive(true);
            m_txtNum.gameObject.TrySetActive(true);
            m_txtName.gameObject.TrySetActive(true);
            m_imgItemIcon.SetSprite(icon,itemLine.SubType);
            m_imgItemIcon.SetNativeSize();
            m_txtNum.text = "x" + info.item_cnt.ToString();
            m_txtName.text = itemLine.DispName;
            m_imgItemIconEx.gameObject.TrySetActive(false);
            m_txtNumEx.gameObject.TrySetActive(false);
            m_bound.gameObject.TrySetActive(true);
        }
        else
        {
            m_txtNumEx.gameObject.TrySetActive(true);
            m_imgItemIconEx.gameObject.TrySetActive(true);
            m_imgItemIconEx.SetSprite(icon);
            m_imgItemIconEx.SetNativeSize();
            m_txtNumEx.text = string.Format("{0}x{1}", itemLine.DispName, info.item_cnt.ToString());
            m_imgItemIcon.gameObject.TrySetActive(false);
            m_txtNum.gameObject.TrySetActive(false);
            m_bound.gameObject.TrySetActive(false);
            m_txtName.gameObject.TrySetActive(false);
        }

    }

}

