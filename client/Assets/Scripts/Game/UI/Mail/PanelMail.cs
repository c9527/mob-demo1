﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


class PanelMail:BasePanel
{
    private Button m_oneKey;
    private DataGrid m_mailgrid;
    private int m_mailNum;
    public static toc_player_mail_list m_list;
    public int m_time;

    public PanelMail()
    {
        SetPanelPrefabPath("UI/Mail/PanelMail");
        AddPreLoadRes("UI/Mail/PanelMail", EResType.UI);
        AddPreLoadRes("UI/Mail/ItemMail", EResType.UI);
        
        AddPreLoadRes("Atlas/Mail", EResType.Atlas);
    }

    public override void Init()
    {
        m_time = 0;
        m_oneKey = m_tran.Find("deleteall").GetComponent<Button>();
        m_mailgrid = m_tran.Find("MailGrid/content").gameObject.AddComponent<DataGrid>();
        // var mail = ResourceManager.LoadUI("UI/Mail/ItemMail").AddComponent<ItemMail>();
        m_mailgrid.SetItemRender(ResourceManager.LoadUIRef("UI/Mail/ItemMail"), typeof(ItemMail));
        m_mailgrid.autoSelectFirst = false;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_oneKey.gameObject).onPointerClick += OneKeyRecieve;
        m_mailgrid.onItemSelected += OnMailSelected;
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel(); };
    }

    private void OnMailSelected(object renderData)
    {
        if (renderData != null)
        {
            var mail = renderData as toc_player_mail_list.mail;

            UIManager.PopPanel<PanelMailContent>(new object[]{mail});
        }
    }
    public override void OnShow()
    {
        if (this.IsInited())
        {
            m_mailgrid.Data = m_list.list;
        }
    }

    public override void OnDestroy()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnHide()
    {
    }

    public override void Update()
    {
    }

    public void OneKeyRecieve(GameObject target, PointerEventData eventData)
    {
        if(m_mailgrid.Data.Length == 0)
        {
            TipsManager.Instance.showTips("没有邮件可以收取");
            return;
        }
        UIManager.ShowTipPanel("确定要收取所有邮件吗？", () =>
        {
            NetLayer.Send(new tos_player_read_all_mail() { });
            m_mailgrid.Data = new object[0];
            BubbleManager.SetBubble(BubbleConst.MailUnread, false);
            TipsManager.Instance.showTips("邮件已全部收取");
        }, null, true, true);
    }


    public void SetMailnum(int num){
        m_mailNum = num;
    }

    public int GetMailnum()
    {

        return m_mailNum;
    }

    public void ShowMail()
    {
        
    }

    static void Toc_player_mail_list(toc_player_mail_list data)
    {
        Array.Sort(data.list, (a, b) => b.create_time - a.create_time);
        m_list = data;
        BubbleManager.SetBubble(BubbleConst.MailUnread, data.list.Length != 0);
        if (UIManager.IsOpen<PanelMail>())
        {
            UIManager.GetPanel<PanelMail>().OnShow();
        }
    }

    public void DelMail(int id)
    {
        for (int i = 0; i < m_mailgrid.Data.Length; i++)
        {
            var mail = m_mailgrid.Data[i] as toc_player_mail_list.mail;
            if (mail.mail_id == id)
            {
                m_mailgrid.Data = m_mailgrid.Data.ArrayDelete<object>(i);
                BubbleManager.SetBubble(BubbleConst.MailUnread, m_mailgrid.Data.Length != 0);
                return;
            }
        }
    }
    static void Toc_player_del_mail(toc_player_del_mail data)
    {
        UIManager.GetPanel<PanelMail>().DelMail(data.mail_id);
    }





}

