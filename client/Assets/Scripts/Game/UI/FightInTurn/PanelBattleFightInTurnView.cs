﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelBattleFightInTurnView : BasePanel
{
    private const int NUM = 5;
    private PlayerListInfo leftPlayerListInfo = new PlayerListInfo();
    private PlayerListInfo rightPlayerListInfo = new PlayerListInfo();

    private FighterInfo leftFighterInfo = new FighterInfo();
    private FighterInfo rightFighterInfo = new FighterInfo();

    public PanelBattleFightInTurnView()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleFightInTurnView");

        AddPreLoadRes("UI/Battle/PanelBattleFightInTurnView", EResType.UI);
        AddPreLoadRes("Atlas/FightInTurn", EResType.Atlas);
    }

    public override void Init()
    {
        InitPlayerListInfo(m_tran.Find("Left"), leftPlayerListInfo);
        InitPlayerListInfo(m_tran.Find("Right"), rightPlayerListInfo);
        InitFighterInfo(m_tran.Find("Top/PlayerLeft"), leftFighterInfo);
        InitFighterInfo(m_tran.Find("Top/PlayerRight"), rightFighterInfo);
    }

    private void InitFighterInfo(Transform transform, FighterInfo info)
    {
        if (transform == null || info == null) return;
        info.name = transform.Find("Name").GetComponent<Text>();
        info.weapon = transform.Find("Weapon").GetComponent<Text>();
        info.hp = transform.Find("HpProcess/num").GetComponent<Text>();
        info.head = transform.Find("Head/Icon").GetComponent<Image>();
        info.hpPercent = transform.Find("HpProcess/up").GetComponent<Image>();
    }

    private void InitPlayerListInfo(Transform transform, PlayerListInfo info)
    {
        if (transform == null || info == null) return;
        for (int i = 0; i < NUM; i++)
        {
            Transform trans = transform.Find(string.Concat("PlayerList/Player", i));
            Text text0 = trans.Find("NameText").GetComponent<Text>();
            Image img = trans.Find("HpProcess/up").GetComponent<Image>();
            GameObject goLight = trans.Find("LightIcon").gameObject;
            GameObject goX = trans.Find("DeadIcon").gameObject;
            trans.gameObject.TrySetActive(false);
            info.XList.Add(goX);
            info.lightList.Add(goLight);
            info.itemList.Add(trans.gameObject);
            info.nameTextList.Add(text0);
            info.processList.Add(img);
            info.idList.Add(0);
        }
    }

    public override void InitEvent()
    {

    }

    public override void OnShow()
    {
        OnUpdate(FightInTurnBehavior.singleton.beginData);
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (FightInTurnBehavior.singleton == null) return;
        UpdatePlayerHp(leftPlayerListInfo, FightInTurnBehavior.singleton.leftIndex);
        UpdatePlayerHp(rightPlayerListInfo, FightInTurnBehavior.singleton.rightIndex);
        UpdateFighterInfo(leftFighterInfo, FightInTurnBehavior.singleton.leftFighterId);
        UpdateFighterInfo(rightFighterInfo, FightInTurnBehavior.singleton.rightFighterId);
    }

    private void UpdatePlayerHp(PlayerListInfo info, int selectedIndex)
    {
        if (info == null || selectedIndex < 0) return;
        int length = info.idList.Count;
        for (int i = 0; i < length; i++)
        {
            int id = info.idList[i];
            BasePlayer player = WorldManager.singleton.GetPlayerById(id);
            if (player == null || player.serverData == null) continue;
            info.processList[i].fillAmount = i < selectedIndex ? 0f : i > selectedIndex ? 1 : (player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f);
        }
    }

    public void OnUpdate(toc_fight_arena_begin_info data)
    {
        if (!IsInited() || data == null) return;
        UpdatePlayerInfo(leftPlayerListInfo, data.a_info, data.a_index-1);
        UpdatePlayerInfo(rightPlayerListInfo, data.b_info, data.b_index-1);
    }

    private void UpdatePlayerInfo(PlayerListInfo info, p_arena_player_info[] list, int selectedIndex)
    {
        if (info == null || list == null) return;
        int length = list.Length;
        int j = 0;
        for (int i = 0; i < length; i++)
        {
            int id = list[i].actor_id;
            BasePlayer player = WorldManager.singleton.GetPlayerById(id);
            if (player == null || player.serverData == null) continue;
            info.itemList[j].TrySetActive(true);
            info.idList[j] = id;
            info.XList[j].TrySetActive(i < selectedIndex);
            Util.SetGoGrayShader(info.itemList[j], i < selectedIndex);
            info.lightList[j].TrySetActive(i == selectedIndex);
            info.nameTextList[j].text = player.PlayerName;
            info.processList[j].fillAmount = i < selectedIndex ? 0f : i > selectedIndex ? 1 : (player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f);
            j++;
        }
        for (int k = j; k < NUM; k++)
        {
            info.itemList[k].TrySetActive(false);
        }
    }

    private void UpdateFighterInfo(FighterInfo info, int playerId)
    {
        if (info == null) return;
        BasePlayer player = WorldManager.singleton.GetPlayerById(playerId);
        if (player == null || player.serverData == null) return;
        info.name.text = player.PlayerName;
        info.weapon.text = player.CurWeaponDisName;
        info.hp.text = player.CurHp.ToString();
        info.hpPercent.fillAmount = player.MaxHp > 0 ? (float)player.CurHp / (float)player.MaxHp : 0f;
        info.head.SetSprite(ResourceManager.LoadRoleIcon(player.serverData.icon, player.Camp));
        info.head.rectTransform.sizeDelta = new Vector2(38, 38);
    }

    public class FighterInfo
    {
        public Text hp;
        public Text name;
        public Image head;
        public Text weapon;
        public Image hpPercent;
    }

    class PlayerListInfo
    {
        public List<int> idList = new List<int>(NUM);
        public List<GameObject> itemList = new List<GameObject>(NUM);
        public List<GameObject> XList = new List<GameObject>(NUM);
        public List<GameObject> lightList = new List<GameObject>(NUM);
        public List<Text> nameTextList = new List<Text>(NUM);
        public List<Image> processList = new List<Image>(NUM);
    }
}
