﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelBattleFightInTurnBegin : BasePanel
{
    private const int NUM = 5;
    private PlayerListInfo leftPlayerListInfo = new PlayerListInfo();
    private PlayerListInfo rightPlayerListInfo = new PlayerListInfo();

    public PanelBattleFightInTurnBegin()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleFightInTurnBegin");

        AddPreLoadRes("UI/Battle/PanelBattleFightInTurnBegin", EResType.UI);
        AddPreLoadRes("Atlas/FightInTurn", EResType.Atlas);
    }

    public override void Init()
    {
        EnableMouseEvent = false;
        InitPlayerListInfo(m_tran.Find("Left"), leftPlayerListInfo);
        InitPlayerListInfo(m_tran.Find("Right"), rightPlayerListInfo);
    }

    private void InitPlayerListInfo(Transform transform, PlayerListInfo info)
    {
        if (transform == null || info == null) return;
        for (int i = 0; i < NUM; i++)
        {
            Transform trans = transform.Find(string.Concat("PlayerList/Player", i, "/Info"));
            Text text0 = trans.Find("NameText").GetComponent<Text>();
            Image img = trans.Find("Head/Icon").GetComponent<Image>();
            GameObject goLight = trans.Find("LightIcon").gameObject;
            trans.gameObject.TrySetActive(false);
            info.headList.Add(img);
            info.lightList.Add(goLight);
            info.itemList.Add(trans.gameObject);
            info.nameTextList.Add(text0);
            info.idList.Add(0);
        }
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        OnUpdate(FightInTurnBehavior.singleton.beginData);
    }

    public override void OnHide()
    {
        
    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
    }

    void HideAllItem()
    {
        for (int i = 0; i < NUM; i++)
        {
            //itemList[i].TrySetActive(false);
        }
    }

    public override void Update()
    {
    }

    public void OnUpdate(toc_fight_arena_begin_info data)
    {
        if (!IsInited() || data == null) return;
        UpdatePlayerInfo(leftPlayerListInfo, data.a_info, data.a_index-1);
        UpdatePlayerInfo(rightPlayerListInfo, data.b_info, data.b_index-1);
    }

    private void UpdatePlayerInfo(PlayerListInfo info, p_arena_player_info[] list, int selectedIndex)
    {
        if (info == null || list == null) return;
        int length = list.Length;
        int j = 0;
        for (int i = 0; i < length; i++)
        {
            int id = list[i].actor_id;
            BasePlayer player = WorldManager.singleton.GetPlayerById(id);
            if (player == null || player.serverData == null) continue;
            info.itemList[j].TrySetActive(true);
            info.idList[j] = id;
            Util.SetGoGrayShader(info.itemList[j], i < selectedIndex);
            info.lightList[j].TrySetActive(i == selectedIndex);
            info.nameTextList[j].text = player.PlayerName;
            info.headList[j].SetSprite(ResourceManager.LoadRoleIcon(player.serverData.icon, player.Camp));
            info.headList[j].rectTransform.sizeDelta = new Vector2(38, 38);
            j++;
        }
        for (int k = j; k < NUM; k++)
        {
            info.itemList[k].TrySetActive(false);
        }
    }

    class PlayerListInfo
    {
        public List<int> idList = new List<int>(NUM);
        public List<GameObject> itemList = new List<GameObject>(NUM);
        public List<GameObject> lightList = new List<GameObject>(NUM);
        public List<Text> nameTextList = new List<Text>(NUM);
        public List<Image> headList = new List<Image>(NUM);
    }
}
