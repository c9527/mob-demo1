﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MiniTipsData
{
    /// <summary>
    /// 标题
    /// </summary>
    public string name;
    /// <summary>
    /// 描述
    /// </summary>
    public string dec;

    public object[] props;

}

public class MiniTipsManager : MonoBehaviour
{
    public delegate MiniTipsData MiniTipsMsg(GameObject go);
    /// <summary>
    /// 提示文字
    /// </summary>
    public MiniTipsMsg miniTipsDataFunc;
    public void initEvent()
    {
        UGUIUpDownHandler.Get(gameObject).onPointerDown += onPointerDownHandler;
        UGUIUpDownHandler.Get(gameObject).onPointerUp += onPointerUpHandler;
    }

    public void RemoveListener()
    {
        UGUIUpDownHandler.Get(gameObject).onPointerDown -= onPointerDownHandler;
        UGUIUpDownHandler.Get(gameObject).onPointerUp -= onPointerUpHandler;
    }
    private void onPointerDownHandler(GameObject target, PointerEventData eventData)
    {
        if (miniTipsDataFunc != null)
        {
            Vector3 vec3=UIManager.m_uiCamera.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 0));
            UIManager.ShowMiniTipPanel(miniTipsDataFunc(target), vec3);
        }
    }
    private void onPointerUpHandler(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowMiniTipPanel();
    }
   
    public static MiniTipsManager get(GameObject go)
    {
        MiniTipsManager script = go.GetComponent<MiniTipsManager>();
        if (script == null)
        {
            script = go.AddComponent<MiniTipsManager>();
            script.initEvent();
        }
        return script;
    }

    public static void Remove(GameObject go)
    {
        MiniTipsManager script = go.GetComponent<MiniTipsManager>();
        if (script != null)
        {
            script.RemoveListener();
            Destroy(script);
        }
    }
}