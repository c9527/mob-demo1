﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemInviteAI : MonoBehaviour
{
    public int m_id;
    public int m_camp;
    private Image m_imgBg;
    //GameObject m_goLvl;
    private Image m_imgArmy;
    private Image m_imgHead;
    private Image m_imgHeadBack;
    public Text m_txtName;
    private Text m_txtLv;
    private Image m_imgOK;
    private GameObject m_goOk;
    private GameObject m_goHead;
    // Use this for initialization
    private void Awake()
    {
        var tran = transform;
        m_imgBg = tran.Find("imgBackground").GetComponent<Image>();
        m_txtLv = tran.Find("imgLv/Text").GetComponent<Text>();
        m_imgArmy = tran.Find("imgArmy").GetComponent<Image>();
        m_imgHead = tran.Find("imgBackground/Image").GetComponent<Image>();
        m_goHead = tran.Find("imgBackground/Image").gameObject;
        m_imgHeadBack = tran.Find("imgBackground/headback").GetComponent<Image>();
        m_txtName = tran.Find("imgBackground/txtName").GetComponent<Text>();
        m_imgOK = tran.Find("imgOK").GetComponent<Image>();
        m_goOk = tran.Find("imgOK").gameObject;
    }

    // Update is called once per frame
    public void SetCamp(int camp)
    {
//        m_camp = camp;
//        if (m_camp == 2)
//        {
//            m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "blue"));
//        }
//        else
//        {
//            m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "red"));
//        }
    }

    public void SetNormalArmy()
    {
        //m_imgArmy.gameObject.TrySetActive(false);
        //transform.Find("imgLv").gameObject.TrySetActive(false);
        m_txtLv.text = "1级";
        m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(1));
        m_imgArmy.SetNativeSize();
    }

    public void SetAI(int lvl, string name)
    {
        m_txtLv.text = String.Format("{0}级",  lvl);
        m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(lvl));
        m_imgArmy.SetNativeSize();
        m_txtName.text = name;
        m_goOk.TrySetActive(true);
        if (m_camp == 2)
        {
            m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(2));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "bluehead"));
        }
        else
        {
            m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(6));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "redhead"));
        }
        m_imgHead.SetNativeSize();
        m_imgHeadBack.SetNativeSize();
    }

    public bool IsReady
    {
        set { m_imgOK.enabled = value; }
        get { return m_imgOK.enabled; }
    }
}