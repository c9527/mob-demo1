﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


class PanelMatchRoom : PanelRoomBase
{
     public PanelMatchRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.NormalRoommate, 
            RoomFunc.FightState, 
            RoomFunc.Viewing, 
            RoomFunc.Chat,
            RoomFunc.Password, 
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.ChangeVsIcon,
            RoomFunc.SmallMap
        });

        SetPanelPrefabPath("UI/Room/PanelMatchRoom");
        AddPreLoadRes("UI/Room/PanelMatchRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

     public override void Init()
     {
         base.Init();
     }

     public override void InitEvent()
     {
         base.InitEvent();
     }

     public override void OnShow()
     {
         base.OnShow();
     }

     public override void OnHide()
     {
         base.OnHide();
     }

     public override void OnBack()
     {
         base.OnBack();
     }

     public override void OnDestroy()
     {
         base.OnDestroy();
     }

     public override void Update()
     {
         base.Update();
     }
}

