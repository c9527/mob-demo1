﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/1/21 14:31:01
 * Description:
 * Update History:
 *
 **************************************************************************/

public class PanelZombieInfo : BasePanel
{
    public class ZombieInfoRewardItem : ItemRender
    {
        private GameObject m_equipList;
        private GameObject m_itemList;
        public override void Awake()
        {
            m_equipList = transform.Find("equip_list").gameObject;
            m_itemList = transform.Find("item_list").gameObject;
        }
        protected override void OnSetData(object data)
        {
            if (data == null)
            {
                gameObject.TrySetActive(false);
                return;
            }
            string rewardStr = data as string;
            ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(rewardStr);
            if (rewardLine != null && rewardLine.ItemList != null && rewardLine.ItemList.Length != 0)
            {

                var info = ItemDataManager.GetItem(rewardLine.ItemList[0].ID);
                if (info.Type == GameConst.ITEM_TYPE_EQUIP)
                {
                    m_equipList.TrySetActive(true);
                    m_itemList.TrySetActive(false);
                    var cfg = ItemDataManager.GetItem(rewardLine.ItemList[0].ID);
                    m_equipList.transform.FindChild("img_equip").GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(rewardLine.ItemList[0].ID),cfg!=null?cfg.SubType:"");
                    m_equipList.transform.FindChild("img_equip").GetComponent<Image>().SetNativeSize();
                }
                else
                {
                    m_equipList.TrySetActive(false);
                    m_itemList.TrySetActive(true);
                    int lngth = rewardLine.ItemList.Length;
                    GameObject tempGo;
                    for (int i = 0; i < 1; ++i)
                    {
                        tempGo = m_itemList.transform.FindChild("Icon" + (i + 1)).gameObject;
                        if (i < lngth)
                        {
                            var cfg = ItemDataManager.GetItem(rewardLine.ItemList[i].ID);
                            tempGo.GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(rewardLine.ItemList[i].ID),cfg!=null?cfg.SubType:"");
                           tempGo.GetComponent<Image>().SetNativeSize();
                            tempGo.transform.Find("Text").GetComponent<Text>().text = "X"+rewardLine.ItemList[i].cnt;
                            tempGo.TrySetActive(true);
                        }else
                            tempGo.TrySetActive(false);
                    }
                }
            }
        }
    }

    private Image m_mapIcon;
    private Text m_mapDec;
    private GameObject m_rewardGo;
    private GameObject m_btnGo;
    private ConfigStageSurvivalLine m_cfgData;
    private GameObject m_tipsGo;
    public PanelZombieInfo()
    {
        // 构造器
        SetPanelPrefabPath("UI/Room/PanelZombieInfo");
        AddPreLoadRes("UI/Room/PanelZombieInfo", EResType.UI);
        AddPreLoadRes("Atlas/PVE", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
    }
    public override void Init()
    {
        m_mapIcon = m_tran.Find("FrameInfo/Map/MapIcon").GetComponent<Image>();
        m_mapDec = m_tran.Find("FrameInfo/Map/NameTxt").GetComponent<Text>();
        m_rewardGo = m_tran.Find("FrameInfo/Reward").gameObject;
        m_btnGo = m_tran.Find("FrameInfo/Btn_go").gameObject;
        m_tipsGo = m_tran.Find("FrameInfo/TextTip").gameObject;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnGo).onPointerClick += onClickBtnGoHandler;
    }
    private void onClickBtnGoHandler(GameObject target, PointerEventData eventData)
    {
        if (m_cfgData.StageId <= PanelZombiePass.stageId + 1)
        {
            PanelZombieRoom.stageId = m_cfgData.StageId;
            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.stage_survival), stage = m_cfgData.StageId });
        }else
            TipsManager.Instance.showTips("该关卡尚未解锁！");
    }

    public override void OnShow()
    {
        if (m_params != null && m_params.Length > 0)
        {
            UpdateView(m_params[0]);
        }
    }

    public void UpdateView(object obj)
    {
        m_cfgData = obj as ConfigStageSurvivalLine;
        if (m_cfgData == null)
        {
            return;
        }
        m_mapIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.SmallMap, m_cfgData.MapId.ToString()));
        m_mapDec.text = m_cfgData.RoomDec;
        int lngth = m_cfgData.ConDec.Length;
        for (int i = 0; i < lngth; i++)
        {
            m_tran.Find("FrameInfo/AimInfo" + (i + 1) + "/Text").GetComponent<Text>().text = m_cfgData.ConDec[i];
        }
        lngth = m_cfgData.StarReward.Length;
        GameObject tempGo;
        ZombieInfoRewardItem rewardItem;
        for (int k = 0; k < lngth; k++)
        {
            tempGo = m_rewardGo.transform.Find("Item" + (k + 1)).gameObject;
            rewardItem = tempGo.GetComponent<ZombieInfoRewardItem>();
            if (rewardItem == null)
            {
                rewardItem = tempGo.AddComponent<ZombieInfoRewardItem>();
            }
            rewardItem.SetData(m_cfgData.StarReward[k]);
        }
        if (m_cfgData.StageId <= PanelZombiePass.stageId + 1)
        {
            // Util.SetGoGrayShader(m_btnGo,false);
            m_btnGo.TrySetActive(true);
            m_tipsGo.TrySetActive(false);
        }
        else
        {
            m_btnGo.TrySetActive(false);
            m_tipsGo.TrySetActive(true);
            // Util.SetGoGrayShader(m_btnGo, true);
        }
    }

    public override void OnHide() { }
    public override void OnBack() { }
    public override void OnDestroy() { }
    public override void Update() { }
}
