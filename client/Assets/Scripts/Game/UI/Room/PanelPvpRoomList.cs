﻿class PanelPvpRoomList : PanelRoomListBase
{
    public PanelPvpRoomList() : base(ChannelTypeEnum.pvp)
    {
        SetPanelPrefabPath("UI/Room/PanelPvpRoomList");
        AddPreLoadRes("UI/Room/PanelPvpRoomList", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
    }

    public override void Init()
    {
        m_enableSmallMap = true;
        m_enableViewer = true;
        m_enableAutoRefresh = true;
        m_enableFilterRule = true;
        m_enableChannelList = true;
        base.Init();
        
        m_dataGrid.SetItemRender(m_tran.Find("pageframe/list/content/item").gameObject, typeof(ItemPvpRoomRender));
    }

    

    public override void InitEvent()
    {
        base.InitEvent();
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();

        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }
}