﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;


class PanelLimitSelect:BasePanel
{
    public PanelLimitSelect()
    {
        SetPanelPrefabPath("UI/Room/PanelLimitSelect");
        AddPreLoadRes("UI/Room/PanelLimitSelect", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    DataGrid m_datagrid;
    GameObject m_sureBtn;
    GameObject m_close;
    int levelLimit;
    private GameObject m_modeClone;



    public override void Init()
    {
        m_modeClone = m_tran.Find("frame/copeitem").gameObject;
        m_datagrid = m_tran.Find("frame/normalgridlayout").gameObject.AddComponent<DataGrid>();
        m_sureBtn = m_tran.Find("frame/ButtonSure").gameObject;
        m_close = m_tran.Find("frame/close").gameObject;
        m_datagrid.SetItemRender(m_modeClone, typeof(ItemOtherRender));
        m_datagrid.autoSelectFirst = false;
        
        
    }

    private void SelectLimit(object renderData)
    {
        var info = renderData as ItemOtherRender.ItemOtherInfo;
        if (info.enable)
        {
            levelLimit = int.Parse(info.data);
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_close).onPointerClick += delegate { this.HidePanel(); };
        m_datagrid.onItemSelected += SelectLimit;
        UGUIClickHandler.Get(m_sureBtn).onPointerClick += delegate { NetLayer.Send(new tos_room_level_limit() { level_limit = levelLimit }); this.HidePanel(); };
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        int maxlevel = 1;
        if (HasParams())
        {
            maxlevel = int.Parse(m_params[0].ToString());
        }
        levelLimit = RoomModel.RoomInfo.level_limit;
        var list = new[]
        {
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[1], data = "1"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[2], data = "2"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[3], data = "3"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[4], data = "4"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[5], data = "5"},
        };

        for (int i = 0; i < list.Length; i++)
        {
            list[i].enable = i <= maxlevel;
        }

        m_datagrid.Data = list;

        if (RoomModel.RoomInfo.level_limit != 0)
        {
            m_datagrid.Select(RoomModel.RoomInfo.level_limit - 1);
        }
        else
        {
            m_datagrid.Select(0);
        }
    }

    public override void Update()
    {
       
    }}

