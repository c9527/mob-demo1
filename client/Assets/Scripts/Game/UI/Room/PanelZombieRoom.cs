﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
class PanelZombieRoom : PanelRoomBase
{
    public static string[] m_levelNames = new[] { "", "简单", "普通", "困难", "噩梦", "地狱" };
    private Text roomName;
    public static int stageId;
    public PanelZombieRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.Chat,
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.AutoAim,
            RoomFunc.SmallMap,
            RoomFunc.FightState
        });

        SetPanelPrefabPath("UI/Room/PanelZombieRoom");
        AddPreLoadRes("UI/Room/PanelZombieRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemCommonspeak", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
        m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
        roomName = m_tran.Find("optionframe/roomname/Text").GetComponent<Text>();
        m_tran.Find("optionframe/btnLeftMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnRightMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnShowAllMap/Image").gameObject.TrySetActive(false);

    }

    public override void InitEvent()
    {
        base.InitEvent();
    }

    public override void OnShow()
    {
        base.OnShow();
        ConfigStageSurvivalLine cfg = ConfigManager.GetConfig<ConfigStageSurvival>().GetLine(stageId) as ConfigStageSurvivalLine;
        if (cfg!=null)
        roomName.text = cfg.MissionName;
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelZombiePass>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }

    protected override void OnStartClick(GameObject target, PointerEventData eventdata)
    {
        if (!RoomModel.IsInRoom)
            return;
        if (CheckAndUpdateMap())
            return;

        var realNum = 0;
        if (IsHost)
        {
            foreach (var player in RoomModel.RoomInfo.players)
            {
                if (!player.ready && player.id != RoomModel.RoomInfo.leader)
                {
                    UIManager.ShowTipPanel("请等待所有玩家准备");
                    return;
                }
                if (player.id > 0)
                    realNum++;
            }
            var configLine = ConfigManager.GetConfig<ConfigStageSurvival>().GetLine(RoomModel.RoomInfo.stage);
            if (realNum == 1 && configLine != null && configLine.IsHard)
                UIManager.ShowTipPanel("该关卡异常凶险，建议邀请好友共同挑战！", () => NetLayer.Send(new tos_room_start_game()), null, false, true, "我要硬闯", "返回邀请");
            else
                NetLayer.Send(new tos_room_start_game());
        }
        else if (RoomModel.RoomInfo.fight)
            NetLayer.Send(new tos_room_join_game());
        else
        {
            NetLayer.Send(new tos_room_ready { ready = !IsReady });
            UserChooseReady = !IsReady;
        }
    }

    protected override void UpdateView()
    {
        base.UpdateView();
        m_tran.Find("optionframe/btnLeftMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnRightMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnShowAllMap/Image").gameObject.TrySetActive(false);
        
        if (!RoomModel.IsInRoom)
            return;

        var myId = PlayerSystem.roleId;
        var roomInfo = RoomModel.RoomInfo;
        var group = m_goPopListAnchor.GetComponent<ToggleGroup>();
        List<GameObject> list = new List<GameObject>();
        foreach (Transform tran in m_roomParent)
        {
            list.Add(tran.gameObject);
        }

        for (int i = 0; i < list.Count; i++)
        {
            GameObject.Destroy(list[i]);
        }
        m_mates = new List<ItemRoommate>();

        int playerIndex = 0;

        var itemSpaceClone = m_tran.Find("pageframe/itemSpace").gameObject;
        for (int i = roomInfo.players.Length; i < 5; i++)
        {
            var item = (UIHelper.Instantiate(itemSpaceClone) as GameObject).AddComponent<ItemRoomspace>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, 0, i * -69);
            UGUIClickHandler.Get(item.gameObject).onPointerClick += delegate {
                int stage = 0;
                string stageType = "";
                if (RoomModel.ChannelType == ChannelTypeEnum.stage)
                {
                    stage = PanelPassRoom.m_iStageId;
                    stageType = ChannelTypeEnum.stage.ToString();
                }
                else if (RoomModel.ChannelType == ChannelTypeEnum.stage_survival)
                {
                    stage = PanelZombieRoom.stageId;
                    stageType = ChannelTypeEnum.stage_survival.ToString();
                }
                NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = stage, stage_type = stageType });
            };
        }

        var itemClone = m_tran.Find("pageframe/item").gameObject;
        foreach (var player in roomInfo.players)
        {
            int campId = 0;

            var item = (UIHelper.Instantiate(itemClone) as GameObject).AddComponent<ItemRoommate>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, campId * 307, (player.pos - 1) * -69);
            item.m_camp = campId;
            item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
            item.m_iHead = player.icon;
            item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, campId));
            item.m_imgHead.SetNativeSize();
            item.m_iLvl = player.level;
            if(player.id == myId)
            {
                item.SetHeroCardHead(player.hero_type);
            }            

//            MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
//            Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;
//
//            if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
//            {
//                item.m_goVip.TrySetActive(false);
//                Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
//                item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);
//
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }


            item.m_txtLv.text = String.Format("{0}级", player.level);
            item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
            if (player.id == roomInfo.leader)
                item.IsHost = true;
            if (player.id == myId)
            {
                m_self = item;
                item.gameObject.GetComponent<Toggle>().enabled = false;
            }
            item.SetState(player.ready, player.fight);
            item.m_iPlayerId = player.id;
            if (m_self != null)
            {
                if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId)
                {
                    UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                    item.gameObject.GetComponent<Toggle>().group = group;
                }
            }
            //item.SetCampColor(campId+1);
            if (item.m_iPlayerId == myId)
            {
                item.m_txtName.color = new Color32(134, 253, 131, 255);
            }
            playerIndex++;
            m_mates.Add(item);
            item.title_cnt[1].text = player.gold_ace.ToString();
            item.title_cnt[0].text = player.mvp.ToString();
            item.m_silverace.text = player.silver_ace.ToString();

            Image m_ace = item.transform.Find("ace").GetComponent<Image>();
            Image m_silver = item.transform.Find("silverace").GetComponent<Image>();


            if (player.titles != null && player.titles.Length >= 3)
            {

                item.m_title[0].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[2].title, player.titles[2].type)));
                item.m_title[0].SetNativeSize();
                //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                item.title_cnt[0].text = player.titles[2].cnt.ToString();

                m_ace.SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[1].title, player.titles[1].type)));
                m_ace.SetNativeSize();
                //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                item.title_cnt[1].text = player.titles[1].cnt.ToString();

                m_silver.SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[0].title, player.titles[0].type)));
                m_silver.SetNativeSize();
                //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                item.m_silverace.text = player.titles[0].cnt.ToString();
            }
        }
    }

    void Toc_room_survival_leader(toc_room_survival_leader data)
    {
        TipsManager.Instance.showTips("你已成为房主，难度调整为" + m_levelNames[data.level]);
    }
}
