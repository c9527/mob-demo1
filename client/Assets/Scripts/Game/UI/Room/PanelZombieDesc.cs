﻿using UnityEngine;
using UnityEngine.UI;


class PanelZombieDesc : BasePanel
{
    private GameObject m_masterSystem;
    private GameObject m_masterGrade;
    public PanelZombieDesc()
    {
        SetPanelPrefabPath("UI/Room/PanelZombieDesc");
        AddPreLoadRes("UI/Room/PanelZombieDesc", EResType.UI);
    }

    public override void Init()
    {
      
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok_btn")).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
       
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {

    }
}

