﻿class PanelSurvivalRoomList : PanelRoomListBase
{
    public PanelSurvivalRoomList() : base(ChannelTypeEnum.survival)
    {
        SetPanelPrefabPath("UI/Room/PanelSurvivalRoomList");
        AddPreLoadRes("UI/Room/PanelSurvivalRoomList", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/FlopCard", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
    }

    public override void Init()
    {
        m_enableSmallMap = true;
        m_enableViewer = true;
        m_enableAutoRefresh = true;
        m_enableFilterRule = true;
        m_enableChannelList = true;
        base.Init();

        m_dataGrid.SetItemRender(m_tran.Find("pageframe/list/content/item").gameObject, typeof(ItemSurvivalRoomRender));
    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_tran.Find("optionframe/fightstate/survivalReward")).onPointerClick += ClickReward;
    }

    private void ClickReward(UnityEngine.GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        UIManager.PopPanel<PanelSurvivalReward>(null,true);
    }


    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();

        UIManager.ShowPanel<PanelPVE>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }
}