﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelSurvivalReward : BasePanel {

    private DataGrid m_modeDataGrid;

    private DataGrid m_ruleGroupDataGrid;

    public PanelSurvivalReward()
    {
        SetPanelPrefabPath("UI/Room/PanelSurvivalReward");
        AddPreLoadRes("UI/Room/PanelSurvivalReward", EResType.UI);
        AddPreLoadRes("Atlas/Room",EResType.Atlas);
        AddPreLoadRes("Atlas/FlopCard",EResType.Atlas);
    }

    public override void Init()
    {
        GameObject item0 = m_tran.Find("vertical/item").gameObject;
        m_modeDataGrid = m_tran.Find("vertical/content").gameObject.AddComponent<DataGrid>();
        m_modeDataGrid.SetItemRender(item0, typeof(RewardGroupRender));
        m_modeDataGrid.useLoopItems = false;
        GameObject ruleItem0 = m_tran.Find("left/mode0").gameObject;
        m_ruleGroupDataGrid = m_tran.Find("left/content").gameObject.AddComponent<DataGrid>();
        m_ruleGroupDataGrid.SetItemRender(ruleItem0, typeof(RewardRuleRender));
        m_ruleGroupDataGrid.onItemSelected += RuleSelected;
        m_ruleGroupDataGrid.autoSelectFirst = true;
        m_ruleGroupDataGrid.Data = SurvivalRewardData.Ins.ruleRewardDic.Keys.ToList<string>().ToArray();
    }

    private void RuleSelected(object renderData)
    {
        string key = renderData as string;
        m_modeDataGrid.Data = SurvivalRewardData.Ins.ruleRewardDic[key].Values.ToList<SurvivalGroupData>().ToArray();
    }

    public override void InitEvent()
    {
        //UGUIClickHandler.Get(m_tran.Find("left/content/mode0")).onPointerClick += OnClickEradicate;
        //UGUIClickHandler.Get(m_tran.Find("left/content/mode1")).onPointerClick += OnClickDefend;
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += delegate { HidePanel(); };
    }

    //private void OnClickDefend(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    //{
    //    m_modeDataGrid.Data = SurvivalRewardData.Ins.defendArray;
    //    target.GetComponent<Toggle>().isOn = true;
    //}

    //private void OnClickEradicate(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    //{
    //    m_modeDataGrid.Data = SurvivalRewardData.Ins.eradicateArray;
    //    target.GetComponent<Toggle>().isOn = true;
    //}

    public override void OnShow()
    {        
//        m_ruleGroupDataGrid.Select(0);
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}
