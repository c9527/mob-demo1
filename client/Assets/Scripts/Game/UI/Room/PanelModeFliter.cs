﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

class ItemModeRender : ItemRender
{
    private Text m_txtName;
    private Image m_imgBg;
    private Image m_icon;//hot、 new图标
    private Toggle m_toggle;
    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("Background/Checkmark/Text").GetComponent<Text>();
        m_imgBg = tran.Find("Background").GetComponent<Image>();
        m_toggle = tran.GetComponent<Toggle>();
        m_icon = tran.Find("Background/icon").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var info = data as ConfigGameRuleLine;

        m_toggle.interactable = info != null;
        m_imgBg.enabled = info != null;

        name = info != null ? info.Type : "empty";
        m_txtName.text = info != null ? info.Mode_Name : "";
        m_icon.gameObject.TrySetActive(false);
        if( info != null && !String.IsNullOrEmpty(info.ModeSubIcon))
        {
            m_icon.gameObject.TrySetActive(true);
            m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, info.ModeSubIcon));
            m_icon.SetNativeSize();
        }
    }
}

public class PanelModeFliter : BasePanel
{
    private enum ModeClass
    {
        Nomarl = 0,
        Special = 1,
    }

    private GameObject m_modeClone;
    private Transform m_goNormalLayout;
    private DataGrid m_dataGrid;
    private Toggle m_allmode;
    private string m_strModeName;
    public Action<string> m_callBack; 

    public PanelModeFliter()
    {
        SetPanelPrefabPath("UI/Room/PanelModeFliter");
        AddPreLoadRes("UI/Room/PanelModeFliter", EResType.UI);
    }

    public override void Init()
    {
        m_modeClone = m_tran.Find("frame/copeitem").gameObject;
        m_goNormalLayout = m_tran.Find("frame/normalgridlayout");

        m_dataGrid = m_goNormalLayout.gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_modeClone, typeof(ItemModeRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.onItemSelected += OnSelectMode;

        m_allmode = m_tran.Find("frame/allmode").GetComponent<Toggle>();
        m_allmode.group = m_dataGrid.GetComponent<ToggleGroup>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/ButtonSure")).onPointerClick += OnClickConfirmMode;
        UGUIClickHandler.Get(m_allmode.gameObject).onPointerClick += delegate { m_strModeName = ""; };
    }

    public override void OnShow()
    {
        if (HasParams())
            m_strModeName = m_params[0] as string;
        UpdateModeToggles();
    }

    public override void OnHide()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    public override void OnBack()
    {
    }
   
    private void UpdateModeToggles()
    {
        var list1 = new List<ConfigGameRuleLine>();
        var list2 = new List<ConfigGameRuleLine>();
        var arr = ConfigManager.GetConfig<ConfigGameRule>().m_dataArr;
        for (int i = 0; i < arr.Length; i++)
        {
            var line = arr[i];
            if( line.IsShow(null, RoomModel.ChannelId) )
            {
                if (line.Mode_Class == (int)ModeClass.Nomarl)
                    list1.Add(line);
                else if (line.Mode_Class == (int) ModeClass.Special)
                    list2.Add(line);
            }
        }
        list1.Sort(sortMode);
        var emptyCount = 15 - list1.Count;
        for (int i = 0; i < emptyCount; i++)
        {
            list1.Add(null);
        }
        list2.Sort(sortMode);
        list1.AddRange(list2);

        m_dataGrid.Data = list1.ToArray();

        var dataGridHas = false;
        for (int i = 0; i < list1.Count; i++)
        {
            if (list1[i] != null && list1[i].Type == m_strModeName)
            {
                dataGridHas = true;
                m_dataGrid.Select(i);
                break;
            }
        }
        if (!dataGridHas)
        {
            m_strModeName = "";
            m_allmode.isOn = true;
        }
    }

    private int sortMode(ConfigGameRuleLine a, ConfigGameRuleLine b)
    {
            if (a.ModeSort > b.ModeSort)
            {
                return 1;
            }
            else if (a.ModeSort == b.ModeSort)
            {
                return 0;
            }
            return -1;
    }

    private void OnSelectMode(object renderData)
    {
        var config = renderData as ConfigGameRuleLine;
        if (config == null)
            return;

        m_strModeName = config.Type;
    }

    private void OnClickConfirmMode(GameObject target, PointerEventData eventData)
    {
        HidePanel();

        if (m_callBack != null)
        {
            var func = m_callBack;
            m_callBack = null;
            func(m_strModeName);
        }
    }

    
}