﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
class PanelCorpsGuardRoom : PanelRoomBase
{
    public static string[] m_levelNames = new[] { "", "简单", "普通", "困难", "噩梦", "地狱" };

    public Text _cd_txt;
    GameObject m_goLeftMap;
    GameObject m_goRightMap;
    GameObject m_goShowAllMap;
    public PanelCorpsGuardRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.FightState, 
            RoomFunc.AutoKick, 
            RoomFunc.Viewing, 
            RoomFunc.Chat,
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.SmallMap,
            RoomFunc.AutoAim
        });

        SetPanelPrefabPath("UI/Room/PanelCorpsGuardRoom");
        AddPreLoadRes("UI/Room/PanelCorpsGuardRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemCommonspeak", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
        m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
        _cd_txt = m_tran.Find("optionframe/InputField/Text").GetComponent<Text>();
        m_goLeftMap = m_tran.Find("optionframe/btnLeftMap").gameObject;
        m_goRightMap = m_tran.Find("optionframe/btnRightMap").gameObject;
        m_goShowAllMap =  m_tran.Find("optionframe/btnShowAllMap").gameObject;
    }

    override protected void OnClickInviteFriend(GameObject target, PointerEventData eventdata)
    {
        if (IsViewing)
            return;
        NetLayer.Send(new tos_player_room_invitelist { is_friend = false, stage = 0, stage_type = "" });
    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnRank")).onPointerClick += delegate
        {
            UIManager.PopPanel<PanelCorpsRoomRank>(null, true, this);
        };
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelSocity>(new object[] { "corps" });
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
        var cd = CorpsDataManager.GedtGuardCD();
        _cd_txt.text = TimeUtil.FormatTime(cd);
        m_goLeftMap.TrySetActive(false);
        m_goRightMap.TrySetActive(false);
        m_goShowAllMap.TrySetActive(false);

    }

    protected override void UpdateView()
    {
        base.UpdateView();

        if (!RoomModel.IsInRoom)
            return;

        m_btnStartGo.gameObject.TrySetActive(IsHost);

        var myId = PlayerSystem.roleId;
        var roomInfo = RoomModel.RoomInfo;
        var group = m_goPopListAnchor.GetComponent<ToggleGroup>();
        List<GameObject> list = new List<GameObject>();
        foreach (Transform tran in m_roomParent)
        {
            list.Add(tran.gameObject);
        }

        for (int i = 0; i < list.Count; i++)
        {
            GameObject.Destroy(list[i]);
        }
        m_mates = new List<ItemRoommate>();

        int playerIndex = 0;

        var itemSpaceClone = m_tran.Find("pageframe/itemSpace").gameObject;
        for (int i = roomInfo.players.Length; i < 5; i++)
        {
            var item = (UIHelper.Instantiate(itemSpaceClone) as GameObject).AddComponent<ItemRoomspace>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, 0, i * -69);
            UGUIClickHandler.Get(item.gameObject).onPointerClick += delegate { NetLayer.Send(new tos_player_room_invitelist() { is_friend = false, stage = 0, stage_type = "" }); };
        }

        var itemClone = m_tran.Find("pageframe/item").gameObject;
        foreach (var player in roomInfo.players)
        {
            int campId = 0;

            var item = (UIHelper.Instantiate(itemClone) as GameObject).AddComponent<ItemRoommate>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, campId * 307, (player.pos-1) * -69);
            item.m_camp = campId;
            item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
            item.m_iHead = player.icon;
            item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, campId));
            item.m_imgHead.SetNativeSize();
            item.m_iLvl = player.level;

//            MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
//            Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;
//
//            if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
//            {
//                item.m_goVip.TrySetActive(false);
//                Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
//                item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);
//
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }


            item.m_txtLv.text = String.Format("{0}级",  player.level);
            item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
            if (player.id == roomInfo.leader)
                item.IsHost = true;
            if (player.id == myId)
            {
                m_self = item;
                item.gameObject.GetComponent<Toggle>().enabled = false;
            }
            item.SetState(player.ready, player.fight);
            item.m_iPlayerId = player.id;
            if (m_self != null)
            {
                if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId)
                {
                    UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                    item.gameObject.GetComponent<Toggle>().group = group;
                }
            }
            //item.SetCampColor(campId+1);
            if (item.m_iPlayerId == myId)
            {
                item.m_txtName.color = new Color32(134, 253, 131, 255);
            }
            playerIndex++;
            m_mates.Add(item);
            item.title_cnt[1].text = player.gold_ace.ToString();
            item.title_cnt[0].text = player.mvp.ToString();
            item.m_silverace.text = player.silver_ace.ToString();
        }
    }
}
