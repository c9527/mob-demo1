﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelRobotSelect : BasePanel
{
    private GameObject m_goClose;
    private GameObject m_goNew;
    private GameObject m_goNormal;
    private GameObject m_goOld;
    private GameObject m_goBt;

    public PanelRobotSelect()
    {
        SetPanelPrefabPath("UI/Room/PanelRobotSelect");
        AddPreLoadRes("UI/Room/PanelRobotSelect", EResType.UI);
    }

    public override void Init()
    {
        m_goClose = m_tran.Find("close").gameObject;
        m_goNew = m_tran.Find("new").gameObject;
        m_goNormal = m_tran.Find("normal").gameObject;
        m_goOld = m_tran.Find("old").gameObject;
        m_goBt = m_tran.Find("bt").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_goClose).onPointerClick += OnClose;
        UGUIClickHandler.Get(m_goNew).onPointerClick += OnSelectRobet;
        UGUIClickHandler.Get(m_goNormal).onPointerClick += OnSelectRobet;
        UGUIClickHandler.Get(m_goOld).onPointerClick += OnSelectRobet;
        UGUIClickHandler.Get(m_goBt).onPointerClick += OnSelectRobet;
    }

    public override void OnShow()
    {
        
    }

    public override void OnHide()
    {
    }

    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_goClose).onPointerClick -= OnClose;
        UGUIClickHandler.Get(m_goNew).onPointerClick -= OnSelectRobet;
        UGUIClickHandler.Get(m_goNormal).onPointerClick -= OnSelectRobet;
        UGUIClickHandler.Get(m_goOld).onPointerClick -= OnSelectRobet;
        UGUIClickHandler.Get(m_goBt).onPointerClick -= OnSelectRobet;
    }

    public override void Update()
    {
    }

    public override void OnBack()
    {
    }

    void    OnClose( GameObject target, PointerEventData eventData )
    {
        HidePanel();
    }

    void    OnSelectRobet( GameObject target, PointerEventData eventData )
    {
        string pos = m_params[0] as string;
        GameDispatcher.Dispatch(GameEvent.UI_ROBOT_SELECT, target.name, pos);
        HidePanel();
    }
}
