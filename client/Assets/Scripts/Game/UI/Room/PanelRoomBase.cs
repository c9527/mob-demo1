﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;

enum RoomFunc
{
    NormalRoommate,
    FightState,
    AutoKick,
    Viewing,
    Chat,
    Password,
    ChangeMode,
    DisableMode,
    InviteFriend,
    ChangeCamp,
    ChangeVsIcon,
    AutoStart,
    SmallMap,
    AutoAim,
    RankMatching,
    WorldBoss
}

public class chatRecord
{
    public int pos;
    public float startTime;
    public string msg;
    public long id;
}

public class roomPunish
{
    public long player_id;
    public string name;     // 被禁赛的玩家名
    public int release_time;    // 解禁时间戳
}
class PanelRoomBase : BasePanel
{

    public static List<chatRecord> chatList;

    //数据
    private HashSet<RoomFunc> m_roomFuncDic = new HashSet<RoomFunc>();
    private p_spectator_info m_selfView;
    protected p_player_info m_selfPlay;
    private readonly tos_room_rule m_roomRule = new tos_room_rule();     //本地规则数据对象
    protected bool IsViewing { get { return m_selfView != null; } }     //自己在观战
    protected bool IsReady { get { return m_selfView != null ? m_selfView.ready : m_selfPlay != null && m_selfPlay.ready; } }   //自己已准备
    protected bool UserChooseReady;
    protected bool IsHost { get { return m_selfPlay != null && m_selfPlay.id == RoomModel.RoomInfo.leader; } }  //自己是房主
    protected bool IsPlayer { get { return m_selfPlay != null; } }  //自己是玩家
    private static int m_RemindTimer = 0;    

    //房间格子
    private ItemDropInfo[] m_hostOpArr;
    private ItemDropInfo[] m_guestOpArr;
    private ItemDropInfo[] m_leaderOpArr;
    protected List<ItemRoommate> m_mates;

    protected GameObject m_goPopListAnchor;
    protected GameObject m_goPopListPanel;
    private int m_camp1;
    private int m_camp2;
    protected Transform m_roomParent;
    protected ItemRoommate m_self;
    private ItemRoommate m_selRoomMate;

    //小地图
    private int m_mapIndex;
    private Image m_imgSmallMap;
    private Text m_txtMapName;
    private GameObject m_goLeftMap;
    private GameObject m_goRightMap;
    private GameObject m_goShowAllMap;
    private GameObject m_triangleImage;
    private Text m_fightstate;
    private GameObject m_goDownloadPg;
    private Image m_imgDownloadPg;
    private static Dictionary<int, bool> m_mapNeedUpdateDic = new Dictionary<int, bool>(); //需要更新的地图的字典
    private static Dictionary<int, bool> m_mapDownloadingDic = new Dictionary<int, bool>(); //正在下载的地图的字典
    private int m_lastCheckMapId;       //上一次检查CRC的地图id
    private bool m_lastCheckMapResult;  //上一次检查地图的结果
    private string m_modeName = "";

    private bool MapNeedUpdate
    {
        set { if (RoomModel.IsInRoom) m_mapNeedUpdateDic[RoomModel.RoomInfo.map] = value; }
        get { return RoomModel.IsInRoom && m_mapNeedUpdateDic.GetValue(RoomModel.RoomInfo.map); }
    }

    private bool MapDownloading
    {
        set { if (RoomModel.IsInRoom) m_mapDownloadingDic[RoomModel.RoomInfo.map] = value; }
        get { return RoomModel.IsInRoom && m_mapDownloadingDic.GetValue(RoomModel.RoomInfo.map); }
    }

    //自动踢人
    private float m_lastActiveTime;
    private int m_cfgRemindTime;
    private int m_cfgRemindKickTime;

    //观战
    public Button m_btnChangeMode;
    public Text m_txtChangeMode;
    private GameObject m_spchat;
    private GameObject m_chatclose;
    private GameObject m_spchatbubble;
    private List<Text> m_texts;
    private int m_lines;
    GameObject m_chattxt;
    Text m_spnum;
    GameObject m_sptxt;
    GameObject m_spchatimage;
    float m_spchatTime;
    bool m_isspchat;
    Text m_sps;

    //聊天、常用语
    private InputField m_chat;
    private ItemDropInfo[] m_commonSpeakArr;

    //自动瞄准开关
    private Text m_txtAim;
    private Toggle m_toggleAutoAim;
    private Image m_toggleAutoAimOffImg;
    private Image m_toggleAutoAimOnImg;

    //房间密码
    private Text m_txtPassword;
    private GameObject m_passin;
    private string m_password;
    private Text m_ipt;
    private Image m_passimage;

    //模式选择
    private Text m_txtModeBtnName;
    private GameObject m_btnModeGo;
    private Text m_modeDisableText;
    private GameObject m_modeDisable;

    //开始
    protected GameObject m_btnStartGo;
    private Text m_startORprepare;

    //邀请好友
    private GameObject m_btnInviteFriendGo;

    //阵营变更
    private Button m_btnChangeCamp;
    private Image m_imgvs;

    //自动开始
    float m_startTime;
    private int m_resttime;

    private DataGrid m_campGrid1;
    private DataGrid m_campGrid2;

    private static PanelRoomBase m_currentRoom;


    #region 房间功能开关
    protected void SetRoomFunc(RoomFunc[] funcs)
    {
        m_roomFuncDic.Clear();
        for (int i = 0; i < funcs.Length; i++)
        {
            m_roomFuncDic.Add(funcs[i]);
        }
    }

    private bool IsSupport(RoomFunc func)
    {
        return m_roomFuncDic.Contains(func);
    }
    #endregion

    public override void Init()
    {
        m_roomParent = m_tran.Find("pageframe/list");
        chatList = new List<chatRecord>();
        
        InitRoommate();
        InitMap();
        InitAutoKick();
        InitViewing();
        InitChat();
        InitAutoAim();
        InitPassword();
        InitRule();
        InitStart();
        InitInviteFriend();
        InitCampChange();
        InitAutoStart();
    }

    public override void InitEvent()
    {
        AddEventListener<int>(GameEvent.UI_SELECT_MAP, OnMapSelect);
    }

    private void OnMapSelect(int mapid)
    {
        m_roomRule.map = mapid;
        var configs = ConfigManager.GetConfig<ConfigMapList>();
        ConfigMapListLine mapLine = null;
        if( configs != null )
        {
            mapLine = configs.GetLine(mapid.ToString());
        }
        m_txtMapName.text = mapLine == null ? "" : mapLine.MapName;
        ApplyRoomRule();
    }


    public override void OnShow()
    {
        if (UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.HidePanel<PanelMainPlayerInfo>();
        }

        if (UIManager.IsOpen<PanelFriend>())
        {
            UIManager.HidePanel<PanelFriend>();
        }


        chatList.Clear();
        UIManager.HideDropList();
        //DelRemindTimer();
        if(UIManager.IsOpen<PanelHall>()) 
        {
            UIManager.GetPanel<PanelHall>().SetNormalBack();
        }
        m_currentRoom = this;
        UISystem.panelRoomParent = m_tran.parent;
        m_password = "";
        m_lastActiveTime = Time.realtimeSinceStartup;
        UpdateData();
        UpdateView();
        
        //自动指定房间模式
        if (RoomModel.m_autoRoomRule != null)
        {
            if (IsSupport(RoomFunc.ChangeMode))
            {
                if (RoomModel.ChannelType != ChannelTypeEnum.survival)
                {
                    //团体竞技特殊处理
                    if (RoomModel.RoomInfo.rule.IndexOf(RoomModel.m_autoRoomRule) == -1)
                        OnRuleChanged(RoomModel.m_autoRoomRule, "10");
                    RoomModel.m_autoRoomRule = null;
                }
            }
        }

        
    }

    public static bool IsCurrentRoomOpen()
    {
        if (m_currentRoom == null || !m_currentRoom.IsOpen())
            return false;
        return true;
    }


    public static void ShowMinimizeRoom()
    {
        if(m_currentRoom!=null&&!m_currentRoom.IsOpen())
            NetLayer.Send(new tos_room_chat { msg = "我又回来了" });
        if(m_currentRoom!=null)
            m_currentRoom.ShowPanel(UISystem.panelRoomParent);        
        if(UIManager.CurContentPanel!=null)
            UIManager.CurContentPanel.HidePanel();
        UIManager.CurContentPanel = m_currentRoom;
        UISystem.isMinimizeRoom = false;
    }

    public override void OnHide()
    {
        m_startTime = -1.0f;        
        if (RoomModel.IsInRoom && !RoomModel.m_roomswitch && !UISystem.isMinimizeRoom)
        {
            NetLayer.Send(new tos_room_leave());            
        }            
        else
        {                        
            if (UISystem.isMinimizeRoom)
            {
                NetLayer.Send(new tos_room_chat { msg = "我要暂离一会" });
                //AddRemindTimer();
                var hall = UIManager.GetPanel<PanelHall>();
                if (hall != null)
                    hall.MinimizePanelRoom();                
                if(!IsHost&&IsReady)
                {
                    NetLayer.Send(new tos_room_ready() { ready=false });
                    UserChooseReady = false;
                }
            }
        }
        RoomModel.m_roomswitch = false;
        PanelMatchingTips.m_sid = 0;
        UIManager.HidePanel<PanelInviteFriend>();
        UIManager.HidePanel<PanelMapSelect>();
    }

    private static void AddRemindTimer()
    {
        int remindTime = m_currentRoom.m_cfgRemindTime - m_currentRoom.m_cfgRemindKickTime;
        int kickTime = m_currentRoom.m_cfgRemindKickTime;
        m_RemindTimer = TimerManager.SetTimeOut(remindTime, () =>
            {                
                UIManager.ShowTipPanel(string.Format("您在房间{0}秒内未操作，{1}秒后将被踢出房间", remindTime, kickTime), () =>
                {
                    ShowMinimizeRoom();
                    var panelHall = UIManager.GetPanel<PanelHall>();
                    if (panelHall != null)
                        panelHall.SetNormalBack();                   
                }, () => 
                {
                    var panelHall = UIManager.GetPanel<PanelHall>();
                    if (panelHall != null)
                        panelHall.SetNormalBack();
                    NetLayer.Send(new tos_room_leave());                    
                },false,true,"回到房间","离开房间");
                m_RemindTimer = 0;             
            }, true);       
    }

    private static void DelRemindTimer()
    {
        if (m_RemindTimer >0)
            TimerManager.RemoveTimeOut(m_RemindTimer);
        m_RemindTimer = 0;        
    }

    public static void RoomMinimizeCheck(Action callback)
    {
        if (UISystem.isMinimizeRoom)
        {
            //DelRemindTimer();
            UIManager.ShowTipPanel("已经在房间中，是否离开房间", () =>
            {                
                var panelHall = UIManager.GetPanel<PanelHall>();
                if (panelHall != null)
                    panelHall.SetNormalBack();
                UISystem.isMinimizeRoom = false;
                NetLayer.Send(new tos_room_leave());
                callback();
            }, null, true, true);
        }
        else
        {
            callback();
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (!IsOpen() || !RoomModel.IsInRoom)
            return;

        //自动踢人
//        Update_AutoKick();

        //观战
        Update_Viewing();

        //自动开始
        Update_AutoStart();

        //小地图
        Update_Map();
    }

    #region 房间格子

    private void InitRoommate()
    {
        if (IsSupport(RoomFunc.NormalRoommate))
        {
            m_camp1 = 0;
            m_camp2 = 0;

            m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
            m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
        }

        m_mates = new List<ItemRoommate>();
        m_hostOpArr = new[]
        {
            new ItemDropInfo {type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
            new ItemDropInfo {type = "OPERATION", subtype = "ADDFRIEND", label = "加为好友"},
            new ItemDropInfo {type = "OPERATION", subtype = "CHANGE", label = "转让房主"},
            new ItemDropInfo {type = "OPERATION", subtype = "KICK", label = "踢出房间"},
            new ItemDropInfo {type = "OPERATION", subtype = "CHAT", label = "私聊"}
        };
        m_guestOpArr = new[]
        {
            new ItemDropInfo {type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
            new ItemDropInfo {type = "OPERATION", subtype = "ADDFRIEND", label = "加为好友"},
            new ItemDropInfo {type = "OPERATION", subtype = "CHAT", label = "私聊"}
        };

        m_leaderOpArr = new[]
        {
            new ItemDropInfo {type = "OPERATION", subtype = "LOOKUP", label = "查看信息"},
            new ItemDropInfo {type = "OPERATION", subtype = "ADDFRIEND", label = "加为好友"},
            new ItemDropInfo {type = "OPERATION", subtype = "KICK", label = "踢出房间"},
            new ItemDropInfo {type = "OPERATION", subtype = "CHAT", label = "私聊"}
        };


    }

    private void UpdateRoommate()
    {
        if (!IsSupport(RoomFunc.NormalRoommate))
            return;
        var myId = PlayerSystem.roleId;
        Transform m_camp1grid = m_tran.Find("pageframe/Camp1/content");
        Transform m_camp2grid = m_tran.Find("pageframe/Camp2/content");
        GameObject[] camps1 = new GameObject[5];
        GameObject[] camps2 = new GameObject[5];

        foreach (Transform item in m_camp1grid)
        {
            GameObject.Destroy(item.gameObject);
        }
        foreach (Transform item in m_camp2grid)
        {
            GameObject.Destroy(item.gameObject);
        }



        m_mates = new List<ItemRoommate>();
        var group = m_goPopListAnchor.GetComponent<ToggleGroup>();
        m_camp1 = 0;
        m_camp2 = 0;
        var list = new List<GameObject>();
        foreach (Transform tran in m_roomParent)
        {
            list.Add(tran.gameObject);
        }

        for (int i = 0; i < list.Count; i++)
        {
            Object.Destroy(list[i]);
        }

        bool modecamp0 = false;
        int playerIndex = 0;
        var m_mode = RoomModel.RoomInfo.rule;
        if (m_mode == GameConst.GAME_RULE_ZOMBIE || m_mode == GameConst.GAME_RULE_ZOMBIE_HERO || m_mode == GameConst.GAME_RULE_ZOMBIE_ULTIMATE)
        {
            modecamp0 = true;
        }

        if (modecamp0)
        {
            if (RoomModel.RoomInfo.max_player <= 10)
            {
                for (int i = m_camp1; i < 5; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace").AddComponent<ItemRoomspace>();
                    camps1[i] = item.gameObject;
                }
                for (int i = m_camp2; i < 5; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace").AddComponent<ItemRoomspace>();
                    item.m_camp = 2;
                    item.SetCampColor(2);
                    camps2[i] = item.gameObject;
                }
            }
            else
            {
                camps1 = new GameObject[8];
                camps2 = new GameObject[8];
                for (int i = m_camp1; i < 8; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace8").AddComponent<ItemRoomspace8>();
                    camps1[i] = item.gameObject;
                }
                for (int i = m_camp2; i < 8; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace8").AddComponent<ItemRoomspace8>();
                    item.m_camp = 2;
                    item.SetCampColor(2);
                    camps2[i] = item.gameObject;
                }
            }
        }
        else
        {
            camps1 = new GameObject[RoomModel.RoomInfo.max_player / 2];
            camps2 = new GameObject[RoomModel.RoomInfo.max_player / 2];
            if (RoomModel.RoomInfo.max_player <= 10)
            {
                for (int i = m_camp1; i < RoomModel.RoomInfo.max_player / 2; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace").AddComponent<ItemRoomspace>();
                    camps1[i] = item.gameObject;
                }
                for (int i = m_camp2; i < RoomModel.RoomInfo.max_player / 2; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace").AddComponent<ItemRoomspace>();
                    item.m_camp = 2;
                    item.SetCampColor(2);
                    camps2[i] = item.gameObject;
                }
            }
            else
            {
                for (int i = m_camp1; i < RoomModel.RoomInfo.max_player / 2; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace8").AddComponent<ItemRoomspace8>();
                    camps1[i] = item.gameObject;
                }
                for (int i = m_camp2; i < RoomModel.RoomInfo.max_player / 2; i++)
                {
                    var item = ResourceManager.LoadUI("UI/Room/ItemRoomspace8").AddComponent<ItemRoomspace8>();
                    item.m_camp = 2;
                    item.SetCampColor(2);
                    camps2[i] = item.gameObject;
                }
            }
        }

        foreach (var player in RoomModel.RoomInfo.players)
        {
            int campId = (player.pos - 1) % 2;
            if (RoomModel.RoomInfo.max_player <= 10)
            {
                GameObject go = ResourceManager.LoadUI("UI/Room/ItemRoommate");
                var item = go.AddComponent<ItemRoommate>();
                item.m_camp = campId;
                item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
                item.m_iHead = player.icon;
                item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, campId));

                MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
                Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;

                item.setVipInfo(eType, player.level,player.honor);
                item.SetHeroCardHead(player.hero_type);
               /* if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
                {
                    item.m_goVip.TrySetActive(false);
                    Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
                    item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);

                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
                    item.m_imgVip.SetNativeSize();
                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
                    item.m_imgVip.SetNativeSize();
                }*/

                item.m_iLvl = player.level;
                item.m_txtLv.text = player.level.ToString();
                item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
                if (player.id == RoomModel.RoomInfo.leader)
                    item.IsHost = true;
                if (player.id == myId)
                {
                    m_self = item;
                    item.gameObject.GetComponent<Toggle>().enabled = false;
                }
                item.SetState(player.ready, player.fight);
                item.m_iPlayerId = player.id;
                if (m_self != null /*&& m_self.IsHost */)
                {
                    if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId /* && item.m_iPlayerId != m_roomInfo.Int("leader") */ )
                    {
                        UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                        item.gameObject.GetComponent<Toggle>().group = group;
                    }
                }
                item.SetCampColor(campId + 1);
                if (item.m_iPlayerId == myId)
                {
                    item.m_txtName.color = new Color32(134, 253, 131, 255);
                }
                playerIndex++;
                m_mates.Add(item);
                item.title_cnt[0].text = "0";
                item.title_cnt[1].text = "0";
                for (int e = 0; e < item.m_title.Length; e++)
                {
                    if (player.titles.Length <= 2)
                    {
                        item.m_title[0].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon("mvp", 1)));
                        item.m_title[0].SetNativeSize();
                        item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                        item.title_cnt[0].text = player.mvp.ToString();

                        item.m_title[1].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon("gold_ace", 1)));
                        item.m_title[1].SetNativeSize();
                        item.m_title[1].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                        item.title_cnt[1].text = player.gold_ace.ToString();
                    }
                    else
                    {
                        item.m_title[e].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[e].title, player.titles[e].type)));
                        item.m_title[e].SetNativeSize();
                        item.m_title[e].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                        item.title_cnt[e].text = player.titles[e].cnt.ToString();
                    }
                }
                int i = (player.pos - 1) / 2;
                if (campId == 0)
                {
                    GameObject.Destroy(camps1[i]);
                    camps1[i] = item.gameObject;
                }
                else
                {
                    GameObject.Destroy(camps2[i]);
                    camps2[i] = item.gameObject;
                }
            }
            else
            {
                GameObject go = ResourceManager.LoadUI("UI/Room/ItemRoommate8");
                var item = go.AddComponent<ItemRoommate8>();
                item.m_camp = campId;
                item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
                item.m_iHead = player.icon;
                item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, campId));

                MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
                Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;

                item.setVipInfo(eType,player.level,player.honor);
                item.SetHeroCardHeadV8(player.hero_type);
                /*if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
                {
                    item.m_goVip.TrySetActive(false);
                    Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
                    item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);

                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
                    item.m_imgVip.SetNativeSize();
                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
                    item.m_imgVip.SetNativeSize();
                }*/

                item.m_iLvl = player.level;
                item.m_txtLv.text = player.level.ToString();
                item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
                if (player.id == RoomModel.RoomInfo.leader)
                    item.IsHost = true;
                if (player.id == myId)
                {
                    m_self = item;
                    item.gameObject.GetComponent<Toggle>().enabled = false;
                }
                item.SetState(player.ready, player.fight);
                item.m_iPlayerId = player.id;
                if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId /* && item.m_iPlayerId != m_roomInfo.Int("leader") */ )
                {
                    UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                    item.gameObject.GetComponent<Toggle>().group = group;
                }
                item.SetCampColor(campId + 1);
                if (item.m_iPlayerId == myId)
                {
                    item.m_txtName.color = new Color32(134, 253, 131, 255);
                }
                playerIndex++;
                m_mates.Add(item);
                int i = (player.pos - 1) / 2;
                if (campId == 0)
                {
                    GameObject.Destroy(camps1[i]);
                    camps1[i] = item.gameObject;
                }
                else
                {
                    GameObject.Destroy(camps2[i]);
                    camps2[i] = item.gameObject;
                }
            }
        }

        for (int i = 0; i < camps1.Length; i++)
        {
            camps1[i].gameObject.GetRectTransform().SetUILocation(m_camp1grid);
            camps2[i].gameObject.GetRectTransform().SetUILocation(m_camp2grid);
        }




    }

    protected void OnClickOperation(GameObject target, PointerEventData eventdata)
    {
        var itemRooMate = target.GetComponent<ItemRoommate>();
        if (itemRooMate == null)
            return;
        m_selRoomMate = itemRooMate;
        //m_selPlayerId = itemRooMate.m_iPlayerId;
        if (RoomModel.RoomInfo.channel != 8)
        {
            UIManager.ShowDropList(m_self.IsHost ? m_hostOpArr : m_guestOpArr, OnOpListItemSelected, UIManager.GetPanel<PanelHall>().transform,
                target, 220, 30, UIManager.DropListPattern.Ex);
        }
        else
        {
            UIManager.ShowDropList(CorpsDataManager.IsLeader() ? m_leaderOpArr : m_guestOpArr, OnOpListItemSelected, UIManager.GetPanel<PanelHall>().transform,
                target, 220, 30, UIManager.DropListPattern.Ex);
        }
        SetPopListPanelCallBack(true, OnClickBeyoundPopList);
    }

    private void SetPopListPanelCallBack(bool value, UGUIClickHandler.PointerEvetCallBackFunc func)
    {
        if (IsSupport(RoomFunc.NormalRoommate))
        {
            m_goPopListPanel.TrySetActive(value);
            if (value)
            {
                UGUIClickHandler.Get(m_goPopListPanel).onPointerClick += func;
            }
            else
            {
                UGUIClickHandler.Get(m_goPopListPanel).onPointerClick -= func;
                
            }
        }
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        if (m_selRoomMate == null)
            return;

        if (data.subtype == "CHANGE")
        {
            NetLayer.Send(new tos_room_leader { id = m_selRoomMate.m_iPlayerId });
        }
        else if (data.subtype == "KICK")
        {
            NetLayer.Send(new tos_room_kick_player { id = m_selRoomMate.m_iPlayerId });
        }
        else if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(m_selRoomMate.m_iPlayerId);
        }
        else if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(m_selRoomMate.m_iPlayerId);
        }
        else if (data.subtype == "CHAT")
        {
            var rcd = new PlayerRecord
			{
				m_iPlayerId = m_selRoomMate.m_iPlayerId,
				m_strName = m_selRoomMate.m_txtName.text,
				m_iLevel = m_selRoomMate.m_iLvl,
				m_bOnline = true,
				m_isFriend = PlayerFriendData.singleton.isFriend(m_selRoomMate.m_iPlayerId),
				m_iHead = m_selRoomMate.m_iHead
			};
			var msgTips = new ChatMsgTips { m_rcd = rcd, m_eChannel = CHAT_CHANNEL.CC_PRIVATE };
			UIManager.PopChatPanel<PanelChatSmall>(new object[] { msgTips });
        }

        m_selRoomMate = null;
        SetPopListPanelCallBack(false, OnClickBeyoundPopList);
    }

    private void OnClickBeyoundPopList(GameObject target, PointerEventData eventdata)
    {
        SetPopListPanelCallBack(false, OnClickBeyoundPopList);
        UIManager.HideDropList();
    }

    protected void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }
    #endregion

    #region 小地图
    private void InitMap()
    {
        if (!IsSupport(RoomFunc.SmallMap))
        {
            return;
        }
        m_imgSmallMap = m_tran.Find("optionframe/smallMap/map").GetComponent<Image>();
        m_txtMapName = m_tran.Find("optionframe/smallMap/mapname").GetComponent<Text>();
        m_goLeftMap = m_tran.Find("optionframe/btnLeftMap").gameObject;
        m_goRightMap = m_tran.Find("optionframe/btnRightMap").gameObject;
        m_goShowAllMap = m_tran.Find("optionframe/btnShowAllMap").gameObject;
        m_triangleImage = m_tran.Find("optionframe/btnShowAllMap/Image").gameObject;
        if (m_tran.Find("optionframe/smallMap/dlPg"))
            m_goDownloadPg = m_tran.Find("optionframe/smallMap/dlPg").gameObject;
        if (m_tran.Find("optionframe/smallMap/dlPg/value"))
            m_imgDownloadPg = m_tran.Find("optionframe/smallMap/dlPg/value").GetComponent<Image>();
        UGUIClickHandler.Get(m_goLeftMap).onPointerClick += delegate { ChangeMap(false); ApplyRoomRule(); };
        UGUIClickHandler.Get(m_goRightMap).onPointerClick += delegate { ChangeMap(true); ApplyRoomRule(); };
        UGUIClickHandler.Get(m_goShowAllMap).onPointerClick += delegate
        {
            if (mapList.Count > 0)
            {
                int mapid = RoomModel.RoomInfo.map;
                UIManager.PopPanel<PanelMapSelect>(new object[] { mapList, m_modeName, mapid }, true);
            }
            
        };
        m_triangleImage.TrySetActive(false);
        if (IsSupport(RoomFunc.FightState))
            m_fightstate = m_tran.Find("optionframe/fightstate/fight").GetComponent<Text>();
    }

    /// <summary>
    /// 检查地图是否是最新
    /// </summary>
    /// <param name="mapid"></param>
    /// <returns></returns>
    private bool CheckMapCRC(int mapid)
    {
        if (!Driver.isMobilePlatform)
            return true;
        if (mapid == m_lastCheckMapId)
            return m_lastCheckMapResult;
        m_lastCheckMapId = mapid;

        m_lastCheckMapResult = ResourceManager.CheckOutsideAssetsCrc(GetMapDependAssets(mapid));
        return m_lastCheckMapResult;
    }

    private int GetMapSize(int mapid)
    {
        return ResourceManager.GetAssetsSize(GetMapDependAssets(mapid));
    }

    private string[] GetMapDependAssets(int mapid)
    {
        var scenePath = "Scenes/" + PathHelper.GetBattleScenePath(mapid);
        var prefabPath = PathHelper.GetScenePrefabPath(mapid);
        var lightMapPath = PathHelper.GetLightMapPath(mapid);

        var lightProbePath = "";
        var configMap = ConfigManager.GetConfig<ConfigMapList>().GetLine(mapid.ToString());
        if (configMap != null && !configMap.LightProbe.IsNullOrEmpty())
            lightProbePath = PathHelper.GetLightProbePath(mapid, configMap.LightProbe);
        var paths = new[] { scenePath, prefabPath, lightMapPath, lightProbePath };
        return paths;
    }

    /// <summary>
    /// 根据RoomMode中数据更新地图界面显示
    /// </summary>
    private void UpdateMap()
    {
        if (!IsSupport(RoomFunc.SmallMap))
        {
            return;
        }
        var roomInfo = RoomModel.RoomInfo;
        var configs = ConfigManager.GetConfig<ConfigMapList>();

        MapNeedUpdate = !CheckMapCRC(RoomModel.RoomInfo.map);
        if (!IsHost)
        {
            if (MapNeedUpdate && IsReady)
                NetLayer.Send(new tos_room_ready {ready = false});
            else if (!MapNeedUpdate && UserChooseReady != IsReady)
                NetLayer.Send(new tos_room_ready { ready = UserChooseReady });
        }

        var spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, roomInfo.map.ToString());
        if (spriteMap == null || MapNeedUpdate)
            spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, "unknown");
        m_imgSmallMap.SetSprite(spriteMap);

        if (m_goDownloadPg)
            m_goDownloadPg.TrySetActive(MapDownloading);

        var newMap = configs.GetLine(roomInfo.map.ToString());
        if (MapDownloading)
        {
            var info = ResourceManager.DownloadProgressDic.GetValue(RoomModel.RoomInfo.map.ToString());
            if (m_imgDownloadPg)
                m_imgDownloadPg.fillAmount = info.Progress;
            m_txtMapName.text = "下载中" + (int)(info.Progress * 100) + "%";
        }
        else
            m_txtMapName.text = newMap != null ? newMap.MapName : "未知";
        if(newMap == null)
            UIManager.ShowTipPanel("请重启客户端，更新游戏到最新版本");

        m_goRightMap.TrySetActive(IsHost);
        m_goLeftMap.TrySetActive(IsHost);
        m_triangleImage.TrySetActive(IsHost);

        //矫正mapIndex
        for (int i = 0; i < configs.m_dataArr.Length; i++)
        {
            if (newMap != null && newMap.MapId == configs.m_dataArr[i].MapId)
            {
                m_mapIndex = i;
                break;
            }
        }

        if (IsSupport(RoomFunc.FightState))
        {
            if (roomInfo.state == 1)
            {
                m_fightstate.text = "等待加入";
            }
            else if (roomInfo.state == 2)
            {
                m_fightstate.text = "战斗中";
            }
            else if (roomInfo.state == 3)
            {
                m_fightstate.text = "快结束";
            }
        }
    }

    private void Update_Map()
    {
        if (MapDownloading)
        {
            var key = RoomModel.RoomInfo.map.ToString();
            var progress = ResourceManager.DownloadProgressDic.ContainsKey(key) ? ResourceManager.DownloadProgressDic[key].Progress : 0f;
            if (m_imgDownloadPg)
                m_imgDownloadPg.fillAmount = progress;
            m_txtMapName.text = "下载中" + (int)(progress * 100) + "%";
        }
        
    }

    /// <summary>
    /// 切换地图
    /// </summary>
    /// <param name="nextMap">true下一个，false上一个</param>
    private void ChangeMap(bool nextMap)
    {
        var arr = ConfigManager.GetConfig<ConfigMapList>().m_dataArr;
        var step = nextMap ? 1 : -1;

        //防止无限循环，设置最大2倍遍历长度
        for (int i = 0; i < arr.Length * 2; i++)
        {
            //使index循环
            int tempMapIndex = (m_mapIndex + (i + 1) * step + arr.Length * 2) % arr.Length;
            if (arr[tempMapIndex].isCanUse(m_roomRule.rule))
            {
                if (RoomModel.ChannelType == ChannelTypeEnum.custom && arr[tempMapIndex].HideInUserDefine)
                    continue;
                m_mapIndex = tempMapIndex;
                break;
            }
        }

        m_roomRule.map = int.Parse(arr[m_mapIndex].MapId);
    }

    private List<ConfigMapListLine> mapList = new List<ConfigMapListLine>(); 
    private void curModeMapList()
    {
        mapList.Clear();
        var arr = ConfigManager.GetConfig<ConfigMapList>().m_dataArr;
        for (int i = 0; i < arr.Length ; i++)
        {
            if (arr[i].isCanUse(m_roomRule.rule))
            {
                if (RoomModel.ChannelType == ChannelTypeEnum.custom && arr[i].HideInUserDefine)
                    continue;
                mapList.Add(arr[i]);
            }
        }
    }

    #endregion

    #region 自动踢人
    private void InitAutoKick()
    {
        m_cfgRemindTime = ConfigMisc.GetInt("room_idle_remind");
        m_cfgRemindKickTime = ConfigMisc.GetInt("room_idle_remind_kick");
    }

    private void Update_AutoKick()
    {
        if (Input.anyKeyDown || IsViewing || (!IsHost && IsReady))
            m_lastActiveTime = Time.realtimeSinceStartup;

        if (IsSupport(RoomFunc.AutoKick) && Time.realtimeSinceStartup > m_lastActiveTime + m_cfgRemindTime)
        {
            if (Time.realtimeSinceStartup > m_lastActiveTime + m_cfgRemindTime + m_cfgRemindKickTime)
            {
                UIManager.ShowTipPanel("由于您长时间未操作，系统将您移至大厅");
                UISystem.isNormalBack = true;
                UIManager.BackPanel();
            }
            else if (!UIManager.IsOpen<PanelTip>())
                UIManager.ShowTipPanel("您在" + m_cfgRemindTime + "秒内没有操作，即将回到大厅", null, null, false);
        }
    }
    #endregion

    #region 观战
    private void InitViewing()
    {
        if (!IsSupport(RoomFunc.Viewing))
            return;
        m_isspchat = true;
        m_lines = 0;
        m_lines = 0;
        m_texts = new List<Text>();
        m_sptxt = m_tran.Find("optionframe/viewing").gameObject;
        m_spnum = m_tran.Find("optionframe/viewnum").GetComponent<Text>();
        m_btnChangeMode = m_tran.Find("optionframe/fightstate/change_mod").GetComponent<Button>();
        m_txtChangeMode = m_tran.Find("optionframe/fightstate/change_mod/Text").GetComponent<Text>();
        m_spchat = m_tran.Find("optionframe/spector").gameObject;
        m_chatclose = m_tran.Find("optionframe/spector/chatclose").gameObject;
        m_chatclose.TrySetActive(false);
        m_spchatbubble = m_tran.Find("optionframe/spchat/Image").gameObject;
        m_spchatimage = m_tran.Find("optionframe/spchat").gameObject;
        m_chattxt = m_tran.Find("optionframe/spchat/Image/chat").gameObject;
        m_sps = m_tran.Find("optionframe/spector/num").GetComponent<Text>();
        UGUIClickHandler.Get(m_btnChangeMode.gameObject).onPointerClick += OnChangeModeClick;
        UGUIClickHandler.Get(m_spchat).onPointerClick += OnClickSpchat;
    }

    private void OnClickSpchat(GameObject target, PointerEventData eventData)
    {
        m_isspchat = !m_isspchat;
        m_spchatimage.TrySetActive(!m_spchatimage.activeSelf);
        m_chatclose.TrySetActive(!m_chatclose.activeSelf);
        TipsManager.Instance.showTips(!m_chatclose.activeSelf ? "您已打开观察者发言" : "您已屏蔽观察者发言");
    }

    private void OnChangeModeClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_room_change_spectator { spectator = !IsViewing });
    }

    private void Update_Viewing()
    {
        if (!IsSupport(RoomFunc.Viewing))
            return;
        m_spchatimage.TrySetActive(m_texts.Count > 0 && m_isspchat);
        if (Time.realtimeSinceStartup - m_spchatTime >= 3 && m_texts.Count > 0)
        {
            if (m_texts[0].preferredHeight > 40)
                m_lines -= 2;
            else
                m_lines -= 1;
            Object.Destroy(m_texts[0].gameObject);
            m_texts.RemoveAt(0);
        }
    }

    private void UpdateViewing()
    {
        if (!IsSupport(RoomFunc.Viewing))
            return;
        m_spchat.TrySetActive(RoomModel.RoomInfo.spectators.Length > 0);
        m_txtChangeMode.text = IsViewing ? "进入战斗" : "参加观战";

        m_sptxt.gameObject.TrySetActive(IsViewing);
        m_spnum.gameObject.TrySetActive(IsViewing);
        m_spnum.text = string.Format("当前观战人数{0}/50", RoomModel.RoomInfo.spectators.Length);
        m_sps.text = string.Format("观察者({0}/50)", RoomModel.RoomInfo.spectators.Length);
    }
    #endregion

    #region 聊天、常用语
    private void InitChat()
    {
        if (!IsSupport(RoomFunc.Chat))
            return;
        m_chat = m_tran.Find("pageframe/chatInput").GetComponent<InputField>();
        m_chat.gameObject.AddComponent<InputFieldFix>().onEndEdit += OnBtnChatClick;
        m_commonSpeakArr = new[]
        {
            new ItemDropInfo {type = "", subtype = "", label = "开开开开开！！！"},
            new ItemDropInfo {type = "", subtype = "", label = "请踢掉不准备的人！"},
            new ItemDropInfo {type = "", subtype = "", label = "可以换张图吗？"},
            new ItemDropInfo {type = "", subtype = "", label = "换个模式可以吗？"},
            new ItemDropInfo {type = "", subtype = "", label = "请等我一会，谢谢！"}
        };
        UGUIClickHandler.Get(m_tran.Find("pageframe/btnCommonSpeak")).onPointerClick += OnBtnCommonSpeakClick;
    }

    private void OnBtnChatClick(string content)
    {
        content = content.Replace("\n", "");
        content = content.Replace(" ", "");
        if (content == "")
            return;
        NetLayer.Send(new tos_room_chat { msg = WordFiterManager.Fiter(content) });
        m_chat.text = "";
    }

    private void OnBtnCommonSpeakClick(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowDropListUp(m_commonSpeakArr, info => NetLayer.Send(new tos_room_chat { msg = info.label }), m_tran, target, 130);
    }

    protected void Toc_room_chat_msg(toc_room_chat_msg proto)
    {
        if (!IsSupport(RoomFunc.Chat))
            return;
        var name = proto.name;
        var msg = proto.msg;
        if (GetMate(proto.id) != null)
        {
            AddInChatList(new chatRecord() { msg = proto.msg, id = proto.id, startTime = Time.time });
            CheckShowChat();
        }
        else
        {
            var go = UIHelper.Instantiate(m_chattxt) as GameObject;
            if (go != null)
            {
                var te = go.GetComponent<Text>();
                te.text = "<color=#70e1f9ff>[" + PlayerSystem.GetRoleNameFull(name, proto.id) + "]</color>" + msg;
                go.TrySetActive(true);
                go.transform.SetParent(m_spchatbubble.transform);
                go.transform.localScale = new Vector3(1f, 1f, 1f);
                float a = te.preferredHeight;
                int line = a >= 40 ? 2 : 1;

                if (m_lines + line > 4)
                {
                    while (m_lines + line > 4)
                    {
                        if (m_texts[0].preferredHeight > 40)
                        {
                            m_lines -= 2;
                        }
                        else
                        {
                            m_lines -= 1;
                        }
                        Object.Destroy(m_texts[0].gameObject);
                        m_texts.RemoveAt(0);
                    }
                }
                m_lines += line;
                m_texts.Add(te);
            }
            m_spchatTime = Time.realtimeSinceStartup;
        }
    }

    private ItemRoommate GetMate(long id)
    {
        var players = RoomModel.RoomInfo.players;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].id == id)
            {
                return m_mates[i];
            }
        }
        return null;
    }
    #endregion

    #region 自动瞄准开关
    private void InitAutoAim()
    {
        if (!IsSupport(RoomFunc.AutoAim))
        {
            return;
        }
        m_txtAim = m_tran.Find("optionframe/aimtext").GetComponent<Text>();
        m_toggleAutoAim = m_tran.Find("optionframe/toggleAutoAim").GetComponent<Toggle>();
        m_toggleAutoAimOffImg = m_tran.Find("optionframe/toggleAutoAim/offTick").GetComponent<Image>();
        m_toggleAutoAimOnImg = m_tran.Find("optionframe/toggleAutoAim/onTick").GetComponent<Image>();
        m_toggleAutoAimOnImg.gameObject.TrySetActive(true);
        m_toggleAutoAim.isOn = true;
        m_toggleAutoAimOffImg.enabled = false;
        m_toggleAutoAimOnImg.enabled = true;
        UGUIClickHandler.Get(m_toggleAutoAim.gameObject).onPointerClick += OnAutoAimClick;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_txtAim.gameObject.TrySetActive(false);
        m_toggleAutoAim.gameObject.TrySetActive(false);
#endif
    }

    private void OnAutoAimClick(GameObject target, PointerEventData eventData)
    {
        if (m_toggleAutoAim.interactable == false)
            return;
        AutoAim.Enable = m_toggleAutoAim.isOn && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
        m_toggleAutoAimOffImg.enabled = !m_toggleAutoAim.isOn;
        NetLayer.Send(new tos_room_auto_aim { auto_aim = m_toggleAutoAim.isOn });
    }

    private void UpdateAutoAim()
    {
        if (!IsSupport(RoomFunc.AutoAim))
        {
            return;
        }
        m_toggleAutoAim.gameObject.TrySetActive(!IsViewing);
        m_txtAim.gameObject.TrySetActive(!IsViewing);

        m_toggleAutoAim.interactable = IsHost;
        m_txtAim.color = IsHost ? new Color(1f, 1f, 1f) : new Color(0.63f, 0.63f, 0.63f);

        m_toggleAutoAimOffImg.enabled = !RoomModel.RoomInfo.auto_aim;
        m_toggleAutoAimOnImg.enabled = RoomModel.RoomInfo.auto_aim;
        AutoAim.Enable = RoomModel.RoomInfo.auto_aim && (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTOAIM) == GameConst.AUTOAIM_OPEN);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_txtAim.gameObject.TrySetActive(false);
        m_toggleAutoAim.gameObject.TrySetActive(false);
#endif
    }
    #endregion

    #region 房间密码
    private void InitPassword()
    {
        if (!IsSupport(RoomFunc.Password))
            return;
        m_txtPassword = m_tran.Find("optionframe/passwordtext").GetComponent<Text>();
        m_passin = m_tran.Find("optionframe/InputField").gameObject;
        m_ipt = m_tran.Find("optionframe/InputField/Text").GetComponent<Text>();
        m_passimage = m_tran.Find("optionframe/InputField/Image").GetComponent<Image>();
        m_passimage.enabled = false;
        UGUIClickHandler.Get(m_passin).onPointerClick += OnInputPasswordClick;
    }

    private void OnInputPasswordClick(GameObject target, PointerEventData eventdata)
    {
        if (IsHost)
        {
            UIManager.PopPanel<PanelPassward>(new object[] { m_password }, true, this).m_callBack = s =>
            {
                m_password = s;
                NetLayer.Send(new tos_room_password { password = m_password });
            };
        }
    }

    private void UpdatePassword()
    {
        if (!IsSupport(RoomFunc.Password))
            return;
        m_ipt.text = RoomModel.RoomInfo.password ? "已设置" : "未设置";
        m_passin.gameObject.TrySetActive(!IsViewing);
        m_txtPassword.gameObject.TrySetActive(!IsViewing);
        m_txtPassword.color = IsHost ? new Color(1.0f, 1.0f, 1.0f) : new Color(0.63f, 0.63f, 0.63f);
    }
    #endregion

    #region 模式选择
    private void InitRule()
    {
        if (IsSupport(RoomFunc.ChangeMode))
        {
            m_btnModeGo = m_tran.Find("optionframe/btnMode").gameObject;
            m_txtModeBtnName = m_tran.Find("optionframe/btnMode/Text").GetComponent<Text>();
            AddEventListener<string, string>(GameEvent.UI_ROOM_MODE_SELECTED, OnRuleChanged);
            UGUIClickHandler.Get(m_btnModeGo).onPointerClick += OnClickSelectMode;
        }

        if (IsSupport(RoomFunc.DisableMode))
        {
            m_modeDisableText = m_tran.Find("optionframe/modename/Text").GetComponent<Text>();
            m_modeDisable = m_tran.Find("optionframe/modename").gameObject;
        }
    }

    /// <summary>
    /// 将本地选择的规则应用到服务器上
    /// </summary>
    private void ApplyRoomRule()
    {
        m_roomRule.rule_list = new string[0];
        NetLayer.Send(m_roomRule);
    }

    private void OnRuleChanged(string rule, string people)
    {
        if (!IsOpen())
            return;
        if (RoomModel.ChannelType == ChannelTypeEnum.survival)
        {
            if (m_roomRule.rule != rule)
            {
                NetLayer.Send(new tos_room_level_limit() { level_limit = 0 });
            }
            
        }
        m_roomRule.rule = rule;

        if (RoomModel.ChannelType == ChannelTypeEnum.survival)
            m_roomRule.level = int.Parse(people);
        else
            m_roomRule.max_player = int.Parse(people);

        //如果旧地图不符合新模式，则重新选一个地图
        if (!ConfigManager.GetConfig<ConfigMapList>().m_dataArr[m_mapIndex].isCanUse(rule))
        {
            m_mapIndex = 0;
            ChangeMap(true);
        }
        ApplyRoomRule();
    }

    private void OnClickSelectMode(GameObject target, PointerEventData eventdata)
    {
        if (RoomModel.ChannelType == ChannelTypeEnum.survival)
        {
            int maxLevel = 0;
            var arr = PlayerSystem.survival_data.types_data;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].type == RoomModel.RoomInfo.rule)
                {
                    maxLevel = arr[i].top_level;
                    break;
                }
            }
            UIManager.PopPanel<PanelModeSelect>(new object[] { RoomModel.RoomInfo.rule, null, RoomModel.RoomInfo.level.ToString(), maxLevel }, true, this);
        }
        else
            UIManager.PopPanel<PanelModeSelect>(new object[] { RoomModel.RoomInfo.rule, RoomModel.RoomInfo.max_player.ToString(), null }, true, this);
    }

    private void UpdateMode()
    {
        if (IsSupport(RoomFunc.ChangeMode))
        {
            m_btnModeGo.TrySetActive(IsHost);
            if (IsSupport(RoomFunc.DisableMode))
                m_modeDisable.gameObject.TrySetActive(!IsHost);
            if (RoomModel.ChannelType == ChannelTypeEnum.survival)
                m_txtModeBtnName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name + "(" + PanelSurvivalRoom.m_levelNames[RoomModel.RoomInfo.level] + ")";
            else
                m_txtModeBtnName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name;
            m_modeName = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name;
            curModeMapList();
        }

        if (IsSupport(RoomFunc.DisableMode))
        {
            if (RoomModel.ChannelType == ChannelTypeEnum.survival)
                m_modeDisableText.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name + "(" + PanelSurvivalRoom.m_levelNames[RoomModel.RoomInfo.level] + ")";
            else
                m_modeDisableText.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name;
            m_modeName = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule).Mode_Name;
        }
        
    }
    #endregion

    #region 开始
    private void InitStart()
    {
        m_btnStartGo = m_tran.Find("optionframe/btnStart").gameObject;
        m_startORprepare = m_tran.Find("optionframe/btnStart/Text").GetComponent<Text>();
        UGUIClickHandler.Get(m_btnStartGo).onPointerClick += OnStartClick;
    }

    protected bool CheckAndUpdateMap()
    {
        if (MapDownloading)
            return true;
        else if (MapNeedUpdate)
        {
            var mapSize = GetMapSize(RoomModel.RoomInfo.map);
            UIManager.ShowTipPanel("更新本张地图约需要" + mapSize + "KB，可能会消耗流量，是否加载？", () =>
            {
                var paths = GetMapDependAssets(RoomModel.RoomInfo.map);
                ResourceManager.DownloadAsset(paths, OnProgressCallBack, RoomModel.RoomInfo.map.ToString());
                MapDownloading = true;
                UpdateView();

            }, null, true, true, "确认加载");
            return true;
        }
        else
            return false;
    }

    protected virtual void OnStartClick(GameObject target, PointerEventData eventdata)
    {
        if (!RoomModel.IsInRoom)
            return;

        if (CheckAndUpdateMap())
            return;

        if (IsSupport(RoomFunc.RankMatching))
        {
            if (IsHost)
            {
                NetLayer.Send(new tos_room_start_match());
                UIManager.PopPanel<PanelMatchingTips>(null, true);
            }
        }
        else if (IsSupport(RoomFunc.WorldBoss))
        {
            if (IsHost)
            {
                if (PanelWorldBoss.m_isOver)
                {
                    UIManager.ShowTipPanel("活动已结束");
                    return;
                }
                else if(RoomModel.RoomInfo.rule == GameConst.GAME_RULE_WORLD_BOSS)
                    NetLayer.Send(new tos_room_start_game());
                else if (RoomModel.RoomInfo.rule == GameConst.GAME_RULE_BOSS_NEWER)
                    NetLayer.Send(new tos_room_start_match());
            }
        }
        else
        {
            if (IsHost && !RoomModel.RoomInfo.fight)
                NetLayer.Send(new tos_room_start_game());
            else if (RoomModel.RoomInfo.fight)
                NetLayer.Send(new tos_room_join_game());
            else
            {
                NetLayer.Send(new tos_room_ready {ready = !IsReady});
                UserChooseReady = !IsReady;
            }
        }
    }

    private static void OnProgressCallBack(string key, DownloadAssetsInfo info)
    {
        var mapDownloadComplete = false;
        if (Mathf.Approximately(info.Progress, 1))
        {
            mapDownloadComplete = true;
            m_mapDownloadingDic[int.Parse(key)] = false;
        }
        else
            m_mapDownloadingDic[int.Parse(key)] = true;

        //加载进度回调如果不是当前地图的，则不显示
        if (m_currentRoom == null || !m_currentRoom.IsInited() || !RoomModel.IsInRoom || RoomModel.RoomInfo.map.ToString() != key)
            return;

        if (mapDownloadComplete)
        {
            m_currentRoom.m_lastCheckMapId = 0;
            m_currentRoom.UpdateView();
        }
        else
        {
            if (m_currentRoom.m_imgDownloadPg)
                m_currentRoom.m_imgDownloadPg.fillAmount = info.Progress;
            m_currentRoom.m_txtMapName.text = "下载中" + (int)(info.Progress * 100) + "%";
        }
    }

    private void UpdateStart()
    {
        if (IsSupport(RoomFunc.RankMatching))
        {
            return;
        }
        if (MapDownloading)
            m_startORprepare.text = "下载中";
        else if (MapNeedUpdate)
            m_startORprepare.text = "加载地图";
        else if (RoomModel.RoomInfo!=null&&RoomModel.RoomInfo.fight)
            m_startORprepare.text = "进入";
        else if (IsHost)
            m_startORprepare.text = "开始";
        else
            m_startORprepare.text = IsReady ? "取消准备" : "准备";
    }
    #endregion

    #region 邀请好友
    private void InitInviteFriend()
    {
        if (!IsSupport(RoomFunc.InviteFriend))
            return;
        m_btnInviteFriendGo = m_tran.Find("optionframe/btnInviteFriend").gameObject;
        UGUIClickHandler.Get(m_btnInviteFriendGo).onPointerClick += OnClickInviteFriend;
    }

    private void UpdateInvite()
    {
        if (!IsSupport(RoomFunc.InviteFriend))
            return;
        m_btnInviteFriendGo.GetComponent<Button>().interactable = !IsViewing;
    }

    virtual protected void OnClickInviteFriend(GameObject target, PointerEventData eventdata)
    {
        if (IsViewing)
            return;
        int stage = 0;
        string stageType = "";
        if (RoomModel.ChannelType == ChannelTypeEnum.stage)
        {
            stage = PanelPassRoom.m_iStageId;
            stageType = ChannelTypeEnum.stage.ToString();
        }
        else if (RoomModel.ChannelType == ChannelTypeEnum.stage_survival)
        {
            stage = PanelZombieRoom.stageId;
            stageType = ChannelTypeEnum.stage_survival.ToString();
        }
        NetLayer.Send(new tos_player_room_invitelist { is_friend = true, stage = stage, stage_type = stageType });
    }
    #endregion

    #region 阵营变更
    private void InitCampChange()
    {
        if (IsSupport(RoomFunc.RankMatching))
        {
            return;
        }
        if (!IsSupport(RoomFunc.ChangeCamp))
            return;
        if (IsSupport(RoomFunc.ChangeVsIcon))
            m_imgvs = m_tran.Find("pageframe/imgvs").GetComponent<Image>();
        m_btnChangeCamp = m_tran.Find("pageframe/btnChangeCamp").GetComponent<Button>();
        UGUIClickHandler.Get(m_btnChangeCamp.gameObject).onPointerClick += OnChangeCampClick;
        UGUIClickHandler.Get(m_tran.Find("pageframe/head/team1Bg")).onPointerClick += delegate
        {
            var mode = RoomModel.RoomInfo.rule;
            if (mode != "zombie" && mode != "solo" && mode != "king_solo")
                NetLayer.Send(new tos_room_side { side = 1 });
        };
        UGUIClickHandler.Get(m_tran.Find("pageframe/head/team2Bg")).onPointerClick += delegate
        {
            var mode = RoomModel.RoomInfo.rule;
            if (mode != "zombie" && mode != "solo" && mode != "king_solo")
                NetLayer.Send(new tos_room_side { side = 2 });
        };
    }

    private void OnChangeCampClick(GameObject target, PointerEventData eventData)
    {
        if (m_self == null)
            return;
        var mode = RoomModel.RoomInfo.rule;
        if (mode != "zombie" && mode != "solo" && mode != "king_solo")
            NetLayer.Send(new tos_room_side { side = (m_self.m_camp == 1 ? 1 : 2) });
    }

    private void UpdateCampChange()
    {
        if (!IsSupport(RoomFunc.ChangeCamp))
            return;
        //vs标志
        if (IsSupport(RoomFunc.RankMatching))
        {
            return;
        }
        var mode = RoomModel.RoomInfo.rule;
        var vsImageVisible = mode == GameConst.GAME_RULE_SOLO || mode == GameConst.GAME_RULE_KING_SOLO || mode == GameConst.GAME_RULE_ZOMBIE || mode == GameConst.GAME_RULE_ZOMBIE_HERO || mode == GameConst.GAME_RULE_ZOMBIE_ULTIMATE;
        m_btnChangeCamp.gameObject.TrySetActive(!vsImageVisible);

        if (IsSupport(RoomFunc.ChangeVsIcon))
            m_imgvs.gameObject.TrySetActive(vsImageVisible);
    }
    #endregion

    #region 自动开始
    private void InitAutoStart()
    {
        m_resttime = -1;
        m_startTime = -1.0f;
    }

    private void Update_AutoStart()
    {
        if (!IsSupport(RoomFunc.AutoStart) || m_startTime <= 0)
            return;

        if (UIManager.IsOpen<PanelModeSelect>())
        {
            m_startTime = -1.0f;
            return;
        }
        if (RoomModel.RoomInfo.fight)
        {
        }
        else if (m_self != null && m_self.IsHost)
        {

            if (Time.realtimeSinceStartup - m_startTime >= 10.0f && m_startTime > 0)
            {
                m_startTime = -1.0f;
            }
            else if (m_startTime > 0)
            {
                if (10 - Convert.ToInt32(Time.realtimeSinceStartup - m_startTime) != m_resttime)
                {
                    m_resttime = 10 - Convert.ToInt32(Time.realtimeSinceStartup - m_startTime);
                    // PanelRoomList.SendMsg("chat", "游戏将在"+m_resttime.ToString()+"秒后自动开始");
                    //NetLayer.Send(new tos_room_chat() { msg = "游戏将在" + m_resttime.ToString() + "秒后自动开始" });
                }
            }
        }
    }

    private void UpdateAutoStart()
    {
        if (!IsSupport(RoomFunc.AutoStart))
            return;

        var roomInfo = RoomModel.RoomInfo;
        if (roomInfo.fight || CheckCanStart())
        {
            m_startTime = Time.realtimeSinceStartup;
            m_resttime = 10;
        }
    }

    private bool CheckCanStart()
    {
        if (!RoomModel.IsInRoom)
            return false;
        var roomInfo = RoomModel.RoomInfo;

        var playernum = new int[2];
        playernum[0] = 0;
        playernum[1] = 0;
        foreach (var player in roomInfo.players)
        {
            playernum[player.pos % 2]++;
            if (!player.ready && player.id != roomInfo.leader)
            {
                return false;
            }
        }
        if (playernum[0] == 0 && playernum[1] == 0)
        {
            return false;
        }
        return true;
    }
    #endregion

    protected virtual void UpdateView()
    {
        UpdateMap();
        UpdateAutoAim();
        UpdatePassword();
        UpdateInvite();
        UpdateMode();
        UpdateStart();
        UpdateViewing();
        UpdateCampChange();
        UpdateRoommate();
        UpdateAutoStart();
    }

    private void UpdateData()
    {
        if (!RoomModel.IsInRoom)
            return;

        var myId = PlayerSystem.roleId;
        var roomInfo = RoomModel.RoomInfo;

        m_roomRule.map = roomInfo.map;
        m_roomRule.max_player = roomInfo.max_player;
        m_roomRule.rule = roomInfo.rule;
        m_roomRule.level = roomInfo.level;

        //观战对象
        m_selfView = null;
        for (int i = 0; i < roomInfo.spectators.Length; i++)
        {
            if (myId == roomInfo.spectators[i].id)
            {
                m_selfView = roomInfo.spectators[i];
                break;
            }
        }

        //玩家对象
        m_selfPlay = null;
        for (int i = 0; i < roomInfo.players.Length; i++)
        {
            if (myId == roomInfo.players[i].id)
            {
                m_selfPlay = roomInfo.players[i];
                break;
            }
        }
    }

    protected void Toc_room_info(toc_room_info proto)
    {
        UpdateData();
        UpdateView();
        CheckShowChat();

    }

    /// <summary>
    /// 主要处理房间中被踢
    /// </summary>
    /// <param name="data"></param>
    protected void Toc_room_leave_room(toc_room_leave_room data)
    {        
        UISystem.isNormalBack = true;
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.SetNormalBack();
        if (RoomModel.RoomLeaveType == RoomLeaveType.Kicked)
        {            
            TipsManager.Instance.showTips("您已被踢出房间");
            if(m_currentRoom.IsOpen())
                OnBack();
        }
        else if (RoomModel.RoomLeaveType == RoomLeaveType.TimeOut)
        {
            UIManager.ShowTipPanel("由于您长时间未操作，系统将您移至大厅");
            if (m_currentRoom.IsOpen())
                OnBack();
        }
        else
        {
            SetPopListPanelCallBack(false, OnClickBeyoundPopList);
            UIManager.HideDropList();
            UIManager.ShowPanel<PanelHallBattle>();
        }
    }


    static void Toc_room_match_time(toc_room_match_time data)
    {
        if (data.match_time == 0)
        {
            UIManager.HidePanel<PanelMatchingTips>();
            if (data.name != PlayerSystem.roleData.name)
                TipsManager.Instance.showTips(PlayerSystem.GetRoleNameFull(data.name, data.id) + "取消了匹配");
        }
        else
        {
            UIManager.PopPanel<PanelMatchingTips>(null, true);

        }
    }

    static void Toc_room_notice_leave(toc_room_notice_leave data )
    {
        if (!GuideManager.IsGuideFinish() || GuideManager.IsGuiding())
        {
            if (UISystem.isMinimizeRoom)
                ShowMinimizeRoom();
            var panelHall = UIManager.GetPanel<PanelHall>();
            if (panelHall != null)
                panelHall.SetNormalBack();
            NetLayer.Send(new tos_room_notice_leave());
            return;
        }
        else
        {
            string okContent = UISystem.isMinimizeRoom ? "回到房间" : "待在房间";
            UIManager.ShowTipPanel(string.Format("因为你长时间没有操作，即将被踢出房间"), () =>
            {
                if (UISystem.isMinimizeRoom)
                    ShowMinimizeRoom();
                var panelHall = UIManager.GetPanel<PanelHall>();
                if (panelHall != null)
                    panelHall.SetNormalBack();
                NetLayer.Send(new tos_room_notice_leave());
            }, () =>
            {
                var panelHall = UIManager.GetPanel<PanelHall>();
                if (panelHall != null)
                    panelHall.SetNormalBack();
                NetLayer.Send(new tos_room_leave());
            }, false, true, okContent, "离开房间");
        }
    }

    static void Toc_room_match_punished(toc_room_match_punished data)
    {
        var length = data.list.Length;
        if( length == 0)
            return;
        var data_max = data.list[0];
        int max_time = data_max.release_time;
        
        if(length > 1)
        {
            for (int i = 1; i < data.list.Length; i++)
            {
                if (data.list[i].release_time > max_time)
                {
                    max_time = data.list[i].release_time;
                    data_max = data.list[i];
                }
            }
        }
        roomPunish punishData = new roomPunish()
        {
            name = data_max.name,
            player_id = data_max.player_id,
            release_time = data_max.release_time
        };
        if (punishData.release_time != 0 && TimeUtil.GetRemainTimeType((uint)punishData.release_time) != -1)
        {
            UIManager.HidePanel<PanelMatchingTips>();
            UIManager.PopPanel<PanelTeamRankPunish>(new object[] { punishData }, true);
        }
    }

    public void CheckShowChat()
    {
        for (int i = 0; i < chatList.Count; i++)
        {
            if (Time.time - chatList[i].startTime <= 3)
            {
                if (GetMate(chatList[i].id)!= null)
                {
                    GetMate(chatList[i].id).ShowChat(chatList[i]);
                }
                else
                {
                    chatList.RemoveAt(i);
                    i--;
                }
            }
            else
            {
                chatList.RemoveAt(i);
                i--;
            }
        }
    }

    void AddInChatList(chatRecord cr)
    {
        for (int i = 0; i < chatList.Count; i++)
        {
            if (chatList[i].id == cr.id)
            {
                chatList.RemoveAt(i);
                chatList.Add(cr);
                return;
            }
        }
        chatList.Add(cr);
    }

}