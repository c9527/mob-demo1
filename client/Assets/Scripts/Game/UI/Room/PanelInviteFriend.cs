﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/*
class InviteItem
{

    private GameObject m_goItem;

    Image m_imageHead;
    Image m_imageArmyLvl;

    Text m_textName;
    Image m_textOnline;

    Transform m_btnInvite;
    public int m_iUserData;
    PanelInviteFriend.ListItemData m_kData;

    public PanelInviteFriend m_parentPanel;

    static Color ONLINE_COLOR = new Color( 72,255,0 );
    static Color OFFLINE_COLOR = new Color(210, 210, 210);

    static string ONLINE_NAME = "在线";
    static string OFFLINE_NAME = "离线";

    public void Init( GameObject go  )
    {
        m_kData = new PanelInviteFriend.ListItemData();
        m_goItem = go;
        m_iUserData = 0;
        m_imageHead = m_goItem.transform.Find("headframe/head").gameObject.GetComponent<Image>();
        m_imageArmyLvl = m_goItem.transform.Find("army").gameObject.GetComponent<Image>();

        m_textName = m_goItem.transform.Find("slide/name").gameObject.GetComponent<Text>();
        m_textOnline = m_goItem.transform.Find("online").gameObject.GetComponent<Image>();
        m_btnInvite = m_goItem.transform.Find("invite");


        UGUIClickHandler.Get(m_btnInvite).onPointerClick += OnClickInvite;

    }

    public void SetHead( Sprite headSprite )
    {
        m_imageHead.SetSprite(headSprite);
    }

    public void SetArmyLvl( Sprite armySprite )
    {
        m_imageArmyLvl.SetSprite(armySprite);
    }

    public void SetName( string name )
    {
        m_textName.text = name;
    }

    public void SetOnlineState( bool online )
    {
        if( online )
        {
            m_textOnline.enabled = true;
        }
        else
        {
            m_textOnline.enabled = false;
        }        
    }

    public void SetUserData( int data )
    {
        m_iUserData = data;
    }

    void OnClickInvite( GameObject target, PointerEventData eventData )
    {
        if( m_parentPanel != null && m_parentPanel.selectCallBack != null )
        {
            //m_parentPanel.selectCallBack( m_kData );
        }

    }

    public void OnDispose()
    {
        UGUIClickHandler.Get(m_btnInvite).onPointerClick -= OnClickInvite;
        m_goItem.TrySetActive(false);
        m_goItem.transform.SetParent(null, false);
        GameObject.DestroyObject(m_goItem);
    }

    public void SetData( PanelInviteFriend.ListItemData kData )
    {
        m_kData = kData;
    }
}
*/

public class PanelInviteFriend : BasePanel 
{
    static public readonly int LISTTYPE_FRIEND = 1;
    static public readonly int LISTTYPE_MEMBER = 2;

    private int _list_value;

	// Use this for initialization
    List<Toggle> _tab_group;
    Toggle m_tabFrinedList;
    Toggle m_tabTeamList;

    GameObject m_toggleFriendList;
    GameObject m_toggleCorpsList;

    //GameObject m_goProtoItem;
    Transform m_goContentRect;

    //List<InviteItem> m_listInvite;

    Transform m_btnClose;

    public delegate void SelectCallBack(PanelInviteFriend.ListItemData selectData);

    private static List<ListItemData> m_frinedList = new List<ListItemData>();
    private static List<ListItemData> m_corpsList = new List<ListItemData>();

    GameObject m_goNoFriendsTips;
    GameObject m_goNoCorpsTips;

    private DataGrid m_dataGrid;
    //private static toc_player_stage_invitelist.PlayerInfo[] m_arrFriendList;
    //private static toc_player_stage_invitelist.PlayerInfo[] m_arrCorpsList;

    private Button _invite_btn;
    private Image _cd_mask;
    private Image _cost;
    private Text _num;

    static private float _INVITE_LIMIT = 60;
    static private float _LAST_INVITE_TIME;
    private int cost;
    private string coin;

    public class ListItemData
    {
        public string m_playerName;
        public int m_armyLvl;
        public int m_icon;
        public bool m_isOnline;
        public long m_iPlayerId;
        public bool m_bInFight;
        public bool m_bInRoom;
        public int m_mvp;
        public int m_ace;
        public MemberManager.MEMBER_TYPE m_eType;
    }

    public PanelInviteFriend()
    {
        SetPanelPrefabPath("UI/Room/PanelInviteFriend");
        AddPreLoadRes("UI/Room/PanelInviteFriend", EResType.UI);
        AddPreLoadRes("UI/Room/ItemInvite", EResType.UI);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
    }


    public override void Init()
    {
        //m_listInvite = new List<InviteItem>();
        m_toggleFriendList = m_go.transform.Find("frame/tabbar/tabfriendlist").gameObject;
        m_tabFrinedList = m_toggleFriendList.GetComponent<Toggle>();

        m_toggleCorpsList = m_go.transform.Find("frame/tabbar/tabcorpsmate").gameObject;
        m_tabTeamList = m_toggleCorpsList.GetComponent<Toggle>();

        _tab_group = new List<Toggle>();
        _tab_group.Add(m_tabFrinedList);
        _tab_group.Add(m_tabTeamList);

        //m_goProtoItem = m_go.transform.Find("frame/ScrollRect/item").gameObject;

        //m_goContentRect = m_go.transform.Find("frame/ScrollDataGrid/content");

        //RectTransform inviteRectTransform = m_goContentRect as RectTransform;
        //Vector2 pivot = new Vector2(0.5f, 1);
        //inviteRectTransform.anchorMin = pivot;
        //inviteRectTransform.anchorMax = pivot;
        //inviteRectTransform.pivot = pivot;

        m_btnClose = m_go.transform.Find("frame/close");

        m_goNoFriendsTips = m_tran.Find("frame/ScrollDataGrid/friendtips").gameObject;
        m_goNoCorpsTips = m_tran.Find("frame/ScrollDataGrid/corpstips").gameObject;
        m_tabFrinedList.isOn = true;

        m_dataGrid = m_tran.Find("frame/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Room/ItemInvite"), typeof(ItemInviteData));

        _invite_btn = m_tran.Find("frame/invite_btn").GetComponent<Button>();
        _cd_mask = m_tran.Find("frame/invite_btn/CDMask").GetComponent<Image>();
        _cost = m_tran.Find("frame/invite_btn/Image").GetComponent<Image>();
        _num = m_tran.Find("frame/invite_btn/num").GetComponent<Text>();

        ConfigMisc cfm = ConfigManager.GetConfig<ConfigMisc>();
        var data = cfm.GetLine("world_invite_cost");
        var list = data.ValueStr.Split(';');
        
        list[0] = list[0].Remove(0, 1);
        list[0] = list[0].Remove(list[0].Length - 1, 1);
        
        if(list[0]=="coin")
        {
            _cost.SetSprite(ResourceManager.LoadSprite(AtlasName.Common,"gold"));
        }
        else if(list[0] == "diamond")
        {
            _cost.SetSprite(ResourceManager.LoadSprite(AtlasName.Common,"diamond"));
        }
        else if(list[0] == "medal")
        {
            _cost.SetSprite(ResourceManager.LoadSprite(AtlasName.Common,"medal"));
        }
        coin = list[0];
        cost = int.Parse(list[1]);
        _num.text = list[1];



        _cd_mask.fillAmount = 0;

        //OnClickShowFriendList(null, null);
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += OnClickClose;
        UGUIClickHandler.Get(m_toggleFriendList).onPointerClick += OnClickFriend;
        UGUIClickHandler.Get(m_toggleCorpsList).onPointerClick += OnClickCorps;
        UGUIClickHandler.Get(_invite_btn.gameObject).onPointerClick += OnInviteBtnClick;
    }

    private void OnInviteBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_LAST_INVITE_TIME != 0 && Time.time < (_LAST_INVITE_TIME + _INVITE_LIMIT))
        {
            return;
        }

        if (coin == "coin")
        {
            if (PlayerSystem.roleData.coin < cost)
            {
                TipsManager.Instance.showTips("金币不足");
                return;
            }
        }
        else if (coin == "diamond")
        {
            if (PlayerSystem.roleData.diamond < cost)
            {
                TipsManager.Instance.showTips("钻石不足");
                return;
            }
        }
        else if (coin == "medal")
        {
            if (PlayerSystem.roleData.medal < cost)
            {
                TipsManager.Instance.showTips("奖章不足");
                return;
            }
        }

        var msg = "";
        var channel = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        if (channel != null 
            && channel.Type != ChannelTypeEnum.match
            && channel.Type != ChannelTypeEnum.corpsmatch
            && channel.Type != ChannelTypeEnum.corpsmatch_group
            )
        {
            var rule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.RoomInfo.rule);
            var map = ConfigManager.GetConfig<ConfigMapList>().GetLine(RoomModel.RoomInfo.map.ToString());
            msg = (channel != null ? channel.Name : "") + "，" + (map != null ? map.MapName : "") + "-" + (rule != null ? rule.Mode_Name : "") + (RoomModel.RoomInfo.password ? "(加密房间)" : "") + "，一起来战吧！";
        }
        else
        {
            msg = (channel != null ? channel.Name : "") + "，一起来战吧！";
        }
        msg += string.Format("<color=#8600FFFF><a=apply_battle|{0}|{1}|{2}>加入房间</a></color>", RoomModel.ChannelId, RoomModel.RoomInfo.id, RoomModel.RoomInfo.password);
        //if (NetChatData.SndChatInfo((int)CHAT_CHANNEL.CC_LOCALSERVER, 0, msg))
        //{
        //    _LAST_INVITE_TIME = Time.time;
        //}
        
        if (NetChatData.CanChat())
        {
            NetChatData.SndChatInfo((int)CHAT_CHANNEL.CC_LOCALSERVER, 0, msg, null, 0, 1, "world_invite");
            _LAST_INVITE_TIME = Time.time;
        }
        else
        {
            TipsManager.Instance.showTips("达到"+NetChatData.Get_lower_lvl()+"级后才能在世界频道发言");
        }
    }

    public override void OnShow()
    {
        _list_value = 0;
        if (HasParams())
        {
            _list_value = (int)m_params[0];
        }
        UpdateView();
        var channel = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        _invite_btn.gameObject.TrySetActive(channel!=null && channel.DisableWorldInvite==0);
    }
    public override void OnHide()
    {
        foreach (var toggle in _tab_group)
        {
            toggle.isOn = false;
        }
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        m_frinedList.Clear();
        m_corpsList.Clear();
    }

    public override void Update()
    {
        if (_LAST_INVITE_TIME == 0)
        {
            return;
        }
        var pass_time = Time.time - _LAST_INVITE_TIME;
        _cd_mask.fillAmount = 1 - pass_time / _INVITE_LIMIT;
        if (pass_time >= _INVITE_LIMIT)
        {
            _LAST_INVITE_TIME = 0;
        }
    }

    void OnClickClose( GameObject target, PointerEventData eventData )
    {
        HidePanel();
    }

    void OnClickFriend( GameObject target, PointerEventData eventData  )
    {
        UpdateView();
    }
    
    void OnClickCorps( GameObject target, PointerEventData eventData  )
    {
        if( m_corpsList.Count == 0 )
        {
            tos_player_room_invitelist msg = new tos_player_room_invitelist();
            msg.is_friend = false;
            if (RoomModel.ChannelType == ChannelTypeEnum.stage)
            {
                msg.stage = PanelPassRoom.m_iStageId;
                msg.stage_type = ChannelTypeEnum.stage.ToString();
            }
            else if (RoomModel.ChannelType == ChannelTypeEnum.stage_survival)
            {
                msg.stage = PanelZombieRoom.stageId;
                msg.stage_type = ChannelTypeEnum.stage_survival.ToString();
            }
            else
            {
                msg.stage = 0;
                msg.stage_type = "";
            }
            NetLayer.Send(msg);
            return;
        }

        UpdateView();
    }

    public void OnInvited( ListItemData item )
    {
        List<ListItemData> lstItem = null;
        if (m_tabFrinedList.isOn == true)
            lstItem = m_frinedList;
        else
            lstItem = m_corpsList;

        for( int i = 0 ; i < lstItem.Count; ++i )
        {
            if( lstItem[i].m_iPlayerId == item.m_iPlayerId )
            {
                lstItem.RemoveAt(i);
                break;
            }
        }

        UpdateView();
    }

    public void UpdateView()
    {
        m_goNoFriendsTips.TrySetActive(false);
        m_goNoCorpsTips.TrySetActive(false);
        m_tabFrinedList.gameObject.TrySetActive(_list_value == 0 || (_list_value & LISTTYPE_FRIEND)>0);
        m_tabTeamList.gameObject.TrySetActive(_list_value == 0 || (_list_value & LISTTYPE_MEMBER) > 0);
        Toggle selected_toggle = null;
        foreach (var toggle in _tab_group)
        {
            if (toggle.IsActive())
            {
                if (selected_toggle == null || toggle.isOn)
                {
                    selected_toggle = toggle;
                }
            }
            else
            {
                toggle.isOn = false;
            }
        }
        if (selected_toggle!=null && selected_toggle.isOn==false)
        {
            selected_toggle.isOn = true;
        }
        if( m_tabFrinedList.isOn == true )
        {
            m_dataGrid.Data = m_frinedList.ToArray();
            if(m_frinedList.Count == 0)
                m_goNoFriendsTips.TrySetActive(true);          
        }
        else if(m_tabTeamList.isActiveAndEnabled==true)
        {
            m_dataGrid.Data = m_corpsList.ToArray();
            if (m_corpsList.Count == 0)
                m_goNoCorpsTips.TrySetActive(true);
        }
    }

    private static void Toc_player_room_invitelist(toc_player_room_invitelist proto)
    {
        List<ListItemData> lslItem;
        if (proto.is_friend)
            lslItem = m_frinedList;
        else
            lslItem = m_corpsList;

        lslItem.Clear();
        for (int i = 0; i < proto.list.Length; ++i )
        {
            ListItemData newItem = new ListItemData();
            newItem.m_iPlayerId = proto.list[i].id;
            newItem.m_playerName = proto.list[i].name;
            newItem.m_bInFight = proto.list[i].fight;
            newItem.m_armyLvl = proto.list[i].level;
            newItem.m_icon = proto.list[i].icon;
            newItem.m_isOnline = proto.list[i].handle > 0;
            newItem.m_mvp = proto.list[i].mvp;
            newItem.m_ace = proto.list[i].gold_ace;
            newItem.m_eType = (MemberManager.MEMBER_TYPE)proto.list[i].vip_level;
            if (proto.list[i].room != 0 || proto.list[i].team != 0)
                newItem.m_bInRoom = true;
            else
                newItem.m_bInRoom = false;
                    
            if (newItem.m_isOnline)
                lslItem.Add(newItem);
        }

        if (UIManager.IsOpen<PanelInviteFriend>())
            UIManager.GetPanel<PanelInviteFriend>().UpdateView();
        else
            UIManager.PopPanel<PanelInviteFriend>(new object[] { RoomModel.RoomInfo != null && (RoomModel.ChannelType == ChannelTypeEnum.defend || RoomModel.ChannelType == ChannelTypeEnum.corpsmatch_group) ? 2 : 0 }, true);
    }
}
