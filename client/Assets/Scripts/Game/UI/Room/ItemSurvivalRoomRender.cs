﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemSurvivalRoomRender : ItemRender
{
    private int m_id;
    private Text m_txtHostName;
    private Text m_txtMapName;
    private Text m_txtModeName;
    public Text m_txtStateName;
    public Text m_txtLevel;
    private Vector2 m_stateTextPos;
    private RectTransform m_tranState;
    public Text m_txtRoundName;
    private Text m_txtPeopleNum;
    public int m_people;
    public int m_maxpeople;
    private Toggle m_toggle;
    

    private string m_mapId;
    private Image m_imgLock;
    private Image m_imgHeroCardBg;
    public int m_spnum;
    string m_strGameType;
    public p_room_info m_data;
    GameObject m_select;

    private Color m_selectedColor;
    private Color m_unSelectedColor;
    private Text m_limit;

    public override void Awake()
    {
        var tran = transform;
        m_txtHostName = tran.Find("hostname").GetComponent<Text>();
        m_txtMapName = tran.Find("mapname").GetComponent<Text>();
        m_txtModeName = tran.Find("modename").GetComponent<Text>();
        m_txtStateName = tran.Find("statename").GetComponent<Text>();
        m_txtLevel = tran.Find("level").GetComponent<Text>();
        m_txtRoundName = tran.Find("round").GetComponent<Text>();
        m_txtPeopleNum = tran.Find("peoplenum").GetComponent<Text>();
        m_imgLock = tran.Find("lock").GetComponent<Image>();
        m_imgHeroCardBg = tran.Find("roomBlock").GetComponent<Image>();
        m_toggle = tran.GetComponent<Toggle>();
        m_toggle.onValueChanged.AddListener(OnValueChanged);
        m_select = tran.Find("selectBg").gameObject;
        m_limit = tran.Find("Text").GetComponent<Text>();
        HasPassward = false;

        m_unSelectedColor = new Color(0.93f, 0.93f, 0.93f);
        m_selectedColor = new Color(1f, 0.97f, 0.6f);

        m_tranState = m_txtStateName.rectTransform;
        m_stateTextPos = m_tranState.anchoredPosition;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        UGUIClickHandler.Get(gameObject).onDoublePointerClick += onDoubleClickItemHandler;
#endif
    }
    private void onDoubleClickItemHandler(GameObject target, PointerEventData eventData)
    {
        GameDispatcher.Dispatch<p_room_info>(GameEvent.UI_DOUBLE_CLICK_ROOMLIST, m_data);
    }
    private void OnValueChanged(bool selected)
    {
        SetColor(selected);
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_room_info;
        m_id = m_data.id;
        m_txtHostName.text = PlayerSystem.GetRoleNameFull(m_data.leader, m_data.leader_id);
        m_mapId = m_data.map.ToString();
        m_txtMapName.text = ConfigManager.GetConfig<ConfigMapList>().GetLine(m_mapId).MapName;
        m_txtModeName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(m_data.rule).Mode_Name;
        m_txtLevel.text = string.Format("({0})",PanelSurvivalRoom.m_levelNames[m_data.level]);
        roomGameType = m_data.rule;
        if (m_data.state==2)
        {
            m_tranState.anchoredPosition = m_stateTextPos;
            m_txtStateName.text = "战斗中";
            m_txtRoundName.text = string.Format("{0}/{1}回合", m_data.cur_round, m_data.max_round);
        }
        else if(m_data.state==1)
        {
            m_tranState.anchoredPosition = new Vector2(m_stateTextPos.x, 0);
            m_txtStateName.text = "等待加入";
            m_txtRoundName.text = "等待加入";
        }
        else if (m_data.state == 3)
        {
            m_tranState.anchoredPosition = m_stateTextPos;
            m_txtStateName.text = "快结束";
            m_txtRoundName.text = "快结束";
        }
        
        
        HasPassward = m_data.password == true;
        m_people = m_data.player_count;
        m_maxpeople = m_data.max_player;
        m_txtPeopleNum.text = string.Format("{0}/{1}", m_data.player_count, m_data.max_player);
        m_spnum = m_data.spectators_count;
        SetColor(m_toggle.isOn);

        m_imgHeroCardBg.gameObject.TrySetActive(false);
        if (m_data.leader_hero != (int)EnumHeroCardType.none)
        {
            m_imgHeroCardBg.gameObject.TrySetActive(true);
            if (m_data.leader_hero == (int)EnumHeroCardType.normal)
            {
                m_imgHeroCardBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.ROOM_NORMAL));
            }
            else if (m_data.leader_hero == (int)EnumHeroCardType.super)
            {
                m_imgHeroCardBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.ROOM_SUPER));
            }
            else if (m_data.leader_hero == (int)EnumHeroCardType.legend)
            {
                m_imgHeroCardBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.ROOM_LEGEND));
            }
        }
        if (m_data.level_limit <= 1)
        {
            m_limit.text = "";
        }
        else
        {
            m_limit.text = "限制：" + PanelSurvivalRoom.m_levelNames[m_data.level_limit];
        }

    }

    public bool HasPassward
    {
        set { m_imgLock.enabled = value; }
        get { return m_imgLock.enabled; }
    }

    public string roomGameType
    {
        get
        {
            return m_strGameType;
        }
        set
        {
            m_strGameType = value;
        }
    }

    public void SetColor(bool select)
    {
        if (select)
        {
            m_select.TrySetActive(true);
            m_txtHostName.color = m_selectedColor;
            m_txtMapName.color = m_selectedColor;
            m_txtModeName.color = m_selectedColor;
            m_txtStateName.color = m_selectedColor;
            m_txtPeopleNum.color = m_selectedColor;
        }
        else
        {
            m_select.TrySetActive(false);
            m_txtHostName.color = m_unSelectedColor;
            m_txtMapName.color = m_unSelectedColor;
            m_txtModeName.color = m_unSelectedColor;
            m_txtStateName.color = m_unSelectedColor;
            m_txtPeopleNum.color = m_unSelectedColor;
        }
        if (m_txtStateName.text == "战斗中")
        {
            m_txtStateName.color = new Color(0.94f, 0.73f, 0.47f);
        }
        else
        {
            m_txtStateName.color = new Color(0.46f, 0.61f, 0.74f);
        }
    }
}
