﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;


class PanelPassward:BasePanel
{
    private InputField m_password;
    private RectTransform m_tranOk;
    private RectTransform m_tranCancel;
    private RectTransform m_tranBtnClose;
    
    public Action<string> m_callBack; 

    public PanelPassward()
    {
        SetPanelPrefabPath("UI/Common/PanelPassword");
        AddPreLoadRes("UI/Common/PanelPassword", EResType.UI);
    }

    public override void Init()
    {
        m_password = m_tran.Find("background/InputPassword").GetComponent<InputField>();
        m_password.gameObject.AddComponent<InputFieldFix>();
        m_tranOk = m_tran.Find("background/btnOk").GetComponent<RectTransform>();
        m_tranCancel = m_tran.Find("background/btnCancel").GetComponent<RectTransform>();
        m_tranBtnClose = m_tran.Find("background/btnClose").GetComponent<RectTransform>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tranOk).onPointerClick += OnOkClick;
        UGUIClickHandler.Get(m_tranCancel).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tranBtnClose).onPointerClick += delegate { HidePanel(); };
    }

    private void OnOkClick(GameObject target, PointerEventData eventData)
    {
        HidePanel();

        if (m_callBack != null)
        {
            var func = m_callBack;
            m_callBack = null;
            func(m_password.text);
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    public override void OnShow()
    {
        m_password.text = !HasParams() ? "" : m_params[0].ToString();
    }

    public override void OnHide()
    {
        
    }
}

