﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemRoommate : MonoBehaviour
{
    public int m_camp;
    public int m_uicamp;//显示上的camp
    public Image m_imgHead;
    public Image m_imgHeadVip;
    public Image m_imgArmy;
    public Text m_txtLv;
    public Text m_txtName;
    public Text m_txtVip;
    public Image m_bg;
    public Image m_bound;
    public string m_strType;
    private Image m_imgOK;
    public Image m_imgHost;
    public Image m_speak;
    public Text m_chat;
    public float m_chatTime;
    public float m_starttime;
    public long m_iPlayerId;
    private Image m_imgHeadBack;
    public Text m_silverace;
    public int m_pos;
    private bool m_fight;
    private bool m_ready;
    public int m_iLvl;
    public int m_iHead;
    public Image m_imgVip;
    public GameObject m_goPlayerName;
    public GameObject m_goVip;
    private Text m_lV;
    private Image m_imgLv;
    private UIEffect m_headEffect;
    private RectTransform m_effectPoint;

    public Image[] m_title;
    public Text[] title_cnt;
    private void Awake()
    {
        var tran = transform;
        m_chatTime = -1.0f;
        m_uicamp = 1;
        m_imgHead = tran.Find("imgHead/head").GetComponent<Image>();
        m_imgHeadVip = tran.Find("imgHeadVip").GetComponent<Image>();
        m_effectPoint = tran.Find("imgHeadVip/effect").GetComponent<RectTransform>();
        m_imgHeadBack = tran.Find("imgHead").GetComponent<Image>();
        m_imgArmy = tran.Find("imgArmy").GetComponent<Image>();
        m_txtLv = tran.Find("imgLv/Text").GetComponent<Text>();
        m_txtName = tran.Find("txtName").GetComponent<Text>();
        m_imgVip = tran.Find("vip").GetComponent<Image>();
        m_goVip = tran.Find("vip").gameObject;
        m_goPlayerName = tran.Find("txtName").gameObject;
        m_imgOK = tran.Find("imgOK").GetComponent<Image>();
        m_imgHost = tran.Find("imgHost").GetComponent<Image>();
        m_bg = tran.Find("imgBackground").GetComponent<Image>();
        //m_bound = tran.Find("bound").GetComponent<Image>();
        m_speak = tran.Find("speak").GetComponent<Image>();
        m_chat = tran.Find("speak/Text").GetComponent<Text>();
        title_cnt = new Text[2];
        title_cnt[0] = tran.Find("mvpText").GetComponent<Text>();
        title_cnt[1] = tran.Find("aceText").GetComponent<Text>();
        if (tran.Find("silveraceText"))
            m_silverace = tran.Find("silveraceText").GetComponent<Text>();
        m_speak.gameObject.TrySetActive(false);
        IsHost = false;
        IsReady = false;
        m_title = new Image[2];
        m_title[0] = tran.Find("mvp").GetComponent<Image>();
        if (tran.Find("Image") != null)
        {
            m_title[1] = tran.Find("Image").GetComponent<Image>();
        }
        //m_lV = tran.Find("imgLv/Lv").GetComponent<Text>();
        //m_imgLv = tran.Find("imgLv").GetComponent<Image>();
    }

    /// <summary>
    /// 设置VIP信息
    /// </summary>
    /// <param name="type"></param>
    public void setVipInfo(MemberManager.MEMBER_TYPE type, int playerLv = 0,int honor=0)
    {
        m_goVip.TrySetActive(false);
        int camp = m_camp + 1;
        switch (type)
        {
            case MemberManager.MEMBER_TYPE.MEMBER_NONE:
                //段位积分显示
                ConfigChannelLine channelCfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(3);
                TDScoreTitle stCfg = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(honor);
                if (playerLv >= channelCfg.LevelMin&&stCfg != null && stCfg.BatteRoomBg != "")
                {
                    m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, stCfg.BatteRoomBg));
                }
                else
                    //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "room_player_bg_camp_" + camp));
                    m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
                break;
            case MemberManager.MEMBER_TYPE.MEMBER_NORMAL:
                //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "room_player_bg_camp_" + camp + "_vip"));
                m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
                break;
            case MemberManager.MEMBER_TYPE.MEMBER_HIGH:
                //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "room_player_bg_camp_" + camp + "_super"));
                m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
                break;
        }
    }

    public void ChangeUICamp()
    {
        if (m_uicamp == 1)
        {
            m_uicamp = 2;
        }
        else
        {
            m_uicamp = 1;
        }
    }

    public void Update()
    {
        if (m_chatTime < 0)
        {
            return;
        }
        else
        {
            if (Time.time - m_chatTime >= 3)
            {
                HideChat();
            }
        }
    }

    public bool IsHost
    {
        set { m_imgHost.enabled = value; }
        get { return m_imgHost != null && m_imgHost.enabled; }
    }

    public bool IsReady
    {
        set { SetState(value, m_fight); }
        get { return m_ready; }
    }

    public void SetState(bool ready, bool fight)
    {
        m_ready = ready;
        m_fight = fight;
        m_imgOK.enabled = !IsHost && (ready || fight);
        if (ready)
            m_imgOK.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "ready"));
        else if (fight)
            m_imgOK.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "fighting"));
        if (m_imgOK.enabled)
            m_imgOK.SetNativeSize();
    }

    public void SetHeroCardHead(int heroType)
    {
        m_imgHeadVip.gameObject.TrySetActive(false);
        if(heroType != (int)EnumHeroCardType.none)
        {
            m_imgHeadVip.gameObject.TrySetActive(true);
            if(heroType == (int)EnumHeroCardType.normal)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == (int)EnumHeroCardType.super)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == (int)EnumHeroCardType.legend)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (m_headEffect == null)
                {
                    m_headEffect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD, new Vector2(-2, 2));
                }
            }
        }
    }

    public bool IsFight
    {
        get { return m_fight; }
    }

    public void SetCampColor(int camp)
    {
        if (camp == 2)
        {
            //            m_imgHead.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "headyazhou"));
            // m_bound.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "bluebound"));
            // m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "room_player_bg_camp_2"));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            m_txtName.color = new Color(1, 1, 1);
            //m_lV.color = new Color(147f / 255f, 220f / 255f, 240f / 255f);
            //m_imgLv.SetSprite(ResourceManager.LoadSprite(AtlasName.FightInTurn, "camp2bg2"));
        }
        else
        {
            //            m_imgHead.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, "omm1"));
            //  m_bound.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "redbound"));
            // m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "room_player_bg_camp_1"));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            m_txtName.color = new Color(254f / 255f, 220f / 255f, 186f / 255f);
            //m_lV.color = new Color(255f / 255f, 212f / 255f, 171f / 255f);
            //m_imgLv.SetSprite(ResourceManager.LoadSprite(AtlasName.FightInTurn, "camp1bg2"));
        }
    }

    public void ShowChat(chatRecord chat)
    {
        m_chat.text = chat.msg;
        m_speak.gameObject.TrySetActive(true);
        m_chatTime = chat.startTime;
    }

    public void HideChat()
    {
        m_speak.gameObject.TrySetActive(false);
        m_chatTime = -1.0f;
    }
}
