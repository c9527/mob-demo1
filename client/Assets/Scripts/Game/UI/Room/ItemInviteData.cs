﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;


 public class PlayerInviteRecord
    {
        public bool canInvite = true;
        public float lastTime = 0f;
        public float lastInviteTime = -61.0f;
    }


public class ItemInviteData : ItemRender
{
    private PanelInviteFriend.ListItemData m_data;
    //private GameObject m_goItem;
    
    private Image m_imageHead;
    private Text m_txtArmyLvl;
    private Image m_imageArmyLvl;

    private Text m_textName;
    private Image m_imgState;
    private Text m_txtState;
    private Text m_txtMvp;
    private Text m_txtAce;

    Transform m_btnInvite;
    public int m_iUserData;
    //PanelInviteFriend.ListItemData m_kData;

    public PanelInviteFriend m_parentPanel;
    private Image m_imgVip;
    private GameObject m_goVip;
    private GameObject m_goName;
    private Vector2 m_anchorName;
    private static Dictionary<long, PlayerInviteRecord> _dicInviteRecord;
    private static Dictionary<long, PlayerInviteRecord> m_dicInviteRecord
    {
        get
        {
            if (_dicInviteRecord == null)
                _dicInviteRecord = new Dictionary<long, PlayerInviteRecord>();
            return _dicInviteRecord;
        }
    }
    private PlayerInviteRecord m_localRecord
    {
        get
        {
            if(m_dicInviteRecord.ContainsKey(m_data.m_iPlayerId))
            {
                return m_dicInviteRecord[m_data.m_iPlayerId];
            }
            else
            {
                return null;
            }
        }
    }
    private Text m_InviteTxt;
    public override void Awake()
    {
        m_iUserData = 0;
        m_imageHead = transform.Find("headframe/head").GetComponent<Image>();
        m_imageArmyLvl = transform.Find("army").GetComponent<Image>();
        m_txtArmyLvl = transform.Find("lvl/Text").GetComponent<Text>();

        m_textName = transform.Find("name").GetComponent<Text>();
        m_goName = transform.Find("name").gameObject;
        m_goVip = transform.Find("vip").gameObject;
        m_imgVip = transform.Find("vip").GetComponent<Image>();
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition;

        m_imgState = transform.Find("imgState").GetComponent<Image>();
        m_btnInvite = transform.Find("invite");
        m_txtState = transform.Find("txtState").GetComponent<Text>();
        m_txtMvp = transform.Find("mvp/Text").GetComponent<Text>();
        m_txtAce = transform.Find("ace/Text").GetComponent<Text>();
        m_parentPanel = UIManager.GetPanel<PanelInviteFriend>();
        m_InviteTxt = transform.Find("invite/Text").GetComponent<Text>();
        UGUIClickHandler.Get(m_btnInvite).onPointerClick += OnClickInvite;
        
    }
    
    void Update()
    {
        if (m_localRecord == null)
            return;
        if (m_localRecord.canInvite)
            return;
       UpdateBtnLabel();
    }

    private void UpdateBtnLabel()
    {
        if ((Time.realtimeSinceStartup - m_localRecord.lastInviteTime) < 60.0f)
        {
            float diff = Time.realtimeSinceStartup - m_localRecord.lastInviteTime;
            m_localRecord.lastTime = Time.realtimeSinceStartup;
            m_InviteTxt.text = (60 - Mathf.CeilToInt(diff)).ToString();
            return;
        }
        if (UIManager.IsOpen<PanelTeam>() && !PlayerSystem.IsRankMatchOpen(m_data.m_armyLvl))
        {
            m_localRecord.canInvite = false;
            m_InviteTxt.text = "等级不足";
            Util.SetGrayShader(m_InviteTxt);
        }
        else
        {
            m_localRecord.canInvite = true;
            m_InviteTxt.text = "邀请";
            Util.SetNormalShader(m_InviteTxt);
        }
    }

    protected override void OnSetData(object data)
    {
        m_data = data as PanelInviteFriend.ListItemData;        
        Update();
        if (!m_dicInviteRecord.ContainsKey(m_data.m_iPlayerId))
        {
            PlayerInviteRecord localRecord = new PlayerInviteRecord();
            m_dicInviteRecord.Add(m_data.m_iPlayerId, localRecord);
        }

        UpdateBtnLabel();

        m_textName.text = m_data.m_playerName;
        if (m_data.m_bInFight)
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "fighting"));
            m_btnInvite.gameObject.TrySetActive(false);
            m_txtState.gameObject.TrySetActive(true);
            m_txtState.text = "战斗中";
        }
        else if( m_data.m_bInRoom )
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "inroom"));
            m_btnInvite.gameObject.TrySetActive(false);
            m_txtState.gameObject.TrySetActive(true);
            m_txtState.text = "房间中";
        }
        else if (m_data.m_isOnline)
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "online"));
            m_btnInvite.gameObject.TrySetActive(true);
            m_txtState.gameObject.TrySetActive(false);
        }
        else
        {
            m_imgState.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, "outline"));
            m_btnInvite.gameObject.TrySetActive(false);
            m_txtState.gameObject.TrySetActive(true);
            m_txtState.text = "离线";
        }
        m_imgState.SetNativeSize();
        m_imageHead.SetSprite(ResourceManager.LoadRoleIcon(m_data.m_icon));
        m_imageHead.SetNativeSize();

        m_txtMvp.text = m_data.m_mvp.ToString();
        m_txtAce.text = m_data.m_ace.ToString();

        m_txtArmyLvl.text = String.Format("{0}级",  m_data.m_armyLvl);
        m_imageArmyLvl.SetSprite(ResourceManager.LoadArmyIcon(m_data.m_armyLvl));
        m_imageArmyLvl.SetNativeSize();

        m_goName.GetComponent<RectTransform>().anchoredPosition = m_anchorName;
        if (m_data.m_eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            m_goVip.TrySetActive(false);
            Vector2 vipSizeData = m_goVip.GetComponent<RectTransform>().sizeDelta;
            m_goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (m_data.m_eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (m_data.m_eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }
        //m_iUserData = m_data.m_iPlayerId; ;   
    }

    void OnClickInvite(GameObject target, PointerEventData eventData)
    {
        if (m_localRecord!=null&&!m_localRecord.canInvite)
            return;
        var cl = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        if (!cl.ByCenter)
        {
            if(PlayerSystem.IsCrossServerRole(m_data.m_iPlayerId))
            {
                TipsManager.Instance.showTips("你所在的频道不支持跨服邀请");
                return;
            }
        }
        m_localRecord.canInvite = false;
        m_localRecord.lastInviteTime = Time.realtimeSinceStartup;
        Util.SetGrayShader(m_InviteTxt);
        TipsManager.Instance.showTips(string.Format("你已经向{0}发送了邀请", m_data.m_playerName));
        //Logger.Log(m_data.m_iPlayerId);        
        NetLayer.Send(new tos_room_invite() { id = m_data.m_iPlayerId });        
        if (m_parentPanel != null)
            m_parentPanel.OnInvited(m_data);
    }
}
