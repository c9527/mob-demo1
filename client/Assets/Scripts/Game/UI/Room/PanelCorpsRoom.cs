﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

class PanelCorpsRoom : PanelRoomBase
{
    public PanelCorpsRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.Viewing, 
            RoomFunc.Chat,
            RoomFunc.ChangeMode, 
            RoomFunc.DisableMode, 
            RoomFunc.SmallMap,
            RoomFunc.NormalRoommate,
        });

        SetPanelPrefabPath("UI/Room/PanelCorpsRoom");
        AddPreLoadRes("UI/Room/PanelCorpsRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemCommonspeak", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    Text m_team1name;
    Text m_team2name;
    Image m_team1icon;
    Image m_team2icon;
    Text m_timeDown;
    DateTime curtime;
    DateTime deadline;
    Text team1winNum;
    Text team2winNum;
    GameObject teamFlag1;
    GameObject teamFlag2;

    public override void Init()
    {
        base.Init();
        m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
        m_team1name = m_tran.Find("pageframe/head/team1Bg/name").GetComponent<Text>();
        m_team2name = m_tran.Find("pageframe/head/team2Bg/name").GetComponent<Text>();
        m_team1icon = m_tran.Find("pageframe/head/team1Bg/flag/icon").GetComponent<Image>();
        m_team2icon = m_tran.Find("pageframe/head/team2Bg/flag/icon").GetComponent<Image>();
        team1winNum = m_tran.Find("pageframe/head/team1Bg/num").GetComponent<Text>();
        team2winNum = m_tran.Find("pageframe/head/team2Bg/num").GetComponent<Text>();
        m_timeDown = m_tran.Find("optionframe/fightstate/fight").GetComponent<Text>();
        teamFlag1 = m_tran.Find("pageframe/head/team1Bg/flag").gameObject;
        teamFlag2 = m_tran.Find("pageframe/head/team2Bg/flag").gameObject;

        curtime = TimeUtil.GetNowTime();
        deadline = new DateTime(curtime.Year, curtime.Month, curtime.Day, 21, 0, 0);
        GameObject go = m_tran.Find("optionframe/btnInviteFriend").gameObject;
        Util.SetGrayShader(go.GetComponent<Image>());
    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnInviteFriend").gameObject).onPointerClick += null;

    }

    public override void OnShow()
    {
        base.OnShow();
        NetLayer.Send(new tos_room_ready { ready = true });
        UserChooseReady = true;
    }

    public override void OnHide()
    {
        base.OnHide();
        if (UIManager.GetPanel<PanelGivingUp>() != null)
        {
            UIManager.GetPanel<PanelGivingUp>().HidePanel();
        }
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelCorpsEnter>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {

        TimeSpan time = TimeUtil.GetRemainTime((uint)(TimeUtil.GetTimeStamp(deadline)));
        int hour = time.Hours;
        int munite = time.Minutes;
        int second = time.Seconds;

        string h;
        string m;
        string s;

        if (hour > 0)
        {
            h = hour.ToString() + ":";
        }
        else
        {
            h = "";
        }
        if (munite < 10)
        {
            m = "0" + munite.ToString() + ":";
        }
        else
        {
            m = munite.ToString() + ":";
        }
        if (second < 10)
        {
            s = "0" + second.ToString();
        }
        else
        {
            s = second.ToString();
        }
        m_timeDown.text = h + m + s + "后战斗开始";
        if (hour <= 0 && munite <= 0 && second <= 0)
        {
            m_timeDown.text = "战斗将在2分钟后开始";
        }
        base.Update();
    }

    protected override void UpdateView()
    {
        base.UpdateView();
        var info = RoomModel.RoomInfo;

        var selfIndex = 0;
        var otherIndex = 1;
        if (IsPlayer)
        {
            selfIndex = 1 - m_selfPlay.pos % 2;
            otherIndex = m_selfPlay.pos % 2;
            Logger.Log("IsPlayer = " + IsPlayer + "    m_selfPlay.pos = " + m_selfPlay == null ? "00" : m_selfPlay.pos + "       len =" + CorpsDataManager.m_roomdata.Length);
        }
        else
        {
             selfIndex = 1;
             otherIndex = 0;
        }
        m_team1name.text = "";
        m_team2name.text = "";

        teamFlag1.TrySetActive(CorpsDataManager.m_roomdata[selfIndex] != null);
        teamFlag2.TrySetActive(CorpsDataManager.m_roomdata[otherIndex] != null);
        team1winNum.text = "0";
        team2winNum.text = "0";

        for (int i = 0; i < info.round.Length;i++ )
        {
            Logger.Log("id=" + info.round[i].id + " times =  " + info.round[i].times);
        }
        for (int i = 0; i < CorpsDataManager.m_roomdata.Length; i++)
        {
            Logger.Log("corpsid=" + CorpsDataManager.m_roomdata[i].corpsid + "  corpsname =  " + CorpsDataManager.m_roomdata[i].corpsname);
        }

        if (CorpsDataManager.m_roomdata[selfIndex] != null)
        {
            m_team1name.text = CorpsDataManager.m_roomdata[selfIndex].corpsname;
            m_team1icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, "biaozhi_" + CorpsDataManager.m_roomdata[selfIndex].corpsicon));
        }

        if (CorpsDataManager.m_roomdata[otherIndex] != null)
        {
            m_team2name.text = CorpsDataManager.m_roomdata[otherIndex].corpsname;
            m_team2icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, "biaozhi_" + CorpsDataManager.m_roomdata[otherIndex].corpsicon));

        }

        if (!IsPlayer)
        {
            if (CorpsDataManager.m_roomdata[selfIndex] != null)
            {
                team1winNum.text = selfIndex < info.round.Length ? info.round[selfIndex].times.ToString() : "0";
            }
            if (CorpsDataManager.m_roomdata[otherIndex] != null)
            {
                team2winNum.text = otherIndex < info.round.Length ? info.round[otherIndex].times.ToString() : "0";
            }

        }
        else
        {
            for (int i = 0; i < info.round.Length; i++)
            {
                int times = info.round[i].times;
                if (CorpsDataManager.m_roomdata[selfIndex] != null && CorpsDataManager.m_roomdata[selfIndex].corpsid == info.round[i].id)
                {
                    team1winNum.text = times.ToString();
                }
                if (CorpsDataManager.m_roomdata[otherIndex] != null && CorpsDataManager.m_roomdata[otherIndex].corpsid == info.round[i].id)
                {
                    team2winNum.text = times.ToString();
                }
            }
        }


        m_btnChangeMode.gameObject.TrySetActive(IsPlayer);

        /*
        string biaozhi1 = "biaozhi_" + CorpsDataManager.m_roomdata[0].corpsicon;
        string biaozhi2 = "biaozhi_" + CorpsDataManager.m_roomdata[1].corpsicon;
        for (int i = 0; i < info.players.Length; i++)
        {
            if (PlayerSystem.roleId == info.players[i].id)
            {
                m_btnChangeMode.gameObject.TrySetActive(true);
                if (info.players[i].pos % 2 == 1)
                {
                    m_team1name.text = CorpsDataManager.m_roomdata[0].corpsname;
                    m_team2name.text = CorpsDataManager.m_roomdata[1].corpsname;
                    m_team1icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi1));
                    m_team2icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi2));
                    if (info.round.Length >= 1)
                    {
                        if (info.round[0].id == CorpsDataManager.m_roomdata[1].corpsid)
                        {
                            team2winNum.text = info.round[0].times.ToString();
                        }
                        else
                        {
                            team1winNum.text = info.round[0].times.ToString();
                        }
                    }
                    if (info.round.Length >= 2)
                    {
                        if (info.round[1].id == CorpsDataManager.m_roomdata[1].corpsid)
                        {
                            team2winNum.text = info.round[1].times.ToString();
                        }
                        else
                        {
                            team1winNum.text = info.round[1].times.ToString();
                        }
                    }

                }
                else
                {
                    m_team1name.text = CorpsDataManager.m_roomdata[1].corpsname;
                    m_team2name.text = CorpsDataManager.m_roomdata[0].corpsname;
                    m_team1icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi2));
                    m_team2icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi1));
                    if (info.round.Length >= 1)
                    {
                        if (info.round[0].id == CorpsDataManager.m_roomdata[1].corpsid)
                        {
                            team1winNum.text = info.round[0].times.ToString();
                        }
                        else
                        {
                            team2winNum.text = info.round[0].times.ToString();
                        }
                    }
                    if (info.round.Length >= 2)
                    {
                        if (info.round[1].id == CorpsDataManager.m_roomdata[1].corpsid)
                        {
                            team1winNum.text = info.round[1].times.ToString();
                        }
                        else
                        {
                            team2winNum.text = info.round[1].times.ToString();
                        }
                    }
                }
            }
        }

        for (int i = 0; i < info.spectators.Length; i++)
        {
            if (info.spectators[i].id == PlayerSystem.roleId)
            {
                m_team1name.text = CorpsDataManager.m_roomdata[0].corpsname;
                m_team2name.text = CorpsDataManager.m_roomdata[1].corpsname;
                m_team1icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi1));
                m_team2icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi2));
                m_btnChangeMode.gameObject.TrySetActive(false);
            }
        }
         */
    }

    void Toc_room_corpsmatch_give_up(toc_room_corpsmatch_give_up data)
    {
        string[] str = new string[1];
        var info = RoomModel.RoomInfo;
        bool team1 = false;
        bool team2 = false;
        for (int i = 0; i < info.players.Length; i++)
        {
            if (info.players[i].ready)
            {
                if (info.players[i].pos % 2 == 1)
                {
                    team2 = true;
                }
                else
                {
                    team1 = true;
                }
            }
        }
        if (team1)
        {
            str[0] = m_team1name.text;
        }
        else
        {
            str[0] = m_team2name.text;
        }
        if (team1 == false && team2 == false)
        {
            str[0] = "双方";
        }
        UIManager.PopPanel<PanelGivingUp>(str, true);
    }
}

