﻿class PanelRoom : PanelRoomBase
{
    public PanelRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.NormalRoommate, 
            RoomFunc.FightState, 
            RoomFunc.AutoKick, 
            RoomFunc.Viewing, 
            RoomFunc.Chat,
            RoomFunc.Password, 
            RoomFunc.ChangeMode, 
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.ChangeVsIcon,
            RoomFunc.ChangeCamp,
            RoomFunc.AutoAim,
            RoomFunc.SmallMap
        });

        SetPanelPrefabPath("UI/Room/PanelRoom");
        AddPreLoadRes("UI/Room/PanelRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/FightInTurn", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
    }

    public override void InitEvent()
    {
        base.InitEvent();
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelPvpRoomList>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }
}
