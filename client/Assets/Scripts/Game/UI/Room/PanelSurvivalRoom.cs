﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
class PanelSurvivalRoom : PanelRoomBase
{
    public static string[] m_levelNames = new[] { "", "简单", "普通", "困难", "噩梦", "地狱" };

    Text m_limit;
    Text m_ltxt;
    GameObject m_limitBtn;
    

    public PanelSurvivalRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.FightState, 
            RoomFunc.AutoKick, 
            RoomFunc.Viewing, 
            RoomFunc.Chat,
            RoomFunc.Password, 
            RoomFunc.ChangeMode, 
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.SmallMap,
            RoomFunc.AutoAim
        });

        SetPanelPrefabPath("UI/Room/PanelSurvivalRoom");
        AddPreLoadRes("UI/Room/PanelSurvivalRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemCommonspeak", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoomspace", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
        m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;

        m_limit = m_tran.Find("optionframe/limittext/Image/Text").GetComponent<Text>();
        m_ltxt = m_tran.Find("optionframe/limittext").GetComponent<Text>();
        m_limitBtn = m_tran.Find("optionframe/limittext/Image").gameObject;

        
    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_limitBtn).onPointerClick += delegate { ShowPanelLimit(); };
    }

    void ShowPanelLimit()
    {
        if (IsHost)
        {
            int maxLevel = 0;
            var arr = PlayerSystem.survival_data.types_data;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].type == RoomModel.RoomInfo.rule)
                {
                    maxLevel = arr[i].top_level;
                    break;
                }
            }
            UIManager.PopPanel<PanelLimitSelect>(new object[] { maxLevel }, true);
        }

    }

    public override void OnShow()
    {
        base.OnShow();
        
        
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelSurvivalRoomList>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }

    protected override void UpdateView()
    {
        base.UpdateView();

        if (!RoomModel.IsInRoom)
            return;
        
        var myId = PlayerSystem.roleId;
        var roomInfo = RoomModel.RoomInfo;
        var group = m_goPopListAnchor.GetComponent<ToggleGroup>();
        if (RoomModel.RoomInfo.level_limit <= 1)
            m_limit.text = "未限制";
        else
        {
            m_limit.text = "限制:" + m_levelNames[RoomModel.RoomInfo.level_limit];
        }
        
        List<GameObject> list = new List<GameObject>();
        foreach (Transform tran in m_roomParent)
        {
            list.Add(tran.gameObject);
        }

        for (int i = 0; i < list.Count; i++)
        {
            GameObject.Destroy(list[i]);
        }
        m_mates = new List<ItemRoommate>();

        int playerIndex = 0;

        var itemSpaceClone = m_tran.Find("pageframe/itemSpace").gameObject;
        for (int i = roomInfo.players.Length; i < 5; i++)
        {
            var item = (UIHelper.Instantiate(itemSpaceClone) as GameObject).AddComponent<ItemRoomspace>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, 0, i * -69);
            UGUIClickHandler.Get(item.gameObject).onPointerClick += delegate { NetLayer.Send(new tos_player_room_invitelist() { is_friend = true, stage = 0, stage_type = "" }); };
        }

        var itemClone = m_tran.Find("pageframe/item").gameObject;
        foreach (var player in roomInfo.players)
        {
            int campId = 0;

            var item = (UIHelper.Instantiate(itemClone) as GameObject).AddComponent<ItemRoommate>();
            item.gameObject.TrySetActive(true);
            item.gameObject.GetRectTransform().SetUILocation(m_roomParent, campId * 307, (player.pos - 1) * -69);
            item.m_camp = campId;
            item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
            item.m_iHead = player.icon;
            item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, campId));
            item.m_imgHead.SetNativeSize();
            item.m_iLvl = player.level;

//            MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
//            Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;
//
//            if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
//            {
//                item.m_goVip.TrySetActive(false);
//                Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
//                item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);
//
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }
//            else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
//            {
//                item.m_goVip.TrySetActive(true);
//                item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
//                item.m_imgVip.SetNativeSize();
//            }


            item.m_txtLv.text = String.Format("{0}级", player.level);
            item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
            if (player.id == roomInfo.leader)
                item.IsHost = true;
            if (player.id == myId)
            {
                m_self = item;
                item.gameObject.GetComponent<Toggle>().enabled = false;
            }
            item.SetState(player.ready, player.fight);
            item.m_iPlayerId = player.id;
            item.SetHeroCardHead(player.hero_type);
            if (m_self != null)
            {
                if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId)
                {
                    UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                    item.gameObject.GetComponent<Toggle>().group = group;
                }
            }
            //item.SetCampColor(campId+1);
            if (item.m_iPlayerId == myId)
            {
                item.m_txtName.color = new Color32(134, 253, 131, 255);
            }
            playerIndex++;
            m_mates.Add(item);
            item.title_cnt[1].text = player.gold_ace.ToString();
            item.title_cnt[0].text = player.mvp.ToString();
            item.m_silverace.text = player.silver_ace.ToString();
            Image m_ace = item.transform.Find("ace").GetComponent<Image>();
            Image m_silver = item.transform.Find("silverace").GetComponent<Image>();


            if (player.titles != null && player.titles.Length >= 3)
            {

                item.m_title[0].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[2].title, player.titles[2].type)));
                    item.m_title[0].SetNativeSize();
                    //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    item.title_cnt[0].text = player.titles[2].cnt.ToString();

                    m_ace.SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[1].title, player.titles[1].type)));
                    m_ace.SetNativeSize();
                    //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    item.title_cnt[1].text = player.titles[1].cnt.ToString();

                    m_silver.SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[0].title, player.titles[0].type)));
                    m_silver.SetNativeSize();
                    //item.m_title[0].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                    item.m_silverace.text = player.titles[0].cnt.ToString();
            }
        }

        if (IsHost)
        {
            m_ltxt.color = Color.white;

        }
        else
        {
            m_ltxt.color = new Color(160f / 255f, 160f / 255f, 160f / 255f);
        }
    }

    void Toc_room_survival_leader(toc_room_survival_leader data)
    {
        TipsManager.Instance.showTips("你已成为房主，难度调整为" + m_levelNames[data.level]);
    }
}
