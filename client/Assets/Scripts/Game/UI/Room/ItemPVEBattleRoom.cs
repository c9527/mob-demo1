﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemPVEBattleRoom : ItemRender
{
    private int m_id;
    private Text m_txtHostName;
    private Text m_txtMapName;
    private Text m_txtModeName;
    public Text m_txtStateName;
    private Text m_txtPeopleNum;
    public int m_people;
    public int m_maxpeople;
    private Toggle m_toggle;

    private string m_mapId;
    private Image m_imgLock;
    public int m_spnum;
    string m_strGameType;
    public p_room_info m_data;

    public override void Awake()
    {
        var tran = transform;
        m_txtHostName = tran.Find("hostname").GetComponent<Text>();
        m_txtMapName = tran.Find("mapname").GetComponent<Text>();
        m_txtModeName = tran.Find("modename").GetComponent<Text>();
        m_txtStateName = tran.Find("statename").GetComponent<Text>();
        m_txtPeopleNum = tran.Find("peoplenum").GetComponent<Text>();
        m_imgLock = tran.Find("lock").GetComponent<Image>();
        m_toggle = tran.GetComponent<Toggle>();
        m_toggle.onValueChanged.AddListener(OnValueChanged);

        HasPassward = false;
    }

    private void OnValueChanged(bool selected)
    {
        SetColor(selected);
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_room_info;
        m_id = m_data.id;
        m_txtHostName.text = m_data.leader;
        m_mapId = m_data.map.ToString();
        m_txtMapName.text = ConfigManager.GetConfig<ConfigMapList>().GetLine(m_mapId).MapName;
        m_txtModeName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(m_data.rule).Mode_Name;
        roomGameType = m_data.rule;
        if (m_data.state == 1)
        {
            m_txtStateName.text = "等待加入";
        }
        else if(m_data.state==2)
        {
            m_txtStateName.text = "战斗中";
        }
        else if (m_data.state == 3)
        {
            m_txtStateName.text = "快结束";
        }
        HasPassward = m_data.password == true;
        m_people = m_data.player_count;
        m_maxpeople = m_data.max_player;
        m_txtPeopleNum.text = string.Format("{0}/{1}", m_data.player_count, m_data.max_player);
        m_spnum = m_data.spectators_count;
        SetColor(m_toggle.isOn);
    }

    public bool HasPassward
    {
        set { m_imgLock.enabled = value; }
        get { return m_imgLock.enabled; }
    }

    public string roomGameType
    {
        get
        {
            return m_strGameType;
        }
        set
        {
            m_strGameType = value;
        }
    }

    public void SetColor(bool select)
    {
        if (select)
        {
            m_txtHostName.color = new Color(0.94f, 0.73f, 0.47f);
            m_txtMapName.color = new Color(0.94f, 0.73f, 0.47f);
            m_txtModeName.color = new Color(0.94f, 0.73f, 0.47f);
            m_txtStateName.color = new Color(0.94f, 0.73f, 0.47f);
            m_txtPeopleNum.color = new Color(0.94f, 0.73f, 0.47f);
        }
        else
        {
            m_txtHostName.color = new Color(0.61f, 0.83f, 0.94f);
            m_txtMapName.color = new Color(0.61f, 0.83f, 0.94f);
            m_txtModeName.color = new Color(0.46f, 0.61f, 0.74f);
            m_txtStateName.color = new Color(0.46f, 0.61f, 0.74f);
            m_txtPeopleNum.color = new Color(0.61f, 0.83f, 0.94f);
        }
        if (m_txtStateName.text == "战斗中")
        {
            m_txtStateName.color = new Color(0.94f, 0.73f, 0.47f);
        }
        else
        {
            m_txtStateName.color = new Color(0.46f, 0.61f, 0.74f);
        }
    }
}
