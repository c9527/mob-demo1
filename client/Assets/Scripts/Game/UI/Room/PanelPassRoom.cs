﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;


class PanelPassRoom : PanelRoomBase
{

    public static int m_iStageId;
    private UIEffect _headEffect;

    public PanelPassRoom()
    {
        SetRoomFunc(new[]
        {
            RoomFunc.Chat,
            RoomFunc.DisableMode, 
            RoomFunc.InviteFriend, 
            RoomFunc.AutoAim,
            RoomFunc.SmallMap
        });

        SetPanelPrefabPath("UI/Room/PanelPassRoom");
        AddPreLoadRes("UI/Room/PanelPassRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemInviteAI", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
        m_goPopListAnchor = m_tran.Find("poplistanchor").gameObject;
        m_goPopListPanel = m_tran.Find("poplistpanel").gameObject;
        m_tran.Find("optionframe/btnLeftMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnRightMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnShowAllMap/Image").gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        base.InitEvent();
    }

    protected override void OnStartClick(GameObject target, PointerEventData eventdata)
    {
        if (!RoomModel.IsInRoom)
            return;
        if (CheckAndUpdateMap())
            return;

        var realNum = 0;
        if (IsHost)
        {
            foreach (var player in RoomModel.RoomInfo.players)
            {
                if (!player.ready && player.id != RoomModel.RoomInfo.leader)
                {
                    UIManager.ShowTipPanel("请等待所有玩家准备");
                    return;
                }
                if (player.id > 0)
                    realNum++;
            }
            var configLine = ConfigManager.GetConfig<ConfigStage>().GetLine(RoomModel.RoomInfo.stage);
            if (realNum == 1 && configLine != null && configLine.IsHard)
                UIManager.ShowTipPanel("该关卡异常凶险，建议邀请好友共同挑战！", () => NetLayer.Send(new tos_room_start_game()), null, false, true, "我要硬闯", "返回邀请");
            else
                NetLayer.Send(new tos_room_start_game());
        }
        else if (RoomModel.RoomInfo.fight)
            NetLayer.Send(new tos_room_join_game());
        else
        {
            NetLayer.Send(new tos_room_ready {ready = !IsReady});
            UserChooseReady = !IsReady;
        }
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelLvlPass>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }

    protected override void UpdateView()
    {
        base.UpdateView();
        m_tran.Find("optionframe/btnLeftMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnRightMap").gameObject.TrySetActive(false);
        m_tran.Find("optionframe/btnShowAllMap/Image").gameObject.TrySetActive(false);
        
        foreach (Transform tran in m_roomParent)
        {
            Object.Destroy(tran.gameObject);
        }
        if (!RoomModel.IsInRoom)
            return;
        var roomInfo = RoomModel.RoomInfo;
        ToggleGroup group = m_goPopListAnchor.GetComponent<ToggleGroup>();
        int campIndex = 0;
        var myId = PlayerSystem.roleId;
        int maxpos = 0;
        m_mates = new List<ItemRoommate>();
        foreach (p_player_info player in roomInfo.players)
        {
            if (player.id > 0 ) 
            {
                if (maxpos < player.pos)
                {
                    maxpos = player.pos;
                }
            }
            if (true)
            {
                var item = ResourceManager.LoadUI("UI/Room/ItemRoommate").AddComponent<ItemRoommate>();
                item.gameObject.GetRectTransform().SetUILocation(m_roomParent, ((player.pos-1)%2) * 307, ((player.pos-1)/2) * -66);
                item.gameObject.GetComponent<Toggle>().group = group;
                item.m_camp = 0;
                item.m_iHead = player.icon;
                item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(player.icon, item.m_camp));
                item.m_imgHead.SetNativeSize();
                item.SetCampColor(item.m_camp+1);

                item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);
                item.m_iLvl = player.level;
                item.m_txtLv.text = String.Format("{0}级", player.level);
                item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));

                var eType = (MemberManager.MEMBER_TYPE)player.vip_level;
                Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;

                item.setVipInfo(eType);
               /* if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
                {
                    item.m_goVip.TrySetActive(false);
                    Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
                    item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);

                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
                    item.m_imgVip.SetNativeSize();
                }
                else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
                {
                    item.m_goVip.TrySetActive(true);
                    item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
                    item.m_imgVip.SetNativeSize();
                }*/

                if (player.id == roomInfo.leader)
                {
                    item.IsHost = true;
                }

                if (player.id == myId)
                {
                    m_self = item;
                    item.gameObject.GetComponent<Toggle>().enabled = false;
                }

                item.SetState(player.ready, false);
                item.m_iPlayerId = player.id;
                if (m_self != null)
                {
                    if (item.m_iPlayerId > 0 && item.m_iPlayerId != myId)
                        UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickOperation;
                }
                if (item.m_iPlayerId == myId)
                {
                    item.m_txtName.color = new Color32(134, 253, 131, 255);
                }
                item.title_cnt[0].text = player.gold_ace.ToString();
                item.title_cnt[1].text = player.mvp.ToString();
                if (player.titles.Length > 0)
                {
                    for (int e = 0; e < item.m_title.Length; e++)
                    {
                        item.m_title[e].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[e].title, player.titles[e].type)));
                        item.m_title[e].SetNativeSize();
                        item.m_title[e].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                        item.title_cnt[e].text = player.titles[e].cnt.ToString();
                    }
                }
                item.m_imgHeadVip.gameObject.TrySetActive(false);
                if(player.hero_type != (int)EnumHeroCardType.none)
                {
                    item.m_imgHeadVip.gameObject.TrySetActive(true);
                    if (player.hero_type == (int)EnumHeroCardType.normal)
                    {
                        item.m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
                    }
                    else if (player.hero_type == (int)EnumHeroCardType.super)
                    {
                        item.m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
                    }
                    else if (player.hero_type == (int)EnumHeroCardType.legend)
                    {
                        item.m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                        if (_headEffect == null)
                        {
                            _headEffect = UIEffect.ShowEffect(item.m_imgHeadVip.transform, EffectConst.UI_HEAD_HERO_CARD);
                        }
                    }
                }
                m_mates.Add(item);
            }
        }
        if (maxpos < 9)
        {
            var itemInvite = ResourceManager.LoadUI("UI/Room/ItemInviteAI").AddComponent<ItemInviteAI>();
            itemInvite.gameObject.GetRectTransform().SetUILocation(m_roomParent, (maxpos-1)%2 * 307, (maxpos+1)/2 * -66);
            itemInvite.SetCamp(maxpos);
            itemInvite.SetNormalArmy();
            UGUIClickHandler.Get(itemInvite.gameObject).onPointerClick += OnClickInviteFriend;
        }
    }
}
