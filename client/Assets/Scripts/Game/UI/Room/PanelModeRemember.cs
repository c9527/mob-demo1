﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelModeRemember : BasePanel
{
    private enum ModeClass
    {
        Nomarl = 0,
        Special = 1,
    }

    private GameObject m_modeClone;
    //private GameObject m_goSelectNum;
    private GameObject m_goSpeciaMode;
    private Transform m_tranNormalLayout;
    private DataGrid m_dataGrid;
    //private DataGrid m_dataGrid2;
    //private Transform m_tranNumLayout;
    private Text m_text;
    private GameObject m_tips;

    private string m_strModeName;
    private string m_strPeopleNum;
    private string m_strLevelNum;
    private int m_strLevelHostMaxNum;
    private Image m_SportsModelImg;
    private Toggle m_allModeToggle;
    public static string[] m_rememberModeName = new string[0];
    private bool isShowAllMode =  true;

    public PanelModeRemember()
    {
        SetPanelPrefabPath("UI/Room/PanelModeRemember");
        AddPreLoadRes("UI/Room/PanelModeRemember", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    public override void Init()
    {
        m_modeClone = m_tran.Find("frame/copeitem").gameObject;
        m_goSpeciaMode = m_tran.Find("frame/speciamode").gameObject;
        m_tranNormalLayout = m_tran.Find("frame/normalgridlayout");

        m_SportsModelImg = m_tran.Find("frame/normalmode").gameObject.GetComponent<Image>();

        m_dataGrid = m_tranNormalLayout.gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_modeClone, typeof(ItemModeRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useClickEvent= false;
        m_dataGrid.onItemSelected += OnSelectMode;

        m_allModeToggle = m_tran.Find("frame/allMode").GetComponent<Toggle>();
        m_allModeToggle.isOn = true;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/ButtonSure")).onPointerClick += OnClickConfirmMode;
        UGUIClickHandler.Get(m_tran.Find("frame/allMode").gameObject).onPointerClick += OnClickAllModeToggle;
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            m_strModeName = m_params.Length > 0 ? (string)m_params[0] : "";
            m_strPeopleNum = m_params.Length > 1 ? (string)m_params[1] : null;
            m_strLevelNum = m_params.Length > 2 ? (string)m_params[2] : null;
            m_strLevelHostMaxNum = m_params.Length > 3 ? (int)m_params[3] : 0;
        }
        m_goSpeciaMode.TrySetActive(m_strLevelNum == null);
        if (m_strLevelNum == null)
        {
            m_SportsModelImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "SportsModel"));
        }
        else
        {
            m_SportsModelImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "commommode"));
        }

        UpdateModeToggles();
    }

    public override void OnHide()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    public override void OnBack()
    {
    }

    /// <summary>
    /// 根据阵营最大人数选择对应的rule
    /// </summary>
    private void UpdateTypeByPlayerCount()
    {
        var config = ConfigManager.GetConfig<ConfigGameRule>();
        var dataArr = config.m_dataArr;
        var line = config.GetLine(m_strModeName);
        if (line == null || string.IsNullOrEmpty(line.ParentGameRule))
            return;

        var parentGameRule = line.ParentGameRule;
        var groupList = new List<ConfigGameRuleLine>();
        for (int i = 0; i < dataArr.Length; i++)
        {
            if (dataArr[i].ParentGameRule == parentGameRule && dataArr[i].Type != parentGameRule && dataArr[i].IsCanUse(null, RoomModel.ChannelId))
                groupList.Add(dataArr[i]);
        }

        var maxPlayers = int.Parse(m_strPeopleNum);
        groupList.Sort((a, b) => a.AllPlayersMax() - b.AllPlayersMax());
        if (maxPlayers > groupList[groupList.Count - 1].AllPlayersMax())
            m_strModeName = groupList[groupList.Count - 1].Type;
        else if (maxPlayers <= groupList[0].AllPlayersMax())
            m_strModeName = groupList[0].Type;
        else
        {
            for (int i = 0; i < groupList.Count - 1; i++)
            {
                if (maxPlayers > groupList[i].AllPlayersMax() && maxPlayers <= groupList[i + 1].AllPlayersMax())
                {
                    m_strModeName = groupList[i + 1].Type;
                    break;
                }
            }
        }
        Logger.Log("替换的模式为：{0}    人数为：{1}", m_strModeName, m_strPeopleNum);
    }

    private void UpdateModeToggles()
    {
        var list1 = new List<ConfigGameRuleLine>();
        var list2 = new List<ConfigGameRuleLine>();
        var config = ConfigManager.GetConfig<ConfigGameRule>();
        var line = config.GetLine("killall");
        if (line != null && !string.IsNullOrEmpty(line.ParentGameRule))
            m_strModeName = line.ParentGameRule;

        var arr = config.m_dataArr;
        for (int i = 0; i < arr.Length; i++)
        {
            var gameRuleLine = arr[i];
            if (gameRuleLine.IsShow(null, RoomModel.ChannelId))
            {
                if (gameRuleLine.Mode_Class == (int)ModeClass.Nomarl)
                    list1.Add(gameRuleLine);
                else if (gameRuleLine.Mode_Class == (int)ModeClass.Special)
                    list2.Add(gameRuleLine);
            }
        }

        list1.Sort(sortMode);
        var emptyCount = 15 - list1.Count;
        for (int i = 0; i < emptyCount; i++)
        {
            list1.Add(null);
        }
        list2.Sort(sortMode);
        list1.AddRange(list2);

        m_dataGrid.Data = list1.ToArray();

        var dataGridHas = false;
        for (int i = 0; i < list1.Count; i++)
        {
            if (list1[i] != null && list1[i].Type == m_strModeName)
            {
                dataGridHas = true;
                m_dataGrid.Select(i);
                break;
            }
        }
        if (!dataGridHas)
            m_strModeName = "";   
        if(isShowAllMode)
        {
            m_dataGrid.ShowSelectedMultiData(true);
            isShowAllMode = false;
        }
    }

    private int sortMode(ConfigGameRuleLine a, ConfigGameRuleLine b)
    {
        if (a.ModeSort > b.ModeSort)
        {
            return 1;
        }
        else if (a.ModeSort == b.ModeSort)
        {
            return 0;
        }
        return -1;
    }

    private void OnSelectMode(object renderData)
    {
        var config = renderData as ConfigGameRuleLine;
        if (config == null)
            return;

        m_strModeName = config.Type;
        if (!config.CheckGameTags(new string[] { "survivalMode" }))
        {
            //UpdatePeopleToggles();
        }
        else
        {
            if (m_strLevelNum != null)
            {
                m_strLevelHostMaxNum = 0;
                var arr = PlayerSystem.survival_data.types_data;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].type == m_strModeName)
                    {
                        m_strLevelHostMaxNum = arr[i].top_level;
                        break;
                    }
                }
            }
            //UpdateLevelToggles();
        }

        if (m_strLevelNum != null)
            GameDispatcher.Dispatch(GameEvent.UI_ROOM_MODE_SELECTED, m_strModeName, m_strLevelNum);
    }

    private void OnClickAllModeToggle(GameObject target, PointerEventData eventData)
    {
        if (m_allModeToggle.isOn)
            m_dataGrid.ShowSelectedMultiData(true);
        else
            m_dataGrid.ShowSelectedMultiData(false);
    }

    private void OnClickConfirmMode(GameObject target, PointerEventData eventData)
    {
        //UpdateTypeByPlayerCount();
        var dataToggle = m_dataGrid.SelectedMultiData<ConfigGameRuleLine>();        
        int i = 0; 
        m_rememberModeName = new string[dataToggle.Count];
        foreach(var data in dataToggle)
        {
            m_rememberModeName[i] = data.Type;
            i++;
        }
        //GameDispatcher.Dispatch(GameEvent.UI_ROOM_MODE_SELECTED, m_strModeName, m_strPeopleNum ?? (m_strLevelNum ?? "0"));
        if (m_rememberModeName.Length < 1)
            TipsManager.Instance.showTips("至少需要选择1个模式保存");
        else
            HidePanel();                   
    }
}