﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelLvlPass : BasePanel
{
    UIEffect _effect_player;
    UIEffect m_effect_light;

    UIScrollRect _scroll_rect;
    Transform _map_layer;
    Transform _render_layer;
    GameObject _render_class;
    private Transform _right_arrow_btn;
    

    public PanelLvlPass()
    {
        SetPanelPrefabPath("UI/Room/PanelLvlPass");
        AddPreLoadRes("UI/Room/PanelLvlPass", EResType.UI);
        AddPreLoadRes("UI/Room/PVEMissionItemRender", EResType.UI);
        AddPreLoadRes("Atlas/PVE", EResType.Atlas);
    }

    public override void Init()
    {
        _scroll_rect = m_tran.Find("mission_layer").gameObject.AddComponent<UIScrollRect>();
        _scroll_rect.horizontal = true;
        _scroll_rect.vertical = false;
        _scroll_rect.content = m_tran.Find("mission_layer/content").GetComponent<RectTransform>();
        _map_layer = m_tran.Find("mission_layer/content/map_layer");
        _render_layer = m_tran.Find("mission_layer/content/item_layer");
        _right_arrow_btn = m_tran.Find("mission_layer/right_arrow");
        _render_class = ResourceManager.LoadUIRef("UI/Room/PVEMissionItemRender");
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_right_arrow_btn.gameObject).onPointerClick += onRightArrowClick;
    }

    public override void OnShow()
    {
        if (m_effect_light == null)
            m_effect_light = UIEffect.ShowEffect(m_tran, EffectConst.UI_PVEMISSION_LIGHT_EFFECT, true);
        else
            m_effect_light.Play();

        var config = ConfigManager.GetConfig<ConfigStage>();
        var source_data = new List<ConfigStageLine>();
        for(int i=0;i<config.m_dataArr.Length;i++)
        {
            if(config.m_dataArr[i].StageId<10000)
            {
                source_data.Add(config.m_dataArr[i]);
            }
        }
        int mission_num = 0;
        int old_num = _render_layer.childCount;
        var cur_pos = Vector3.zero;
        var per_pos = Vector3.zero;
        for (mission_num = 0; mission_num < source_data.Count; mission_num++)
        {
            PVEMissionItemRender render = null;
            if (mission_num < old_num)
            {
                render = _render_layer.Find("mission_item_" + mission_num).GetComponent<PVEMissionItemRender>();
            }
            else
            {
                var go = UIHelper.Instantiate(_render_class) as GameObject;
                go.name = "mission_item_" + mission_num;
                go.transform.SetParent(_render_layer,false);
                render = go.AddComponent<PVEMissionItemRender>();
                UGUIClickHandler.Get(render.gameObject).onPointerClick += onMissionItemClick;
            }
            render.SetData(source_data[mission_num]);
            if (source_data[mission_num].StageId == PlayerSystem.roleData.stage || source_data[mission_num].StageId == (PlayerSystem.roleData.stage + 1))
            {
                cur_pos = render.transform.localPosition;
            }
            if (mission_num>0)
            {
                addGuideLine(mission_num, old_num - 1, per_pos, render.transform.localPosition);
            }
            per_pos = render.transform.localPosition;
            if (source_data[mission_num].StageId == (PlayerSystem.roleData.stage+1))
            {
                if (_effect_player != null)
                {
                    _effect_player.Destroy();
                    _effect_player = null;
                }
                _effect_player = UIEffect.ShowEffect(render.transform, EffectConst.UI_PVEMISSION_EFFECT, 0, new Vector2(0, 0));
            }
        }
        int rest_num = _render_layer.childCount - mission_num;
        while (rest_num-->0)
        {
            GameObject.Destroy(_render_layer.Find("mission_item_" + mission_num));
            GameObject.Destroy(_map_layer.Find("guide_line_" + mission_num));
            mission_num++;
        }
        var rect_w = _scroll_rect.GetComponent<RectTransform>().rect.width;
        cur_pos.x -= 80 + rect_w / 2;
        if (cur_pos.x < 0)
        {
            cur_pos.x = 0;
        }
        else if ((cur_pos.x + rect_w) > _scroll_rect.content.sizeDelta.x)
        {
            cur_pos.x = _scroll_rect.content.sizeDelta.x - rect_w;
        }
        _scroll_rect.horizontalNormalizedPosition = cur_pos.x / (_scroll_rect.content.sizeDelta.x - rect_w);
        if (m_params!=null&&m_params.Length != 0)
        {
            int index = (int)m_params[0];
            GameObject aimGo = _render_layer.Find("mission_item_" + (index - 1)).gameObject;
            if (aimGo != null)
            {
                onMissionItemClick(aimGo,null);
            }
        }
    }

    private void addGuideLine(int index,int max,Vector3 per_pos, Vector3 pos)
    {
        Image line_img = null;
        if (index > max)
        {
            line_img = new GameObject("guide_line_" + index).AddComponent<Image>();
            line_img.rectTransform.SetParent(_map_layer);
            line_img.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "line_point"));
            line_img.type = Image.Type.Tiled;
            line_img.rectTransform.pivot = new Vector2(0.05f, 0.5f);
            line_img.rectTransform.localScale = Vector3.one;
            
        }
        else
        {
            line_img = _map_layer.Find("guide_line_" + index).GetComponent<Image>();
        }
        line_img.rectTransform.localPosition = per_pos;
        line_img.rectTransform.sizeDelta = new Vector2(Vector3.Distance(pos,per_pos), 22);
        line_img.rectTransform.localEulerAngles = new Vector3(0, 0, (pos.y<per_pos.y? -1: 1) * Vector3.Angle(Vector3.right, pos - per_pos));
    }

    void onMissionItemClick(GameObject target, PointerEventData eventData)
    {
        var render = target.GetComponent<PVEMissionItemRender>();
        if (render != null && render.Data != null && render.interactive)
        {
            var info = render.Data;
            if (info.StageId <= (PlayerSystem.roleData.stage + 1))
            {
                //UIManager.ShowPanel<PanelPassRoom>(new object[] { info.StageId });
//                tos_room_create_room msg = new tos_room_create_room();
//                msg.type = "stage";
//                msg.typeid = info.StageId;
                PanelPassRoom.m_iStageId = info.StageId;
                NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.stage), stage = info.StageId });
            }
            else
            {
                render.interactive = false;
                TipsManager.Instance.showTips("尚未解锁！");
            }
        }
    }

    private void onRightArrowClick(GameObject target, PointerEventData eventData)
    {
        _scroll_rect.horizontalNormalizedPosition = 1f;
    }

    public override void OnHide()
    {
        if (_effect_player != null)
            _effect_player.Destroy();
        _effect_player = null;

        if (m_effect_light != null)
            m_effect_light.Destroy();
        m_effect_light = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelPVE>();
    }

    public override void Update()
    {
    }

    public override void OnDestroy()
    {
        if (_effect_player != null)
            _effect_player.Destroy();
        _effect_player = null;

        if (m_effect_light != null)
            m_effect_light.Destroy();
        m_effect_light = null;
    }
}
