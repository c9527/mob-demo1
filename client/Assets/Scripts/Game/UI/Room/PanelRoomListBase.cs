﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelRoomListBase : BasePanel
{
    protected ChannelTypeEnum m_channelType;

    protected DataGrid m_dataGrid;
    protected int m_selectedRoomId;
    protected p_room_info[] m_roomInfos;

    protected bool m_enableSmallMap = true;
    protected Image m_imgSmallMap;  //小地图
    protected Text m_txtMapName;    //地图名
    protected Text m_txtfightState;    //状态标题

    protected bool m_enableViewer = true;
    protected GameObject m_goViewerChat;    //观察者聊天对象
    protected Text m_txtViewerNum;           //观察者人数

    protected bool m_enableAutoRefresh = true;
    protected float m_nextRefreshTime = 0;  //下次更新列表时间
    protected float m_refreshInterval = 5;  //更新时间间隔

    protected bool m_enableFilterRule = true;
    public string m_filterRule = "";  //过滤用的规则模式
    private Text m_txtFilterRuleName;       //按钮上显示的模式名称

    protected bool m_enableSort = true;
    protected Dictionary<string, int> m_mapPriorityDic;     //每次点击排序随机一次地图优先级
    protected Dictionary<string, int> m_modePriorityDic;     //每次点击排序随机一次模式优先级

    protected bool m_enableChannelList = true;
    protected Text m_channelName;

    protected Text m_roomNum;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channelType">房间类型，例如：RoomConst.ROOM_TYPE_PV</param>
    protected PanelRoomListBase(ChannelTypeEnum channelType)
    {
        m_channelType = channelType;
        m_mapPriorityDic = new Dictionary<string, int>();
        m_modePriorityDic = new Dictionary<string, int>();
    }

    public override void Init()
    {
        m_channelName = m_tran.Find("room").GetComponent<Text>();

        m_dataGrid = m_tran.Find("pageframe/list/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useLoopItems = true;
        m_dataGrid.SetItemClickSound(AudioConst.selectMallItem);
        m_dataGrid.onItemSelected += OnItemRoomClick;

        if (m_enableSmallMap)
        {
            m_imgSmallMap = m_tran.Find("optionframe/smallMap/map").GetComponent<Image>();
            m_txtMapName = m_tran.Find("optionframe/smallMap/mapname").GetComponent<Text>();
            m_txtfightState = m_tran.Find("optionframe/fightstate/fight").GetComponent<Text>();
        }

        if (m_enableViewer)
        {
            m_goViewerChat = m_tran.Find("optionframe/spector").gameObject;
            m_txtViewerNum = m_tran.Find("optionframe/spector/num").GetComponent<Text>();
        }

        if (m_enableFilterRule)
            m_txtFilterRuleName = m_tran.Find("pageframe/btnMode/Text").GetComponent<Text>();
        var num = m_tran.Find("pageframe/room_num");
        if (num != null)
        {
            m_roomNum = num.GetComponent<Text>();
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("pageframe/btnMode")).onPointerClick += OnRuleClick;
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnViewBattle")).onPointerClick += delegate { JoinRoom(true); };
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnCreatRoom")).onPointerClick += delegate { CreateRoom(); };
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnJoinBattle")).onPointerClick += delegate { JoinRoom(false); };
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnQuickStart")).onPointerClick += delegate { QuickStart(); };
        UGUIClickHandler.Get(m_tran.Find("optionframe/btnRememberChoose")).onPointerClick += RememberChoose;


        if (m_enableSort)
        {
            UGUIClickHandler.Get(m_tran.Find("pageframe/head/imgMap")).onPointerClick += OnMapHeadClick;
            UGUIClickHandler.Get(m_tran.Find("pageframe/head/imgMode")).onPointerClick += OnModeHeadClick;
        }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener<p_room_info>(GameEvent.UI_DOUBLE_CLICK_ROOMLIST, OnDoubleClickRoomList);
#endif

        if (m_enableChannelList)
            UGUIClickHandler.Get(m_tran.Find("pageframe/btnChangeChannel")).onPointerClick += delegate { NetLayer.Send(new tos_player_channel_list()); };

        Transform ruleTipsBtn = m_tran.Find("optionframe/ruleTipsBtn");
        if (ruleTipsBtn != null)
        {
            UGUIClickHandler.Get(ruleTipsBtn).onPointerClick += ShowRuleTips;
            ruleTipsBtn.gameObject.TrySetActive(false);
        }
            
    }

    private void ShowRuleTips(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelRuleTips>();
    }

    private void OnDoubleClickRoomList(p_room_info m_roomData)
    {
        RoomModel.JoinRoom(RoomModel.m_channel, m_roomData.id, m_roomData.password, false, m_roomData.rule, this);
    }


    public override void OnShow()
    {
        m_mapPriorityDic.Clear();
        m_modePriorityDic.Clear();
        m_nextRefreshTime = 0;
        m_selectedRoomId = 0;
        RoomModel.ChannelType = m_channelType;
        m_channelName.text = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.m_channel).Name;

        UpdateRule();
    }
    /// <summary>
    /// 自动进入某一类型的房间
    /// </summary>
    /// <param name="roomRule"></param>
    private void autoEnterRoomByRule(string roomRule)
    {
        m_filterRule = roomRule;
        var list = m_roomInfos;
        if (list == null)
            return;

        //过滤
        if (m_enableFilterRule && !string.IsNullOrEmpty(m_filterRule))
        {
            bool hasRoom = false;
            p_room_info pRoomInfo = new p_room_info();
            var config = ConfigManager.GetConfig<ConfigGameRule>();
            var filteredInfos = new List<p_room_info>();
            for (int i = 0; i < m_roomInfos.Length; i++)
            {
                pRoomInfo = m_roomInfos[i];
                var gameRuleLine = config.GetLine(pRoomInfo.rule);
                if (gameRuleLine == null)
                    continue;
                if (!string.IsNullOrEmpty(gameRuleLine.ParentGameRule))
                {
                    if (m_filterRule == gameRuleLine.ParentGameRule || gameRuleLine.ParentGameRule.IndexOf(m_filterRule) != -1)
                    {
                        m_filterRule = gameRuleLine.ParentGameRule;
                        if (pRoomInfo.player_count < pRoomInfo.max_player)
                        {
                            hasRoom = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (m_filterRule == gameRuleLine.Type)
                    {
                        if (pRoomInfo.player_count < pRoomInfo.max_player)
                        {
                            hasRoom = true;
                            break;
                        }
                    }
                }
            }
            if (hasRoom)
            {
                RoomModel.JoinRoom(RoomModel.m_channel, pRoomInfo.id, pRoomInfo.password, false, pRoomInfo.rule, this);
            }
            else
            {
                RoomModel.m_autoRoomRule = m_filterRule;
                CreateRoomCallBack();
            }
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {
        if (!IsOpen())
            return;

        if (m_enableAutoRefresh && Time.realtimeSinceStartup >= m_nextRefreshTime)
        {
            m_nextRefreshTime = Time.realtimeSinceStartup + m_refreshInterval;
            RefreshRoomList();
        }
    }

    #region 点击事件
    private void OnItemRoomClick(object renderdata)
    {
        var roomInfo = renderdata as p_room_info;
        m_selectedRoomId = roomInfo != null ? roomInfo.id : 0;
        UpdateSmallMap(roomInfo);
        UpdateSpectator(roomInfo);
    }

    private void OnRuleClick(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelModeFliter>(new[] { m_filterRule }, true, this).m_callBack = rule =>
        {
            m_filterRule = rule;
            UpdateRule();
            UpdateRoomList();
        };
    }

    private void OnMapHeadClick(GameObject target, PointerEventData eventData)
    {
        var arr = ConfigManager.GetConfig<ConfigMapList>().m_dataArr;
        var riorityArr = new int[arr.Length];
        for (int i = 0; i < riorityArr.Length; i++)
            riorityArr[i] = i;

        riorityArr.Random();
        for (int i = 0; i < arr.Length; i++)
        {
            m_mapPriorityDic[arr[i].MapId] = riorityArr[i];
        }

        m_modePriorityDic.Clear();
        UpdateRoomList();
    }

    private void OnModeHeadClick(GameObject target, PointerEventData eventData)
    {
        var arr = ConfigManager.GetConfig<ConfigGameRule>().m_dataArr;
        var riorityArr = new int[arr.Length];
        for (int i = 0; i < riorityArr.Length; i++)
            riorityArr[i] = i;

        riorityArr.Random();
        for (int i = 0; i < arr.Length; i++)
        {
            m_modePriorityDic[arr[i].Type] = riorityArr[i];
        }

        m_mapPriorityDic.Clear();
        UpdateRoomList();
    }
    #endregion

    #region 更新视图
    /// <summary>
    /// 更新小地图视图
    /// </summary>
    /// <param name="roomInfo"></param>
    protected void UpdateSmallMap(p_room_info roomInfo)
    {
        if (!m_enableSmallMap)
            return;
        if (roomInfo == null)
        {
            m_txtMapName.text = "";
            m_txtfightState.text = "";
            return;
        }

        var spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, roomInfo.map.ToString());
        if (spriteMap == null)
            spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, "unknown");
        m_imgSmallMap.SetSprite(spriteMap);
        var configMap = ConfigManager.GetConfig<ConfigMapList>().GetLine(roomInfo.map.ToString());
        m_txtMapName.text = configMap != null ? configMap.MapName : "未知";
        if (roomInfo.state == 1)
        {
            m_txtfightState.text = "等待加入";
        }
        else if (roomInfo.state == 2)
        {
            m_txtfightState.text = "战斗中";
        }
        else if (roomInfo.state == 3)
        {
            m_txtfightState.text = "快结束";
        }
    }

    /// <summary>
    /// 更新观战视图
    /// </summary>
    /// <param name="roomInfo"></param>
    protected void UpdateSpectator(p_room_info roomInfo)
    {
        if (!m_enableViewer)
            return;
        m_goViewerChat.TrySetActive(roomInfo != null && roomInfo.spectators_count > 0);
        m_txtViewerNum.text = roomInfo != null ? roomInfo.spectators_count.ToString() : "";
    }

    /// <summary>
    /// 更新房间列表
    /// </summary>
    public void UpdateRoomList()
    {
        var list = m_roomInfos;
        if (list != null && m_roomNum != null)
        {
            m_roomNum.text = "房间数：<color=#00ff00>" + list.Length + "</color>";
        }


        if (list == null)
            return;
        var config = ConfigManager.GetConfig<ConfigGameRule>();
        //过滤
        if (m_enableFilterRule && !string.IsNullOrEmpty(m_filterRule))
        {
            var filteredInfos = new List<p_room_info>();
            bool isAdd = false;
            for (int i = 0; i < m_roomInfos.Length; i++)
            {
                isAdd = false;
                var gameRuleLine = config.GetLine(m_roomInfos[i].rule);
                if (gameRuleLine == null)
                    continue;
                if (!string.IsNullOrEmpty(gameRuleLine.ParentGameRule))
                {
                    if (m_filterRule == gameRuleLine.ParentGameRule)
                        isAdd = true;
                }
                else
                {
                    if (m_filterRule == gameRuleLine.Type)
                        isAdd = true;
                }
                // if (isAdd && !RoomModel.IsDivideByPlat(gameRuleLine, m_roomInfos[i]))
                if (isAdd)
                    filteredInfos.Add(m_roomInfos[i]);
            }
            list = filteredInfos.ToArray();
        }

        //双端分离操作
        for (int k = 0; k < list.Length; k++)
        {
            var cfg = config.GetLine(m_roomInfos[k].rule);
            //被分离
            if (RoomModel.IsDivideByPlat(cfg, list[k]))
            {
                list = list.ArrayDelete<p_room_info>(k);
                k--;
            }
        }

        //排序
        if (m_enableSort)
        {
            if (m_mapPriorityDic.Count > 0)
                Array.Sort(list, (a, b) =>
                {
                    var result = m_mapPriorityDic[a.map.ToString()] - m_mapPriorityDic[b.map.ToString()];
                    if (result == 0)
                        result = (a.player_count == a.max_player).CompareTo(b.player_count == b.max_player);
                    return result;
                });
            else if (m_modePriorityDic.Count > 0)
                Array.Sort(list, (a, b) =>
                {
                    var result = m_modePriorityDic[a.rule] - m_modePriorityDic[b.rule];
                    if (result == 0)
                        result = (a.player_count == a.max_player).CompareTo(b.player_count == b.max_player);
                    return result;
                });
            else
                Array.Sort(list, (a, b) =>
                {
                    var compare = (a.player_count == a.max_player).CompareTo(b.player_count == b.max_player);
                    if (compare == 0)
                    {
                        if (a.leader_id < 0 && b.leader_id > 0)
                            compare = - 1;
                        else if (a.leader_id > 0 && b.leader_id < 0)
                            compare = 1;
                    }
                    return compare;
                });
        }

        m_dataGrid.Data = list;
        if (m_selectedRoomId != 0)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].id == m_selectedRoomId)
                {
                    m_dataGrid.Select(list[i]);
                    break;
                }
            }
        }
        else if (m_selectedRoomId == 0 && list.Length > 0)
            m_dataGrid.Select(list[0]);
        m_channelName.text = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.m_channel).Name;
    }

    public void UpdateRule()
    {
        if (!m_enableFilterRule)
            return;

        var rule = !string.IsNullOrEmpty(m_filterRule) ? ConfigManager.GetConfig<ConfigGameRule>().GetLine(m_filterRule) : null;
        m_txtFilterRuleName.text = rule == null ? "所有模式" : rule.Mode_Name;
    }

    #endregion

    #region Toc消息
    protected void Toc_player_room_list(toc_player_room_list data)
    {
        m_roomInfos = data.list;
        if (m_params != null && m_params.Length != 0)
        {
            m_filterRule = (string)m_params[0];
            autoEnterRoomByRule(m_filterRule);
        }
        UpdateRoomList();
    }

    protected void Toc_player_channel_list(toc_player_channel_list data)
    {
        //新手完成后才加窗口缓动效果
        if (!GuideManager.IsGuideFinish() || GuideManager.IsGuiding())
        {
            UIManager.ShowPanel<PanelChannelList>(data.channels);
            return;
        }

        var pageframe = m_tran.Find("pageframe").GetComponent<RectTransform>();
        var optionframe = m_tran.Find("optionframe").GetComponent<RectTransform>();
        TweenPosition.Begin(pageframe.gameObject, 0.2f, pageframe.localPosition - new Vector3(pageframe.sizeDelta.x + 100f, 0, 0)).method = UITweener.Method.EaseIn;
        TweenPosition.Begin(optionframe.gameObject, 0.2f, optionframe.localPosition + new Vector3(optionframe.sizeDelta.x + 100f, 0, 0)).method = UITweener.Method.EaseIn;
        TimerManager.SetTimeOut(0.2f, () => { UIManager.ShowPanel<PanelChannelList>(data.channels); });
    }
    #endregion

    #region Tos消息
    /// <summary>
    /// 创建房间
    /// </summary>
    protected void CreateRoom()
    {
        if (!RoomModel.IsHasMainWeapon())
        {
            UIManager.ShowTipPanel("当前背包没有主武器，可能影响战斗，是否前往装备？", CreateRoomCallBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                 true, "继续战斗", "前往仓库");
        }
        else if (RoomModel.IsWeaponBroken())
        {
            UIManager.ShowTipPanel("当前背包有破损武器（耐久度为0），可能影响战斗（破损武器伤害下降至10%），是否前往修理？", CreateRoomCallBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                 true, "继续战斗", "前往背包");
        }
        else
        {
            CreateRoomCallBack();
        }
    }

    private void CreateRoomCallBack()
    {
        var channelTemp = 0;
        if (SdkManager.SdkName == "4399iOSKrSdk" && (Driver.m_platform == EnumPlatform.android ||
            Driver.m_platform == EnumPlatform.ios))
            channelTemp = (int)EnumChannelNation.KORO_PHONE;
        else if (SdkManager.SdkName == "4399iOSKrSdk" && Driver.m_platform == EnumPlatform.pc)
            channelTemp = (int)EnumChannelNation.KORO_PC_WEB;
        else
            channelTemp = RoomModel.m_channel;
        NetLayer.Send(new tos_joinroom_create() { channel = channelTemp, stage = 0 });
    }

    /// <summary>
    /// 加入房间
    /// </summary>
    /// <param name="isView">是否观战</param>
    protected void JoinRoom(bool isView)
    {
        if (m_dataGrid.SelectedData<p_room_info>() == null)
        {
            TipsManager.Instance.showTips("请先选择一个房间");
            return;
        }

        var roomInfo = m_dataGrid.SelectedData<p_room_info>();
        RoomModel.JoinRoom(RoomModel.m_channel, roomInfo.id, roomInfo.password, isView, roomInfo.rule, this);
    }

    /// <summary>
    /// 快速开始
    /// </summary>
    protected void QuickStart()
    {
        // NetLayer.Send(new tos_joinroom_quick_join() { channel = RoomModel.channel, rule = m_filterRule });
        //fjs:20151030  增加判断，玩家进入战斗前进行判断，没有主武器，提示“当前背包没有主武器，可能影响战斗，是否前往装备？【前往仓库】【继续战斗】
        if (!RoomModel.IsHasMainWeapon())
        {
            UIManager.ShowTipPanel("当前背包没有主武器，可能影响战斗，是否前往装备？", callStartQuick, () => UIManager.ShowPanel<PanelDepot>(), false,
                 true, "继续战斗", "前往仓库");
        }
        else if (RoomModel.IsWeaponBroken())
        {
            UIManager.ShowTipPanel("当前背包有破损武器（耐久度为0），可能影响战斗（破损武器伤害下降至10%），是否前往修理？", callStartQuick, () => UIManager.ShowPanel<PanelDepot>(), false,
                 true, "继续战斗", "前往背包");
        }
        else
        {
            callStartQuick();
        }
    }

    private void RememberChoose(GameObject target, PointerEventData eventData)
    {
        //Debug.Log(RoomModel.RoomInfo.rule);
        UIManager.PopPanel<PanelModeRemember>(null, true, this);
    }

    /// <summary>
    /// 请求进入快速战斗
    /// </summary>
    void callStartQuick()
    {
        NetLayer.Send(new tos_joinroom_quick_join() { channel = RoomModel.m_channel, rule = m_filterRule, rulelist = PanelModeRemember.m_rememberModeName, source = 0 });
    }

    /// <summary>
    /// 刷新房间列表
    /// </summary>
    protected void RefreshRoomList()
    {
        if (RoomModel.m_channel == 0)
        {
            RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.pvp, new[] { "new", "adv" });
        }
        var channelTemp = 0;
        if (SdkManager.SdkName == "4399iOSKrSdk" && (Driver.m_platform == EnumPlatform.android ||
            Driver.m_platform == EnumPlatform.ios))
            channelTemp = (int)EnumChannelNation.KORO_PHONE;
        else if (SdkManager.SdkName == "4399iOSKrSdk" && Driver.m_platform == EnumPlatform.pc)
            channelTemp = (int)EnumChannelNation.KORO_PC_WEB;
        else
            channelTemp = RoomModel.m_channel;
        NetLayer.Send(new tos_player_room_list() { channel = channelTemp });
    }

    #endregion
}