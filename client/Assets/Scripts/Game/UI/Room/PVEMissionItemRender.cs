﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PVEMissionItemRender : ItemRender
{
    ConfigStageLine _data;

    float _last_interactive_time;
    int _style;

    Transform _desc_canvas;
//    Image _title_bg;
    Image _desc_bg;
    Image _avatar_img;
    Image _title_txt;
    Text _desc_txt;
    Image[] _reward_imgs;


    public bool interactive
    {
        get { return (Time.realtimeSinceStartup - _last_interactive_time) >= 3f; }
        set { _last_interactive_time = value == true ? 0 : Time.realtimeSinceStartup; }
    }

    public ConfigStageLine Data
    {
        get { return _data; }
    }

    public override void Awake()
    {
        _avatar_img = transform.GetComponent<Image>();
        _desc_canvas = transform.Find("desc_canvas");
//        _title_bg = transform.Find("desc_canvas/title_bg").GetComponent<Image>();
        _desc_bg = transform.Find("desc_canvas/desc_bg").GetComponent<Image>();
        _title_txt = transform.Find("desc_canvas/title_txt").GetComponent<Image>();
        _desc_txt = transform.Find("desc_canvas/desc_txt").GetComponent<Text>();

        _reward_imgs = new Image[3];
        _reward_imgs[0] = transform.Find("desc_canvas/reward_img_0").GetComponent<Image>();
        _reward_imgs[1] = transform.Find("desc_canvas/reward_img_1").GetComponent<Image>();
        _reward_imgs[2] = transform.Find("desc_canvas/reward_img_2").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as ConfigStageLine;
        if (_data == null)
        {
            gameObject.TrySetActive(false);
            return;
        }
        gameObject.TrySetActive(true);
        transform.localPosition = new Vector3(_data.Pos.x, _data.Pos.y);
        int index = _data.StageId % 100 - 1;
        ConfigRewardLine reward_data = null;
        if (_data.StageId <= PlayerSystem.roleData.stage)//已通关
        {
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "mission_status_1"));
            _avatar_img.SetNativeSize();
            _title_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "mission_name_" + (index < 10 ? "0" : "") + index + "0"));
            
            reward_data = ConfigManager.GetConfig<ConfigReward>().GetLine(_data.RewardID);
            _desc_canvas.gameObject.TrySetActive(true);
            _desc_txt.text = "";
        }
        else//未通关
        {
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, _data.StageId == (PlayerSystem.roleData.stage+1) ? "mission_status_2" : "mission_status_0"));
            _avatar_img.SetNativeSize();

            _title_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "mission_name_" + (index < 10 ? "0" : "") + index + "0"));
            reward_data = ConfigManager.GetConfig<ConfigReward>().GetLine(_data.FirstRewardID);
            _desc_canvas.gameObject.TrySetActive(_data.StageId == (PlayerSystem.roleData.stage + 1));
            _desc_txt.text = "通关奖励";
        }
        if (_desc_canvas.gameObject.activeSelf)
        {
            updateStyles(reward_data);
            _desc_bg.gameObject.TrySetActive(true);
            _desc_txt.gameObject.TrySetActive(true);
//            _desc_bg.gameObject.TrySetActive(reward_data != null && reward_data.ItemList.Length > 0);
//            _desc_txt.gameObject.TrySetActive(reward_data != null && reward_data.ItemList.Length > 0);
            var offset_x = 20 + _desc_txt.preferredWidth;
            for (int i = 0; i < _reward_imgs.Length; i++)
            {
                var img = _reward_imgs[i];
                if (reward_data != null && i < reward_data.ItemList.Length)
                {
                    img.gameObject.TrySetActive(true);
                    var info = reward_data.ItemList[i];
                    string icon = GetItemIcon(info.ID);
                    img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common,icon));
                    img.SetNativeSize();

                    var txt = img.GetComponentInChildren<Text>();

                    txt.text = info.cnt.ToString();
                    
                    var temp_pos = img.rectTransform.localPosition;
                    temp_pos.x = offset_x;
                    img.rectTransform.localPosition = temp_pos;
                    offset_x += 20 + txt.preferredWidth;
                    if (icon != "gold")
                    {
                        img.transform.localScale = new Vector3(0.3f, 0.3f, 0f);
                        txt.transform.localScale = new Vector3(10f / 3f, 10f / 3f, 0f);
                        txt.transform.localPosition = new Vector3(90, -1, 0);
                    }

                }
                else
                {
                    img.gameObject.TrySetActive(false);
                }
            }
         }
    }

    private void updateStyles(ConfigRewardLine rewardData)
    {
        var scale = new Vector3(0.7f, 0.56f, 1);
        var canvasPos = new Vector3(-55, -29, 0);
        if (rewardData != null)
        {
            scale.Set(1 + 0.25f * (rewardData.ItemList.Length - 1), 1, 1);
            canvasPos.Set(-67f,-42,0);
        }
        _desc_bg.transform.localScale = scale;
        _desc_canvas.transform.localPosition = canvasPos;
    }

    string GetItemIcon(int id)
    {

        var items = ConfigManager.GetConfig<ConfigItemOther>();
        string icon = items.GetLine(id).Icon;
        return icon;
    }
}

