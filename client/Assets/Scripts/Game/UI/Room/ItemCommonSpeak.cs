﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemCommonSpeak : MonoBehaviour
{
    public Text m_commonText;

    private void Awake()
    {
        var tran=transform;
        m_commonText = tran.Find("Text").GetComponent<Text>();
    }

    public void SetText(string Text)
    {
        m_commonText.text = Text;
    }

    public string GetText()
    {
        return m_commonText.text;
    }
    
}

