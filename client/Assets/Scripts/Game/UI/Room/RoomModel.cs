﻿using System;
using System.Collections.Generic;

/// <summary>
/// 房间、天梯战队的数据层
/// </summary>
static class RoomModel
{
    private static toc_room_info m_roomInfo;
    private static toc_room_info m_lastRoomInfo;
    private static ChannelTypeEnum m_channelType;
    private static RoomLeaveType m_roomLeaveType;
    public static bool m_roomswitch = false;
    public static int m_channel = 0;
    public static List<ConfigJokeModeGroupLine> m_rankmodeSelect = new List<ConfigJokeModeGroupLine>();
    public static string m_gamerule;
    public static int matchType = 0;
    /// <summary>
    /// 自动创建房间标志
    /// </summary>
    public static string m_autoRoomRule = null;

    //是否在房间中
    public static bool IsInRoom { get { return m_roomInfo != null; } }
    //房间信息
    public static toc_room_info RoomInfo { get { return m_roomInfo; } }
    //上一次房间信息，在离开房间后，依然可以访问，主要是给结算界面用，回到大厅时清除
    public static toc_room_info LastRoomInfo { get { return m_lastRoomInfo; } }
    //最近一次所在房间的频道类型，或者是刚进入房间列表时主动赋值的频道类型
    public static ChannelTypeEnum ChannelType { get { return m_channelType; } set { m_channelType = value; } }
    //离开房间的类型
    public static RoomLeaveType RoomLeaveType { get { return m_roomLeaveType; } }
    //当前频道id
    public static int ChannelId
    {
        get
        {
            return m_roomInfo != null ? m_roomInfo.channel : FindChannelByType(m_channelType);
        }
    }
    //当前是否是新手关卡
    public static bool IsGuideStage { get { return m_channelType == ChannelTypeEnum.stage && m_roomInfo != null && m_roomInfo.stage == GuideManager.m_guideStageId; } }

    //房间信息变更，包括PVP，PVE
    private static void Toc_room_info(toc_room_info proto)
    {
        m_roomInfo = proto;
        m_lastRoomInfo = proto;
        m_roomLeaveType = RoomLeaveType.None;
        var gamerule = ConfigManager.GetConfig<ConfigChannel>().GetLine(m_roomInfo.channel);
        m_channelType = gamerule.Type;
        m_channel = m_roomInfo.channel;
        if (!UIManager.IsOpen<PanelHall>())
            return;
        if (IsGuideStage)
        {
            NetLayer.Send(new tos_room_start_game());
            return;
        }

        if (gamerule.Type == ChannelTypeEnum.pvp)
        {
            if (m_roomInfo.max_player <= 10)
            {
                if (!UIManager.IsOpen<PanelRoom>())
                {
                    if (UIManager.IsOpen<PanelRoom8v8>())
                    {
                        RoomModel.m_roomswitch = true;
                    }
                    if (!UISystem.isMinimizeRoom)
                    {
                        UIManager.ShowPanel<PanelRoom>();
                    }
                }
            }
            else
            {
                if (!UIManager.IsOpen<PanelRoom8v8>())
                {
                    if (UIManager.IsOpen<PanelRoom>())
                    {
                        RoomModel.m_roomswitch = true;
                    }
                    UIManager.ShowPanel<PanelRoom8v8>();
                }
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.stage)
        {
            if (!UIManager.IsOpen<PanelPassRoom>())
            {
                UIManager.ShowPanel<PanelPassRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.match)
        {
            if (!UIManager.IsOpen<PanelTeam>())
            {
                UIManager.ShowPanel<PanelTeam>(new object[] { m_roomInfo ,matchType});
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.custom)
        {
            if (!UIManager.IsOpen<PanelUserDefRoom>())
            {
                UIManager.ShowPanel<PanelUserDefRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.survival)
        {
            if (!UIManager.IsOpen<PanelSurvivalRoom>())
            {
                UIManager.ShowPanel<PanelSurvivalRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.stage_survival)
        {
            if (!UIManager.IsOpen<PanelZombieRoom>())
            {
                PanelZombieRoom.stageId = m_roomInfo.stage;
                UIManager.ShowPanel<PanelZombieRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.corpsmatch)
        {
            if (!UIManager.IsOpen<PanelCorpsRoom>())
            {
                UIManager.ShowPanel<PanelCorpsRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.defend)
        {
            if (!UIManager.IsOpen<PanelCorpsGuardRoom>())
            {
                UIManager.ShowPanel<PanelCorpsGuardRoom>();
            }
        }
        else if (gamerule.Type == ChannelTypeEnum.corpsmatch_group)
        {
            if (!UIManager.IsOpen<PanelCorpsMatchRoom>())
                if (CorpsDataManager.m_CorpsMatcheftTimes > 0)
                {
                    UIManager.ShowPanel<PanelCorpsMatchRoom>();
                }
                else
                {
                    UIManager.ShowPanel<PanelCorpsEnter>();
                    //if (CorpsDataManager.m_matchStage == 0)
                    //{
                    //TipsManager.Instance.showTips("今日参赛次数已经用完");
                    NetLayer.Send(new tos_room_leave());
                    // }
                }
        }
        else if (gamerule.Type == ChannelTypeEnum.worldboss)
        {
            if (!UIManager.IsOpen<PanelWorldBoss>())
            {
                UIManager.ShowPanel<PanelWorldBoss>();
            }
        }
        else if(gamerule.Type==ChannelTypeEnum.boss4newer)
        {
            /*var panel = UIManager.GetPanel<PanelWorldBoss>();
            if(panel==null||!panel.is4newer)*/
                UIManager.ShowPanel<PanelWorldBoss>(new object[] { "boss4newer" });
        }
         
    }

    //离开房间
    private static void Toc_room_leave_room(toc_room_leave_room data)
    {
        if (data.reason == "kicked")
            m_roomLeaveType = RoomLeaveType.Kicked;
        else if (data.reason == "finished")
            m_roomLeaveType = RoomLeaveType.Finished;
        else if (data.reason == "force")
            m_roomLeaveType = RoomLeaveType.Force;
        else if (data.reason == "timeout")
        {
            m_roomLeaveType = RoomLeaveType.Force;
            UIManager.ShowTipPanel(string.Format("您因长时间未操作，被踢出房间"));
        }
        else if (data.reason == "quit")
        {
            m_roomLeaveType = RoomLeaveType.Quit;
        }
        else
            m_roomLeaveType = RoomLeaveType.Close;

        if (UIManager.IsOpen<PanelHall>())
            UIManager.GetPanel<PanelHall>().SetNormalBack();
        m_roomInfo = null;
    }

    /// <summary>
    /// 自定义RoomLeaveType信息
    /// </summary>
    /// <param name="roomLeaveType"></param>
    public static void SetCustomRoomLeaveType(RoomLeaveType roomLeaveType)
    {
        m_roomLeaveType = roomLeaveType;
        m_roomInfo = null;
    }

    private static void Toc_room_invite(toc_room_invite data)
    {
        if (WorldManager.singleton.fightEntered)
            return;

        string strDesc = "";
        string displayName = "";
        var config = ConfigManager.GetConfig<ConfigChannel>().GetLine(data.channel);
        if (config != null)
            displayName = config.DisplayName;
        else
            displayName = "房间";
        var ruleConifg = ConfigManager.GetConfig<ConfigGameRule>().GetLine(data.rule);
        string strRule = null;
        if (ruleConifg != null)
            strRule = ruleConifg.Mode_Name;
        else if (!string.IsNullOrEmpty(data.name))
            strRule = data.name;

        if (data.type == ChannelTypeEnum.stage_survival.ToString())
        {
            strRule = (ConfigManager.GetConfig<ConfigStageSurvival>().GetLine(data.stage) as ConfigStageSurvivalLine).MissionName;
        }
        strDesc = string.Format("{0}邀请你进入{1}", data.inviter, displayName);
        if (!string.IsNullOrEmpty(strRule))
            strDesc += string.Format("【{0}】", strRule);
        if (!IsPlatformEnable(data.channel))
        {
            strDesc += ",但平台不同无法进入";
            UIManager.ShowTipPanel(strDesc);
            return;
        }
        UIManager.ShowTipPanel(strDesc, () => NetLayer.Send(new tos_joinroom_accept_invite() { channel = data.channel, room = data.room }),
            null, true, true, "接受", "拒绝");
    }

    public static void ShowBattleEndPanel()
    {
        if (IsInRoom)
        {
            if (ChannelType == ChannelTypeEnum.pvp)
            {
                if (m_roomInfo.max_player <= 10)
                {
                    UIManager.ShowPanel<PanelRoom>();
                }
                else
                {
                    UIManager.ShowPanel<PanelRoom8v8>();
                }
            }
            else if (ChannelType == ChannelTypeEnum.stage)
                UIManager.ShowPanel<PanelPassRoom>();
            else if (ChannelType == ChannelTypeEnum.custom)
                UIManager.ShowPanel<PanelUserDefRoom>();
            else if (ChannelType == ChannelTypeEnum.match)
                UIManager.ShowPanel<PanelTeam>();
            else if (ChannelType == ChannelTypeEnum.survival)
                UIManager.ShowPanel<PanelSurvivalRoom>();
            else if (ChannelType == ChannelTypeEnum.stage_survival)
                UIManager.ShowPanel<PanelZombieRoom>();
            else if (ChannelType == ChannelTypeEnum.corpsmatch)
            {
                UIManager.ShowPanel<PanelCorpsRoom>();
            }
            else if (ChannelType == ChannelTypeEnum.corpsmatch_group)
            {
                //请求我的积分，我的战队积分、剩余次数信息
                NetLayer.Send(new tos_corpsmatch_score_info());
            }
            else if (ChannelType == ChannelTypeEnum.defend)
                UIManager.ShowPanel<PanelCorpsGuardRoom>();
            else if (ChannelType == ChannelTypeEnum.worldboss)
            {
                if (PanelWorldBoss.IsOpenEvent()!=0)
                {
                    if (!UIManager.IsOpen<PanelWorldBoss>())
                    {
                        UIManager.ShowPanel<PanelWorldBoss>();
                    }
                }
                else
                {
                    NetLayer.Send(new tos_room_leave());
                    UIManager.ShowPanel<PanelHallBattle>();
                }
            }
            else if (ChannelType == ChannelTypeEnum.boss4newer)
            {
                NetLayer.Send(new tos_room_leave());
                UIManager.ShowPanel<PanelHallBattle>();
            }
            else if (ChannelType == ChannelTypeEnum.legionwar&&m_roomLeaveType == RoomLeaveType.Close)
            {
                NetLayer.Send(new tos_legionwar_data());
                object[] a = new object[1];
                UIManager.PopPanel<PanelLegionMap>(a);
            }
            else
                UIManager.ShowPanel<PanelHallBattle>();
        }
        else
        {
            if (RoomLeaveType == RoomLeaveType.Close && ChannelType == ChannelTypeEnum.stage)
            {
                if (IsGuideStage)
                    UIManager.ShowPanel<PanelHallBattle>();
                else
                    UIManager.ShowPanel<PanelLvlPass>();
            }
            else if (RoomLeaveType == RoomLeaveType.Close && ChannelType == ChannelTypeEnum.survival)
            {
                UIManager.ShowPanel<PanelSurvivalRoomList>();
            }
            else if (ChannelType == ChannelTypeEnum.defend)
            {
                UIManager.ShowPanel<PanelSocity>(new object[] { "corps" });
            }
            else if (RoomLeaveType == RoomLeaveType.Close && ChannelType == ChannelTypeEnum.legionwar)
            {
                NetLayer.Send(new tos_legionwar_data());
                object[] a = new object[1];
                UIManager.PopPanel<PanelLegionMap>(a);
            }
            else
                UIManager.ShowPanel<PanelHallBattle>();

        }
    }
    /// <summary>
    /// 判断房间是否为双端分离
    /// </summary>
    /// <param name="cfg"></param>
    /// <param name="pRoomInfo"></param>
    /// <returns></returns>
    public static bool IsDivideByPlat(ConfigGameRuleLine cfg, p_room_info pRoomInfo)
    {
        if (cfg == null || pRoomInfo == null) return false;
        ConfigChannelLine channelCfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(m_channel) as ConfigChannelLine;
        if (channelCfg == null) return false;
        if (!channelCfg.PlatDivide) return false;
        if (pRoomInfo.fight)
        {//战斗中的房间  跟进游戏规则来做判断
            if (cfg.DivideType == EnumDivideType.entertainment.ToString())
            {//预览娱乐模式不分平台
                return false;
            }
        }
        //竞技模式必须在同端
        if (getPlatTypeByPlatform(pRoomInfo.leader_device) == getPlatTypeByPlatform(Driver.m_platform.ToString()))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// 用设备来获取对应的平台
    /// </summary>
    /// <param name="deviceStr"></param>
    /// <returns></returns>
    public static EnumPlatType getPlatTypeByPlatform(string deviceStr)
    {
        if (deviceStr == EnumPlatform.ios.ToString() || deviceStr == EnumPlatform.android.ToString())
        {
            return EnumPlatType.phone;
        }
        else if (deviceStr == EnumPlatform.pc.ToString() || deviceStr == EnumPlatform.web.ToString())
        {
            return EnumPlatType.pc;
        }
        return EnumPlatType.other;
    }
    /// <summary>
    /// 判断是不是pvp
    /// </summary>
    /// <returns></returns>
    public static bool IsPvp()
    {
        if (ChannelType == ChannelTypeEnum.defend || ChannelType == ChannelTypeEnum.stage || ChannelType == ChannelTypeEnum.survival ||
            ChannelType == ChannelTypeEnum.custom || ChannelType == ChannelTypeEnum.worldboss || ChannelType == ChannelTypeEnum.stage_survival||ChannelType==ChannelTypeEnum.boss4newer)
        {
            return false;
        }
        return true;
    }

    public static bool IsCrossServer(int channel)
    {
        var configChannel = ConfigManager.GetConfig<ConfigChannel>().GetLine(channel);
        if (configChannel == null)
            return false;
        return configChannel.ByCenter;
    }

    public static bool IsPlatformEnable(int channel)
    {
        var configChannel = ConfigManager.GetConfig<ConfigChannel>().GetLine(channel);
        if (configChannel == null)
            return false;
        var enable = false;
        for (int j = 0; j < configChannel.EnablePlatforms.Length; j++)
        {
            if (Driver.m_platform == configChannel.EnablePlatforms[j])
            {
                enable = true;
                break;
            }
        }
        if (!enable)
            return false;

        return true;
    }

    #region 加入房间、跟随玩家通用接口，包含特殊模式提醒、密码输入处理
    private static int m_joinChannel;
    private static bool m_joinNeedPassword;
    private static int m_joinRoomId;    //要加入的房间id
    private static long m_followRoleId;    //要跟随的玩家id
    private static bool m_joinIsView;
    private static BasePanel m_joinParent;

    /// <summary>
    /// 加入房间
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="roomId"></param>
    /// <param name="needPassword"></param>
    /// <param name="isView"></param>
    /// <param name="rule"></param>
    /// <param name="parent"></param>
    public static void JoinRoom(int channel, int roomId, bool needPassword, bool isView, string rule = null, BasePanel parent = null)
    {
        if (roomId == 0)
        {
            TipsManager.Instance.showTips("目标房间不存在");
            return;
        }

        EnterRoom(channel, needPassword, roomId, 0, isView, rule, parent);
    }

    /// <summary>
    /// 跟随角色
    /// </summary>
    /// <param name="roleId"></param>
    /// <param name="needPassword"></param>
    /// <param name="isView"></param>
    /// <param name="rule"></param>
    /// <param name="parent"></param>
    public static void FollowRole(long roleId, bool needPassword, bool isView, string rule = null, BasePanel parent = null)
    {
        if (roleId == 0)
        {
            TipsManager.Instance.showTips("目标角色不存在");
            return;
        }

        EnterRoom(-1, needPassword, 0, roleId, isView, rule, parent);
    }

    /// <summary>
    /// 跟随、观战好友
    /// </summary>
    /// <param name="roleId"></param>
    /// <param name="needPassword"></param>
    /// <param name="isView"></param>
    /// <param name="rule"></param>
    /// <param name="parent"></param>
    public static void FollowFriend(long roleId, bool needPassword, bool isView, string rule = null, BasePanel parent = null)
    {
        var friendData = PlayerFriendData.singleton.GetFriend(roleId);
        if (friendData == null)
        {
            TipsManager.Instance.showTips("对方不是你的好友,不能进入他的房间");
            return;
        }
        if (friendData.m_state == FRIEND_STATE.INROOM || friendData.m_state == FRIEND_STATE.INFIGHT)
        {
            FollowRole(roleId, needPassword, isView, rule, parent);
        }
        else
        {
            TipsManager.Instance.showTips("玩家不在房间或者战斗中，无法跟随");
        }
    }

    private static bool CheckChannel(int channel)
    {
        if (channel == 0)
        {
            TipsManager.Instance.showTips("目标不在房间中");
            return false;
        }

        var channelInfo = ConfigManager.GetConfig<ConfigChannel>().GetLine(channel);
        if (channelInfo == null)
            return false;
        if (channelInfo.Type == ChannelTypeEnum.match)
        {
            TipsManager.Instance.showTips(channelInfo.Name + "不可观战/跟随");
            return false;
        }

        if (!Driver.IsNewServer() && (PlayerSystem.roleData.level < channelInfo.LevelMin || PlayerSystem.roleData.level > channelInfo.LevelMax))
        {
            TipsManager.Instance.showTips(string.Format("进入指定频道{0}级", PlayerSystem.roleData.level < channelInfo.LevelMin ? "需要达到" + channelInfo.LevelMin : "不能超过" + channelInfo.LevelMax));
            return false;
        }
        //检查当前运行平台是否允许进入该频道
        if (!IsPlatformEnable(channel))
        {
            TipsManager.Instance.showTips("该频道不支持跨服");
            return false;
        }



        return true;
    }

    private static void EnterRoom(int channel, bool needPassword, int roomId, long roleId, bool isView, string rule = null, BasePanel parent = null)
    {
        if (channel != -1 && !CheckChannel(channel))
            return;

        m_joinChannel = channel;
        m_joinNeedPassword = needPassword;
        m_joinRoomId = roomId;
        m_followRoleId = roleId;
        m_joinIsView = isView;
        m_joinParent = parent;

        //fjs:20151030  增加判断，玩家进入战斗前进行判断，没有主武器，提示“当前背包没有主武器，可能影响战斗，是否前往装备？【前往仓库】【继续战斗】

        if (!IsHasMainWeapon())
        {
            UIManager.ShowTipPanel("当前背包没有主武器，可能影响战斗，是否前往装备？", null, () => UIManager.ShowPanel<PanelDepot>(), false,
                  true, "继续战斗", "前往仓库");
        }
        else if (IsWeaponBroken())
        {
            UIManager.ShowTipPanel("当前背包有破损武器（耐久度为0），可能影响战斗（破损武器伤害下降至10%），是否前往修理？", null, () => UIManager.ShowPanel<PanelDepot>(), false,
                 true, "继续战斗", "前往背包");
        }
        //特殊模式提示
        if (rule == GameConst.GAME_RULE_DAGGER || rule == GameConst.GAME_RULE_GRENADE || rule == GameConst.GAME_RULE_SNIPE || rule == GameConst.GAME_RULE_CHALLENGE)
            OnReadyJoinRoomBattle(rule, CheckPassword);
        else
            CheckPassword();
    }

    //检查是否需要输入密码
    private static void CheckPassword()
    {
        if (m_joinNeedPassword)
            UIManager.PopPanel<PanelPassward>(null, true, m_joinParent).m_callBack = EnterRoomWithPassword;
        else
            EnterRoomWithPassword("");
    }

    //进入房间的最后一步
    private static void EnterRoomWithPassword(string password)
    {
        if (m_joinRoomId != 0)
            NetLayer.Send(new tos_joinroom_join() { channel = m_joinChannel, password = password, room = m_joinRoomId, spectator = m_joinIsView }, OnErrorJoin);
        else if (m_followRoleId != 0)
            NetLayer.Send(new tos_joinroom_follow() { password = password, roleid = m_followRoleId, spectator = m_joinIsView }, OnError);
    }

    private static void OnError(toc_player_err_msg proto)
    {
        if (proto.err_code != 100003)
            return;
        UIManager.ShowTipPanel("目标房间人员已满，是否进入观战？",
            () => FollowFriend(m_followRoleId, m_joinNeedPassword, true, null, m_joinParent));
    }

    private static void OnErrorJoin(toc_player_err_msg proto)
    {
        if (proto.err_code != 100003)
            return;
        UIManager.ShowTipPanel("目标房间人员已满，是否进入观战？",
            () => JoinRoom(m_joinChannel, m_joinRoomId, m_joinNeedPassword, true, null, m_joinParent));
    }

    /// <summary>
    /// 判断背包中是否有主武器
    /// </summary>
    /// <returns></returns>
    public static bool IsHasMainWeapon()
    {
        bool hasMainWeapon = false;
        p_bag[] weaponBag = ItemDataManager.m_bags;
        int lngth = weaponBag.Length;
        for (int i = 0; i < lngth; i++)
        {
            if (weaponBag[i].equip_list.WEAPON1 > 0)
            {
                hasMainWeapon = true;
                break;
            }
        }
        return hasMainWeapon;
    }

    public static bool IsWeaponBroken()
    {
        var pitem = ItemDataManager.GetDepotItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON1);
        if (pitem != null && pitem.durability == 0)
            return true;

        pitem = ItemDataManager.GetDepotItemByUid(ItemDataManager.m_bags[0].equip_list.WEAPON2);
        if (pitem != null && pitem.durability == 0)
            return true;

        return false;
    }

    private static void OnReadyJoinRoomBattle(string gameRule, Action callBack)
    {
        if (gameRule == GameConst.GAME_RULE_SNIPE)
        {
            bool bHadSnipGun = false;
            p_bag[] playerBag = ItemDataManager.m_bags;
            for (int index = 0; index < playerBag.Length; ++index)
            {
                p_bag bagdata = playerBag[index];
                var weaponConfig = ItemDataManager.GetItemByUid(bagdata.equip_list.WEAPON1) as ConfigItemWeaponLine;
                if (weaponConfig != null && weaponConfig.Class == WeaponManager.WEAPON_TYPE_SNIPER_GUN)
                {
                    bHadSnipGun = true;
                    break;
                }
            }

            if (!bHadSnipGun)
            {
                UIManager.ShowTipPanel("你的背包中没有狙击枪，是否强行加入？", callBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                    true, "确定", "前往仓库");
            }
            else
            {
                callBack();
            }
        }
        else if (gameRule == GameConst.GAME_RULE_GRENADE)
        {
            bool bHadGrenade = false;
            p_bag[] playerBag = ItemDataManager.m_bags;
            for (int index = 0; index < playerBag.Length; ++index)
            {
                p_bag bagdata = playerBag[index];
                var weaponConfig = ItemDataManager.GetItemByUid(bagdata.equip_list.GRENADE) as ConfigItemWeaponLine;
                if (weaponConfig != null && weaponConfig.Class == WeaponManager.WEAPON_TYPE_GRENADE)
                {
                    bHadGrenade = true;
                    break;
                }
            }

            if (!bHadGrenade)
            {
                UIManager.ShowTipPanel("你的背包中没有手雷，是否强行加入？", callBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                    true, "确定", "前往仓库");
            }
            else
            {
                callBack();
            }
        }
        else if (gameRule == GameConst.GAME_RULE_DAGGER)
        {
            bool bHadDagger = false;
            p_bag[] playerBag = ItemDataManager.m_bags;
            for (int index = 0; index < playerBag.Length; ++index)
            {
                p_bag bagdata = playerBag[index];
                var weaponConfig = ItemDataManager.GetItemByUid(bagdata.equip_list.DAGGER) as ConfigItemWeaponLine;
                if (weaponConfig != null && weaponConfig.Class == WeaponManager.WEAPON_TYPE_KNIFE)
                {
                    bHadDagger = true;
                    break;
                }
            }

            if (!bHadDagger)
            {
                UIManager.ShowTipPanel("你的背包中没有刀，是否强行加入？", callBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                    true, "确定", "前往仓库");
            }
            else
            {
                callBack();
            }
        }
        else if(gameRule == GameConst.GAME_RULE_CHALLENGE)
        {
            bool bHadMachineGun = false;
            p_bag[] playerBag = ItemDataManager.m_bags;
            for (int index = 0; index < playerBag.Length; ++index)
            {
                p_bag bagdata = playerBag[index];
                var weaponConfig = ItemDataManager.GetItemByUid(bagdata.equip_list.WEAPON1) as ConfigItemWeaponLine;
                if (weaponConfig != null && weaponConfig.Class == WeaponManager.WEAPON_TYPE_MACHINE_GUN)
                {
                    bHadMachineGun = true;
                    break;
                }
            }

            if (!bHadMachineGun)
            {
                UIManager.ShowTipPanel("该模式适合使用机枪应战，否则将异常凶险", callBack, () => UIManager.ShowPanel<PanelDepot>(), false,
                    true, "我要硬闯", "前往仓库");
            }
            else
            {
                callBack();
            }
        }
    }

    public static int FindChannelByType(ChannelTypeEnum type, string[] subTypes = null)
    {
        var arr = ConfigManager.GetConfig<ConfigChannel>().m_dataArr;
        if (PlayerSystem.roleData == null)
            return 0;
        int level = PlayerSystem.roleData.level;
        for (int i = 0; i < arr.Length; i++)
        {
            var cn = arr[i];
            if (!cn.IsPlatformEnable())
                continue;

            if (cn.Type == type && (Driver.IsNewServer() || level >= cn.LevelMin && level <= cn.LevelMax))
            {
                if (subTypes == null || subTypes.Length == 0 || Array.IndexOf(subTypes, cn.SubType) != -1)
                    return cn.Id;
            }
        }
        return 0;
    }
    #endregion

    public static void SendChannelChoose(string gamerule=null)
    {
        int level = PlayerSystem.roleData.level;
        m_gamerule = gamerule;
        if (level >= 15)
        {
            NetLayer.Send(new tos_player_subtype_room_list() { subtype = "adv" });
        }
        else
        {
            NetLayer.Send(new tos_player_subtype_room_list() { subtype = "new" });
        }
    }

    private static void Toc_player_subtype_room_list(toc_player_subtype_room_list data)
    {
        m_channel = data.channel;
        if (m_gamerule == null)
            UIManager.ShowPanel<PanelPvpRoomList>();
        else
            UIManager.ShowPanel<PanelPvpRoomList>(new[] { m_gamerule });
    }

    private static void Toc_player_available_channel(toc_player_available_channel data)
    {
        if (UIManager.IsOpen<PanelPvpRoomList>() || UIManager.IsOpen<PanelSurvivalRoomList>())
        {
            string info = "当前频道玩家已满，是否跳转到其他频道？";
            Action a = () => { m_channel = data.channel; NetLayer.Send(new tos_player_room_list() { channel = m_channel }); };
            UIManager.ShowTipPanel(info, a, null, true, true, "跳转", "取消");
        }
        else
        {
            TipsManager.Instance.showTips("该频道人数已满，暂时无法跟随进入或观战。");
        }
    }
}

/// <summary>
/// 游戏规则划分  休闲和竞技
/// </summary>
public enum EnumDivideType
{
    entertainment = 1,//休闲类型
    athletics,//竞技类型
}

public enum ChannelTypeEnum
{
    none,
    pvp,
    match,
    stage,
    custom,
    survival,
    corpsmatch,
    defend,
    corpsmatch_group,
    worldboss,    
    stage_survival,
    legionwar,
    boss4newer,
}

enum RoomLeaveType
{
    None,       //在房间中
    Kicked,     //被踢
    Finished,   //关卡完成
    Force,      //强制/主动离开
    Close,
    TimeOut,    //房间中没有操作，超时
    Quit,
}