﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PanelPVE : BasePanel
{
    public static string[] m_RandomNameList = { "Apollo","Brandon","Carl","Duke","Eddie","Fred","Gary","Hank","Ian","Jesse","Kyle",
                                               "Lee","Max","Nelson","Owen","Philip","Randy","Spark"};
    public static string GetRandomName()
    {
        int index = Random.Range(0, m_RandomNameList.Length - 1);
        return m_RandomNameList[index];
    }

    private static void Toc_player_win_stage(toc_player_win_stage proto)
    {
        if (proto.firsttime)
            PlayerSystem.roleData.stage = proto.stageid;
    }
    public PanelPVE()
    {
        SetPanelPrefabPath("UI/Room/PanelPVE");
        AddPreLoadRes("UI/Room/PanelPVE", EResType.UI);  
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    public override void Init()
    { }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame_lvl/lvl")).onPointerClick += OnSelLvlPass;
        UGUIClickHandler.Get(m_tran.Find("frame_user_define/user_define")).onPointerClick += OnSelUserDefine;
        UGUIClickHandler.Get(m_tran.Find("frame_survive/survive")).onPointerClick += OnSelSurvive;
        UGUIClickHandler.Get(m_tran.Find("frame_zombie/survive")).onPointerClick += OnSelZombie;
    }

    public override void OnShow()
    {
        var panelHall = UIManager.GetPanel<PanelHall>();
        if (panelHall != null)
            panelHall.CheckChosen(panelHall.m_btgBattle);
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void Update()
    {
    }

    public override void OnDestroy()
    {
       
    }


    void OnSelZombie(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelZombiePass>();
    }

    void OnSelLvlPass(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowPanel<PanelLvlPass>();
    }

    void OnSelUserDefine(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.custom), stage = 0 });
        //UIManager.ShowPanel<PanelUserDefRoom>();
    }

    void OnSelSurvive(GameObject target, PointerEventData eventData)
    {
        RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.survival);
        UIManager.ShowPanel<PanelSurvivalRoomList>();
    }
}
