﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
/// <summary>
/// 创建房间 地图选择面板
/// </summary>
public class PanelMapSelect : BasePanel {

    private Text m_modeNameText;
    private List<ConfigMapListLine> m_mapList = new List<ConfigMapListLine>();
    private DataGrid m_mapGrid;
    private int m_mapId;
    public PanelMapSelect()
    {
        SetPanelPrefabPath("UI/Room/PanelMapSelect");
        AddPreLoadRes("UI/Room/PanelMapSelect", EResType.UI);
    }
    public override void Init()
    {
        m_modeNameText = m_tran.Find("modeText").GetComponent<Text>();
        m_mapGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        var mapItem = m_tran.Find("ScrollDataGrid/content/smallMap").gameObject;
        m_mapGrid.SetItemRender(mapItem, typeof(MapItemRender));
        m_mapGrid.autoSelectFirst = false;
        m_mapGrid.onItemSelected += OnMapSelect;
    }

    private void OnMapSelect(object renderData)
    {
        var config = renderData as ConfigMapListLine;
        m_mapId = config == null ? m_mapId : int.Parse(config.MapId);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok_btn").gameObject).onPointerClick += delegate
        {
            GameDispatcher.Dispatch<int>(GameEvent.UI_SELECT_MAP, m_mapId);
            HidePanel();
        };
    }

    public override void OnShow()
    {
        if (HasParams() == true)
        {
            m_modeNameText.text = m_params[1].ToString();
            m_mapList = m_params[0] as List<ConfigMapListLine>;
            m_mapId = (int)m_params[2];
        }
        m_mapGrid.Data = m_mapList.ToArray();
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

class MapItemRender : ItemRender
{
    private Text m_txtName;
    private Image m_imgBg;

    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("mapname").GetComponent<Text>();
        m_imgBg = tran.Find("map").GetComponent<Image>();
       
    }

    protected override void OnSetData(object data)
    {
        var info = data as ConfigMapListLine;
        m_txtName.text = info != null ? info.MapName : "";
        var spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, info.MapId.ToString());
        if (spriteMap == null )
        {
            spriteMap = ResourceManager.LoadSprite(AtlasName.SmallMap, "unknown");
        }
        m_imgBg.SetSprite(spriteMap);
    }
}
