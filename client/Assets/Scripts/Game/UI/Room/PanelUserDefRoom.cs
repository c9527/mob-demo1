﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

internal class PanelUserDefRoom : PanelRoomBase
{
    private int m_selectedRobotPos;

    public PanelUserDefRoom()
    {
        SetRoomFunc(new[] {
        RoomFunc.ChangeMode,
        RoomFunc.SmallMap,
        RoomFunc.AutoAim});

        SetPanelPrefabPath("UI/Room/PanelUserDefRoom");
        AddPreLoadRes("UI/Room/PanelUserDefRoom", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRoommate", EResType.UI);
        AddPreLoadRes("UI/Room/ItemRobotSelect", EResType.UI);
        AddPreLoadRes("UI/Room/ItemInviteAI", EResType.UI);
        AddPreLoadRes("Atlas/SmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
    }

    public override void Init()
    {
        base.Init();
        var m_chat = m_tran.Find("pageframe/chatInput").GetComponent<InputField>();
        m_chat.enabled = false;
        var btn = m_tran.Find("pageframe/btnCommonSpeak").gameObject;
        btn.GetComponent<Button>().enabled = false;
        Util.SetGrayShader(btn.GetComponent<Image>());
        
    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_tran.Find("optionframe/quick_add")).onPointerClick += OnQuickAdd;
        AddEventListener<string, string>(GameEvent.UI_ROBOT_SELECT, OnSelectRobot);
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public override void OnHide()
    {
        base.OnHide();
    }

    public override void OnBack()
    {
        base.OnBack();
        UIManager.ShowPanel<PanelPVE>();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void Update()
    {
        base.Update();
    }

    private void CreateRoomMate(p_player_info player)
    {
        var item = ResourceManager.LoadUI("UI/Room/ItemRoommate").AddComponent<ItemRoommate>();
        item.gameObject.GetRectTransform().SetUILocation(m_roomParent, (player.pos - 1)%2*313, ((player.pos - 1)/2)*-69);
        item.gameObject.name = "Item" + ((player.pos - 1)%2) + (player.pos/2);
        item.m_camp = (player.pos - 1)%2;
        item.m_txtName.text = PlayerSystem.GetRoleNameFull(player.name, player.id);

        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)player.vip_level;
        Vector2 anchorName = item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition;
        item.setVipInfo(eType);
       /* if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            item.m_goVip.TrySetActive(false);
            Vector2 vipSizeData = item.m_goVip.GetComponent<RectTransform>().sizeDelta;
            item.m_goPlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2(anchorName.x - vipSizeData.x, anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            item.m_goVip.TrySetActive(true);
            item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            item.m_imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            item.m_goVip.TrySetActive(true);
            item.m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            item.m_imgVip.SetNativeSize();
        }*/

        item.m_txtLv.text = String.Format("{0}级", player.level);
        item.m_imgArmy.SetSprite(ResourceManager.LoadArmyIcon(player.level));
        item.m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(PlayerSystem.roleData.icon, (player.pos - 1)%2));
        item.m_imgHead.SetNativeSize();
        item.title_cnt[0].text = player.mvp.ToString();
        item.title_cnt[0].text = player.gold_ace.ToString();
        item.SetCampColor((player.pos - 1)%2 + 1);
        item.SetState(true, false);
        item.m_iPlayerId = player.id;

        if (player.id > 0)
            item.m_txtName.color = new Color32(134, 253, 131, 255);
        else
            UGUIClickHandler.Get(item.gameObject).onPointerClick += OnClickRoomMate;
        if (player.titles.Length > 0)
        {
            for (int e = 0; e < item.m_title.Length; e++)
            {
                item.m_title[e].SetSprite(ResourceManager.LoadSprite("PlayerInfo", ConfigTitle.GetIcon(player.titles[e].title, player.titles[e].type)));
                item.m_title[e].SetNativeSize();
                item.m_title[e].transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
                item.title_cnt[e].text = player.titles[e].cnt.ToString();
            }
        }
        if(player.id == PlayerSystem.roleId)
        {
            var heroType = HeroCardDataManager.getHeroMaxCardType();
            item.SetHeroCardHead((int)heroType);
        }
    }

    private GameObject CreateRobetSelect(int camp, int playerIndex, int pos)
    {
        var item = ResourceManager.LoadUI("UI/Room/ItemRobotSelect").AddComponent<ItemRobotSelect>();
        item.gameObject.GetRectTransform().SetUILocation(m_roomParent, (camp - 1)*313, playerIndex*-69);
        item.gameObject.name = "Item" + (camp - 1) + playerIndex;
        item.m_camp = camp;
        item.pos = pos;
        item.SetCampColor(camp);
        return item.gameObject;
    }

    private void OnQuickAdd(GameObject target, PointerEventData eventdata)
    {
        NetLayer.Send(new tos_room_quick_robot());
    }

    private void OnClickRoomMate(GameObject target, PointerEventData eventdata)
    {
        var info = target.GetComponent<ItemRoommate>();
        NetLayer.Send(new tos_room_kick_robot {id = info.m_iPlayerId});
    }

    private void OnClickRobetSelect(GameObject target, PointerEventData eventdata)
    {
        var info = target.GetComponent<ItemRobotSelect>();
        m_selectedRobotPos = info.pos;
        string position = target.name.Substring(4);
        UIManager.PopPanel<PanelRobotSelect>(new[] {position}, true);
    }

    private void OnSelectRobot(string name, string pos)
    {
        int rank = 1;
        if (name == "new")
            rank = 1;
        else if (name == "normal")
            rank = 2;
        else if (name == "old")
            rank = 3;
        else if (name == "bt")
            rank = 4;
        NetLayer.Send(new tos_room_robot {index = rank, pos = m_selectedRobotPos});
    }

    protected override void UpdateView()
    {
        base.UpdateView();

        Transform listParent = m_tran.Find("pageframe/list");

        foreach (Transform item in listParent)
        {
            GameObject.Destroy(item.gameObject);
        }

        var info = RoomModel.RoomInfo;
        var people = info.max_player.ToString();
        var hasplayer = new bool[10];
        for (int i = 0; i < info.players.Length; i++)
        {
            CreateRoomMate(info.players[i]);
            hasplayer[info.players[i].pos - 1] = true;
        }
        for (int i = 0; i < int.Parse(people); i++)
        {
            if (!hasplayer[i])
            {
                GameObject goSel = CreateRobetSelect((i)%2 + 1, (i)/2, i + 1);
                UGUIClickHandler.Get(goSel).onPointerClick += OnClickRobetSelect;
            }
        }
    }
}