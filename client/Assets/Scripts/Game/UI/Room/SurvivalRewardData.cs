﻿using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

    public class SurvivalGroupData
    {        

        public List<ItemInfo> itemList=new List<ItemInfo>();                
        public string name;
        
        public void AddItems(string[] strs)
        {            
            //var groups=strs.Split(';');
            //int count = groups.Length;
            //for (int i = 0; i < count; i++)
            //{
            //    string id = groups[i].Split('#')[0];
            //    ConfigRewardLine cfg = ConfigManager.GetConfig<ConfigReward>().GetLine(id);
            //    if (cfg == null)
            //        continue;
            //    for (int j = 0; j < cfg.ItemList.Length; j++)
            //    {
            //        AddItem(cfg.ItemList[j]);
            //    }
            //}
            //Debug.LogError(strs);
            for(int i=0;i<strs.Length;i++)
            {
                var cfg = ConfigManager.GetConfig<ConfigReward>().GetLine(strs[i]);
                if (cfg == null)
                    continue;
                itemList.AddRange(cfg.ItemList);
            }
        }

        //private void AddItem(ItemInfo item)
        //{
        //    var arr = itemList.ToArray();
        //    for(int i=0;i<arr.Length;i++)
        //    {
        //        if(arr[i].ID==item.ID)
        //        {
        //            if (arr[i].day <= 0)
        //                return;
        //            else if (arr[i].day < item.day)
        //            {
        //                arr[i] = item;                        
        //            }
        //            return;
        //        }
        //    }
        //    itemList.Add(item);
        //}

    }

    public class RewarditemRender:ItemRender
    {
        private Image m_icon;
        private Image m_bg;
        private Text m_text;


        public override void Awake()
        {
            m_icon = transform.Find("icon").GetComponent<Image>();
            m_text = transform.Find("name").GetComponent<Text>();
            m_bg = transform.GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            ItemInfo info = data as ItemInfo;
            if (info == null)
                return;
            ConfigItemLine cfg = ItemDataManager.GetItem(info.ID);
            if(cfg!=null)
            {
                m_icon.SetSprite(ResourceManager.LoadIcon(info.ID),cfg.SubType);
                m_icon.SetNativeSize();
                m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(cfg.RareType)));
                if (cfg is ConfigItemWeaponLine)
                {
                    ItemTime time = ItemDataManager.FloatToDayHourMinute(info.day);
                    if (info.day < 0)
                        m_text.text = cfg.DispName;
                    else if (info.day == 0)
                    {
                        if (cfg is ConfigItemWeaponLine)
                            m_text.text = string.Format("{0}(永久)", cfg.DispName);
                        else
                            m_text.text = cfg.DispName;
                    }
                    else
                        m_text.text = string.Format("{0}{1}", cfg.DispName, time.ToString());
                }
                else
                {
                    m_text.text = string.Format("{0}x{1}", cfg.DispName, info.cnt);
                }
            }
        }
    }

    public class RewardGroupRender:ItemRender
    {
        private Text m_difficult;
        private DataGrid m_dataGrid;

        public override void Awake()
        {
            m_difficult = transform.Find("horizontal/difficult").GetComponent<Text>();
            GameObject item0 = transform.Find("horizontal/scrollView/content/item").gameObject;
            item0.TrySetActive(false);
            m_dataGrid = item0.transform.parent.gameObject.AddComponent<DataGrid>();
            m_dataGrid.SetItemRender(item0, typeof(RewarditemRender));
            m_dataGrid.useLoopItems = true;
        }

        protected override void OnSetData(object data)
        {
            SurvivalGroupData d = data as SurvivalGroupData;
            if(d!=null)
            {
                m_dataGrid.Data = d.itemList.ToArray();
                m_difficult.text = d.name;
            }
            
        }
    }

    public class RewardRuleRender:ItemRender
    {
        Text m_text;
        public override void Awake()
        {
            m_text = transform.Find("GameObject").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            m_text.text = (string)data;
        }
    }


public class SurvivalRewardData  {
    private static SurvivalRewardData _ins;
    public static SurvivalRewardData Ins
    {
        get
        {
            if (_ins == null)
                _ins = new SurvivalRewardData();
            return _ins;
        }
    }
    public Dictionary<string, Dictionary<string,SurvivalGroupData>> ruleRewardDic = new Dictionary<string, Dictionary<string, SurvivalGroupData>>();
    //public SurvivalGroupData[] eradicateArray;
    //public SurvivalGroupData[] defendArray;
    //public SurvivalGroupData[] worldBossArray;
	public SurvivalRewardData()
    {
        ConfigFlopRewardLine[] arr = ConfigManager.GetConfig<ConfigFlopReward>().m_dataArr;
        Dictionary<string, SurvivalGroupData> dic = null;
        SurvivalGroupData data=null;
        var tmpRuleRewardDic = new Dictionary<string, Dictionary<string, SurvivalGroupData>>();
        for(int i=0;i<arr.Length;i++)
        {
            ConfigFlopRewardLine cfg = arr[i];
            string key = cfg.Rule + cfg.Level.ToString();
            if (tmpRuleRewardDic.ContainsKey(cfg.RuleName))
                dic = tmpRuleRewardDic[cfg.RuleName];
            else
            {
                dic = new Dictionary<string, SurvivalGroupData>();
                tmpRuleRewardDic.Add(cfg.RuleName,dic );
            }
            if (cfg.RewardList.Length > 0)
            {
                if (dic.ContainsKey(key))
                    data = dic[key];
                else
                {
                    data = new SurvivalGroupData();
                    dic.Add(key, data);
                }
                data.name = cfg.Name;
                data.AddItems(cfg.RewardList);            
            }
        }
        ruleRewardDic.Clear();
	    foreach (var pair in tmpRuleRewardDic)
	    {
	        var rewardDic = pair.Value;
	        if (rewardDic.Count > 0)
	        {
	            ruleRewardDic.Add(pair.Key, pair.Value);
	        }
	    }
    }


}
