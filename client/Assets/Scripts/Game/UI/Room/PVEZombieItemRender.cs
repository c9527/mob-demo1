﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PVEZombieItemRender : ItemRender
{
    ConfigStageSurvivalLine _data;

    float _last_interactive_time;
    int _style;

    Transform _desc_canvas;
    Image _title_bg;
    Image _desc_bg;
    Image _avatar_img;
    Image _title_txt;
    Text _desc_txt;
    Image[] _reward_imgs;


    public bool interactive
    {
        get { return (Time.realtimeSinceStartup - _last_interactive_time) >= 3f; }
        set { _last_interactive_time = value == true ? 0 : Time.realtimeSinceStartup; }
    }

    public ConfigStageSurvivalLine Data
    {
        get { return _data; }
    }

    public override void Awake()
    {
        _avatar_img = transform.GetComponent<Image>();
        _desc_canvas = transform.Find("desc_canvas");
        _title_bg = transform.Find("desc_canvas/title_bg").GetComponent<Image>();
        _desc_bg = transform.Find("desc_canvas/desc_bg").GetComponent<Image>();
        _title_txt = transform.Find("desc_canvas/title_txt").GetComponent<Image>();
        _desc_txt = transform.Find("desc_canvas/desc_txt").GetComponent<Text>();

        _reward_imgs = new Image[3];
        _reward_imgs[0] = transform.Find("desc_canvas/reward_img_0").GetComponent<Image>();
        _reward_imgs[1] = transform.Find("desc_canvas/reward_img_1").GetComponent<Image>();
        _reward_imgs[2] = transform.Find("desc_canvas/reward_img_2").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as ConfigStageSurvivalLine;
        if (_data == null)
        {
            gameObject.TrySetActive(false);
            return;
        }
        gameObject.TrySetActive(true);
        transform.localPosition = new Vector3(_data.Pos.x, _data.Pos.y);
        int index = _data.StageId % 100 - 1;
        ConfigRewardLine reward_data = null;
        if (PanelZombiePass.stageStarInfo.ContainsKey(_data.StageId))//已通关
        {
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "mission_status_1_zombie"));
            _title_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "zombie_room_name_" + _data.StageId));
            // reward_data = ConfigManager.GetConfig<ConfigReward>().GetLine(_data.RewardID);
            _desc_canvas.gameObject.TrySetActive(true);
            _desc_txt.text = "奖励";
        }
        else//未通关
        {
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, _data.StageId == (PanelZombiePass.stageId + 1) ? "mission_status_2_zombie" : "mission_status_0"));
            _title_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.PVE, "zombie_room_name_" + _data.StageId));
            //reward_data = ConfigManager.GetConfig<ConfigReward>().GetLine(_data.FirstRewardID);
            _desc_canvas.gameObject.TrySetActive(_data.StageId == (PanelZombiePass.stageId + 1));
            _desc_txt.text = "首通奖励";
        }
        if (_desc_canvas.gameObject.activeSelf)
        {
            updateStyles(_data.DescStyle);
           // _desc_bg.gameObject.TrySetActive(reward_data != null && reward_data.ItemList.Length > 0);
            _desc_txt.gameObject.TrySetActive(reward_data != null && reward_data.ItemList.Length > 0);
            var offset_x = 20 + _desc_txt.preferredWidth;
            Image img;
            int starNum;
            for (int i = 0; i < _reward_imgs.Length; i++)
            {
                if (PanelZombiePass.stageStarInfo.ContainsKey(_data.StageId))
                {
                    starNum = PanelZombiePass.stageStarInfo[_data.StageId];
                }
                else
                {
                    starNum = -1;
                }
                img = _reward_imgs[i];
                if (i < starNum)
                {
                    img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "star"));
                }
                else
                {
                    img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "starbg"));
                }
            }
        }
    }

    private void updateStyles(int value = -1)
    {
        if (_style == value)
        {
            return;
        }
        _style = value > 0 ? value : _style;
        var pos = new Vector3(35, -7, 0);
//        var bg_scale = new Vector3(1, -1, 1);
//        if (_style == 3)
//        {
////            bg_scale.Set(-1, -1, 1);
//            pos.Set(-195, 0, 0);
//        }
//        else if (_style == 2)
//        {
////            bg_scale.Set(-1, -1, 1);
//            pos.Set(-195, 80, 0);
//        }
        if (_style == 1)
        {
//            bg_scale.Set(1, -1, 1);
            pos.Set(40, 72, 0);
        }
//        _desc_bg.transform.localScale = bg_scale;
        _desc_canvas.localPosition = pos;
    }
}

