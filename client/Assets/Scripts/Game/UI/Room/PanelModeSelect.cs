﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

class ItemOtherRender : ItemRender
{ 
    private Text m_txtName;
    private Image m_imgBg;
    private Toggle m_toggle;
    private Image m_lock;
    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("Background/Checkmark/Text").GetComponent<Text>();
        m_imgBg = tran.Find("Background").GetComponent<Image>();
        m_lock = tran.Find("lock").GetComponent<Image>();
        m_toggle = tran.GetComponent<Toggle>();
        tran.Find("Background/icon").gameObject.TrySetActive(false);
    }

    protected override void OnSetData(object data)
    {
        var info = data as ItemOtherInfo;

        m_toggle.interactable = info != null && info.enable;
        m_imgBg.enabled = info != null;

        m_txtName.text = info != null ? info.label : "";

        m_lock.enabled = info != null && !info.enable;
    }

    public class ItemOtherInfo
    {
        public string label;
        public string data;
        public bool enable;
    }
}

public class PanelModeSelect : BasePanel
{
    private enum ModeClass
    {
        Nomarl = 0,
        Special = 1,
    }

    private GameObject m_modeClone;
    private GameObject m_goSelectNum;
    private GameObject m_goSpeciaMode;
    private Transform m_tranNormalLayout;
    private DataGrid m_dataGrid;
    private DataGrid m_dataGrid2;
    private Transform m_tranNumLayout;
    private Text m_text;
    private GameObject m_tips;

    private string m_strModeName;
    private string m_strPeopleNum;
    private string m_strLevelNum;
    private int m_strLevelHostMaxNum;
    private Image m_SportsModelImg;

    public PanelModeSelect()
    {
        SetPanelPrefabPath("UI/Room/PanelModeSelect");
        AddPreLoadRes("UI/Room/PanelModeSelect", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
    }

    public override void Init()
    {
        m_modeClone = m_tran.Find("frame/copeitem").gameObject;
        m_goSelectNum = m_tran.Find("frame/selectnum").gameObject;
        m_goSpeciaMode = m_tran.Find("frame/speciamode").gameObject;
        m_tranNormalLayout = m_tran.Find("frame/normalgridlayout");
        m_tranNumLayout = m_tran.Find("frame/numgridlayout");
        m_text = m_tran.Find("frame/Text").GetComponent<Text>();
        m_tips = m_tran.Find("frame/tip").gameObject;

        m_SportsModelImg = m_tran.Find("frame/normalmode").gameObject.GetComponent<Image>();

        m_dataGrid = m_tranNormalLayout.gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_modeClone, typeof(ItemModeRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.onItemSelected += OnSelectMode;

        m_dataGrid2 = m_tranNumLayout.gameObject.AddComponent<DataGrid>();
        m_dataGrid2.SetItemRender(m_modeClone, typeof(ItemOtherRender));
        m_dataGrid2.autoSelectFirst = false;
        m_dataGrid2.onItemSelected += OnSelectPeople;

    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/ButtonSure")).onPointerClick += OnClickConfirmMode;
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            m_strModeName = m_params.Length > 0 ? (string) m_params[0] : "";
            m_strPeopleNum = m_params.Length > 1 ? (string) m_params[1] : null;
            m_strLevelNum = m_params.Length > 2 ? (string)m_params[2] : null;
            m_strLevelHostMaxNum = m_params.Length > 3 ? (int)m_params[3] : 0;
        }

        m_goSelectNum.TrySetActive(m_strPeopleNum != null);
        m_goSpeciaMode.TrySetActive(m_strLevelNum == null);
        if (m_strLevelNum == null)
        {
            m_SportsModelImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "SportsModel"));
        }
        else 
        {
            m_SportsModelImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "commommode"));
        }

        m_text.gameObject.TrySetActive(m_strLevelNum != null);
        m_tips.gameObject.TrySetActive(m_strPeopleNum != null);

        UpdateModeToggles();

        if (m_strPeopleNum != null)
            UpdatePeopleToggles();
        else if (m_strLevelNum != null)
            UpdateLevelToggles();
        else
            m_dataGrid2.Data = new object[0];
    }

    public override void OnHide()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    public override void OnBack()
    {
    }

    /// <summary>
    /// 根据阵营最大人数选择对应的rule
    /// </summary>
    private void UpdateTypeByPlayerCount()
    {
        var config = ConfigManager.GetConfig<ConfigGameRule>();
        var dataArr = config.m_dataArr;
        var line = config.GetLine(m_strModeName);
        if (line == null || string.IsNullOrEmpty(line.ParentGameRule))
            return;

        var parentGameRule = line.ParentGameRule;
        var groupList = new List<ConfigGameRuleLine>();
        for (int i = 0; i < dataArr.Length; i++)
        {
            if (dataArr[i].ParentGameRule == parentGameRule && dataArr[i].Type != parentGameRule && dataArr[i].IsCanUse(null, RoomModel.ChannelId))
                groupList.Add(dataArr[i]);
        }

        var maxPlayers = int.Parse(m_strPeopleNum);
        groupList.Sort((a, b) => a.AllPlayersMax() - b.AllPlayersMax());
        if (maxPlayers > groupList[groupList.Count - 1].AllPlayersMax())
            m_strModeName = groupList[groupList.Count - 1].Type;
        else if (maxPlayers <= groupList[0].AllPlayersMax())
            m_strModeName = groupList[0].Type;
        else
        {
            for (int i = 0; i < groupList.Count-1; i++)
            {
                if (maxPlayers > groupList[i].AllPlayersMax() && maxPlayers <= groupList[i + 1].AllPlayersMax())
                {
                    m_strModeName = groupList[i + 1].Type;
                    break;
                }
            }
        }
        Logger.Log("替换的模式为：{0}    人数为：{1}", m_strModeName, m_strPeopleNum);
    }

    private void UpdateModeToggles()
    {
        var list1 = new List<ConfigGameRuleLine>();
        var list2 = new List<ConfigGameRuleLine>();
        var config = ConfigManager.GetConfig<ConfigGameRule>();
        var line = config.GetLine(m_strModeName);
        if (line != null && !string.IsNullOrEmpty(line.ParentGameRule))
            m_strModeName = line.ParentGameRule;

        var arr = config.m_dataArr;
        for (int i = 0; i < arr.Length; i++)
        {
            var gameRuleLine = arr[i];
            if (gameRuleLine.IsShow(null, RoomModel.ChannelId))
            {
                if (gameRuleLine.Mode_Class == (int)ModeClass.Nomarl)
                    list1.Add(gameRuleLine);
                else if (gameRuleLine.Mode_Class == (int)ModeClass.Special)
                    list2.Add(gameRuleLine);
            }
        }

        list1.Sort(sortMode);
        var emptyCount = 15 - list1.Count;
        for (int i = 0; i < emptyCount; i++)
        {
            list1.Add(null);
        }
        list2.Sort(sortMode);
        list1.AddRange(list2);

        m_dataGrid.Data = list1.ToArray();

        var dataGridHas = false;
        for (int i = 0; i < list1.Count; i++)
        {
            if (list1[i] != null && list1[i].Type == m_strModeName)
            {
                dataGridHas = true;
                m_dataGrid.Select(i);
                break;
            }
        }
        if (!dataGridHas)
            m_strModeName = "";
    }

    private int sortMode(ConfigGameRuleLine a, ConfigGameRuleLine b)
    {
        if (a.ModeSort > b.ModeSort)
        {
            return 1;
        }
        else if (a.ModeSort == b.ModeSort)
        {
            return 0;
        }
        return -1;
    }

    private void UpdatePeopleToggles()
    {
        var cgk = ConfigManager.GetConfig<ConfigGameRule>();
        ItemOtherRender.ItemOtherInfo[] list;
        //排除新手频道
        if (m_strModeName != "" && RoomModel.ChannelId != 1 && (cgk.GetLine(m_strModeName).CampPlayersMax == 8 || cgk.GetLine(m_strModeName).CampPlayersMax == 16))
        {
            list = new[]
            {
                new ItemOtherRender.ItemOtherInfo(){ label = "2人", data = "2", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "4人", data = "4", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "6人", data = "6", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "8人", data = "8", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "10人", data = "10", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "12人", data = "12", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "14人", data = "14", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "16人", data = "16", enable = true},
            };
            m_tips.TrySetActive(true);
        }
        else
        {
            list = new[]
            {
                new ItemOtherRender.ItemOtherInfo(){ label = "2人", data = "2", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "4人", data = "4", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "6人", data = "6", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "8人", data = "8", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "10人", data = "10", enable = true},
            };
            m_tips.TrySetActive(false);
            
        }
        if (UIManager.IsOpen<PanelUserDefRoom>())
        {
            list = new[]
            {
                new ItemOtherRender.ItemOtherInfo(){ label = "2人", data = "2", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "4人", data = "4", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "6人", data = "6", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "8人", data = "8", enable = true},
                new ItemOtherRender.ItemOtherInfo(){ label = "10人", data = "10", enable = true},
            };
            m_tips.TrySetActive(false);
        }

        m_dataGrid2.Data = list;
        var dataGridHas = false;
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].data == m_strPeopleNum)
            {
                dataGridHas = true;
                m_dataGrid2.Select(i);
                break;
            }
        }
        if (int.Parse(m_strPeopleNum) > int.Parse(list[list.Length - 1].data))
        {
            m_dataGrid2.Select(4);
            m_strPeopleNum = "10";
            dataGridHas = true;
        }

        if (!dataGridHas)
            m_strModeName = "";
    }

    private void UpdateLevelToggles()
    {
        var list = new[]
        {
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[1], data = "1"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[2], data = "2"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[3], data = "3"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[4], data = "4"},
            new ItemOtherRender.ItemOtherInfo(){ label = PanelSurvivalRoom.m_levelNames[5], data = "5"},
        };
        for (int i = 0; i < list.Length; i++)
        {
            list[i].enable = i <= m_strLevelHostMaxNum;
        }

        m_dataGrid2.Data = list;

        var dataGridHas = false;
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].data == m_strLevelNum && list[i].enable)
            {
                dataGridHas = true;
                m_dataGrid2.Select(i);
                break;
            }
        }
        if (!dataGridHas)
        {
            m_dataGrid2.Select(0);
            m_strLevelNum = "1";
        }
    }

    private void OnSelectMode(object renderData)
    {
        var config = renderData as ConfigGameRuleLine;
        if (config == null)
            return;

        m_strModeName = config.Type;
        if (!config.CheckGameTags(new string[] { "survivalMode" }))
        {
            UpdatePeopleToggles();
        }
        else
        {
            if (m_strLevelNum != null)
            {
                m_strLevelHostMaxNum = 0;
                var arr = PlayerSystem.survival_data.types_data;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].type == m_strModeName)
                    {
                        m_strLevelHostMaxNum = arr[i].top_level;
                        break;
                    }
                }
            }
            UpdateLevelToggles();
        }

        if (m_strLevelNum != null)
            GameDispatcher.Dispatch(GameEvent.UI_ROOM_MODE_SELECTED, m_strModeName, m_strLevelNum);
    }

    private void OnSelectPeople(object renderData)
    {
        var data = renderData as ItemOtherRender.ItemOtherInfo;
        if (data == null)
            return;
        if (!data.enable)
        {
            TipsManager.Instance.showTips("请先通关前一等级难度");
            return;
        }

        if (m_strPeopleNum != null)
            m_strPeopleNum = data.data;
        else if (m_strLevelNum != null)
            m_strLevelNum = data.data;
    }

    private void OnClickConfirmMode(GameObject target, PointerEventData eventData)
    {
        UpdateTypeByPlayerCount();
        GameDispatcher.Dispatch(GameEvent.UI_ROOM_MODE_SELECTED, m_strModeName, m_strPeopleNum ?? (m_strLevelNum ?? "0"));
        HidePanel();
    }

    private void Toc_room_info(toc_room_info proto)
    {
        //生存房主在信息变更时才刷新，可能是模式改变
        if (m_strLevelNum != null && proto.leader == PlayerSystem.roleId)
        {
            m_strLevelHostMaxNum = 0;
            var arr = PlayerSystem.survival_data.types_data;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].type == m_strModeName)
                {
                    m_strLevelHostMaxNum = arr[i].top_level;
                    UpdateLevelToggles();
                    break;
                }
            }
        }
    }
}
