﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemRoommate8 : ItemRoommate
{
    private Image m_imgOK;
    private Image m_imgHeadBack;
    private Image m_imgHeadVip;
    private bool m_fight;
    private bool m_ready;
    private UIEffect m_headEffect;
    private RectTransform m_effectPoint;
    Text m_lV;
    Image m_imgLv;
    private void Awake()
    {
        var tran = transform;
        m_chatTime = -1.0f;
        m_uicamp = 1; ;
        m_imgHead = tran.Find("imgHead/head").GetComponent<Image>();
        m_imgHeadBack = tran.Find("imgHead").GetComponent<Image>();
        m_imgHeadVip = tran.Find("imgHeadVip").GetComponent<Image>();
        m_effectPoint = tran.Find("imgHeadVip/effect").GetComponent<RectTransform>();
        m_imgArmy = tran.Find("imgArmy").GetComponent<Image>();
        m_txtLv = tran.Find("imgLv/Text").GetComponent<Text>();
        m_txtName = tran.Find("txtName").GetComponent<Text>();
        m_imgVip = tran.Find("vip").GetComponent<Image>();
        m_goVip = tran.Find("vip").gameObject;
        m_goPlayerName = tran.Find("txtName").gameObject;
        m_imgOK = tran.Find("imgOK").GetComponent<Image>();
        m_imgHost = tran.Find("imgHost").GetComponent<Image>();
        m_bg = tran.Find("imgBackground").GetComponent<Image>();
        m_speak = tran.Find("speak").GetComponent<Image>();
        m_chat = tran.Find("speak/Text").GetComponent<Text>();
        m_speak.gameObject.TrySetActive(false);
        IsHost = false;
        IsReady = false;
        m_lV = tran.Find("imgLv/LV").GetComponent<Text>();
        m_imgLv = tran.Find("imgLv").GetComponent<Image>();
    }

    public void ChangeUICamp()
    {
        if (m_uicamp == 1)
        {
            m_uicamp = 2;
        }
        else
        {
            m_uicamp = 1;
        }
    }

    public void Update()
    {
        if (m_chatTime < 0)
        {
            return;
        }
        else
        {
            if (Time.time - m_chatTime >= 3)
            {
                HideChat();
            }
        }
    }

    public bool IsHost
    {
        set { m_imgHost.enabled = value; }
        get { return m_imgHost != null && m_imgHost.enabled; }
    }

    public bool IsReady
    {
        set { SetState(value, m_fight); }
        get { return m_ready; }
    }

    public void SetState(bool ready, bool fight)
    {
        m_ready = ready;
        m_fight = fight;
        m_imgOK.enabled = ready || fight;
        if (ready)
            m_imgOK.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "ready"));
        else if (fight)
            m_imgOK.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "fighting"));
        if (m_imgOK.enabled)
            m_imgOK.SetNativeSize();
    }

    public bool IsFight
    {
        get { return m_fight; }
    }


    /// <summary>
    /// 设置VIP信息
    /// </summary>
    /// <param name="type"></param>
    public void setVipInfo(MemberManager.MEMBER_TYPE type, int playerLv = 0, int honor = 0)
    {
        m_goVip.TrySetActive(false);
        int camp = m_camp + 1;
        string str = "red8";
        if (camp == 2)
        {
            str = "blue8";
        }

        switch (type)
        {
            case MemberManager.MEMBER_TYPE.MEMBER_NONE:
                break;
            case MemberManager.MEMBER_TYPE.MEMBER_NORMAL:
                str += "_vip";
                break;
            case MemberManager.MEMBER_TYPE.MEMBER_HIGH:
                str += "_super";
                break;
        }
        if (MemberManager.MEMBER_TYPE.MEMBER_NONE == type)
        {
            //段位积分显示
            ConfigChannelLine channelCfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(3);
            TDScoreTitle stCfg = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(honor);

            if (playerLv >= channelCfg.LevelMin&&stCfg != null && stCfg.BatteRoom8v8Bg != "")
            {
                //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, stCfg.BatteRoom8v8Bg));
                m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            }
            else
                //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, str));
                m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
        }
        else
            //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, str));
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
    }

    public void SetCampColor(int camp)
    {
        if (camp == 2)
        {
            // m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "blue8"));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "bluehead"));
            m_txtName.color = new Color(1, 1, 1);
            m_lV.color = new Color(147f / 255f, 220f / 255f, 240f / 255f);
            m_imgLv.SetSprite(ResourceManager.LoadSprite(AtlasName.FightInTurn, "camp2bg2"));
        }
        else
        {
            //m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "red8"));
            m_imgHeadBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "redhead"));
            m_txtName.color = new Color(254f / 255f, 220f / 255f, 186f / 255f);
            m_lV.color = new Color(255f / 255f, 212f / 255f, 171f / 255f);
            m_imgLv.SetSprite(ResourceManager.LoadSprite(AtlasName.FightInTurn, "camp1bg2"));
        }
    }

    public void SetHeroCardHeadV8(int heroType)
    {
        m_imgHeadVip.gameObject.TrySetActive(false);
        if (heroType != (int)EnumHeroCardType.none)
        {
            m_imgHeadVip.gameObject.TrySetActive(true);
            if (heroType == (int)EnumHeroCardType.normal)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == (int)EnumHeroCardType.super)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == (int)EnumHeroCardType.legend)
            {
                m_imgHeadVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (m_headEffect == null)
                {
                    m_headEffect = UIEffect.ShowEffect(m_effectPoint.transform, EffectConst.UI_HEAD_HERO_CARD_XIAO);
                }
            }
        }
    }

    public void ShowChat(string chat)
    {
        m_chat.text = chat;
        m_speak.gameObject.TrySetActive(true);
        m_chatTime = Time.time;
    }

    public void HideChat()
    {
        m_speak.gameObject.TrySetActive(false);
        m_chatTime = -1.0f;
    }
}
