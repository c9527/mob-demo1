﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelZombieSelect : BasePanel
{
    ConfigItemRoleLine[] _source_data;

    int _page_index = 0;
    int _page_num = 3;

    ZombieItemRender[] _item_list;
    Toggle[] _page_btn_list;

    public PanelZombieSelect()
    {
        SetPanelPrefabPath("UI/Zombie/PanelZombieSelect");
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Role", EResType.Atlas);
        AddPreLoadRes("UI/Zombie/PanelZombieSelect", EResType.UI);
    }

    public override void Init()
    {
        _source_data = ConfigManager.GetConfig<ConfigItemRole>().getZombieList();

        _item_list = new ZombieItemRender[_page_num];
        for (int i = 0; i < _item_list.Length; i++)
        {
            _item_list[i] = m_tran.Find("item_" + i).gameObject.AddComponent<ZombieItemRender>();
        }

        ToggleGroup page_group = m_tran.Find("page_tabar").GetComponent<ToggleGroup>();
        _page_btn_list = new Toggle[(int)(Math.Ceiling(_source_data.Length * 1f / _page_num))];
        for (int j = 0; j < _page_btn_list.Length; j++)
        {
            if (j == 0)
            {
                _page_btn_list[0] = m_tran.Find("page_tabar/page_btn_0").GetComponent<Toggle>();
            }
            else
            {
                _page_btn_list[j] = ((GameObject)UIHelper.Instantiate(_page_btn_list[0].gameObject)).GetComponent<Toggle>();
                _page_btn_list[j].name = "page_btn_" + j;
            }
            _page_btn_list[j].transform.SetParent(page_group.transform);
            _page_btn_list[j].transform.localScale = Vector3.one;
            _page_btn_list[j].transform.localPosition = new Vector3(j * 50, 0, 0);
            _page_btn_list[j].transform.Find("Label").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "num_b" + (j + 1)));
            _page_btn_list[j].group = page_group;
            _page_btn_list[j].isOn = j == 0;
        }
    }


    public override void InitEvent()
    {
        for (int i = 0; i < _item_list.Length; i++)
        {
            UGUIClickHandler.Get(_item_list[i].gameObject).onPointerClick += onZombieItemClick;
        }
        for (int j = 0; j < _page_btn_list.Length; j++)
        {
            UGUIClickHandler.Get(_page_btn_list[j].gameObject).onPointerClick += onPageBtnClick;
        }
        //UGUIDragHandler.Get(m_tran.Find("drag_layer")).onEndDrag += onDragZombieItemView;


        AddEventListener(GameEvent.GAME_BATTLE_ROUND_START, HidePanel);
        AddEventListener(GameEvent.GAME_BATTLE_ROUND_OVER, HidePanel);
        AddEventListener(GameEvent.GAME_BATTLE_OVER, HidePanel);
    }

    private void onPageBtnClick(GameObject target, PointerEventData eventData)
    {
        int.TryParse(target.name.Substring(target.name.LastIndexOf("_") + 1), out _page_index);
        updatePageView();
    }

    private void onDragZombieItemView(GameObject target, PointerEventData eventData)
    {
        var step = (eventData.position - eventData.pressPosition).x > 0 ? 1 : -1;
        _page_index += step;
        if (_page_index >= _page_btn_list.Length)
        {
            _page_index = _page_btn_list.Length - 1;
        }
        if (_page_index < 0)
        {
            _page_index = 0;
        }
        if (_page_index < _page_btn_list.Length)
        {
            _page_btn_list[_page_index].isOn = true;
        }
        updatePageView();
    }




    private void onZombieItemClick(GameObject target = null, PointerEventData eventData = null)
    {
        var info = target != null ? target.GetComponent<ZombieItemRender>().m_renderData as ConfigItemRoleLine : null;
        int id = info != null ? info.ID : PlayerBattleModel.Instance.fight_role;
        int idx = PlayerBattleModel.Instance.getPlayerZombieData(id);
        if (idx > 0)
        {
            HidePanel();
            PlayerBattleModel.Instance.fight_role = id;
            PlayerBattleModel.Instance.TosSelectZombie(idx);
        }
    }

    public override void OnShow()
    {
        _page_index = 0;
        if (_page_index < _page_btn_list.Length)
        {
            _page_btn_list[_page_index].isOn = true;
        }
        updatePageView();
        TimerManager.RemoveTimeOut(HidePanel);
        //TimerManager.SetTimeOut(60, HidePanel);
        TimerManager.SetTimeOut(10, HidePanel);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        Screen.lockCursor = false;
#endif
    }

    private void updatePageView()
    {
        var page_data = new ConfigItemRoleLine[_page_num];
        for (int i = 0; i < _page_num; i++)
        {
            if ((_page_index * _page_num + i) < _source_data.Length)
            {
                page_data[i] = _source_data[_page_index * _page_num + i];
            }
            else
            {
                page_data[i] = null;
            }
        }
        for (int j = 0; j < _item_list.Length; j++)
        {
            _item_list[j].SetData(j < page_data.Length ? page_data[j] : null);
        }
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (UIManager.IsBattle())
            Screen.lockCursor = true;
#endif
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {

    }
}

