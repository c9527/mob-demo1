﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBattleZombie : BasePanel
{
    GameObject m_goZombieHeroInfo;
    UIImgNumText m_kHeroCount;
    UIImgNumText m_kZombieCount;



    private GameObject m_becomeHeroTip;
    private GameObject m_becomeHeroBtn;

    private GameObject m_becomeMatrixTip;
    private GameObject m_becomeMatrixBtn;

    private GameObject m_matrixSwitchWeapon;

    private bool m_isShortRangeWeapon = true;
    private Text m_pcKeyTipsText;

    public PanelBattleZombie()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleZombie");

        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("UI/Battle/PanelBattleZombie", EResType.UI);
    }

    public override void Init()
    {
        m_kHeroCount = new UIImgNumText(m_tran.Find("ZombieHeroInfo/herocount"), "mode", AtlasName.Battle, 0, -8);
        m_kZombieCount = new UIImgNumText(m_tran.Find("ZombieHeroInfo/zombiecount"), "mode", AtlasName.Battle, 0, -8);

        m_becomeHeroTip = m_tran.Find("BecomeZombieHeroTip").gameObject;
        m_becomeHeroTip.TrySetActive(ZombieHeroBehaviour.singleton != null ? ZombieHeroBehaviour.singleton.isShowBecomeHeroTip : false);
        m_becomeHeroBtn = m_tran.Find("BecomeZombieHeroBtn").gameObject;
        m_becomeHeroBtn.TrySetActive(ZombieHeroBehaviour.singleton != null ? ZombieHeroBehaviour.singleton.isShowBecomeHeroTip : false);

        m_becomeMatrixTip = m_tran.Find("BecomeUltimateMatrixTip").gameObject;
        m_becomeMatrixTip.TrySetActive(ZombieUltimateBehaviour.singleton != null ? ZombieUltimateBehaviour.singleton.isShowBecomeMatrixTip : false);
        m_becomeMatrixBtn = m_tran.Find("BecomeUltimateMatrixBtn").gameObject;
        m_becomeMatrixBtn.TrySetActive(ZombieUltimateBehaviour.singleton != null ? ZombieUltimateBehaviour.singleton.isShowBecomeMatrixTip : false);

        m_goZombieHeroInfo = m_tran.Find("ZombieHeroInfo").gameObject;
        m_goZombieHeroInfo.TrySetActive(ZombieHeroBehaviour.singleton != null ? ZombieHeroBehaviour.singleton.isShowInfoPanel : false);

        m_matrixSwitchWeapon = m_tran.Find("MatrixSwitchWeaponBtn").gameObject;
        m_matrixSwitchWeapon.TrySetActive(ZombieUltimateBehaviour.singleton != null ? ZombieUltimateBehaviour.singleton.isShowMatrixSwitchWeapon : false);

        m_pcKeyTipsText = m_tran.Find("PcKeyTipsText").GetComponent<Text>();
        m_pcKeyTipsText.gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_becomeHeroBtn).onPointerClick += OnBecomeHero;
        UGUIClickHandler.Get(m_becomeMatrixBtn).onPointerClick += OnBecomeMatrix;
        UGUIClickHandler.Get(m_matrixSwitchWeapon).onPointerClick += OnSwitchWeapon;

        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            AddEventListener(GameEvent.INPUT_KEY_Q_DOWN, () =>
            {
                if (m_matrixSwitchWeapon.activeSelf)
                    OnSwitchWeapon(m_matrixSwitchWeapon, null);
            });
            AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
            {
                if (m_becomeHeroBtn.activeSelf)
                    OnBecomeHero(null, null);
                else if (m_becomeMatrixBtn.activeSelf)
                    OnBecomeMatrix(null, null);
            });
        }
    }

    public override void OnShow()
    {
        if (m_goZombieHeroInfo != null && m_goZombieHeroInfo.activeSelf)
            RefreshData();
    }

    public override void OnDestroy()
    {

    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }

    public override void Update()
    {

    }

    void OnPlayerDie(BasePlayer killer, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (dier.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
            --m_kZombieCount.value;
        else
            --m_kHeroCount.value;
    }

    void OnPlayerLeave(BasePlayer player)
    {
        if (player.isAlive)
        {
            if (player.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                --m_kZombieCount.value;
            else
                --m_kHeroCount.value;
        }
    }

    void OnBecomeHero(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive)
            NetLayer.Send(new tos_fight_update_actor_type() { actor_type = GameConst.ACTOR_TYPE_HERO });
    }

    void OnBecomeMatrix(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive)
            NetLayer.Send(new tos_fight_update_actor_type() { actor_type = GameConst.ACTOR_TYPE_ULTIMATE_MATRIX });
    }

    void OnSwitchWeapon(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive)
        {
            MainPlayer.singleton.weaponMgr.preWeapon = MainPlayer.singleton.GetNextWeaponID();
            MainPlayer.singleton.SwitchWeapon(MainPlayer.singleton.weaponMgr.preWeapon, true, true);
            SetRangeWeapon(!m_isShortRangeWeapon);
            //img.SetNativeSize();
        }
    }

    public void RefreshData()
    {
        if (m_goZombieHeroInfo == null)
            return;

        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        int heroCount = 0;
        int zombieCount = 0;

        foreach (var player in data.m_allPlayers.Values)
        {
            if (!player.alive)
                continue;
            if (player.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                ++zombieCount;
            else if (player.actor_type == GameConst.ACTOR_TYPE_HERO)
                ++heroCount;
        }

        m_kHeroCount.value = heroCount;
        m_kZombieCount.value = zombieCount;
    }

    public void ShowZombieHeroInfo(bool isShow)
    {
        if (m_goZombieHeroInfo != null)
            m_goZombieHeroInfo.TrySetActive(isShow);
    }

    public void ShowBecomeHeroTip(bool isShow)
    {

        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            if (isShow && m_pcKeyTipsText != null)
            {
                m_pcKeyTipsText.gameObject.TrySetActive(true);
                m_pcKeyTipsText.text = "按 " + GlobalBattleParams.singleton.GetPcKeyTextByKey(GameEvent.INPUT_KEY_E_DOWN) + " 变身为人类英雄";
            }
            else
            {
                if (m_pcKeyTipsText != null)
                    m_pcKeyTipsText.gameObject.TrySetActive(m_becomeHeroBtn.activeSelf || m_becomeMatrixBtn.activeSelf);
            }
        }
        else
        {
            if (m_becomeHeroTip != null)
                m_becomeHeroTip.TrySetActive(isShow);
            if (m_pcKeyTipsText != null)
                m_pcKeyTipsText.gameObject.TrySetActive(false);
        }
    }

    public void ShowBecomeHeroBtn(bool isShow)
    {
        if (m_becomeHeroBtn != null)
            m_becomeHeroBtn.TrySetActive(isShow);
    }

    public void ShowBecomeMatrixTip(bool isShow)
    {
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            if (m_pcKeyTipsText == null && m_becomeHeroBtn == null)
                return;

            if (isShow)
            {
                m_pcKeyTipsText.gameObject.TrySetActive(true);
                m_pcKeyTipsText.text = "按 " + GlobalBattleParams.singleton.GetPcKeyTextByKey(GameEvent.INPUT_KEY_E_DOWN) + " 变身为终极母体";
            }
            else
            {
                m_pcKeyTipsText.gameObject.TrySetActive(m_becomeHeroBtn.activeSelf || m_becomeMatrixBtn.activeSelf);
            }
        }
        else
        {
            if (m_becomeMatrixTip != null)
                m_becomeMatrixTip.TrySetActive(isShow);
            if (m_pcKeyTipsText != null)
                m_pcKeyTipsText.gameObject.TrySetActive(false);
        }
    }

    /// <summary>
    /// PC版判断是否可以切换远程近战
    /// </summary>
    /// <returns></returns>
    public bool isMatrixSwitchWeaponBtnShow()
    {
        return m_matrixSwitchWeapon.activeSelf;
    }

    public void ShowBecomeMatrixBtn(bool isShow)
    {
        if (m_becomeMatrixBtn != null)
            m_becomeMatrixBtn.TrySetActive(isShow);
    }

    public void ShowMatrixSwitchWeaponBtn(bool isShow)
    {
        if (m_matrixSwitchWeapon != null)
        {
            m_matrixSwitchWeapon.TrySetActive(isShow);
            SetRangeWeapon(true);
        }
    }

    private void SetRangeWeapon(bool isShortRange)
    {
        m_isShortRangeWeapon = isShortRange;
        Image img = m_matrixSwitchWeapon.GetComponent<Image>();
        img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_isShortRangeWeapon ? "zombie_long_range_weapon" : "zombie_short_range_weapon"));
    }
}
