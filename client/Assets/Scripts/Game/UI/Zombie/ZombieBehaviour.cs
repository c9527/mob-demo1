﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 生化模式
/// </summary>
class ZombieBehaviour : MonoBehaviour
{
    protected float _maul_time;
    protected Image _maul_tip;

    protected TDGameDrop _drop_info;
    protected bool _show_round_refresh_tip;

    protected bool m_bIsPanelBattleInited;
    protected bool m_bReNotice;
    protected float m_beginTime;
    protected int m_bornTime;

    protected int m_dropItemConfigId = 1020;

    protected virtual void Awake()
    {
        _drop_info = ConfigManager.GetConfig<ConfigGameDrop>().GetGameDropInfo(WorldManager.singleton.gameRule, m_dropItemConfigId);
        NetLayer.AddHandler(this);
        GameDispatcher.AddEventListener<DropItem>(GameEvent.DROPSYS_PRE_PICKUP, OnDelDropItemInScene);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        ConfigGameRuleLine configGameRule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        m_beginTime = configGameRule.Time_begin;
        m_bornTime = ConfigMisc.GetInt("zombie_born_time");
    }

    protected virtual void Start()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }
        if (!m_bIsPanelBattleInited)
        {
            m_bReNotice = true;
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattleNoticePanel);
            return;
        }
    }

    protected virtual void OnPlayerDie(BasePlayer killer, BasePlayer dier, toc_fight_actor_die proto)
    {
        ConfigItemWeaponLine weapon = ItemDataManager.GetItemWeapon(proto.weapon);
        if (weapon != null && weapon.SubWeapon > 0 && WeaponAtkTypeUtil.IsSubWeaponType(proto.atk_type))
        {
            weapon = ItemDataManager.GetItemWeapon(weapon.SubWeapon);
        }

        if (killer != null && killer.curWeapon != null && weapon.Class == (int)WeaponManager.WEAPON_TYPE_KNIFE)
        {
            if (dier.serverData.actor_type == GameConst.ACTOR_TYPE_ELITE || dier.serverData.actor_type == GameConst.ACTOR_TYPE_GENERAL)
            {
                if (dier is MainPlayer)
                {
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "aliveable", 2f, new Vector2(0, 80));
                }
                else
                {
                    NoticePanelProp prop = new NoticePanelProp();
                    //底图
                    prop.bgAltas = AtlasName.BattleNew;
                    prop.bgName = "black_bg";
                    prop.bgSize = new Vector2(382, 60);
                    //特杀图标
                    prop.imgPos = new Vector2(-142, 224);
                    //玩家名字
                    prop.msg = dier.PlayerName + "被近战武器消灭";
                    prop.txtPos = new Vector2(10, 219);
                    prop.textProp = new TextProp() { fontSize = 18 };
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanelQueue(AtlasName.BattleNew, "", 2f, false, new Vector2(-56, 222), prop, new Vector2(19, -81));
                }
            }
        }
    }

    protected virtual void OnDestroy()
    {
        NetLayer.RemoveHandler(this);
        GameDispatcher.RemoveEventListener<DropItem>(GameEvent.DROPSYS_PRE_PICKUP, OnDelDropItemInScene);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattleNoticePanel);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        if (_maul_tip != null)
        {
            GameObject.Destroy(_maul_tip.gameObject);
            _maul_tip = null;
        }
    }

    protected virtual void Update()
    {
        if (SceneManager.singleton.battleSceneLoaded == false)
        {
            return;
        }
        if (_drop_info != null)
        {
            if (_show_round_refresh_tip && PlayerBattleModel.Instance.battle_info.round_time < _drop_info.Time)
            {
                _show_round_refresh_tip = false;
            }
            else if (_show_round_refresh_tip == false && PlayerBattleModel.Instance.battle_info.round_time >= _drop_info.Time)
            {
                _show_round_refresh_tip = true;
                TipsManager.Instance.showTips("弹药即将全部刷新");
            }
        }
        if (_maul_time > 0)
        {
            if (_maul_tip == null)
            {
                _maul_tip = new GameObject("zombie_maul_tip").AddComponent<Image>();
                _maul_tip.gameObject.layer = LayerMask.NameToLayer("UI");
                _maul_tip.rectTransform.SetParent(TipsManager.Instance.transform);
                _maul_tip.rectTransform.localScale = Vector3.one;
                _maul_tip.rectTransform.localPosition = new Vector3(0, -100, 0);
                ResourceManager.LoadSprite(AtlasName.Battle, "hurt_zombie_good", obj =>
                {
                    if (_maul_tip == null)
                    {
                        return;
                    }
                    _maul_tip.SetSprite(obj);
                    _maul_tip.SetNativeSize();
                });
            }
            if (Time.time <= (_maul_time + 2))
            {
                var scale = EaseMethod.UpdateByEaseOut(Time.time - _maul_time, 2f, 1f, 0f);
                _maul_tip.rectTransform.localScale = new Vector3(scale, scale, 1);
            }
            else if (Time.time <= (_maul_time + 2.5))
            {
                var alpha = EaseMethod.UpdateByEaseOut(Time.time - _maul_time - 2f, 0.5f, -1f, 1f);
                _maul_tip.color = new Color(1, 1, 1, alpha);
            }
            if (Time.time >= (_maul_time + 2.5))
            {
                _maul_time = 0;
                GameObject.Destroy(_maul_tip.gameObject);
                _maul_tip = null;
            }
        }
    }

    protected virtual void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        switch (data.state)
        {
            case GameConst.Fight_State_RoundStart:
                PlayNotice();
                break;
        }
    }

    protected virtual void PlayNotice()
    {
        if (!m_bIsPanelBattleInited) return;

        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time - m_beginTime, 0);
        if (PlayerBattleModel.Instance.battle_state.state == 2 && run_time < m_bornTime)
        {
            float duration = m_bornTime - run_time;
            NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
            prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "num_o_second", pos = new Vector2(230, -5) });
            prop.numIconList.Add(new ImgNumProp() { preName = "num_o", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(210, 0) });
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_cd_tips", duration, Vector2.zero, prop);
        }
    }

    protected void OnDelDropItemInScene(DropItem dropItem)
    {
        var info = ConfigManager.GetConfig<ConfigDropItem>().GetLine(dropItem.dropID);
        if (info != null && info.DropType == GameConst.DROP_ITEM_ZOMBIE)
        {
            var str = "";
            var item_list = info.GetItemList();
            for (int i = 0; i < item_list.Length; i++)
            {
                var drop_info = item_list[i];
                if (drop_info.key == 1)
                {
                    str += "获取弹药箱";
                }
                else if (drop_info.key == 2)
                {
                    str += "获得武器箱";
                }
                else if (drop_info.key == 3)
                {
                    var buff_info = ConfigManager.GetConfig<ConfigBuff>().GetLine(drop_info.value);
                    str += buff_info.Desc + (buff_info.DamageRate > 0 ? "+" + buff_info.DamageRate * 100 + "%" : "+" + buff_info.MaxHP);
                }
                if ((i + 1) < item_list.Length)
                {
                    str += "\n";
                }
            }
            TipsManager.Instance.showTips(str);
        }
    }

    protected void Toc_fight_maul_heavily(toc_fight_maul_heavily data)
    {
        _maul_time = Time.time;
    }

    protected void OnCreateBattleNoticePanel()
    {
        m_bIsPanelBattleInited = true;
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattleNoticePanel);
        RePlayNotice();
    }

    virtual protected void RePlayNotice()
    {
        if (m_bReNotice)
        {
            PlayNotice();
        }
    }
}

