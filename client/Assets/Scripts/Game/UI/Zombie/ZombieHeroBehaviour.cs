﻿using UnityEngine;

/// <summary>
/// 生化英雄模式（基于生化模式）
/// </summary>
class ZombieHeroBehaviour : ZombieBehaviour
{
    public static ZombieHeroBehaviour singleton;

    protected GameObject m_goZombieHeroInfo;
    protected PanelBattleZombie m_panelZombie;
    protected bool m_bIsZombieHeroStage;
    protected bool m_bIsShowInfoPanel;
    protected bool m_bIsShowBecomeHeroTip;

    protected int m_iNoticeTimerId = -1;
    //protected int m_iTipsTimerId = -1;

    protected float m_roundTime;
    protected int m_heroTime;

    private float m_tick = 0;

    public ZombieHeroBehaviour()
    {
        singleton = this;
    }

    //覆盖函数
    protected override void Awake()
    {
        base.Awake();
        NetLayer.AddHandler(this);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener<bool>(GameEvent.UI_SHOW_ZOMBIEHERO_INFO, ShowZombieHeroInfo);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);

        ConfigGameRuleLine configGameRule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        m_roundTime = configGameRule.Time_play;
        m_heroTime = ConfigMisc.GetInt("zombie_hero_time");
    }

    protected void OnSceneLoaded()
    {
        PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
        if (battlePanel != null && battlePanel.IsInited())
            Init();
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        Init();
    }

    protected void Init()
    {
        m_panelZombie = UIManager.PopPanel<PanelBattleZombie>();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.UI_SHOW_ZOMBIEHERO_INFO, ShowZombieHeroInfo);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);

        ShowZombieHeroInfo(false);
        if (m_iNoticeTimerId != -1)
            TimerManager.RemoveTimeOut(m_iNoticeTimerId);
        GameObject.Destroy(m_goZombieHeroInfo);
    }

    protected override void Update()
    {
        base.Update();
        m_tick += Time.deltaTime;
        if (m_tick >= 1f)
        {
            m_tick = 0;
            OnBattleInfoUpdate();
        }
    }

    protected override void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        switch (data.state)
        {
            case GameConst.Fight_State_RoundStart:
                if (m_iNoticeTimerId != -1)
                    TimerManager.RemoveTimeOut(m_iNoticeTimerId);
                m_iNoticeTimerId = -1;
                HideZombiePanelInfo();
                m_bIsZombieHeroStage = false;
                PlayNotice();
                break;
            case GameConst.Fight_State_RoundEnd:
                TimerManager.SetTimeOut(2f, () => { HideZombiePanelInfo(); });
                if (m_iNoticeTimerId != -1)
                    TimerManager.RemoveTimeOut(m_iNoticeTimerId);
                m_iNoticeTimerId = -1;
                break;
        }
    }

    protected override void PlayNotice()
    {
        if (!m_bIsPanelBattleInited)
        {
            m_bReNotice = true;
            return;
        }
        m_bReNotice = false;

        if (PlayerBattleModel.Instance.battle_state.state == 2)
        {
            var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time - m_beginTime, 0);
            m_bIsZombieHeroStage = IsZombieHeroStage();
            ShowZombieHeroInfo(m_bIsZombieHeroStage);

            if (run_time < m_bornTime)
            {
                NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "num_o_second", pos = new Vector2(230, -5) });
                prop.numIconList.Add(new ImgNumProp() { preName = "num_o", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(210, 0) });
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_cd_tips", m_bornTime - run_time, new Vector2(0, 0), prop);
                m_iNoticeTimerId = TimerManager.SetTimeOut(m_heroTime - run_time - 6, () =>
                {
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_hero_is_coming", 5, new Vector2(0, 130));
                    AudioManager.PlayFightUISound(AudioConst.finalMoment);
                }, true);
                //m_iTipsTimerId = TimerManager.SetTimeOut(m_heroTime - run_time, () => { ShowBecomeHeroTip(true); }, true);
            }
            else if (run_time < m_heroTime - 6)
            {
                m_iNoticeTimerId = TimerManager.SetTimeOut(m_heroTime - run_time - 6, () =>
                {
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_hero_is_coming", 5, new Vector2(0, 130));
                    AudioManager.PlayFightUISound(AudioConst.finalMoment);
                }, true);
                //m_iTipsTimerId = TimerManager.SetTimeOut(m_heroTime - run_time, () => { ShowBecomeHeroTip(true); }, true);
            }
            else if (run_time <= m_heroTime)
            {
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_hero_is_coming", m_heroTime - run_time + 1, new Vector2(0, 130));
                AudioManager.PlayFightUISound(AudioConst.finalMoment);
                //m_iTipsTimerId = TimerManager.SetTimeOut(m_heroTime - run_time, () => { ShowBecomeHeroTip(true); }, true);
            }
        }
    }

    //特有函数
    protected virtual void OnPlayerRevive(string name, BasePlayer player)
    {        
        if (player.serverData.actor_type == GameConst.ACTOR_TYPE_HERO ) //生化英雄
        {
            if (MainPlayer.singleton != null && player.PlayerId == MainPlayer.singleton.PlayerId)
            {
                ShowBecomeHeroTip(false);
            }
            if(!m_bIsZombieHeroStage)
            {
                m_bIsZombieHeroStage = true;
                UIManager.GetPanel<PanelBattle>().StopNoticePanel();
                ShowZombieHeroInfo(true);          
            }
          
        }
    }

    protected override void OnPlayerDie(BasePlayer killer, BasePlayer dier, toc_fight_actor_die proto)
    {
        base.OnPlayerDie(killer, dier, proto);
        if (dier != MainPlayer.singleton)
        {
            return;
        }
        ShowBecomeHeroTip(false);
        
    }

    protected void OnBattleInfoUpdate()
    {
        if (PlayerBattleModel.Instance.battle_info != null)
        {
            if (m_bIsZombieHeroStage)
            {
                if (m_panelZombie != null)
                    m_panelZombie.RefreshData();
            }
        }
    }

    protected void ShowZombieHeroInfo(bool isShow)
    {
        m_bIsShowInfoPanel = isShow;
        if (m_panelZombie != null)
            m_panelZombie.ShowZombieHeroInfo(isShow);
    }

    protected void ShowBecomeHeroTip(bool isShow)
    {
        if (isShow)
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.isAlive && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.ZOMBIE && MainPlayer.singleton.serverData.actor_type != GameConst.ACTOR_TYPE_HERO )
            {
                m_bIsShowBecomeHeroTip = true;
                if (m_panelZombie != null)
                {
                    m_panelZombie.ShowBecomeHeroTip(true);
                    m_panelZombie.ShowBecomeHeroBtn(true);
                }
            }
        }
        else
        {
            m_bIsShowBecomeHeroTip = false;
            if (m_panelZombie != null)
            {
                m_panelZombie.ShowBecomeHeroTip(false);
                m_panelZombie.ShowBecomeHeroBtn(false);
            }
        }
    }

    virtual protected void HideZombiePanelInfo()
    {
        ShowZombieHeroInfo(false);
        ShowBecomeHeroTip(false);
    }

    protected bool IsZombieHeroStage()
    {
        foreach (BasePlayer player in WorldManager.singleton.allPlayer.Values)
        {
            if (player.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
            {
                return true;
            }
        }
        return false;
    }

    virtual protected void Toc_fight_hero_time(toc_fight_hero_time data)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.isAlive && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.ZOMBIE && MainPlayer.singleton.serverData.actor_type != GameConst.ACTOR_TYPE_HERO)
        {
            if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_BECOME_HERO) != 0)
                NetLayer.Send(new tos_fight_update_actor_type() { actor_type = GameConst.ACTOR_TYPE_HERO });
            else
                ShowBecomeHeroTip(true);
        }
    }

    public bool isZombieHeroStage
    {
        get { return m_bIsZombieHeroStage; }
    }

    public bool isShowInfoPanel
    {
        get { return m_bIsShowInfoPanel; }
    }

    public bool isShowBecomeHeroTip
    {
        get { return m_bIsShowBecomeHeroTip; }
    }
}


