﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class ZombieItemRender : ItemRender
{
    ConfigItemRoleLine _data;
    ConfigMallLine _mallLine;
    Image _item_img;
    Text _name_txt;
    Image _status_ico;
    GameObject _btnBuy;
    Text _buyInfoTxt;
    Image _coin;

    public override void Awake()
    {
        _item_img = transform.Find("item_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _status_ico = transform.Find("status_ico").GetComponent<Image>();
        _btnBuy = transform.Find("btnBuy").gameObject;
        _coin = transform.Find("btnBuy/Icon").GetComponent<Image>();
        _buyInfoTxt = _btnBuy.transform.Find("Text").GetComponent<Text>();
        UGUIClickHandler.Get(_btnBuy).onPointerClick += onClickBtnBuyHandler;
    }

    private void onClickBtnBuyHandler(GameObject target, PointerEventData eventData)
    {
        //NetLayer.Send(new tos_player_buy_item() { mall_id = _mallLine.ID, days = 1, in_blackmarket=false });
        UIManager.PopPanel<PanelBuy>(new object[] { _mallLine, "", false }, true);
    }

    protected override void OnSetData(object data)
    {
        _data = data as ConfigItemRoleLine;
        if (_data == null)
        {
            gameObject.TrySetActive(false);
            return;
        }
        gameObject.TrySetActive(true);

        _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Role, _data.ID.ToString()));
        _item_img.SetNativeSize();
        _name_txt.text = _data.DispName;
        _mallLine = ConfigManager.GetConfig<ConfigMall>().GetMallLine(_data.ID);
        if (_mallLine == null)
        {
            _btnBuy.TrySetActive(false);
        }
        else
        {
            _btnBuy.TrySetActive(true);
            int price = 0;
            int lngth = _mallLine.Price.Length;
            for (int i = 0; i < lngth; i++)
            {
                if (_mallLine.Price[i].time == 1)
                {
                    price = _mallLine.Price[i].price;
                    if (_mallLine.MoneyType == EnumMoneyType.DIAMOND)
                    {
                        _coin.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "diamond"));
                    }
                    if (_mallLine.MoneyType == EnumMoneyType.COIN)
                    {
                        _coin.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "gold"));
                    }
                    if (_mallLine.MoneyType == EnumMoneyType.CASH)
                    {
                        _coin.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "cash"));
                    }
                    if (_mallLine.MoneyType == EnumMoneyType.MEDAL)
                    {
                        _coin.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "medal"));
                    }
                    if (_mallLine.MoneyType == EnumMoneyType.EVENTCOIN_002)
                    {
                        _coin.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voucher"));
                    }
                    break;
                }
            }

            _buyInfoTxt.text = ItemDataManager.GetPrice(_mallLine,price).ToString() + "(限时1天)";
            
            

        }
        bool hasOwner = PlayerBattleModel.Instance.getPlayerZombieData(_data.ID) > 0;
        _status_ico.enabled = !hasOwner;
        _btnBuy.TrySetActive(!hasOwner);
    }
}
