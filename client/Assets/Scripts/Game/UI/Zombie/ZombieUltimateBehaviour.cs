﻿
using System;
using UnityEngine;
public class ZombieUltimateEvent
{
    public const string START_ZOMBIE2HUMAN = "start_zombie2human";       //开始僵尸变人类
}


/// <summary>
/// 终于生化模式（基于生化英雄）
/// </summary>
class ZombieUltimateBehaviour : ZombieHeroBehaviour
{
    public static ZombieUltimateBehaviour singleton;
    //常量定义
    static readonly Vector2 ProgressBarPos = new Vector2(0, -115);
    static readonly Vector2 ProgressBarTextPos = new Vector2(0, -115);
    static readonly Vector2 Zombie2HumanBtnPos = new Vector2(260, -115);
    const string Zombie2HumanFormatStr = "{0}s内，使用解药，变回人类";
    const string PC_Zombie2HumanFormatStr = "{0}s内，按E使用解药，变回人类";

    const string Zombie2HumanValidateStr = "解药将在2s后生效";
    const string Zombie2HumanTipStr = "消灭一个人即可获得解药";
    const float Zombie2HumanValidateTime = 2f;
    private int m_matrixZombieId;
    private bool m_showTipBtn = false;
    protected bool m_bIsShowBecomeMatrixTip;
    protected bool m_bIsPlayGameStartTip = false;
    protected bool m_bIsShowMatrixSwitchWeapon;

    public ZombieUltimateBehaviour()
    {
        singleton = this;
    }

    protected override void Awake()
    {
        base.Awake();
        m_heroTime = ConfigMisc.GetInt("ultimate_hero_time");
        m_matrixZombieId = ConfigMisc.GetInt("matrix_zombie_id");
        GameDispatcher.AddEventListener(ZombieUltimateEvent.START_ZOMBIE2HUMAN, StartZombie2human);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameDispatcher.RemoveEventListener(ZombieUltimateEvent.START_ZOMBIE2HUMAN, StartZombie2human);
    }

    protected override void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;
        switch (data.state)
        {
            case GameConst.Fight_State_GameInit:
                if (m_bIsPanelBattleInited)
                {
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.Battle, "zombie_ultimate_game_start_tip", 5, false,  new Vector2(0, 60f));
                    AudioManager.PlayFightUISound(AudioConst.finalMoment);
                    m_bIsPlayGameStartTip = true;
                }
                break;
            case GameConst.Fight_State_RoundStart:
                if (m_iNoticeTimerId != -1)
                    TimerManager.RemoveTimeOut(m_iNoticeTimerId);
                m_iNoticeTimerId = -1;
                //if (m_iTipsTimerId != -1)
                //    TimerManager.RemoveTimeOut(m_iTipsTimerId);
                //m_iTipsTimerId = -1;
                HideZombiePanelInfo();
                CloseZombie2Human();
                ShowProgress(false);
                m_bIsZombieHeroStage = false;
                PlayNotice();
                if (UIManager.IsOpen<PanelBattle>())
                    UIManager.GetPanel<PanelBattle>().ShowBattleTip(false);
                break;
            case GameConst.Fight_State_RoundEnd:
                TimerManager.SetTimeOut(2f, () => { HideZombiePanelInfo(); });
                if (m_iNoticeTimerId != -1)
                    TimerManager.RemoveTimeOut(m_iNoticeTimerId);
                m_iNoticeTimerId = -1;
                //if (m_iTipsTimerId != -1)
                //    TimerManager.RemoveTimeOut(m_iTipsTimerId);
                //m_iTipsTimerId = -1;
                break;
        }
    }

    protected override void OnPlayerRevive(string name, BasePlayer player)
    {
        if (MainPlayer.singleton != null && player.PlayerId == MainPlayer.singleton.PlayerId)
        {
            if (player.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
            {
                ShowBecomeHeroTip(false);
            }
            else if (player.serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX)
            {
                ShowBecomeMatrixTip(false);
                if (m_panelZombie != null)
                    m_panelZombie.ShowMatrixSwitchWeaponBtn(true);
            }
        }

        if ((player.serverData.actor_type == GameConst.ACTOR_TYPE_HERO || player.serverData.actor_type == GameConst.ACTOR_TYPE_ULTIMATE_MATRIX) && !m_bIsZombieHeroStage)
        {
            m_bIsZombieHeroStage = true;
            UIManager.GetPanel<PanelBattle>().StopNoticePanel();
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "zombie_ultimate_final_time_tip", 5, Vector2.zero);
            ShowZombieHeroInfo(true);
            AudioManager.PlayFightUISound(AudioConst.finalMoment);
        }
        else if (player is MainPlayer && UIManager.IsOpen<PanelBattle>())
        {
            if (player.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && player.RoleId != m_matrixZombieId)
            {
                if (!m_showTipBtn)//第一次show
                {
                    UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(true, false, Zombie2HumanTipStr, 260);
                    m_showTipBtn = true;
                }
                UIManager.GetPanel<PanelBattle>().ShowBattleTipMsg(Zombie2HumanTipStr);
                UIManager.GetPanel<PanelBattle>().ShowBattleTip(true);
            }
            else
            {
                UIManager.GetPanel<PanelBattle>().ShowBattleTip(false);
            }
        }
    }

    protected override void OnPlayerDie(BasePlayer killer, BasePlayer dier, toc_fight_actor_die proto)
    {
        base.OnPlayerDie(killer, dier, proto);
        if (dier != MainPlayer.singleton)
        {
            return;
        }
        ShowBecomeMatrixTip(false);
        if (m_panelZombie != null)
            m_panelZombie.ShowMatrixSwitchWeaponBtn(false);
    }

    protected override void PlayNotice()
    {
        base.PlayNotice();
    }

    private void StartZombie2human()
    {
        Logger.Log("ZombieUltimateBehaviour:StartZombie2human");
        CloseZombie2Human();
        NetLayer.Send(new tos_fight_start_zombie2human());
    }

    private void Toc_fight_whether2human(toc_fight_whether2human proto)
    {
        Logger.Log("ZombieUltimateBehaviour:Toc_fight_whether2human-->" + proto.time_limit);
        if (UIManager.IsOpen<PanelBattle>())
        {
            UIManager.GetPanel<PanelBattle>().ShowZombie2HumanBtn(true, Zombie2HumanBtnPos);
            if (Driver.m_platform==EnumPlatform.pc||Driver.m_platform==EnumPlatform.web)
            {
                string str = "{0}s内，按"+GlobalBattleParams.singleton.GetPcKeyTextByKey(GameEvent.INPUT_KEY_E_DOWN)+"使用解药，变回人类";
                ShowProgress(true, proto.time_limit, string.Format(str, proto.time_limit), CloseZombie2Human);
            }
            else
            ShowProgress(true, proto.time_limit, string.Format(Zombie2HumanFormatStr, proto.time_limit), CloseZombie2Human);
        }
    }

    private void Toc_fight_start_zombie2human(toc_fight_start_zombie2human proto)
    {
        Logger.Log("ZombieUltimateBehaviour:Toc_fight_start_zombie2human-->" + proto.from);
        ShowProgress(true, Zombie2HumanValidateTime, Zombie2HumanValidateStr);
    }

    override protected void Toc_fight_hero_time(toc_fight_hero_time data)
    {
        if (MainPlayer.singleton == null)
            return;

        if (MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.ZOMBIE)
            base.Toc_fight_hero_time(data);
        else if (MainPlayer.singleton != null && MainPlayer.singleton.isAlive)
        {
            if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX)
            {
                if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_CHANGE_MATRIX) != 0)
                    NetLayer.Send(new tos_fight_update_actor_type() { actor_type = GameConst.ACTOR_TYPE_ULTIMATE_MATRIX });
                else
                    ShowBecomeMatrixTip(true);
            }
        }
    }

    private void CloseZombie2Human()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            UIManager.GetPanel<PanelBattle>().ShowZombie2HumanBtn(false);
        }
    }

    private ProgressBarProp prop;
    private void ShowProgress(bool isShow, float duration = 1f, string msg = "", Action onTimeOut = null)
    {
        if (!UIManager.IsOpen<PanelBattle>())
        {
            return;
        }

        if (!isShow) { UIManager.GetPanel<PanelBattle>().StopProgressBar("ZombieUltimate"); return; }
        if (prop == null)
        {
            prop = new ProgressBarProp() { bgPos = ProgressBarPos, progressPos = ProgressBarPos, txtPos = ProgressBarTextPos, OnTimeOut = onTimeOut, textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } } };
        }
        else
            prop.OnTimeOut = onTimeOut;
        UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", msg, duration, prop, "ZombieUltimate");
    }

    protected void ShowBecomeMatrixTip(bool isShow)
    {
        if (isShow)
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.isAlive && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_MATRIX)
            {
                m_bIsShowBecomeMatrixTip = true;
                if (m_panelZombie != null)
                {
                    m_panelZombie.ShowBecomeMatrixTip(true);
                    m_panelZombie.ShowBecomeMatrixBtn(true);
                }
            }
        }
        else
        {
            m_bIsShowBecomeMatrixTip = false;
            if (m_panelZombie != null)
            {
                m_panelZombie.ShowBecomeMatrixTip(false);
                m_panelZombie.ShowBecomeMatrixBtn(false);
            }
        }
    }

    protected override void HideZombiePanelInfo()
    {
        base.HideZombiePanelInfo();
        ShowBecomeMatrixTip(false);
        if (m_panelZombie != null)
            m_panelZombie.ShowMatrixSwitchWeaponBtn(false);
    }

    protected override void RePlayNotice()
    {
        base.RePlayNotice();
        if(!m_bIsPlayGameStartTip)
        {
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.Battle, "zombie_ultimate_game_start_tip" , 5, false, new Vector2(0, 60f));
            AudioManager.PlayFightUISound(AudioConst.finalMoment);
            m_bIsPlayGameStartTip = true;
        }
    }

    public bool isShowBecomeMatrixTip
    {
        get { return m_bIsShowBecomeMatrixTip; }
    }

    public bool isShowMatrixSwitchWeapon
    {
        get { return m_bIsShowMatrixSwitchWeapon; }
    }
}
