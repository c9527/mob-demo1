﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ItemHeroCardReward : ItemRender
{
    private Image m_imgIcon;
    private Text m_txtNum;
    private ItemInfo m_data;
    private ConfigItemLine m_cfg;
    private Image m_qualityBg;
    public override void Awake()
    {
        m_imgIcon = transform.Find("Icon").GetComponent<Image>();
        m_txtNum = transform.Find("Num").GetComponent<Text>();
        m_qualityBg = transform.Find("Bg").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ItemInfo;
        if (m_data == null) return;

        m_cfg = ItemDataManager.GetItem(m_data.ID);
        if (m_cfg == null) return;

        m_imgIcon.SetSprite(ResourceManager.LoadIcon(m_data.ID), m_cfg.SubType);
        m_imgIcon.SetNativeSize();
        m_txtNum.text = m_cfg.DispName + "X" + m_data.cnt;
        var colorValue = 0;
        if (m_cfg != null)
        {
            colorValue = m_cfg.RareType;
        }
        m_qualityBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue))); 
    }
}

public class PanelHeroCardReward : BasePanel
{
    private GameObject m_btnGo;
    private GameObject m_btnDraw;
    private Text m_txtDraw;
    private Text m_txtTip;
    private Text m_txtTipNoHeroCard;
    private DataGrid m_dataGrid;
    private GameObject m_item;
    private List<Text> infoList;
    public PanelHeroCardReward()
    {
        SetPanelPrefabPath("UI/Activity/PanelHeroCardReward");
        AddPreLoadRes("UI/Activity/PanelHeroCardReward", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }
    public override void Init()
    {
        m_btnGo = m_tran.Find("frame/BtnGo").gameObject;
        m_btnDraw = m_tran.Find("frame/BtnDraw").gameObject;
        m_txtDraw = m_tran.Find("frame/BtnDraw/Text").GetComponent<Text>();
        m_txtTip = m_tran.Find("frame/TextTips").GetComponent<Text>();
        m_txtTipNoHeroCard = m_tran.Find("frame/TextTipsNoHeroCard").GetComponent<Text>();
        m_dataGrid = m_tran.Find("frame/ItemList").GetComponent<DataGrid>();
        if (m_dataGrid == null)
            m_dataGrid = m_tran.Find("frame/ItemList").gameObject.AddComponent<DataGrid>();
        m_item = m_tran.Find("frame/ItemList/Item").gameObject;
        m_item.TrySetActive(false);
        m_dataGrid.SetItemRender(m_item, typeof(ItemHeroCardReward));
        infoList = new List<Text>();
        for (int i = 1; i <= 3; i++)
        {
            infoList.Add(m_tran.Find("frame/InfoList/TextInfo" + i + "/TxtTime").GetComponent<Text>());
        }
        m_btnDraw.gameObject.TrySetActive(false);
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnDraw).onPointerClick += onClickBtnDrawHandler;
        UGUIClickHandler.Get(m_btnGo).onPointerClick += onClickBtnGoHandler;
    }

    private void onClickBtnDrawHandler(GameObject target, PointerEventData eventData)
    {
        EnumHeroCardType type = HeroCardDataManager.getHeroMaxCardType();
        if ((int)type == 0)
        {
            return ;
        }
        if (HeroCardDataManager.isCanDrawGif())
        {
            NetLayer.Send(new tos_player_take_hero_card_reward());
            
        }
        else
        {
            TipsManager.Instance.showTips("已领取奖励，不能再领取");
        }
    }

    private void Toc_player_hero_card_data(toc_player_hero_card_data data)
    {
        showView();
    }


    /// <summary>
    /// 领取每日奖励
    /// </summary>
    /// <param name="data"></param>
    private void Toc_player_take_hero_card_reward(toc_player_take_hero_card_reward data)
    {
        HeroCardDataManager.setRewardTime(data.reward_time);
        if (HeroCardDataManager.isCanDrawGif())
        {
            Util.SetGoGrayShader(m_btnDraw, false);
        }         
        else
        {
            Util.SetGoGrayShader(m_btnDraw, true);
            EnumHeroCardType type = HeroCardDataManager.getHeroMaxCardType();
            string rewardStr = ConfigMisc.GetValue("hero_card_rewards");
            string[] rewardArray = rewardStr.Split(';');
            if (rewardArray == null) return;
            ConfigRewardLine temp = ConfigManager.GetConfig<ConfigReward>().GetLine(rewardArray[(int)type - 1]) as ConfigRewardLine;
            if (temp == null) return;                
            var itemList = temp.ItemList;
            PanelRewardItemTipsWithEffect.DoShow(itemList, true);
        }            
        showView();
    }

    private void onClickBtnGoHandler(GameObject target, PointerEventData eventData)
    {
        int index = (int)HeroCardDataManager.getHeroMaxCardType();
        if (index < (int)EnumHeroCardType.legend)
        {
            ConfigMallLine cfg = ConfigManager.GetConfig<ConfigMall>().GetLine(HeroCardDataManager.MallCardArray[index]) as ConfigMallLine;
            if (cfg != null)
                UIManager.PopPanel<PanelBuy>(new object[] { cfg, "", false }, true);
        }
    }

    public override void OnShow()
    {
        showView();
    }
    /// <summary>
    /// 显示界面
    /// </summary>
    private void showView()
    {
        EnumHeroCardType type = HeroCardDataManager.getHeroMaxCardType();
        string rewardStr = ConfigMisc.GetValue("hero_card_rewards");
        string[] rewardArray = rewardStr.Split(';');
        if (rewardArray == null) return;
        if ((int)type == 0)
        {
            //m_dataGrid.Data = new object[] { };
            ConfigRewardLine temp = ConfigManager.GetConfig<ConfigReward>().GetLine(rewardArray[0]) as ConfigRewardLine;
            m_dataGrid.Data = temp.ItemList;
        }
        else
        {
            ConfigRewardLine temp = ConfigManager.GetConfig<ConfigReward>().GetLine(rewardArray[(int)type - 1]) as ConfigRewardLine;
            m_dataGrid.Data = temp.ItemList;
            m_txtTipNoHeroCard.gameObject.TrySetActive(false);
            m_btnDraw.gameObject.TrySetActive(true);
        }
      
    
        if (HeroCardDataManager.isCanDrawGif())
        {
            m_txtDraw.text = "领取";
            Util.SetGoGrayShader(m_btnDraw, false);
        }            
        else
        {
            Util.SetGoGrayShader(m_btnDraw, true);
            m_txtDraw.text = "已领取";
        }


        int cost = HeroCardDataManager.getNextUpCost();
        m_txtTip.gameObject.TrySetActive(cost!=0);
        m_btnGo.TrySetActive(cost!=0);
        if (type != EnumHeroCardType.legend)
        {
            m_txtTip.text = "仅需" + cost + "钻石即可升级" + HeroCardDataManager.HeroNameArray[(int)type];
        }
        showRestTime();
    }

    /// <summary>
    /// 显示剩余时间
    /// </summary>
    private void showRestTime()
    {
        foreach (var temp in infoList)
        {
            temp.transform.parent.gameObject.TrySetActive(false);
        }
        int count = 0;
        //显示奖励剩余时间
        var tempCard = HeroCardDataManager.heroCardData.cards;
        if (HeroCardDataManager.heroCardData != null)
        {            
            if (HeroCardDataManager.heroCardData.cards != null)
            {                
                switch(tempCard.Length)
                {
                    case 1:
                    {
                        if (TimeUtil.GetRemainTimeType((uint)tempCard[0].end_time) != -1)
                        {
                            CountTimeBegin(0, tempCard);
                            CountTimeActive(0, true, tempCard);
                        }
                        else
                        {
                            CountTimeActive(0, false, tempCard);
                        }
                        break;
                    }
                    case 2:
                    {
                        if (TimeUtil.GetRemainTimeType((uint)tempCard[1].end_time) != -1)
                        {
                            if(tempCard[1].end_time >= tempCard[0].end_time)
                            {
                                CountTimeBegin(1, tempCard);
                                CountTimeActive(1, true, tempCard);
                                CountTimeActive(0, false, tempCard);
                            }
                            else
                            {
                                CountTimeBegin(1, tempCard);
                                CountTimeActive(1, true, tempCard);
                                CountTimestop(0, 1, tempCard);
                                CountTimeActive(0, true, tempCard);
                            }
                        }
                        else if (TimeUtil.GetRemainTimeType((uint)tempCard[0].end_time) != -1)
                        {
                            CountTimeActive(1, false, tempCard);
                            CountTimeBegin(0, tempCard);
                            CountTimeActive(0, true, tempCard);
                        }
                        else
                        {
                            CountTimeActive(1, false, tempCard);
                            CountTimeActive(0, false, tempCard);
                        }
                        break;
                    }
                    case 3:
                    {
                        if (TimeUtil.GetRemainTimeType((uint)tempCard[2].end_time) != -1)
                        {
                            if (tempCard[2].end_time >= tempCard[1].end_time)
                            {
                                CountTimeBegin(2, tempCard);
                                CountTimeActive(2, true, tempCard);
                                CountTimeActive(1, false, tempCard);
                                if(tempCard[2].end_time >= tempCard[0].end_time)
                                {
                                    CountTimeActive(0, false, tempCard);
                                }
                                if (tempCard[2].end_time < tempCard[0].end_time)
                                {
                                    CountTimestop(0, 2, tempCard);
                                    CountTimeActive(0, true, tempCard);
                                }
                            }
                            else
                            {
                                CountTimeBegin(2, tempCard);
                                CountTimeActive(2, true, tempCard);
                                CountTimestop(1, 2, tempCard);
                                CountTimeActive(1, true, tempCard);
                                if (tempCard[0].end_time < tempCard[1].end_time)
                                {
                                    CountTimeActive(0, false, tempCard);
                                }
                                else
                                {
                                    CountTimestop(0, 1, tempCard);
                                    CountTimeActive(0, true, tempCard);
                                }
                            }
                        }
                        else if (TimeUtil.GetRemainTimeType((uint)tempCard[1].end_time) != -1)
                        {
                            CountTimeActive(2, false, tempCard);
                            if (tempCard[1].end_time >= tempCard[0].end_time)
                            {
                                CountTimeBegin(1, tempCard);
                                CountTimeActive(1, true, tempCard);
                                CountTimeActive(0, false, tempCard);
                            }
                            else
                            {
                                CountTimeBegin(1, tempCard);
                                CountTimeActive(1, true, tempCard);
                                CountTimestop(0, 1, tempCard);
                                CountTimeActive(0, true, tempCard);
                            }
                        }
                        else if (TimeUtil.GetRemainTimeType((uint)tempCard[0].end_time) != -1)
                        {
                            CountTimeActive(1, false, tempCard);
                            CountTimeBegin(0, tempCard);
                            CountTimeActive(0, true, tempCard);
                        }
                        else
                        {
                            CountTimeActive(2, false, tempCard);
                            CountTimeActive(1, false, tempCard);
                            CountTimeActive(0, false, tempCard);
                        }
                        break;
                    }
                    default:
                        break;  
                }
            }
        }
        m_dataGrid.gameObject.GetRectTransform().anchoredPosition = new Vector2(121, -80 + (3 - tempCard.Length) * 20);
        m_btnDraw.GetRectTransform().anchoredPosition = new Vector2(121, -190 + (3 - tempCard.Length) * 20);
    }

    private void CountTimeBegin(int type, p_hero_card_info[] tempCard)
    {
        infoList[tempCard[type].hero_type - 1].text = TimeUtil.FormatTime((int)TimeUtil.GetRemainTime((uint)tempCard[type].end_time).TotalSeconds, 4, true);
    }

    private void CountTimestop(int higher, int lower, p_hero_card_info[] tempCard)
    {
        var timeStop = tempCard[higher].end_time - tempCard[lower].end_time;
        infoList[tempCard[higher].hero_type - 1].text = TimeUtil.FormatTime((int)timeStop, 4, true);
    }

    private void CountTimeActive(int type, bool state, p_hero_card_info[] tempCard)
    {
        infoList[tempCard[type].hero_type - 1].transform.parent.gameObject.TrySetActive(state);
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }

    private float clickTime = 0;
    public override void Update()
    {
        clickTime += Time.deltaTime;
        if (clickTime > 1)
        {
            showRestTime();
            clickTime = 0;
        }
    }
}

