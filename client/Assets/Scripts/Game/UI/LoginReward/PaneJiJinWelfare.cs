﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


class PanelJiJinWelfare : BasePanel
{
    public PanelJiJinWelfare()
    {
        SetPanelPrefabPath("UI/Activity/PanelJiJinWelfare");
        AddPreLoadRes("UI/Activity/PanelJiJinWelfare", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    DataGrid m_dataGrid;
    int type;

    public class ItemJijinRender : ItemRender
    {
        private WelfareDataManger.InvestmentData m_data;
        private Text m_txtRewardName;
        private GameObject m_goUnComplete;
        private GameObject m_goUnGained;
        private GameObject m_goGained;
        private GameObject m_goItem1;
        private Image m_imgItem1;
        private Text m_txtItem1Count;
        private GameObject m_goItem2;
        private Image m_imgItem2;
        private Text m_txtItem2Count;

        private Image m_qualityBg1;
        private Image m_qualityBg2;

        private ConfigItemLine m_itemCfg1;
        private ConfigItemLine m_itemCfg2;
        private GridLayoutGroup m_rewardGrid;
        public override void Awake()
        {
            var tran = transform;
            m_txtRewardName = tran.Find("Image/rewardname").GetComponent<Text>();
            m_goUnComplete = tran.Find("uncomplete").gameObject;
            m_goUnGained = tran.Find("ungained").gameObject;
            m_goGained = tran.Find("gained").gameObject;
            m_goItem1 = tran.Find("rewardlist/item1").gameObject;
            m_qualityBg1 = m_goItem1.GetComponent<Image>();
            m_imgItem1 = tran.Find("rewardlist/item1/icon").GetComponent<Image>();
            m_txtItem1Count = tran.Find("rewardlist/item1/Text").GetComponent<Text>();
            m_goItem2 = tran.Find("rewardlist/item2").gameObject;
            m_imgItem2 = tran.Find("rewardlist/item2/icon").GetComponent<Image>();
            m_qualityBg2 = m_goItem2.GetComponent<Image>();
            m_txtItem2Count = tran.Find("rewardlist/item2/Text").GetComponent<Text>();
            m_rewardGrid = tran.Find("rewardlist").GetComponent<GridLayoutGroup>();

            UGUIClickHandler.Get(m_goUnGained).onPointerClick += OnClickGain;
            MiniTipsManager.get(m_goItem1).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem2).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg2 == null) return null;
                return new MiniTipsData() { name = m_itemCfg2.DispName, dec = m_itemCfg2.Desc };
            };
        }

        protected override void OnSetData(object data)
        {
            m_data = data as WelfareDataManger.InvestmentData;
            if (m_data == null)
                return;

            var configWelfareLine = m_data.data;
            m_txtRewardName.text = string.Format("等级{0}级", configWelfareLine.TargetLevel);

            var rewards = ConfigManager.GetConfig<ConfigReward>().GetLine(configWelfareLine.Reward).ItemList;
            var colorValue = 0;
            if (rewards.Length > 0)
            {
                m_goItem1.TrySetActive(true);
                var cfg = ItemDataManager.GetItem(rewards[0].ID);
                m_itemCfg1 = cfg;
                m_imgItem1.SetSprite(ResourceManager.LoadIcon(rewards[0].ID), cfg != null ? cfg.SubType : "");
                m_imgItem1.SetNativeSize();
                m_txtItem1Count.text = "x" + rewards[0].cnt;
                colorValue = cfg.RareType;
                m_qualityBg1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            }
            else
                m_goItem1.TrySetActive(false);

            if (rewards.Length > 1)
            {
                m_goItem2.TrySetActive(true);
                var cfg = ItemDataManager.GetItem(rewards[1].ID);
                m_itemCfg2 = cfg;
                m_imgItem2.SetSprite(ResourceManager.LoadIcon(rewards[1].ID), cfg != null ? cfg.SubType : "");
                m_imgItem2.SetNativeSize();
                m_txtItem2Count.text = "x" + rewards[1].cnt;
                colorValue = cfg.RareType;
                m_qualityBg2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            }
            else
                m_goItem2.TrySetActive(false);
            if (rewards.Length == 1)
            {
                m_rewardGrid.cellSize = new Vector2(170, 80);
                m_imgItem1.rectTransform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            }
            else
            {
                m_rewardGrid.cellSize = new Vector2(80, 80);
                m_imgItem1.rectTransform.localScale = Vector3.one;
            }
            if (m_data.state == 2)
            {
                m_goGained.TrySetActive(false);
                m_goUnGained.TrySetActive(true);
                m_goUnComplete.TrySetActive(false);
            }
            else if (m_data.state == 3)
            {
                m_goGained.TrySetActive(true);
                m_goUnGained.TrySetActive(false);
                m_goUnComplete.TrySetActive(false);
            }
            else
            {
                m_goGained.TrySetActive(false);
                m_goUnGained.TrySetActive(false);
                m_goUnComplete.TrySetActive(true);
            }
        }

        private void OnClickGain(GameObject target, PointerEventData eventData)
        {
            //NetLayer.Send(new tos_player_get_welfare_reward() { id =  });
            NetLayer.Send(new tos_player_get_foundation_reward() { id = m_data.data.ID });
        }
    }


    public override void Init()
    {
        m_dataGrid = m_tran.Find("frame/itemlist/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("frame/itemlist/content/item").gameObject, typeof(ItemJijinRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useClickEvent = false;
        m_dataGrid.useLoopItems = true;
    }

    public override void InitEvent()
    {
        GameDispatcher.AddEventListener("InvestmentUpdate", UpdateGrid);
    }

    public override void OnBack()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void OnShow()
    {
        type = (int)m_params[0];
        UpdateGrid();
        
    }

    void UpdateGrid()
    {
        var list = WelfareDataManger.singleton.InvestmentDatas;
        List<WelfareDataManger.InvestmentData> data = new List<WelfareDataManger.InvestmentData>();
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].data.ID / 100 == type)
            {
                data.Add(list[i]);
            }
        }
        m_dataGrid.Data = data.ToArray();
        return;
    }

    public override void Update()
    {
       
    }
}

