﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MasterGiftDataManager
{
    public List<p_master_welfare_toc> m_gifts;
    public List<p_master_base_info_toc> m_applies;
    public bool m_hasGift;
    public bool m_hasApply;
    public static MasterGiftDataManager singleton = null;
    public MasterGiftDataManager()
    {
        if (singleton != null)
        {
            Logger.Error("PlayerFriendData is a singleton");
        }

        singleton = this;
        Init();
    }
    void Init()
    {
        m_hasGift = false;
        m_hasApply = false;
        m_gifts = new List<p_master_welfare_toc>();
        m_applies = new List<p_master_base_info_toc>();
    }

    static void Toc_master_master_welfare(toc_master_master_welfare data)
    {
        singleton.m_gifts = new List<p_master_welfare_toc>();
        var list = data;
        bool find = false;
        singleton.m_hasGift = false;
        bool hasApprenticeGif = false;
        bool hasMasterGif = false;
        if (data.welfares.Length == 0)
        {
            singleton.m_hasGift = false;
        }
        else
        {
            for (int i = 0; i < data.welfares.Length; i++)
            {
                /*if (data.welfares[i].gold > 0 || data.welfares[i].welfare_ids.Length > 0)
                {
                    singleton.m_hasGift = true;
                }*/

                if (data.welfares[i].player_id == PlayerSystem.roleId)
                {
                    //导师给的礼包
                    if (data.welfares[i].welfare_ids.Length > 0)
                    {
                        hasMasterGif = true;
                    }
                }
                else if (data.welfares[i].gold > 0 || data.welfares[i].welfare_ids.Length > 0)
                {
                    //学徒给的礼包
                    hasApprenticeGif = true;
                }
            }
        }
        BubbleManager.SetBubble(BubbleConst.ApprenticeGift, hasApprenticeGif);
        BubbleManager.SetBubble(BubbleConst.MasterGift, hasMasterGif);
        for (int i = 0; i < data.welfares.Length; i++)
        {
            find = false;
            for (int j = 0; j < singleton.m_gifts.Count; j++)
            {
                if (singleton.m_gifts[j].player_id == data.welfares[i].player_id)
                {
                    singleton.m_gifts[j] = data.welfares[i];
                    find = true;
                    break;
                }
            }
            if (!find)
            {
                singleton.m_gifts.Add(data.welfares[i]);
            }
        }
        if (UIManager.IsOpen<PanelSubMasterReward>())
        {
            UIManager.GetPanel<PanelSubMasterReward>().SetGift();
        }
    }

    public p_master_welfare_toc GetGiftInfoById(int id)
    {
        for (int i = 0; i < m_gifts.Count; i++)
        {
            if (m_gifts[i].player_id == id)
            {
                return m_gifts[i];
            }
        }
        return null;
    }
    //踢出好友或导师时使用
    public bool DeleteGiftById(int id)
    {
        for (int i = 0; i < m_gifts.Count; i++)
        {
            if (m_gifts[i].player_id == id)
            {
                m_gifts.RemoveAt(id);
                return true; ;
            }
        }
        return false;
    }

    //领取对应礼包后使用
    public bool DeleteGiftIdFromGift(int playerid, int giftid)
    {

        for (int i = 0; i < m_gifts.Count; i++)
        {
            if (m_gifts[i].player_id == playerid)
            {
                for (int j = 0; j < m_gifts[i].welfare_ids.Length; j++)
                {
                    if (m_gifts[i].welfare_ids[j] == giftid)
                    {
                        m_gifts[i].welfare_ids.ArrayDelete(j);
                        return true;
                    }
                }
            }
        }
        return false;
    }



    static void Toc_master_has_welfare_apply(toc_master_has_welfare_apply data)
    {
        if (data.apply)
        {
            singleton.m_hasApply = data.apply;
            NetLayer.Send(new tos_master_apply_master_list());
            BubbleManager.SetBubble(BubbleConst.StudentApply, data.apply);
        }
        else if (data.has_master || data.has_apprentice)
        {
            singleton.m_hasGift = true;
            NetLayer.Send(new tos_master_apprentice_info());
            BubbleManager.SetBubble(BubbleConst.MasterGift, data.has_master);
            BubbleManager.SetBubble(BubbleConst.ApprenticeGift, data.has_apprentice);
        }
    }

    static void Toc_master_apply_master_list(toc_master_apply_master_list data)
    {
        singleton.m_applies = new List<p_master_base_info_toc>();
        for (int i = 0; i < data.applies.Length; i++)
        {
            singleton.m_applies.Add(data.applies[i]);
        }
    }

    static void Toc_master_grade_has_welfare(toc_master_grade_has_welfare data)
    {
        BubbleManager.SetBubble(BubbleConst.MasterGrade, true);
    }
}

