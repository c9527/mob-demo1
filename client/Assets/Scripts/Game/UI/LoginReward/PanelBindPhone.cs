﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using System.Collections;

public class PanelBindPhone : BasePanel
{
    private InputField m_inputNumber;
    private Button m_btnSure;
    private Image m_imgSure;
    private string m_phoneNumber;
    private Text m_tips;
    private string m_nextRegistTime;

    public PanelBindPhone()
    {
        SetPanelPrefabPath("UI/Activity/PanelBindPhone");
        AddPreLoadRes("UI/Activity/PanelBindPhone", EResType.UI);
    }

    public override void Init()
    {
        m_inputNumber = m_tran.Find("frame/phone/InputFieldPhone").GetComponent<InputField>();
        m_inputNumber.gameObject.AddComponent<InputFieldFix>();
        m_btnSure = m_tran.Find("frame/phone/Button").GetComponent<Button>();
        m_imgSure = m_tran.Find("frame/phone/Button").GetComponent<Image>();
        m_tips = m_tran.Find("frame/phone/tips/Text").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnSure.gameObject).onPointerClick += OnClickSure;
    }

    public void OnClickSure(GameObject target, PointerEventData evenData)
    {
        if (m_btnSure.enabled)
        {
            m_phoneNumber = m_inputNumber.text;
            if (m_phoneNumber.Length == 0)
                TipsManager.Instance.showTips("请输入手机号码");
            else if (m_phoneNumber[0] != '1' || m_phoneNumber.Length != 11)
            {
                TipsManager.Instance.showTips("请输入正确的中国大陆手机号码");
            }
            else
            {
                NetLayer.Send(new tos_player_phone_register() { phone_num = m_phoneNumber });
            }
        }
       
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_player_phone_info());
    }

    public override void Update()
    {
        
    }

    public override void OnHide()
    {
        m_inputNumber.text = "";
    }

    public override void OnDestroy()
    {
        m_inputNumber.text = "";
    }

    public override void OnBack()
    {

    }

    void Toc_player_phone_register(toc_player_phone_register data)
    {
        if (data.state == 0)
            UIManager.PopPanel<PanelBindPhoneVerify>(new object[] { m_phoneNumber }, true);    
        else if (data.state == 1)
        {
            UIManager.ShowTipPanel("验证成功，请到邮箱查收奖励!", null, null, true, false, "确定");
            NetLayer.Send(new tos_player_phone_info());
        }
        else
        {
            TipsManager.Instance.showTips("您的角色已绑定手机号码");
        }
    }

    void Toc_player_phone_info(toc_player_phone_info data)
    {
        var isPass = (TimeUtil.GetRemainTimeType(data.reg_time + 30*24*60*60) == -1);
        if (data.phone_num == null || data.reg_time == 0 || isPass)
        {
            m_tips.text = "绑定成功后，奖励发送到邮箱，每30天可绑定一次并获得奖励！";
            m_imgSure.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "YellowBtn_upSkin"));
            Util.SetGoGrayShader(m_btnSure.gameObject, false);
            m_btnSure.enabled = true;
        }
        else
        {
            DateTime reTime = TimeUtil.GetTime(data.reg_time);
            DateTime nextTime = reTime.AddDays(30);
            m_tips.text = string.Format("您已绑定成功!{0}年{1}月{2}日" + "后可重新绑定！", nextTime.Year.ToString(), nextTime.Month.ToString(), nextTime.Day.ToString());
            m_imgSure.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "WhiteBtn_upSkin"));
            Util.SetGoGrayShader(m_btnSure.gameObject, true);
            m_btnSure.enabled = false;
            m_inputNumber.text = "";
        }
    }
}