﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


class PanelInvestment : BasePanel
{

    public class ItemInvestment : ItemRender
    {
        Text m_name;
        Text m_desc;
        Text m_state;
        GameObject m_stateBtn;
        ConfigFoundationRewardLine m_data;
        int state;
        GameObject m_buybtn;
        GameObject m_get;
        GameObject m_hasbuy;
        Image m_item;

        Image m_sp1;
        Image m_sp2;
        Image m_sp3;
        public override void Awake()
        {
            m_name = transform.Find("title").GetComponent<Text>();
            m_desc = transform.Find("desc").GetComponent<Text>();
            m_state = transform.Find("ActionButton/Text").GetComponent<Text>();
            m_stateBtn = transform.Find("ActionButton").gameObject;
            m_buybtn = transform.Find("BuyBtn").gameObject;
            m_get = transform.Find("GetBtn").gameObject;
            m_item = transform.Find("item").GetComponent<Image>();
            m_sp1 = transform.Find("sp1").GetComponent<Image>();
            m_sp2 = transform.Find("sp2").GetComponent<Image>();
            m_sp3 = transform.Find("sp3").GetComponent<Image>();
            m_hasbuy = transform.Find("buy").gameObject;
            UGUIClickHandler.Get(m_stateBtn).onPointerClick += BuyInvestment;
            UGUIClickHandler.Get(m_buybtn).onPointerClick += BuyInvestment;
            UGUIClickHandler.Get(m_get).onPointerClick += BuyInvestment;
        }

        void BuyInvestment(GameObject target, PointerEventData data)
        {
            if (state == 0)
            {
                UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetMallLine(m_data.ItemId, -1) }, true);
            }
            if (state == 2)
            {
                NetLayer.Send(new tos_player_get_foundation_reward() { id = m_data.ID });
            }
        }

        protected override void OnSetData(object data)
        {
            var info = data as ConfigFoundationRewardLine;
            m_data = info;
            m_name.text = info.DisplayName;
            m_desc.text = info.Desc;
            for (int i = 0; i < WelfareDataManger.singleton.InvestmentDatas.Length; i++)
            {
                if (WelfareDataManger.singleton.InvestmentDatas[i].data.ID == info.ID)
                {
                    state = WelfareDataManger.singleton.InvestmentDatas[i].state;
                }
            }
            m_item.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, info.Icon));
            m_sp1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, info.Tag[0]));
            m_sp2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, info.Tag[1]));
            m_sp3.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, info.Tag[2]));
            m_sp1.SetNativeSize();
            m_sp2.SetNativeSize();
            m_sp3.SetNativeSize();
            CheckState(state);

            //if (PlayerSystem.roleData.level <= info.LevelRange[1] && PlayerSystem.roleData.level >= info.LevelRange[0])
            //{
            //    m_state.text = "购买";
            //    Util.SetNormalShader(m_stateBtn.GetComponent<Button>());
            //}
            //else
            //{
            //    m_state.text = "不可购买";
            //    Util.SetGrayShader(m_stateBtn.GetComponent<Button>());
            //}
        }

        void CheckState(int state)
        {
            if (state == -1)
            {
                m_state.text = "不可购买";
                Util.SetGrayShader(m_stateBtn.GetComponent<Button>());
                m_buybtn.TrySetActive(false);
                m_get.TrySetActive(false);
                m_stateBtn.TrySetActive(true);
                m_hasbuy.TrySetActive(false);
                return;
            }
            if (state == 0)
            {
                m_state.text = "购买";
                Util.SetNormalShader(m_stateBtn.GetComponent<Button>());
                m_buybtn.TrySetActive(true);
                m_get.TrySetActive(false);
                m_stateBtn.TrySetActive(false);
                m_hasbuy.TrySetActive(false);
            }
            if (state == 1)
            {
                m_state.text = "已购买";
                Util.SetGrayShader(m_stateBtn.GetComponent<Button>());
                m_buybtn.TrySetActive(false);
                m_get.TrySetActive(false);
                m_stateBtn.TrySetActive(true);
                m_hasbuy.TrySetActive(true);
            }
            if (state == 2)
            {
                m_state.text = "领取";
                Util.SetNormalShader(m_stateBtn.GetComponent<Button>());
                m_buybtn.TrySetActive(false);
                m_get.TrySetActive(true);
                m_stateBtn.TrySetActive(false);
                m_hasbuy.TrySetActive(true);
            }
            if (state == 3)
            {
                m_state.text = "已领取";
                Util.SetGrayShader(m_stateBtn.GetComponent<Button>());
                m_buybtn.TrySetActive(false);
                m_get.TrySetActive(false);
                m_stateBtn.TrySetActive(true);
                m_hasbuy.TrySetActive(true);
            }
        }
    }

    DataGrid m_datalist;

     public PanelInvestment()
    {
        SetPanelPrefabPath("UI/Activity/PanelTouZiReward");
        AddPreLoadRes("UI/Activity/PanelTouZiReward", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

     public override void Init()
     {
         m_datalist = m_tran.Find("TouziList/content").gameObject.AddComponent<DataGrid>();
         m_datalist.SetItemRender(m_tran.Find("TouziList/content/ItemTouzi").gameObject, typeof(ItemInvestment));
         m_datalist.Data = ConfigManager.GetConfig<ConfigFoundationReward>().m_dataArr;
     } 

     public override void InitEvent()
     {
         GameDispatcher.AddEventListener("InvestmentUpdate", UpdateList);

     }


     void UpdateList()
     {
         m_datalist.Data = ConfigManager.GetConfig<ConfigFoundationReward>().m_dataArr;
         m_datalist.UpdateView();
     }
     public override void OnBack()
     {

     }

     public override void OnDestroy()
     {

     }

     public override void OnHide()
     {

     }

     public override void OnShow()
     {
         m_datalist.Data = ConfigManager.GetConfig<ConfigFoundationReward>().m_dataArr;
         m_datalist.UpdateView();
     }


     public override void Update()
     {

     }
}

