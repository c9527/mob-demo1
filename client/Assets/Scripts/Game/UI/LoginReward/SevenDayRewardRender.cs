﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public enum SevenDayRewardState
{
    UnCompleted, //未达成
    UnGained, //领取
    Gained, //已领取
}

public class SevenDayRewardRender : ItemRender {


    public SevenDayRewardVO cache;    
    private Text m_rewardItem;
    private Image m_rewardIcon;   
    private GameObject m_uncomplete; //未达成
    private GameObject m_ungained;     //领取
    private GameObject m_gained; //已领取    
    private DataGrid m_DataGrid;
    private Text m_rewardItem2;
    private Text m_rewardItem3;
    private Text m_rewardName;
    private Text m_dayText;
    private Image m_rewardBg;
    private ConfigItemLine m_rewardItemCfg;
    public override   void Awake()
    {
        m_rewardName = transform.Find("rewardBg/name").GetComponent<Text>();
        m_rewardItem = transform.Find("rewardItem").GetComponent<Text>();
        m_rewardItem2 = transform.Find("rewardItem2").GetComponent<Text>();
        m_rewardItem3 = transform.Find("rewardItem3").GetComponent<Text>();
        m_dayText = transform.Find("redbar/signdate").GetComponent<Text>();
        m_rewardIcon = transform.Find("rewardIcon").GetComponent<Image>();
        m_rewardBg = transform.Find("rewardBg").GetComponent<Image>();
        m_uncomplete = transform.Find("uncomplete").gameObject;
        m_ungained = transform.Find("ungained").gameObject;
        m_gained = transform.Find("gained").gameObject;     
        Transform item=transform.Find("rewardlist/item1");
        item.gameObject.TrySetActive(false);
        m_DataGrid=transform.Find("rewardlist").gameObject.AddComponent<DataGrid>();
        m_DataGrid.SetItemRender(item.gameObject,typeof(ItemRewardRender));
        UGUIClickHandler.Get(m_ungained).onPointerClick += OnClickUnGained;
        MiniTipsManager.get(m_rewardIcon.gameObject).miniTipsDataFunc = (go) =>
        {
            if (m_rewardItemCfg == null) return null;
            return new MiniTipsData() { name = m_rewardItemCfg.DispName, dec = m_rewardItemCfg.Desc };
        };        
    }

    private void OnClickUnGained(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_sign_in() { type = 3 });
    }
   
    

    protected  override  void OnSetData(object  data)
    {
        cache = data as SevenDayRewardVO;
        if (cache == null)
            return;
        m_dayText.text =  "第"+ cache.day + "天";
        if(cache.day!=1)
        {
            m_rewardIcon.gameObject.TrySetActive(true);
            m_DataGrid.transform.localPosition = new Vector3(45, 0, 0);
            m_DataGrid.GetComponent<GridLayoutGroup>().spacing = new Vector2(14, 0);            
            if (cache.itemInfo != null)
            {
                ConfigItemLine item = ItemDataManager.GetItem(cache.itemInfo[0].ID);
                if (item == null)
                    return;
                m_rewardItemCfg = item;
                m_rewardItem.text = item.DispName;
                m_rewardName.text = item.DispName;
                m_rewardIcon.gameObject.TrySetActive(true);
                m_rewardItem.gameObject.TrySetActive(false);
                m_rewardItem2.gameObject.TrySetActive(false);
                m_rewardItem3.gameObject.TrySetActive(false);
                m_rewardBg.gameObject.TrySetActive(true);
                m_rewardIcon.SetSprite(ResourceManager.LoadIcon(cache.itemInfo[0].ID), item.SubType);
                m_rewardIcon.SetNativeSize();
                List<ItemInfo> itemInfos = new List<ItemInfo>();
                for (int i = 1; i < cache.itemInfo.Length; i++)
                {
                    itemInfos.Add(cache.itemInfo[i]);
                }
                m_DataGrid.Data = itemInfos.ToArray();
            }        
        }
        else
        {
            m_rewardBg.gameObject.TrySetActive(false);
            m_rewardIcon.gameObject.TrySetActive(true);
            m_DataGrid.transform.localPosition = new Vector3(-104, 0, 0);
            m_DataGrid.GetComponent<GridLayoutGroup>().spacing = new Vector2(50, 0);
            if (cache.itemInfo != null)
            {
                ConfigItemLine item = ItemDataManager.GetItem(cache.itemInfo[0].ID);
                if (item == null)
                    return;
                m_rewardItemCfg = item;
                m_rewardItem.text = item.DispName;
                m_rewardIcon.gameObject.TrySetActive(false);
                m_rewardItem.gameObject.TrySetActive(true);
                m_rewardItem2.gameObject.TrySetActive(true);
                m_rewardItem2.text = ItemDataManager.GetItem(cache.itemInfo[1].ID).DispName;
                m_rewardItem3.gameObject.TrySetActive(true);
                m_rewardItem3.text = ItemDataManager.GetItem(cache.itemInfo[2].ID).DispName;
                //m_rewardItem2.text = ItemDataManager.GetItem(cache.itemInfo[1].ID).DispName;
                //m_rewardItem3.text = ItemDataManager.GetItem(cache.itemInfo[2].ID).DispName;
                List<ItemInfo> itemInfos = new List<ItemInfo>();
                m_DataGrid.Data = cache.itemInfo;
            }        
        }
        SetState(cache.state);
    }

    private void SetState(SevenDayRewardState state )
    {
        m_gained.TrySetActive(false);
        m_uncomplete.TrySetActive(false);
        m_ungained.TrySetActive(false);
        if(state==SevenDayRewardState.Gained)
        {
            m_gained.TrySetActive(true);
        }
        else if(state==SevenDayRewardState.UnCompleted)
        {
            m_uncomplete.TrySetActive(true);
        }
        else if(state==SevenDayRewardState.UnGained)
        {
            m_ungained.TrySetActive(true);
        }
    }
}
