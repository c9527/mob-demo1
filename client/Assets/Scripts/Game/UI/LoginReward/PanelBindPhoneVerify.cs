﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class PanelBindPhoneVerify : BasePanel
{
    private string m_phoneNum;
    private InputField m_inputCode;
    //private Image m_reward;
    private Button m_btnGetCode;
    private Button m_btnSend;
    private Button m_btnClose;
    private Text m_verifyTime;
    private Text m_tips;
    private Image m_imgGetCode;

    private float m_maxTime = 0;

    private int m_state;

    public PanelBindPhoneVerify()
    {
        SetPanelPrefabPath("UI/Activity/PanelBindPhoneVerify");
        AddPreLoadRes("UI/Activity/PanelBindPhoneVerify", EResType.UI);
    }

    public override void Init()
    {
        m_inputCode = m_tran.Find("frame/verify/InputFieldCode").GetComponent<InputField>();
        m_inputCode.gameObject.AddComponent<InputFieldFix>();
        //m_reward = m_tran.Find("frame/reward").GetComponent<Image>();
        m_btnGetCode = m_tran.Find("frame/verify/getCode").GetComponent<Button>();
        m_imgGetCode = m_tran.Find("frame/verify/getCode").GetComponent<Image>();
        m_verifyTime = m_tran.Find("frame/verify/getCode/Text").GetComponent<Text>();
        m_tips = m_tran.Find("frame/verify/tips/Text").GetComponent<Text>();
        m_btnSend = m_tran.Find("frame/verify/send").GetComponent<Button>();
        m_btnClose = m_tran.Find("frame/verify/close").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnGetCode.gameObject).onPointerClick += OnClickGetCode;
        UGUIClickHandler.Get(m_btnSend.gameObject).onPointerClick += OnClickSend;
        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += OnClickClose;
    }

    public void OnClickGetCode(GameObject target, PointerEventData evenData)
    {
        if (m_btnGetCode.enabled)
        {
            NetLayer.Send(new tos_player_phone_code() { phone_num = m_phoneNum });    
        }
    }

    public void OnClickClose(GameObject target, PointerEventData evenData)
    {
        UIManager.HidePanel<PanelBindPhoneVerify>();
    }

    public void OnClickSend(GameObject target, PointerEventData eventData)
    {
        if(m_inputCode == null || m_inputCode.text.Length != 6)
        {
            TipsManager.Instance.showTips("请输入6位数的验证码");
        }
        else
        {
            NetLayer.Send(new tos_player_phone_verify() { phone_num = m_phoneNum, code = m_inputCode.text });            
        }            
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            m_phoneNum = m_params[0].ToString();
        }
        m_verifyTime.text = "获取验证码";
        m_tips.text = "这是您第一次绑定手机号" + m_phoneNum.ToString() + "哦";
        m_imgGetCode.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "YellowBtn_upSkin"));
    }

    private float clickTime = 0;
    public override void Update()
    {
        clickTime = clickTime + Time.deltaTime;
        if (clickTime > 1)
        {
            if (m_maxTime > 0)
            {
                --m_maxTime;
                m_verifyTime.text = m_maxTime.ToString() + "秒";
                Util.SetGoGrayShader(m_btnGetCode.gameObject, true);
                m_btnGetCode.enabled = false;
            }
            else
            {
                m_verifyTime.text = "获取验证码";
                m_imgGetCode.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "YellowBtn_upSkin"));
                Util.SetGoGrayShader(m_btnGetCode.gameObject, false);
                m_btnGetCode.enabled = true;
            }
            clickTime = 0;
        }
    }

    public override void OnHide()
    {
        m_phoneNum = null;
        m_inputCode.text = "";
    }

    public override void OnDestroy()
    {

    }

    public override void OnBack()
    {

    }

    void Toc_player_phone_code(toc_player_phone_code data)
    {
        m_maxTime = 60;
        m_imgGetCode.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "WhiteBtn_upSkin"));
    }

    void Toc_player_phone_verify(toc_player_phone_verify data)
    {
        UIManager.ShowTipPanel("验证成功，请到邮箱查收奖励!", null, null, true, false, "确定");
        NetLayer.Send(new tos_player_phone_info());
        UIManager.HidePanel<PanelBindPhoneVerify>();
    }
}