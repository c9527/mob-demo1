﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PanelSubKeyReward : BasePanel
{
    private InputField m_cdkey;
    private Button m_sendkey;
    
    public PanelSubKeyReward()
    {
        SetPanelPrefabPath("UI/Activity/PanelSubKey");
        AddPreLoadRes("UI/Activity/PanelSubKey", EResType.UI);
    }

    public override void Init()
    {
        m_cdkey = m_tran.Find("frame/InputField").GetComponent<InputField>();
        m_sendkey = m_tran.Find("frame/Button").GetComponent<Button>();
        m_cdkey.gameObject.AddComponent<InputFieldFix>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/Button")).onPointerClick += SendCode;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnShow()
    {
    }

    public override void Update()
    {
    }

    void SendCode(GameObject target, PointerEventData eventData)
    {
        string exchange_code = m_cdkey.text;
        Logger.Log(string.Format("exchange_code:{0}", exchange_code));
        NetExchangeCodeData.SndExchangeCode(exchange_code);
        m_cdkey.text = "";
    }

    void Toc_player_use_exchange_code(toc_player_use_exchange_code data)
    {
        if (Driver.m_platform == EnumPlatform.ios)
        {
            SdkManager.ExchangeStatistic(data.exchange_code);
		    Debug.Log ("data.exchange_code:"+data.exchange_code);
        }
        UIManager.ShowTipPanel("礼包已领取，请提取邮件附件查收", null, null, true, false, "确定");
    }
}
