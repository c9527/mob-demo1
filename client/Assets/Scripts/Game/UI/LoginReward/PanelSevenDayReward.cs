﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
/// <summary>
/// 新人登陆7天奖励
/// </summary>
public class PanelSevenDayReward :BasePanel{

    public static ConfigSevenDaySignin config;
    private GameObject m_item0;
    private Scrollbar m_scrollBar;
    public static bool firstOpen = true;
    private GameObject m_btnClose;
    private GameObject m_grid;    
    public DataGrid dateGrid;   
    public static PanelSevenDayReward instance;
    public static bool m_needClose = false;
	public static bool m_isSecondDayGift = false;

    public PanelSevenDayReward()
    {
        SetPanelPrefabPath("UI/Activity/PanelSevenDayReward");
        AddPreLoadRes("UI/Activity/PanelSevenDayReward", EResType.UI);
        AddPreLoadRes("Atlas/SevenDay", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);       
        instance = this;
    }

    public override void Init()
    {
        SevenDayRewardCache cache = SevenDayRewardCache.instance;
        m_item0 = m_tran.Find("content/frame/itemlist/content/item").gameObject;
        m_item0.TrySetActive(false);
        m_scrollBar = m_tran.Find("content/frame/VBScrollBar").GetComponent<Scrollbar>();
        m_grid = m_tran.Find("content/frame/itemlist/content").gameObject;
        m_btnClose = m_tran.Find("close").gameObject;
        dateGrid = m_grid.gameObject.AddComponent<DataGrid>();
        dateGrid.SetItemRender(m_item0, typeof(SevenDayRewardRender));
        dateGrid.autoSelectFirst = false;
        dateGrid.useClickEvent = false;
        dateGrid.useLoopItems = false;
        dateGrid.Data = cache.list.ToArray();
        int index = SevenDayRewardCache.instance.GetUnGainedIndex();
        dateGrid.ResetScrollPosition(index);
        config = ConfigManager.GetConfig<ConfigSevenDaySignin>();
    }


    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += OnBtnCloseClick;
        MiniTipsManager.get(m_tran.Find("content/left/UpIcon").gameObject).miniTipsDataFunc = (go) =>
        {
            var cfg = ItemDataManager.GetItem(1012);
            if (cfg == null) return null;
            return new MiniTipsData() { name = cfg.DispName, dec = cfg.Desc };
        };
        
        MiniTipsManager.get(m_tran.Find("content/left/DownIcon").gameObject).miniTipsDataFunc = (go) =>
        {
            var cfg = ItemDataManager.GetItem(1013);
            if (cfg == null) return null;
            return new MiniTipsData() { name = cfg.DispName, dec = cfg.Desc };
        };
    }

    private void OnBtnCloseClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        OnHide();
    }


    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        instance = null;
    }

    public override void OnHide()
    {
        base.HidePanel();
        if (firstOpen)
        {
            firstOpen = false;
            if (GlobalConfig.clientValue.guideChapterName != "")
            {
                if (Driver.m_platform == EnumPlatform.ios)
                {
                    if (GlobalConfig.serverValue.activity_open_ios)
                    {
                        UIManager.PopPanel<PanelEvent>(null, true, null);
                    }
                }
                else
                {
                    UIManager.PopPanel<PanelEvent>(null, true, null);
                }                    
            }
        }
    }

    public override void Update()
    {

    }

    public override void OnShow()
    {        
        dateGrid.Data = SevenDayRewardCache.instance.list.ToArray();
        int index = SevenDayRewardCache.instance.GetUnGainedIndex();
        dateGrid.ResetScrollPosition(index);
    }
    


    private static  void Toc_player_signin_data(toc_player_signin_data data)
    {        
        PanelSevenDayReward panel = UIManager.GetPanel<PanelSevenDayReward>();        
        SevenDayRewardCache.instance.signTimes = data.day_signin.signined_times;
        SevenDayRewardCache.instance.InitListByData(data);
        int index = SevenDayRewardCache.instance.GetUnGainedIndex();
        if(index==-1) //没有可以领取的物品
        {
            if(firstOpen)
           {
               if (GuideManager.IsGuideFinish() || !GuideManager.IsGuiding())
               {
                   if (Driver.m_platform == EnumPlatform.ios)
                   {
                       if (GlobalConfig.serverValue.activity_open_ios)
                           UIManager.PopPanel<PanelEvent>(null, true, null);
                   }
                   else
                   {
                       UIManager.PopPanel<PanelEvent>(null, true, null);
                   }                       
               }
            }            
        }
        else
        {
            if(firstOpen)
            {
                if (GuideManager.IsGuideFinish() || !GuideManager.IsGuiding())
                    UIManager.PopPanel<PanelSevenDayReward>(null, true);
            }      
                
        }
        if(data.day_signin.signined_times>=ConfigManager.GetConfig<ConfigSevenDaySignin>().m_dataArr.Length||data.day_signin.signined_times<0)
        {
            PanelHallBattle.showSevenDay = false;
            PanelHallBattle hallbattle = UIManager.GetPanel<PanelHallBattle>();
            {
                if (hallbattle != null&&hallbattle.IsInited())
                    hallbattle.imgSevenDay.TrySetActive(false);
            }            
        }             
        if (panel!=null&& panel.dateGrid != null)
        {
            panel.dateGrid.Data = SevenDayRewardCache.instance.list.ToArray();          
            if(index!=-1)
            {
                panel.dateGrid.ResetScrollPosition(index);                
            }
            else
            {                
                panel.dateGrid.ResetScrollPosition(0);
            }            
        }
        BubbleManager.SetBubble(BubbleConst.SevenDayReward, index!=-1);
        //if (firstOpen && !GuideManager.IsGuideFinish())
        //{
        //    firstOpen = false;
        //    panel.OnHide();            
        //    return;
        //}                    
    }



     private void Toc_player_sign_in(toc_player_sign_in data)
     {
         if (data.type != 3)
             return;         
         BubbleManager.SetBubble(BubbleConst.SevenDayReward, false);
         SevenDayRewardCache.instance.InitListByData(new toc_player_signin_data() { day_signin = data.data });
         if(PanelSevenDayReward.instance!=null)
         {
             PanelSevenDayReward.instance.dateGrid.Data = SevenDayRewardCache.instance.list.ToArray();
         }
         //显示上次获得的物品
             var line = ConfigManager.GetConfig<ConfigSevenDaySignin>().GetLine(data.data.signined_times);
             var rewarditem = ConfigManager.GetConfig<ConfigReward>().GetLine(line.Reward).ItemList;

             //var needEquip = false;
             for (int i = 0; i < rewarditem.Length; i++)
             {
                 var item = ItemDataManager.GetItem(rewarditem[i].ID);
                 //if (item.Type == "EQUIP" && item.SubType != "BULLET")
                 //    needEquip = true;
				 if (item.ID == 1016)
				 {
				     m_isSecondDayGift = true;
				 }
             }
             //if (!needEquip)
             //{
                 PanelRewardItemTipsWithEffect.DoShow(rewarditem);
             //}
             if (data.data.signined_times >= ConfigManager.GetConfig<ConfigSevenDaySignin>().m_dataArr.Length)
         {
             PanelHallBattle panel = UIManager.GetPanel<PanelHallBattle>();
             if (panel != null)
                 panel.imgSevenDay.TrySetActive(false);
             PanelHallBattle.showSevenDay = false;
         }
     }
}

public class SevenDayRewardVO
{
    public int day;    
    public SevenDayRewardState state=SevenDayRewardState.UnCompleted;
    public ItemInfo[]   itemInfo;
}

public class SevenDayRewardCache
{
    public int signTimes;
    private static SevenDayRewardCache m_instance;
    public static int voucher;
    public static int voucherGet;
    public static SevenDayRewardCache instance
    {
        get
        {
            if (m_instance == null)
                m_instance = new SevenDayRewardCache();
            return m_instance;
        }
        set
        {
            m_instance = value;
        }
    }
    public List<SevenDayRewardVO> list = new List<SevenDayRewardVO>();
    public int GetUnGainedIndex()
    {
        if (signTimes < 0)
            return -1;
        for(int i=0;i<list.Count;i++)
        {
            if (list[i].state == SevenDayRewardState.UnGained)
                return i;
        }
        return -1;
    }

    public static ConfigSevenDaySigninLine GetConfigByID(int id)
    {
        ConfigSevenDaySignin config = PanelSevenDayReward.config;
        if (config == null)
            config = ConfigManager.GetConfig<ConfigSevenDaySignin>();
        for(int i=0;i<config.m_dataArr.Length;i++)
        {
            if (config.m_dataArr[i].ID == id)
                return config.m_dataArr[i];
        }
        return null;
    }

    internal void InitListByData(toc_player_signin_data data)
    {
        voucher=0;
        voucherGet = 0;
        int signTimes = data.day_signin.signined_times;                
        System.DateTime now = TimeUtil.GetNowTime();
        System.DateTime last = TimeUtil.GetTime(data.day_signin.last_sginin_time);        
        bool isToday = (now.Day == last.Day&&now.Month==last.Month&&last.Year==now.Year);
        SevenDayRewardCache cache = SevenDayRewardCache.instance;
        cache.list.Clear();
        for (int i = 0; i < ConfigManager.GetConfig<ConfigSevenDaySignin>().m_dataArr.Length; i++)
        {
            SevenDayRewardVO vo = new SevenDayRewardVO();
            vo.itemInfo = ConfigManager.GetConfig<ConfigReward>().GetLine(GetConfigByID(i + 1).Reward).ItemList;
            vo.day = i + 1;
            if (i <signTimes)
            {
                vo.state = SevenDayRewardState.Gained;
                
            }
            else if (i == signTimes)
            {
                if (!isToday)
                {
                    vo.state = SevenDayRewardState.UnGained;
                }
                else
                {
                    vo.state = SevenDayRewardState.UnCompleted;
                }
            }
            else vo.state = SevenDayRewardState.UnCompleted;

            int check = signTimes - 1;
            if (isToday)
            {
                check = signTimes - 1;
            }
            else
            {
                check = signTimes;
            }

            if (i == check)
            {
                for (int j = 0; j < vo.itemInfo.Length; j++)
                {
                    if (vo.itemInfo[j].ID == 5052)
                    {
                        voucher += vo.itemInfo[j].cnt;
                        if (isToday)
                        {
                            voucherGet += vo.itemInfo[j].cnt;
                        }
                    }

                }
            }

            cache.list.Add(vo);            
        }
    }

    
}

