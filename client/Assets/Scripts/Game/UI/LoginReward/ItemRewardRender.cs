﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemRewardRender : ItemRender {
    private Image m_icon;
    private Text m_cont;
    private ItemInfo m_info;
    private ConfigItemLine m_itemCfg;
    //private Text m_name;
    public override void Awake()
    {
        m_icon = transform.Find("icon").GetComponent<Image>();
        m_cont = transform.Find("Text").GetComponent<Text>();
        //m_name = transform.Find("name").GetComponent<Text>();
        MiniTipsManager.get(gameObject).miniTipsDataFunc = (go) =>
        {
            if (m_itemCfg == null) return null;
            return new MiniTipsData() { name = m_itemCfg.DispName, dec = m_itemCfg.Desc };
        };
    }

    protected override void OnSetData(object data)
    {
        m_info = data as ItemInfo;
        var cfg = ItemDataManager.GetItem(m_info.ID);
        m_itemCfg = cfg;
        m_icon.SetSprite(ResourceManager.LoadIcon(m_info.ID),cfg!=null?cfg.SubType:"");
        m_icon.SetNativeSize();
        m_cont.text = "x" + m_info.cnt;
        SevenDayRewardRender render = transform.parent.parent.GetComponent<SevenDayRewardRender>();
        if (render != null && render.cache != null && render.cache.day == 1)
        {
            transform.GetComponent<Image>().enabled = false;
        }
        else
        {
            transform.GetComponent<Image>().enabled = true;
        }
        //m_name.text = ItemDataManager.GetItem(m_info.ID).DispName;
    }

}
