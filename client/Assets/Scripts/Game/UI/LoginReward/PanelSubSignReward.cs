﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelSubSignReward : BasePanel
{
    public enum SignState
    {
        CannotSigned,
        CanSigned,
        Signed,
    }

    /// <summary>
    /// 渲染数据结构
    /// </summary>
    public class ItemSignRewardInfo
    {
        public ConfigMonthlySignInLine m_config;
        public SignState m_state;
        public bool m_isFake;
    }

    /// <summary>
    /// 日历渲染项
    /// </summary>
    private class ItemSignRewardDateRender : ItemRender
    {
        private Image m_signed;
        private Text m_txtDay;
        private Color m_oldTextColor;
        private ItemSignRewardInfo m_data;

        public override void Awake()
        {
            var tran = transform;
            m_signed = tran.Find("signed").GetComponent<Image>();
            m_txtDay = tran.Find("Text").GetComponent<Text>();
            m_oldTextColor = m_txtDay.color;
        }

        protected override void OnSetData(object data)
        {
            m_data = data as ItemSignRewardInfo;
            if (m_data == null)
                return;

            var day = m_data.m_config.ID%100;
            m_txtDay.text = day.ToString("00");
            m_signed.gameObject.TrySetActive(m_data.m_state == SignState.Signed);

            if (m_data.m_isFake)
                m_txtDay.color = new Color32(139, 139, 139, 255);
            else
                m_txtDay.color = m_oldTextColor;
        }
    }


    private class ItemLeijiReward : ItemRender
    {
        Image item;
        Text day;
        GameObject arrow;
        ConfigRewardLine cfg;
        GameObject get;
        int state;
        UIEffect m_hasReward;
        GameObject m_effect;
        int days;
        public override void Awake()
        {
            item = transform.Find("item").GetComponent<Image>();
            day = transform.Find("Text").GetComponent<Text>();
            arrow = transform.Find("Image").gameObject;
            get = transform.Find("get").gameObject;
            m_effect = transform.Find("effect").gameObject;
            
                string desc = "";

                MiniTipsManager.get(item.gameObject).miniTipsDataFunc = (go) =>
                {
                    if (cfg == null)
                    {
                        return null;
                    }
                    desc = "";
                    desc += ItemDataManager.GetItem(cfg.ItemList[0].ID).DispName + "X" + cfg.ItemList[0].cnt;
                    for (int i =1; i < cfg.ItemList.Length; i++)
                    {
                        desc += "\n";
                        desc += ItemDataManager.GetItem(cfg.ItemList[i].ID).DispName + "X" + cfg.ItemList[i].cnt;
                    }
                    return new MiniTipsData() { name = "累计奖励", dec = desc };
                };

                UGUIClickHandler.Get(this.gameObject).onPointerClick += getReward;
            
        }

        void getReward(GameObject target, PointerEventData eventData)
        {
            if (state == 1)
            {
                NetLayer.Send(new tos_player_sign_in() { type = 4,flag = days });
            }

        }

        protected override void OnSetData(object data)
        {
            var info = data as LeijiData;
            days = info.data.ID % 100;
            day.text = (info.data.ID % 100).ToString() + "天";
            cfg = ConfigManager.GetConfig<ConfigReward>().GetLine(info.data.Reward);
            state = info.state;
            get.TrySetActive(false);
            if (state == 2)
            {
                get.TrySetActive(true);
            }
            if (state == 1)
            {
                m_effect.TrySetActive(true);
                if (m_hasReward == null)
                {
                    m_hasReward = UIEffect.ShowEffectBefore(m_effect.transform, EffectConst.UI_QIANDAO_LEIJI, new Vector3(-5, 5, 0));
                }
            }
            else
            {
                m_effect.TrySetActive(false);
                if (m_hasReward != null)
                {
                    m_hasReward.Destroy();
                }
            }

        }
    }
    /// <summary>
    /// 物品渲染项
    /// </summary>
    private class ItemSignRewardRender : ItemRender
    {
        private ItemSignRewardInfo m_data;
        private int m_signId;
        private int m_day;

        private Image m_imgWeaponIcon;
        private Text m_txtInfo;
        private GameObject m_unsigned;
        private Image m_vipMore;
        private GameObject m_goVipMore;
        private GameObject m_goSigned;
        private GameObject m_goItem1;
        private Image m_quality1;
        private Image m_imgItem1;
        private Text m_txtItem1Count;
        private GameObject m_goItem2;
        private Image m_quality2;
        private Image m_imgItem2;
        private Text m_txtItem2Count;

        private Text m_signnum;
        private GameObject m_get;
        private Text m_getText;
        private Image m_btnImage;
        private bool m_had;
        private ConfigItemLine m_itemCfg1;
        private ConfigItemLine m_itemCfg2;

        
        private void SetSigned(bool value)
        {
            m_unsigned.TrySetActive(!value);
            m_goSigned.TrySetActive(value);
            m_get.TrySetActive(!value);
        }

        public override void Awake()
        {
            m_had = true;
            var tran = transform;
            m_imgWeaponIcon = tran.Find("icon").GetComponent<Image>();
            m_goItem1 = tran.Find("rewardlist/item1").gameObject;
            m_imgItem1 = tran.Find("rewardlist/item1/icon").GetComponent<Image>();
            m_quality1 = m_goItem1.GetComponent<Image>();
            m_txtItem1Count = tran.Find("rewardlist/item1/Text").GetComponent<Text>();
            m_goItem2 = tran.Find("rewardlist/item2").gameObject;
            m_quality2 = m_goItem2.GetComponent<Image>();
            m_imgItem2 = tran.Find("rewardlist/item2/icon").GetComponent<Image>();
            m_txtItem2Count = tran.Find("rewardlist/item2/Text").GetComponent<Text>();
            m_txtInfo = tran.Find("Text").GetComponent<Text>();
            m_unsigned = tran.Find("unsign").gameObject;
            m_goVipMore = tran.Find("vip").gameObject;
            m_vipMore = m_goVipMore.GetComponent<Image>();
            m_goSigned = tran.Find("signed").gameObject;
            m_signnum = tran.Find("redbar/signdate").GetComponent<Text>();
            m_get = tran.Find("Button").gameObject;
            m_getText = tran.Find("Button/Text").GetComponent<Text>();
            m_btnImage = m_get.GetComponent<Image>();
            UGUIClickHandler.Get(m_get).onPointerClick += OnSign;
            m_goSigned.TrySetActive(false);

            MiniTipsManager.get(m_imgWeaponIcon.gameObject).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem1).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem2).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg2 == null) return null;
                return new MiniTipsData() { name = m_itemCfg2.DispName, dec = m_itemCfg2.Desc };
            };
        }

        protected override void OnSetData(object data)
        {
            m_data = data as ItemSignRewardInfo;
            if (m_data == null)
                return;

            if (m_data.m_state == SignState.Signed)
                SetSigned(true);
            else if (m_data.m_state == SignState.CanSigned)
            {
                m_btnImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "YellowBtn_upSkin"));
                m_getText.text = "领取";
                SetSigned(false);
            }
            else
            {
                m_btnImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, "notComplete"));
                m_getText.text = "";
                SetSigned(false);
            }

            m_signId = m_data.m_config.ID;
            m_day = m_signId%100;

            var line = m_data.m_config;
            var rewards = ConfigManager.GetConfig<ConfigReward>().GetLine(line.Reward).ItemList;

            var item = ItemDataManager.GetItem(rewards[0].ID);
            if (item == null)
                return;
            m_itemCfg1 = item;
            m_txtInfo.text = item.DispName;
            if (item is ConfigItemWeaponLine)
            {
                m_txtInfo.rectTransform.anchoredPosition = new Vector2(-128, -4);
                m_txtInfo.rectTransform.sizeDelta = new Vector2(99, 62);
                m_imgWeaponIcon.gameObject.TrySetActive(true);
                m_goItem1.TrySetActive(false);
                m_goItem2.TrySetActive(false);

                m_imgWeaponIcon.SetSprite(ResourceManager.LoadIcon(rewards[0].ID), item.SubType);
                m_imgWeaponIcon.SetNativeSize();
                m_had = ItemDataManager.GetDepotItem(item.ID)!=null;

            }
            else
            {
                m_txtInfo.rectTransform.anchoredPosition = new Vector2(-128, -4);
                m_txtInfo.rectTransform.sizeDelta = new Vector2(86, 62);
                m_imgWeaponIcon.gameObject.TrySetActive(false);
                var colorValue = 0;
                if (rewards.Length > 0)
                {
                    colorValue = m_itemCfg1.RareType;
                    m_goItem1.TrySetActive(true);
                    m_imgItem1.SetSprite(ResourceManager.LoadIcon(rewards[0].ID),item.SubType);
                    m_imgItem1.SetNativeSize();
                    m_txtItem1Count.text = "x" + rewards[0].cnt;
                    m_quality1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
                }
                else
                    m_goItem1.TrySetActive(false);

                if (rewards.Length > 1)
                {
                    item = ItemDataManager.GetItem(rewards[1].ID);
                    m_itemCfg2 = item;
                    m_goItem2.TrySetActive(true);
                    m_imgItem2.SetSprite(ResourceManager.LoadIcon(rewards[1].ID), item!=null?item.SubType:"");
                    m_imgItem2.SetNativeSize();
                    m_txtItem2Count.text = "x" + rewards[1].cnt;
                    colorValue = m_itemCfg2.RareType;
                    m_quality2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
                }
                else
                    m_goItem2.TrySetActive(false);
            }

            
            m_unsigned.TrySetActive(true);

            Sprite vipMoreIcon = ResourceManager.LoadIcon(line.VipDoubleIcon);
            if (vipMoreIcon != null)
            {
                m_vipMore.SetSprite(vipMoreIcon);
                m_vipMore.SetNativeSize();
                m_goVipMore.TrySetActive(true);
            }
            m_signnum.text = "签到" + m_day + "天";
        }

        private void OnSign(GameObject target, PointerEventData eventData)
        {
            if (m_data != null && m_data.m_state == PanelSubSignReward.SignState.CanSigned)
            {
                NetLayer.Send(new tos_player_sign_in() { type = 1 });
            }
        }
    }

    private static PanelSubSignReward m_instance;
    private DataGrid m_dataGridItem;
    private DataGrid m_dataGridDay;
    private DataGrid m_dataGridLeiji;
    private Image m_leijiBg;
    private Text m_leijiTip;
    private Image m_leijiDay1;
    private Image m_leijiDay2;
    private List<LeijiData> leijidatas;
    public int signday;
    public int leijiday;
    public bool leijicanGet;
    public int[] days;
    public class LeijiData
    {
        public ConfigCumulateSignInNewLine data;
        public int state; //0 不可领取 1可领取 2已领取
    }


    public PanelSubSignReward()
    {
        m_instance = this;

        SetPanelPrefabPath("UI/Activity/PanelSubSignReward");
        AddPreLoadRes("UI/Activity/PanelSubSignReward", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    public override void Init()
    {

        m_dataGridItem = m_tran.Find("frame/itemlist/content").gameObject.AddComponent<DataGrid>();
        m_dataGridItem.SetItemRender(m_tran.Find("frame/itemlist/content/item").gameObject, typeof (ItemSignRewardRender));
        m_dataGridItem.autoSelectFirst = false;
        m_dataGridItem.useClickEvent = false;
        m_dataGridItem.useLoopItems = true;

        m_dataGridDay = m_tran.Find("frame/datelist/content").gameObject.AddComponent<DataGrid>();
        m_dataGridDay.SetItemRender(m_tran.Find("frame/datelist/content/dateitem").gameObject, typeof (ItemSignRewardDateRender));
        m_dataGridDay.autoSelectFirst = false;
        m_dataGridDay.useClickEvent = false;

        m_dataGridLeiji = m_tran.Find("frame/LeijiGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGridLeiji.SetItemRender(m_tran.Find("frame/LeijiGrid/content/ItemLeiji").gameObject, typeof(ItemLeijiReward));
        m_dataGridLeiji.autoSelectFirst = false;
        m_dataGridLeiji.useClickEvent = false;

        m_leijiBg = m_tran.Find("frame/leijibg").GetComponent<Image>();
        m_leijiTip = m_tran.Find("frame/leijibg/leijiTip").GetComponent<Text>();

        m_leijiDay1 = m_tran.Find("frame/numGrid/content/num1/num1").GetComponent<Image>();
        m_leijiDay2 = m_tran.Find("frame/numGrid/content/num2/num2").GetComponent<Image>();
        InitLeiji();
        leijicanGet = false;
        
    }

    public override void InitEvent()
    {
        m_dataGridLeiji.onItemSelected += GetLeijiReward;
    }

    void GetLeijiReward(object renderData)
    {
        var info = renderData as LeijiData;
        if (info == null)
        {
            return;
        }
        if (info.state == 1)
        {
            NetLayer.Send(new tos_player_sign_in() { type = 4 });
        }
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_player_signin_data());
        
    }

    public void InitLeiji()
    {
        int month = TimeUtil.GetNowTime().Month;
        
        //m_leijiBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity,);
        var cfgs = ConfigManager.GetConfig<ConfigCumulateSignInNew>().m_dataArr;
        List<ConfigCumulateSignInNewLine> leijis = new List<ConfigCumulateSignInNewLine>();
        leijidatas = new List<LeijiData>();
        for (int i = 0; i < cfgs.Length; i++)
        {
            if (cfgs[i].ID / 100 == month)
            {
                var a = new LeijiData();
                a.data = cfgs[i];
                a.state = 0;
                leijidatas.Add(a);
            }
        }
        m_leijiBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, leijidatas[0].data.Icon));
        m_leijiTip.text = leijidatas[0].data.Tip;
        m_dataGridLeiji.Data = leijidatas.ToArray();
    }

    public void setLeijiDay(int day)
    {
        if (day < 10)
        {
            m_leijiDay1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, day.ToString()));
            m_leijiDay2.gameObject.TrySetActive(false);
        }
        else
        {
            m_leijiDay1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (day / 10).ToString()));
            m_leijiDay2.gameObject.TrySetActive(true);
            m_leijiDay2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (day % 10).ToString()));
        }
        m_leijiDay1.SetNativeSize();
        m_leijiDay2.SetNativeSize();
    }


    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void UpdateSign(p_signin_data_toc data, bool showGained = false)
    {
        var signData = data;
        var dataInfos = new List<ItemSignRewardInfo>();
        var nowDate = TimeUtil.GetNowTime();
        setLeijiDay(signData.signined_times);
        signday = signData.signined_times;
        //获取当月的签到配置数据
        var configMonthSign = ConfigManager.GetConfig<ConfigMonthlySignIn>();
        for (int index = 0; index < configMonthSign.m_dataArr.Length; ++index)
        {
            var configLine = configMonthSign.m_dataArr[index];
            if ((configLine.ID/100) == nowDate.Month)
            {
                var info = new ItemSignRewardInfo();
                info.m_config = configLine;
                dataInfos.Add(info);
            }
        }

        //处理之前已签的
        for (int i = 0; i < dataInfos.Count && i < signData.signined_times; i++)
        {
            dataInfos[i].m_state = SignState.Signed;
        }

        //处理今天的
        var lastSignDate = TimeUtil.GetTime(signData.last_sginin_time);
        var isTodaySigned = nowDate.Date == lastSignDate.Date;
        if (!isTodaySigned && signData.signined_times < dataInfos.Count)
        {
            dataInfos[signData.signined_times].m_state = SignState.CanSigned;
            

        }
        BubbleManager.SetBubble(BubbleConst.WelfareSignReward, !isTodaySigned);
        var rewardcfg = ConfigManager.GetConfig<ConfigReward>().GetLine(dataInfos[signData.signined_times].m_config.Reward);


        m_dataGridItem.Data = dataInfos.ToArray();

        int fakeItemId = 1;
        while (dataInfos.Count < 33)
        {
            dataInfos.Add(new ItemSignRewardInfo() { m_isFake = true, m_state = SignState.CannotSigned, m_config = new ConfigMonthlySignInLine() { ID = fakeItemId } });
            fakeItemId++;
        }

        m_dataGridDay.Data = dataInfos.ToArray();

        m_dataGridItem.ShowItemOnTop(signData.signined_times);

        


        //显示上次获得的物品
        if (showGained)
        {
            var line = ConfigManager.GetConfig<ConfigMonthlySignIn>().GetLine(nowDate.Month * 100 + data.signined_times);
            var rewarditem = ConfigManager.GetConfig<ConfigReward>().GetLine(line.Reward).ItemList;

            var needEquip = false;
            for (int i = 0; i < rewarditem.Length; i++)
            {
                var item = ItemDataManager.GetItem(rewarditem[i].ID);
                if (item.Type == "EQUIP" && item.SubType != "BULLET")
                    needEquip = true;
            }

            if (!needEquip)
            {
                //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(null, true, m_instance);
                //panelItemTip.SetRewardItemList(rewarditem);
                PanelRewardItemTipsWithEffect.DoShow(rewarditem);
            }
        }
        UpdateLeiji(data.signined_times, m_instance.leijiday,m_instance.days);

    }

    void UpdateLeiji(int day,int leijiTimes,int[] days)
    {
        leijicanGet = false;
        BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, false);
        List<int> daylist = new List<int>();
        if (days != null)
        {
            daylist = new List<int>(days);
        }
        for (int i = 0; i < leijidatas.Count; i++)
        {
            //if (i < leijiTimes)
            //{
            //    leijidatas[i].state = 2;
            //    continue;
            //}
            //if (leijidatas[i].data.ID % 100 <= day)
            //{
            //    leijidatas[i].state = 1;
            //    BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, true);
            //    leijicanGet = true;
            //}
            if (leijidatas[i].data.ID % 100 <= day)
            {
                leijidatas[i].state = 1;

            }
            if (days == null)
            {
                continue;
            }
            if (daylist.Contains(leijidatas[i].data.ID % 100))
            {
                leijidatas[i].state = 2;
                //BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, true);
            }
        }

        m_dataGridLeiji.Data = leijidatas.ToArray();
        for (int i = 0; i < leijidatas.Count; i++)
        {
            if (leijidatas[i].state == 1)
            {
                BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, true);
            }
        }
        
    }

    private static bool CheckLeiji(p_signin_data_toc data,int[] days,int day)
    {
        int month = TimeUtil.GetNowTime().Month;
        var cfgs = ConfigManager.GetConfig<ConfigCumulateSignInNew>().m_dataArr;
        List<ConfigCumulateSignInNewLine> leijis = new List<ConfigCumulateSignInNewLine>();
        var leijidatas = new List<LeijiData>();
        for (int i = 0; i < cfgs.Length; i++)
        {
            if (cfgs[i].ID / 100 == month)
            {
                var a = new LeijiData();
                a.data = cfgs[i];
                a.state = 0;
                leijidatas.Add(a);
            }
        }
        var leiji = false;
        BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, false);
        List<int> daylist = new List<int>();
        if (days != null)
        {
            daylist = new List<int>(days);
        }
        
        for (int i = 0; i < leijidatas.Count; i++)
        {
            //if (i < data.signined_times)
            //{
            //    leijidatas[i].state = 2;
            //    continue;
            //}
            if (leijidatas[i].data.ID % 100 <= day)
            {
                leijidatas[i].state = 1;

            }
            if (days == null)
            {
                continue;
            }
            if (daylist.Contains(leijidatas[i].data.ID % 100))
            {
                leijidatas[i].state = 2;
                //BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, true);
            }
        }
        for (int i = 0; i < leijidatas.Count; i++)
        {
            if (leijidatas[i].state == 1)
            {
                BubbleManager.SetBubble(BubbleConst.WelfareLeijiReward, true);
            }
        }

        return leiji;

    }


    private static void UpdateBubble(p_signin_data_toc data)
    {
        var signData = data;
        var nowDate = TimeUtil.GetNowTime();        
        var lastSignDate = TimeUtil.GetTime(signData.last_sginin_time);
        var isTodaySigned = nowDate.Date == lastSignDate.Date;
        BubbleManager.SetBubble(BubbleConst.WelfareSignReward, !isTodaySigned);
        WelfareDataManger.signVoucherGet = 0;
        WelfareDataManger.signVoucher = 0;
        var configMonthSign = ConfigManager.GetConfig<ConfigMonthlySignIn>();
        ConfigMonthlySignInLine todaydata = null;
        for (int index = 0; index < configMonthSign.m_dataArr.Length; ++index)
        {
            var configLine = configMonthSign.m_dataArr[index];
            if ((configLine.ID / 100) == nowDate.Month)
            {
                if (isTodaySigned) 
                {
                    if (configLine.ID % 100 == data.signined_times)
                    {
                        todaydata = configLine;
                    }
                }
                else
                {
                    if (configLine.ID % 100 == data.signined_times+1)
                    {
                        todaydata = configLine;
                    }
                }
                
            }
        }
        if (todaydata != null)
        {
            var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(todaydata.Reward);
            if (reward != null)
            {
                for (int i = 0; i < reward.ItemList.Length; i++)
                {
                    if (reward.ItemList[i].ID == 5052)
                    {
                        WelfareDataManger.signVoucher += reward.ItemList[i].cnt;
                        if (isTodaySigned)
                        {
                            WelfareDataManger.signVoucherGet += reward.ItemList[i].cnt;
                        }
                    }
                    
                }
            }
        }


    }

    private static void Toc_player_signin_data(toc_player_signin_data data)
    {
        if (m_instance != null && m_instance.IsOpen())
        {
            m_instance.UpdateSign(data.monthly_signin);
            m_instance.signday = data.monthly_signin.signined_times;
            m_instance.leijiday = data.cumulate_signin_new.signined_times;
            m_instance.days = data.cumulate_signin_new.rewarded;
            m_instance.UpdateLeiji(data.monthly_signin.signined_times, data.cumulate_signin_new.signined_times,m_instance.days);
        }
        UpdateBubble(data.monthly_signin);
        CheckLeiji(data.cumulate_signin_new, data.cumulate_signin_new.rewarded, data.monthly_signin.signined_times);
    }

    private static void Toc_player_sign_in(toc_player_sign_in data)
    {
        if (m_instance != null && m_instance.IsOpen())
        {
            if (data.type == 4)
            {
                m_instance.leijiday = data.data.signined_times;
                m_instance.days = data.data.rewarded;
                m_instance.UpdateLeiji(m_instance.signday, data.data.signined_times,m_instance.days);
                m_instance.ShowLeijiItem(data.data,data.flag);
            }
        }
        if (data.type != 1)
            return;
        if (m_instance != null && m_instance.IsOpen())
            m_instance.UpdateSign(data.data, true);

        UpdateBubble(data.data);
    }

    public void ShowLeijiItem(p_signin_data_toc data,int flag)
    {
        int id = flag + TimeUtil.GetNowTime().Month * 100;
        var cfg = ConfigManager.GetConfig<ConfigCumulateSignInNew>().GetLine(id);
        var rewarditem = ConfigManager.GetConfig<ConfigReward>().GetLine(cfg.Reward).ItemList;
        var needEquip = false;
        for (int i = 0; i < rewarditem.Length; i++)
        {
            var item = ItemDataManager.GetItem(rewarditem[i].ID);
            if (item.Type == "EQUIP" && item.SubType != "BULLET")
                needEquip = true;
        }

        if (!needEquip)
        {
            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(null, true, m_instance);
            //panelItemTip.SetRewardItemList(rewarditem);
            PanelRewardItemTipsWithEffect.DoShow(rewarditem);
        }
    }
    
}
