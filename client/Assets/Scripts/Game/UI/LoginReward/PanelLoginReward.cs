﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PanelLoginReward : BasePanel
{
    public PanelLoginReward()
    {
        SetPanelPrefabPath("UI/Activity/PanelLoginReward");
        AddPreLoadRes("UI/Activity/PanelLoginReward", EResType.UI);

        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    
    BasePanel m_curPopPanel;

    Toggle m_toggleSign;
    GameObject m_lvlbubble;
    GameObject m_signbubble;
    GameObject m_onlinebubble;
    GameObject m_masterbubble;
    GameObject m_sharebubble;
    GameObject m_herobubble;
    GameObject m_investbubble;


    GameObject tabVKshare;
    GameObject tabShare;
    GameObject tabFBShare;

    private Transform m_keyreward;
    private Transform m_sharereward;
    private Transform m_bindPhone;
    private Toggle m_toggleLevelreward;
    private Toggle m_online;
    GameObject[] m_jijin = new GameObject[3];
    GameObject[] m_jbubble = new GameObject[3];
    public override void Init()
    {
        m_lvlbubble = m_tran.Find("frame/group/content/levelreward/bubble").gameObject;
        m_signbubble = m_tran.Find("frame/group/content/signreward/bubble").gameObject;
        m_onlinebubble = m_tran.Find("frame/group/content/onlinereward/bubble").gameObject;
        m_masterbubble = m_tran.Find("frame/group/content/masterreward/bubble").gameObject;
        m_sharebubble = m_tran.Find("frame/group/content/sharereward/bubble").gameObject;
        //m_investbubble = m_tran.Find("frame/group/content/touzireward/bubble").gameObject;
        m_toggleSign = m_tran.Find("frame/group/content/signreward").GetComponent<Toggle>();
        m_herobubble = m_tran.Find("frame/group/content/heroreward/bubble").gameObject;
        m_keyreward = m_tran.Find("frame/group/content/keyreward");
        m_sharereward = m_tran.Find("frame/group/content/sharereward");
        m_bindPhone = m_tran.Find("frame/group/content/phonereward");
        m_toggleLevelreward = m_tran.Find("frame/group/content/levelreward").GetComponent<Toggle>();
        m_online = m_tran.Find("frame/group/content/onlinereward").GetComponent<Toggle>();

        var list = ConfigManager.GetConfig<ConfigFoundationReward>().GetBaseReward();
        int index = list.Length;
        m_jijin = new GameObject[index];
        m_jbubble = new GameObject[index];
        for (int i = 0; i < index; i++)
        {
            m_jijin[i] = m_tran.Find("frame/group/content/touzireward" + i.ToString()).gameObject;
            m_jbubble[i] = m_tran.Find("frame/group/content/touzireward" + i.ToString()+"/bubble").gameObject;
        }

        tabVKshare = m_tran.Find("frame/group/content/vksharereward").gameObject;
        tabShare = m_tran.Find("frame/group/content/sharereward").gameObject;
        tabFBShare = m_tran.Find("frame/group/content/fbsharereward").gameObject;

    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/signreward")).onPointerClick += OnChangeSignRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/levelreward")).onPointerClick += OnChangeLevelRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/onlinereward")).onPointerClick += OnChangeOnlineRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/keyreward")).onPointerClick += OnChangeKeyRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/masterreward")).onPointerClick += OnChangeMasterRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/sharereward")).onPointerClick += OnChangeShareRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/phonereward")).onPointerClick += OnChangePhoneRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/fbsharereward")).onPointerClick += OnChangeFacebookRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/heroreward")).onPointerClick += OnChangeHeroRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/vksharereward")).onPointerClick += OnChangeVKShareRewardView;
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/touzireward0")).onPointerClick += delegate { showJijin(1); };
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/touzireward1")).onPointerClick += delegate { showJijin(2); };
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/touzireward2")).onPointerClick += delegate { showJijin(3); };
        UGUIClickHandler.Get(m_tran.Find("frame/group/content/touzireward3")).onPointerClick += delegate { showJijin(4); };
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
    }

    void showJijin(int i)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelJiJinWelfare>(new object[] { i });
        m_curPopPanel.ShowPanel();
    }

    private void UpdateBubble()
    {
        m_lvlbubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.WelfareLevelReward));
        m_onlinebubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.WelfareOnlineReward));
        m_signbubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.WelfareSignReward) || BubbleManager.HasBubble(BubbleConst.WelfareLeijiReward));
       // m_masterbubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.MasterGift) || BubbleManager.HasBubble(BubbleConst.StudentApply));
        m_sharebubble.TrySetActive(false);
        m_herobubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.HeroCardReward));
        for (int i = 0; i < m_jbubble.Length; i++)
        {
            m_jbubble[i].TrySetActive(BubbleManager.HasBubble("Investment" + (i + 1).ToString()));
        }
        //m_investbubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.Investment));
    }

    private void checkShareTab()
    {
        tabFBShare.TrySetActive(false);
    }



    public override void OnShow()
    {
        UpdateBubble();
        checkShareTab();
        m_keyreward.gameObject.TrySetActive(GlobalConfig.serverValue.giftCode_open);
        m_bindPhone.gameObject.TrySetActive(GlobalConfig.serverValue.bindPhone_open);
        if (Driver.m_platform == EnumPlatform.pc)
            m_sharereward.gameObject.TrySetActive(GlobalConfig.serverValue.share_open_pc);
        else if (Driver.m_platform == EnumPlatform.web)
            m_sharereward.gameObject.TrySetActive(GlobalConfig.serverValue.share_open_web);
        else if (Driver.m_platform == EnumPlatform.ios)
            m_sharereward.gameObject.TrySetActive(GlobalConfig.serverValue.share_open_ios);
        else
            m_sharereward.gameObject.TrySetActive(GlobalConfig.serverValue.share_open);


        var list = ConfigManager.GetConfig<ConfigFoundationReward>().GetBaseReward();
        for (int i = 0; i < list.Length; i++)
        {
            if (ItemDataManager.IsDepotHas(list[i].ItemId))
            {
                m_jijin[i].TrySetActive(true);
            }
            else
            {
                m_jijin[i].TrySetActive(false);
            }
        }


        if (HasParams())
        {
            string tab = m_params[0] as string;
            if (tab == "shareshop")
            {
                if (m_curPopPanel != null)
                {
                    m_curPopPanel.HidePanel();
                }
                m_curPopPanel = UIManager.PopPanel<PanelShare>(new object[]{"shop"});
                m_curPopPanel.ShowPanel();
            }
            if (tab == "online")
            {
                if (m_curPopPanel != null)
                {
                    m_curPopPanel.HidePanel();
                }
                OnChangeOnlineRewardView(null, null);
                m_curPopPanel.ShowPanel();
            }
            if (tab == "sign")
            {
                if (m_curPopPanel != null)
                {
                    m_curPopPanel.HidePanel();
                }
                OnChangeSignRewardView(null, null);
                m_curPopPanel.ShowPanel();
            }
            
        }
        else
        {
            ShowSignReward();
            SwitchSubPanel();
        }



        
    }


    public override void OnHide()
    {
        if (UIManager.GetPanel<PanelSubSignReward>() != null)
        {
            UIManager.GetPanel<PanelSubSignReward>().HidePanel();
        }
        if (UIManager.GetPanel<PanelSubKeyReward>() != null)
        {
            UIManager.GetPanel<PanelSubKeyReward>().HidePanel();
        }
        if (UIManager.GetPanel<PanelSubWelfare>() != null)
        {
            UIManager.GetPanel<PanelSubWelfare>().HidePanel();
        }
        if (UIManager.GetPanel<PanelSubMasterReward>()!=null)
        {
            UIManager.GetPanel<PanelSubMasterReward>().HidePanel();
        }
        if (UIManager.GetPanel<PanelShare>() != null)
        {
            UIManager.GetPanel<PanelShare>().HidePanel();
        }
        if (UIManager.GetPanel<PanelHeroCardReward>() != null)
        {
            UIManager.GetPanel<PanelHeroCardReward>().HidePanel();
        }
        if (UIManager.GetPanel<PanelInvestment>() != null)
        {
            UIManager.GetPanel<PanelInvestment>().HidePanel();
        }
        if (UIManager.GetPanel<PanelJiJinWelfare>() != null)
        {
            UIManager.GetPanel<PanelJiJinWelfare>().HidePanel();
        }
        if (UIManager.GetPanel<PanelBindPhone>() != null)
        {
            UIManager.GetPanel<PanelBindPhone>().HidePanel();
        }

        if (UIManager.GetPanel<PanelFacebookShare>() != null)
        {
            UIManager.GetPanel<PanelFacebookShare>().HidePanel();
        }
        if (UIManager.GetPanel<PanelVKShare>() != null)
        {
            UIManager.GetPanel<PanelVKShare>().HidePanel();
        }
    }
    public override void OnBack()
    {
        HidePanel();
        UIManager.ShowPanel<PanelHallBattle>();
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {

    }

    void SwitchSubPanel()
    {
        bool levelReward = BubbleManager.HasBubble(BubbleConst.WelfareLevelReward);
        bool online = BubbleManager.HasBubble(BubbleConst.WelfareOnlineReward);
        bool sign = BubbleManager.HasBubble(BubbleConst.WelfareSignReward) || BubbleManager.HasBubble(BubbleConst.WelfareLeijiReward);
        if(levelReward)
        {
            m_toggleLevelreward.isOn = true;
            OnChangeLevelRewardView(null, null);
        }
        else if(online)
        {
            m_online.isOn = true;
            OnChangeOnlineRewardView(null, null);
        }
        else if(sign)
        {
            OnChangeSignRewardView(null, null);
        }
    }



    void OnChangeHeroRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelHeroCardReward>();
        m_curPopPanel.ShowPanel();
    }

    void OnChangeShareRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelShare>();
        m_curPopPanel.ShowPanel();
    }


    void OnChangeVKShareRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        NetLayer.Send(new tos_player_social_join_media());

        m_curPopPanel = UIManager.PopPanel<PanelVKShare>();
        m_curPopPanel.ShowPanel();

    }

    void Toc_player_social_join_media(toc_player_social_join_media data)
    {
        m_curPopPanel = UIManager.PopPanel<PanelVKShare>();
        m_curPopPanel.ShowPanel();
    }
    void OnChangePhoneRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelBindPhone>();
        m_curPopPanel.ShowPanel();
    }

    void OnChangeFacebookRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelFacebookShare>();
        m_curPopPanel.ShowPanel();
    }

    void OnChangeMasterRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelSubMasterReward>();
        m_curPopPanel.ShowPanel();
    }

    void OnChangeKeyRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelSubKeyReward>();
        m_curPopPanel.ShowPanel();
    }

    void OnChangeTouziRewardView(GameObject target, PointerEventData eventData)
    {
        if (m_curPopPanel != null)
        {
            m_curPopPanel.HidePanel();
        }
        m_curPopPanel = UIManager.PopPanel<PanelInvestment>();
        m_curPopPanel.ShowPanel();
    }


    void OnChangeSignRewardView( GameObject target, PointerEventData eventData )
    {
        m_toggleSign.isOn = true;
        ShowSignReward();
    }
    void OnChangeLevelRewardView(GameObject target, PointerEventData eventData)
    {
        ShowWelfarePanel(1, "军衔等级越高" + "\n" + "可获奖励越多");
    }
    void OnChangeOnlineRewardView(GameObject target, PointerEventData eventData)
    {
        m_online.isOn = true;
        ShowWelfarePanel(2, "在线时间越长" + "\n" + "可获奖励越多");
    }
    void OnChangePassRewardView(GameObject target, PointerEventData eventData)
    {
        ShowWelfarePanel(3,"通关相应的关卡可以获取奖励");
    }
    void OnChangeCompeteRewardView( GameObject target, PointerEventData eventData )
    {
        ShowWelfarePanel(4,"在排位赛的名次越高，获取的奖励越好");
    }

    void OnClickClose( GameObject target, PointerEventData eventData )
    {
        if (m_curPopPanel != null)
            m_curPopPanel.HidePanel();
        HidePanel();
    }

    void ShowSignReward( )
    {
        if( m_curPopPanel != null )
            m_curPopPanel.HidePanel();
        m_curPopPanel = UIManager.PopPanel<PanelSubSignReward>(null, false, this);
        m_toggleSign.isOn = true;
    }

    void ShowWelfarePanel( int tag,string strDesc )
    {
        if (m_curPopPanel != null)
            m_curPopPanel.HidePanel();
        m_curPopPanel = UIManager.PopPanel<PanelSubWelfare>(new object[]{tag, strDesc}, false, this);
    }
}
