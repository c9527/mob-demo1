﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;


/// <summary>
/// 这个Panel包含在线奖励和升级两部分,在线奖励已经移出,但代码保留原样
/// 以便有错误可以查看.
/// </summary>
public class PanelSubWelfare : BasePanel
{
    public enum WelfareState
    {
        Uncomplete = 0, //未达成
        Complete = 1,   //已达成
        Gained = 2      //已领奖
    }

    public enum WelfareTag
    {
        Level = 1,      //等级奖励
        Online = 2,     //在线奖励
        Stage = 3,      //关卡奖励
        Athletics = 4   //竞技奖励
    }

    /// <summary>
    /// 福利渲染数据结构
    /// </summary>
    public class ItemWelfareInfo
    {
        public ConfigWelfareRewardLine m_config;
        public WelfareState m_state;
    }

    /// <summary>
    /// 福利渲染项
    /// </summary>
    public class ItemWelfareRender : ItemRender
    {
        private ItemWelfareInfo m_data;
        private Text m_txtRewardName;
        private GameObject m_goUnComplete;
        private GameObject m_goUnGained;
        private GameObject m_goGained;
        private GameObject m_goItem1;
        private Image m_imgItem1;
        private Text m_txtItem1Count;
        private GameObject m_goItem2;
        private Image m_imgItem2;
        private Text m_txtItem2Count;

        private Image m_qualityBg1;
        private Image m_qualityBg2;

        private ConfigItemLine m_itemCfg1;
        private ConfigItemLine m_itemCfg2;
        private GridLayoutGroup m_rewardGrid;
        public override void Awake()
        {
            var tran = transform;
            m_txtRewardName = tran.Find("Image/rewardname").GetComponent<Text>();
            m_goUnComplete = tran.Find("uncomplete").gameObject;
            m_goUnGained = tran.Find("ungained").gameObject;
            m_goGained = tran.Find("gained").gameObject;
            m_goItem1 = tran.Find("rewardlist/item1").gameObject;
            m_qualityBg1 = m_goItem1.GetComponent<Image>();
            m_imgItem1 = tran.Find("rewardlist/item1/icon").GetComponent<Image>();
            m_txtItem1Count = tran.Find("rewardlist/item1/Text").GetComponent<Text>();
            m_goItem2 = tran.Find("rewardlist/item2").gameObject;
            m_imgItem2 = tran.Find("rewardlist/item2/icon").GetComponent<Image>();
            m_qualityBg2 = m_goItem2.GetComponent<Image>();
            m_txtItem2Count = tran.Find("rewardlist/item2/Text").GetComponent<Text>();
            m_rewardGrid = tran.Find("rewardlist").GetComponent<GridLayoutGroup>();

            UGUIClickHandler.Get(m_goUnGained).onPointerClick += OnClickGain;
            MiniTipsManager.get(m_goItem1).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg1 == null) return null;
                return new MiniTipsData() { name = m_itemCfg1.DispName, dec = m_itemCfg1.Desc };
            };
            MiniTipsManager.get(m_goItem2).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfg2 == null) return null;
                return new MiniTipsData() { name = m_itemCfg2.DispName, dec = m_itemCfg2.Desc };
            };
        }

        protected override void OnSetData(object data)
        {
            m_data = data as ItemWelfareInfo;
            if (m_data == null)
                return;

            var configWelfareLine = m_data.m_config;
            m_txtRewardName.text = configWelfareLine.CondDesc;

            var rewards = ConfigManager.GetConfig<ConfigReward>().GetLine(configWelfareLine.Reward).ItemList;
            var colorValue = 0;
            if (rewards.Length > 0)
            {
                m_goItem1.TrySetActive(true);
                var cfg = ItemDataManager.GetItem(rewards[0].ID);
                m_itemCfg1 = cfg;
                m_imgItem1.SetSprite(ResourceManager.LoadIcon(rewards[0].ID), cfg != null ? cfg.SubType : "");
                m_imgItem1.SetNativeSize();
                m_txtItem1Count.text = "x" + rewards[0].cnt;
                colorValue = cfg.RareType;
                m_qualityBg1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            }
            else
                m_goItem1.TrySetActive(false);

            if (rewards.Length > 1)
            {
                m_goItem2.TrySetActive(true);
                var cfg = ItemDataManager.GetItem(rewards[1].ID);
                m_itemCfg2 = cfg;
                m_imgItem2.SetSprite(ResourceManager.LoadIcon(rewards[1].ID), cfg != null ? cfg.SubType : "");
                m_imgItem2.SetNativeSize();
                m_txtItem2Count.text = "x" + rewards[1].cnt;
                colorValue = cfg.RareType;
                m_qualityBg2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            }
            else
                m_goItem2.TrySetActive(false);
            if (rewards.Length == 1)
            {
                m_rewardGrid.cellSize = new Vector2(170, 80);
                m_imgItem1.rectTransform.localScale = new Vector3(0.7f,0.7f,0.7f);
            }
            else
            {
                m_rewardGrid.cellSize = new Vector2(80, 80);
                m_imgItem1.rectTransform.localScale = Vector3.one;
            }
            if (m_data.m_state == WelfareState.Complete)
            {
                m_goGained.TrySetActive(false);
                m_goUnGained.TrySetActive(true);
                m_goUnComplete.TrySetActive(false);
            }
            else if (m_data.m_state == WelfareState.Gained)
            {
                m_goGained.TrySetActive(true);
                m_goUnGained.TrySetActive(false);
                m_goUnComplete.TrySetActive(false);
            }
            else
            {
                m_goGained.TrySetActive(false);
                m_goUnGained.TrySetActive(false);
                m_goUnComplete.TrySetActive(true);
            }
        }

        private void OnClickGain(GameObject target, PointerEventData eventData)
        {
            NetLayer.Send(new tos_player_get_welfare_reward() { id = m_data.m_config.ID });
        }
    }

    private static PanelSubWelfare m_instance;
    public PanelSubWelfare()
    {
        SetPanelPrefabPath("UI/Activity/PanelSubWelfare");
        AddPreLoadRes("UI/Activity/PanelSubWelfare", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    private bool m_send;
    private WelfareTag m_mode;
    private int m_needtime;
    //private Text m_textDesc;
    private GameObject m_army;
    private GameObject m_clock;
    private GameObject m_online;
    private Image m_minute1;
    private Image m_minute2;
    private Image m_second1;
    private Image m_second2;
    private Image m_hour1;
    private Image m_point;
    private int m_minute;
    private int m_second;
    private int m_hour;
    private float m_starttime;
    private int m_resttime;
    private Image m_midepoint;
    private bool m_isHour;

    private DataGrid m_dataGrid;

    public override void Init()
    {
        m_instance = this;
        m_isHour = false;
        m_starttime = -1f;
        m_resttime = -1;
        m_minute = 0;
        m_second = 0;
        m_hour = 0;
        m_needtime = 0;
        m_army = m_tran.Find("frame/army").gameObject;
        m_online = m_tran.Find("frame/onlineImg").gameObject;
        m_clock = m_tran.transform.Find("frame/clock").gameObject;
        //m_textDesc = m_tran.Find("frame/desc").GetComponent<Text>();
        m_minute1 = m_tran.Find("frame/clock/minute1").GetComponent<Image>();
        m_minute2 = m_tran.Find("frame/clock/minute2").GetComponent<Image>();
        m_second1 = m_tran.Find("frame/clock/second1").GetComponent<Image>();
        m_second2 = m_tran.Find("frame/clock/second2").GetComponent<Image>();
        m_hour1 = m_tran.Find("frame/clock/hour").GetComponent<Image>();
        m_point = m_tran.Find("frame/clock/point2").GetComponent<Image>();
        m_midepoint = m_tran.Find("frame/clock/point").GetComponent<Image>();
        m_send = true;

        m_dataGrid = m_tran.Find("frame/itemlist/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("frame/itemlist/content/item").gameObject, typeof(ItemWelfareRender));
        m_dataGrid.autoSelectFirst = false;
        m_dataGrid.useClickEvent = false;
        m_dataGrid.useLoopItems = true;
    }

    private void SetTimeUI()
    {
        if (m_isHour)
        {
            m_hour1.gameObject.TrySetActive(true);
            m_point.gameObject.TrySetActive(true);
            m_minute1.transform.localPosition = new Vector3(-27, -28, 0);
            m_minute2.transform.localPosition = new Vector3(7, -28, 0);
            m_midepoint.transform.localPosition = new Vector3(24, -28, 0);
        }
        else
        {
            m_hour1.gameObject.TrySetActive(false);
            m_point.gameObject.TrySetActive(false);

            m_minute1.transform.localPosition = new Vector3(-61, -28, 0);
            m_minute2.transform.localPosition = new Vector3(-27, -28, 0);
            m_midepoint.transform.localPosition = new Vector3(7, -28, 0);
          
        }
    }



    private void SetTime()
    {
        if (m_hour > 0)
        {
            m_isHour = true;
        }
        else
        {
            m_isHour = false;
        }
        SetTimeUI();
        m_hour1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_hour).ToString()));
        m_minute1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_minute / 10).ToString()));
        m_minute2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_minute % 10).ToString()));
        m_second1.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_second / 10).ToString()));
        m_second2.SetSprite(ResourceManager.LoadSprite(AtlasName.Activity, (m_second % 10).ToString()));
    }




    public override void InitEvent()
    {

    }

    public override void OnShow()
    {
        if (HasParams())
        {
            FlushByTag((int)m_params[0]);
            SetDesc((string)m_params[1]);
        }

        NetLayer.Send(new tos_player_welfare_data());
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {

        if (m_starttime < 0)
        {
            return;
        }
        else
        {
            int i = Convert.ToInt32(Time.realtimeSinceStartup - m_starttime);
            int time = m_resttime - i;
            if (time == 0 && m_send)
            {
                m_send = false;
                TimerManager.SetTimeOut(1, () => NetLayer.Send(new tos_player_welfare_data()));
            }
            if (time < 0)
            {
                return;
            }
            m_hour = time / 3600;
            m_minute = time / 60 - m_hour * 60;
            m_second = time % 60;
            SetTime();
        }
    }

    public void FlushByTag(int tag)
    {
        m_mode = (WelfareTag)tag;
        if (m_mode == WelfareTag.Level)
        {
            m_army.TrySetActive(true);
            m_online.TrySetActive(false);
            m_clock.TrySetActive(false);
        }
        else
        {
            m_army.TrySetActive(false);
            m_clock.TrySetActive(true);
            m_online.TrySetActive(true);
        }

        //        m_configWelfareReward = ConfigManager.GetConfig<ConfigWelfareReward>();
    }

    public void SetDesc(string strDesc)
    {
        //m_textDesc.text = strDesc;
    }




    private void UpdateView(p_welfare_data_toc data)
    {
        m_starttime = Time.realtimeSinceStartup;
        var topShowIndex = -1;
        var welfareData = data.welfare_list;
        var dataInfos = new List<ItemWelfareInfo>();

        //获取当前Tag类型的配置数据
        var configWelfare = ConfigManager.GetConfig<ConfigWelfareReward>();
        for (int i = 0; i < configWelfare.m_dataArr.Length; ++i)
        {
            var configLine = configWelfare.m_dataArr[i];
            if (configLine.Tag == m_mode)
            {
                var info = new ItemWelfareInfo();
                info.m_config = configLine;
                info.m_state = (WelfareState)welfareData[i];
                dataInfos.Add(info);

                if (topShowIndex == -1 && (info.m_state == WelfareState.Complete || info.m_state == WelfareState.Uncomplete))
                    topShowIndex = dataInfos.Count - 1;
            }
        }
        if (topShowIndex == -1)
            topShowIndex = dataInfos.Count - 1;
        m_dataGrid.Data = dataInfos.ToArray();
        m_dataGrid.ShowItemOnTop(topShowIndex);

        //在线奖励额外处理
        if (m_mode == WelfareTag.Online)
        {
            var configWelfareReward = ConfigManager.GetConfig<ConfigWelfareReward>();
            m_needtime = -1;
            for (int i = 0; i < welfareData.Length; i++)
            {
                var cfg = configWelfareReward.GetLine(i + 1);
                if (cfg == null || cfg.RewardCond != "online")
                    continue;
                switch ((WelfareState)welfareData[i])
                {
                    case WelfareState.Uncomplete:
                        if (m_needtime < 0)
                            m_needtime = cfg.CondParam;
                        break;
                    case WelfareState.Complete:
                        m_needtime = 0;
                        break;
                }
            }

            int time = data.total_online_time - data.last_online_time;
            time = m_needtime - time;
            if (time <= 0)
            {
                time = 0;
            }
            m_resttime = time;
            m_hour = time / 3600;
            m_minute = time / 60 - m_hour * 60;
            m_second = time % 60;
            SetTime();
        }
    }

    private static void UpdateBubble(p_welfare_data_toc data)
    {
        var welfareData = data.welfare_list;
        var canGaineLevelReward = false;
        var canGaineOnlineReward = false;
        //获取当前Tag类型的配置数据
        var configWelfare = ConfigManager.GetConfig<ConfigWelfareReward>();
        for (int i = 0; i < configWelfare.m_dataArr.Length; ++i)
        {
            var configLine = configWelfare.m_dataArr[i];

            //需要气泡提醒
            if ((WelfareState)welfareData[i] == WelfareState.Complete)
            {
                if (configLine.Tag == WelfareTag.Level)
                    canGaineLevelReward = true;
                else if (configLine.Tag == WelfareTag.Online)
                    canGaineOnlineReward = true;
            }
        }
        BubbleManager.SetBubble(BubbleConst.WelfareLevelReward, canGaineLevelReward);
        BubbleManager.SetBubble(BubbleConst.WelfareOnlineReward, canGaineOnlineReward);
    }

    private static void Toc_player_welfare_data(toc_player_welfare_data proto)
    {
        /*
        PanelHallBattle hallBattle = UIManager.GetPanel<PanelHallBattle>();
        if (hallBattle != null)
            hallBattle.UpdateOnlineReward(proto.data);

        PanelOnlineReward onlineReward = UIManager.GetPanel<PanelOnlineReward>();
        if (onlineReward != null && onlineReward.IsOpen())
            onlineReward.UpdateView(proto.data);

        if (m_instance != null && m_instance.IsOpen())
            m_instance.UpdateView(proto.data);
        */
        



        OnUpdateWelfareData(proto.data);
        UpdateBubble(proto.data);
    }

    private static void Toc_player_get_welfare_reward(toc_player_get_welfare_reward proto)
    {
        /*
        PanelHallBattle panel = UIManager.GetPanel<PanelHallBattle>();
        if (panel != null)
            panel.UpdateOnlineReward(proto.data);

        PanelOnlineReward onlineReward = UIManager.GetPanel<PanelOnlineReward>();
        if (onlineReward != null && onlineReward.IsOpen())
            onlineReward.UpdateView(proto.data);
        */
        PanelOnlineReward onlineReward = UIManager.GetPanel<PanelOnlineReward>();
        OnUpdateWelfareData(proto.data);
        if ((m_instance != null && m_instance.IsOpen()) || (onlineReward != null && onlineReward.IsOpen()))
        {
            //m_instance.UpdateView(proto.data);
            //var tip = UIManager.PopPanel<PanelRewardItemTip>(null, true, m_instance);
            var configWelfareReward = ConfigManager.GetConfig<ConfigWelfareReward>().GetLine(proto.id);
            if (configWelfareReward == null)
                return;
            var configReward = ConfigManager.GetConfig<ConfigReward>().GetLine(configWelfareReward.Reward);
            if (configReward == null)
                return;
            var itemList = configReward.ItemList;
            //tip.SetRewardItemList(itemList);
            PanelRewardItemTipsWithEffect.DoShow(itemList, true);
        }
        UpdateBubble(proto.data);
    }

    private static void OnUpdateWelfareData(p_welfare_data_toc data)
    {
        var configWelfareReward = ConfigManager.GetConfig<ConfigWelfareReward>();
        WelfareDataManger.onlineVoucher = 0;
        WelfareDataManger.onlineVoucherGet = 0;
        for (int i = 0; i < data.welfare_list.Length; i++)
        {
            var cfg = configWelfareReward.GetLine(i + 1);
            if (cfg == null || cfg.RewardCond != "online")
                continue;
            var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(cfg.Reward);
            if (reward != null)
            {
                for (int j = 0; j < reward.ItemList.Length; j++)
                {
                    if (reward.ItemList[j].ID == 5052)
                    {
                        WelfareDataManger.onlineVoucher += reward.ItemList[j].cnt;
                        switch ((WelfareState)data.welfare_list[i])
                        {
                            case WelfareState.Uncomplete:
                                break;
                            case WelfareState.Gained:
                                WelfareDataManger.onlineVoucherGet += reward.ItemList[j].cnt;
                                break;
                        }
                    }
                }
            }

        }

        PanelHallBattle hallBattle = UIManager.GetPanel<PanelHallBattle>();
        if (hallBattle != null)
            hallBattle.UpdateOnlineReward(data);

        PanelOnlineReward onlineReward = UIManager.GetPanel<PanelOnlineReward>();
        if (onlineReward != null && onlineReward.IsOpen())
            onlineReward.UpdateView(data);

        if (m_instance != null && m_instance.IsOpen())
            m_instance.UpdateView(data);
    }
}