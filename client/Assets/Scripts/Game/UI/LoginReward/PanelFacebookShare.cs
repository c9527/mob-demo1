﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PanelFacebookShare : BasePanel
{
    GameObject m_tabFb;
    GameObject m_tabShare;
    GameObject m_tabCode;

    GameObject m_fbFrame;
    GameObject m_shareFrame;
    GameObject m_codeFrame;

    InputField m_codeInput;
    Text m_codeName;
    Text m_myCode;

    public PanelFacebookShare()
    {
        SetPanelPrefabPath("UI/Activity/PanelFacebookShare");
        AddPreLoadRes("UI/Activity/PanelFacebookShare", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    public override void Init()
    {
        m_tabFb = m_tran.Find("tab_bar/btn_0").gameObject;
        m_tabShare = m_tran.Find("tab_bar/btn_1").gameObject;
        m_tabCode = m_tran.Find("tab_bar/btn_2").gameObject;
        m_fbFrame = m_tran.Find("fb_frame").gameObject;
        m_shareFrame = m_tran.Find("share_frame").gameObject;
        m_codeFrame = m_tran.Find("code_frame").gameObject;
        m_codeInput = m_tran.Find("code_frame/InputField").GetComponent<InputField>();
        m_codeName = m_tran.Find("code_frame/name").GetComponent<Text>();
        m_myCode = m_tran.Find("code_frame/invite_code").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tabFb).onPointerClick += delegate { m_fbFrame.TrySetActive(true); m_shareFrame.TrySetActive(false); m_codeFrame.TrySetActive(false); };
        UGUIClickHandler.Get(m_tabShare).onPointerClick += delegate { m_fbFrame.TrySetActive(false); m_shareFrame.TrySetActive(true); m_codeFrame.TrySetActive(false); };
        UGUIClickHandler.Get(m_tabCode).onPointerClick += delegate { m_fbFrame.TrySetActive(false); m_shareFrame.TrySetActive(false); m_codeFrame.TrySetActive(true); };
        UGUIClickHandler.Get(m_tran.Find("fb_frame/Go")).onPointerClick += delegate { SdkManager.JoinVkOrFaceBook(); };
        //UGUIClickHandler.Get(m_tran.Find("code_frame/commit")).onPointerClick += delegate { SdkManager.ShareToVkOrFB("https://vk.com/ssjj.tatru", "test test", "https://pp.vk.me/c836330/v836330929/a201/LjdqVESwwGY.jpg", "Лаги Богинь, 360 градусов, супер свободная игра! Мой код приглашения-это9ymp,вы можете получить "); };
        UGUIClickHandler.Get(m_tran.Find("share_frame/Invite")).onPointerClick += delegate { SdkManager.InviteFriend(); };
        UGUIClickHandler.Get(m_tran.Find("code_frame/commit")).onPointerClick += submitCode;
        m_codeInput.onEndEdit.AddListener(delegate { endedit(); });
    }

    void submitCode(GameObject target, PointerEventData eventData)
    {
        long invitecode;
        if (long.TryParse(m_codeInput.text, out invitecode))
        {
            NetLayer.Send(new tos_player_social_bind_invite_code() { player_id = invitecode, device_code = SystemInfo.deviceUniqueIdentifier });
        }
        else
        {
            m_codeInput.text = "输入邀请码不合法";
        }
    }

    void endedit()
    {
        long invitecode;
        if (long.TryParse(m_codeInput.text, out invitecode))
        {
            NetLayer.Send(new tos_player_social_invitor_name() { player_id = long.Parse(m_codeInput.text) });
        }
        else
        {
            m_codeInput.text = "输入邀请码不合法";
        }
    }


    void Toc_player_social_invitor_name(toc_player_social_invitor_name data)
    {
        m_codeName.text = data.invitor_name;
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        m_myCode.text = "你的邀请码：" + PlayerSystem.roleId;
    }

    public override void Update()
    {

    }
}

