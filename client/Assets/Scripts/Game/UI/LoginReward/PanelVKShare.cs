﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PanelVKShare : BasePanel
{
    public PanelVKShare()
    {
        SetPanelPrefabPath("UI/Activity/PanelVKShare");
        AddPreLoadRes("UI/Activity/PanelVKShare", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }


    GameObject tabjoin;
    GameObject tabshare;
    GameObject tabcode;
    GameObject tabmall;

    GameObject joingo;
    GameObject sharego;
    GameObject codego;
    GameObject mallgo;

    InputField code;
    Text codename;
    Text mycode;
    Text jointext;

    Text codetxt;
    Button bidbtn;

    Text hasget;

    public DataGrid item_list;

    public override void Init()
    {
        tabjoin = m_tran.Find("tab_bar/btn_0").gameObject;
        tabshare = m_tran.Find("tab_bar/btn_1").gameObject;
        tabcode = m_tran.Find("tab_bar/btn_2").gameObject;
        tabmall = m_tran.Find("tab_bar/btn_3").gameObject;
        joingo = m_tran.Find("main_frame").gameObject;
        sharego = m_tran.Find("share_frame").gameObject;
        codego = m_tran.Find("code_frame").gameObject;
        mallgo = m_tran.Find("shop_frame").gameObject;
        code = m_tran.Find("code_frame/InputField").GetComponent<InputField>();
        codename = m_tran.Find("code_frame/name").GetComponent<Text>();
        mycode = m_tran.Find("code_frame/Text").GetComponent<Text>();
        jointext = m_tran.Find("main_frame/Button/Text").GetComponent<Text>();
        codetxt = m_tran.Find("code_frame/Button/Text").GetComponent<Text>();
        bidbtn = m_tran.Find("code_frame/Button").GetComponent<Button>();
        joingo.TrySetActive(true); 
        sharego.TrySetActive(false); 
        codego.TrySetActive(false);
        tabjoin.GetComponent<Toggle>().isOn = true;
        hasget = m_tran.Find("main_frame/Text").GetComponent<Text>();

        item_list = mallgo.transform.Find("item_list/content").gameObject.AddComponent<DataGrid>();
        item_list.autoSelectFirst = false;
        item_list.SetItemRender(mallgo.transform.Find("item_list/Render").gameObject, typeof(ShareShopItemRender));


    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(tabjoin).onPointerClick += delegate { joingo.TrySetActive(true); sharego.TrySetActive(false); codego.TrySetActive(false); mallgo.TrySetActive(false); };
        UGUIClickHandler.Get(tabshare).onPointerClick += delegate { joingo.TrySetActive(false); sharego.TrySetActive(true); codego.TrySetActive(false); mallgo.TrySetActive(false); };
        UGUIClickHandler.Get(tabcode).onPointerClick += delegate { joingo.TrySetActive(false); sharego.TrySetActive(false); codego.TrySetActive(true); mallgo.TrySetActive(false); };
        UGUIClickHandler.Get(tabmall).onPointerClick += delegate { joingo.TrySetActive(false); sharego.TrySetActive(false); codego.TrySetActive(false); mallgo.TrySetActive(true); item_list.Data = ItemDataManager.GetMallItemsByShopType(1); };
        UGUIClickHandler.Get(m_tran.Find("main_frame/Button")).onPointerClick += delegate { SdkManager.JoinVkOrFaceBook(); };
        UGUIClickHandler.Get(m_tran.Find("share_frame/Button")).onPointerClick += delegate { SdkManager.ShareToVkOrFB("https://vk.com/ssjj.tatru", "test test", "https://pp.vk.me/c836330/v836330929/a201/LjdqVESwwGY.jpg", "Лаги Богинь, 360 градусов, супер свободная игра! Мой код приглашения-это9ymp,вы можете получить "); };
        UGUIClickHandler.Get(m_tran.Find("share_frame/Invite")).onPointerClick += delegate { SdkManager.InviteFriend(); };
        UGUIClickHandler.Get(m_tran.Find("code_frame/Button")).onPointerClick += submitCode;
        UGUIClickHandler.Get(m_tran.Find("code_frame/rule1")).onPointerClick += delegate { UIManager.PopRulePanel("ShareCode1"); };
        UGUIClickHandler.Get(m_tran.Find("code_frame/rule2")).onPointerClick += delegate { UIManager.PopRulePanel("ShareCode2"); };
        code.onEndEdit.AddListener(delegate { endedit(); });
        item_list.onItemSelected = OnShareShopItemClick;
    }

    private void OnShareShopItemClick(object renderData)
    {
        var shop_data = renderData as ConfigMallLine;
        if (shop_data != null)
        {

            var itemRender = item_list.TargetGo.GetComponent<ShareShopItemRender>();

            if (itemRender != null && itemRender.CanBuy())
            {
                UIManager.PopPanel<PanelBuy>(new object[] { shop_data }, true, this);
            }
        }
    }

    void submitCode(GameObject target, PointerEventData eventData)
    {
        long invitecode;
        if (long.TryParse(code.text, out invitecode))
        {
            NetLayer.Send(new tos_player_social_bind_invite_code() { player_id = invitecode, device_code = SystemInfo.deviceUniqueIdentifier });
        }
        else
        {
            codename.text = "输入邀请码不合法";
        }
        if (code.text == "")
        {

        }
    }

    void endedit()
    {
        long invitecode;

        if (long.TryParse(code.text,out invitecode))
        {
            NetLayer.Send(new tos_player_social_invitor_name() { player_id = long.Parse(code.text) });
        }
        else
        {
            codename.text = "输入邀请码不合法";
        }

        if (code.text == "")
        {
            codename.text = "";
            
        }
    }

    void Toc_player_social_bind_invite_code(toc_player_social_bind_invite_code data)
    {
        if (data.result == 0)
        {
            TipsManager.Instance.showTips("邀请码不存在");
        }
        if (data.result == 1)
        {
            UIManager.ShowTipPanel("您当前使用的设备已经和您的好友" + codename.text + "绑定，无法再使用该设备绑定");
        }
        if (data.result == 2)
        {
            UIManager.ShowTipPanel("您的好友" + codename.text + "本周已满绑定上限，您可下周再和他绑定哦");
        }
        if (data.result == 3)
        {
            UIManager.ShowTipPanel("绑定成功");
        }

    }



    void Toc_player_social_invitor_name(toc_player_social_invitor_name data)
    {
        codename.text = data.invitor_name;
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        mycode.text = "我的邀请码：" + PlayerSystem.roleId;
        jointext.text = "JOIN";
        codename.text = "";
        if (HasParams())
        {
            toc_player_social_join_media data = m_params[0] as toc_player_social_join_media;
            if (data.join_reward)
            {
                jointext.text = "VISIT";
            }
            hasget.gameObject.TrySetActive(data.join_reward);
            if (data.bind_invite_code)
            {
                code.gameObject.TrySetActive(false);
                Util.SetGoGrayShader(bidbtn.gameObject);
                codetxt.text = "已绑定";
            }
        }

    }

    public override void Update()
    {

    }

    void Toc_player_social_invite_friend(toc_player_social_invite_friend data)
    {
        if (!data.result)
        {
            TipsManager.Instance.showTips("今天已邀请过该好友，明天再来！");
        }
    }

    void Toc_player_social_join_media(toc_player_social_join_media data)
    {
        if (data.join_reward)
        {
            jointext.text = "VISIT";
        }
        else
        {
            jointext.text = "GO";
        }
        hasget.gameObject.TrySetActive(data.join_reward);
        if (data.bind_invite_code)
        {
            code.gameObject.TrySetActive(false);
            Util.SetGoGrayShader(bidbtn.gameObject);
            codetxt.text = "已绑定";
        }
    }

    private void Toc_player_buy_item(toc_player_buy_item data)
    {
            item_list.Data = ItemDataManager.GetMallItemsByShopType(1);
    }

}

