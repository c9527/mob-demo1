﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class WelfareDataManger
{
    public bool CanGet;
    public bool CanBuy;

    public static int onlineVoucher;
    public static int onlineVoucherGet;
    public static int signVoucher;
    public static int signVoucherGet;

    private static WelfareDataManger _singleton;
    public static WelfareDataManger singleton
    {
        get
        {
            if (_singleton == null)
            {
                _singleton = new WelfareDataManger();
            }
            return _singleton;
        }
    }
    public InvestmentData[] InvestmentDatas = null;
    public p_foundation_info[] info = null;


    public class InvestmentData
    {
        public int state = 0;//0-可购买，1-已购买未领取，2-已购买可领取 3-已领取
        public ConfigFoundationRewardLine data;
    }

    private WelfareDataManger()
    {
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_LEVEL_UP, UpdateData);
        Init();
    }

    void Init()
    {
        CanBuy = false;
        CanGet = false;
        UpdateData();
    }

    int CheckState(ConfigFoundationRewardLine data,int state)
    {
        if (ItemDataManager.GetDepotItem(data.ItemId) != null)
        {
            if (PlayerSystem.roleData.level >= data.TargetLevel)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
        return state;
    }

    static void Toc_player_foundation_data(toc_player_foundation_data data)
    {
        for (int i = 0; i < data.data.Length; i++)
        {
            for (int j = 0; j < WelfareDataManger.singleton.InvestmentDatas.Length; j++)
            {
                if (data.data[i].foundation_id == WelfareDataManger.singleton.InvestmentDatas[j].data.ID)
                {
                    if (data.data[i].rewarded)
                    {
                        WelfareDataManger.singleton.InvestmentDatas[j].state = 3;
                    }
                }
            }
        }
        WelfareDataManger.singleton.CanGet = false;
        var lists = ConfigManager.GetConfig<ConfigFoundationReward>().GetAllConst();

        for (int i = 0; i < lists.Length; i++)
        {
            BubbleManager.SetBubble(lists[i], false);
        }


        for (int i = 0; i < WelfareDataManger.singleton.InvestmentDatas.Length; i++)
        {
            if (WelfareDataManger.singleton.InvestmentDatas[i].state == 0)
            {
                WelfareDataManger.singleton.CanBuy = true;
            }
            if (WelfareDataManger.singleton.InvestmentDatas[i].state == 2)
            {
                WelfareDataManger.singleton.CanGet = true;
                BubbleManager.SetBubble(WelfareDataManger.singleton.InvestmentDatas[i].data.Bubble, true);
            }
        }
        //BubbleManager.SetBubble(BubbleConst.Investment, WelfareDataManger.singleton.CanGet);
        GameDispatcher.Dispatch("InvestmentUpdate");
    }

    

    public void UpdateData()
    {
        var config = ConfigManager.GetConfig<ConfigFoundationReward>();
        InvestmentDatas = new InvestmentData[config.m_dataArr.Length];
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            InvestmentDatas[i] = new InvestmentData();
            InvestmentDatas[i].data = config.m_dataArr[i];
            InvestmentDatas[i].state = 0;
            if (PlayerSystem.roleData.level <= InvestmentDatas[i].data.LevelRange[1] && PlayerSystem.roleData.level >= InvestmentDatas[i].data.LevelRange[0])
            {
                InvestmentDatas[i].state = 0;
            }
            else
            {
                InvestmentDatas[i].state = -1;
            }
            InvestmentDatas[i].state = CheckState(InvestmentDatas[i].data, InvestmentDatas[i].state);
        }
        NetLayer.Send(new tos_player_foundation_data());

    }
    static void Toc_item_new(toc_item_new data)
    {
        WelfareDataManger.singleton.UpdateData();
        
    }

    static void Toc_player_get_foundation_reward(toc_player_get_foundation_reward data)
    {
        WelfareDataManger.singleton.UpdateData();
        var cfg = ConfigManager.GetConfig<ConfigFoundationReward>().GetLine(data.id);
        var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(cfg.Reward);
        PanelRewardItemTipsWithEffect.DoShow(reward.ItemList);
    }

    


}

