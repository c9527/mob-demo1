﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoginRemindMangager  {
    private static LoginRemindMangager _instance;
    public static LoginRemindMangager Ins 
    {
        get
        {
            if (_instance == null)
                _instance = new LoginRemindMangager();
            return _instance;
        }
    }
    /// <summary>
    /// {师徒，好友，战队,家族,夫妻}0:关闭,1：飘字,2:聊天
    /// </summary>
    private List<int> m_reminds = new List<int>();

    public LoginRemindMangager()
    {
        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("login_remind");
        m_reminds.Clear();
        string[] arr = cfg.ValueStr.Split(';');
        for(int i=0;i<arr.Length;i++)
        {
            m_reminds.Add(int.Parse(arr[i]));
        }
    }
    /// <summary>
    ///0~4师徒,家族,好友，战队,夫妻,,优先级师徒->好友->战队
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="playerID"></param>
    public void ShowTips(int obj,long playerID)
    {
        string content = "";
        p_corps_member_toc corpsMemmber = CorpsDataManager.GetMemberById(playerID);
        FriendData friendData = PlayerFriendData.singleton.GetFriend(playerID);
        bool isMaster = IsMaster(playerID);
        bool isStudent = IsStudent(playerID);
        bool isFamily = FamilyDataManager.Instance.IsFamilyMember(playerID);
        if(obj==0||obj==1) //师徒,好友
        {
            if (isMaster)
            {
                content = string.Format("师父<color='#AF50FFFF'>{0}</color>已上线", PlayerFriendData.singleton.GetFriend(playerID).m_name);
                obj = 0;
            }
            else if (isStudent)
            {
                content = string.Format("徒弟<color='#AF50FFFF'>{0}</color>已上线", PlayerFriendData.singleton.GetFriend(playerID).m_name);
                obj = 0;
            }
            else if (isFamily)
            {
                content = string.Format("{0}<color='#AF50FFFF'>{1}</color>已上线", FamilyDataManager.Instance.GetRelativeName(playerID), PlayerFriendData.singleton.GetFriend(playerID).m_name);
                obj = 0;
            }
            else if (friendData != null)
                content = string.Format("好友<color='#AF50FFFF'>{0}</color>已上线", friendData.m_name);
            else if (corpsMemmber != null)
                content = string.Format("战队成员<color='#AF50FFFF'>{0}</color>已上线", corpsMemmber.name);
        }      
        else if(obj==2) //战队
        {
            if (isMaster||isStudent||friendData!=null||isFamily)
                return;
            if(corpsMemmber!=null)
            {
                content = string.Format("战队成员<color='#AF50FFFF'>{0}</color>已上线", corpsMemmber.name);
            }
        }        
        if(!string.IsNullOrEmpty(content))
            ShowTipsByType(content, m_reminds[obj]);
    }
    //1飘字，2聊天频道
    private void ShowTipsByType(string content,int type)
    {
        //if (!UIManager.IsOpen<PanelHall>())
        //{
        //    TipsManager.Instance.showTips(content);
        //    return;
        //}
        if(type==1)
        {
            TipsManager.Instance.showTips(content);
        }
        else if(type==2)
        {
            NetChatData.MockChatMsg(content);
        }
    }

    private bool IsMaster(long id)
    {
        return PlayerFriendData.singleton.m_masters.Count > 0 && PlayerFriendData.singleton.m_masters[0].id == id;    
    }

    private bool IsStudent(long id)
    {
        p_master_base_info_toc[] arr = PlayerFriendData.singleton.m_students.ToArray();
        for(int i=0;i<arr.Length;i++)
        {
            if(arr[i].id==id)
            {
                return true;
            }
        }
        return false;
    }

}

