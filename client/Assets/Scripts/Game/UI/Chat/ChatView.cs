﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 聊天内容专用显示组件
/// </summary>
public class ChatView : MonoBehaviour
{
    private ScrollRect m_scrollView;
    private RectTransform m_rectScrollView;
    private RectTransform m_rectContent;

    private float m_lastPosY;
    private float m_contentHeight;
    private readonly List<ChatInfo> m_list = new List<ChatInfo>();  //确保和显示的顺序一致
    private const float LOCK_HEIGHT = 40;   //滚动锁定的高度
    private const float INSTANCE_COUNT_PER_TIME = 10;   //每次实例化的数量
    private float m_currentVelocity;
    private bool m_beginTweenToTop;

    private void Awake()
    {
        m_rectContent = gameObject.GetRectTransform();
        m_rectScrollView = m_rectContent.parent.gameObject.GetRectTransform();
        m_scrollView = m_rectScrollView.GetComponent<ScrollRect>();

        #region

//        m_scrollViewRect.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_UI);

//        var canvas = m_scrollViewRect.gameObject.AddMissingComponent<Canvas>();
//        canvas.overridePixelPerfect = true;
//        canvas.pixelPerfect = false;
//
//        m_scrollViewRect.gameObject.AddMissingComponent<SortingOrderRenderer>();
//        SortingOrderRenderer.RebuildAll();

        #endregion
    }

    private void Update()
    {
        if (m_rectContent.anchoredPosition.y == m_lastPosY)
            return;
        if (m_rectContent.anchoredPosition.y - m_lastPosY > 0 && m_rectContent.anchoredPosition.y - m_lastPosY < 20)
            m_beginTweenToTop = false;
        m_lastPosY = m_rectContent.anchoredPosition.y;

        //根据对象的位置动态判断，隐藏不需要显示的项，提高滑动操作的效率
        if (!gameObject.activeInHierarchy)
            return;
        for (int i = 0; i < m_list.Count; i++)
        {
            if (!m_list[i].instanced)
                continue;
            var itemRect = m_list[i].rect;
            if (itemRect != null)
            {
                var shouldShow = ShouldItemShow(itemRect);
                if (shouldShow != itemRect.gameObject.activeSelf)
                    itemRect.gameObject.TrySetActive(shouldShow);
            }
        }

        //滚动锁
        if (m_rectContent.anchoredPosition.y <= LOCK_HEIGHT)
            GameDispatcher.Dispatch(GameEvent.UI_CHAT_ALL_MSG_READED);

        //触发显示下一组项
        if (m_scrollView.verticalNormalizedPosition < 0)
            ShowNextGroupItems();
    }

    private void LateUpdate()
    {
        //实现新消息滑动显示的效果
        if (Mathf.Abs(m_rectContent.anchoredPosition.y) < 0.001f || !m_beginTweenToTop || m_rectContent.rect.height <= m_rectScrollView.rect.height)
        {
            m_beginTweenToTop = false;
            return;
        }
        var contentPos = m_rectContent.anchoredPosition;
        contentPos.y = Mathf.SmoothDamp(contentPos.y, 0, ref m_currentVelocity, m_scrollView.elasticity, float.PositiveInfinity, Time.unscaledDeltaTime);
        m_rectContent.anchoredPosition = contentPos;
    }

    /// <summary>
    /// 增加单个项，只用在有新消息来的时候
    /// </summary>
    /// <param name="item"></param>
    public void AddItem(ChatRecord item)
    {
        var chatItem = CreateChatItem(item, 0, true);

        //从下往上显示
        m_contentHeight += chatItem.m_itemHeight;
        m_rectContent.sizeDelta = new Vector2(m_rectContent.sizeDelta.x, m_contentHeight);
        var contentPos = m_rectContent.anchoredPosition;
        if (m_rectContent.anchoredPosition.y > LOCK_HEIGHT)
        {
            m_rectContent.anchoredPosition = new Vector2(contentPos.x,contentPos.y + chatItem.m_itemHeight);
            GameDispatcher.Dispatch(GameEvent.UI_CHAT_NEW_UNREAD_MSG);
        }
        else
        {
            m_rectContent.anchoredPosition = new Vector2(contentPos.x, chatItem.m_itemHeight);
            m_beginTweenToTop = true;
        }
        var tempHeight = chatItem.m_itemHeight;
        for (int i = 0; i < m_list.Count; i++)
        {
            if (m_list[i].rect != null)
            {
                m_list[i].rect.anchoredPosition = new Vector2(m_list[i].rect.anchoredPosition.x, -tempHeight);
            }
            tempHeight += m_list[i].itemHeight;
        }

        m_list.Insert(0, new ChatInfo { renderData = item, instanced = true, rect = chatItem.m_rect, itemHeight = chatItem.m_itemHeight});
    }

    /// <summary>
    /// 先清空，再重新显示项，为了效率考虑，只先创建一组（默认10个），后续再由用户的操作来触发创建下一组
    /// </summary>
    /// <param name="items"></param>
    public void ReShowAllItems(ChatRecord[] items)
    {
        Clear();

        for (int i = 0; i < items.Length; i++)
        {
            if (i >= INSTANCE_COUNT_PER_TIME)
            {
                m_list.Add(new ChatInfo { renderData = items[i], instanced = false });
                continue;
            }

            var chatItem = CreateChatItem(items[i], 0, true);

            //从上往下显示
            chatItem.m_rect.anchoredPosition = new Vector2(chatItem.m_rect.anchoredPosition.x, -m_contentHeight);
            m_contentHeight += chatItem.m_itemHeight;

            m_list.Add(new ChatInfo { renderData = items[i], instanced = true, rect = chatItem.m_rect, itemHeight = chatItem.m_itemHeight });
        }

        m_rectContent.sizeDelta = new Vector2(m_rectContent.sizeDelta.x, m_contentHeight);
    }

    /// <summary>
    /// 追加显示(创建)下一组(默认10个)未实例化的项
    /// </summary>
    private void ShowNextGroupItems()
    {
        //如果为空或者最后一项已经实例化，则直接退出
        if (m_list.Count == 0 || m_list[m_list.Count-1].instanced)
            return;

        var startIndex = -1;
        for (int i = 0; i < m_list.Count; i++)
        {
            if (m_list[i].instanced)
                continue;
            if (startIndex == -1)
                startIndex = i;
            if (i < startIndex + INSTANCE_COUNT_PER_TIME)
            {
                var chatItem = CreateChatItem(m_list[i].renderData, 0, false);

                //从上往下显示
                chatItem.m_rect.anchoredPosition = new Vector2(chatItem.m_rect.anchoredPosition.x, -m_contentHeight);
                m_contentHeight += chatItem.m_itemHeight;

                m_list[i].instanced = true;
                m_list[i].rect = chatItem.m_rect;
                m_list[i].itemHeight = chatItem.m_itemHeight;
            }
        }

        if (startIndex != -1)
            m_rectContent.sizeDelta = new Vector2(m_rectContent.sizeDelta.x, m_contentHeight);
    }

    /// <summary>
    /// 判断item是否在可视范围内
    /// </summary>
    /// <param name="itemRect"></param>
    /// <returns></returns>
    private bool ShouldItemShow(RectTransform itemRect)
    {
        if (itemRect == null)
            return false;
        return Mathf.Abs(m_rectScrollView.InverseTransformPoint(itemRect.position).y) < 0.5f * m_rectScrollView.rect.height + itemRect.rect.height;
    }

    /// <summary>
    /// 创建聊天项
    /// </summary>
    /// <param name="item"></param>
    /// <param name="chatItemType"></param>
    /// <param name="goActive"></param>
    /// <returns></returns>
    private ChatItem CreateChatItem(ChatRecord item, int chatItemType, bool goActive)
    {
        var goItem = UIHelper.Instantiate(ResourceManager.LoadUIRef("UI/Chat/ItemChat")) as GameObject;
        var rectItem = goItem.GetComponent<RectTransform>();
        rectItem.SetParent(m_rectContent, false);
        goItem.TrySetActive(goActive);

        var scriptItem = goItem.AddComponent<ChatItem>();
        scriptItem.CreateView(item);

        return scriptItem;
    }

    /// <summary>
    /// 置顶
    /// </summary>
    public void ResetToTop()
    {
        m_scrollView.verticalNormalizedPosition = 1;
    }

    /// <summary>
    /// 清除内容
    /// </summary>
    public void Clear()
    {
        for (int i = 0; i < m_list.Count; i++)
        {
            if (m_list[i].rect != null)
            {
                Destroy(m_list[i].rect.gameObject);
            }
        }
        m_list.Clear();
        m_contentHeight = 0;
        m_currentVelocity = 0;
        m_beginTweenToTop = false;
        m_rectContent.sizeDelta = new Vector2(m_rectContent.sizeDelta.x, m_contentHeight);
    }
}

public class ChatInfo
{
    public bool instanced = false;
    public ChatRecord renderData = null;
    public RectTransform rect = null;
    public float itemHeight;
}