﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 聊天显示项
/// </summary>
class ChatItem : MonoBehaviour
{
    public RectTransform m_rect;
    public float m_itemHeight;
    private ChatRecord m_chatRecord;
    private Image m_imgAudioIcon;
    private readonly ItemDropInfo[] m_popMenuWorld;
    private readonly ItemDropInfo[] m_popMenuOther;

    private float m_nameBgInitWidth;

    public ChatItem()
    {
        if (m_popMenuWorld == null)
        {
            m_popMenuWorld = new[]
            {
                new ItemDropInfo {type = "CHAT", subtype = "ADDFRIEND", label = "加为好友"},
                new ItemDropInfo {type = "CHAT", subtype = "LOOKUP", label = "查看信息"},                
                new ItemDropInfo {type = "CHAT", subtype = "FOLLOW", label = "跟随"},
                new ItemDropInfo {type = "CHAT", subtype = "PRIVATE", label = "私聊"},
                new ItemDropInfo {type = "CHAT", subtype = "BLACK", label = "拉黑"},                
            };
        }

        if (m_popMenuOther == null)
        {
            m_popMenuOther = new[]
            {
                new ItemDropInfo {type = "CHAT", subtype = "ADDFRIEND", label = "加为好友"},
                new ItemDropInfo {type = "CHAT", subtype = "LOOKUP", label = "查看信息"},                
                new ItemDropInfo {type = "CHAT", subtype = "FOLLOW", label = "跟随"},
                new ItemDropInfo {type = "CHAT", subtype = "BLACK", label = "拉黑"},
            };
        }
    }

    public void CreateView(ChatRecord data)
    {
        m_chatRecord = data;
        m_chatRecord.m_strChatInfo = m_chatRecord.m_strChatInfo.Replace("00EAFF", "503C64");
        if (m_chatRecord == null)
            return;

        var tran = GetComponent<RectTransform>();
        var imgHead = tran.Find("head").GetComponent<Image>();
        var imgHeadBg = tran.Find("headBg").GetComponent<Image>();
        var imgHeadVipBg = tran.Find("headVipBg").GetComponent<Image>();
        var txtChat = tran.Find("chat").GetComponent<Text>();
        var imgChatBg = tran.Find("chatBg").GetComponent<Image>();
        var layoutH = tran.Find("info").GetComponent<HorizontalLayoutGroup>();
        var txtChannel = tran.Find("info/channel/Text").GetComponent<Text>();
        var imgChannel = tran.Find("info/channel").GetComponent<Image>();
        var imgSex = tran.Find("info/sex").GetComponent<Image>();
        var imgTitle = tran.Find("info/title").GetComponent<Image>();
        var txtName = tran.Find("info/name").GetComponent<Text>();
        var nameBgLayout = tran.Find("info/channel").GetComponent<LayoutElement>();
        m_nameBgInitWidth = nameBgLayout.preferredWidth;

        var minChatBgWidth = 0;
        if (HasAudio())
        {
            var tranAudio = tran.Find("btnAudio").GetComponent<RectTransform>();
            var txtAudio = tran.Find("btnAudio/Text").GetComponent<Text>();
            m_imgAudioIcon = tran.Find("imgAudioIcon").GetComponent<Image>();
            if (m_chatRecord.m_isMyself)
            {
                SetToRight(tranAudio);
                MirrirToRight(m_imgAudioIcon.rectTransform);
            }
            tranAudio.gameObject.TrySetActive(true);
            m_imgAudioIcon.gameObject.TrySetActive(true);
            txtAudio.text = m_chatRecord.m_audio.duration + "\"";
            minChatBgWidth = 105;
            UGUIClickHandler.Get(tranAudio).onPointerClick += OnAudioBtnClick;
        }
        m_rect = tran;
        imgHead.SetSprite(ResourceManager.LoadRoleIcon(m_chatRecord.m_iHead));
        string areaStr = (m_chatRecord.m_area == null || m_chatRecord.m_area == "") ? "" : "(" + m_chatRecord.m_area + ")";
        txtName.text = string.Format("<color='#898989'>{0}</color>", m_chatRecord.m_strName + areaStr);        
        SetChannelIcon(txtChannel, imgChannel, nameBgLayout);
        imgSex.SetSprite(ResourceManager.LoadSprite(AtlasName.FRIEND, m_chatRecord.m_iSex == 0 ? "man" : "woman"));
        if (m_chatRecord.m_heroType == (int)EnumHeroCardType.none)
            UGUIClickHandler.Get(imgHead.gameObject).onPointerClick += OnClickOperation;
        else
            UGUIClickHandler.Get(imgHeadVipBg.gameObject).onPointerClick += OnClickOperation;
        
        if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LABA)
            imgChatBg.color = new Color32(255, 160, 0, 255);

        if (m_chatRecord.m_eMrbType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
            imgTitle.enabled = false;
        else
        {
            imgTitle.enabled = true;
            imgTitle.SetSprite(ResourceManager.LoadVipIcon(m_chatRecord.m_eMrbType));
        }

        string userName = null;
        if (data.m_heroType == (int) EnumHeroCardType.super)
            userName = RightManager.USER_HERO_CARD_SUPER;
        else if (data.m_heroType == (int) EnumHeroCardType.legend)
            userName = RightManager.USER_HERO_CARD_LEGEND;
        string chatString = string.Empty;
        if(data.m_heroType == (int)EnumHeroCardType.none)
        {
            chatString = string.Format("<color='#BFB9B2'>{0}</color>", m_chatRecord.m_strChatInfo);
        }     
        else
        {
            chatString = m_chatRecord.m_strChatInfo;
        }
        txtChat.text = HasAudio() ? "\n" + chatString : chatString;
        txtChat.gameObject.AddComponent<MotionText>().Create(NetChatData.HandleLinkAction, userName);
        imgChatBg.rectTransform.sizeDelta = new Vector2(Mathf.Clamp(txtChat.preferredWidth, minChatBgWidth, txtChat.rectTransform.sizeDelta.x) + 18, txtChat.preferredHeight + 7);

        imgHeadVipBg.gameObject.TrySetActive(false);
        if(data.m_heroType != (int)EnumHeroCardType.none)
        {
            imgHeadVipBg.gameObject.TrySetActive(true);
            if (data.m_heroType == (int)EnumHeroCardType.normal)
            {
                SetHeadHeroCard(imgChatBg, imgHeadVipBg, GameConst.BUBBLE_NORMAL, GameConst.HEAD_NORMAL);
            }
            else if (data.m_heroType == (int)EnumHeroCardType.super)
            {
                SetHeadHeroCard(imgChatBg, imgHeadVipBg, GameConst.BUBBLE_SUPER, GameConst.HEAD_SUPER);
            }
            else if (data.m_heroType == (int)EnumHeroCardType.legend)
            {
                SetHeadHeroCard(imgChatBg, imgHeadVipBg, GameConst.BUBBLE_LEGEND, GameConst.HEAD_LEGEND);
            }
        }

        m_itemHeight = 37 + txtChat.preferredHeight + 7;
        tran.sizeDelta = new Vector2(tran.sizeDelta.x, m_itemHeight);

        if (m_chatRecord.m_isMyself)
        {
            SetToRight(imgHead.rectTransform);
            SetToRight(imgHeadBg.rectTransform);
            SetToRight(imgHeadVipBg.rectTransform);
            MirrirToRight(imgChatBg.rectTransform);
            
            SetToRight(layoutH.GetComponent<RectTransform>());
            layoutH.childAlignment = TextAnchor.MiddleRight;

            SetToRight(txtChat.rectTransform);
            var chatPos = txtChat.rectTransform.anchoredPosition;
            var chatWidth = txtChat.rectTransform.sizeDelta.x;
            chatPos.x += chatWidth - Mathf.Min(txtChat.preferredWidth, chatWidth);
            txtChat.rectTransform.anchoredPosition = chatPos;            
        }
    }

    private void SetHeadHeroCard(Image charBg, Image headBg, string bubble, string head)
    {
        charBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, bubble));
        headBg.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, head));
    }

    private void SetToRight(RectTransform rect)
    {
        var pos = rect.anchoredPosition;
        rect.pivot = Vector2.one;
        rect.anchorMin = Vector2.one;
        rect.anchorMax = Vector2.one;
        rect.anchoredPosition = new Vector2(-pos.x, pos.y);
    }

    private void MirrirToRight(RectTransform rect)
    {
        SetToRight(rect);
        rect.localScale = new Vector3(-1, 1, 1);
        var pos = rect.anchoredPosition;
        pos.x -= rect.rect.width;
        rect.anchoredPosition = pos;
    }

    private void SetChannelIcon(Text label, Image img, LayoutElement nameBgLayout)
    {
        if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER)
        {
            label.text = NetChatData.WorldChannelCrsSerOpen ? "本服" : "世界";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
        }
        if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_CRSSERVER)
        {
            label.text = NetChatData.WorldChannelCrsSerOpen ? "世界" : "跨服";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
        }
        if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LABA)
        {
            label.text = "喇叭";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
        }
        else if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_FRIEND)
        {
            if ((!m_chatRecord.m_isMyself && FamilyDataManager.Instance.IsFamilyMember(m_chatRecord.m_iPlayerId)))
            {
                var str = FamilyDataManager.Instance.GetRelativeName(m_chatRecord.m_iPlayerId);
                nameBgLayout.preferredWidth = m_nameBgInitWidth * str.Length / 2f;
                label.text = str;
            }
            else
            {
                nameBgLayout.preferredWidth = m_nameBgInitWidth;
                label.text = "好友"; 
            }
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "friend_channel"));
        }
        else if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_CORPS)
        {
            label.text = "战队";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "team_channel"));
        }
        else if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_PRIVATE)
        {
            label.text = "私聊";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "friend_channel"));
        }
        else if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LEGION)
        {
            label.text = "军团";
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "legion_channel"));
        }
    }

    private void Update()
    {
        if (m_playTime <= 0 || m_imgAudioIcon == null)
            return;

        var index = (int)Mathf.Floor((Time.time - m_playTime) * 3);
        index = index % 3 + 1;
        if (index != m_audioIconIndex)
        {
            m_audioIconIndex = index;
            m_imgAudioIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voice_" + m_audioIconIndex));
        }
    }

    private void OnClickOperation(GameObject target, PointerEventData eventData)
    {
        if (m_chatRecord.m_isMyself)
            return;

        var lst = m_popMenuOther;
        if (m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER || m_chatRecord.m_eChannel == CHAT_CHANNEL.CC_LABA)
            lst = m_popMenuWorld;

        var parent = m_rect.parent.parent.parent as RectTransform;
        var offsetY = parent.InverseTransformPoint(target.GetComponent<RectTransform>().position).y;
        UIManager.ShowDropList(lst, OnOpListItemSelected, parent, parent, new Vector2(325, offsetY - 92), UIManager.Direction.Down, 145, 39, UIManager.DropListPattern.Ex);
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        if (PlayerFriendData.singleton.isFriend(m_chatRecord.m_playerRcd.m_iPlayerId))
            m_chatRecord.m_playerRcd.m_isFriend = true;
        else
            m_chatRecord.m_playerRcd.m_isFriend = false;

        if (data.subtype == "ADDFRIEND")
        {
            PlayerFriendData.AddFriend(m_chatRecord.m_iPlayerId);
        }
        else if (data.subtype == "FOLLOW")
        {
            RoomModel.FollowFriend(m_chatRecord.m_iPlayerId, false, false);
        }
        else if (data.subtype == "BLACK")
        {
            NetChatData.AddBlack(m_chatRecord.m_iPlayerId);
        }
        else if (data.subtype == "LOOKUP")
        {
            PlayerFriendData.ViewPlayer(m_chatRecord.m_iPlayerId);
        }
        else if (data.subtype == "PRIVATE")
        {
            PanelChatSmall pPanelSmall = UIManager.GetPanel<PanelChatSmall>();
            if (pPanelSmall != null && pPanelSmall.IsOpen())
            {
                pPanelSmall.SwitchPersonChat(m_chatRecord.m_playerRcd);
                return;
            }

            PanelChatBig pPanelBig = UIManager.GetPanel<PanelChatBig>();
            if (pPanelBig != null && pPanelBig.IsOpen())
            {
                pPanelBig.SwitchPersonChat(m_chatRecord.m_playerRcd);
                return;
            }
        }
    }

    #region 语音
    private float m_playTime;
    private int m_audioIconIndex;
    private bool HasAudio()
    {
        return m_chatRecord.m_audio != null && m_chatRecord.m_audio.audio_id != 0;
    }

    private void OnAudioBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!HasAudio())
            return;
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            UIManager.ShowNoThisFunc();
            return;
        }

        if (m_playTime <= 0)
        {
            m_playTime = Time.time;
            NetChatData.playSpeech(m_chatRecord.m_audio.audio_id, OnResetItem, m_chatRecord.m_audio.content, m_chatRecord.m_audio.duration);
        }
        else
        {
            NetChatData.stopSpeech();
            OnResetItem();
        }
    }

    private void OnResetItem()
    {
        m_playTime = 0;
        m_audioIconIndex = 0;
        if (m_imgAudioIcon != null)
        {
            m_imgAudioIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voice_0"));
            m_imgAudioIcon.SetNativeSize();
        }
    }
    #endregion
}