﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelLabaInput : BasePanel
{
    private RectTransform m_tranOk;
    private RectTransform m_tranBtnClose;
    private InputField m_input;
    private Text m_txtCost;
    private Image m_imgIcon;
    private Text m_txtLabaCount;

    public PanelLabaInput()
    {
        SetPanelPrefabPath("UI/Chat/PanelLabaInput");
        AddPreLoadRes("UI/Chat/PanelLabaInput", EResType.UI);
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
    }

    public override void Init()
    {
        m_tranOk = m_tran.Find("background/btnOk").GetComponent<RectTransform>();
        m_tranBtnClose = m_tran.Find("background/btnClose").GetComponent<RectTransform>();
        m_input = m_tran.Find("background/InputField").GetComponent<InputField>();
        m_input.gameObject.AddComponent<InputFieldFix>();
        m_txtCost = m_tran.Find("background/btnOk/txtCost").GetComponent<Text>();
        m_imgIcon = m_tran.Find("background/btnOk/imgIcon").GetComponent<Image>();
        m_txtLabaCount = m_tran.Find("background/txtLabaCount").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tranBtnClose).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tranOk).onPointerClick += OnSendBtnClick;
    }

    private void OnSendBtnClick(GameObject target, PointerEventData eventData)
    {
        var msg = m_input.text.Trim(' ', '\n');
        if (string.IsNullOrEmpty(msg))
        {
            TipsManager.Instance.showTips("内容不能为空");
            return;
        }

        var canSend = false;
        if (ItemDataManager.IsDepotHas(GameConst.ITEM_ID_LABA) && ItemDataManager.GetDepotItem(GameConst.ITEM_ID_LABA).item_cnt > 0)
            canSend = true;
        if (!canSend && PlayerSystem.CheckMoney(ConfigMisc.GetInt("chat_diamond"), EnumMoneyType.DIAMOND, null))
            canSend = true;

        if (canSend)
        {
            msg = WordFiterManager.Fiter(msg);
            NetChatData.SndChatInfo((int)CHAT_CHANNEL.CC_LABA, 0, msg);
            HidePanel();
        }
        else
        {
            HidePanel();
        }
    }

    public override void OnShow()
    {
        UpdateView();
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void UpdateView()
    {
        var labaItemId = 5015;
        var pitem = ItemDataManager.GetDepotItem(labaItemId);
        var labaCount = pitem != null ? pitem.item_cnt : 0;

        if (labaCount == 0)
        {
            m_imgIcon.SetSprite(ResourceManager.LoadMoneyIcon(EnumMoneyType.DIAMOND));
            m_imgIcon.SetNativeSize();
            m_txtCost.text = ConfigMisc.GetInt("chat_diamond").ToString();
        }
        else
        {
            m_imgIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "laba"));
            m_imgIcon.SetNativeSize();
            m_txtCost.text = "1";
        }

        m_txtLabaCount.text = labaCount.ToString();
    }
}