﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelChatMotion : BasePanel                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
{
    static private Sprite _motion_texture;

    private Vector3 _position;
    private Vector2 _offset;
    private Sprite[] _motion_sprites;

    public Transform frame_trans;
    public Transform motion_list;
    private GameObject motion_class;
    public Action<string> MotionSelected;

    public PanelChatMotion()
    {
        SetPanelPrefabPath("UI/Chat/ChatMotionList");

        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        //AddPreLoadRes("Atlas/ChatEmotion", EResType.Atlas);
        AddPreLoadRes("UI/Chat/ChatMotionList", EResType.UI);
    }

    public override void Init()
    {
        frame_trans = m_tran.Find("frame");
        motion_class = m_tran.Find("frame/motion_list/Render").gameObject;
        motion_list = m_tran.Find("frame/motion_list/content");
    }

    public override void InitEvent()
    {
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, ReloadUpdate);
        AddEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, UpdateView);
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _position = (Vector3)m_params[0];
            if(m_params.Length>1)
            {
                _offset = (Vector2)m_params[1];
            }
            else
            {
                _offset = Vector2.zero;
            }
        }
        var rect_trans = frame_trans.GetComponent<RectTransform>();
        var offset = Vector2.zero;
        if(rect_trans!=null)
        {
            var size = rect_trans.sizeDelta;
            offset.x = size.x * rect_trans.pivot.x;
            offset.y = size.y * rect_trans.pivot.y;
            offset = offset - size + _offset;
        }
        frame_trans.position = _position;
        frame_trans.localPosition += (Vector3)offset;
        UpdateView();
    }

    private void ReloadUpdate()
    {
        _motion_texture = null;
        UpdateView();;
    }

    private void UpdateView()
    {
        if (_motion_texture == null)
        {
            _motion_texture = ResourceManager.LoadSprite(AtlasName.Common, "motion");
            var heroType = HeroCardDataManager.getHeroMaxCardType();
            if (heroType == EnumHeroCardType.super || heroType == EnumHeroCardType.legend)
            {
                _motion_sprites = new Sprite[ConfigManager.GetConfig<ConfigMisc>().GetLine("chat_motion_hero_card").ValueInt];
            }
            else
            {
                _motion_sprites = new Sprite[ConfigManager.GetConfig<ConfigMisc>().GetLine("chat_motion").ValueInt];
            }
            for (int i = 0; i < _motion_sprites.Length; i++)
            {
                var row = (int)(i / 8);
                var col = i % 8;
                if (i < 48)
                {
                    Rect rect = new Rect(col * 32, row * 32, 32, 32);
                    rect.position += _motion_texture.textureRect.position;
                    _motion_sprites[i] = Sprite.Create(_motion_texture.texture, rect, Vector2.one / 2);
                }
                else
                {
                    //Rect rect_ = new Rect(col * 48, 6 * 32 + (row-6) * 48, 48, 48);
                    //rect_.position =  new Vector2(col * 48, 6 * 32 + (row - 6) * 48);
                    var motion_vip_texture = ResourceManager.LoadSprite(AtlasName.Common, "motion_hero_card" + row.ToString() + col.ToString());
                    _motion_sprites[i] = motion_vip_texture;

                }

                //_motion_sprites[i] = ResourceManager.LoadSprite(AtlasName.ChatEmotion, "emotion_"+i);
                GameObject motion_item = null;
                if (i < motion_list.childCount)
                {
                    motion_item = motion_list.GetChild(i).gameObject;
                }
                else
                {
                    motion_item = UIHelper.Instantiate(motion_class) as GameObject;
                    motion_item.transform.SetParent(motion_list);
                    motion_item.transform.localScale = Vector3.one;
                    UGUIClickHandler.Get(motion_item).onPointerClick += OnMotionClick;
                }
                motion_item.TrySetActive(true);
                motion_item.name = "#" + row + col;
                var motion_img = motion_item.transform.Find("emotion").GetComponent<Image>();
                motion_img.SetSprite(_motion_sprites[i]);
                motion_img.SetNativeSize();
            }
            for (int m = _motion_sprites.Length; m < motion_list.childCount; m++)
            {
                motion_list.GetChild(m).gameObject.TrySetActive(false);
            }
        }
    }

    private void OnMotionClick(GameObject target, PointerEventData eventData)
    {
        var motion_key = target.name;
        if (MotionSelected != null)
        {
            MotionSelected.Invoke(motion_key);
        }
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        _motion_texture = null;
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, ReloadUpdate);
    }

    public override void Update()
    {
        
    }
}
