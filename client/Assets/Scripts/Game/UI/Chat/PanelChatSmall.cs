﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelChatSmall : BasePanel
{
    private GameObject m_goSmallPlayer;
    private GameObject m_goSmallChat;
    private GameObject m_goSmallInput;

    private GameObject m_goSmallMicroBlog;
    private InputField m_input;
    private Button m_microBlogMotionBtn;
    private Image m_headIcon;
    private DataGrid m_microBlogDataGrid;
    private List<MicroBlogData> m_microBlogDataList;
    public GameObject dropTarget;

    private GameObject m_audio_go;

    private ChatView m_smallChatView;
    private Transform m_tranTopbar;
    private Text m_txtUnreadMsg;
    private int m_unreadMsgCount;

    private DataGrid m_smallPlayerData;

    private InputField m_smallInputField;
    private InputField m_smallFindFriend;

    private readonly PlayerRecord m_curSpeakTo = new PlayerRecord();

    private GameObject m_goNewMsgPlayerNum;
    private Text m_txtNewMsgPlayerNum;

    private GameObject m_goNewMsgPlayerNumPrivate;
    private Text m_txtNewMsgPlayerNumPrivate;

    private GameObject m_goFriend;
    private GameObject m_goCorps;

    private FRAME_TYPE m_curFrameType;

    private Toggle m_toggleWorld;
    private Toggle m_toggleFriend;
    private Toggle m_toggleCorps;
    private Toggle m_togglePrivate;
    private Toggle m_toggleLegion;
    private Toggle m_toggleLocalOpen;
    private Toggle m_toggleMicroBlog;

    private Text m_txtWorld;
    private Text m_txtFriend;
    private Text m_txtCorps;
    private Text m_txtPrivate;
    private Text m_txtLegion;
    private Text m_txtLocalOpen;
    private Text m_txtMicroBlog;
   

    private Text m_toggleLocalOpenTxt;

    private GameObject m_goPopListAnchor;
    private GameObject m_goPopListPanel;

    private Button m_btnAudioAction;
    private Button m_sendback;
    private Button m_motion_btn;

    private GameObject m_goCorpsBubble;
    private GameObject m_goLegionBubble;

    public PanelChatSmall()
    {
        SetPanelPrefabPath("UI/Chat/PanelChatSmall");
        AddPreLoadRes("UI/Chat/PanelChatSmall", EResType.UI);
        AddPreLoadRes("UI/Chat/ItemPlayerInfo", EResType.UI);
        AddPreLoadRes("UI/Chat/ItemChat", EResType.UI);
        AddPreLoadRes("UI/PlayerInfo/MicroBlog/ItemMicroBlogSmall", EResType.UI);

        AddPreLoadRes("Atlas/MicroBlog", EResType.Atlas);
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
        AddPreLoadRes("Atlas/HeroCard", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
    }

    public override void Init()
    {
        m_goSmallPlayer = m_tran.Find("frame_small/player").gameObject;
        m_goSmallChat = m_tran.Find("frame_small/chat_info").gameObject;
        m_goSmallInput = m_tran.Find("frame_small/input").gameObject;

        /*if (Driver.m_platform == EnumPlatform.ios)
        {
            m_audio_go = m_tran.Find("frame_small/input/audio").gameObject;
            m_audio_go.TrySetActive(false);
        }*/

        m_smallChatView = m_tran.Find("frame_small/chat_info/content").gameObject.AddComponent<ChatView>();

        m_tranTopbar = m_tran.Find("frame_small/imgTopbar");
        m_txtUnreadMsg = m_tran.Find("frame_small/imgTopbar/Text").GetComponent<Text>();

        m_smallPlayerData = m_tran.Find("frame_small/player/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_smallPlayerData.SetItemRender(ResourceManager.LoadUIRef("UI/Chat/ItemPlayerInfo"), typeof(ItemPlayerRecord));
        m_smallPlayerData.useLoopItems = true;

        m_goSmallMicroBlog = m_tran.Find("frame_small/microblog").gameObject;

        m_smallInputField = m_tran.Find("frame_small/input/InputField").GetComponent<InputField>();
        m_smallInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitSmall;

        m_smallFindFriend = m_tran.Find("frame_small/player/find_btn/InputField").GetComponent<InputField>();
        m_smallFindFriend.gameObject.AddComponent<InputFieldFix>().onEndEdit += OnFindFriend;
        m_goFriend = m_tran.Find("frame_small/player/find_btn").gameObject;
        m_goCorps = m_tran.Find("frame_small/player/corps").gameObject;

        m_toggleWorld = m_tran.Find("frame_small/grid/ToggleWorld").GetComponent<Toggle>();
        m_toggleFriend = m_tran.Find("frame_small/grid/ToggleFriend").GetComponent<Toggle>();
        m_toggleCorps = m_tran.Find("frame_small/grid/ToggleCorps").GetComponent<Toggle>();
        m_togglePrivate = m_tran.Find("frame_small/grid/TogglePrivate").GetComponent<Toggle>();
        m_toggleLegion = m_tran.Find("frame_small/grid/ToggleLegion").GetComponent<Toggle>();
        m_toggleLocalOpen = m_tran.Find("frame_small/grid/ToggleLocalOpen").GetComponent<Toggle>();
        m_toggleMicroBlog = m_tran.Find("frame_small/grid/ToggleMicroBlog").GetComponent<Toggle>();

        m_txtWorld = m_tran.Find("frame_small/grid/ToggleWorld/Label").GetComponent<Text>();
        m_txtFriend = m_tran.Find("frame_small/grid/ToggleFriend/Label").GetComponent<Text>();
        m_txtCorps = m_tran.Find("frame_small/grid/ToggleCorps/Label").GetComponent<Text>();
        m_txtPrivate = m_tran.Find("frame_small/grid/TogglePrivate/Label").GetComponent<Text>();
        m_txtLegion = m_tran.Find("frame_small/grid/ToggleLegion/Label").GetComponent<Text>();
        m_txtLocalOpen = m_tran.Find("frame_small/grid/ToggleLocalOpen/Label").GetComponent<Text>();
        m_txtMicroBlog = m_tran.Find("frame_small/grid/ToggleMicroBlog/Label").GetComponent<Text>();

        m_goNewMsgPlayerNum = m_tran.Find("frame_small/grid/ToggleFriend/bubble").gameObject;
        m_txtNewMsgPlayerNum = m_tran.Find("frame_small/grid/ToggleFriend/bubble/Text").GetComponent<Text>();
        m_goNewMsgPlayerNumPrivate = m_tran.Find("frame_small/grid/TogglePrivate/bubble").gameObject;
        m_txtNewMsgPlayerNumPrivate = m_tran.Find("frame_small/grid/TogglePrivate/bubble/Text").GetComponent<Text>();

        m_goPopListAnchor = m_tran.Find("pop_list_anchor").gameObject;
        m_goPopListPanel = m_tran.Find("frame_small/pop_list_panel").gameObject;

        m_btnAudioAction = m_tran.Find("frame_small/input/audio_action_btn").GetComponent<Button>();
        m_btnAudioAction.gameObject.TrySetActive(false);

        m_sendback = m_tran.Find("frame_small/input/send_back").GetComponent<Button>();
        m_motion_btn = m_tran.Find("frame_small/input/motion_btn").GetComponent<Button>();

        m_goCorpsBubble = m_tran.Find("frame_small/grid/ToggleCorps/bubble").gameObject;
        m_goLegionBubble = m_tran.Find("frame_small/grid/ToggleLegion/bubble").gameObject;

#region microblog
        m_microBlogMotionBtn = m_tran.Find("frame_small/microblog/up/btn_emoji").GetComponent<Button>();
        m_headIcon = m_tran.Find("frame_small/microblog/up/headframe/head").GetComponent<Image>();
        m_input = m_tran.Find("frame_small/microblog/up/InputField").GetComponent<InputField>();
        m_input.gameObject.AddComponent<InputFieldFix>();
        dropTarget = m_tran.Find("frame_small/microblog/dropTarget").gameObject;

        m_microBlogDataGrid = m_tran.Find("frame_small/microblog/middle/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_microBlogDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/PlayerInfo/MicroBlog/ItemMicroBlogSmall"), typeof(ItemMicroBlog));
#endregion

    }

    public override void InitEvent()
    {
        
        //if (Driver.m_platform == EnumPlatform.ios)
        //    UGUIClickHandler.Get(m_tran.Find("frame_small/input/audio")).onPointerClick += null;
        //else
            UGUIClickHandler.Get(m_tran.Find("frame_small/input/audio")).onPointerClick += OnAudioBtnClick;
        UGUIClickHandler.Get(m_tran.Find("frame_small/laba")).onPointerClick += delegate
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            UIManager.PopPanel<PanelLabaInput>(null, true, null, LayerType.Chat);
        };
        UGUIClickHandler.Get(m_motion_btn.gameObject).onPointerClick += (target, evt) =>
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(60, -30)}, false, this).MotionSelected = OnSelectedMotion;
            }
        };
        UGUIUpDownHandler.Get(m_btnAudioAction.gameObject).onPointerUp += delegate { PanelSpeechSpeaker.showSpeaker(1); };
        UGUIUpDownHandler.Get(m_btnAudioAction.gameObject).onPointerDown += delegate { PanelSpeechSpeaker.showSpeaker(); };
        UGUIClickHandler.Get(m_tran.Find("frame_small/expand").gameObject).onPointerClick += OnExpand;
        UGUIClickHandler.Get(m_tran.Find("frame_small/close").gameObject).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_toggleWorld.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_WORLD); };
        UGUIClickHandler.Get(m_toggleLocalOpen.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_LOCAL); };
        UGUIClickHandler.Get(m_toggleCorps.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_CORPS); };
        UGUIClickHandler.Get(m_toggleFriend.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_FRIEND); };
        UGUIClickHandler.Get(m_togglePrivate.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_PRIVATE); };
        UGUIClickHandler.Get(m_toggleLegion.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_LEGION); };
        UGUIClickHandler.Get(m_toggleMicroBlog.gameObject).onPointerClick += delegate { ShowTab(FRAME_TYPE.SMALL_MICROBLOG); };
        UGUIClickHandler.Get(m_sendback.gameObject).onPointerClick += OnBtnSendChatMsgClick;
        UGUIClickHandler.Get(m_tranTopbar).onPointerClick += delegate { m_smallChatView.ResetToTop(); };
        AddEventListener<ChatRecord>(GameEvent.UI_NEW_CHAT_MSG, OnNewChatMsg);
        AddEventListener(GameEvent.UI_NEW_MSG_PLAYER_NUM, OnUpdateNewMsgPlayerNum);
        AddEventListener(GameEvent.UI_NEW_MSG_PLAYER_NUM_PRIVATE, OnUpdateNewMsgPlayerNumPrivate);
        AddEventListener(PlayerFriendData.FRIEND_DATA_UPDATE, OnFriendUpdate);
        //AddEventListener(PlayerFriendData.ADD_FRIEND, OnFriendUpdate);
        AddEventListener(GameEvent.UI_GUESTS_UPDATE, OnGuestsUpdate);
        AddEventListener<int, string>(GameEvent.SDK_SPEECH_COMPLETE, OnSpeechHandler);
        AddEventListener(GameEvent.UI_CHAT_NEW_UNREAD_MSG, OnNewUnreadMsgAdd);
        AddEventListener(GameEvent.UI_CHAT_ALL_MSG_READED, OnAllMsgReaded);
        AddEventListener(GameEvent.MAINPLAYER_HEROCARD_UPDATE, UpdateView);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener(GameEvent.INPUT_KEY_ENTER_UP, () => { OnBtnSendChatMsgClick(null, null); });
#endif

        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
        UGUIClickHandler.Get(m_microBlogMotionBtn.gameObject).onPointerClick += (target, evt) =>
        {
            if (UIManager.IsOpen<PanelChatMotion>())
            {
                UIManager.GetPanel<PanelChatMotion>().HidePanel();
            }
            else
            {
                UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(60, -30) }, false, this).MotionSelected = OnSelectedMotion;
            }
        };
        UGUIClickHandler.Get(m_tran.Find("frame_small/microblog/up/btn_comment")).onPointerClick += OnClickSendBtn;

    }

    private void OnSelectedMotion(string key)
    {
        var input = m_curFrameType == FRAME_TYPE.SMALL_MICROBLOG ? m_input : m_smallInputField;

        if (input.text.Length + key.Length > input.characterLimit)
            return;
        input.text += key;

    }

    /// <summary>
    /// 所有消息已读
    /// </summary>
    private void OnAllMsgReaded()
    {
        m_unreadMsgCount = 0;
        m_tranTopbar.gameObject.TrySetActive(false);
    }

    /// <summary>
    /// 有新未读消息
    /// </summary>
    private void OnNewUnreadMsgAdd()
    {
        m_unreadMsgCount++;
        m_tranTopbar.gameObject.TrySetActive(true);
        m_txtUnreadMsg.text = string.Format("您有{0}条未读消息", m_unreadMsgCount);
    }

    /// <summary>
    /// 开始录音
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnAudioBtnClick(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        if (SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            var audio_mode = !m_btnAudioAction.gameObject.activeSelf;
            m_smallInputField.gameObject.TrySetActive(!audio_mode);
            m_motion_btn.gameObject.TrySetActive(!audio_mode);
            m_sendback.gameObject.TrySetActive(!audio_mode);
            m_btnAudioAction.gameObject.TrySetActive(audio_mode);
        }
        else
        {
            UIManager.ShowNoThisFunc();
        }
    }

    /// <summary>
    /// 当语音录制完毕
    /// </summary>
    /// <param name="status"></param>
    /// <param name="msg"></param>
    private void OnSpeechHandler(int status, string msg)
    {
        PanelSpeechSpeaker.closeSpeaker();
        if (status == 1 && !string.IsNullOrEmpty(msg))
        {
            var paramData = msg.Split('|');
            SendChatMsg(paramData[0], NetChatData.loadSpeechData(), int.Parse(paramData[1]));
        }
        else
        {
            UIManager.ShowTipPanel(status == 1 ? string.Format("Speech Error: \nstatus:{0},\nmessage: {1}", status, msg) : msg);
        }
    }

    public override void OnShow()
    {
        if (NetChatData.WorldChannelCrsSerOpen)
            m_txtLocalOpen.text = string.Format("<color='#282C2F'>{0}</color>", "跨服");
        else
            m_txtLocalOpen.text = string.Format("<color='#282C2F'>{0}</color>", "本服");
        m_txtWorld.text = string.Format("<color='#282C2F'>{0}</color>", "世界");
        m_txtFriend.text = string.Format("<color='#282C2F'>{0}</color>", "好友");

        if (m_params == null)
        {
            ShowTab(FRAME_TYPE.SMALL_LOCAL);
        }
        else if (m_params.Length >= 1)
        {
            var msgTip = (ChatMsgTips)m_params[0];
            if (msgTip.m_eChannel == CHAT_CHANNEL.CC_FRIEND || msgTip.m_eChannel == CHAT_CHANNEL.CC_PRIVATE)
            {
                PlayerRecord rcd = msgTip.m_rcd;
                if (PlayerFriendData.singleton.isFriend(rcd.m_iPlayerId))
                    rcd.m_isFriend = true;
                else
                    rcd.m_isFriend = false;

                if (rcd.m_isFriend)
                {
                    m_toggleFriend.isOn = true;
                    ShowTab(FRAME_TYPE.SMALL_FRIEND);
                }
                else
                {
                    m_togglePrivate.isOn = true;
                    ShowTab(FRAME_TYPE.SMALL_PRIVATE);
                    NetChatData.AddGuest(rcd);
                }

                SwitchPersonChat(rcd);
            }
            else if (msgTip.m_eChannel == CHAT_CHANNEL.CC_CORPS)
            {
                m_toggleCorps.isOn = true;
                ShowTab(FRAME_TYPE.SMALL_CORPS);
            }
            else if( msgTip.m_eChannel == CHAT_CHANNEL.CC_LEGION )
            {
                m_toggleLegion.isOn = true;
                ShowTab(FRAME_TYPE.SMALL_LEGION);
            }
            else if (msgTip.m_eChannel == CHAT_CHANNEL.CC_CRSSERVER)
            {
                m_toggleWorld.isOn = true;
                ShowTab(FRAME_TYPE.SMALL_WORLD);
            }
            else if (msgTip.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER)
            {
                m_toggleLocalOpen.isOn = true;
                ShowTab(FRAME_TYPE.SMALL_LOCAL);
            }
        }

        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();
        UpdateCorpsBubble();
        UpdateLegionBubble();
    }

    public override void OnHide()
    {
        m_smallInputField.gameObject.TrySetActive(true);
        m_motion_btn.gameObject.TrySetActive(true);
        m_sendback.gameObject.TrySetActive(true);
        m_btnAudioAction.gameObject.TrySetActive(false);
        UIManager.HideDropList();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    /// <summary>
    /// 点击发送按钮
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnBtnSendChatMsgClick(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        SendChatMsg(m_smallInputField.text);
        if (m_smallInputField != null)
            m_smallInputField.text = "";
    }

    /// <summary>
    /// 聊天内容submit后自动发送
    /// </summary>
    /// <param name="text"></param>
    private void OnChatSubmitSmall(string text)
    {
        SendChatMsg(text);
        if (m_smallInputField != null)
            m_smallInputField.text = "";
    }

    /// <summary>
    /// 发送聊天信息
    /// </summary>
    /// <param name="content"></param>
    /// <param name="audioData"></param>
    /// <param name="duration"></param>
    private void SendChatMsg(string content, byte[] audioData = null, int duration = 0)
    {
        if (string.IsNullOrEmpty(content))
            return;

        if (content == "fpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.AddMissingComponent<UIGmPanel>();
            p.enabled = true;
            return;
        }
        else if (content == "hidefpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.GetComponent<UIGmPanel>();
            if (p != null)
                p.enabled = false;
            return;
        }

        content = WordFiterManager.Fiter(content);
        var eChannel = CHAT_CHANNEL.CC_LOCALSERVER;
        long toId = 0;
        if (m_curFrameType == FRAME_TYPE.SMALL_WORLD)
        {
            eChannel =NetChatData.WorldChannelCrsSerOpen?  CHAT_CHANNEL.CC_CRSSERVER:CHAT_CHANNEL.CC_LOCALSERVER;
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_CORPS)
        {
            eChannel = CHAT_CHANNEL.CC_CORPS;
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_LEGION)
        {
            eChannel = CHAT_CHANNEL.CC_LEGION;
        }
        else if(m_curFrameType==FRAME_TYPE.SMALL_LOCAL)
        {
            eChannel = NetChatData.WorldChannelCrsSerOpen ? CHAT_CHANNEL.CC_LOCALSERVER : CHAT_CHANNEL.CC_CRSSERVER;
        }
        else
        {
            toId = m_curSpeakTo.m_iPlayerId;
            List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
            var newRcd = new ChatRecord
            {
                m_iHead = PlayerSystem.roleData.icon,
                m_strName = PlayerSystem.roleData.name,
                m_strChatInfo = content,
                m_audio = new p_audio_chat
                {
                    audio_id = audioData != null ? int.MaxValue : 0,
                    content = audioData,
                    duration = duration
                },
                m_isMyself = true,
                m_iSex = PlayerSystem.roleData.sex
            };
            if (MemberManager.IsHighMember())
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_HIGH;
            else if (MemberManager.IsMember())
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_NORMAL;
            else
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_NONE;

            if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
            {
                eChannel = CHAT_CHANNEL.CC_FRIEND;
                newRcd.m_eChannel = CHAT_CHANNEL.CC_FRIEND;

                lstRcds.Insert(0, newRcd);
                GameDispatcher.Dispatch(GameEvent.UI_NEW_CHAT_MSG, newRcd);
            }
            else if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
            {
                eChannel = CHAT_CHANNEL.CC_PRIVATE;
                toId = m_curSpeakTo.m_iPlayerId;
                newRcd.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
                lstRcds.Insert(0, newRcd);
                NetChatData.AddGuest(m_curSpeakTo);
                GameDispatcher.Dispatch(GameEvent.UI_NEW_CHAT_MSG, newRcd);
            }
        }
        NetChatData.SndChatInfo((int)eChannel, toId, content, audioData, duration);
    }

    /// <summary>
    /// 查找好友
    /// </summary>
    /// <param name="name"></param>
    private void OnFindFriend(string name)
    {
        if (string.IsNullOrEmpty(name))
            return;

        for (int i = 0; i < NetChatData.m_lstAllFriends.Count; ++i)
        {
            PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
            if (rcd.m_strName == name)
            {
                if (rcd.m_bOnline)
                    SwitchPersonChat(rcd);
                else
                    UIManager.ShowTipPanel("离线不可以聊天");
                return;
            }
        }
        UIManager.ShowTipPanel("该用户不是好友");
    }

    /// <summary>
    /// 展开大聊天窗
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnExpand(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        UIManager.PopChatPanel<PanelChatBig>(null, true, null);
    }

    /// <summary>
    /// 切换tab页
    /// </summary>
    /// <param name="eType"></param>
    private void ShowTab(FRAME_TYPE eType)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        UIManager.HideDropList();
        if (eType > FRAME_TYPE.SMALL_LOCAL)
            return;

        FRAME_TYPE oldType = m_curFrameType;

        m_curSpeakTo.Reset();
        m_tran.Find("frame_small/grid/ToggleFriend/Label").GetComponent<Text>().text = "好友";
        m_tran.Find("frame_small/grid/TogglePrivate/Label").GetComponent<Text>().text = "私聊";

        m_curFrameType = eType;
        m_toggleWorld.isOn = m_curFrameType == FRAME_TYPE.SMALL_WORLD;
        m_toggleCorps.isOn = m_curFrameType == FRAME_TYPE.SMALL_CORPS;
        m_toggleFriend.isOn = m_curFrameType == FRAME_TYPE.SMALL_FRIEND;
        m_togglePrivate.isOn = m_curFrameType == FRAME_TYPE.SMALL_PRIVATE;
        m_toggleLocalOpen.isOn = m_curFrameType == FRAME_TYPE.SMALL_LOCAL;
        m_toggleLegion.isOn = m_curFrameType == FRAME_TYPE.SMALL_LEGION;

        m_txtFriend.text = string.Format("<color='#A0A0A0'>{0}</color>", "好友");
        m_txtPrivate.text = string.Format("<color='#A0A0A0'>{0}</color>", "私聊");
        m_txtCorps.text = string.Format("<color='#A0A0A0'>{0}</color>", "战队");
        m_txtLegion.text = string.Format("<color='#A0A0A0'>{0}</color>", "军团");
        m_txtWorld.text = string.Format("<color='#A0A0A0'>{0}</color>", "世界");
        m_txtLocalOpen.text = string.Format("<color='#A0A0A0'>{0}</color>", "本服");
        m_txtMicroBlog.text = string.Format("<color='#A0A0A0'>{0}</color>", "好友圈");

        m_goSmallMicroBlog.TrySetActive(m_curFrameType == FRAME_TYPE.SMALL_MICROBLOG);

        if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
        {
            m_goSmallPlayer.TrySetActive(true);
            m_goFriend.TrySetActive(true);
            m_goCorps.TrySetActive(false);
            m_goSmallChat.TrySetActive(false);
            m_goSmallInput.TrySetActive(false);
            m_txtFriend.text = string.Format("<color='#282C2F'>{0}</color>","好友");
            //NetChatData.FlushFriends();
            m_smallPlayerData.Data = NetChatData.m_lstAllFriends.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
        {
            m_goSmallPlayer.TrySetActive(true);
            m_goFriend.TrySetActive(false);
            m_goCorps.TrySetActive(false);
            m_goSmallChat.TrySetActive(false);
            m_goSmallInput.TrySetActive(false);
            m_txtPrivate.text = string.Format("<color='#282C2F'>{0}</color>", "私聊");
            //NetChatData.FlushGuests();
            m_smallPlayerData.Data = NetChatData.m_lstGuests.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_CORPS)
        {
            List<PlayerRecord> lstRcds = CorpsDataManager.GetCorpsMember();
            if (lstRcds.Count == 0)
            {
                TipsManager.Instance.showTips("你还没有战队");
                ShowTab(oldType);
                return;
            }
            m_goSmallPlayer.TrySetActive(false);
            m_goSmallChat.TrySetActive(true);
            m_goSmallInput.TrySetActive(true);
            m_txtCorps.text = string.Format("<color='#282C2F'>{0}</color>", "战队");
            UpdateCorpsBubble();
            m_smallChatView.ReShowAllItems(NetChatData.m_lstCorpsChatRcds.ToArray());
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_LEGION)
        {
//            if (!LegionManager.IsJoinLegion())
//            {
//                TipsManager.Instance.showTips("你还没有军团");
//                ShowTab(oldType);
//                return;
//            }
            m_goSmallPlayer.TrySetActive(false);
            m_goSmallChat.TrySetActive(true);
            m_goSmallInput.TrySetActive(true);
            m_txtLegion.text = string.Format("<color='#282C2F'>{0}</color>", "军团");
            UpdateLegionBubble();
            m_smallChatView.ReShowAllItems(NetChatData.m_lstLegionChatRcds.ToArray());
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_WORLD)
        {
            m_goSmallPlayer.TrySetActive(false);
            m_goSmallChat.TrySetActive(true);
            m_goSmallInput.TrySetActive(true);
            m_txtWorld.text = string.Format("<color='#282C2F'>{0}</color>", "世界");
            ChatRecord[] arr = NetChatData.WorldChannelCrsSerOpen ? NetChatData.m_lstCrsServerChatRcds.ToArray() : NetChatData.m_lstLocalServerChatRcds.ToArray();
            m_smallChatView.ReShowAllItems(arr);
        }
        else if(m_curFrameType==FRAME_TYPE.SMALL_LOCAL)
        {
            m_goSmallPlayer.TrySetActive(false);
            m_goSmallChat.TrySetActive(true);
            m_goSmallInput.TrySetActive(true);
            m_txtLocalOpen.text = string.Format("<color='#282C2F'>{0}</color>", "本服");
            ChatRecord[] arr = NetChatData.WorldChannelCrsSerOpen ? NetChatData.m_lstLocalServerChatRcds.ToArray() : NetChatData.m_lstCrsServerChatRcds.ToArray();
            m_smallChatView.ReShowAllItems(arr);
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_MICROBLOG)
        {
            m_goSmallPlayer.TrySetActive(false);
            m_goFriend.TrySetActive(false);
            m_goCorps.TrySetActive(false);
            m_goSmallChat.TrySetActive(false);
            m_goSmallInput.TrySetActive(false);
            m_txtMicroBlog.text = string.Format("<color='#282C2F'>{0}</color>", "好友圈");
            OnShowMicroBlog();
        }
        m_smallInputField.text = "";
        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();
    }

    /// <summary>
    /// 移除聊天对象列表中，有新消息的标记
    /// </summary>
    /// <param name="id"></param>
    /// <param name="isFriend"></param>
    private void DecMsgNum(long id, bool isFriend)
    {
        if (isFriend)
        {
            int idx = NetChatData.m_lstHasMsgPlayerId.IndexOf(id);
            if (idx > -1)
                NetChatData.m_lstHasMsgPlayerId.RemoveAt(idx);
        }
        else
        {
            int idx = NetChatData.m_lstHasMsgPlayerIdPrivate.IndexOf(id);
            if (idx > -1)
                NetChatData.m_lstHasMsgPlayerIdPrivate.RemoveAt(idx);
        }
    }

    /// <summary>
    /// 当点选好友或者私聊的人
    /// </summary>
    /// <param name="rcd"></param>
    public void SwitchPersonChat(PlayerRecord rcd)
    {
        if (rcd.m_isFriend)
        {
            m_goNewMsgPlayerNum.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatFriend, false);
            //DecMsgNum(rcd.m_iPlayerId, true);
            NetChatData.DecNewMsgId(rcd.m_iPlayerId, true);
            OnUpdateNewMsgPlayerNum();
            ShowTab(FRAME_TYPE.SMALL_FRIEND);
        }
        else
        {
            m_goNewMsgPlayerNumPrivate.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatPrivate, false);
            //DecMsgNum(rcd.m_iPlayerId, false);
            NetChatData.DecNewMsgId(rcd.m_iPlayerId, false);
            OnUpdateNewMsgPlayerNumPrivate();
            ShowTab(FRAME_TYPE.SMALL_PRIVATE);
        }

        m_curSpeakTo.Clone(rcd);

        if (!NetChatData.m_dicPrivateChatRcds.ContainsKey(m_curSpeakTo.m_iPlayerId))
        {
            NetChatData.m_dicPrivateChatRcds[m_curSpeakTo.m_iPlayerId] = new List<ChatRecord>();
        }
        List<ChatRecord> lstRcds = NetChatData.m_dicPrivateChatRcds[m_curSpeakTo.m_iPlayerId];

        m_goSmallPlayer.TrySetActive(false);
        m_goSmallChat.TrySetActive(true);
        m_goSmallInput.TrySetActive(true);

        if (m_curSpeakTo.m_isFriend)            
            m_txtFriend.text = string.Format("<color='#282C2F'>{0}</color>",m_curSpeakTo.m_strName);
        else
            m_txtFriend.text = string.Format("<color='#A0A0A0'>{0}</color>", m_curSpeakTo.m_strName);
        m_smallChatView.ReShowAllItems(lstRcds.ToArray());

        NetChatData.FlushOfflineMsg(rcd.m_iPlayerId);
    }

    /// <summary>
    /// 服务端新消息到来
    /// </summary>
    /// <param name="msg"></param>
    private void OnNewChatMsg(ChatRecord msg)
    {
        if (msg.m_eChannel == CHAT_CHANNEL.CC_LOCALSERVER || msg.m_eChannel == CHAT_CHANNEL.CC_LABA)
        {
            if(NetChatData.WorldChannelCrsSerOpen)
            {
                if (m_curFrameType == FRAME_TYPE.SMALL_LOCAL)
                    m_smallChatView.AddItem(NetChatData.m_lstLocalServerChatRcds[0]);
            }
            else
            {
                if(m_curFrameType==FRAME_TYPE.SMALL_WORLD)
                    m_smallChatView.AddItem(NetChatData.m_lstLocalServerChatRcds[0]);
            }
        }
        else if(msg.m_eChannel==CHAT_CHANNEL.CC_CRSSERVER||msg.m_eChannel==CHAT_CHANNEL.CC_LABA)
        {
            if(NetChatData.WorldChannelCrsSerOpen)
            {
                if (m_curFrameType == FRAME_TYPE.SMALL_WORLD)
                    m_smallChatView.AddItem(NetChatData.m_lstCrsServerChatRcds[0]);
            }
            else
            {
                if(m_curFrameType==FRAME_TYPE.SMALL_LOCAL)
                    m_smallChatView.AddItem(NetChatData.m_lstCrsServerChatRcds[0]);
            }
        }
        else if ( msg.m_eChannel == CHAT_CHANNEL.CC_CORPS)
        {
            if (m_curFrameType == FRAME_TYPE.SMALL_CORPS)
            {
                m_smallChatView.AddItem(NetChatData.m_lstCorpsChatRcds[0]);
                NetChatData.m_bHasCorpsChatMsg = false;
            }
                
            else
                UpdateCorpsBubble();

        }
        else if ( msg.m_eChannel == CHAT_CHANNEL.CC_LEGION)
        {
            if (m_curFrameType == FRAME_TYPE.SMALL_LEGION)
            {
                m_smallChatView.AddItem(NetChatData.m_lstLegionChatRcds[0]);
                NetChatData.m_bHasLegionChatMsg = false;
            }   
            else
                UpdateLegionBubble();
        }
        else if ( msg.m_eChannel == CHAT_CHANNEL.CC_FRIEND)
        {
            if( m_curFrameType == FRAME_TYPE.SMALL_FRIEND )
            {
                if (m_curSpeakTo.m_iPlayerId == 0)
                    return;
                //DecMsgNum(m_curSpeakTo.m_iPlayerId, true);
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, true);
                List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
                m_smallChatView.AddItem(lstRcds[0]);
            }
            
        }
        else if (msg.m_eChannel == CHAT_CHANNEL.CC_PRIVATE)
        {
            if( m_curFrameType == FRAME_TYPE.SMALL_PRIVATE )
            {
                if (m_curSpeakTo.m_iPlayerId == 0)
                    return;

                //DecMsgNum(m_curSpeakTo.m_iPlayerId, false);
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, false);
                List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
                m_smallChatView.AddItem(lstRcds[0]);
            }   
        }
    }

    /// <summary>
    /// 更新好友聊天对象列表的显示
    /// </summary>
    private void OnUpdateNewMsgPlayerNum()
    {
        if (!IsOpen())
            return;

        int num = NetChatData.m_lstHasMsgPlayerId.Count;
        if (num == 0)
        {
            m_goNewMsgPlayerNum.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatFriend, false);
            m_txtNewMsgPlayerNum.text = "";

            if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
            {
                if (m_curSpeakTo.m_iPlayerId == 0)
                {
                    //NetChatData.FlushFriends();
                    m_smallPlayerData.Data = NetChatData.m_lstAllFriends.ToArray();
                }
            }
        }
        else
        {
            if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
            {
                m_goNewMsgPlayerNum.TrySetActive(false);
                BubbleManager.SetBubble(BubbleConst.HallChatFriend, false);
                m_txtNewMsgPlayerNum.text = "";
                if (m_curSpeakTo.m_iPlayerId == 0)
                {
                    //NetChatData.FlushFriends();
                    m_smallPlayerData.Data = NetChatData.m_lstAllFriends.ToArray();
                }
            }
            else
            {
                m_goNewMsgPlayerNum.TrySetActive(true);
                BubbleManager.SetBubble(BubbleConst.HallChatFriend, true);
                m_txtNewMsgPlayerNum.text = num.ToString();
            }
        }

    }

    /// <summary>
    /// 更新私聊聊天对象列表的显示
    /// </summary>
    private void OnUpdateNewMsgPlayerNumPrivate()
    {
        if (!IsOpen())
            return;

        int num = NetChatData.m_lstHasMsgPlayerIdPrivate.Count;
        if (num == 0)
        {
            m_goNewMsgPlayerNumPrivate.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatPrivate, false);
            m_txtNewMsgPlayerNumPrivate.text = "";
            if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
            {
                if (m_curSpeakTo.m_iPlayerId == 0)
                {
                    //NetChatData.FlushGuests();
                    m_smallPlayerData.Data = NetChatData.m_lstGuests.ToArray();
                }
            }
        }
        else
        {
            if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
            {
                m_goNewMsgPlayerNumPrivate.TrySetActive(false);
                BubbleManager.SetBubble(BubbleConst.HallChatPrivate, false);
                m_txtNewMsgPlayerNumPrivate.text = "";
                if (m_curSpeakTo.m_iPlayerId == 0)
                {
                    //NetChatData.FlushGuests();
                    m_smallPlayerData.Data = NetChatData.m_lstGuests.ToArray();
                }
            }
            else
            {
                m_goNewMsgPlayerNumPrivate.TrySetActive(true);
                BubbleManager.SetBubble(BubbleConst.HallChatPrivate, true);
                m_txtNewMsgPlayerNumPrivate.text = num.ToString();
            }
        }
    }

    public GameObject GetPopListAnchor()
    {
        return m_goPopListAnchor;
    }

    public GameObject GetPopListPanel()
    {
        return m_goPopListPanel;
    }

    public RectTransform GetTrans()
    {
        return m_tran;
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[2] { data,UIManager.CurContentPanel}, true, null, LayerType.Chat);
        }
    }

    private void OnFriendUpdate()
    {
        if (!IsOpen())
            return;

        NetChatData.FlushFriends();

        if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
        {        
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstAllFriends.Count; i++)
                {
                    PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);
                        break;
                    }
                }
            }
            if (m_goSmallPlayer.activeSelf)
                ShowTab(FRAME_TYPE.SMALL_FRIEND);
        }
    }

    private void OnGuestsUpdate()
    {
        if (!IsOpen())
            return;

        if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstGuests.Count; ++i)
                {
                    PlayerRecord rcd = NetChatData.m_lstGuests[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);
                        break;
                    }
                }
            }
            if (m_goSmallPlayer.activeSelf)
                ShowTab(FRAME_TYPE.SMALL_PRIVATE);
        }
    }

    private void UpdateCorpsBubble()
    {
        if( NetChatData.m_bHasCorpsChatMsg == false || m_curFrameType == FRAME_TYPE.SMALL_CORPS)
        {
            m_goCorpsBubble.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatCorps, false);
            NetChatData.m_bHasCorpsChatMsg = false;
        }
        else
        {
            m_goCorpsBubble.TrySetActive(true);
            BubbleManager.SetBubble(BubbleConst.HallChatCorps, true);
        }
    }

    private void UpdateLegionBubble()
    { 
        if( NetChatData.m_bHasLegionChatMsg == false || m_curFrameType == FRAME_TYPE.SMALL_LEGION )
        {
            m_goLegionBubble.TrySetActive(false);
            BubbleManager.SetBubble(BubbleConst.HallChatLegion, false);
            NetChatData.m_bHasLegionChatMsg = false;
        }
        else
        {
            m_goLegionBubble.TrySetActive(true);
            BubbleManager.SetBubble(BubbleConst.HallChatLegion, true);
        }
    }

    public void UpdateView()
    {
        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();
        
        if (m_curFrameType == FRAME_TYPE.SMALL_FRIEND)
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                if ( !PlayerFriendData.singleton.isFriend(m_curSpeakTo.m_iPlayerId))
                {
                    m_curSpeakTo.Reset();
                    ShowTab(FRAME_TYPE.SMALL_FRIEND);
                }
                else
                {
                    for (int i = 0; i < NetChatData.m_lstAllFriends.Count; i++)
                    {
                        PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
                        if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                        {
                            string tips = rcd.m_strName + "已经下线了!";
                            TipsManager.Instance.showTips(tips);
                            break;
                        }
                    }
                }
                
            }
            if (m_goSmallPlayer.activeSelf)
                ShowTab(FRAME_TYPE.SMALL_FRIEND);
        }
        else if (m_curFrameType == FRAME_TYPE.SMALL_PRIVATE)
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstGuests.Count; ++i)
                {
                    PlayerRecord rcd = NetChatData.m_lstGuests[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);
                        break;
                    }
                }

                if(PlayerFriendData.singleton.isFriend(m_curSpeakTo.m_iPlayerId) )
                    ShowTab(FRAME_TYPE.SMALL_PRIVATE);
            }
            if (m_goSmallPlayer.activeSelf)
                ShowTab(FRAME_TYPE.SMALL_PRIVATE);
        }

    }

    #region microblog
       
    private void OnClickSendBtn(GameObject target, PointerEventData data)
    {
        string inputText = WordFiterManager.Fiter(m_input.text.Trim());
       
        if (!String.IsNullOrEmpty(inputText))
        {
            m_input.text = String.Empty;
            NetLayer.Send(new tos_microblog_add_blog() { content = inputText });
        }
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
    }

    public void OnShowMicroBlog()
    {
        m_headIcon.SetRoleIconOrSelfIcon(PlayerSystem.roleData.icon, PhotoDataManager.IconUrl);
        dropTarget.TrySetActive(false);
        NetLayer.Send(new tos_microblog_feed_list(){last_id = 0, refresh = true});
    }

    private void Toc_microblog_feed_list(toc_microblog_feed_list data)
    {
        var feedList = MicroBlogDataManager.ConvertToMicroBlogDataList(data.author_list, data.list);
        m_microBlogDataList = feedList;
        m_microBlogDataGrid.Data = feedList.ToArray();

    }

    private void Toc_microblog_add_blog(toc_microblog_add_blog data)
    {
        var selfAuthorInfo = MicroBlogDataManager.GetSelfMicroBlogAuthorInfo();
        var blogData = new MicroBlogData();
        blogData.AuthorInfo = selfAuthorInfo;
        blogData.Content = data.content;
        m_microBlogDataList.Insert(0, blogData);
        m_microBlogDataGrid.Data = m_microBlogDataList.ToArray();
    }

    private void Toc_microblog_praise_blog(toc_microblog_praise_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(true, data.praise_cnt);
        }
        
    }

    private void Toc_microblog_cancel_praise(toc_microblog_cancel_praise data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdatePraise(false, data.praise_cnt);
        }
    }

    private void Toc_microblog_comment_blog(toc_microblog_comment_blog data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private void Toc_microblog_del_comment(toc_microblog_del_comment data)
    {
        var item = GetMicroBlog(data.blog_id);
        if (item != null)
        {
            item.UpdateCommentCnt(data.comment_cnt);
            item.UpdatePraise(data.praise_cnt);
        }
    }

    private void Toc_microblog_del_blog(toc_microblog_del_blog data)
    {
        for (int i = 0; i < m_microBlogDataList.Count; i++)
        {
            if (m_microBlogDataList[i].Content.id == data.blog_id)
            {
                m_microBlogDataList.RemoveAt(i);
                break;
            }
        }
        m_microBlogDataGrid.Data = m_microBlogDataList.ToArray();
    }


    private ItemMicroBlog GetMicroBlog(long blogId)
    {
        var items = m_microBlogDataGrid.ItemRenders;
        foreach (var item in items)
        {
            var microBlogData = item.m_renderData as MicroBlogData;
            if (microBlogData.Content.id == blogId)
            {
                return item as ItemMicroBlog;
            }
        }
        return null;
    }

    void OnCloseClick(GameObject target, PointerEventData data)
    {
        if (UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
        }
        if (UIManager.IsOpen<PanelPlayerZone>())
        {
            UIManager.GetPanel<PanelPlayerZone>().HidePanel();
        }
        HidePanel();
    }



    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    
    #endregion
}