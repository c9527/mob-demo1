﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelChatBig : BasePanel
{
    private GameObject m_goFrameBig;
    private GameObject m_goBigWorld;
    private GameObject m_goBigCorps;
    private GameObject m_goBigFriend;
    private GameObject m_goBigPrivate;
    private GameObject m_goBigLegion;
    private GameObject m_goBigLocal;

    private Text m_goLocalTxt;

    private ChatView m_bigWorldChatData;
    private ChatView m_bigLocalChatData;
    private ChatView m_bigCorpsChatData;
    private ChatView m_bigFriendChatData;
    private DataGrid m_bigCorpsPlayerData;
    private DataGrid m_bigFriendData;
    private DataGrid m_bigPrivateData;
    private ChatView m_bigPrivateChatData;
    private DataGrid m_bigLegionPlayerData;
    private ChatView m_bigLegionChatData;


    private InputField m_bigWorldInputField;
    private InputField m_bigLocalInputField;
    private InputField m_bigFriendInputField;
    private InputField m_bigFindFriend;
    private InputField m_bigPrivateInputField;
    private InputField m_bigCorpsInputField;
    private InputField m_bigLegionInputField;

    private PlayerRecord m_curSpeakTo = new PlayerRecord();

    private GameObject m_goNewMsgPlayerNum;
    private Text m_txtNewMsgPlayerNum;

    private GameObject m_goNewMsgPlayerNumPrivate;
    private Text m_txtNewMsgPlayerNumPrivate;

    private FRAME_TYPE m_curFrameType;

    private Toggle m_toggleWorld;
    private Toggle m_toggleFriend;
    private Toggle m_toggleCorps;
    private Toggle m_togglePrivate;
    private Toggle m_toggleLegion;
    private Toggle m_toggleLocal;

    private GameObject m_goPopListAnchor;
    private GameObject m_goPopListPanel;

    private GameObject m_w_audio_go;    
    private GameObject m_local_audio;
    private GameObject m_f_audio_go;
    private GameObject m_c_audio_go;
    private GameObject m_p_audio_go;
    private GameObject m_l_audio_go;
    private Button m_w_audio_action_btn;
    private Button m_local_audio_btn;
    private Button m_f_audio_action_btn;
    private Button m_c_audio_action_btn;
    private Button m_p_audio_action_btn;
    private Button m_l_audio_action_btn;
    private bool m_isInLegion;
    private int m_nowPage;
    private int m_maxPage;

    private GameObject m_goCorpsBubble;
    private GameObject m_goLegionBubble;
    public PanelChatBig()
    {
        SetPanelPrefabPath("UI/Chat/PanelChatBig");
        AddPreLoadRes("UI/Chat/PanelChatBig", EResType.UI);
        AddPreLoadRes("UI/Chat/ItemChatBigWorld", EResType.UI);
        AddPreLoadRes("UI/Chat/ItemChatBig", EResType.UI);
        AddPreLoadRes("UI/Chat/ItemPlayerInfoBig", EResType.UI);
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
    }

    public override void Init()
    {
        m_goLocalTxt = m_tran.Find("frame_big/ToggleLocal/Label").GetComponent<Text>();
        m_goFrameBig = m_tran.Find("frame_big").gameObject;
        m_goBigWorld = m_tran.Find("frame_big/world").gameObject;
        m_goBigLocal = m_tran.Find("frame_big/local").gameObject;
        m_goBigCorps = m_tran.Find("frame_big/corps").gameObject;
        m_goBigFriend = m_tran.Find("frame_big/friend").gameObject;
        m_goBigPrivate = m_tran.Find("frame_big/private").gameObject;
        m_goBigLegion = m_tran.Find("frame_big/Legion").gameObject;
        m_isInLegion = false;
        m_bigWorldChatData = m_tran.Find("frame_big/world/ScrollDataGrid/content").gameObject.AddComponent<ChatView>();
        m_bigLocalChatData = m_tran.Find("frame_big/local/ScrollDataGrid/content").gameObject.AddComponent<ChatView>();
        m_nowPage = 0;
        m_maxPage = 1;
        m_bigWorldInputField = m_tran.Find("frame_big/world/input/InputField").GetComponent<InputField>();
        m_bigWorldInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitWorld;

        m_bigLocalInputField = m_tran.Find("frame_big/local/input/InputField").GetComponent<InputField>();
        m_bigLocalInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitWorld;

        m_bigFriendData = m_tran.Find("frame_big/friend/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_bigFriendData.SetItemRender(ResourceManager.LoadUIRef("UI/Chat/ItemPlayerInfoBig"), typeof(ItemPlayerRecord));
        m_bigFriendData.useLoopItems = true;

        m_bigFriendChatData = m_tran.Find("frame_big/friend/ScrollDataChatInfo/content").gameObject.AddComponent<ChatView>();

        m_bigLegionPlayerData = m_tran.Find("frame_big/Legion/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_bigLegionPlayerData.SetItemRender(ResourceManager.LoadUIRef("UI/Chat/ItemPlayerInfoBig"), typeof(ItemPlayerRecord));
        m_bigLegionPlayerData.useLoopItems = true;

        m_bigLegionChatData = m_tran.Find("frame_big/Legion/ScrollDataChatInfo/content").gameObject.AddComponent<ChatView>();

        m_bigLegionInputField = m_tran.Find("frame_big/Legion/input/InputField").GetComponent<InputField>();
        m_bigLegionInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitLegion;

        m_bigFriendInputField = m_tran.Find("frame_big/friend/input/InputField").GetComponent<InputField>();
        m_bigFriendInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitFriend;
        m_bigFindFriend = m_tran.Find("frame_big/friend/find_btn/InputField").GetComponent<InputField>();
        m_bigFindFriend.gameObject.AddComponent<InputFieldFix>().onEndEdit += OnBigFindFriend;

        m_bigCorpsPlayerData = m_tran.Find("frame_big/corps/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_bigCorpsPlayerData.SetItemRender(ResourceManager.LoadUIRef("UI/Chat/ItemPlayerInfoBig"), typeof(ItemPlayerRecord));
        m_bigCorpsPlayerData.useLoopItems = true;

        m_bigCorpsChatData = m_tran.Find("frame_big/corps/ScrollDataChatInfo/content").gameObject.AddComponent<ChatView>();

        m_bigCorpsInputField = m_tran.Find("frame_big/corps/input/InputField").GetComponent<InputField>();
        m_bigCorpsInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitCorps;

        m_bigPrivateData = m_tran.Find("frame_big/private/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_bigPrivateData.SetItemRender(ResourceManager.LoadUIRef("UI/Chat/ItemPlayerInfoBig"), typeof(ItemPlayerRecord));
        m_bigPrivateData.useLoopItems = true;

        m_bigPrivateChatData = m_tran.Find("frame_big/private/ScrollDataChatInfo/content").gameObject.AddComponent<ChatView>();

        m_bigPrivateInputField = m_tran.Find("frame_big/private/input/InputField").GetComponent<InputField>();
        m_bigPrivateInputField.gameObject.AddComponent<InputFieldFix>();//.onEndEdit += OnChatSubmitPrivate;


        m_goNewMsgPlayerNum = m_tran.Find("frame_big/ToggleFriend/bubble").gameObject;
        m_txtNewMsgPlayerNum = m_tran.Find("frame_big/ToggleFriend/bubble/Text").GetComponent<Text>();
        m_goNewMsgPlayerNumPrivate = m_tran.Find("frame_big/TogglePrivate/bubble").gameObject;
        m_txtNewMsgPlayerNumPrivate = m_tran.Find("frame_big/TogglePrivate/bubble/Text").GetComponent<Text>();

        m_toggleWorld = m_tran.Find("frame_big/ToggleWorld").GetComponent<Toggle>();
        m_toggleLocal = m_tran.Find("frame_big/ToggleLocal").GetComponent<Toggle>();
        m_toggleFriend = m_tran.Find("frame_big/ToggleFriend").GetComponent<Toggle>();
        m_toggleCorps = m_tran.Find("frame_big/ToggleCorps").GetComponent<Toggle>();
        m_togglePrivate = m_tran.Find("frame_big/TogglePrivate").GetComponent<Toggle>();
        m_toggleLegion = m_tran.Find("frame_big/ToggleLegion").GetComponent<Toggle>();

        m_goCorpsBubble = m_tran.Find("frame_big/ToggleCorps/bubble").gameObject;
        m_goLegionBubble = m_tran.Find("frame_big/ToggleLegion/bubble").gameObject;

        m_goPopListAnchor = m_tran.Find("frame_big/pop_list_anchor").gameObject;
        m_goPopListPanel = m_tran.Find("frame_big/pop_list_panel").gameObject;

        m_w_audio_go = m_tran.Find("frame_big/world/input/audio").gameObject;
        m_local_audio  = m_tran.Find("frame_big/local/input/audio").gameObject;
        m_f_audio_go = m_tran.Find("frame_big/friend/input/audio").gameObject;
        m_c_audio_go = m_tran.Find("frame_big/corps/input/audio").gameObject;
        m_p_audio_go = m_tran.Find("frame_big/private/input/audio").gameObject;
        m_l_audio_go = m_tran.Find("frame_big/Legion/input/audio").gameObject;

        /*if (Driver.m_platform == EnumPlatform.ios)
        {
            m_w_audio_go.TrySetActive(false);
            m_local_audio.TrySetActive(false);
            m_f_audio_go.TrySetActive(false);
            m_c_audio_go.TrySetActive(false);
            m_p_audio_go.TrySetActive(false);
            m_l_audio_go.TrySetActive(false);
        }*/

        m_w_audio_action_btn = m_tran.Find("frame_big/local/input/audio_action_btn").GetComponent<Button>();
        m_w_audio_action_btn.gameObject.TrySetActive(false);
        m_local_audio_btn  = m_tran.Find("frame_big/world/input/audio_action_btn").GetComponent<Button>();
        m_local_audio_btn.gameObject.TrySetActive(false);
        m_f_audio_action_btn = m_tran.Find("frame_big/friend/input/audio_action_btn").GetComponent<Button>();
        m_f_audio_action_btn.gameObject.TrySetActive(false);
        m_c_audio_action_btn = m_tran.Find("frame_big/corps/input/audio_action_btn").GetComponent<Button>();
        m_c_audio_action_btn.gameObject.TrySetActive(false);
        m_p_audio_action_btn = m_tran.Find("frame_big/private/input/audio_action_btn").GetComponent<Button>();
        m_p_audio_action_btn.gameObject.TrySetActive(false);
        m_l_audio_action_btn = m_tran.Find("frame_big/Legion/input/audio_action_btn").GetComponent<Button>();
        m_l_audio_action_btn.gameObject.TrySetActive(false);



        ResetSpeakTo();
    }

    public override void InitEvent()
    {
        /*if (Driver.m_platform == EnumPlatform.ios)
        {
            UGUIClickHandler.Get(m_w_audio_go).onPointerClick += null;
            UGUIClickHandler.Get(m_local_audio).onPointerClick += null;
            UGUIClickHandler.Get(m_f_audio_go).onPointerClick += null;
            UGUIClickHandler.Get(m_c_audio_go).onPointerClick += null;
            UGUIClickHandler.Get(m_p_audio_go).onPointerClick += null;
            UGUIClickHandler.Get(m_l_audio_go).onPointerClick += null;
        }*/
        //else
        //{
            UGUIClickHandler.Get(m_w_audio_go).onPointerClick += OnAudioBtnClick;
            UGUIClickHandler.Get(m_local_audio).onPointerClick += OnAudioBtnClick;
            UGUIClickHandler.Get(m_f_audio_go).onPointerClick += OnAudioBtnClick;
            UGUIClickHandler.Get(m_c_audio_go).onPointerClick += OnAudioBtnClick;
            UGUIClickHandler.Get(m_p_audio_go).onPointerClick += OnAudioBtnClick;
            UGUIClickHandler.Get(m_l_audio_go).onPointerClick += OnAudioBtnClick;
        //}

        UGUIUpDownHandler.Get(m_w_audio_action_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_w_audio_action_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;
        UGUIUpDownHandler.Get(m_local_audio_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_local_audio_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;
        UGUIUpDownHandler.Get(m_f_audio_action_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_f_audio_action_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;
        UGUIUpDownHandler.Get(m_c_audio_action_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_c_audio_action_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;
        UGUIUpDownHandler.Get(m_p_audio_action_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_p_audio_action_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;
        UGUIUpDownHandler.Get(m_l_audio_action_btn.gameObject).onPointerUp += OnAudioActionBtnUpHandler;
        UGUIUpDownHandler.Get(m_l_audio_action_btn.gameObject).onPointerDown += OnAudioActionBtnDownHandler;

        UGUIClickHandler.Get(m_tran.Find("frame_big/shrink").gameObject).onPointerClick += OnShrink;
        UGUIClickHandler.Get(m_tran.Find("frame_big/close").gameObject).onPointerClick += OnClose;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleWorld").gameObject).onPointerClick += OnClkBigWorld;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleLocal").gameObject).onPointerClick += OnClkBigLocal;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleCorps").gameObject).onPointerClick += OnClkBigCorps;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleFriend").gameObject).onPointerClick += OnClkBigFriend;
        UGUIClickHandler.Get(m_tran.Find("frame_big/TogglePrivate").gameObject).onPointerClick += OnClkBigPrivate;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleLegion").gameObject).onPointerClick += OnClkBigLegion;

        UGUIClickHandler.Get(m_tran.Find("frame_big/world/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigWorldInputField); };
        UGUIClickHandler.Get(m_tran.Find("frame_big/local/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigLocalInputField); };
        UGUIClickHandler.Get(m_tran.Find("frame_big/friend/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigFriendInputField); };
        UGUIClickHandler.Get(m_tran.Find("frame_big/corps/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigCorpsInputField); };
        UGUIClickHandler.Get(m_tran.Find("frame_big/private/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigPrivateInputField); };
        UGUIClickHandler.Get(m_tran.Find("frame_big/Legion/input/send_back")).onPointerClick += delegate { OnChatSubmit(m_bigLegionInputField); };

        UGUIClickHandler.Get(m_tran.Find("frame_big/world/input/icon_emotion")).onPointerClick += OnShowEmotionPanel;
        UGUIClickHandler.Get(m_tran.Find("frame_big/friend/input/icon_emotion")).onPointerClick += OnShowEmotionPanel;
        UGUIClickHandler.Get(m_tran.Find("frame_big/corps/input/icon_emotion")).onPointerClick += OnShowEmotionPanel;
        UGUIClickHandler.Get(m_tran.Find("frame_big/private/input/icon_emotion")).onPointerClick += OnShowEmotionPanel;
        UGUIClickHandler.Get(m_tran.Find("frame_big/Legion/input/icon_emotion")).onPointerClick += OnShowEmotionPanel;

        AddEventListener(GameEvent.UI_UPDATE_CHAT_MSG, OnGetChatMsg);
        AddEventListener(GameEvent.UI_NEW_MSG_PLAYER_NUM, OnUpdateNewMsgPlayerNum);
        AddEventListener(GameEvent.UI_NEW_MSG_PLAYER_NUM_PRIVATE, OnUpdateNewMsgPlayerNumPrivate);
        //AddEventListener(PlayerFriendData.FRIEND_DATA_UPDATE, OnFriendUpdate);
        //AddEventListener(PlayerFriendData.ADD_FRIEND, OnFriendUpdate);
        //AddEventListener(GameEvent.UI_GUESTS_UPDATE, OnGuestsUpdate);
        AddEventListener<int, string>(GameEvent.SDK_SPEECH_COMPLETE, OnSpeechHandler);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
       AddEventListener(GameEvent.INPUT_KEY_ENTER_UP, () => {
           switch(m_curFrameType){
               case FRAME_TYPE.BIG_CORPS:
                   OnChatSubmit(m_bigCorpsInputField);
                   break;
               case FRAME_TYPE.BIG_FRIEND:
                   OnChatSubmit(m_bigFriendInputField);
                   break;
               case FRAME_TYPE.BIG_LEGION:
                   OnChatSubmit(m_bigLegionInputField);
                   break;
               case FRAME_TYPE.BIG_PRIVATE:
                   OnChatSubmit(m_bigPrivateInputField);
                   break;
               case FRAME_TYPE.BIG_WORLD:
                   OnChatSubmit(m_bigWorldInputField);
                   break;
               default:
                   break;
           }
       });
#endif
    }

    private void OnClkBigLocal(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_LOCAL);
    }

    private void OnShowEmotionPanel(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        else
        {
            UIManager.PopChatPanel<PanelChatMotion>(new object[] { target.transform.position, new Vector2(20, -20) }, false, this).MotionSelected = OnSelectedMotion; 
        }
    }

    private void OnSelectedMotion(string key)
    {
        InputField input_text = null;
        if (m_bigFriendInputField.isActiveAndEnabled)
        {
            input_text = m_bigFriendInputField;
        }
        else if (m_bigPrivateInputField.isActiveAndEnabled)
        {
            input_text = m_bigPrivateInputField;
        }
        else if (m_bigCorpsInputField.isActiveAndEnabled)
        {
            input_text = m_bigCorpsInputField;
        }
        else if (m_bigLegionInputField.isActiveAndEnabled)
        {
            input_text = m_bigLegionInputField;
        }
        else if(m_bigLocalInputField.isActiveAndEnabled)
        {
            input_text = m_bigLocalInputField;
        }
        else
        {
            input_text = m_bigWorldInputField;
        }
        input_text.text += key;
    }

    private void OnSpeechHandler(int status, string msg)
    {
        PanelSpeechSpeaker.closeSpeaker();
        if (status == 1 && !string.IsNullOrEmpty(msg))
        {
            var param_data = msg.Split('|');
            OnChatSubmit(null, param_data[0], NetChatData.loadSpeechData(), int.Parse(param_data[1]));
        }
        else
        {
            UIManager.ShowTipPanel(status == 1 ? string.Format("Speech Error: \nstatus:{0},\nmessage: {1}", status, msg) : msg);
        }
    }

    private void OnAudioActionBtnUpHandler(GameObject target, PointerEventData eventData)
    {
        PanelSpeechSpeaker.showSpeaker(1);
    }

    private void OnAudioActionBtnDownHandler(GameObject target, PointerEventData eventData)
    {
        PanelSpeechSpeaker.showSpeaker();
    }

    private void OnAudioBtnClick(GameObject target, PointerEventData eventData)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        if (SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            Button action_btn = null;
            if (target==m_w_audio_go)
            {
                action_btn = m_w_audio_action_btn;
            }
            else if(target==m_local_audio)
            {
                action_btn = m_local_audio_btn;
            }
            else if (target == m_f_audio_go)
            {
                action_btn = m_f_audio_action_btn;
            }
            else if (target == m_c_audio_go)
            {
                action_btn = m_c_audio_action_btn;
            }
            else if (target == m_p_audio_go)
            {
                action_btn = m_p_audio_action_btn;
            }
            else if (target == m_l_audio_go)
            {
                action_btn = m_l_audio_action_btn;
            }
            action_btn.gameObject.TrySetActive(!action_btn.gameObject.activeSelf);
        }
        else
        {
            UIManager.ShowNoThisFunc();
        }
    }

    public override void OnShow()
    {
        m_goLocalTxt.text = NetChatData.WorldChannelCrsSerOpen ? "本服" : "跨服";
        ShowBig(FRAME_TYPE.BIG_LOCAL);
        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();
        UpdateCorpsBubble();
        UpdateLegionBubble();
        m_nowPage = 1;
        //LegionManager.RequestLegionData();
    }
    public override void OnHide()
    {
        m_w_audio_action_btn.gameObject.TrySetActive(false);
        m_c_audio_action_btn.gameObject.TrySetActive(false);
        m_p_audio_action_btn.gameObject.TrySetActive(false);
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("frame_big/shrink").gameObject).onPointerClick -= OnShrink;
        UGUIClickHandler.Get(m_tran.Find("frame_big/close").gameObject).onPointerClick -= OnClose;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleWorld").gameObject).onPointerClick -= OnClkBigWorld;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleCorps").gameObject).onPointerClick -= OnClkBigCorps;
        UGUIClickHandler.Get(m_tran.Find("frame_big/ToggleFriend").gameObject).onPointerClick -= OnClkBigFriend;
        UGUIClickHandler.Get(m_tran.Find("frame_big/TogglePrivate").gameObject).onPointerClick -= OnClkBigPrivate;
    }
    public override void Update()
    {
        if (m_curFrameType == FRAME_TYPE.BIG_LEGION)
        {
            if (m_bigLegionPlayerData.verticalPos < -0.15f)
            {
                if (m_nowPage < m_maxPage)
                {
                    LegionManager.RequestPlayerList(++m_nowPage, false);
                }
                else
                {

                }
            }
            if (m_bigLegionPlayerData.verticalPos >= 1)
            {
                if (m_nowPage > 1)
                {
                    LegionManager.RequestPlayerList(--m_nowPage, false);
                }
                else
                {
               
                }
            }
        }
//        Logger.Log(m_bigLegionPlayerData.verticalPos.ToString());
    }


    public void OnChatSubmitWorld(string text)
    {
        OnChatSubmit(m_bigWorldInputField);
    }

    public void OnChatSubmitFriend(string text)
    {
        OnChatSubmit(m_bigFriendInputField);
    }

    public void OnChatSubmitPrivate(string text)
    {
        OnChatSubmit(m_bigPrivateInputField);
    }

    public void OnChatSubmitCorps( string text )
    {
        OnChatSubmit(m_bigCorpsInputField);
    }

    public void OnChatSubmitLegion(string text)
    {
        OnChatSubmit(m_bigLegionInputField);
    }

    public void OnChatSubmit(InputField input,string msg=null,byte[] audio_data=null,int duration=0)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        var content = input != null ? input.text : msg;
        if (string.IsNullOrEmpty(content))
        {
            return;
        }
        content = WordFiterManager.Fiter(content);
        CHAT_CHANNEL eChannel = CHAT_CHANNEL.CC_LOCALSERVER;
        long toId = 0;

        if ( m_curFrameType == FRAME_TYPE.BIG_WORLD)
        {
            eChannel =NetChatData.WorldChannelCrsSerOpen?   CHAT_CHANNEL.CC_CRSSERVER:CHAT_CHANNEL.CC_LOCALSERVER;
            m_bigWorldChatData.ResetToTop();
        }
        else if(m_curFrameType==FRAME_TYPE.BIG_LOCAL)
        {
            eChannel = NetChatData.WorldChannelCrsSerOpen ? CHAT_CHANNEL.CC_LOCALSERVER : CHAT_CHANNEL.CC_CRSSERVER;
            m_bigLocalChatData.ResetToTop();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_CORPS)
        {
            eChannel = CHAT_CHANNEL.CC_CORPS;
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_LEGION)
        {
            eChannel = CHAT_CHANNEL.CC_LEGION;
        }
        else
        {
            toId = m_curSpeakTo.m_iPlayerId;
            if( toId == 0 )
            {
                TipsManager.Instance.showTips("玩家不存在!");
                return;
            }
            List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
            ChatRecord newRcd = new ChatRecord();
            newRcd.m_iHead = PlayerSystem.roleData.icon;
            newRcd.m_strName = PlayerSystem.roleData.name;
            newRcd.m_strChatInfo = content;
            newRcd.m_audio = new p_audio_chat() { audio_id = audio_data != null ? int.MaxValue : 0, content = audio_data, duration = duration };
            newRcd.m_isMyself = true;
            newRcd.m_iSex = PlayerSystem.roleData.sex;
            if (MemberManager.IsHighMember())
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_HIGH;
            else if(MemberManager.IsMember())
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_NORMAL;
            else
                newRcd.m_eMrbType = MemberManager.MEMBER_TYPE.MEMBER_NONE;

            if ( m_curFrameType == FRAME_TYPE.BIG_FRIEND)
            {
                eChannel = CHAT_CHANNEL.CC_FRIEND;
              
                newRcd.m_eChannel = CHAT_CHANNEL.CC_FRIEND;         
                lstRcds.Insert(0, newRcd);
                ShowBigChat();
            }
            else if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE )
            {
                eChannel = CHAT_CHANNEL.CC_PRIVATE;
                toId = m_curSpeakTo.m_iPlayerId;          
                newRcd.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;              
                lstRcds.Insert(0, newRcd);
                NetChatData.AddGuest(m_curSpeakTo);
                ShowBigChat();
            }
        }
       
        NetChatData.SndChatInfo((int)eChannel, toId, content, audio_data, duration);
        if (input != null)
        {
            input.text = "";
        }
    }


    protected void OnShrink(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        UIManager.PopChatPanel<PanelChatSmall>();
    }

    public void OnClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    protected void ShowBig(FRAME_TYPE eType)
    {
        if (UIManager.IsOpen<PanelChatMotion>())
        {
            UIManager.GetPanel<PanelChatMotion>().HidePanel();
        }
        FRAME_TYPE oldType = m_curFrameType;

        m_curFrameType = eType;
        if (m_curFrameType < FRAME_TYPE.BIG_WORLD)
            return;

        ResetSpeakTo();
        m_toggleWorld.isOn = m_curFrameType == FRAME_TYPE.BIG_WORLD;
        m_toggleCorps.isOn = m_curFrameType == FRAME_TYPE.BIG_CORPS;
        m_toggleFriend.isOn = m_curFrameType == FRAME_TYPE.BIG_FRIEND;
        m_togglePrivate.isOn = m_curFrameType == FRAME_TYPE.BIG_PRIVATE;
        m_toggleLegion.isOn = m_curFrameType == FRAME_TYPE.BIG_LEGION;
        m_toggleLocal.isOn = m_curFrameType == FRAME_TYPE.BIG_LOCAL;

        m_goBigWorld.TrySetActive(false);
        m_goBigLocal.TrySetActive(false);
        m_goBigCorps.TrySetActive(false);
        m_goBigFriend.TrySetActive(false);
        m_goBigPrivate.TrySetActive(false);
        m_goBigLegion.TrySetActive(false);

        if (m_curFrameType == FRAME_TYPE.BIG_WORLD)
        {
            m_goBigWorld.TrySetActive(true);
            //m_bigWorldChatData.Data = NetChatData.m_lstWorldChatRcds.ToArray();
        }
        else if(m_curFrameType==FRAME_TYPE.BIG_LOCAL)
        {
            m_goBigLocal.TrySetActive(true);
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_CORPS)
        {
            List<PlayerRecord> lstRcds = CorpsDataManager.GetCorpsMember();
            if (lstRcds.Count == 0)
            {
                TipsManager.Instance.showTips("你还没有战队");
                ShowBig(oldType);
                return;
            }
            m_goBigCorps.TrySetActive(true);
            m_bigCorpsChatData.ReShowAllItems(NetChatData.m_lstCorpsChatRcds.ToArray());
            m_bigCorpsPlayerData.Data = lstRcds.ToArray();
            UpdateCorpsBubble();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_FRIEND)
        {
            m_goBigFriend.TrySetActive(true);
            //NetChatData.FlushFriends();
            m_bigFriendData.Data = NetChatData.m_lstAllFriends.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE)
        {
            m_goBigPrivate.TrySetActive(true);
            //NetChatData.FlushGuests();
            m_bigPrivateData.Data = NetChatData.m_lstGuests.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_LEGION)
        {
            //if (!LegionManager.IsJoinLegion())
            //{
            //    TipsManager.Instance.showTips("你还没有加入军团");
            //    ShowBig(oldType);
            //    return;
            //}
            m_goBigLegion.TrySetActive(true);
            //LegionManager.RequestPlayerList(1, false);
            UpdateLegionBubble();
        }
        ShowBigChat();
        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();
    }

    protected void ShowBigChat()
    {
        if (m_curFrameType == FRAME_TYPE.BIG_WORLD)
        {
            ChatRecord[] arr = NetChatData.WorldChannelCrsSerOpen ? NetChatData.m_lstCrsServerChatRcds.ToArray() : NetChatData.m_lstLocalServerChatRcds.ToArray();
            m_bigWorldChatData.ReShowAllItems(arr);
        }
        else if(m_curFrameType==FRAME_TYPE.BIG_LOCAL)
        {
            ChatRecord[] arr = NetChatData.WorldChannelCrsSerOpen ? NetChatData.m_lstLocalServerChatRcds.ToArray() : NetChatData.m_lstCrsServerChatRcds.ToArray();
            m_bigLocalChatData.ReShowAllItems(arr);
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_CORPS)
        {
            m_bigCorpsChatData.ReShowAllItems(NetChatData.m_lstCorpsChatRcds.ToArray());
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_FRIEND)
        {
            List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
            m_bigFriendChatData.ReShowAllItems(lstRcds.ToArray());
        }
        else if( m_curFrameType == FRAME_TYPE.BIG_PRIVATE )
        {
            List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId);
            m_bigPrivateChatData.ReShowAllItems(lstRcds.ToArray());
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_LEGION)
        {
            m_bigLegionChatData.ReShowAllItems(NetChatData.m_lstLegionChatRcds.ToArray());
        }

        UpdateCorpsBubble();
        UpdateLegionBubble();
    }

    protected void OnClkBigWorld(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_WORLD);
    }

    protected void OnClkBigCorps(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_CORPS);
    }

    protected void OnClkBigFriend(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_FRIEND);
    }

    protected void OnClkBigPrivate(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_PRIVATE);
    }

    protected void OnClkBigLegion(GameObject target, PointerEventData eventData)
    {
        ShowBig(FRAME_TYPE.BIG_LEGION);
    }

    /*
    private void DecMsgNum(long id, bool isFriend)
    {
        if (isFriend)
        {
            int idx = NetChatData.m_lstHasMsgPlayerId.IndexOf(id);
            if (idx > -1)
            {
                NetChatData.m_lstHasMsgPlayerId.RemoveAt(idx);
            }
        }
        else
        {
            int idx = NetChatData.m_lstHasMsgPlayerIdPrivate.IndexOf(id);
            if (idx > -1)
            {
                NetChatData.m_lstHasMsgPlayerIdPrivate.RemoveAt(idx);
            }
        }
    }

     */
     
    public void SwitchPersonChat(PlayerRecord rcd)
    {
        /*
        m_curSpeakTo.m_iPlayerId = iSpkToId;
        m_curSpeakTo.m_strName = strName;
        m_goNewMsgPlayerNum.TrySetActive(false);
        int idx = NetChatData.m_lstHasMsgPlayerId.IndexOf(iSpkToId);
        if (idx > -1)
        {
            NetChatData.m_lstHasMsgPlayerId.Remove(idx);
        }

        if (!NetChatData.m_dicPrivateChatRcds.ContainsKey(iSpkToId))
        {
            NetChatData.m_dicPrivateChatRcds[iSpkToId] = new List<ChatRecord>();
        }
        List<ChatRecord> lstRcds = NetChatData.m_dicPrivateChatRcds[iSpkToId];

       
        if (m_curFrameType == FRAME_TYPE.BIG_CORPS || m_curFrameType == FRAME_TYPE.BIG_FRIEND)
        {
            if (m_curFrameType == FRAME_TYPE.BIG_CORPS)
            {
                m_tran.Find("frame_big/ToggleCorps").GetComponent<Toggle>().isOn = false;
                m_tran.Find("frame_big/ToggleFriend").GetComponent<Toggle>().isOn = true;

            }
           
            m_bigFriendChatData.Data = lstRcds.ToArray();
            m_tran.Find("frame_big/ToggleFriend/Label").GetComponent<Text>().text = strName;
        }
         * */

        if (m_curFrameType == FRAME_TYPE.BIG_CORPS||m_curFrameType == FRAME_TYPE.BIG_LEGION)
            return;
        
        if (rcd.m_isFriend)
        {
            m_goNewMsgPlayerNum.TrySetActive(false);
            //DecMsgNum(rcd.m_iPlayerId, true);
            NetChatData.DecNewMsgId(rcd.m_iPlayerId, true);
            OnUpdateNewMsgPlayerNum();
            ShowBig(FRAME_TYPE.BIG_FRIEND);
        }
        else
        {
            m_goNewMsgPlayerNumPrivate.TrySetActive(false);
            //DecMsgNum(rcd.m_iPlayerId, false);
            NetChatData.DecNewMsgId(rcd.m_iPlayerId, false);
            OnUpdateNewMsgPlayerNumPrivate();
            ShowBig(FRAME_TYPE.BIG_PRIVATE);
        }

        m_curSpeakTo.Clone(rcd);
        List<ChatRecord> lstRcds = NetChatData.GetPersonChatRcds(m_curSpeakTo.m_iPlayerId); ;


        if (m_curSpeakTo.m_isFriend)
        {
            m_tran.Find("frame_big/ToggleFriend/Label").GetComponent<Text>().text = m_curSpeakTo.m_strName;
            m_bigFriendChatData.ReShowAllItems(lstRcds.ToArray());    
        }
        else
        {
            m_tran.Find("frame_big/TogglePrivate/Label").GetComponent<Text>().text = m_curSpeakTo.m_strName;
            m_bigPrivateChatData.ReShowAllItems(lstRcds.ToArray());
        }
          
    }
   
  


    protected void OnGetChatMsg( )
    {
        /*
        if (m_curFrameType == FRAME_TYPE.BIG_WORLD)
        {
            m_bigWorldChatData.Data = NetChatData.m_lstWorldChatRcds.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_CORPS)
        {
            m_bigCorpsChatData.Data = NetChatData.m_lstCorpsChatRcds.ToArray();
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_FRIEND)
        {

        }
        */
        if (!IsOpen())
            return;
        ShowBigChat();
    }

    public void OnBigFindFriend(string text)
    {
        OnFindFriend(m_bigFindFriend);
    }

    public void OnFindFriend(InputField input)
    {
        string name = input.textComponent.text;
        if (name == "")
            return;
       // List<FriendData> dataList = PlayerFriendData.singleton.friendList;
        for (int i = 0; i < NetChatData.m_lstAllFriends.Count; ++i)
        {
            PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
            //FriendData syncData = dataList[i];
            if (rcd.m_strName == name)
            {
                if (rcd.m_bOnline)
                {
                    SwitchPersonChat(rcd);

                }
                else
                {
                    UIManager.ShowTipPanel("离线不可以聊天");
                }
                return;
            }
        }

        UIManager.ShowTipPanel("该用户不是好友");
    }

    protected void ResetSpeakTo()
    {
        //m_curSpeakTo.m_iPlayerId = 0;
        //m_curSpeakTo.m_strName = "";
        m_curSpeakTo.Reset();

        m_tran.Find("frame_big/ToggleFriend/Label").GetComponent<Text>().text = "好友";
        m_tran.Find("frame_big/TogglePrivate/Label").GetComponent<Text>().text = "私聊";
    }

    protected void OnUpdateNewMsgPlayerNum()
    {
        if (!IsOpen())
            return;

        int num = NetChatData.m_lstHasMsgPlayerId.Count;
        if (num == 0)
        {
            m_goNewMsgPlayerNum.TrySetActive(false);
            m_txtNewMsgPlayerNum.text = "";

            if (m_curFrameType == FRAME_TYPE.BIG_FRIEND)
            {
                //DecMsgNum(m_curSpeakTo.m_iPlayerId, true);
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, true);
                //NetChatData.FlushFriends();
                m_bigFriendData.Data = NetChatData.m_lstAllFriends.ToArray();
            }

        }
        else
        {
            if (m_curFrameType == FRAME_TYPE.BIG_FRIEND)
            {
                m_goNewMsgPlayerNum.TrySetActive(false);
                m_txtNewMsgPlayerNum.text = "";
                //DecMsgNum(m_curSpeakTo.m_iPlayerId, true);
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, true);
                //NetChatData.FlushFriends();
                m_bigFriendData.Data = NetChatData.m_lstAllFriends.ToArray();
            }
            else
            {
                m_goNewMsgPlayerNum.TrySetActive(true);
                m_txtNewMsgPlayerNum.text = num.ToString();
            }
        }
    }

    protected void OnUpdateNewMsgPlayerNumPrivate()
    {
        if (!IsOpen())
            return;

        int num = NetChatData.m_lstHasMsgPlayerIdPrivate.Count;
        if(num == 0)
        {
            m_goNewMsgPlayerNumPrivate.TrySetActive(false);
            m_txtNewMsgPlayerNumPrivate.text = "";

            if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE)
            {
                //DecMsgNum(m_curSpeakTo.m_iPlayerId, false);
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, false);
                //NetChatData.FlushGuests();
                m_bigPrivateData.Data = NetChatData.m_lstGuests.ToArray();
            }
        }
        else
        {
            if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE)
            {
                m_goNewMsgPlayerNumPrivate.TrySetActive(false);
                m_txtNewMsgPlayerNumPrivate.text = "";
                //DecMsgNum(m_curSpeakTo.m_iPlayerId, false);    
                NetChatData.DecNewMsgId(m_curSpeakTo.m_iPlayerId, false);
                //NetChatData.FlushGuests();
                m_bigPrivateData.Data = NetChatData.m_lstGuests.ToArray();
            }
            else
            {
                m_goNewMsgPlayerNumPrivate.TrySetActive(true);
                m_txtNewMsgPlayerNumPrivate.text = num.ToString();
            }
        }

    }

    public GameObject GetPopListAnchor()
    {
        return m_goPopListAnchor;
    }

    public GameObject GetPopListPanel()
    {
        return m_goPopListPanel;
    }
    public RectTransform GetTrans()
    {
        return m_tran;
    }

    protected void OnFriendUpdate()
    {
        if (!IsOpen())
            return;

        //NetChatData.FlushFriends();

        if (m_curFrameType == FRAME_TYPE.BIG_FRIEND /* && m_curSpeakTo.m_iPlayerId == 0 */ )
        {
            //m_bigFriendData.Data = NetChatData.m_lstAllFriends.ToArray();
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstAllFriends.Count; i++)
                {
                    PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);

                        m_curSpeakTo.Reset();
                        m_tran.Find("frame_big/ToggleFriend/Label").GetComponent<Text>().text = "好友";
                        //m_tran.Find("frame_big/TogglePrivate/Label").GetComponent<Text>().text = "私聊";
                        ShowBigChat();
                        break;
                    }
                }
            }
        } 
    }

    protected void OnGuestsUpdate()
    {
        if (!IsOpen())
            return;

        if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE)
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstGuests.Count; ++i)
                {
                    PlayerRecord rcd = NetChatData.m_lstGuests[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);

                        m_curSpeakTo.Reset();
                        m_tran.Find("frame_big/TogglePrivate/Label").GetComponent<Text>().text = "私聊";
                        ShowBigChat();
                        break;
                    }
                }
            }
            ShowBig(FRAME_TYPE.BIG_PRIVATE);
        }
    }

    void Toc_legion_get_member_list(toc_legion_get_member_list data)
    {
        if (LegionManager.m_iswork)
        {
            return;
        }
        NetChatData.SetLegionList(data.list);
        m_maxPage = data.pages;
        //m_nowPage = data.page;
        m_bigLegionPlayerData.Data = NetChatData.m_lstAllLegionMember.ToArray();
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data,UIManager.CurContentPanel  }, true, this, LayerType.Chat);
        }
    }

    private void UpdateCorpsBubble()
    {
        if (NetChatData.m_bHasCorpsChatMsg == false || m_curFrameType == FRAME_TYPE.BIG_CORPS)
        {
            m_goCorpsBubble.TrySetActive(false);
            NetChatData.m_bHasCorpsChatMsg = false;
        }
        else
        {
            m_goCorpsBubble.TrySetActive(true);
        }
    }

    private void UpdateLegionBubble()
    {
        if (NetChatData.m_bHasLegionChatMsg == false || m_curFrameType == FRAME_TYPE.BIG_LEGION)
        {
            m_goLegionBubble.TrySetActive(false);
            NetChatData.m_bHasLegionChatMsg = false;
        }
        else
        {
            m_goLegionBubble.TrySetActive(true);
        }
    }


    public void UpdateView()
    {
        OnUpdateNewMsgPlayerNum();
        OnUpdateNewMsgPlayerNumPrivate();

        if (m_curFrameType == FRAME_TYPE.BIG_FRIEND )
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstAllFriends.Count; i++)
                {
                    PlayerRecord rcd = NetChatData.m_lstAllFriends[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);

                        m_curSpeakTo.Reset();
                        m_tran.Find("frame_big/ToggleFriend/Label").GetComponent<Text>().text = "好友";
                        ShowBigChat();
                        break;
                    }
                }
            }
        }
        else if (m_curFrameType == FRAME_TYPE.BIG_PRIVATE)
        {
            if (m_curSpeakTo.m_iPlayerId != 0)
            {
                for (int i = 0; i < NetChatData.m_lstGuests.Count; ++i)
                {
                    PlayerRecord rcd = NetChatData.m_lstGuests[i];
                    if (!rcd.m_bOnline && rcd.m_iPlayerId == m_curSpeakTo.m_iPlayerId)
                    {
                        string tips = rcd.m_strName + "已经下线了!";
                        TipsManager.Instance.showTips(tips);

                        m_curSpeakTo.Reset();
                        m_tran.Find("frame_big/TogglePrivate/Label").GetComponent<Text>().text = "私聊";
                        ShowBigChat();
                        break;
                    }
                }
            }
            ShowBig(FRAME_TYPE.BIG_PRIVATE);
        }
    }

}
