﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ItemPlayerRecord : ItemRender
{
    private PlayerRecord m_data;// = new PlayerRecord();
    private Text m_txtPlayerName;
    //private Image m_imgSex;
    private Text m_txtOnlineStat;
    private GameObject m_goBackBlue;
    private GameObject m_goBackRed;
    private Image m_imgBack;
    private Image m_imgLine1;
    private Image m_imgLine2;
    private Image m_imgRank;
    private Image m_imgHead;
    private Text m_txtLevel;
    private ItemDropInfo[] m_lstOperation;
    private GameObject m_goPopListAnchor;
    private GameObject m_goPopListPanel;
    private RectTransform m_tranParent;

    public override void Awake()
    {
        m_txtPlayerName = transform.Find("name").GetComponent<Text>();
        m_txtOnlineStat = transform.Find("online_stat").GetComponent<Text>();
        m_goBackBlue = transform.Find("back").gameObject;
        m_goBackRed = transform.Find("back2").gameObject;
        m_imgBack = m_goBackBlue.GetComponent<Image>();
        m_imgLine1 = transform.Find("line1").GetComponent<Image>();
        m_imgLine2 = transform.Find("line2").GetComponent<Image>();
        m_imgRank = transform.Find("rank").GetComponent<Image>();
        m_txtLevel = transform.Find("rank/level_back/level").GetComponent<Text>();
        m_imgHead = transform.Find("head").GetComponent<Image>();
        //m_imgSex = transform.Find("head").GetComponent<Image>();

        m_lstOperation = new[]
        {
             new ItemDropInfo { type = "CHAT", subtype = "FOLLOW", label = " 跟随"},
             new ItemDropInfo { type = "CHAT", subtype = "BLACK", label = " 拉黑"}
        };

        PanelChatSmall panelSmall = UIManager.GetPanel<PanelChatSmall>();
        if (panelSmall != null && panelSmall.IsOpen())
        {
            m_goPopListAnchor = panelSmall.GetPopListAnchor();
            m_tranParent = panelSmall.GetTrans();
            m_goPopListPanel = panelSmall.GetPopListPanel();
            //UIManager.ShowDropList(m_lstOperation, OnOpListItemSelected, panelSmall.GetTrans(), panelSmall.GetPopListAnchor(), 220, 30, false);
            //return;
        }

        PanelChatBig panelBig = UIManager.GetPanel<PanelChatBig>();
        if (panelBig != null && panelBig.IsOpen())
        {
            m_goPopListAnchor = panelBig.GetPopListAnchor();
            m_tranParent = panelBig.GetTrans();
            m_goPopListPanel = panelBig.GetPopListPanel();
            //UIManager.ShowDropList(m_lstOperation, OnOpListItemSelected, panelBig.GetTrans(), panelBig.GetPopListAnchor(), 220, 30, false);
            //return;
        }

        UGUIClickHandler.Get(gameObject).onPointerClick += OnClickChat;
        GameObject goInfo = transform.Find("info").gameObject;
        UGUIClickHandler.Get(goInfo ).onPointerClick += OnClickOplist;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as PlayerRecord;
        //m_data.Clone( data as PlayerRecord );
        m_txtPlayerName.text = m_data.m_strName;

        /* if (m_data.m_iSex == PlayerConst.SEX_MALE)
        {
            m_imgSex.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
        }
        else if (m_data.m_iSex == PlayerConst.SEX_FEMALE)
        {
            m_imgSex.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
        }
        */

        if(m_data.m_bOnline)
        {
            m_txtOnlineStat.text = string.Format("<color='#00FF00'>{0}</color>", "在线");
            m_imgBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            m_imgLine1.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "blue_line_v"));
            m_imgLine2.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "blue_line_v"));
        }
        else
        {
            m_txtOnlineStat.text = string.Format("<color='#A0A0A0'>{0}</color>", "离线");
            m_imgBack.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            m_imgLine1.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "gray_line_v"));
            m_imgLine2.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "gray_line_v"));
            //Util.SetGrayShader(m_imgBack);
        }

        m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(m_data.m_iHead));
        m_imgHead.SetNativeSize();

        m_imgRank.SetSprite(ResourceManager.LoadArmyIcon(m_data.m_iLevel));
        //m_imgRank.SetNativeSize();
        m_txtLevel.text = String.Format("{0}级",  m_data.m_iLevel);
        if(m_data.m_bHasMsg)
        {
            m_goBackBlue.TrySetActive(false);
            m_goBackRed.TrySetActive(true);
        }
        else
        {
            m_goBackBlue.TrySetActive(true);
            m_goBackRed.TrySetActive(false);
        }
    }

    protected void OnClickChat(GameObject target, PointerEventData eventData)
    {
        //if(!m_data.m_bOnline)
        //{
        //    UIManager.ShowTipPanel("离线不可以聊天");
        //    return;
       // }
        PanelChatSmall pPanelSmall = UIManager.GetPanel<PanelChatSmall>();
        if (pPanelSmall != null && pPanelSmall.IsOpen() )
        {
            pPanelSmall.SwitchPersonChat(m_data);
            return;
        }

        PanelChatBig pPanelBig = UIManager.GetPanel<PanelChatBig>();
        if(pPanelBig != null && pPanelBig.IsOpen() )
        {
            pPanelBig.SwitchPersonChat(m_data);
            return;
        }
    }

    protected void OnClickOplist( GameObject target, PointerEventData eventData )
    {
        //UIManager.PopPanel<PanelOpList>();

        if (!m_data.m_bOnline || m_data.m_iPlayerId==0)
            return;

        UIManager.ShowDropList(m_lstOperation, OnOpListItemSelected, m_tranParent, m_goPopListAnchor, 220, 30, UIManager.DropListPattern.Ex);
        m_goPopListPanel.TrySetActive(true);
        UGUIClickHandler.Get(m_goPopListPanel).onPointerClick += OnClickBeyoundPopList;
    }

    private void OnOpListItemSelected(ItemDropInfo data)
    {
        if (data.subtype == "FOLLOW")
        {
            RoomModel.FollowFriend(m_data.m_iPlayerId, false, false);
        }
        else if (data.subtype == "BLACK")
        {
            NetChatData.AddBlack(m_data.m_iPlayerId);
        }
    }

    void OnClickBeyoundPopList(GameObject target, PointerEventData eventdata)
    {
        UGUIClickHandler.Get(m_goPopListPanel).onPointerClick -= OnClickBeyoundPopList;
        m_goPopListPanel.TrySetActive(false);
        UIManager.HideDropList();
    }
}
