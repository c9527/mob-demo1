﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;


public enum FRAME_TYPE
{
    /// <summary>
    /// 界面显示为世界，实际可能是本服或者跨服
    /// </summary>
    SMALL_WORLD = 0,
    SMALL_CORPS = 1,
    SMALL_FRIEND,
    SMALL_PRIVATE,
    SMALL_LEGION,
    SMALL_MICROBLOG,
    /// <summary>
    /// 界面显示为本服或者跨服
    /// </summary>
    SMALL_LOCAL,
    BIG_WORLD,
    BIG_CORPS,
    BIG_FRIEND,
    BIG_PRIVATE,
    BIG_LEGION,
    BIG_LOCAL,
}

public class ChatRecord
{
    public long m_iPlayerId;
    public int m_iHead;
    public string m_strName;
    public string m_strChatInfo;
    public p_audio_chat m_audio;
    public bool m_isMyself = false;
    public CHAT_CHANNEL m_eChannel;
    public PlayerRecord m_playerRcd;
    public int m_iSex;
    public MemberManager.MEMBER_TYPE m_eMrbType;
    public string m_area = "";
    public int m_heroType;
}

public class PlayerRecord
{
    public void Reset()
    {
        m_iPlayerId = 0;
        m_strName = "";
        m_bOnline = false;
        m_iSex = 0;
        m_bHasMsg = false;
        m_iLevel = 0;
        m_iHead = 0;
        m_isFriend = false;
        m_bHasOfflineMsg = false;
        m_area = "";
    }

    public void Clone(PlayerRecord rcd)
    {
        m_iPlayerId = rcd.m_iPlayerId;
        m_strName = rcd.m_strName;
        m_bOnline = rcd.m_bOnline;
        m_iSex = rcd.m_iSex;
        m_bHasMsg = rcd.m_bHasMsg;
        m_iLevel = rcd.m_iLevel;
        m_iHead = rcd.m_iHead;
        m_isFriend = rcd.m_isFriend;
        m_bHasOfflineMsg = rcd.m_bHasOfflineMsg;
        m_area = rcd.m_area;
    }
    public long m_iPlayerId;
    public string m_strName;
    public bool m_bOnline;
    public int m_iSex;
    public bool m_bHasMsg;
    public int m_iLevel;
    public int m_iHead;
    public bool m_isFriend = false;
    public bool m_bHasOfflineMsg = false;
    public string m_area = "";
}

public enum CHAT_CHANNEL
{
    /// <summary>
    /// 本服
    /// </summary>
    CC_LOCALSERVER = 1,
    CC_CORPS = 2,
    CC_FRIEND = 3,
    CC_PRIVATE = 4,
    CC_LEGION = 7,
    CC_LABA = 8,
    /// <summary>
    /// 跨服
    /// </summary>
    CC_CRSSERVER = 9,
}

public class ChatMsgTips
{
    public CHAT_CHANNEL m_eChannel;
    public string m_strName;
    public string m_strChatMsg;
    public PlayerRecord m_rcd;
    public bool m_is_audio;
    public string m_area;
    public int m_heroType;
}

class NetChatData
{
    public static string SPEECH_DATA_URL = UpdateWorker.SdRootPath + "speech_r_audio.amr";
    public static string SPEECH_PLAY_URL = UpdateWorker.SdRootPath + "speech_p_audio.amr";
    public static List<ChatRecord> m_lstLocalServerChatRcds = new List<ChatRecord>();
    public static List<ChatRecord> m_lstCrsServerChatRcds = new List<ChatRecord>();
    public static List<ChatRecord> m_lstCorpsChatRcds = new List<ChatRecord>();
    public static List<ChatRecord> m_lstLegionChatRcds = new List<ChatRecord>();
    public static Dictionary<long, List<ChatRecord>> m_dicPrivateChatRcds = new Dictionary<long, List<ChatRecord>>();
    public static List<long> m_lstHasMsgPlayerId = new List<long>();
    public static List<long> m_lstHasMsgPlayerIdPrivate = new List<long>();
    public static List<PlayerRecord> m_lstAllLegionMember = new List<PlayerRecord>();
    public static List<PlayerRecord> m_lstAllFriends = new List<PlayerRecord>();
    public static List<PlayerRecord> m_lstGuests = new List<PlayerRecord>();
    public static bool WorldChannelCrsSerOpen
    {
        get
        {
            return ConfigManager.GetConfig<ConfigMisc>().GetLine("worldChannel_CrsSer_Open").ValueInt == 1;
        }
    }
    public static List<long> m_lstBlack = new List<long>();
    private static int WORLD_CHAT_NUM_MAX = 500;
    private static int PERSON_CHAT_NUM_MAX = 300;
    private static int m_plaing_audio_id;
    private static Action m_speech_player_complete_handler = null;
    public static bool m_bHasCorpsChatMsg = false;
    public static bool m_bHasLegionChatMsg = false;
    private static byte[] m_sending_audio_data;

    private static int _CHAT_LVL_LOWER = -1;

    static public int Get_lower_lvl()
    {
        return _CHAT_LVL_LOWER;
    }

    static public void HandleLinkAction(string url)
    {
        var url_data = !string.IsNullOrEmpty(url) ? url.Split('|') : null;
        if (url_data == null || url_data.Length == 0)
        {
            return;
        }
        if (url_data[0] == "apply_trainee")
        {
            if (url_data.Length > 1)
            {
                NetLayer.Send(new tos_master_add_master() { id = long.Parse(url_data[1]) });
            }
        }
        else if (url_data[0] == "apply_corps")
        {
            var corps_id = url_data.Length > 1 ? long.Parse(url_data[1]) : 0;
            if (corps_id > 0)
            {
                if (CorpsDataManager.m_corpsData.corps_id == corps_id)
                {
                    UIManager.ShowTipPanel("你已经是该战队成员了");
                }
                else if (CorpsDataManager.m_corpsData.corps_id != 0)
                {
                    UIManager.ShowTipPanel("请先退出战队");
                }
                else
                {
                    NetLayer.Send(new tos_corps_apply_enter() { corps_id = long.Parse(url_data[1]) });
                }
            }
        }
        else if (url_data[0] == "apply_battle")
        {
            var channel = int.Parse(url_data[1]);
            var room = int.Parse(url_data[2]);
            if (channel > 0 && room > 0)
            {
                RoomModel.JoinRoom(channel, room, url_data.Length > 3 && url_data[3].ToLower() == "true", false);
            }
            else
            {
                UIManager.ShowTipPanel("无效的房间！");
            }
        }
        else if (url_data[0] == "link")
        {
            if (Driver.m_platform == EnumPlatform.web)
            {
                PlatformAPI.Call("openURL", url_data[1], "_blank");
            }
            else
            {
                Application.OpenURL(url_data[1]);
            }
        }
    }

    static public void FlushOfflineMsg(long read_uid = 0)
    {
        if (read_uid > 0)
        {
            var success_read = false;
            var player = m_lstAllFriends.Find(vo => vo.m_iPlayerId == read_uid);
            if (player != null && player.m_bHasOfflineMsg)
            {
                player.m_bHasOfflineMsg = false;
                success_read = true;
            }
            player = m_lstGuests.Find(vo => vo.m_iPlayerId == read_uid);
            if (player != null && player.m_bHasOfflineMsg)
            {
                player.m_bHasOfflineMsg = false;
                success_read = true;
            }
            if (success_read == true)
            {
                NetLayer.Send(new tos_player_comfirm_offline_msg() { player_id = read_uid });
            }
        }
        var chat_bubble = m_lstHasMsgPlayerId.Count > 0 || m_lstHasMsgPlayerIdPrivate.Count > 0;
        if (BubbleManager.HasBubble(BubbleConst.ChatOfflineMsg) != chat_bubble)
        {
            BubbleManager.SetBubble(BubbleConst.ChatOfflineMsg, chat_bubble);
        }
    }

    static protected void OnFriendUpdate()
    {
        FlushFriends();
        PanelChatSmall panel = UIManager.GetPanel<PanelChatSmall>();
        if (panel != null && panel.IsOpen())
        {
            panel.UpdateView();
        }
    }
    static public void ResetData()
    {
        GameDispatcher.RemoveEventListener(PlayerFriendData.FRIEND_DATA_UPDATE, OnFriendUpdate);
        GameDispatcher.RemoveEventListener(PlayerFriendData.ADD_FRIEND, OnFriendUpdate);
        GameDispatcher.AddEventListener(PlayerFriendData.FRIEND_DATA_UPDATE, OnFriendUpdate);
        GameDispatcher.AddEventListener(PlayerFriendData.ADD_FRIEND, OnFriendUpdate);

        m_lstLocalServerChatRcds.Clear();
        m_lstCrsServerChatRcds.Clear();
        m_lstCorpsChatRcds.Clear();
        m_lstLegionChatRcds.Clear();
        m_dicPrivateChatRcds.Clear();
        m_lstHasMsgPlayerId.Clear();
        m_lstHasMsgPlayerIdPrivate.Clear();
        m_lstAllLegionMember.Clear();
        m_lstAllFriends.Clear();
        m_lstGuests.Clear();
        m_lstBlack.Clear();
    }

    static public void CacheSpeechData(byte[] data)
    {
        m_sending_audio_data = data;
    }

    static public byte[] loadSpeechData()
    {
        return m_sending_audio_data;
    }

    static public void playSpeech(int uid, Action handler = null, byte[] data = null, int duration = 0)
    {
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            return;
        }
        stopSpeech(true);
        m_plaing_audio_id = uid;
        m_speech_player_complete_handler = handler;
        if (data != null && data.Length > 0)
        {
            m_plaing_audio_id = int.MaxValue;
            Toc_player_audio(new toc_player_audio() { audio_chat = new p_audio_chat() { audio_id = m_plaing_audio_id, content = data, duration = duration } });
        }
        else
        {
            NetLayer.Send(new tos_player_audio() { audio_id = m_plaing_audio_id });
        }
    }

    private static void Toc_player_audio(toc_player_audio data)
    {
        if (data.audio_chat.audio_id == 0)
        {
            OnSpeechPlayCompleted();
            TipsManager.Instance.showTips("找不到该语音");
            return;
        }
        if (m_plaing_audio_id == 0 || m_plaing_audio_id != data.audio_chat.audio_id)
        {
            return;
        }
        if (data.audio_chat.audio_id > 0)
        {
            AudioManager.MusicMute = true;
            SdkManager.PlaySpeech(data.audio_chat.content);
            TimerManager.SetTimeOut(data.audio_chat.duration + 1f, OnSpeechPlayCompleted);
        }
    }

    private static void OnSpeechPlayCompleted()
    {
        TimerManager.RemoveTimeOut(OnSpeechPlayCompleted);
        m_plaing_audio_id = 0;
        AudioManager.MusicMute = false;
        if (m_speech_player_complete_handler != null)
        {
            var complete_handler = m_speech_player_complete_handler;
            m_speech_player_complete_handler = null;
            complete_handler.Invoke();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fire_event">是否执行回调</param>
    static public void stopSpeech(bool fire_event = false)
    {
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            return;
        }
        SdkManager.StopSpeech();
        if (fire_event == false)
        {
            m_speech_player_complete_handler = null;
        }
        OnSpeechPlayCompleted();
    }

    public static void MockChatMsg(string msg, CHAT_CHANNEL channel = CHAT_CHANNEL.CC_LOCALSERVER, string sender = null)
    {
        Toc_player_chat_msg(new toc_player_chat_msg() { channel = Math.Max((int)channel, 1), from = 0, icon = 8, name = !string.IsNullOrEmpty(sender) ? sender : "系统", msg = msg });
    }

    static void Toc_player_chat_msg(toc_player_chat_msg proto)
    {
        if (m_lstBlack.IndexOf(proto.from) > -1)
            return;

        ChatRecord newRcd = new ChatRecord();
        newRcd.m_iHead = proto.icon;
        newRcd.m_strName = proto.name;
        newRcd.m_strChatInfo = proto.msg;
        newRcd.m_audio = proto.audio_chat;
        newRcd.m_iPlayerId = proto.from;
        newRcd.m_iSex = proto.sex;
        newRcd.m_eMrbType = (MemberManager.MEMBER_TYPE)proto.vip_level;
        newRcd.m_area = proto.area;
        newRcd.m_heroType = proto.hero_type;
        if (proto.from == PlayerSystem.roleId)
            newRcd.m_isMyself = true;
        newRcd.m_eChannel = (CHAT_CHANNEL)proto.channel;

        PlayerRecord rcd = new PlayerRecord();
        rcd.m_iPlayerId = proto.from;
        rcd.m_strName = proto.name;
        rcd.m_iHead = proto.icon;
        rcd.m_bOnline = true;
        rcd.m_iLevel = proto.level;
        rcd.m_isFriend = PlayerFriendData.singleton.isFriend(rcd.m_iPlayerId);
        rcd.m_area = proto.area;
        newRcd.m_playerRcd = rcd;

        ChatMsgTips chatMsgTips = new ChatMsgTips();
        chatMsgTips.m_eChannel = (CHAT_CHANNEL)proto.channel;
        chatMsgTips.m_strName = proto.name;
        chatMsgTips.m_strChatMsg = newRcd.m_strChatInfo;
        chatMsgTips.m_area = proto.area;
        chatMsgTips.m_rcd = rcd;
        chatMsgTips.m_is_audio = proto.audio_chat != null && proto.audio_chat.audio_id > 0;
        chatMsgTips.m_heroType = proto.hero_type;

        if (proto.channel == (int)CHAT_CHANNEL.CC_LOCALSERVER)
        {
            m_lstLocalServerChatRcds.Insert(0, newRcd);
            if (m_lstLocalServerChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstLocalServerChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100, m_lstLocalServerChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }
        }
        if (proto.channel == (int)CHAT_CHANNEL.CC_CRSSERVER)
        {
            m_lstCrsServerChatRcds.Insert(0, newRcd);
            if (m_lstCrsServerChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstCrsServerChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100, m_lstCrsServerChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }
        }
        else if (proto.channel == (int)CHAT_CHANNEL.CC_LABA)
        {
            if (!UIManager.IsBattle())
                UIManager.ShowLaba(newRcd);
            m_lstLocalServerChatRcds.Insert(0, newRcd);
            if (m_lstLocalServerChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstLocalServerChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100, m_lstLocalServerChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }
            m_lstCrsServerChatRcds.Insert(0, newRcd);
            if (m_lstCrsServerChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstCrsServerChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100, m_lstCrsServerChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }
        }
        else if (proto.channel == (int)CHAT_CHANNEL.CC_CORPS)
        {
            m_lstCorpsChatRcds.Insert(0, newRcd);
            if (m_lstCorpsChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstCorpsChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100,
                    m_lstCorpsChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }

            m_bHasCorpsChatMsg = true;
        }
        else if (proto.channel == (int)CHAT_CHANNEL.CC_FRIEND)
        {
            rcd.m_isFriend = true;
            InsertChatRecords(proto.from, newRcd);
            foreach (var player in m_lstAllFriends)
            {
                if (player.m_iPlayerId == proto.from)
                {
                    player.m_bHasMsg = true;
                    break;
                }
            }
            if (!m_lstHasMsgPlayerId.Exists(id => id == proto.from))
            {
                m_lstHasMsgPlayerId.Add(proto.from);
                GameDispatcher.Dispatch(GameEvent.UI_NEW_MSG_PLAYER_NUM);
            }
        }
        else if (proto.channel == (int)CHAT_CHANNEL.CC_LEGION)
        {
            m_lstLegionChatRcds.Insert(0, newRcd);
            if (m_lstLegionChatRcds.Count > WORLD_CHAT_NUM_MAX)
            {
                m_lstLegionChatRcds.RemoveRange(WORLD_CHAT_NUM_MAX - 100,
                    m_lstLegionChatRcds.Count - WORLD_CHAT_NUM_MAX + 100);
            }
            m_bHasLegionChatMsg = true;
        }
        else if (proto.channel == (int)CHAT_CHANNEL.CC_PRIVATE)
        {
            InsertChatRecords(proto.from, newRcd);

            AddGuest(rcd);

            if (!m_lstHasMsgPlayerIdPrivate.Exists(id => id == proto.from))
            {
                m_lstHasMsgPlayerIdPrivate.Add(proto.from);
                GameDispatcher.Dispatch(GameEvent.UI_NEW_MSG_PLAYER_NUM_PRIVATE);
            }
        }

        GameDispatcher.Dispatch<ChatMsgTips>(GameEvent.UI_GET_CHAT_MSG, chatMsgTips);
        GameDispatcher.Dispatch(GameEvent.UI_UPDATE_CHAT_MSG);
        GameDispatcher.Dispatch(GameEvent.UI_NEW_CHAT_MSG, newRcd);
    }

    static void Toc_player_chat(toc_player_chat proto)
    {
        if (proto.err_code == 1)
        {
            for (int i = 0; i < m_lstGuests.Count; ++i)
            {
                PlayerRecord rcd = m_lstGuests[i];
                if (rcd.m_iPlayerId == proto.pid)
                {
                    rcd.m_bOnline = false;
                    GameDispatcher.Dispatch(GameEvent.UI_GUESTS_UPDATE);
                    break;
                }
            }
        }
    }

    private static void InsertChatRecords(long id, ChatRecord rcd)
    {
        List<ChatRecord> lstRcds = null;
        if (!m_dicPrivateChatRcds.ContainsKey(id))
        {
            m_dicPrivateChatRcds[id] = new List<ChatRecord>();
        }

        lstRcds = m_dicPrivateChatRcds[id];
        lstRcds.Insert(0, rcd);
        if (lstRcds.Count > PERSON_CHAT_NUM_MAX)
        {
            lstRcds.RemoveRange(PERSON_CHAT_NUM_MAX - 100, lstRcds.Count - PERSON_CHAT_NUM_MAX + 100);
        }
    }

    public static void AddGuest(PlayerRecord rcd)
    {
        bool bNewGuest = true;
        for (int i = 0; i < m_lstGuests.Count; i++)
        {
            if (m_lstGuests[i].m_iPlayerId == rcd.m_iPlayerId)
            {
                bNewGuest = false;
                m_lstGuests[i].m_bHasMsg = true;
                break;
            }
        }

        if (bNewGuest)
        {
            PlayerRecord rewRcd = new PlayerRecord();
            rewRcd.Clone(rcd);
            m_lstGuests.Insert(0, rewRcd);
        }
    }
    public static List<ChatRecord> GetPersonChatRcds(long id)
    {
        if (id == 0)
        {
            return new List<ChatRecord>();
        }

        List<ChatRecord> lstRcds;
        if (NetChatData.m_dicPrivateChatRcds.ContainsKey(id))
        {
            lstRcds = NetChatData.m_dicPrivateChatRcds[id];
        }
        else
        {
            lstRcds = new List<ChatRecord>();
            NetChatData.m_dicPrivateChatRcds[id] = lstRcds;
        }
        return lstRcds;
    }
    public static bool SndChatInfo(int channel, long to, string msg, byte[] audio_data = null, int duration = 0, int log_tip = 0, string cost_type = "")
    {

        if (_CHAT_LVL_LOWER == -1)
        {
            var info = ConfigManager.GetConfig<ConfigMisc>().GetLine("world_chat_lv");
            _CHAT_LVL_LOWER = info != null ? info.ValueInt : 0;
        }
        if (string.IsNullOrEmpty(msg.Trim(' ')))
            return false;
        msg = msg.Replace("\n", "");
        msg = msg.Replace("\r", "");
        if (WordFiterManager.isFiterCheat(msg))
        {
            toc_player_chat_msg msgChat = new toc_player_chat_msg()
            {
                channel = channel,
                from = PlayerSystem.roleId,
                msg = msg,
                icon = PlayerSystem.roleData.icon,
                name = PlayerSystem.roleData.name,
                sex = PlayerSystem.roleData.sex,
                level = PlayerSystem.roleData.level,
                vip_level = PlayerSystem.roleData.vip_level,
            };
            Toc_player_chat_msg(msgChat);
            return false;
        }
        tos_player_chat proto = new tos_player_chat();
        proto.channel = channel;
        proto.pid = to;
        proto.msg = WordFiterManager.Fiter(msg);
        proto.audio_chat = new p_audio_chat() { content = audio_data != null ? audio_data : new byte[0], duration = duration };
        proto.cost_type = cost_type;
        proto.log_type = log_tip;
        NetLayer.Send(proto);
        return PlayerSystem.roleData.level >= _CHAT_LVL_LOWER;
    }

    public static bool CanChat()
    {
        if (_CHAT_LVL_LOWER == -1)
        {
            var info = ConfigManager.GetConfig<ConfigMisc>().GetLine("world_chat_lv");
            _CHAT_LVL_LOWER = info != null ? info.ValueInt : 0;
        }
        return PlayerSystem.roleData.level >= _CHAT_LVL_LOWER;
    }

    public static void FlushGuests()
    {
        for (int i = 0; i < m_lstGuests.Count; ++i)
        {
            PlayerRecord rcd = m_lstGuests[i];
            if (NetChatData.m_lstHasMsgPlayerIdPrivate.Exists(id => id == rcd.m_iPlayerId))
                rcd.m_bHasMsg = true;
            else
                rcd.m_bHasMsg = false;
        }
    }
    public static void FlushFriends()
    {
        //bool guestChanged = false;
        List<FriendData> dataList = PlayerFriendData.singleton.friendList;

        //if (m_lstAllFriends.Count != dataList.Count)
        //    guestChanged = true;

        //m_lstAllFriends.Clear();
        for (int i = m_lstAllFriends.Count - 1; i >= 0; i--)
        {
            bool bDel = true;
            for (int j = 0; j < dataList.Count; j++)
            {
                FriendData syncData = dataList[j];
                if (m_lstAllFriends[i].m_iPlayerId == syncData.m_id)
                {
                    if (syncData.m_state == FRIEND_STATE.OFFLINE)
                        m_lstAllFriends[i].m_bOnline = false;

                    else
                        m_lstAllFriends[i].m_bOnline = true;

                    bDel = false;
                    if (NetChatData.m_lstHasMsgPlayerId.Exists(id => id == m_lstAllFriends[i].m_iPlayerId))
                    {
                        m_lstAllFriends[i].m_bHasMsg = true;
                    }
                    break;
                }
            }

            if (bDel)
            {
                //guestChanged = true;
                if (NetChatData.m_lstHasMsgPlayerId.Exists(id => id == m_lstAllFriends[i].m_iPlayerId))
                {
                    NetChatData.m_lstHasMsgPlayerIdPrivate.Add(m_lstAllFriends[i].m_iPlayerId);
                    NetChatData.m_lstHasMsgPlayerId.Remove(m_lstAllFriends[i].m_iPlayerId);
                }
                m_lstAllFriends[i].m_isFriend = false;
                m_lstGuests.Add(m_lstAllFriends[i]);
                m_lstAllFriends.RemoveAt(i);
            }
        }

        for (int i = 0; i < dataList.Count; ++i)
        {
            FriendData syncData = dataList[i];
            if (m_lstBlack.IndexOf(syncData.m_id) > -1)
                continue;
            bool bAdd = true;
            for (int j = 0; j < m_lstAllFriends.Count; j++)
            {
                if (syncData.m_id == m_lstAllFriends[j].m_iPlayerId)
                    bAdd = false;
            }

            if (bAdd)
            {
                PlayerRecord newRcd = new PlayerRecord();
                newRcd.m_strName = syncData.m_name;
                newRcd.m_iPlayerId = syncData.m_id;
                newRcd.m_iSex = syncData.m_sex;
                newRcd.m_iLevel = syncData.m_level;
                newRcd.m_iHead = syncData.m_icon;
                newRcd.m_isFriend = true;

                if (NetChatData.m_lstHasMsgPlayerIdPrivate.Exists(id => id == newRcd.m_iPlayerId))
                {
                    NetChatData.m_lstHasMsgPlayerIdPrivate.Remove(newRcd.m_iPlayerId);
                    NetChatData.m_lstHasMsgPlayerId.Add(newRcd.m_iPlayerId);
                }

                //if (NetChatData.m_lstHasMsgPlayerId.Exists(id => id == syncData.m_id))
                //    newRcd.m_bHasMsg = true;

                if (syncData.m_state == FRIEND_STATE.OFFLINE)
                {
                    newRcd.m_bOnline = false;
                    m_lstAllFriends.Add(newRcd);
                }
                else
                {
                    newRcd.m_bOnline = true;
                    m_lstAllFriends.Insert(0, newRcd);
                }

                for (int j = 0; j < m_lstGuests.Count; ++j)
                {
                    if (m_lstGuests[j].m_iPlayerId == newRcd.m_iPlayerId)
                    {
                        m_lstGuests.RemoveAt(j);
                        //guestChanged = true;
                        break;
                    }
                }
            }
        }
        /*
            for (int i = 0; i < dataList.Count; ++i)
            {
                FriendData syncData = dataList[i];
                if (m_lstBlack.IndexOf(syncData.m_id) > -1)
                    continue;

                PlayerRecord newRcd = new PlayerRecord();
                newRcd.m_strName = syncData.m_name;
                newRcd.m_iPlayerId = syncData.m_id;
                newRcd.m_iSex = syncData.m_sex;
                newRcd.m_iLevel = syncData.m_level;
                newRcd.m_iHead = syncData.m_icon;
                newRcd.m_isFriend = true;

                if (NetChatData.m_lstHasMsgPlayerIdPrivate.Exists(id => id == newRcd.m_iPlayerId))
                {
                    NetChatData.m_lstHasMsgPlayerIdPrivate.Remove(newRcd.m_iPlayerId);
                    NetChatData.m_lstHasMsgPlayerId.Add(newRcd.m_iPlayerId);
                }

                if (NetChatData.m_lstHasMsgPlayerId.Exists(id => id == syncData.m_id))
                    newRcd.m_bHasMsg = true;

                //m_lstAllFriends.Add(newRcd);
                if (syncData.m_state == FRIEND_STATE.OFFLINE)
                {
                    newRcd.m_bOnline = false;
                    //m_lstOfflineFriends.Add(newRcd);
                    m_lstAllFriends.Add(newRcd);
                }
                else
                {
                    newRcd.m_bOnline = true;
                    //m_lstOnlineFriends.Add(newRcd);
                    m_lstAllFriends.Insert(0, newRcd);
                }

                for (int j = 0; j < m_lstGuests.Count; ++j)
                {
                    if (m_lstGuests[j].m_iPlayerId == newRcd.m_iPlayerId)
                    {
                        m_lstGuests.RemoveAt(j);
                        guestChanged = true;
                        break;
                    }
                }
            }
         */
        //if (guestChanged )
        //{
        //    GameDispatcher.Dispatch(GameEvent.UI_GUESTS_UPDATE);
        //    GameDispatcher.Dispatch(GameEvent.UI_NEW_MSG_PLAYER_NUM);
        //    GameDispatcher.Dispatch(GameEvent.UI_NEW_MSG_PLAYER_NUM_PRIVATE);
        // }

    }

    static public void AddBlack(long id)
    {
        if (m_lstBlack.IndexOf(id) > -1)
            return;
        PlayerFriendData.AddBlack(id);

        m_lstBlack.Add(id);
        for (int i = m_lstLocalServerChatRcds.Count - 1; i >= 0; --i)
        {
            ChatRecord rcd = m_lstLocalServerChatRcds[i];
            if (rcd.m_iPlayerId == id)
                m_lstLocalServerChatRcds.RemoveAt(i);
        }
        for (int i = m_lstCrsServerChatRcds.Count - 1; i >= 0; --i)
        {
            ChatRecord rcd = m_lstCrsServerChatRcds[i];
            if (rcd.m_iPlayerId == id)
                m_lstCrsServerChatRcds.RemoveAt(i);
        }
        for (int i = m_lstAllFriends.Count - 1; i >= 0; --i)
        {
            PlayerRecord rcd = m_lstAllFriends[i];
            if (rcd.m_iPlayerId == id)
                m_lstAllFriends.RemoveAt(i);
        }

        if (m_dicPrivateChatRcds.ContainsKey(id))
        {
            m_dicPrivateChatRcds.Remove(id);
        }

        GameDispatcher.Dispatch(GameEvent.UI_UPDATE_CHAT_MSG);
    }

    static public void SetLegionList(p_legion_member_toc[] list)
    {
        m_lstAllLegionMember = new List<PlayerRecord>();
        for (int i = 0; i < list.Length; i++)
        {
            m_lstAllLegionMember.Add(GetRecord(list[i]));
        }
    }

    static public PlayerRecord GetRecord(p_legion_member_toc data)
    {
        PlayerRecord pr = new PlayerRecord();
        pr.m_strName = data.name;
        pr.m_iPlayerId = data.id;
        pr.m_iLevel = data.level;
        pr.m_bOnline = data.handle != 0;
        pr.m_iHead = data.icon;
        return pr;
    }

    static private void Toc_master_add_master(CDict data)
    {
        TipsManager.Instance.showTips("申请成功发送");
    }

    static public void MockOfflineMsg()
    {
        var rmi = new toc_player_offline_chat_msg() { list = new toc_player_offline_chat_msg.offline_msg[5] };
        rmi.list[0] = new toc_player_offline_chat_msg.offline_msg() { time = 1, from = 709394, icon = 2, channel = 3, level = 20, msg = "测试一下1", name = "test1", sex = 1, vip_level = 1 };
        rmi.list[1] = new toc_player_offline_chat_msg.offline_msg() { time = 2, from = 709395, icon = 4, channel = 4, level = 40, msg = "测试1", name = "test2", sex = 2, vip_level = 1 };
        rmi.list[2] = new toc_player_offline_chat_msg.offline_msg() { time = 3, from = 709395, icon = 4, channel = 4, level = 40, msg = "测试2", name = "test2", sex = 2, vip_level = 1 };
        rmi.list[3] = new toc_player_offline_chat_msg.offline_msg() { time = 4, from = 709394, icon = 2, channel = 4, level = 40, msg = "测试一下2", name = "test1", sex = 1, vip_level = 2 };
        rmi.list[4] = new toc_player_offline_chat_msg.offline_msg() { time = 5, from = 709395, icon = 4, channel = 3, level = 40, msg = "测试4", name = "test2", sex = 2, vip_level = 1 };
        Toc_player_offline_chat_msg(rmi);
    }

    static private void Toc_player_offline_chat_msg(toc_player_offline_chat_msg rmi)
    {
        long key = -1;
        var temp_player_map = new Dictionary<long, List<toc_player_offline_chat_msg.offline_msg>>();
        var temp_corps_data = new List<toc_player_offline_chat_msg.offline_msg>();
        List<toc_player_offline_chat_msg.offline_msg> cur_list = null;
        foreach (var sdata in rmi.list)
        {
            if (sdata.channel == (int)CHAT_CHANNEL.CC_CORPS)
            {
                temp_corps_data.Add(sdata);
            }
            else
            {
                if (cur_list == null || sdata.from != key)
                {
                    key = sdata.from;
                    if (temp_player_map.ContainsKey(key))
                    {
                        cur_list = temp_player_map[key];
                    }
                    else
                    {
                        cur_list = temp_player_map[key] = new List<toc_player_offline_chat_msg.offline_msg>();
                    }
                }
                cur_list.Add(sdata);
            }
        }
        List<ChatRecord> vo_list = null;
        ChatRecord vo = null;
        //战队离线消息
        if (temp_corps_data.Count > 0)
        {
            //m_bHasCorpsChatMsg = true;//战队历史和离线消息暂时区别不开，所以就不要再提示了
            temp_corps_data.Sort((a, b) => b.time - a.time);
            foreach (var rd in temp_corps_data)
            {
                m_lstCorpsChatRcds.Add(ConvertSOfflineMsgToVO(rd));
            }
        }
        //玩家离线消息
        if (temp_player_map.Count > 0)
        {
            foreach (var rd_list in temp_player_map)
            {
                rd_list.Value.Sort((a, b) => b.time - a.time);
                var last_rd = rd_list.Value.Count > 0 ? rd_list.Value[0] : null;//玩家发的最后一条消息作为最终频道
                if (m_dicPrivateChatRcds.ContainsKey(rd_list.Key))
                {
                    vo_list = m_dicPrivateChatRcds[rd_list.Key];
                }
                else
                {
                    vo_list = m_dicPrivateChatRcds[rd_list.Key] = new List<ChatRecord>();
                }
                foreach (var rd in rd_list.Value)
                {
                    vo = ConvertSOfflineMsgToVO(rd);
                    vo.m_eChannel = (CHAT_CHANNEL)last_rd.channel;
                    vo_list.Add(vo);
                }
                if (last_rd != null)
                {
                    var msg_list = last_rd.channel == (int)CHAT_CHANNEL.CC_FRIEND ? m_lstHasMsgPlayerId : m_lstHasMsgPlayerIdPrivate;
                    var player_list = last_rd.channel == (int)CHAT_CHANNEL.CC_FRIEND ? m_lstAllFriends : m_lstGuests;
                    if (msg_list.IndexOf(last_rd.from) == -1)
                    {
                        msg_list.Add(last_rd.from);
                        var prd = player_list.Find(p => p.m_iPlayerId == last_rd.from);
                        if (prd == null)
                        {
                            prd = new PlayerRecord();
                            prd.m_iPlayerId = rd_list.Value[0].from;
                            prd.m_strName = rd_list.Value[0].name;
                            prd.m_iSex = rd_list.Value[0].sex;
                            prd.m_iLevel = rd_list.Value[0].level;
                            prd.m_iHead = rd_list.Value[0].icon;
                            prd.m_isFriend = rd_list.Value[0].channel == (int)CHAT_CHANNEL.CC_FRIEND;
                            prd.m_bHasMsg = true;
                            prd.m_area = rd_list.Value[0].area;
                            player_list.Add(prd);
                        }
                        prd.m_bHasOfflineMsg = true;
                    }
                }
            }
        }
        FlushOfflineMsg();
    }

    static private ChatRecord ConvertSOfflineMsgToVO(toc_player_offline_chat_msg.offline_msg data)
    {
        var vo = new ChatRecord();
        vo.m_iHead = data.icon;
        vo.m_strName = data.name;
        vo.m_strChatInfo = data.msg;
        vo.m_audio = new p_audio_chat();
        vo.m_iPlayerId = data.from;
        vo.m_iSex = data.sex;
        vo.m_eMrbType = (MemberManager.MEMBER_TYPE)data.vip_level;
        vo.m_eChannel = (CHAT_CHANNEL)data.channel;
        vo.m_isMyself = data.from == PlayerSystem.roleId;
        vo.m_area = data.area;
        return vo;
    }

    static public void DecNewMsgId(long id, bool isFriend)
    {
        if (isFriend)
        {
            int idx = m_lstHasMsgPlayerId.IndexOf(id);
            if (idx > -1)
                m_lstHasMsgPlayerId.RemoveAt(idx);

            foreach (var player in m_lstAllFriends)
            {
                if (player.m_iPlayerId == id)
                {
                    player.m_bHasMsg = false;
                }
            }
        }
        else
        {
            int idx = m_lstHasMsgPlayerIdPrivate.IndexOf(id);
            if (idx > -1)
                m_lstHasMsgPlayerIdPrivate.RemoveAt(idx);

            foreach (var player in m_lstGuests)
            {
                if (player.m_iPlayerId == id)
                {
                    player.m_bHasMsg = false;
                }
            }
        }
    }
}
