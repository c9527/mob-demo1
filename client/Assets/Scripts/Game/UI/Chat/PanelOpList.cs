﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelOpList : BasePanel
{
    public PanelOpList()
    {
        SetPanelPrefabPath("UI/Chat/PanelOpList");
        AddPreLoadRes("UI/Chat/PanelOpList", EResType.UI);
      
        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
    }

    public override void Init()
    {
    }

    public override void Update()
    {
    }

     public override void InitEvent()
     {
         UGUIClickHandler.Get(m_go).onPointerClick += OnClkBlank;
     }

     public override void OnShow()
     { }

     public override void OnHide()
     {

     }

     public override void OnBack()
     {
     }

     public override void OnDestroy()
     {
         UGUIClickHandler.Get(m_go).onPointerClick -= OnClkBlank;
     }

     protected void OnClkBlank( GameObject target, PointerEventData eventData )
     {
         HidePanel();
     }
}
