﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelSpeechSpeaker : BasePanel
{
    static PanelSpeechSpeaker _instance;

    int _status = -1;//-1-初始化 0-listening 1-loaidng
    int _volume = 0;
    bool _volume_updated = false;
    float _deadline;

    GameObject _speaker_go;
    GameObject _loading_go;
    Image _speaker_img;

    public PanelSpeechSpeaker()
    {
        SetPanelPrefabPath("UI/Chat/PanelSpeechSpeaker");

        AddPreLoadRes("Atlas/Chat", EResType.Atlas);
        AddPreLoadRes("UI/Chat/PanelSpeechSpeaker", EResType.UI);
    }

    public override void Init()
    {
        _speaker_go = m_tran.Find("speaker").gameObject;
        _speaker_img = _speaker_go.transform.Find("avatar").GetComponent<Image>();
        _loading_go = m_tran.Find("loading_img").gameObject;
    }

    public override void InitEvent()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="status">status==0时会打开对讲机同时停止语音播放</param>
    static public void showSpeaker(int status=0)
    {
        if (SdkManager.Enable && !SdkManager.CurrentSdk.IsSpeechInitialized())
        {
            SdkManager.InitSpeaker("timeout=15000");
            if (!SdkManager.CurrentSdk.IsSpeechInitialized())
            {
                return;
            }
        }
        if (status==0 && (_instance==null || !_instance.IsOpen()))
        {
            _instance = UIManager.PopChatPanel<PanelSpeechSpeaker>(); 
        }
        if (_instance != null)
        {
            var old_status = _instance._status;
            _instance._status = status;
            if (_instance._status != old_status)
            {
                if (_instance._status == 0)
                {
                    NetChatData.stopSpeech(true);//会设置恢复背景声音
                    AudioManager.MusicMute = true;
                    SdkManager.ShowSpeaker(false);
                }
                else
                {
                    _instance._deadline = Time.time + 5;
                    SdkManager.CloseSpeaker(true);
                }
            }
        }
    }

    static public void closeSpeaker()
    {
        if (_instance != null)
        {
            _instance.HidePanel();
            _instance = null;
        }
        AudioManager.MusicMute = false;
        SdkManager.CloseSpeaker();
    }

    static public void UpdateSpeaker(int value=0)
    {
        if (_instance != null)
        {
            _instance.UpdateData(value);
        }
    }

    public void UpdateData(int value=0)
    {
        if (_volume != value)
        {
            _volume_updated = true;
        }
        _volume = value;
    }

    public override void OnShow()
    {
        _status = -1;
        _deadline = 0;
        _volume = 0;
        if (m_tran == null)
        {
            return;
        }
        ((RectTransform)_speaker_img.transform).sizeDelta = Vector2.zero;
        _loading_go.gameObject.TrySetActive(false);
    }

    public override void OnHide()
    {
        _status = -1;
        _deadline = 0;
        _volume = 0;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (m_tran == null)
        {
            return;
        }
        if (_status == 1)
        {
            if(_deadline>0 && Time.time>=_deadline)
            {
                closeSpeaker();
                return;
            }
            if (_loading_go.gameObject.activeSelf == false)
            {
                ((RectTransform)_speaker_img.transform).sizeDelta = Vector2.zero;
                _loading_go.gameObject.TrySetActive(true);
            }
            _loading_go.transform.localEulerAngles -= new Vector3(0, 0, 10);
        }
        else
        {
            if (_loading_go.gameObject.activeSelf == true)
            {
                ((RectTransform)_speaker_img.transform).sizeDelta = Vector2.zero;
                _loading_go.gameObject.TrySetActive(false);
            }
            if (_volume_updated == true)
            {
                _volume_updated = false;
                ((RectTransform)_speaker_img.transform).sizeDelta = new Vector2(222, 222) * _volume / 30f;
            }
        }
    }
}

