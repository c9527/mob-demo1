﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class ChatAudioItem : ItemRender
{
    p_audio_chat _data;
    float _play_time;
    Button _btn;
    Image _img;

    public override void Awake()
    {
        _btn = transform.GetComponent<Button>();
        _img = transform.Find("status_img").GetComponent<Image>();

        UGUIClickHandler.Get(_btn.gameObject).onPointerClick += OnAudioBtnClick;
    }

    private void OnAudioBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data==null || _data.audio_id==0)
        {
            return;
        }
        if (SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            if (_play_time == 0)
            {
                _play_time = Time.time;
                NetChatData.playSpeech(_data.audio_id,OnResetItem,_data.content,_data.duration);
            }
            else
            {
                NetChatData.stopSpeech();
                OnResetItem();
            }
        }
        else
        {
            UIManager.ShowNoThisFunc();
        }
    }

    private void OnResetItem()
    {
        _play_time = 0;
        if (_img != null)
        {
            _img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voice_0"));
            _img.SetNativeSize();
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as p_audio_chat;
        if (_data==null)
        {
            return;
        }
        _btn.transform.Find("Text").GetComponent<Text>().text = _data.duration + "秒";
        _img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voice_0"));
        _img.SetNativeSize();
    }

    private void Update()
    {
        if (_play_time == 0)
        {
            return;
        }
        var voice_value = Mathf.Floor((Time.time - _play_time) * 3 / 1f);
        voice_value = voice_value%3 + 1;
        _img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "voice_" + voice_value));
    }
}
