﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PanelLaba : BasePanel
{
    private RectTransform m_tranMsg;
    private Image m_title;
    private Text m_msg;
    private static List<ChatRecord> m_msgList = new List<ChatRecord>();
    private float m_startTime;
    private float m_continueTime;       //不是最后一个喇叭时的持续时间
    private float m_lastContinueTime;  //最后一个喇叭时的持续时间

    public PanelLaba()
    {
        SetPanelPrefabPath("UI/Chat/PanelLaba");
        AddPreLoadRes("UI/Chat/PanelLaba", EResType.UI);
    }

    public override void Init()
    {
        m_tranMsg = m_tran.Find("content/Text").gameObject.GetRectTransform();
        m_title = m_tran.Find("content/title").GetComponent<Image>();
        m_msg = m_tranMsg.GetComponent<Text>();
        EnableMouseEvent = false;

        m_continueTime = ConfigMisc.GetInt("chat_continue_time");
        m_lastContinueTime = ConfigMisc.GetInt("chat_last_continue_time");
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        ShowNextMsg();
    }

    public static void AddMsg(ChatRecord msg)
    {
        m_msgList.Add(msg);
    }

    private void ShowNextMsg()
    {
        if (m_msgList.Count == 0)
        {
            HidePanel();
            return;
        }

        var item = m_msgList[0];
        m_title.enabled = item.m_eMrbType != MemberManager.MEMBER_TYPE.MEMBER_NONE;
        m_title.SetSprite(ResourceManager.LoadVipIcon(item.m_eMrbType));
        m_msg.text = string.Format("<color='#00EAFF'>[{0}]</color>{1}", item.m_strName, item.m_strChatInfo);
        m_msgList.RemoveAt(0);
        m_startTime = Time.realtimeSinceStartup;
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (!IsOpen())
            return;

        var showTime = m_msgList.Count == 0 ? m_lastContinueTime : m_continueTime;
        if (Time.realtimeSinceStartup - m_startTime > showTime)
            ShowNextMsg();
    }
}