﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

class PhotoDataManager : Singleton<PhotoDataManager>
{
    public List<string> UrlList
    {
        get { return _longUrlList; }
    }

    public int SelectedIconIndex
    {
        get { return _selectedIconIndex; }
    }

    private int _selectedIconIndex = -1;

    private string[] _urlArray;

    public static string IconUrl
    {
        get { return _currentIconUrl; }
    }

    private static string _currentIconUrl;

    public static string IconUrlPrefix = "http://f1.img4399.com/syavatar";

    private List<string> _shortUrlList;
    private List<string> _longUrlList;

    public PhotoDataManager()
    {
        _shortUrlList = new List<string>();
        _longUrlList = new List<string>();
        _urlArray = new string[3];
        _urlArray[0] = IconUrlPrefix;
    }

    public void AddUrl(string url)
    {
        _longUrlList.Add(url);
        url = GetShortUrl(url);
        _shortUrlList.Add(url);
        // notify server
        NetLayer.Send(new tos_player_add_icon_url{icon_url = url});
    }

    public void DelUrl(int index)
    {
        if (index >= 0 && index < _longUrlList.Count)
        {
            _longUrlList.RemoveAt(index);
            _shortUrlList.RemoveAt(index);
            NetLayer.Send(new tos_player_del_icon_url { del_index = index + 1 });
        }
    }

    public string GetUrl(int serverIndex)
    {
        if (serverIndex > 0 && serverIndex  <= _shortUrlList.Count)
        {
            return _longUrlList[serverIndex-1];
        }
        return String.Empty;
    }

    public void SetUrl(int index)
    {
        if (index >= 0 && index < _longUrlList.Count && _selectedIconIndex != index)
        {
            NetLayer.Send(new tos_player_set_icon_url() { new_index = index + 1 });
        }
    }


    public string GetLongUrl(string url, string suffix = "128x128")
    {
        if (String.IsNullOrEmpty(url))
        {
            return String.Empty;
        }
        _urlArray[1] = url;
        _urlArray[2] = suffix;
        return String.Join("~", _urlArray).TrimEnd('~');
    }

    public string GetShortUrl(string url)
    {
        if (url.StartsWith("http"))
        {
            var strArray = url.Split('~');
            return strArray[1];
        }
        return url;
    }

    public void ReqUrlList()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        NetLayer.Send(new tos_player_icon_url_data());
#endif
    }

    private static void Toc_player_icon_url_data(toc_player_icon_url_data data)
    {
        HandleUrlListUpdate(data);
    }

    private static void Toc_player_set_icon_url(toc_player_set_icon_url data)
    {
        HandleUrlListUpdate(data);
    }

    private static void Toc_player_add_icon_url(toc_player_add_icon_url data)
    {
        HandleUrlListUpdate((data));
    }

    private static void Toc_player_del_icon_url(toc_player_del_icon_url data)
    {
        HandleUrlListUpdate(data);
    }

    private static void HandleUrlListUpdate(toc_player_icon_url_data data)
    {
        var urlList = data.url_list.ToList();
        Instance._shortUrlList = urlList;
        Instance._longUrlList.Clear();
        for (int i = 0; i < urlList.Count; i++)
        {
            Instance._longUrlList.Add(Instance.GetLongUrl(urlList[i]));
        }
        Instance._selectedIconIndex = data.cur_index - 1;
        _currentIconUrl = Instance.GetUrl(data.cur_index);
        GameDispatcher.Dispatch(GameEvent.UI_SELF_ICON_URL_RETRIVED);
    }

    
}
