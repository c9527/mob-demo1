﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelHeadIcon:BasePanel
{
    private const int BIG_ICON_HEIGHT = 256;
    private const int BIG_ICON_WIDTH = 256;
    private const int SMALL_ICON_HEIGHT = 48;
    private const int SMALL_ICON_WIDTH = 48;


    private RawImage _bigHeadIcon;
    private RawImage _smallHeadIcon;

    private Text _originalInputPathTxt;
    private Text _cropBigImgPathTxt;
    private Text _cropSmallImgPathTxt;
    private Text _outputUrlTxt;


    private string originalInputPath;
    private string _selectInputPath;
    private string _cropImgPath;
    private string _outputUrl;
    private GameObject _albumPhoto;
    private GameObject _cameraPhoto;


    public PanelHeadIcon()
    {
        SetPanelPrefabPath("UI/Moment/PanelHeadIcon");
        AddPreLoadRes("UI/Moment/PanelHeadIcon",EResType.UI);
    }
    public override void Init()
    {
        _albumPhoto = m_tran.Find("select_pic").gameObject;
        _cameraPhoto = m_tran.Find("selfie").gameObject;        
        AddEventListener<string>(GameEvent.UI_SELECT_EXTERN_PIC, (path) => 
        {
            _selectInputPath = path;
            string outputPath = ResourceManager.ExternalCacheDir + Path.GetFileName(path);
            Logger.Error("outputpath:" + outputPath);
            if (!String.IsNullOrEmpty(_selectInputPath))
            {
                SdkManager.CropPic(_selectInputPath, outputPath, 1, 1, BIG_ICON_WIDTH, BIG_ICON_HEIGHT);
            }
        });
        AddEventListener<string, string>(GameEvent.UI_CROP_PIC, (name, path) => 
        { 
            _cropImgPath = path;
            Logger.Error("crop path:" + path);
            SdkManager.UploadPic(path);
        });
        AddEventListener<string, string>(GameEvent.UI_UPLOAD_PIC, (name, path) =>
        {
            _outputUrl = path;
            Logger.Error("outpput path:" + path);
            PhotoDataManager.Instance.AddUrl(path);
            HidePanel();
        });
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("selfie")).onPointerClick += delegate
        {
            SdkManager.SelfiePic();
        };
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel();};
        UGUIClickHandler.Get(m_tran.Find("select_pic")).onPointerClick += delegate
        {
            SdkManager.SelectPic();
        };
    }

    public override void OnShow()
    {
        if (Driver.m_platform == EnumPlatform.android)
        {
            _cameraPhoto.TrySetActive(true);
        }
        else if(Driver.m_platform == EnumPlatform.ios)
        {
            _cameraPhoto.TrySetActive(false);
            _albumPhoto.transform.localPosition = new Vector3(10, 38, 0);            
        }
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
    
}