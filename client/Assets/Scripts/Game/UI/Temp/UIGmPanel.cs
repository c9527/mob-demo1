﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class UIGmPanel : MonoBehaviour
{
    static public UIGmPanel singleton;

    toc_player_gm.gm_def[] mServerGmList = null;
    string mShowTab = null;
    private EventSystem mEventSystem;
    public static bool m_editorGuide;
    public string platform_api_cmd = "";

    private SkillRangeTest m_skillRangeTest;

    // Use this for initialization
    void Start()
    {
        singleton = this;
        mEventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        m_skillRangeTest = gameObject.AddMissingComponent<SkillRangeTest>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        //自动瞄准锁定点测试
        //        GUI.Box(new Rect(AutoAim.TargetScreenPos.x-5, Screen.height-AutoAim.TargetScreenPos.y-5, 10,10), "");

        GUI.matrix = Matrix4x4.Scale(new Vector3(Screen.width / 800f, Screen.height / 480f, 1));

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUILayout.Space(120);        
        if (GUILayout.Button("GM", GUILayout.Height(40), GUILayout.Width(40)))
        {
            if (mShowTab == null)
            {
                mShowTab = "";
                mEventSystem.enabled = false;
            }
            else
            {
                mShowTab = null;
                mEventSystem.enabled = true;
            }
        }

        GUILayout.BeginVertical();
        if( LogItem.isOpen)
        {
            GUILayout.BeginHorizontal();
            if (LogItem.isShow = GUILayout.Toggle(LogItem.isShow, "显示日志"))
            {
                GUILayout.Label("日志等级");
                LogItem.isShowLogType = GUILayout.Toggle(LogItem.isShowLogType, "Log");
                LogItem.isShowWarningType = GUILayout.Toggle(LogItem.isShowWarningType, "Warning");
                LogItem.isShowErrorType = GUILayout.Toggle(LogItem.isShowErrorType, "Error");

                LogItem.isShowDetail = GUILayout.Toggle(LogItem.isShowDetail, "显示细节");

                GUILayout.Label("   过滤串");
                LogItem.filterText = GUILayout.TextField(LogItem.filterText, GUILayout.Height(26), GUILayout.Width(100));

                if (GUILayout.Button("清空日志", GUILayout.Width(100)))
                {
                    LogItem.logQueue.Clear();
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();

                LogItem.scrollPos = GUILayout.BeginScrollView(LogItem.scrollPos, GUILayout.Width(400), GUILayout.Height(200));
                LogItem[] arr = LogItem.logQueue.ToArray();
                for (int i = 0; i < arr.Length;i++ )
                {
                    LogType type = arr[i].type;
                    if (type == LogType.Log && !LogItem.isShowLogType
                       || type == LogType.Warning && !LogItem.isShowWarningType
                       || type == LogType.Error && !LogItem.isShowErrorType)
                        continue;

                    if (!string.IsNullOrEmpty(LogItem.filterText) && !arr[i].content.Contains(LogItem.filterText))
                        continue;

                    if(LogItem.isShowDetail)
                    {
                        GUILayout.TextArea(string.Format("{0}\n{1}\n{2}", arr[i].content, arr[i].statck, arr[i].type.ToString()));
                    }
                    else
                    {
                        GUILayout.TextArea(string.Format("{0}", arr[i].content, arr[i].statck, arr[i].type.ToString()));
                    }
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndHorizontal();
        }
        if(m_playStory)
        {
            GUILayout.BeginHorizontal();
            m_storyId = GUILayout.TextField(m_storyId, GUILayout.Height(26), GUILayout.Width(100));
            if (GUILayout.Button("播放剧情", GUILayout.Width(100)))
            {
                int storyId = -1;
                if (int.TryParse(m_storyId, out storyId))
                {
                    if (StoryManager.singleton != null)
                        StoryManager.singleton.PlayStory(storyId);
                }
            }

            if (GUILayout.Button("停止播放", GUILayout.Width(100)))
            {
                if (StoryManager.singleton != null)
                    StoryManager.singleton.EndStoryForce();
            }

            GUILayout.EndHorizontal();
        }

        if (m_useSkill)
        {
            GUILayout.BeginHorizontal();
            m_skillId = GUILayout.TextField(m_skillId, GUILayout.Height(26), GUILayout.Width(100));
            if (GUILayout.Button("施放", GUILayout.Width(50)))
            {
                int skillId = -1;
                if (int.TryParse(m_skillId, out skillId))
                    MainPlayer.singleton.UseSkill(skillId);
            }
            GUILayout.EndHorizontal();
        }

        if (m_drawRange)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("半径/长：", GUILayout.Height(26), GUILayout.Width(100));
            m_drawRangeParam1 = GUILayout.TextField(m_drawRangeParam1, GUILayout.Height(26), GUILayout.Width(100));
            GUILayout.Label("内角/宽：", GUILayout.Height(26), GUILayout.Width(100));
            m_drawRangeParam2 = GUILayout.TextField(m_drawRangeParam2, GUILayout.Height(26), GUILayout.Width(100));
            if (GUILayout.Button("圆形/扇形", GUILayout.Width(100)))
            {
                float radius = 0f;
                float angle = 0;
                if (float.TryParse(m_drawRangeParam1, out radius) && float.TryParse(m_drawRangeParam2, out angle))
                {
                    m_skillRangeTest.Draw(MainPlayer.singleton.transform, 0, radius, angle);
                }
            }
            if (GUILayout.Button("矩形", GUILayout.Width(50)))
            {
                float length = 0;
                float width = 0;
                if (float.TryParse(m_drawRangeParam1, out length) && float.TryParse(m_drawRangeParam2, out width))
                {
                    m_skillRangeTest.Draw(MainPlayer.singleton.transform, 1, length, width);
                }
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        if (mShowTab == null)
            return;

        GUILayout.BeginHorizontal();
        if (ShowRow("微调"))
        {
            GUILayout.EndHorizontal();
            if (ShowSlider("加速起始点", ref PanelBattle.m_fAimAccStart, 0, 100))
            {
                if (PanelBattle.m_fAimAccEnd < PanelBattle.m_fAimAccStart)
                    PanelBattle.m_fAimAccEnd = PanelBattle.m_fAimAccStart;
            }
            if (ShowSlider("加速最大点", ref PanelBattle.m_fAimAccEnd, 0, 100))
            {
                if (PanelBattle.m_fAimAccStart > PanelBattle.m_fAimAccEnd)
                    PanelBattle.m_fAimAccStart = PanelBattle.m_fAimAccEnd;
            }
            ShowSlider("加速最大值", ref PanelBattle.m_fAimAccTimes, 0, 20);
            ShowSlider("转向因子(Dpi:"+Screen.dpi+")", ref TouchRotationSmooth.factor, 0, 3);
        }
        else
        {
            GUILayout.EndHorizontal();
        }

        if (mServerGmList != null)
        {
            foreach (var gm in mServerGmList)
            {
                GUILayout.BeginHorizontal();
                if (ShowRow(gm.name))
                {
                    foreach (string cmd in gm.cmds)
                    {
                        if (GUILayout.Button(cmd, GUILayout.Height(40)))
                            NetLayer.Send(new tos_player_gm { name = gm.name, cmd = cmd });
                    }
                }
                GUILayout.EndHorizontal();
            }
        }

        GUILayout.BeginHorizontal();
        if (ShowRow("特效"))
        {
            var arr = GameObject.Find("UGUIRootCanvas").transform.GetComponentsInChildren<Transform>(true);
            GUILayout.BeginVertical();
            var visibleCount = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (visibleCount % 10 == 0)
                {
                    GUILayout.EndVertical();
                    GUILayout.BeginVertical();
                }
                var goEffect = arr[i].gameObject;
                if (!goEffect.name.StartsWith("UI_"))
                    continue;
                visibleCount++;
                var tmp = ToggleButton(goEffect.activeSelf, goEffect.name, GUILayout.Width(200));
                if (goEffect.activeSelf != tmp)
                    goEffect.TrySetActive(tmp);
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (ShowRow("前端1"))
        {
            GUILayout.BeginVertical();
            if (GUILayout.Button("CameraInfo"))
            {
                string msg = "LocalPos = " + SceneCamera.singleton.localPosition + "\n";
                msg += "ParentPos = " + (SceneCamera.singleton.parent != null ? SceneCamera.singleton.parent.localPosition : Vector3.one * -10) + "\n";
                msg += "Parent = " + (SceneCamera.singleton.parent != null ? SceneCamera.singleton.parent.name : "null") + "\n";
                msg += "Root = " + SceneCamera.singleton.parent.parent.name + "\n";
                Logger.Warning(msg);
            }
            Logger.IsShowLog = ToggleButton(Logger.IsShowLog, "ShowLog", GUILayout.Height(30));
            Logger.IsShowFunCostTime = ToggleButton(Logger.IsShowFunCostTime, "ShowFunCostTime", GUILayout.Height(30));
            GameSetting.enableCameraLog = ToggleButton(GameSetting.enableCameraLog, "CameraLog", GUILayout.Height(30));

            if(GUILayout.Button("OPTweenInfo"))
            {
                ServerPlayer sp = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID) as ServerPlayer;
                if(sp == null)
                {
                    Debug.LogError("WatcherPlayer is null");
                    return;
                }

                string msg = "WatchPlayerTweenInfo: " +
                    " PosQueueCnt = " + sp.posCount +
                    " Tweening = " + sp.tweening +
                    " lastTweenTime = " + sp.lastTweenTime +
                    " TweenerEnable = " + sp.tweenerEnable;
                Debug.LogWarning(msg);
            }

            if(GUILayout.Button("RamInfo"))
            {
                if (MonoRunInfo.singleton != null)
                {
                    int usedMemory = Util.GetUsedMemory();
                    int freeMemory = Util.GetFreeMemory();
                    bool isLowMemory = Util.IsLowMemory();

                    MonoRunInfo.singleton.m_usedMemory = usedMemory;
                    MonoRunInfo.singleton.m_freeMemory = freeMemory;
                    MonoRunInfo.singleton.m_isLowMemory = isLowMemory;
                }

                Debug.LogWarning(Util.GetMemoryInfo());
            }

            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            //GameSetting.openGravity = ToggleButton(GameSetting.openGravity, "Gravity", GUILayout.Height(30));
            //GameSetting.openJumpUp = ToggleButton(GameSetting.openJumpUp, "OpenJumpUp", GUILayout.Height(30));
            //GameSetting.jumpUpMsg = ToggleButton(GameSetting.jumpUpMsg, "jumpUpMsg", GUILayout.Height(30));
            //GameSetting.openMove = ToggleButton(GameSetting.openMove, "OpenMove", GUILayout.Height(30));
            ////GameSetting.openMoveStack = ToggleButton(GameSetting.openMoveStack, "OpenMoveStack", GUILayout.Height(30));

            //GameSetting.enableOPRayTest = ToggleButton(GameSetting.enableOPRayTest, "OPFireRay", GUILayout.Height(30));
            
            ////GameSetting.enableGunFire = ToggleButton(GameSetting.enableGunFire, "枪火", GUILayout.Height(40));
            ////GameSetting.enableGunShell = ToggleButton(GameSetting.enableGunShell, "弹壳", GUILayout.Height(40));
            ////GameSetting.enableDanheng = ToggleButton(GameSetting.enableDanheng, "弹痕", GUILayout.Height(40));
            //////GameSetting.enableGunRaytest = ToggleButton(GameSetting.enableGunRaytest, "开枪射线", GUILayout.Height(40));
            //////GameSetting.enableGravityRay = ToggleButton(GameSetting.enableGravityRay, "重力射线", GUILayout.Height(40));
            //////GameSetting.enableRayName = ToggleButton(GameSetting.enableRayName, "名字射线", GUILayout.Height(40));
            ////GameSetting.enableSound = ToggleButton(GameSetting.enableSound, "声音", GUILayout.Height(40));
            GlobalConfig.gameSetting.TweenTime = float.Parse(GUILayout.TextField(GlobalConfig.gameSetting.TweenTime.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GlobalConfig.gameSetting.AITweenTime = float.Parse(GUILayout.TextField(GlobalConfig.gameSetting.AITweenTime.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GlobalConfig.gameSetting.PosQueueMaxNum = int.Parse(GUILayout.TextField(GlobalConfig.gameSetting.PosQueueMaxNum.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GlobalConfig.gameSetting.PosQueueNormalNum = int.Parse(GUILayout.TextField(GlobalConfig.gameSetting.PosQueueNormalNum.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GlobalConfig.gameSetting.TweenSpeedUpFactor = float.Parse(GUILayout.TextField(GlobalConfig.gameSetting.TweenSpeedUpFactor.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GameSetting.fireRateMsg = ToggleButton(GameSetting.fireRateMsg, "fireRateMsg", GUILayout.Height(30));
            //GameSetting.enableBattleUI = ToggleButton(GameSetting.enableBattleUI, "UI", GUILayout.Height(40));
            GameSetting.enableSmallMap = ToggleButton(GameSetting.enableSmallMap, "SmallUI", GUILayout.Height(40));
            //GameSetting.enableRigRot = ToggleButton(GameSetting.enableRigRot, "RigRot", GUILayout.Height(40));
            Util.AlwaysSmallMem = ToggleButton(Util.AlwaysSmallMem, "SmallRam", GUILayout.Height(40));
            AntiCheatManager.Enable = ToggleButton(AntiCheatManager.Enable, "CheckCheat", GUILayout.Height(40));

            GUILayout.EndVertical();

            //GUILayout.BeginVertical();

            //if (GUILayout.Button("EnableItemLP"))
            //    SceneManager.singleton.EnableUseLightProbe(true);
            //if (GUILayout.Button("DisItemLP"))
            //    SceneManager.singleton.EnableUseLightProbe(false);
            //if (GUILayout.Button("RefMat"))
            //    SceneManager.singleton.RefreshItemMat();
            //if (GUILayout.Button("ShowLPMsg"))
            //    SceneManager.singleton.ShowLPMsg();

            //if (GUILayout.Button("OpenKey"))
            //    SceneManager.singleton.EnableLightProbeKeyWord(true);
            //if (GUILayout.Button("CloseKey"))
            //    SceneManager.singleton.EnableLightProbeKeyWord(false);

            // GUILayout.EndVertical();

            GUILayout.BeginVertical();
            
            platform_api_cmd = GUILayout.TextArea(platform_api_cmd, GUILayout.Height(80), GUILayout.Width(160));
            if (GUILayout.Button("PlatformAPI.Call", GUILayout.Height(40), GUILayout.Width(160)))
            {
                if (!string.IsNullOrEmpty(platform_api_cmd))
                {
                    var param_data = new List<string>(platform_api_cmd.Split(new char[]{ ',', '\n' }));
                    var method = param_data[0];
                    param_data.RemoveAt(0);
                    PlatformAPI.Call(method, param_data.ToArray());
                }
            }
            GUILayout.EndVertical();


            GUILayout.BeginVertical();

            GameSetting.openCheckPosMsg = ToggleButton(GameSetting.openCheckPosMsg, "ChckPosMsg", GUILayout.Height(30));
            GameSetting.openFireHitInfo = ToggleButton(GameSetting.openFireHitInfo, "HitInfo", GUILayout.Height(30));
            if (GUILayout.Button("VtfTest"))
                VertificateUtil.GMTest();
            if(GUILayout.Button("BuildingShader"))
            {
                string msg = "";
                Renderer[] arrRender = SceneManager.singleton.sceneRenderer;
                foreach(Renderer r in arrRender)
                {
                    msg += r.sharedMaterial.shader.name + " " + r.sharedMaterial.shader.renderQueue + " " + r.sharedMaterial.shader.GetHashCode() + " " + "\n";
                }

                Logger.Log(msg);
            }

            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
            
        GUILayout.BeginHorizontal();
        if (ShowRow("前端2"))
        {
            if (GUILayout.Button("FPS", GUILayout.Height(40)))
                Driver.singleton.gameObject.AddMissingComponent<MonoRunInfo>();

            GUILayout.BeginVertical();
            GlobalConfig.gameSetting.SendPosInterval = float.Parse(GUILayout.TextField(GlobalConfig.gameSetting.SendPosInterval.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GameSetting.MPSpeedFactor = float.Parse(GUILayout.TextField(GameSetting.MPSpeedFactor.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GameSetting.MPFireFactor = float.Parse(GUILayout.TextField(GameSetting.MPFireFactor.ToString(), GUILayout.Height(20), GUILayout.Width(80)));
            GUILayout.EndVertical();

            //if(GUILayout.Button("关闭角色CC", GUILayout.Height(40)))
            //    WorldManager.singleton.EnableRoleBodyPartCollider(false);

            //if(GUILayout.Button("开启角色CC", GUILayout.Height(40)))
            //    WorldManager.singleton.EnableRoleBodyPartCollider(true);

            NetLayer.SHOW_PROTO = ToggleButton(NetLayer.SHOW_PROTO, "网络日志", GUILayout.Height(40));
            Logger.IsShowLog = ToggleButton(Logger.IsShowLog, "Logger开关", GUILayout.Height(40));

            if (GUILayout.Button("清除指引进度", GUILayout.Height(40)))
            {
                GlobalConfig.clientValue.guideChapterName = "";
                GlobalConfig.SaveClientValue("guideChapterName");
                Logger.Log("保存指引进度：" + GlobalConfig.clientValue.guideChapterName);
            }


          
            m_editorGuide = ToggleButton(m_editorGuide, "继续指引", GUILayout.Height(40));

            if (GUILayout.Button("+++++++", GUILayout.Height(40)))
                Logger.Error("UpdateFrameInterval: " + ++GameSetting.smallMapUpdateFrameInterval);

            if (GUILayout.Button("-------", GUILayout.Height(40)))
                Logger.Error("UpdateFrameInterval: " + --GameSetting.smallMapUpdateFrameInterval);

            PanelBattle.m_useRotationRough = ToggleButton(PanelBattle.m_useRotationRough, "转向方式2", GUILayout.Height(40));
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (ShowRow("前端3"))
        {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            var tmpParamsDebug = ToggleButton(m_paramsDebug, "参数调整", GUILayout.Height(40));
            if (tmpParamsDebug != m_paramsDebug)
            {
                m_paramsDebug = tmpParamsDebug;
                if (m_paramsDebug)
                    Driver.singleton.gameObject.AddMissingComponent<BinaryRotateAssistor>();
                else
                    GameObject.Destroy(Driver.singleton.GetComponent<BinaryRotateAssistor>());
            }
            AudioManager.MusicMute = ToggleButton(AudioManager.MusicMute, "音乐开关", GUILayout.Height(40));
            Util.AlwaysSmallMem = ToggleButton(Util.AlwaysSmallMem, "小内存", GUILayout.Height(40));
            GlobalConfig.showRecoilDebugInfo = ToggleButton(GlobalConfig.showRecoilDebugInfo, "后座信息", GUILayout.Height(40));
            GlobalConfig.showSpreadDebugInfo = ToggleButton(GlobalConfig.showSpreadDebugInfo, "散射信息", GUILayout.Height(40));
            GlobalConfig.showHitDebugInfo = ToggleButton(GlobalConfig.showHitDebugInfo, "子弹击中数据", GUILayout.Height(40));
            LogItem.isOpen = ToggleButton(LogItem.isOpen, "开启Log",GUILayout.Height(40));
            if (LogItem.isOpen )
            {
                if (!LogItem.isRegis)
                {
                    Driver.RegisterLogCallback(LogItem.HandleLog);
                    LogItem.isRegis = true;
                }
            }
                
            if (!LogItem.isOpen)
                LogItem.logQueue.Clear();
            QuickTurnManager.Ins.isShowGUI = ToggleButton(QuickTurnManager.Ins.isShowGUI,"快速转身",GUILayout.Height(40));
            //WorldManager.singleton.GMAITest = ToggleButton(WorldManager.singleton.GMAITest, "boss技能", GUILayout.Height(40));
            GUILayout.BeginVertical();
            if (GUILayout.Button("skill1", GUILayout.Width(50)))
            {
                WorldManager.singleton.GMClientAISkill(1);
            }
            if (GUILayout.Button("skill2", GUILayout.Width(50)))
            {
                WorldManager.singleton.GMClientAISkill(2);
            }
            if (GUILayout.Button("skill4", GUILayout.Width(50)))
            {
                WorldManager.singleton.GMClientAISkill(4);
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            m_playStory = ToggleButton(m_playStory, "播放剧情", GUILayout.Width(80), GUILayout.Height(40));
            m_showObfuscate = ToggleButton(m_showObfuscate, "混淆查看", GUILayout.Width(80), GUILayout.Height(40));
            if (m_showObfuscate)
            {
                ShowObfuscate();
            }

            m_useSkill = ToggleButton(m_useSkill, "使用技能", GUILayout.Width(80), GUILayout.Height(40));
            if((m_drawRange = ToggleButton(m_drawRange, "画攻击范围", GUILayout.Width(80), GUILayout.Height(40))))
            {
                m_skillRangeTest.enabled = m_drawRange;
            }

            if ((m_showShader = ToggleButton(m_showShader, "检测透视", GUILayout.Width(80), GUILayout.Height(40))))
            {
                if (CheckShader.singleton != null)
                    CheckShader.singleton.CheckShaderTest(m_showShader);
            }
            else
            {
                if (CheckShader.singleton != null)
                    CheckShader.singleton.CheckShaderTest(m_showShader);
            }


            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        GUILayout.EndHorizontal();
    }

    public static bool m_paramsDebug;
    public static bool m_playStory = false;
    public static bool m_showObfuscate = false;
    public static string m_storyId = "";
    public static bool m_useSkill = false;
    public static string m_skillId = "";

    public static bool m_drawRange;
    public static string m_drawRangeParam1 = "";
    public static string m_drawRangeParam2 = "";

    public static bool m_showShader = false;

    private bool ShowSlider(string name, ref float value, int min, int max)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(140);
        GUILayout.Label(name);
        float old = value;
        value = GUILayout.HorizontalSlider(value, min, max, GUILayout.Width(400));
        GUILayout.Label(value.ToString("f2"));
        GUILayout.EndHorizontal();
        return value != old;
    }

    private bool ToggleButton(bool value, string text, params GUILayoutOption[] options)
    {
        var lastColor = GUI.color;
        if (!value)
            GUI.color = Color.red;
        else
            GUI.color = Color.green;
        var result = GUILayout.Toggle(value, text, GUI.skin.button, options);
        GUI.color = lastColor;
        return result;
    }

    bool ShowRow(string name)
    {
        GUILayout.Space(120);
        if (GUILayout.Button(name, GUILayout.Height(40), GUILayout.Width(40)))
        {
            if (mShowTab != name)
                mShowTab = name;
            else
                mShowTab = "";
        }
        return mShowTab == name;
    }

    private void ShowObfuscate()
    {
        GUILayout.Label("是否混淆:" + (typeof(BasePlayer).Name != "BasePlayer"), GUILayout.Height(40), GUILayout.Width(60));
        if (GUILayout.Button("打印当前堆栈", GUILayout.Height(40), GUILayout.Width(120)))
        {
            Debug.LogError("打印当前堆栈:\n" + StackTraceUtility.ExtractStackTrace());
        }
    }

    static void Toc_player_gm(toc_player_gm proto)
    {
        UIGmPanel panel = Driver.singleton.GetComponent<UIGmPanel>();
        if (panel == null)
            panel = Driver.singleton.gameObject.AddComponent<UIGmPanel>();
        panel.mServerGmList = proto.list;
    }
}

public class LogItem
{
    public static bool isOpen=false;
    public static bool isShow=false;
    public static bool isShowDetail=false;
    public static bool isRegis = false;
    public static bool isShowLogType = true;
    public static bool isShowWarningType = false;
    public static bool isShowErrorType = true;
    public static string filterText = "";
    public static Vector2 scrollPos=new Vector2(0,0);
    public string content;
    public string statck;
    public LogType type;
    public const int logMaxCount = 100;
    public static Queue<LogItem> logQueue = new Queue<LogItem>(logMaxCount);
    public static void HandleLog(string str,string stack,LogType type)
    {
        logQueue.Enqueue(new LogItem() { content = str, statck = stack, type = type });
    }
}
