﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelLegionCreate : BasePanel
{
    private InputField m_inputName;
    private int m_iPanel = 1;
    private int m_iColor = 1;
    private int m_iBiaozhi = 1;

    private static int BIAOZHI_MAX = 2;

    private Image m_imgBadge;
    private Text m_txtBiaozhiName;

    private int m_iDiamondCost;
    private int m_iCoinCost;

    private Text m_txtDiamondCost;
    private Text m_txtCoinCost;
    public PanelLegionCreate()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegionCreate");
        AddPreLoadRes("UI/Legion/PanelLegionCreate", EResType.UI);
        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
    }

    public override void Init()
    {
        m_iDiamondCost = ConfigMisc.GetInt("create_corps_reqdiamond");
        m_iCoinCost = ConfigMisc.GetInt("create_legion_reqcoin");
        m_txtDiamondCost = m_tran.Find("diamond_create/num").GetComponent<Text>();
        m_txtDiamondCost.text = m_iDiamondCost.ToString();
        m_txtCoinCost = m_tran.Find("coin_create/num").GetComponent<Text>();
        m_txtCoinCost.text = m_iCoinCost.ToString();

        m_inputName = m_tran.Find("name_input").GetComponent<InputField>(); ;
        m_inputName.gameObject.AddComponent<InputFieldFix>();
        m_imgBadge = m_tran.Find("legion_icon/badge").GetComponent<Image>();
        m_txtBiaozhiName = m_tran.Find("biaozhi_sel/name").GetComponent<Text>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("coin_create/create").gameObject).onPointerClick += OnClkCoinCreate;
        UGUIClickHandler.Get(m_tran.Find("diamond_create/create").gameObject).onPointerClick += OnClkDiamondCreate;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClose;
        UGUIClickHandler.Get(m_tran.Find("biaozhi_sel/biaozhi_left").gameObject).onPointerClick += OnBiaozhiLeft;
        UGUIClickHandler.Get(m_tran.Find("biaozhi_sel/biaozhi_right").gameObject).onPointerClick += OnBiaozhiRight;
    }

    public override void OnShow()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }
   

    protected void OnBiaozhiLeft(GameObject target, PointerEventData eventData)
    {
        if (m_iBiaozhi <= 1)
            return;
        m_iBiaozhi--;
        ResetBiaozhi();
    }

    protected void OnBiaozhiRight(GameObject target, PointerEventData eventData)
    {
        if (m_iBiaozhi >= BIAOZHI_MAX)
            return;
        m_iBiaozhi++;
        ResetBiaozhi();
    }


    protected void ResetBiaozhi()
    {
        string name = "badge_" + m_iBiaozhi.ToString();
        m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Legion, name));

        m_txtBiaozhiName.text = "标志" + m_iBiaozhi.ToString();
    }

    protected void OnClkCoinCreate(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.CheckMoney(m_iCoinCost, EnumMoneyType.COIN, null) == false)
        {
            HidePanel();
            return;
        }

        if (m_inputName.text == "")
        {
            UIManager.ShowTipPanel("还未取名字");
            return;
        }
        tos_legion_create_legion msg = new tos_legion_create_legion();
        msg.icon = m_iBiaozhi;
        msg.legion_name = m_inputName.text;
        msg.money_type = 0;
        NetLayer.Send(msg);
        HidePanel();
    }

    protected void OnClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }
    protected void OnClkDiamondCreate(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.roleData.diamond < m_iDiamondCost)
        if(PlayerSystem.CheckMoney(m_iDiamondCost,EnumMoneyType.DIAMOND,null)==false)
        {
            HidePanel();
            return;
        }

        if (m_inputName.text == "")
        {
            UIManager.ShowTipPanel("还未取名字");
            return;
        }

        tos_legion_create_legion msg = new tos_legion_create_legion();
        msg.icon = m_iBiaozhi;
        msg.legion_name = m_inputName.text;
        msg.money_type = 1;
        NetLayer.Send(msg);
        HidePanel();
    }
}
