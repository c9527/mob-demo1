﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PanelFindLegion : BasePanel
{
    private InputField m_inputSearch;

      public PanelFindLegion()
    {
        SetPanelPrefabPath("UI/Legion/PanelFindLegion");

        AddPreLoadRes("UI/Legion/PanelFindLegion", EResType.UI);
    }

    public override void Init()
    {
        m_inputSearch = m_tran.Find("frame/InputField").GetComponent<InputField>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += (target,event_data) => { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("frame/search_btn")).onPointerClick += OnSearchBtnClick;
    }

    private void OnSearchBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!string.IsNullOrEmpty(m_inputSearch.text))
        {
            PanelLegion.search_legion_key = m_inputSearch.text.Trim();
            NetLayer.Send(new tos_legion_search_name() { legion_name = PanelLegion.search_legion_key });
            m_inputSearch.text = "";
            HidePanel();
        }
        else
        {
            TipsManager.Instance.showTips("请输入正确的军团名称或者ID");
        }
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
