﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelLegionAuthorize : BasePanel
{
    private p_legion_corps_toc m_data;
    private LEGION_MEMBER_TYPE m_selType;
    private Text m_txtCorpsName;
    private Text m_txtLeaderName;
    public PanelLegionAuthorize()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegionAuthorize");
        AddPreLoadRes("UI/Legion/PanelLegionAuthorize", EResType.UI);

        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
    }

    public override void Init()
    {
        //m_selType = LEGION_MEMBER_TYPE.TYPE_MEMBER;
        m_txtCorpsName = m_tran.Find("notice/corps_name").GetComponent<Text>();
        m_txtLeaderName = m_tran.Find("notice/leader_name").GetComponent<Text>();
        
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok").gameObject).onPointerClick += OnClkOK;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("leader").gameObject).onPointerClick += OnSelLeader;
        UGUIClickHandler.Get(m_tran.Find("vice_leader").gameObject).onPointerClick += OnSelViceLeader;
        UGUIClickHandler.Get(m_tran.Find("member").gameObject).onPointerClick += OnSelMember;
    }

    public override void OnShow()
    {
        if (m_params.Length < 1)
            return;

        m_data = m_params[0] as p_legion_corps_toc;
        m_txtCorpsName.text = m_data.name;
        m_txtLeaderName.text = m_data.leader_name;
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }

    protected void OnSelLeader(GameObject target, PointerEventData eventData)
    {
        m_selType = LEGION_MEMBER_TYPE.TYPE_LEADER;
    }

    protected void OnSelViceLeader(GameObject target, PointerEventData eventData)
    {
        m_selType = LEGION_MEMBER_TYPE.TYPE_VICELEADER;
    }

    protected void OnSelMember(GameObject target, PointerEventData eventData)
    {
        m_selType = LEGION_MEMBER_TYPE.TYPE_MEMBER;
    }
   
    protected void OnClkOK(GameObject target, PointerEventData eventData)
    {
        if( m_data.legion_post == (int)m_selType )
        {
            TipsManager.Instance.showTips( "请选择与原来不同的职位!" );
            return;
        }

        if (m_selType == LEGION_MEMBER_TYPE.TYPE_LEADER)
        {
            string info = string.Format("是否转让军团长给{0} 队长 {1}", m_data.name,m_data.leader_name );
            UIManager.ShowTipPanel(info, () => { NetLayer.Send(new tos_legion_set_leader() { leader_cid = m_data.corps_id }); }, null, true, true, "确定", "取消");
        }
        else
        {
            if( m_selType == LEGION_MEMBER_TYPE.TYPE_VICELEADER )
            {
                if( LegionManager.m_lstManager.Count >= 3 )
                {
                    TipsManager.Instance.showTips("副军团长名额已满!");
                    return;
                }
            }
            tos_legion_change_corps_post msg = new tos_legion_change_corps_post();
            msg.corps_id = m_data.corps_id;
            msg.post_type = (int)m_selType;
            NetLayer.Send(msg);
        }
        //HidePanel();
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void Toc_legion_change_corps_post( toc_legion_change_corps_post proto )
    {
        TipsManager.Instance.showTips("修改战队职位成功!");
        HidePanel();
    }

    private void Toc_legion_set_leader( toc_legion_set_leader proto )
    {
        TipsManager.Instance.showTips("设置军团长成功!");
        HidePanel();
    }
}
