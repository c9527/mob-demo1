﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemMrbCorpsInfo : ItemRender
{
    private Text m_txtName;
    private Image m_imgFlag;
    private Image m_imgBiaozhi;
    private p_legion_corps_toc m_data;

    private GameObject m_goBack;
    private GameObject m_goCheckMark;
    private GameObject m_goName;
    private GameObject m_goFlag;
    private GameObject m_goBiaozhi;
    private GameObject m_goEmpty;
    private GameObject m_goAdd;
    private bool m_bClkAdd = false;

    public override void Awake()
    {
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_imgFlag = transform.Find("flag").GetComponent<Image>();
        m_imgBiaozhi = transform.Find("biaozhi").GetComponent<Image>();

        m_goBack = transform.Find("back").gameObject;
        m_goCheckMark = transform.Find("CheckMark").gameObject;
        m_goName = transform.Find("name").gameObject;
        m_goFlag = transform.Find("flag").gameObject;
        m_goBiaozhi = transform.Find("biaozhi").gameObject;
        m_goEmpty = transform.Find("empty").gameObject;
        m_goAdd = transform.Find("add").gameObject;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_legion_corps_toc;
        if( m_bClkAdd )
        {
            UGUIClickHandler.Get(transform).onPointerClick -= OnClkAdd;
            m_bClkAdd = false;
        }
        m_goBack.TrySetActive(true);
        m_goCheckMark.TrySetActive(true);
        m_goFlag.TrySetActive(true);
        m_goBiaozhi.TrySetActive(true);
        m_goEmpty.TrySetActive(false);
        m_goAdd.TrySetActive(false);

        if (m_data.corps_id > 0 )
        {
            m_txtName.text = m_data.name;
            string flag = "f" + m_data.logo_pannel.ToString() + "_" + "c" + m_data.logo_color.ToString();
            m_imgFlag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
            string biaozhi = "biaozhi_" + m_data.logo_icon.ToString();
            m_imgBiaozhi.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        }
        else if( m_data.corps_id == 0)
        {
            m_txtName.text = "";
            m_goCheckMark.TrySetActive(false);
            m_goFlag.TrySetActive(false);
            m_goBiaozhi.TrySetActive(false);
            m_goEmpty.TrySetActive(true);
        }
        else if( m_data.corps_id == -1 )
        {
            m_txtName.text = "";
            m_goBack.TrySetActive(false);
            m_goCheckMark.TrySetActive(false);
            m_goFlag.TrySetActive(false);
            m_goBiaozhi.TrySetActive(false);
            m_goAdd.TrySetActive(true);
            UGUIClickHandler.Get(transform).onPointerClick += OnClkAdd;
            m_bClkAdd = true;
        }
    }

    protected void OnClkAdd(GameObject target, PointerEventData eventData)
    {
        if (!LegionManager.IsLeader(PlayerSystem.roleId))
            return;

        UIManager.PopPanel<PanelLegionEnlarge>(null, true, UIManager.GetPanel<PanelLegion>());
    }
}
