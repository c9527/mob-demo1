﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ItemOrganize : ItemRender
{
    private Text m_txtCorpsName;
    private Image m_imgFlag;
    private Image m_imgBiaozhi;
    private p_legion_corps_toc m_data;
    private bool m_bRegEvent = false;
    public override void Awake()
    {
        m_txtCorpsName = transform.Find("name").GetComponent<Text>();
        m_imgFlag = transform.Find("flag").GetComponent<Image>();
        m_imgBiaozhi = transform.Find("biaozhi").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    { 
        m_data = data as p_legion_corps_toc;
        m_txtCorpsName.text = m_data.name;
        string flag = "f" + m_data.logo_pannel.ToString() + "_" + "c" + m_data.logo_color.ToString();
        m_imgFlag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flag));
        string biaozhi = "biaozhi_" + m_data.logo_icon.ToString();
        m_imgBiaozhi.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhi));
        if (!m_bRegEvent)
        {
            UGUIClickHandler.Get(transform).onPointerClick += OnClkCorpsLookup;
            m_bRegEvent = true;
        }
            
    }

    protected void OnClkCorpsLookup(GameObject target, PointerEventData eventData)
    {
        tos_corps_view_info msg = new tos_corps_view_info();
        msg.corps_id = m_data.corps_id;
        NetLayer.Send(msg);
    }

    //private void Toc_corps_view_info(toc_corps_view_info proto)
    //{
    //    UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, UIManager.GetPanel<PanelLegionView>() );
    //}
}
	