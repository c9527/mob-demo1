﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelLegion : BasePanel
{
    private Toggle m_toggleLegionList;
    private Toggle m_toggleLegionMembers;
    private Toggle m_toggleLegionHomepage;

    private GameObject m_goLegionList;
    private GameObject m_goLegionMembers;
    private GameObject m_goLegionHomepage;

    private Button m_btnApply;
    private Button m_btnAuthorrize;
    private Button m_btnKickout;
    private Button m_btnQuit;

    private DataGrid m_legionListDataGrid;
   
    private int m_iCurLegionListPageNum;
    public static string search_legion_key;

    private LegionData m_legionData;

    private Text m_txtLegionName;
    private Image m_imgBadge;
    private Text m_txtLegionId;
    private Text m_txtLegionLvl;
    private Text m_txtLegionCapacity;
    private Text m_txtLegionActiveness;
    private Text m_txtLegionNotice;
    private GameObject m_goLeader;
    private GameObject[] m_goViceLeader = new GameObject[2];

    private DataGrid m_corpsListDataGrid;
    private DataGrid m_memberListDataGrid;
    private GameObject m_goMrbFrameCorps;
    private GameObject m_goMrbFramePlayer;
    private int m_iCurPlayerListPageNum;
    protected enum FRAME_TYPE
    {
        TYPE_HOMEPAGE = 1,
        TYPE_MEMBER_LIST = 2,
        TYPE_LEGION_LIST = 3
    }
    private FRAME_TYPE m_curFrame = FRAME_TYPE.TYPE_HOMEPAGE;
    protected enum MEMBER_FRAME_TYPE
    {
        TYPE_CORPS = 1,
        TYPE_PLAYER = 2
    }

    private MEMBER_FRAME_TYPE m_curMrbFrame = MEMBER_FRAME_TYPE.TYPE_CORPS;
    private List<p_legion_corps_toc> m_lstViewCorps;
    //private List<p_legion_corps_toc> m_lstManagerCorps;

    private GameObject m_goBubble;
    private Text m_txtBubble;

    private int CREATE_LEGION_FIGHTING;
    public PanelLegion()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegion");
        AddPreLoadRes("UI/Legion/PanelLegion", EResType.UI);
        AddPreLoadRes("UI/Legion/ItemLegion", EResType.UI);
        AddPreLoadRes("UI/Legion/ItemMrbCorps", EResType.UI);
        AddPreLoadRes("UI/Legion/ItemMrbPlayer", EResType.UI);
        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        CREATE_LEGION_FIGHTING = ConfigMisc.GetInt("create_legion_reqfighting");
        m_legionData = LegionManager.m_legionData;
        m_toggleLegionList = m_tran.Find("ToggleLegionList").GetComponent<Toggle>();
        m_toggleLegionMembers = m_tran.Find("ToggleLegionMembers").GetComponent<Toggle>();
        m_toggleLegionHomepage = m_tran.Find("ToggleLegionHomepage").GetComponent<Toggle>();

        m_goLegionList = m_tran.Find("frame_legion_list").gameObject;
        m_goLegionMembers = m_tran.Find("frame_legion_members").gameObject;
        m_goLegionHomepage = m_tran.Find("frame_legion_homepage").gameObject;

        m_goMrbFrameCorps = m_tran.Find("frame_legion_members/frame_corps").gameObject;
        m_goMrbFramePlayer = m_tran.Find("frame_legion_members/frame_player").gameObject;

        m_btnApply = m_tran.Find("frame_legion_homepage/apply").GetComponent<Button>();
        m_btnAuthorrize = m_tran.Find("frame_legion_members/frame_corps/operation/give_position").GetComponent<Button>();
        m_btnKickout = m_tran.Find("frame_legion_members/frame_corps/operation/kick_out").GetComponent<Button>();
        m_btnQuit = m_tran.Find("frame_legion_homepage/quit").GetComponent<Button>();

        m_tran.Find("frame_legion_list/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_legionListDataGrid = m_tran.Find("frame_legion_list/ScrollDataGrid/content").GetComponent<DataGrid>();
        m_legionListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Legion/ItemLegion"), typeof(ItemLegionInfo));
        m_legionListDataGrid.useLoopItems = true;
        m_iCurLegionListPageNum = 1;

        m_txtLegionName = m_tran.Find("frame_legion_homepage/icon_kuang/name").GetComponent<Text>();
        m_imgBadge =  m_tran.Find("frame_legion_homepage/icon_kuang/badge").GetComponent<Image>();
        m_txtLegionId = m_tran.Find("frame_legion_homepage/legion_data/legion_id/value").GetComponent<Text>();
        m_txtLegionLvl =  m_tran.Find("frame_legion_homepage/legion_data/legion_lvl/value").GetComponent<Text>();
        m_txtLegionCapacity = m_tran.Find("frame_legion_homepage/legion_data/legion_capacity/value").GetComponent<Text>();
        m_txtLegionActiveness = m_tran.Find("frame_legion_homepage/legion_data/legion_activeness/value").GetComponent<Text>();
        m_txtLegionNotice = m_tran.Find("frame_legion_homepage/bulitin/content").GetComponent<Text>();
        m_goLeader = m_tran.Find("frame_legion_homepage/official/leader").gameObject;
        m_goViceLeader[0] = m_tran.Find("frame_legion_homepage/official/vice_leader_1").gameObject;
        m_goViceLeader[1] = m_tran.Find("frame_legion_homepage/official/vice_leader_2").gameObject;
        m_goBubble = m_tran.Find("frame_legion_homepage/apply/bubble_tip").gameObject;
        m_txtBubble = m_tran.Find("frame_legion_homepage/apply/bubble_tip/Text").GetComponent<Text>();

        m_tran.Find("frame_legion_members/frame_corps/ScrollDataCorps/content").gameObject.AddComponent<DataGrid>();
        m_corpsListDataGrid = m_tran.Find("frame_legion_members/frame_corps/ScrollDataCorps/content").GetComponent<DataGrid>();
        m_corpsListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Legion/ItemMrbCorps"), typeof(ItemMrbCorpsInfo));
        m_corpsListDataGrid.useLoopItems = true;

        m_tran.Find("frame_legion_members/frame_player/ScrollDataPlayer/content").gameObject.AddComponent<DataGrid>();
        m_memberListDataGrid = m_tran.Find("frame_legion_members/frame_player/ScrollDataPlayer/content").GetComponent<DataGrid>();
        m_memberListDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Legion/ItemMrbPlayer"), typeof(ItemMrbPlayerInfo));
        m_memberListDataGrid.useLoopItems = true;

        m_iCurPlayerListPageNum = 1;
        m_lstViewCorps = new List<p_legion_corps_toc>();
        //m_lstManagerCorps = new List<p_legion_corps_toc>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ToggleLegionList").gameObject).onPointerClick += OnClkToggleFrame;
        UGUIClickHandler.Get(m_tran.Find("ToggleLegionMembers").gameObject).onPointerClick += OnClkToggleFrame;
        UGUIClickHandler.Get(m_tran.Find("ToggleLegionHomepage").gameObject).onPointerClick += OnClkToggleFrame;

        UGUIClickHandler.Get(m_tran.Find("frame_legion_homepage/log").gameObject).onPointerClick += OnClkLegionLog;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_homepage/apply").gameObject).onPointerClick += OnClkLegionApply;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_homepage/quit").gameObject).onPointerClick += OnClkLegionQuit;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_homepage/bulitin/edit").gameObject).onPointerClick += OnClkBulitinModify;

        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_corps/operation/look_up").gameObject).onPointerClick += OnClkMemberFrame;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_corps/operation/corps_info").gameObject).onPointerClick += OnCorpsLookUp;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_corps/operation/kick_out").gameObject).onPointerClick += OnClkMemberKickout;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_corps/operation/give_position").gameObject).onPointerClick += OnClkMemberAuthorize;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_player/operation/return_list").gameObject).onPointerClick += OnClkMemberFrame;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_player/operation/look_up").gameObject).onPointerClick += OnClkPlayerLookup;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_player/operation/page/left").gameObject).onPointerClick += OnClkPlayerListPagedown;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_members/frame_player/operation/page/right").gameObject).onPointerClick += OnClkPlayerListPageup;

        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/search_btn").gameObject).onPointerClick += OnClkLegionFind;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/look_up").gameObject).onPointerClick += OnClkLegionLookup;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/my_legion").gameObject).onPointerClick += OnClkLegionMy;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/add_in").gameObject).onPointerClick += OnClkLegionAdd;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/page/left").gameObject).onPointerClick += OnClkLegionListdown;
        UGUIClickHandler.Get(m_tran.Find("frame_legion_list/operation/page/right").gameObject).onPointerClick += OnClkLegionListPageup;

        UGUIClickHandler.Get(m_goLeader).onPointerClick += OnClkManager;
        UGUIClickHandler.Get(m_goViceLeader[0]).onPointerClick += OnClkManager;
        UGUIClickHandler.Get(m_goViceLeader[1]).onPointerClick += OnClkManager;
        AddEventListener(GameEvent.UI_ROLE_ICON_CHANGED, OnChangePlayerIcon);
    }

    public override void OnShow()
    {
        UpdateView();
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelSocity>();
    }
    public override void OnDestroy()
    {
       
    }

    public void UpdateView()
    {
        if ( LegionManager.m_legionData.legion_id == 0)
        {
            Util.SetGrayShader(m_toggleLegionMembers);
            Util.SetGrayShader(m_toggleLegionHomepage);
            SwitchFrame(FRAME_TYPE.TYPE_LEGION_LIST);
        }
        else
        {
            Util.SetNormalShader(m_toggleLegionMembers);
            Util.SetNormalShader(m_toggleLegionHomepage);
            SwitchFrame(m_curFrame);
        }        
    }

    private void OnChangePlayerIcon()
    {
        if (m_curFrame == FRAME_TYPE.TYPE_HOMEPAGE && LegionManager.IsManager())
        {
            p_legion_corps_toc corps = GetMyselfManager();
            if (corps != null )
            {
                corps.leader_icon = PlayerSystem.roleData.icon;
                UpdateHomePage();
            }           
        }
    }
    private void UpdateHomePage()
    {
        m_txtLegionName.text = m_legionData.name;
        string badge = "badge_" + m_legionData.logo_icon.ToString();
        m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Legion, badge));
        m_txtLegionId.text = m_legionData.legion_id.ToString();
        //m_txtLegionLvl.text = LegionManager.GetLegionLvlStr(m_legionData.activeness);
        m_txtLegionLvl.text = LegionManager.GetConfigLegionLevelLine(m_legionData.activeness).Disp;
        m_txtLegionCapacity.text = m_legionData.sum_mrb.ToString() + "/" + (m_legionData.capacity * CorpsDataManager.CORPS_PLAYER_NUM).ToString();
        m_txtLegionActiveness.text = m_legionData.activeness.ToString() + "/" + LegionManager.GetUpgradeActiveness(m_legionData.activeness).ToString();
        m_txtLegionNotice.text = m_legionData.notice;

        if (CorpsDataManager.IsLeader())
        {
            Util.SetNormalShader(m_btnQuit);
        }
        else
        {
            Util.SetGrayShader(m_btnQuit);
        }

        GameObject[] goArrLeaders = { m_goLeader, m_goViceLeader[0], m_goViceLeader[1] };
        for (int idx = 0; idx < goArrLeaders.Length; ++idx)
        {
            goArrLeaders[idx].transform.FindChild("head").gameObject.TrySetActive(false);
            goArrLeaders[idx].transform.FindChild("name").GetComponent<Text>().text = "";
            goArrLeaders[idx].transform.FindChild("unknow").gameObject.TrySetActive(true);
        }

        int viceIdx = 0;
        for( int i =0; i < LegionManager.m_lstManager.Count; ++i )
        {
            p_legion_corps_toc corps = LegionManager.m_lstManager[i];   
            GameObject goManager = null;

            if (corps.legion_post == (int)LEGION_MEMBER_TYPE.TYPE_LEADER)
            {
                goManager = m_goLeader;
            }
            else if (corps.legion_post == (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
            {
                goManager = m_goViceLeader[viceIdx];
                viceIdx++;
            }

            if (goManager != null)
            {
                //m_lstManagerCorps.Add(corps);
                goManager.transform.FindChild("name").GetComponent<Text>().text = corps.leader_name;
                goManager.transform.FindChild("head").gameObject.TrySetActive(true);
                goManager.transform.FindChild("unknow").gameObject.TrySetActive(false);
                goManager.transform.FindChild("head").GetComponent<Image>().SetSprite(ResourceManager.LoadRoleIconBig(corps.leader_icon));
                goManager.transform.FindChild("head").GetComponent<Image>().SetNativeSize();
                goManager.transform.FindChild("id").GetComponent<Text>().text = corps.leader_id.ToString();
            }
        }
        if( LegionManager.IsManager() )
        {
            Util.SetNormalShader(m_btnApply);
            if( m_legionData.m_lstApply.Count > 0 )
            {
                m_goBubble.TrySetActive(true);
                m_txtBubble.text = m_legionData.m_lstApply.Count.ToString();
            }
            else
            {
                m_goBubble.TrySetActive(false);
            }
        }
        else
        {
            Util.SetGrayShader(m_btnApply);
            m_goBubble.TrySetActive(false);
        }

        if (UIManager.IsOpen<PanelLegionApply>())
        {
            UIManager.GetPanel<PanelLegionApply>().UpdateView(LegionManager.m_legionData.m_lstApply);
        }
    }

    protected void OnClkManager( GameObject target, PointerEventData eventData )
    {
        if (target == null)
            return;

        long id = long.Parse(target.transform.FindChild("id").GetComponent<Text>().text);
        PlayerFriendData.ViewPlayer(id);
    }


    private p_legion_corps_toc GetMyselfManager()
    {
        for (int i = 0; i < LegionManager.m_lstManager.Count; i++)
        {
            if (LegionManager.m_lstManager[i].leader_id == PlayerSystem.roleId)
                return LegionManager.m_lstManager[i];
        }
        return null;
    }
    private void UpdateLegionMember()
    {
        if (LegionManager.IsLeader( PlayerSystem.roleId) )
        {
            Util.SetNormalShader(m_btnAuthorrize);
            Util.SetNormalShader(m_btnKickout);
        }
        else
        {
            Util.SetGrayShader(m_btnAuthorrize);
            Util.SetGrayShader(m_btnKickout);
        }

        if (m_curMrbFrame == MEMBER_FRAME_TYPE.TYPE_CORPS)
        {
            m_goMrbFrameCorps.TrySetActive(true);
            m_goMrbFramePlayer.TrySetActive(false);
            m_lstViewCorps.Clear();
            m_lstViewCorps.AddRange(m_legionData.m_lstCorps);
         
            int iMaxCorpsNum = LegionManager.GetConfigLegionLevelLine(m_legionData.activeness).MaxCorps;
            bool bAdd = false;
            if( iMaxCorpsNum - m_legionData.capacity > 0 )
                bAdd = true;

            int iEmpty = m_legionData.capacity - m_legionData.m_lstCorps.Count;

            if (iEmpty > 0)
            {
                for (int i = 0; i < iEmpty; ++i)
                {
                    p_legion_corps_toc legion_corps = new p_legion_corps_toc();
                    legion_corps.corps_id = 0;
                    m_lstViewCorps.Add(legion_corps);
                }
            }

            if (bAdd)
            {
                p_legion_corps_toc legion_corps = new p_legion_corps_toc();
                legion_corps.corps_id = -1;
                m_lstViewCorps.Add(legion_corps);
            }

            m_corpsListDataGrid.Data = m_lstViewCorps.ToArray();
        }
        else if (m_curMrbFrame == MEMBER_FRAME_TYPE.TYPE_PLAYER)
        {
            m_goMrbFrameCorps.TrySetActive(false);
            m_goMrbFramePlayer.TrySetActive(true);
            if (LegionManager.m_dicPlayerList.ContainsKey(m_iCurPlayerListPageNum))
            {
                List<p_legion_member_toc> lstPlayer = LegionManager.m_dicPlayerList[m_iCurPlayerListPageNum];
                m_memberListDataGrid.Data = lstPlayer.ToArray();

            }
            else
            {
                LegionManager.RequestPlayerList(m_iCurPlayerListPageNum);
            }

            ResetPlayerListPage();
        }
    }

    private void UpdateLegionList()
    {
        if (LegionManager.m_legionData.legion_id == 0)
        {
            m_goLegionList.transform.FindChild("operation/my_legion/Text").GetComponent<Text>().text = "创建军团";
        }
        else
        {
            m_goLegionList.transform.FindChild("operation/my_legion/Text").GetComponent<Text>().text = "我的军团";
        }

        if (LegionManager.m_dicLegion.ContainsKey(m_iCurLegionListPageNum))
        {
            ResetLegionListPage();
            List<BaseLegionData> lstLegion = LegionManager.m_dicLegion[m_iCurLegionListPageNum];
            m_legionListDataGrid.Data = lstLegion.ToArray();
            if (!string.IsNullOrEmpty(search_legion_key))
            {
                var key = search_legion_key;
                search_legion_key = null;
                var index_for_id = -1;
                var index_for_name = -1;
                for (int i = 0; i < lstLegion.Count; i++)
                {
                    if (lstLegion[i].data.legion_id.ToString() == key)
                    {
                        index_for_id = i;
                        break;
                    }
                    if (lstLegion[i].data.name == key)
                    {
                        index_for_name = i;
                    }
                }
                var index = index_for_id != -1 ? index_for_id : index_for_name;
                if (index != -1)
                {
                    m_legionListDataGrid.Select(index);
                    m_legionListDataGrid.ResetScrollPosition(index);
                }
            }
        }
    }

    protected void SwitchFrame(FRAME_TYPE eType)
    {
        m_curFrame = eType;

        m_toggleLegionList.isOn = false;
        m_toggleLegionMembers.isOn = false;
        m_toggleLegionHomepage.isOn = false;
        m_goLegionList.TrySetActive(false);
        m_goLegionMembers.TrySetActive(false);
        m_goLegionHomepage.TrySetActive(false);

        if (m_curFrame == FRAME_TYPE.TYPE_HOMEPAGE)
        {
            m_toggleLegionHomepage.isOn = true;
            m_goLegionHomepage.TrySetActive(true);
            UpdateHomePage();
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_MEMBER_LIST)
        {
            m_toggleLegionMembers.isOn = true;
            m_goLegionMembers.TrySetActive(true);
            UpdateLegionMember();
        }
        else if (m_curFrame == FRAME_TYPE.TYPE_LEGION_LIST)
        {
            m_toggleLegionList.isOn = true;
            m_goLegionList.TrySetActive(true);
            UpdateLegionList();
        }
    }

    protected void OnClkToggleFrame(GameObject target, PointerEventData eventData)
    {
        if( target.name == "ToggleLegionHomepage" )
        {
            if( LegionManager.m_legionData.legion_id == 0 )
            {
                m_toggleLegionList.isOn = true;
                m_toggleLegionHomepage.isOn = false;
                TipsManager.Instance.showTips("你还未加入军团!");
                return;
            }
            SwitchFrame(FRAME_TYPE.TYPE_HOMEPAGE);
        }
        else if( target.name == "ToggleLegionMembers" )
        {
            if (LegionManager.m_legionData.legion_id == 0)
            {
                m_toggleLegionList.isOn = true;
                m_toggleLegionMembers.isOn = false;
                TipsManager.Instance.showTips("你还未加入军团!");
                return;
            }
            SwitchFrame(FRAME_TYPE.TYPE_MEMBER_LIST);
        }
        else if (target.name == "ToggleLegionList")
        {
            SwitchFrame(FRAME_TYPE.TYPE_LEGION_LIST);
        }
    }

    protected void OnClkLegionLog(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelLegionLog>(new object[] { m_legionData.m_lstLog }, true, this);
    }

    protected void OnClkLegionApply(GameObject target, PointerEventData eventData)
    {
        if (LegionManager.m_legionData.legion_id == 0)
            return;

          if (CorpsDataManager.m_corpsData.corps_id == 0)
            return;

        p_legion_corps_toc self = LegionManager.GetCorps(CorpsDataManager.m_corpsData.corps_id);
        if (self == null)
            return;

        if (self.legion_post > (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
        {
            TipsManager.Instance.showTips("无权限同意申请");
            return;
        }
        UIManager.PopPanel<PanelLegionApply>(new object[] { LegionManager.m_legionData.m_lstApply }, true, this);
      
    }

    protected void OnClkLegionQuit(GameObject target, PointerEventData eventData)
    {
        if (LegionManager.m_legionData.legion_id == 0)
            return;

        if (!CorpsDataManager.IsLeader())
        {
            TipsManager.Instance.showTips("战队队长才有资格退出军团!");
        }
        if (LegionManager.IsLeader(PlayerSystem.roleId) )
        {
            if (LegionManager.m_legionData.m_lstCorps.Count <= 1)
            {
                string info = "军团解散后经验值清0，无法恢复\n 确定要退出并解散该军团吗？";
                UIManager.ShowTipPanel(info, () => { NetLayer.Send(new tos_legion_dismiss()); }, null, true, true, "确定", "取消");
            }
            else
            {
                UIManager.ShowTipPanel("团长必须先移交团长职务，才能退出军团");
            }
            return;
        }
        string strDesc = "是否退出军团";
        UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_legion_quit()); }, null, true, true, "确定", "取消");
    }

    private static void Toc_legion_quit(toc_legion_quit proto )
    {
        TipsManager.Instance.showTips("你已退出军团!");
        LegionManager.OnLeaveLegion();
    }

    protected void OnClkMemberFrame( GameObject target, PointerEventData eventData )
    {
        if (m_curMrbFrame == MEMBER_FRAME_TYPE.TYPE_CORPS)
            m_curMrbFrame = MEMBER_FRAME_TYPE.TYPE_PLAYER;
        else
            m_curMrbFrame = MEMBER_FRAME_TYPE.TYPE_CORPS;

        UpdateLegionMember();
    }

    protected void ResetPlayerListPage()
    {
        m_tran.Find("frame_legion_members/frame_player/operation/page/page").GetComponent<Text>().text = m_iCurPlayerListPageNum.ToString() + "/" + LegionManager.m_iPlayerListPageNum.ToString();
    }
    protected void OnClkPlayerListPageup(GameObject target, PointerEventData eventData)
    {
        if (m_iCurPlayerListPageNum >= LegionManager.m_iPlayerListPageNum)
        {
            TipsManager.Instance.showTips("已经是最后一页了");
            return;
        }

        m_iCurPlayerListPageNum++;
        OnChangePlayerListPageNum();
    }

    protected void OnClkPlayerListPagedown(GameObject target, PointerEventData eventData)
    {
        if (m_iCurPlayerListPageNum <= 1)
        {
            TipsManager.Instance.showTips("已经是第一页了");
            return;
        }
        m_iCurPlayerListPageNum--;
        OnChangePlayerListPageNum();
    }

    protected void OnChangePlayerListPageNum()
    {
        if (LegionManager.m_dicLegion.ContainsKey(m_iCurPlayerListPageNum))
            UpdateLegionMember();

        else
            LegionManager.RequestPlayerList(m_iCurPlayerListPageNum);
    }

    protected void OnClkPlayerLookup(GameObject target, PointerEventData eventData)
    {
        var item = m_memberListDataGrid.SelectedData<p_legion_member_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择查看信息的人");
            return;
        }

        if (item.id == PlayerSystem.roleId)
        {
            return;
        }

        PlayerFriendData.ViewPlayer(item.id);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    protected void OnClkBulitinModify(GameObject target, PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData.corps_id == 0)
            return;

        p_legion_corps_toc self = LegionManager.GetCorps(CorpsDataManager.m_corpsData.corps_id);
        if (self == null)
            return;

        if (self.legion_post > (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
        {
            TipsManager.Instance.showTips("无权限修改公告");
            return;
        }

        UIManager.PopPanel<PanelLegionBulitin>(null, true, this);
    }

    protected void OnCorpsLookUp(GameObject target, PointerEventData eventData)
    {
        var item = m_corpsListDataGrid.SelectedData<p_legion_corps_toc>();
        if (item == null)
        {
            TipsManager.Instance.showTips("请选择查看信息的战队");
            return;
        }

        if (item.corps_id == CorpsDataManager.m_corpsData.corps_id)
        {
            return;
        }
     
        NetLayer.Send(new tos_corps_view_info() { corps_id = item.corps_id});
        string strDesc = string.Format("是否将{0}请出军团", item.name);
    }

    private void Toc_corps_view_info(toc_corps_view_info proto)
    {
        UIManager.PopPanel<PanelCorpsView>(new object[] { proto }, true, this);
    }

   
    protected void OnClkMemberAuthorize(GameObject target, PointerEventData eventData)
    {
        var item = m_corpsListDataGrid.SelectedData<p_legion_corps_toc>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择分派职位的战队");
            return;
        }

        if (item.corps_id == CorpsDataManager.m_corpsData.corps_id)
        {
            return;
        }

        if ( !LegionManager.IsLeader( PlayerSystem.roleId ) )
        {
            TipsManager.Instance.showTips("军团长才有权利授予职位");
            return;
        }
        UIManager.PopPanel<PanelLegionAuthorize>(new object[] { item }, true, this);
       
    }

    protected void OnClkMemberKickout(GameObject target, PointerEventData eventData)
    {
        var item = m_corpsListDataGrid.SelectedData<p_legion_corps_toc>();
        if (item == null)
        {
            TipsManager.Instance.showTips("请选择踢出的战队");
            return;
        }

        if (item.corps_id == CorpsDataManager.m_corpsData.corps_id)
        {
            TipsManager.Instance.showTips("不能踢出自己的战队");
            return;
        }

        if ( !LegionManager.IsLeader( PlayerSystem.roleId ) )
        {
            TipsManager.Instance.showTips("军团长才有权利踢人");
            return;

        }

        string strDesc = string.Format("是否将{0}战队请出军团", item.name);
        UIManager.ShowTipPanel(strDesc, () => { NetLayer.Send(new tos_legion_kick_corps() { corps_id = item.corps_id }); }, null, true, true, "确定", "取消");
    }

    private static void Toc_legion_kick_corps( toc_legion_kick_corps proto )
    {
        TipsManager.Instance.showTips("踢出战队成功!");
    }

    protected void OnClkLegionFind(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelFindLegion>(null, true, this);
    }
    protected void OnClkLegionLookup(GameObject target, PointerEventData eventData)
    {
        var item = m_legionListDataGrid.SelectedData<BaseLegionData>(); ;
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择军团");
            return;
        }

        tos_legion_view_info msg = new tos_legion_view_info();
        msg.legion_id = item.data.legion_id;
        NetLayer.Send(msg);
    }

    private void Toc_legion_view_info(toc_legion_view_info proto)
    {
        UIManager.PopPanel<PanelLegionView>(new object[] { proto }, true, this);
    }


    protected void OnClkLegionMy(GameObject target, PointerEventData eventData)
    {
        if (LegionManager.m_legionData.legion_id == 0)
        {
            if (!CorpsDataManager.IsLeader() )
            {
                TipsManager.Instance.showTips("战队队长才有权响应招募");
                return;
            }
            if (CorpsDataManager.GetFightting() < CREATE_LEGION_FIGHTING )
            {
                TipsManager.Instance.showTips(string.Format("创建军团需要战队贡献{0}点",CREATE_LEGION_FIGHTING.ToString() ));
                return;
            }

            UIManager.PopPanel<PanelLegionCreate>(null, true, this);
        }
        else
        {
            search_legion_key = LegionManager.m_legionData.legion_id.ToString();
            NetLayer.Send(new tos_legion_search_name() { legion_name = search_legion_key });
        }
    }

    protected void OnClkLegionAdd(GameObject target, PointerEventData eventData)
    {
        if (CorpsDataManager.m_corpsData.corps_id == 0)
        {
            TipsManager.Instance.showTips("还未加入战队!");
            return;
        }

        if (!CorpsDataManager.IsLeader())
        {
            TipsManager.Instance.showTips( "战队队长才有资格申请加入!" );
            return;
        }

        if (LegionManager.m_legionData.legion_id != 0)
        {
            UIManager.ShowTipPanel("请先退出军团");
            return;
        }
        var item = m_legionListDataGrid.SelectedData<BaseLegionData>();
        if (item == null)
        {
            UIManager.ShowTipPanel("请选择军团");
            return;
        }

        if( item.data.capacity == item.data.corps_cnt )
        {
            TipsManager.Instance.showTips("该军团的战队席位已满！");
            return;
        }
        tos_legion_apply_enter msg = new tos_legion_apply_enter();
        msg.legion_id = item.data.legion_id;
        NetLayer.Send(msg);

        TipsManager.Instance.showTips("申请已发送！");
    }

    protected void ResetLegionListPage()
    {
        m_tran.Find("frame_legion_list/operation/page/page").GetComponent<Text>().text = m_iCurLegionListPageNum + "/" + LegionManager.m_iLegionListPageNum.ToString();
    }

    public void SetLegionListCurPage( int iPage )
    {
        m_iCurLegionListPageNum = iPage;
    }
    protected void OnClkLegionListPageup(GameObject target, PointerEventData eventData)
    {
        if (m_iCurLegionListPageNum >= LegionManager.m_iLegionListPageNum)
        {
            TipsManager.Instance.showTips("已经是最后一页了");
            return;
        }

        m_iCurLegionListPageNum++;
        OnChangeLegionListPageNum();
    }

    protected void OnClkLegionListdown(GameObject target, PointerEventData eventData)
    {
        if (m_iCurLegionListPageNum <= 1)
        {
            TipsManager.Instance.showTips("已经是第一页了");
            return;
        }
        m_iCurLegionListPageNum--;
        OnChangeLegionListPageNum();
    }

    protected void OnChangeLegionListPageNum()
    {
        if (LegionManager.m_dicLegion.ContainsKey(m_iCurLegionListPageNum))
            UpdateLegionList();

        else
            LegionManager.RequestLegionList(m_iCurLegionListPageNum);
    }

    private void Toc_legion_enlarge_capacity( toc_legion_enlarge_capacity proto )
    {
        TipsManager.Instance.showTips("你的军团扩容成功!");
    }
}
