﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemLegionApply : ItemRender
{
    private p_legion_apply_toc m_data;
    private Image m_imgFlag;
    private Image m_imgBiaozhi;
    private Text m_txtCorpsName;
    private Text m_txtLeaderName;
    private Text m_txtConstruction;
    private Text m_txtCapacity;
  

    public override void Awake()
    {
        m_imgFlag = transform.Find("flag").GetComponent<Image>();
        m_imgBiaozhi = transform.Find("biaozhi").GetComponent<Image>();
        m_txtCorpsName = transform.Find("corps_name").GetComponent<Text>();
        m_txtLeaderName = transform.Find("leader/leader_name").GetComponent<Text>();
        m_txtConstruction = transform.Find("construction/value").GetComponent<Text>();
        m_txtCapacity = transform.Find("capacity").GetComponent<Text>();

        UGUIClickHandler.Get(transform.Find("agree").gameObject).onPointerClick += OnClkAgree;
        UGUIClickHandler.Get(transform.Find("refuse").gameObject).onPointerClick += OnRefuse;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_legion_apply_toc;
        string flagName = "f" + m_data.logo_pannel.ToString() + "_" + "c" + m_data.logo_color.ToString();
        m_imgFlag.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, flagName));
        string biaozhiName = "biaozhi_" + m_data.logo_icon.ToString();
        m_imgBiaozhi.SetSprite(ResourceManager.LoadSprite(AtlasName.Corps, biaozhiName));
        m_txtCorpsName.text = m_data.name;
        m_txtLeaderName.text = m_data.leader_name;
        m_txtConstruction.text = m_data.fighting.ToString();
        m_txtCapacity.text = m_data.member_cnt.ToString() + "/20";

    }

    protected void OnClkAgree(GameObject target, PointerEventData eventData)
    {
        tos_legion_agree_apply msg = new tos_legion_agree_apply();
        msg.apply_id = m_data.corps_id;
        NetLayer.Send(msg);
        //OnRemoveApply();
    }

    protected void OnRefuse(GameObject target, PointerEventData eventData)
    {
        tos_legion_refuse_apply msg = new tos_legion_refuse_apply();
        msg.apply_id = m_data.corps_id;
        NetLayer.Send(msg);
        //OnRemoveApply();
    }

    protected void OnRemoveApply()
    {
        if (UIManager.IsOpen<PanelLegionApply>())
        {
            UIManager.GetPanel<PanelLegionApply>().RemoveApply(m_data.corps_id);
        }
    }
}
