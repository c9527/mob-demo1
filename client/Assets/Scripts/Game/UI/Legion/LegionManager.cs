﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LegionData
{
    public LegionData()
    {
        m_lstLog = new List<p_legion_log_toc>();
        m_lstCorps = new List<p_legion_corps_toc>();
        m_lstApply = new List<p_legion_apply_toc>();
    }

    public void Reset()
    {
        legion_id = 0;
        name = "";
        logo_icon = 0;
        notice = "";
        create_time = 0;
        creator_cid = 0;
        creator_pid = 0;
        creator_name = "";
        leader_cid = 0;
        leader_pid = 0;
        leader_name = "";
        activeness = 0;
        capacity = 0;

        m_lstLog.Clear();
        m_lstCorps.Clear();
        m_lstApply.Clear();
    }
    public long legion_id;
    public string name;
    public int logo_icon;
    public string notice;
    public int create_time;
    public long creator_cid;
    public long creator_pid;
    public string creator_name;
    public long leader_cid;
    public int leader_pid;
    public string leader_name;
    public int activeness;  // 建设值
    public int capacity;
    public int sum_mrb;
    public List<p_legion_log_toc> m_lstLog;
    public List<p_legion_corps_toc> m_lstCorps;
    public List<p_legion_apply_toc> m_lstApply;
}

public class BaseLegionData
{
    public int m_iQueue;
    public p_legion_info_toc data;
}

public enum LEGION_MEMBER_TYPE
{
    TYPE_LEADER = 1,    // -- 军团长
    TYPE_VICELEADER = 2,// -- 副军团长
    TYPE_SECRETARY = 3,// -- 秘书
    TYPE_COMMANDER = 4,// -- 指挥官
    TYPE_EXAMINER = 5,// -- 考官
    TYPE_DIPLOMAT = 6,// -- 外交官
    TYPE_MEMBER = 7 // -- 普通成员
    
}

public class LegionManager 
{
    public static LegionData m_legionData = new LegionData();
    public static Dictionary<int, List<BaseLegionData>> m_dicLegion = new Dictionary<int, List<BaseLegionData>>();
    private static bool m_bGetLegionList = false;
    public static int m_iLegionListPageNum;
    public static bool m_iswork;
    public static int m_iPlayerListPageNum;
    public static Dictionary<int, List<p_legion_member_toc>> m_dicPlayerList = new Dictionary<int, List<p_legion_member_toc>>();

    public static string[] m_strPosition = { "军团长", "副军团长", "秘书", "指挥官", "考官", "外交官", "普通成员" };
    public static List<p_legion_corps_toc> m_lstManager = new List<p_legion_corps_toc>();
    private static void ResetLegionData( p_legion_data_toc data )
    {
        m_legionData.activeness = data.activeness;
        m_legionData.create_time = data.create_time;
        m_legionData.creator_cid = data.creator_cid;
        m_legionData.creator_name = data.creator_name;
        m_legionData.creator_pid = data.creator_pid;
        m_legionData.logo_icon = data.logo_icon;
        m_legionData.leader_cid = data.leader_cid;
        m_legionData.leader_name = data.leader_name;
        m_legionData.legion_id = data.legion_id;
        m_legionData.name = data.name;
        m_legionData.notice = data.notice;
        m_legionData.capacity = data.capacity;

        m_legionData.m_lstApply.Clear();
        m_legionData.m_lstApply.AddRange(data.apply_list);

        m_legionData.m_lstCorps.Clear();
        m_legionData.m_lstCorps.AddRange(data.corps_list);
        m_legionData.m_lstCorps.Sort((x, y) => x.enter_time - y.enter_time);

        m_legionData.m_lstLog.Clear();
        m_legionData.m_lstLog.AddRange(data.log_list);
        m_lstManager.Clear();
        CalcSumMrb();
        CalcManager();
        BubbleManager.SetBubble(BubbleConst.LegionApply, IsManager() ? m_legionData.m_lstApply.Count : 0);
        ShowPanelLegion();
    }

    public static int GetMrbLegionPost( long id )
    {
        for( int i = 0; i < m_lstManager.Count; ++i )
        {
            if (m_lstManager[i].leader_id == id)
                return m_lstManager[i].legion_post;
        }

        return (int)LEGION_MEMBER_TYPE.TYPE_MEMBER;
    }

    private static void CalcManager()
    {
        m_lstManager.Clear();
        for (int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if (m_legionData.m_lstCorps[i].legion_post <= (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
                m_lstManager.Add(m_legionData.m_lstCorps[i]);
        }
    }
    private static void CalcSumMrb()
    {
        m_legionData.sum_mrb = 0;
        for( int i = 0; i < m_legionData.m_lstCorps.Count;++i )
        {
            m_legionData.sum_mrb += m_legionData.m_lstCorps[i].member_cnt;
        }
    }

    public static bool IsJoinLegion()
    {
        return m_legionData.legion_id != 0;
    }

    public static bool IsLeader( long id )
    {
        for( int i = 0; i < m_lstManager.Count; ++i )
        {
            if (m_lstManager[i].legion_post == (int)LEGION_MEMBER_TYPE.TYPE_LEADER && m_lstManager[i].leader_id == id)
                return true;
        }

        return false;
    }

    public static bool IsManager()
    {
        if (CorpsDataManager.IsLeader())
        {
            for (int i = 0; i < m_lstManager.Count; i++)
            {
                if (m_lstManager[i].corps_id == CorpsDataManager.m_corpsData.corps_id)
                    return true;
            }
        }

        return false;
    }
    
    public static ConfigLegionLevelLine GetConfigLegionLevelLine(int activeness)
    {
         var config = ConfigManager.GetConfig<ConfigLegionLevel>();
         ConfigLegionLevelLine configLine = null;
         for (int i = 0; i < config.m_dataArr.Length; i++)
         {
             if (config.m_dataArr[i].Activeness <= activeness)
             {
                 configLine = config.m_dataArr[i];
             }
         }
         return configLine;
    }

    public static int GetUpgradeActiveness( int activeness )
    {
        var config = ConfigManager.GetConfig<ConfigLegionLevel>();
        ConfigLegionLevelLine configLine = null;
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            if (config.m_dataArr[i].Activeness > activeness)
            {
                configLine = config.m_dataArr[i];
                break;
            }
        }

        if (configLine == null)
            configLine = config.m_dataArr[config.m_dataArr.Length -1];
        return configLine.Activeness;
    }
    public static bool CorpsIsFull()
    {
        return m_legionData.m_lstCorps.Count >= m_legionData.capacity;
    }
    public static bool IsViceLeader( int id )
    {
        return false;
    }
    public static void ShowPanelLegion()
    {
        if (UIManager.IsOpen<PanelCorps>())
            return;

        if (UIManager.IsOpen<PanelLegion>())
        {
            UIManager.GetPanel<PanelLegion>().UpdateView();
        }
        else if (UIManager.IsOpen<PanelSocity>())
        {
            PanelSocity panelParent = UIManager.GetPanel<PanelSocity>();
            PanelSocity.m_childPanel = UIManager.PopPanel<PanelLegion>(null, false, panelParent);
        }  
    }

    internal static void UpdateLegionView()
    {
        if (UIManager.IsOpen<PanelLegion>())
        {
            UIManager.GetPanel<PanelLegion>().UpdateView();
        }
    }

    public static void OnLeaveLegion()
    {
        m_legionData.Reset();
        m_dicLegion.Clear();
        m_dicPlayerList.Clear();
        RequestLegionList();
    }

    internal static p_legion_apply_toc GetApply(long id)
    {
        for (int i = 0; i < m_legionData.m_lstApply.Count; ++i)
        {
            if (m_legionData.m_lstApply[i].corps_id == id)
            {
                return m_legionData.m_lstApply[i];
            }
        }
        return null;
    }
    internal static void AddApply(p_legion_apply_toc apply)
    {
        p_legion_apply_toc corps_appy = GetApply(apply.corps_id);
        if (corps_appy != null)
            return;

        m_legionData.m_lstApply.Add(apply);
    }

    public static void RemoveInApply(long id)
    {
        for (int i = 0; i < m_legionData.m_lstApply.Count; ++i)
        {
            if (m_legionData.m_lstApply[i].corps_id == id)
            {
                m_legionData.m_lstApply.RemoveAt(i);
                break;
            }
        }
    }

    internal static p_legion_corps_toc GetCorps(long id)
    {
        for (int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if (m_legionData.m_lstCorps[i].corps_id == id)
                return m_legionData.m_lstCorps[i];
        }
        return null;
    }

    internal static void AddCorps( p_legion_corps_toc corps )
    {
        for( int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if( m_legionData.m_lstCorps[i].corps_id == corps.corps_id )
                return;
        }

        m_legionData.m_lstCorps.Add( corps );
    }

    internal static void RemoveCorps( long corps_id )
    {
         for( int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if( m_legionData.m_lstCorps[i].corps_id == corps_id )
            {
                m_legionData.m_lstCorps.RemoveAt( i );
                m_dicPlayerList.Clear();
                return;
            }     
        }
    }

    internal static void ChangeCorpsPos( p_legion_corps_toc corps )
    {
        for( int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if( m_legionData.m_lstCorps[i].corps_id == corps.corps_id )
            {
                m_legionData.m_lstCorps[i].legion_post = corps.legion_post;
                return;
            }    
        }
    }

    internal static void ModifyCorps( p_legion_corps_toc corps )
    {
        for (int i = 0; i < m_legionData.m_lstCorps.Count; ++i)
        {
            if (m_legionData.m_lstCorps[i].corps_id == corps.corps_id)
            {
                m_legionData.m_lstCorps[i] = corps;
                return;
            }
        }
    }
    public static void RequestLegionData()
    {
        if( m_legionData.legion_id  == 0 )
            NetLayer.Send(new tos_legion_get_data());
    }
    private static void Toc_legion_data(toc_legion_data proto)
    {
        ResetLegionData(proto.data);
    }

    private static void Toc_legion_create_legion(toc_legion_create_legion proto)
    {
        if (proto.data.legion_id == 0)
        {
            UIManager.ShowTipPanel("你的军团创建失败!");
            return;
        }
        UIManager.ShowTipPanel("你的军团创建成功!");
        ResetLegionData(proto.data);
        m_dicLegion.Clear();
        RequestLegionList();
    }
  
    public static void RequestLegionList(int ipage = 1)
    {
        tos_legion_get_list msg = new tos_legion_get_list();
        msg.page = ipage;
        NetLayer.Send(msg);
    }
    private static void Toc_legion_get_list(toc_legion_get_list proto)
    {
        int iPage = proto.page;
        m_iLegionListPageNum = proto.pages;
        PanelLegion panel = UIManager.GetPanel<PanelLegion>();
        if (panel != null)
            panel.SetLegionListCurPage(iPage);
        m_bGetLegionList = true;
        List<BaseLegionData> lstLegion = null;
        if (m_dicLegion.ContainsKey(iPage))
        {
            lstLegion = m_dicLegion[iPage];
        }
        else
        {
            lstLegion = new List<BaseLegionData>();
            m_dicLegion[iPage] = lstLegion;
        }

        lstLegion.Clear();
        for (int i = 0; i < proto.list.Length; i++)
        {
            BaseLegionData newData = new BaseLegionData();
            newData.m_iQueue = proto.start + i;
            newData.data = new p_legion_info_toc();
            newData.data.activeness = proto.list[i].activeness;
            newData.data.leader_name = proto.list[i].leader_name;
            newData.data.legion_id = proto.list[i].legion_id;
            newData.data.name = proto.list[i].name;
            newData.data.member_cnt = proto.list[i].member_cnt;
            newData.data.capacity = proto.list[i].capacity;
            newData.data.corps_cnt = proto.list[i].corps_cnt;
            lstLegion.Add(newData);
        }
        
        ShowPanelLegion();
    }

    public static void RequestPlayerList(int ipage = 1,bool work = true)
    {
        NetLayer.Send(new tos_legion_get_member_list() { page = ipage  });
        LegionManager.m_iswork = work;
    }

    private static void Toc_legion_get_member_list(toc_legion_get_member_list proto)
    {
        if (!m_iswork)
        {
            return;
        }
        int iPage = proto.page;
        m_iPlayerListPageNum = proto.pages;
        List<p_legion_member_toc> lstPlayer = null;
        if (m_dicPlayerList.ContainsKey(iPage))
        {
            lstPlayer = m_dicPlayerList[iPage];
        }
        else
        {
            lstPlayer = new List<p_legion_member_toc>();
            m_dicPlayerList[iPage] = lstPlayer;
        }

        lstPlayer.Clear();
        lstPlayer.AddRange(proto.list);
        ShowPanelLegion();
    }
    private static void Toc_legion_notify_apply(toc_legion_notify_apply proto)
    {
        if (proto.type == "add")
        {
            AddApply(proto.apply);
        }
        else if (proto.type == "del")
        {
            RemoveInApply(proto.apply.corps_id);
        }

        BubbleManager.SetBubble(BubbleConst.LegionApply, IsManager() ? m_legionData.m_lstApply.Count : 0);
        UpdateLegionView();
    }

    private static void Toc_legion_notify_corps(toc_legion_notify_corps proto)
    {
        for (int i = 0; i < proto.list.Length; ++i)
        {
            toc_legion_notify_corps.notify_corps ntf_mbr = proto.list[i];

            if (ntf_mbr.type == "add")
            {
                AddCorps(ntf_mbr.corps);
            }
            else if (ntf_mbr.type == "del")
            {
                RemoveCorps(ntf_mbr.corps.corps_id);
            }
            else if (ntf_mbr.type == "change_post")
            {
                ChangeCorpsPos(ntf_mbr.corps);
            }
            else if( ntf_mbr.type == "modify" )
            {
                ChangeCorpsPos(ntf_mbr.corps);
            }
        }

        CalcSumMrb();
        CalcManager();
        UpdateLegionView();
    }

    private static void Toc_legion_notify_infos(toc_legion_notify_infos proto)
    {
        for (int i = 0; i < proto.list.Length; ++i)
        {
            toc_legion_notify_infos.notify_info ntf_info = proto.list[i];
            if (ntf_info.key == "activeness")
            {
                m_legionData.activeness = ntf_info.int_val;
            }
            else if (ntf_info.key == "notice")
            {
                m_legionData.notice = ntf_info.str_val;
            }
            else if (ntf_info.key == "icon")
            {
                m_legionData.logo_icon = ntf_info.int_val;
            }
            else if( ntf_info.key == "capacity" )
            {
                m_legionData.capacity = ntf_info.int_val;
            }
        }

        UpdateLegionView();
    }

    private static void Toc_legion_notify_logs(toc_legion_notify_logs proto)
    {
        m_legionData.m_lstLog.Clear();
        m_legionData.m_lstLog.AddRange(proto.list);
        UpdateLegionView();
    }

    private static void Toc_legion_message(toc_legion_message proto)
    {
       if (proto.type == "agree_apply")
       {
           p_legion_apply_toc apply = GetApply(proto.pid);
            if (apply == null)
                return;

            string info = string.Format("{0}被同意加入军团", apply.name);
            TipsManager.Instance.showTips(info);
        }
        else if (proto.type == "refuse_apply")
        {
            p_legion_apply_toc apply = GetApply(proto.pid);
            if (apply == null)
                return;

            string info = string.Format("{0}被拒绝加入军团", apply.name);
            TipsManager.Instance.showTips(info);
        
        }
        else if (proto.type == "quit")
        {
            if (proto.cid == CorpsDataManager.m_corpsData.corps_id)
            {
                OnLeaveLegion();
            }
            else
            {
                p_legion_corps_toc corps = GetCorps(proto.cid);
                if (corps == null)
                    return;

                string info = string.Format("{0}退出了军团", corps.name);
                TipsManager.Instance.showTips(info);
            }

        }
        else if (proto.type == "dismiss")
        {
            OnLeaveLegion();
        }
        else if (proto.type == "kick")
        {
            if (proto.cid == CorpsDataManager.m_corpsData.corps_id)
            {
                TipsManager.Instance.showTips("你被军团长踢出了军团!");
                OnLeaveLegion();
            }
            else
            {
                p_legion_corps_toc corps = GetCorps(proto.cid);
                if (corps == null)
                    return;

                string info = string.Format("{0}被军团长踢出了军团!", corps.name);
                TipsManager.Instance.showTips(info);
            }
        }
        else if (proto.type == "change_member_type")
        {
            p_legion_corps_toc corps = GetCorps(proto.cid);
            if (corps == null)
                return;

            string info = string.Format("{0}的职务从{1}变成了{2}", corps.name, m_strPosition[corps.legion_post - 1], proto.msg);
            TipsManager.Instance.showTips(info);
        }
        else if (proto.type == "set_leader")
        {
            p_legion_corps_toc corps = GetCorps(proto.cid);
            if (corps == null)
                return;

            string info = string.Format("{0}成了新军团长", corps.leader_name);
            TipsManager.Instance.showTips(info);
        }
        else if (proto.type == "update_notice")
        {
            TipsManager.Instance.showTips("军团长修改了公告!");
        }
    }
}
