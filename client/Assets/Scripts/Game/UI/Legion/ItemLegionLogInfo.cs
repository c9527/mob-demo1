﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemLegionLogInfo : ItemRender
{
    private p_legion_log_toc m_data;
    private Text m_txtLog; 

    public override void Awake()
    {
        m_txtLog = transform.Find("log").GetComponent<Text>();
    }
	
	// Update is called once per frame
    protected override void OnSetData(object data)
    {
        m_data = data as p_legion_log_toc;
        string strLog = "";
        string strTime = TimeUtil.GetTime(m_data.log_time).ToString("M月d号 HH:mm");
        strLog += strTime;
        strLog += " ";
        if (m_data.log_type == "agree_apply")
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            strLog += "批准";
            strLog += m_data.name;
            strLog += "加入军团";
        }
        else if (m_data.log_type == "kick")
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            strLog += "将";
            strLog += m_data.name;
            strLog += "移出军团";
        }
        else if (m_data.log_type == "quit")
        {
            strLog += m_data.opr_name;
            strLog += "退出军团";
        }
        else if (m_data.log_type == "change_post_type")
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            if( m_data.param >= 0 )
            {
                strLog += "任命";
                strLog += m_data.name;
                strLog += "为副团长";
            }
            else
            {
                strLog += "撤销了";
                strLog += m_data.name;
                strLog += "的副团长职务";
            }
         
        }
        else if (m_data.log_type == "update_notice")
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            strLog += "更新了军团公告";
        }
        else if (m_data.log_type == "change_icon")
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            strLog += "更新了军团图标";
        }
        else if (m_data.log_type == "set_leader")
        {
            strLog += m_data.opr_name;
            strLog += "转让军团长职务给";
            strLog += m_data.name;
        }
        else if( m_data.log_type == "enlarge_capacity" )
        {
            strLog += LegionManager.m_strPosition[m_data.opr_post - 1];
            strLog += m_data.opr_name;
            strLog += "为军团扩容";
        }
        else if( m_data.log_type == "level_up" )
        {
            strLog += "军团等级提升至";
            strLog += m_data.param.ToString();
            strLog += "级";
        }
        else
        {
            return;
        }
        m_txtLog.text = strLog;
    }
}
