﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemLegionInfo : ItemRender
{
    private BaseLegionData m_data;
    private Text m_txtQueue;
    private Text m_txtName;
    private Text m_txtLvl;
    private Text m_txtLeader;
    private Text m_txtCapacity;

    public override void Awake()
    {
        m_txtQueue = transform.Find("queue").GetComponent<Text>();
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_txtLvl = transform.Find("lvl").GetComponent<Text>();
        m_txtLeader = transform.Find("leader").GetComponent<Text>();
        m_txtCapacity = transform.Find("capacity").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as BaseLegionData;
        m_txtQueue.text = m_data.m_iQueue.ToString();
        m_txtName.text = m_data.data.name;
        m_txtLvl.text = LegionManager.GetConfigLegionLevelLine(m_data.data.activeness).Disp;
        m_txtLeader.text = m_data.data.leader_name;
        m_txtCapacity.text = m_data.data.member_cnt.ToString() + "/" + (m_data.data.capacity * 20).ToString(); ;
    }
}
