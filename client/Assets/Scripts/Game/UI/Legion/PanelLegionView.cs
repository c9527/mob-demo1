﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelLegionView : BasePanel
{
    private Text m_txtLegionName;
    private Text m_txtLegionId;
    private Text m_txtLvl;
    private Text m_txtCapacity;
    private Text m_txtActiveness;
    private Image m_imgBadge;
    private GameObject m_goLeader;
    private GameObject[] m_goViceLeader = new GameObject[2];
    private GameObject m_goOfficial;
    private GameObject m_goOrganize;
    private toc_legion_view_info m_data;
    private DataGrid m_corpsDataGrid;
    private Text m_txtNotice;
    private Toggle m_toggleOfficial;
    private Toggle m_toggleOrganize;
    public PanelLegionView()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegionView");
        AddPreLoadRes("UI/Legion/PanelLegionView", EResType.UI);
        AddPreLoadRes("UI/Legion/ItemOrganize", EResType.UI);

        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtLegionName = m_tran.Find("icon_kuang/name").GetComponent<Text>();
        m_imgBadge = m_tran.Find("icon_kuang/badge").GetComponent<Image>();

        m_txtLvl = m_tran.Find("legion_data/legion_lvl/value").GetComponent<Text>();
        m_txtLegionId = m_tran.Find("legion_data/legion_id/value").GetComponent<Text>();
        m_txtCapacity = m_tran.Find("legion_data/legion_capacity/value").GetComponent<Text>();
        m_txtActiveness = m_tran.Find("legion_data/legion_activeness/value").GetComponent<Text>();
        m_txtNotice = m_tran.Find("bulitin/content").GetComponent<Text>();
        m_goLeader = m_tran.Find("official/leader").gameObject;
        m_goViceLeader[0] = m_tran.Find("official/vice_leader_1").gameObject;
        m_goViceLeader[1] = m_tran.Find("official/vice_leader_2").gameObject;
        m_goOfficial = m_tran.Find("official").gameObject;
        m_goOrganize = m_tran.Find("organize").gameObject;
        m_toggleOfficial = m_tran.Find("ToggleOfficial").GetComponent<Toggle>();
        m_toggleOrganize = m_tran.Find("ToggleOrganize").GetComponent<Toggle>();
        
        m_tran.Find("organize/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_corpsDataGrid = m_tran.Find("organize/ScrollDataGrid/content").GetComponent<DataGrid>();
        m_corpsDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Legion/ItemOrganize"), typeof(ItemOrganize));
        m_corpsDataGrid.useLoopItems = true;
    }

    public override void Update()
    {
    }

     public override void InitEvent()
     {
         UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClose;
         UGUIClickHandler.Get(m_tran.Find("ToggleOfficial").gameObject).onPointerClick += OnClkFrame;
         UGUIClickHandler.Get(m_tran.Find("ToggleOrganize").gameObject).onPointerClick += OnClkFrame;
     }

     public override void OnShow()
     {
        if( m_params.Length <= 0 )
         return;

        m_data = m_params[0] as toc_legion_view_info;
        InitView();
     }

    protected void InitView()
    {
        m_txtLegionName.text = m_data.name;
        m_txtLegionId.text = m_data.legion_id.ToString();
        //m_txtLvl.text = LegionManager.GetLegionLvlStr(m_data.activeness);
        m_txtLvl.text = LegionManager.GetConfigLegionLevelLine(m_data.activeness).Disp;
        m_txtCapacity.text = m_data.member_cnt.ToString() + "/" + (m_data.capacity * CorpsDataManager.CORPS_PLAYER_NUM).ToString();
        m_txtActiveness.text = m_data.activeness.ToString() + "/" + LegionManager.GetUpgradeActiveness(m_data.activeness).ToString();
        string badge = "badge_" + m_data.logo_icon.ToString();
        m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Legion, badge));
        m_txtNotice.text = "公告:" + m_data.notice;

        int viceIdx = 0;
        for (int i = 0; i < m_data.managers.Length; ++i)
        {
            p_legion_manager_toc manager = m_data.managers[i];
            GameObject goManager = null;
            if (manager.legion_post == (int)LEGION_MEMBER_TYPE.TYPE_LEADER)
            {
                goManager = m_goLeader;
            }
            else if (manager.legion_post == (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
            {
                goManager = m_goViceLeader[viceIdx];
                viceIdx++;
            }
            if (goManager != null)
            {
                goManager.transform.FindChild("name").GetComponent<Text>().text = manager.name;
                goManager.transform.FindChild("head").gameObject.TrySetActive(true);
                goManager.transform.FindChild("unknow").gameObject.TrySetActive(false);
                goManager.transform.FindChild("head").GetComponent<Image>().SetSprite(ResourceManager.LoadRoleIconBig(manager.icon));
                goManager.transform.FindChild("head").GetComponent<Image>().SetNativeSize();
                goManager.transform.FindChild("id").GetComponent<Text>().text = manager.id.ToString();
                UGUIClickHandler.Get(goManager).onPointerClick += OnViewLeader;
            }
        }

        m_goOrganize.TrySetActive(true); 
        m_corpsDataGrid.Data = m_data.corps_list;

        if( m_toggleOfficial.isOn )
        {
            ShowOfficial();
        }
        else
        {
            ShowOrganize();
        }
        
     }

    protected void OnViewLeader(GameObject target, PointerEventData eventData)
    {
        int id = int.Parse(target.transform.FindChild("id").GetComponent<Text>().text);
        if (id != 0 && id != PlayerSystem.roleId )
        {
            PlayerFriendData.ViewPlayer(id);
        }
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

     public override void OnHide()
     {

     }

     public override void OnBack()
     {
     }

     public override void OnDestroy()
     { }

     protected void OnClose(GameObject target, PointerEventData eventData)
     {
         HidePanel();
     }

     protected void OnClkFrame(GameObject target, PointerEventData eventData)
     {
         if( target.name == "ToggleOfficial" )
         {
             ShowOfficial();
         }
         else if( target.name == "ToggleOrganize" )
         {
             ShowOrganize();
         }
     }
    
     protected void ShowOfficial()
     {
         m_goOfficial.TrySetActive(true);
         m_goOrganize.TrySetActive(false);
     }

     protected void ShowOrganize()
     {
         m_goOfficial.TrySetActive(false);
         m_goOrganize.TrySetActive(true);     
     }
}
