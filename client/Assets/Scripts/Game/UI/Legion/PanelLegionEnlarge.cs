﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelLegionEnlarge : BasePanel
{
    private Text m_txtCost;
    private int m_iCost;
    public PanelLegionEnlarge()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegionEnlarge");
        AddPreLoadRes("UI/Legion/PanelLegionEnlarge", EResType.UI);

        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
    }

    public override void Init()
    {
        m_iCost = ConfigMisc.GetInt("enlarge_legion_capacity_diamond");
        m_txtCost = m_tran.Find("cost").GetComponent<Text>();
        m_txtCost.text = m_iCost.ToString();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("submit").gameObject).onPointerClick += OnClkSubmit;
    }

    public override void OnShow()
    {
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    protected void OnClkSubmit(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_legion_enlarge_capacity());
        HidePanel();
    }
}
