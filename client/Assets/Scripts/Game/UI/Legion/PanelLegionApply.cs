﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelLegionApply : BasePanel
{
     private DataGrid m_applyDataGrid;
     private List<p_legion_apply_toc> m_lstApply;
    //private p_corps_data_toc.apply[] m_lstApply;
    public PanelLegionApply()
    {
        SetPanelPrefabPath("UI/Legion/PanelLegionApply");
        AddPreLoadRes("UI/Legion/PanelLegionApply", EResType.UI);
        AddPreLoadRes("UI/Legion/ItemLegionApply", EResType.UI);

        AddPreLoadRes("Atlas/Corps", EResType.Atlas);
        AddPreLoadRes("Atlas/Legion", EResType.Atlas);
    }
    public override void Init()
    {
        m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_applyDataGrid = m_tran.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
        m_applyDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Legion/ItemLegionApply"), typeof(ItemLegionApply));
        m_applyDataGrid.useLoopItems = true;

        m_lstApply = new List<p_legion_apply_toc>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        //UGUIClickHandler.Get(m_tran.Find("agree").gameObject).onPointerClick += OnClkAgree;
        //UGUIClickHandler.Get(m_tran.Find("ignore").gameObject).onPointerClick += OnClkIgnore;
    }

    public override void OnShow()
    {
        if (m_params.Length < 1)
            return;

        List<p_legion_apply_toc> lstApply = m_params[0] as List<p_legion_apply_toc>;
        m_lstApply.Clear();
        m_lstApply.AddRange(lstApply);
        m_applyDataGrid.Data = m_lstApply.ToArray();
    }

    public void UpdateView( List<p_legion_apply_toc> lstApply )
    {
        m_lstApply.Clear();
        m_lstApply.AddRange(lstApply);
        m_applyDataGrid.Data = lstApply.ToArray();
    }
    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }

    protected void OnClkClose( GameObject target, PointerEventData eventData )
    {
        HidePanel();
    }

    public void RemoveApply( long id )
    {
        for( int i = 0; i < m_lstApply.Count; ++i )
        {
            if( m_lstApply[i].corps_id == id )
            {
                m_lstApply.RemoveAt(i);
                break;
            }
        }

        m_applyDataGrid.Data = m_lstApply.ToArray();
    }
}
