﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ItemMrbPlayerInfo : ItemRender
{
    private p_legion_member_toc m_data;
    private Text m_txtName;
    private Text m_txtPos;
    private Text m_txtLvl;
    private Text m_txtCorps;
    private Text m_txtLastLogin;
    private GameObject m_goGray;
    private Image m_imgVip;
    private GameObject m_goVip;
    private GameObject m_goName;
    private Vector2 m_anchorName;

    public override void Awake()
    {
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_txtPos = transform.Find("position").GetComponent<Text>();
        m_txtLvl = transform.Find("lvl").GetComponent<Text>();
        m_txtCorps = transform.Find("corps").GetComponent<Text>();
        m_txtLastLogin = transform.Find("last_login").GetComponent<Text>();
        m_goGray = transform.Find("gray").gameObject;

        m_goVip = transform.Find("vip").gameObject;
        m_imgVip = transform.Find("vip").GetComponent<Image>();
        m_goName = transform.Find("name").gameObject;
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition; 

    }

    protected override void OnSetData(object data)
    {
        m_data = data as p_legion_member_toc;
        m_txtName.text = m_data.name;

        int iLegionPost = LegionManager.GetMrbLegionPost(m_data.id);
        if (iLegionPost <= (int)LEGION_MEMBER_TYPE.TYPE_VICELEADER)
        {
            m_txtPos.text = LegionManager.m_strPosition[iLegionPost - 1];
        }
        else
        {
            m_txtPos.text = PanelCorps.m_strPosition[m_data.member_type - 1];
        } 
        m_txtLvl.text = String.Format("{0}级", m_data.level);
        m_txtCorps.text = m_data.corps_name;
        m_txtLastLogin.text = TimeUtil.LastLoginString(m_data.logout_time);

        if( m_data.handle == 0 )
        {
            m_goGray.TrySetActive(true);
        }
        else
        {
            m_goGray.TrySetActive(false);
            m_txtLastLogin.text = "在线";
        }

        m_goName.GetComponent<RectTransform>().anchoredPosition = m_anchorName;
        MemberManager.MEMBER_TYPE eType = (MemberManager.MEMBER_TYPE)m_data.vip_level; ;
        if (eType == MemberManager.MEMBER_TYPE.MEMBER_NONE)
        {
            m_goVip.TrySetActive(false);
            Vector2 vipSizeData = m_goVip.GetComponent<RectTransform>().sizeDelta;
            m_goName.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_anchorName.x - vipSizeData.x, m_anchorName.y);

        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "member_icon"));
            m_imgVip.SetNativeSize();
        }
        else if (eType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
        {
            m_goVip.TrySetActive(true);
            m_imgVip.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "high_member_icon"));
            m_imgVip.SetNativeSize();
        }
    }
}
