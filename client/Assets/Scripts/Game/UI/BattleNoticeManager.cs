﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleNoticeManager
{
    static BattleNoticeManager _instance;
    private HashSet<long> m_hasNoticeSet = new HashSet<long>();
    private List<BasePlayer> m_waitNoticeList = new List<BasePlayer>();

    private bool m_bIsPanelBattleInited;
    private PanelBattle m_battlePanel;

    public static BattleNoticeManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public BattleNoticeManager()
    {
        if (_instance != null)
            _instance.Release();

        _instance = this;

        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            m_battlePanel = UIManager.GetPanel<PanelBattle>();
            if (m_battlePanel.IsInited()) 
                m_bIsPanelBattleInited = true;
        }
        else
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        }

        PlayGameStartNotice();
    }

    public void PlayGameStartNotice()
    {
        if (SceneManager.singleton.battleSceneLoaded)
        {
            if (!UIManager.IsInited<PanelBattleNotice>())
            {
                UIManager.PopPanel<PanelBattleNotice>();
            }
            else
            {
                UIManager.GetPanel<PanelBattleNotice>().GameStartNotice();
            }
        }
    }

    private void OnCreateBattlePanel()
    {
        m_bIsPanelBattleInited = true;
        m_battlePanel = UIManager.GetPanel<PanelBattle>();
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);

        for (int i = 0, imax = m_waitNoticeList.Count; i < imax; ++i)
            OnPlayerJoin(m_waitNoticeList[i]);
    }

    public void OnPlayerJoin(BasePlayer bp)
    {
        if (bp.released)
            return;

        if(!m_bIsPanelBattleInited)
        {
            m_waitNoticeList.Add(bp);
            return;
        }

        if (m_hasNoticeSet.Contains(bp.Pid))
            return;

        m_hasNoticeSet.Add(bp.Pid);

        int sacredWeaponId = bp.serverData.roleData.sacred_weapon_id;

        if(sacredWeaponId != 0)
        {
            ConfigItemWeaponLine kWL = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(sacredWeaponId);
            if (kWL == null)
            {
                Logger.Error("Can't get ConfigWeaponLine, id " + sacredWeaponId);
                return;
            }

            string icon = kWL.Icon;
            string playName = bp.ColorName;

            NoticePanelProp prop = new NoticePanelProp();
            //底图
            prop.bgAltas = AtlasName.BattleNew;
            prop.bgName = "join_notice_bg";
            prop.bgSize = new Vector2(380, 56);
            //特杀图标
            prop.imgPos = new Vector2(-142, 282);
            prop.bgType = Image.Type.Sliced;
            //玩家名字
            prop.msg = string.Format("持有者{0}加入战斗", playName);
            prop.txtPos = new Vector2(10, 282);
            prop.textProp = new TextProp() { fontSize = 18, color = Color.green };
            prop.imgScale = new Vector2(0.5f, 0.5f);

            m_battlePanel.PlayNoticePanelQueue2(AtlasName.ICON, icon, 3, false, new Vector2(-56, 286), prop, new Vector2(56, -50));
        }
    }

    public void OnPlayerLeave(BasePlayer player)
    {
        if (m_hasNoticeSet.Contains(player.Pid))
            m_hasNoticeSet.Remove(player.Pid);
    }

    private void OnSceneLoaded()
    {
        PlayGameStartNotice();
    }

    public void Release()
    {
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        m_hasNoticeSet.Clear();
        m_waitNoticeList.Clear();
        _instance = null;
    }

}
