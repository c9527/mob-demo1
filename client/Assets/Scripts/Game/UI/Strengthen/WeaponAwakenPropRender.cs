﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class WeaponAwakenPropRender : ItemRender
{
    private CDAwakenProp _data;

    public Image bg_0;
    public Image bg_1;
    public Text desc_txt;

    public override void Awake()
    {
        bg_0 = transform.Find("bg_0").GetComponent<Image>();
        bg_1 = transform.Find("bg_1").GetComponent<Image>();
        desc_txt = transform.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDAwakenProp;
        if (_data == null || _data.info==null)
        {
            return;
        }
        bg_0.gameObject.TrySetActive(_data.info.AwakenLevel % 2 == 0);
        bg_1.gameObject.TrySetActive(_data.info.AwakenLevel % 2 == 1);
        desc_txt.text = _data.GenInfoTxt();
    }
}

public class CDAwakenProp
{
    public TDWeaponAwaken info;
    public p_awaken_data data;

    public CDAwakenProp(TDWeaponAwaken info, p_awaken_data data = null)
    {
        this.info = info;
        this.data = data;
    }

    public string GenInfoTxt()
    {
        var str = "";
        if (info!=null && data != null && info.AwakenLevel<=(data.level+1))
        {
            var awaken = info.AwakenLevel <= data.level;
            str = !string.IsNullOrEmpty(info.Desc) ? string.Format("<color={1}>{0}</color>", info.Desc, awaken ? "#ffffff" : "#828282") : "";
            if (info.PromoteCon != null && info.PromoteCon.Length > 0)
            {
                var upgrade = true;
                var temp = new List<string>();
                foreach (var code in info.PromoteCon)
                {
                    var titem = ItemDataManager.GetItem(code);
                    if (titem != null)
                    {
                        var sitem = ItemDataManager.GetDepoUnlimitItem(code);

                        var forver_own = (sitem != null && TimeUtil.GetRemainTimeType(sitem.time_limit) == 0);
                        temp.Add(string.Format("<color={1}>{0}</color>", titem.DispName, forver_own ? "#53e99ff" : "#828282"));
                        upgrade = upgrade && forver_own;
                    }
                }
                str += string.Format("\n<color={2}>晋升（永久拥有{0}）：{1}</color>", string.Join("、", temp.ToArray()), info.PromoteDesc, upgrade ? "#53e99ff" : "#828282");
            }
        }
        else
        {
            str = "<color=#c0c1c1>下一级：未知（开启上一级超级属性后可见）</color>";
        }
        return str;
    }
}
