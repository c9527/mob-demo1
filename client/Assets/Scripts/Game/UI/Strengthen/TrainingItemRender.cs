﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

class TrainingItemRender : ItemRender
{

    public const int ExpandCard0=5047;
    public const int ExpandCard1 = 5048;
    public static p_item ExpandItem0
    {
        get
        {
            return ItemDataManager.GetDepotItem(ExpandCard0);
        }
    }
    public static p_item ExpandItem1
    {
        get
        {
            return ItemDataManager.GetDepotItem(ExpandCard1);
        }
    }
    toc_player_weapon_train_info.train_info _data;

    float _last_time;
    Vector3 _orgin_worldPos;//对指引做的特殊处理，因为这个按钮会在指引完后设置按钮的位置造成显示错误
    Vector3 _orgin_localPos;

    Transform _train_layer;
    Transform _none_layer;
    Image _desc_txt;
    Image _item_img;
    Text _name_txt;
    Text _mode_txt;
    Text _cd_txt;
    Text _info_txt;
    Button _action_btn;
    Button _speedup_btn;
    GameObject m_haveItem;
    GameObject m_noItem;
    Text m_leftTimeTxt;
    GameObject m_text;
    GameObject m_txtgo;

    public override void Awake()
    {
        _train_layer = transform.Find("train_layer");
        _none_layer = transform.Find("none_layer");

        _desc_txt = transform.Find("none_layer/haveItem/desc_txt").GetComponent<Image>();
        m_txtgo = _desc_txt.transform.Find("Text").gameObject;
        _item_img = transform.Find("item_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _mode_txt = transform.Find("train_layer/mode_txt").GetComponent<Text>();
        _cd_txt = transform.Find("train_layer/cd_txt").GetComponent<Text>();
        _info_txt = transform.Find("train_layer/info_txt").GetComponent<Text>();
        _action_btn = transform.Find("train_layer/action_btn").GetComponent<Button>();
        _speedup_btn = transform.Find("train_layer/speedup_btn").GetComponent<Button>();
        _orgin_localPos = _speedup_btn.transform.localPosition;
        _orgin_worldPos = _speedup_btn.transform.position;
        m_haveItem = transform.Find("none_layer/haveItem").gameObject;
        m_noItem = transform.Find("none_layer/noItem").gameObject;
        m_leftTimeTxt = transform.Find("none_layer/haveItem/leftTimeTxt").GetComponent<Text>();
        m_text = transform.Find("none_layer/haveItem/Text").gameObject;
        UGUIClickHandler.Get(_desc_txt.gameObject).onPointerClick += onItemClickHandler;
        UGUIClickHandler.Get(_action_btn.gameObject).onPointerClick += onEndTrainHandler;
        UGUIClickHandler.Get(_speedup_btn.gameObject).onPointerClick += onEndTrainHandler;
        UGUIClickHandler.Get(transform.Find("none_layer/noItem/Button")).onPointerClick += OnBuyClick;
    }

    private void OnBuyClick(GameObject target, PointerEventData eventData)
    {
        //UIManager.GotoPanel("mall");
        if(_data.index==2)
        {            
            int mallID = GetMallID(ExpandCard0);
            if(mallID!=-1)
                UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetLine(mallID) });            
        }
        else if(_data.index==4)
        {
            int mallID = GetMallID(ExpandCard1);
            if (mallID != -1)
                UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetLine(mallID) });
        }
    }


    private int GetMallID(int itemID)
    {
        int mallID = -1;
        ConfigMallLine[] arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
        for(int i=0;i<arr.Length;i++)
        {
            if(arr[i].ItemId==itemID)
            {
                mallID = arr[i].ID;
                break;
            }
        }
        return mallID;
    }

    private void onEndTrainHandler(GameObject target, PointerEventData eventData)
    {
        if (_data == null || _data.weapon == 0)
        {
            return;
        }
        if (TimeUtil.GetRemainTime((uint)(_data.start_time + _data.need_time)).TotalSeconds <= 0)//完成
        {
            StrengthenModel.Instance.Tos_player_stop_weapon_train(_data.index, 0);
            return;
        }
        if (target.name == "action_btn")//停止
        { 
            UIManager.ShowTipPanel("停止训练只能获得部分经验，不返还消耗的资源，是否停止？", () => { StrengthenModel.Instance.Tos_player_stop_weapon_train(_data.index, 0); }); 
        }
        else//加速
        {
            var config = ConfigManager.GetConfig<ConfigWeaponTrainQuicken>();
            var speedup_upper = config.GetQuickenUpper(PlayerSystem.roleData.vip_level);
            int times = speedup_upper != null ? speedup_upper.Times : 0;
            times += TrainingItemRender.ExpandItem0 != null && TrainingItemRender.ExpandItem0.time_limit > TimeUtil.GetNowTimeStamp() ? 1 : 0;
            times += TrainingItemRender.ExpandItem1 != null&&TrainingItemRender.ExpandItem1.time_limit>TimeUtil.GetNowTimeStamp() ? 1 : 0;
            if (speedup_upper==null || StrengthenModel.Instance.training_data.quicken_times >= times) //加速用完
            {
                //UIManager.ShowTipPanel(string.Format("{0}加速上限为{1}次",PlayerSystem.roleData.vip_level==1?"会员":"尊贵会员",speedup_upper!=null ? speedup_upper.Times : 0));
                int timesMax = config.GetMaxQuickenTimes(PlayerSystem.roleData.vip_level);                
                if(StrengthenModel.Instance.training_data.quicken_times>=timesMax)
                {
                    TipsManager.Instance.showTips("已达单日加速次数上限");
                }
                else
                {
                    TipsManager.Instance.showTips("单日加速次数已用完，购买训练槽扩展卡可以增加加速次数");
                }
            }
            else //加速没用完
            {
                var speedup_info = config.GetLine(StrengthenModel.Instance.training_data.quicken_times+1);
                if (speedup_info != null)
                {
                    //if (speedup_info.ReqVip > PlayerSystem.roleData.vip_level)
                    //{
                    //    UIManager.ShowTipPanel(string.Format("第{0}次加速要求玩家成为{1}",speedup_info.Times,speedup_info.ReqVip==1 ? "会员":"尊贵会员"));
                    //}
                    if (speedup_info.Diamond == 0)
                    {
                        UIManager.ShowTipPanel(string.Format("今日剩余免费加速次数：1，是否加速？"), 
                            () => { StrengthenModel.Instance.Tos_player_stop_weapon_train(_data.index, 1); }, 
                            null, true, true);
                    }
                    else
                    {
                        UIManager.ShowTipPanel(string.Format("本次加速消耗钻石{0}，是否加速？", speedup_info.Diamond),
                                            () => { StrengthenModel.Instance.Tos_player_stop_weapon_train(_data.index, 1); },
                                            null, true, true);
                    }
                }
                else
                {
                    UIManager.ShowTipPanel("今天的加速次数已经用完");
                }
            }
        } 
    }

    private void onItemClickHandler(GameObject target, PointerEventData eventData)
    {
        if (_data!=null && _data.weapon==0)
        {
            if (//(_data.index==1 && PlayerSystem.roleData.level>=2)
                _data.index==1
                || (_data.index == 2 && (PlayerSystem.roleData.vip_level >= 1||(TrainingItemRender.ExpandItem0!=null&&TrainingItemRender.ExpandItem0.time_limit>TimeUtil.GetNowTimeStamp())))
                || (_data.index==3 && PlayerSystem.roleData.level>=30)
                || (_data.index==4 && (PlayerSystem.roleData.vip_level>=2||(TrainingItemRender.ExpandItem1!=null&&TrainingItemRender.ExpandItem1.time_limit>TimeUtil.GetNowTimeStamp())))
                )
            {
                UIManager.ShowPanel<PanelStrengthenTrainSet>(new object[]{_data.index});
            }
        }
    }

    protected override void OnSetData(object data)
    {

        m_txtgo.TrySetActive(true);
        _desc_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "YellowBtn_upSkin"));
        _desc_txt.rectTransform.sizeDelta = new Vector2(162.2f, 49);
        _data = data as toc_player_weapon_train_info.train_info;
        p_item item0 = ExpandItem0;
        p_item item1 = ExpandItem1;
        if (_data==null || _data.weapon==0)
        {
            _none_layer.gameObject.TrySetActive(true);
            _train_layer.gameObject.TrySetActive(false);
            var item_img_name = "train_item_none";
            var desc_txt_name = "select_item_txt";
            if (_data != null)
            {
                //if (_data.index == 1 && PlayerSystem.roleData.level < 2)
                //{
                //    item_img_name = "train_item_locked";
                //    desc_txt_name = "open_tips_3";
                //}
                //else 
                if (_data.index == 2 )
                {
                    if((item0!=null&&(item0.time_limit==0|| item0.time_limit>TimeUtil.GetNowTimeStamp()))||PlayerSystem.roleData.vip_level>=1)
                    {
                        m_haveItem.TrySetActive(true);
                        m_noItem.TrySetActive(false);
                        if (item0 != null)
                        {
                            m_leftTimeTxt.text = TimeUtil.GetRemainTimeString(item0.time_limit);
                            m_leftTimeTxt.gameObject.TrySetActive(true);
                            m_text.TrySetActive(true);
                        }
                        else
                        {
                            m_leftTimeTxt.gameObject.TrySetActive(false);
                            m_text.TrySetActive(false);
                        }
                    }
                    else
                    {
                        m_haveItem.TrySetActive(false);
                        m_noItem.TrySetActive(true);
                        item_img_name = "train_item_locked";
                        desc_txt_name = "open_tips_0";
                    }
                }
                else if (_data.index == 3 && PlayerSystem.roleData.level < 30)
                {
                    item_img_name = "train_item_locked";
                    desc_txt_name = "open_tips_1";
                    _desc_txt.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, desc_txt_name));
                    _desc_txt.SetNativeSize();
                    m_txtgo.TrySetActive(false);
                }
                else if(_data.index==4 )
                {
                    if((item1!=null&&(item1.time_limit==0|| item1.time_limit>TimeUtil.GetNowTimeStamp()))||PlayerSystem.roleData.vip_level>=2)
                    {
                        m_haveItem.TrySetActive(true);
                        m_noItem.TrySetActive(false);
                        if(item1!=null)
                        {
                            m_leftTimeTxt.text = TimeUtil.GetRemainTimeString(item1.time_limit);
                            m_leftTimeTxt.gameObject.TrySetActive(true);
                            m_text.TrySetActive(true);
                        }
                        else
                        {
                            m_leftTimeTxt.gameObject.TrySetActive(false);
                            m_text.TrySetActive(false);
                        }
                    }
                    else
                    {
                        m_haveItem.TrySetActive(false);
                        m_noItem.TrySetActive(true);
                        item_img_name = "train_item_locked";
                        desc_txt_name = "open_tips_2";
                    }
                }
            }
            _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, item_img_name));
            _item_img.SetNativeSize();
            _item_img.transform.localScale = new Vector3(1f,1f,1);
            _item_img.transform.localPosition = new Vector3(0,75,0);
            _item_img.transform.localEulerAngles = new Vector3(0,0,0);
            
            _name_txt.text = "";
        }
        else
        {
            _none_layer.gameObject.TrySetActive(false);
            _train_layer.gameObject.TrySetActive(true);

            var info = ItemDataManager.GetItem(_data.weapon);
            _name_txt.text = info.DispName;
            _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            _item_img.SetNativeSize();
            _item_img.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            _item_img.transform.localPosition = new Vector3(0, 65, 0);
            _item_img.transform.localEulerAngles = new Vector3(0, 0, -10);
            _mode_txt.text = "训练模式：+" + (_data.ratio * 100) + "%";
            int cd = (int)(TimeUtil.GetRemainTime((uint)(_data.start_time + _data.need_time)).TotalSeconds);
            cd = cd > 0 ? cd : 0;
            if (cd > 0)
            {
                _speedup_btn.transform.Find("Text").GetComponent<Text>().text = "加速";
                _speedup_btn.transform.localPosition = _orgin_localPos;
                _action_btn.gameObject.TrySetActive(true);
            }
            else
            {
                _speedup_btn.transform.Find("Text").GetComponent<Text>().text = "完成";
                _speedup_btn.transform.localPosition = new Vector3(0, _orgin_localPos.y, _orgin_localPos.z);
                _action_btn.gameObject.TrySetActive(false);
            }
            _last_time = 0;
        }
    }

    void Update()
    {
        if (_last_time==-1 || _data == null || _data.weapon == 0)
        {
            return;
        }
        if ((Time.realtimeSinceStartup - _last_time) >= 1)
        {
            _last_time = Time.realtimeSinceStartup;
            var cd = TimeUtil.GetRemainTime((uint)(_data.start_time + _data.need_time)).TotalSeconds;
            cd = cd > 0 ? cd : 0;
            var total_min = Math.Floor(_data.need_time / 60f);
            var per_total = _data.total_exp / total_min;
            var pass_min = Math.Floor((_data.need_time - cd) / 60f);
            _info_txt.text = "获得熟练度：" + Mathf.Max((int)(per_total * pass_min),0);
            _cd_txt.text = "训练时间：" + TimeUtil.FormatTime((int)cd);
            if (cd<=0)
            {
                SetData(_data);
                _last_time = -1;
            }
        }      
    }
}

