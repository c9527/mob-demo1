﻿using UnityEngine;
using UnityEngine.UI;


class TrainItemSelectRender : ItemRender
{
    p_item _data;
    Image _itembg;
    Image _item_img;
    Text _info_txt;
    Text _name_txt;
    Text _status_txt;
    Transform _star_group_trans;

    public override void Awake()
    {
        _itembg = transform.Find("").GetComponent<Image>();
        _item_img = transform.Find("item_img").GetComponent<Image>();
        _info_txt = transform.Find("info_txt").GetComponent<Text>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _status_txt = transform.Find("status_txt").GetComponent<Text>();
        _star_group_trans = transform.Find("starGroup/star");
    }

    protected override void OnSetData(object data)
    {
        _data = data as p_item;
        if (_data == null)
        {
            return;
        }



        var info = ItemDataManager.GetItem(_data.item_id);
        _itembg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(info.RareType)));
        if (info != null)
        {
            _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            _item_img.SetNativeSize();
            var strengthen_info = StrengthenModel.Instance.getSStrengthenDataByCode(info.ID);
            _info_txt.text = string.Format("熟练度： {0}级", strengthen_info != null ? strengthen_info.level : 1);
            _name_txt.text = info.DispName;
            if (_star_group_trans != null)
            {
                _star_group_trans.gameObject.TrySetActive(true);
                int count = strengthen_info != null ? strengthen_info.state : 0;
                foreach (Transform tran in _star_group_trans)
                {
                    tran.gameObject.TrySetActive(count > 0);
                    count--;
                }
            }
            _status_txt.enabled = false;
            if(StrengthenModel.Instance.training_data!=null && StrengthenModel.Instance.training_data.train_list!=null)
            {
                for (var i = 0; i < StrengthenModel.Instance.training_data.train_list.Length; i++)
                {
                    if (StrengthenModel.Instance.training_data.train_list[i].weapon == info.ID)
                    {
                        _status_txt.enabled = true;
                        break;
                    }
                }
            }
            
        }
        else
        {
            _item_img.SetSprite(null);
            _item_img.SetNativeSize();
            _info_txt.text = "";
            _name_txt.text = "";
            _star_group_trans.gameObject.TrySetActive(false);
        }
    }
}

