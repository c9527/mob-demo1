﻿using UnityEngine;
using UnityEngine.UI;

class ItemMaterialConverRender : ItemRender
{
    private Image m_imgIcon;
    private Text m_txtName;
    private Text m_txtCount;

    public override void Awake()
    {
        var tran = transform;
        m_imgIcon = tran.Find("icon").GetComponent<Image>();
        m_txtName = tran.Find("name").GetComponent<Text>();
        m_txtCount = tran.Find("count").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        ConfigItemOtherLine item = null;
        var info = data as ItemPItemConverInfo;
        if (info != null)
        {
            item = info.data;
            m_txtCount.text = info.count.ToString();

//            if (pitem.forbidden)
//                Util.SetGrayShader(this);
//            else
//                Util.SetNormalShader(this);
//            Util.SetNormalShader(m_txtName);
        }
        else
        {
            item = data as ConfigItemOtherLine;
            int count = 0;
            var pitem = item != null ? ItemDataManager.GetDepotItem(item.ID) : null;
            if (pitem != null)
                count = pitem.item_cnt;
            m_txtCount.text = count.ToString();
        }

        m_imgIcon.SetSprite(item != null ? ResourceManager.LoadIcon(item.Icon) : null,item != null ?item.SubType:"");
        if (m_imgIcon.sprite == null)
            m_imgIcon.SetSprite(ResourceManager.LoadIcon("ak47"));
//        var weaponItem = item != null ? ItemDataManager.GetWeaponItemByMaterial(item.ID) : null;
        m_txtName.text = item != null ? item.DispShortName : "";
        m_txtName.color = item != null ? ColorUtil.GetRareColor(item.RareType) : Color.white;
    }
}

class ItemPItemConverInfo
{
    public ConfigItemOtherLine data;
    public int count;
    public bool forbidden; //被禁用的
}