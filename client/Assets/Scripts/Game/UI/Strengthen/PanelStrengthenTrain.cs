﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

class PanelStrengthenTrain : BasePanel
{
    toc_player_weapon_train_info.train_info[] _traing_data;
    Dictionary<int,p_weapon_strength_info_toc> _train_strengthen_map = new Dictionary<int,p_weapon_strength_info_toc>();

    Text _title_txt;
    TrainingItemRender[] _item_list;

    public PanelStrengthenTrain()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthenTrain");

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
        AddPreLoadRes("UI/Strengthen/PanelStrengthenTrain", EResType.UI);
    }

    public override void Init()
    {
        _title_txt = m_tran.Find("title_txt").GetComponent<Text>();

        _item_list = new TrainingItemRender[4];
        for (int i = 0; i < _item_list.Length; i++)
        {
            var go = m_tran.Find("grid/train_item_" + i);
            var render = go.GetComponent<TrainingItemRender>();
            if (render==null)
            {
                render = go.gameObject.AddComponent<TrainingItemRender>();
            }
            render.SetData(null);
            _item_list[i] = render;
        }
    } 

    public override void InitEvent()
    {
        
    }

    private void updateTrainDataByCMD()
    {
        var config = ConfigManager.GetConfig<ConfigWeaponTrainQuicken>();        
        var free_times = config.GetTotalFreeQuickenTimes(PlayerSystem.roleData.vip_level);
        if (StrengthenModel.Instance.training_data.quicken_times >= free_times)
        {
            var speedup_upper = config.GetQuickenUpper(PlayerSystem.roleData.vip_level);
            int result=0;
            if(speedup_upper!=null)
            {
                result = speedup_upper.Times;
                result += TrainingItemRender.ExpandItem0 != null&&TrainingItemRender.ExpandItem0.time_limit>TimeUtil.GetNowTimeStamp() ? 1 : 0;
                result += TrainingItemRender.ExpandItem1 != null&&TrainingItemRender.ExpandItem1.time_limit>TimeUtil.GetNowTimeStamp() ? 1 : 0;
            }
            _title_txt.text = string.Format("今日可购买加速次数：{0}", speedup_upper != null ? result - StrengthenModel.Instance.training_data.quicken_times : 0);
        }
        else
        {
            _title_txt.text = string.Format("今日可免费加速次数：{0}", free_times - StrengthenModel.Instance.training_data.quicken_times);
        }
        _traing_data = StrengthenModel.Instance.training_data.train_list;
        _train_strengthen_map.Clear();
        for (int i = 0;_traing_data!=null && i < _traing_data.Length;i++ )
        {
            var info = StrengthenModel.Instance.getSStrengthenDataByCode(_traing_data[i].weapon);
            //拷贝内容
            info = new p_weapon_strength_info_toc() { weapon_id = info.weapon_id, state = info.state, level = info.level, cur_exp = info.cur_exp };
            _train_strengthen_map[info.weapon_id] = info;
        }
        updateItemList();
    }

    public override void OnShow()
    {
        StrengthenModel.Instance.Tos_player_weapon_train_info();
    }

    private void updateItemList()
    {    
        for (int i = 0; i < _item_list.Length; i++)
        {
            var render = _item_list[i];
            if (_traing_data!=null && i < _traing_data.Length)
            {
                render.SetData(_traing_data[i]);
            }
            else
            {
                render.SetData(new toc_player_weapon_train_info.train_info() { index=i+1, weapon=0 });
            }
        }
    } 

    public override void OnHide()
    {
        _traing_data = null;
        updateItemList();

    }

    public override void OnBack()
    {
        //UIManager.ShowPanel<PanelHallBattle>();
        UIManager.ShowPanel<PanelStrengthen>();
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }

    void Toc_player_weapon_train_info(toc_player_weapon_train_info data)
    {
        updateTrainDataByCMD();
    }

    void Toc_player_stop_weapon_train(toc_player_stop_weapon_train data)
    {
        toc_player_weapon_train_info.train_info info = null;
        for (int i = 0; i < _traing_data.Length; i++)
        {
            if (((toc_player_weapon_train_info.train_info)_traing_data[i]).index==data.index)
            {
                info = _traing_data[i] as toc_player_weapon_train_info.train_info;
                break;
            }
        }
        if (info != null && info.weapon!=0)
        {
            p_weapon_strength_info_toc old_data = null;
            _train_strengthen_map.TryGetValue(info.weapon, out old_data);
            if (old_data != null)
            {
                UIManager.ShowPanel<PanelStrengthenDetail>(new object[] { ItemDataManager.GetItem(info.weapon), data, old_data });
            }
        } 
        StrengthenModel.Instance.Tos_player_weapon_train_info();
    }


    void Toc_item_new(toc_item_new data)
    {
        OnShow();
    }

    void Toc_item_update(toc_item_update data )
    {
        OnShow();
    }

    void Toc_item_del(toc_item_del dtat)
    {
        OnShow();
    }
}
