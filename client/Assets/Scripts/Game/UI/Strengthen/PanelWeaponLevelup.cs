﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelWeaponLevelup : BasePanel
{
    Text _name_txt;
    Image _item_img;
    Transform[] _prop_txt_ary;

    public PanelWeaponLevelup()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelWeaponLevelup");

        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("UI/Strengthen/PanelWeaponLevelup", EResType.UI);
    }

    public override void Init()
    {
        _name_txt = m_tran.Find("name_txt").GetComponent<Text>();
        _item_img = m_tran.Find("item_img").GetComponent<Image>();
        _prop_txt_ary = new Transform[8];
        for (int i = 0; i < _prop_txt_ary.Length; i++)
        {
            _prop_txt_ary[i] = m_tran.Find("prop_txt_" + i);
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += onClickPanelHandler;
    }

    private void onClickPanelHandler(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    public override void OnShow()
    {
        if (!HasParams() || m_params.Length<2)
        {
            return;
        }
        updateData(m_params[0] as p_weapon_strength_info_toc, (int)m_params[1]);
        UIEffect.ShowEffect(m_tran,EffectConst.UI_STRENGTHEN_LEVELUP,4f);
        AudioManager.PlayUISound(AudioConst.weaponLevelup);
    }

    private void updateData(p_weapon_strength_info_toc data,int old_level)
    {
        if(data==null)
        {
            return;
        }
        _item_img.SetSprite(ResourceManager.LoadIcon(ItemDataManager.GetItem(data.weapon_id).Icon), ItemDataManager.GetItem(data.weapon_id).SubType);
        _item_img.SetNativeSize();
        var info = ItemDataManager.GetItem(data.weapon_id) as ConfigItemWeaponLine;
        var level_info = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(old_level, data.state, info.StrengthenGroup);
        level_info = level_info != null ? level_info : new TDStrengthenLevel() { Level=1 };
        var next_info = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(data.level, data.state, info.StrengthenGroup);
        next_info = next_info != null ? next_info : new TDStrengthenLevel();

        var sdata = ItemDataManager.GetDepotItem(info.ID);
        var part_add_map = StrengthenModel.Instance.GenPartPropAddMap(sdata);
        var suit_add_map = StrengthenModel.Instance.GenSuitPropAddMap(sdata);
        Func<int, KeyValue<int, int>, float> Formula = (v, p) => p.key == 1 ? v * p.value / 1000 : p.value;
        KeyValue<int, int> vo = null;
        var add_value = 0f;
        
        _prop_txt_ary[0].Find("Text0").GetComponent<Text>().text = level_info.Level.ToString();
        _prop_txt_ary[0].Find("Text1").GetComponent<Text>().text = next_info.Level.ToString();

        add_value = level_info.DispDamage;
        if (part_add_map.TryGetValue("DispDamage", out vo))
        {
            add_value += Formula(info.DispDamage, vo);
        }
        if (suit_add_map.TryGetValue("DispDamage", out vo))
        {
            add_value += Formula(info.DispDamage, vo);
        }
        _prop_txt_ary[1].Find("Text0").GetComponent<Text>().text = (info.DispDamage + (int)add_value).ToString();
        _prop_txt_ary[1].Find("Text1").GetComponent<Text>().text = (next_info.DispDamage + info.DispDamage + (int)add_value - level_info.DispDamage) + (next_info.DispDamage > level_info.DispDamage ? " ↑" : "");

        add_value = level_info.DispShootSpeed;
        if (part_add_map.TryGetValue("DispShootSpeed", out vo))
        {
            add_value += Formula(info.DispShootSpeed, vo);
        }
        if (suit_add_map.TryGetValue("DispShootSpeed", out vo))
        {
            add_value += Formula(info.DispShootSpeed, vo);
        }
        _prop_txt_ary[2].Find("Text0").GetComponent<Text>().text = (info.DispShootSpeed + (int)add_value).ToString();
        _prop_txt_ary[2].Find("Text1").GetComponent<Text>().text = (next_info.DispShootSpeed + info.DispShootSpeed + (int)add_value - level_info.DispShootSpeed) + (next_info.DispShootSpeed > level_info.DispShootSpeed ? " ↑" : "");

        add_value = level_info.DispAccuracy;
        if (part_add_map.TryGetValue("DispAccuracy", out vo))
        {
            add_value += Formula(info.DispAccuracy, vo);
        }
        if (suit_add_map.TryGetValue("DispAccuracy", out vo))
        {
            add_value += Formula(info.DispAccuracy, vo);
        }
        _prop_txt_ary[3].Find("Text0").GetComponent<Text>().text = (info.DispAccuracy + (int)add_value).ToString();
        _prop_txt_ary[3].Find("Text1").GetComponent<Text>().text = (next_info.DispAccuracy + info.DispAccuracy + (int)add_value - level_info.DispAccuracy) + (next_info.DispAccuracy > level_info.DispAccuracy ? " ↑" : "");

        add_value = level_info.DispRecoil;
        if (part_add_map.TryGetValue("DispRecoil", out vo))
        {
            add_value += Formula(info.DispRecoil, vo);
        }
        if (suit_add_map.TryGetValue("DispRecoil", out vo))
        {
            add_value += Formula(info.DispRecoil, vo);
        }
        _prop_txt_ary[4].Find("Text0").GetComponent<Text>().text = (info.DispRecoil + (int)add_value).ToString();
        _prop_txt_ary[4].Find("Text1").GetComponent<Text>().text = (next_info.DispRecoil + info.DispRecoil + (int)add_value - level_info.DispRecoil) + (next_info.DispRecoil > level_info.DispRecoil ? " ↑" : "");

        add_value = level_info.DispPierce;
        if (part_add_map.TryGetValue("DispPierce", out vo))
        {
            add_value += Formula(info.DispPierce, vo);
        }
        if (suit_add_map.TryGetValue("DispPierce", out vo))
        {
            add_value += Formula(info.DispPierce, vo);
        }
        _prop_txt_ary[5].Find("Text0").GetComponent<Text>().text = (info.DispPierce + (int)add_value).ToString();
        _prop_txt_ary[5].Find("Text1").GetComponent<Text>().text = (next_info.DispPierce + info.DispPierce + (int)add_value - level_info.DispPierce) + (next_info.DispPierce > level_info.DispPierce ? " ↑" : "");

        add_value = level_info.DispWeight;
        if (part_add_map.TryGetValue("DispWeight", out vo))
        {
            add_value += Formula(info.DispWeight, vo);
        }
        if (suit_add_map.TryGetValue("DispWeight", out vo))
        {
            add_value += Formula(info.DispWeight, vo);
        }
        _prop_txt_ary[6].Find("Text0").GetComponent<Text>().text = (info.DispWeight + (int)add_value).ToString();
        _prop_txt_ary[6].Find("Text1").GetComponent<Text>().text = (next_info.DispWeight + info.DispWeight + (int)add_value - level_info.DispWeight) + (next_info.DispWeight > level_info.DispWeight ? " ↑" : "");

        add_value = level_info.DispClipCap;
        if (part_add_map.TryGetValue("DispClipCap", out vo))
        {
            add_value += Formula(info.DispClipCap, vo);
        }
        if (suit_add_map.TryGetValue("DispClipCap", out vo))
        {
            add_value += Formula(info.DispClipCap, vo);
        }
        _prop_txt_ary[7].Find("Text0").GetComponent<Text>().text = (info.DispClipCap + (int)add_value).ToString();
        _prop_txt_ary[7].Find("Text1").GetComponent<Text>().text = (next_info.DispClipCap + info.DispClipCap + (int)add_value - level_info.DispClipCap) + (next_info.DispClipCap > level_info.DispClipCap ? " ↑" : "");
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}

