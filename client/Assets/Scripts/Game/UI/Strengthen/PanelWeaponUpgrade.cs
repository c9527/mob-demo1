﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelWeaponUpgrade : BasePanel
{
    private ConfigWeaponStrengthenStateLine _upgrade_info;

    Image _item_img;
    Text _name_txt;
    Text _level_txt;
    Transform[] _material_list;
    Image[] _before_stars;
    Image[] _after_stars;
    Transform[] _prop_txt_ary;

    public PanelWeaponUpgrade()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelWeaponUpgrade");

        AddPreLoadRes("UI/Strengthen/PanelWeaponUpgrade", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        _item_img = m_tran.Find("item_img").GetComponent<Image>();
        _name_txt = m_tran.Find("name_txt").GetComponent<Text>();
        _level_txt = m_tran.Find("level_txt").GetComponent<Text>();
        _prop_txt_ary = new Transform[10];
        for (int i = 0; i < _prop_txt_ary.Length; i++)
        {
            _prop_txt_ary[i] = m_tran.Find("prop_txt_" + i);
        }
        _material_list = new Transform[4];
        _before_stars = new Image[3];
        _after_stars = new Image[_before_stars.Length];
        for (int i = 0; i < _material_list.Length; i++)
        {
            _material_list[i] = m_tran.Find("material_" + i);
            if (i < _before_stars.Length)
            {
                _before_stars[i] = m_tran.Find("b_star_" + i).GetComponent<Image>();
                _after_stars[i] = m_tran.Find("a_star_" + i).GetComponent<Image>();
            }
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close_btn")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("upgrade_btn")).onPointerClick += onUpgradeBtnClick;

        for (int i = 0; i < _material_list.Length; i++)
        {
            UGUIClickHandler.Get(_material_list[i].gameObject).onPointerClick += OnShowItemGuideWin;
        }
    }

    private void OnShowItemGuideWin(GameObject target, PointerEventData eventData)
    {
        var index = int.Parse(target.name.Substring(target.name.LastIndexOf('_') + 1));
        if (_upgrade_info != null && index<_upgrade_info.Consume.Length)
        {
            UIManager.PopPanel<PanelItemGuide>(new object[1] {_upgrade_info.Consume[index] }, true, this);
        }
        else
        {
            UIManager.ShowTipPanel("进阶配置数据错误");
        }
    }

    private void onUpgradeBtnClick(GameObject target, PointerEventData eventData)
    {
        if (!HasParams() || !(m_params[0] is ConfigItemWeaponLine))
        {
            return;
        }
        var info = m_params[0] as ConfigItemWeaponLine;
        var strengthen_info = StrengthenModel.Instance.getSStrengthenDataByCode(info.ID);
        var next_info = strengthen_info != null ? ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(strengthen_info.level + 1, strengthen_info.state, info.StrengthenGroup) : null;
        if (strengthen_info != null && next_info != null && next_info.State > strengthen_info.state)
        {
            var sitem = ItemDataManager.GetDepotItem(info.ID);
            if (sitem != null)
            {
                StrengthenModel.Instance.TosUpgradeWeapon(sitem.uid);
            }
            else
            {
                HidePanel();
                UIManager.ShowTipPanel("你尚未拥有该枪械");
            }
        }
        else
        {
            HidePanel();
            if (strengthen_info!=null && next_info == null)
            {
                TipsManager.Instance.showTips("此枪械无法再进阶");
            }
            else
            {
                TipsManager.Instance.showTips("熟练度满级可以进阶");
            }
        }
    }

    public override void OnShow()
    {
        if (!HasParams() || !(m_params[0] is ConfigItemWeaponLine))
        {
            return;
        }
        updateViewByData( m_params[0] as ConfigItemWeaponLine);
    }

    private void updateViewByData(ConfigItemWeaponLine info)
    {
        if (info == null)
        {
            return;
        }
        _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
        _item_img.SetNativeSize();
        _name_txt.text = info.DispName;
        var strengthen_info = StrengthenModel.Instance.getSStrengthenDataByCode(info.ID);
        _upgrade_info = strengthen_info!=null ? ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(strengthen_info.weapon_id,strengthen_info.state+1) : null;
        for (int i = 0; i < _material_list.Length; i++)
        {
            var item = _material_list[i];
            if (_upgrade_info != null && i<_upgrade_info.Consume.Length)
            {
                item.gameObject.TrySetActive(true);
                var material_info = ItemDataManager.GetItem(_upgrade_info.Consume[i].ID);
                var img = item.Find("Image").GetComponent<Image>();
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, material_info.Icon));
                img.SetNativeSize();
                var lbl = item.Find("Text").GetComponent<Text>();
                var p_item = ItemDataManager.GetDepotItem(_upgrade_info.Consume[i].ID);
                lbl.text = string.Format("<color='{2}'>{0}/{1}</color>", p_item != null ? p_item.item_cnt : 0, _upgrade_info.Consume[i].cnt, p_item != null && p_item.item_cnt >= _upgrade_info.Consume[i].cnt ? "#00ff00" : "#ff0000");
            }
            else
            {
                item.gameObject.TrySetActive(false);
            }
        }
        for (int j = 0; j < _before_stars.Length; j++)
        {
            var b_star = _before_stars[j];
            var a_star = _after_stars[j];
            b_star.enabled = strengthen_info!=null && j < strengthen_info.state;
            a_star.enabled = _upgrade_info!=null && j < _upgrade_info.State;
        }
        var prop_txt = "";
        var level_info = strengthen_info!=null ? ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(strengthen_info.level,strengthen_info.state,info.StrengthenGroup) : null;
        level_info = level_info != null ? level_info : new TDStrengthenLevel();
        var next_info = _upgrade_info!=null ? ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(1,_upgrade_info.State,info.StrengthenGroup) : null;
        next_info = next_info != null ? next_info : new TDStrengthenLevel();
        var current_levels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(strengthen_info.state, info.StrengthenGroup);
        var next_levels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(next_info.State, info.StrengthenGroup);
        var current_upper = current_levels != null && current_levels.Length > 0 ? current_levels[current_levels.Length - 1] : null;
        var next_upper = next_levels != null && next_levels.Length > 0 ? next_levels[next_levels.Length - 1] : null;

        _prop_txt_ary[0].Find("Text0").GetComponent<Text>().text = (current_upper != null ? current_upper.Level : 0).ToString();
        _prop_txt_ary[0].Find("Text1").GetComponent<Text>().text = (next_upper != null ? next_upper.Level : 0).ToString();

        _prop_txt_ary[1].Find("Text0").GetComponent<Text>().text = Math.Min(level_info.State + 1, 4).ToString();
        _prop_txt_ary[1].Find("Text1").GetComponent<Text>().text = Math.Min(next_info.State + 1, 4).ToString();

        _prop_txt_ary[2].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispDamage;
        _prop_txt_ary[2].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispDamage + (next_info.DispDamage > level_info.DispDamage ? " ↑" : "");

        _prop_txt_ary[3].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispShootSpeed;
        _prop_txt_ary[3].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispShootSpeed + (next_info.DispShootSpeed > level_info.DispShootSpeed ? " ↑" : "");

        _prop_txt_ary[4].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispFireRange;
        _prop_txt_ary[4].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispFireRange + (next_info.DispFireRange > level_info.DispFireRange ? " ↑" : "");

        _prop_txt_ary[5].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispAccuracy;
        _prop_txt_ary[5].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispAccuracy + (next_info.DispAccuracy > level_info.DispAccuracy ? " ↑" : "");

        _prop_txt_ary[6].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispRecoil;
        _prop_txt_ary[6].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispRecoil + (next_info.DispRecoil > level_info.DispRecoil ? " ↑" : "");

        _prop_txt_ary[7].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispPierce;
        _prop_txt_ary[7].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispPierce + (next_info.DispPierce > level_info.DispPierce ? " ↑" : "");

        _prop_txt_ary[8].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispWeight;
        _prop_txt_ary[8].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispWeight + (next_info.DispWeight > level_info.DispWeight ? " ↑" : "");

        _prop_txt_ary[9].Find("Text0").GetComponent<Text>().text = "+" + level_info.DispClipCap;
        _prop_txt_ary[9].Find("Text1").GetComponent<Text>().text = "+" + next_info.DispClipCap + (next_info.DispClipCap > level_info.DispClipCap ? " ↑" : "");

        _level_txt.text = string.Format("熟练度：<color={2}>{0}/{1}</color>", strengthen_info.level, current_upper != null ? current_upper.Level : 0, current_upper != null && strengthen_info.level>=current_upper.Level ? "#00ff00" : "#ff0000");
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }

    //=================================== s to c
    private void Toc_player_upgrade_weapon_strengthen_state(toc_player_upgrade_weapon_strengthen_state data)
    {
        HidePanel();
    }
}

