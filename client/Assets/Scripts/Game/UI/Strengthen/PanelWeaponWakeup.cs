﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelWeaponWakeup : BasePanel
{
    private ConfigItemWeaponLine _data;
    private p_weapon_strength_info_toc _sdata;

    private ConfigItemLine _materail_data;

    public Transform weapon_item;
    public Transform material_item;
    public Transform lucky_value_pb;
    public GameObject level_render;
    public Transform last_level_canvas;
    public Transform next_level_canvase;
    public Text info_txt;
    public Text cost_txt;
    public Button confirm_btn;
    private UIEffect _effect_player;

    public PanelWeaponWakeup()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelWeaponWakeup");

        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
        AddPreLoadRes("UI/Strengthen/PanelWeaponWakeup", EResType.UI);
    }

    public override void Init()
    {
        _materail_data = ItemDataManager.GetItem(ConfigManager.GetConfig<ConfigMisc>().GetLine("weapon_awaken_item").ValueInt);

        weapon_item = transform.Find("frame/weapon_item");
        material_item = transform.Find("frame/material_item");
        lucky_value_pb = transform.Find("frame/lucky_value_pb");
        level_render = transform.Find("frame/LevelItemRender").gameObject;
        last_level_canvas = transform.Find("frame/last_level_canvas");
        next_level_canvase = transform.Find("frame/next_level_canvas");
        info_txt = transform.Find("frame/ScrollDataGrid/info_txt").GetComponent<Text>();
        cost_txt = transform.Find("frame/confirm_btn/cost_txt").GetComponent<Text>();
        confirm_btn = transform.Find("frame/confirm_btn").GetComponent<Button>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(transform.Find("frame/close_btn")).onPointerClick += (target, evt) => HidePanel();
        UGUIClickHandler.Get(confirm_btn.gameObject).onPointerClick += OnConfirmBtnClick;
    }

    private void OnConfirmBtnClick(GameObject target, PointerEventData eventData)
    {
        var info = _sdata != null ? ConfigManager.GetConfig<ConfigWeaponAwakenGroup>().GetLine(_sdata.weapon_id) : null;
        if (info == null)
        {
            return;
        }
        UIManager.ShowTipPanel(string.Format("是否消耗{0}个{1}进行觉醒？", info.ItemNum, _materail_data != null ? _materail_data.DispName : "???"), OnConfirmWakeup);
    }

    private void OnConfirmWakeup()
    {
        var sdata = _data != null ?  ItemDataManager.GetDepoUnlimitItem(_data.ID) : null;
        if (sdata == null)
        {
            return;
        }
        StrengthenModel.Instance.TosWakeupWeapon(sdata.uid);
        //StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID).awaken_data.level = 5;
        //Toc_player_weapon_awaken(new toc_player_weapon_awaken() { ret_code = true, weapon_id=_data.ID });
    }

    public override void OnShow()
    {
        _data = null;
        _sdata = null;
        if (HasParams())
        {
            _data = m_params[0] as ConfigItemWeaponLine;
        }
        if (_data != null)
        {
            _sdata = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);
        }
        OnUpdateView();
        if (_effect_player == null)
        {
            //_effect_player = UIEffect.ShowEffect(weapon_item, EffectConst.UI_STRENGTHEN_AWAKEN_BG,new Vector2(0,-10));
        }
    }

    private void OnUpdateView()
    {
        if (_data == null || _sdata == null)
        {
            return;
        }
        var img = weapon_item.Find("item_img").GetComponent<Image>();
        img.SetSprite(ResourceManager.LoadIcon(_data.Icon),_data.SubType);
        img.SetNativeSize();
        weapon_item.Find("name_txt").GetComponent<Text>().text = _data.DispName;

        if(_materail_data!=null)
        {
            img = material_item.Find("avatar_img").GetComponent<Image>();
            img.SetSprite(ResourceManager.LoadIcon(_materail_data.Icon), _materail_data.SubType);
            img.SetNativeSize();
            material_item.Find("name_txt").GetComponent<Text>().text = _materail_data.DispName;
            var sitem = ItemDataManager.GetDepotItem(_materail_data.ID);
            material_item.Find("num_txt").GetComponent<Text>().text = (sitem!=null ? sitem.item_cnt : 0).ToString();
        }

        var awaken_data = _sdata.awaken_data != null ? _sdata.awaken_data : new p_awaken_data();
        var next_info = ConfigManager.GetConfig<ConfigWeaponAwaken>().GetWakeupLevelData(_sdata.weapon_id, awaken_data.level + 1);
        var max_random = 0;
        if (next_info != null)
        {
            max_random = next_info.GetMaxRandom();
            lucky_value_pb.Find("value_img").GetComponent<Image>().fillAmount = max_random>1 ? (float)awaken_data.times / (max_random-1) : 1;
        }
        else
        {
            lucky_value_pb.Find("value_img").GetComponent<Image>().fillAmount = 1f;
        }
        lucky_value_pb.Find("Text").GetComponent<Text>().text = string.Format("{0}/{1}", awaken_data.times, max_random);

        var awaken_level = awaken_data.level;
        var i = 0;
        Transform item = null;
        for (int j = 0; j < last_level_canvas.childCount; j++)
        {
            item = last_level_canvas.GetChild(j);
            var awaken_img = item.GetComponent<Image>();
            awaken_img.fillAmount = 0f;
        }
        for (int j = 0; j < next_level_canvase.childCount; j++)
        {
            item = next_level_canvase.GetChild(j);
            var awaken_img = item.GetComponent<Image>();
            awaken_img.fillAmount = 0f;
        }
            for (i = 0; i < awaken_level; i++)
            {
                var index = i / 2;
                if (index < last_level_canvas.childCount)
                {
                    item = last_level_canvas.GetChild(index);
                }
                else
                {
                    item = (UIHelper.Instantiate(level_render) as GameObject).transform;
                    item.SetParent(last_level_canvas, false);
                }
                item.gameObject.TrySetActive(true);
                var awaken_img = item.GetComponent<Image>();
                awaken_img.fillAmount = i % 2 == 1 ? 1 : 0.5f;
            }
        var rest = last_level_canvas.childCount - awaken_level;
        while (rest > 0)
        {
            last_level_canvas.GetChild(last_level_canvas.childCount-rest).gameObject.TrySetActive(false);
            rest--;
        }

        awaken_level = next_info != null ? next_info.AwakenLevel : awaken_data.level;
        for (i = 0; i < awaken_level; i++)
        {
            var index = i / 2;
            if (index < next_level_canvase.childCount)
            {
                item = next_level_canvase.GetChild(index);
            }
            else
            {
                item = (UIHelper.Instantiate(level_render) as GameObject).transform;
                item.SetParent(next_level_canvase,false);
            }
            item.gameObject.TrySetActive(true);
            var awaken_img = item.GetComponent<Image>();
            awaken_img.fillAmount = i % 2 == 1 ? 1 : 0.5f;
        }
        rest = next_level_canvase.childCount - awaken_level;
        while (rest > 0)
        {
            next_level_canvase.GetChild(next_level_canvase.childCount - rest).gameObject.TrySetActive(false);
            rest--;
        }

        if (next_info != null)
        {
            var str =  next_info.Desc;
            if (next_info.PromoteCon != null && next_info.PromoteCon.Length > 0)
            {
                var temp = new List<string>();
                foreach (var code in next_info.PromoteCon)
                {
                    var titem = ItemDataManager.GetItem(code);
                    if(titem!=null)
                    {
                        temp.Add(titem.DispName);
                    }
                }
                str += string.Format("\n晋升（永久拥有{0}）：{1}",string.Join("、",temp.ToArray()),next_info.PromoteDesc);
            }
            info_txt.text =  str;
            cost_txt.text = ConfigManager.GetConfig<ConfigWeaponAwakenGroup>().GetLine(_sdata.weapon_id).ItemNum.ToString();
        }
        else
        {
            info_txt.text = "<color=#64c6ff>已觉醒至满级</color>";
            cost_txt.text = "";
        }
    }

    public override void OnHide()
    {
        if (_effect_player != null)
        {
            _effect_player.Destroy();
            _effect_player = null;
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    //===========================================s to c
    private void Toc_player_weapon_awaken(toc_player_weapon_awaken data)
    {
        if (!IsOpen())
        {
            return;
        }
        if (_data != null && _data.ID == data.weapon_id)
        {
            _sdata = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);
            UIEffect.ShowEffect(weapon_item, EffectConst.UI_STRENGTHEN_AWAKEN,2f, new Vector2(-95, -35));
            if (data.ret_code)
                AudioManager.PlayUISound(AudioConst.strengthenWakeUpSuccess);
            else
                AudioManager.PlayUISound(AudioConst.strengthenWakeUpFail);
            TimerManager.SetTimeOut(1.5f, () =>
            {
                if (!IsOpen())
                {
                    return;
                }
                if (data.ret_code)
                {
                    UIEffect.ShowEffect(weapon_item, EffectConst.UI_STRENGTHEN_AWAKEN_SUCCESS,1.5f,new Vector2(30,10));
                }
                else
                {
                    UIEffect.ShowEffect(weapon_item, EffectConst.UI_STRENGTHEN_AWAKEN_FAIL,1.5f, new Vector2(30, 10));
                }
                OnUpdateView();
            });
        }
    }
}
