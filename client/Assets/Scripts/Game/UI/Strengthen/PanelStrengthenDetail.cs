﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

public class PanelStrengthenDetail : BasePanel
{
    ConfigItemWeaponLine _data;
    p_item _sdata;
    p_weapon_strength_info_toc _strengthen_data = null;
    toc_player_stop_weapon_train _gain_data = null;
    private RawImage m_weaponModel;
    
    /// <summary>
    /// 0-normal,1-sell mode,2-appraise mode
    /// </summary>
    int _op_mode;
    int _view_type;
    int _selected_pos;
    ItemDropInfo[] _part_type_data;
    Vector3 _tween_data;
    float _tween_v;
    Queue<Vector3> _tween_list;

    GameObject _pop_frame;
    Mask _mask_frame;
    GameObject _info_frame;
    GameObject _part_frame;
    //GameObject _awaken_prop_render;
    //Transform _awaken_prop_list;
    RectTransform _awaken_prop_txt;
    Text _name_txt;
    Text _info_txt;
    Image _item_img;
    Transform _exp_pb;
    Text _desc_txt;
    //Transform[] _prop_txt_list;
    Image _upgrade_tip;
    GameObject _awake_tip;
    PartSetItemRender[] _set_part_list;
    DataGrid _part_dg;
    //PartItemInfoRender _set_part_item;
    //PartItemInfoRender _show_part_item;
    Button _extend_btn;
    Button _cancel_btn;
    Button _sell_btn;
    Button _appraise_btn;
    Button _action_btn;

    UIEffect _item_effect;
    UIEffect _item_effect_cold;

    private GameObject m_goRenderTexture;
//    private Camera m_weaponCamera;
//    private Transform m_tranWeapon;
//    private string m_lastWeaponModeName;
    private Transform m_tranStarParent;
    private Transform _awakenGroup;
    private bool m_userDraging;     //用户正在拖拽模型
    private float m_rotationCenter = 2000.12f; //模型旋转的中心
    private Text m_specialAttrText;
    private GameObject m_textNoAttr;
    private DataGrid m_starList;
    private GameObject m_starItem0;

    private GameObject m_train_btn;
    private GameObject m_upgrade_btn;
    private GameObject m_active_btn;

    private GameObject m_superAttr;
    private GameObject m_awaken_prop_txt;
    private GameObject m_bottomLine;


    //强化开启
    private GameObject m_nowTxt;
    private GameObject m_nextTxt;
    private List<GameObject> m_arrowList = new List<GameObject>();
    private List<GameObject> m_leftTextList = new List<GameObject>();
    private List<GameObject> m_rightTextList = new List<GameObject>();
    private Vector3 m_leftPos=new Vector3(95,0,0);
    private Vector3 m_middlePos=new Vector3(159,0,0);
    private Vector3 m_leftTextPos = new Vector3(-192, 110, 0);
    private Vector3 m_middleTextPos = new Vector3(-123, 110, 0);

    private Image m_levelTag;
    private GameObject m_levelTagObj;
    private GameObject m_starGroup;

    private Text m_buyBtnTxt;
    private ConfigMallLine m_weaponLine;

    private GameObject m_tequanBtn;
    private ConfigItemWeaponLine m_itemline;

    private DataGrid m_propGrid;

    private GameObject m_tabAwaken;
    private GameObject m_tabProp;

    private Text m_Txtawaken;
    private Text m_Txtprop;

    private Text m_Typetxt;

    private GameObject m_weaponDrag;

    enum shopType
    {
        MALLSHOP = 0,
        SHARESHOP,
        CORPSSHOP,
        SECRETSHOP,
        HEROSHOP,
        STORYSHOP,
        NOSHOP
    }

    shopType m_shoptype;
    public PanelStrengthenDetail()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthenDetail");

        AddPreLoadRes("UI/Strengthen/PanelStrengthenDetail", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
//        AddPreLoadRes("UI/Strengthen/WeaponTexture", EResType.RenderTexture);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON, EResType.Material);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON_CRYSTAL, EResType.Material);
    }

    public override void Init()
    {
        _part_type_data = new ItemDropInfo[] {
            new ItemDropInfo(){ type="",label="全部"},
            new ItemDropInfo(){ type="1",label="枪口配件"},
            new ItemDropInfo(){ type="2",label="前握把"},
            new ItemDropInfo(){ type="3",label="弹夹"},
            new ItemDropInfo(){ type="4",label="瞄具"},
        };
        
        m_buyBtnTxt = m_tran.Find("MainFrame/grid/buy_btn/Text").GetComponent<Text>();
        _pop_frame = m_tran.Find("PopFrame").gameObject;
        _pop_frame.AddComponent<SortingOrderRenderer>();
        _mask_frame = m_tran.Find("LFrame").GetComponent<Mask>();
        _info_frame = m_tran.Find("LFrame/InfoFrame").gameObject;
        _part_frame = m_tran.Find("LFrame/PartFrame").gameObject;

        //_awaken_prop_list = m_tran.Find("LFrame/InfoFrame/content/awaken_prop_list");
        //_awaken_prop_render = m_tran.Find("LFrame/InfoFrame/content/AwakenPropRender").gameObject;
        _awaken_prop_txt = m_tran.Find("LFrame/InfoFrame/awaken/awaken_prop_txt").transform as RectTransform;

        _desc_txt = m_tran.Find("LFrame/InfoFrame/content/desc_txt").GetComponent<Text>();
        //_prop_txt_list = new Transform[8];
        //for (int i = 0; i < _prop_txt_list.Length; i++)
        //{
        //    _prop_txt_list[i] = m_tran.Find("LFrame/InfoFrame/content/prop_txt_" + i);
        //}
        _name_txt = m_tran.Find("MainFrame/name_txt").GetComponent<Text>();
        _info_txt = m_tran.Find("MainFrame/info_txt").GetComponent<Text>();
        _item_img = m_tran.Find("MainFrame/item_img").GetComponent<Image>();
        _exp_pb = m_tran.Find("MainFrame/exp_pb");
        m_goRenderTexture = m_tran.Find("MainFrame/weaponImage").gameObject;
        m_tranStarParent = m_tran.Find("MainFrame/starGroup/star");
        _awakenGroup = m_tran.Find("MainFrame/awakenGroup/avatar");
        _upgrade_tip = m_tran.Find("MainFrame/grid/upgrade_btn/bubble_tip").GetComponent<Image>();
        _awake_tip = m_tran.Find("MainFrame/grid/active_btn/bubble_tip").gameObject;
        _set_part_list = new PartSetItemRender[4];
        m_tran.Find("MainFrame/part_group").gameObject.AddComponent<SortingOrderRenderer>();
        for (int i = 0; i < _set_part_list.Length; i++)
        {
            _set_part_list[i] = m_tran.Find("MainFrame/part_group/part_item_" + i).gameObject.AddComponent<PartSetItemRender>();
        }
        var list_go = m_tran.Find("LFrame/PartFrame/part_canvas/part_list");
        _part_dg = list_go.Find("content").gameObject.AddComponent<DataGrid>();
        _part_dg.autoSelectFirst = false;
        _part_dg.SetItemRender(list_go.Find("Render").gameObject,typeof(UIItemRender));

        _extend_btn = m_tran.Find("LFrame/PartFrame/part_canvas/extend_btn").gameObject.GetComponent<Button>();
        _cancel_btn = m_tran.Find("LFrame/PartFrame/part_canvas/cancel_btn").gameObject.GetComponent<Button>();
        _sell_btn = m_tran.Find("LFrame/PartFrame/part_canvas/sell_btn").gameObject.GetComponent<Button>();
        _appraise_btn = m_tran.Find("LFrame/PartFrame/part_canvas/appraise_btn").gameObject.GetComponent<Button>();
        _action_btn = m_tran.Find("LFrame/PartFrame/part_canvas/action_btn").gameObject.GetComponent<Button>();
        _action_btn.gameObject.TrySetActive(false);


        m_train_btn = m_tran.Find("MainFrame/grid/train_btn").gameObject;
        m_upgrade_btn = m_tran.Find("MainFrame/grid/upgrade_btn").gameObject;
        m_active_btn = m_tran.Find("MainFrame/grid/active_btn").gameObject;
        //_set_part_item = m_tran.Find("LFrame/PartFrame/set_item").gameObject.AddComponent<PartItemInfoRender>();
        //_show_part_item = m_tran.Find("LFrame/PartFrame/show_item").gameObject.AddComponent<PartItemInfoRender>();
        //_show_part_item.gameObject.AddComponent<SortingOrderRenderer>();

//        m_weaponCamera = new GameObject("WeaponCamera").AddComponent<Camera>();
//        m_weaponCamera.orthographic = false;
//        m_weaponCamera.orthographicSize = 0.51f;
//        m_weaponCamera.fieldOfView = 35;
//        m_weaponCamera.farClipPlane = 5;
//        m_weaponCamera.targetTexture = ResourceManager.LoadRenderTexture("UI/Strengthen/WeaponTexture");
//        m_weaponCamera.transform.SetLocation(null, m_rotationCenter, 0f, 1.4f);
//        m_weaponCamera.transform.localRotation = Quaternion.Euler(0, 180f, 0);

        m_superAttr = m_tran.Find("LFrame/InfoFrame/content/Static/SuperAttr").gameObject;
        m_awaken_prop_txt = m_tran.Find("LFrame/InfoFrame/awaken/awaken_prop_txt").gameObject;
        m_bottomLine = m_tran.Find("LFrame/InfoFrame/content/Static/bottomLine").gameObject;

        m_specialAttrText = m_tran.Find("LFrame/InfoFrame/content/Static/specialAttrText").GetComponent<Text>();
        m_textNoAttr = m_tran.Find("LFrame/InfoFrame/awaken/textNoAttr").gameObject;
        m_starList = m_tran.Find("LFrame/InfoFrame/awaken/starList").gameObject.AddComponent<DataGrid>();
        m_starItem0 = m_starList.transform.Find("starItem0").gameObject;
        m_starItem0.TrySetActive(false);
        m_starList.SetItemRender(m_starItem0,typeof(ItemStarRender));


        m_levelTagObj = m_tran.Find("MainFrame/levelTag").gameObject;
        m_levelTag = m_levelTagObj.GetComponent<Image>();

        m_nowTxt = m_tran.Find("LFrame/InfoFrame/content/Static/nowTxt").gameObject;
        m_nextTxt = m_tran.Find("LFrame/InfoFrame/content/Static/nextTxt").gameObject;

        m_starGroup = m_tran.Find("MainFrame/starGroup").gameObject;
        m_tequanBtn = m_tran.Find("MainFrame/TequanBtn").gameObject;
        m_tequanBtn.AddComponent<SortingOrderRenderer>();
        //for(int i=0;i<_prop_txt_list.Length;i++)
        //{
        //    m_leftTextList.Add(_prop_txt_list[i].Find("Text0").gameObject);
        //    m_rightTextList.Add(_prop_txt_list[i].Find("Text1").gameObject);
        //}
        for(int i=0;i<8;i++)
        {
            m_arrowList.Add(m_tran.Find(string.Format("LFrame/InfoFrame/content/Static/arrow{0}",i)).gameObject);
        }

        m_propGrid = m_tran.Find("LFrame/InfoFrame/propgrid/content").gameObject.AddComponent<DataGrid>();
        m_propGrid.SetItemRender(m_tran.Find("LFrame/InfoFrame/propgrid/content/ItemPropRender").gameObject, typeof(ItemPropRender));


        m_tabAwaken = m_tran.Find("LFrame/InfoFrame/statGroup/awaken").gameObject;
        m_tabProp = m_tran.Find("LFrame/InfoFrame/statGroup/Stat").gameObject;
        m_Txtawaken = m_tran.Find("LFrame/InfoFrame/statGroup/awaken/Label").gameObject.GetComponent<Text>();
        m_Txtprop = m_tran.Find("LFrame/InfoFrame/statGroup/Stat/Label").gameObject.GetComponent<Text>();

        m_awaken_prop_txt.TrySetActive(false);
        m_Typetxt = m_tran.Find("MainFrame/Type").gameObject.GetComponent<Text>();

        m_weaponDrag = m_tran.Find("MainFrame/weaponDrag").gameObject;
        m_weaponDrag.AddComponent<SortingOrderRenderer>();


        m_weaponModel = m_goRenderTexture.GetComponent<RawImage>();
        m_goRenderTexture.AddComponent<CanvasGroup>().blocksRaycasts = false;
        m_weaponModel.enabled = true;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("MainFrame/grid/buy_btn")).onPointerClick += buyBtnClick;
        UGUIClickHandler.Get(m_tran.Find("MainFrame/grid/train_btn")).onPointerClick += trainBtnClick;
        UGUIClickHandler.Get(m_tran.Find("MainFrame/grid/upgrade_btn")).onPointerClick += upgradeBtnClick;
        UGUIClickHandler.Get(m_tran.Find("MainFrame/grid/active_btn")).onPointerClick += activeBtnClick;
        for (int i = 0; i < _set_part_list.Length; i++)
        {
            UGUIClickHandler.Get(_set_part_list[i].gameObject).onPointerClick += OnSetPartListItemClick;
        }
        UGUIClickHandler.Get(m_tran.Find("LFrame/PartFrame/tab")).onPointerClick += OnPartTabClick;
        UGUIClickHandler.Get(_extend_btn.gameObject).onPointerClick += OnPartFunBtnClick;
        UGUIClickHandler.Get(_cancel_btn.gameObject).onPointerClick += OnPartFunBtnClick;
        UGUIClickHandler.Get(_sell_btn.gameObject).onPointerClick += OnPartFunBtnClick;
        UGUIClickHandler.Get(_appraise_btn.gameObject).onPointerClick += OnPartFunBtnClick;
        UGUIClickHandler.Get(_action_btn.gameObject).onPointerClick += OnPartFunBtnClick;
        _part_dg.onItemSelected = OnPartListItemClick;
        //_set_part_item.OnFunCallBack = OnPartItemFunHandler;
        //_show_part_item.OnFunCallBack = OnPartItemFunHandler;
        //_show_part_item.OnCloseCallBack = (render) => { render.gameObject.TrySetActive(false); };
        UGUIUpDownHandler.Get(m_goRenderTexture).onPointerDown += delegate { m_userDraging = true; };
        UGUIUpDownHandler.Get(m_goRenderTexture).onPointerUp += delegate { m_userDraging = false; };
        UGUIDragHandler.Get(m_weaponDrag).onDrag += OnDrag;
        UGUIClickHandler.Get(m_tran.Find("MainFrame/TequanBtn")).onPointerClick += OnTequanBtnClick;

        UGUIClickHandler.Get(m_tabAwaken).onPointerClick += OnPropTabClick;
        UGUIClickHandler.Get(m_tabProp).onPointerClick += OnPropTabClick;



    }

    private void OnPropTabClick(GameObject target, PointerEventData eventData)
    {
        if (target.name == "Stat")
        {
            m_awaken_prop_txt.TrySetActive(false);
            m_propGrid.gameObject.TrySetActive(true);
        }
        else
        {
            m_awaken_prop_txt.TrySetActive(true);
            m_propGrid.gameObject.TrySetActive(false);
        }
    }

    private void OnTequanBtnClick(GameObject target, PointerEventData eventData)
    {
        object[] obj = new object[2];
        obj[0] = ConfigManager.GetConfig<ConfigWeaponSacredProp>().GetLine(m_itemline.ID);
        obj[1] = m_itemline.DispName;
        UIManager.PopPanel<PanelTequan>(obj, true);
    }

    private void OnPartTabClick(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowDropList(_part_type_data, OnUpdatePartListData, m_tran, target,120,35);
    }

    private void OnUpdatePartListData(ItemDropInfo data = null)
    {
        OnUpdatePartListData(data, 0);
    }

    private void OnUpdatePartListData(ItemDropInfo data,int selected_uid)
    {
        if (_part_frame.activeSelf == false)
        {
            return;
        }
        var part_type = data != null ? data.type : "";
        var source_data = ItemDataManager.GetDepotItemsByType(GameConst.ITEM_TYPE_MATERIAL,GameConst.WEAPON_SUBTYPE_ACCESSORY);
        var temp = new List<CDItemData>();
        for (int i = 0; i < source_data.Length; i++)
        {
            var info = ItemDataManager.GetItem(source_data[i].item_id) as ConfigItemAccessoriesLine;
            if (info!=null && (string.IsNullOrEmpty(part_type) || part_type == info.Part.ToString()))
            {
                temp.Add(new CDItemData(source_data[i]));
            }
        }
        source_data = ItemDataManager.GetDepotItemsByType(GameConst.ITEM_TYPE_EQUIP, GameConst.WEAPON_SUBTYPE_ACCESSORY);
        CDItemData selected_vo = null;
        for (int j = 0; j < source_data.Length; j++)
        {
            var info = ItemDataManager.GetItem(source_data[j].item_id) as ConfigItemAccessoriesLine;
            if (info != null && (string.IsNullOrEmpty(part_type) || part_type == info.Part.ToString()))
            {
                var vo = new CDItemData(source_data[j]);
                temp.Add(vo);
                if (selected_uid>0 && source_data[j].uid == selected_uid)
                {
                    selected_vo = vo;
                }
            }
        }
        if (temp.Count==0)
        {
            _sell_btn.gameObject.TrySetActive(false);
            _appraise_btn.gameObject.TrySetActive(false);
            _extend_btn.gameObject.TrySetActive(true);
        }
        else
        {
            _sell_btn.gameObject.TrySetActive(true);
            _appraise_btn.gameObject.TrySetActive(true);
            _extend_btn.gameObject.TrySetActive(false);
        }
        while (temp.Count < 15)
        {
            temp.Add(null);
        }
        _part_dg.Data = temp.ToArray();
        if (selected_vo != null)
        {
            _part_dg.ShowItemOnTop(selected_vo);
        }
    }

    private void OnPartItemFunHandler(CDItemData data)
    {
        var info = data!=null && data.info!=null ? data.info as ConfigItemAccessoriesLine : null;
        if (info == null)
        {
            return;
        }
        if (info.Type == GameConst.ITEM_TYPE_MATERIAL)
        {//鉴定
            UIManager.ShowTipPanel(string.Format("本次鉴定需要{0}金币？", ConfigMisc.GetInt("accessory_meterial_test_cost")), () => { StrengthenModel.Instance.TosTestPartMeterial(new int[1] { data.sdata.uid }); }, null, true, true);
        }
        else
        {
            if (_sdata != null)
            {
                if(data.sdata.uid>0)
                {//镶嵌
                    var need_replace = false;
                    var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(_sdata.item_id);
                    if (accessoryList != null && accessoryList.Length > 0)
                    {
                        foreach (var spart in accessoryList)
                        {
                            if (spart.pos == _selected_pos)
                            {
                                need_replace = true;
                                break;
                            }
                        }
                    }
                    if (need_replace == false)
                    {
                        StrengthenModel.Instance.TosEquipWeaponPart(_sdata.item_id, data.sdata.uid, _selected_pos);
                    }
                    else
                    {
                        UIManager.ShowTipPanel("已镶嵌的配件将消失，是否镶嵌", () => { StrengthenModel.Instance.TosEquipWeaponPart(_sdata.item_id, data.sdata.uid, _selected_pos); }, null, true, true);	
                    }
                }
                else if(data.extend is KeyValue<int,int>)
                {//拆除
                    var cost = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(data.GetQuality());
                    UIManager.ShowTipPanel(string.Format("要消耗{0}金币进行拆除吗？", cost != null ? cost.UnequipCoin.ToString() : "???"), () => { StrengthenModel.Instance.TosUnEquipWeaponPart(_sdata.item_id, (data.extend as KeyValue<int, int>).value); }, null, true, true);	
                }
            }
            else
            {
                TipsManager.Instance.showTips("你尚未拥有该枪械");
            }
        }
    }

    private void OnSetPartListItemClick(GameObject target, PointerEventData eventData)
    {
        var render = target.GetComponent<PartSetItemRender>();
        var set_vo = render!=null ? render.m_renderData as CDItemData : null;
        int index = int.Parse(target.name.Replace("part_item_", ""));
        if(index>0&&!PanelStrengthenMain.IsStrengthenOpen)
        {
            return;
        }
        if (set_vo == null || set_vo.sdata.item_id==-1)
        {
            TipsManager.Instance.showTips("枪械进阶，开启配件槽");
            return;
        }
        int selid = int.Parse(target.name.Replace("part_item_", "")) + 1;
        UIManager.PopPanel<PanelStrengthenParts>(new object[5] { _data, _sdata, _strengthen_data, _gain_data,selid }, true);


        //if (set_vo.extend is KeyValue<int, int>)
        //{
        //    _selected_pos = (set_vo.extend as KeyValue<int, int>).value;
        //}
        //_selected_pos = Mathf.Max(_selected_pos, 1);
        //if (_part_frame.activeSelf == false)
        //{
        //    _view_type = 1;
        //    SwitchToView(true);
        //    OnUpdatePartListData();
        //}
        //_set_part_item.SetData(set_vo);
    }

    private void OnPartListItemClick(object renderData)
    {
        var vo = renderData as CDItemData;
        //if (vo == null)
        //{
        //    _show_part_item.gameObject.TrySetActive(false);
        //    return;
        //}
        //if (_show_part_item.gameObject.activeSelf==false)
        //{
        //    _show_part_item.gameObject.TrySetActive(true);
        //    SortingOrderRenderer.RebuildAll();
        //}
        //_show_part_item.SetData(vo);
        if (_op_mode == 1)
        {
            foreach(UIItemRender render in _part_dg.ItemRenders)
            {
                if (render!=null && render.m_renderData == vo)
                {
                    render.selected = !vo.selected;
                }
            }
        }
        else if (_op_mode == 2)
        {
            if (vo.info != null && vo.info.Type==GameConst.ITEM_TYPE_MATERIAL)
            {
                foreach (UIItemRender render in _part_dg.ItemRenders)
                {
                    if (render != null && render.m_renderData == vo)
                    {
                        render.selected = !vo.selected;
                    }
                }
            }
        }
    }

    private void OnPartFunBtnClick(GameObject target, PointerEventData eventData)
    {
        if (target == _extend_btn.gameObject)
        {
            UIManager.ShowTipPanel("【闯关挑战-生存模式】可以获得配件", () => { UIManager.ShowPanel<PanelPVE>(); }, null, true, true, "立刻前往");
            return;
        }
        else if (target == _sell_btn.gameObject && _op_mode==0)
        {
            if (_sell_btn.enabled == false)
            {
                return;
            }
            _op_mode = 1;
            _sell_btn.gameObject.TrySetActive(false);
            _appraise_btn.gameObject.TrySetActive(false);
            _action_btn.gameObject.TrySetActive(true);
            _action_btn.transform.Find("Text").GetComponent<Text>().text = "出售所选";
            _action_btn.transform.Find("Image").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "sell_ico"));
            SetupPopFrame(true);
            ClearPartList();
        }
        else if (target == _appraise_btn.gameObject && _op_mode == 0)
        {
            if (_appraise_btn.enabled == false)
            {
                return;
            }
            _op_mode = 2;
            _sell_btn.gameObject.TrySetActive(false);
            _appraise_btn.gameObject.TrySetActive(false);
            _action_btn.gameObject.TrySetActive(true);
            _action_btn.transform.Find("Text").GetComponent<Text>().text = "鉴定所选";
            _action_btn.transform.Find("Image").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "test_ico"));
            SetupPopFrame(true);
            ClearPartList();
        }
        else if (target == _action_btn.gameObject && _op_mode>0)
        {
            var source_data = new List<int>();
            var total_coin = 0;
            if (_part_dg.Data != null)
            {
                var test_cost = ConfigMisc.GetInt("accessory_meterial_test_cost");
                for (int i = 0; i < _part_dg.Data.Length; i++)
                {
                    var vo = _part_dg.Data[i] as CDItemData;
                    if (vo != null && vo.selected == true)
                    {
                        source_data.Add(vo.sdata.uid);
                        var info = ItemDataManager.GetItem(vo.sdata.item_id) as ConfigItemAccessoriesLine;
                        if(info!=null)
                        {
                            if (_op_mode == 1)
                            {
                                var cost_info = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(Mathf.Max(vo.GetQuality(),1));
                                total_coin += cost_info!=null ? cost_info.BuybackCoin : 0;
                            }
                            else
                            {
                                total_coin += test_cost;
                            }
                        }
                    }
                }
            }
            if (source_data.Count > 0)
            {
                if (_op_mode == 1)
                {//出售
                    UIManager.ShowTipPanel(string.Format("全部卖给系统可获得{0}金币？", total_coin), () => { StrengthenModel.Instance.TosSellWeaponPart(source_data.ToArray()); }, null, true, true);
                }
                else if (_op_mode == 2)
                { //鉴定
                    UIManager.ShowTipPanel(string.Format("本次鉴定需要{0}金币？", total_coin), () => { StrengthenModel.Instance.TosTestPartMeterial(source_data.ToArray()); }, null, true, true);
                }
            }
            else
            {
                UIManager.ShowTipPanel("未选择配件");
            }
        }
        else if(target==null || target==_cancel_btn.gameObject && _op_mode>0)
        {
            _op_mode = 0;
            _sell_btn.gameObject.TrySetActive(true);
            _appraise_btn.gameObject.TrySetActive(true);
            _action_btn.gameObject.TrySetActive(false);
            SetupPopFrame(false);
            ClearPartList();
        }
        else
        {
            _op_mode = 0;
            _view_type = 0;
            SwitchToView(true);
        }
    }

    private void SetupPopFrame(bool open=false)
    {
        if (_pop_frame.gameObject.activeSelf != open)
        {
            _pop_frame.gameObject.TrySetActive(open);
            if (_pop_frame.gameObject.activeSelf)
            {
                _part_dg.transform.parent.parent.SetParent(_pop_frame.transform, true);
                _part_dg.transform.parent.parent.SetAsFirstSibling();
                //_show_part_item.transform.SetParent(_pop_frame.transform, true);
                //_show_part_item.transform.parent.parent.SetAsLastSibling();
            }
            else
            {
                _part_dg.transform.parent.parent.SetParent(_part_frame.transform, true);
                _part_dg.transform.parent.parent.SetAsFirstSibling();
                //_show_part_item.transform.SetParent(_part_frame.transform, true);
                //_show_part_item.transform.parent.parent.SetAsLastSibling();
            }
            SortingOrderRenderer.RebuildAll();
        }
    }

    private void ClearPartList()
    {
        if(_part_dg.ItemRenders!=null && _part_dg.ItemRenders.Count>0)
        {
            foreach(var render in _part_dg.ItemRenders)
            {
                if (render is UIItemRender)
                {
                    (render as UIItemRender).selected = false;
                }
            }
        }
    }

    private void SwitchToView(bool tween=false)
    {
        UIManager.HideDropList();
        //_show_part_item.gameObject.TrySetActive(false);
        var hide_view = _view_type == 1 ? _info_frame : _part_frame;
        var show_view = _view_type == 1 ? _part_frame : _info_frame;
        var zero_x = -65;
        var offset_x = -65 - 330;
        if (tween)
        {
            _mask_frame.enabled = true;
            //_mask_frame.GetComponent<Image>().enabled = true;
            hide_view.gameObject.TrySetActive(true);
            show_view.gameObject.TrySetActive(true);
            hide_view.transform.localPosition = new Vector3(zero_x, 0, 0);
            show_view.transform.localPosition = new Vector3(offset_x,0,0);
            //TweenPosition.Begin(hide_view.gameObject, 0.25f, new Vector3(offset_x, 0, 0));
            //TweenPosition.Begin(show_view.gameObject, 0.25f, new Vector3(zero_x, 0, 0)).SetOnFinished(() => { SwitchToView(); });
        }
        else
        {
            SetupPopFrame(false);
            _mask_frame.enabled = false;
            //_mask_frame.GetComponent<Image>().enabled = false;
            hide_view.gameObject.TrySetActive(false);
            show_view.gameObject.TrySetActive(true);
            //hide_view.transform.localPosition = new Vector3(zero_x, 0, 0);
            //show_view.transform.localPosition = new Vector3(zero_x, 0, 0);
            if (show_view == _info_frame && _set_part_list!=null && _set_part_list.Length>0)
            {
                var toggle = _set_part_list[0].GetComponent<Toggle>();
                if (toggle != null && toggle.group != null)
                {
                    toggle.group.SetAllTogglesOff();
                }
            }
        }
    }

    private void OnDrag(GameObject target, PointerEventData eventData)
    {
        WeaponDisplay.Rotation(eventData.delta.x);
    }

    private void trainBtnClick(GameObject target, PointerEventData eventData)
    {
        if(!PanelStrengthenMain.IsStrengthenOpen)
        {
            UIManager.ShowTipPanel(string.Format("{0}级开启枪械训练", PanelStrengthenMain.OpenLv));
            return;
        }
        if (_data == null || _sdata==null)
        {
            TipsManager.Instance.showTips("你尚未拥有此枪");
            return;
        }
        UIManager.ShowPanel<PanelStrengthenTrain>();
    }

    private void updateStrengthenData(p_weapon_strength_info_toc data)
    {
        if (_data == null || _data.ID != data.weapon_id)
        {
            return;
        }
        _sdata = ItemDataManager.GetDepoUnlimitItem(_data.ID);


        _strengthen_data = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);
        UpdateItemDetailView();
		if (data.level == 1 && data.cur_exp == 0)
        {
            UIEffect.ShowEffect(_item_img.transform, EffectConst.UI_STRENGTHEN_UPGRADE, 3f);
            AudioManager.PlayUISound(AudioConst.weaponUpgrade);
            if (data.state > 0 && data.state<m_tranStarParent.childCount)
            {
                var star_obj = m_tranStarParent.GetChild(data.state - 1);
                var start_pos = star_obj.localPosition;
                star_obj.localPosition = new Vector3(0, 140, 0);
                var tween = TweenPosition.Begin(star_obj.gameObject, 0.25f, start_pos);
                tween.delay = 2f;
                tween.AddOnFinished(() => { UIEffect.ShowEffect(star_obj.transform, EffectConst.UI_STRENGTHEN_UPGRADE_STAR,1f);});
            }
        }    
    }

    private void upgradeBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data == null)
        {
            return;
        }
        if (_sdata != null && TimeUtil.GetRemainTimeType(_sdata.time_limit)!=-1)
        {
            var strengthen_info = StrengthenModel.Instance.getSStrengthenDataByCode(_sdata.item_id);
            var next_state_info = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(_sdata.item_id,strengthen_info!=null ? strengthen_info.state+1:1);
            if (next_state_info != null)
            {
                UIManager.PopPanel<PanelWeaponUpgrade>(new object[] { _data }, true, this);
            }
            else
            {
                UIManager.ShowTipPanel("您的枪械已满阶");
            }
        }
        else
        {
			TipsManager.Instance.showTips("你尚未拥有此枪");        
        }    
    }

    private void activeBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_data == null)
        {
            return;
        }
        if (_sdata != null)
        {
            if (StrengthenModel.Instance.IsFullStrengthen(_sdata.item_id) && TimeUtil.GetRemainTimeType(_sdata.time_limit) == 0)
            {
                var info = ConfigManager.GetConfig<ConfigWeaponAwaken>().GetWakeupLevelData(_sdata.item_id, 1);
                if (info != null && info.Open == 1)
                {
                    UIManager.PopPanel<PanelWeaponWakeup>(new object[] { _data }, true, this);
                }
                else
                {
                    UIManager.ShowTipPanel("此枪械觉醒功能暂未开放");
                }
            }
            else
            {
                UIManager.ShowTipPanel("枪械觉醒必须先强化到顶级及永久拥有"); 
            }
        }
        else
        {
            TipsManager.Instance.showTips("你尚未拥有此枪");
        }
    }

    private void buyBtnClick(GameObject target, PointerEventData eventData)
    {
        if(_data==null)
        {
            return;
        }
        if (_sdata != null && _sdata.time_limit == 0 && _data.AddRule != 4)
        {
            UIManager.ShowTipPanel("你已经拥有此武器了，无需再购买！");
            return;
        }
        if (m_shoptype == shopType.MALLSHOP||m_shoptype==shopType.HEROSHOP||m_shoptype==shopType.STORYSHOP)
        {
            var shop_info = ConfigManager.GetConfig<ConfigMall>().GetMallLine(_data.ID);
            UIManager.PopPanel<PanelBuy>(new object[] { shop_info });
        }
        else
        {
            if (_data != null && !string.IsNullOrEmpty(_data.GetMethodGoto))
                UIManager.GotoPanel(_data.GetMethodGoto);
            else
                TipsManager.Instance.showTips("该武器未开放获得途径");
        }
//        else if (m_shoptype == shopType.SECRETSHOP)
//        {
//            var obj = new object[1];
//            obj[0] = null;
//            UIManager.ShowPanel<PanelMall>(obj);
//        }
//        else if (m_shoptype == shopType.SHARESHOP)
//        {
//            string str = "share";
//            object[] obj = new object[1];
//            obj[0] = str; 
//            UIManager.ShowPanel<PanelLoginReward>(obj);
//        }
//        else if (m_shoptype == shopType.NOSHOP)
//        {
//            UIManager.ShowPanel<PanelLotteryNew>();
//        }

    }

    /// <summary>
    /// m_params: [ConfigItemWeaponLine,
    ///             params toc_player_stop_weapon_train,
    ///                    p_weapon_strength_info_toc
    ///                    string(part pos)]
    /// </summary>
    public override void OnShow()
    {
        m_weaponModel.texture = WeaponDisplay.RenderTextureBig;
        m_weaponModel.SetNativeSize();
        WeaponDisplay.Visible = true;
        StrengthenFilter();
        _data = m_params != null && m_params.Length > 0 ? m_params[0] as ConfigItemWeaponLine : null;
        _sdata = _data!=null && _data.ID>0 ? ItemDataManager.GetDepoUnlimitItem(_data.ID): null;
        _gain_data = null;
        _strengthen_data = null;
        m_shoptype = GetShopType();
        if (m_shoptype != shopType.MALLSHOP)
        {
            m_buyBtnTxt.text = "获得";
        }
        else
        {
            m_buyBtnTxt.text = "购买";
        }
        if (HasParams() && m_params.Length > 1)
        {
            _gain_data = m_params[1] as toc_player_stop_weapon_train;
        }
        if (HasParams() && m_params.Length>2)
        {
            _strengthen_data = m_params[2] as p_weapon_strength_info_toc;
        }
        else
        {
            _strengthen_data = _data!=null ? StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID) : null;
        }
        if (HasParams() && m_params.Length>3 && !string.IsNullOrEmpty((string)m_params[3]))
        {
            _view_type = 1;
            _selected_pos = Math.Max(int.Parse(m_params[3].ToString()), 1);
        }
        else
        {
            _view_type = 0;
        }
        var item = ItemDataManager.GetItem(_data.ID);
        m_tequanBtn.SetActive(item.RareType == 5);
        m_itemline = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(item.ID);
        SwitchToView();
        UpdateItemDetailView();
        OnUpdatePartListData();
//        UpdateMode();
        WeaponDisplay.UpdateMode(_data);
        if (_view_type == 1 && (_selected_pos-1)<_set_part_list.Length)
        {
            var render = _set_part_list[_selected_pos - 1];
            if (render.data.sdata.item_id<0)
            {
                render = _set_part_list[0];
            }
            render.GetComponent<Toggle>().isOn = true;
            OnSetPartListItemClick(render.gameObject, null);
        }
//        if (m_weaponCamera)
//            m_weaponCamera.gameObject.TrySetActive(true);
        WeaponDisplay.Visible = true;
        if (_item_effect == null)
            _item_effect = UIEffect.ShowEffectAfter(m_goRenderTexture.transform, EffectConst.UI_STRENGTHEN_WEAPON_SHOW, new Vector2(-218, 4));
        if (_item_effect_cold == null)
            //_item_effect_cold = UIEffect.ShowEffect(m_tran, EffectConst.UI_STRENGTHEN_WEAPON_BG, new Vector3(27, 212, -20));

        if (_data.SubType == "WEAPON1")
        {
            m_Typetxt.text = "主武器";
        }
        else
        {
            m_Typetxt.text = "副武器";
        }
    }


    shopType GetShopType()
    {
        var config = ConfigManager.GetConfig<ConfigMall>();
        m_weaponLine = config.GetMallLine(_data.ID, 0);
        if (m_weaponLine!=null)
        {
            if (ItemDataManager.IsSale(m_weaponLine))
            {
                return shopType.MALLSHOP;
            }
        }

        //m_weaponLine = config.GetMallLine(_data.ID, 3);
        //if (m_weaponLine != null)
        //{
        //    return shopType.SECRETSHOP;
        //}

        m_weaponLine = config.GetMallLine(_data.ID, 1);
        if (m_weaponLine != null)
        {
            if (ItemDataManager.IsSale(m_weaponLine))
            {
                return shopType.SHARESHOP;
            }
        }
        m_weaponLine = config.GetMallLine(_data.ID, 4);
        if (m_weaponLine != null)
        {
            if (ItemDataManager.IsSale(m_weaponLine))
            {
                return shopType.HEROSHOP;
            }
        }

        m_weaponLine = config.GetMallLine(_data.ID, 5);
        if (m_weaponLine != null)
        {
            if (ItemDataManager.IsSale(m_weaponLine))
            {
                return shopType.STORYSHOP;
            }
        }

        return shopType.NOSHOP;
    }
    private void StrengthenFilter()
    {
        if (PanelStrengthenMain.Lev < PanelStrengthenMain.OpenLv)
        {
            //m_train_btn.TrySetActive(false);
            m_upgrade_btn.TrySetActive(false);
            m_active_btn.TrySetActive(false);
            m_superAttr.TrySetActive(false);
            //m_awaken_prop_txt.TrySetActive(false);
            m_bottomLine.TrySetActive(false);
        }
        else
        {
            //m_train_btn.TrySetActive(true);
            m_upgrade_btn.TrySetActive(true);
            m_active_btn.TrySetActive(true);
            m_superAttr.TrySetActive(true);
            //m_awaken_prop_txt.TrySetActive(true);
            m_bottomLine.TrySetActive(true);
        }
        m_starGroup.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);

    }


    private void UpdateItemDetailView()
    {

        if (_data == null || _tween_data != Vector3.zero)
        {
            return;
        }
        if (!string.IsNullOrEmpty(_data.LevelTag))
        {
            m_levelTagObj.TrySetActive(true);
            m_levelTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, _data.LevelTag));
            m_levelTag.SetNativeSize();
        }
        else
        {
            m_levelTagObj.TrySetActive(false);
        }
        var gain_param = _gain_data;
        _gain_data = null;
        if (gain_param == null || _strengthen_data == null)
        {
            _strengthen_data = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);
        }
        if (gain_param != null && gain_param.gain_exp > 0)
        {
            _tween_v = gain_param.gain_exp / 1.5f;
            _tween_list = new Queue<Vector3>();
            var level_ary = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(_strengthen_data.state, _data.StrengthenGroup);
            if (level_ary!=null && level_ary.Length > 0)
            {
                foreach (var t_data in level_ary)
                {
                    if (t_data.Level > _strengthen_data.level)
                    {
                        if (gain_param.gain_exp > 0)
                        {
                            var tween_param = new Vector3(t_data.Level == (_strengthen_data.level + 1) ? _strengthen_data.cur_exp : 0, 0, t_data.NeedExp);
                            tween_param.y = gain_param.gain_exp > (t_data.NeedExp - tween_param.x) ? t_data.NeedExp : (tween_param.x + gain_param.gain_exp);
                            gain_param.gain_exp -= (int)(tween_param.y - tween_param.x);
                            _tween_list.Enqueue(tween_param);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            _tween_data = _tween_list.Count>0 ? _tween_list.Dequeue() : Vector3.zero;
        }
        else
        {
            _tween_data = Vector3.zero;
        }
        var awaken_level = _strengthen_data.awaken_data != null ? _strengthen_data.awaken_data.level : 0;
        //觉醒属性
        var awaken_data = ConfigManager.GetConfig<ConfigWeaponAwaken>().GetWakeupData(_data.ID);
        var prop_index = 0;
        var content_h = Math.Abs(_awaken_prop_txt.transform.localPosition.y);
        var contents = new List<string>();
        //var content_h = Math.Abs(_awaken_prop_list.transform.localPosition.y);
        //WeaponAwakenPropRender render = null;
        for (prop_index = 0; awaken_data != null && prop_index < awaken_data.Length; prop_index++)
        {
            if (awaken_data[prop_index].AwakenLevel > (awaken_level + 2))
            {
                break;
            }
            var temp = new CDAwakenProp(awaken_data[prop_index], _strengthen_data.awaken_data).GenInfoTxt();
            if (!string.IsNullOrEmpty(temp))
            {
                contents.Add(temp);
            }
            //if (prop_index < _awaken_prop_list.childCount)
            //{
            //    render = _awaken_prop_list.GetChild(prop_index).GetComponent<WeaponAwakenPropRender>();
            //}
            //else
            //{
            //    render = (GameObject.Instantiate(_awaken_prop_render) as GameObject).AddComponent<WeaponAwakenPropRender>();
            //    render.transform.SetParent(_awaken_prop_list);
            //    render.transform.localScale = Vector3.one;
            //}
            //render.gameObject.TrySetActive(true);
            //render.SetData(new CDAwakenProp(awaken_data[prop_index], _strengthen_data.awaken_data));
            //content_h += (render.transform as RectTransform).sizeDelta.y;
        }
        //var rest = _awaken_prop_list.childCount - prop_index;
        //while (rest > 0)
        //{
        //    _awaken_prop_list.GetChild(_awaken_prop_list.childCount - rest).gameObject.TrySetActive(false);
        //    rest--;
        //}
        var txt = _awaken_prop_txt.Find("Text").GetComponent<Text>();
        txt.text = string.Join("\n\n",contents.ToArray());
        //_awaken_prop_txt.sizeDelta = new Vector2(_awaken_prop_txt.sizeDelta.x, txt.preferredHeight + 20);
        content_h += _awaken_prop_txt.sizeDelta.y;
        //var info_trans = _info_frame.transform.Find("content") as RectTransform;
        //if (info_trans != null)
        //{
        //    info_trans.sizeDelta = new Vector2(info_trans.sizeDelta.x, content_h);
        //}
        //星级&觉醒
        if (awaken_level>0)
        {
            _awakenGroup.parent.gameObject.TrySetActive(true);
            m_tranStarParent.parent.gameObject.TrySetActive(false);
            var show_num = Mathf.Ceil(awaken_level / 2f);
            for (int i = 0; i < _awakenGroup.childCount; i++)
            {
                var img = _awakenGroup.GetChild(i).GetComponent<Image>();
                img.gameObject.TrySetActive(i < show_num);
                if (img.gameObject.activeSelf)
                {
                    img.fillAmount = awaken_level < (i + 1) * 2 ? 0.5f : 1f;
                }
            }
        }
        else
        {
            _awakenGroup.parent.gameObject.TrySetActive(false);
            if(PanelStrengthenMain.IsStrengthenOpen)
                m_tranStarParent.parent.gameObject.TrySetActive(true);
            for (int j = 0; j < m_tranStarParent.childCount; j++)
            {
                m_tranStarParent.GetChild(j).gameObject.TrySetActive(j < _strengthen_data.state);
            }
        }
        //熟练度
        var state = _strengthen_data != null ? _strengthen_data.state : 0;
        var level = _strengthen_data != null ? _strengthen_data.level : 1;
        var currentLevels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(state, _data.StrengthenGroup);
        var maxLevel = currentLevels.Length > 0 ? currentLevels[currentLevels.Length - 1].Level : 0;
        _info_txt.text = String.Format("等级{0}",  level + "/" + maxLevel);
        HideNextLvInfo(level, maxLevel);
        _name_txt.text = _data.DispName;
        _item_img.SetSprite(ResourceManager.LoadIcon(_data.Icon),_data.SubType);
        _item_img.SetNativeSize();
        _desc_txt.text = _data.Desc;
        var config = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>();
        var level_info = config.getStrengthenLevelInfo(_strengthen_data.level, _strengthen_data.state, _data.StrengthenGroup);
        level_info = level_info != null ? level_info : new TDStrengthenLevel();
        var next_info = config.getStrengthenLevelInfo(_strengthen_data.level + 1, _strengthen_data.state, _data.StrengthenGroup);
        next_info = next_info != null ? next_info : new TDStrengthenLevel();
        var part_add_map = StrengthenModel.Instance.GenPartPropAddMap(_sdata);
        var suit_add_map = StrengthenModel.Instance.GenSuitPropAddMap(_sdata);
        KeyValue<int, int> vo = null;
        Func<int, KeyValue<int, int>, float> Formula = (v, p) => p.key == 1 ? v * p.value / 1000f : p.value;
        var add_value = 0f;

        add_value = level_info.DispDamage;
        //if (part_add_map.TryGetValue("DispDamage", out vo))
        //{
        //    add_value += Formula(_data.DispDamage, vo);
        //}
        //if (suit_add_map.TryGetValue("DispDamage", out vo))
        //{
        //    add_value += Formula(_data.DispDamage, vo);
        //}
        //_prop_txt_list[0].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispDamage + (int)add_value);
        //_prop_txt_list[0].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispDamage + _data.DispDamage + (int)add_value - level_info.DispDamage).ToString() : "满级";

        //add_value = level_info.DispShootSpeed;
        //if (part_add_map.TryGetValue("DispShootSpeed", out vo))
        //{
        //    add_value += Formula(_data.DispShootSpeed, vo);
        //}
        //if (suit_add_map.TryGetValue("DispShootSpeed", out vo))
        //{
        //    add_value += Formula(_data.DispShootSpeed, vo);
        //}
        //_prop_txt_list[1].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispShootSpeed + (int)add_value);
        //_prop_txt_list[1].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispShootSpeed + _data.DispShootSpeed + (int)add_value - level_info.DispShootSpeed).ToString() : "满级";

        //add_value = level_info.DispFireRange;
        //if (part_add_map.TryGetValue("DispFireRange", out vo))
        //{
        //    add_value += Formula(_data.DispFireRange, vo);
        //}
        //if (suit_add_map.TryGetValue("DispFireRange", out vo))
        //{
        //    add_value += Formula(_data.DispFireRange, vo);
        //}
        //_prop_txt_list[2].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispFireRange + (int)add_value);
        //_prop_txt_list[2].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispFireRange + _data.DispFireRange + (int)add_value - level_info.DispFireRange).ToString() : "满级";

        //add_value = level_info.DispAccuracy;
        //if (part_add_map.TryGetValue("DispAccuracy", out vo))
        //{
        //    add_value += Formula(_data.DispAccuracy, vo);
        //}
        //if (suit_add_map.TryGetValue("DispAccuracy", out vo))
        //{
        //    add_value += Formula(_data.DispAccuracy, vo);
        //}
        //_prop_txt_list[3].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispAccuracy + (int)add_value);
        //_prop_txt_list[3].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispAccuracy + _data.DispAccuracy + (int)add_value - level_info.DispAccuracy).ToString() : "满级";

        //add_value = level_info.DispRecoil;
        //if (part_add_map.TryGetValue("DispRecoil", out vo))
        //{
        //    add_value += Formula(_data.DispRecoil, vo);
        //}
        //if (suit_add_map.TryGetValue("DispRecoil", out vo))
        //{
        //    add_value += Formula(_data.DispRecoil, vo);
        //}
        //_prop_txt_list[4].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispRecoil + (int)add_value);
        //_prop_txt_list[4].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispRecoil + _data.DispRecoil + (int)add_value - level_info.DispRecoil).ToString() : "满级";

        //add_value = level_info.DispPierce;
        //if (part_add_map.TryGetValue("DispPierce", out vo))
        //{
        //    add_value += Formula(_data.DispPierce, vo);
        //}
        //if (suit_add_map.TryGetValue("DispPierce", out vo))
        //{
        //    add_value += Formula(_data.DispPierce, vo);
        //}
        //_prop_txt_list[5].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispPierce + (int)add_value);
        //_prop_txt_list[5].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispPierce + _data.DispPierce + (int)add_value - level_info.DispPierce).ToString() : "满级";

        //add_value = level_info.DispWeight;
        //if (part_add_map.TryGetValue("DispWeight", out vo))
        //{
        //    add_value += Formula(_data.DispWeight, vo);
        //}
        //if (suit_add_map.TryGetValue("DispWeight", out vo))
        //{
        //    add_value += Formula(_data.DispWeight, vo);
        //}
        //_prop_txt_list[6].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispWeight + (int)add_value);
        //_prop_txt_list[6].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispWeight + _data.DispWeight + (int)add_value - level_info.DispWeight).ToString() : "满级";

        //add_value = level_info.DispClipCap;
        //if (part_add_map.TryGetValue("DispClipCap", out vo))
        //{
        //    add_value += Formula(_data.DispClipCap, vo);
        //}
        //if (suit_add_map.TryGetValue("DispClipCap", out vo))
        //{
        //    add_value += Formula(_data.DispClipCap, vo);
        //}
        //_prop_txt_list[7].Find("Text0").GetComponent<Text>().text = string.Format("{0}", _data.DispClipCap + (int)add_value);
        //_prop_txt_list[7].Find("Text1").GetComponent<Text>().text = next_info.Level > 0 ? (next_info.DispClipCap + _data.DispClipCap + (int)add_value - level_info.DispClipCap).ToString() : "满级";


        m_propGrid.Data = ItemDataManager.GetPropRenderInfos(_data,true);


        if (next_info != null && next_info.NeedExp > 0)
        {
            _exp_pb.Find("pb_value").GetComponent<Image>().fillAmount = _strengthen_data.cur_exp * 1f / next_info.NeedExp;
            _exp_pb.Find("pb_label").GetComponent<Text>().text = string.Format("{0}/{1}", _strengthen_data.cur_exp, next_info.NeedExp);
        }
        else
        {
            _exp_pb.Find("pb_value").GetComponent<Image>().fillAmount = 1;
            _exp_pb.Find("pb_label").GetComponent<Text>().text = "满级";
        }
        var set_data = StrengthenModel.Instance.GenWeaponPartCDData(_sdata, _strengthen_data.state);
        var suit_map = new Dictionary<int,int>();
        if(set_data!=null && set_data.Length>0)
        {
            for(var j=0;j<set_data.Length;j++)
            {
                if(set_data[j].info is ConfigItemAccessoriesLine && (set_data[j].info as ConfigItemAccessoriesLine).Suit>0)
                {
                    var suit_num = 0;
                    suit_map.TryGetValue((set_data[j].info as ConfigItemAccessoriesLine).Suit, out suit_num);
                    suit_num += 1;
                    suit_map[(set_data[j].info as ConfigItemAccessoriesLine).Suit] = suit_num;
                }
            }
        }
        for (int i = 0;i < _set_part_list.Length; i++)
        {
            var set_vo = set_data != null && i < set_data.Length ? set_data[i] : null;
            _set_part_list[i].SetData(set_vo);
            var set_suit_num = 0;
            if (set_vo != null && set_vo.info is ConfigItemAccessoriesLine)
            {
                suit_map.TryGetValue((set_vo.info as ConfigItemAccessoriesLine).Suit,out set_suit_num); 
            }
            _set_part_list[i].PlaySuitEffect(set_suit_num);
        }
        //是否符合进阶条件
        var can_upgrade = level_info != null && next_info != null && next_info.State > level_info.State;
        if (can_upgrade==true)
        {
            var state_info = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(_strengthen_data.weapon_id, next_info.State);
            can_upgrade = state_info != null;
            if (can_upgrade==true)
            {
                foreach (ItemInfo item_info in state_info.Consume)
                {
                    var sitem = ItemDataManager.GetDepotItem(item_info.ID);
                    if (sitem == null || sitem.item_cnt < item_info.cnt)
                    { 
                        can_upgrade = false;
                        break;
                    }
                }
            }
        } 
        _upgrade_tip.enabled = can_upgrade;
        _awake_tip.TrySetActive(ItemDataManager.CanWeaponAwaken(_data.ID));
        //特殊属性
        if(_data.SpecialAtt.Length!=0)
        {
            var cfg = ConfigManager.GetConfig<ConfigWeaponSpecialAttr>().GetLine(_data.SpecialAtt[0]);
            m_starList.gameObject.TrySetActive(true);
            m_textNoAttr.TrySetActive(false);
            m_specialAttrText.text = cfg.Name;
            string[] strs=new string[_data.SpecialAtt[1]];
            for(int i=0;i<strs.Length;i++)
            {
                strs[i] = cfg.Icon;
            }
            m_starList.Data = strs;
        }
        else
        {
            m_starList.gameObject.TrySetActive(false);
            m_textNoAttr.TrySetActive(true);
            m_specialAttrText.text = "特殊属性";
        }
    }

    /// <summary>
    /// 熟练度已满，隐藏下一个等级信息
    /// </summary>
    private void HideNextLvInfo(int level,int maxLevel)
    {
        if (level >= maxLevel) //到最高等级
        {
            m_nowTxt.transform.localPosition = m_middleTextPos; ;
            m_nextTxt.TrySetActive(false);
            var textArr = m_leftTextList.ToArray();
            m_superAttr.TrySetActive(false);
            //m_awaken_prop_txt.TrySetActive(false);
            m_bottomLine.TrySetActive(false);
            for (int i = 0; i < textArr.Length; i++)
            {
                m_leftTextList[i].transform.localPosition = m_middlePos;
                m_rightTextList[i].TrySetActive(false);
                m_arrowList[i].TrySetActive(false);
            }
        }
        else
        {
            m_nowTxt.transform.localPosition = m_leftTextPos;
            m_nextTxt.TrySetActive(true);
            var textArr = m_leftTextList.ToArray();
            m_superAttr.TrySetActive(true);
            //m_awaken_prop_txt.TrySetActive(true);
            m_bottomLine.TrySetActive(true);
            for (int i = 0; i < textArr.Length; i++)
            {
                m_leftTextList[i].transform.localPosition = m_leftPos;
                m_rightTextList[i].TrySetActive(true);
                m_arrowList[i].TrySetActive(true);
            }
        }
    }

//    private void UpdateMode()
//    {
//        if (_data != null)
//        {
//            string model = _data.TujianModel.IsNullOrEmpty() ? _data.MPModel : _data.TujianModel;
//            if (m_lastWeaponModeName != model)
//            {
//                m_lastWeaponModeName = model;
//                ResourceManager.LoadModel("Weapons/" + m_lastWeaponModeName, (go) =>
//                {
//                    if (!m_go)
//                    {
//                        Object.Destroy(go);
//                        return;
//                    }
//
//                    if (m_tranWeapon)
//                        Object.DestroyImmediate(m_tranWeapon.gameObject);
//
//                    m_goRenderTexture.TrySetActive(true);
//                    m_tranWeapon = go.transform;
//                    m_tranWeapon.localPosition = new Vector3(2000, 0, 0);
//                    m_tranWeapon.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
//
//                    // 排除特效节点
//                    ExtendFuncHelper.CallBackBool<GameObject> funVisit = (GameObject child)=>
//                    {
//                        string name = child.name.ToLower();
//                        if(name.ToLower().StartsWith(ModelConst.WEAPON_EFFECT))
//                            return false;
//
//                        Renderer render = child.GetComponent<Renderer>();
//                        if (render != null)
//                        {
//                            Material newMat = null;
//                            Material oldMat = render.material;
//
//                            if (m_tranWeapon.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
//                            {
//                                newMat = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON_CRYSTAL);
//                                if (ResourceManager.m_loadMode == ELoadMode.AssetBundle)
//                                    newMat = Object.Instantiate(newMat) as Material;
//                            }
//                            else
//                            {
//                                newMat = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON);
//                                if (ResourceManager.m_loadMode == ELoadMode.AssetBundle)
//                                    newMat = Object.Instantiate(newMat) as Material;
//
//                                newMat.CopyPropTexFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWTEX);
//                                newMat.CopyPropColorFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWTEX_COLOR);
//                                newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWDIR_X);
//                                newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWDIR_Y);
//                                newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWSPEED);
//
//                                ConfigItemWeaponLine wl = GlobalConfig.configWeapon.GetLine(_data.ID);
//                                if(wl != null && wl.FlowTexColor.Length == 3)
//                                {
//                                    Color flowColor = new Color(wl.FlowTexColor[0]/255, wl.FlowTexColor[1]/255, wl.FlowTexColor[2]/255);
//                                    if(flowColor != Color.white && newMat.HasProperty(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR))
//                                    {
//                                        newMat.SetColor(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR, flowColor);
//                                    }
//                                }
//                            }
//
//                            newMat.mainTexture = oldMat.mainTexture;
//                            render.material = newMat;
//                        }
//                        return true;
//                    };
//
//                    m_tranWeapon.VisitChild(funVisit, true);
//                    
//                }, true);
//            }
//            else
//            {
//                if (m_goRenderTexture)
//                    m_goRenderTexture.TrySetActive(true);
//            }
//                
//        }
//    }

    public override void OnHide()
    {
        m_params = null;
        _data = null;
        _tween_data = Vector3.zero;
        _tween_list = null;
        WeaponDisplay.Visible = false;
//        if (m_goRenderTexture)
//            m_goRenderTexture.TrySetActive(false);
        if (_item_effect != null)
        {
            _item_effect.Destroy();
            _item_effect = null;
        }
        if (_item_effect_cold != null)
        {
            _item_effect_cold.Destroy();
            _item_effect_cold = null;
        }
    }

    public override void OnBack()
    {
        if (m_params != null && m_params.Length == 3) 
        {
            UIManager.ShowPanel<PanelStrengthenTrain>();
        }
        else
        {
            object[] objs = new object[3];
            UIManager.ShowPanel<PanelStrengthen>(objs);
            
        }
    }

    public override void OnDestroy()
    {
//        if (m_weaponCamera)
//            Object.Destroy(m_weaponCamera.gameObject);
//        if (m_tranWeapon)
//            Object.Destroy(m_tranWeapon.gameObject);
    }

    public override void Update()
    {
        if (!m_userDraging && IsOpen())
            WeaponDisplay.Rotation(-Time.deltaTime*25);
//            m_tranWeapon.RotateAround(new Vector3(m_rotationCenter, 0, 0), Vector3.up, Time.deltaTime * 5);
        if (_tween_data == Vector3.zero)
        {
            return;
        }
        _tween_data.x += _tween_v * Time.deltaTime;
        if (_tween_data.z>0)        
        {
			_exp_pb.Find("pb_value").GetComponent<Image>().fillAmount = _tween_data.x * 1f / _tween_data.z;
            _exp_pb.Find("pb_label").GetComponent<Text>().text = string.Format("{0}/{1}", (int)(_tween_data.x), _tween_data.z);        
        }
        if (_tween_data.x >= _tween_data.y)
        {
            _tween_data = _tween_list.Count>0 ? _tween_list.Dequeue() : Vector3.zero;
        }
        if(_tween_data==Vector3.zero)
        {
            _tween_data = Vector3.zero;
            _strengthen_data = _data != null ? StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID) : null;
            UpdateItemDetailView();
        }
    }

    private void Toc_player_upgrade_weapon_strengthen_state(toc_player_upgrade_weapon_strengthen_state data)
    {
        updateStrengthenData(StrengthenModel.Instance.getSStrengthenDataByCode(data.weapon_id));
    }

    private void Toc_player_weapon_strengthen_info(toc_player_weapon_strengthen_info data)
    {
        updateStrengthenData(StrengthenModel.Instance.getSStrengthenDataByCode(data.info.weapon_id));
    }

    private void Toc_player_weapon_awaken(toc_player_weapon_awaken data)
    {
        if (_data==null || _data.ID != data.weapon_id)
        {
            return;
        }
        _strengthen_data = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);
        UpdateItemDetailView();
    }

    private void Toc_player_buyback_weapon_accessory(toc_player_buyback_weapon_accessory data)
    {
        TipsManager.Instance.showTips(string.Format("卖出部件获得{0}金币", data.coin));
        OnPartFunBtnClick(null, null);
        OnUpdatePartListData();
        //_show_part_item.gameObject.TrySetActive(false);
    }

    private void Toc_player_unequip_weapon_accessory(toc_player_unequip_weapon_accessory data)
    {
        TipsManager.Instance.showTips("拆除成功");
        if (_data == null || _data.ID <= 0)
        {
            return;
        }
        _sdata = ItemDataManager.GetDepoUnlimitItem(_data.ID);
      
        UpdateItemDetailView();
        OnUpdatePartListData();
        //_set_part_item.SetData(null);
    }

    private void Toc_player_equip_weapon_accessory(toc_player_equip_weapon_accessory data)
    {
        TipsManager.Instance.showTips("镶嵌成功");
        if (_data == null || _data.ID <= 0)
        {
            return;
        }
        _sdata = ItemDataManager.GetDepoUnlimitItem(_data.ID);
        UpdateItemDetailView();
        OnUpdatePartListData();
        //_show_part_item.gameObject.TrySetActive(false);
//        var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(_sdata.item_id);
//        if (_sdata != null && accessoryList != null && accessoryList.Length > 0)
//        {
//            foreach (var spart in accessoryList)
//            {
//                if (spart.pos == data.pos)
//                {
//                    var vo = new CDItemData(spart.item_id);
//                    vo.sdata.accessory_values = spart.accessory_values;
//                    vo.extend = new KeyValue<int, int>(data.weapon_id,data.pos);
//                    //_set_part_item.SetData(vo);
//                }
//            }
//        }
//        else
//        {
//            //_set_part_item.SetData(null);
//        }
        if (_set_part_list != null && _set_part_list.Length>0)
        {
            foreach (var render in _set_part_list)
            {
                if (render.data!=null && render.data.extend is KeyValue<int,int>)
                {
                    if ((render.data.extend as KeyValue<int, int>).value == data.pos)
                    {
                        UIEffect.ShowEffect(render.transform, EffectConst.UI_STRENGTHEN_EMBED_PART, .5f);
                        break;
                    }
                }
            }
        }
    }

    void Toc_player_test_accessory_meterial(toc_player_test_accessory_meterial data)
    {
        TipsManager.Instance.showTips("鉴定成功");
        AudioManager.PlayUISound(AudioConst.strengthenAppraise);
        OnPartFunBtnClick(null,null);
        //if (data.accessory_list.Length == 1 && _show_part_item.gameObject.activeSelf)
        //{
        //    _show_part_item.SetData(new CDItemData(data.accessory_list[0]));
        //}
        //else
        //{
        //    _show_part_item.gameObject.TrySetActive(false);
        //}
        var renders = _part_dg.ItemRenders;
        for (var i = 0; i < data.meterial_list.Length;i++ )
        {
            foreach (var render in renders)
            {
                if (render.m_renderData is CDItemData && (render.m_renderData as CDItemData).sdata.uid == data.meterial_list[i])
                {
                    PlayTestEffect(render, data.accessory_list[i]);
                    break;
                }
            }
        }
        TimerManager.SetTimeOut(1f,()=>OnUpdatePartListData(null,data.accessory_list[0].uid));
    }

    void Toc_item_new(toc_item_new data)
    {
        if (_data != null && _data.ID == data.item.item_id)
        {
            _sdata = ItemDataManager.GetDepoUnlimitItem(_data.ID);
        }
    }

    private void PlayTestEffect(ItemRender render, p_item sdata)
    {
        UIEffect.ShowEffect(render.transform, EffectConst.UI_STRENGTHEN_TEST_PART, .5f);
        var info = ItemDataManager.GetItem(sdata.item_id) as ConfigItemAccessoriesLine;
        var add_props = info!=null ? info.GetAddProp() : null;
        if (add_props != null && add_props.Length > 0)
        {
            var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
            var result = new List<string>();
            for (int i = 0; add_props != null && i < add_props.Length; i++)
            {
                if (sdata.accessory_values != null && i < sdata.accessory_values.Length)
                {
                    if (add_props[i] != null)
                    {
                        var add_value = Mathf.Abs(sdata.accessory_values[i]);
                        result.Add(string.Format("{0}+{1}", prop_config.GetDispPropName(info.DispProps[i]), add_props[i].AddType == 1 ? (add_value / 10f).ToString("0.##") + "%" : add_value.ToString()));
                    }
                }
            }
            if (result.Count > 0)
            {
                FloatTextUIEffect.Instance.Play(result.ToArray(), render.transform,18,0.5f,0.25f);
            }
        }
    }
}
