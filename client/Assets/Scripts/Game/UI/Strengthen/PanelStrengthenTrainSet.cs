﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

class PanelStrengthenTrainSet : BasePanel
{
    List<ConfigWeaponStrengthenTrainLine> _value_cost;
    List<ConfigWeaponStrengthenTrainLine> _time_cost;
    p_item _selected_data;
    int _current_time;
    int _current_vlaue;

    DataGrid _page_data;
    Image _item_img;
    Text _name_txt;
    Text _desc_txt;
    Image _pb_value;
    Text _pb_lbl;
    Text _time_txt;
    Text _value_txt;
    Text _gold_txt;
    Text _honor_txt;
    Text _diamond_txt;
     int m_index; //选中训练槽，从1开始
    public PanelStrengthenTrainSet()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthenTrainSet");

        AddPreLoadRes("UI/Strengthen/PanelStrengthenTrainSet", EResType.UI);
        AddPreLoadRes("UI/Strengthen/TrainItemSelectRender", EResType.UI);
    }

    public override void Init()
    {
        _page_data = m_tran.Find("item_list/content").gameObject.AddComponent<DataGrid>();
        _page_data.SetItemRender(ResourceManager.LoadUI("UI/Strengthen/TrainItemSelectRender"), typeof(TrainItemSelectRender));
        _page_data.autoSelectFirst = true;
        _page_data.useLoopItems = true;
        _page_data.onItemSelected = onTrainItemSelected;

        _item_img = m_tran.Find("item_img").GetComponent<Image>();
        _name_txt = m_tran.Find("name_txt").GetComponent<Text>();
        _desc_txt = m_tran.Find("desc_txt").GetComponent<Text>();
        _pb_value = m_tran.Find("exp_pb/pb_value").GetComponent<Image>();
        _pb_lbl = m_tran.Find("exp_pb/pb_label").GetComponent<Text>();
        _time_txt = m_tran.Find("train_cd_txt").GetComponent<Text>();
        _value_txt = m_tran.Find("value_txt").GetComponent<Text>();
        _gold_txt = m_tran.Find("gold_txt").GetComponent<Text>();
        _honor_txt = m_tran.Find("honor_txt").GetComponent<Text>();
        _diamond_txt = m_tran.Find("diamond_txt").GetComponent<Text>();
    }

    private void onTrainItemSelected(object renderData)
    {
        _selected_data = renderData is p_item ? renderData as p_item : _selected_data;
        if (_selected_data != null)
        {
            _current_time = 0;
            _current_vlaue = 0;
            var info = ItemDataManager.GetItem(_selected_data.item_id) as ConfigItemWeaponLine;
            _item_img.gameObject.TrySetActive(true);
            _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
            _item_img.SetNativeSize();
            _name_txt.text = info.DispName;
            updateCostByTrainData();
        }
        else
        {
            _current_time = 0;
            _current_vlaue = 0;
            _item_img.gameObject.TrySetActive(false);
            _name_txt.text = "没有可训练的武器";
            _desc_txt.text = "熟练度等级  0/0";
            _time_txt.text = "00:00:00";
            _value_txt.text = "0%";
            _gold_txt.text = "0";
            _honor_txt.text = "0";
            _diamond_txt.text = "0";
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("tab_bar/type_btn_0")).onPointerClick += onTabBarChanged;
        UGUIClickHandler.Get(m_tran.Find("tab_bar/type_btn_1")).onPointerClick += onTabBarChanged;
        UGUIClickHandler.Get(m_tran.Find("subcd_btn")).onPointerClick += clickCDStepBtn;
        UGUIClickHandler.Get(m_tran.Find("addcd_btn")).onPointerClick += clickCDStepBtn;
        UGUIClickHandler.Get(m_tran.Find("subvalue_btn")).onPointerClick += clickValueStepBtn;
        UGUIClickHandler.Get(m_tran.Find("addvalue_btn")).onPointerClick += clickValueStepBtn;
        UGUIClickHandler.Get(m_tran.Find("starttrain_btn")).onPointerClick += startTrainBtnClick;        
    }

    private void startTrainBtnClick(GameObject target, PointerEventData eventData)
    {
        if (_selected_data == null || !HasParams())
        {
            if (_selected_data == null)
                TipsManager.Instance.showTips("请选择要训练的枪械");
            return;
        }
        var cost1 = _time_cost[_current_time];
        var cost2 = _value_cost[_current_vlaue];
        if (PlayerSystem.CheckMoney((cost1.Diamond + cost2.Diamond),EnumMoneyType.DIAMOND,null)==false)
        {
            return;
        }
        else if (PlayerSystem.CheckMoney((cost1.Coin + cost2.Coin),EnumMoneyType.COIN,null)==false)
        {
            return;
        }
        else if (PlayerSystem.CheckMoney((cost1.Medal + cost2.Medal),EnumMoneyType.MEDAL,null)==false)
        {
            return;
        }
        int pos = (int)(m_params[0]);
        StrengthenModel.Instance.Tos_player_start_weapon_train(pos, _selected_data.uid, cost1.ID, cost2.ID);
    }

    private void clickValueStepBtn(GameObject target, PointerEventData eventData)
    {
        if (_selected_data == null)
        {
            return;
        }
        var new_value = _current_vlaue + (target.name == "addvalue_btn" ? 1 : -1);
        if (new_value >= _value_cost.Count)
        {
            new_value = _value_cost.Count - 1;
        }
        if (new_value < 0)
        {
            new_value = 0;
        }
        var info = _value_cost[new_value];
        if (info != null &&Efficiency(info))
        {
            _current_vlaue = new_value;
            updateCostByTrainData();
        }
        //else
        //{
        //    TipsManager.Instance.showTips(string.Format("需要{0}", info != null && info.ReqVip==1 ? "会员":"尊贵会员"));
        //}
    }

    private void clickCDStepBtn(GameObject target, PointerEventData eventData)
    {        
        if (_selected_data== null)
        {
            return;
        }
        var new_value = _current_time + (target.name == "addcd_btn" ? 1 : -1);
        if (new_value >= _time_cost.Count)
        {
            new_value = _time_cost.Count - 1;
        }
        if (new_value < 0)
        {
            new_value = 0;
        }
        var info = _time_cost[new_value];
        if(info!=null && TimeCheck(info))
        {
            _current_time = new_value;
            updateCostByTrainData();            
        }
        //else
        //{
        //    TipsManager.Instance.showTips(string.Format("需要{0}", info != null && info.ReqVip==1 ? "会员":"尊贵会员"));
        //}
    }
        
    private bool TimeCheck(ConfigWeaponStrengthenTrainLine cost)
    {
        int trainTime = cost.Time;
        bool result = false;        
        if (PlayerSystem.roleData.vip_level >= cost.ReqVip) //满足会员
        {
            result = true;
        }
        else if (cost.NeedItem > 0) //满足道具
        {
            p_item item0 = ItemDataManager.GetDepotItem(cost.NeedItem);
            if (item0 == null || item0.time_limit < TimeUtil.GetNowTimeStamp()) //无道具或者道具过期
            {
                TipsManager.Instance.showTips(string.Format("购买{0}可扩充训练时间", ItemDataManager.GetItem(cost.NeedItem).DispName));
                result = false;
            }
            else
            {
                int time = (int)item0.time_limit - TimeUtil.GetNowTimeStamp();
                result = time > trainTime;
                if (!result)
                    TipsManager.Instance.showTips(string.Format("训练槽扩展卡不足{0}小时，请购买{1}", trainTime / 3600, ItemDataManager.GetItem(TrainingItemRender.ExpandCard0).DispName));
            }
        }
        else
            result = false;    
        return result;
    }

    private bool Efficiency(ConfigWeaponStrengthenTrainLine cost)
    {  
        bool result = false;
        
        if (PlayerSystem.roleData.vip_level >= cost.ReqVip) //要求会员等级
        {
            result = true;
        }
        else if(cost.NeedItem>0) //要求满足道具
        {
            p_item item1 = ItemDataManager.GetDepotItem(cost.NeedItem);
            if (item1 == null || item1.time_limit < TimeUtil.GetNowTimeStamp())
                TipsManager.Instance.showTips(string.Format("购买{0}可扩充训练效率", ItemDataManager.GetItem(TrainingItemRender.ExpandCard1).DispName));
            else
                result = true;
        }
        return result;
    }


    private void updateCostByTrainData()
    {
        var cost1 = _time_cost[_current_time];
        var cost2 = _value_cost[_current_vlaue];
        _time_txt.text = TimeUtil.FormatTime(cost1.Time);
        _value_txt.text = (int)(cost2.Ratio * 100) + "%";
        _gold_txt.text = (cost1.Coin + cost2.Coin).ToString();
        _honor_txt.text = (cost1.Medal + cost2.Medal).ToString();
        _diamond_txt.text = (cost1.Diamond + cost2.Diamond).ToString();
        if (_selected_data != null)
        {
            var info = ItemDataManager.GetItem(_selected_data.item_id) as ConfigItemWeaponLine;
            var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(_selected_data.item_id);
            var level = sdata!=null ? sdata.level : 1;
            var state = sdata!=null ? sdata.state : 0;
            var total_exp = Mathf.Ceil((sdata != null ? sdata.cur_exp : 0) + cost1.Exp * cost2.Ratio);//原经验+经验加成;
            var level_ary = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(state, info.StrengthenGroup);
            TDStrengthenLevel next_info = null;
            if (level_ary != null)
            {
                foreach(var t_data in level_ary)
                {
                    if (t_data.Level > level)
                    {
                        next_info = t_data;
                        if (total_exp >= t_data.NeedExp)
                        {
                            level = t_data.Level;
                            total_exp -= t_data.NeedExp;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            _desc_txt.text = "熟练度等级 " + string.Format("<color={1}>{0}</color>",level,((sdata==null && level>1) || (sdata!=null && level>sdata.level)) ? "#00ff00":"#ffffff") + "/" + (level_ary.Length > 0 ? level_ary[level_ary.Length - 1].Level : 0);
            if(next_info==null || level>=next_info.Level)
            {
                _pb_lbl.text = "满级";
                _pb_value.rectTransform.sizeDelta = new Vector2(418f, 20.3f);
            }
            else
            {
                _pb_lbl.text = total_exp + "/" + next_info.NeedExp;
                _pb_value.rectTransform.sizeDelta = new Vector2(418f * (float)total_exp / next_info.NeedExp, 20.3f); 
            }
        }
    }

    private void onTabBarChanged(GameObject target, PointerEventData eventData)
    {
        int index = 0;
        if (target!=null)
        {
            int.TryParse(target.name[target.name.Length-1].ToString(),out index);
        }
        string[] temp = new string[] { "WEAPON1", "WEAPON2"};
        var source_data = ItemDataManager.GetDepotItemsByType("EQUIP", index<temp.Length ? temp[index] : temp[0]);
        var page_data = new List<p_item>();
        var pageItemId = new List<int>();
        if (source_data != null && source_data.Length > 0)
        {
            for (int i = 0; i < source_data.Length; i++)
            {
                var data = source_data[i];

                if ((data.time_limit == 0 || data.time_limit > TimeUtil.GetNowTimeStamp()) && !pageItemId.Contains(data.item_id))
                {
                    page_data.Add(data);
                    pageItemId.Add(data.item_id);
                }
            }
        }
        _page_data.ResetScrollPosition();
        _page_data.Data = page_data.ToArray();
        if (_page_data.Data.Length > 0)
            _page_data.Select(page_data[0]);
    }

    public override void OnShow()
    {
        if(HasParams())
        {
            m_index = (int)m_params[0];
        }
        var cost_data = ConfigManager.GetConfig<ConfigWeaponStrengthenTrain>().m_dataArr;
        _time_cost = new List<ConfigWeaponStrengthenTrainLine>();
        _value_cost = new List<ConfigWeaponStrengthenTrainLine>();
        for (int i = 0; i < cost_data.Length; i++)
        {
            var tdata = cost_data[i];
            if (tdata.Time > 0)
            {
                _time_cost.Add(tdata);
            }
            else
            {
                _value_cost.Add(tdata);
            }
        }
        _current_time = 0;
        _current_vlaue = 0;
        UGUIClickHandler.Get(m_tran.Find("tab_bar/type_btn_0")).GetComponent<Toggle>().isOn = true;        
        onTabBarChanged(null, null);
    }

    public override void OnHide()
    {
        _selected_data = null;
        _time_cost = null;
        _value_cost = null;
        m_params = null;
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelStrengthenTrain>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        
    }

    void Toc_player_start_weapon_train(toc_player_start_weapon_train data)
    {
        StrengthenModel.Instance.Tos_player_weapon_train_info();
        UIManager.ShowPanel<PanelStrengthenTrain>();
    }
}

