﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelMaterialConver : BasePanel
{
    private ItemDropInfo[] m_weaponClassifyArr;
    private DataGrid m_leftDataGrid;
    private DataGrid m_rightDataGrid;
    private Text m_txtLeftFilter;
    private Text m_txtRightFilter;
    private Text m_txtLeftTitle;

    private Image m_imgIcon1;
    private Text m_txtName1;
    private Text m_txtCount1;
    private Image m_imgIcon2;
    private Text m_txtName2;
    private Transform m_tranCircleInner;
    private Transform m_tranCircleOuter;

    private ItemDropInfo m_leftFilter;
    private ItemDropInfo m_rightFilter;

    public PanelMaterialConver()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Strengthen/PanelMaterialConver");
        AddPreLoadRes("UI/Strengthen/PanelMaterialConver", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_weaponClassifyArr = new[]
        {
            new ItemDropInfo { subtype = "-1", label = "全部" }, 
            new ItemDropInfo { subtype = "1", label = "普通级" }, 
            new ItemDropInfo { subtype = "2", label = "优秀级" },
            new ItemDropInfo { subtype = "3", label = "稀有级" }, 
            new ItemDropInfo { subtype = "4", label = "传说级" }, 
            new ItemDropInfo { subtype = "5", label = "圣兽级" }, 
        };

        m_imgIcon1 = m_tran.Find("center/item1/icon").GetComponent<Image>();
        m_txtName1 = m_tran.Find("center/item1/name").GetComponent<Text>();
        m_txtCount1 = m_tran.Find("center/item1/count").GetComponent<Text>();
        m_imgIcon2 = m_tran.Find("center/item2/icon").GetComponent<Image>();
        m_txtName2 = m_tran.Find("center/item2/name").GetComponent<Text>();
        m_tranCircleInner = m_tran.Find("center/circleInner");
        m_tranCircleOuter = m_tran.Find("center/circleOuter");

        m_txtLeftFilter = m_tran.Find("bound/left/btnFilter/Text").GetComponent<Text>();
        m_txtRightFilter = m_tran.Find("bound/right/btnFilter/Text").GetComponent<Text>();
        m_txtLeftTitle = m_tran.Find("bound/left/txtTitle").GetComponent<Text>();

        m_leftDataGrid = m_tran.Find("bound/left/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_leftDataGrid.SetItemRender(m_tran.Find("itemRender").gameObject, typeof(ItemMaterialConverRender));
        m_leftDataGrid.SetItemClickSound(AudioConst.activityBtnClick);
        m_leftDataGrid.onItemSelected += OnDepotItemSelected;
        m_leftDataGrid.autoSelectFirst = false;
        m_leftDataGrid.useLoopItems = true;

        m_rightDataGrid = m_tran.Find("bound/right/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_rightDataGrid.SetItemRender(m_tran.Find("itemRender").gameObject, typeof(ItemMaterialConverRender));
        m_rightDataGrid.SetItemClickSound(AudioConst.activityBtnClick);
        m_rightDataGrid.onItemSelected += OnTargetItemSelected;
        m_rightDataGrid.autoSelectFirst = false;
        m_rightDataGrid.useLoopItems = true;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("center/item1")).onPointerClick += delegate { m_leftDataGrid.Select(null); };
        UGUIClickHandler.Get(m_tran.Find("center/item2")).onPointerClick += delegate { m_rightDataGrid.Select(null); };
        UGUIClickHandler.Get(m_tran.Find("center/btnConver")).onPointerClick += OnBtnConverClick;
        UGUIClickHandler.Get(m_tran.Find("center/btnRule")).onPointerClick += delegate { UIManager.ShowTipMulTextPanel(ConfigMisc.GetValue("materialConverRuleDesc").Replace("<br>","\n")); };
        UGUIClickHandler.Get(m_tran.Find("bound/left/btnFilter")).onPointerClick +=
            (target, data) => UIManager.ShowDropListUp(m_weaponClassifyArr, OnDepotDropListItemSelected, m_tran.Find("bound/left"), target, 140);
        UGUIClickHandler.Get(m_tran.Find("bound/right/btnFilter")).onPointerClick +=
            (target, data) => UIManager.ShowDropListUp(m_weaponClassifyArr, OnTargetDropListItemSelected, m_tran.Find("bound/right"), target, 140);
    }

    public override void OnShow()
    {
        UIManager.HideDropList();

        m_leftFilter = m_weaponClassifyArr[0];
        m_rightFilter = m_weaponClassifyArr[0];

        m_txtLeftFilter.text = m_leftFilter.label;
        m_leftDataGrid.ResetScrollPosition();
        m_leftDataGrid.Data = new object[0];
        OnTargetItemSelected(null);

        UpdateRightDataGridView();
        m_rightDataGrid.ResetScrollPosition();
        m_rightDataGrid.Select(null);
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (!IsOpen())
            return;

        m_tranCircleInner.RotateAround(m_tranCircleInner.position, Vector3.forward, Time.smoothDeltaTime * 30);
        m_tranCircleOuter.RotateAround(m_tranCircleOuter.position, Vector3.back, Time.smoothDeltaTime * 50);
    }

    private void UpdateRightDataGridView()
    {
        var itemArr = ItemDataManager.GetMaterialsByRareType(int.Parse(m_rightFilter.subtype));
        Array.Sort(itemArr, OnSortTargetItems);
        m_rightDataGrid.Data = itemArr;

        m_txtRightFilter.text = m_rightFilter.label;
    }

    private void UpdateLeftDataGridView()
    {
        var rightItem = m_rightDataGrid.SelectedData<ConfigItemOtherLine>();
        var itemArr = ItemDataManager.GetMaterialsByConvertType(rightItem.ConvertType);

        var list = new List<ItemPItemConverInfo>();
        for (int i = 0; i < itemArr.Length; i++)
        {
            var item = itemArr[i];
            if (item != null && item.ID != rightItem.ID && (m_leftFilter.subtype == "-1" || m_leftFilter.subtype == item.RareType.ToString()))
            {
                int count = 0;
                var pitem = ItemDataManager.GetDepotItem(item.ID);
                if (pitem != null)
                    count = pitem.item_cnt;
                list.Add(new ItemPItemConverInfo() { data = itemArr[i], forbidden = false, count = count });
            }
        }

//        list.Sort(OnSortDepotItems);
        m_leftDataGrid.Data = list.ToArray();

        m_txtLeftFilter.text = m_leftFilter.label;
    }

    private void UpdateLeftDataGridWhenItemUpdate()
    {
        if (!IsOpen())
            return;
        if (m_rightDataGrid.SelectedData<ConfigItemOtherLine>() == null)
            return;

        var lastSelect = m_leftDataGrid.SelectedData<ItemPItemConverInfo>();
        UpdateLeftDataGridView();

        int newCount = 0;
        if (lastSelect != null && ItemDataManager.GetDepotItem(lastSelect.data.ID) != null)
            newCount = ItemDataManager.GetDepotItem(lastSelect.data.ID).item_cnt;

        if (newCount < 2)
            m_leftDataGrid.Select(null);
        else if (lastSelect != null)
        {
            var arr = m_leftDataGrid.Data;
            for (int i = 0; i < arr.Length; i++)
            {
                var pitem = arr[i] as ItemPItemConverInfo;
                if (pitem != null && pitem.data.ID == lastSelect.data.ID)
                {
                    m_leftDataGrid.Select(pitem);
                    break;
                }
            }
        }
    }

    private void UpdateRightDataGridWhenItemUpdate()
    {
        if (!IsOpen())
            return;

        var lastSelect = m_rightDataGrid.SelectedData<ConfigItemOtherLine>();
        UpdateRightDataGridView();

        if (lastSelect != null)
        {
            var arr = m_leftDataGrid.Data;
            for (int i = 0; i < arr.Length; i++)
            {
                var item = arr[i] as ConfigItemOtherLine;
                if (item != null && item.ID == lastSelect.ID)
                {
                    m_leftDataGrid.Select(item);
                    break;
                }
            }
        }
    }

    private void OnBtnConverClick(GameObject target, PointerEventData eventData)
    {
        if (m_rightDataGrid.SelectedData<ConfigItemOtherLine>() == null)
            TipsManager.Instance.showTips("请先在右侧选择【要兑换的】碎片");
        else if (m_leftDataGrid.SelectedData<ItemPItemConverInfo>() == null)
            TipsManager.Instance.showTips("请在左侧选择【已拥有的】碎片");
        else if (m_leftDataGrid.SelectedData<ItemPItemConverInfo>().count < 2)
            TipsManager.Instance.showTips("需要2个碎片才能进行兑换");
        else
        {
            var srcId = m_leftDataGrid.SelectedData<ItemPItemConverInfo>().data.ID;
            var desId = m_rightDataGrid.SelectedData<ConfigItemOtherLine>().ID;
            if (srcId == desId)
            {
                TipsManager.Instance.showTips("兑换碎片和目标碎片不可相同");
                return;
            }
            NetLayer.Send(new tos_player_compound()
            {
                item_src_id = srcId,
                item_des_id = desId
            });
        }
    }

    private void OnDepotDropListItemSelected(ItemDropInfo dropInfo)
    {
        if (m_rightDataGrid.SelectedData<ConfigItemOtherLine>() == null)
            return;

        m_leftFilter = dropInfo;
        UpdateLeftDataGridView();
        m_leftDataGrid.Select(null);
    }

    private void OnTargetDropListItemSelected(ItemDropInfo dropInfo)
    {
        m_rightFilter = dropInfo;
        UpdateRightDataGridView();
        m_rightDataGrid.Select(null);
    }

    private void OnDepotItemSelected(object renderData)
    {
        var pitem = renderData as ItemPItemConverInfo;
        var isShow = pitem != null && !pitem.forbidden;
        m_imgIcon1.enabled = isShow;
        m_imgIcon1.SetSprite(isShow ? ResourceManager.LoadIcon(pitem.data.Icon) : null,pitem.data.SubType);
        m_imgIcon1.SetNativeSize();
        m_txtCount1.text = isShow ? "x2" : "";

        var item = isShow ? pitem.data : null;
        m_txtName1.text = item != null ? item.DispShortName : "";
        m_txtName1.color = item != null ? ColorUtil.GetRareColor(item.RareType) : Color.white;

        if (pitem != null && pitem.forbidden)
        {
            TipsManager.Instance.showTips("只能兑换相同颜色的碎片");
            m_leftDataGrid.Select(null);
        }
        else if (pitem != null && pitem.count < 2)
        {
            TipsManager.Instance.showTips("需要2个碎片才能进行兑换");
            m_leftDataGrid.Select(null);
        }
    }

    private void OnTargetItemSelected(object renderData)
    {
        var item = renderData as ConfigItemOtherLine;
        m_imgIcon2.enabled = item != null;
        m_imgIcon2.SetSprite(item != null ? ResourceManager.LoadIcon(item.Icon) : null, item != null ?item.SubType : "");
        m_imgIcon2.SetNativeSize();

        m_txtName2.text = item != null ? item.DispShortName : "";
        m_txtName2.color = item != null ? ColorUtil.GetRareColor(item.RareType) : Color.white;

        if (item != null)
        {
            UpdateLeftDataGridView();
            m_leftDataGrid.Select(null);
        }
        else
            m_leftDataGrid.Data = new object[0];

        m_txtLeftTitle.enabled = item != null;
    }

    private int OnSortDepotItems(ItemPItemConverInfo a, ItemPItemConverInfo b)
    {
        int result = 0;
        if (a.forbidden && !b.forbidden)
            result = 1;
        else if (!a.forbidden && b.forbidden)
            result = -1;
        else
            result = 0;
        if (result == 0)
            result = b.count - a.count;
        if (result == 0)
            return b.data.RareType - b.data.RareType;
        return result;
    }

    private int OnSortTargetItems(ConfigItemOtherLine a, ConfigItemOtherLine b)
    {
        if (a.RareType == b.RareType)
            return a.ID - b.ID;
        else
            return b.RareType - a.RareType;
    }

    private void Toc_player_compound(toc_player_compound data)
    {
        UIEffect.ShowEffect(m_tran, EffectConst.UI_CONVERT, 2, true);
        AudioManager.PlayUISound(AudioConst.convert);
        TimerManager.SetTimeOut(1, () => TipsManager.Instance.showTips("【兑换成功】"));
    }

    private void Toc_item_new(toc_item_new data)
    {
        TimerManager.SetTimeOut(1, UpdateAllDataGridWhenItemUpdate);
    }

    private void Toc_item_update(toc_item_update data)
    {
        TimerManager.SetTimeOut(1, UpdateAllDataGridWhenItemUpdate);
    }

    private void Toc_item_del(toc_item_del data)
    {
        TimerManager.SetTimeOut(1, UpdateAllDataGridWhenItemUpdate);
    }

    private void UpdateAllDataGridWhenItemUpdate()
    {
        UpdateRightDataGridWhenItemUpdate();
        UpdateLeftDataGridWhenItemUpdate();
    }
}