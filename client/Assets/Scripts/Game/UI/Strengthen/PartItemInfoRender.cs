﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PartItemInfoRender : ItemRender
{
    CDItemData _data;

    Image _none_img;
    //Image _frame_img;
    Image _item_img;
    Text _name_txt;
    Text _prop_txt;
    Text _suit_txt;
    //Button _fun_btn;

    public Action<PartItemInfoRender> OnCloseCallBack;
    public Action<CDItemData> OnFunCallBack;

    public override void Awake()
    {
        if (transform.Find("none_img")!=null)
        {
            _none_img = transform.Find("none_img").GetComponent<Image>();
        }
        //_frame_img = transform.Find("frame_img").GetComponent<Image>();
        //_frame_img = transform.Find("frame_img").GetComponent<Image>();
        _item_img = transform.Find("item_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        _prop_txt = transform.Find("prop_txt").GetComponent<Text>();
        _suit_txt = transform.Find("suit_txt").GetComponent<Text>();
        //_fun_btn = transform.Find("fun_btn").GetComponent<Button>();

        //UGUIClickHandler.Get(_fun_btn.gameObject).onPointerClick += OnFunBtnClick;
        var close_btn_trans = transform.Find("close_btn");
        if (close_btn_trans != null)
        {
            UGUIClickHandler.Get(close_btn_trans).onPointerClick += OnCloseBtnClick;
        }
    }

    private void OnCloseBtnClick(GameObject target, PointerEventData eventData)
    {
        if (OnCloseCallBack != null)
        {
            OnCloseCallBack.Invoke(this);
        }
    }

    private void OnFunBtnClick(GameObject target, PointerEventData eventData)
    {
        if (OnFunCallBack != null)
        {
            OnFunCallBack.Invoke(_data);
        }
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDItemData;
        if (_none_img != null)
        {
            _none_img.enabled = _data == null || _data.info == null;
        }
        if (_data != null && _data.info!=null)
        {
            //_fun_btn.gameObject.TrySetActive(true);
            //if (_data.extend is KeyValue<int,int> && (_data.extend as KeyValue<int,int>).value>0)
            //{
            //    _fun_btn.transform.Find("Text").GetComponent<Text>().text = "拆除";
            //    _fun_btn.transform.Find("Image").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "remove_ico"));
            //}
            //else if(_data.info!=null)
            //{
            //    _fun_btn.transform.Find("Text").GetComponent<Text>().text = _data.info.Type==GameConst.ITEM_TYPE_EQUIP ? "镶嵌" : "鉴定";
            //    _fun_btn.transform.Find("Image").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, _data.info.Type == GameConst.ITEM_TYPE_EQUIP ? "embed_ico" : "test_ico"));
            //}
            var info = _data.info as ConfigItemAccessoriesLine;
            var quality = _data.GetQuality();
            //_frame_img.enabled = quality>0;
            //_frame_img.SetSprite(quality>0 ? ResourceManager.LoadSprite(AtlasName.Strengthen, "item_frame_" + quality) : null);
            //_frame_img.SetNativeSize();
            _item_img.enabled = true;
            _item_img.SetSprite(ResourceManager.LoadIcon(_data.info.Icon),_data.info.SubType);
            _item_img.SetNativeSize();
            _name_txt.text = _data.info.DispName;
            var str = "";
            var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
            //属性
            str = "";
            var add_props = info.GetAddProp();
            for (int i = 0; add_props!=null && i < add_props.Length; i++)
            {
                if(_data.sdata.accessory_values!=null && i<_data.sdata.accessory_values.Length)
                {
                    if (add_props[i] != null)
                    {
                        var temp_value = Mathf.Abs(_data.sdata.accessory_values[i]);
                        str += string.Format("{0}：+{1}", prop_config.GetDispPropName(info.DispProps[i]),add_props[i].AddType == 1? (temp_value/10f+"%"):temp_value.ToString());
                    }
                    else
                    {
                        str += info.AddProps[i] +"：unknow";
                    }
                }
                else
                {
                    str += "???：???";
                }
                str += i % 2 == 1 ? "\n" : " ";
            }
            _prop_txt.text = str;
            //套件组合、属性
            str = "";
            var suit_config = ConfigManager.GetConfig<ConfigAccessorySuit>();
            var set_part_count = 0;
            var item_id = _data.extend is KeyValue<int, int> ? (_data.extend as KeyValue<int, int>).key : 0;
            var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(item_id);
            if (accessoryList != null && accessoryList.Length > 0)
            {
                foreach (var spart in accessoryList)
                {
                    var tpart = ItemDataManager.GetItem(spart.item_id);
                    if (tpart is ConfigItemAccessoriesLine)
                    {
                        set_part_count += (tpart as ConfigItemAccessoriesLine).Suit == info.Suit ? 1 : 0;
                    }
                }
            }
            var suit_group = info.Suit>0 ? suit_config.GetSuitGroupAddPropData(info.Suit) : null;
            if (suit_group!=null && suit_group.Length>0)
            {
                str += (!string.IsNullOrEmpty(suit_group[0].Desc) ? suit_group[0].Desc : "未命名套装") + "\n";
                var prop_list = new List<string>();
                var prep_map = new Dictionary<string,int>();
                foreach(var suit_add in suit_group)
                {
                    prop_list.Clear();
                    for (int i = 0; suit_add.AddProps != null && i < suit_add.AddProps.Length; i++)
                    {
                        var prop_strs = suit_add.AddProps[i].Split('#');
                        var add_value = 0;
                        if (prep_map.TryGetValue(prop_strs[3], out add_value))
                        {
                            add_value = int.Parse(prop_strs[2]) - add_value;
                        }
                        else
                        {
                            add_value = int.Parse(prop_strs[2]);
                        }
                        prop_list.Add(string.Format("{0}+{1}", prop_config.GetDispPropName(prop_strs[3]), prop_strs[1] == "1" ? (add_value/10f+"%"):add_value.ToString()));
                        prep_map[prop_strs[3]] = int.Parse(prop_strs[2]);
                    }
                    str += string.Format("<color='{2}'>{0}件：{1}</color>\n", suit_add.Parts, prop_list.ToStr<string>(" "), set_part_count >= suit_add.Parts ? "#FDD79B" : "#999999");
                }
            }
            _suit_txt.text = str;
        }
        else
        {
            //_frame_img.enabled = false;
            _item_img.enabled = true;
            _item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common,"unknow"));
            _item_img.SetNativeSize();
            _name_txt.text = "";
            _prop_txt.text = "";
            _suit_txt.text = "";
            //_fun_btn.gameObject.TrySetActive(false);
        }
    }
}
