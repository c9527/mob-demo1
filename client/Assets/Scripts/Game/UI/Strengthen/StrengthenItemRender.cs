﻿using System;
using UnityEngine;
using UnityEngine.UI;

class StrengthenItemRender : ItemRender
{
    ConfigItemWeaponLine _data;

    Image m_item_img;
    GameObject m_starGroup;
    GameObject m_sunGroup;
    GameObject[] m_stars;
    GameObject[] m_suns;
    Image m_lvlup;
    Text m_name;
    Text m_info;
    GameObject m_canUp;
    Transform m_content;
    Image m_bg;
    string[] colors;
    GameObject bubble;

    public override void Awake()
    {
        m_content = transform.Find("content_layer");
        m_item_img = transform.Find("content_layer/item_img").GetComponent<Image>();
        m_starGroup = transform.Find("content_layer/starGroup/stars").gameObject;
        m_sunGroup = transform.Find("content_layer/starGroup/suns").gameObject;
        bubble = transform.Find("content_layer/bubble").gameObject;
        colors = new string[5] { "white", "green", "blue", "purple", "gold" };
        m_stars = new GameObject[3];
        for (int i = 0; i < 3; i++)
        {
            m_stars[i] = transform.Find("content_layer/starGroup/stars/star"+(i+1).ToString()+"bg/Image").gameObject;
        }
        
        m_suns = new GameObject[5];
        for (int i = 0; i < 5; i++)
        {
            m_suns[i] = transform.Find("content_layer/starGroup/suns/sun"+(i+1).ToString()).gameObject;
        }

        m_canUp = transform.Find("content_layer/canUp").gameObject;
        m_lvlup = transform.Find("content_layer/lvlbg/bar").GetComponent<Image>();

        m_name = transform.Find("content_layer/name_txt").GetComponent<Text>();

        m_info = transform.Find("content_layer/info_txt").GetComponent<Text>();
        m_bg = transform.Find("content_layer").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        bubble.SetActive(false);
        m_lvlup.gameObject.TrySetActive(true);
        m_info.gameObject.TrySetActive(true);
        m_canUp.gameObject.TrySetActive(true);
        _data = data as ConfigItemWeaponLine;
        m_content.gameObject.TrySetActive(_data != null);
        //_content_layer.gameObject.TrySetActive(_data!=null);        
        if(_data!=null)
        {
            //m_starGroup.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
            //m_imgStuff.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
            //m_txtCount.gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
            //if (!string.IsNullOrEmpty(_data.LevelTag))
            //{
            //    m_levelTagObj.TrySetActive(true);
            //    m_levelTag.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, _data.LevelTag));
            //    m_levelTag.SetNativeSize();
            //}
            //else
            //{
            //    m_levelTagObj.TrySetActive(false);
            //}
            m_name.text = _data.DispName;
            gameObject.name = "ItemRender_"+_data.ID;
            var sdata = ItemDataManager.GetDepoUnlimitItem(_data.ID);
            bool has_own = sdata != null && (sdata.time_limit==0 || TimeUtil.GetNowTimeStamp() < sdata.time_limit);
            var strengthenInfo = StrengthenModel.Instance.getSStrengthenDataByCode(_data.ID);

            //熟练度
            var state = strengthenInfo != null ? strengthenInfo.state : 0;
            var level = strengthenInfo != null ? strengthenInfo.level : 1;
            var currentLevels = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStateLevelInfo(state, _data.StrengthenGroup);
            var maxLevel = currentLevels.Length > 0 ? currentLevels[currentLevels.Length - 1].Level : 0;
            m_info.text = String.Format("等级{0}",  + level + "/" + maxLevel);
            m_lvlup.rectTransform.sizeDelta = new Vector2(103.5f, m_lvlup.rectTransform.sizeDelta.y);
            m_lvlup.rectTransform.sizeDelta = new Vector2(m_lvlup.rectTransform.sizeDelta.x * (float)level / (float)maxLevel, m_lvlup.rectTransform.sizeDelta.y);

            //材料
            var nextState = strengthenInfo != null ? strengthenInfo.state + 1 : 1;
            var upgradeInfo = ConfigManager.GetConfig<ConfigWeaponStrengthenState>().getUpgradeInfo(_data.ID, nextState);

            //升阶提示
            var can_upgrade = StrengthenModel.Instance.isAccordUpgrade(strengthenInfo) && upgradeInfo!=null&&PanelStrengthenMain.Lev>=PanelStrengthenMain.OpenLv;
            if (can_upgrade==true)
            {
                bubble.TrySetActive(true); 
                foreach (ItemInfo item_info in upgradeInfo.Consume)
                {
                    var sitem = ItemDataManager.GetDepotItem(item_info.ID);
                    if (sitem == null || sitem.item_cnt < item_info.cnt)
                    {
                        can_upgrade = false;
                        break;
                    }
                }
            }
            m_canUp.TrySetActive(can_upgrade);
            if (can_upgrade&&((level==maxLevel)&&maxLevel<9))
            {
                m_info.text = "可进阶";
                m_lvlup.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "greenbg"));
                bubble.TrySetActive(true); 
            }
            else
            {
                m_lvlup.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "bluebg"));
                bubble.TrySetActive(false); 
            }

            //if (level == maxLevel && !can_upgrade && maxLevel == 9)
            //{
            //    bubble.TrySetActive(false);
            //    var info = ConfigManager.GetConfig<ConfigWeaponAwaken>().GetWakeupLevelData(_data.ID, 1);
            var canawake = ItemDataManager.CanWeaponAwaken(_data.ID);
            if (canawake)
            {
                m_info.text = "可觉醒";
                m_lvlup.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "greenbg"));
                bubble.TrySetActive(true);
            }
            else if (((level == maxLevel) && maxLevel == 9))
            {
                m_lvlup.gameObject.TrySetActive(false);
                m_info.gameObject.TrySetActive(false);
                m_canUp.gameObject.TrySetActive(false);
                bubble.TrySetActive(false);
            }
                

            //}

            //if (upgradeInfo != null && upgradeInfo.DisplayStuff != 0)
            //{
            //    m_imgStuff.SetSprite(ResourceManager.LoadIcon(ItemDataManager.GetItem(upgradeInfo.DisplayStuff).Icon), ItemDataManager.GetItem(upgradeInfo.DisplayStuff).SubType);
            //    m_imgStuff.enabled = true;
            //    if(PanelStrengthenMain.IsStrengthenOpen)
            //        m_txtCount.gameObject.TrySetActive(true);
            //    var pitemStuff = ItemDataManager.GetDepotItem(upgradeInfo.DisplayStuff);
            //    m_txtCount.text = "X " + (pitemStuff != null ? pitemStuff.item_cnt : 0);
            //}
            //else
            //{
            //    m_imgStuff.enabled = false;
            //    m_txtCount.gameObject.TrySetActive(false);
            //}
            


            //星级
            if (strengthenInfo.awaken_data != null && strengthenInfo.awaken_data.level > 0)
            {
                m_sunGroup.TrySetActive(true);
                m_starGroup.TrySetActive(false);
                var show_num = Mathf.Ceil(strengthenInfo.awaken_data.level / 2f);
                for (int i = 0; i < m_suns.Length; i++)
                {
                    var img = m_suns[i].GetComponent<Image>();
                    img.gameObject.TrySetActive(i < show_num);
                    if (img.gameObject.activeSelf)
                    {
                        img.fillAmount = strengthenInfo.awaken_data.level < (i + 1) * 2 ? 0.5f : 1f;
                    }
                }
                m_lvlup.gameObject.TrySetActive(strengthenInfo.awaken_data.level != 10 && canawake);
                m_info.gameObject.TrySetActive(strengthenInfo.awaken_data.level != 10 && canawake);
                m_canUp.gameObject.TrySetActive(strengthenInfo.awaken_data.level != 10 && canawake);
                bubble.TrySetActive(strengthenInfo.awaken_data.level != 10&&canawake); 
            }
            else
            {
                m_sunGroup.TrySetActive(false);
                if(PanelStrengthenMain.IsStrengthenOpen)
                    m_starGroup.TrySetActive(true); 
                for (int j = 0; j < m_stars.Length; j++)
                {
                    m_stars[j].TrySetActive(j < strengthenInfo.state);
                }
            }

            //武器图标
            m_item_img.transform.localScale = Vector3.one;
            m_item_img.SetSprite(ResourceManager.LoadIcon(_data.Icon),_data.SubType);
            m_item_img.SetNativeSize();
            m_item_img.transform.localScale *= 0.8f;
            if (!has_own)
            {
                Util.SetGrayShader(m_content.GetComponent<Image>());
            }
            else
                Util.SetNormalShader(m_content.GetComponent<Image>());

            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + colors[_data.RareType - 1]));
        }
    }
}

