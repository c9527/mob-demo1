﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelStrengthen : BasePanel
{
    int _total_page = 1;
    int _current_page = 1;
    List<ConfigItemWeaponLine> _source_data;
    ItemDropInfo[] _weapon_type_data;
    ItemDropInfo[] _weapon_raretype_data;
    ScrollRect _item_view;
    DataGrid _item_list;
    Text _page_txt;
    Button _filter_btn;
    Button _raretype_btn;
    string now_rareType;
    string now_weapon;
    bool _isScrolling;
    Text m_weaponType;
    Text m_rareType;

    DataGrid m_pieces;
    GameObject m_pieceGroup;

    DataGrid m_pages;

    Text m_getNum;
    Text m_hunNum;

    GameObject m_bubble;

    class p_page
    {
        public int page;
        public bool ison;
    }

    class ItemPage : ItemRender
    {
        GameObject m_pageOn;
        public int page;

        public override void Awake()
        {
            m_pageOn = transform.Find("isnow").gameObject;
            page = 0;
        }

        protected override void OnSetData(object data)
        {
            var info = data as p_page;
            page = info.page;
            m_pageOn.TrySetActive(info.ison);
        }


    }



    class p_piece
    {
        public ConfigItemOtherLine data;
        public int num;
    }

    class ItemPiece : ItemRender
    {
        Image m_item;
        Text m_name;
        Text m_num;

        public override void Awake()
        {
            m_item = transform.Find("itemImage").GetComponent<Image>();
            m_name = transform.Find("name").GetComponent<Text>();
            m_num = transform.Find("num").GetComponent<Text>();
        }

        protected override void OnSetData(object data)
        {
            var _data = data as p_piece;
            m_item.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, _data.data.Icon));
            m_name.text = _data.data.DispName;
            m_num.text = _data.num.ToString();
        }
    }
    


    public PanelStrengthen()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthen");

        AddPreLoadRes("UI/Strengthen/PanelStrengthen", EResType.UI);
        AddPreLoadRes("UI/Strengthen/StrengthenItemRender", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
    }

    public override void Init()
    {
        now_rareType = "ALL";
        now_weapon = "ALL";
        _weapon_type_data = new ItemDropInfo[] {
            new ItemDropInfo { type = "WEAPON", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "MACHINEGUN", label = "机枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "PISTOL", label = "手枪" },
            new ItemDropInfo { type = "WEAPON", subtype = "RIFLE", label = "步枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SHOTGUN", label = "散弹枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SNIPER", label = "狙击枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SUBMACHINE", label = "冲锋枪" },
            new ItemDropInfo { type = "WEAPON", subtype = "BOW", label = "弓箭" }
        };

        _weapon_raretype_data = new ItemDropInfo[]{
            new ItemDropInfo{type = "RARETYPE",subtype = "ALL",label = "品质"  },
            new ItemDropInfo{type = "RARETYPE",subtype = "1",label = "普通"  },
            new ItemDropInfo{type = "RARETYPE",subtype = "2",label = "优秀"  },
            new ItemDropInfo{type = "RARETYPE",subtype = "3",label = "精英"  },
            new ItemDropInfo{type = "RARETYPE",subtype = "4",label = "传说"  },
            new ItemDropInfo{type = "RARETYPE",subtype = "5",label = "英雄"  },
        };

        _item_view = m_tran.Find("WeaponGrid").GetComponent<ScrollRect>();
        _item_list = m_tran.Find("WeaponGrid/content").gameObject.AddComponent<DataGrid>();
        _item_list.SetItemRender(ResourceManager.LoadUIRef("UI/Strengthen/StrengthenItemRender"), typeof(StrengthenItemRender));
        //_page_txt = m_tran.Find("page_txt").GetComponent<Text>();
        _filter_btn = m_tran.Find("fliterGroup/Type").GetComponent<Button>();
        _raretype_btn = m_tran.Find("fliterGroup/Rare").GetComponent<Button>();
        m_weaponType = m_tran.Find("fliterGroup/Type/Text").GetComponent<Text>();
        m_rareType = m_tran.Find("fliterGroup/Rare/Text").GetComponent<Text>();

        m_pieceGroup = m_tran.Find("pieceGroup").gameObject;
        m_pieces = m_tran.Find("pieceGroup/content").gameObject.AddComponent<DataGrid>();
        m_pieces.SetItemRender(m_tran.Find("pieceGroup/content/pieceItem").gameObject, typeof(ItemPiece));

        m_pages = m_tran.Find("PageGroup").gameObject.AddComponent<DataGrid>();
        m_pages.SetItemRender(m_tran.Find("itempage").gameObject, typeof(ItemPage));

        m_getNum = m_tran.Find("weaponNum").GetComponent<Text>();
        m_hunNum = m_tran.Find("hun_num").GetComponent<Text>();
        m_bubble = m_tran.Find("OpenTrain/bubble").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("pageDown")).onPointerClick += OnPageViewChangeHandler;
        UGUIClickHandler.Get(m_tran.Find("pageUp")).onPointerClick += OnPageViewChangeHandler;
        _item_list.autoSelectFirst = false;
        _item_list.onItemSelected = onItemClickHandler;
        UGUIDragHandler.Get(_item_view.gameObject).onEndDrag += OnPageViewChangeHandler;
        UGUIScrollHandler.Get(_item_view.gameObject).onScroll += OnPageViewChangeHandler;
        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);


        UGUIClickHandler.Get(_filter_btn.gameObject).onPointerClick += OnFilterBtnClick;
        UGUIClickHandler.Get(_raretype_btn.gameObject).onPointerClick += OnRareTypeBtnClick;
        UGUIClickHandler.Get(m_tran.Find("fliterGroup/Peice").gameObject).onPointerClick += delegate { UpdataPiece(); m_pieceGroup.TrySetActive(!m_pieceGroup.activeSelf); };
        UGUIClickHandler.Get(m_tran.Find("OpenTrain")).onPointerClick += delegate { if (PlayerSystem.roleData.level < 8) { UIManager.ShowTipPanel(string.Format("{0}级开启枪械训练", 8)); } else {UIManager.ShowPanel<PanelStrengthenTrain>();} };
    }

    private void UpdateBubble()
    {
        m_bubble.TrySetActive(BubbleManager.HasBubble(BubbleConst.StrengthenTrain));
    }


    private void OnFilterBtnClick(GameObject target, PointerEventData eventData)
    {
        
        UIManager.ShowDropList(_weapon_type_data, OnUpdateWeaponSourceData, m_tran, target, 150, 50);
    }

    private void OnRareTypeBtnClick(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowDropList(_weapon_raretype_data, OnUpdateWeaponRareTypeData, m_tran, target, 150, 50);
    }



    private void OnUpdateWeaponSourceData(ItemDropInfo vo=null)
    {
        var config = ConfigManager.GetConfig<ConfigItemWeapon>();
        _source_data = new List<ConfigItemWeaponLine>();
        if (vo != null)
        {
            now_weapon = vo.subtype;
            m_weaponType.text = vo.label;
        }
        int hasnum = 0;
        int total = 0;
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            if (config.m_dataArr[i].Dispbook == 1 && (config.m_dataArr[i].SubType == "WEAPON1" || config.m_dataArr[i].SubType == "WEAPON2"))
            {
                total++;
                var a = ItemDataManager.GetDepoUnlimitItem(config.m_dataArr[i].ID);
                if (a != null && TimeUtil.GetRemainTimeType(a.time_limit) != -1)
                {
                    hasnum++;
                }
                if (vo == null || vo.subtype == "ALL" || (vo.subtype==config.m_dataArr[i].WeaponType))
                {
                    if (now_rareType == "ALL" || config.m_dataArr[i].RareType.ToString() == now_rareType)
                    {
                        if (now_weapon == "ALL" || config.m_dataArr[i].WeaponType == now_weapon)
                        {
                    _source_data.Add(config.m_dataArr[i]);
                }
            }
        }
            }
        }
        _source_data.Sort((a, b) =>
        {
            var result = 0;
            var sa = ItemDataManager.GetDepoUnlimitItem(a.ID);
            var sb = ItemDataManager.GetDepoUnlimitItem(b.ID);
            var a_value = sa != null && TimeUtil.GetRemainTimeType(sa.time_limit) != -1 ? 1 : 0;
            var b_value = sb != null && TimeUtil.GetRemainTimeType(sb.time_limit) != -1 ? 1 : 0;
            result = b_value - a_value;
            if (result == 0)
            {
                result = a.ID - b.ID;
            }
            return result;
        });
        _total_page = Math.Max((_source_data.Count - 1) / 8 + 1, 1);
        _current_page = 1;
        //_filter_btn.transform.Find("Text").GetComponent<Text>().text = vo != null ? vo.label : "全部";
        updatePageView();
        m_getNum.text = "已拥有：<color=lime>" + hasnum.ToString() + "</color>" + "/" + total.ToString();
        var hun = ItemDataManager.GetDepotItem(5016);
        if (hun != null)
        {
            m_hunNum.text = hun.item_cnt.ToString();
    }
        else
        {
            m_hunNum.text = "0";
        }
    }

    private void OnUpdateWeaponRareTypeData(ItemDropInfo vo = null)
    {
        var config = ConfigManager.GetConfig<ConfigItemWeapon>();
        _source_data = new List<ConfigItemWeaponLine>();
        if (vo != null)
        {
            
            now_rareType = vo.subtype;
            m_rareType.text = vo.label;
        }
        for (int i = 0; i < config.m_dataArr.Length; i++)
        {
            if (config.m_dataArr[i].Dispbook == 1 && (config.m_dataArr[i].SubType == "WEAPON1" || config.m_dataArr[i].SubType == "WEAPON2"))
            {
                if (vo == null || vo.subtype == "ALL" || vo.subtype == config.m_dataArr[i].RareType.ToString())
                {
                    if (now_weapon == "ALL" || config.m_dataArr[i].WeaponType == now_weapon)
                    {
                        _source_data.Add(config.m_dataArr[i]);
                    }
                }
            }
        }
        _source_data.Sort((a, b) =>
        {
            var result = 0;
            var sa = ItemDataManager.GetDepoUnlimitItem(a.ID);
            var sb = ItemDataManager.GetDepoUnlimitItem(b.ID);
            var a_value = sa != null && TimeUtil.GetRemainTimeType(sa.time_limit) != -1 ? 1 : 0;
            var b_value = sb != null && TimeUtil.GetRemainTimeType(sb.time_limit) != -1 ? 1 : 0;
            result = b_value - a_value;
            if (result == 0)
            {
                result = a.ID - b.ID;
            }
            return result;
        });
        _total_page = Math.Max((_source_data.Count - 1) / 8 + 1, 1);
        _current_page = 1;
        //_filter_btn.transform.Find("Text").GetComponent<Text>().text = vo != null ? vo.label : "全部";
        updatePageView();
    }


    private void onItemClickHandler(object renderData)
    {
        if (renderData == null)
        {
            return;
        }
        UIManager.ShowPanel<PanelStrengthenDetail>(new object[] { renderData });
        
    }


    public override void OnShow()
    {
        OnUpdateWeaponSourceData();
        StrengthenModel.UpdateBubble();
        UpdateBubble();
        UpdataPiece();
        m_pieceGroup.TrySetActive(false);
    }

    public override void OnHide()
    {
        _source_data = null;
        UIManager.HideDropList();
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
//        PanelStrengthenMain.showSubPanel<PanelStrengthenTrain>();
    }

    public override void Update()
    {

    }

    public override void OnDestroy()
    {
        _item_list = null;
    }

    private void OnPageViewChangeHandler(GameObject obj, PointerEventData data)
    {
        if (_total_page < 2 || _isScrolling)
        {
            return;
        }
        var step = 0;
        if (obj == _item_view.gameObject)
        {
            if (!Mathf.Approximately(data.scrollDelta.y, 0))
            {
                step = (int)Mathf.Sign(data.scrollDelta.y);
            }
            else
            {
                _item_view.StopMovement();
                var trans = _item_view.transform as RectTransform;
                if (data.delta.x != 0 || (trans != null && Mathf.Abs(_item_list.transform.localPosition.x) > trans.sizeDelta.x / 2))
                {
                    step = _item_list.transform.localPosition.x < 0 ? 1 : -1;
                }
            }
        }
        else if (obj != null)
        {
            step = obj.name == "pageUp" ? -1 : 1;
        }

        _current_page += step;
        if (_current_page < 1)
        {
            _current_page = _total_page;
        }
        else if (_current_page > _total_page)
        {
            _current_page = 1;
        }
        PlayPageTween(step);
    }

    private void updatePageView(int page = -1)
    {
        _isScrolling = false;
        _current_page = page > 0 ? page : _current_page;
        int start_index = 0;
        if (_current_page <= 1)
        {
            start_index = (_total_page - 1) * 8;
        }
        else
        {
            start_index = (_current_page - 1 - 1) * 8;
        }
        object[] page_data = _source_data != null && _source_data.Count > 8 ? new object[24] : new object[8];
        for (int i = 0; i < page_data.Length; i++)
        {
            var data_index = start_index + i;
            if (data_index >= _total_page * 8)
            {
                data_index %= _total_page * 8;
            }
            var fix_i = i % 8;
            var pos = page_data.Length / 2 * (i % 2) + i / 2;
            page_data[pos] = _source_data != null && data_index < _source_data.Count ? _source_data[data_index] : null;
        }
        _item_list.Data = page_data;
        _item_list.Select(null);
        //_page_txt.text = _current_page + "/" + _total_page;
        _item_view.StopMovement();
        //_item_list.horizonPos = 0.5f;
        _item_list.transform.localPosition = new Vector3(0f, _item_list.transform.localPosition.y, 0f);
        UpdatePage();
    }

    private void PlayPageTween(int direction = 0)
    {
        var trans = _item_view.transform as RectTransform;
        if (trans!=null)
        {
            var t_pos = Vector3.zero;
            if (direction != 0)
            {
                t_pos.x = trans.sizeDelta.x * (direction < 0 ? 1 : -1);
               
            }
            t_pos.y = _item_list.transform.localPosition.y;
            _isScrolling = true;
            var tween = TweenPosition.Begin(_item_list.gameObject, 0.25f, t_pos);
            tween.method = UITweener.Method.Linear;
            tween.SetOnFinished(() => updatePageView());
        }
        else
        {
            updatePageView();
        }
    }

    private void Toc_player_trigger_guide(toc_player_trigger_guide data)
    {
        if (!data.is_trigger)
            return;

//        GuideManager.StartChapterName("WeaponDetail");
    }

    void UpdataPiece()
    {
        ConfigItemOtherLine[] data = ItemDataManager.GetMaterialsByRareType(-1);
        p_piece[] pie = new p_piece[data.Length];
        for (int i = 0; i < data.Length; i++)
        {
            int num = 0;
            var info = ItemDataManager.GetDepotItem(data[i].ID);
            if (info != null)
            {
                num = info.item_cnt;
                
            }
            pie[i] = new p_piece() { num = num, data = data[i] };
        }

        m_pieces.Data = pie;
    }

    void UpdatePage()
    {
        p_page[] pages = new p_page[_total_page];
        for (int i = 0; i < _total_page; i++)
        {
            pages[i] = new p_page() { ison = false, page = i + 1 };
            if (i + 1 == _current_page)
            {
                pages[i].ison = true;
            }
        }
        m_pages.Data = pages;
    }
}
