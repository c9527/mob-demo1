﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelItemGuide : BasePanel
{
    private ItemInfo _info;

    public Image item_img;
    public Text name_txt;
    public Text value_txt;
    public Text desc_txt;
    public DataGrid guide_list;
    

    public PanelItemGuide()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelItemGuide");

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("UI/Strengthen/PanelItemGuide", EResType.UI);
    }

    public override void Init()
    {
        item_img = m_tran.Find("frame/item_img/Image").GetComponent<Image>();
        name_txt = m_tran.Find("frame/name_txt").GetComponent<Text>();
        value_txt = m_tran.Find("frame/value_txt").GetComponent<Text>();
        desc_txt = m_tran.Find("frame/desc_txt").GetComponent<Text>();

        guide_list = m_tran.Find("frame/guide_list/content").gameObject.AddComponent<DataGrid>();
        guide_list.autoSelectFirst = false;
        guide_list.SetItemRender(m_tran.Find("frame/guide_list/Render").gameObject, typeof(ItemGuideRender));
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/close_btn")).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
        if (HasParams())
        {
            _info = m_params[0] as ItemInfo;
        }
        UpdateView();
    }

    private void UpdateView()
    {
        if (_info == null)
        {
            return;
        }
        var titem = ItemDataManager.GetItem(_info.ID);
        var sitem = ItemDataManager.GetDepotItem(_info.ID);
        name_txt.text = titem.DispName;
        if(_info.ID!=5003)
        {
            value_txt.text = string.Format("<color={2}>{0}/{1}</color>", sitem != null ? sitem.item_cnt : 0, _info.cnt, sitem != null && sitem.item_cnt >= _info.cnt ? "green" : "red");
        }
        else
        {
            value_txt.text = _info.cnt.ToString();
        }        
        desc_txt.text = titem.Desc;
        item_img.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, titem.Icon));
        item_img.SetNativeSize();        
        var guide_data = (titem is ConfigItemOtherLine) && !string.IsNullOrEmpty((titem as ConfigItemOtherLine).GuideData) ? (titem as ConfigItemOtherLine).GuideData.Split(';') : null;
        var source_data = new List<CDItemGuide>();
        if (guide_data != null && guide_data.Length > 0)
        {
            for (int i = 0; i < guide_data.Length; i++)
            {
                var temp = guide_data[i].Split('#');
                source_data.Add(new CDItemGuide() { index=i+1, guide_type=int.Parse(temp[0]), desc=temp[1] });
            }
        }
        guide_list.Data = source_data.ToArray();
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}

public class CDItemGuide
{
    public int index;
    public int guide_type;
    public string desc;
}

public class ItemGuideRender : ItemRender
{
    CDItemGuide _info;

    public Text name_txt;
    public Button fun_btn;

    public override void Awake()
    {
        name_txt = transform.Find("Text").GetComponent<Text>();
        fun_btn = transform.Find("Button").GetComponent<Button>();

        UGUIClickHandler.Get(fun_btn.gameObject).onPointerClick += OnFunBtnClick;
    }

    private void OnFunBtnClick(GameObject target, PointerEventData eventData)
    {
        UIManager.GetPanel<PanelItemGuide>().HidePanel();
        if (_info == null || _info.guide_type == 0)
        {
            UIManager.ShowTipPanel("数据配置错误");
            return;
        }
        if (_info.guide_type == 1)
        {
            PanelRoomBase.RoomMinimizeCheck( ()=>
                {
                    UIManager.GotoPanel("battle");
                });            
        }
        else if (_info.guide_type == 2)
        {
            PanelRoomBase.RoomMinimizeCheck(() =>
            {
                UIManager.ShowPanel<PanelPVE>();
            });     
            
        }
        else if (_info.guide_type == 3)
        {
            PanelRoomBase.RoomMinimizeCheck(() =>
            {
                UIManager.ShowPanel<PanelTeam>();
            });  
            
        }
        else if (_info.guide_type == 4)
        {
            TipsManager.Instance.showTips("此功能目前未开放");
            //PanelStrengthenMain.showSubPanel<PanelMaterialConver>();
        }
        else if (_info.guide_type == 5)
        {
            UIManager.ShowPanel<PanelMall>();
        }
        else if(_info.guide_type==6) //每日任务
        {
            UIManager.GetPanel<PanelHall>().OnMissionToggleClick(null,null);
            UIManager.GotoPanel("mission");
        }
        else if(_info.guide_type==7) //在线领奖
        {
            UIManager.PopPanel<PanelOnlineReward>(null, true, null);
        }
        else if(_info.guide_type==8) //完成战队任务
        {
            if(!CorpsDataManager.IsJoinCorps())
            {
                UIManager.ShowTipPanel("你还未加入战队，是否加入战队？", () =>
                {
                    UIManager.GotoPanel("corps");
                });
            }
            else
            {
                UIManager.GotoPanel("corps#corpsActivity#corpsCoperateTask");
            }
        }
        else if(_info.guide_type==9) //参与战队副本
        {
            if (!CorpsDataManager.IsJoinCorps())
            {
                UIManager.ShowTipPanel("你还未加入战队，是否加入战队？", () => {
                    UIManager.GotoPanel("corps");
                });
            }
            else
            {
                UIManager.GotoPanel("corps#corpsActivity");
            }
        } 
        else if(_info.guide_type==10) //重置
        {
            SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
        }
        else if(_info.guide_type==11) //世界boss
        {
            UIManager.GotoPanel("boss");
        }
    }

    protected override void OnSetData(object data)
    {
        _info = data as CDItemGuide;
        if (_info == null)
        {
            return;
        }
        name_txt.text = _info.index + "." + _info.desc;
    }
}
