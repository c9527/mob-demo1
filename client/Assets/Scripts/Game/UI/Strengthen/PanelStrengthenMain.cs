﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelStrengthenMain : BasePanel
{
    BasePanel _current_panel;

    Image _train_tip_img;
    Image _handbook_tip_img;
    private static ConfigMiscLine m_strengthenOpenLv;

    /// <summary>
    /// 强化系统开启等级
    /// </summary>
    public static int OpenLv
    {
        get
        {
            if (m_strengthenOpenLv == null)
                m_strengthenOpenLv = ConfigManager.GetConfig<ConfigMisc>().GetLine("strength_openLv");
            return m_strengthenOpenLv.ValueInt;
        }
    }

    public static int Lev
    {
        get
        {
            return PlayerSystem.roleData.level;
        }
    }
    public static bool IsStrengthenOpen
    {
        get
        {
            return Lev >= OpenLv;
        }
    }
    private GameObject m_talent_btn;
    private GameObject m_train_btn;
    private GameObject m_conver_btn;

    public PanelStrengthenMain()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthenMain");

        AddPreLoadRes("Atlas/Strengthen", EResType.Atlas);
        AddPreLoadRes("UI/Strengthen/PanelStrengthenMain", EResType.UI); 
    }

    static public void showSubPanel<T>(object[] @params = null) where T : BasePanel, new()
    {
        var main_panel = UIManager.GetPanel<PanelStrengthenMain>();
        if (main_panel != null && main_panel.IsOpen())
        {
            main_panel.openSubPanel<T>(@params,true);
        }
    }

    private T openSubPanel<T>(object[] @params=null,bool setup_tab=false) where T : BasePanel,new()
    {
        if(_current_panel==null || !(_current_panel is T))
        {
            if (_current_panel != null)
            {
                _current_panel.HidePanel();
            }
            _current_panel = UIManager.PopPanel<T>(@params,false,this, LayerType.Content);
            if (_current_panel is PanelStrengthen)
            {
                m_tran.Find("tabbar/handbook_btn").GetComponent<Toggle>().isOn = true;
            }
            else if (_current_panel is PanelStrengthenTrain)
            {
                m_tran.Find("tabbar/train_btn").GetComponent<Toggle>().isOn = true;
            }
        }
        return _current_panel as T;
    }

    public override void Init()
    {
        m_tran.Find("tabbar/talent_btn").GetComponent<Toggle>().enabled = false;
        _train_tip_img = m_tran.Find("tabbar/train_btn/bubble_tip").GetComponent<Image>();
        _handbook_tip_img = m_tran.Find("tabbar/handbook_btn/bubble_tip").GetComponent<Image>();
        m_talent_btn = m_tran.Find("tabbar/talent_btn").gameObject;
        m_train_btn = m_tran.Find("tabbar/train_btn").gameObject;
        m_conver_btn = m_tran.Find("tabbar/conver_btn").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("tabbar/train_btn")).onPointerClick += onFunBtnClick;
        UGUIClickHandler.Get(m_tran.Find("tabbar/handbook_btn")).onPointerClick += onFunBtnClick;
        UGUIClickHandler.Get(m_tran.Find("tabbar/talent_btn")).onPointerClick += onFunBtnClick;
        UGUIClickHandler.Get(m_tran.Find("tabbar/conver_btn")).onPointerClick += onFunBtnClick;

        AddEventListener(GameEvent.UI_BUBBLE_CHANGE, UpdateBubble);
    }

    private void UpdateBubble()
    {
        _train_tip_img.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.StrengthenTrain));
        _handbook_tip_img.gameObject.TrySetActive(BubbleManager.HasBubble(BubbleConst.StrengthenUpgrade));
    }

    private void onFunBtnClick(GameObject target, PointerEventData eventData=null)
    {
        OpenSubPanelByParam(target.name);
    }

    private void OpenSubPanelByParam(string param)
    {
        var arr = param.Split('#');
        var name = arr[0];
        if (name == "train_btn")
        {
            if (IsStrengthenOpen)
                showSubPanel<PanelStrengthenTrain>();
            else
            {
                TipsManager.Instance.showTips(string.Format("强化训练{0}级开启", OpenLv));                     
            }
        }
        else if (name == "handbook_btn")
            showSubPanel<PanelStrengthen>();
        else if (name == "conver_btn")
            showSubPanel<PanelMaterialConver>();
        else if (name == "detail")
            showSubPanel<PanelStrengthenDetail>(new object[] { ItemDataManager.GetItemWeapon(int.Parse(arr[1])), null, null, arr.Length > 2 ? arr[2] : null });
        else
        {
            UIManager.ShowTipPanel("该功能尚未开启");
            return;
        }
        //var tab_btn = m_tran.Find("tabbar/" + name);
        //if (tab_btn != null)
        //{
        //    var tab_toggle = tab_btn.GetComponent<Toggle>();
        //    if (tab_toggle != null && tab_toggle.isOn == false)
        //    {
        //        tab_toggle.isOn = true;
        //    }
        //}
    }

    public override void OnShow()
    {
        StrengthenOpenFilter();
        var hall = UIManager.GetPanel<PanelHall>();
        if (hall != null && hall.IsOpen())
        {
            hall.CheckChosen(hall.m_btgEnhance);
        }
        var fun_btn = m_tran.Find("tabbar/handbook_btn").GetComponent<Toggle>();
        fun_btn.isOn = true;
        UpdateBubble();

        
        if (HasParams())
            OpenSubPanelByParam(m_params[0].ToString());
        else
            onFunBtnClick(fun_btn.gameObject);
    }

    void StrengthenOpenFilter()
    {
        if(Lev<OpenLv )
        {
            m_talent_btn.TrySetActive(false);
            //m_train_btn.TrySetActive(false);
            m_train_btn.GetComponent<Toggle>().enabled = false;
            m_train_btn.transform.Find("lock").gameObject.TrySetActive(true);
            m_conver_btn.TrySetActive(false);
        }
        else
        {
            m_talent_btn.TrySetActive(true);
            m_train_btn.GetComponent<Toggle>().enabled = true;
            m_train_btn.transform.Find("lock").gameObject.TrySetActive(false);
            //m_conver_btn.TrySetActive(true);
        }
    }
    public override void OnHide()
    {
        if (_current_panel != null)
        {
            _current_panel.HidePanel();
            _current_panel = null;
        }
    }

    public override void OnBack()
    {
        if (_current_panel == null)
        {
            UIManager.ShowPanel<PanelHallBattle>();
        }
        else
        {
            _current_panel.OnBack();
        }
    }

    public override void OnDestroy()
    {
        
    }

    public override void Update()
    {
        
    }
}

