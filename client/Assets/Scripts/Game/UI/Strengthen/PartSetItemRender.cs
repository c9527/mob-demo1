﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PartSetItemRender : ItemRender
{
    CDItemData _data;

    Toggle _toggle;
    Image _frame_img;
    Image _item_img;
    Text _name_txt;
    UIEffect _suit_effect;

    public CDItemData data
    {
        get{return _data;}
    }

    public override void Awake()
    {
        _toggle = transform.GetComponent<Toggle>();
        _frame_img = transform.Find("Frame").GetComponent<Image>();
        _item_img = transform.Find("Image").GetComponent<Image>();
        _name_txt = transform.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as CDItemData;
        _toggle.interactable = false;
        if (_data != null)
        {
            if (_data.sdata.item_id == 0)
            {
                _toggle.interactable = true;
                _frame_img.enabled = true;
                _frame_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "set_frame_1"));
                _item_img.enabled = false;
                _name_txt.text = "";
            }
            else if (_data.sdata.item_id > 0)
            {
                _toggle.interactable = true;
                _item_img.enabled = true;
                var info = _data.info as ConfigItemAccessoriesLine;
                var quality = _data.GetQuality();
                _frame_img.enabled = quality > 0;
                _frame_img.SetSprite(quality > 0 ? ResourceManager.LoadSprite(AtlasName.Strengthen, "set_frame_" + quality) : null);
                _frame_img.SetNativeSize();
                _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
                _item_img.SetNativeSize();
                _name_txt.text = info.partTypeName;
            }
            else
            {
                _frame_img.enabled = false;
                _item_img.enabled = false;
                _name_txt.text = "";
            }
        }
        else
        {
            _frame_img.enabled = false;
            _item_img.enabled = false;
            _name_txt.text = "";
        }
    }

    public void PlaySuitEffect(int value=0)
    {
        if (_suit_effect != null)
        {
            _suit_effect.Destroy();
            _suit_effect = null;
        }
        if (value >= 4)
        {
            _suit_effect = UIEffect.ShowEffect(transform, EffectConst.UI_STRENGTHEN_SUIT_EFFECT_3, 0f);
        }
        else if (value >= 3)
        {
            _suit_effect = UIEffect.ShowEffect(transform, EffectConst.UI_STRENGTHEN_SUIT_EFFECT_2, 0f);
        }
        else if (value >= 2)
        {
            _suit_effect = UIEffect.ShowEffect(transform, EffectConst.UI_STRENGTHEN_SUIT_EFFECT_1, 0f);
        }
    }
}

