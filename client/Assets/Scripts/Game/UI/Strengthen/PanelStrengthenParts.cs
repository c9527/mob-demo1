﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class PanelStrengthenParts : BasePanel
{
    public PanelStrengthenParts()
    {
        SetPanelPrefabPath("UI/Strengthen/PanelStrengthenParts");
        AddPreLoadRes("UI/Strengthen/PanelStrengthenParts", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }


    ConfigItemWeaponLine _data;
    p_item _sdata;
    p_weapon_strength_info_toc _strengthen_data = null;
    toc_player_stop_weapon_train _gain_data = null;
    Button m_equip;
    Button m_remove;
    Button m_sellout;
    Button m_showinfo;
    GameObject m_noneEquiped;
    GameObject m_noneChosen;
    GameObject m_Equiped;
    GameObject m_Chosen;

    Image m_equipIcon;
    Image m_equipBg;
    Text m_equipName;
    Text m_equipProp;
    Text m_suitName;
    Text m_suitProp;

    Image m_chosenIcon;
    Image m_chosenBg;
    Text m_chosenName;
    Text m_chosenProp;
    Text m_chosensuitName;
    Text m_chosensuitProp;
    DataGrid _part_dg;

    ItemDropInfo[] _part_type_data;

    PartSetItemRender[] _set_part_list;

    CDItemData m_selpart;
    CDItemData m_selequip;
    int selid;
    int opmode;
    string type;

    Text m_type;
    GameObject m_btnjian;
    GameObject m_btnsell;
    Text m_weaponname;
    GameObject m_mask;
    Text m_btntxt;
    
    public override void Init()
    {
        m_selequip = null;
        m_selpart = null;

        m_remove = m_tran.Find("Equiped/chaichu").GetComponent<Button>();
        m_equip = m_tran.Find("Chosen/xiangqian").GetComponent<Button>();
        m_sellout = m_tran.Find("sell").GetComponent<Button>();
        m_showinfo = m_tran.Find("jianding").GetComponent<Button>();

        m_noneEquiped = m_tran.Find("noneEquiped").gameObject;
        m_noneChosen = m_tran.Find("noneChosen").gameObject;
        m_Equiped = m_tran.Find("Equiped").gameObject;
        m_Chosen = m_tran.Find("Chosen").gameObject;

        m_equipIcon = m_tran.Find("Equiped/ItemPart/Image").GetComponent<Image>();
        m_equipBg = m_tran.Find("Equiped/ItemPart").GetComponent<Image>();
        m_equipName = m_tran.Find("Equiped/Name").GetComponent<Text>();
        m_equipProp = m_tran.Find("Equiped/prop").GetComponent<Text>();
        //m_suitName = m_tran.Find("Equiped/suitname").GetComponent<Text>();
        m_suitProp = m_tran.Find("Equiped/suitprop").GetComponent<Text>();

        m_chosenIcon = m_tran.Find("Chosen/ItemPart/Image").GetComponent<Image>();
        m_chosenBg = m_tran.Find("Chosen/ItemPart").GetComponent<Image>();
        m_chosenName = m_tran.Find("Chosen/Name").GetComponent<Text>();
        m_chosenProp = m_tran.Find("Chosen/prop").GetComponent<Text>();
        //m_chosensuitName = m_tran.Find("Chosen/suitname").GetComponent<Text>();
        m_chosensuitProp = m_tran.Find("Chosen/suitprop").GetComponent<Text>();
        opmode = 0;
        _part_type_data = new ItemDropInfo[] {
            new ItemDropInfo(){ type="",label="全部"},
            new ItemDropInfo(){ type="1",label="枪口配件"},
            new ItemDropInfo(){ type="2",label="前握把"},
            new ItemDropInfo(){ type="3",label="弹夹"},
            new ItemDropInfo(){ type="4",label="瞄具"},
        };


        _set_part_list = new PartSetItemRender[4];
        m_tran.Find("part_group").gameObject.AddComponent<SortingOrderRenderer>();
        for (int i = 0; i < _set_part_list.Length; i++)
        {
            _set_part_list[i] = m_tran.Find("part_group/part_item_" + i).gameObject.AddComponent<PartSetItemRender>();
        }

        _part_dg = m_tran.Find("partGrid/content").gameObject.AddComponent<DataGrid>();
        _part_dg.autoSelectFirst = false;
        _part_dg.SetItemRender(m_tran.Find("partGrid/content/Itempart").gameObject, typeof(UIItemRender));

        _part_dg.onItemSelected += OnPartListItemClick;
        selid = 0;

        m_type = m_tran.Find("partbtn/Text").GetComponent<Text>();
        m_btnjian = m_tran.Find("jianding").gameObject;
        m_btnsell = m_tran.Find("sell").gameObject;
        m_weaponname = m_tran.Find("part_group/Text").GetComponent<Text>();
        m_mask = m_tran.Find("mask").gameObject;
        m_btntxt = m_tran.Find("Chosen/xiangqian/Text").GetComponent<Text>();
    }

    public override void OnShow()
    {
        
        if (HasParams())
        {
            _data = m_params[0] as ConfigItemWeaponLine;
            _sdata = m_params[1] as p_item;
            _strengthen_data = m_params[2] as p_weapon_strength_info_toc;
            _gain_data = m_params[3] as toc_player_stop_weapon_train;
            selid = int.Parse(m_params[4].ToString());
        }

        m_weaponname.text = _data.DispName;
        UpdateEquiped();
        UpdatePartsList();
        _part_dg.Select(0);
        OnSetPartListItemClick(_set_part_list[selid-1].gameObject, null);
        opmode = 0;
        m_btnjian.SetActive(true);
        m_btnsell.SetActive(true);
        m_mask.TrySetActive(false);
    }


    void UpdateEquiped()
    {
        _sdata = ItemDataManager.GetDepotItem(_data.ID);
        var set_data = StrengthenModel.Instance.GenWeaponPartCDData(_sdata, _strengthen_data.state);
        var suit_map = new Dictionary<int, int>();
        if (set_data != null && set_data.Length > 0)
        {
            for (var j = 0; j < set_data.Length; j++)
            {
                if (set_data[j].info is ConfigItemAccessoriesLine && (set_data[j].info as ConfigItemAccessoriesLine).Suit > 0)
                {
                    var suit_num = 0;
                    suit_map.TryGetValue((set_data[j].info as ConfigItemAccessoriesLine).Suit, out suit_num);
                    suit_num += 1;
                    suit_map[(set_data[j].info as ConfigItemAccessoriesLine).Suit] = suit_num;
                }
            }
        }
        for (int i = 0; i < _set_part_list.Length; i++)
        {
            var set_vo = set_data != null && i < set_data.Length ? set_data[i] : null;
            _set_part_list[i].SetData(set_vo);
            var set_suit_num = 0;

            if (set_vo != null && set_vo.info is ConfigItemAccessoriesLine)
            {
                suit_map.TryGetValue((set_vo.info as ConfigItemAccessoriesLine).Suit, out set_suit_num);
            }
            _set_part_list[i].PlaySuitEffect(set_suit_num);
        }
    }
    public void UpdatePartsList()
    {
        var source_data = ItemDataManager.GetDepotItemsByType(GameConst.ITEM_TYPE_MATERIAL, GameConst.WEAPON_SUBTYPE_ACCESSORY);
        var temp = new List<CDItemData>();
        for (int i = 0; i < source_data.Length; i++)
        {
            var info = ItemDataManager.GetItem(source_data[i].item_id) as ConfigItemAccessoriesLine;
            if (info != null && (string.IsNullOrEmpty(type) || type == info.Part.ToString()))
            {
                temp.Add(new CDItemData(source_data[i]));
            }
        }

        source_data = ItemDataManager.GetDepotItemsByType(GameConst.ITEM_TYPE_EQUIP, GameConst.WEAPON_SUBTYPE_ACCESSORY);
        CDItemData selected_vo = null;
        for (int j = 0; j < source_data.Length; j++)
        {
            var info = ItemDataManager.GetItem(source_data[j].item_id) as ConfigItemAccessoriesLine;
            if (info != null && (string.IsNullOrEmpty(type) || type == info.Part.ToString()))
            {
                var vo = new CDItemData(source_data[j]);
                temp.Add(vo);
                //if (selected_uid > 0 && source_data[j].uid == selected_uid)
                //{
                //    selected_vo = vo;
                //}
            }
        }

        _part_dg.Data = temp.ToArray();

    }

    public override void InitEvent()
    {
        for (int i = 0; i < _set_part_list.Length; i++)
        {
            UGUIClickHandler.Get(_set_part_list[i].gameObject).onPointerClick += OnSetPartListItemClick;
        }

        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += delegate { HidePanel(); };
        UGUIClickHandler.Get(m_sellout.gameObject).onPointerClick += sellPart;
        UGUIClickHandler.Get(m_showinfo.gameObject).onPointerClick += JiangdingPart;
        UGUIClickHandler.Get(m_equip.gameObject).onPointerClick += delegate { OnPartItemFunHandler(m_selpart); };
        UGUIClickHandler.Get(m_remove.gameObject).onPointerClick += delegate { OnPartItemFunHandlerRemove(m_selequip); };
        UGUIClickHandler.Get(m_tran.Find("partbtn")).onPointerClick += OnPartTabClick;
        UGUIClickHandler.Get(m_tran.Find("howToget")).onPointerClick += delegate { UIManager.ShowTipPanel("【闯关挑战-生存模式】可以获得配件", () => { UIManager.ShowPanel<PanelPVE>(); this.HidePanel(); }, null, true, true, "立刻前往"); };
    }

    void sellPart(GameObject target, PointerEventData eventData)
    {
        if (opmode != 1)
        {
            opmode = 1;
            m_btnjian.SetActive(false);
            m_btnsell.SetActive(true);
            m_mask.TrySetActive(true);
            return;
        }

        var source_data = new List<int>();
        var total_coin = 0;
        if (_part_dg.Data != null)
        {
            var test_cost = ConfigMisc.GetInt("accessory_meterial_test_cost");
            for (int i = 0; i < _part_dg.Data.Length; i++)
            {
                var vo = _part_dg.Data[i] as CDItemData;
                if (vo != null && vo.selected == true)
                {
                    source_data.Add(vo.sdata.uid);
                    var info = ItemDataManager.GetItem(vo.sdata.item_id) as ConfigItemAccessoriesLine;
                    if (info != null)
                    {
                            var cost_info = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(Mathf.Max(vo.GetQuality(), 1));
                            total_coin += cost_info != null ? cost_info.BuybackCoin : 0;
                    }
                }
            }
        }
        if (source_data.Count > 0)
        {
            UIManager.ShowTipPanel(string.Format("全部卖给系统可获得{0}金币？", total_coin), () => { StrengthenModel.Instance.TosSellWeaponPart(source_data.ToArray()); opmode = 0; }, null, true, true);
        }
        else
        {
            UIManager.ShowTipPanel("未选择配件");
            m_mask.TrySetActive(false);
            m_btnjian.SetActive(true);
            m_btnsell.SetActive(true);
            opmode = 0;
        }
    }

    void JiangdingPart(GameObject target, PointerEventData eventData)
    {
        if (opmode != 2)
        {
            opmode = 2;
            m_btnjian.SetActive(true);
            m_btnsell.SetActive(false);
            m_mask.TrySetActive(true);
            return;
        }

        var source_data = new List<int>();
        var total_coin = 0;
        if (_part_dg.Data != null)
        {
            var test_cost = ConfigMisc.GetInt("accessory_meterial_test_cost");
            for (int i = 0; i < _part_dg.Data.Length; i++)
            {
                var vo = _part_dg.Data[i] as CDItemData;
                if (vo != null && vo.selected == true)
                {
                    source_data.Add(vo.sdata.uid);
                    var info = ItemDataManager.GetItem(vo.sdata.item_id) as ConfigItemAccessoriesLine;
                    if (info != null)
                    {
                        //var cost_info = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(Mathf.Max(vo.GetQuality(), 1));
                        //total_coin += cost_info != null ? cost_info.BuybackCoin : 0;
                        total_coin += test_cost;
                    }
                }
            }
        }
        if (source_data.Count > 0)
        {
            UIManager.ShowTipPanel(string.Format("本次鉴定需要{0}金币？", total_coin), () => { StrengthenModel.Instance.TosTestPartMeterial(source_data.ToArray()); }, null, true, true);
        }
        else
        {
            UIManager.ShowTipPanel("未选择配件");
            m_mask.TrySetActive(false);
            m_btnjian.SetActive(true);
            m_btnsell.SetActive(true);
            opmode = 0;
        }
    }
    private void OnSetPartListItemClick(GameObject target, PointerEventData eventData)
    {
        selid = int.Parse(target.name.Replace("part_item_", "")) + 1;
        target.GetComponent<Toggle>().isOn = true;
        var render = target.GetComponent<PartSetItemRender>();
        var set_vo = render != null ? render.m_renderData as CDItemData : null;
        m_selequip = set_vo;
        int index = int.Parse(target.name.Replace("part_item_", ""));
        if (index > 0 && !PanelStrengthenMain.IsStrengthenOpen)
        {
            return;
        }
        if (set_vo == null || set_vo.sdata.item_id == -1)
        {
            TipsManager.Instance.showTips("枪械进阶，开启配件槽");
            return;
        }
        m_noneEquiped.TrySetActive(set_vo.sdata.item_id==0);
        m_Equiped.TrySetActive(set_vo.sdata.item_id != 0);

        if (set_vo.sdata.item_id != 0)
        {
            UpdateEquipInfo(set_vo);
            m_noneEquiped.TrySetActive(false);
            m_Equiped.TrySetActive(true);
        }
        else
        {
            m_noneEquiped.TrySetActive(true);
            m_Equiped.TrySetActive(false);
        }
        
    }

    void UpdateEquipInfo(CDItemData item)
    {
        if (item == null)
        {
            return;
        }

        if (item.sdata.item_id <= 0)
        {
            m_noneEquiped.TrySetActive(true);
            m_Equiped.TrySetActive(false);
        }
        else
        {
            m_noneEquiped.TrySetActive(false);
            m_Equiped.TrySetActive(true);
        }
        var cfg = ConfigManager.GetConfig<ConfigItemAccessories>().GetLine(item.sdata.item_id);
        m_equipName.text = cfg.DispName;
        var info = item.info as ConfigItemAccessoriesLine;
        var quality = item.GetQuality();
        m_equipBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "item_frame_" + quality.ToString()));
        var str = "";
        var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
        //属性
        str = "";
        m_equipIcon.SetSprite(ResourceManager.LoadIcon(item.info.Icon), item.info.SubType);
        var add_props = info.GetAddProp();
        for (int i = 0; add_props != null && i < add_props.Length; i++)
        {
            if (item.sdata.accessory_values != null && i < item.sdata.accessory_values.Length)
            {
                if (add_props[i] != null)
                {
                    var temp_value = Mathf.Abs(item.sdata.accessory_values[i]);
                    str += string.Format("{0}：+{1}", prop_config.GetDispPropName(info.DispProps[i]), add_props[i].AddType == 1 ? (temp_value / 10f + "%") : temp_value.ToString());
                }
                else
                {
                    str += info.AddProps[i] + "：unknow";
                }
            }
            else
            {
                str += "???：???";
            }
            str += i % 2 == 1 ? "\n" : " ";
        }
        m_equipProp.text = str;

        str = "";
        var suit_config = ConfigManager.GetConfig<ConfigAccessorySuit>();
        var set_part_count = 0;
        var item_id = item.extend is KeyValue<int, int> ? (item.extend as KeyValue<int, int>).key : 0;
        var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(item_id);
        if (accessoryList != null && accessoryList.Length > 0)
        {
            foreach (var spart in accessoryList)
            {
                var tpart = ItemDataManager.GetItem(spart.item_id);
                if (tpart is ConfigItemAccessoriesLine)
                {
                    set_part_count += (tpart as ConfigItemAccessoriesLine).Suit == info.Suit ? 1 : 0;
                }
            }
        }

        var suit_group = info.Suit > 0 ? suit_config.GetSuitGroupAddPropData(info.Suit) : null;

        if (suit_group!=null && suit_group.Length>0)
        {
                str += (!string.IsNullOrEmpty(suit_group[0].Desc) ? suit_group[0].Desc : "未命名套装") + "\n";
                var prop_list = new List<string>();
                var prep_map = new Dictionary<string,int>();
                foreach(var suit_add in suit_group)
                {
                    prop_list.Clear();
                    for (int i = 0; suit_add.AddProps != null && i < suit_add.AddProps.Length; i++)
                    {
                        var prop_strs = suit_add.AddProps[i].Split('#');
                        var add_value = 0;
                        if (prep_map.TryGetValue(prop_strs[3], out add_value))
                        {
                            add_value = int.Parse(prop_strs[2]) - add_value;
                        }
                        else
                        {
                            add_value = int.Parse(prop_strs[2]);
                        }
                        prop_list.Add(string.Format("{0}+{1}", prop_config.GetDispPropName(prop_strs[3]), prop_strs[1] == "1" ? (add_value/10f+"%"):add_value.ToString()));
                        prep_map[prop_strs[3]] = int.Parse(prop_strs[2]);
                    }
                    str += string.Format("<color='{2}'>{0}件：{1}</color>\n", suit_add.Parts, prop_list.ToStr<string>(" "), set_part_count >= suit_add.Parts ? "#FDD79B" : "#999999");
                }
                m_suitProp.text = str;
        }

    }


    void UpdateChosenInfo(CDItemData item)
    {
        if (item == null)
        {
            return;
        }

        m_selpart = item;
        if (item.info.Type == GameConst.ITEM_TYPE_MATERIAL)
        {
            m_btntxt.text = "鉴定";
        }
        else
        {
            m_btntxt.text = "镶嵌";
        }
        var cfg = ConfigManager.GetConfig<ConfigItemAccessories>().GetLine(item.sdata.item_id);
        m_chosenName.text = cfg.DispName;
        var info = item.info as ConfigItemAccessoriesLine;
        var quality = item.GetQuality();
        if (quality > 0)
        {
            m_chosenBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Strengthen, "item_frame_" + quality.ToString()));
        }
        else
        {
            m_chosenBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "kuang"));
        }
        var str = "";
        var prop_config = ConfigManager.GetConfig<ConfigPropDef>();
        //属性
        str = "";
        m_chosenIcon.SetSprite(ResourceManager.LoadIcon(item.info.Icon), item.info.SubType);
        var add_props = info.GetAddProp();
        for (int i = 0; add_props != null && i < add_props.Length; i++)
        {
            if (item.sdata.accessory_values != null && i < item.sdata.accessory_values.Length)
            {
                if (add_props[i] != null)
                {
                    var temp_value = Mathf.Abs(item.sdata.accessory_values[i]);
                    str += string.Format("{0}：+{1}", prop_config.GetDispPropName(info.DispProps[i]), add_props[i].AddType == 1 ? (temp_value / 10f + "%") : temp_value.ToString());
                }
                else
                {
                    str += info.AddProps[i] + "：unknow";
                }
            }
            else
            {
                str += "???：???";
            }
            str += i % 2 == 1 ? "\n" : " ";
        }
        if (add_props.Length == 0)
        {
            str += "";
        }
        m_chosenProp.text = str;

        str = "";
        var suit_config = ConfigManager.GetConfig<ConfigAccessorySuit>();
        var set_part_count = 0;
        var item_id = item.extend is KeyValue<int, int> ? (item.extend as KeyValue<int, int>).key : 0;

        var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(item_id);

        if (accessoryList != null && accessoryList.Length > 0)
        {
            foreach (var spart in accessoryList)
            {
                var tpart = ItemDataManager.GetItem(spart.item_id);
                if (tpart is ConfigItemAccessoriesLine)
                {
                    set_part_count += (tpart as ConfigItemAccessoriesLine).Suit == info.Suit ? 1 : 0;
                }
            }
        }

        var suit_group = info.Suit > 0 ? suit_config.GetSuitGroupAddPropData(info.Suit) : null;

        if (suit_group != null && suit_group.Length > 0)
        {
            str += (!string.IsNullOrEmpty(suit_group[0].Desc) ? suit_group[0].Desc : "未命名套装") + "\n";
            var prop_list = new List<string>();
            var prep_map = new Dictionary<string, int>();
            foreach (var suit_add in suit_group)
            {
                prop_list.Clear();
                for (int i = 0; suit_add.AddProps != null && i < suit_add.AddProps.Length; i++)
                {
                    var prop_strs = suit_add.AddProps[i].Split('#');
                    var add_value = 0;
                    if (prep_map.TryGetValue(prop_strs[3], out add_value))
                    {
                        add_value = int.Parse(prop_strs[2]) - add_value;
                    }
                    else
                    {
                        add_value = int.Parse(prop_strs[2]);
                    }
                    prop_list.Add(string.Format("{0}+{1}", prop_config.GetDispPropName(prop_strs[3]), prop_strs[1] == "1" ? (add_value / 10f + "%") : add_value.ToString()));
                    prep_map[prop_strs[3]] = int.Parse(prop_strs[2]);
                }
                str += string.Format("<color='{2}'>{0}件：{1}</color>\n", suit_add.Parts, prop_list.ToStr<string>(" "), set_part_count >= suit_add.Parts ? "#FDD79B" : "#999999");
            }
            m_chosensuitProp.text = str;
        }
        else
        {
            str = "";
            m_chosensuitProp.text = str;
        }

    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {
        
    }

    public override void Update()
    {
        
    }

    private void OnPartListItemClick(object renderData)
    {
       
        var vo = renderData as CDItemData;
        m_selpart = vo;
        if (opmode == 1)
        {
            foreach (UIItemRender render in _part_dg.ItemRenders)
            {
                if (render != null && render.m_renderData == vo)
                {
                    render.selected = !vo.selected;
                    return;
                }
            }
        }
        else if (opmode == 2)
        {
            if (vo.info != null && vo.info.Type == GameConst.ITEM_TYPE_MATERIAL)
            {
                foreach (UIItemRender render in _part_dg.ItemRenders)
                {
                    if (render != null && render.m_renderData == vo)
                    {
                        render.selected = !vo.selected;
                        return;
                    }
                }
            }
        }

        if (vo == null)
        {
            m_noneChosen.TrySetActive(true);
            m_Chosen.TrySetActive(false);
            return;
        }
        m_noneChosen.TrySetActive(false);
        m_Chosen.TrySetActive(true);
        UpdateChosenInfo(vo);
    }

    private void Toc_player_buyback_weapon_accessory(toc_player_buyback_weapon_accessory data)
    {
        UpdatePartsList();
        _part_dg.Select(0);
        opmode = 0;
        m_btnjian.SetActive(true);
        m_btnsell.SetActive(true);
        m_mask.TrySetActive(false);

    }
    private void Toc_player_test_accessory_meterial(toc_player_test_accessory_meterial data)
    {
        UpdatePartsList();
        _part_dg.Select(0);
        if (data.accessory_list.Length == 1)
        {
            for (int i = 0; i < _part_dg.Data.Length; i++)
            {
                var inf = _part_dg.Data[i] as CDItemData;
                if (inf.sdata.item_id == data.accessory_list[0].item_id)
                {
                    _part_dg.Select(i);
                }
            }
        }
        opmode = 0;
        m_btnjian.SetActive(true);
        m_btnsell.SetActive(true);
        m_mask.TrySetActive(false);
    }
    private void Toc_player_equip_weapon_accessory(toc_player_equip_weapon_accessory data)
    {
        UpdateEquiped();
        UpdatePartsList();
        _part_dg.Select(0);
        OnSetPartListItemClick(_set_part_list[selid - 1].gameObject, null);
        
    }
    private void OnPartItemFunHandler(CDItemData data)
    {

        var info = data != null && data.info != null ? data.info as ConfigItemAccessoriesLine : null;
        if (info == null)
        {
            return;
        }
        if (info.Type == GameConst.ITEM_TYPE_MATERIAL)
        {//鉴定
            UIManager.ShowTipPanel(string.Format("本次鉴定需要{0}金币？", ConfigMisc.GetInt("accessory_meterial_test_cost")), () => { StrengthenModel.Instance.TosTestPartMeterial(new int[1] { data.sdata.uid }); }, null, true, true);
        }
        else
        {
            if (_sdata != null)
            {
                if (data.sdata.uid > 0)
                {//镶嵌
                    var need_replace = false;
                    var accessoryList = StrengthenModel.Instance.GetAccessoryListByWeaponId(_sdata.item_id);
                    if (accessoryList != null && accessoryList.Length > 0)
                    {
                        foreach (var spart in accessoryList)
                        {
                            if (spart.pos == selid )
                            {
                                need_replace = true;
                                break;
                            }
                        }
                    }
                    if (need_replace == false)
                    {
                        StrengthenModel.Instance.TosEquipWeaponPart(_sdata.item_id, data.sdata.uid, selid);
                    }
                    else
                    {
                        UIManager.ShowTipPanel("已镶嵌的配件将消失，是否镶嵌", () => { StrengthenModel.Instance.TosEquipWeaponPart(_sdata.item_id, data.sdata.uid, selid); }, null, true, true);
                    }
                }
            }
            else
            {
                TipsManager.Instance.showTips("你尚未拥有该枪械");
            }
        }
    }

    private void OnPartItemFunHandlerRemove(CDItemData data)
    {
        var info = data != null && data.info != null ? data.info as ConfigItemAccessoriesLine : null;
        if (info == null)
        {
            return;
        }
        if (info.Type == GameConst.ITEM_TYPE_MATERIAL)
        {//鉴定
            UIManager.ShowTipPanel(string.Format("本次鉴定需要{0}金币？", ConfigMisc.GetInt("accessory_meterial_test_cost")), () => { StrengthenModel.Instance.TosTestPartMeterial(new int[1] { data.sdata.uid }); }, null, true, true);
        }
        else
        {
            if (_sdata != null)
            {
                if (data.extend is KeyValue<int, int>)
                {//拆除
                    var cost = ConfigManager.GetConfig<ConfigAccessoryCost>().GetLine(data.GetQuality());
                    UIManager.ShowTipPanel(string.Format("要消耗{0}金币进行拆除吗？", cost != null ? cost.UnequipCoin.ToString() : "???"), () => { StrengthenModel.Instance.TosUnEquipWeaponPart(_sdata.item_id, (data.extend as KeyValue<int, int>).value); }, null, true, true);
                }
            }
            else
            {
                TipsManager.Instance.showTips("你尚未拥有该枪械");
            }
        }
    }

    private void Toc_player_unequip_weapon_accessory(toc_player_unequip_weapon_accessory data)
    {
        UpdateEquiped();
        OnSetPartListItemClick(_set_part_list[selid - 1].gameObject, null);
        UpdatePartsList();
        _part_dg.Select(0);
    }

    private void OnPartTabClick(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowDropList(_part_type_data, OnUpdatePartListData, m_tran, target, 120, 35);
    }

    private void OnUpdatePartListData(ItemDropInfo data = null)
    {
        type = data.type;
        m_type.text = data.label;
        UpdatePartsList();
        _part_dg.Select(0);

    }
}

