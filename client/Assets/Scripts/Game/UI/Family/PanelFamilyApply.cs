﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelFamilyApply : BasePanel
{
    private Text _content;
    private UIEffect _headEffect;
    private Toggle _ignoreToggle;
    private Text _remainSecTxt;
    private float _remainTime;
    private int _timerId;
    private Image m_armylvl;
    private Text m_goldace;
    private Image m_headVip;
    private Image m_headicon;
    private Text m_lvl;
    private Text m_mvp;
    private Text m_name;
    private Image m_sexImg;
    private Text m_sliverace;

    public PanelFamilyApply()
    {
        SetPanelPrefabPath("UI/Family/PanelFamilyApply");
        AddPreLoadRes("UI/Family/PanelFamilyApply", EResType.UI);

        //            AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/Family", EResType.Atlas);
    }


    public override void Init()
    {
        _ignoreToggle = m_tran.Find("ignore").GetComponent<Toggle>();
        _content = m_tran.Find("content").GetComponent<Text>();

        Transform tran = m_tran.Find("target_info");
        m_headicon = tran.Find("headframe/head").GetComponent<Image>();
        m_headVip = tran.Find("headframe/heroVip").GetComponent<Image>();
        m_armylvl = tran.Find("level/imgArmy").GetComponent<Image>();
        m_lvl = tran.Find("level/Text").GetComponent<Text>();
        m_name = tran.Find("namebk/name").GetComponent<Text>();
        m_mvp = tran.Find("recordinfo/achieve/Text").GetComponent<Text>();
        m_goldace = tran.Find("recordinfo/goldace/Text").GetComponent<Text>();
        m_sliverace = tran.Find("recordinfo/sliverace/Text").GetComponent<Text>();
        m_sexImg = tran.Find("sex").GetComponent<Image>();
        _remainSecTxt = m_tran.Find("warning/remain_sec").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("BtnCancel")).onPointerClick += OnClickCancel;
        UGUIClickHandler.Get(m_tran.Find("BtnConfirm")).onPointerClick += OnClickConfirm;
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnClickCancel(GameObject target, PointerEventData eventData)
    {
        toc_family_family_apply_info applyInfo = FamilyDataManager.Instance.ApplyInfo;
        FamilyDataManager.Instance.ApplyInfo = null;
        if (_ignoreToggle.isOn)
        {
            FamilyDataManager.Instance.AddIgnoreList(applyInfo.id);
        }
        NetLayer.Send(new tos_family_agree_family {id = applyInfo.id, agree = false});
        HidePanel();
    }

    private void OnClickConfirm(GameObject target, PointerEventData eventData)
    {
        toc_family_family_apply_info applyInfo = FamilyDataManager.Instance.ApplyInfo;
        FamilyDataManager.Instance.ApplyInfo = null;
        NetLayer.Send(new tos_family_agree_family {id = applyInfo.id, agree = true});
        HidePanel();
    }

    public override void OnShow()
    {
        _ignoreToggle.isOn = false;
        toc_family_family_apply_info applyInfo = FamilyDataManager.Instance.ApplyInfo;
        _remainTime = 60f;
        _timerId = TimerManager.SetTimeOut(60f, () =>
        {
            if (UIManager.IsOpen<PanelFamilyApply>())
            {
                OnClickCancel(null, null);
            }
        });
        m_headicon.SetSprite(ResourceManager.LoadRoleIcon(applyInfo.data.icon));
        m_armylvl.SetSprite(ResourceManager.LoadArmyIcon(applyInfo.data.level));
        m_lvl.text = string.Format("{0}级", applyInfo.data.level);
        m_name.text = PlayerSystem.GetRoleNameFull(applyInfo.data.name, applyInfo.data.id);
        EnumHeroCardType heroType = HeroCardDataManager.getHeroCardTypeByCardInfo(applyInfo.data.hero_cards);
        m_headVip.gameObject.TrySetActive(false);
        if (heroType != (int) EnumHeroCardType.none)
        {
            m_headVip.gameObject.TrySetActive(true);
            if (heroType == EnumHeroCardType.normal)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (heroType == EnumHeroCardType.super)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (heroType == EnumHeroCardType.legend)
            {
                m_headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (_headEffect == null)
                {
                    _headEffect = UIEffect.ShowEffect(m_headVip.transform, EffectConst.UI_HEAD_HERO_CARD);
                }
            }
        }

        m_goldace.text = applyInfo.data.gold_ace.ToString();
        m_sliverace.text = applyInfo.data.silver_ace.ToString();
        m_mvp.text = applyInfo.data.mvp.ToString();
        if (applyInfo.data.sex == 0)
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "male"));
        }
        else
        {
            m_sexImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "female"));
        }

        _content.text = "<color='FFAE00'>" + applyInfo.data.name + "</color>邀请你成为他的家族成员,是否接受?\n接受邀请后,他将成为你的<color='05EEEE'>" + applyInfo.from_relative +
                        "</color>,你将成为他的<color='05EEEE'>" + applyInfo.to_relative + "</color>";
    }

    public override void OnHide()
    {
        TimerManager.RemoveTimeOut(_timerId);
        _timerId = 0;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        _remainTime -= Time.deltaTime;
        if (_remainTime > 0)
        {
            _remainSecTxt.text = Mathf.Ceil(_remainTime).ToString();
        }
    }
}