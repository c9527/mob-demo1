﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FamilyOption
{
    public const string Del = "删除";
    public const string LookUp = "查看";
    public const string Follow = "跟随";
    public const string Chat = "私聊";
}

class PanelFamily : BasePanel
{
    public const int MEMBER_COUNT = 8;
    private ItemDropInfo[] _familyDropOption;
    private FamilyMember[] _familyMembers;
    private Image _selfHeadIcon;
    private Text _selfName;

    public GameObject dropTarget;

    public PanelFamily()
    {
        SetPanelPrefabPath("UI/Family/PanelFamily");
        AddPreLoadRes("UI/Family/PanelFamily", EResType.UI);

//            AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/Family", EResType.Atlas);
    }

    public ItemDropInfo[] FamilyDropOption
    {
        get { return _familyDropOption; }
    }


    public override void Init()
    {
        _selfName = m_tran.Find("list/self/Name/Label").GetComponent<Text>();
        _selfHeadIcon = m_tran.Find("list/self/Head").GetComponent<Image>();
        _familyMembers = new FamilyMember[MEMBER_COUNT];
        dropTarget = m_tran.Find("dropTarget").gameObject;

        for (int i = 0; i < MEMBER_COUNT; i++)
        {
            _familyMembers[i] = new FamilyMember(m_tran.Find("list/member_" + i).gameObject, this);
        }

        _familyDropOption = new[]
        {
            new ItemDropInfo {type = "Family", subtype = FamilyOption.Del, label = FamilyOption.Del},
            new ItemDropInfo {type = "Family", subtype = FamilyOption.LookUp, label = FamilyOption.LookUp},
            new ItemDropInfo {type = "Family", subtype = FamilyOption.Follow, label = FamilyOption.Follow},
            new ItemDropInfo {type = "Family", subtype = FamilyOption.Chat, label = FamilyOption.Chat}
        };
    }

    public override void InitEvent()
    {
        for (int i = 0; i < MEMBER_COUNT; i++)
        {
            _familyMembers[i].InitEvent();
        }
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
        UGUIClickHandler.Get(m_tran.Find("BtnRule")).onPointerClick += delegate { UIManager.PopRulePanel("Family"); };
    }

    public override void OnShow()
    {
        dropTarget.TrySetActive(false);

        RoleData roleData = PlayerSystem.roleData;
        _selfHeadIcon.SetSprite(ResourceManager.LoadRoleIconBig(roleData.icon));
        _selfName.text = roleData.name;
        List<p_family_data> familyDataList = FamilyDataManager.Instance.FamilyDataDic.Values.ToList();
        var availSlot =  FamilyDataManager.Instance.OpenSlotNum;
        for (int i = 0, dataCount = familyDataList.Count; i < availSlot; i++)
        {
            if (i >= dataCount)
            {
                _familyMembers[i].SetData(null);
            }
            else
            {
                _familyMembers[i].SetData(familyDataList[i]);
            }
        }
        for (int i = availSlot; i < MEMBER_COUNT; i++)
        {
            _familyMembers[i].Lock();
        }
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelSocity>();
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    private void Toc_family_open_family_pos(toc_family_open_family_pos msg)
    {
        _familyMembers[FamilyDataManager.Instance.OpenSlotNum++].UnLock();
    }
}

class FamilyMember
{
    private readonly GameObject _addIcon;
    private readonly GameObject _go;
    private readonly Image _headIcon;
    private readonly GameObject _nameGo;
    private readonly Text _nameTxt;
    private readonly PanelFamily _parentPanel;
    private readonly GameObject _relativeGo;
    private readonly Text _relativeTxt;
    private p_family_data _data;
    private GameObject _intimate;
    private Text _intimateValue;
    private bool _lock;
    private Transform _lockTran;
    private GameObject _upgradeIntimateBtn;


    public FamilyMember(GameObject go, BasePanel parentPanel)
    {
        _go = go;
        _parentPanel = (PanelFamily) parentPanel;
        Transform tran = _go.transform;
        _nameGo = tran.Find("Name").gameObject;
        _nameTxt = tran.Find("Name/Label").GetComponent<Text>();
        _headIcon = tran.Find("Head").GetComponent<Image>();
        _relativeGo = tran.Find("Relative").gameObject;
        _relativeTxt = tran.Find("Relative/Text").GetComponent<Text>();
        _addIcon = tran.Find("add").gameObject;
        _intimate = tran.Find("Intimate").gameObject;
        _intimateValue = tran.Find("Intimate/Text").GetComponent<Text>();
        _lockTran = tran.Find("lock");
        _upgradeIntimateBtn = tran.Find("Intimate/upgradeBtn").gameObject;
        _lock = false;

    }

    public void SetData(p_family_data data)
    {
        _data = data;
        UpdateView();
    }

  

    private void UpdateView()
    {
        if (_data == null)
        {
            _nameGo.TrySetActive(false);
            _relativeGo.TrySetActive(false);
            _addIcon.TrySetActive(!_lock);
            _lockTran.gameObject.TrySetActive(_lock);
            _headIcon.gameObject.TrySetActive(false);
            _intimate.TrySetActive(false);
        }
        else
        {
            _lockTran.gameObject.TrySetActive(false);
            _addIcon.TrySetActive(false);
            _nameTxt.text = _data.data.name;
            _nameGo.TrySetActive(true);
            _relativeTxt.text = FamilyDataManager.Instance.GetRelativeName(_data);
            _relativeGo.TrySetActive(true);
            _headIcon.SetSprite(ResourceManager.LoadRoleIconBig(_data.data.icon));
            _intimate.TrySetActive(true);
            _intimateValue.text = _data.value + "/" + FamilyDataManager.MaxRelativeValue;
            _headIcon.gameObject.TrySetActive(true);
            _upgradeIntimateBtn.TrySetActive(_data.value < FamilyDataManager.MaxRelativeValue);
        }
    }

    public void InitEvent()
    {
        UGUIClickHandler.Get(_go).onPointerClick += OnClickMember;
        UGUIClickHandler.Get(_upgradeIntimateBtn).onPointerClick += OnClickUpgradeIntimate;
    }

    private void OnClickMember(GameObject target, PointerEventData eventData)
    {
        if (_lock)
        {
            var costStr = ConfigMisc.GetValue("open_family_pos_cost");
            string[] costItem = costStr.Split(';');
            var currentOpenPosIndex = FamilyDataManager.Instance.OpenSlotNum - 4;
            var itemStr= costItem[currentOpenPosIndex].Split('#');
            var itemName = ItemDataManager.GetItem(int.Parse(itemStr[0])).DispName;
            var costCount = int.Parse(itemStr[1]);
            UIManager.ShowTipPanel("是否花费" + itemName+"<COLOR=FFAE00>"+costCount+ "</COLOR>开启家族成员位置?",
                () => NetLayer.Send(new tos_family_open_family_pos() { pos = currentOpenPosIndex + 1 }));
            return;
        }
        if (_data == null)
        {
            UIManager.PopPanel<PanelFamilyInvite>(null, true, _parentPanel);
        }
      
        else
        {
             _parentPanel.dropTarget.TrySetActive(true);
            var dir = UIManager.Direction.Down;
            if (target.GetRectTransform().anchoredPosition.y < -100)
            {
                dir = UIManager.Direction.Up;
            }
            UIManager.ShowDropList(_parentPanel.FamilyDropOption, ItemClickHandler, _parentPanel.transform,
                target.transform, Vector2.zero, dir, 132, 45, UIManager.DropListPattern.Pop);
        }
       
    }


    private void OnClickUpgradeIntimate(GameObject target, PointerEventData eventData)
    {
        if (_lock ||  _data.value >= FamilyDataManager.MaxRelativeValue)
        {
            return;
        }
        var costItem = ConfigMisc.GetItemInfo("family_upgrade_cost");
        var itemName = ItemDataManager.GetItem(costItem.ID).DispName;
        UIManager.ShowTipPanel(String.Format("是否花费{0}{1}将亲密度提升至100?", itemName, costItem.cnt),
            () => NetLayer.Send(new tos_family_add_family_relation() {id = _data.data.id}));
    }

    private void ItemClickHandler(ItemDropInfo info)
    {
        _parentPanel.dropTarget.TrySetActive(false);
        string subType = info.subtype;
        if (subType == FamilyOption.Chat)
            OnClickChat(null, null);
        else if (subType == FamilyOption.Del)
            OnClickDelete(null, null);
        else if (subType == FamilyOption.Follow)
            OnClickFollow(null, null);
        else if (subType == FamilyOption.LookUp)
            OnClickLookOver(null, null);
    }

    public void Lock()
    {
        _lock = true;
        SetData(null);
    }

    public void UnLock()
    {
        if (_lock)
        {
            _lock = false;
            SetData(null);
        }
    }

    private void OnClickChat(GameObject target, PointerEventData eventData)
    {
        p_friend_data data = _data.data;
        bool isFriend = PlayerFriendData.singleton.isFriend(data.id);
        var rcd = new PlayerRecord();
        rcd.m_iPlayerId = data.id;
        rcd.m_strName = data.name;
        rcd.m_iSex = data.sex;
        rcd.m_iLevel = data.level;
        rcd.m_bOnline = true;
        rcd.m_isFriend = isFriend;
        rcd.m_iHead = data.icon;
        var msgTips = new ChatMsgTips();
        msgTips.m_rcd = rcd;
        msgTips.m_eChannel = CHAT_CHANNEL.CC_PRIVATE;
        object[] chatParams = {msgTips};
        UIManager.PopChatPanel<PanelChatSmall>(chatParams, false, _parentPanel);
    }

    private void OnClickDelete(GameObject target, PointerEventData eventData)
    {
        UIManager.ShowTipPanel("您确定删除<Color='#996600'> " + _data.data.name + " </Color>家人吗？", SureDelete, null, false,
            true, "确定", "取消");
    }

    private void SureDelete()
    {
        FamilyDataManager.Instance.ReqDelFamily(_data.data.id);
    }

    private void OnClickFollow(GameObject target, PointerEventData eventData)
    {
        RoomModel.FollowFriend(_data.data.id, false, false);
    }

    private void OnClickLookOver(GameObject target, PointerEventData eventData)
    {
        PlayerFriendData.ViewPlayer(_data.data.id);
    }
}