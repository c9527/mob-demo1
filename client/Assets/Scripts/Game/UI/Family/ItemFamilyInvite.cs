﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 家族邀请界面列表item
/// </summary>
public class ItemFamilyInvite : ItemRender
{
    private static Dictionary<long, PlayerInviteRecord> _dicInviteRecord;
    private Text m_InviteTxt;
    private Vector2 m_anchorName;
    private Transform m_btnInvite;
    private PanelFamilyInvite.ListItemData m_data;
    private GameObject m_goName;
    private GameObject m_goVip;
    public int m_iUserData;
    //private GameObject m_goItem;

    private Image m_imageArmyLvl;
    private Image m_imageHead;

    private Image m_imgState;
    public PanelFamilyInvite m_parentPanel;
    private Text m_textName;
    private Text m_txtAce;
    private Text m_txtArmyLvl;
    private Text m_txtMvp;
    private Text m_txtState;

    private static Dictionary<long, PlayerInviteRecord> m_dicInviteRecord
    {
        get
        {
            if (_dicInviteRecord == null)
                _dicInviteRecord = new Dictionary<long, PlayerInviteRecord>();
            return _dicInviteRecord;
        }
    }

    private PlayerInviteRecord m_localRecord
    {
        get
        {
            if (m_dicInviteRecord.ContainsKey(m_data.m_iPlayerId))
            {
                return m_dicInviteRecord[m_data.m_iPlayerId];
            }
            return null;
        }
    }

    public override void Awake()
    {
        m_iUserData = 0;
        m_imageHead = transform.Find("headframe/head").GetComponent<Image>();
        m_imageArmyLvl = transform.Find("army").GetComponent<Image>();
        m_txtArmyLvl = transform.Find("lvl/Text").GetComponent<Text>();

        m_textName = transform.Find("name").GetComponent<Text>();
        m_goName = transform.Find("name").gameObject;
        m_goVip = transform.Find("vip").gameObject;
        m_anchorName = m_goName.GetComponent<RectTransform>().anchoredPosition;

        m_imgState = transform.Find("imgState").GetComponent<Image>();
        m_btnInvite = transform.Find("invite");
        m_txtState = transform.Find("txtState").GetComponent<Text>();
        m_txtMvp = transform.Find("mvp/Text").GetComponent<Text>();
        m_txtAce = transform.Find("ace/Text").GetComponent<Text>();
        m_parentPanel = UIManager.GetPanel<PanelFamilyInvite>();
        m_InviteTxt = transform.Find("invite/Text").GetComponent<Text>();
        UGUIClickHandler.Get(m_btnInvite).onPointerClick += OnClickInvite;
    }

    private void Update()
    {
        if (m_localRecord == null)
            return;
        if (m_localRecord.canInvite)
            return;
        UpdateBtnLabel();
    }

    private void UpdateBtnLabel()
    {
        if ((Time.realtimeSinceStartup - m_localRecord.lastInviteTime) < 60.0f)
        {
            float diff = Time.realtimeSinceStartup - m_localRecord.lastInviteTime;
            m_localRecord.lastTime = Time.realtimeSinceStartup;
            m_InviteTxt.text = (60 - Mathf.CeilToInt(diff)).ToString();
            return;
        }
        if (FamilyDataManager.Instance.IsFamilyMember(m_data.m_iPlayerId))
        {
            m_localRecord.canInvite = false;
            m_InviteTxt.text = "已是家族成员";
            Util.SetGrayShader(m_InviteTxt);
        }
        else
        {
            m_localRecord.canInvite = true;
            m_InviteTxt.text = "邀请";
            Util.SetNormalShader(m_InviteTxt);
        }
    }

    protected override void OnSetData(object data)
    {
        m_data = data as PanelFamilyInvite.ListItemData;
        Update();
        if (!m_dicInviteRecord.ContainsKey(m_data.m_iPlayerId))
        {
            var localRecord = new PlayerInviteRecord();
            m_dicInviteRecord.Add(m_data.m_iPlayerId, localRecord);
        }

        UpdateBtnLabel();

        m_textName.text = m_data.m_playerName;
        m_imgState.gameObject.TrySetActive(false);
        m_txtMvp.transform.parent.gameObject.TrySetActive(false);
        m_txtState.gameObject.TrySetActive(false);
        m_txtAce.transform.parent.gameObject.TrySetActive(false);
        m_imageHead.SetSprite(ResourceManager.LoadRoleIcon(m_data.m_icon));
        m_imageHead.SetNativeSize();

//        m_txtMvp.text = m_data.m_mvp.ToString();
//        m_txtAce.text = m_data.m_ace.ToString();

        m_txtArmyLvl.text = String.Format("{0}级",  m_data.m_armyLvl);
        m_imageArmyLvl.SetSprite(ResourceManager.LoadArmyIcon(m_data.m_armyLvl));
        m_imageArmyLvl.SetNativeSize();
        m_goVip.TrySetActive(false);
    }

    /// <summary>
    /// 邀请按钮回调
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnClickInvite(GameObject target, PointerEventData eventData)
    {
        if (m_localRecord != null && !m_localRecord.canInvite)
            return;
        m_localRecord.canInvite = false;
        m_localRecord.lastInviteTime = Time.realtimeSinceStartup;
        Util.SetGrayShader(m_InviteTxt);
//        TipsManager.Instance.showTips(string.Format("你已经向{0}发送了邀请", m_data.m_playerName));
//       Logger.Error(m_data.m_iPlayerId);        
        // pop add family panel
        if (FamilyDataManager.Instance.IsFamilyMember(m_data.m_iPlayerId))
        {
            TipsManager.Instance.showTips("已经是家族成员了");
            return;
        }
        if (!FamilyDataManager.IsReachFamilyOpenLvl(m_data.m_armyLvl))
        {
            TipsManager.Instance.showTips("该玩家等级不足，无法接受该邀请");
            return;
        }
        UIManager.PopPanel<PanelFamilyAdd>(new object[] {m_data.m_iPlayerId, m_data.m_playerName}, true,
            UIManager.GetPanel<PanelFamily>());
        if (m_parentPanel != null)
            m_parentPanel.OnInvited(m_data);
    }
}