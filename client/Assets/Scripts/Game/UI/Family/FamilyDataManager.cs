﻿using System;
using System.Collections.Generic;
/// <summary>
/// 家族数据类,单例
/// </summary>
sealed class FamilyDataManager
{
    private static FamilyDataManager _instance;
    private readonly Dictionary<long, p_family_data> _familyDataDic;
    private readonly HashSet<long> _ignoreIdList;

    public static int MaxRelativeValue = ConfigMisc.GetInt("family_max_relative_value");

    private toc_family_family_apply_info _applyInfo;

    public int OpenSlotNum { get; set; }

    private FamilyDataManager()
    {
        _familyDataDic = new Dictionary<long, p_family_data>();
        _ignoreIdList = new HashSet<long>();
        GameDispatcher.AddEventListener(GameEvent.UI_SETTLE_END, OnSettleEnd);
    }

    /// <summary>
    /// 家族成员字典
    /// </summary>
    public Dictionary<long, p_family_data> FamilyDataDic
    {
        get { return _familyDataDic; }
    }

    /// <summary>
    /// 当前家族申请信息
    /// </summary>
    public toc_family_family_apply_info ApplyInfo
    {
        get { return _applyInfo; }
        set { _applyInfo = value; }
    }

    /// <summary>
    /// 单例引用
    /// </summary>
    public static FamilyDataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FamilyDataManager();
            }
            return _instance;
        }
    }
    
    /// <summary>
    /// 向服务端请求家族成员列表
    /// </summary>
    public void ReqFamilyList()
    {
        NetLayer.Send(new tos_family_family_list());
    }

    /// <summary>
    /// 向服务端发送删除家族成员协议
    /// </summary>
    /// <param name="id"></param>
    public void ReqDelFamily(long id)
    {
        NetLayer.Send(new tos_family_del_family {id = id});
    }

   
    /// <summary>
    /// 删除家族成员
    /// </summary>
    /// <param name="id">玩家id</param>
    private void DelFamily(long id)
    {
//           Logger.Error("RemoveFamily");
        _familyDataDic.Remove(id);
    }

    /// <summary>
    /// 返回家人简称
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public string GetShortName(long id)
    {
        var cfg = ConfigManager.GetConfig<ConfigFamilyTitle>();
        var relativeLongName = GetRelativeName(id);
        var shortName = cfg.GetShortName(relativeLongName);
        if(String.IsNullOrEmpty(shortName))
        {
            return "家";
        }
        return shortName;
    }

    /// <summary>
    /// 加入黑名单
    /// </summary>
    /// <param name="id">黑名单玩家id</param>
    public void AddIgnoreList(long id)
    {
        _ignoreIdList.Add(id);
    }

    
    
    /// <summary>
    /// 添加家族成员
    /// </summary>
    /// <param name="addData"></param>
    private void AddFamily(p_family_data addData)
    {
//           Logger.Error("AddFamily");
        _familyDataDic[addData.data.id] = addData;
    }

    /// <summary>
    /// 判断是否家族成员
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public bool IsFamilyMember(long id)
    {
        return _familyDataDic.ContainsKey(id);
    }

    /// <summary>
    /// 获取家族成员昵称
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public string GetRelativeName(long id)
    {
        if (_familyDataDic.ContainsKey(id))
        {
            return GetRelativeName(_familyDataDic[id]);
        }
        return "";
    }

     public string GetRelativeName(p_family_data data)
    {
         if (data != null)
         {
             if (data.value >= MaxRelativeValue)
             {
                 return data.relative;
             }
             return "准成员";
         }
         return "";
    }

    /// <summary>
    /// 判断是否到达家族开启等级
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public static bool IsReachFamilyOpenLvl(int level)
    {
        var openLvl = ConfigMisc.GetInt("family_level");
        return level >= openLvl;
    }

    /// <summary>
    /// 处理服务端发来的家族成员列表
    /// </summary>
    /// <param name="list"></param>
    private static void Toc_family_family_list(toc_family_family_list list)
    {
        //           Logger.Error("Toc_family_family_list");
        Instance._familyDataDic.Clear();
        Instance.OpenSlotNum = list.max_family_cnt;
        foreach (p_family_data familyItem in list.family_list)
        {
            Instance._familyDataDic.Add(familyItem.data.id, familyItem);
        }
//
//        PlayerFriendData.singleton.FlushFamilyList();
    }

    /// <summary>
    /// 处理更新家族成员协议
    /// </summary>
    /// <param name="updateData"></param>
    private static void Toc_family_update_family(toc_family_update_family updateData)
    {
        if (updateData.del)
        {
            Instance.DelFamily(updateData.family_data.data.id);
        }
        else
        {
            Instance.AddFamily(updateData.family_data);
        }
//        PlayerFriendData.singleton.FlushFamilyList();
        // panel update
        if (UIManager.IsOpen<PanelFamily>())
        {
            UIManager.GetPanel<PanelFamily>().OnShow();
        }
    }


    /// <summary>
    /// 处理申请成为家族成员的协议
    /// </summary>
    /// <param name="applyInfo"></param>
    private static void Toc_family_family_apply_info(toc_family_family_apply_info applyInfo)
    {
        Instance._applyInfo = applyInfo;

        if (!WorldManager.singleton.fightEntered)
        {
            // pop panel and clear apply info
//               Logger.Error("apply panel pop");
            if (!Instance._ignoreIdList.Contains(applyInfo.id))
            {
                UIManager.PopPanel<PanelFamilyApply>(null, true);
            }
            else
            {
                Instance._applyInfo = null;
                NetLayer.Send(new tos_family_agree_family {id = applyInfo.id, agree = false});
            }
        }
    }

    /// <summary>
    /// 战斗结束后处理玩家家族申请
    /// </summary>
    private void OnSettleEnd()
    {
        if (Instance._applyInfo != null)
        {
            // pop panel and clear apply info
//               Logger.Error("apply panel pop");
            UIManager.PopPanel<PanelFamilyApply>(null, true);
        }
    }
}