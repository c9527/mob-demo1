﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelFamilyInvite : BasePanel
{
    public delegate void SelectCallBack(ListItemData selectData);

    public static readonly int LISTTYPE_FRIEND = 1;
    public static readonly int LISTTYPE_MEMBER = 2;

    private static readonly List<ListItemData> m_frinedList = new List<ListItemData>();
    private static readonly List<ListItemData> m_corpsList = new List<ListItemData>();
    private static float _INVITE_LIMIT = 60;
    private static float _LAST_INVITE_TIME;

    private Image _cd_mask;
    private Image _cost;
    private Button _invite_btn;
    private int _list_value;
    private Text _num;
    private List<Toggle> _tab_group;
    private string coin;
    private int cost;
    private Transform m_btnClose;
    private DataGrid m_dataGrid;
    private Transform m_goContentRect;
    private GameObject m_goFind;
    private GameObject m_goNoCorpsTips;
    private GameObject m_goNoFriendsTips;
    private InputField m_inputTxt;
    private Toggle m_tabFindList;
    private Toggle m_tabFrinedList;
    private Toggle m_tabTeamList;
    private GameObject m_toggleCorpsList;
    private GameObject m_toggleFind;
    private GameObject m_toggleFriendList;

    public PanelFamilyInvite()
    {
        SetPanelPrefabPath("UI/Family/PanelFamilyInvite");
        AddPreLoadRes("UI/Family/PanelFamilyInvite", EResType.UI);
        AddPreLoadRes("UI/Room/ItemInvite", EResType.UI);
        AddPreLoadRes("Atlas/Friend", EResType.Atlas);
    }


    public override void Init()
    {
        //m_listInvite = new List<InviteItem>();
        m_toggleFriendList = m_go.transform.Find("frame/tabbar/tabfriendlist").gameObject;
        m_tabFrinedList = m_toggleFriendList.GetComponent<Toggle>();

        m_toggleCorpsList = m_go.transform.Find("frame/tabbar/tabcorpsmate").gameObject;
        m_tabTeamList = m_toggleCorpsList.GetComponent<Toggle>();

        m_toggleFind = m_tran.Find("frame/tabbar/tabfind").gameObject;
        m_tabFindList = m_toggleFind.GetComponent<Toggle>();

        m_goFind = m_tran.Find("frame/find").gameObject;

        _tab_group = new List<Toggle>();
        _tab_group.Add(m_tabFrinedList);
        _tab_group.Add(m_tabTeamList);
        _tab_group.Add(m_tabFindList);

        m_inputTxt = m_tran.Find("frame/find/InputField").GetComponent<InputField>();
        m_inputTxt.gameObject.AddComponent<InputFieldFix>();

        m_btnClose = m_go.transform.Find("frame/close");

        m_goNoFriendsTips = m_tran.Find("frame/ScrollDataGrid/friendtips").gameObject;
        m_goNoCorpsTips = m_tran.Find("frame/ScrollDataGrid/corpstips").gameObject;
        m_tabFrinedList.isOn = true;

        m_dataGrid = m_tran.Find("frame/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Room/ItemInvite"), typeof (ItemFamilyInvite));


        //OnClickShowFriendList(null, null);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btnClose).onPointerClick += OnClickClose;
        UGUIClickHandler.Get(m_toggleFriendList).onPointerClick += OnClickFriend;
        UGUIClickHandler.Get(m_toggleCorpsList).onPointerClick += OnClickCorps;
        UGUIClickHandler.Get(m_toggleFind).onPointerClick += OnClickFind;
        UGUIClickHandler.Get(m_tran.Find("frame/find/add")).onPointerClick += OnClickAdd;
//            UGUIClickHandler.Get(_invite_btn.gameObject).onPointerClick += OnInviteBtnClick;
    }


    public override void OnShow()
    {
        UpdateList();
        UpdateView();
    }

    private void UpdateList()
    {
        m_frinedList.Clear();
        m_corpsList.Clear();
        List<FriendData> friendList = PlayerFriendData.singleton.friendList;
        for (int i = 0; i < friendList.Count; ++i)
        {
            var newItem = new ListItemData();
            newItem.m_iPlayerId = friendList[i].m_id;
            newItem.m_playerName = friendList[i].m_name;
            newItem.m_bInFight = false;
            newItem.m_armyLvl = friendList[i].m_level;
            newItem.m_icon = friendList[i].m_icon;
//                newItem.m_isOnline = friendList[i].m_state> 0;
            newItem.m_ace = friendList[i].m_goldace;
            newItem.m_eType = (MemberManager.MEMBER_TYPE) friendList[i].m_vip_level;
            if (!FamilyDataManager.Instance.IsFamilyMember(friendList[i].m_id))
            {
                m_frinedList.Add(newItem);
            }
        }

        // 处理玩家战队人员
        List<p_corps_member_toc> corpsList = null;
        if (CorpsDataManager.IsJoinCorps())
        {
            corpsList = CorpsDataManager.m_lstMember;
            for (int i = 0; i < corpsList.Count; ++i)
            {
                var newItem = new ListItemData();
                newItem.m_iPlayerId = corpsList[i].id;
                newItem.m_playerName = corpsList[i].name;
                newItem.m_bInFight = false;
                newItem.m_armyLvl = corpsList[i].level;
                newItem.m_icon = corpsList[i].icon;
                newItem.m_isOnline = corpsList[i].handle > 0;
                newItem.m_ace = 0;

                newItem.m_eType = (MemberManager.MEMBER_TYPE) corpsList[i].vip_level;
                if (!FamilyDataManager.Instance.IsFamilyMember(corpsList[i].id) &&
                    corpsList[i].id != PlayerSystem.roleId)
                {
                    m_corpsList.Add(newItem);
                }
            }
        }
    }

    public override void OnHide()
    {
        foreach (Toggle toggle in _tab_group)
        {
            toggle.isOn = false;
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        m_frinedList.Clear();
        m_corpsList.Clear();
    }

    public override void Update()
    {
    }

    private void OnClickClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnClickFriend(GameObject target, PointerEventData eventData)
    {
        UpdateView();
    }

    private void OnClickFind(GameObject target, PointerEventData eventData)
    {
        UpdateView();
    }

    private void OnClickCorps(GameObject target, PointerEventData eventData)
    {
        UpdateView();
    }

    public void OnInvited(ListItemData item)
    {
        List<ListItemData> lstItem = null;
        if (m_tabFrinedList.isOn)
            lstItem = m_frinedList;
        else
            lstItem = m_corpsList;

//        for (int i = 0; i < lstItem.Count; ++i)
//        {
//            if (lstItem[i].m_iPlayerId == item.m_iPlayerId)
//            {
//                lstItem.RemoveAt(i);
//                break;
//            }
//        }

        UpdateView();
    }

    public void UpdateView()
    {
        m_goFind.TrySetActive(false);
        m_goNoFriendsTips.TrySetActive(false);
        m_goNoCorpsTips.TrySetActive(false);
        m_tabFrinedList.gameObject.TrySetActive(_list_value == 0 || (_list_value & LISTTYPE_FRIEND) > 0);
        m_tabTeamList.gameObject.TrySetActive(_list_value == 0 || (_list_value & LISTTYPE_MEMBER) > 0);
        Toggle selected_toggle = null;
        foreach (Toggle toggle in _tab_group)
        {
            if (toggle.IsActive())
            {
                if (selected_toggle == null || toggle.isOn)
                {
                    selected_toggle = toggle;
                }
            }
            else
            {
                toggle.isOn = false;
            }
        }

        if (selected_toggle != null && selected_toggle.isOn == false)
        {
            selected_toggle.isOn = true;
        }
        if (m_tabFrinedList.isOn)
        {
            m_dataGrid.Data = m_frinedList.ToArray();
            m_dataGrid.transform.parent.gameObject.TrySetActive(true);
            if (m_frinedList.Count == 0)
                m_goNoFriendsTips.TrySetActive(true);
        }
        else if (m_tabTeamList.isOn)
        {
            m_dataGrid.Data = m_corpsList.ToArray();
            m_dataGrid.transform.parent.gameObject.TrySetActive(true);
            if (m_corpsList.Count == 0)
                m_goNoCorpsTips.TrySetActive(true);
        }
        else
        {
            m_dataGrid.transform.parent.gameObject.TrySetActive(false);
            m_goFind.TrySetActive(true);
        }
    }

    private void OnClickAdd(GameObject target, PointerEventData eventData)
    {
        string nameorId = m_inputTxt.text;

        long isId;
        if (long.TryParse(nameorId, out isId))
            PlayerFriendData.ViewPlayer(isId);
        else
            PlayerFriendData.ViewPlayer(nameorId);
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (FamilyDataManager.Instance.IsFamilyMember(data.id))
        {
            TipsManager.Instance.showTips("已经是家族成员了");
            return;
        }

        UIManager.GetPanel<PanelMainPlayerInfo>().HidePanel();
        //判断是否到达等级
        if (!FamilyDataManager.IsReachFamilyOpenLvl(data.level))
        {
            TipsManager.Instance.showTips("该玩家等级不足，无法接受该邀请");
            return;
        }
        if (!UIManager.IsOpen<PanelFamilyAdd>())
        {
            HidePanel();
            UIManager.PopPanel<PanelFamilyAdd>(new object[] {data.id, data.name}, true,
                UIManager.GetPanel<PanelFamily>());
        }
    }

    public class ListItemData
    {
        public int m_ace;
        public int m_armyLvl;
        public bool m_bInFight;
        public bool m_bInRoom;
        public MemberManager.MEMBER_TYPE m_eType;
        public long m_iPlayerId;
        public int m_icon;
        public bool m_isOnline;
        public int m_mvp;
        public string m_playerName;
    }
}