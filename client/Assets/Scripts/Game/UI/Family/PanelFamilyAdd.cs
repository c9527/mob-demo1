﻿using System;
using ComponentAce.Compression.Libs.zlib;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelFamilyAdd : BasePanel
{
    private GameObject _btnSwap;
    private int _currentRelationship;
    private InputField _myRelativeInput;
    private Text _myRelativeName;

    private long _playerId;
    private string _playerName;


    private ItemDropInfo[] _relationDropOption;
    private Text _relativeNameTxt;
    private Text _targetNameTxt;
    private InputField _targetRelativeInput;
    private Text _targetRelativeName;
    public GameObject dropTarget;

    public PanelFamilyAdd()
    {
        SetPanelPrefabPath("UI/Family/PanelFamilyAdd");
        AddPreLoadRes("UI/Family/PanelFamilyAdd", EResType.UI);

        //            AddPreLoadRes("Atlas/RoleIcon", EResType.Atlas);
        AddPreLoadRes("Atlas/Family", EResType.Atlas);
    }


    public override void Init()
    {
        _targetNameTxt = m_tran.Find("target/name").GetComponent<Text>();
        _relativeNameTxt = m_tran.Find("option/btnOption/relative").GetComponent<Text>();
        _myRelativeName = m_tran.Find("myrelative/Text").GetComponent<Text>();
        _targetRelativeName = m_tran.Find("target_relative/Text").GetComponent<Text>();
        _btnSwap = m_tran.Find("BtnSwap").gameObject;
        _myRelativeInput = m_tran.Find("myrelative/InputField").GetComponent<InputField>();
        m_tran.Find("myrelative/InputField").gameObject.AddMissingComponent<InputFieldFix>();
        _targetRelativeInput = m_tran.Find("target_relative/InputField").GetComponent<InputField>();
        m_tran.Find("target_relative/InputField").gameObject.AddMissingComponent<InputFieldFix>();
        dropTarget = m_tran.Find("dropTarget").gameObject;


        var cfg = ConfigManager.GetConfig<ConfigFamilyTitle>();

        _relationDropOption = new ItemDropInfo[cfg.m_dataArr.Length + 1];
        for (int i = 0; i < cfg.m_dataArr.Length; i++)
        {
            var item = new ItemDropInfo();
            item.subtype = cfg.m_dataArr[i].ID.ToString();
            item.type = "Relationship";
            item.label = cfg.m_dataArr[i].Name;
            _relationDropOption[i] = item;
        }
        _relationDropOption[_relationDropOption.Length - 1] = new ItemDropInfo
        {
            type = "Relationship",
            subtype = ((int) Relationship.DIY).ToString(),
            label = "自定义"
        };
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(_btnSwap).onPointerClick += SwapRelative;
        UGUIClickHandler.Get(m_tran.Find("BtnConfirm")).onPointerClick += OnClickConfirm;
        UGUIClickHandler.Get(m_tran.Find("option/btnOption")).onPointerClick += ShowDropList;
        UGUIClickHandler.Get(dropTarget).onPointerClick += OnDropClick;
    }

    public override void OnShow()
    {
        if (m_params.Length == 2)
        {
            _playerId = (long) m_params[0];
            _playerName = (string) m_params[1];
        }
        dropTarget.TrySetActive(false);
        _targetNameTxt.text = _playerName;
        _currentRelationship = (int) Relationship.START;
        ChangeRelativeByOption(((int) Relationship.START).ToString());
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void SwapRelative(GameObject target, PointerEventData eventData)
    {
        string tmp = _myRelativeName.text;
        _myRelativeName.text = _targetRelativeName.text;
        _targetRelativeName.text = tmp;
    }

    private void OnDropClick(GameObject target, PointerEventData eventData)
    {
        UIManager.HideDropList();
        dropTarget.TrySetActive(false);
    }

    private void ShowDropList(GameObject target, PointerEventData eventData)
    {
        dropTarget.TrySetActive(true);
        UIManager.ShowDropList(_relationDropOption, ItemClickHandler, m_tran, target.transform, new Vector2(0, -15),
            UIManager.Direction.Down, 116, 30);
    }

    private void ItemClickHandler(ItemDropInfo info)
    {
        dropTarget.TrySetActive(false);
        ChangeRelativeByOption(info.subtype);
    }

    private void ChangeRelativeByOption(string subType)
    {
        _myRelativeName.gameObject.TrySetActive(true);
        _targetRelativeName.gameObject.TrySetActive(true);
        _myRelativeInput.gameObject.TrySetActive(false);
        _targetRelativeInput.gameObject.TrySetActive(false);
        _btnSwap.TrySetActive(true);
        int cfgId = int.Parse(subType);
        if (cfgId != (int) Relationship.DIY)
        {
            var cfg = ConfigManager.GetConfig<ConfigFamilyTitle>();
            ConfigFamilyTitleLine cfgLine = cfg.GetLine(cfgId);
            _myRelativeName.text = cfgLine.FromTitle[0];
            _targetRelativeName.text = cfgLine.ToTitle[0];
            _relativeNameTxt.text = cfgLine.Name;
            _currentRelationship = cfgLine.ID;
        }
        else
        {
            _myRelativeName.gameObject.TrySetActive(false);
            _targetRelativeName.gameObject.TrySetActive(false);
            _relativeNameTxt.text = "自定义";
            _myRelativeInput.gameObject.TrySetActive(true);
            _targetRelativeInput.gameObject.TrySetActive(true);
            _btnSwap.TrySetActive(false);
            _currentRelationship = (int) Relationship.DIY;
        }
    }

    private void SendReq()
    {

        if (_currentRelationship != (int)Relationship.DIY)
        {
            NetLayer.Send(new tos_family_add_family
            {
                id = _playerId,
                from_relative = _myRelativeName.text,
                to_relative = _targetRelativeName.text
            });
        }
        else
        {
            var from_relative = WordFiterManager.Fiter(_myRelativeInput.text);
            var to_relative = WordFiterManager.Fiter(_targetRelativeInput.text);
            if (from_relative != _myRelativeInput.text || to_relative != _targetRelativeInput.text)
            {
                TipsManager.Instance.showTips("称谓包含屏蔽字，请重新输入");
                return;
            }
            NetLayer.Send(new tos_family_add_family
            {
                id = _playerId,
                from_relative = from_relative,
                to_relative = to_relative
            });
        }
        HidePanel();
       
        
    }

    private void OnClickConfirm(GameObject target, PointerEventData eventData)
    {
        if (ItemDataManager.IsDepotHas(GameConst.FAMILY_CARD_ID))
        {
            SendReq();
        }
        else
        {
            var configMall = ConfigManager.GetConfig<ConfigMall>().GetMallLine(GameConst.FAMILY_CARD_ID);
            var price = configMall.Price;
            var showPrice = 0;
            var strMoneytype = String.Empty;
            for (int i = 0; i < price.Length; i++)
            {
                if (price[i].time == 1)
                {
                    showPrice = price[i].price;
                }
            }
            if (configMall.MoneyType == EnumMoneyType.ALL)
                strMoneytype = "货币";
            else if (configMall.MoneyType == EnumMoneyType.DIAMOND)
                strMoneytype = "钻石";
            else if (configMall.MoneyType == EnumMoneyType.COIN)
                strMoneytype = "金币";
            else if (configMall.MoneyType == EnumMoneyType.MEDAL)
                strMoneytype = "勋章";
            else if (configMall.MoneyType == EnumMoneyType.EVENTCOIN_002)
                strMoneytype = "点券";
            UIManager.ShowTipPanel("家族卡剩余数量不足,是否直接花费" + strMoneytype + showPrice + "进行邀请?", SendReq);
        }
    }

    private enum Relationship
    {
        START = 1,
        DIY = 999
    }
}