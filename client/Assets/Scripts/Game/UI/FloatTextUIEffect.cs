﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class FloatTextUIEffect
{
    static FloatTextUIEffect _instance;

    static public FloatTextUIEffect Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FloatTextUIEffect();
            }
            return _instance;
        }
    }

    public void Play(string[] data, Transform trans = null, float distance = 18, float interval = 0.5f, float delay=0,int font_size = 16, float max_w = 0, Vector2 offset = default(Vector2))
    {
        if (data == null || data.Length == 0)
        {
            return;
        }
        foreach (var str in data)
        {
            var txt = Util.CreateText(trans, offset, font_size, Color.white, null, TextAnchor.MiddleCenter);
            txt.text = str;
            txt.horizontalOverflow = max_w > 0 ? HorizontalWrapMode.Wrap : HorizontalWrapMode.Overflow;
            max_w = max_w > 0 ? max_w : txt.preferredWidth;
            txt.rectTransform.sizeDelta = new Vector2(max_w, font_size + 4);
            if (txt.rectTransform.sizeDelta.y < txt.preferredHeight)
            {
                txt.rectTransform.sizeDelta = new Vector2(max_w+4, Math.Max(txt.preferredHeight, font_size) + 4);
            }
            var tween = TweenPosition.Begin(txt.gameObject, 0.5f, txt.transform.localPosition + new Vector3(0, distance, 0));
            tween.delay = delay;
            tween.SetOnFinished(() =>GameObject.Destroy(txt.gameObject));
            delay += interval;
        }
    }
}
