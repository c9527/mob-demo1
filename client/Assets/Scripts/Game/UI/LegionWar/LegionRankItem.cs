﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战排行榜项
//创建者：HWL
//创建时间: 2016/3/22 15:27:48
///////////////////////////

class LegionRankItem : ItemRender
{
    private Image m_selectImg;
    private Image m_myselFlagImg;
    private Image m_rankIcon;
    private Text m_rankText;
    private Text m_corpNameText;
    private Text m_playerNameText;
    private Text m_serverNameText;
    private Text m_legionNameText;
    private Text m_devoteText;
    private Text m_jobText;
    private Image m_miritaryIcon;//军衔

    public override void Awake()
    {
        m_selectImg = transform.Find("selectImg").GetComponent<Image>();
        m_myselFlagImg = transform.Find("myselfImg").GetComponent<Image>();

        m_rankIcon = transform.Find("rank/rankImg").GetComponent<Image>();
        m_rankText = transform.Find("rank").GetComponent<Text>();
        m_miritaryIcon = transform.Find("miritaryIcon").GetComponent<Image>();
        m_playerNameText = transform.Find("playerNameText").GetComponent<Text>();
        m_legionNameText = transform.Find("legionNameText").GetComponent<Text>();
        m_serverNameText = transform.Find("serverText").GetComponent<Text>();
        m_jobText = transform.Find("jobText").GetComponent<Text>();
        m_devoteText = transform.Find("devoteText").GetComponent<Text>();
        m_corpNameText = transform.Find("corpNameText").GetComponent<Text>();
        
    }

    protected override void OnSetData(object data)
    {
        LegionRankItemInfo item = data as LegionRankItemInfo;
        m_rankIcon.gameObject.TrySetActive(false);
        if (item.rank < 4)
        {
            m_rankIcon.gameObject.TrySetActive(true);
            m_rankIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round3_" + item.rank));
            m_rankText.text = "";
        }
        else
        {
            m_rankText.text = item.rank.ToString();
        }
        m_myselFlagImg.gameObject.TrySetActive(false);
        if( PlayerSystem.roleId == item.id)
        {
            m_myselFlagImg.gameObject.TrySetActive(true);
        }
        m_miritaryIcon.SetSprite(ResourceManager.LoadArmyIcon(item.level));
        m_playerNameText.text = item.playerName;
        ConfigLegionPositionLine positionLine = ConfigManager.GetConfig<ConfigLegionPosition>().GetLine(item.job);
        m_jobText.text = positionLine == null ? "无职位" : positionLine.PositionName;
        m_serverNameText.text = item.serverName;
        ConfigLegionLine legionLine = ConfigManager.GetConfig<ConfigLegion>().GetLine(item.legionid);
        m_legionNameText.text = legionLine == null ? "未知":legionLine.LegionName;
        m_corpNameText.text = item.corpName;
        m_devoteText.text = item.devote.ToString();
    }
}

public class LegionRankItemInfo
{
    public long id;
    public int rank;
    public int devote;//贡献
    public int level;//军衔
    public string playerName;
    public string corpName;
    public string serverName;
    public int legionid;
    public int job;//职位
}
