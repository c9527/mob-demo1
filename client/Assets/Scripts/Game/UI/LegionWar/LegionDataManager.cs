﻿using System;
using System.Collections.Generic;
using UnityEngine;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战数据管理器
//创建者：HWL
//创建时间: 2016/3/25 9:25:17
///////////////////////////

public class LegionDataManager
{
    public static long MyLegionId = 0;
    public static int MyCurLegionPosition = 0;//当前职位
    public static List<p_legionwar_legion_info> legionwarLegionInfoList = new List<p_legionwar_legion_info>();
    public static List<p_legionwar_log> battleLogsList = new List<p_legionwar_log>();
    public static List<p_legionwar_eminentor> LegionWarFamousList = new List<p_legionwar_eminentor>();
    public static p_legionwar_territory[] territory_list;
    public static object HistoryPositionList;
    public static List<p_legionwar_officer> legionOfficer = new List<p_legionwar_officer>();//军官列表
    public static int battle;//参战场次
    public static int win;//战胜场次
    public static int kill;//杀敌数
    public static int death;//死亡数
    public static int contribution;//贡献数
    public static int season;
    public static bool have_reward;//是否有奖励可以领取
    public static int daily_battle;
    public static int daily_total_battle;
    /// <summary>
    /// 军官列表
    /// </summary>
    /// <param name="officers"></param>
    private static void Toc_legionwar_legion_officers(toc_legionwar_legion_officers officers)
    {
        legionOfficer.Clear();
        legionOfficer.AddRange(officers.officers);
        GameDispatcher.Dispatch(GameEvent.LEGION_OFFICERS_INFO_UPDATE);
    }

    /// <summary>
    /// 名人堂数据
    /// </summary>
    /// <param name="famous"></param>
    private static void Toc_legionwar_eminentors(toc_legionwar_eminentors famous)
    {
        LegionWarFamousList.Clear();
        LegionWarFamousList.AddRange(famous.eminentors);
        GameDispatcher.Dispatch(GameEvent.LEGION_FAMOUS_INFO_UPDATE);
        HeroCardDataManager.getHeroCardTypeByCardInfo(famous.hero_cards);
    }
    
    private static void Toc_legionwar_my_info(toc_legionwar_my_info myLegionInfo)
    {
        MyLegionId = myLegionInfo.legion_id;
        //Logger.Log("MyLegionId = " + MyLegionId);
        MyCurLegionPosition = myLegionInfo.cur_position;
        HistoryPositionList = myLegionInfo.history_position;
        battle = myLegionInfo.battle;
        win = myLegionInfo.win;
        kill = myLegionInfo.kill;
        death = myLegionInfo.death;
        contribution = myLegionInfo.contribution;
        have_reward = myLegionInfo.have_reward;
        daily_battle = myLegionInfo.daily_battle;
        GameDispatcher.Dispatch(GameEvent.LEGION_BUBBLE_TIP);
        GameDispatcher.Dispatch(GameEvent.LEGION_MYLEGION_INFO_UPDATE);
        var misc = ConfigManager.GetConfig<ConfigMisc>();
        daily_total_battle= misc.GetLine("legionwar_daily_reward_battles").ValueInt;
    }

    private static void Toc_legionwar_data(toc_legionwar_data legionWarData)
    {
        legionwarLegionInfoList.Clear();
        battleLogsList.Clear();
        legionwarLegionInfoList.AddRange(legionWarData.legion_list);
        battleLogsList.AddRange(legionWarData.battle_logs);
        territory_list = legionWarData.territory_list;
        GameDispatcher.Dispatch(GameEvent.LEGION_WAR_INFO_UPDATE);
        season = legionWarData.cur_season;
    }

    /// <summary>
    /// 通过军团id获取军团信息    /// </summary>
    /// <param name="legionId"></param>
    /// <returns></returns>
    public static p_legionwar_legion_info GetLegionWarInfoByLegionId(long legionId)
    {
        for( int i = 0; i <  legionwarLegionInfoList.Count; i++)
        {
            if( legionwarLegionInfoList[i].id == legionId)
            {
                return legionwarLegionInfoList[i];
            }
        }
        return null;
    }



    /// <summary>
    /// 获取军团名字
    /// </summary>
    /// <param name="legionId"></param>
    /// <returns></returns>
    public static string GetLegionName(int legionId)
    {
        string legionName = "";
        ConfigLegion legionData = ConfigManager.GetConfig<ConfigLegion>();
        if( legionData != null )
        {
            ConfigLegionLine data = legionData.GetLine(legionId);
            legionName = data == null ? "未知" : data.LegionName;
        }
        return legionName;
    }



    /// <summary>
    /// 获取领土名字
    /// </summary>
    /// <param name="territoryId"></param>
    /// <returns></returns>
    public static string GetLegionTerritoryName(int territoryId)
    {
        string territoryName = "未知";
        ConfigLegionTerritory territory = ConfigManager.GetConfig<ConfigLegionTerritory>();
        if (territory != null)
        {
            ConfigLegionTerritoryLine territoryLine = territory.GetLine(territoryId);
            territoryName = territoryLine == null ? "" : territoryLine.TerritoryWorldName;
        }
        return territoryName;
    }


    private static int _legionwarTips = -10000;
    private static int m__legionwarTips
    {
        get
        {
            if (_legionwarTips == -10000)
                _legionwarTips = PlayerPrefs.GetInt("legionwarTips");
            return _legionwarTips;
        }
        set
        {
            if (_legionwarTips != value)
            {
                _legionwarTips = value;
                PlayerPrefs.SetInt("legionwarTips", value);
                PlayerPrefs.Save();
            }
        }
    }

    public static void CheckLegionWarStart()
    {
        var week = ConfigManager.GetConfig<ConfigEventList>();
        var wls = week.GetLine(1200);
        var wl = wls.WeekLimit;
        var dtw = TimeUtil.GetNowTime().DayOfWeek;
        var dts = TimeUtil.GetNowTime();
        int startTime = int.Parse(wls.TimeLimit.Split('#')[0]) / 100;
        int endTime = int.Parse(wls.TimeLimit.Split('#')[1]) / 100;
        bool m_istime = false;
        for (int i = 0; i < wl.Length; i++)
        {
            if (wl[i] == (int)dtw)
            {
                if (dts.Hour >= startTime && dts.Hour <= endTime)
                {
                    m_istime = true;
                }
            }
        }
        int tips = m__legionwarTips;
        if (m_istime)
        {
            if (tips == 1)
            {
                return;
            }
            else
            {
                NetChatData.MockChatMsg("世界争霸已经开启，火速前往战场！", CHAT_CHANNEL.CC_LOCALSERVER);
                //PlayerPrefs.SetInt("legionwarTips", 1);
                //PlayerPrefs.Save();
                m__legionwarTips = 1;
            }
        }
        else
        {
            //PlayerPrefs.SetInt("legionwarTips", 0);
            //PlayerPrefs.Save();
            m__legionwarTips = 0;
        }

    }

    public static bool IsHasRestBattle()
    {
        return daily_battle < daily_total_battle;
    }
}
