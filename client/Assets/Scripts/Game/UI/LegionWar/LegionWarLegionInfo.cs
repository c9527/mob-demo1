﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团信息页签
//创建者：HWL
//创建时间: 2016/3/19 16:10:32
///////////////////////////
public class LegionWarLegionInfo
{
    private Transform m_tran;
    private Text m_legionNameText;
    //军团势力
    private Text m_legionPower1;
    private Text m_legionPower2;
    private Text m_legionPower3;
    private Image m_legionIcon1;
    private Image m_legionIcon2;
    private Image m_legionIcon3;

    private Text m_joinFightTimesText;
    private Text m_victoryText;
    private Text m_killenemyText;
    private Text m_diedText;
    private Text m_doveteText;
    private ItemDropInfo[] m_camp_type_data;
    private DataGrid m_dataGrid;
    private Image m_maskImg;
    public LegionWarLegionInfo(Transform transform)
    {
        m_tran = transform;
        if(m_tran == null)
        {
            return;
        }
        init();
    }

    private void init()
    {
        m_legionNameText = m_tran.Find("legion/LegionName").GetComponent<Text>();
        Transform leftTran = m_tran.Find("left");
       

        //军团势力
        m_legionPower1 = leftTran.transform.Find("LegionPower/Legion1/LegionPower1").GetComponent<Text>();
        m_legionPower2 = leftTran.transform.Find("LegionPower/Legion2/LegionPower2").GetComponent<Text>();
        m_legionPower3 = leftTran.transform.Find("LegionPower/Legion3/LegionPower3").GetComponent<Text>();

        m_legionIcon1 = leftTran.transform.Find("LegionPower/Legion1").GetComponent<Image>();
        m_legionIcon2 = leftTran.transform.Find("LegionPower/Legion2").GetComponent<Image>();
        m_legionIcon3 = leftTran.transform.Find("LegionPower/Legion3").GetComponent<Image>();

        m_maskImg = m_tran.Find("poplistpanel").GetComponent<Image>();
        m_maskImg.gameObject.TrySetActive(false);

        m_joinFightTimesText = leftTran.Find("MyInfo/content/item0/text1").GetComponent<Text>();
        m_victoryText = leftTran.Find("MyInfo/content/item1/text1").GetComponent<Text>();
        m_killenemyText = leftTran.Find("MyInfo/content/item2/text1").GetComponent<Text>();
        m_diedText = leftTran.Find("MyInfo/content/item3/text1").GetComponent<Text>();
        m_doveteText = leftTran.Find("MyInfo/content/item4/text1").GetComponent<Text>();

        m_dataGrid = m_tran.Find("right/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("right/ScrollDataGrid/content/ItemInfo").gameObject, typeof(LegionItem));
        m_dataGrid.useLoopItems = true;
        initEvent();
    }

    private void initEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("right/changeCampBtn")).onPointerClick += changeCamp;
        UGUIClickHandler.Get(m_tran.Find("right/jobDescBtn")).onPointerClick += openJobDescPanel;
        UGUIClickHandler.Get(m_tran.Find("right/myHistroyJobBtn")).onPointerClick += openMyHistroyJobPanel;
        UGUIClickHandler.Get(m_maskImg.gameObject).onPointerClick += delegate { m_maskImg.gameObject.TrySetActive(false); UIManager.HideDropList(); };
    }

    /// <summary>
    /// 更新军团信息页签面板
    /// </summary>
    public void UpdateLegionView()
    {
        long legionId = LegionDataManager.MyLegionId;
        ConfigLegion config = ConfigManager.GetConfig<ConfigLegion>();
        ConfigLegionLine line = config.GetLine((int)legionId);
        m_legionNameText.text = line  == null ? "未知" : line.LegionName;

        m_camp_type_data = new ItemDropInfo[3];
        string legionName = "";
        p_legionwar_legion_info legionInfo;
        for( int i = 0; i < LegionDataManager.legionwarLegionInfoList.Count;i++ )
        {
            legionName = "未知";
            legionInfo = LegionDataManager.legionwarLegionInfoList[i];
            if (config != null)
            {
                line = config.GetLine(legionInfo.id);
                legionName = line == null ? "未知" : line.LegionName;
            }
            m_camp_type_data[i] = new ItemDropInfo { type = "REPORT", subtype = legionInfo.id.ToString(), label = legionName };
            showLegionPowerInfo(legionInfo.id);
        }
    }

    /// <summary>
    /// 军团势力
    /// </summary>
    /// <param name="legionId"></param>
    private void showLegionPowerInfo(int legionId)
    {
        string legionName = "";
        ConfigLegion legion = ConfigManager.GetConfig<ConfigLegion>();
        if( legion != null )
        {
            ConfigLegionLine legionLine = legion.GetLine(legionId);
            if( legionLine != null )
            {
                legionName = legionLine.LegionName;
            }
        }
        if (legionId == (int)LegionNameIdEnum.XUESELIMING)
        {
            m_legionIcon1.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + legionId));
            m_legionPower1.text = legionName;
        }
        else if (legionId == (int)LegionNameIdEnum.ZHIYOUZHIYI)
        {
            m_legionIcon2.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + legionId));
            m_legionPower2.text = legionName;
        }
        else if (legionId == (int)LegionNameIdEnum.CANGQIONGZHIGUANG)
        {
            m_legionIcon3.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + legionId));
            m_legionPower3.text = legionName;
        }
    }


    /// <summary>
    /// 更新我的战绩
    /// </summary>
    public void UpdateMyInfo()
    {
        m_joinFightTimesText.text = LegionDataManager.battle + " 场";
        m_victoryText.text = LegionDataManager.win + " 场";
        m_killenemyText.text = LegionDataManager.kill + " 人";
        m_diedText.text = LegionDataManager.death + " 次";
        m_doveteText.text =  LegionDataManager.contribution + " 分";
    }

    public void updateGridList()
    {
       p_legionwar_officer officer;
       List<LegionItemInfo> list = new List<LegionItemInfo>();
       
        for (int i = 0; i < LegionDataManager.legionOfficer.Count; i++)
        {
            LegionItemInfo item = new LegionItemInfo();
            officer = LegionDataManager.legionOfficer[i];
            item.playerId = officer.player_id;
            item.job = officer.position;
            item.name = officer.name;
            item.server = PlayerSystem.GetRoleSeverName(item.playerId);
            item.devote = officer.contribution;
            list.Add(item);
        }
         
        m_dataGrid.Data = list.ToArray();
    }

    //切换阵营
    private void changeCamp(GameObject target,PointerEventData data)
    {
        m_maskImg.gameObject.TrySetActive(true);
        UIManager.ShowDropListUp(m_camp_type_data, OnChangeCampType, m_tran, target, 120, 42);
    }

    private void OnChangeCampType(ItemDropInfo vo = null)
    {
        m_maskImg.gameObject.TrySetActive(false);
        Logger.Error(vo.label + "______" + vo.subtype);
        NetLayer.Send(new tos_legionwar_legion_officers() { legion_id = int.Parse(vo.subtype) });
        //updateGridList(long.Parse(vo.subtype));
    }

    //职位说明面板
    private void openJobDescPanel(GameObject targe, PointerEventData data)
    {
        string content = "";
        ConfigLegionIntro infoTro = ConfigManager.GetConfig<ConfigLegionIntro>();
        if( infoTro == null )
        {
            Logger.Log("legion position config data is null");
        }
        if( infoTro != null )
        {
            ConfigLegionIntroLine introLine = infoTro.GetLine("Pos");
            content = introLine == null ? "未知":introLine.Content.Replace("\\n","\n");
        }

        //UIManager.PopRulePanel("LegionPos");
        UIManager.ShowTipMulTextPanel(content, "职位说明");
    }

    //历史职位信息
    private void openMyHistroyJobPanel(GameObject targe, PointerEventData data)
    {
        UIManager.PopPanel<PanelLegionWarMyHistoryJob>(null, true);
    }
}
