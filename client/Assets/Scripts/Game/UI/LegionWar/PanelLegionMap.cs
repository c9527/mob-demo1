﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using System.Collections;
using System.Collections.Generic;

public class PanelLegionMap :BasePanel
{

    public class blockData
    {
        public string m_name;
        public string worldname;
        public int m_id;
        public int[] nearBlock;//相邻块的id;
        public int m_legion_id;
        public int m_state;//0---不可参展
                           //1---交战
                           //2---夹击
                           //3---包围
                           //4---濒危
                           // -1---处于交战但无法加入
        public float[] Occupy;
        public GameObject m_block;
        public Image Image;
        public Image point;
        public bool is_base;
        public int orOnwer;
        public int[] legionData;
        public bool isFlag = false;
        public int resNum;
        public Image barBg;
        public Image bar1;
        public Image bar2;
        public Image bar3;
        public Text txtper;
        public p_legionwar_territory_legion[] legion_info; 
    }

    Text m_restTime;
    int m_flagInd;
    public Image m_flag;
    object[] passTo;
    Camera m_mapCamera;
    GameObject m_earth;
    RawImage m_image;
    Vector3 m_last;
    bool m_isMove;
    bool m_down;
    bool m_isclick;
    Vector3 m_downPoint;
    Vector3 m_clickTarget;
    Quaternion m_perRotate;
    GameObject m_clickBlock;
    float m_centerAngle;
    Vector3 m_centerLine;
    string m_centerName;//当前在c位的对象，move后设为空
    int m_centerTime;
    int m_centerMode;//0--不处于集中观察模式
                     //1--转换中，不响应操作
                     //2--转换完成，处于集中观察模式
    int m_showTime;
    int m_showOffMode;//0--显示UI
    //1--开始隐藏
    //2--隐藏UI
    //3--开始显示
    /*-------------------------*/
    //UIgroup
    GameObject m_btnGroup;
    GameObject m_btnRank;
    GameObject m_btnInfo;
    GameObject m_btnBack;

    GameObject m_btnShowOff;

    GameObject m_titleGroup;
    Image m_perA;
    Image m_perB;
    Image m_perC;
    Text m_nameA;
    Text m_nameB;
    Text m_nameC;
    Text m_numA;
    Text m_numB;
    Text m_numC;

    GameObject m_joinGroup;
    GameObject m_btnLookfor;
    GameObject m_btnJoin;

    GameObject m_btnDesc;

    GameObject m_tagGroup;
    Text m_timeText;

    GameObject m_chatGroup;

    Image m_Showoff;
    string m_blockname;

    GameObject[] m_blockA;
    GameObject[] m_blockB;
    GameObject[] m_blockC;

    Image[] m_imageA;
    Image[] m_imageB;
    Image[] m_imageC;
    GameObject m_tabGroup;
    List<string> m_infoList;

    Text m_chatItem;

    blockData[] m_blocks;
    GameObject[] m_blockGroup;
    int[] m_legionBlocks;
    bool m_hasFlag;

    static public bool m_isFlag;
    GameObject m_rotate;
    GameObject m_bar;
    int startTime;
    int endTime;
    int startM;
    int endM;

    static public bool isTime;//是否休战
    Image Notime;
    Image m_myLegion;
    Text m_myLegionName;
    GameObject m_buddleTipGo;
    Text m_restBattle;
    DateTime m_nowTime;
    DateTime m_startTime;
    DateTime m_endTime;
    DateTime m_targetTime;
    int[] m_weeklimit;
    ConfigEventListLine m_cel;
    TimeSpan m_resttime;
    ConfigLegionSeason clg;
    int m_nowSeason;
    bool is_inseason;
    public PanelLegionMap()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionMap");
        AddPreLoadRes("UI/LegionWar/PanelLegionMap", EResType.UI);
        AddPreLoadRes("Atlas/LegionWar", EResType.Atlas);
        AddPreLoadRes("SceneItem/Diqiu", EResType.UIModel);
        AddPreLoadRes("UI/LegionWar/MapTexture", EResType.RenderTexture);
        AddPreLoadRes("Materials/mat_legion1", EResType.Material);
        AddPreLoadRes("Materials/mat_legion2", EResType.Material);
        AddPreLoadRes("Materials/mat_legion3", EResType.Material);
        AddPreLoadRes("Materials/mat_legion1_battle", EResType.Material);
        AddPreLoadRes("Materials/mat_legion2_battle", EResType.Material);
        AddPreLoadRes("Materials/mat_legion3_battle", EResType.Material);
    }

    public override void Init()
    {

        m_restTime = m_tran.Find("resttimeText").GetComponent<Text>();
        Notime = m_tran.Find("NoTime").GetComponent<Image>();
        m_rotate = new GameObject("rot");
        m_flagInd = -1;
        m_hasFlag = false;
        m_isFlag = false;
        passTo = new object[2];
        m_legionBlocks = new int[3];
        m_centerTime = 0;
        m_isMove = false;
        m_mapCamera = new GameObject("mapCamera").AddComponent<Camera>();
        m_tabGroup = m_tran.Find("stateGroup").gameObject;
        m_mapCamera.transform.SetLocation(null, 0, 9f, 28f);
        m_mapCamera.transform.Rotate(0, 180, 0);
        m_mapCamera.backgroundColor = Color.black;
        m_centerName = "";
        ResourceManager.LoadModelRef("SceneItem/Diqiu", res =>
        {
            m_earth = Object.Instantiate(res) as GameObject;
            m_earth.transform.SetLocation(null,0, 0, 0);
        }, false);
        m_image = m_tran.Find("Map").GetComponent<RawImage>();
        m_mapCamera.targetTexture = ResourceManager.LoadRenderTexture("UI/LegionWar/MapTexture");
        m_mapCamera.targetTexture.antiAliasing = 4;
        m_mapCamera.orthographic = false;
        m_isclick = false;
        m_centerMode = 0;
        m_showOffMode = 0;
        //--UI初始化//
        m_btnGroup = m_tran.Find("buttonGroup").gameObject;
        m_btnRank = m_tran.Find("buttonGroup/btnRank").gameObject;
        m_btnInfo = m_tran.Find("buttonGroup/btnWarInfo").gameObject;
        m_btnBack = m_tran.Find("buttonGroup/btnBack").gameObject;

        m_btnShowOff = m_tran.Find("ShowOff").gameObject;

        m_titleGroup = m_tran.Find("Title").gameObject;
        m_perA = m_tran.Find("Title/perGroup/perA").GetComponent<Image>();
        m_perB = m_tran.Find("Title/perGroup/perB").GetComponent<Image>();
        m_perC = m_tran.Find("Title/perGroup/perC").GetComponent<Image>();

        m_nameA = m_tran.Find("Title/nameGroup/nameA").GetComponent<Text>();
        m_nameB = m_tran.Find("Title/nameGroup/nameB").GetComponent<Text>();
        m_nameC = m_tran.Find("Title/nameGroup/nameC").GetComponent<Text>();

        m_numA = m_tran.Find("Title/nameGroup/nameA/Text").GetComponent<Text>();
        m_numB = m_tran.Find("Title/nameGroup/nameB/Text").GetComponent<Text>();
        m_numC = m_tran.Find("Title/nameGroup/nameC/Text").GetComponent<Text>();
        m_bar = m_tran.Find("bar").gameObject;
        m_joinGroup = m_tran.Find("joinGroup").gameObject;
        m_btnLookfor = m_tran.Find("joinGroup/btnLookFor").gameObject;
        m_btnJoin = m_tran.Find("joinGroup/btnJoin").gameObject;

        m_btnDesc = m_tran.Find("btnDesc").gameObject;

        m_tagGroup = m_tran.Find("tagGroup").gameObject;
        m_timeText = m_tran.Find("timeText").GetComponent<Text>();

        m_buddleTipGo = m_tran.Find("buttonGroup/btnRank/bubble_tip").gameObject;

        m_chatGroup = m_tran.Find("chatbg").gameObject;
        m_showTime = 0;
        m_Showoff = m_btnShowOff.GetComponent<Image>();
        m_blockA = new GameObject[15];
        m_blockB = new GameObject[15];
        m_blockC = new GameObject[15];
        m_imageA = new Image[15];
        m_imageB = new Image[15];
        m_imageC = new Image[15];

        m_infoList = new List<string>();
        m_chatItem = m_tran.Find("chatbg/chatItem").GetComponent<Text>();
        m_legionBlocks[0] = 0;
        m_legionBlocks[1] = 0;
        m_legionBlocks[2] = 0;
        InitBlockData();
        UpDateBlockInfo();
        CheckAllBlock();
        SetABCPercent(m_legionBlocks[0], m_legionBlocks[1], m_legionBlocks[2]);
        ConfigLegion cl = ConfigManager.GetConfig<ConfigLegion>();
        m_nameA.text = cl.GetLine(1).LegionName;
        m_nameB.text = cl.GetLine(2).LegionName;
        m_nameC.text = cl.GetLine(3).LegionName;
        SetBattleLog();

        m_myLegion = m_tran.Find("myLegion").GetComponent<Image>();
        m_myLegionName = m_tran.Find("myLegionName").GetComponent<Text>();
        m_restBattle = m_tran.Find("restBattle").GetComponent<Text>();
        m_nowTime = TimeUtil.GetNowTime();
        m_startTime = new DateTime();
        m_endTime = new DateTime();
        var con = ConfigManager.GetConfig<ConfigEventList>();
        m_cel = con.GetLine(1200);
        clg = ConfigManager.GetConfig<ConfigLegionSeason>();
        return;
       
    }

    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }

    public override void InitEvent()
    {
        GameDispatcher.AddEventListener(GameEvent.LEGION_BUBBLE_TIP, updateBuddleTip);
        UGUIUpDownHandler.Get(m_image.gameObject).onPointerDown += StartDrag;
        UGUIUpDownHandler.Get(m_image.gameObject).onPointerUp += EndDrag;
        UGUIClickHandler.Get(m_btnBack).onPointerClick += ClickBack;
        UGUIClickHandler.Get(m_btnShowOff).onPointerClick += ShowOffUI;//隐藏UI
        UGUIClickHandler.Get(m_btnRank).onPointerClick += ClickRank;
        UGUIClickHandler.Get(m_btnInfo).onPointerClick += ClickInfo;
        UGUIClickHandler.Get(m_btnLookfor).onPointerClick += ClickBlockInfo;
        GameDispatcher.AddEventListener(GameEvent.LEGION_WAR_INFO_UPDATE, OnDataChange);
        UGUIClickHandler.Get(m_tran.Find("btnDesc")).onPointerClick += delegate { UIManager.PopPanel<PanelLegionWarDesc>(null, true); };
        UGUIClickHandler.Get(m_tran.Find("joinGroup/btnJoin")).onPointerClick += JoinGame;
        UGUIClickHandler.Get(m_tran.Find("chaticon")).onPointerClick += delegate{UIManager.PopChatPanel<PanelChatSmall>();};
    }

    //更新排行榜按钮红点
    private void updateBuddleTip()
    {
        if (m_buddleTipGo != null)
        {
            m_buddleTipGo.TrySetActive(LegionDataManager.have_reward);
        }
    }

    void OnDataChange()
    {
        if (IsInited())
        {
            UpDateBlockInfo();
            CheckAllBlock();
            SetABCPercent(m_legionBlocks[0], m_legionBlocks[1], m_legionBlocks[2]);
            SetBattleLog();
        }
    }

    void ClickBlockInfo(GameObject target, PointerEventData eventdata)
    {
        
        for (int i = 0; i < 45; i++)
        {
            if (m_blocks[i].m_name == m_blockname)
            {
                passTo[0] = m_blocks[i];
                NetLayer.Send(new tos_legionwar_territory_info(){ territory_id = m_blocks[i].m_id });
            }
        }
    }

    void Toc_legionwar_my_territory_info(toc_legionwar_my_territory_info data)
    {
        passTo[1] = data;
        UIManager.PopPanel<PanellegionBlockInfo>(passTo, true);
    }

    void ClickInfo(GameObject target, PointerEventData eventdata)
    {
        object[] bnum = new object[3];
        bnum[0] = m_legionBlocks[0];
        bnum[1] = m_legionBlocks[1];
        bnum[2] = m_legionBlocks[2];
        UIManager.PopPanel<PanelLegionWar>(bnum, true);
    }

    void ClickRank(GameObject target, PointerEventData eventdata)
    {
        UIManager.PopPanel<PanelLegionRank>(null,true);
    }
    void ClickBack(GameObject target, PointerEventData eventData)
    {
        UIManager.HidePanel<PanelLegionMap>();
        UIManager.ShowPanel<PanelHallBattle>();
    }


    void ShowOffUI(GameObject target, PointerEventData eventData)
    {
        if (m_showOffMode == 0)
        {
            m_showOffMode = 1;
            m_showTime = 0;
            m_Showoff.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "showBtn"));
            
        }
        else if(m_showOffMode == 2)
        {
            m_showOffMode = 3;
            m_showTime = 0;
            m_Showoff.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "hideBtn"));
        }
    }
    void StartDrag(GameObject target, PointerEventData eventData)
    {
        m_down = true;
        m_isMove = false;
    }

    void EndDrag(GameObject target, PointerEventData eventData)
    {
        m_down = false;
        m_last = Vector3.zero;
        if (m_isMove == false)
        {
            if (m_centerMode == 0)
            {
                m_clickBlock = null;
                m_blockname = "";
                m_isclick = true;
                Ray ray = m_mapCamera.ViewportPointToRay(GetNomalVector(eventData.position));
                RaycastHit hitinfo;
                if (Physics.Raycast(ray, out hitinfo))
                {
                    GameObject go = hitinfo.collider.gameObject;
                    //Logger.Log(go.name);
                    if (go.name != "map_diqiu01"&&go.name != "map_diqiu02"&&go.name != "map_diqiu03")
                    {
                        m_clickBlock = go;
                        m_blockname = go.name;
                        m_isclick = true;
                        m_centerTime = 0;
                    }
                    else
                    {
                        m_clickBlock = null;
                        m_isclick = false;
                        return;
                    }
                    
                    Ray ray1 = m_mapCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                    RaycastHit hit2;
                    Physics.Raycast(ray1, out hit2);
                    m_centerLine = GetRotateLine(m_clickBlock.transform.position, hit2.point);
                    m_centerAngle = Vector3.Angle(m_clickBlock.transform.position, hit2.point);
                }
            }
            if (m_centerMode == 2)
            {
                m_isclick = true;
                m_centerTime = 0;
                m_joinGroup.TrySetActive(false);
            }
        }
        else
        {

        }
        m_downPoint = Vector3.zero;
        m_isMove = false;
    }
    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_BUBBLE_TIP, updateBuddleTip);
    }

    public override void OnHide()
    {
        m_earth.TrySetActive(false);
    }
    public override void OnShow()
    {
        int rest = LegionDataManager.daily_total_battle - LegionDataManager.daily_battle;
        if (rest < 0)
        {
            rest = 0;
        }
        m_restBattle.text = string.Format("今日四倍贡献剩余场次:{0}/{1}", rest, LegionDataManager.daily_total_battle);
        m_buddleTipGo.TrySetActive(LegionDataManager.have_reward);
        var a = ConfigManager.GetConfig<ConfigLegion>();
        m_centerMode = 0;
        m_mapCamera.orthographicSize = 10;
        m_joinGroup.TrySetActive(false);
        UpDateBlockInfo();
        CheckAllBlock();
        SetABCPercent(m_legionBlocks[0], m_legionBlocks[1], m_legionBlocks[2]);
        m_mapCamera.transform.SetLocation(null, 0, 9f, 28f);
        var sc = ConfigManager.GetConfig<ConfigLegionSeason>();
        Logger.Error(LegionDataManager.season.ToString());
        if (sc.GetLine(LegionDataManager.season) != null)
        {
            int time = sc.GetLine(LegionDataManager.season).EndTime;
            var dt = TimeUtil.GetTime((int)time);
            m_timeText.text = "本轮结束时间:" + dt.Month.ToString() + "月" + dt.Day.ToString() + "日 00时";
        }
        m_weeklimit = m_cel.WeekLimit;
        m_nowTime = TimeUtil.GetNowTime();
        startTime = int.Parse(m_cel.TimeLimit.Split('#')[0]) / 100;
        endTime = int.Parse(m_cel.TimeLimit.Split('#')[1]) / 100;
        endM = int.Parse(m_cel.TimeLimit.Split('#')[1]) % 100;
        startM = int.Parse(m_cel.TimeLimit.Split('#')[0]) % 100;
        isTime = false;
        bool isDay = false;
        for (int i = 0; i < m_weeklimit.Length; i++)
        {
            if (m_weeklimit[i] == (int)m_nowTime.DayOfWeek||((m_nowTime.DayOfWeek==0)&&(m_weeklimit[i]==7)))
            {
                isDay = true;    
                m_startTime = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, startTime, startM, 0);
                m_endTime = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, endTime, endM, 0);
                if (m_nowTime >= m_startTime && m_nowTime <= m_endTime)
                {
                    isTime = true;
                }
                else
                {
                    isTime = false;
                }
                break;
            }
        }

        if (!isDay)
        {
            for (int i = 0; i < m_weeklimit.Length; i++)
            {
                if (m_weeklimit[i] > (int)m_nowTime.DayOfWeek)
                {
                    m_startTime = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, startTime, startM, 0).AddDays(m_weeklimit[i] - (int)m_nowTime.DayOfWeek);
                    m_endTime = new DateTime(m_nowTime.Year, m_nowTime.Month, m_nowTime.Day, endTime, endM, 0).AddDays(m_weeklimit[i] - (int)m_nowTime.DayOfWeek);
                }
            }
        }

        m_targetTime = GetTargetTime(m_nowTime, isTime);
        isInSeason();

        if (!isTime)
        {
            Notime.gameObject.TrySetActive(true);
        }
        else
        {
            Notime.gameObject.TrySetActive(false);
        }
        m_myLegion.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + LegionDataManager.MyLegionId.ToString()));
        m_myLegionName.text = a.m_dataArr[LegionDataManager.MyLegionId - 1].LegionName;
        m_earth.TrySetActive(true);
        PanelTv.ClearTv();
    }

    void isInSeason()
    {
        int now = TimeUtil.GetNowTimeStamp();
        var seasons = clg.m_dataArr;
        for (int i = 0; i < seasons.Length; i++)
        {
            if (seasons[i].EndTime >= now)
            {
                if (seasons[i].StartTime > now)
                {
                    is_inseason = false;
                    m_targetTime = TimeUtil.GetTime(seasons[i].StartTime).AddHours((double)startTime + (double)startM / 60.0);
                    Logger.Error(m_targetTime.ToString());
                    isTime = false;
                    m_timeText.text = "下赛季开始时间:" + TimeUtil.GetTime(seasons[i].StartTime).Month.ToString() + "月" + TimeUtil.GetTime(seasons[i].StartTime).Day.ToString() + "日" + TimeUtil.GetTime(seasons[i].StartTime).Hour.ToString() + "时";
                    return;
                }
                else
                {
                    is_inseason = true;
                    return;
                    
                }
            }
        }
    }

    string GetRestTime()
    {
        m_nowTime = TimeUtil.GetNowTime();
        if (m_nowTime >= m_targetTime)
        {
            isTime = !isTime;
            Notime.gameObject.TrySetActive(!isTime);
            m_targetTime = GetTargetTime(m_nowTime, isTime);
        }
        m_resttime = m_targetTime - m_nowTime;
        if (isTime)
        {
            return "距离本日活动结束还有" + (new DateTime() + m_resttime).ToString("HH:mm:ss");
        }
        else
        {
            if (m_resttime.Days == 0)
            {
                return "距离下次活动开始还有" + (new DateTime() + m_resttime).ToString("HH:mm:ss");
            }
            else
            {
                return "距离下次活动开始还有" + ((int)m_resttime.TotalHours).ToString() + (new DateTime() + m_resttime).ToString(":mm:ss");
            }
        }
    }

    public override void Update()
    {
        m_restTime.text = GetRestTime();
        //CheckRotation(m_earth);
        if (m_down)
        {
            if (m_downPoint == Vector3.zero)
            {
                m_downPoint = Input.mousePosition;
            }
            else
            {
                if (Vector3.Distance(m_downPoint, Input.mousePosition) > 5)
                {
                    m_isMove = true;
                }
                else
                {
                    m_isMove = false;
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            Ray ray = m_mapCamera.ViewportPointToRay(GetNomalVector(Input.mousePosition));
            RaycastHit hitinfo;
            if (Physics.Raycast(ray, out hitinfo))
            {
                if (m_isMove&&m_centerMode == 0)
                {
                    GameObject go = hitinfo.collider.gameObject;
                    if (m_last == Vector3.zero)
                    {
                        m_last = hitinfo.point;
                        return;
                    }
                    float angle = Vector3.Angle(m_last, hitinfo.point);
                    Vector3 line = GetRotateLine(m_last, hitinfo.point);
                    m_rotate.transform.Rotate(GetRotateLine(m_last, hitinfo.point), Vector3.Angle(m_last, hitinfo.point), Space.World);
                    CheckRotation(m_rotate);
                    m_earth.transform.eulerAngles = m_rotate.transform.eulerAngles;
                    m_last = hitinfo.point;
                }
            }
        }
        if (m_isclick && m_centerTime <= 8&&m_centerMode !=2&&m_centerMode!=3&&m_clickBlock!=null)
        {
            m_mapCamera.transform.position += new Vector3(0, 0, -5f/8f);
            m_rotate.transform.Rotate(m_centerLine, m_centerAngle / 8f, Space.World);
            //CheckRotation(m_rotate);
            m_earth.transform.eulerAngles = m_rotate.transform.eulerAngles;
            m_centerTime++;
            m_centerMode = 1;
            if (m_centerTime > 8)
            {
                m_isclick = false;
                m_centerMode = 2;
                m_joinGroup.TrySetActive(true);
                char[] chs = m_clickBlock.name.ToCharArray();
                int id = (chs[0] - 65) * 15 + (chs[1] - 48) * 10 + chs[2] - 48 - 1;
                if (m_blocks[id].m_state <= 0)
                {
                    Util.SetGrayShader(m_btnJoin.GetComponent<Image>());
                }  
                else
                {
                    Util.SetNormalShader(m_btnJoin.GetComponent<Image>());
                }
            }
        }
        if (m_centerMode!=0&&m_centerMode!=1&&m_isclick)
        {
            m_mapCamera.transform.position += new Vector3(0, 0, 5f/8f);
            m_rotate.transform.Rotate(m_centerLine, -m_centerAngle / 8f, Space.World);
            m_earth.transform.eulerAngles = m_rotate.transform.eulerAngles;
            m_centerTime++;
            m_centerMode = 3;
            if (m_centerTime > 8)
            {
                m_isclick = false;
                m_centerMode = 0;
            }
        }
        if (m_showOffMode == 1)
        {
            m_titleGroup.transform.Translate(0f, 0.5f, 0f);
            m_btnGroup.transform.Translate(1f, 0, 0);
            m_chatGroup.transform.Translate(2f, 0, 0);
            m_tagGroup.transform.Translate(0, -1f, 0);
            m_btnDesc.transform.Translate(1, 0, 0);
            m_timeText.transform.Translate(0, -1, 0);
            m_restTime.transform.Translate(0, -1, 0);
            m_restBattle.transform.Translate(2f, 0, 0);
            m_myLegion.transform.Translate(-1, 0, 0);
            m_myLegionName.transform.Translate(-1, 0, 0);
            m_showTime++; 
            if (m_showTime >= 10)
            {
                m_showOffMode = 2;
            }
        }
        else if(m_showOffMode == 3)
        {
            m_titleGroup.transform.Translate(0f, -0.5f, 0f);
            m_btnGroup.transform.Translate(-1f, 0, 0);
            m_chatGroup.transform.Translate(-2f, 0, 0);
            m_tagGroup.transform.Translate(0, 1f, 0);
            m_btnDesc.transform.Translate(-1, 0, 0);
            m_timeText.transform.Translate(0, 1, 0);
            m_restTime.transform.Translate(0, 1, 0);
            m_restBattle.transform.Translate(-2f, 0, 0);
            m_myLegion.transform.Translate(1, 0, 0);
            m_myLegionName.transform.Translate(1, 0, 0);
            m_showTime++;
            if (m_showTime >= 10)
            {
                m_showOffMode = 0;
            }
        }
        for (int i = 0; i < 45; i++)
        {
            if (m_blocks[i].Image != null)
            {
                GetStateImage(m_blocks[i].Image, m_blocks[i].m_block, 1);
            }
            if (m_blocks[i].barBg != null)
            {
                GetBarImage(m_blocks[i].barBg, m_blocks[i].m_block);
            }
            if (m_blocks[i].txtper != null)
            {
                GetText(m_blocks[i].txtper, m_blocks[i].m_block);
            }
        }
        GetFlagImage();
    }

    Vector3 GetRotateLine(Vector3 lastpoint, Vector3 dirpoint)
    {
        return Vector3.Cross(lastpoint, dirpoint);
    }

    Vector3 GetNomalVector(Vector3 vec)
    {
        return new Vector3(vec.x / (float)Screen.width, vec.y / (float)Screen.height, 0.0f);
    }

    void GetBarImage(Image img, GameObject target)
    {
        Vector3 vec = m_mapCamera.WorldToViewportPoint(target.transform.position);
        //img.SetNativeSize();
        //img.transform.localScale = new Vector3(1, 1, 1);
        //var a = Screen.GetResolution;
        if ((int)Screen.width / 3 == (int)Screen.height / 2)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.55f) * 640f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 9)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1280f, (vec.y - 0.55f) * 720f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 10)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1152f, (vec.y - 0.55f) * 720f, 0);
        }
        else if ((int)Screen.width / 4 == (int)Screen.height / 3)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1000f, (vec.y - 0.55f) * 750f, 0);
        }
        else
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.55f) * 640f, 0);
        }
        var pos = m_mapCamera.WorldToScreenPoint(target.transform.position);
        bool showImg = true;
        //img.gameObject.TrySetActive(true);

        if (target.transform.position.z <= 12)
        {
            //img.gameObject.TrySetActive(false);
            showImg = false;
        }
        //if (img.gameObject.activeSelf != showImg)
        //    img.gameObject.TrySetActive(showImg);
        img.gameObject.TrySetActive(showImg);
    }

    void GetText(Text img, GameObject target)
    {
        Vector3 vec = m_mapCamera.WorldToViewportPoint(target.transform.position);
        //img.SetNativeSize();
        //img.transform.localScale = new Vector3(1, 1, 1);
        //var a = Screen.GetResolution;
        if ((int)Screen.width / 3 == (int)Screen.height / 2)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.58f) * 640f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 9)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1280f, (vec.y - 0.58f) * 720f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 10)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1152f, (vec.y - 0.58f) * 720f, 0);
        }
        else if ((int)Screen.width / 4 == (int)Screen.height / 3)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1000f, (vec.y - 0.58f) * 750f, 0);
        }
        else
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.58f) * 640f, 0);
        }
        var pos = m_mapCamera.WorldToScreenPoint(target.transform.position);
        var showImg = true;
        //img.gameObject.TrySetActive(true);

        if (target.transform.position.z <= 12)
        {
//            img.gameObject.TrySetActive(false);
            showImg = false;
        }
        //if (img.gameObject.activeSelf != showImg)
        //    img.gameObject.TrySetActive(showImg);
        img.gameObject.TrySetActive(showImg);
    }


    void GetStateImage(Image img, GameObject target, int state)
    {
        Vector3 vec = m_mapCamera.WorldToViewportPoint(target.transform.position);
        //img.SetNativeSize();
        //img.transform.localScale = new Vector3(1, 1, 1);
        //var a = Screen.GetResolution;
        if ((int)Screen.width / 3 == (int)Screen.height / 2)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.5f) * 640f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 9)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1280f, (vec.y - 0.5f) * 720f, 0);
        }
        else if ((int)Screen.width / 16 == (int)Screen.height / 10)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1152f, (vec.y - 0.5f) * 720f, 0);
        }
        else if ((int)Screen.width / 4 == (int)Screen.height / 3)
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 1000f, (vec.y - 0.5f) * 750f, 0);
        }
        else
        {
            img.transform.localPosition = new Vector3((vec.x - 0.5f) * 960f, (vec.y - 0.5f) * 640f, 0);
        }
        var pos = m_mapCamera.WorldToScreenPoint(target.transform.position);
        bool showImg = true;
        //img.gameObject.TrySetActive(true);

        if (target.transform.position.z <= 12)
        {
            //img.gameObject.TrySetActive(false);
            showImg = false;
        }
        //if (img.gameObject.activeSelf != showImg)
        //    img.gameObject.TrySetActive(showImg);
        img.gameObject.TrySetActive(showImg);
    }


    void GetFlagImage()
    {
        if (m_hasFlag&&m_flag!=null)
        {
            Vector3 vec = m_mapCamera.WorldToViewportPoint(m_blocks[m_flagInd].m_block.transform.position);
            m_flag.SetNativeSize();
            m_flag.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            if ((int)Screen.width / 3 == (int)Screen.height / 2)
            {
                m_flag.transform.localPosition = new Vector3((vec.x - 0.55f) * 960f, (vec.y - 0.5f) * 640f, 0);
            }
            else if ((int)Screen.width / 16 == (int)Screen.height / 9)
            {
                m_flag.transform.localPosition = new Vector3((vec.x - 0.55f) * 1280f, (vec.y - 0.5f) * 720f, 0);
            }
            else if ((int)Screen.width / 16 == (int)Screen.height / 10)
            {
                m_flag.transform.localPosition = new Vector3((vec.x - 0.55f) * 1152f, (vec.y - 0.5f) * 720f, 0);
            }
            else if ((int)Screen.width / 4 == (int)Screen.height / 3)
            {
                m_flag.transform.localPosition = new Vector3((vec.x - 0.55f) * 1000f, (vec.y - 0.5f) * 750f, 0);
            }
            else
            {
                m_flag.transform.localPosition = new Vector3((vec.x - 0.55f) * 960f, (vec.y - 0.5f) * 640f, 0);
            }
            var pos = m_mapCamera.WorldToScreenPoint(m_blocks[m_flagInd].m_block.transform.position);
            m_flag.gameObject.TrySetActive(true);
            if (pos.x <= 0 || pos.x >= Screen.width)
            {
                m_flag.gameObject.TrySetActive(false);
            }
            if (pos.y <= 0 || pos.y >= Screen.height)
            {
                m_flag.gameObject.TrySetActive(false);
            }
            if (m_blocks[m_flagInd].m_block.transform.position.z <= 12)
            {
                m_flag.gameObject.TrySetActive(false);
            }
        }
    }

    void SetABCPercent(int A, int B, int C)
    {
        float pera = (float)A / (float)(A + B + C);
        float perb = (float)B / (float)(A + B + C);
        float perc = (float)C / (float)(A + B + C);
        m_perC.fillAmount = 1f;
        m_perB.fillAmount = pera + perb;
        m_perA.fillAmount = pera;
        m_numA.text = A.ToString();
        m_numB.text = B.ToString();
        m_numC.text = C.ToString();
    }

    void UpdateNewInfo()
    {
        if (m_infoList.Count > 3)
        {
            m_infoList.RemoveAt(0);
        }
        m_infoList.Clear();
        for (int i = 0; i < m_infoList.Count; i++)
        {
            m_chatItem.text += m_infoList[i] + "/n";
        }
    }

    void InitBlockData()
    {
        m_blocks = new blockData[45];
        var cft = ConfigManager.GetConfig<ConfigLegionTerritory>();
        var data = cft.m_dataArr;
        for (int i = 0; i < data.Length; i++)
        {
            m_blocks[i] = new blockData();
            m_blocks[i].m_name = data[i].TerritoryName;
            m_blocks[i].m_id = data[i].ID;
            m_blocks[i].m_legion_id = data[i].Legion;
            m_blocks[i].nearBlock = data[i].NearTerritory;
            
            m_blocks[i].orOnwer = data[i].Legion;
            m_blocks[i].Occupy = new float[3];
            m_blocks[i].Occupy[0] = 0;
            m_blocks[i].Occupy[1] = 0;
            m_blocks[i].Occupy[2] = 0;
            m_blocks[i].Occupy[m_blocks[i].orOnwer - 1] = 1;
            m_blocks[i].is_base = data[i].IsBase;
            m_blocks[i].resNum = data[i].Resource;
            m_blocks[i].worldname = data[i].TerritoryWorldName;
            if (i < 15)
            {
                m_blocks[i].m_block = m_earth.transform.Find("map_diqiu01/A/" + m_blocks[i].m_name).gameObject;
            }
            if (i >= 15&&i<30)
            {
                m_blocks[i].m_block = m_earth.transform.Find("map_diqiu01/B/" + m_blocks[i].m_name).gameObject;
            }
            if (i>=30)
            {
                m_blocks[i].m_block = m_earth.transform.Find("map_diqiu01/C/" + m_blocks[i].m_name).gameObject;
            }
        }
    }

    void ClearAllTag()
    {
        for (int i = 0; i < 45; i++)
        {
            if(m_blocks[i].Image!=null)
            {
                GameObject.Destroy(m_blocks[i].Image.gameObject);
            }
            if (m_blocks[i].point != null)
            {
                GameObject.Destroy(m_blocks[i].point.gameObject);
            }
            if (m_blocks[i].barBg != null)
            {
                GameObject.Destroy(m_blocks[i].barBg.gameObject);
            }
            if (m_blocks[i].bar1 != null)
            {
                GameObject.Destroy(m_blocks[i].bar1.gameObject);
            }
            if (m_blocks[i].bar2 != null)
            {
                GameObject.Destroy(m_blocks[i].bar2.gameObject);
            }
            if (m_blocks[i].bar3 != null)
            {
                GameObject.Destroy(m_blocks[i].bar3.gameObject);
            }
            if (m_blocks[i].txtper != null)
            {
                GameObject.Destroy(m_blocks[i].txtper.gameObject);
            }
        }
        if (m_flag != null)
        {
            GameObject.Destroy(m_flag.gameObject);
        }
    }

    void CheckAllBlock()
    {
        var data = ConfigManager.GetConfig<ConfigLegionTerritory>().m_dataArr;
        ClearAllTag();
        for (int i = 0; i < 45; i++)
        {
            if (m_blocks[i].is_base)
            {
                m_blocks[i].Image = new GameObject(m_blocks[i].m_name).AddComponent<Image>();
                UIHelper.DealImageEtc(m_blocks[i].Image);
                m_blocks[i].Image.transform.SetParent(m_tabGroup.transform);
                m_blocks[i].Image.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "base" + m_blocks[i].m_legion_id.ToString()));
                m_blocks[i].Image.SetNativeSize();
                m_blocks[i].Image.transform.localScale = new Vector3(1, 1, 1);
                GetStateImage(m_blocks[i].Image, m_blocks[i].m_block, CheckBlockState(m_blocks[i]));
            }
            else
            {
                if (CheckBlockState(m_blocks[i]) > 0)
                {
                    m_blocks[i].Image = new GameObject(m_blocks[i].m_name).AddComponent<Image>();
                    UIHelper.DealImageEtc(m_blocks[i].Image);
                    m_blocks[i].Image.transform.SetParent(m_tabGroup.transform);
                    Material myM = ResourceManager.LoadMaterial("mat_legion" + m_blocks[i].m_legion_id.ToString() + "_battle");
                    m_blocks[i].m_block.gameObject.renderer.material = myM;
                    if (m_blocks[i].m_state == 1)
                    {
                        m_blocks[i].Image.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "fight"));
                    }
                    else if (m_blocks[i].m_state == 2)
                    {
                        m_blocks[i].Image.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "attack"));
                    }
                    else if (m_blocks[i].m_state == 3)
                    {
                        m_blocks[i].Image.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "surround"));
                    }
                    else if (m_blocks[i].m_state == 4)
                    {
                        m_blocks[i].Image.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "endangered"));
                    }
                    GetStateImage(m_blocks[i].Image, m_blocks[i].m_block, CheckBlockState(m_blocks[i]));
                    m_blocks[i].Image.SetNativeSize();
                    m_blocks[i].Image.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
                    m_blocks[i].barBg = new GameObject("barbg").AddComponent<Image>();
                    UIHelper.DealImageEtc(m_blocks[i].barBg);
                    m_blocks[i].barBg.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "smallbarbg"));
                    m_blocks[i].barBg.SetNativeSize();
                    m_blocks[i].barBg.transform.SetParent(m_tabGroup.transform);

                    m_blocks[i].bar1 = new GameObject("bar").AddComponent<Image>();
                    UIHelper.DealImageEtc(m_blocks[i].bar1);
                    m_blocks[i].bar1.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "violetProgress"));
                    m_blocks[i].bar1.transform.SetParent(m_blocks[i].barBg.transform);
                    m_blocks[i].bar1.SetNativeSize();
                    m_blocks[i].bar1.transform.localScale = new Vector3(1.1f, 0.5f, 1f);
                    m_blocks[i].bar1.fillMethod = Image.FillMethod.Horizontal;
                    m_blocks[i].bar1.fillAmount = 1;

                    m_blocks[i].bar2 = new GameObject("bar").AddComponent<Image>();
                    UIHelper.DealImageEtc(m_blocks[i].bar2);
                    m_blocks[i].bar2.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "blueprogress"));
                    m_blocks[i].bar2.transform.SetParent(m_blocks[i].barBg.transform);
                    m_blocks[i].bar2.SetNativeSize();
                    m_blocks[i].bar2.transform.localScale = new Vector3(1.1f, 0.5f, 1f);
                    m_blocks[i].bar2.fillMethod = Image.FillMethod.Horizontal;
                    m_blocks[i].bar2.fillAmount = 1;

                    m_blocks[i].bar3 = new GameObject("bar").AddComponent<Image>();
                    UIHelper.DealImageEtc(m_blocks[i].bar3);
                    m_blocks[i].bar3.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "redProgress"));
                    m_blocks[i].bar3.transform.SetParent(m_blocks[i].barBg.transform);
                    m_blocks[i].bar3.SetNativeSize();
                    m_blocks[i].bar3.transform.localScale = new Vector3(1.1f, 0.5f, 1f);
                    m_blocks[i].bar3.fillMethod = Image.FillMethod.Horizontal;
                    m_blocks[i].bar3.fillAmount = 1;



                    //m_blocks[i].bar.fillAmount = (float)m_blocks[i].Occupy[m_blocks[i].m_legion_id - 1] / (float)data[i].Occupy;
                    m_blocks[i].bar1.type = Image.Type.Filled;
                    m_blocks[i].bar2.type = Image.Type.Filled;
                    m_blocks[i].bar3.type = Image.Type.Filled;

                    int[] oc = { 0, 0, 0 };
                    for (int e = 0; e < m_blocks[i].legionData.Length; e++)
                    {
                        oc[e] = m_blocks[i].legionData[e];
                    }

                    m_blocks[i].bar1.fillAmount = 1f;
                    m_blocks[i].bar2.fillAmount = (float)(oc[0] + oc[1]) / (float)(oc[0] + oc[1] + oc[2]);
                    m_blocks[i].bar3.fillAmount = (float)(oc[0]) / (float)(oc[0] + oc[1] + oc[2]);


                    m_blocks[i].barBg.transform.localScale = new Vector3(8f, 1f, 1f);
                    GetBarImage(m_blocks[i].barBg, m_blocks[i].m_block);
                    float[] a = new float[3];
                    a[0] = (float)(oc[0]) / (float)(oc[0] + oc[1] + oc[2]) * 100f;
                    a[1] = (float)(oc[1]) / (float)(oc[0] + oc[1] + oc[2]) * 100f;
                    a[2] = (float)(oc[2]) / (float)(oc[0] + oc[1] + oc[2]) * 100f;

                    m_blocks[i].txtper = new GameObject("txt").AddComponent<Text>();
                    m_blocks[i].txtper.color = Color.black;
                    m_blocks[i].txtper.fontSize = 20;
                    m_blocks[i].txtper.text = GetPer(a, (int)LegionDataManager.MyLegionId - 1).ToString() + "%";
                    m_blocks[i].txtper.transform.SetParent(m_tabGroup.transform);
                    m_blocks[i].txtper.transform.localScale = new Vector3(1f, 1f, 1f);
                    m_blocks[i].txtper.font = ResourceManager.LoadFont();
                    m_blocks[i].txtper.alignment = TextAnchor.MiddleCenter;
                    m_blocks[i].txtper.rectTransform.sizeDelta = new Vector2(60f, 40f);
                    GetText(m_blocks[i].txtper, m_blocks[i].m_block);
                }
            }
        }
        SetFlagImage();
    }

    void SetFlagImage()
    {
        var list = LegionDataManager.legionwarLegionInfoList;
        if (list == null)
        {
            return;
        }
        if (list[(int)LegionDataManager.MyLegionId - 1] == null)
        {
            return;
        }
        if (list[(int)LegionDataManager.MyLegionId - 1].mass_order > 0)
        {
            int id = 15 * (list[(int)LegionDataManager.MyLegionId - 1].mass_order / 100 - 1) + list[(int)LegionDataManager.MyLegionId - 1].mass_order % 100 - 1;
            m_flagInd = id;
            m_hasFlag = true;
            if (m_flag != null)
            {
                GameObject.Destroy(m_flag.gameObject);
            }
            m_flag = new GameObject("flag").AddComponent<Image>();
            UIHelper.DealImageEtc(m_flag);
            m_flag.transform.SetParent(m_tabGroup.transform);
            m_flag.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "aggregation"));
            m_flag.SetNativeSize();
        }
        else
        {
            m_hasFlag = true;
        }
    }

    int CheckBlockState(blockData data)
    {
        int state = 0;
        int[] blocks = { 0, 0, 0 };
        for (int i = 0; i < data.nearBlock.Length; i++)
        {
            blocks[GetBlockByID(data.nearBlock[i]).m_legion_id - 1]++;
        }
        if (blocks[data.m_legion_id-1] == data.nearBlock.Length)
        {
            state = 0;
        }
        else
        {
            state = 1;
        }
        for (int i = 0; i < 3; i++)
        {
            if (blocks[i] >= (float)data.nearBlock.Length / 2f)
            {
                if (i != data.m_legion_id-1)
                {
                    state = 2;
                }
            }
            if (blocks[i] == data.nearBlock.Length)
            {
                if (i != data.m_legion_id-1) 
                {
                    state = 3;
                }
            }
        }
        if (data.Occupy[data.m_legion_id - 1] <= 0.2f)
        {
            state = 4;
        }
        if (data.m_legion_id == LegionDataManager.MyLegionId)
        {
            if (data.Occupy[LegionDataManager.MyLegionId-1] == 1)
            {
                state = 0;
            }
        }
        bool hasNear = false;
        for (int i = 0; i < data.nearBlock.Length; i++)
        {
            int id = (data.nearBlock[i] / 100 - 1) * 15 + data.nearBlock[i] % 100 - 1;
            if (m_blocks[id].m_legion_id == LegionDataManager.MyLegionId)
            {
                hasNear = true;
            }
        }
        if (!hasNear&&data.m_legion_id!=LegionDataManager.MyLegionId)
        {
            if (state != 0)
            {
                state = -1;
            }
        }
        data.m_state = state;
        return state; 
    }

    blockData GetBlockByID(int id)
    {
        for (int i = 0; i < m_blocks.Length; i++)
        {
            if (m_blocks[i].m_id == id)
            {
                return m_blocks[i];
            }
        }
        return null;
    }


    void UpDateBlockInfo()
    {
        var list = LegionDataManager.territory_list;
        var data = ConfigManager.GetConfig<ConfigLegionTerritory>().m_dataArr;
        var llist = LegionDataManager.legionwarLegionInfoList;
        for (int i = 0; i < llist.Count; i++)
        {
            if (llist[i].id == LegionDataManager.MyLegionId)
            {
                if (llist[i].mass_order > 0)
                {
                    int id = (llist[i].mass_order / 100 - 1) * 15 + (llist[i].mass_order % 100) - 1;
                    m_blocks[id].isFlag = true;
                }
            }
        }
        m_legionBlocks[0] = 0;
        m_legionBlocks[1] = 0;
        m_legionBlocks[2] = 0;

        for (int i = 0; i < list.Length; i++)
        {
            int id = (list[i].id / 100 - 1) * 15 + (list[i].id % 100) - 1;
            if (list[i].legions != null)
            {
                for (int e = 0; e < list[i].legions.Length; e++)
                {
                    m_blocks[id].Occupy[e] = (float)list[i].legions[e] / (float)data[id].Occupy;
                }
                m_blocks[id].m_legion_id = list[i].owner;
                Material myM = ResourceManager.LoadMaterial("mat_legion"+list[i].owner.ToString());
                m_blocks[id].m_block.gameObject.renderer.material = myM;
                m_legionBlocks[list[i].owner - 1]++;
            }
            m_blocks[id].legionData = list[i].legions;
        }
    }

    void CheckRotation(GameObject go)
    {
        Vector3 vec = go.transform.eulerAngles;
        bool m_need = false;
        if (vec.x > 50 && vec.x < 100)
        {
            vec.x = 50;
            m_need = true;
        }
        else if (vec.x >= 100 &&vec.x<352)
        {
            vec.x = 352;
            m_need = true;
        }
        if (vec.y > 25 && vec.y < 100)
        {
            vec.y = 25;
            m_need = true;
        }
        else if (vec.y < 310 && vec.y >= 100)
        {
            vec.y = 310;
            m_need = true;
        }
        if (vec.z > 5 && vec.z < 100)
        {
            vec.z = 5;
            m_need = true;
        }
        else if (vec.z >= 100 && vec.z < 355)
        {
            vec.z = 355;
            m_need = true;
        }
        if (m_need)
        {
            go.transform.eulerAngles = vec;
        }
    }

    DateTime GetTargetTime(DateTime date, bool isTime)
    {
        if (isTime)
        {
            return m_endTime;
        }
        else
        {

            bool go = true;
            if (m_nowTime < m_startTime)
            {
                go = false;
            }

            if (!go)
            {
                for (int i = 0; i <m_weeklimit.Length; i++)
                {
                    if (m_weeklimit[i] == (int)date.DayOfWeek)
                    {
                        return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0);
                    }
                }
                for (int i = 0; i < m_weeklimit.Length; i++)
                {
                    if (m_weeklimit[i] > (int)date.DayOfWeek)
                    {
                        int day = m_weeklimit[i] - (int)date.DayOfWeek;
                        return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0).AddDays(day);
                    }
                }
                if ((int)date.DayOfWeek == 0)
                {
                    int day = m_weeklimit[0];
                    return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0).AddDays(day);
                }
            }
            else
            {
                for (int i = 0; i < m_weeklimit.Length; i++)
                {
                    if (m_weeklimit[i] > (int)date.DayOfWeek)
                    {
                        int day = m_weeklimit[i] - (int)date.DayOfWeek;
                        return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0).AddDays(day);
                    }
                }
                if ((int)date.DayOfWeek == 0)
                {
                    int day = m_weeklimit[0];
                    return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0).AddDays(day);
                }
                if ((int)date.DayOfWeek == 6)
                {
                    int day = 1;
                    return new DateTime(date.Year, date.Month, date.Day, startTime, startM, 0).AddDays(day);
                }
            }
        }
        return new DateTime();
    }

    void JoinGame(GameObject target, PointerEventData data)
    {
        CheckJoinGame();
    }

    void CheckJoinGame()
    {
        if (!isTime)
        {
            TipsManager.Instance.showTips("无法在休战期加入战斗");
            return;
        }
        char[] chs = m_clickBlock.name.ToCharArray();
        int id = (chs[0] - 64) * 100 + (chs[1] - 48) * 10 + chs[2] - 48;
        int bid = (chs[0] - 65) * 15 + (chs[1] - 48) * 10 + chs[2] - 48 - 1;
        if (m_blocks[bid].m_state == 0)
        {
            TipsManager.Instance.showTips("无法在和平地区加入战斗");
            return;
        }
        else if (m_blocks[bid].m_state == -1)
        {
            TipsManager.Instance.showTips("无法在未接壤的地区进入战斗");
            return;
        }
        if (!LegionDataManager.IsHasRestBattle())
        {
            UIManager.ShowTipPanel("今日四倍贡献场次已用完，继续战斗只能获得正常占领度，是否进入战斗", () => { NetLayer.Send(new tos_legionwar_start_match() { territory_id = id }); }, null, false, true);
        }
        else
        {
            NetLayer.Send(new tos_legionwar_start_match() { territory_id = id });
        }
    }

    void Toc_legionwar_set_mass_order(toc_legionwar_set_mass_order data)
    {
        if (data.territory_id > 0)
        {
            LegionDataManager.legionwarLegionInfoList[(int)LegionDataManager.MyLegionId - 1].mass_order = data.territory_id;
            SetFlagImage();
        }
    }

    void SetBattleLog()
    {
        if (LegionDataManager.battleLogsList != null)
        {
            var logList = LegionDataManager.battleLogsList;
            string[] log;
            if (logList.Count > 3)
            {
                log = new string[logList.Count];
            }
            else
            {
                log = new string[3];
            }
           
            for (int i = 0; i < log.Length; i++)
            {
                log[i] = "";
            }
            for (int i = 0; i < logList.Count; i++)
            {
                var logInfo = logList[i];
                if (logInfo.type == (int)PanelLegionWar.BattleRecordTypeEnum.beat)
                {
                    log[i] = GetLegionColor(logInfo.name1) + "军团在" + logInfo.param1 + "区战胜了" + GetLegionColor(logInfo.name2) + "军团！";
                }
                else if (logInfo.type == (int)PanelLegionWar.BattleRecordTypeEnum.occupy)
                {
                    log[i] = GetLegionColor(logInfo.name1) + "军团占领了" + logInfo.param1 + "区！";
                }
                else if (logInfo.type == (int)PanelLegionWar.BattleRecordTypeEnum.areaState)
                {
                    log[i] = logInfo.name1 + "地图正处于交战状态！";
                }
                else if (logInfo.type == (int)PanelLegionWar.BattleRecordTypeEnum.victory)
                {
                    log[i] = GetLegionColor(LegionDataManager.GetLegionName(logInfo.param1)) + "军团在" +
                        LegionDataManager.GetLegionTerritoryName(logInfo.param2) + "区取得了胜利";
                }
                else if (logInfo.type == (int)PanelLegionWar.BattleRecordTypeEnum.position)
                {
                    log[i] = logInfo.name1 + "担任了" + GetLegionColor(LegionDataManager.GetLegionName(logInfo.param1)) + "的军团长！";
                }
                var dt = TimeUtil.GetTime(logInfo.time);
                log[i] = TimeUtil.GetTime(logInfo.time).ToString("[yyyy-MM-dd] HH:mm:ss ") + log[i];
                //log[i] = TimeUtil.GetTime(logInfo.time).ToString("[yyyy-mm-dd]hh:mm:ss") + log[i];
            }
            m_chatItem.text = "";
            m_chatItem.text += log[0] + "\n";
            m_chatItem.text += log[1] + "\n";
            m_chatItem.text += log[2] + "\n";
        }
    }

    string GetLegionColor(string name)
    {
        if (name == LegionDataManager.GetLegionName(1))
        {
            return "<color=red>" + name + "</color>";
        }
        if (name == LegionDataManager.GetLegionName(2))
        {
            return "<color=cyan>" + name + "</color>";
        }
        if (name == LegionDataManager.GetLegionName(3))
        {
            return "<color=lime>" + name + "</color>";
        }
        return name;
    }
    void Toc_legionwar_start_match(toc_legionwar_start_match data)
    {
            UIManager.PopPanel<PanelMatchingTips>(null, true);
    }

    void Toc_legionwar_territory_info(toc_legionwar_territory_info data)
    {
        var a = passTo[0] as blockData;
        //Logger.Log(data.legions == null);
        a.legion_info = data.legions;
        //Logger.Log(data.legions.Length);
        a.m_legion_id = data.owner;
        passTo[0] = a;
        NetLayer.Send(new tos_legionwar_my_territory_info { territory_id=data.id });
    }
    //获得第n个军团处理后的百分比
    int GetPer(float[] per, int n)
    {
        bool max=true;
        for (int i = 0; i < per.Length; i++)
        {
            if (per[n] < per[i])
            {
                max = false;
            }
            if (per[n] == per[i])
            {
                if (n > i)
                {
                    max = false;
                }
            }
        }

        if (max)
        {
            int a = 0;
            for (int i = 0; i < 3; i++)
            {
                if(i!=n)
                {
                    a += (int)Math.Ceiling((double)per[i]);
                }
            }
            a = 100 - a;
            return a;
        }
        else
        {
            return (int)Math.Ceiling((double)per[n]);
        }
    }
}

