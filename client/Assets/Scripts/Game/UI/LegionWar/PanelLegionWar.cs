﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战(世界战况)
//创建者：HWL
//创建时间: 2016/3/19 10:37:53
///////////////////////////
public class PanelLegionWar : BasePanel
{
    private PanelFamous m_PanelFamous;
    private LegionWarLegionInfo m_LegionInfo;

    private GameObject m_WarInfoGo;//世界战况
    private GameObject m_LegionInfoGo;//军团信息
    private GameObject m_FamusGo;//名人堂

    private Text m_NameText1;
    private Text m_BattleText1;
    private Text m_WinText1;
    private Text m_CaptureText1;
    private Text m_DeathText1;
    private Text m_CurFighterText1;
    private Text m_curbattlesText1;

    private Text m_NameText2;
    private Text m_BattleText2;
    private Text m_WinText2;
    private Text m_CaptureText2;
    private Text m_DeathText2;
    private Text m_CurFighterText2;
    private Text m_curbattlesText2;

    private Text m_NameText3;
    private Text m_BattleText3;
    private Text m_WinText3;
    private Text m_CaptureText3;
    private Text m_DeathText3;
    private Text m_CurFighterText3;
    private Text m_curbattlesText3;


    private Text m_legionNameText1;
    private Text m_legionNameText2;
    private Text m_legionNameText3;
    private Text m_legionProgressText1;
    private Text m_legionProgressText2;
    private Text m_legionProgressText3;

    private Image m_bar1;
    private Image m_bar2;
    private Image m_bar3;
    

    private DataGrid m_FightRecordDataGrid;
    private const int TOTAL_AREA_NUM = 45;

    /// <summary>
    /// 战斗记录类型
    /// </summary>
    public enum BattleRecordTypeEnum
    {
        beat = 1,
        occupy = 2,
        areaState = 3,
        position = 4,
        victory = 5
    }



    public PanelLegionWar()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionWar");
        AddPreLoadRes("UI/LegionWar/PanelLegionWar", EResType.UI);
        AddPreLoadRes("Atlas/LegionWar", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }
    public override void Init()
    {
        m_WarInfoGo = m_tran.Find("WarInfo").gameObject;
        m_LegionInfoGo = m_tran.Find("LegionInfo").gameObject;
        m_FamusGo = m_tran.Find("FamousInfo").gameObject;



        Transform warTranform = m_WarInfoGo.transform;

        warTranform.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_FightRecordDataGrid = warTranform.Find("ScrollDataGrid/content").GetComponent<DataGrid>();
        m_FightRecordDataGrid.SetItemRender(warTranform.Find("ScrollDataGrid/content/FightRecord").gameObject, typeof(LegionWarFightItem));
        m_FightRecordDataGrid.useLoopItems = true;
        //军团1
        m_NameText1 = warTranform.Find("bottom/LegionName1").GetComponent<Text>();
        m_BattleText1 = warTranform.Find("bottom/LegionName1/text1").GetComponent<Text>();
        m_WinText1 = warTranform.Find("bottom/LegionName1/text2").GetComponent<Text>();
        m_CaptureText1 = warTranform.Find("bottom/LegionName1/text3").GetComponent<Text>();
        m_DeathText1 = warTranform.Find("bottom/LegionName1/text4").GetComponent<Text>();
        m_CurFighterText1 = warTranform.Find("bottom/LegionName1/text5").GetComponent<Text>();
        m_curbattlesText1 = warTranform.Find("bottom/LegionName1/text6").GetComponent<Text>();
        //军团2
        m_NameText2 = warTranform.Find("bottom/LegionName2").GetComponent<Text>();
        m_BattleText2 = warTranform.Find("bottom/LegionName2/text1").GetComponent<Text>();
        m_WinText2 = warTranform.Find("bottom/LegionName2/text2").GetComponent<Text>();
        m_CaptureText2 = warTranform.Find("bottom/LegionName2/text3").GetComponent<Text>();
        m_DeathText2 = warTranform.Find("bottom/LegionName2/text4").GetComponent<Text>();
        m_CurFighterText2 = warTranform.Find("bottom/LegionName2/text5").GetComponent<Text>();
        m_curbattlesText2 = warTranform.Find("bottom/LegionName2/text6").GetComponent<Text>();
        //军团3
        m_NameText3 = warTranform.Find("bottom/LegionName3").GetComponent<Text>();
        m_BattleText3 = warTranform.Find("bottom/LegionName3/text1").GetComponent<Text>();
        m_WinText3 = warTranform.Find("bottom/LegionName3/text2").GetComponent<Text>();
        m_CaptureText3 = warTranform.Find("bottom/LegionName3/text3").GetComponent<Text>();
        m_DeathText3 = warTranform.Find("bottom/LegionName3/text4").GetComponent<Text>();
        m_CurFighterText3 = warTranform.Find("bottom/LegionName3/text5").GetComponent<Text>();
        m_curbattlesText3 = warTranform.Find("bottom/LegionName3/text6").GetComponent<Text>();

        m_legionNameText1 = warTranform.Find("Top/LegionName1").GetComponent<Text>();
        m_legionNameText2 = warTranform.Find("Top/LegionName2").GetComponent<Text>();
        m_legionNameText3 = warTranform.Find("Top/LegionName3").GetComponent<Text>();
        m_legionProgressText1 = warTranform.Find("Top/LegionProgress1").GetComponent<Text>();
        m_legionProgressText2 = warTranform.Find("Top/LegionProgress2").GetComponent<Text>();
        m_legionProgressText3 = warTranform.Find("Top/LegionProgress3").GetComponent<Text>();


        m_bar1 = warTranform.Find("Top/progressBar/barA").GetComponent<Image>();
        m_bar2 = warTranform.Find("Top/progressBar/barB").GetComponent<Image>();
        m_bar3 = warTranform.Find("Top/progressBar/barC").GetComponent<Image>();

    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("tabbar/WarInfoToggle")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("tabbar/LegionInfoToggle")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("tabbar/famousToggle")).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += delegate { this.HidePanel(); };
        GameDispatcher.AddEventListener(GameEvent.LEGION_WAR_INFO_UPDATE, updateView);
        GameDispatcher.AddEventListener(GameEvent.LEGION_MYLEGION_INFO_UPDATE, updateMyInfoView);

        GameDispatcher.AddEventListener(GameEvent.LEGION_OFFICERS_INFO_UPDATE, updateOfficersListInfo);
        GameDispatcher.AddEventListener(GameEvent.LEGION_FAMOUS_INFO_UPDATE, updateFamousInfo);

    }


    public override void OnShow()
    {
        NetLayer.Send(new tos_legionwar_my_info() { });
        UGUIClickHandler.Get(m_tran.Find("tabbar/WarInfoToggle")).GetComponent<Toggle>().isOn = true;
        OnClickToggle(m_tran.Find("tabbar/WarInfoToggle").gameObject, null);

        updateFightRecord();
        if (HasParams())
        {
            ShowProgress();
        }
    }

    private void ShowProgress()
    {
        float total = Convert.ToSingle(m_params[0]) + Convert.ToSingle(m_params[1]) + Convert.ToSingle(m_params[2]);
        m_bar1.fillAmount = Convert.ToSingle(m_params[0]) / total;
        m_bar2.fillAmount = (Convert.ToSingle(m_params[0]) + Convert.ToSingle(m_params[1])) / total;
        m_legionProgressText1.text = m_params[0].ToString();
        m_legionProgressText2.text = m_params[1].ToString();
        m_legionProgressText3.text = m_params[2].ToString();

    }

    private void updateMyInfoView()
    {
        if (m_LegionInfoGo != null && m_LegionInfoGo.activeInHierarchy == true)
        {
            m_LegionInfo.UpdateMyInfo();
        }
    }

    private void updateOfficersListInfo()
    {
        if (m_LegionInfoGo != null && m_LegionInfoGo.activeInHierarchy == true)
        {
            if (m_LegionInfo != null)
            {
                m_LegionInfo.updateGridList();
            }
        }
    }

    private void updateFamousInfo()
    {
        if (m_FamusGo != null && m_FamusGo.activeInHierarchy == true)
        {
            if (m_PanelFamous != null)
            {
                m_PanelFamous.UpdateFramousList();
            }
        }
    }

    private void updateView()
    {
        if (m_WarInfoGo != null && m_WarInfoGo.activeInHierarchy == true)
        {
            updateLegionInfo();
        }
        else if (m_LegionInfoGo != null && m_LegionInfoGo.activeInHierarchy == true)
        {
            if (m_LegionInfo != null)
            {
                m_LegionInfo.UpdateLegionView();
            }
        }
    }

    //更新世界战况面板
    private void updateLegionInfo()
    {
        List<p_legionwar_legion_info> list = LegionDataManager.legionwarLegionInfoList;
        p_legionwar_legion_info info;

        for (int i = 0; i < list.Count; i++)
        {
            info = list[i];
            ConfigLegionLine legionData = ConfigManager.GetConfig<ConfigLegion>().GetLine(info.id);
            if (i == 0)
            {
                m_legionNameText1.text = legionData.LegionName;
                m_NameText1.text = legionData.LegionName;
                m_BattleText1.text = info.battle.ToString();
                m_WinText1.text = info.win.ToString();
                m_CaptureText1.text = info.capture.ToString();
                m_DeathText1.text = info.death.ToString();
                m_CurFighterText1.text = info.cur_fighter.ToString();
                m_curbattlesText1.text = info.cur_battles.ToString();
            }
            else if (i == 1)
            {

                m_legionNameText2.text = legionData.LegionName;
                m_NameText2.text = legionData.LegionName;
                m_BattleText2.text = info.battle.ToString();
                m_WinText2.text = info.win.ToString();
                m_CaptureText2.text = info.capture.ToString();
                m_DeathText2.text = info.death.ToString();
                m_CurFighterText2.text = info.cur_fighter.ToString();
                m_curbattlesText2.text = info.cur_battles.ToString();
            }
            else if (i == 2)
            {
                m_legionNameText3.text = legionData.LegionName;
                m_NameText3.text = legionData.LegionName;
                m_BattleText3.text = info.battle.ToString();
                m_WinText3.text = info.win.ToString();
                m_CaptureText3.text = info.capture.ToString();
                m_DeathText3.text = info.death.ToString();
                m_CurFighterText3.text = info.cur_fighter.ToString();
                m_curbattlesText3.text = info.cur_battles.ToString();
            }
        }
        updateFightRecord();
    }



    private void OnClickToggle(GameObject target, PointerEventData eventData)
    {
        if (target.name == "WarInfoToggle")
        {
            m_WarInfoGo.TrySetActive(true);
            m_LegionInfoGo.TrySetActive(false);
            m_FamusGo.TrySetActive(false);
            updateLegionInfo();
        }
        else if (target.name == "LegionInfoToggle")
        {
            m_WarInfoGo.TrySetActive(false);
            m_LegionInfoGo.TrySetActive(true);
            m_FamusGo.TrySetActive(false);

            if (null == m_LegionInfo)
            {
                m_LegionInfo = new LegionWarLegionInfo(m_tran.Find("LegionInfo"));
            }
            NetLayer.Send(new tos_legionwar_legion_officers() { legion_id = (int)LegionDataManager.MyLegionId });
            m_LegionInfo.UpdateLegionView();
            m_LegionInfo.UpdateMyInfo();
        }
        else if (target.name == "famousToggle")
        {
            NetLayer.Send(new tos_legionwar_eminentors() { });

            m_WarInfoGo.TrySetActive(false);
            m_LegionInfoGo.TrySetActive(false);
            m_FamusGo.TrySetActive(true);
            if (m_PanelFamous == null)
            {
                m_PanelFamous = new PanelFamous(m_tran.Find("FamousInfo"));
            }
            m_PanelFamous.UpdateFramousList();
        }
    }


    //更新战斗记录
    private void updateFightRecord()
    {
        List<p_legionwar_log> logList = LegionDataManager.battleLogsList;
        List<LegionWarFightRecordInfo> tempLogList = new List<LegionWarFightRecordInfo>();
        p_legionwar_log logInfo;
        object[] list = new LegionWarFightRecordInfo[12];
        for (int i = 0; i < logList.Count; i++)
        {
            logInfo = logList[i];
            var item = new LegionWarFightRecordInfo();
            if (logInfo.type == (int)BattleRecordTypeEnum.beat)
            {
                item.content = LegionDataManager.GetLegionName(logInfo.param1) + "军团在" +
                    LegionDataManager.GetLegionTerritoryName(logInfo.param2) +
                    "区战胜了" + LegionDataManager.GetLegionName(logInfo.param3) + "军团！";
            }
            else if (logInfo.type == (int)BattleRecordTypeEnum.occupy)
            {
                item.content = LegionDataManager.GetLegionName(logInfo.param1) + "军团占领了" +
                    LegionDataManager.GetLegionTerritoryName(logInfo.param2) + "区！";
            }
            else if (logInfo.type == (int)BattleRecordTypeEnum.areaState)
            {
                item.content = LegionDataManager.GetLegionTerritoryName(logInfo.param2) + "地图正处于交战状态！";
            }
            else if (logInfo.type == (int)BattleRecordTypeEnum.position)
            {
                item.content = logInfo.name1 + "担任了" + LegionDataManager.GetLegionName(logInfo.param1) + "的军团长！";
            }
            else if (logInfo.type == (int)BattleRecordTypeEnum.victory)
            {
                item.content = LegionDataManager.GetLegionName(logInfo.param1) + "军团在" +
                    LegionDataManager.GetLegionTerritoryName(logInfo.param2) + "区取得了胜利";
            }
            item.time = TimeUtil.GetTime(logInfo.time).ToString("[yyyy-MM-dd]HH:mm:ss");
            tempLogList.Add(item);
        }
        m_FightRecordDataGrid.Data = tempLogList.ToArray();
    }

    //清除面板信息
    private void clearPanelInfo()
    {
        m_legionNameText1.text = "";
        m_legionNameText2.text = "";
        m_legionNameText3.text = "";
        m_legionProgressText1.text = "";
        m_legionProgressText2.text = "";
        m_legionProgressText3.text = "";
    }

    public override void OnHide()
    {
        clearPanelInfo();
        base.HidePanel();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_WAR_INFO_UPDATE, updateView);
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_MYLEGION_INFO_UPDATE, updateMyInfoView);

        GameDispatcher.RemoveEventListener(GameEvent.LEGION_OFFICERS_INFO_UPDATE, updateOfficersListInfo);
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_FAMOUS_INFO_UPDATE, updateFamousInfo);
    }

    public override void Update()
    {
    }
}

/// <summary>
/// 军团名
/// </summary>
public enum LegionNameIdEnum
{
    XUESELIMING = 1,
    ZHIYOUZHIYI = 2,
    CANGQIONGZHIGUANG = 3
}