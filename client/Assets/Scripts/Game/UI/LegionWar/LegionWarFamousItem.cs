﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：名人堂项
//创建者：HWL
//创建时间: 2016/3/19 16:10:32
///////////////////////////

public class LegionWarFamousItem : ItemRender
{
    private Text m_countText;
    private Text m_playerNameText;
    private Text m_corpNameText;
    private Text m_serverNameText;
    private Text m_rankText;
    private Image m_numIcon;
    private Image m_headIcon;
    private Image m_bg;

    public override void Awake()
    {
        m_countText = transform.Find("count").GetComponent<Text>();
        m_playerNameText = transform.Find("playerName").GetComponent<Text>();
        m_corpNameText = transform.Find("corpName").GetComponent<Text>();
        m_serverNameText = transform.Find("serverName").GetComponent<Text>();
        m_rankText = transform.Find("numText").GetComponent<Text>();

        m_numIcon = transform.Find("numIcon").GetComponent<Image>();
        m_headIcon = transform.Find("headIcon").GetComponent<Image>();
        m_bg = transform.Find("bg").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        LegionWarFamousInfo item = data as LegionWarFamousInfo;
        m_countText.text = "<color='#00ff00'>" + item.m_count + "</color>";
        m_playerNameText.text = item.m_playerName;
        m_corpNameText.text = item.m_corpName;
        m_serverNameText.text = item.m_serverName;
        if(item.m_rank < 3)
        {
            m_numIcon.gameObject.TrySetActive(true);
            m_rankText.text = "";
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "topBg" + (item.m_rank + 1)));
            m_numIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "top" + (item.m_rank + 1)));
        }
        else
        {
            m_numIcon.gameObject.TrySetActive(false);
            m_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "famousBg"));
            m_rankText.text = (item.m_rank + 1).ToString();
        }
        m_headIcon.SetSprite(ResourceManager.LoadRoleIcon(item.m_headIcon));
    }
}

public class LegionWarFamousInfo
{
    public string m_playerName;
    public string m_serverName;
    public int m_count;
    public int m_rank;
    public string m_corpName;
    public int m_headIcon;

}
