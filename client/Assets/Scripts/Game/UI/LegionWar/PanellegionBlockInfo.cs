﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PanellegionBlockInfo : BasePanel
{
    public class ItemLegionSoilder : ItemRender
    {
        Text m_name;
        Text m_severName;
        Image m_head;
        Image m_headbg;
        Image m_image;

        public override void Awake()
        {
            m_name = transform.Find("name").GetComponent<Text>();
            m_severName = transform.Find("server").GetComponent<Text>();
            m_head = transform.Find("headbg/head").GetComponent<Image>();
            m_headbg = transform.Find("headbg").GetComponent<Image>();
            m_image = transform.Find("Image").GetComponent<Image>();
        }

        protected override void OnSetData(object data) 
        {
            if (data == null)
            {
                m_name.gameObject.TrySetActive(false);
                m_severName.gameObject.TrySetActive(false);
                m_head.gameObject.TrySetActive(false);
                m_headbg.gameObject.TrySetActive(false);
                m_image.gameObject.TrySetActive(false);
                return;
            }
            var info = data as p_legionwar_officer;
            m_name.text = info.name;
            m_head.SetSprite(ResourceManager.LoadRoleIcon(info.icon));
            m_severName.text = PlayerSystem.GetRoleSeverName(info.player_id);
        }
    }


    public PanellegionBlockInfo()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionBlockInfo");
        AddPreLoadRes("UI/LegionWar/PanelLegionBlockInfo", EResType.UI);
        AddPreLoadRes("Atlas/LegionWar", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    int blockid;
    Text m_blockName;

    Text m_state;
    Text m_resNum;
    Text m_blockState;
    Text m_blockHistory;
    Text m_myRecord;

    Image m_perA;
    Image m_perB;
    Image m_perC;

    DataGrid m_Asoilder;
    DataGrid m_Bsoilder;
    DataGrid m_Csoilder;

    Text m_nameA;
    Text m_nameB; 
    Text m_nameC;

    Text m_hisA;
    Text m_hisB;
    Text m_hisC;
    PanelLegionMap.blockData m_blockData;
    ConfigLegionTerritory m_clt;

    p_legionwar_officer[] m_groupA;
    p_legionwar_officer[] m_groupB;
    p_legionwar_officer[] m_groupC;

    GameObject m_btnFlag;
    GameObject m_btnJoin;
    Text m_flag;

    Text m_legion1;
    Text m_legion2;
    Text m_legion3;

    Image m_owner;

    //UIEffect m_perUIeffect;
    public override void Init()
    {
        m_legion1 = m_tran.Find("leftGroup/LegionGroup/Legion1/Text").GetComponent<Text>();
        m_legion2 = m_tran.Find("leftGroup/LegionGroup/Legion2/Text").GetComponent<Text>();
        m_legion3 = m_tran.Find("leftGroup/LegionGroup/Legion3/Text").GetComponent<Text>();
        m_owner = m_tran.Find("owner").GetComponent<Image>();
        m_blockName = m_tran.Find("blockName").GetComponent<Text>();
        m_perA = m_tran.Find("leftGroup/perGroup/perA").GetComponent<Image>();
        m_perB = m_tran.Find("leftGroup/perGroup/perB").GetComponent<Image>();
        m_perC = m_tran.Find("leftGroup/perGroup/perC").GetComponent<Image>();
        m_blockHistory = m_tran.Find("leftGroup/historyGroup/history").GetComponent<Text>();
        m_hisA = m_tran.Find("leftGroup/historyGroup/history/A").GetComponent<Text>();
        m_hisB = m_tran.Find("leftGroup/historyGroup/history/B").GetComponent<Text>();
        m_hisC = m_tran.Find("leftGroup/historyGroup/history/C").GetComponent<Text>();
        m_myRecord = m_tran.Find("leftGroup/myGroup/data").GetComponent<Text>();
        m_state = m_tran.Find("rightGroup/title/state").GetComponent<Text>();
        m_resNum = m_tran.Find("rightGroup/title/resource").GetComponent<Text>();
        m_blockState = m_tran.Find("rightGroup/title/battleStatus").GetComponent<Text>();
        m_nameA = m_tran.Find("rightGroup/infoGroup/infoA/name").GetComponent<Text>();
        m_nameB = m_tran.Find("rightGroup/infoGroup/infoB/name").GetComponent<Text>();
        m_nameC = m_tran.Find("rightGroup/infoGroup/infoC/name").GetComponent<Text>();

        m_Asoilder = m_tran.Find("rightGroup/infoGroup/infoA/playerGroup").gameObject.AddComponent<DataGrid>();
        m_Asoilder.SetItemRender(m_tran.Find("ItemPlayer").gameObject, typeof(ItemLegionSoilder)); 
        m_Bsoilder = m_tran.Find("rightGroup/infoGroup/infoB/playerGroup").gameObject.AddComponent<DataGrid>();
        m_Bsoilder.SetItemRender(m_tran.Find("ItemPlayer").gameObject, typeof(ItemLegionSoilder));
        m_Csoilder = m_tran.Find("rightGroup/infoGroup/infoC/playerGroup").gameObject.AddComponent<DataGrid>();
        m_Csoilder.SetItemRender(m_tran.Find("ItemPlayer").gameObject, typeof(ItemLegionSoilder));

        m_btnFlag = m_tran.Find("rightGroup/btnGroup/flag").gameObject;
        m_clt = ConfigManager.GetConfig<ConfigLegionTerritory>();
        m_blockData = new PanelLegionMap.blockData();
        InitGroup();
        

        ConfigLegion cl = ConfigManager.GetConfig<ConfigLegion>();
        m_nameA.text = cl.GetLine(1).LegionName;
        m_nameB.text = cl.GetLine(2).LegionName;
        m_nameC.text = cl.GetLine(3).LegionName;

        blockid = 0;

        m_flag = m_tran.Find("rightGroup/title/jijie").GetComponent<Text>();
        m_btnJoin = m_tran.Find("rightGroup/btnGroup/Join").gameObject;

        //m_perUIeffect = UIEffect.ShowEffectAfter(m_tran.Find("leftGroup/perGroup"), EffectConst.UI_JUNTUANSHILIFENBU, new Vector3(-314.5f, 148, 0));
       
    }

    void InitGroup()
    {
        m_groupA = new p_legionwar_officer[3];
        m_groupA[0] = null;
        m_groupA[1] = null;
        m_groupA[2] = null;

        m_groupB = new p_legionwar_officer[3];
        m_groupB[0] = null;
        m_groupB[1] = null;
        m_groupB[2] = null;

        m_groupC = new p_legionwar_officer[3];
        m_groupC[0] = null;
        m_groupC[1] = null;
        m_groupC[2] = null;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_btnFlag.gameObject).onPointerClick += SetFlag;
        UGUIClickHandler.Get(m_tran.Find("rightGroup/btnGroup/Join")).onPointerClick += JoinGame;
    }

    void SetFlag(GameObject target, PointerEventData data)
    {
        UIManager.ShowTipPanel("确定要在" + m_blockName.text + "设立集结令吗", () =>
        {
            SetFlag();
        }, null, true, true, "确定");
    }

    void SetFlag()
    {
        char[] chs = m_blockName.text.ToCharArray();
        int id = (chs[0] - 64) * 100 + (chs[1]-48) * 10 + chs[2]-48;
        NetLayer.Send(new tos_legionwar_set_mass_order() { territory_id = id });
    }

    void JoinGame(GameObject target, PointerEventData data)
    {
        if (!PanelLegionMap.isTime)
        {
            TipsManager.Instance.showTips("无法在休战期加入战斗");
            return;
        }
        if (!LegionDataManager.IsHasRestBattle())
        {
            UIManager.ShowTipPanel("今日四倍贡献场次已用完，继续战斗只能获得正常占领度，是否进入战斗", () => { NetLayer.Send(new tos_legionwar_start_match() { territory_id = blockid }); }, null, false, true);
        }
        else
        {
            NetLayer.Send(new tos_legionwar_start_match() { territory_id = blockid });
        }
        if (PlayerSystem.roleData.level >= 15)
        {

        }

    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        //if (m_perUIeffect != null)
        //{
        //    m_perUIeffect.Destroy();
        //    m_perUIeffect = null;
        //}
    }

    public override void OnShow()
    {
        //Logger.Log(m_perUIeffect == null);
        //if (m_perUIeffect == null)
        //{
        //    Logger.Log("do");
        //    m_perUIeffect = UIEffect.ShowEffectAfter(m_tran.Find("leftGroup/perGroup"), EffectConst.UI_JUNTUANSHILIFENBU, new Vector3(-314.5f, 148, 0));
        //}
        m_Asoilder.Data = new object[0];  
        m_Bsoilder.Data = new object[0];
        m_Csoilder.Data = new object[0];
        InitGroup();
        if (LegionDataManager.MyCurLegionPosition != 1)
        {
            m_btnFlag.TrySetActive(false);
        }
        else
        {
            m_btnFlag.TrySetActive(true);
        }
        if (HasParams())
        {
            var data = m_params[0] as PanelLegionMap.blockData;
            var mydata = m_params[1] as toc_legionwar_my_territory_info;
            blockid = data.m_id;
            m_blockName.text = data.m_name + data.worldname;
            m_blockData = data;
            int[] joins = { 0, 0, 0 };
            int[] deads = { 0, 0, 0 };
            int[] wins = { 0, 0, 0 }; 
            int[] a = { 0, 0, 0 };
            a[0] = data.legionData[0];
            a[1] = data.legionData[1];
            a[2] = data.legionData[2];
            for (int i = 0; i < data.legion_info.Length; i++)
            {
                
                joins[data.legion_info[i].legion - 1] = data.legion_info[i].battle;
                deads[data.legion_info[i].legion - 1] = data.legion_info[i].death;
                wins[data.legion_info[i].legion - 1] = data.legion_info[i].win;
                for (int e = 0; e < data.legion_info[i].bestors.Length; e++)
                {
                    if (data.legion_info[i].legion == 1)
                    {
                        m_groupA[e] = data.legion_info[i].bestors[e];
                    }
                    if (data.legion_info[i].legion == 2)
                    {
                        m_groupB[e] = data.legion_info[i].bestors[e];
                    }
                    if (data.legion_info[i].legion == 3)
                    {
                        m_groupC[e] = data.legion_info[i].bestors[e];
                    }
                }
            }
            SetABCPercent(a[0], a[1], a[2]);
            SetBlockHistory(joins, deads, wins);
            m_Asoilder.Data = m_groupA;
            m_Bsoilder.Data = m_groupB;
            m_Csoilder.Data = m_groupC;
            SetMyHistory(mydata);
            SetbBlockState(data);
            var cfg = ConfigManager.GetConfig<ConfigLegion>();

            m_flag.gameObject.TrySetActive(false);
            for (int i = 0; i < LegionDataManager.legionwarLegionInfoList.Count; i++)
            {
                if (LegionDataManager.legionwarLegionInfoList[i].mass_order > 0)
                {
                    if (LegionDataManager.legionwarLegionInfoList[i].mass_order == data.m_id)
                    {
                        m_flag.gameObject.TrySetActive(true);
                        m_flag.text = cfg.GetLine(LegionDataManager.legionwarLegionInfoList[i].id).LegionName + "集结令";
                    }
                }
            }
            m_owner.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + data.m_legion_id.ToString()));
        }
        

    }

    public override void Update()
    {
        
    }   

    void SetABCPercent(int A, int B, int C)
    {

        float pera = (float)A / (float)(A + B + C);
        float perb = (float)B / (float)(A + B + C);
        float perc = (float)C / (float)(A + B + C);
        m_perC.fillAmount = 1f;
        m_perB.fillAmount = pera + perb;
        m_perA.fillAmount = pera;
        float[] a = new float[3];
        a[0] = pera * 100f;
        a[1] = perb * 100f;
        a[2] = perc * 100f;

        m_legion1.text = GetPer(a, 0).ToString() + "%";
        m_legion2.text = GetPer(a, 1).ToString() + "%";
        m_legion3.text = GetPer(a, 2).ToString() + "%";


    }

    string CheckIntHis(int info)
    {
        if (info / 100000 > 0)
        {
            return ((float)info / (float)1000).ToString("0.0") + "k";
        }
        return info.ToString();
    }

    void SetBlockHistory(int[] join,int[] dead,int[] win)
    {
        m_hisA.text = CheckIntHis(join[0]) + "\n" + CheckIntHis(dead[0]) + "\n" + CheckIntHis(win[0]);
        m_hisB.text = CheckIntHis(join[1]) + "\n" + CheckIntHis(dead[1]) + "\n" + CheckIntHis(win[1]);
        m_hisC.text = CheckIntHis(join[2]) + "\n" + CheckIntHis(dead[2]) + "\n" + CheckIntHis(win[2]);
    }

    void SetbBlockState(PanelLegionMap.blockData data)
    {
        m_btnJoin.TrySetActive(true);
        if (data.m_state == 0)
        {
            m_state.text = "和平";
            m_blockState.text = "状态：和平";
            m_btnJoin.TrySetActive(false);
        }
        if (data.m_state == -1)
        {
            m_state.text = "交战中";
            m_btnJoin.TrySetActive(false);
        }
        if (data.m_state > 0 && data.m_legion_id == LegionDataManager.MyLegionId)
        {
            m_state.text = "交战中<color=lime>(防守)</color>";
        }
        else if (data.m_state > 0 && data.m_legion_id != LegionDataManager.MyLegionId)
        {
            m_state.text = "交战中<color=red>(进攻)</color>";
        }
       
        if (data.m_state == 1)
        {
            m_blockState.text = "状态：<color=orange>交战中</color>";
        }
        else if (data.m_state == 2)
        {
            m_blockState.text = "状态：<color=lime>夹击</color>";
        }
        else if (data.m_state == 3)
        {
            m_blockState.text = "状态：<color=cyan>被包围</color>";
        }
        else if (data.m_state == 4)
        {
            m_blockState.text = "状态：<color=red>濒危</color>";
        }
        else if (data.m_state == -1)
        {
            m_blockState.text = "状态：<color=orange>交战中</color>";
        }
        m_resNum.text = "资源总量:<color=lime>" + m_clt.GetLine(data.m_id).Resource.ToString() + "</color>";
        
    }

    void SetMyHistory(toc_legionwar_my_territory_info data)
    {
        m_myRecord.text = data.battle.ToString() + "\n"
                        + data.win.ToString() + "\n"
                        + data.kill.ToString() + "\n"
                        + data.death.ToString() + "\n"
                        + data.contribution.ToString();
    }

    int GetPer(float[] per, int n)
    {
        bool max = true;
        for (int i = 0; i < per.Length; i++)
        {
            if (per[n] < per[i])
            {
                max = false;
            }
            if (per[n] == per[i])
            {
                if (n > i)
                {
                    max = false;
                }
            }
        }

        if (max)
        {
            int a = 0;
            for (int i = 0; i < 3; i++)
            {
                if (i != n)
                {
                    a += (int)Math.Ceiling((double)per[i]);
                }
            }
            a = 100 - a;
            return a;
        }
        else
        {
            return (int)Math.Ceiling((double)per[n]);
        }
    }
}

