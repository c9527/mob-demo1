﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战排行榜
//创建者：HWL
//创建时间: 2016/3/22 15:10:34
///////////////////////////

public class PanelLegionRank : BasePanel
{
    private Toggle m_personalRankToggle;
    private Toggle m_CorpsRankToggle;
    private DataGrid m_rankDataGrid;
    private DataGrid m_CorpsRankDataGrid;

    private GameObject m_rankDataListGo;
    private GameObject m_CorpsRankDataListGo;
    private GameObject m_rankTitleGo;//个人的title
    private GameObject m_corpsRankTitleGo;//战队的title
    private const int MAX_NUM = 30;
    Text m_page;
    int page;
    int maxPage = 1;
    private GameObject m_buddleTipGo;
    private int m_rankType = 1;

    private GameObject m_rewardBtnGo;
    private GameObject m_rewardDescBtnGo;
    private List<int> serverIdList = new List<int>();

    public PanelLegionRank()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionRank");
        AddPreLoadRes("UI/LegionWar/PanelLegionRank", EResType.UI);
    }

    public override void Init()
    {
        m_personalRankToggle = m_tran.Find("tabbar/personalRankToggle").GetComponent<Toggle>();
        m_CorpsRankToggle = m_tran.Find("tabbar/LegionCorpsRankToggle").GetComponent<Toggle>();

        m_rankTitleGo = m_tran.Find("titleBG").gameObject;
        m_corpsRankTitleGo = m_tran.Find("corpstitleBG").gameObject;

        m_rankDataListGo = m_tran.Find("RankList").gameObject;
        m_rankDataGrid = m_tran.Find("RankList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_rankDataGrid.SetItemRender(m_tran.Find("RankList/ScrollDataGrid/content/RankItem").gameObject, typeof(LegionRankItem));
        //战队排行
        m_CorpsRankDataListGo = m_tran.Find("CorpsRankList").gameObject;
        m_CorpsRankDataGrid = m_tran.Find("CorpsRankList/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_CorpsRankDataGrid.SetItemRender(m_tran.Find("CorpsRankList/ScrollDataGrid/content/RankItem").gameObject, typeof(LegionCorpsRankItem));
        m_buddleTipGo = m_tran.Find("right/getRewardBtn/bubble_tip").gameObject;
        m_page = m_tran.Find("page/page").GetComponent<Text>();
        m_rankDataGrid.useLoopItems = false;
        m_rankDataGrid.autoSelectFirst = true;

        m_CorpsRankDataGrid.useLoopItems = false;
        m_rankDataGrid.autoSelectFirst = false;

        m_rewardBtnGo = m_tran.Find("right/getRewardBtn").gameObject;
        m_rewardDescBtnGo = m_tran.Find("right/RewardDescBtn").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("right/getRewardBtn")).onPointerClick += GetAwardHandle;
        UGUIClickHandler.Get(m_tran.Find("right/RewardDescBtn")).onPointerClick += openRewardDescView;
        m_rankDataGrid.onItemSelected += onSelect;

        UGUIClickHandler.Get(m_tran.Find("page/left")).onPointerClick += delegate { SendMsg(false); };
        UGUIClickHandler.Get(m_tran.Find("page/right")).onPointerClick += delegate { SendMsg(true); };
        GameDispatcher.AddEventListener(GameEvent.LEGION_BUBBLE_TIP, updateBuddle);


        UGUIClickHandler.Get(m_personalRankToggle.gameObject).onPointerClick += OnClickToggle;
        UGUIClickHandler.Get(m_CorpsRankToggle.gameObject).onPointerClick += OnClickToggle;

    }

    private void OnClickToggle(GameObject target, PointerEventData eventData)
    {
        page = 1;
        if (target.name == "personalRankToggle")
        {
            m_rankType = 1;
            NetLayer.Send(new tos_legionwar_fighter_ranks() { page = page });
        }
        else if (target.name == "LegionCorpsRankToggle")
        {
            m_rankType = 2;
            NetLayer.Send(new tos_legionwar_corps_ranks() { page = page });
        }
        updateView();
    }

    private void updateBuddle()
    {
        if (m_buddleTipGo != null)
        {
            m_buddleTipGo.TrySetActive(LegionDataManager.have_reward);
        }
    }

    public void SendMsg(bool add)
    {
        if (add)
        {
            if (page >= maxPage)
            {
                return;
            }
            else
            {
                page++;
            }
        }
        else
        {
            if (page <= 1)
            {
                return;
            }
            else
            {
                page--;
            }
        }
        if( m_rankType == 1 )
        {
            NetLayer.Send(new tos_legionwar_fighter_ranks() { page = page });
        }
        else if (m_rankType == 2)
        {
            NetLayer.Send(new tos_legionwar_corps_ranks() { page = page });
        }

    }

    private void onSelect(object renderData)
    {
    }

    public override void OnShow()
    {
        m_personalRankToggle.isOn = true;
        m_rankType = 1;
        NetLayer.Send(new tos_legionwar_fighter_ranks() { page = 1 });
        m_buddleTipGo.TrySetActive(LegionDataManager.have_reward);
        updateView();
    }

    private void updateView()
    {
        if( m_rankType == 1 )
        {
            m_rankDataListGo.TrySetActive(true);
            m_rankTitleGo.TrySetActive(true);
            m_corpsRankTitleGo.TrySetActive(false);
            m_CorpsRankDataListGo.TrySetActive(false);
            m_rewardBtnGo.TrySetActive(true);
            m_rewardDescBtnGo.TrySetActive(true);
        }
        else
        {
            m_CorpsRankDataListGo.TrySetActive(true);
            m_rankDataListGo.TrySetActive(false);
            m_rankTitleGo.TrySetActive(false);
            m_corpsRankTitleGo.TrySetActive(true);
            m_rewardBtnGo.TrySetActive(false);
            m_rewardDescBtnGo.TrySetActive(false);
        }
    }


    private void Toc_legionwar_fighter_ranks(toc_legionwar_fighter_ranks ranks)
    {
        List<LegionRankItemInfo> list = new List<LegionRankItemInfo>();
        p_legionwar_fighter rankData;
        for (int i = 0; i < ranks.list.Length; i++)
        {
            rankData = ranks.list[i];
            LegionRankItemInfo info = new LegionRankItemInfo();
            info.id = rankData.player_id;
            info.rank = ranks.start + i;
            info.level = rankData.level;
            info.playerName = rankData.name;
            info.corpName = rankData.corps_name;
            info.devote = rankData.contribution;
            info.legionid = rankData.legion_id;
            info.serverName = PlayerSystem.GetRoleSeverName(info.id);
            info.job = rankData.position;
            list.Add(info);
        }
        m_rankDataGrid.Data = list.ToArray();

        if (ranks.size != 0)
        {
            maxPage = ranks.size % MAX_NUM == 0 ? ranks.size / MAX_NUM : (ranks.size / MAX_NUM + 1);
            maxPage = maxPage == 0 ? 1 : maxPage;
        }
        m_page.text = ranks.page + "/" + maxPage;
    }

    /// <summary>
    /// 战队排行
    /// </summary>
    /// <param name="ranks"></param>
    private void Toc_legionwar_corps_ranks(toc_legionwar_corps_ranks ranks)
    {
        List<LegionCorpRankItemInfo> list = new List<LegionCorpRankItemInfo>();
        p_legionwar_corps rankData;
        for (int i = 0; i < ranks.list.Length; i++)
        {
            rankData = ranks.list[i];
            LegionCorpRankItemInfo info = new LegionCorpRankItemInfo();
            info.corpId = rankData.corps_id;
            info.rank = ranks.start + i;
            info.name = rankData.name;
            info.contribution = rankData.contribution;
            info.legion_id = rankData.legion_id;
            info.serverName = PlayerSystem.GetRoleSeverName(rankData.corps_id); ;
            info.member_cnt = rankData.member_cnt;
            info.leader = rankData.leader;
            list.Add(info);
        }
        m_CorpsRankDataGrid.Data = list.ToArray();

        if (ranks.size != 0)
        {
            maxPage = ranks.size % MAX_NUM == 0 ? ranks.size / MAX_NUM : (ranks.size / MAX_NUM + 1);
            maxPage = maxPage == 0 ? 1 : maxPage;
        }
        m_page.text = ranks.page + "/" + maxPage;
    }


    //领取奖励
    private void GetAwardHandle(GameObject target, PointerEventData data)
    {
        if( LegionDataManager.have_reward == true )
        {
            NetLayer.Send(new tos_legionwar_get_reward() { });
        }
        else
        {
            TipsManager.Instance.showTips("没有奖励可以领取");
        }
    }

    private void Toc_legionwar_get_reward(toc_legionwar_get_reward reward)
    {
        PanelRewardItemTipsWithEffect.DoShow(reward.item_list, true);
    }

    //打开奖励说明面板
    private void openRewardDescView( GameObject target, PointerEventData data)
    {
        string content = "";
        ConfigLegionIntro infoTro = ConfigManager.GetConfig<ConfigLegionIntro>();
        if (infoTro == null)
        {
            Logger.Log("legion position config data is null");
        }
        if (infoTro != null)
        {
            ConfigLegionIntroLine introLine = infoTro.GetLine("Rank");
            content = introLine == null ? "未知" : introLine.Content.Replace("\\n", "\n");
        }
        UIManager.PopRulePanel("LegionReward");
    }

    public override void OnHide()
    {
        base.HidePanel();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.LEGION_BUBBLE_TIP, updateBuddle);
    }

    public override void Update()
    {
    }

    
    private void getSever(long legionid)
    {
        ConfigMisc misc = ConfigManager.GetConfig<ConfigMisc>();
        serverIdList.Clear();
        if( misc != null )
        {
            ConfigMiscLine miscLine = misc.GetLine("server_legion_list");
            if( miscLine != null )
            {
                string[] info = miscLine.ValueStr.Split(';');
                for( int i = 0; i < info.Length;i++ )
                {
                    string[] str = info[i].Split('#');
                    if (legionid == long.Parse(str[1]))
                    {
                        serverIdList.Add(int.Parse(str[0]));
                    }
                }
            }
        }
    }
}
