﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战战队排行榜项
//创建者：HWL
//创建时间: 2016/6/24 15:27:48
public class LegionCorpsRankItem : ItemRender {
    private Image m_selectImg;
    private Image m_myselFlagImg;
    private Image m_rankIcon;
    private Text m_rankText;
    private Text m_corpNameText;
    private Text m_serverNameText;
    private Text m_leaderNameText;
    private Text m_devoteText;
    private Text m_legionNameText;
    private Text m_corpMemberNum;

    public override void Awake()
    {
        m_selectImg = transform.Find("selectImg").GetComponent<Image>();
        m_myselFlagImg = transform.Find("myselfImg").GetComponent<Image>();

        m_rankIcon = transform.Find("rank/rankImg").GetComponent<Image>();
        m_rankText = transform.Find("rank").GetComponent<Text>();
        m_corpNameText = transform.Find("CorpNameText").GetComponent<Text>();
        m_serverNameText = transform.Find("serverText").GetComponent<Text>();
        m_legionNameText = transform.Find("legionNameText").GetComponent<Text>();
        m_devoteText = transform.Find("devoteText").GetComponent<Text>();
        m_leaderNameText = transform.Find("leaderNameText").GetComponent<Text>();

        m_corpMemberNum = transform.Find("corpMemberNumTxt").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        LegionCorpRankItemInfo item = data as LegionCorpRankItemInfo;
        m_rankIcon.gameObject.TrySetActive(false);
        if (item.rank < 4)
        {
            m_rankIcon.gameObject.TrySetActive(true);
            m_rankIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round3_" + item.rank));
            m_rankText.text = "";
        }
        else
        {
            m_rankText.text = item.rank.ToString();
        }
        m_myselFlagImg.gameObject.TrySetActive(false);
        if (CorpsDataManager.m_corpsData != null && CorpsDataManager.m_corpsData.corps_id == item.corpId)
        {
            m_myselFlagImg.gameObject.TrySetActive(true);
        }
        m_corpNameText.text = item.name;
        m_serverNameText.text = item.serverName;
        ConfigLegionLine legionLine = ConfigManager.GetConfig<ConfigLegion>().GetLine(item.legion_id);
        m_legionNameText.text = legionLine == null ? "" : legionLine.LegionName;
        m_devoteText.text = item.contribution.ToString();
        m_corpMemberNum.text = item.member_cnt.ToString();
        m_leaderNameText.text = item.leader;
    }
}

public class LegionCorpRankItemInfo
{
    public long corpId;
    public int rank;
    public string name;
    public string leader;
    public int member_cnt;
    public int legion_id;
    public int contribution;//贡献
    public string serverName;
}