﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团战历史职位面板
//创建者：HWL
//创建时间: 2016/3/21 15:38:07
///////////////////////////
public class PanelLegionWarMyHistoryJob : BasePanel
{
    private Text[] m_jobTextList = new Text[5];
    private Text[] m_jobCountTextList = new Text[5];

    public PanelLegionWarMyHistoryJob()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionWarMyHistoryJob");
        AddPreLoadRes("UI/LegionWar/PanelLegionWarMyHistoryJob", EResType.UI);
    }
    public override void Init()
    {
        for (int i = 0; i < 5; i++ )
        {
            m_jobTextList[i] = m_tran.Find("job_txt" + (i+1)).GetComponent<Text>();
            m_jobCountTextList[i] = m_tran.Find("times_txt" + (i + 1)).GetComponent<Text>();
        }
    }

    public override void InitEvent()
    {
        GameDispatcher.AddEventListener(GameEvent.LEGION_MYLEGION_INFO_UPDATE, updateView);
        UGUIClickHandler.Get(m_tran.Find("ok_btn")).onPointerClick += delegate{ this.HidePanel(); };
    }

    public override void OnShow()
    {
        updateView();
    }

    private void updateView()
    {
        clearTextInfo();
        toc_legionwar_my_info.HistoryPosition[] historyPositionList = LegionDataManager.HistoryPositionList as toc_legionwar_my_info.HistoryPosition[];
        if( historyPositionList == null )
        {
            Logger.Log("historyPositionList is null");
            return;
        }
        string positionName = "";
        ConfigLegionPositionLine positionLine;

        for (int i = 0; i < historyPositionList.Length; i++)
        {
            positionLine = ConfigManager.GetConfig<ConfigLegionPosition>().GetLine(historyPositionList[i].position);
            if (positionLine != null)
            {
                positionName = ConfigManager.GetConfig<ConfigLegionPosition>().GetLine(historyPositionList[i].position).PositionName;
            }
            else
            {
                positionName = "无职位";
            }
            m_jobTextList[i].text = positionName;
            m_jobCountTextList[i].text = historyPositionList[i].times + "次";
        }
    }

    private void clearTextInfo()
    {
        for( int i = 0; i < 5;i++ )
        {
            m_jobCountTextList[i].text = "";
            m_jobTextList[i].text = "";
        }
    }

    public override void OnHide()
    {
        base.HidePanel();
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}
