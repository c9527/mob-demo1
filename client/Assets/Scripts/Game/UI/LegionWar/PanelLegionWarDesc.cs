﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PanelLegionWarDesc : BasePanel
{
    public PanelLegionWarDesc()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionWarDesc");
        AddPreLoadRes("UI/LegionWar/PanelLegionWarDesc", EResType.UI);
        AddPreLoadRes("Atlas/LegionWar", EResType.Atlas);
    }

    Dictionary<string,string> m_desDic;
    Text m_text;
    Toggle m_intro;
    Toggle m_block;
    Toggle m_other;
    Toggle m_pos;
    public override void Init()
    {
        m_desDic = new Dictionary<string,string>();
        var data = ConfigManager.GetConfig<ConfigLegionIntro>().m_dataArr;
        for (int i = 0; i < data.Length; i++)
        {
            m_desDic.Add(data[i].TabName, data[i].Content.Replace("\\n", "\n"));
        }
        m_text = m_tran.Find("ScrollDataGrid/content/Text").GetComponent<Text>();
        m_intro = m_tran.Find("tabGroup/Intro").GetComponent<Toggle>();
        m_block = m_tran.Find("tabGroup/Block").GetComponent<Toggle>();
        m_other = m_tran.Find("tabGroup/Other").GetComponent<Toggle>();
        m_pos = m_tran.Find("tabGroup/Pos").GetComponent<Toggle>();
        OnTabClick(m_tran.Find("tabGroup/Intro").gameObject, null);
        m_intro.isOn = true;
        m_block.isOn = false;
        m_other.isOn = false;
        m_pos.isOn = false;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("btnClose")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("Button")).onPointerClick += delegate { this.HidePanel(); };
        UGUIClickHandler.Get(m_tran.Find("tabGroup/Block")).onPointerClick += OnTabClick;
        UGUIClickHandler.Get(m_tran.Find("tabGroup/Intro")).onPointerClick += OnTabClick;
        UGUIClickHandler.Get(m_tran.Find("tabGroup/Other")).onPointerClick += OnTabClick;
        UGUIClickHandler.Get(m_tran.Find("tabGroup/Pos")).onPointerClick += OnTabClick;

    }

    void OnTabClick(GameObject target,PointerEventData data)
    {
        m_text.text = m_desDic.GetValue(target.name);
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        
    }

    public override void Update()
    {
        
    }
}

