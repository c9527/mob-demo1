﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelLegionFirst : BasePanel
{
    Text m_bigTitle;
    Text m_smallTitle;
    Image m_icon;


    public PanelLegionFirst()
    {
        SetPanelPrefabPath("UI/LegionWar/PanelLegionFirst");
        AddPreLoadRes("UI/LegionWar/PanelLegionFirst", EResType.UI);
        AddPreLoadRes("Atlas/LegionWar", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_bigTitle = m_tran.Find("bigTitle").GetComponent<Text>();
        m_smallTitle = m_tran.Find("smallTitle").GetComponent<Text>();
        m_icon = m_tran.Find("icon").GetComponent<Image>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Button")).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        if (LegionDataManager.MyLegionId != 0)
        {
            var cfg = ConfigManager.GetConfig<ConfigLegion>().m_dataArr;
            m_bigTitle.text = "你已经被分配到" + "<color=cyan>" + cfg[LegionDataManager.MyLegionId-1].LegionName + "</color>";
            m_smallTitle.text = "大战一触即发！\n你和你的战友将代表" + "<color=cyan>" + cfg[LegionDataManager.MyLegionId - 1].LegionName + "</color>" + "\n参与激烈的领土争夺战！";
            m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.LegionWar, "legion" + LegionDataManager.MyLegionId.ToString()));
            m_icon.SetNativeSize();
        }
        NetLayer.Send(new tos_player_update_tag() { tag_id = (int)EnumTag.LEGION_WAR_FIRST, value = 1 });
    }

    public override void Update()
    {
       
    }
}
