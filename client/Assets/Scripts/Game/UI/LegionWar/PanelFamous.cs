﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：名人堂
//创建者：HWL
//创建时间: 2016/3/19 15:56:28
///////////////////////////

class PanelFamous
{
    private Transform m_tran;
    private DataGrid m_famousDataGrid;
    public PanelFamous(Transform transfrom)
    {
        m_tran = transfrom;
        if( m_tran == null )
        {
            return;
        }

        Init();
    }

    private void Init()
    {
        m_famousDataGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_famousDataGrid.SetItemRender(m_tran.Find("ScrollDataGrid/content/FamousItem").gameObject, typeof(LegionWarFamousItem));
        m_famousDataGrid.useLoopItems = true;
        UpdateFramousList();
    }

    public void UpdateFramousList()
    {
        List<LegionWarFamousInfo> list = new List<LegionWarFamousInfo>();
        p_legionwar_eminentor famousData;
        for (int i = 0; i < LegionDataManager.LegionWarFamousList.Count; i++)
        {
            famousData = LegionDataManager.LegionWarFamousList[i];
            LegionWarFamousInfo item = new LegionWarFamousInfo();
            item.m_corpName = famousData.corps_name;
            item.m_headIcon = famousData.icon;
            item.m_playerName = famousData.name;
            item.m_serverName = PlayerSystem.GetRoleSeverName(famousData.player_id);
            item.m_count = famousData.lead_times;
            item.m_rank =  i;
            list.Add(item);
        }
        m_famousDataGrid.Data = list.ToArray();
    }
}
