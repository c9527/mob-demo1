﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团信息交战记录
//创建者：HWL
//创建时间: 2016/3/19 14:20:41
///////////////////////////

class LegionWarFightItem : ItemRender
{
    private Text m_txtTime;
    private Text m_txtContent;
    public override void Awake()
    {
        m_txtTime = transform.Find("timeText").GetComponent<Text>();
        m_txtContent = transform.Find("descText").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        LegionWarFightRecordInfo data1 = data as LegionWarFightRecordInfo;
        m_txtTime.text = data1.time.ToString();
        m_txtContent.text = data1.content.ToString();
    }
}


public class LegionWarFightRecordInfo
{
    public string time;
    public string content;
}
