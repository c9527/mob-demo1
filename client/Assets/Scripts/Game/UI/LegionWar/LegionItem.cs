﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：军团信息页签列表项
//创建者：HWL
//创建时间: 2016/3/21 17:02:19
///////////////////////////

class LegionItem : ItemRender
{
    private Text m_jobText;
    private Text m_nameText;
    private Text m_devoteText;
    private Text m_serverText;

    public override void Awake()
    {
        m_jobText = transform.Find("jobText").GetComponent<Text>();
        m_nameText = transform.Find("playerNameText").GetComponent<Text>();
        m_devoteText = transform.Find("devoteText").GetComponent<Text>();
        m_serverText = transform.Find("serverText").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        LegionItemInfo item = data as LegionItemInfo;
        ConfigLegionPosition legion = ConfigManager.GetConfig<ConfigLegionPosition>();
        string positionName = "";
        if( legion != null )
        {
            ConfigLegionPositionLine legionPositionLine = legion.GetLine(item.job);
            positionName = legionPositionLine == null ? "无职位" : legionPositionLine.PositionName;
        }
        m_jobText.text = positionName;
        m_nameText.text = item.name;
        m_devoteText.text = item.devote.ToString();
        m_serverText.text = item.server;
    }
}

public class LegionItemInfo
{
    public long playerId;
    public int job;
    public string name;
    public int devote;
    public string server;
}
