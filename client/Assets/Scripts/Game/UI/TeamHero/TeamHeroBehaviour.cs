﻿using System.Collections.Generic;
using UnityEngine;

public class TeamHeroInfo
{
    public BasePlayer player;
    public int heroLevel;
    public int soul;
}

public class TeamHeroBehaviour : MonoBehaviour
{
    static public TeamHeroBehaviour singleton;
    private PanelBattleTeamHero m_panelBattleTeamHero;

    private Dictionary<int, TeamHeroInfo> m_allTeamHeroInfo;

    protected bool m_bIsPanelBattleInited;
    protected SBattleState m_bs;
    protected BasePlayer m_kPlayer;

    public TeamHeroBehaviour()
    {
        singleton = this;
        m_allTeamHeroInfo = new Dictionary<int, TeamHeroInfo>();
    }

    protected void Awake()
    {
        NetLayer.AddHandler(this);
        InitEvent();
    }

    protected void Start()
    {
        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
            return;
        }
    }

    protected void OnSceneLoaded()
    {
        if (m_bIsPanelBattleInited)
            Init();
    }

    protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        m_bIsPanelBattleInited = true;
        Init();
    }

    protected void Init()
    {
        m_panelBattleTeamHero = UIManager.PopPanel<PanelBattleTeamHero>();

        if (WorldManager.singleton.isViewBattleModel)
        {
            OnSwitchPlayer(ChangeTeamView.watchedPlayerID);
        }
        else
        {
            BindPlayer(MainPlayer.singleton);
        }
        if (m_bs != null)
        {
            OnUpdateBattleState(m_bs);
        }
    }

    protected void BindPlayer(BasePlayer player)
    {
        m_kPlayer = player;
        if(m_panelBattleTeamHero != null && m_panelBattleTeamHero.IsInited())
        {
            m_panelBattleTeamHero.InitShow();
        }
    }

    protected void InitEvent()
    {
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        //GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        //GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);
    }

    protected void OnUpdateBattleState(SBattleState data)
    {
        m_bs = data;

        if (m_bs == null || m_panelBattleTeamHero == null || !m_bIsPanelBattleInited)
            return;

        switch (m_bs.state)
        {
            case GameConst.Fight_State_RoundStart:
                if (m_panelBattleTeamHero.IsInited())
                    m_panelBattleTeamHero.InitShow();
                break;
            case GameConst.Fight_State_RoundEnd:
                m_allTeamHeroInfo.Clear();
                break;
        }
    }

    //void OnPlayerDie(BasePlayer killer, BasePlayer dier, int iLastHitPart, int weapon)
    //{
    //
    //}
    //
    //void OnPlayerRevive(string eventName, BasePlayer who)
    //{
    //    if (who == m_kPlayer && m_panelBattleTeamHero != null)
    //    {
    //        if (who.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
    //        {
    //            m_panelBattleTeamHero.ShowHeroInfo(true);
    //            m_panelBattleTeamHero.ShowRushBtn(true);
    //        }
    //        else
    //        {
    //            m_panelBattleTeamHero.ShowHeroInfo(false);
    //            m_panelBattleTeamHero.ShowRushBtn(false);
    //        }
    //    }
    //}

    private void OnSwitchPlayer(int partnerID)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(partnerID);
        if (player != null)
        {
            BindPlayer(player);
        }
    }

    protected void Destory()
    {
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        //GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        //GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);
        NetLayer.RemoveHandler(this);
    }

    void Toc_fight_team_hero_level(toc_fight_team_hero_level proto)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(proto.actor_id);
        if (player == null)
            return;

        TeamHeroInfo thi;
        if (!m_allTeamHeroInfo.TryGetValue(proto.actor_id,out thi))
        {
            thi = new TeamHeroInfo();
            thi.player = player;
            m_allTeamHeroInfo.Add(proto.actor_id, thi);
        }
        thi.heroLevel = proto.level;
        thi.soul = proto.soul;

        if(player == m_kPlayer) //主角
        {
            if (m_panelBattleTeamHero != null && m_panelBattleTeamHero.IsInited())
            {
                if (proto.level > 0)
                {
                    m_panelBattleTeamHero.SetupHeroInfo(thi);
                }
                else
                {
                    m_panelBattleTeamHero.ShowHeroInfo(false);
                    m_panelBattleTeamHero.ShowRushBtn(false);
                }
            }
        }
        else if(player is OtherPlayer) //其他玩家，更新头顶图标
        {
            (player as OtherPlayer).setupLogoGO();
        }
    }

    public BasePlayer curPlayer
    {
        get { return m_kPlayer; }
    }

    public TeamHeroInfo GetTeamHeroInfo(int id)
    {
        TeamHeroInfo thi;
        if (m_allTeamHeroInfo.TryGetValue(id, out thi))
        {
            return thi;
        }
        return null;
    }


}
