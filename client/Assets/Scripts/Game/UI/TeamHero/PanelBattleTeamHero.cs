﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class TeamHeroLevelGrid
{
    class LevelSprite
    {
        public const string LEVEL_SHORT_1 = "team_hero_progress_blue_short";
        public const string LEVEL_SHORT_2 = "team_hero_progress_red_short";
        public const string LEVEL_SHORT_3 = "team_hero_progress_yellow_short";
        public const string LEVEL_LONG_1 = "team_hero_progress_blue_long";
        public const string LEVEL_LONG_2 = "team_hero_progress_red_long";
        public const string LEVEL_LONG_3 = "team_hero_progress_yellow_long";
    }


    GameObject m_go;
    Transform m_trans;
    Image m_image;
    bool m_isShort;

    public TeamHeroLevelGrid(Transform tran, bool isShort)
    {
        m_trans = tran;
        m_isShort = isShort;
        m_go = m_trans.gameObject;
        m_image = m_trans.GetComponent<Image>();
    }

    public void SetLevel(int level)
    {
        switch (level)
        {
            case 0:
                m_image.enabled = false;
                break;
            case 1:
                m_image.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_isShort ? LevelSprite.LEVEL_SHORT_1 : LevelSprite.LEVEL_LONG_1));
                m_image.enabled = true;
                break;
            case 2:
                m_image.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_isShort ? LevelSprite.LEVEL_SHORT_2 : LevelSprite.LEVEL_LONG_2));
                m_image.enabled = true;
                break;
            case 3:
                m_image.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_isShort ? LevelSprite.LEVEL_SHORT_3 : LevelSprite.LEVEL_LONG_3));
                m_image.enabled = true;
                break;
            default:
                m_image.enabled = false;
                break;
        }
    }

}

public class PanelBattleTeamHero : BasePanel
{
    private const int LevelGridConut = 20;
    private const int LevelLongGridCount = 2;
    private const int RushSkillId = 640001;

    List<TeamHeroLevelGrid> m_lstLevelGrid;
    Image m_levelTip;
    private GameObject m_rushBtn;
    private SkillItemRender m_rushSkillRender;
    private GameObject m_heroInfo;

    private ConfigTeamHeroLevel m_configTeamHeroLevel;

    public PanelBattleTeamHero()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleTeamHero");

        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("UI/Battle/PanelBattleTeamHero", EResType.UI);

        m_lstLevelGrid = new List<TeamHeroLevelGrid>(LevelGridConut);
        m_configTeamHeroLevel = ConfigManager.GetConfig<ConfigTeamHeroLevel>();
    }

    public override void Init()
    {
        //m_go.GetComponent<RectTransform>().SetAsFirstSibling();

        for (int i = 0; i < LevelGridConut - LevelLongGridCount; ++i)
            m_lstLevelGrid.Add(new TeamHeroLevelGrid(m_tran.Find(string.Format("TeamHeroInfo/Progress/grids_short/grid_{0}", i)), true));
        for (int i = 0; i < LevelLongGridCount; ++i)
            m_lstLevelGrid.Add(new TeamHeroLevelGrid(m_tran.Find(string.Format("TeamHeroInfo/Progress/grids_long/grid_{0}", i)), false));
        m_rushBtn = m_tran.Find("RushBtn").gameObject;
        m_levelTip = m_tran.Find("TeamHeroInfo/levelTip").GetComponent<Image>();
        m_heroInfo = m_tran.Find("TeamHeroInfo").gameObject;
        m_rushSkillRender = m_rushBtn.AddComponent<SkillItemRender>();
        InitShow();
    }


    public override void InitEvent()
    {
        GameDispatcher.AddEventListener<int>(GameEvent.UI_REFRESH_SKILL, OnRefreshSkillItem);
        UGUIClickHandler.Get(m_rushBtn).onPointerClick += OnRush;
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
            AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
            {
                if (m_rushBtn.activeSelf)
                    OnRush(null, null);
            });
    }

    public override void OnShow()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<int>(GameEvent.UI_REFRESH_SKILL, OnRefreshSkillItem);
        m_lstLevelGrid.Clear();
    }

    public override void Update()
    {

    }

    public void InitShow()
    {
        BasePlayer curPlayer = TeamHeroBehaviour.singleton.curPlayer;

        if (curPlayer != null)
        {
            bool isShowInfo = curPlayer.serverData.actor_type == GameConst.ACTOR_TYPE_HERO;
            if (isShowInfo)
                SetupHeroInfo(TeamHeroBehaviour.singleton.GetTeamHeroInfo(curPlayer.PlayerId));
            else
            {
                ShowHeroInfo(false);
                ShowRushBtn(false);
            }
        }
        else
        {
            ShowHeroInfo(false);
            ShowRushBtn(false);
        }
    }

    public void ShowHeroInfo(bool isShow)
    {
        if (m_heroInfo != null)
            m_heroInfo.TrySetActive(isShow);
    }

    public void SetupHeroInfo(TeamHeroInfo thi)
    {
        CongigTeamHeroLevelLine thic;
        if (thi == null || (thic = m_configTeamHeroLevel.GetLine(thi.heroLevel)) == null)
        {
            ShowHeroInfo(false);
            ShowRushBtn(false);
            return;
        }

        int heroLevel = thi.heroLevel;
        int soul = thi.soul;

        ShowHeroInfo(true);

        m_levelTip.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, thic.Tip));

        var thic2 = m_configTeamHeroLevel.GetLine(heroLevel + 1);
        if (thic2 != null)
        {
            int nextSoul = thic2.Soul;
            float oneSoulNeedGrid = (float)LevelGridConut / nextSoul;
            for (int i = 0; i < LevelGridConut; ++i)
                m_lstLevelGrid[i].SetLevel(i / oneSoulNeedGrid < soul ? heroLevel : 0);
        }
        else
        {
            for (int i = 0; i < LevelGridConut; ++i)
                m_lstLevelGrid[i].SetLevel(heroLevel);
        }


        if (thic.Skills != null && thic.Skills.Length > 0)
        {
            ShowRushBtn(true);
            TDSKillInfo cfg = ConfigManager.GetConfig<ConfigSkill>().GetLine(thic.Skills[0]);
            if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN) && (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web))
            {
                cfg.ICON = "PC_skill_rush";
            }
            else
            {
                cfg.ICON = "skill_rush";
            }
            m_rushSkillRender.SetData(cfg);
        }
        else
            ShowRushBtn(false);
    }

    public void ShowRushBtn(bool isShow)
    {
        if (m_rushBtn != null)
            m_rushBtn.TrySetActive(isShow);
           
    }

    private void OnRush(GameObject target, PointerEventData eventData)
    {
        if (m_rushSkillRender != null && m_rushSkillRender.isSetData && !m_rushSkillRender.frozen)
        {
            PlayerBattleModel.Instance.TosUseSkill(m_rushSkillRender.skillId);
        }
    }

    private void OnRefreshSkillItem(int skillId)
    {
        if (m_rushSkillRender != null && skillId == m_rushSkillRender.skillId)
        {
            m_rushSkillRender.frozen = true;
        }
    }
}
