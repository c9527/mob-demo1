﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

class PanelSurvivalSettle : PanelSettle
{
    private Image m_imgRoundNum1;
    private Image m_imgRoundNum2;
    private Image m_imgRoundSuccess;                    
    private Image m_imgKillBossIcon;
    private GameObject m_worldBossTitile;
    private GameObject m_goStarLevel;
    private GameObject m_bg;
    private Button m_shareBtn;

    private const int TotalStarLevel = 3;
    private List<Image> m_lstStarLevel;
    private bool m_isWorldBoss;

    static private int m_starLevel = -1;

    static private string m_gameType;
    static private int m_joinRewardCountUse;
    static private int m_passRewardCountUse;

    public PanelSurvivalSettle()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelSurvivalSettle");

        AddPreLoadRes("UI/Battle/PanelSurvivalSettle", EResType.UI);
        AddPreLoadRes("UI/Battle/SettleWeaponItemRender", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Settle", EResType.Atlas, ResTag.Forever);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/PlayerInfo", EResType.Atlas);
        if (RoomModel.LastRoomInfo.rule != GameConst.GAME_RULE_WORLD_BOSS && RoomModel.LastRoomInfo.rule != GameConst.GAME_RULE_BOSS_NEWER)
            MaxPlayerCount = 5;

        m_lstStarLevel = new List<Image>(TotalStarLevel);
    }

    public override void Init()
    {
        base.Init();
        m_bg = m_tran.Find("background").gameObject;
        m_imgRoundNum1 = m_tran.Find("num1").GetComponent<Image>();
        m_imgRoundNum2 = m_tran.Find("num2").GetComponent<Image>();
        m_imgRoundSuccess = m_tran.Find("success").GetComponent<Image>();
        m_worldBossTitile = m_tran.Find("WorldBossTitile").gameObject;
        m_isWorldBoss = (RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_WORLD_BOSS || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_BOSS_NEWER);
        m_imgRoundNum1.gameObject.TrySetActive(!m_isWorldBoss);
        m_imgRoundNum2.gameObject.TrySetActive(!m_isWorldBoss);
        m_worldBossTitile.TrySetActive(m_isWorldBoss);
        m_tran.Find("info_canvas/round").gameObject.TrySetActive(!m_isWorldBoss);
        m_goStarLevel = m_tran.Find("player_canvas/star_level").gameObject;
        _hero_card_exp = m_tran.Find("hero_card_exp").GetComponent<Image>();
        m_shareBtn = m_tran.Find("player_canvas/share_btn").GetComponent<Button>();
        //m_shareBtn.gameObject.TrySetActive(true);
        for (int i = 1; i <= TotalStarLevel; ++i )
        {
            Image img = m_tran.Find(string.Format("player_canvas/star_level/star_level_{0}/star_level_{0}_icon", i)).GetComponent<Image>();
            m_lstStarLevel.Add(img);
        }

        m_imgKillBossIcon = m_tran.Find("player_canvas2/kill_boss_icon").GetComponent<Image>();

    }

    public override void InitEvent()
    {
        base.InitEvent();
        UGUIClickHandler.Get(m_shareBtn.gameObject).onPointerClick += onSubShareView;
    }
    private void onSubShareView(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelWeixinShare>(new object[] { PanelSettle.gameRule });
    }

	public override void OnShow()
	{
		base.OnShow();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.SolidColor;
		UIManager.m_uiCamera.backgroundColor = UnityEngine.Color.black;
        _double_btn.gameObject.TrySetActive(false);

        if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
        {
            if (PanelSettle.self_win_result && GlobalConfig.serverValue.weixinShare_open)
            {
                m_shareBtn.gameObject.TrySetActive(true);
                PanelSettle.self_win_result = false;
            }
            else
            {
                m_shareBtn.gameObject.TrySetActive(false);
            }
        }
	}

    public override void UpdateWeaponCanvas()
    {
        m_bg.TrySetActive(false);
        m_imgRoundNum1.enabled = false;
        m_imgRoundNum2.enabled = false;
        m_imgRoundSuccess.enabled = false;
        m_worldBossTitile.TrySetActive(false);        
        _lvl_txt.gameObject.TrySetActive(false);
        _exp_new.gameObject.TrySetActive(false);
        _exp_old.gameObject.TrySetActive(false);        
        m_tran.Find("info_canvas").gameObject.TrySetActive(false);
        _exp_txt.gameObject.TrySetActive(false);
        _gold_txt.gameObject.TrySetActive(false);
        _honour_txt.gameObject.TrySetActive(false);
    }

	public override void OnHide()
	{
		base.OnHide();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.Depth;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.Depth;
	}

    protected override void updateData()
    {
        base.updateData();
        _hero_card_exp.gameObject.TrySetActive(false);
        if(_camp0_img!=null)
            _camp0_img.enabled = true;
        m_imgKillBossIcon.enabled = false;
        

        if (RoomModel.ChannelType == ChannelTypeEnum.worldboss||RoomModel.ChannelType==ChannelTypeEnum.boss4newer)
        {
            ShowJoinReward(false);
            ShowFirstReward(false);
            if (m_gameType == ChannelTypeEnum.worldboss.ToString()||m_gameType==ChannelTypeEnum.boss4newer.ToString())
            {                
                ShowPassReward(true);
                var useCount = m_passRewardCountUse;
                var countMax = 1;                
            }
            else
                ShowPassReward(false);

            int killBossPlayerIndex = -1;
            for (int i = 0; i < settle_data.Length; ++i )
            {
                if (settle_data[i].kill > 0)
                {
                    killBossPlayerIndex = i;
                    break;
                }
            }
            if (killBossPlayerIndex != -1 && killBossPlayerIndex < _player_list.Length)
            {
                //m_imgKillBossIcon.enabled = true;
                m_imgKillBossIcon.enabled = false;
                m_imgKillBossIcon.rectTransform.SetParent(_player_list[killBossPlayerIndex].transform);
                m_imgKillBossIcon.SetNativeSize();
                m_imgKillBossIcon.transform.localPosition = _player_list[killBossPlayerIndex].reportBtnPos;
            }
            return;
        }
        m_tran.Find("info_canvas/round").gameObject.TrySetActive(true);
        if (game_round < 10)
        {
            m_imgRoundNum2.enabled = false;
            m_imgRoundNum1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round));
            m_imgRoundNum1.SetNativeSize();
        }
        else
        {
            m_imgRoundNum2.enabled = true;
            m_imgRoundNum1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round / 10));
            m_imgRoundNum2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round % 10));
            m_imgRoundNum1.SetNativeSize();
            m_imgRoundNum2.SetNativeSize();
        }

        m_imgRoundSuccess.enabled = (self_data != null && self_data.camp == win_camp);
        if (m_imgRoundSuccess.enabled == true)
        {
            m_tran.Find("info_canvas/round").gameObject.TrySetActive(false);
            m_imgRoundNum2.enabled = false;
            m_imgRoundNum1.enabled = false;
        }

        if (RoomModel.ChannelType == ChannelTypeEnum.survival)
        {
            ShowJoinReward(false);
            ShowPassReward(false);
            ShowFirstReward(false);
            var useCount = PlayerSystem.survival_data.daily_reward_cnt;
            var countMax = ConfigMisc.GetInt("survival_reward_daily_max");            
        }
        else if (RoomModel.ChannelType == ChannelTypeEnum.defend && m_gameType == ChannelTypeEnum.defend.ToString())
        {
            ShowJoinReward(true);
            ShowPassReward(true);
            ShowFirstReward(false);

            var joinRewradCountMax = ConfigMisc.GetInt("defend_join_reward_daily_max");
            var finishRewardCountMax = ConfigMisc.GetInt("defend_finish_reward_daily_max");            
        }
        else
        {
            //Logger.Error("还没有收到toc_player_game_reward_times协议");
            ShowJoinReward(false);
            ShowPassReward(false);
            ShowFirstReward(false);
        }
        
        ShowStarLevel(m_starLevel != -1, m_starLevel);
    }

    private void ShowStarLevel(bool isShow, int starLevel)
    {
        if(isShow)
        {
            m_goStarLevel.TrySetActive(true);
            for(int i = 0; i < TotalStarLevel; ++i)
            {
                m_lstStarLevel[i].enabled = starLevel > i;
            }
        }
        else
        {
            m_goStarLevel.TrySetActive(false);
        }

    }

    private void ShowJoinReward(bool isShow)
    {
           
    }

    private void ShowPassReward(bool isShow)
    {
              
    }

    private void ShowFirstReward(bool isShow)
    {
          
    }

    static void Toc_player_game_reward_times(toc_player_game_reward_times proto)
    {
        m_gameType = proto.game_type;
        m_joinRewardCountUse = proto.partake_reward;
        m_passRewardCountUse = proto.win_reward;
    }

    static void Toc_player_stage_star(toc_player_stage_star proto)
    {
        Logger.Error("Toc_player_stage_star:  proto.star=" + proto.star);
        m_starLevel = proto.star;
    }

}