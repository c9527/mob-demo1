﻿using UnityEngine;

class ZombieHeroInfo : MonoBehaviour
{
    UIImgNumText m_kHeroCount;
    UIImgNumText m_kZombieCount;

    void Awake()
    {
        Transform tempTransform = gameObject.transform;
        m_kHeroCount = new UIImgNumText(tempTransform.Find("herocount"), "mode", AtlasName.Battle, 0, -8);
        m_kZombieCount = new UIImgNumText(tempTransform.Find("zombiecount"), "mode", AtlasName.Battle, 0, -8);

        //GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        //GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
    }

    public void RefreshView()
    {
        int heroCount = 0;
        int zombieCount = 0;
        foreach(BasePlayer player in WorldManager.singleton.allPlayer.Values)
        {
            if(player.isAlive)
            {
                if(player.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                {
                    ++zombieCount;
                }
                else if (player.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
                {
                    ++heroCount;
                }
            }
        }
        m_kHeroCount.value = heroCount;
        m_kZombieCount.value = zombieCount;
    }

    void Update()
    {

    }

    
    void OnDestroy()
    {
        //GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        //GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
    }

    void OnPlayerDie(BasePlayer killer, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (dier.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
            --m_kZombieCount.value;
        else
            --m_kHeroCount.value;
    }

    void OnPlayerLeave(BasePlayer player)
    {
        if (player.isAlive)
        {
            if (player.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                --m_kZombieCount.value;
            else
                --m_kHeroCount.value;
        }
    }

    public void RefreshData()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        int heroCount = 0;
        int zombieCount = 0;

        foreach (var player in data.m_allPlayers.Values)
        {
            if (!player.alive)
                continue;
            if (player.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                ++zombieCount;
            else if(player.actor_type == GameConst.ACTOR_TYPE_HERO)
                ++heroCount;
        }

        m_kHeroCount.value = heroCount;
        m_kZombieCount.value = zombieCount;
    }
}
