﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ArmorBullet : MonoBehaviour
{
    // Use this for initialization
    Text m_txtArmorNum;
    Toggle m_toggleArmor;
    bool m_isEnable;

    void Awake()
    {
        m_txtArmorNum = transform.FindChild("Text").GetComponent<Text>();
        m_toggleArmor = gameObject.GetComponent<Toggle>();
    }

    void Start()
    {
        GameDispatcher.AddEventListener<int, int, int>(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, OnPlayerAmmUpdate);
        GameDispatcher.AddEventListener(GameEvent.UI_EQUIP_UPDATE, OnArmorUpdate);
        UGUIClickHandler.Get(gameObject).onPointerClick += OnClickArmorBullet;
        if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.curWeapon != null)
        {
            var cur_weapon = MainPlayer.singleton.curWeapon;
            OnPlayerAmmUpdate(cur_weapon.ammoNumInClip, cur_weapon.ammoNumLeft, cur_weapon.advNum);
        }
        else
        {
            OnPlayerAmmUpdate(0, 0, 0);
        }
    }

    void OnPlayerAmmUpdate(int inClipNum, int leftNum, int advNum)
    {
        if (!enable)
            return;

        //Logger.Error("OnPlayerAmmUpdate, in: " + inClipNum + ", left: " + leftNum + ", adv: " + advNum);
        m_txtArmorNum.text = advNum >= 0 ? advNum.ToString() : "0";
        if (advNum <= 0)
        {
            m_toggleArmor.isOn = false;
            if (MainPlayer.singleton != null)
            {
                MainPlayer.singleton.useAdvAmmo = false;
            }
            gameObject.TrySetActive(false);
        }
        else
        {
            gameObject.TrySetActive(true);
            m_toggleArmor.isOn = MainPlayer.singleton != null && MainPlayer.singleton.useAdvAmmo;
        }
    }

    void OnArmorUpdate()
    {
        if (!enable)
            return;

        if (m_toggleArmor != null) m_toggleArmor.isOn = MainPlayer.singleton != null && MainPlayer.singleton.useAdvAmmo;
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<int, int, int>(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, OnPlayerAmmUpdate);
        GameDispatcher.RemoveEventListener(GameEvent.UI_EQUIP_UPDATE, OnArmorUpdate);
    }

    public void shortCutKeyClickArmorBullet()
    {
        if (m_txtArmorNum.text == "0")
        {
            return;
        }
        if (m_toggleArmor.isOn)
        {
            m_toggleArmor.isOn = false;
        }
        else
        {
            m_toggleArmor.isOn = true;
        }
        OnClickArmorBullet(null, null);
    }

    private void OnClickArmorBullet(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (m_txtArmorNum.text == "0")
        {
            m_toggleArmor.isOn = false;
            return;
        }
        if (m_toggleArmor.isOn)
        {
            MainPlayer.singleton.useAdvAmmo = true;
        }
        else
        {
            MainPlayer.singleton.useAdvAmmo = false;
        }
    }

    public bool enable
    {
        get { return m_isEnable; }
        set
        {
            m_isEnable = value;
            gameObject.TrySetActive(value);
        }
    }
}
