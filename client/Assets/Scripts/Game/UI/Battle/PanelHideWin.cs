﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class PanelHideWin : BasePanel
{
    public class winData
    {
        public string name;
        public int id;
    }

    public int m_WinNum;
    DataGrid m_winlist;
    Image m_num1;
    Image m_num2;
    Image[] m_imgs;
    Text[] m_names;
    ConfigItemRole m_cir;
    float m_time;
    public PanelHideWin()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelHideWin");
        AddPreLoadRes("UI/Battle/PanelHideWin", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }



    public override void Init()
    {
        m_time = -1;
        m_imgs = new Image[3];
        m_names = new Text[3];
        for (int i = 0; i < 3; i++)
        {
            m_imgs[i] = m_tran.Find("WinList/content/ItemWin"+(i+1).ToString()+"/hideItem").gameObject.GetComponent<Image>();
            m_names[i] = m_tran.Find("WinList/content/ItemWin"+(i+1).ToString()+"/name").gameObject.GetComponent<Text>();
        }
        m_cir = ConfigManager.GetConfig<ConfigItemRole>();
        m_num1 = m_tran.Find("bg/des/num1").GetComponent<Image>();
        m_num2 = m_tran.Find("bg/des/num2").GetComponent<Image>();
        //m_winlist.Data = new object[3];
    }

    public override void InitEvent()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void OnHide()
    {
    }

    public override void OnShow()
    {
        if (m_params != null)
        {
            CheckNumShow(m_params.Length);
        }
        for (int i = 0; i < 3; i++)
        {
            m_names[i].gameObject.TrySetActive(false);
            m_imgs[i].gameObject.TrySetActive(false);
        }
        for (int i = 0; i < m_names.Length && i < m_params.Length; i++)
        {
            var info = m_params[i] as winData;
            m_names[i].text = info.name;
            string img = m_cir.GetLine(info.id).Icon;
            m_imgs[i].SetSprite(ResourceManager.LoadHideIcon(img));
            m_names[i].gameObject.TrySetActive(true);
            m_imgs[i].gameObject.TrySetActive(true);
            m_imgs[i].SetNativeSize();
        }
        m_time = Time.realtimeSinceStartup;
    }


    public override void Update()
    {
        if (m_time > 0)
        {
            if (Time.realtimeSinceStartup - m_time > 2) 
            {
                this.HidePanel();
                m_time = -1;
            }
        }
    }

    void CheckNumShow(int i)
    {
        if (i >= 10)
        {
            m_num1.gameObject.TrySetActive(true);
            m_num2.gameObject.TrySetActive(true);
            m_num1.transform.localPosition = new Vector3(-84f, -1f, 0f);
            m_num1.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + (i / 10).ToString()));
            m_num2.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + (i % 10).ToString()));
        }
        else
        {
            m_num1.gameObject.TrySetActive(true);
            m_num2.gameObject.TrySetActive(false);
            m_num1.transform.localPosition = new Vector3(-75f, -1f, 0f);
            m_num1.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + i.ToString()));
        }
    }
}
