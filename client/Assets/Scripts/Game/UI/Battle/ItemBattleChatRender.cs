﻿using UnityEngine;
using UnityEngine.UI;

class ItemBattleChatRender : ItemRender
{
    private Text m_txtContent;
    private Text m_txtType;
    private Image m_imgChannel;
    private Image m_audio_img;
    private LayoutElement m_layoutElement;
    private ItemBattleChatInfo m_data;

    public override void Awake()
    {
        m_txtContent = transform.Find("name").GetComponent<Text>();
        m_txtType = transform.Find("channel/Text").GetComponent<Text>();
        m_imgChannel = transform.Find("channel").GetComponent<Image>();
        m_audio_img = transform.Find("audio").GetComponent<Image>();
        m_layoutElement = GetComponent<LayoutElement>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ItemBattleChatInfo;
        gameObject.TrySetActive(m_data != null);
        if (m_data == null)
            return;
        if (Time.realtimeSinceStartup - m_data.m_startTime >= 30)
            gameObject.TrySetActive(false);
        if (m_data.m_proto.audio_chat!=null && m_data.m_proto.audio_chat.audio_id>0)
        {
            m_audio_img.gameObject.TrySetActive(true);
            m_txtContent.rectTransform.anchoredPosition = new Vector2(80,0);
        }
        else
        {
            m_audio_img.gameObject.TrySetActive(false);
            m_txtContent.rectTransform.anchoredPosition = new Vector2(62,0);
        }
        if (m_data.m_proto.type == 0)
        {
            m_txtType.text = "全体";
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "world_channel"));
        }
        else if (m_data.m_proto.type == 1)
        {
            m_txtType.text = "队伍";
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "team_channel"));
        }
        else if (m_data.m_proto.type == 2)
        {
            m_txtType.text = "观战";
            m_imgChannel.SetSprite(ResourceManager.LoadSprite(AtlasName.Chat, "system_channel"));
        }

        if (m_data.m_proto.type == 2)  
        {
            //var name = WorldManager.singleton.GetViewerById(m_data.m_proto.from);
            var name = PlayerBattleModel.Instance.GetSpecatorName(m_data.m_proto.from);
            if (name == "")
            {
                m_txtContent.text = string.Format("<color='#EAFFFF'>{0}</color>{1}", PlayerSystem.GetRoleNameFull(name, m_data.m_proto.from), m_data.m_proto.msg);
            }
            else
            {
                m_txtContent.text = string.Format("<color='#EAFFFF'>{0}</color>：{1}", PlayerSystem.GetRoleNameFull(name, m_data.m_proto.from), m_data.m_proto.msg);
            }
        }
        else
        {
            var player = WorldManager.singleton.GetPlayerByPid(m_data.m_proto.from);
            if( player != null )
            {
                m_txtContent.text = string.Format("<color='#EAFFFF'>{0}</color>：{1}", player != null ? player.PlayerName : "", m_data.m_proto.msg);
            }
            else
            {
                m_txtContent.text = string.Format("<color='#EAFFFF'>{0}</color>{1}", player != null ? player.PlayerName : "", m_data.m_proto.msg);
            }
            
        }
        
        if (m_txtContent.preferredHeight > 20)
            m_layoutElement.preferredHeight = 50;
        else
            m_layoutElement.preferredHeight = 25;
    }
}

class ItemBattleChatInfo
{
    public toc_fight_chat m_proto;
    public float m_startTime;
}