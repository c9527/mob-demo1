﻿using UnityEngine;
using UnityEngine.UI;

public class UIBattleSubInfo : MonoBehaviour 
{
    int _zombie_type = 0;

    BasePlayer _player;
    private Image m_kHpControl;
    private Image _info_bg;
    private Image m_imgHead;
    private Text m_txtHpValue;
    private Image m_kArmorControl;
    private Text m_txtArmorValue;
    Image _status_ico;
    public Transform add_info_canvas;
    public Transform desc_txt;
    public Transform info_trans;
    public GameObject m_goShengHuaFu;
    public GameObject m_goShengHuaRen;

    private float m_tick;
    
    void Awake() 
    {
        Transform trans = transform;

        _info_bg = trans.GetComponent<Image>();
        m_kHpControl = trans.Find("HpProcessBar/processfore").GetComponent<Image>();
        m_txtHpValue = trans.Find("HpProcessBar/Text").GetComponent<Text>();
        m_kArmorControl = trans.Find("ArmorProcessBar/processfore").GetComponent<Image>();
        m_txtArmorValue = trans.Find("ArmorProcessBar/Text").GetComponent<Text>();
        _status_ico = trans.Find("status_ico").GetComponent<Image>();
        m_imgHead = trans.Find("head").GetComponent<Image>();
        add_info_canvas = trans.Find("add_info_canvas");
        desc_txt = trans.Find("add_info_canvas/desc_txt");
        info_trans = trans.Find("add_info_canvas/info");
        m_goShengHuaFu = trans.Find("shenghua/shenghuafu").gameObject;
        m_goShengHuaFu.TrySetActive(false);
        m_goShengHuaRen = trans.Find("shenghua/shenghuaren").gameObject;
        m_goShengHuaRen.TrySetActive(false);

        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_INIT_DONE, initData);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SetupPlayerData);
        GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDead);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PROXY_PLAYER_ACTOR_STATE, OnActorStateUpdated);
        GameDispatcher.AddEventListener(GameEvent.UI_SWITCH_FREE_VIEW, SwitchFreeView);

        initData();
	}

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_INIT_DONE, initData);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SetupPlayerData);
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDead);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PROXY_PLAYER_ACTOR_STATE, OnActorStateUpdated);
        GameDispatcher.RemoveEventListener(GameEvent.UI_SWITCH_FREE_VIEW, SwitchFreeView);
    }

    public void initData()
    {
        gameObject.TrySetActive(true);
        SetupPlayerData(WorldManager.singleton.myid);
    }

    private void SetupPlayerData(int id)
    {
        _player = id == WorldManager.singleton.myid ? MainPlayer.singleton : WorldManager.singleton.GetPlayerById(id);

        if (_player != null)
        {
            if (_player.isCancelDisplayWatchInfo)
            {
                gameObject.TrySetActive(false);
                return;
            }
            gameObject.TrySetActive(true);

            if (_player.serverData != null)
            {
                m_imgHead.SetSprite(ResourceManager.LoadRoleIconBig(_player.serverData.icon, _player.Camp));
                //m_imgHead.SetNativeSize();
            }
            refreshPlayerData();
        }
    }

    private void SwitchFreeView()
    {
        gameObject.TrySetActive(false);
    }

    void OnUpdate()
    {
        m_tick += Time.deltaTime;
        if (m_tick >= 1.0f)
        {
            m_tick = 0;
            refreshPlayerData();
            ShowMVPAndACE();
        }
    }

    void Update()
    {
        bool isViewBattle = WorldManager.singleton!=null && WorldManager.singleton.isViewBattleModel;
        if (!isViewBattle && (_player == null || _player.serverData == null))
        {
            return;
        }
        PlayerServerData data;
        if (isViewBattle)
        {
            BasePlayer player = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            if (player == null || player.serverData == null)
            {
                return;
            }
            data = player.serverData;
        }
        else
        {
            data = _player.serverData;
        }
        if (data.alive==true)
        {
            var now_time = (int)((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) * 1000);
            if (int.Parse(m_txtArmorValue.text) < data.maxArmor() && (now_time - data.last_damage_time) > data.armorRecoverWait())
            {
                m_txtArmorValue.text = data.last_armor.ToString();
                var max_armor = data.maxArmor();
                m_kArmorControl.fillAmount = max_armor>0 ? data.last_armor * 1f / max_armor : 0;
            }
            if (int.Parse(m_txtHpValue.text) < data.maxhp() && (now_time - Mathf.Max(data.last_damage_time, data.last_act_time)) > data.hPRecoverWait())
            {
                m_txtHpValue.text = data.life.ToString();
                var max_hp = data.maxhp();
                m_kHpControl.fillAmount = max_hp>0 ? data.life * 1f / max_hp : 0; 
            }
        }
        else
        {
            m_txtHpValue.text = "0";
            m_kHpControl.fillAmount = 0f;
        }
        OnUpdate();
    }

    void refreshPlayerData()
    {
        if (_player!=null && _player.serverData!=null && _player.serverData.alive == true)
        {
            m_txtHpValue.text = _player.serverData.life.ToString();
            var max_hp = _player.serverData.maxhp();
            m_kHpControl.fillAmount = max_hp > 0 ? (float)_player.serverData.life / max_hp : 0;
            m_txtArmorValue.text = _player.serverData.last_armor.ToString();
            var max_armor = _player.serverData.maxArmor();
            m_kArmorControl.fillAmount = max_armor>0 ? (float)_player.serverData.last_armor / max_armor : 0;
            var show_add_info = WorldManager.singleton.isZombieTypeModeOpen;
            if (show_add_info != add_info_canvas.gameObject.activeSelf)
            {
                add_info_canvas.gameObject.TrySetActive(show_add_info);
            }
            if (add_info_canvas.gameObject.activeSelf)
            {
                var config = ConfigManager.GetConfig<ConfigBuff>();
                var add_life = 0;
                var add_damage_rate = 0f;
                for (int i = 0; _player.serverData.buff_list != null && i < _player.serverData.buff_list.Length; i++)
                {
                    var info = config.GetLine(_player.serverData.buff_list[i].buff_id);
                    if (info != null)
                    {
                        add_life += info.MaxHP;
                        add_damage_rate += info.DamageRate;
                    }
                }
                if (_player.serverData.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                {
                    desc_txt.gameObject.TrySetActive(false);
                    info_trans.gameObject.TrySetActive(true);
                    info_trans.Find("Text").GetComponent<Text>().text = string.Format("血量\n+{0}", add_life);
                    var pb = info_trans.Find("PB");
                    for (int i = 0; i < pb.childCount; i++)
                    {
                        pb.GetChild(i).GetComponent<Image>().enabled = i < _player.serverData.fight_level;
                    }
                }
                else
                {
                    desc_txt.gameObject.TrySetActive(add_damage_rate>0);
                    info_trans.gameObject.TrySetActive(false);
                    if (desc_txt.gameObject.activeSelf)
                    {
                        desc_txt.Find("Text").GetComponent<Text>().text = string.Format("当前攻击加成 {0}%", add_damage_rate * 100);
                    }
                }
            }

            m_goShengHuaFu.TrySetActive((_player.isHuman && _player.serverData.immune > 0) || (_player.isZombie && WorldManager.hasZombieCloth));
            m_goShengHuaRen.TrySetActive(_player.isZombie && WorldManager.hasZombieBlade);
        }
        else
        {
            m_txtHpValue.text = "0";
            m_kHpControl.fillAmount = 0f;
            m_txtArmorValue.text = "0";
            m_kArmorControl.fillAmount = 0f;
            if (add_info_canvas.gameObject.activeSelf)
            {
                add_info_canvas.gameObject.TrySetActive(false);
            }
        }
    }

    void OnPlayerDead(BasePlayer killer, BasePlayer dier, int hitPart, int weapon)
    {
        OnPlayerBeDamaged(new DamageData() {victim = dier});
    }

    void OnPlayerBeDamaged(DamageData damageData)
    {
        if (_player == null || damageData.victim == null ||
            _player.serverData == null || damageData.victim.serverData == null ||
            _player.PlayerId != damageData.victim.PlayerId)
        {
            return;
        }
        refreshPlayerData();
    }

    private void OnActorStateUpdated(BasePlayer target)
    {
        if (_player==null || target == null ||
            target.serverData== null || _player.serverData == null ||
            target.PlayerId != _player.PlayerId)
        {
            return;
        }
        if (target.serverData.actor_type != _zombie_type)
        {
            _zombie_type = target.serverData.actor_type;
        }
        refreshPlayerData();
    }

    private void ShowMVPAndACE()
    {
        if (!PlayerBattleModel.Instance.isShowMVPAndACE)
        {
            transform.Find("MVP").gameObject.TrySetActive(false);
            transform.Find("GoldACE").gameObject.TrySetActive(false);
            transform.Find("SilverACE").gameObject.TrySetActive(false);
            return;
        }

        bool isViewBattle = WorldManager.singleton.isViewBattleModel;
        if ( isViewBattle || (_player != null && _player.serverData != null && _player.PlayerId == WorldManager.singleton.myid))
        {
            BasePlayer player;
            if (isViewBattle)
                player = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            else
                player = MainPlayer.singleton;
            
            if (player == null)
                return;

            // 显示MVP标志
            if (player.isMVP)
            {
                transform.Find("MVP").gameObject.TrySetActive(true);
            }
            else
            {
                transform.Find("MVP").gameObject.TrySetActive(false);
            }
            // 显示金色ACE标志
            if (player.isGoldACE)
            {
                transform.Find("GoldACE").gameObject.TrySetActive(true);
            }
            else
            {
                transform.Find("GoldACE").gameObject.TrySetActive(false);
            }
            // 显示银色ACE标志
            if (player.isSilverACE && !player.isGoldACE)
            {
                transform.Find("SilverACE").gameObject.TrySetActive(true);
            }
            else
            {
                transform.Find("SilverACE").gameObject.TrySetActive(false);
            }
        }
        else//不是自己的话就不显示MVP系列图标
        {
            transform.Find("MVP").gameObject.TrySetActive(false);
            transform.Find("GoldACE").gameObject.TrySetActive(false);
            transform.Find("SilverACE").gameObject.TrySetActive(false);
        }
    }
}
