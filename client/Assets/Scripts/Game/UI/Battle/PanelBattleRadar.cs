﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 这个文件主要是处理战斗场景中的小地图（雷达）
/// </summary>
public class PanelBattleRadar : BasePanel
{
    private readonly int PIXEL_PER_METER_1 = ConfigMisc.GetInt("radar_pixel_per_meter_1");//每米距离对应的像素
    private readonly int PIXEL_PER_METER_2 = ConfigMisc.GetInt("radar_pixel_per_meter_2");//每米距离对应的像素
    private readonly int MAX_VIEW_R_PIXEL = 70;//最大可视半径像素

    private int curResolution = 0;

    private bool canShow = true;
    private Image showMapBtnImg;
    private GameObject showMapBtnGo;
    private ConfigMapItemLine cmil;
    private GameObject smallMapGo;
    private RectTransform smallMapTrans;//小地图
    private RectTransform mainIconTrans;//主角标识
    private Vector2 centerPos = Vector2.zero;
    private int curMapId = 0;
    private List<SmallMapIcon> goIconList;
    //缓存FlashingSprite信息
    private Dictionary<int, FlashingSprite> flashingDic;
    private int curIconIndex = 0;
    public int frame = 0;

    private string previousSprite = "white_point"; //why red_point? I don't know
    private string previousAtlas = AtlasName.BattleNew;
    private Sprite previousImg;

    public static Vector2 pos;

    /// <summary>
    /// 小地图上的图标
    /// </summary>
    class SmallMapIcon
    {
        public GameObject go;
        public RectTransform rt;
        public Image img;
        public bool isInit;
        public SmallMapIcon()
        {
            go = new GameObject();
            rt = go.AddComponent<RectTransform>();
            img = go.AddComponent<Image>();
            isInit = false;
        }

        public void SetSprite(Sprite sprite)
        {
            img.SetSprite(sprite);
            img.SetNativeSize();
        }
    }

    public PanelBattleRadar()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleRadar");

        AddPreLoadRes("UI/Battle/PanelBattleRadar", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }

    public override void Init()
    {
        var smallMap = m_tran.Find("SmallMap/Map");
        smallMapGo = smallMap.gameObject;
        smallMapTrans = smallMap.Find("maskMap/map") as RectTransform;
        mainIconTrans = smallMap.Find("MainIcon") as RectTransform;
        showMapBtnGo = m_tran.Find("SmallMap/ShowSmallMapBtn").gameObject;
        showMapBtnImg = showMapBtnGo.GetComponent<Image>();
        smallMapGo.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(showMapBtnGo).onPointerClick += delegate { OnShowSmallMap(); };
    }

    public override void OnShow()
    {
        if (pos != Vector2.zero)
            SetMapPosition(pos);
    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Update()
    {
        if (!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GUIDE_LEVEL))
            UpdateRadar();
    }

    public void SetMapPosition(Vector2 pos)
    {
        m_tran.Find("SmallMap").GetComponent<RectTransform>().anchoredPosition = pos;
    }

    private void UpdateRadar()
    {
        if (canShow == false) return;
        if (++frame >= GameSetting.smallMapUpdateFrameInterval)
        {
            frame = 0;
            if (smallMapTrans != null)
            {
                if (!GameSetting.enableSmallMap)
                {
                    smallMapGo.TrySetActive(false);
                    return;
                }
                else
                {
                    smallMapGo.TrySetActive(true);
                }

                BasePlayer curPlayer = MainPlayer.singleton;
                if (curPlayer == null || curPlayer.gameObject == null || curPlayer.isDead) curPlayer = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
                if (curPlayer == null || curPlayer.serverData == null || curPlayer.gameObject == null) return;

                initSamllMap(SceneManager.singleton.curMapId);
                if (curMapId == 0 || cmil == null) return;//{ Logger.Error("error, mapId: "+curMapId+", config: "+cmil); return; }

                UpdateCurPlayer(curPlayer);
                UpdateOtherPlayers(curPlayer);
                UpdateBombPoint(curPlayer);
                UnShowSomeIcon();
            }
        }
    }

    private void initSamllMap(int mapId)
    {
        if (mapId == curMapId) return;
        cmil = ConfigManager.GetConfig<ConfigMapItem>().GetLine(mapId);
        if (cmil == null) { smallMapGo.TrySetActive(false); return; };
        curMapId = mapId;
        Sprite sprite = ResourceManager.LoadSprite(AtlasName.BattleSmallMap, mapId.ToString());
        var size = cmil.size;
        if (sprite != null)
        {
            float spW = sprite.rect.width;
            float spH = sprite.rect.height;
            float X1 = size.x / size.y;
            float X2 = spW / spH;
            if (Math.Abs(X1 - X2) > 0.01f)
            {
                Logger.Warning("map: " + mapId + ", size: " + size + ", 比例：" + X1 + ", width: " + spW + ", height: " + spH + ", 比例：" + X2);
            }
            smallMapTrans.GetComponent<Image>().enabled = true;
            smallMapTrans.GetComponent<Image>().SetSprite(sprite);
        }
        else
        {
            smallMapTrans.GetComponent<Image>().enabled = false;
        };
        Vector2 min = cmil.posMin;
        Vector2 max = cmil.posMax;
        centerPos = new Vector2((min.x + max.x) / 2, (min.y + max.y) / 2);
        curResolution = (curMapId > 5000) ? PIXEL_PER_METER_2 : PIXEL_PER_METER_1;
        smallMapTrans.sizeDelta = size / 10 * curResolution;
        smallMapTrans.localPosition = Vector3.zero;
        smallMapGo.TrySetActive(true);

        previousImg = ResourceManager.LoadSprite(previousAtlas, previousSprite);
    }

    private void UpdateCurPlayer(BasePlayer curPlayer)
    {
        smallMapTrans.eulerAngles = new Vector3(0, 0, curPlayer.transform.eulerAngles.y);
        Vector2 playerPos = new Vector2(curPlayer.position.x, curPlayer.position.z);
        Vector2 min = cmil.posMin;
        Vector2 max = cmil.posMax;
        if (playerPos.x > max.x || playerPos.x < min.x || playerPos.y > max.y || playerPos.y < min.y)
        {
            //Logger.Error("pos: " + playerPos + ", min: " + min + ", max: " + max);
            return;
        }
        smallMapTrans.pivot = new Vector2((playerPos.x - min.x) / (max.x - min.x), (playerPos.y - min.y) / (max.y - min.y));
        smallMapTrans.anchoredPosition = Vector2.zero;
    }

    private void UpdateOtherPlayers(BasePlayer curPlayer)
    {
        Vector2 cur_pos = new Vector2(curPlayer.position.x, curPlayer.position.z);
        if (goIconList == null) goIconList = new List<SmallMapIcon>();
        if (flashingDic == null) 
            flashingDic = new Dictionary<int, FlashingSprite>();
        Dictionary<int, BasePlayer> dicAllPlayer = WorldManager.singleton.allPlayer;
        curIconIndex = 0;
        foreach (KeyValuePair<int, BasePlayer> kvp in dicAllPlayer)
        {
            BasePlayer player = kvp.Value;
            if (player != null && player.serverData != null && player.gameObject != null && player != curPlayer)
            {
                if (WorldManager.singleton.isSurvivalTypeModeOpen)
                {
                    //存模式显示僵尸
                }
                else if (WorldManager.singleton.isFightInTurnModeOpen)
                {
                    //擂台赛在最后10s在小地图显示敌人
                }
                else if ((curPlayer is MainPlayer) && (curPlayer as MainPlayer).ScoutSkillState == GameConst.SCOUT_SKILL_STATE_START)
                {
                    //侦察技能
                }
                else if (!WorldManager.singleton.isViewBattleModel && player.Camp != curPlayer.Camp)
                {
                    continue;
                }
                if (player.Camp == (int)WorldManager.CAMP_DEFINE.ALONE) continue;

                if (player.configData != null && player.configData.SubType == GameConst.PLAYER_SUBTYPE_OBJECT) continue;

                if (goIconList.Count <= curIconIndex)
                {
                    goIconList.Add(new SmallMapIcon());
                }
                Vector2 playerPos = new Vector2(player.position.x, player.position.z);
                //if (player.configData.SubType == "BOSS" || player.configData.SubType == "BASE")
                if (null != player.configData && !string.IsNullOrEmpty(player.configData.SmallIcon))
                {
                    if (Vector2.SqrMagnitude(playerPos - cur_pos) > Math.Sqrt(MAX_VIEW_R_PIXEL / curResolution))//调整坐标
                    {
                        playerPos = Vector2.ClampMagnitude(playerPos - cur_pos, MAX_VIEW_R_PIXEL / curResolution) + cur_pos;
                    }
                }
                Vector2 direction = playerPos - centerPos;
                Vector2 pos = direction * curResolution;
                SmallMapIcon curSmallMapIcon = goIconList[curIconIndex++];
                GameObject goIcon = curSmallMapIcon.go;
                string sprite = "red_point";
                string atlas = AtlasName.BattleNew;
                RectTransformProp tran = null;
                FlashingSprite behavior = null;
                OtherPlayer op = player as OtherPlayer;
                if (op != null && op.isShowPosFlashingUI)
                {
                    behavior = FlashingSprite.AddFlashingSpriteBehaviour(ref goIcon);
                    behavior.play(ref goIcon, "player", AtlasName.Battle, sprite, pos, 3f, null, smallMapTrans);
                    //缓存地图移动点
                    if (flashingDic.ContainsKey(curIconIndex - 1))
                    {
                        flashingDic[curIconIndex - 1] = behavior;
                    }
                    else
                    {
                        flashingDic.Add(curIconIndex - 1, behavior);
                    }
                }
                else
                {
                    if (goIcon != null)
                    {
                        if (flashingDic.ContainsKey(curIconIndex - 1))
                        {
                            behavior = flashingDic[curIconIndex - 1];
                            if (behavior != null) behavior.stop();
                        }
                    }
                    if (!WorldManager.singleton.isFightInTurnModeOpen)
                    {
                        sprite = player.isAlive ? player.Camp == curPlayer.Camp ? "blue_point" : "white_point" : "dead_icon";
                        //if (player.configData.SubType == "BOSS" || player.configData.SubType == "BASE")
                        if (null != player.configData && !string.IsNullOrEmpty(player.configData.SmallIcon))
                        {
                            sprite = player.configData.SmallIcon;
                            atlas = AtlasName.ICON;
                        }
                        if (sprite != previousSprite || atlas != previousAtlas) //不同资源
                        {
                            previousSprite = sprite;
                            previousAtlas = atlas;
                            previousImg = ResourceManager.LoadSprite(previousAtlas, previousSprite);
                        }
                        //UIHelper.ShowSprite(ref goIcon, player.PlayerName, atlas, sprite, pos, true, tran, smallMapTrans, true);
                        if (!curSmallMapIcon.isInit)
                        {
                            UIHelper.ShowSprite(ref goIcon, "player", previousImg, pos, true, tran, smallMapTrans, true, Image.Type.Simple, Image.FillMethod.Horizontal);
                            curSmallMapIcon.isInit = true;
                        }
                        else
                        {
                            curSmallMapIcon.go.name = "player";
                            if (!curSmallMapIcon.img.enabled)
                                curSmallMapIcon.img.enabled = true;
                            if (curSmallMapIcon.img.sprite == null || curSmallMapIcon.img.sprite.name != sprite) //如果当前sprite跟新sprite不一样时设置为新的sprite
                                curSmallMapIcon.SetSprite(previousImg);
                            curSmallMapIcon.rt.anchoredPosition = pos;
                        }
                    }
                }
            }
        }
    }

    private void UpdateBombPoint(BasePlayer curPlayer)
    {
        if (BurstBehavior.singleton == null) return;
        Vector2 playerPos = new Vector2(curPlayer.position.x, curPlayer.position.z);
        List<Vector3> bpList = BurstBehavior.singleton.BombPointList;
        for (int i = 0; i < bpList.Count; i++)
        {
            Vector2 point = new Vector2(bpList[i].x, bpList[i].y);
            if (Vector2.SqrMagnitude(point - playerPos) > Math.Sqrt(MAX_VIEW_R_PIXEL/curResolution))//调整坐标
            {
                point = Vector2.ClampMagnitude(point - playerPos, MAX_VIEW_R_PIXEL/curResolution) + playerPos;
            }
            Vector2 direction = point - centerPos;
            Vector2 pos = direction * curResolution;
            if (goIconList.Count <= curIconIndex)
            {
                var tempSmallMapIcon = new SmallMapIcon();
                goIconList.Add(tempSmallMapIcon);
            }
            SmallMapIcon curMapSmallIcon = goIconList[curIconIndex++];

            if (!curMapSmallIcon.isInit)
            {
                UIHelper.ShowSprite(ref curMapSmallIcon.go, "BombPoint" + i, AtlasName.BattleNew, (int)bpList[i].z == 1 ? "icon_a" : "icon_b", pos, true, null, smallMapTrans, true);
                curMapSmallIcon.isInit = true;
            }
            else
            {
                curMapSmallIcon.rt.anchoredPosition = pos;
            }
        }
        if (MainPlayer.singleton!=null && MainPlayer.singleton.Camp==(int)WorldManager.CAMP_DEFINE.REBEL && PlayerBattleModel.Instance.bombHolder == 0)//==0时有可能是掉了
        {
            if (BurstBehavior.singleton.bomb_drop_point != null && BurstBehavior.singleton.bomb_drop_point.Length > 2)
            {
                var bomb_p = new Vector2(BurstBehavior.singleton.bomb_drop_point[0], BurstBehavior.singleton.bomb_drop_point[2]);
                if (Vector2.SqrMagnitude(bomb_p - playerPos) > Math.Sqrt(MAX_VIEW_R_PIXEL / curResolution))//调整坐标
                {
                    bomb_p = Vector2.ClampMagnitude(bomb_p - playerPos, MAX_VIEW_R_PIXEL / curResolution) + playerPos;
                }
                Vector2 pos = (bomb_p - centerPos) * curResolution;
                if (goIconList.Count <= curIconIndex)
                {
                    goIconList.Add(new SmallMapIcon());
                }
                SmallMapIcon curMapSmallIcon = goIconList[curIconIndex++];
                if (!curMapSmallIcon.isInit)
                {
                    UIHelper.ShowSprite(ref curMapSmallIcon.go, "BombC4", AtlasName.Battle, "c4bomb", pos, true, new RectTransformProp() { scale = Vector3.one * 0.4f }, smallMapTrans, true);
                    curMapSmallIcon.isInit = true;
                }
                else
                {
                    curMapSmallIcon.rt.anchoredPosition = pos;
                }
                
            }
        }
    }

    private void UnShowSomeIcon()
    {
        for (; curIconIndex < goIconList.Count; curIconIndex++)
        {
            //goIconList[curIconIndex].go.TrySetActive(false);
            goIconList[curIconIndex].img.enabled = false;
        }
    }

    private void OnShowSmallMap()
    {
        if (smallMapTrans == null || smallMapGo == null) return;
        canShow = !smallMapGo.activeSelf;
        smallMapGo.TrySetActive(canShow);
        showMapBtnImg.transform.localEulerAngles = new Vector3(0, 0, canShow ? 180 : 0);
    }
}
