﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class BattleViewMode
{
    //================================独立组件
    static public uint MODE_INFO = 1;//顶部中间面板
    static public uint MODE_SUBINFO = 2;//左上角玩家头像
    // 4 闲置
    //static public uint MODE_ADDINFO = 4;//玩家头像下面的另一个头像
    static public uint MODE_FUNCTION = 8;//控制speaker，设置，战绩
    static public uint MODE_SKILL = 16;
    static public uint MODE_WEAPON = 32;//管理武器相关（穿甲弹、装弹、换枪）
    static public uint MODE_ACTION = 64;//管理动作（跳、蹲）
    static public uint MODE_CTRL = 128;//方向摇杆、攻击摇杆
    static public uint MODE_SNIPER = 256;//狙击镜
    static public uint MODE_GUIDE_CTRL = 1024;//指引控制
    static public uint MODE_GUN_ROUTE = 2048;//枪王路线面板
    static public uint MODE_GUN_ROUTE_TEAM = 4096;//枪王路线面板
    static public uint MODE_SWITCH_WEAPON = 8192;//切换武器    
    static public uint MODE_TEAM_HERO_INFO = 16384;//团队英雄面板 
    //================================组合模式
    static public uint MODE_NONE = 0;
    //新手指引
    static public uint MODE_GUIDE = 2 | 32 | 128 | 1024 | 8192;
    //死亡(默认只有个人信息和控制，再根据实际游戏模式增加对应的战斗信息组件)
    static public uint MODE_DEAD = 2 | 8 | 8192;
    //普通模式
    static public uint MODE_NORMAL = 1 | 2 | 8 | 32 | 64 | 128 | 8192;
    //dota模式
    static public uint MODE_DOTA = 2 | 8 | 32 | 64 | 128 | 8192;
    //生存模式
    static public uint MODE_SURVIVAL = 2 | 8 | 32 | 64 | 128 | 8192;
    //生化模式-人类模式
    static public uint MODE_ZOMBIE_H = 1 | 2 | 4 | 8 | 32 | 64 | 128 | 8192;
    //生化模式-感染模式
    static public uint MODE_ZOMBIE_Z = 1 | 2 | 4 | 8 | 16 | 64 | 128;
    //英雄模式
    static public uint MODE_HERO = 1 | 2 | 4 | 8 | 64 | 128 | 8192;
    //枪王模式-（个人、团队）
    static public uint MODE_KING_OF_GUN = 2 | 8 | 32 | 64 | 128 | 2048 | 8192;
    static public uint MODE_TEAM_KING_OF_GUN = 2 | 8 | 32 | 64 | 128 | 4096 | 8192;
    //躲猫猫模式（物件）
    static public uint MODE_HIDE_O = 1 | 2 | 4 | 8 | 64 | 128;
    //躲猫猫模式（人类）
    static public uint MODE_HIDE_H = 1 | 2 | 4 | 8 | 32 | 64 | 128 | 8192;
    //团队英雄
    //static public uint MODE_TEAM_HERO = 2 | 8 | 32 | 64 | 128 | 8192 | 16384;
}

public partial class PanelBattle : BasePanel
{
    private ConfigBattleUIControlPos m_defaultControlPosInfo;
    const string SAVE_BATTLE_UI_POS_KEY = "user_set_battle_ui_pos_03";

    uint m_mode = BattleViewMode.MODE_NONE;

    int _team_king_of_gun_step = 0;
    bool _round_become_zombie;
    //bool _round_play_zombie_hero_stage;

    private GameObject m_sightCrossHair;    //开镜准星
    private Image m_CrossHairCenterImage;//瞄准镜中间img
    private Image m_CrossHairHImage;
    private Image m_CrossHairVImage;

    public static RectTransform m_CircleCrossImage;
    public static Image m_CircleImg;
    public static RectTransform[] m_crossHairs;
    private Image[] m_crossHairsColor;
    private Image m_greenCrossHair;
    
    int m_iBurstPoint;

    //战斗开始特效
    private UIEffect m_startBattleEffect;

    Transform m_partnerCameraControllPanel;
    private Button m_bag;
    private GameObject m_bagPCKey;
    private DataGrid m_bagItems;
    GameObject m_bagview;
    GameObject m_switch;
    GameObject m_goodjob;
    GameObject m_goodjobnum;
    Text m_gjobnum;

    GameObject m_BattleInfoPanel;
    UIBattleSubInfo m_BattleSubInfo;
    //BattleAddInfoRender m_BattleAddInfo;
    bool m_battle_info_loading;
    bool m_gun_route_loading;
    bool m_team_gun_route_loading;
    bool m_team_hero_info_loading;
    BattleGunRoutePanel m_gun_route_panel;
    BattleTeamGunRoutePanel m_team_gun_route_panel;
    BattleTeamHeroInfoPanel m_team_hero_info_panel;
    GameObject m_panelmvp;

    Text m_mvpname;
    //GameObject m_goZombieHeroInfo;
    //ZombieHeroInfo m_ZombieHeroInfo;

    GameObject m_setting_btn;
    GameObject m_audio_btn;
    //    GameObject m_record_btn;

   
    GameObject m_SkillList;

    ArmorBullet m_ArmorBullet;
    UIGunList m_goGunList;
    GameObject m_LoadBullet;
    GameObject m_goAutoSwitchGun;


    Toggle m_Magnifier;// 放大镜
    GameObject m_UnMagnifier;

    //子武器攻击
    GameObject m_goSubFire;
    Image m_imgSubFire;

    GameObject m_paneltitle;
    Text m_title;

    //观战类型
    int m_watchType;  //0：第三人称 1：第一人称
    public int watchType
    {
        get { return m_watchType; }
        set { m_watchType = value; }
    }
    GameObject m_goSwitchView;
    Button m_btnSwitchView;
    UGUIClickHandler m_chSwitchView;
    Image m_imgSwitchView;

    //武器技能
    private const int WEAPON_SKILL_1 = 0;
    private const int WEAPON_SKILL_2 = 1;
    private const int WEAPON_SKILL_COUNT = 2;
   

#if UNITY_ANDROID || UNITY_IPHONE
    private Vector3[] m_skillBtnPosArr = new Vector3[]{new Vector3(0f,0f,0f), new Vector3(572f,468f,0f)};
#else
    private Vector3[] m_skillBtnPosArr = new Vector3[]{new Vector3(0f,0f,0f), new Vector3(78f,0f,0f)};
#endif

    Transform m_weaponSkillTran;
    List<GameObject> m_goNormalSkillList;
    List<SkillItemRender> m_normalSkillRenderList;

    //普通武器技能
    Dictionary<int, SkillItemRender> m_posSkillItemRenders = new Dictionary<int, SkillItemRender>();
    Dictionary<int, GameObject> m_skillGos = new Dictionary<int, GameObject>();
    Dictionary<GameObject, SkillItemRender> m_skillGo2SkillRender = new Dictionary<GameObject, SkillItemRender>();
    private Dictionary<int, SkillItemRender> m_dicSkillItemRender = new Dictionary<int, SkillItemRender>();

    private List<int> m_skillIdList = new List<int>();
    private List<SkillItemRender> m_skillItemPoolList = new List<SkillItemRender>();

    //觉醒武器技能
    private GameObject m_goAwakenSkill;
    private WeaponSkillItemRender m_awakenSkillRender;
    int m_awakenWeaponId = -1;
    bool m_isShowWeaponSkill;

    //被动技能列表
    GameObject m_goPassiveSkill;
    GameObject m_goPassiveSkillText;

    GameObject m_Squat;
    GameObject m_Jump;
    GameObject m_goLeftJump;
    private Transform m_tranRoomTip;
    public Transform TranRoomTip
    {
        get
        {
            return m_tranRoomTip;
        }
    }
    GameObject m_goJoystickCross;
    GameObject m_goJoystickTriangle;
    GameObject m_goJoystick;
    GameObject m_goJoyback;
    GameObject m_cloneJoybackGo;
    RectTransform m_cloneJoybackRt;
    float m_joystickMoveType = 0.1f;
    float m_heavyKnifeAtkType = 0;
    Image m_JoybackImg;
    Image m_JoystickImg;
    Vector2 m_kOrigJoyPos;
    Vector2 m_kOrigStickPos;
    private int m_iJoyFingerId = -1;
    private float m_fMaxJoyDis = 0f;
    private float m_fMaxJoyDisPow = 0f;
    private float m_fSlowJoyDisPow = 0f;
    private Vector3 m_moveDir = Vector3.zero;
    private bool m_bJoyTouch = false;

    private GameObject m_goAim;
    private RectTransform m_rtAim;
    private GameObject m_goShoot;
    private RectTransform m_rtShoot;
    private Vector3 m_kAimOriginPos;
    private static RectTransform m_parentRectTransform;
    private Vector3 m_vecDelta;
    //private MainPlayer m_kMP;

    private float m_fMaxShootDis = 0f;
    private float m_fMaxShootDisPow = 0f;

    private int m_iAimFingerId = -1;
    public static float TOUCH_MOVE_SENSITIVITY = 1.5f;

    private bool m_bCover = false;

    public static bool m_useRotationRough = false;
    private TouchRotationSmooth m_touchRotationSmooth;
    private TouchRotationRough m_touchRotationRough;

    private float BE_HIT_TIME = 3f;

    PanelTouch m_panelTouch = null;

    private bool m_auto_play_audio;
    private float m_fAimSensity;
    public static float m_fAimAccStart;
    public static float m_fAimAccEnd;
    public static float m_fAimAccTimes;

    private Transform m_txtCenterName;
    private Text m_txtCenterName_txt;

    //聊天
    private GameObject m_goChat;//聊天相关的整体
    private Button m_btnChatType;
    private Button m_btnChatInput;
    private InputField m_chatInput;
    private DataGrid m_dataGridChat;
    private int m_chatType;
    private static List<ItemBattleChatInfo> m_chatDataList = new List<ItemBattleChatInfo>();

    private static bool m_bIsShootDown = false;

    GameObject m_goGrenade;
    private bool m_bGrenadeSwitch = false;//是否是切雷
    private bool m_bGrenadeUpdate = false;
    private bool m_bGrenadeReady = true;
    private static float GRENADE_TIME_INTERVAL = 1.5f;
    private static float GRENADE_TIME_UP_DOWN_INTERVAL = 0.5f;
    private float m_fLastGrenadeTime = 0f;
    private float m_fPressLastTime = 0f;//按压持续时间

    //
    GameObject m_goFlashBomb;
    private bool m_bFlashBombSwitch = false;
    private bool m_bFlashBombUpdate = false;
    private bool m_bFlashBombReady = true;
    private float m_fLastFlashBombTime = 0f;
    private float m_fFlashBombPressLastTime = 0;

    private int m_iSelectedPlayerID;

    private int m_iBeRescuedPlayerID;

    private float m_fTick = 0.0f;
    private GameObject m_goFps;
    private Text m_txtFps;  //帧率和ping值
    bool m_rollershow;

    //巅峰对决
    private GameObject m_panelMask;
    private GameObject m_fightVideoGo;
    private Text m_playSpeedText;
    private Text m_openDanMubtnText;

    //躲猫猫嘲讽
    private GameObject m_PanelTaunt;
    private GameObject m_BtnTaunt;
    private GameObject m_Emotions;
    private GameObject[] m_EmoItem = new GameObject[4];
    private Image m_tauntSkillMask;
    private GameObject m_showEmo;
    private Text m_showEmoTex;
    private Image m_showEmoIcon;
    Transform m_muchKillMsg;
    //自动开火
    public GameObject m_autoFire;
    private Transform m_autoFireOn;
    private Transform m_autoFireOff;
    private Text m_autoFireTip;
    private bool canShowAutoFire;

    //wifi信号
    Image m_wifiIcon;
    Text m_wifiText;

    //作战指示
    private Image m_FightDirectionIcon;
    private GameObject m_FightDirectionBtn;
    private GameObject m_FightDirectionPanel;
    public GameObject FightDirectionPanel { get { return m_FightDirectionPanel; } }

    //    
    public Transform m_transScreenEffect;
    private int m_timerID;

    ConfigGameRuleLine gameRuleLine = null;
    /// <summary>
    /// 是否是新版的射击UI
    /// </summary>
    private bool isNewShootUI = false;


    ///fjs
    /// <summary>
    /// PC端 WEB端需要隐藏的界面列表
    /// </summary>
    private List<GameObject> pcHideList;
    //是否显示UI界面 当为PC端这不显示
    private bool isShowUIExcPC = true;
    /// <summary>
    /// pc快捷键E的功能回调
    /// </summary>
    UGUIClickHandler.PointerEvetCallBackFunc pcShortCutKeyECallBack;
    /// <summary>
    /// PC版躲猫猫  解除锁定操作
    /// </summary>
    private GameObject m_pcUnLock;

    private bool m_bCheckMyViewerLoaded;
    private float m_fCheckMyViewerTick;

    private RectTransform m_tranPauseMask;
    private GameObject[] m_harmTextList = new GameObject[7];//击杀统计文字列表
    private Image m_ReloadBulletMask;
    private Text m_ReloadBulletCdText;

    //小地图
    private PanelBattleRadar m_panelBattleRadar;

    //boss警告,BOSS技能预警
    private UIEffect m_alertWarning;
    private List<int> m_alerttPlayerID = new List<int>();

    // debug
    static public string ms_lastECall = "no";

    public GameObject BowCrossHair;
    public GameObject bowPoint;
    public GameObject bowCircle;
    public GameObject crosshairG0;
    static public bool powerOn;
    Transform tranbowCircle;

    public GameObject JetGo;
    public Image JetWhite;
    public Image JetRed;
    public Image JetBg;
    public bool jetfadein = false;
    public bool jetfadeout = false;
    private bool isOpenDotaPlayerBuff
    {
        get
        {
            return UIManager.IsOpen<PanelBuyDotaPlayerBuff>();
        }
    }

    private bool isOpenFightDirection
    {
        get
        {
            return m_FightDirectionPanel != null && m_FightDirectionPanel.activeInHierarchy;
        }
    }

    // 战斗目标引导父接点
    private Transform m_targetGuide;
    public Transform targetGuide { get { return m_targetGuide; } }


    // 观战信息入口
    private GameObject m_spectatorInfo;
    public GameObject spectatorInfo { get { return m_spectatorInfo; } }

    class BeHitObj
    {
        const float STAY_TIME = 1f;
        const float VANISH_TIME = 1f;
        GameObject m_goBeHit;
        float m_fStartTime;
        Vector3 attackPos;
        Image m_img;


        public BeHitObj(GameObject go, Vector3 pos)
        {
            m_goBeHit = go;
            attackPos = pos;
            m_fStartTime = Time.realtimeSinceStartup;
            m_img = m_goBeHit.GetComponent<Image>();
            var cg = m_img.gameObject.AddComponent<CanvasGroup>();
            cg.interactable = true;
            cg.blocksRaycasts = false;
        }

        public bool Active()
        {
            float dt = Time.realtimeSinceStartup - m_fStartTime - STAY_TIME;
            if (dt <= 0)
                return true;
            if (dt >= VANISH_TIME)
                return false;
            Color color = m_img.color;
            color.a = (VANISH_TIME - dt) / VANISH_TIME;
            m_img.color = color;

            float angle = CalcAttackAngle(attackPos);
            Vector2 localPos;
            CalcBeHitPos(angle, out localPos);

            RectTransform rectTrans = m_goBeHit.GetComponent<RectTransform>();
            rectTrans.anchoredPosition = localPos;
            //rectTrans.Rotate(0f, 0f, angle);
            rectTrans.localEulerAngles = new Vector3(0f, 0f, angle);

            return true;
        }
        //m_gjobnum.text = "0";

        public void Destory()
        {
            GameObject.Destroy(m_goBeHit);
        }
    }
    class RedObj
    {
        public Image m_imgRed;
        public float m_fTime;
    }
    private List<BeHitObj> m_lstBeHitObjs = new List<BeHitObj>();
    private RedObj m_redObj = new RedObj();

    enum JUMP_BUTTON_DEFINE
    {
        DOUBLEB,
        SINGLEB
    }

    public PanelBattle()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattle");
        AddPreLoadRes("UI/Battle/PanelBattle", EResType.UI);
        AddPreLoadRes("UI/Battle/ViewTeamerView", EResType.UI);
        AddPreLoadRes("UI/Battle/ViewKillInfo", EResType.UI);
        AddPreLoadRes("UI/Battle/ViewRescueInfo", EResType.UI);
        AddPreLoadRes("UI/Common/DownWatch", EResType.UI);
        AddPreLoadRes("UI/Battle/ItemBag", EResType.UI);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Weapon", EResType.Atlas);        
        AddPreLoadRes("Atlas/BattleSmallMap", EResType.Atlas);
        AddPreLoadRes("Atlas/BattleNew", EResType.Atlas);
        AddPreLoadRes("Atlas/Nums", EResType.Atlas);
        AddPreLoadRes("Atlas/FightDirection", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        if( WorldManager.singleton.isDaggerOW )
            AddPreLoadRes("Atlas/DaggerOW", EResType.Atlas);
        //模式的预加载在WorldManager.GetPreloadRes里写
        //boss警告,BOSS技能预警
//        m_alerttPlayerID = new List<int>();
    }

    public override void Init()
    {
        ////> 测试修改位置的时候用
//        if (Application.isEditor)
//        {
//            PlayerPrefs.DeleteKey(SAVE_BATTLE_UI_POS_KEY);
//        }
        powerOn = false;
        m_defaultControlPosInfo = ConfigManager.GetConfig<ConfigBattleUIControlPos>();
        CloneCanDragElements();
        var kill_panel = m_tran.Find("killmsg").GetComponent<RectTransform>();
        kill_panel.gameObject.AddComponent<ShowKillMsg>();
        var goRoomTip = new GameObject("RoomTip").AddComponent<RectTransform>();
        goRoomTip.gameObject.layer = GameSetting.LAYER_VALUE_UI;
        goRoomTip.gameObject.AddComponent<Canvas>();
        goRoomTip.SetParent(m_tran, false);
        goRoomTip.gameObject.AddComponent<BattleRoomTips>();
        m_tranRoomTip = goRoomTip;
        var go_group = m_tranRoomTip.gameObject.AddComponent<CanvasGroup>();
        go_group.interactable = false;
        //go_group.blocksRaycasts = false;

        m_goGunList = m_tran.Find("GunList").gameObject.AddComponent<UIGunList>();

        m_go.AddComponent<BattleTipsHandler>();
        m_go.AddComponent<ChangeTeamView>().Init(m_tran);
        m_go.AddComponent<ShowRoleName>();

        m_parentRectTransform = m_go.transform.parent as RectTransform;
        m_touchRotationSmooth = new TouchRotationSmooth();
        m_touchRotationRough = new TouchRotationRough();
        m_goJoyback = m_tran.FindChild("JoystickBack").gameObject;
        m_goJoystick = m_tran.FindChild("JoystickBack/Joystick").gameObject;
        m_goJoystickCross = m_tran.FindChild("JoystickBack/JoystickCross").gameObject;
        m_goJoystickTriangle = m_tran.FindChild("JoystickBack/JoystickTriangle").gameObject;

        m_JoybackImg = m_goJoyback.GetComponent<Image>();
        m_JoystickImg = m_goJoystick.GetComponent<Image>();
        m_goAim = m_tran.FindChild("Aim").gameObject;
        m_rtAim = m_goAim.GetComponent<RectTransform>();
        m_kAimOriginPos = m_rtAim.anchoredPosition;
        m_goShoot = m_tran.FindChild("Aim/Shoot").gameObject;
        m_rtShoot = m_goShoot.GetComponent<RectTransform>();
        m_redObj.m_imgRed = m_tran.FindChild("Red").GetComponent<Image>();
        m_panelmvp = m_tran.Find("MvpPanel").gameObject;
        m_mvpname = m_tran.Find("MvpPanel/Name").GetComponent<Text>();
        m_panelmvp.TrySetActive(false);

        //初始化准星
        m_crossHairs = new RectTransform[5];
        m_crossHairs[0] = m_tran.Find("crosshairs/right") as RectTransform;
        m_crossHairs[1] = m_tran.Find("crosshairs/top") as RectTransform;
        m_crossHairs[2] = m_tran.Find("crosshairs/left") as RectTransform;
        m_crossHairs[3] = m_tran.Find("crosshairs/bottom") as RectTransform;
        m_crossHairs[4] = m_tran.Find("crosshairs/center") as RectTransform;

        m_CircleCrossImage = m_tran.Find("crosshairs/circle") as RectTransform;
        m_CircleImg = m_tran.Find("crosshairs/circle").GetComponent<Image>();

        m_crossHairsColor = new Image[5];
        m_crossHairsColor[0] = m_crossHairs[0].GetComponent<Image>();
        m_crossHairsColor[1] = m_crossHairs[1].GetComponent<Image>();
        m_crossHairsColor[2] = m_crossHairs[2].GetComponent<Image>();
        m_crossHairsColor[3] = m_crossHairs[3].GetComponent<Image>();
        m_crossHairsColor[4] = m_crossHairs[4].GetComponent<Image>();

        m_greenCrossHair = m_tran.Find("greencrosshair").GetComponent<Image>();

        m_partnerCameraControllPanel = m_tran.Find("ParterCameraControllPanel");
        if (m_partnerCameraControllPanel != null)
            m_partnerCameraControllPanel.SetParent(UIManager.m_rootLayer, false);
        else
        {
            m_partnerCameraControllPanel = UIManager.m_rootLayer.Find("ParterCameraControllPanel");
        }


        m_sightCrossHair = m_tran.Find("sightCrosshair").gameObject;
        m_CrossHairCenterImage = m_tran.Find("sightCrosshair/centerImage").GetComponent<Image>();
        m_CrossHairHImage = m_tran.Find("sightCrosshair/hImage").GetComponent<Image>();
        m_CrossHairVImage = m_tran.Find("sightCrosshair/vImage").GetComponent<Image>();

        m_BattleSubInfo = m_tran.Find("BattleSubInfo").gameObject.AddComponent<UIBattleSubInfo>();
        //m_BattleAddInfo = m_tran.Find("BattleAddInfo").gameObject.AddComponent<BattleAddInfoRender>();
        m_goChat = m_tran.Find("ChatGo").gameObject;
        m_btnChatType = m_tran.Find("ChatGo/btnChatType").GetComponent<Button>();
        m_btnChatInput = m_tran.Find("ChatGo/btnChatInput").GetComponent<Button>();
        m_chatInput = m_tran.Find("ChatGo/btnChatInput/InputField").GetComponent<InputField>();
        m_setting_btn = m_tran.Find("setting_btn").gameObject;
        // m_record_btn = m_tran.Find("record_btn").gameObject;
        m_audio_btn = m_tran.Find("ChatGo/btnChatVoice").gameObject;
        //if (Driver.m_platform == EnumPlatform.ios)
        //m_audio_btn.TrySetActive(false);

        m_SkillList = m_tran.Find("SkillList").gameObject;
        m_ArmorBullet = m_tran.Find("ArmorBullet").gameObject.AddComponent<ArmorBullet>();
        m_tran.Find("ArmorBullet/Key_T").gameObject.TrySetActive(false);
        m_LoadBullet = m_tran.Find("LoadBullet").gameObject;
        m_goAutoSwitchGun = m_tran.Find("SwitchGun").gameObject;

        m_Squat = m_tran.Find("Squat").gameObject;
        m_Squat.AddComponent<SquatAndJump>();
        m_Jump = m_tran.Find("Jump").gameObject;
        m_Jump.AddComponent<SquatAndJump>();
        m_goLeftJump = m_go.transform.Find("LeftJump").gameObject;
        m_goGrenade = m_go.transform.Find("Grenade").gameObject;
        m_goFlashBomb = m_go.transform.Find("FlashBomb").gameObject;

        m_Magnifier = m_go.transform.Find("Magnifier").GetComponent<Toggle>();
        m_UnMagnifier = m_go.transform.Find("UnMagnifier").gameObject;

        m_goSubFire = m_go.transform.Find("HeavyKnife").gameObject;
        m_imgSubFire = m_goSubFire.GetComponent<Image>();
        ShowSubFireBtn(MainPlayer.singleton != null && MainPlayer.singleton.subWeapon != null);

        m_goSwitchView = m_go.transform.Find("SwitchWatchView").gameObject;
        m_imgSwitchView = m_goSwitchView.GetComponent<Image>();
        m_btnSwitchView = m_goSwitchView.GetComponent<Button>();
        m_goSwitchView.TrySetActive(false);

        // 武器技能
        m_weaponSkillTran = m_tran.Find("WeaponSkill");
       //        m_goWeaponSkillList =  new GameObject[WEAPON_SKILL_COUNT];
//        m_weaponSkillList = new List<int>();
//        m_skillItemRenderList = new List<SkillItemRender>();
//        m_weaponSkillRenderList = new SkillItemRender[WEAPON_SKILL_COUNT];
//        for (int i = 0; i < WEAPON_SKILL_COUNT; i++)
//        {
//            var go = m_weaponSkillTran.Find("WeaponSkill" + i).gameObject;
//            m_goWeaponSkillList[i] = go;
//            m_weaponSkillRenderList[i] = go.AddComponent<SkillItemRender>();
//        }

//        m_goAwakenSkill = m_tran.Find("WeaponSkill/awakenSkillBtn").gameObject;
//        m_awakenSkillRender = m_goAwakenSkill.AddMissingComponent<WeaponSkillItemRender>();

        m_goPassiveSkill = m_go.transform.Find("PassiveSkill").gameObject;
        m_goPassiveSkillText = m_go.transform.Find("PassiveSkillText").gameObject;

        m_bag = m_tran.Find("Bagbtn").GetComponent<Button>();
        m_bagview = m_tran.Find("BagMenu").gameObject;
        m_bagview.TrySetActive(false);
        m_bagItems = m_tran.Find("BagMenu/Bag/BagItems/content").gameObject.AddComponent<DataGrid>();

        m_bagItems.SetItemRender(ResourceManager.LoadUIRef("UI/Battle/ItemBag"), typeof(ItemBag));
        m_bagItems.autoSelectFirst = false;


        m_PanelTaunt = m_tran.Find("PanelTaunt").gameObject;
        m_BtnTaunt = m_PanelTaunt.transform.Find("btnTaunt").gameObject;
        m_tauntSkillMask = m_BtnTaunt.transform.Find("btnmask").GetComponent<Image>();
        m_Emotions = m_PanelTaunt.transform.Find("emotions").gameObject;
        //m_Emotions.transform.Find("key123").gameObject.TrySetActive(false);
        m_showEmo = m_tran.Find("showEmo").gameObject;
        m_showEmoTex = m_showEmo.transform.Find("text").GetComponent<Text>();
        m_showEmoIcon = m_showEmo.transform.Find("icon").GetComponent<Image>();
        for (int i = 0; i < 3; i++)
        {
            m_EmoItem[i] = m_Emotions.transform.Find("content/" + i.ToString()).gameObject;
        }

        //自动开火
        m_autoFire = m_tran.Find("autoFire").gameObject; ;
        m_autoFireOn = m_autoFire.transform.Find("btnAutoFire/on");
        m_autoFireOff = m_autoFire.transform.Find("btnAutoFire/off");
        m_autoFireTip = m_autoFire.transform.Find("text").GetComponent<Text>();
        m_autoFire.gameObject.TrySetActive(false);

        //wifi信号
        m_wifiIcon = m_tran.Find("wifi/icon").GetComponent<Image>();
        m_wifiText = m_tran.Find("wifi/text").GetComponent<Text>();

        m_goodjob = m_tran.Find("btnGoodjob").gameObject;
        m_goodjobnum = m_tran.Find("goodjobnum").gameObject;
        m_gjobnum = m_tran.Find("goodjobnum/num").gameObject.GetComponent<Text>();
        m_switch = m_tran.Find("btnSwitch").gameObject;
        m_switch.gameObject.TrySetActive(false);
        m_goodjob.gameObject.TrySetActive(false);
        m_goodjobnum.gameObject.TrySetActive(false);
        if (Application.isEditor)
        {
            m_panelTouch = m_tran.GetComponent<PanelTouch>();
            if (m_panelTouch == null)
                m_panelTouch = m_tran.gameObject.AddComponent<PanelTouch>();
        }

        m_txtCenterName = m_tran.Find("CenterName");
        m_txtCenterName_txt = m_txtCenterName.Find("Name_txt").GetComponent<Text>();

        updateViewMode(BattleViewMode.MODE_NONE, true);
        UpdateUserSetUIPos(ReadControlPosFromPlayerPrefs());
        UpdateJoyPos();
        UpdateShooterPos();

        m_fAimAccStart = ConfigMisc.GetInt("fight_aim_sensity_start");
        m_fAimAccEnd = ConfigMisc.GetInt("fight_aim_sensity_end");

        if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.isDead)
        {
            OnPlayerStartDie();
        }
        m_rollershow = false;
        m_dataGridChat = m_tran.Find("ChatGo/chat").gameObject.AddComponent<DataGrid>();
        m_dataGridChat.useClickEvent = false;
        m_dataGridChat.autoSelectFirst = false;
        m_dataGridChat.SetItemRender(m_tran.Find("ChatGo/chat/tips").gameObject, typeof(ItemBattleChatRender));

        m_goFps = m_tran.Find("fpsCanvas").gameObject;
        m_txtFps = m_tran.Find("fpsCanvas/fps").GetComponent<Text>();
        m_goFps.TrySetActive(false);

        m_goDropGunBtn = m_tran.Find("DropGunBtn").gameObject;
        m_goDropGunBtn.TrySetActive(false);

        m_harmListTips = m_tran.Find("HarmInfoList").gameObject;
        m_harmListTips.TrySetActive(false);
        m_killerNameText = m_tran.Find("HarmInfoList/killerName_txt").GetComponent<Text>();
        m_harmTipGunNameText = m_tran.Find("HarmInfoList/GumName_txt").GetComponent<Text>();
        for (int temp = 0; temp <= 6; temp++)
        {
            m_harmTextList[temp] = m_tran.Find("HarmInfoList/KillItem" + temp).gameObject;
            m_harmTextList[temp].TrySetActive(false);
        }

        m_GunNameText = m_tran.Find("PickUpGunBtn/GunNameText").GetComponent<Text>();
        m_PickUpGunBtn = m_tran.Find("PickUpGunBtn").gameObject;
        m_PickUpGunBtn.TrySetActive(false);

        m_ReloadBulletMask = m_tran.Find("LoadBulletMask").GetComponent<Image>();
        m_ReloadBulletMask.fillAmount = 1;
        m_ReloadBulletMask.gameObject.TrySetActive(false);

        m_ReloadBulletCdText = m_ReloadBulletMask.transform.Find("ReloadBulletCdText").GetComponent<Text>();
        m_ReloadBulletCdText.text = "";

        m_muchKillMsg = m_tran.Find("BattleSubInfo/Muchkillmsg");

        //小地图
        //var smallMap = m_tran.Find("SmallMap/Map");
        //smallMapGo = smallMap.gameObject;
        //smallMapTrans = smallMap.Find("maskMap/map") as RectTransform;
        //mainIconTrans = smallMap.Find("MainIcon") as RectTransform;
        //showMapBtnGo = m_tran.Find("SmallMap/ShowSmallMapBtn").gameObject;
        //showMapBtnImg = showMapBtnGo.GetComponent<Image>();
        //smallMapGo.TrySetActive(false);
        m_panelBattleRadar = UIManager.PopPanel<PanelBattleRadar>();
        // ScreenEffect
        m_transScreenEffect = m_tran.Find("ScreenEffect");
        if (m_transScreenEffect != null)
        {
            if (ScreenEffectManager.singleton == null)
                new ScreenEffectManager();
            ScreenEffectManager.singleton.Init(m_transScreenEffect);
        }

        m_paneltitle = m_tran.Find("TitlePanel").gameObject;
        m_title = m_tran.Find("TitlePanel/Text").GetComponent<Text>();

        m_alerttPlayerID.Clear();

        m_tranPauseMask = m_tran.Find("pauseMask").GetComponent<RectTransform>();

        m_pcUnLock = m_tran.Find("PcUnLock").gameObject;
        m_pcUnLock.TrySetActive(false);
        m_pcChatInputGo = m_tran.Find("PcChatInput").gameObject;
        m_pcChatSend = m_pcChatInputGo.transform.Find("BtnSend").gameObject;
        m_pcChatInputText = m_pcChatInputGo.transform.Find("InputField").GetComponent<InputField>();
        m_pcChatInputText.gameObject.AddComponent<InputFieldFix>();
        m_pcChatChannel = m_pcChatInputGo.transform.Find("btnChatType").GetComponent<Button>();
        showPcChatInput(false);
        m_goChat.transform.Find("PC_btnChatVoice").gameObject.TrySetActive(false);
        if (WorldManager.singleton.fightEntered)
        {
            if (!PlayerBattleModel.Instance.canStart)
                UIManager.PopPanel<PanelBattleReady>();
        }

        m_targetGuide = m_tran.Find("TargetGuide");
        m_spectatorInfo = m_tran.Find("SpectatorInfo").gameObject;

        m_cloneJoybackGo = GameObject.Instantiate(m_goJoyback.gameObject) as GameObject;
        
        m_cloneJoybackGo.transform.parent = m_goJoyback.transform.parent;
        m_cloneJoybackGo.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
        m_cloneJoybackRt = m_cloneJoybackGo.AddMissingComponent<RectTransform>();
        m_cloneJoybackRt.anchoredPosition = m_goJoyback.GetComponent<RectTransform>().anchoredPosition;
        m_cloneJoybackGo.TrySetActive(false);

        m_panelMask = m_tran.Find("panelMask").gameObject;
        m_fightVideoGo = m_tran.Find("FightVideoGo").gameObject;
        m_playSpeedText = m_fightVideoGo.transform.Find("btnMsgSpeed/Text").GetComponent<Text>();
        m_openDanMubtnText = m_fightVideoGo.transform.Find("btnOpenMsg/Text").GetComponent<Text>();
        //作战指示
        m_FightDirectionBtn = m_tran.Find("FightDirectionBtn").gameObject;
        m_FightDirectionIcon = m_FightDirectionBtn.GetComponent<Image>();
        gameRuleLine = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);

        BowCrossHair = m_tran.Find("crosshairsBow").gameObject;
        bowPoint = m_tran.Find("crosshairsBow/bowPoint").gameObject;
        bowCircle = m_tran.Find("crosshairsBow/bowCircle").gameObject;
        crosshairG0 = m_tran.Find("crosshairs").gameObject;
        tranbowCircle = bowCircle.transform;


        JetGo = m_tran.Find("jetImg").gameObject;
        JetBg = m_tran.Find("jetImg").GetComponent<Image>();
        JetRed = m_tran.Find("jetImg/red").GetComponent<Image>();
        JetWhite = m_tran.Find("jetImg/white").GetComponent<Image>();

        //fjs  2015.12.23  m_goChat
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        isShowUIExcPC = false;
        m_goChat.transform.Find("btnChatType").gameObject.TrySetActive(false);
        m_goChat.transform.Find("btnChatInput").gameObject.TrySetActive(false);
        m_goChat.transform.Find("btnChatVoice").gameObject.TrySetActive(false);

        m_FightDirectionIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "fightDirectionPc"));
        m_Emotions.transform.Find("key123").gameObject.TrySetActive(true);

        if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_T_DOWN))
        {
            m_tran.Find("ArmorBullet/Key_T").gameObject.TrySetActive(true);
            m_BtnTaunt.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_tauntBtn"));
        }
        if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_R_DOWN))
            m_switch.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_switch"));
        if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_B_DOWN))
            m_bag.gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_btn_bag"));
        // UIHelper.ShowTextAlign(ref m_bagPCKey, GlobalBattleParams.singleton.GetPcKeyTextByKey(GameEvent.INPUT_KEY_B_DOWN), true, Vector2.zero, null, m_bag.gameObject.transform, true, TextAnchor.LowerCenter);
        if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_H_DOWN))
            m_goodjob.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_goodjob"));
        pcHideList = new List<GameObject>() { 
             m_setting_btn, m_goJoyback, m_goAim,
            m_LoadBullet,m_LoadBullet.gameObject,m_goAutoSwitchGun ,m_goGrenade,m_goFlashBomb,m_UnMagnifier,
        m_Magnifier.gameObject,m_Squat,m_Jump,m_goLeftJump,m_goDropGunBtn,m_goSubFire,};
         if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_G_DOWN))
              m_PickUpGunBtn.transform.Find("GunBG").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "PC_changeGun"));
#endif
    }

    public void CheckJetPack()
    {
        if (MainPlayer.singleton != null&&MainPlayer.singleton.canFly)
        {
            if (MainPlayer.singleton.skillMgr != null)
            {
                if (MainPlayer.singleton.skillMgr.flm != null)
                {
                    JetGo.TrySetActive(true);
                    GameDispatcher.Dispatch(GameEvent.PLAYER_CHECK_FLY);
                    return;
                }
            }
        }
        JetGo.TrySetActive(false);
        GameDispatcher.Dispatch(GameEvent.PLAYER_CHECK_FLY);
    }



    public void CheckBowCross()
    {
        if (MainPlayer.singleton != null)
        {
            if (MainPlayer.singleton.curWeapon != null)
            {
                BowCrossHair.TrySetActive((MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPOM_TYPE_BOW));
                crosshairG0.TrySetActive(!(MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPOM_TYPE_BOW));
            }
            else
            {
                BowCrossHair.TrySetActive(false);
            }
        }
        else
        {
            BowCrossHair.TrySetActive((false));
        }
    }


    void UpdateBag()
    {
        if (WorldManager.singleton.m_bagdata != null)
        {
            var bag_bg = m_bagview.transform.Find("Bag/Bagbg");
            ((RectTransform)bag_bg).sizeDelta = new Vector2(195, 30 + 51 * Mathf.Min(Mathf.Max(WorldManager.singleton.m_bagdata.Length, 1), 8));
            m_bagItems.Data = WorldManager.singleton.m_bagdata;
            var bag_mask = m_bagview.transform.Find("Bag/BagItems");
            ((RectTransform)bag_mask).sizeDelta = ((RectTransform)bag_bg).sizeDelta;
        }
    }

    void CloneCanDragElements()
    {
        for (int i = 0; i < m_defaultControlPosInfo.m_dataArr.Length; i++)
        {
            string controlname = m_defaultControlPosInfo.m_dataArr[i].key;
            Transform uiObj = m_go.transform.Find(controlname);
            if (uiObj != null)
            {
                PanelUserDefine.AddCanDragElements(controlname, uiObj.gameObject);
            }
        }
    }

    public override void InitEvent()
    {
        AddEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION, changeFightDirection);
        GameDispatcher.AddEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE, changeHeavyKnifeMode);
        GameDispatcher.AddEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE, changeJoystick);
        GameDispatcher.AddEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE, changeJoystickMove);
        GameDispatcher.AddEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_CROSS_TYPE, changeCrossType);
        GameDispatcher.AddEventListener<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, SetCrossHairVisible);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_ROUND_OVER, RoundEnd);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_OVER, RoundEnd);
        GameDispatcher.AddEventListener(GameEvent.UI_CLICK_STABLE, () => UpdateUserSetUIPos(ReadControlPosFromPlayerPrefs()));
        UGUIClickHandler.Get(m_setting_btn).onPointerClick += delegate { UIManager.PopPanel<PanelSetting>(null, true); };
        //        UGUIClickHandler.Get(m_record_btn).onPointerClick += delegate { UIManager.PopPanel<PanelBattleRecord>(null, true); };
        //if (Driver.m_platform == EnumPlatform.ios)
        //{
        //    UGUIUpDownHandler.Get(m_audio_btn).onPointerDown += null;
        //    UGUIUpDownHandler.Get(m_audio_btn).onPointerUp += null;
        //}
        // else
        //{
        UGUIUpDownHandler.Get(m_audio_btn).onPointerDown += OnAudioBtnActionDown;
        UGUIUpDownHandler.Get(m_audio_btn).onPointerUp += OnAudioBtnActionUp;
        // }

        UGUIUpDownHandler.Get(m_goGrenade).onPointerDown += OnGrenadeButtonDown;
        UGUIUpDownHandler.Get(m_goGrenade).onPointerUp += OnGrenadeButtonUp;
        UGUIUpDownHandler.Get(m_goFlashBomb).onPointerDown += OnFlashBombButtonDown;
        UGUIUpDownHandler.Get(m_goFlashBomb).onPointerUp += OnFlashBombButtonUp;
        UGUIClickHandler.Get(m_UnMagnifier).onPointerClick += delegate { GameDispatcher.Dispatch(GameEvent.UI_CANCLE_FIRE); };
        UGUIClickHandler.Get(m_Magnifier.gameObject).onPointerClick += OnClickMagnifier;
        UGUIClickHandler.Get(m_goAutoSwitchGun).onPointerClick += delegate { OnPlayerSwitchNextGun(); };
        UGUIDragHandler.Get(m_partnerCameraControllPanel).onDrag += OnDragPartnerCameraControll;
        UGUIUpDownHandler.Get(m_goShoot).onPointerDown += OnShootPointerDown;
        UGUIUpDownHandler.Get(m_goShoot).onPointerUp += OnShootPointerUp;
        UGUIClickHandler.Get(m_LoadBullet).onPointerClick += delegate { GameDispatcher.Dispatch(GameEvent.INPUT_RELOAD_AMMO); };
        UGUIClickHandler.Get(m_bag.gameObject).onPointerClick += ShowBag;
        UGUIClickHandler.Get(m_btnChatType.gameObject).onPointerClick += OnBtnChatTypeClick;
        UGUIClickHandler.Get(m_btnChatInput.gameObject).onPointerClick += OnBtnChatInputClick;
        m_chatInput.gameObject.AddComponent<InputFieldFix>().onEndEdit += OnChatEditEnd;
        GlobalBattleParams.singleton.NotifyEvent += OnPlayerChangeBattleParamSetting;
        UGUIClickHandler.Get(m_switch).onPointerClick += OnClickSwitch;
        UGUIClickHandler.Get(m_goodjob).onPointerClick += OnClickGoodJob;
        UGUIClickHandler.Get(m_goDropGunBtn).onPointerClick += OnClickDropGun;
        UGUIClickHandler.Get(m_PickUpGunBtn).onPointerClick += OnClickPickUpGun;
        UGUIUpDownHandler.Get(m_goSubFire).onPointerDown += OnSubFireDown;
        UGUIUpDownHandler.Get(m_goSubFire).onPointerUp += OnSubFireUp;
        UGUIClickHandler.Get(m_autoFire).onPointerClick += OnClickAutoFire;
        UGUIClickHandler.Get(m_autoFireOn).onPointerClick += OnClickAutoFire;
        UGUIClickHandler.Get(m_autoFireOff).onPointerClick += OnClickAutoFire;
        UGUIClickHandler.Get(m_BtnTaunt).onPointerClick += OnBtnTauntClick;

        UGUIClickHandler.Get(m_FightDirectionBtn).onPointerClick += OnBtnFightDirectionClick;
        if (FightVideoManager.isPlayFight == true)
        {
            UGUIClickHandler.Get(m_fightVideoGo.transform.Find("btnSendMsg").gameObject).onPointerClick += OnBtnFightVideoOpenSendMsgClick;
            UGUIClickHandler.Get(m_fightVideoGo.transform.Find("btnOpenMsg").gameObject).onPointerClick += OnBtnFightVideoShowMsgClick;
            UGUIClickHandler.Get(m_fightVideoGo.transform.Find("btnMsgSpeed").gameObject).onPointerClick += OnBtnFightVideoChangeSpeedClick;
        }
        
        for (int i = 0; i < 3; i++)
        {
            UGUIClickHandler.Get(m_EmoItem[i].transform).onPointerClick += OnEmoItemClick;
        }

        m_chSwitchView = UGUIClickHandler.Get(m_goSwitchView);
        m_chSwitchView.onPointerClick += OnClickSwitchView;

        //UGUIClickHandler.Get(showMapBtnGo).onPointerClick += delegate { OnShowSmallMap(); };

//        for (int i = 0; i < m_goWeaponSkillList.Length; i++)
//        {
//            UGUIClickHandler.Get(m_goWeaponSkillList[i]).onPointerClick += onWeaponSkillItemClick;
//        }

        AddEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        AddEventListener(GameEvent.MAINPLAYER_GAME_START_DIE, OnPlayerStartDie);

        AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        AddEventListener<BasePlayer, bool>(GameEvent.UI_SHOW_AIM_PLAYER_NAME, OnAimPlayer);
        AddEventListener(GameEvent.UI_CANCEL_AIM_PLAYER_NAME, OnCancelAimPlayer);
        AddEventListener<int>(GameEvent.GAME_SHOW_GUNSCOPE, onShowGunScope);
        AddEventListener<ConfigBattleUIControlPos>(GameEvent.UI_USER_DEFINE_CONTROL_POS, OnUserSetPos);
        AddEventListener(GameEvent.GAME_BATTLE_ROUND_START, OnBattleRoundStart);
        AddEventListener(GameEvent.GAME_BATTLE_OVER, OnBattleOver);
        AddEventListener<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_ENTER_BURST_POINT, OnEnterBurstPoint);
        AddEventListener<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_STAYIN_BURST_POINT, OnStayInBurstPoint);
        AddEventListener<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_LEAVE_BURST_POINT, OnLeaveBurstPoint);
        //AddEventListener<bool>( GameEvent.UI_SHOW_GUNSCOPE_BUTTON, OnShowMagnifier );
        AddEventListener<bool>(GameEvent.UI_SHOW_CANCLE_FIRE_BTN, OnShowUnMagnifier);
        AddEventListener<int>(GameEvent.UI_SET_GUNSCOPE_BUTTON_STATUS, onShowGunScope);
        AddEventListener<Vector3>(GameEvent.MAINPLAYER_BEHIT, OnPlayerBeHit);
        AddEventListener(GameEvent.UI_SETTING_OPEN, OnCover);
        AddEventListener(GameEvent.UI_SEETING_CLOSE, OnUnCover);
        AddEventListener(GameEvent.UI_SETTING_UPDATE, OnSettingUpdate);
        AddEventListener<bool>(GameEvent.UI_HIDE_MOVE_ICON, OnForbidMove);
        AddEventListener<bool>(GameEvent.UI_HIDE_SWITCH_ICON, OnForbidSwitch);
        AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        //AddEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnBattleInfoUpdate);
        //AddEventListener<BasePlayer>(GameEvent.PROXY_PLAYER_ACTOR_STATE, OnActorStateUpdated);
        AddEventListener<string>(GameEvent.UI_UPDATE_GUNLIST, OnPlayerSwitchGun);
        AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchCameraToPartner);
        AddEventListener<int, string>(GameEvent.SDK_SPEECH_COMPLETE, OnSpeechHandler);
        AddEventListener<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, OnRescuePlayer);
        AddEventListener<toc_fight_survival_use_revivecoin>(GameEvent.PLAYER_USE_REVIVECOIN, OnPlayerUseRevivecoin); 
        AddEventListener(GameEvent.UI_EQUIP_UPDATE, OnUpdateRoleEquip);

        AddEventListener(GameEvent.MAINPLAYER_INIT_DONE, OnMainPlayerLoaded);
        AddEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerLoaded);
        AddEventListener<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, ShowSubFireBtn);
        AddEventListener<bool>(GameEvent.UI_SHOW_SWITCH_VIEW_BTN, ShowSwitchViewBtn);
        AddEventListener<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, EnableSwitchBtn);
        AddEventListener<int>(GameEvent.UI_UPDATE_WEAPON_SKILL, OnUpdateWeaponSkill);
        AddEventListener<bool>(GameEvent.UI_REFRESH_WEAPON_SKILL, OnRefreshWeaponSkill);
        AddEventListener<bool>(GameEvent.UI_SHOW_WEAPON_SKILL, ShowWeaponSkill);
        AddEventListener<int>(GameEvent.UI_REFRESH_SKILL, OnRefreshSkillItem);
        AddEventListener<int, string, int,long>(GameEvent.UI_REFRESH_SKILL_WITHPARAM, OnRefreshSkillWithParam);
        AddEventListener<bool>(GameEvent.UI_FOCUS_CHANGE, OnFocusChange);
        AddEventListener(GameEvent.UI_SWITCH_FREE_VIEW, OnSwitchFreeView);
        AddEventListener(GameEvent.UI_SWITCH_PERSON_VIEW, OnSwitchPersonView);
        AddEventListener(GameEvent.UI_RESET_WATCH_TYPE, ResetWatchType);

        GameDispatcher.AddEventListener<float, bool>(GameEvent.MAINPLAYER_RELOADBULLET_CD_MASK, updateReloadBulletCDMask);

        m_bagItems.onItemSelected += changeBag;
        AddEventListener<bool>(GameEvent.INPUT_KEY_MOVE, (isMove) =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, isMove);
        });
        AddEventListener(GameEvent.INPUT_KEY_Q_DOWN, () =>
        {
            if (MainPlayer.singleton == null) return;
            if (isPcChatShow && MainPlayer.singleton != null) return;//如果PC端的聊天面板打开 则不触发移动
            if (UIManager.IsOpen<PanelBattleZombie>())
            {
                if (UIManager.GetPanel<PanelBattleZombie>().isMatrixSwitchWeaponBtnShow()) return;
            }
            string strWeaponID = MainPlayer.singleton.GetNextWeaponID();
            if (string.IsNullOrEmpty(strWeaponID) == false)
                GameDispatcher.Dispatch(GameEvent.INPUT_SWITCH_WEAPON, strWeaponID);
        });

        AddEventListener<bool>(GameEvent.INPUT_KEY_SPACE_DOWN, (isDown) =>
        {
            if (isDown && UIManager.IsOpen<PanelFightVideoMask>())
            {
                UIManager.GetPanel<PanelFightVideoMask>().onReplayHandle(null, null);
                return;
            }
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            if (m_goSwitchView.activeSelf)
            {
                OnClickSwitchView(null, null);
                return;
            }
        });

        

        AddEventListener(GameEvent.INPUT_KEY_SPACE_DOWN, () =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            if (MainPlayer.singleton == null) return;
            if (MainPlayer.singleton.status.crouch)
                GameDispatcher.Dispatch(GameEvent.INPUT_STAND);
            else
                GameDispatcher.Dispatch(GameEvent.INPUT_JUMP);
        });


        AddEventListener(GameEvent.INPUT_KEY_SPACE_UP, () =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            if (MainPlayer.singleton == null) return;
            GameDispatcher.Dispatch(GameEvent.INPUT_JUMP_END);
        });

        AddEventListener<bool>(GameEvent.INPUT_KEY_SHIFT, (isDown) =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            if (isDown)
                GameDispatcher.Dispatch(GameEvent.INPUT_CROUCH);
            else
                GameDispatcher.Dispatch(GameEvent.INPUT_STAND);
        });

        AddEventListener(GameEvent.INPUT_KEY_P_DOWN, () =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发移动
            if (m_autoFire != null && m_autoFire.activeSelf)
            {
                GameDispatcher.Dispatch(GameEvent.MAINPLAYER_SWITCH_AUTOFIRE);
                RefreshAutoFire();
            }
        });


#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
        UGUIClickHandler.Get(m_pcChatSend).onPointerClick += onClickBtnChatSend;
        UGUIClickHandler.Get(m_pcChatChannel.gameObject).onPointerClick += OnBtnChatTypeClick;
        UGUIClickHandler.Get(m_pcUnLock).onPointerClick += onClickBtnDMMPcUnLock;
        GameDispatcher.AddEventListener<bool>(GameEvent.MAINPLAYER_SETFREEZE, (isFreeze) =>
         {
             if (MainPlayer.singleton.isDmmModel)
             {
                 /*if (isFreeze)
                    Screen.lockCursor = false;
                else
                    Screen.lockCursor = true;*/
                 if (m_pcUnLock == null)
                     return;
                 m_pcUnLock.TrySetActive(isFreeze && !isShowUIExcPC);
                 updatePcUnlockImg();
             }
         });

        AddEventListener<int>(GameEvent.INPUT_KEY_NUM_DOWN, (num) =>
        {
            if (isPcChatShow) return;//如果PC端的聊天面板打开 则不触发快捷键操作
            if (m_Emotions.activeSelf && m_PanelTaunt.activeSelf)
            {//躲猫猫嘲讽界面打开  发送表情
                if (num >= 1 && num <= 3)
                    sendTauntFace(num - 1);
                return;
            }
            if (m_bagview.gameObject.activeSelf)
                if (num >= 1 && num <= 8)
                    shortCutKeySelectBag(num);
        });
        AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () =>
        {
            if (isPcChatShow || isOpenDotaPlayerBuff || isOpenFightDirection) return;//如果PC端的聊天面板打开 则不触发快捷键操作
            if (m_bagview.gameObject.activeSelf) return;
            m_goGunList.dealShortcutKeySelectWeapon(GameConst.WEAPON_SUBTYPE_WEAPON1);
        });
        AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () =>
        {
            if (isPcChatShow || isOpenDotaPlayerBuff || isOpenFightDirection) return;
            if (m_bagview.gameObject.activeSelf) return;
            m_goGunList.dealShortcutKeySelectWeapon(GameConst.WEAPON_SUBTYPE_WEAPON2);
        });
        AddEventListener(GameEvent.INPUT_KEY_3_DOWN, () =>
        {
            if (isPcChatShow || isOpenDotaPlayerBuff || isOpenFightDirection) return;
            if (m_bagview.gameObject.activeSelf) return;
            m_goGunList.dealShortcutKeySelectWeapon(GameConst.WEAPON_SUBTYPE_DAGGER);
        });
        AddEventListener(GameEvent.INPUT_KEY_4_DOWN, () =>
        {
            if (isPcChatShow || isOpenFightDirection) return;
            if (m_bagview.gameObject.activeSelf) return;
            m_goGunList.dealShortcutKeySelectWeapon();
        });

        AddEventListener(GameEvent.INPUT_KEY_DOWNARROW_UP, () =>
        {
            OnBtnChatTypeClick(m_pcChatChannel.gameObject, null);
        });
        AddEventListener(GameEvent.INPUT_KEY_UPARROW_UP, () =>
        {
            OnBtnChatTypeClick(m_pcChatChannel.gameObject, null);
        });
        AddEventListener(GameEvent.INPUT_KEY_ENTER_UP, () =>
        {
            showPcChatInput(true, true);
        });

        

        AddEventListener<bool>(GameEvent.INPUT_KEY_V_DOWN, (isDown) =>
        {
            if (isPcChatShow) return;
            if (isDown)
                GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_DOWN);
            else
                GameDispatcher.Dispatch(GameEvent.INPUT_KEY_AUDIO_UP);
        });
        AddEventListener<bool>(GameEvent.INPUT_KEY_TAB_DOWN, (isDwon) =>
        {
            if (isDwon)
            {
                if (WorldManager.singleton.isSurvivalTypeModeOpen)
                {
                    return;
                }
                if (UIManager.IsOpen<PanelSetting>()) return;
                UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Player }, true);
            }
            else
            {
                if (UIManager.IsOpen<PanelBattleRecord>())
                    UIManager.GetPanel<PanelBattleRecord>().HidePanel();
            }
        });
        AddEventListener(GameEvent.INPUT_KEY_ESC_DOWN, () =>
        {
            if (UIManager.IsOpen<PanelFightVideoMask>())
            {
                UIManager.GetPanel<PanelFightVideoMask>().onExitHandle(null, null);
                return;
            }
            if (UIManager.IsOpen<PanelSetting>())
            {
                //UIManager.GetPanel<PanelSetting>().OnClickBackBattle(null, null);
            }
            else
                UIManager.PopPanel<PanelSetting>(null, true);
        });

        AddEventListener(GameEvent.INPUT_KEY_F_DOWN, () =>
        {
            if (isPcChatShow) return;
            
            if (m_posSkillItemRenders.ContainsKey(WEAPON_SKILL_1) && m_posSkillItemRenders[WEAPON_SKILL_1].isShow)
            {
                UseWeaponSkill(WEAPON_SKILL_1);
            }
            else if (m_SkillList.activeSelf)
            {
                OnSkillItemClick(null, null);
            }
        });

        AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_posSkillItemRenders.ContainsKey(WEAPON_SKILL_2) && m_posSkillItemRenders[WEAPON_SKILL_2].isShow)
            {
                UseWeaponSkill(WEAPON_SKILL_2);
            }
            else if (m_SkillList.activeSelf)
            {
                OnSkillItemClick(null, null);
            }
        });

        AddEventListener<bool>(GameEvent.INPUT_KEY_BACKQUOTE_DOWN, (isDwon) =>
        {
            if (m_FightDirectionBtn.activeInHierarchy == true)
            {
                OnBtnFightDirectionClick(null, null);
            }
        });
        AddEventListener<bool>(GameEvent.INPUT_KEY_BATTLESPECTATOR_DOWN, (isDwon) =>
        {
            if (isDwon == true)
            {
                if (UIManager.IsOpen<PanelSetting>()) return;
                UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Spectator }, true);
            }
            else 
            {
                if (UIManager.IsOpen<PanelBattleRecord>())
                    UIManager.GetPanel<PanelBattleRecord>().HidePanel();
            }
        }); 
        AddEventListener(GameEvent.INPUT_KEY_B_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_bag.gameObject.activeSelf)
            {
                ShowBag(null, null);
            }
        });
        AddEventListener(GameEvent.INPUT_KEY_T_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_PanelTaunt.activeSelf)
            {
                //躲猫猫模式嘲讽
                OnBtnTauntClick(null, null);
                return;
            }
            m_ArmorBullet.shortCutKeyClickArmorBullet();
        });
        AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_pcUnLock.activeSelf && MainPlayer.singleton.isDmmModel)//都猫猫模式下解锁
            {
                MainPlayer.singleton.SetHideView(false);
                return;
            }
            if (pcShortCutKeyECallBack != null)
            {
                if (null != pcShortCutKeyECallBack.Method)
                {
#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_EDITOR
                    ms_lastECall = pcShortCutKeyECallBack.Method.Name + " " + (int)Time.timeSinceLevelLoad;
                    Logger.Log("EC:" + ms_lastECall);
#endif
                }

                pcShortCutKeyECallBack(null, null);
            }
                
        });

        AddEventListener(GameEvent.INPUT_KEY_H_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_goodjob.activeSelf)
            {
                OnClickGoodJob(null, null);
                return;
            }
            /* if (WorldManager.singleton.IsTeamerDead())
             {
                 Screen.lockCursor = false;
             }*/
            OnClickReviveBtn(null, null);
        });
        AddEventListener(GameEvent.INPUT_KEY_R_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (m_switch.activeSelf)
            {
                OnClickSwitch(null, null);
                return;
            }
            if (MainPlayer.singleton.isDmmModel) return;
            GameDispatcher.Dispatch(GameEvent.INPUT_RELOAD_AMMO);
        });
        AddEventListener(GameEvent.INPUT_KEY_G_DOWN, () =>
        {
            if (isPcChatShow) return;
            if (MainPlayer.singleton == null) return;
            if (MainPlayer.singleton.canDropWeapon)
            {
                OnClickDropGun(null, null);
                if (DropItemManager.singleton.IsDropItemMainWeapon) DropItemManager.singleton.PickUpDropItem();
            }
            else if (DropItemManager.singleton.IsPickable /*MainPlayer.singleton.curWeapon != null && MainPlayer.singleton.curWeapon.weaponConfigLine.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1 && MainPlayer.singleton.curWeapon.weaponConfigLine.RareType == 5*/)
            {
                DropItemManager.singleton.PickUpDropItem();
            }
        });
        AddEventListener<bool>(GameEvent.INPUT_MOUSERIGHT_ACTION, (isDown) =>
        {
            if (MainPlayer.singleton == null) return;
            if (MainPlayer.singleton.isDmmModel) return;//躲猫猫模式屏蔽右键
            if (MainPlayer.singleton.curWeapon == null) return;
            if (MainPlayer.singleton.curWeapon.weaponConfigLine.SubWeapon > 0)
            {
                //第二武器
                GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, isDown);
            }
            else if (MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_KNIFE)
            {//重刀处理
                GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, isDown);
            }
            else
            {
                if (isDown)
                {
                    if ((MainPlayer.singleton.curWeapon is WeaponGun) && (((WeaponGun)MainPlayer.singleton.curWeapon).hasScopeButton || MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SNIPER_GUN))
                    {
                        if (((WeaponGun)MainPlayer.singleton.curWeapon).isIdle)
                        {
                            GameDispatcher.Dispatch(GameEvent.INPUT_ENABLE_GUN_SCOPE, true);
                        }
                        else if (((WeaponGun)MainPlayer.singleton.curWeapon).isZoom)
                        {
                            GameDispatcher.Dispatch(GameEvent.INPUT_ENABLE_GUN_SCOPE, false);
                        }
                    }
                }
            }
        });

        AddEventListener(GameEvent.INPUT_KEY_AUDIO_DOWN, () =>
        {
            if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
            {
                UIManager.ShowNoThisFunc();
                return;
            }
            _current_audio = -1;
            PanelSpeechSpeaker.showSpeaker();
        });

        AddEventListener(GameEvent.INPUT_KEY_AUDIO_UP, () =>
        {
            if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
            {
                return;
            }
            PanelSpeechSpeaker.showSpeaker(1);
            _current_audio = 0;
            PlayNextAudio();
        });

        AddEventListener(GameEvent.INPUT_KEY_F8_DOWN, () =>
        {
            if (isPcChatShow) 
                return;
            if (m_goHadBombTip != null && m_goHadBombTip.activeSelf)
            {
                OnClickDropC4(null, null);
            }
        });

#endif
        GameDispatcher.Dispatch(GameEvent.UI_CREATE_PANEL_BATTLE);
        AddEventListener(GameEvent.PLAYER_FLY_UP, () =>
        {
            jetfadein = true;
            jetfadeout = false;
        });

        AddEventListener(GameEvent.PLAYER_FLY_END, () =>
        {
            jetfadein = false;
            jetfadeout = true;
        });

        AddEventListener(GameEvent.PLAYER_FLY_POWER_OFF, () =>
        {
            jetfadein = false;
            jetfadeout = true;
        });

    }

    /// <summary>
    /// 打开发送弹幕输入面板
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnBtnFightVideoOpenSendMsgClick(GameObject target, PointerEventData eventData)
    {
        if(UIManager.IsOpen<FightVideoSendMsg>() == false)
        {
            UIManager.PopPanel<FightVideoSendMsg>(null,true);
        }
    }

    private int playSpeed = 1;
    /// <summary>
    ///设置录像播放速度
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnBtnFightVideoChangeSpeedClick(GameObject target, PointerEventData eventData)
    {
        playSpeed = playSpeed * 2;
        if (playSpeed > 2)
        {
            playSpeed = 1;
        }
        m_playSpeedText.text = "X" + playSpeed;
        FightVideoManager.PlaySpeed = playSpeed;
        NetLayer.Send(new tos_record_set_speed { speed = FightVideoManager.PlaySpeed, by_center = FightVideoManager.ByCenter() });
    }

    /// <summary>
    /// 显示、隐藏弹幕面板
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnBtnFightVideoShowMsgClick(GameObject target, PointerEventData eventData)
    {
        if( UIManager.IsOpen<FightVideoDanMu>()== true)
        {
            UIManager.HidePanel<FightVideoDanMu>();
            m_openDanMubtnText.text = "打开弹幕";
        }
        else
        {
            UIManager.PopPanel<FightVideoDanMu>();
            m_openDanMubtnText.text = "屏蔽弹幕";
        }
    }

    private bool m_FightDirectionView_loading = false;
    /// <summary>
    /// 打开作战指示面板
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    private void OnBtnFightDirectionClick(GameObject target, PointerEventData eventData)
    {
        if (m_FightDirectionPanel == null && m_FightDirectionView_loading == false)
        {
            m_FightDirectionView_loading = true;
            ResourceManager.LoadUI("UI/FightDirection/ViewFightDirection", go =>
            {
                m_FightDirectionView_loading = false;
                if (m_go == null) { return; }
                m_FightDirectionPanel = go;
                m_FightDirectionPanel.name = "FightDirectionPanel";
                m_FightDirectionPanel.transform.SetParent(m_tran);
                m_FightDirectionPanel.transform.localScale = Vector3.one;
                ((RectTransform)m_FightDirectionPanel.transform).anchoredPosition = new Vector2(112, 0);
                m_FightDirectionPanel.AddMissingComponent<FightDirectionPanel>();
            });
        }
        else if (m_FightDirectionPanel != null && m_FightDirectionPanel.activeSelf == false)
        {
            m_FightDirectionPanel.TrySetActive(true);
            m_FightDirectionPanel.GetComponent<FightDirectionPanel>().Init();
        }
        else if (m_FightDirectionPanel != null && m_FightDirectionPanel.activeSelf == true)
        {
            m_FightDirectionPanel.TrySetActive(false);
        }
    }

    private void updateReloadBulletCDMask(float clikTime, bool reloadButtle)
    {
        m_reloadButtleTime = clikTime;
        m_isReloadBullet = reloadButtle;
        if (reloadButtle == false && m_ReloadBulletMask != null)
        {
            m_ReloadBulletMask.gameObject.TrySetActive(false);
        }
    }

    private void OnFocusChange(bool focus)
    {
        if (Driver.m_platform != EnumPlatform.web)
            return;

        if (focus == false)
        {
            if (!UIManager.fullScreen)
            {
                m_tranPauseMask.SetAsLastSibling();
                m_tranPauseMask.gameObject.TrySetActive(true);
            }
        }
        else
        {
            m_tranPauseMask.gameObject.TrySetActive(false);
        }
    }

    private void RoundEnd()
    {
        PanelBattleSurvivalScore.TryDeleteBossHPBar(null, true);
        PanelBattleDotaScore.TryDeleteBossHPBar(null, true);
    }

    private void OnClickAutoFire(GameObject target, PointerEventData eventData)
    {

        //if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE) < 0.1f)
        //{
        //    GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE, 1.0f);
        //}
        //else
        //{
        //    GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE, 0);
        //}

        setHideRuleAutoFire(target.name);
        AutoFire();
    }

    private void setHideRuleAutoFire(string targetName)
    {
        if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_HIDE)
        {
            if (targetName == m_autoFireOff.name)
            {
                HideBehavior.AutoFire = true;
            }
            else
            {
                HideBehavior.AutoFire = false;
            }
        }
    }

    public void AutoFire()
    {
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_SWITCH_AUTOFIRE);
        RefreshAutoFire();
    }


    float lastPressTime = 0;
    bool skillCD = false; //嘲讽技能是否可用
    bool fristCD = true; //第一次重设CD
    bool isDieUseTauntFace = false;
    private void OnEmoItemClick(GameObject target, PointerEventData eventData)
    {
        int index = int.Parse(target.name);
        /* if (skillCD)
         {
             skillCD = false;
             lastPressTime = Time.realtimeSinceStartup;
             m_Emotions.TrySetActive(false);
             NetLayer.Send(new tos_fight_use_skill() { skill_id = HideBehavior.tauntSkill.ID, skill_param = index.ToString() });
         }*/
        sendTauntFace(index);
        if( MainPlayer.singleton != null && MainPlayer.singleton.isAlive == false )
        {
            OnShowPanelTaunt(false);
        }
    }
    private float m_hideTauntFaceCDRate = 1.0f;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="index"></param> 从0开始
    private void sendTauntFace(int index)
    {
        if (skillCD)
        {
            skillCD = false;
            lastPressTime = Time.realtimeSinceStartup;
            m_Emotions.TrySetActive(false);
            ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("hide_niubi_card_number");
            int cardId = cfg.ValueInt;
            p_item item = ItemDataManager.GetDepotItem(cardId);
            if(item != null)
            {
                cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("hide_skill_shorten");
                m_hideTauntFaceCDRate = cfg == null ? 1.0f : float.Parse(cfg.ValueStr);
            }
            else
            {
                m_hideTauntFaceCDRate = 1.0f;
            }
            NetLayer.Send(new tos_fight_use_skill() { skill_id = HideBehavior.tauntSkill.ID,tauntedPlayerId = ChangeTeamView.watchedPlayerPid, skill_param = index.ToString(), pos = new float[0], target_ids = new int[0] });
        }
    }

    private void OnBtnTauntClick(GameObject target, PointerEventData eventData)
    {
        if (m_Emotions != null && skillCD)
        {
            m_Emotions.TrySetActive(true);
        }
    }


    public void OnShowPanelTaunt(bool isShow)
    {
        if (m_PanelTaunt == null)
            return;
        m_Emotions.TrySetActive(false);
        if (isShow && HideBehavior.singleton.PlayerStartFind())
        {
            //ResetTauntSkill();
            m_PanelTaunt.TrySetActive(true);
            m_Emotions.TrySetActive(false);
        }
        else
        {
            m_PanelTaunt.TrySetActive(false);
            fristCD = true;
        }
    }

    public void showAlertWarning(bool isShowAlert, int playerId = 0)
    {
        if (isShowAlert)
        {
            if (null == m_alertWarning || m_alertWarning.isDestroy)
                m_alertWarning = UIEffect.ShowEffect(m_tranRoomTip, EffectConst.UI_FIGHT_YUJING_ALERT, 4, new Vector2(-8, 80), false);
            if (false == m_alerttPlayerID.Contains(playerId))
                m_alerttPlayerID.Add(playerId);
            AudioManager.PlayUISoundLoop(AudioConst.yuJingInFight); //播放预警音效
        }
        else
        {
            if (m_alerttPlayerID.Contains(playerId))
            {
                if (null != m_alertWarning && !m_alertWarning.isDestroy)
                    m_alertWarning.Destroy();
                m_alertWarning = null;
                m_alerttPlayerID.Remove(playerId);
                AudioManager.StopUISoundLoop(); //关闭预警音效
            }
        }
    }

    private void OnMainPlayerLoaded()
    {
        if (MainPlayer.singleton == WorldManager.singleton.curPlayer)
        {
            doShow();
        }
    }
    private void OnPlayerLoaded(BasePlayer player)
    {
        TimerManager.SetTimeOut(1f, () =>
            {
                if (WorldManager.singleton.isMyViewerLoaded)
                {
                    doShow();
                }
            });
    }

    public void ResetTauntSkill()
    {
        if (!fristCD)
            return;
        fristCD = false;
        skillCD = false;
        m_tauntSkillMask.fillAmount = 1.0f;
        lastPressTime = Time.realtimeSinceStartup;
    }

    private void OnUpdateRoleEquip()
    {
        isHeavyKnifeModeOn = false;
        changeHeavyKnifeTypeIcon();
        OnUpdateRoleSkill();
        OnUpdateEquipSkill();
    }

    private void OnUpdateRoleSkill()
    {
        var role_code = PlayerBattleModel.Instance.fight_role;
        if (role_code == 0 || MainPlayer.singleton == null || MainPlayer.singleton.serverData == null
            || MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.ZOMBIE)
        {
            return;
        }
        var role_info = ConfigManager.GetConfig<ConfigItemRole>().GetLine(role_code);
        var skill_ary = role_info != null && role_info.Skills != null && role_info.Skills.Length > 0 ? role_info.Skills : null;
        if (skill_ary != null && skill_ary.Length > 0)
        {
            SkillItemRender skill_item = null;
            if (m_SkillList.transform.childCount > 0)
            {
                skill_item = m_SkillList.transform.GetChild(0).GetComponent<SkillItemRender>();
            }
            else
            {
                skill_item = CreateSkillItem(m_SkillList.transform, Vector3.zero);
            }
            int skillId = int.Parse(skill_ary[0]);
            skill_item.SetData(ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId));
            if (_round_become_zombie)
            {
                _round_become_zombie = false;
                skill_item.frozen = true;
            }
            if (m_dicSkillItemRender.ContainsKey(skillId))
                m_dicSkillItemRender[skillId] = skill_item;
            else
                m_dicSkillItemRender.Add(skillId, skill_item);
        }
    }

    private SkillItemRender CreateSkillItem(Transform tran, Vector2 position)
    {
        var skill_item = new GameObject("skill_item").AddComponent<SkillItemRender>();
        var rectTran = skill_item.gameObject.GetRectTransform();
        rectTran.SetUILocation(tran, position.x, position.y);
        rectTran.anchorMin = new Vector2(1,0.5f);
        rectTran.anchorMax = new Vector2(1,0.5f);
        skill_item.transform.localScale = Vector3.one;
        UGUIClickHandler.Get(skill_item.gameObject).onPointerClick += OnSkillItemClick;
        return skill_item;
    }


    private WeaponSkillItemRender CreateAwakenSkillItem(Transform tran, Vector2 position)
    {
        var skill_item = new GameObject("weapon_skill_item").AddComponent<WeaponSkillItemRender>();
        var rectTran = skill_item.gameObject.GetRectTransform();
        rectTran.SetUILocation(tran, position.x, position.y);
        rectTran.anchorMin = new Vector2(1,0.5f);
        rectTran.anchorMax = new Vector2(1,0.5f);
        skill_item.transform.localScale = Vector3.one;
        UGUIClickHandler.Get(skill_item.gameObject).onPointerClick += onWeaponSkillItemClick;
        return skill_item;
    }

    private void OnUpdateEquipSkill()
    {
       
        m_skillIdList.Clear();
        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData == null)
        {
            return;
        }
        var mainWeapon = MainPlayer.singleton.curWeapon;
        var subWeapon = MainPlayer.singleton.subWeapon;
        if (mainWeapon != null)
        {
            ConfigItemWeaponLine weaponCfg = (ConfigItemWeaponLine)ItemDataManager.GetItem(int.Parse(mainWeapon.weaponId));
            if (weaponCfg != null)
            {
                m_skillIdList.Add(isShowSkill(weaponCfg.SkillA) ? weaponCfg.SkillA : 0);
                m_skillIdList.Add(isShowSkill(weaponCfg.SkillB) ? weaponCfg.SkillB : 0);
            }
        }
        if (subWeapon != null)
        {
            var weaponCfg = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(int.Parse(subWeapon.weaponId));
            if (weaponCfg != null)
            {
                if (m_skillIdList.Count > 1 && m_skillIdList[1] == 0)
                {
                    m_skillIdList[1] = (isShowSkill(weaponCfg.SkillB) ? weaponCfg.SkillB : 0);
                }
            }
        }
        if (MainPlayer.singleton != null)
        {
            MainPlayer.singleton.canFly = false;
        }
        for (int i = 0; i < m_skillIdList.Count; i++)
        {
            var skillId = m_skillIdList[i];
            SkillItemRender skill_item = null;
            var config = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);
            if (config!=null&&config.Effect == GameConst.SKILL_EFFECT_JETPACK)
            {
                if (MainPlayer.singleton != null)
                {
                    MainPlayer.singleton.canFly = true;
                    MainPlayer.singleton.skillMgr.flm.init(config);
                }
                continue;
            }
            if (m_skillItemPoolList.Count > i)
            {
                skill_item = m_skillItemPoolList[i];
            }
            else
            {
                skill_item = CreateSkillItem(m_weaponSkillTran, m_skillBtnPosArr[i]);
                m_skillItemPoolList.Add(skill_item);
                m_skillGo2SkillRender[skill_item.gameObject] = skill_item;
                skill_item.frozen = false;
            }
            
            if (skillId != 0 && skill_item.skillId != skillId)
            {
                 skill_item.SetData(config);
                 m_dicSkillItemRender[skillId] = skill_item;
                
            }
            var isShowItem =  skillId != 0 && !(m_posSkillItemRenders.ContainsKey(i) && m_posSkillItemRenders[i] is WeaponSkillItemRender
                &&  (m_posSkillItemRenders[i] as WeaponSkillItemRender).isShow);
            skill_item.gameObject.TrySetActive(isShowItem);
            skill_item.isShow = isShowItem;
            if (isShowItem)
            {
                m_posSkillItemRenders[i] = skill_item;
                m_skillGos[i] = skill_item.gameObject;
            }
        }
        CheckJetPack();


    }

    private void OnSpeechHandler(int status, string msg)
    {
        PanelSpeechSpeaker.closeSpeaker();
        if (status == 1 && !string.IsNullOrEmpty(msg))
        {
            var param_data = msg.Split('|');
            OnChatEditEnd(param_data[0], NetChatData.loadSpeechData(), int.Parse(param_data[1]));
        }
        else
        {
            if (!UIManager.IsOpen<PanelBattle>())
            {
                UIManager.ShowTipPanel(status == 1 ? string.Format("Speech Error: \nstatus:{0},\nmessage: {1}", status, msg) : msg);
            }
            else
            {
                TipsManager.Instance.showTips(status == 1 ? string.Format("Speech Error: \nstatus:{0},\nmessage: {1}", status, msg) : msg);
            }
        }
    }

    void UpdatePraiseWhenViewBack()
    {
        m_gjobnum.text = WorldManager.singleton.GetPraise(MainPlayer.singleton.PlayerId).ToString();
    }

    void OnClickSwitch(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton.IsHideView)
        {
            MainPlayer.singleton.IsHideView = false;
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 3;
                obj[1] = 1;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(3, 2);
            }
            m_iSelectedPlayerID = MainPlayer.singleton.PlayerId;
            UpdatePraiseWhenViewBack();
            //UpdatePraise();
            GameDispatcher.Dispatch(GameEvent.PlAYER_HIDE_SWITCH_BACK);
            if( Driver.isMobilePlatform )
            {
                m_switch.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "switch"));
            }
            else
            {
                if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_R_DOWN))
                    m_switch.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_switch"));
            }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            m_pcUnLock.TrySetActive(true && MainPlayer.singleton.IsFrezee && !isShowUIExcPC);
            updatePcUnlockImg();
#endif
        }
        else
        {
            if (MainPlayer.singleton.IsFrezee)
            {
                MainPlayer.singleton.IsHideView = true;
                if (!UIManager.IsOpen<PanelHideMsg>())
                {
                    object[] obj = new object[2];
                    obj[0] = 4;
                    obj[1] = 1;
                    UIManager.PopPanel<PanelHideMsg>(obj);
                }
                else
                {
                    UIManager.GetPanel<PanelHideMsg>().updateview(4, 2);
                }
                GameDispatcher.Dispatch(GameEvent.PLAYER_HIDE_SWITCH);
                if (Driver.isMobilePlatform)
                {
                    m_switch.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hideBack"));
                }
                else
                {
                    if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_R_DOWN))
                        m_switch.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "pc_hide_back"));
                }
            }
            m_pcUnLock.TrySetActive(false);
        }
    }

    /// <summary>
    /// 更新PC版躲猫猫模式的解锁
    /// </summary>
    string last_pcImg = "";
    void updatePcUnlockImg()
    {
        if (m_pcUnLock.activeSelf)
        {
            string str = "PC_unlock_noE";
            if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            {
                str = "PC_unlock";
            }
            if (last_pcImg != str)
            {
                last_pcImg = str;
                m_pcUnLock.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, str));
            }
        }
    }

    void OnClickGoodJob(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_fight_praise() { pid = WorldManager.singleton.GetPlayerById(m_iSelectedPlayerID).serverData.pid });
    }

    private void OnAudioBtnActionDown(GameObject target, PointerEventData eventData)
    {
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            UIManager.ShowNoThisFunc();
            return;
        }
        _current_audio = -1;
        PanelSpeechSpeaker.showSpeaker();
    }

    private void OnAudioBtnActionUp(GameObject target, PointerEventData eventData)
    {
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER))
        {
            return;
        }
        PanelSpeechSpeaker.showSpeaker(1);
        _current_audio = 0;
        PlayNextAudio();
    }

    private void OnChatEditEnd(string content)
    {
        OnChatEditEnd(content, null, 0);
    }

    private void OnChatEditEnd(string s, byte[] audio_data, int duration)
    {
        if (string.IsNullOrEmpty(s.Trim(' ', '\n')))
            return;
        if (s == "fps")
        {
            m_goFps.TrySetActive(!m_goFps.activeSelf);
            return;
        }
        else if (s == "fpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.AddMissingComponent<UIGmPanel>();
            p.enabled = true;
            return;
        }
        else if (s == "hidefpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.GetComponent<UIGmPanel>();
            if(p != null)
                p.enabled = false;
            return;
        }

        NetLayer.Send(new tos_fight_chat() { type = m_chatType, msg = WordFiterManager.Fiter(s), audio_chat = new p_audio_chat() { content = audio_data != null ? audio_data : new byte[0], duration = duration } });
        m_chatInput.text = "";
        m_chatInput.textComponent.text = "";
    }

    private void OnBtnChatInputClick(GameObject target, PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(m_chatInput.gameObject);
    }

    private void onClickBtnDMMPcUnLock(GameObject target, PointerEventData eventData)
    {
        //在次开火解除锁定
        // GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, true);
    }

    private void OnBtnChatTypeClick(GameObject target, PointerEventData eventData)
    {
        if (WorldManager.singleton.isViewBattleModel)
        {
            target.transform.Find("Text").GetComponent<Text>().text = "队伍";
            return;
        }
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            if (MainPlayer.singleton != null)
            {
                if (MainPlayer.singleton.Camp == 4 && !MainPlayer.singleton.isDead)
                {
                    m_chatType = 1;
                    target.transform.Find("Text").GetComponent<Text>().text = "队伍";
                    return;
                }
            }
        }
        m_chatType = (m_chatType + 1) % 2;
        if (m_chatType == 0)
            target.transform.Find("Text").GetComponent<Text>().text = "全体";
        else if (m_chatType == 1)
            target.transform.Find("Text").GetComponent<Text>().text = "队伍";
    }

    private int _current_audio;//0无正在处理音频，-1处于暂停状态，>0正在处理的音频id
    private Queue<int> _audio_play_list = new Queue<int>();

    private int _current_fight_audio;
    private Queue<int> _fight_audio_play_list = new Queue<int>();
    private static bool isplaying = false;

    private static void Toc_fight_chat(toc_fight_chat data)
    {
        var panel = UIManager.GetPanel<PanelBattle>();
        if (panel == null || !panel.IsOpen() || !panel.m_dataGridChat)
            return;

        m_chatDataList.Add(new ItemBattleChatInfo() { m_proto = data, m_startTime = Time.realtimeSinceStartup });
        if (m_chatDataList.Count > 3)
        {
            m_chatDataList.RemoveAt(0);
        }
        if (data.command_id > 0)
        {
            panel._fight_audio_play_list.Enqueue(data.command_id);
        }
        if (panel._fight_audio_play_list.Count > 0)
        {
            if (isplaying == false)
            {
                panel.playFightDireAudio();
                isplaying = true;
            }
            else
            {
                TimerManager.SetTimeOut(1.5f, delegate { panel.playFightDireAudio(); });
            }
        }

        panel.m_dataGridChat.Data = m_chatDataList.ToArray();
        if (!SdkManager.IsSupportFunction(SdkFuncConst.SHOW_SPEAKER) || panel.m_auto_play_audio == false)
        {
            return;
        }
        if (data.audio_chat != null && data.audio_chat.audio_id > 0)//语音
        {
            //if (WorldManager.singleton != null && (data.@from != WorldManager.singleton.myid && WorldManager.singleton.GetViewerById(data.@from)!=PlayerSystem.roleData.name))//自己的不播放
            if (WorldManager.singleton != null && (data.@from != WorldManager.singleton.myid && PlayerBattleModel.Instance.GetSpecatorName(data.from) != PlayerSystem.roleData.name))//自己的不播放
            {
                panel._audio_play_list.Enqueue(data.audio_chat.audio_id);
                if (panel._current_audio == 0)
                {
                    panel.PlayNextAudio();
                }
            }
        }
    }

    private void PlayNextAudio()
    {
        if (m_go == null || _audio_play_list.Count == 0 || _current_audio == -1)
        {
            _current_audio = _current_audio != -1 ? 0 : -1;
            return;
        }
        _current_audio = _audio_play_list.Dequeue();
        NetChatData.playSpeech(_current_audio, PlayNextAudio);
    }

    private void playFightDireAudio()
    {
        if (m_go == null || _fight_audio_play_list.Count == 0 || _current_fight_audio == -1)
        {
            isplaying = false;
            _current_fight_audio = _current_fight_audio != -1 ? 0 : -1;
            return;
        }
        _current_fight_audio = _fight_audio_play_list.Dequeue();

        ConfigFightDirection config = ConfigManager.GetConfig<ConfigFightDirection>();
        if (config != null && config.m_dataArr.Length > _current_fight_audio)
        {
            string sound = config.m_dataArr[_current_fight_audio].Sound;
            if (!String.IsNullOrEmpty(sound))
            {
                AudioManager.PlayFightUIVoice(sound);
            }
        }

        if (_fight_audio_play_list.Count > 0)
        {
            playFightDireAudio();
        }
    }


    private void OnPlayerSwitchGun(string weapon_id)
    {
        var b = (m_mode & BattleViewMode.MODE_WEAPON) > 0;

        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData == null || !(MainPlayer.singleton.curWeapon is WeaponGun) || !((WeaponGun)MainPlayer.singleton.curWeapon).hasScopeButton)
        {
            b = false;
        }
        else if (b)
        {
            b = b && true;
            m_Magnifier.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_Magnifier.isOn ? "UnMagnifier" : "Magnifier"));
        }

        if (b != m_Magnifier.gameObject.activeSelf)
        {
            ShowMagnifier(b);
        }

        if (m_UnMagnifier.activeSelf)
        {
            if (m_sightCrossHair.activeSelf == false)
            {
                m_UnMagnifier.TrySetActive(false);
            }
        }

        m_goGrenade.TrySetActive(IsGrenadeVisible() && isShowUIExcPC);
        if (m_bGrenadeSwitch)
        {
            m_bGrenadeSwitch = false;
            m_goGrenade.TrySetActive(false);
        }

        m_goFlashBomb.TrySetActive(IsFlashBombVisible() && isShowUIExcPC);
        if (m_bFlashBombSwitch)
        {
            m_bFlashBombSwitch = false;
            m_goFlashBomb.TrySetActive(false);
        }

        if (isNewShootUI)
            if (!string.IsNullOrEmpty(weapon_id))
            {
                var info = ItemDataManager.GetItem(int.Parse(weapon_id));
                if (info == null) return;
                string sprite = "on_bullet_icon";
                string subType = ItemDataManager.GetItem(info.ID).SubType;
                if (subType == GameConst.WEAPON_SUBTYPE_GRENADE)
                {
                    sprite = "on_grenade_icon";
                }
                else if (subType == GameConst.WEAPON_SUBTYPE_DAGGER)
                {
                    sprite = "on_knife_icon";
                }
                Image img = m_goShoot.GetComponent<Image>();
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, sprite));
                img.SetNativeSize();
            }
    }

    private void ShowMagnifier(bool isShow)
    {
        if (m_Magnifier != null)
        {
            m_Magnifier.gameObject.TrySetActive(isShow && isShowUIExcPC);
        }
    }

    private void OnSwitchCameraToPartner(int playerID)
    {
        m_iSelectedPlayerID = playerID;
        if (WorldManager.singleton.isHideModeOpen)
        {
            var player = WorldManager.singleton.GetPlayerById(playerID);

            if (player != null && player.Camp == 4)
            {
                m_goodjob.gameObject.TrySetActive(MainPlayer.singleton != null && HideBehavior.singleton.PlayerStartFind() == true);
                m_goodjobnum.gameObject.TrySetActive(true);
                if (player.isDmmModel == true)
                {
                    OnShowPanelTaunt(false);
                }
                else
                {
                    OnShowPanelTaunt(true);
                }
            }
            else
            {
                m_goodjob.gameObject.TrySetActive(false);
                m_goodjobnum.gameObject.TrySetActive(false);
                m_PanelTaunt.TrySetActive(MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT 
                    && HideBehavior.singleton.PlayerStartFind() == true);
            }
            UpdatePraise();
        }
        if( WorldManager.singleton.isViewBattleModel == true )
        {
            NetLayer.Send(new tos_fight_change_spectator() { actor_id = playerID });
        }
    }

    private void changeBag(object renderData)
    {
        /*#if UNITY_WEBPLAYER || UNITY_STANDALONE
                Screen.lockCursor = true;
        #endif*/
        m_bagview.TrySetActive(false);
        var a = renderData as p_bag;
        NetLayer.Send(new tos_fight_select_equip() { bagid = a.group_id });
    }
    /// <summary>
    /// 快捷选择背包
    /// </summary>
    /// <param name="bagNum"></param>
    private void shortCutKeySelectBag(int bagNum)
    {
        if (bagNum < 1) return;
        if (bagNum <= WorldManager.singleton.m_bagdata.Length)
        {
            changeBag(WorldManager.singleton.m_bagdata[bagNum - 1]);
        }
    }


    private void ShowBag(GameObject target, PointerEventData eventData)
    {
        m_bagview.TrySetActive(!m_bagview.activeSelf);
        if (m_bagview.activeSelf)
        {
            var pos = m_bag.gameObject.GetRectTransform().anchoredPosition + new Vector2(233, 220);
            if (WorldManager.singleton.m_bagdata.Length > 6)
                pos.y += 80;
            m_bagview.GetRectTransform().anchoredPosition = pos;
            UpdateBag();
        }
        /*#if UNITY_WEBPLAYER || UNITY_STANDALONE
                Screen.lockCursor = !m_bagview.activeSelf;
        #endif*/
    }

    public void autoSetupViewMode(bool force = false)
    {
        if (WorldManager.singleton.isViewBattleModel
            || (MainPlayer.singleton != null && (MainPlayer.singleton.isDead || MainPlayer.singleton.isDying))
            )
        {
            var mode = BattleViewMode.MODE_DEAD;
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
            {
                mode |= BattleViewMode.MODE_GUN_ROUTE;
            }
            else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_CAMP))
            {
                mode |= BattleViewMode.MODE_GUN_ROUTE_TEAM;
            }
            else if (WorldManager.singleton.isSurvivalTypeModeOpen)
            {

            }
            else
            {
                mode |= BattleViewMode.MODE_INFO;
            }
            updateViewMode(mode, force);
        }
        else if (MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
        {
            updateViewMode(BattleViewMode.MODE_HERO, force);
        }
        else if (WorldManager.singleton.isZombieTypeModeOpen)
        {
            updateViewMode(MainPlayer.singleton.serverData.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE ? BattleViewMode.MODE_ZOMBIE_Z : BattleViewMode.MODE_ZOMBIE_H, force);
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
        {
            updateViewMode(BattleViewMode.MODE_KING_OF_GUN, force);
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_CAMP))
        {
            updateViewMode(BattleViewMode.MODE_TEAM_KING_OF_GUN, force);
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            updateViewMode(MainPlayer.singleton.serverData.camp == (int)WorldManager.CAMP_DEFINE.CAT ? BattleViewMode.MODE_HIDE_O : BattleViewMode.MODE_HIDE_H, force);
            if (MainPlayer.singleton.serverData.camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                m_switch.gameObject.TrySetActive(true);
                m_goodjobnum.gameObject.TrySetActive(true);
                m_goodjob.gameObject.TrySetActive(true);
                UpdateUserSetUIPos(ReadControlPosFromPlayerPrefs());
                m_chatType = 1;
                m_btnChatType.transform.Find("Text").GetComponent<Text>().text = "队伍";
                m_Squat.gameObject.TrySetActive(false);
            }
            else
            {
                m_Squat.gameObject.TrySetActive(true);
                m_switch.gameObject.TrySetActive(false);
                m_goodjobnum.gameObject.TrySetActive(false);
                m_goodjob.gameObject.TrySetActive(false);
                m_chatType = 0;
                m_btnChatType.transform.Find("Text").GetComponent<Text>().text = "全体";
            }

        }
        else if (WorldManager.singleton.isSurvivalTypeModeOpen)
        {
            updateViewMode(BattleViewMode.MODE_SURVIVAL, force);
        }
        else if (WorldManager.singleton.isDotaModeOpen)
        {
            updateViewMode(BattleViewMode.MODE_DOTA, force);
        }
        else
        {
            updateViewMode(BattleViewMode.MODE_NORMAL, force);
        }
    }

    public void updateViewMode(uint mode, bool force = false)
    {
        if (m_mode == mode && force == false)
        {
            return;
        }
        m_mode = mode;
        setupInfoPanel(m_mode);
        setupGunRoutePanel(m_mode);
        setupTeamGunRoutePanel(m_mode);
        //setupTeamHeroInfoPanel(m_mode);
        //狙击组件
        var enabled = (m_mode & BattleViewMode.MODE_SNIPER) > 0;
        if (m_sightCrossHair.activeSelf != enabled)
        {
            m_sightCrossHair.TrySetActive(enabled);
        }
        //玩家个人信息
        enabled = (m_mode & BattleViewMode.MODE_SUBINFO) > 0;
        if (m_BattleSubInfo.gameObject.activeSelf != enabled)
        {
            m_BattleSubInfo.gameObject.TrySetActive(enabled);
        }
        //玩法附加个人信息
        //enabled = (m_mode & BattleViewMode.MODE_ADDINFO) > 0;
        //if (m_BattleAddInfo.gameObject.activeSelf != enabled)
        //{
        //    m_BattleAddInfo.gameObject.TrySetActive(enabled);
        //}
        //功能组件
        enabled = (m_mode & BattleViewMode.MODE_FUNCTION) > 0 && !RoomModel.IsGuideStage;
        if (m_setting_btn.activeSelf != enabled)
        {
            m_setting_btn.TrySetActive(enabled);
        }
        //if (m_record_btn.activeSelf != enabled)
        //{
        //    m_record_btn.TrySetActive(enabled);
        //}
        //技能
        enabled = (m_mode & BattleViewMode.MODE_SKILL) > 0;
        if (m_SkillList.activeSelf != enabled)
        {
            m_SkillList.TrySetActive(enabled);
        }
        //武器相关
        enabled = (m_mode & BattleViewMode.MODE_WEAPON) > 0;
        if (m_ArmorBullet.gameObject.activeSelf != enabled)
        {
            m_ArmorBullet.enable = enabled;
        }
        if (m_LoadBullet.activeSelf != enabled)
        {
            m_LoadBullet.TrySetActive(enabled && isShowUIExcPC);
        }
        if (m_goAutoSwitchGun.activeSelf != enabled)
        {
            m_goAutoSwitchGun.TrySetActive(enabled && isShowUIExcPC);
        }
        if (m_Magnifier.gameObject.activeSelf != enabled)
        {
            ShowMagnifier(enabled);
        }
        //切换武器
        enabled = (m_mode & BattleViewMode.MODE_SWITCH_WEAPON) > 0;
        if (m_goGunList.gameObject.activeSelf != enabled)
        {
            m_goGunList.gameObject.TrySetActive(enabled);
        }
        //动作相关
        enabled = (m_mode & BattleViewMode.MODE_ACTION) > 0;
        if (m_Jump.activeSelf != enabled)
        {
            m_Jump.TrySetActive(enabled && isShowUIExcPC);
        }
        if (m_goLeftJump.activeSelf != enabled)
        {
            m_goLeftJump.TrySetActive(enabled && isShowUIExcPC);
        }
        if (m_Squat.activeSelf != enabled)
        {
            m_Squat.TrySetActive(enabled && isShowUIExcPC);
        }
        //控制相关
        enabled = (m_mode & BattleViewMode.MODE_CTRL) > 0;
        if (m_goJoyback.activeSelf != enabled)
        {
            m_goJoyback.TrySetActive(enabled && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
        }
        if (m_goAim.activeSelf != enabled)
        {
            m_goAim.TrySetActive(enabled && isShowUIExcPC);
        }
        //按照模式设置完毕后，再根据特殊的业务逻辑再刷新一次组件状态
        refreshViewModeByLogic();
        OnShootPointerUp(null, null);
    }

    public static bool IsShootDown
    {
        get
        {
#if UNITY_EDITOR
            //if (Input.GetMouseButton(0))
            //    return true;
#endif
            return m_bIsShootDown;
        }
        set
        {
            m_bIsShootDown = value;
        }
    }

    private void setupInfoPanel(uint mode)
    {
        if ((mode & BattleViewMode.MODE_INFO) > 0)
        {
            if (m_BattleInfoPanel == null && m_battle_info_loading == false)
            {
                m_battle_info_loading = true;
                ResourceManager.LoadUI("UI/Battle/ViewBattlingMode", go =>
                {
                    m_battle_info_loading = false;
                    if (m_go == null || (m_mode & BattleViewMode.MODE_INFO) == 0) { return; }
                    m_BattleInfoPanel = go;
                    m_BattleInfoPanel.name = "ViewBattlingMode";
                    m_BattleInfoPanel.transform.SetParent(m_tran);
                    m_BattleInfoPanel.transform.localScale = Vector3.one;
                    UGUIClickHandler.Get(go).onPointerClick += delegate
                    {
                        if (RoomModel.IsGuideStage)
                            return;
                        if ((m_mode & BattleViewMode.MODE_FUNCTION) > 0)
                        {
                            UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Player }, true);
                        }
                    };
                    ((RectTransform)m_BattleInfoPanel.transform).anchoredPosition = new Vector2(0, 0);
                    m_BattleInfoPanel.AddMissingComponent<BattlingMode>().Init();
                    if (PlayerBattleModel.Instance.battle_info.is_set_once)
                        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state, true);
                });
            }
            else if (m_BattleInfoPanel != null && m_BattleInfoPanel.activeSelf == false)
            {
                m_BattleInfoPanel.TrySetActive(true);
            }
        }
        else if (m_BattleInfoPanel != null && m_BattleInfoPanel.activeSelf)
        {
            m_BattleInfoPanel.TrySetActive(false);
        }
    }

    private void setupTeamHeroInfoPanel(uint mode)
    {
        if ((mode & BattleViewMode.MODE_TEAM_HERO_INFO) > 0)
        {
            if (m_team_hero_info_panel == null && m_team_hero_info_loading == false)
            {
                m_team_hero_info_loading = true;
                ResourceManager.LoadUI("UI/Battle/BattleTeamHeroInfoPanel", go =>
                {
                    m_team_hero_info_loading = false;
                    if (m_go == null || (m_mode & BattleViewMode.MODE_TEAM_HERO_INFO) == 0) { return; }
                    m_team_hero_info_panel = go.AddComponent<BattleTeamHeroInfoPanel>();
                    m_team_hero_info_panel.name = "BattleTeamHeroInfoPanel";
                    m_team_hero_info_panel.transform.SetParent(m_tran);
                    m_team_hero_info_panel.transform.localScale = Vector3.one;
                    m_team_hero_info_panel.Init();
                    UGUIClickHandler.Get(go).onPointerClick += delegate
                    {
                        if ((m_mode & BattleViewMode.MODE_FUNCTION) > 0)
                        {
                            var br = UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Player}, true);
                        }
                    };
                    ((RectTransform)m_team_hero_info_panel.transform).anchoredPosition = new Vector2(-3, 0);
                    if (PlayerBattleModel.Instance.battle_info.is_set_once)
                        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state, true);
                });
            }
            else if (m_team_hero_info_panel != null && m_team_hero_info_panel.gameObject.activeSelf == false)
            {
                m_team_hero_info_panel.gameObject.TrySetActive(true);
            }
        }
        else if (m_team_hero_info_panel != null && m_team_hero_info_panel.gameObject.activeSelf)
        {
            m_team_hero_info_panel.gameObject.TrySetActive(false);
        }
    }

    public void ShowBattleSubInfo(bool isShow)
    {
        if (m_BattleSubInfo != null)
        {
            //Logger.Error("ShowBattleSubInfo: " + isShow);
            m_BattleSubInfo.gameObject.TrySetActive(isShow);
        }
    }

    private void setupTeamGunRoutePanel(uint mode)
    {
        if ((mode & BattleViewMode.MODE_GUN_ROUTE_TEAM) > 0)
        {
            if (m_team_gun_route_panel == null && m_team_gun_route_loading == false)
            {
                m_team_gun_route_loading = true;
                ResourceManager.LoadUI("UI/Battle/BattleTeamGunRoutePanel", go =>
                {
                    m_team_gun_route_loading = false;
                    if (m_go == null || (m_mode & BattleViewMode.MODE_GUN_ROUTE_TEAM) == 0) { return; }
                    m_team_gun_route_panel = go.AddComponent<BattleTeamGunRoutePanel>();
                    m_team_gun_route_panel.transform.SetParent(m_tran);
                    m_team_gun_route_panel.transform.localScale = Vector3.one;
                    ((RectTransform)m_team_gun_route_panel.transform).anchoredPosition = new Vector3(38, 0);
                    m_team_gun_route_panel.updateData();
                    if (PlayerBattleModel.Instance.battle_info.is_set_once)
                        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state, true);
                    UGUIClickHandler.Get(go).onPointerClick += delegate
                    {
                        if ((m_mode & BattleViewMode.MODE_FUNCTION) > 0)
                        {
                            UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Player}, true);
                        }
                    };
                });
            }
            else if (m_team_gun_route_panel != null && m_team_gun_route_panel.gameObject.activeSelf == false)
            {
                m_team_gun_route_panel.gameObject.TrySetActive(true);
            }
        }
        else if (m_team_gun_route_panel != null && m_team_gun_route_panel.gameObject.activeSelf)
        {
            m_team_gun_route_panel.gameObject.TrySetActive(false);
            if (!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO, GameConst.GAME_RULE_KING_CAMP))
            {
                GameObject.Destroy(m_team_gun_route_panel.gameObject);
                m_team_gun_route_panel = null;
            }
        }
    }

    private void setupGunRoutePanel(uint mode)
    {
        if ((mode & BattleViewMode.MODE_GUN_ROUTE) > 0)
        {
            if (m_gun_route_panel == null && m_gun_route_loading == false)
            {
                m_gun_route_loading = true;
                ResourceManager.LoadUI("UI/Battle/BattleGunRoutePanel", go =>
                {
                    m_gun_route_loading = false;
                    if (m_go == null || (m_mode & BattleViewMode.MODE_GUN_ROUTE) == 0) { return; }
                    m_gun_route_panel = go.AddComponent<BattleGunRoutePanel>();
                    m_gun_route_panel.transform.SetParent(m_tran);
                    m_gun_route_panel.transform.localScale = Vector3.one;
                    ((RectTransform)m_gun_route_panel.transform).anchoredPosition = new Vector3(38, 0);
                    m_gun_route_panel.updateData();
                    OnUpdateBattleState(PlayerBattleModel.Instance.battle_state, true);
                    UGUIClickHandler.Get(go).onPointerClick += delegate
                    {
                        if ((m_mode & BattleViewMode.MODE_FUNCTION) > 0)
                        {
                            UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Player}, true);
                        }
                    };
                });
            }
            else if (m_gun_route_panel != null && m_gun_route_panel.gameObject.activeSelf == false)
            {
                m_gun_route_panel.gameObject.TrySetActive(true);
            }
        }
        else if (m_gun_route_panel != null && m_gun_route_panel.gameObject.activeSelf)
        {
            m_gun_route_panel.gameObject.TrySetActive(false);
            if (!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO, GameConst.GAME_RULE_KING_CAMP))
            {
                GameObject.Destroy(m_gun_route_panel.gameObject);
                m_gun_route_panel = null;
            }
        }
    }

    /// <summary>
    /// 主要用于进一步判断是否需要隐藏
    /// </summary>
    private void refreshViewModeByLogic()
    {
        if (m_pcUnLock != null)
        {
            m_pcUnLock.TrySetActive(MainPlayer.singleton != null && MainPlayer.singleton.IsFrezee && !isShowUIExcPC && MainPlayer.singleton.isDmmModel);
            updatePcUnlockImg();
        }
        var enabled = PlayerBattleModel.Instance.bag_selected == false && MainPlayer.singleton != null;// && MainPlayer.singleton.isAlive;
        if (m_bag.gameObject.activeSelf != enabled)
        {
            m_bag.gameObject.TrySetActive(enabled);
        }
        enabled = m_bagview.activeSelf && m_bag.isActiveAndEnabled;
        if (m_bagview.activeSelf != enabled)
        {
            m_bagview.TrySetActive(enabled);
        }
        // 新手教程去掉该功能
        if (WorldManager.singleton != null && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GUIDE_LEVEL))
        {
            if (m_bag.gameObject.activeSelf || m_bagview.activeSelf || m_goAutoSwitchGun.activeSelf)
            {
                m_bag.gameObject.TrySetActive(false);
                m_bagview.TrySetActive(false);
                m_goAutoSwitchGun.TrySetActive(false);
                m_btnChatType.gameObject.TrySetActive(false);
                m_btnChatInput.gameObject.TrySetActive(false);
                m_audio_btn.TrySetActive(false);
            }
        }
        //枪王规则处理
        if (WorldManager.singleton != null && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO, GameConst.GAME_RULE_KING_CAMP))
        {
            if (m_bag.gameObject.activeSelf || m_bagview.activeSelf)
            {
                m_bag.gameObject.TrySetActive(false);
                m_bagview.TrySetActive(false);
            }
        }
        //生化规则处理
        if (WorldManager.singleton != null && WorldManager.singleton.isZombieTypeModeOpen)
        {
            if (m_bag.gameObject.activeSelf || m_bagview.activeSelf)
            {
                if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE || MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_HERO))
                {
                    m_bag.gameObject.TrySetActive(false);
                    m_bagview.TrySetActive(false);
                }
            }
        }
        //躲猫猫规则处理
        if (WorldManager.singleton != null && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            if (MainPlayer.singleton != null)
            {
                if (MainPlayer.singleton.isDead)
                {
                    m_goJoyback.TrySetActive(false);
                    m_goAim.TrySetActive(false);
                }
                m_bagview.TrySetActive(false);
                m_bag.gameObject.TrySetActive(false);
                //m_BattleAddInfo.gameObject.TrySetActive(false);
                if (MainPlayer.singleton.Camp == 4)
                {
                    m_Squat.TrySetActive(true && isShowUIExcPC);

                    //猫猫特殊处理
                }
            }
        }
        if (m_Magnifier.gameObject.activeSelf)
        {
            if (MainPlayer.singleton == null || MainPlayer.singleton.serverData == null || !(MainPlayer.singleton.curWeapon is WeaponGun) || !((WeaponGun)MainPlayer.singleton.curWeapon).hasScopeButton)
            {
                ShowMagnifier(false);
            }
            else
            {
                m_Magnifier.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, m_Magnifier.isOn ? "UnMagnifier" : "Magnifier"));
            }
        }
        if (m_UnMagnifier.activeSelf)
        {
            if (m_sightCrossHair.activeSelf == false)
            {
                m_UnMagnifier.TrySetActive(false);
            }
        }
        if (m_goLeftJump.activeSelf)
        {
            if ((JUMP_BUTTON_DEFINE)GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_JUMP) == JUMP_BUTTON_DEFINE.DOUBLEB)
            {
                m_goLeftJump.AddMissingComponent<SquatAndJump>();
            }
            else
            {
                m_goLeftJump.TrySetActive(false);
            }
        }
        if (MainPlayer.singleton != null && MainPlayer.singleton.Camp == 4)
        {
            m_btnChatType.transform.Find("Text").GetComponent<Text>().text = "队伍";
            m_chatType = 1;
        }
        if (WorldManager.singleton != null && WorldManager.singleton.isViewBattleModel)
        {
            if (WorldManager.singleton.curPlayer == null || WorldManager.singleton.curPlayer is OtherPlayer || (WorldManager.singleton.curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.CAT))
                SetCrossHairVisible(false);//默认准星
            else
                SetCrossHairVisible(true);
            //m_CircleCrossImage.gameObject.TrySetActive(false);
            //m_btnChatType.gameObject.TrySetActive(false);
            m_btnChatType.transform.Find("Text").GetComponent<Text>().text = "队伍";
            m_chatType = 2;
        }
        if (WorldManager.singleton != null && WorldManager.singleton.isFightInTurnModeOpen)
        {
            if (FightInTurnBehavior.singleton != null && !FightInTurnBehavior.singleton.isSelfFighter)
            {
                ShowBattleSubInfo(false);
            }
            else
            {
                ShowBattleSubInfo(true);
            }
        }

        m_goGrenade.TrySetActive(IsGrenadeVisible() && isShowUIExcPC);
        m_goFlashBomb.TrySetActive(IsFlashBombVisible() && isShowUIExcPC);
        if ((WorldManager.singleton != null && WorldManager.singleton.isViewBattleModel) || (MainPlayer.singleton != null && MainPlayer.singleton.isDead))
            ShowSubFireBtn(false);
    }

    private void OnSkillItemClick(GameObject target, PointerEventData eventData)
    {
        SkillItemRender render;
        if (target == null)
            render = m_SkillList.transform.GetChild(0).GetComponent<SkillItemRender>();
        else
            render = target.GetComponent<SkillItemRender>();

         if (MainPlayer.singleton.ForbidUseSkill)
            {
                return;
            }
        if (render != null)
        {
            render.UseSkill();
        }
    }

    private void OnRefreshSkillItem(int skillId)
    {
        SkillItemRender render = null;
        if (m_dicSkillItemRender.TryGetValue(skillId, out render))
        {
            render.frozen = true;
        }
    }

    private void OnRefreshSkillWithParam(int skillId, string skillParam, int playerId,long tauntedPlayerId)
    {
        if (HideBehavior.tauntSkill == null)
            return;
        if (skillId == HideBehavior.tauntSkill.ID)
        {
            ResetTauntSkill();
        }
        BasePlayer p = WorldManager.singleton.GetPlayerById(playerId);
        
        if (p != null && m_showEmoTex != null)
        {
            string name = p.PlayerName;
            if (WorldManager.singleton.curPlayer != null)
            {
                if (tauntedPlayerId == WorldManager.singleton.curPlayer.Pid)
                {
                    if( WorldManager.singleton.curPlayer == MainPlayer.singleton)
                        m_showEmoTex.text = string.Format("{0}嘲讽了你", name);
                    else
                        m_showEmoTex.text = string.Format("你嘲讽了{0}", WorldManager.singleton.curPlayer.PlayerName);
                }
                else
                {
                    m_showEmoTex.text = string.Format("{0}嘲讽了人类", name);
                }
            }
            
            m_showEmo.TrySetActive(true);
            m_showEmoIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "tauntEmo" + (int.Parse(skillParam) + 1).ToString()));
            showCD = true;
            showStartTime = Time.realtimeSinceStartup;
            AudioManager.PlayHitSound(p.position, string.Format("taunt{0}{1}", skillParam, new System.Random().Next(0, 4)), 15);
        }
    }

    private void OnRescuePlayer(toc_fight_actor_rescued data)
    {
        if (!WorldManager.singleton.isRescueModeOpen) return;

        bool isRescueRevive = MainPlayer.singleton != null ? data.actor == MainPlayer.singleton.PlayerId : false;
        if (!isRescueRevive) return;

        if (PlayerBattleModel.Instance.pre_bag_selected == true && PlayerBattleModel.Instance.bag_selected == false)
        {
            PlayerBattleModel.Instance.bag_selected = true;
            PlayerBattleModel.Instance.pre_bag_selected = false;
        }
    }

    private void OnPlayerUseRevivecoin(toc_fight_survival_use_revivecoin data)
    {
        if (MainPlayer.singleton == null || data.actor != MainPlayer.singleton.PlayerId)
            return;

        PlayerBattleModel.Instance.pre_bag_selected = PlayerBattleModel.Instance.bag_selected;
        PlayerBattleModel.Instance.bag_selected = false;
        refreshViewModeByLogic();
    }

    private void OnPlayerRevive(string name, BasePlayer player)
    {
        if (WorldManager.singleton.isHideModeOpen && MainPlayer.singleton != null && player.PlayerId == MainPlayer.singleton.PlayerId)
        {
            if (MainPlayer.singleton.serverData.camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                m_switch.gameObject.TrySetActive(true);
            }
        }

        if (player != MainPlayer.singleton)
        {
            return;
        }
        RefreshAutoFire();
        ShowMuchKillMsg(0);

        if (!(WorldManager.singleton.isRescueModeOpen && RescueBehavior.singleton != null && RescueBehavior.singleton.isRescueRevive))
        {
            //fjs:2015.12.03 wenle's task
            //生存模式和战队副本战斗回合开始 如果已经选择过武器 则 bag_selected 状态保持不变  即这两个模式只能有一次机会选择武器
            if ((WorldManager.singleton.isSurvivalModeOpen || WorldManager.singleton.isDefendModeOpen || WorldManager.singleton.isChallengeModeOpen) && PlayerBattleModel.Instance.bag_selected)
            {
            }
            else
            {
                PlayerBattleModel.Instance.pre_bag_selected = PlayerBattleModel.Instance.bag_selected;
                PlayerBattleModel.Instance.bag_selected = false;
                //fjs  后台修改流程，进入游戏后会先处于锁定状态，再变换到复活状态
                refreshViewModeByLogic();
            }
        }

        _round_become_zombie = _round_become_zombie && MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE;
        autoSetupViewMode();
        SetCrossHairVisible(true);//默认准星
        //changeCrossType(m_crossType);//默认准星
        m_BattleSubInfo.initData();
        jetfadeout = true;
        jetfadein = false;
        OnUpdateRoleEquip();
    }

    private void OnPlayerSetState(BasePlayer player)
    {
        if (WorldManager.singleton.isViewBattleModel || player == null || player.serverData == null) return;


        if (player.serverData.state == GameConst.SERVER_ACTOR_STATUS_DEAD)
        {
            if (player.PlayerId == m_iBeRescuedPlayerID)
            {
                m_iBeRescuedPlayerID = 0;
                ShowRescueBtn(false);
            }

            if (player.preState == GameConst.SERVER_ACTOR_STATUS_DYING && player == MainPlayer.singleton && WorldManager.singleton.isRescueModeOpen)
            {
                //if (m_notice_panel != null)
                //{
                //    m_notice_panel.play(WorldManager.singleton.gameRule, 5, 1);
                //}
                PlayNoticePanel(AtlasName.Battle, "noone_rescue", 5, new Vector2(0, 80), null, false);
            }

            if (player is MainPlayer)
            {
                ShowBuyGunBtn(false);
                ShowBuyBuffBtn(false);
                ShowBuyBulletBtn(false);

                if (WorldManager.singleton.isSurvivalTypeModeOpen)
                    ShowReviveBtn();
            }

        }
        else if (player.serverData.state == GameConst.SERVER_ACTOR_STATUS_LIVE)
        {
            if (player is MainPlayer)
            {
                ShowReviveBtn(false);
            }
        }
        else if (player.serverData.state == GameConst.SERVER_ACTOR_STATUS_DYING)
        {
            if (player is MainPlayer)
            {
                autoSetupViewMode();
                ShowBuyGunBtn(false);
                ShowBuyBuffBtn(false);
                ShowBuyBulletBtn(false);

                if (WorldManager.singleton.isSurvivalTypeModeOpen)
                    ShowReviveBtn();
            }
        }

    }

    public void OnPlayerStartDie()
    {
        OnPlayerDie(null, MainPlayer.singleton, -1, -1);
        jetfadeout = true;
        jetfadein = false;
    }

    public void OnPlayerDie(BasePlayer shooter, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (dier != MainPlayer.singleton)
        {
            return;
        }
        if( MainPlayer.singleton!=null && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
        {
            OnShowPanelTaunt(false);
        }
        
        m_autoFire.TrySetActive(false);
        if (WorldManager.singleton.isHideModeOpen)
        {
            MainPlayer.singleton.IsHideView = true;
        }
        if (ChangeTeamView.RootTeamViewer != null)
        {
            ChangeTeamView.RootTeamViewer.TrySetActive(false);
            GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_SWITCH_VIEW_BTN, false);
        }
        autoSetupViewMode(true);
        SetCrossHairVisible(false);//默认准星
        //m_CircleCrossImage.gameObject.TrySetActive(false);
        //changeCrossType(m_crossType);//默认准星
        UnshowSomeBtn();
        ShowPickUpHander(false);
        Color newClr = m_redObj.m_imgRed.color;
        newClr.a = 0f;
        m_redObj.m_imgRed.color = newClr;
        m_bag.gameObject.TrySetActive(false);
        if (m_lstBeHitObjs.Count > 0)
        {
            for (int i = m_lstBeHitObjs.Count - 1; i >= 0; i--)
            {
                m_lstBeHitObjs[i].Destory();
            }
            m_lstBeHitObjs.Clear();
        }
        if (WorldManager.singleton.isHideModeOpen && MainPlayer.singleton != null && dier.PlayerId == MainPlayer.singleton.PlayerId)
        {
            m_switch.gameObject.TrySetActive(false);
            m_chatType = 0;
            m_btnChatType.transform.Find("Text").GetComponent<Text>().text = "全体";
        }
        GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_POWER_OFF);

    }

    public List<HarmInfo> m_watchViewHarmList = new List<HarmInfo>();
    private void Toc_fight_spectator_record(toc_fight_spectator_record spectator_record_data)
    {
        m_watchViewHarmList.Clear();
        HarmInfo harmInfo = null;
        p_spectator_record data = null;
        p_spectator_detail info = null;
        for (int i = 0; i < spectator_record_data.list.Length; i++)
        {
            data = spectator_record_data.list[i];

            for (int j = 0; j < data.detail_info.Length; j++)
            {
                harmInfo = new HarmInfo();
                info =  data.detail_info[j];
                harmInfo.beActorId = data.actor_id;
                harmInfo.pid = info.player_id;
                harmInfo.harm = info.damage;
                harmInfo.beActorName = info.damage_name;
                harmInfo.part = info.hit_position;
                if (info.killed_by_who == -1)
                {
                    harmInfo.killByMyself = -1;
                    harmInfo.die = false;
                }
                else
                {
                    if (info.player_id == WorldManager.singleton.curPlayer.Pid)
                    {
                        harmInfo.killByMyself = 1;
                    }
                    else
                    {
                        harmInfo.killByMyself = 0;
                    }
                    harmInfo.die = true;
                }
                m_watchViewHarmList.Add(harmInfo);
            }
        }
    }


    private void SetCrossHairVisible(bool visible)
    {
        bool isCircle = m_crossType == GameConst.CROSS_CIRCLE;
        bool isShowCircle = visible && isCircle;
        bool isShowLine = visible && !isCircle;

        m_CircleCrossImage.gameObject.TrySetActive(isShowCircle);
        for (int i = 0; m_crossHairsColor != null && i < m_crossHairsColor.Length; i++)
        {
            m_crossHairsColor[i].enabled = isShowLine;
        }
        ShowRedPoion();
    }

    private void ShowRedPoion() 
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.curWeapon != null && MainPlayer.singleton.curWeapon.weaponConfigLine != null)
        {
            if(MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_SNIPER_GUN )
            {
                m_crossHairsColor[4].enabled = true;
                m_CircleImg.enabled = false;
            }
            else
            {
                if (m_crossType == GameConst.CROSS_CIRCLE)
                {
                    if (m_CircleImg.rectTransform.sizeDelta.x < 0)
                    {
                        m_CircleImg.enabled = false;
                    }
                    else
                    {
                        m_CircleImg.enabled = true;
                    }
                    
                }
                if(m_crossType == GameConst.CROSS_NO_POION|| m_crossType == GameConst.CROSS_CIRCLE)
                {
                    m_crossHairsColor[4].enabled = false;
                }
                else
                {
                    m_crossHairsColor[4].enabled = true;
                }
            }
        }
    }

    private float m_crossType;
    private void changeCrossType(float type)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive == false)
        {
            return;
        }
        m_crossType = type;
        if ((WorldManager.singleton.curPlayer == null && MainPlayer.singleton != null) || (WorldManager.singleton.curPlayer != null && !(WorldManager.singleton.curPlayer is OtherPlayer)))
            SetCrossHairVisible(true);
    }

    private float m_joystickType;
    private void changeJoystick(float joystickType)
    {
        m_joystickType = joystickType;
        string background = "JoystickBack1";
        string joystick = "Joystick1";
        if (joystickType == GameConst.JOYSTICK1)
        {
            background =  "JoystickBack1";
            joystick = "Joystick1";
        }
        else if (joystickType == GameConst.JOYSTICK2)
        {
            background = "JoystickBack2";
            joystick = "Joystick2";
           
        }
        else if (joystickType == GameConst.JOYSTICK3)
        {
            background = "JoystickBack3";
            joystick = "Joystick3";
        }
        m_JoybackImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, background));
        m_JoystickImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, joystick));
    }

    private float m_FightDirectionType;
    /// <summary>
    /// 设置作战指示按钮位置
    /// </summary>
    /// <param name="value"></param>
    private void changeFightDirection(float type)
    {
        m_FightDirectionType = type;
        RectTransform rtf = m_FightDirectionBtn.GetRectTransform();
        rtf.sizeDelta = new Vector2(55, 53);
        if( m_FightDirectionType == GameConst.FIGHTDIRECTIONBTN_TOP )//左上
        {
            m_FightDirectionBtn.TrySetActive(true);
            rtf.anchorMax = UIHelper.ZERO_X_ONE_Y_VEC;
            rtf.anchorMin = UIHelper.ZERO_X_ONE_Y_VEC;
            rtf.anchoredPosition = new Vector3(29, -28, 0);
            rtf.localRotation = Quaternion.Euler(0f, 0f, 180f);
        }
        else if(m_FightDirectionType == GameConst.FIGHTDIRECTIONBTN_BOTTOM)//右下
        {
            m_FightDirectionBtn.TrySetActive(true);
            rtf.anchorMax = new Vector2(1f, 0f);
            rtf.anchorMin = new Vector2(1f, 0f);
            rtf.anchoredPosition = new Vector3(-29f, 32f, 0f);
            rtf.localRotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else if (m_FightDirectionType == GameConst.FIGHTDIRECTIONBTN_HIDE)//隐藏
        {
            m_FightDirectionBtn.TrySetActive(false);
        }
        showFightDirectBtn();
    }

    private void changeJoystickMove(float joystickMoveType)
    {
        m_joystickMoveType = joystickMoveType;
//       Logger.Error(m_joystickMoveType == GameConst.JOYSTICK_MOVE1);
        m_goJoyback.TrySetActive( isShowUIExcPC && m_joystickMoveType == GameConst.JOYSTICK_MOVE1);
    }

    private void changeHeavyKnifeMode(float heavyKnife)
    {
        m_heavyKnifeAtkType = heavyKnife;
        isHeavyKnifeModeOn = false;
        changeHeavyKnifeTypeIcon();
    }

    /// <summary>
    /// 判断是不是普通的重刀（重刀单独一个按钮）
    /// </summary>
    /// <returns></returns>
    private bool isNormalKnifeMode()
    {
        return m_heavyKnifeAtkType == GameConst.NORMAL_HEAVYKNIFE;
    }

    public override void OnShow()
    {
        /*if( FightVideoManager.isPlayFight == true )
        {
            NetLayer.Send(new tos_record_start_watch() { });
        }*/
        
        //m_goSwitchView.TrySetActive(true);

        //BowCrossHair.TrySetActive(MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPOM_TYPE_BOW);

        CheckBowCross();
        CheckJetPack();
        SetAimShootImg("Aim1", "Shoot1");
        m_alerttPlayerID.Clear();
        RefreshAutoFire();
        m_crossType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_CROSS_TYPE);
        if (m_timerID > -1)
        {
            TimerManager.RemoveTimeOut(m_timerID);
        }

        m_CircleImg.color = Color.green;
        if ( (WorldManager.singleton.curPlayer == null && MainPlayer.singleton != null ) || (WorldManager.singleton.curPlayer != null && !(WorldManager.singleton.curPlayer is OtherPlayer)))
            m_timerID = TimerManager.SetTimeOut(1f, () => { SetCrossHairVisible(true); /*changeCrossType(m_crossType);*/ });

        m_joystickType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_JOYSTICK_TYPE);
        changeJoystick(m_joystickType);
        m_heavyKnifeAtkType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_HEAVYKNIFE);
        changeHeavyKnifeTypeIcon();
        m_joystickMoveType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_JOYSTICK_MOVE_TYPE);
        m_goJoyback.TrySetActive(isShowUIExcPC && m_joystickMoveType == GameConst.JOYSTICK_MOVE1);
        if (HasParams() && (string)m_params[0] == "link_to_PanelUserDefine")
        {//临时的恶心处理方案
            m_params = null;
            HidePanel();
            UIManager.PopChatPanel<PanelUserDefine>();
            return;
        }

        if (WorldManager.singleton.isMyViewerLoaded)
        {
            doShow();
        }
        else
        {
            m_canvas.enabled = false;
            m_bCheckMyViewerLoaded = true;
        }

        if (m_partnerCameraControllPanel != null)
            m_partnerCameraControllPanel.gameObject.SetActive(true);
        //if (WorldManager.singleton.isDotaModeOpen && WorldManager.singleton.isViewBattleModel)
        //{
        //    m_goSwitchView.TrySetActive(false);
        //}

        if (MainPlayer.singleton == null && FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching == false && PlayerBattleModel.Instance.battle_info.spectator_switch == true)
        {
            UIManager.PopPanel<PanelESGkillRank>();
        }

        
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        //等级小于8级且不在观战模式下
        if (PlayerSystem.roleData.level < PanelGuidePC.SHOW_LEVEL && !WorldManager.singleton.isViewBattleModel)
            UIManager.PopPanel<PanelGuidePC>();
        m_goChat.transform.Find("PC_btnChatVoice").gameObject.TrySetActive(true);
        if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_V_DOWN))
        {

            m_goChat.transform.Find("PC_btnChatVoice").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "PC_speaker_btn"));
        }
        else
        {
            m_goChat.transform.Find("PC_btnChatVoice").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "speaker_btn"));
        }
#endif

        if (FightVideoManager.isPlayFight == true)
        {
            m_panelMask.TrySetActive(true);
            m_fightVideoGo.TrySetActive(true);
            m_panelMask.transform.SetAsLastSibling();
            m_setting_btn.transform.SetAsLastSibling();

            TimerManager.SetTimeOut(1f, () => { m_fightVideoGo.transform.SetAsLastSibling(); });
            UIManager.PopPanel<FightVideoDanMu>();
        }
        else
        {
            m_panelMask.TrySetActive(false);
            m_fightVideoGo.TrySetActive(false);
        }
		FightVideoManager.PlaySpeed = 1;
        m_playSpeedText.text = "X" + FightVideoManager.PlaySpeed;
        m_openDanMubtnText.text = "屏蔽弹幕";
        #region 作战指示相关
        m_FightDirectionType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_FIGHT_DIRECTION);
        changeFightDirection(m_FightDirectionType);
        
        
        #endregion
    }

    private void showFightDirectBtn()
    {
        ConfigFightDirection config = ConfigManager.GetConfig<ConfigFightDirection>() ;
        if(config != null && config.GetFightDirectionLineListByType(1).Count > 0 )
        {
            bool ishow = !WorldManager.singleton.isViewBattleModel;
            m_FightDirectionBtn.TrySetActive(true && ishow && (m_FightDirectionType !=GameConst.FIGHTDIRECTIONBTN_HIDE));
        }
        else
        {
            m_FightDirectionBtn.TrySetActive(false);
        }
    }

    public void RefreshAutoFire()
    {
        if (WorldManager.singleton.gameRuleLine == null)
        {
            Logger.Error("null");
            
            return;
        }
        if (WorldManager.singleton.gameRuleLine.CanAutoShoot == 0 || (WorldManager.singleton.gameRuleLine.CanAutoShoot != 2 && !Driver.isMobilePlatform && WorldManager.singleton.gameRule 
            != GameConst.GAME_RULE_HIDE) || MainPlayer.singleton == null || !MainPlayer.singleton.isAlive) //没有自动开火功能
        {
            m_autoFire.gameObject.TrySetActive(false);
        }
        else
        {
            string pcTips = "";
            if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
            {
                if (GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_P_DOWN))
                {
                    ConfigBattlePcKeySettingLine pcl = ConfigManager.GetConfig<ConfigBattlePcKeySetting>().GetLine(GameEvent.INPUT_KEY_P_DOWN) as ConfigBattlePcKeySettingLine;
                    if (pcl != null) pcTips = "<color='#8DEEEE'>[" + GlobalBattleParams.singleton.PcKeyToText(pcl.Value) + "]</color>";
                }
            }
            m_autoFire.gameObject.TrySetActive(true);
            if (Driver.m_platform == EnumPlatform.ios && WorldManager.singleton.gameRule == GameConst.GAME_RULE_HIDE)
            {
                m_autoFire.gameObject.TrySetActive(false);
            }
            if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE) < 0.1f) //关闭自动开火
            {
                m_autoFireOff.gameObject.TrySetActive(true);
                m_autoFireOn.gameObject.TrySetActive(false);
                if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_HIDE)
                {
                    HideBehavior.AutoFire = false;
                }
                m_autoFireTip.text = WorldManager.singleton.gameRuleLine.CanAutoShoot == 1 ? "手动开火" + pcTips : "手动战斗" + pcTips;
                
            }
            else
            {
                if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_HIDE)
                {
                    HideBehavior.AutoFire = true;
                }
                m_autoFireOff.gameObject.TrySetActive(false);
                m_autoFireOn.gameObject.TrySetActive(true);
                m_autoFireTip.text = WorldManager.singleton.gameRuleLine.CanAutoShoot == 1 ? "自动开火" + pcTips : "自动战斗" + pcTips;
            }
        }
        GameDispatcher.Dispatch<bool>(GameEvent.MAINPLAYER_SET_IS_CAN_AUTOFIRE, m_autoFire.activeSelf);
    }

    private void doShow()
    {
        //Logger.Error("doshow");
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerLoaded);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_INIT_DONE, OnMainPlayerLoaded);

        //isEnemy = false;
        m_canvas.enabled = true;
        var state = PlayerBattleModel.Instance.battle_state;
        autoSetupViewMode();
        OnSettingUpdate();
        OnUpdateRoleEquip();
        OnUpdateWeaponSkill(m_awakenWeaponId);

        if (PlayerBattleModel.Instance.battle_info.is_set_once)
            OnUpdateBattleState(PlayerBattleModel.Instance.battle_state, true);

        if ((state.state == 1 || state.state == 2) && state.round == 1)
        {
            if (null == m_startBattleEffect || m_startBattleEffect.isDestroy)
            {
                PlayStateEffect(EffectConst.UI_FIGHT_GAME_BEGIN);
                AudioManager.PlayFightUISound(AudioConst.battleStart);
            }
        }

        if (PlayerBattleModel.Instance.hasBlockVotePanel)
        {
            PlayerBattleModel.Instance.ShowKickOutVotePanel(0, 0, 15);
        }
    }

    void PlayStateEffect(string name)
    {
        //临时解决擂台赛里面不弹特效，后续再慢慢整理
        if (WorldManager.singleton.isFightInTurnModeOpen) return;
        //战斗开始特效经常被update调用，这里简单优化下
        if (name == EffectConst.UI_FIGHT_GAME_BEGIN)
            m_startBattleEffect = UIEffect.ShowEffect(m_tranRoomTip, name, 2, true, false);
        else
            UIEffect.ShowEffect(m_tranRoomTip, name, 2, true, false);
    }

    public override void OnHide()
    {
        _team_king_of_gun_step = 0;
        _current_audio = 0;
        _audio_play_list.Clear();
        NetChatData.stopSpeech();
        if (m_partnerCameraControllPanel != null)
            m_partnerCameraControllPanel.gameObject.SetActive(false);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        if (UIManager.IsOpen<PanelGuidePC>())
            UIManager.GetPanel<PanelGuidePC>().HidePanel();
#endif
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        AutoAim.Clear();
        m_chatDataList.Clear();
        m_crossHairs = null;
        GlobalBattleParams.singleton.NotifyEvent -= OnPlayerChangeBattleParamSetting;
        _team_king_of_gun_step = 0;
        _current_audio = 0;
        _audio_play_list.Clear();
        m_dicSkillItemRender.Clear();
        m_skillGo2SkillRender.Clear();
        m_skillGos.Clear();
        m_skillItemPoolList.Clear();
        m_skillIdList.Clear();
        m_posSkillItemRenders.Clear();

        NetChatData.stopSpeech();
        if (ScreenEffectManager.singleton != null)
        {
            ScreenEffectManager.singleton.Release();
        }
        if (m_partnerCameraControllPanel != null)
        {
            GameObject.Destroy(m_partnerCameraControllPanel.gameObject);
            m_partnerCameraControllPanel = null;
        }

        GameDispatcher.RemoveEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_CROSS_TYPE, changeCrossType);
        GameDispatcher.RemoveEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE, changeJoystick);
        GameDispatcher.RemoveEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE, changeJoystickMove);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.UI_SETCROSSHAIRVISIBLE, SetCrossHairVisible);
        GameDispatcher.RemoveEventListener<float>(GameEvent.UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE, changeHeavyKnifeMode);
        
    }

    static string _PingTxt(int val)
    {
        string color = val < 0 || val > 200 ? "#FF0000" : "#FFFFFF";
        return string.Format("<color='{0}'>{1}</color>", color, val);
    }

    public static int ping;
    private void UpdateFps()
    {
        //帧率ping值，1秒更新一次
        m_fTick += Time.deltaTime;
        if (m_fTick >= 1.0f)
        {
            m_fTick = 0.0f;
            if (m_goFps.activeSelf)
            {
                int fps = Driver.LastFps;
                string fpsColor = fps < 25 ? "#FF0000" : "#FFFFFF";
                m_txtFps.text = string.Format("<color='{1}'>FPS:{0}</color>  Ping:{2}/{3}/{4}", fps, fpsColor,
                    _PingTxt(NetLayer.PingTime), _PingTxt(NetLayer.PingOutTime), _PingTxt(NetLayer.PingServerTime));
            }
        }
    }

    private void UpdateRuleHide()
    {
        if (MainPlayer.singleton != null)
        {
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
            {
                if (MainPlayer.singleton.PlayerId == m_iSelectedPlayerID || m_iSelectedPlayerID == 0)
                {
                    m_goodjob.gameObject.TrySetActive(false);
                    if (MainPlayer.singleton.IsHideView || m_rollershow)
                    {
                        m_goAim.TrySetActive(false);
                        if (FreeViewPlayer.singleton == null || !FreeViewPlayer.singleton.isRuning)
                            m_goJoyback.TrySetActive(false);
                        else
                            m_goJoyback.TrySetActive(true && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
                    }
                    else
                    {
                        m_goAim.TrySetActive(true && isShowUIExcPC);
                        m_goJoyback.TrySetActive(true && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
                    }
                }
                else
                {
                    if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE) && MainPlayer.singleton.Camp == 4)
                    {
                        m_goodjob.gameObject.TrySetActive(true);
                        if (MainPlayer.singleton.IsHideView)
                        {
                            m_goAim.TrySetActive(false);
                            if (FreeViewPlayer.singleton == null || !FreeViewPlayer.singleton.isRuning)
                                m_goJoyback.TrySetActive(false);
                            else
                                m_goJoyback.TrySetActive(true && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
                        }
                        else
                        {
                            m_goAim.TrySetActive(true && isShowUIExcPC);
                            m_goJoyback.TrySetActive(true && isShowUIExcPC);
                        }
                    }
                    if (WorldManager.singleton.GetPlayerById(m_iSelectedPlayerID) != null)
                    {
                        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE) && WorldManager.singleton.GetPlayerById(m_iSelectedPlayerID).serverData.camp != 4)
                        {
                            m_goodjob.gameObject.TrySetActive(false);
                        }
                        else
                        {
                            m_goodjob.gameObject.TrySetActive(true && HideBehavior.singleton.PlayerStartFind());
                        }
                    }
                }
            }
        }
    }

    private void UpdateChatData()
    {
        for (int i = 0; i < m_chatDataList.Count; i++)
        {
            if (Time.realtimeSinceStartup - m_chatDataList[i].m_startTime >= 10)
            {
                m_chatDataList.RemoveAt(i);
                m_dataGridChat.Data = m_chatDataList.ToArray();
                break;
            }
        }
    }

    private void UpdateBagView()
    {
        if (m_bag == null || m_bagview == null)
            return;

        if (WorldManager.singleton.isViewBattleModel)
        {
            m_bag.gameObject.TrySetActive(false);
            m_bagview.TrySetActive(false);
        }

        if (MainPlayer.singleton != null && !MainPlayer.singleton.alive)
        {
            m_bag.gameObject.TrySetActive(false);
            m_bagview.TrySetActive(false);
        }

        if (m_bag.gameObject.activeSelf && PlayerBattleModel.Instance.bag_selected)
        {
            m_bag.gameObject.TrySetActive(false);
            m_bagview.TrySetActive(false);
        }
    }

    private void UpdateBombTip()
    {
        if (WorldManager.singleton.isExplodeModeOpen)
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.isBombHolder)
            {
                ShowHoldBombTipSprite(true, true);
            }
            else if ((WorldManager.singleton.isViewBattleModel && ChangeTeamView.watchedPlayerID == PlayerBattleModel.Instance.bombHolder))
            {
                ShowHoldBombTipSprite(true);
            }
            else
            {
                ShowHoldBombTipSprite(false);
            }
        }
        else
        {
            ShowHoldBombTipSprite(false);
        }
    }

    private void UpdateTouch()
    {
        if (m_bCover || UIManager.m_uiCamera == null || EventSystem.current == null)
            return;

        bool isMainPlayerNull = (MainPlayer.singleton == null || MainPlayer.singleton.mainplayerController == null);
        bool isFreeViewPlayerRuning = (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning);

        if (isMainPlayerNull && !isFreeViewPlayerRuning)
            return;

        MyTouch[] allTouch = GetTouchs();
        if (allTouch == null)
            return;

        if (allTouch.Length > 0 && !isMainPlayerNull)
            MainPlayer.singleton.SetLastAction(ActionType.TOUCH);

        for (int i = 0; i < allTouch.Length; i++)
        {
            MyTouch touch = allTouch[i];
            Vector2 vecScreen = new Vector2(touch.position.x, touch.position.y);
            Vector2 loaclPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(m_parentRectTransform, vecScreen, UIManager.m_uiCamera, out loaclPos);
            Vector2 localJoy;
            RectTransform stickRTans = null;
            if (m_joystickMoveType == GameConst.JOYSTICK_MOVE1)
            {
                stickRTans = m_goJoystick.GetComponent<RectTransform>();
            }
            else
            {
                stickRTans = m_cloneJoybackGo.transform.Find("Joystick").GetComponent<RectTransform>();
            }
            RectTransformUtility.ScreenPointToLocalPointInRectangle(stickRTans.transform.parent as RectTransform, vecScreen, UIManager.m_uiCamera, out localJoy);
            if (touch.phase == TouchPhase.Began)
            {
                if (m_joystickMoveType == GameConst.JOYSTICK_MOVE2)
                {
                    RectTransform joyRTrans = m_cloneJoybackRt;
                    Vector2 joySizeData = joyRTrans.sizeDelta;
                    joyRTrans.anchoredPosition = loaclPos;
                    m_cloneJoybackGo.TrySetActive(false);
                    m_goJoyback.TrySetActive(false);
                    if (isNewShootUI)
                    {
                        m_fMaxJoyDis = joySizeData.x * 0.5f * 1.3f;
                    }
                    else
                    {
                        m_fMaxJoyDis = joySizeData.x * 0.5f;

                    }
                    m_fMaxJoyDisPow = Mathf.Pow(m_fMaxJoyDis, 2);
                    m_fSlowJoyDisPow = Mathf.Pow(m_fMaxJoyDis * 0.7f, 2);
                    m_kOrigStickPos = stickRTans.anchoredPosition;
                }
                GameObject obj = EventSystem.current.currentSelectedGameObject;
                if (obj != null && obj != m_goGrenade && obj != m_goFlashBomb && obj != m_goSubFire && obj != m_PickUpGunBtn)
                    continue;
                if (IsInJoyArea(vecScreen))
                {
                    if (m_bJoyTouch)
                        continue;
                    m_iJoyFingerId = touch.fingerId;
                    m_bJoyTouch = true;
                    //RectTransform joyRTrans = m_goJoyback.GetComponent<RectTransform>();
                    //joyRTrans.anchoredPosition = loaclPos;
                }
                else
                {
                    if (!isFreeViewPlayerRuning && (isMainPlayerNull || MainPlayer.singleton.ForbidLookAround))
                        continue;
                    if (m_iAimFingerId >= 0)
                    {
                        continue;
                    }
                    m_iAimFingerId = touch.fingerId;
                    m_touchRotationSmooth.TouchBegan();
                    m_touchRotationRough.TouchBegan();
                    if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_FIRE) == 0)
                    {
                        if (MainPlayer.singleton != null && MainPlayer.singleton.Camp != 4 && obj == null)
                        {
                            //延迟一帧移动，防止误判点击事件
                            TimerManager.SetTimeOut(0.001f, () => m_rtAim.anchoredPosition = loaclPos);
                        }
                    }
                }
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                if (touch.fingerId == m_iJoyFingerId)
                {
                    if (m_joystickMoveType == GameConst.JOYSTICK_MOVE2)
                    {
                        TimerManager.SetTimeOut(1.5f, delegate() { m_cloneJoybackRt.anchoredPosition = loaclPos; });
                    }
                    
                    //m_cloneRt.anchoredPosition = loaclPos;//无摇杆
                    SetJoyPos(localJoy);
                    GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, true);
                }
                else if (touch.fingerId == m_iAimFingerId)
                {
                    if (!isFreeViewPlayerRuning && (isMainPlayerNull || MainPlayer.singleton.ForbidLookAround))
                        continue;

                    if (!m_useRotationRough)
                    {
                        var aimAccTimes = m_fAimAccTimes;
                        if (Driver.m_platform == EnumPlatform.ios)  //ios设备降低为默认的4/30
                            aimAccTimes /= 7.5f;
                        float l = touch.deltaPosition.magnitude;
                        float ft;
                        if (l <= m_fAimAccStart)
                            ft = 0;
                        else if (l >= m_fAimAccEnd)
                            ft = aimAccTimes;
                        else
                            ft = (l - m_fAimAccStart) * aimAccTimes / (m_fAimAccEnd - m_fAimAccStart);
                        m_touchRotationSmooth.TouchMove(touch, m_fAimSensity * (1 + ft));
                        if (!isFreeViewPlayerRuning)
                            MainPlayer.singleton.mainplayerController.TouchLookRotate(m_touchRotationSmooth.PitchAdd, m_touchRotationSmooth.YawAdd);
                        else
                            FreeViewPlayer.singleton.playerController.TouchLookRotate(m_touchRotationSmooth.PitchAdd, m_touchRotationSmooth.YawAdd);
                    }
                    else
                    {
                        m_touchRotationRough.TouchMove(touch);
                        if (!isFreeViewPlayerRuning)
                            MainPlayer.singleton.mainplayerController.TouchLookRotate(m_touchRotationRough.PitchAdd, m_touchRotationRough.YawAdd);
                        else
                            FreeViewPlayer.singleton.playerController.TouchLookRotate(m_touchRotationRough.PitchAdd, m_touchRotationRough.YawAdd);
                    }

                    if (IsInJoyArea(vecScreen))
                        continue;

                    if (!IsInShootArea(loaclPos))
                        continue;

                    if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_FIRE) == 0)
                    {
                        if (!isMainPlayerNull && MainPlayer.singleton.Camp != 4)
                        {
                            m_rtAim.anchoredPosition = loaclPos;
                        }
                    }
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (touch.fingerId == m_iJoyFingerId)
                {
                    SetJoyPos(localJoy, true);
                    GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, false);
                    m_bJoyTouch = false;
                    m_iJoyFingerId = -1;
                }
                else if (touch.fingerId == m_iAimFingerId)
                {
                    m_iAimFingerId = -1;
                    m_touchRotationRough.TouchEnd();
                }
            }
        }
    }


    private void UpdateWifi()
    {
        //int ping = NetLayer.PingTime;
        int ping = (int)(UDPping.Instance.ping * 1000);
        if (MainPlayer.singleton == null || MainPlayer.singleton.alive == false)
            ping = NetLayer.PingTime;

        m_wifiText.color = new Color(86f / 255f, 235f / 255f, 9f / 255f);
        m_wifiText.text = string.Format("{0}ms", ping);
        if (ping <= 50)
        {
            m_wifiIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "wifi3"));
        }
        else if (ping <= 150)
        {
            m_wifiIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "wifi2"));
        }
        else
        {
            m_wifiIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "wifi1"));
            m_wifiText.color = new Color(1, 0, 0);
        }
    }

    private float m_tick;
    public override void Update()
    {
        UpdateFps();

        //if(m_bGrenadeUpdate) UpdateGrenade();

        //if (!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GUIDE_LEVEL))
        //    UpdateRadar();
        if (isShowUIExcPC == true)
        {
            UpdateReLoadBulletMask();
        }

        UpdateBagView();

        UpdateRuleHide();

        UpdateChatData();

        UpdateBombTip();

        UpdateBeHit();

        UpdateTouch();

        BowCrossDeal();
        UpdateJetPack();
        //UpdateAutoBattle();

        UpdateWifi();
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            UpdateTauntSKill();

            UpdateShowEmo();
        }
        m_tick += Time.deltaTime;
        if (m_tick > 1.5f)
        {
            m_tick = 0;
            OnBattleInfoUpdate();
        }

        

        if (m_bCheckMyViewerLoaded)
        {
            m_fCheckMyViewerTick += Time.deltaTime;
            if (m_fCheckMyViewerTick > 1f)
            {
                m_fCheckMyViewerTick = 0;
                if (WorldManager.singleton.isMyViewerLoaded)
                {
                    m_bCheckMyViewerLoaded = false;
                    doShow();
                }
            }
        }

        if (ScreenEffectManager.singleton != null)
            ScreenEffectManager.singleton.Update();

        //fjs  2015.12.23
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        foreach (GameObject temp in pcHideList)
        {
            if (temp.activeSelf)
            {
                temp.TrySetActive(false);
            }
        }
#endif
    }

    private Dictionary<int, MyTouch[]> m_mapMyTouch = new Dictionary<int, MyTouch[]>();
    private MyTouch[] GetTouchs()
    {
        if (Application.isEditor)
            return m_panelTouch.GetTouchs();
        MyTouch[] arrTouch = null;

        int touchCount = Input.touchCount;
        m_mapMyTouch.TryGetValue(touchCount, out arrTouch);
        if (arrTouch == null)
        {
            arrTouch = new MyTouch[touchCount];
            m_mapMyTouch.Add(touchCount, arrTouch);

            for (int i = 0; i < touchCount; i++)
                arrTouch[i] = new MyTouch();
        }

        for (int i = 0; i < arrTouch.Length; i++)
            arrTouch[i].SetTouch(Input.GetTouch(i));
        return arrTouch;

        //MyTouch[] allTouch = new MyTouch[Input.touchCount];
        //for (int i = 0; i < allTouch.Length; i++)
        //allTouch[i] = new MyTouch(Input.GetTouch(i));
        //return allTouch;
    }

    float showTime = 3f;
    bool showCD = false; //是否需要显示嘲讽界面
    float showStartTime = 0f;
    void UpdateShowEmo()
    {
        if (!showCD)
            return;
        if ((Time.realtimeSinceStartup - showStartTime) >= showTime)
        {
            showCD = false;
            m_showEmo.TrySetActive(false);
        }
    }


    void UpdateTauntSKill()
    {
        if (skillCD)
            return;
        if (HideBehavior.tauntSkill == null)
            return;
        float nowTime = Time.realtimeSinceStartup;
        float diff = nowTime - lastPressTime;
        if (diff >= HideBehavior.tauntSkill.CD * m_hideTauntFaceCDRate * 1.0f)
        {
            skillCD = true;
            m_tauntSkillMask.fillAmount = 0;
        }
        else
        {
            m_tauntSkillMask.fillAmount = 1.0f - diff / (m_hideTauntFaceCDRate*HideBehavior.tauntSkill.CD * 1.0f);
        }
    }

    private bool m_isReloadBullet = false;
    private float m_reloadButtleTime = 0;
    void UpdateReLoadBulletMask()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.curWeapon == null || MainPlayer.singleton.curWeapon.weaponConfigLine == null)
        {
            m_isReloadBullet = false;
            m_ReloadBulletMask.fillAmount = 1;
            m_ReloadBulletCdText.text = "";
            m_ReloadBulletMask.gameObject.TrySetActive(false);
            return;
        }

        if (m_isReloadBullet == false)
        {
            return;
        }
        float nowTime = Time.realtimeSinceStartup;
        float diff = nowTime - m_reloadButtleTime;
        float tempTime = MainPlayer.singleton.curWeapon.ReloadTime * 1.0f;
        if (diff >= tempTime)
        {
            m_isReloadBullet = false;
            m_ReloadBulletMask.fillAmount = 1;
            m_ReloadBulletCdText.text = "";
            m_ReloadBulletMask.gameObject.TrySetActive(false);
        }
        else
        {
            m_ReloadBulletCdText.text = Convert.ToSingle(tempTime - (diff * 1.0f)).ToString("0.0") + "s";
            m_ReloadBulletMask.gameObject.TrySetActive(true);
            m_ReloadBulletMask.fillAmount = 1.0f - diff / tempTime;
        }
    }

    private bool isEnemy = false;
    void OnAimPlayer(BasePlayer player, bool bEnemy)
    {
        if (m_crossHairsColor == null)
            return;
        if (((WorldManager.singleton.isBigHeadModeOpen|| WorldManager.singleton.isDotaModeOpen ||
            MainPlayer.singleton.curWeapon.IsHeal) && m_sightCrossHair.activeSelf == false)
                && (null != player.configData && player.configData.SubType == GameConst.ROLE_SUBTYPE_ROLE)
                && (null != player.serverData && player.serverData.actor_type == GameConst.ACTOR_TYPE_PERSON))
        {
            if (bEnemy)
            {
                if (m_greenCrossHair.enabled)
                {
                    SetCrossHairVisible(true);
                    //changeCrossType(m_crossType);
                    m_greenCrossHair.enabled = false;
                }
                for (int i = 0; i < 4; i++)
                {
                    m_crossHairsColor[i].color = Color.red;
                }
                if (m_crossType == GameConst.CROSS_CIRCLE)
                {
                    m_CircleImg.color = Color.red;
                }
            }
            else
            {
                SetCrossHairVisible(false);
                //m_CircleCrossImage.gameObject.TrySetActive(false);
                m_greenCrossHair.enabled = true;
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                m_crossHairsColor[i].color = bEnemy ? Color.red : Color.green;
            }

            if (m_crossType == GameConst.CROSS_CIRCLE)
            {
                m_CircleImg.color = bEnemy ? Color.red : Color.green;
            }
        }
    }

    void OnCancelAimPlayer()
    {
        if (m_crossHairsColor == null)
            return;
        if (m_greenCrossHair.enabled)
        {
            m_greenCrossHair.enabled = false;
            SetCrossHairVisible(true);
            //changeCrossType(m_crossType);
        }
        else if (m_crossHairsColor[0].color != Color.green)
        {
            for (int i = 0; i < 4; i++)
            {
                m_crossHairsColor[i].color = Color.green;
            }
        }
        if (m_CircleImg.color != Color.green)
        {
            m_CircleImg.color = Color.green;
        }
    }

    void OnPlayerChangeBattleParamSetting(string paramName, float newValue)
    {

        if (paramName == GlobalBattleParams.KEY_JUMP)
        {
            if ((JUMP_BUTTON_DEFINE)(newValue) == JUMP_BUTTON_DEFINE.DOUBLEB)
            {
                if ((m_mode & BattleViewMode.MODE_ACTION) == 0)
                {
                    return;
                }
                m_goLeftJump.TrySetActive(true && isShowUIExcPC);
                m_goLeftJump.AddMissingComponent<SquatAndJump>();
            }
            else
            {
                m_goLeftJump.TrySetActive(false);
            }
        }
    }

    void OnUserSetPos(ConfigBattleUIControlPos userSetPos)
    {
        UpdateUserSetUIPos(userSetPos);
        UpdateJoyPos();
        UpdateShooterPos();
    }

    void UpdateUserSetUIPos(ConfigBattleUIControlPos controlsUserSetPos)
    {
        foreach (ConfigBattleUIControlPosLine kObjPosInfo in m_defaultControlPosInfo.m_dataArr)
        {
            Transform controlTransform = m_go.transform.Find(kObjPosInfo.key);
            if (controlTransform != null)
            {
                GameObject control = controlTransform.gameObject;
                ConfigBattleUIControlPosLine userLine = controlsUserSetPos.GetLine(kObjPosInfo.key);
                if (control != null)
                {
                    if (userLine != null)
                    {
                        SetUIControlPos(control, userLine);
                        if (userLine.key == "SwitchGun" && (userLine.anchorX != kObjPosInfo.anchorX || userLine.anchorY != kObjPosInfo.anchorY))
                        {
                            m_SkillList.GetComponent<RectTransform>().anchoredPosition = control.GetComponent<RectTransform>().anchoredPosition;
                        }
                        if (userLine.key == "SmallMap")
                        {
                            if (m_panelBattleRadar != null)
                                m_panelBattleRadar.SetMapPosition(control.GetComponent<RectTransform>().anchoredPosition);
                            else
                                PanelBattleRadar.pos = control.GetComponent<RectTransform>().anchoredPosition;
                        }
                    }
                    else
                    {
                        SetUIControlPos(control, kObjPosInfo);
                    }
                }
            }
        }
    }

    void UpdateJoyPos(bool isClone = false)
    {
        RectTransform joyRTrans = m_goJoyback.GetComponent<RectTransform>();
        Vector2 joySizeData = joyRTrans.sizeDelta;

        RectTransform stickRTans = m_goJoystick.GetComponent<RectTransform>();
        //Vector2 stickSizeData = stickRTans.sizeDelta;
        //stickSizeData.x = joySizeData.x * 0.7f;
        //stickSizeData.y = joySizeData.y * 0.7f;
        //stickRTans.sizeDelta = stickSizeData;

        if (isNewShootUI)
        {
            m_fMaxJoyDis = joySizeData.x * 0.5f * 1.3f;
        }
        else
        {
            m_fMaxJoyDis = joySizeData.x * 0.5f;

        }
        m_fMaxJoyDisPow = Mathf.Pow(m_fMaxJoyDis, 2);
        m_fSlowJoyDisPow = Mathf.Pow(m_fMaxJoyDis * 0.7f, 2);
        m_kOrigStickPos = stickRTans.anchoredPosition;
    }

    void UpdateShooterPos()
    {
        RectTransform aimRTans = m_rtAim; //m_goAim.GetComponent<RectTransform>();
        Vector2 aimSizeData = aimRTans.sizeDelta;
        m_kAimOriginPos = aimRTans.anchoredPosition;

        RectTransform shootRTans = m_rtShoot; // m_goShoot.GetComponent<RectTransform>();
        Vector2 shootSizeData = shootRTans.sizeDelta;
        //shootSizeData.x = aimSizeData.x * 1f;
        //shootSizeData.y = aimSizeData.y * 1f;
        //shootRTans.sizeDelta = shootSizeData;

        m_fMaxShootDis = shootSizeData.x;
        m_fMaxShootDisPow = Mathf.Pow(m_fMaxShootDis, 2);
    }

    void SetUIControlPos(GameObject obj, ConfigBattleUIControlPosLine info)
    {
        RectTransform goRectTransform = obj.GetComponent<RectTransform>();
        Vector2 anchor = goRectTransform.anchorMin;
        anchor.x = info.fAnchorMinX;
        anchor.y = info.fAnchorMinY;
        goRectTransform.anchorMin = anchor;

        anchor = goRectTransform.anchorMax;
        anchor.x = info.fAnchorMaxX;
        anchor.y = info.fAnchorMaxY;
        goRectTransform.anchorMax = anchor;

        Vector2 sizeData = goRectTransform.sizeDelta;
        sizeData.x = info.fWidth;
        sizeData.y = info.fHeight;
        goRectTransform.sizeDelta = sizeData;

        Vector3 ScaleData = goRectTransform.localScale;
        ScaleData.x = info.fLocalScale;
        ScaleData.y = info.fLocalScale;
        ScaleData.z = info.fLocalScale;
        goRectTransform.localScale = ScaleData;

        Vector2 anchorPos = goRectTransform.anchoredPosition;
        anchorPos.x = info.anchorX;
        anchorPos.y = info.anchorY;
        goRectTransform.anchoredPosition = anchorPos;

        return;
    }

    ConfigBattleUIControlPos ReadControlPosFromPlayerPrefs()
    {
        ConfigBattleUIControlPos userSetControlPosInfo = ScriptableObject.CreateInstance<ConfigBattleUIControlPos>();
        string strUIInfo = PlayerPrefs.GetString(SAVE_BATTLE_UI_POS_KEY, "");
        bool hasError = false;
        if (strUIInfo != "")
        {
            try
            {
                userSetControlPosInfo.Load(WWW.UnEscapeURL(strUIInfo));
            }
            catch
            {
                hasError = true;
                PlayerPrefs.DeleteKey(SAVE_BATTLE_UI_POS_KEY);
            }
        }

        if (hasError || strUIInfo == "")
        {
            userSetControlPosInfo.Load(m_defaultControlPosInfo);
        }

        return userSetControlPosInfo;
    }

    void OnBattleRoundStart()
    {
        RefreshAutoFire();
        ShowPickUpHander(false);
        DropItemManager.singleton.ClearDropItems();
        m_paneltitle.TrySetActive(false);
    }

    void OnBattleOver()
    {
        PanelBattleSurvivalScore.TryDeleteBossHPBar(null, true);
        PanelBattleDotaScore.TryDeleteBossHPBar(null, true);
        ShowPickUpHander(false);
        DropItemManager.singleton.ClearDropItems();
    }

    void OnEnterBurstPoint(GameObject whichBurstPoint, GameObject go, string type, int id)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (go == MainPlayer.singleton.gameObject)
        {
            if (type == "Burst")
            {
                m_iBurstPoint = Convert.ToInt32(whichBurstPoint.name);
                if (MainPlayer.singleton.isBombHolder)
                {
                    ShowPlaceBombBtn(true);
                }
                else if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.UNION && BurstBehavior.singleton.IsC4BombPlaced() && m_iBurstPoint == BurstBehavior.singleton.bomb_placed_point)
                {
                    ShowRemoveBombBtn(true);
                }
            }
            else if (type == "Rescue")
            {
                m_iBeRescuedPlayerID = id;
                CheckShowRescueBtn(id);
            }
            else if (type == "Buff")
            {
                ShowBuyBuffBtn();
            }
            else if (type == "Gun")
            {
                ShowBuyGunBtn();
            }
            else if (type == "Bullet")
            {
                ShowBuyBulletBtn();
            }
            else if (type == "DefendBuff")
            {
                ShowBuyBuffBtn();
            }
            else if (type == "DefendGun")
            {
                ShowBuyGunBtn();
            }
            else if (type == "DefendBullet")
            {
                ShowBuyBulletBtn();
            }
        }
    }

    void OnStayInBurstPoint(GameObject whichBurstPoint, GameObject go, string type, int id)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (go == MainPlayer.singleton.gameObject)
        {
            if (type == "Burst")
            {
                m_iBurstPoint = Convert.ToInt32(whichBurstPoint.name);
                //if (MainPlayer.singleton.isBombHolder)
                //{
                //    ShowPlaceBombBtn(true);
                //}
                //else
                //{
                //    ShowPlaceBombBtn(false);
                //}
                if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.UNION && BurstBehavior.singleton.IsC4BombPlaced() && m_iBurstPoint == BurstBehavior.singleton.bomb_placed_point)
                {
                    ShowRemoveBombBtn(true);
                }
            }
            else if (type == "Rescue")
            {
                m_iBeRescuedPlayerID = id;
                CheckShowRescueBtn(id);
            }
        }
    }

    void OnLeaveBurstPoint(GameObject whichBurstPoint, GameObject go, string type, int id)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (go == MainPlayer.singleton.gameObject)
        {
            if (type == "Burst")
            {
                ShowPlaceBombBtn(false);

                ShowRemoveBombBtn(false);

                m_iBurstPoint = 0;
            }
            else if (type == "Rescue")
            {
                m_iBeRescuedPlayerID = 0;
                ShowRescueBtn(false);
                if (RescueBehavior.singleton != null) RescueBehavior.singleton.StopRescue();
            }
            else if (type == "Buff")
            {
                ShowBuyBuffBtn(false);
            }
            else if (type == "Gun")
            {
                ShowBuyGunBtn(false);
            }
            else if (type == "Bullet")
            {
                ShowBuyBulletBtn(false);
                if (SurvivalBehavior.singleton != null) SurvivalBehavior.singleton.StopBuy();
            }
            else if (type == "DefendBuff")
            {
                ShowBuyBuffBtn(false);
            }
            else if (type == "DefendGun")
            {
                ShowBuyGunBtn(false);
            }
            else if (type == "DefendBullet")
            {
                ShowBuyBulletBtn(false);
                if (SurvivalBehavior.singleton != null) SurvivalBehavior.singleton.StopBuy();
            }
            pcShortCutKeyECallBack = null;
        }
    }

    #region Quick Throw Weapon Button : Grenade FlashBomb

    bool hasPress = false;
    void UpdateGrenade()
    {
        if (m_bGrenadeReady)
        {
            m_fPressLastTime += Time.deltaTime;
            if (m_fPressLastTime >= GRENADE_TIME_UP_DOWN_INTERVAL)
            {
                //按下
                if (!hasPress)
                {
                    hasPress = true;
                    OnGrenadePressed();
                }
            }
        }
        else
        {
            m_bGrenadeUpdate = false;
            hasPress = false;
            if (m_fPressLastTime < GRENADE_TIME_UP_DOWN_INTERVAL)
            {
                //切雷
                if (MainPlayer.singleton == null) return;
                m_bGrenadeSwitch = true;
                string strWeaponId = MainPlayer.singleton.serverData.grenadeID.ToString();
                OnPlayerSwitchNextGun(strWeaponId);
            }
            else
            {
                //扔出去
                OnGrenadeThrowed();
            }
        }
    }

    bool hasFlashBombPress = false;
    void UpdateFlashBomb()
    {
        if (m_bFlashBombReady)
        {
            m_fFlashBombPressLastTime += Time.deltaTime;
            if (m_fFlashBombPressLastTime >= GRENADE_TIME_UP_DOWN_INTERVAL)
            {
                //按下
                if (!hasFlashBombPress)
                {
                    hasFlashBombPress = true;
                    OnFlashBombPressed();
                }
            }
        }
        else
        {
            m_bFlashBombUpdate = false;
            hasFlashBombPress = false;
            if (m_fFlashBombPressLastTime < GRENADE_TIME_UP_DOWN_INTERVAL)
            {
                //切雷
                if (MainPlayer.singleton == null) return;
                m_bFlashBombSwitch = true;
                string strWeaponId = MainPlayer.singleton.serverData.flashBombID.ToString();
                OnPlayerSwitchNextGun(strWeaponId);
            }
            else
            {
                //扔出去
                OnFlashBombThrowed();
            }
        }
    }

    void OnGrenadeButtonDown(GameObject target, PointerEventData eventData)
    {
        if (canUseQuickThrowButton == false)
            return;

        //        if (m_iAimFingerId >= 0)
        //        {
        //            m_bGrenadeReady = false;
        //            return;
        //        }
        if (Time.realtimeSinceStartup - m_fLastGrenadeTime < GRENADE_TIME_INTERVAL)
        {
            m_bGrenadeReady = false;
            return;
        }
        m_fLastGrenadeTime = Time.realtimeSinceStartup;
        m_fPressLastTime = 0f;
        m_bGrenadeReady = true;
        m_bGrenadeUpdate = true;

        OnGrenadePressed();
    }

    void OnFlashBombButtonDown(GameObject target, PointerEventData eventData)
    {
        if (canUseQuickThrowButton == false)
            return;

        //        if (m_iAimFingerId >= 0)
        //        {
        //            m_bFlashBombReady = false;
        //            return;
        //        }
        if (Time.time - m_fLastFlashBombTime < GRENADE_TIME_INTERVAL)
        {
            m_bFlashBombReady = false;
            return;
        }
        m_fLastFlashBombTime = Time.time;
        m_fFlashBombPressLastTime = 0f;
        m_bFlashBombReady = true;
        m_bFlashBombUpdate = true;

        OnFlashBombPressed();
    }

    void OnGrenadePressed()
    {
        if (canUseQuickThrowButton == false)
            return;

        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_GRENADE, true);
    }

    void OnFlashBombPressed()
    {
        if (canUseQuickThrowButton == false)
            return;

        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_FLASHBOMB, true);
    }

    void OnGrenadeButtonUp(GameObject target, PointerEventData eventData)
    {
        if (canUseQuickThrowButton == false)
            return;

        m_bGrenadeReady = false;
        m_goGrenade.TrySetActive(false);

        OnGrenadeThrowed();
    }

    void OnFlashBombButtonUp(GameObject target, PointerEventData eventData)
    {
        if (canUseQuickThrowButton == false)
            return;

        m_bFlashBombReady = false;
        m_goFlashBomb.TrySetActive(false);

        OnFlashBombThrowed();
    }

    void OnGrenadeThrowed()
    {
        if (canUseQuickThrowButton == false)
            return;

        PlayerBattleModel.Instance.bag_selected = true;
        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_GRENADE, false);
    }

    void OnFlashBombThrowed()
    {
        if (canUseQuickThrowButton == false)
            return;

        PlayerBattleModel.Instance.bag_selected = true;
        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_FLASHBOMB, false);
    }


    // 正在使用快捷扔手雷，闪光弹等时，不能再按，否则前一个的Throw会被打断，但已经扔过了
    private bool canUseQuickThrowButton
    {
        get
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.weaponMgr != null && MainPlayer.singleton.weaponMgr.usingThrowWeapon == false && !MainPlayer.singleton.ForbidFire)
                return true;
            return false;
        }
    }

    #endregion

    private void onShowGunScope(int zoomMode)
    {
        if (zoomMode == 0)//关镜
        {
            m_Magnifier.isOn = false;
            m_Magnifier.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "Magnifier"));
            if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && (MainPlayer.singleton.curWeapon is WeaponGun) && ((WeaponGun)MainPlayer.singleton.curWeapon).hasScopeButton)
            {
                ShowMagnifier(true);
            }
            m_UnMagnifier.TrySetActive(false);
            m_sightCrossHair.TrySetActive(false);
            //changeCrossType(m_crossType);
            SetCrossHairVisible(true);

        }
        else if (zoomMode > 0)//开镜（带镜框）
        {
            SetCrossHairVisible(false);
            m_sightCrossHair.TrySetActive(true);
            changeCrossHairType();
        }
        else if (zoomMode < 0)//开镜（无镜框）
        {

        }
    }

    /// <summary>
    /// 更改狙击枪瞄准镜类型
    /// </summary>
    private void changeCrossHairType()
    {
        int sniperUI = 0;
        if (WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.curWeapon != null)
            sniperUI = WorldManager.singleton.curPlayer.curWeapon.weaponConfigLine.SniperUI;
        else if (MainPlayer.singleton != null && MainPlayer.singleton.curWeapon != null)
            sniperUI = MainPlayer.singleton.curWeapon.weaponConfigLine.SniperUI;
        if (sniperUI == 1)
        {
            m_CrossHairCenterImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "simpleCrosshair"));
            m_CrossHairHImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "simpleImage"));
            m_CrossHairVImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "simpleImage"));
            m_CrossHairCenterImage.rectTransform.sizeDelta = new Vector2(640.0f, 640.0f);
        }
        else if (sniperUI == 2)
        {
            m_CrossHairCenterImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "complexCrosshair"));
            m_CrossHairHImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "complexImage"));
            m_CrossHairVImage.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "complexImage"));
            m_CrossHairCenterImage.SetNativeSize();
        }
        m_CrossHairHImage.SetNativeSize();
        m_CrossHairVImage.SetNativeSize();

    }

    void OnShowUnMagnifier(bool value)
    {
        if ((m_mode & BattleViewMode.MODE_WEAPON) == 0 || m_Magnifier.isOn)
        {
            return;
        }
        m_UnMagnifier.TrySetActive(value && isShowUIExcPC);
        if (value == true)
        {
            ShowMagnifier(false);
        }
        else if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && (MainPlayer.singleton.curWeapon is WeaponGun) && ((WeaponGun)MainPlayer.singleton.curWeapon).hasScopeButton)
        {
            ShowMagnifier(true);
        }
    }

    //void OnShowMagnifier( bool bShow )
    //{
    //    Logger.Error("OnShowMagnifier, "+bShow);
    //    if ((m_mode & BattleViewMode.MODE_WEAPON) == 0)
    //    {
    //        return;
    //    }
    //    m_Magnifier.gameObject.TrySetActive(bShow);
    //}

    void OnClickMagnifier(GameObject target, PointerEventData eventData)
    {
        if (m_Magnifier.isOn)
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_ENABLE_GUN_SCOPE, true);
            m_Magnifier.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "UnMagnifier"));
        }
        else
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_ENABLE_GUN_SCOPE, false);
            m_Magnifier.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "Magnifier"));
        }
    }


    //private void OnActorStateUpdated(BasePlayer target)
    //{
    //    if (target == MainPlayer.singleton && target.serverData != null)
    //    {
    //        if (WorldManager.singleton.isZombieTypeModeOpen)
    //        {
    //            //m_BattleAddInfo.SetData(target.serverData);
    //        }
    //    }
    //}

    private void OnBattleInfoUpdate()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        if (data != null)
        {
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
            {
                if (m_gun_route_panel != null)
                {
                    m_gun_route_panel.updateData();
                }
            }
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_CAMP))
            {
                if (m_team_gun_route_panel != null)
                {
                    m_team_gun_route_panel.updateData();
                }
                if (data.camps != null && data.camps.Count > 0 && data.camps[0].players != null && data.camps[0].players.Count > 0)
                {
                    var old_step = _team_king_of_gun_step;
                    _team_king_of_gun_step = data.camps[0].players[0].king_weapon_seq;
                    if (old_step > 0 && _team_king_of_gun_step != old_step)
                    {
                        //m_notice_panel.play(GameConst.GAME_RULE_KING_CAMP, 5f, PlayerBattleModel.Instance.battle_info);
                        KingCampPlayNoticePanel();
                    }
                }
            }
        }
    }

    void KingCampPlayNoticePanel()
    {
        SBattleInfo battle_info = PlayerBattleModel.Instance.battle_info;
        SBattleCamp win_camp = null;
        SBattleCamp lost_camp = null;
        foreach (var camp in battle_info.camps)
        {
            if (camp.camp == battle_info.king_camp_last_win)
            {
                win_camp = camp;
            }
            else
            {
                lost_camp = camp;
            }
        }
        var weapon_seq = battle_info.camps[0].players[0].king_weapon_seq;
        var cofing = ConfigManager.GetConfig<ConfigKing>();
        NoticePanelProp prop = new NoticePanelProp();
        //胜利阵营标记
        string symbol0 = win_camp == null || win_camp.camp == 1 ? "camp_ico_1" : "camp_ico_2";
        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = symbol0, pos = new Vector2(70, 56) });
        //下阶段武器标记
        var next_rule = cofing.GetLine(weapon_seq);
        string symbol1 = next_rule != null ? "weapon_type_txt_" + next_rule.WeaponTypeInt : "weapon_type_txt_2";
        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = symbol1, pos = new Vector2(-110, -55) });
        //比分
        string num0 = win_camp == null || win_camp.camp == 1 ? "num_o" : "num_b";
        int val0 = win_camp != null ? win_camp.king_camp_score : 0;
        prop.numIconList.Add(new ImgNumProp() { preName = num0, atlas = AtlasName.Battle, align = 0, gap = -7, pos = new Vector2(-140, 0), value = val0 });

        string num1 = "num_gray";
        int val1 = lost_camp != null ? lost_camp.king_camp_score : 0;
        prop.numIconList.Add(new ImgNumProp() { preName = num1, atlas = AtlasName.Battle, align = 0, gap = -7, pos = new Vector2(-50, 0), value = val1 });
        //奖励积分
        string symbol2 = "num_o_plus";
        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = symbol2, pos = new Vector2(130, 5) });

        string num2 = "num_gray";
        var rule = cofing.GetLine(weapon_seq > 1 ? weapon_seq - 1 : 1);
        int val2 = rule != null ? rule.CampScore : 0;
        prop.numIconList.Add(new ImgNumProp() { preName = num2, atlas = AtlasName.Battle, align = 0, gap = -7, pos = new Vector2(150, 5), value = val2 });
        PlayNoticePanel(AtlasName.GunKing, "gun_route_step_tips", 5, new Vector2(0, 0), prop);
    }

    void PlayRoundTip(int round)
    {
        if (WorldManager.singleton.isFightInTurnModeOpen && FightInTurnBehavior.singleton != null)
        {
            NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
            prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Common, sprite = "round3_" + round, pos = new Vector2(114, -90) });
            PlayNoticePanel(AtlasName.Common, "round3", FightInTurnBehavior.singleton.BEFORE_START_SEC, new Vector2(0, -90), prop);
        }
    }

    void OnUpdateBattleState(SBattleState data)
    {
        OnUpdateBattleState(data, false);
    }

    void OnUpdateBattleState(SBattleState data, bool force)
    {
        var rule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        switch (data.state)
        {
            case GameConst.Fight_State_GameInit:
                break;
            case GameConst.Fight_State_GameStart:
                refreshViewModeByLogic();
                UnshowSomeBtn();
                ResetBattleCD(0);
                if (data.round == 1)
                {
                    if (null == m_startBattleEffect || m_startBattleEffect.isDestroy)
                        PlayStateEffect(EffectConst.UI_FIGHT_GAME_BEGIN);
                    PlayerBattleModel.Instance.ClearBattleRecord(); //清空战绩
                }
                AudioManager.PlayFightUISound(AudioConst.battleStart);
                if (MainPlayer.singleton != null) MainPlayer.singleton.IsHideView = false;
                m_gjobnum.text = "0";
                break;
            case GameConst.Fight_State_RoundStart:
                ShowMuchKillMsg(0);
                PlayRoundTip(data.round);
                _round_become_zombie = true;
                m_iSelectedPlayerID = 0;
                m_gjobnum.text = "0";
                if (UIManager.IsOpen<PanelHideWin>())
                {
                    UIManager.GetPanel<PanelHideWin>().HidePanel();
                }
                ResetBattleCD(PlayerBattleModel.Instance.battle_info.round_time);

                if (rule.CheckGameTags(new string[] { GameConst.ZOMBIE_MODE, GameConst.SURVIVAL_MODE }))
                    AudioManager.PlayBgMusic();
                break;
            case GameConst.Fight_State_RoundEnd:
                if (rule.CheckGameTags(new string[] { GameConst.ZOMBIE_MODE, GameConst.SURVIVAL_MODE }) && AudioManager.IsBgMusicPlaying())
                    AudioManager.StopBgMusic();
                GameDispatcher.Dispatch<bool>(PanelGuidePC.EVENT_STEP_USESKILL, false);
                UnshowSomeBtn();
                ResetBattleCD(-2);
                if (rule.WinType == "zombie" || rule.WinType == "zombie_hero")   // 生化模式弹 人类 or 生化 胜利
                    PlayStateEffect(PlayerBattleModel.Instance.battle_state.last_win == 1 ? EffectConst.UI_FIGHT_HUMAN_WIN : EffectConst.UI_FIGHT_ZOMBIE_WIN);
                else if (rule.WinType == "hide")
                {
                    if (GetHideAlive().Length > 0)
                    {
                        PlayStateEffect(EffectConst.UI_FIGHT_HIDE_WIN);
                        UIManager.PopPanel<PanelHideWin>(GetHideAlive());
                    }
                    else
                    {
                        PlayStateEffect(EffectConst.UI_FIGHT_HUMAN_WIN);
                    }
                }
                else if (rule.WinType == "challenge")
                {
                    if (PlayerBattleModel.Instance.CheckRoundWin(m_iSelectedPlayerID))
                        PlayStateEffect(EffectConst.UI_FIGHT_CHALLENGE_ROUND_WIN);
                }
                else if (rule.MaxRound > 1)  // 单回合的不提示
                {
                    string effectName = PlayerBattleModel.Instance.CheckRoundWin(m_iSelectedPlayerID) ? EffectConst.UI_FIGHT_ROUND_WIN : EffectConst.UI_FIGHT_ROUND_LOSE;
                    PlayStateEffect(effectName);
                    if (WorldManager.singleton.isFightInTurnModeOpen)
                        UIEffect.ShowEffect(m_tranRoomTip, effectName, 2, true, false);
                }

                break;
            case GameConst.Fight_State_GameOver:
                UnshowSomeBtn();
                ResetBattleCD(-1);
                //                EnableMouseEvent = false;
                if (rule.WinType == "zombie" || rule.WinType == "zombie_hero")
                {
                    PlayStateEffect(EffectConst.UI_FIGHT_GAME_END);
                    AudioManager.PlayFightUISound(AudioConst.battleEnd);
                }
                else if (rule.WinType == "king_solo")   // 枪王之王
                {
                    SBattlePlayer king = PlayerBattleModel.Instance.GetKingSolo();
                    if (king != null)
                    {
                        m_txtCenterName_txt.text = PlayerSystem.GetRoleNameFull(king.name, king.player_id);
                        GameObject go = m_txtCenterName.gameObject;
                        go.TrySetActive(true);
                        UIEffect.ShowEffect(m_txtCenterName, UIEffectShowType.SiblingAfter, EffectConst.UI_FIGHT_KINGSOLO_END, 4, true, Vector2.zero);
                        TimerManager.SetTimeOut(4, () =>
                        {
                            if (go != null)
                                go.TrySetActive(false);
                        });
                    }
                    else
                    {
                        PlayStateEffect(EffectConst.UI_FIGHT_GAME_END);
                        AudioManager.PlayFightUISound(AudioConst.battleEnd);
                    }
                }
                else if (rule.WinType == "hide")
                {
                    PlayStateEffect(EffectConst.UI_FIGHT_GAME_END);
                    AudioManager.PlayFightUISound(AudioConst.battleEnd);
                }
                else
                {
                    var win = PlayerBattleModel.Instance.CheckGameWin(m_iSelectedPlayerID);
                    PlayStateEffect(win ? EffectConst.UI_FIGHT_GAME_WIN : EffectConst.UI_FIGHT_GAME_LOSE);
                    AudioManager.PlayFightUISound(win ? AudioConst.battleWin : AudioConst.battleFail);
                    AudioManager.PlayFightUIVoice(win ? AudioConst.victory : AudioConst.defeat);
                }
                //这个是WinType不要用GameConst中的GAME_RULE_xxx去比较
                if (rule.WinType != "king_solo"
                    && rule.WinType != "survival"
                    && rule.WinType != "defend"
                    && rule.WinType != "world_boss"
                    && rule.WinType != "hide"
                    && rule.WinType != "zombie"
                    && rule.WinType != "zombie_hero"
                    && rule.WinType != "zombie_ultimate"
                    && rule.WinType != "challenge"
                    )
                {
                    if (m_panelmvp != null)
                    {
                        m_panelmvp.TrySetActive(true);
                        m_mvpname.text = GetMvpName();
                        TimerManager.SetTimeOut(3, () =>
                        {
                            if (m_panelmvp != null)
                                m_panelmvp.TrySetActive(false);
                        });
                    }
                }
                AudioManager.StopBgMusic();
                break;
        }
        if (WorldManager.singleton.isRescueModeOpen && !WorldManager.singleton.isViewBattleModel)
        {
            if (data.state == 0 || force)
            {
                //m_notice_panel.play(WorldManager.singleton.gameRule, 8);
                string spriteName = "";
                if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_EXPLODE_REVIVE))
                {
                    BasePlayer curPlayer = MainPlayer.singleton;
                    if (curPlayer != null)
                    {
                        spriteName = curPlayer.Camp == 1 ? "explode_notice1" : "explode_notice2";
                    }
                }
                else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_DAGGER_REVIVE))
                {
                    spriteName = "dagger_notice";
                }
                if (spriteName.IsNullOrEmpty()) return;
                PlayNoticePanel(AtlasName.Battle, spriteName, 8, new Vector2(0, 80));
            }
        }

        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
        {
            if (data.state == 0 || force)
            {
                NoticePanelProp prop = new NoticePanelProp() { bgAltas = AtlasName.Battle, bgName = "battle_step_tips_bg" };
                if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
                    PlayNoticePanel(AtlasName.GunKing, "gun_route_tips_0", 5, new Vector3(-600, 0), prop, true, new Vector3(0.5f, 0.5f, 0.5f));
                else
                    PlayNoticePanel(AtlasName.GunKing, "gun_route_tips_0", 5, Vector2.zero, prop);
                //m_notice_panel.play(rule.Type, 5);
            }
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_CAMP))
        {
            if (data.state == 0 || force)
            {
                NoticePanelProp prop = new NoticePanelProp() { bgAltas = AtlasName.Battle, bgName = "battle_step_tips_bg" };
                if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
                    PlayNoticePanel(AtlasName.GunKing, "gun_route_tips_1", 5, new Vector3(-600, 0), prop, true, new Vector3(0.5f, 0.5f, 0.5f));
                else
                    PlayNoticePanel(AtlasName.GunKing, "gun_route_tips_1", 5, Vector2.zero, prop);
            }
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_DAGGER, GameConst.GAME_RULE_GRENADE, GameConst.GAME_RULE_HANDGUN, GameConst.GAME_RULE_SNIPE))
        {
            if (data.state == 2)
            {
                if (WorldManager.singleton.isViewBattleModel) return;
                string spriteName = rule.GameType + "_tip";
                PlayNoticePanel(AtlasName.Battle, spriteName, 5, new Vector2(0, 80));
                //m_notice_panel.play(rule.Type, 5);
            }
        }
    }

    string GetMvpName()
    {
        foreach (KeyValuePair<int, BasePlayer> kvp in WorldManager.singleton.allPlayer)
        {
            if (kvp.Value.isMVP)
            {
                return kvp.Value.PlayerName;
            }
        }
        return "";
    }

    private PanelHideWin.winData[] GetHideAlive()
    {
        PanelHideWin.winData[] data = new PanelHideWin.winData[5];
        int i = 0;
        foreach (BasePlayer player in WorldManager.singleton.allPlayer.Values)
        {
            if (player.serverData.alive)
            {
                if (player.serverData.camp == 4)
                {
                    PanelHideWin.winData win = new PanelHideWin.winData();
                    win.id = player.configData.ID;
                    win.name = player.serverData.name;
                    data[i] = win;
                    i++;
                }
            }
        }
        PanelHideWin.winData[] data2 = new PanelHideWin.winData[i];
        for (int j = 0; j < i; j++)
        {
            data2[j] = data[j];
        }
        return data2;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="run_time">-1设置清零，-2设置暂停</param>
    private void ResetBattleCD(float run_time = -1)
    {
        var rule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        if (rule != null)
        {
            if (rule.Time_play > 0)
            {
                var cd = run_time >= 0 ? rule.Time_play - run_time : (run_time == -2 ? -1 : 0);
                if (m_gun_route_panel != null)
                {
                    m_gun_route_panel.setCD(cd);
                }
                if (m_team_gun_route_panel != null)
                {
                    m_team_gun_route_panel.setCD(cd);
                }
            }
        }
    }

    void OnPlayerSwitchNextGun(string strWeaponID = "")
    {
        if (string.IsNullOrEmpty(strWeaponID) == true)
        {
            //bool canGetGrenade = WorldManager.singleton.isGrenadeInfinite;
            strWeaponID = MainPlayer.singleton.GetNextWeaponID(false);
        }
        if (string.IsNullOrEmpty(strWeaponID) == false)
            GameDispatcher.Dispatch(GameEvent.INPUT_SWITCH_WEAPON, strWeaponID);
    }

    private void OnDragPartnerCameraControll(GameObject target, PointerEventData eventdata)
    {
        if (SceneCameraController.singleton != null)
        {
            SceneCameraController.singleton.OnDrag(eventdata.delta.x, eventdata.delta.y);
        }
    }
    public RectTransform getRectTransform
    {
        get
        {
            return m_go.AddMissingComponent<RectTransform>();
        }
    }

    public void OnShootPointerDown(GameObject go, BaseEventData eventData)
    {
        if (m_bCover)
            return;
        if (isNewShootUI)
        {
            if (!IsShootDown)
            {
                Vector3 scale = m_goAim.transform.localScale;
                m_goAim.transform.localScale = scale / 1.2f;
            }
        }
        else
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
            {
                SetAimShootImg("AimPress1", "ShootPress1");
            }
        }
        IsShootDown = true;
        fire(true);
    }

    public void SetAimShootImg(string aimString, string ShootStr)
    {
        m_goAim.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, aimString));
        m_goShoot.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, ShootStr));
    }

    public void OnShootPointerUp(GameObject go, BaseEventData eventData)
    {
        if (isNewShootUI)
        {
            if (IsShootDown)
            {
                Vector3 scale = m_goAim.transform.localScale;
                m_goAim.transform.localScale = scale * 1.2f;
            }
        }
        else
        {
            //m_bShoot = false;
            if (MainPlayer.singleton != null && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
            {
                SetAimShootImg("Aim1", "Shoot1");
            }
        }
        IsShootDown = false;
        GameDispatcher.Dispatch(GameEvent.MAINPLAYER_STOP_AUTOFIRE);
        fire(false);
    }

    private void fire(bool keyDown)
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.isDmmModel)
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, keyDown);
            return;
        }
        if (MainPlayer.singleton != null && MainPlayer.singleton.curWeapon != null &&
            MainPlayer.singleton.curWeapon.weaponConfigLine!= null )
        {
            if (MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_KNIFE ||
                MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_PAW ||
                MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPON_TYPE_PAWGUN)
            {
                if (!isNormalKnifeMode())
                {
                    if (isHeavyKnifeModeOn)
                    {
                        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, keyDown);
                    }
                    else
                    {
                        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, keyDown);
                    }
                }
                else
                {
                    GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, keyDown);
                }
            }
            else
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, keyDown);
            }
        }
    }

    protected void OnPlayerBeHit(Vector3 attkPos)
    {
        float angle = CalcAttackAngle(attkPos);
        //Logger.Log("$$$$$$$$$$$$$$$$$$$$$ angle is " + angle);
        Vector2 localPos;
        CalcBeHitPos(angle, out localPos);

        GameObject goTexiao = new GameObject("behit");
        goTexiao.transform.SetParent(m_tran, false);
        goTexiao.AddMissingComponent<RectTransform>();
        goTexiao.AddMissingComponent<Image>();

        ResourceManager.LoadSprite(AtlasName.Battle, "be_hit", delegate(Sprite sp)
        {
            Image imgShoot = goTexiao.GetComponent<Image>();
            imgShoot.SetSprite(sp);
            RectTransform rectTrans = goTexiao.GetComponent<RectTransform>();
            rectTrans.anchoredPosition = localPos;
            float fSpriteWidth = sp.rect.width;
            float fSpriteHeight = sp.rect.height;
            Vector2 middle = new Vector2(0.5f, 0.5f);
            rectTrans.anchorMin = middle;
            rectTrans.anchorMax = middle;

            Vector2 sizeV = rectTrans.sizeDelta;
            sizeV.x = fSpriteWidth;
            sizeV.y = fSpriteHeight;
            rectTrans.sizeDelta = sizeV;
            //rectTrans.Rotate(0f, 0f, angle);
            rectTrans.localEulerAngles = new Vector3(0f, 0f, angle);
        }
        );

        BeHitObj hitObj = new BeHitObj(goTexiao, attkPos);

        //hitObj.m_fAngle = angle;
        m_lstBeHitObjs.Add(hitObj);
        Color newClr = m_redObj.m_imgRed.color;
        m_redObj.m_fTime = BE_HIT_TIME;
        newClr.a = 1f;
        m_redObj.m_imgRed.color = newClr;
    }

    static float CalcAttackAngle(Vector3 attkPos)
    {
        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null)
        {
            if (MainPlayer.singleton == null) return 0f;
            curPlayer = MainPlayer.singleton;
        }

        Vector3 dir1 = attkPos - curPlayer.position;
        dir1.y = 0;
        dir1.Normalize();
        Vector3 dir2 = curPlayer.transform != null ? curPlayer.transform.forward : Vector3.zero;
        dir2.y = 0;
        dir2.Normalize();
        float angle;
        if (Vector3.Cross(dir1, dir2).y > 0)
            angle = Vector3.Angle(dir1, dir2);
        else
            angle = 360 - Vector3.Angle(dir1, dir2);

        return angle;
    }
    static void CalcBeHitPos(float angle, out Vector2 anchorPos)
    {
        float radius = 194f * Screen.height / 640f;
        float x = Screen.width / 2 + radius * Mathf.Cos((angle + 90) * Mathf.PI / 180);
        float y = Screen.height / 2 + radius * Mathf.Sin((angle + 90) * Mathf.PI / 180);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_parentRectTransform, new Vector2(x, y), UIManager.m_uiCamera, out anchorPos);
        //Logger.Log("######### x: " + anchorPos.x.ToString() + " y: " + anchorPos.y.ToString());
    }

    protected void OnCover()
    {
        m_bCover = true;
        OnShootPointerUp(null, null);
    }

    protected void OnUnCover()
    {
        m_bCover = false;
    }

    protected void OnSettingUpdate()
    {
        m_goGrenade.TrySetActive(IsGrenadeVisible() && isShowUIExcPC);
        var enabled = (m_mode & BattleViewMode.MODE_WEAPON) > 0 && GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_WEAPON_SWITCH) == 1;
        if (m_goAutoSwitchGun.activeSelf != enabled && isShowUIExcPC)
        {
            m_goAutoSwitchGun.TrySetActive(enabled && isShowUIExcPC);
        }
        m_rtAim.anchoredPosition = m_kAimOriginPos;

        m_fAimSensity = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AIM);
        m_fAimAccTimes = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AIMACC) * 50;
        m_auto_play_audio = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_BATTLEAUTOAUDIO) == 1;
        if (m_auto_play_audio == false)
        {
            _audio_play_list.Clear();
        }
    }

    private void OnForbidSwitch(bool forbid)
    {
        m_goGunList.gameObject.TrySetActive(!forbid);
    }

    private void OnForbidMove(bool forbid)
    {
        m_goJoyback.TrySetActive(!forbid && isShowUIExcPC);
    }

    #region 武器第二攻击

    void OnSubFireUp(GameObject target, PointerEventData eventData)
    {
        if (isNormalKnifeMode())
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, false);
        }
        else
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.curWeapon != null &&
                MainPlayer.singleton.curWeapon.weaponConfigLine != null)
            {
                if (MainPlayer.singleton.curWeapon.weaponConfigLine.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1)
                {
                    GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, false);
                }
            }
        }
    }

    private bool isHeavyKnifeModeOn = false;
    void OnSubFireDown(GameObject target, PointerEventData eventData)
    {

        if( isNormalKnifeMode())
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, true);
        }
        else
        {
            if (MainPlayer.singleton != null && MainPlayer.singleton.curWeapon != null &&
                 MainPlayer.singleton.curWeapon.weaponConfigLine != null)
            {
                if (MainPlayer.singleton.curWeapon.weaponConfigLine.Class != WeaponManager.WEAPON_TYPE_KNIFE
                    && MainPlayer.singleton.curWeapon.weaponConfigLine.Class != WeaponManager.WEAPON_TYPE_PAW
                    && MainPlayer.singleton.curWeapon.weaponConfigLine.Class != WeaponManager.WEAPON_TYPE_PAWGUN)
                {
                    GameDispatcher.Dispatch(GameEvent.INPUT_FIRE_SUB, true);
                }
                else
                {
                    isHeavyKnifeModeOn = !isHeavyKnifeModeOn;
                    changeHeavyKnifeTypeIcon();
                }
            }
        }
    }

    private void changeHeavyKnifeTypeIcon()
    {
        if( isHeavyKnifeModeOn)
        {
            m_imgSubFire.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew,"heavyKnife"));
        }
        else
        {
            m_imgSubFire.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "big_knife_btn"));
        }
    }

    void ShowSubFireBtn(bool isShow)
    {
        if (m_goSubFire == null)
            return;
        m_goSubFire.TrySetActive(isShow && isShowUIExcPC);
    }

    #endregion

    #region 视角切换
    void OnClickSwitchView(GameObject target, PointerEventData eventData)
    {
        m_watchType = ++m_watchType % GameConst.WATCH_TYPE_TOTAL_COUNT;
        //Debug.LogError("m_watchType___________________" + m_watchType + "  MainPlayer.singleton = " + MainPlayer.singleton);
        if (m_watchType == GameConst.WATCH_TYPE_FIRST_PERSON_VIEW && WorldManager.singleton.isHideModeOpen)
        {
            BasePlayer player = WorldManager.singleton.GetPlayerById(ChangeTeamView.watchedPlayerID);
            if (player != null && !player.canFirstPersonWatch)
                m_watchType = GameConst.WATCH_TYPE_THIRD_PERSON_VIEW;
        }

        if (m_watchType == GameConst.WATCH_TYPE_THIRD_PERSON_VIEW)
        {
            if (FreeViewPlayer.singleton.isRuning)
                FreeViewPlayer.singleton.Stop();
            GameDispatcher.Dispatch<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, false);
            GameDispatcher.Dispatch(GameEvent.UI_SWITCH_PERSON_VIEW);
        }
        else if (m_watchType == GameConst.WATCH_TYPE_FIRST_PERSON_VIEW)
        {
            if (FreeViewPlayer.singleton.isRuning)
                FreeViewPlayer.singleton.Stop();
            GameDispatcher.Dispatch<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, true);
            if (PlayerBattleModel.Instance.battle_info.spectator_switch == false )
            {
                m_watchType = GameConst.WATCH_TYPE_GOD_VIEW;
            }
        }
        else if (m_watchType == GameConst.WATCH_TYPE_GOD_VIEW)
        {
            GameDispatcher.Dispatch<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, false);
            FreeViewPlayer.singleton.Run();
            OnSwitchFreeView();
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, ChangeTeamView.watchedPlayerID);
            //if(MainPlayer.singleton!= null && MainPlayer.singleton.isDmmModel == true &&WorldManager.singleton.gameRuleLine.WatchFreeViewMode == 1  )
            //{
            //    if (FreeViewPlayer.singleton != null)
            //    {
            //        FreeViewPlayer.singleton.Stop();
            //        GameDispatcher.Dispatch<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, false);
            //    }
            //}
        }
        if (PlayerBattleModel.Instance.battle_info.spectator_switch == true)
        {
            dealESGRankPanel();
        }        
    }

    private void dealESGRankPanel()
    {
        var panel = UIManager.GetPanel<PanelESGkillRank>();
        if (m_watchType == GameConst.WATCH_TYPE_FIRST_PERSON_VIEW )
        {
            if (panel != null && panel.IsOpen() == true)
                panel.HidePanel();
        }
        else
        {
            if (panel == null || panel.IsOpen() == false)
            {
                UIManager.PopPanel<PanelESGkillRank>();
            }  
        }
    }

    void ShowSwitchViewBtn(bool isShow)
    {
        if (m_goSwitchView == null)
            return;
        m_goSwitchView.TrySetActive(isShow);
    }

    void EnableSwitchBtn(bool isEnable)
    {
        if (m_goSwitchView == null)
            return;
        
        m_btnSwitchView.enabled = isEnable;
        m_chSwitchView.enabled = isEnable;
    }

    #endregion

    #region 武器技能
    private void onWeaponSkillItemClick(GameObject target, PointerEventData eventData)
    {
        var render = m_skillGo2SkillRender[target];
        if (!MainPlayer.singleton.ForbidUseSkill && render != null)
        {
            render.UseSkill();
        }
    }

    private void UseWeaponSkill(int index)
    {
        if (!m_posSkillItemRenders.ContainsKey(index) || !m_posSkillItemRenders[index].isShow)
        {
            return;
        }
        onWeaponSkillItemClick(m_skillGos[index], null);
    }

    private void OnUpdateWeaponSkill(int awakenWeaponId)
    {
        int prevAwakenWeaponId = m_awakenWeaponId;
        m_awakenWeaponId = awakenWeaponId;
        m_isShowWeaponSkill = false;

        int awakenSkillId = 0;
        int skillPos = 1;
        if (awakenWeaponId == -1)
        {
            OnHideAwakenSkill();
            return;
        }
        TDWeaponAwakenProp awakenConfig = ConfigManager.GetConfig<ConfigWeaponAwakenProp>().GetLine(m_awakenWeaponId);
        string[] param = awakenConfig.Param.Split(',');

        if (param.Length > 1)
        {
            int.TryParse(param[0], out awakenSkillId);
            int.TryParse(param[1], out skillPos);
        }
        else
        {
            int.TryParse(param[0], out awakenSkillId);
        }
        int skillPosIndex = skillPos - 1;
        if (m_awakenSkillRender == null)
        {
            m_awakenSkillRender = CreateAwakenSkillItem(m_weaponSkillTran, m_skillBtnPosArr[skillPosIndex]);
            m_goAwakenSkill = m_awakenSkillRender.gameObject;
            m_skillGo2SkillRender[m_goAwakenSkill] = m_awakenSkillRender;
            m_awakenSkillRender.frozen = false;
        }

        bool showSkill = isShowSkill(awakenSkillId);

        if (showSkill)
        {
            // hide normal skill btn
            if (m_posSkillItemRenders.ContainsKey(skillPosIndex))
            {
                SkillItemRender skillItem = m_posSkillItemRenders[skillPosIndex];
                skillItem.isShow = false;
                skillItem.gameObject.TrySetActive(false);
                
            }
            m_posSkillItemRenders[skillPosIndex] = m_awakenSkillRender;
            m_skillGos[skillPosIndex] = m_goAwakenSkill;
            m_awakenSkillRender.SetData(awakenConfig);
            m_goAwakenSkill.TrySetActive(true);
            m_awakenSkillRender.isShow = true;
            m_isShowWeaponSkill = true;
        }
        else
        {
            OnHideAwakenSkill();
        }
    }


    private void OnHideAwakenSkill()
    {
        // cancel prev skill btn
        if (m_awakenSkillRender != null)
        {
            m_goAwakenSkill.TrySetActive(false);
            m_awakenSkillRender.isShow = false;
            // show backup btn
            for (int i = 0; i < m_skillIdList.Count; i++)
            {
                if (m_skillIdList[i] != 0 && m_posSkillItemRenders[i] == m_awakenSkillRender)
                {
                    SkillItemRender skillItem = m_skillItemPoolList[i];
                    m_posSkillItemRenders[i] = skillItem;
                    m_skillGos[i] = skillItem.gameObject;
                    skillItem.isShow = true;
                    skillItem.gameObject.TrySetActive(true);
                }
            }
        }
    }

    private bool isShowSkill(int skillId)
    {
        bool isShowSkill = false;
        if (skillId != 0)
        {
            var skillConfig = ConfigManager.GetConfig<ConfigSkill>().GetLine(skillId);


            //检查是否不受限房间
            for (int i = 0; i < skillConfig.UnlimitedChannel.Length; ++i)
                if (skillConfig.UnlimitedChannel[i] == RoomModel.m_channel)
                {
                    isShowSkill = true;
                    break;
                }

            //如果是受限房间检查是否在可用游戏模式里
            if (!isShowSkill)
            {
                if (skillConfig.UsableGameType == null || skillConfig.UsableGameType.Length == 0 ||
                    (skillConfig.UsableGameType.Length == 1 && string.IsNullOrEmpty(skillConfig.UsableGameType[0])))
                    isShowSkill = true;
                else
                {
                    for (int i = 0; i < skillConfig.UsableGameType.Length; ++i)
                        if (skillConfig.UsableGameType[i] == WorldManager.singleton.gameRuleLine.GameType)
                        {
                            isShowSkill = true;
                            break;
                        }
                }
            }
        }
        return isShowSkill;
    }


    void ShowWeaponSkill(bool isShow)
    {
        if (m_awakenSkillRender == null || !m_isShowWeaponSkill)
        {
            return;
        }
        m_awakenSkillRender.isShow = (isShow);
    }

    void OnRefreshWeaponSkill(bool isFrozen)
    {
        if (m_awakenSkillRender == null)
        {
            return;
        }
        OnUpdateWeaponSkill(m_awakenWeaponId);
        if (m_awakenSkillRender.cdcleartype == GameConst.CDCLEARTYPE_ROUNDEND)
            m_awakenSkillRender.frozen = isFrozen;
    }

    #endregion

    #region 被动技能列表
    public GameObject passiveSkill
    {
        get { return m_goPassiveSkill; }
    }

    public GameObject passiveSkillText
    {
        get { return m_goPassiveSkillText; }
    }
    #endregion

    private void UpdateJoyAndAim()
    {

    }

    private bool IsInJoyArea(Vector2 pos)
    {
        if (pos.x >= Screen.width / 2)
            return false;

        return true;
    }

    private bool IsInShootArea(Vector2 pos)
    {
        const float minD = 10;
        Rect pr = m_parentRectTransform.rect;
        pos.x -= pr.xMin;
        pos.y -= pr.yMin;
        if (pos.x < minD || pos.x > pr.width - minD || pos.y < minD || pos.y > pr.height - minD)
            return false;
        foreach (Transform tran in m_tran)
        {
            if (tran == null || tran.name == "Aim" || !tran.gameObject.activeSelf)
                continue;
            if (tran.GetComponent<Selectable>() == null && tran.GetComponent<UGUIClickHandler>() == null)
                continue;
            RectTransform rect = tran.GetComponent<RectTransform>();
            if (rect == null)
                continue;
            if (rect.anchorMin != rect.anchorMax)
                continue;
            Rect r = rect.rect;
            float left = pr.width * rect.anchorMin.x + rect.anchoredPosition.x + r.xMin;
            float bottom = pr.height * rect.anchorMin.y + rect.anchoredPosition.y + r.yMin;
            if (pos.x > left - minD && pos.x < left + r.width + minD && pos.y > bottom - minD && pos.y < bottom + r.height + minD)
                return false;
        }

        return true;
    }

    void SetJoyPos(Vector2 loaclPos, bool bReset = false)
    {
        //Logger.Error("pos: "+loaclPos);
        if (bReset)
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_MOVE_SLOW, false);
            m_goJoystick.GetComponent<RectTransform>().anchoredPosition = m_kOrigStickPos;
            m_moveDir = Vector2.zero;
            ShowJoyCrossAndTriangle(false);
            return;
        }

        ShowJoyCrossAndTriangle(true);

        float fX = loaclPos.x;
        float fY = loaclPos.y;
        float fDisPow = (Mathf.Pow(fX - m_kOrigStickPos.x, 2) + Mathf.Pow(fY - m_kOrigStickPos.y, 2));
        Vector2 newPos;
        if (fDisPow < m_fMaxJoyDisPow)
        {
            newPos = new Vector2(m_kOrigStickPos.x + fX - m_kOrigStickPos.x, m_kOrigStickPos.y + fY - m_kOrigStickPos.y);
            m_goJoystick.GetComponent<RectTransform>().anchoredPosition = newPos;
            if (fDisPow < m_fSlowJoyDisPow)
            {
                //慢走
                //Debug.LogError("慢走");
                GameDispatcher.Dispatch(GameEvent.INPUT_MOVE_SLOW, true);
            }
            else
            {
                //正常行走
                GameDispatcher.Dispatch(GameEvent.INPUT_MOVE_SLOW, false);
            }
        }
        else
        {
            //正常行走
            GameDispatcher.Dispatch(GameEvent.INPUT_MOVE_SLOW, false);
            float fDelta = Mathf.Atan2(fY - m_kOrigStickPos.y, fX - m_kOrigStickPos.x);
            newPos = new Vector3(m_kOrigStickPos.x + Mathf.Cos(fDelta) * m_fMaxJoyDis, m_kOrigStickPos.y + Mathf.Sin(fDelta) * m_fMaxJoyDis);
            m_goJoystick.GetComponent<RectTransform>().anchoredPosition = newPos;
        }

        float fDisSqrt = Mathf.Sqrt((Mathf.Pow(newPos.x - m_kOrigStickPos.x, 2) + Mathf.Pow(newPos.y - m_kOrigStickPos.y, 2)));
        m_moveDir.x = (newPos.x - m_kOrigStickPos.x) / fDisSqrt;
        m_moveDir.z = (newPos.y - m_kOrigStickPos.y) / fDisSqrt;

        if (isNewShootUI)
        {
            float angle = Vector2.Angle(new Vector2(1, 0), loaclPos);
            angle = loaclPos.y > 0 ? angle : 360 - angle;
            m_goJoystickTriangle.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, angle - 45);
        }
        
        // Logger.Error("angle: "+angle);
    }

    private void ShowJoyCrossAndTriangle(bool isShow)
    {
        if (isNewShootUI)
        {
            m_goJoystickCross.TrySetActive(isShow);
            m_goJoystickTriangle.TrySetActive(isShow);
        }
    }

    public Vector3 GetMoveDir()
    {
        return new Vector3(m_moveDir.x, m_moveDir.y, m_moveDir.z);
    }

    /// <summary>
    /// 是否正在操作瞄准或移动
    /// </summary>
    /// <returns></returns>
    public bool IsOperation()
    {
        var result = m_iAimFingerId != -1 || m_iJoyFingerId != -1 || MainPlayer.singleton.status.move;
#if UNITY_EDITOR
        result |= Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0;
#endif
        return result;
    }

    public bool CanShoot(Vector2 curPos)
    {
        Vector2 pos = m_rtAim.anchoredPosition;
        return (curPos - pos).sqrMagnitude < m_fMaxShootDisPow;
    }

    bool IsGrenadeVisible()
    {
        if ((m_mode & BattleViewMode.MODE_WEAPON) == 0)
            return false;
        if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_GRENADE) == 0)
            return false;
        // 新手关
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GUIDE_LEVEL))
            return false;
        // 枪王
        if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_KING_SOLO || WorldManager.singleton.gameRule == GameConst.GAME_RULE_KING_CAMP)
            return false;
        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData.hasGrenade == false)
            return false;
        return true;
    }

    bool IsFlashBombVisible()
    {
        if ((m_mode & BattleViewMode.MODE_WEAPON) == 0)
            return false;
        // 新手关
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GUIDE_LEVEL))
            return false;
        // 枪王
        if (WorldManager.singleton.gameRule == GameConst.GAME_RULE_KING_SOLO || WorldManager.singleton.gameRule == GameConst.GAME_RULE_KING_CAMP)
            return false;
        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData.hasFlashBomb == false)
            return false;
        return true;
    }

    void UpdateBeHit()
    {
        m_redObj.m_fTime = m_redObj.m_fTime - Time.deltaTime;
        if (m_redObj.m_fTime < 0f)
        {
            m_redObj.m_fTime = 0f;
            Color newClr = m_redObj.m_imgRed.color;
            newClr.a = 0f;
            m_redObj.m_imgRed.color = newClr;
        }
        else
        {
            Color newClr = m_redObj.m_imgRed.color;
            newClr.a = m_redObj.m_fTime / BE_HIT_TIME;
            m_redObj.m_imgRed.color = newClr;
        }

        if (m_lstBeHitObjs.Count > 0)
        {
            for (int i = m_lstBeHitObjs.Count - 1; i >= 0; i--)
            {
                BeHitObj bHit = m_lstBeHitObjs[i];
                if (!bHit.Active())
                {
                    bHit.Destory();
                    m_lstBeHitObjs.RemoveAt(i);
                }
            }
        }
    }

    void OnSwitchFreeView()
    {
        if (m_goJoyback.activeSelf != true)
        {
            m_goJoyback.TrySetActive(true && isShowUIExcPC);
        }
    }

    void OnSwitchPersonView()
    {
        if (m_goJoyback.activeSelf != false)
        {
            m_goJoyback.TrySetActive(false);
        }
    }

    void ResetWatchType()
    {
        m_watchType = 0;
    }

    void Toc_fight_praise(toc_fight_praise data)
    {
        string name;
        if (data.from == MainPlayer.singleton.PlayerId)
        {
            name = WorldManager.singleton.GetPlayerByPid(data.pid).serverData.name;
            TipsManager.Instance.showTips("你赞了" + name);
        }
        else if (data.pid == MainPlayer.singleton.serverData.pid)
        {
            name = WorldManager.singleton.GetPlayerById(data.from).serverData.name;
            TipsManager.Instance.showTips(name + "赞了你");
        }
        else
        {
            string name1 = WorldManager.singleton.GetPlayerById(data.from).serverData.name;
            name = WorldManager.singleton.GetPlayerByPid(data.pid).serverData.name;
            TipsManager.Instance.showTips(name1 + "赞了" + name);
        }
        UpdatePraise();
    }

    void UpdatePraise()
    {
        m_gjobnum.text = WorldManager.singleton.GetPraise(m_iSelectedPlayerID).ToString();
    }

    public void updateHideState(bool bo)
    {
        if (WorldManager.singleton.gameRule != GameConst.GAME_RULE_HIDE)
        {
            return;
        }
        m_rollershow = !bo;
        m_bagview.TrySetActive(bo);
        m_goAim.TrySetActive(bo && isShowUIExcPC);
        m_goJoyback.TrySetActive(bo && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
        m_Jump.TrySetActive(bo && isShowUIExcPC);
        m_switch.TrySetActive(bo);
        m_bag.gameObject.TrySetActive(false);
        m_bagview.gameObject.TrySetActive(false);
        m_goLeftJump.TrySetActive(bo && isShowUIExcPC);
        m_Squat.TrySetActive(bo && isShowUIExcPC);
        if (MainPlayer.singleton != null)
        {
            if (MainPlayer.singleton.IsHideView || MainPlayer.singleton.isDead)
            {
                m_goAim.TrySetActive(false);
                if (FreeViewPlayer.singleton == null || !FreeViewPlayer.singleton.isRuning)
                    m_goJoyback.TrySetActive(false);
                else
                    m_goJoyback.TrySetActive(true && isShowUIExcPC && (m_joystickMoveType == GameConst.JOYSTICK_MOVE1));
                m_goLeftJump.TrySetActive(false);
                m_Jump.TrySetActive(false);
            }
            if (bo)
            {
                if (MainPlayer.singleton.Camp == 4 && !MainPlayer.singleton.isDead)
                {
                    if( MainPlayer.singleton.IsFrezee)
                    {
                        m_switch.TrySetActive(true);
                    }else
                    {
                        m_switch.TrySetActive(false);
                    }
                }
                else
                {
                    m_switch.TrySetActive(false);
                }
            }
            if (MainPlayer.singleton.isDead || MainPlayer.singleton.IsHideView)
            {
                m_Squat.TrySetActive(false);
                if((WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.CAT))
                    SetCrossHairVisible(false);
                else
                {
                    if (watchType == GameConst.WATCH_TYPE_FIRST_PERSON_VIEW)
                        SetCrossHairVisible(true);
                }
                    
            }
            else
            {
                m_Squat.TrySetActive(true && isShowUIExcPC);
                if((WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.Camp != (int)WorldManager.CAMP_DEFINE.CAT) && FreeViewPlayer.singleton!=null && !FreeViewPlayer.singleton.isRuning)
                    SetCrossHairVisible(true);
                else
                    SetCrossHairVisible(false);
            }
        }

        if ((JUMP_BUTTON_DEFINE)GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_JUMP) != JUMP_BUTTON_DEFINE.DOUBLEB)
        {
            m_goLeftJump.TrySetActive(false);
        }
    }

    void Toc_fight_round_best(toc_fight_round_best data)
    {
        string t = "";
        var config = ConfigManager.GetConfig<ConfigTitle>();
        for (int i = 0; i < data.round_bests.Length; i++)
        {

            var title = config.GetLine(data.round_bests[i].title);
            string str = (title == null ? data.round_bests[i].title : title.Name) + ":" + data.round_bests[i].name + "  +" + data.round_bests[i].score;
            t += str + "\n";
        }
        m_paneltitle.gameObject.TrySetActive(true);
        m_title.text = t;
    }

    void BowCrossDeal()
    {
        if (MainPlayer.singleton != null&&MainPlayer.singleton.curWeapon!=null)
        {
            if (powerOn && bowCircle.transform.localScale.x >= 0 && MainPlayer.singleton.powerOnStartTime != 0)
            {
                if (MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPOM_TYPE_BOW)
                {
                    float time = Time.timeSinceLevelLoad - MainPlayer.singleton.powerOnStartTime;
                    float a = 1f / (MainPlayer.singleton.curWeapon.weaponConfigLine.MaxPowerOnTime) * time;
                    tranbowCircle.localScale = new Vector3(1f, 1f, 1) - new Vector3(a, a, 0);
                    if (tranbowCircle.localScale.x <= 0)
                    {
                        tranbowCircle.localScale = new Vector3(0, 0, 1);
                    }
                    bowCircle.TrySetActive(true);
                }
            }
            else if (!powerOn || MainPlayer.singleton.curWeapon.weaponConfigLine.Class == WeaponManager.WEAPOM_TYPE_BOW)
            {
                tranbowCircle.localScale = Vector3.one;
                bowCircle.TrySetActive(false);
            }
        }
        else
        {
            tranbowCircle.localScale = Vector3.one;
            bowCircle.TrySetActive(false);
        }
    }

    void UpdateJetPack()
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.canFly)
        {
            //JetRed.fillAmount = MainPlayer.singleton.skillMgr.flm.nowPower / MainPlayer.singleton.skillMgr.flm.maxPower;
            JetWhite.fillAmount = MainPlayer.singleton.skillMgr.flm.nowPower / MainPlayer.singleton.skillMgr.flm.maxPower;
            if (jetfadein)
            {
                if (JetRed.color.a < 1)
                {
                    JetRed.color = JetRed.color + new Color(0, 0, 0, 0.1f);
                }
                if (JetBg.color.a < 1)
                {
                    JetBg.color = JetBg.color + new Color(0, 0, 0, 0.1f);
                }
                if (JetWhite.color.a < 1)
                {
                    JetWhite.color = JetWhite.color + new Color(0, 0, 0, 0.1f);
                }
            }
            if (jetfadeout)
            {
                if (JetRed.color.a >0)
                {
                    JetRed.color = JetRed.color - new Color(0, 0, 0, 0.01f);
                }
                if (JetBg.color.a >0)
                {
                    JetBg.color = JetBg.color - new Color(0, 0, 0, 0.01f);
                }
                if (JetWhite.color.a >0 )
                {
                    JetWhite.color = JetWhite.color - new Color(0, 0, 0, 0.01f);
                }
            }
        }
    }

    #region Proto Function

    static void Toc_fight_attack_failed(toc_fight_attack_failed proto)
    {
        if (MainPlayer.singleton == null)
            return;

        if (MainPlayer.singleton.PlayerId == proto.from_id && MainPlayer.singleton.isZombie)
        {
            GameDispatcher.Dispatch(GameEvent.UI_GANRANSHIBAI);
            Logger.Warning("攻击失效");

        }
        else if (MainPlayer.singleton.PlayerId == proto.actor_id && MainPlayer.singleton.isHuman)
        {
            TipsManager.Instance.showTips("生化服已破损，成功抵挡了一次感染");
            Logger.Warning("护盾消失");
        }
    }

    void Toc_player_notice(toc_player_notice data)
    {
        if (data.type == 4)
        {
            UIManager.ShowTv(data.msg);
        }
    }

    #endregion
}
