﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChooseHeroRender : MonoBehaviour
{
    GameObject m_chooseEffect;
    Image m_image;
    GameObject m_choosed;
    GameObject m_notChoosed;
    void Awake()
    {
        m_chooseEffect = transform.Find("chooseEffect").gameObject;
        m_image = transform.Find("image").GetComponent<Image>();
        m_choosed = transform.Find("choosed").gameObject;
        m_notChoosed = transform.Find("notchoosed").gameObject;
    }

    public void UpdateView(bool choosed, string image)
    {
        if (!string.IsNullOrEmpty(image))
        {
            Sprite sprite = ResourceManager.LoadSprite(AtlasName.ChooseHero, image);
            m_image.SetSprite(sprite);
            m_image.SetNativeSize();
        }
        m_chooseEffect.TrySetActive(choosed);
        m_choosed.TrySetActive(choosed);
        m_notChoosed.TrySetActive(!choosed);
    }
}

public class PanelChooseHero : BasePanel
{
    bool m_male = true;
    ChooseHeroRender m_right;
    ChooseHeroRender m_left;
    bool m_isChooseing = false;
    float m_startTime; //选择开始时间
    const float CHOOSE_LIMIT = 15f; //选择时间限制
    const int YUANCHENG_NAN_NAME_WEAPON_ID = 1911;
    const int YUANCHENG_NV_NAME_WEAPON_ID = 1912;
    public static int[] heroIDArray;

    //PC版快捷键操作
    private GameObject m_pcKey;

    public PanelChooseHero()
    {
        SetPanelPrefabPath("UI/Battle/PanelChooseHero");
        AddPreLoadRes("UI/Battle/PanelChooseHero", EResType.UI);
        //AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/ChooseHero", EResType.Atlas);
    }

    public override void Init()
    {
        m_right = m_tran.Find("right").gameObject.AddComponent<ChooseHeroRender>();
        m_left = m_tran.Find("left").gameObject.AddComponent<ChooseHeroRender>();
        m_pcKey = m_tran.Find("PC_key").gameObject;
        m_pcKey.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_right.gameObject).onPointerClick += OnRightClick;
        UGUIClickHandler.Get(m_left.gameObject).onPointerClick += OnLeftClick;
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_ROUND_OVER, Over);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_OVER, Over);
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            AddEventListener<int>(GameEvent.INPUT_KEY_NUM_DOWN, (num) =>
            {
                if (num == 1)
                {
                    OnLeftClick(null, null);
                }
                else if (num == 2)
                {
                    OnRightClick(null, null);
                }
            });
            //AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () => { OnJinZhanClick(null, null); });
            //AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () => { OnYuanChengClick(null, null); });
        }
    }

    private void Over()
    {
        if (UIManager.IsOpen<PanelChooseHero>())
        {
            HidePanel();
        }
    }

    private void OnLeftClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        m_isChooseing = false;
        SetHero(m_left, heroIDArray[0], true);
        SetHero(m_right, heroIDArray[1], false);
        HidePanel();
        
    }

    private void OnRightClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        SetHero(m_left, heroIDArray[0], false);
        SetHero(m_right, heroIDArray[1], true);
        NetLayer.Send(new tos_fight_select_role() { index = 2 });
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {
        if (MainPlayer.singleton.IsFrezee)
        {
            MainPlayer.singleton.SetFreeze(false);
        }
        //if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        // Screen.lockCursor = true;
    }

    public override void OnShow()
    {
        RefreshView();
        
        m_startTime = Time.realtimeSinceStartup;
        m_isChooseing = true;
        if (!MainPlayer.singleton.IsFrezee)
            MainPlayer.singleton.SetFreeze(true);
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
            m_pcKey.TrySetActive(true);

    }

    public override void Update()
    {
        if (m_isChooseing)
        {
            if ((Time.realtimeSinceStartup - m_startTime) > CHOOSE_LIMIT)
            {
                m_isChooseing = false;
                HidePanel();
            }
        }
    }

    private void RefreshView()
    {
        if (heroIDArray == null || heroIDArray.Length != 2)
            return;

        SetHero(m_left, heroIDArray[0], true);
        SetHero(m_right, heroIDArray[1], false);
    }

    private void SetHero(ChooseHeroRender hero, int roleId, bool isChoose)
    {
        ConfigItemRoleLine heroLine = ConfigManager.GetConfig<ConfigItemRole>().GetLine(roleId);
        if (heroLine == null)
            return;
        hero.UpdateView(isChoose, heroLine.ChooseImage);
    }

    private void Toc_fight_select_role(toc_fight_select_role toc)
    {
        HidePanel();
        //Logger.Log("recv:" + toc.index);
    }

}
