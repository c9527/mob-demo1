﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class BattleTeamGunRoutePanel : MonoBehaviour
{
    ConfigGameRuleLine _rule;
    Dictionary<int,string[]> _weapon_type_config;
    float _end_time = 0;
    float _last_time = 0;

    Image _team_0_value;
    Image _team_1_value;
    Image _weapon_img;
    Text _cd_txt;
    Text _step_txt;
    UIImgNumText _reward_txt;
    UIImgNumText _kill_txt_0;
    UIImgNumText _kill_txt_1;
    UIImgNumText _score_txt_0;
    UIImgNumText _score_txt_1;

    void Awake()
    {
        _rule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(GameConst.GAME_RULE_KING_CAMP);
        
        _weapon_type_config = new Dictionary<int, string[]>();
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_PISTOL, new string[] { "手枪", "usp" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_RIFLE, new string[] { "步枪", "m4a1" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_SUBMACHINE_GUN, new string[] { "冲锋枪", "mp5" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_SHOT_GUN, new string[] { "散弹枪", "spas12" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_MACHINE_GUN, new string[] { "机枪", "m249" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_SNIPER_GUN, new string[] { "狙击枪", "awp" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_KNIFE, new string[] { "雷/近战", "grenade_melee" });
        _weapon_type_config.Add(WeaponManager.WEAPON_TYPE_GRENADE, new string[] { "手雷", "grenade" });

        _team_0_value = gameObject.transform.Find("team_0_value").GetComponent<Image>();
        _team_1_value = gameObject.transform.Find("team_1_value").GetComponent<Image>();
        _weapon_img = gameObject.transform.Find("weapon_img").GetComponent<Image>();
        _cd_txt = gameObject.transform.Find("cd_txt").GetComponent<Text>();
        _step_txt = gameObject.transform.Find("step_txt").GetComponent<Text>();

        _reward_txt = new UIImgNumText(transform,"time", AtlasName.Battle, 0, -8);
        ((RectTransform)_reward_txt.transform).anchoredPosition = new Vector2(0,-20);
        _kill_txt_0 = new UIImgNumText(transform, "mode", AtlasName.Battle, 2, -6);
        ((RectTransform)_kill_txt_0.transform).anchoredPosition = new Vector2(-70, -11);
        _kill_txt_1 = new UIImgNumText(transform, "mode", AtlasName.Battle, 1, -6);
        ((RectTransform)_kill_txt_1.transform).anchoredPosition = new Vector2(70, -11);
        _score_txt_0 = new UIImgNumText(transform, "time", AtlasName.Battle, 2, -6);
        ((RectTransform)_score_txt_0.transform).anchoredPosition = new Vector2(-245, -45);
        _score_txt_1 = new UIImgNumText(transform, "time", AtlasName.Battle, 1, -6);
        ((RectTransform)_score_txt_1.transform).anchoredPosition = new Vector2(245, -45);
    }

    public void setCD(float value)
    {
        _end_time = value < 0 ? -1 : Time.realtimeSinceStartup + value;
        _last_time = 0;
    }

    public void updateData()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        if (_rule == null || data==null)
        {
            return;
        }
        if (data.camps != null && data.camps.Count >= 2 && data.camps[0].players.Count > 0)
        {
            var weapon_seq = data.camps[0].players[0].king_weapon_seq;
            _kill_txt_0.value = data.camps[0].king_camp_cur_kill;
            _kill_txt_1.value = data.camps[1].king_camp_cur_kill;
            _score_txt_0.value = data.camps[0].king_camp_score;
            _score_txt_1.value = data.camps[1].king_camp_score;
            var round_rule = ConfigManager.GetConfig<ConfigKing>().GetLine(weapon_seq);
            if (round_rule != null)
            {
                _reward_txt.value = round_rule.CampScore;
                _team_0_value.fillAmount = (float)data.camps[0].king_camp_cur_kill / round_rule.CampNeedKill;
                _team_1_value.fillAmount = (float)data.camps[1].king_camp_cur_kill / round_rule.CampNeedKill;
            }
            else
            {
                _reward_txt.value = 0;
                _team_0_value.fillAmount = 0;
                _team_1_value.fillAmount = 0;
            }
            string[] type_info = null;
            _weapon_type_config.TryGetValue(round_rule!=null ? round_rule.WeaponTypeInt : 1, out type_info);
            if (type_info != null && type_info.Length>0)
            {
                _step_txt.text = "武器限制：" + type_info[0];
                _weapon_img.enabled = true;
                _weapon_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Weapon, type_info[1]));
            }
            else
            {
                _step_txt.text = "武器限制：-";
                _weapon_img.SetSprite(null);
                _weapon_img.enabled = false;
            }
        }
    }

    void Update()
    {
        if (_rule == null || _end_time == -1)
        {
            return;
        }
        if ((Time.realtimeSinceStartup - _last_time) >= 1)
        {
            _last_time = Time.realtimeSinceStartup;
            _cd_txt.text = _end_time > 0 ? TimeUtil.FormatTime((int)(_end_time - Time.realtimeSinceStartup)) : "00:00";
            if (_last_time >= _end_time)
            {
                _end_time = -1;
                _last_time = 0;
            }
        }
    }
}

