﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class SkillItemRender : ItemRender
{
    protected Image _avatar_img;
    protected Image _cd_mask;

    protected float _last_time = -1;
    protected float _cd = 0;
    protected int _id = -1;
    protected bool _isSetData;
    protected int _cdcleartype;
    protected bool _reverse;
    protected string _cd_mask_img = "skill_cd_mask";

    private bool m_lastFrozen = true;

    public override void Awake()
    {
        _avatar_img = gameObject.AddMissingComponent<Image>();

        _cd_mask = new GameObject("cd_mask").AddComponent<Image>();
        _cd_mask.transform.SetParent(transform);
        _cd_mask.transform.localScale = Vector3.one;
        _cd_mask.transform.localPosition = Vector3.zero;
        _cd_mask.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, _cd_mask_img));
        _cd_mask.SetNativeSize();
        _cd_mask.type = Image.Type.Filled;
        _cd_mask.fillMethod = Image.FillMethod.Radial360;
        _cd_mask.fillOrigin = (int)Image.Origin360.Top;
        _cd_mask.fillClockwise = false;
        _cd_mask.fillAmount = 0;
    }

    public void SetPos(Vector3 pos)
    {
        transform.localPosition = pos;
    }

    public virtual void UseSkill()
    {
        if (isSetData && !frozen)
        {
            PlayerBattleModel.Instance.TosUseSkill(skillId);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            GameDispatcher.Dispatch<bool>(PanelGuidePC.EVENT_STEP_USESKILL, false);
#endif
        }
    }

    protected void Update()
    {

        if (frozen == false)
        {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            if (m_lastFrozen && m_renderData!=null)
            {
                m_lastFrozen = true;
                GameDispatcher.Dispatch<bool>(PanelGuidePC.EVENT_STEP_USESKILL, true);
            }
#endif
            _cd_mask.fillAmount = _reverse ? 1f : 0;
            return;
        }
        m_lastFrozen = frozen;
        if (_reverse)
            _cd_mask.fillAmount = (Time.realtimeSinceStartup - _last_time) / _cd;
        else
            _cd_mask.fillAmount = 1 - (Time.realtimeSinceStartup - _last_time) / _cd;
    }

    protected override void OnSetData(object data)
    {
        if (m_renderData == null)
        {
            return;
        }

        var info = m_renderData as TDSKillInfo;

        if (info == null)
        {
            Logger.Error("Data is null");
            return;
        }

        _isSetData = true;

        if (_last_time == -1)
        {
            frozen = true;
        }

        SetKillInfo(info.ID, info);
    }

    protected void SetKillInfo(int id, TDSKillInfo info)
    {
        _id = id;
        _cd = info.CD + 0.5f;
        _cdcleartype = info.CDClearType;

        _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, info.ICON));
        _avatar_img.SetNativeSize();

        if (!string.IsNullOrEmpty(info.Mask))
        {
            _cd_mask.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, info.Mask));
            _cd_mask.SetNativeSize();
        }

        _cd_mask.rectTransform.anchoredPosition = info.MaskOffset;
    }

    public bool frozen
    {
        get { return m_renderData != null ? _last_time > 0 && (Time.realtimeSinceStartup - _last_time) < _cd : false; }
        set { _last_time = value ? Time.realtimeSinceStartup : 0; }
    }

    public bool isShow
    {
        set
        {
            _avatar_img.enabled = value;
            _cd_mask.enabled = value;
        }
        get
        {
            return _avatar_img.enabled;
        }
    }

    public int skillId
    {
        get { return _id; }
    }

    public bool isSetData
    {
        get { return _isSetData; }
    }

    public int cdcleartype
    {
        get { return _cdcleartype; }
    }

    public float cd
    {
        get { return _cd; }
        set { _cd = value; }
    }

    public float leftTime
    {
        get { return _cd - (Time.realtimeSinceStartup - _last_time); }
    }
}

