﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class PanelHideMsg :BasePanel
{
    Image m_img;
    GameObject m_bg;
    float m_time;
    float m_starttime;

    public PanelHideMsg()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelHideMsg");
        AddPreLoadRes("UI/Battle/PanelHideMsg", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }
    
    public override void Init()
    {
        m_img = m_tran.Find("txt").GetComponent<Image>();
        m_bg = m_tran.Find("bg").gameObject;
        m_time = 0;
        m_starttime = -1;
        m_img.gameObject.TrySetActive(false);
        m_bg.TrySetActive(false);
    
    }

    public override void InitEvent()
    {

    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public void updateview(int state, int time)
    {
        m_time = (float)time;
        m_starttime = Time.realtimeSinceStartup;
        if (state == 0)
        {
            m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "firelock"));
            m_img.SetNativeSize();
            m_img.gameObject.TrySetActive(true);
            m_bg.TrySetActive(true);
        }
        if (state == 1)
        {
            AudioManager.PlayUISound(AudioConst.hideLockUnlock, false);
            m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "locked"));
            m_img.SetNativeSize();
            m_bg.TrySetActive(true);
            m_img.gameObject.TrySetActive(true);
        }
        if (state == 2)
        {
            AudioManager.PlayUISound(AudioConst.hideLockUnlock, false);
            m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "unlock"));
            m_img.SetNativeSize();
            m_bg.TrySetActive(true);
            m_img.gameObject.TrySetActive(true);
        }
        if (state == 3)
        {
            m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "switchtxt"));
            m_img.SetNativeSize();
            m_bg.TrySetActive(true);
            m_img.gameObject.TrySetActive(true);
        }
        if (state == 4)
        {
            m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "clickswitch"));
            m_img.SetNativeSize();
            m_bg.TrySetActive(true);
            m_img.gameObject.TrySetActive(true);
        }
    }

    public override void OnShow()
    {
        if (m_params != null)
        {
            int state = (int)m_params[0];
            int time = (int)m_params[1];
            m_time = (float)time;
            m_starttime = Time.realtimeSinceStartup;
            if (state == 0)
            {
                 m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "firelock"));
                 m_img.SetNativeSize();
                 m_img.gameObject.TrySetActive(true);
                 m_bg.TrySetActive(true);
            }
            if (state == 1)
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "locked"));
                m_img.SetNativeSize();
                m_bg.TrySetActive(true);
                m_img.gameObject.TrySetActive(true);
            }
            if (state == 2)
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "unlock"));
                m_img.SetNativeSize();
                m_bg.TrySetActive(true);
                m_img.gameObject.TrySetActive(true);
            }
            if (state == 3)
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "switchtxt"));
                m_img.SetNativeSize();
                m_bg.TrySetActive(true);
                m_img.gameObject.TrySetActive(true);
            }
            if (state == 4)
            {
                m_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "clickswitch"));
                m_img.SetNativeSize();
                m_bg.TrySetActive(true);
                m_img.gameObject.TrySetActive(true);
            }
        }
        else
        {
            return;
        }
    }

    public override void Update()
    {
        if (m_starttime < 0)
        {
            return;
        }
        else
        {
            if (Time.realtimeSinceStartup - m_starttime >= m_time)
            {
                m_starttime = -1;
                this.HidePanel();
            }
        }
    }
}


