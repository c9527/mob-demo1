﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBattleShop : BasePanel
{
    private Text m_txtSupplyPointNum;
    private Text m_txtName;
    private Text m_txtDesc;
    private Image m_imgEquip;
    private Vector2 m_equipImagePos;

    private DataGrid m_dataGrid;
    private GameObject m_goTabbar;

    private DataGrid m_dataGridProp;

    private Toggle m_lastToggle;
    private UIEffect m_halo;

    public PanelBattleShop()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleBuyGun");
        AddPreLoadRes("UI/Battle/PanelBattleBuyGun", EResType.UI);
        AddPreLoadRes("UI/Package/ItemBattleBuyGunRender", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPropRender", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        m_txtSupplyPointNum = m_tran.Find("txtSupplyPoint").GetComponent<Text>();
        m_txtName = m_tran.Find("infoframe/sortorder/txtName").GetComponent<Text>();
        m_txtDesc = m_tran.Find("infoframe/txtDesc").GetComponent<Text>();
        m_imgEquip = m_tran.Find("infoframe/sortorder/imgEquip").GetComponent<Image>();
        m_equipImagePos = m_imgEquip.rectTransform.anchoredPosition;

        m_dataGrid = m_tran.Find("equipframe/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemBattleBuyGunRender"), typeof(ItemBattleShopRender));
        m_dataGrid.SetItemClickSound(AudioConst.selectMallItem);
        m_dataGrid.useLoopItems = true;

        m_dataGridProp = m_tran.Find("infoframe/list").gameObject.AddComponent<DataGrid>();
        m_dataGridProp.useClickEvent = false;
        m_dataGridProp.autoSelectFirst = false;
        m_dataGridProp.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemPropRender"), typeof(ItemPropRender));

        m_goTabbar = m_tran.Find("equipframe/tabbar").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("equipframe/tabbar/tabAll")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("equipframe/tabbar/tabMain")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("equipframe/tabbar/tabSub")).onPointerClick += OnTabbarClick;
        UGUIClickHandler.Get(m_tran.Find("equipframe/tabbar/tabOther")).onPointerClick += OnTabbarClick;

        UGUIClickHandler.Get(m_tran.Find("Close")).onPointerClick += (target, event_data) => { HidePanel(); };

        m_dataGrid.onItemSelected += OnItemSelected;
    }

    private void OnItemSelected(object renderData)
    {
        if (renderData == null)
        {
            m_txtName.text = "";
            m_txtDesc.text = "";
            m_imgEquip.enabled = false;
            m_dataGridProp.Data = new object[0];
            return;
        }

        ConfigGameShopLine line = renderData as ConfigGameShopLine;
        var item = ItemDataManager.GetItem(line.Param);

        ConfigMallLine mallLine = ConfigManager.GetConfig<ConfigMall>().GetMallLine(line.Param, -1);
        if (mallLine == null)
        {
            return;
        }

        m_txtName.text = item.DispName;
        m_txtDesc.text = item.Desc;
        if (!string.IsNullOrEmpty(item.Icon))
        {
            m_imgEquip.SetSprite(ResourceManager.LoadIcon(item.Icon),item.SubType);
            m_imgEquip.SetNativeSize();
            m_imgEquip.enabled = true;
            m_imgEquip.rectTransform.anchoredPosition = m_equipImagePos + mallLine.IconOffset;
            m_imgEquip.rectTransform.localRotation = Quaternion.Euler(0, 0, item.IconRotation);
            m_imgEquip.rectTransform.localScale = new Vector3(0.2f, 0.2f, 1);
            TweenScale.Begin(m_imgEquip.gameObject, 0.1f, new Vector3(1, 1, 1));
        }
        else
            m_imgEquip.enabled = false;

        var itemWeapon = item as ConfigItemWeaponLine;
        m_dataGridProp.Data = itemWeapon != null ? ItemDataManager.GetPropRenderInfos(itemWeapon) : new object[0];
    }

    public override void OnShow()
    {
        m_lastToggle = null;
        m_dataGrid.ResetScrollPosition();

        if (m_halo == null)
            m_halo = UIEffect.ShowEffectAfter(m_imgEquip.transform.parent, EffectConst.UI_MALL_HALO, new Vector2(75f, 168f));

        m_dataGrid.Data = CreateGroupItems(0);

        m_txtSupplyPointNum.text = SurvivalBehavior.singleton.supplypoint.ToString();
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = false;
#endif
    }

    public override void OnBack()
    {
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            Screen.lockCursor = true;
#endif
        if (m_halo != null)
            m_halo.Destroy();
        m_halo = null;
    }

    public override void OnDestroy()
    {
        m_dataGrid = null;
        if (m_halo != null)
            m_halo.Destroy();
        m_halo = null;
    }

    public override void Update()
    {
    }

    private ConfigGameShopLine[] CreateGroupItems(int sort)
    {
        var list = new List<ConfigGameShopLine>();

        list = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopList(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_WEAPON, sort);

        list.Sort((x, y) => x.Order - y.Order);
        return list.ToArray();
    }

    private void OnTabbarClick(GameObject target, PointerEventData data)
    {
        //var toggle = target.GetComponent<Toggle>();
        switch (target.name)
        {
            case "tabAll": m_dataGrid.Data = CreateGroupItems(0); break;
            case "tabMain": m_dataGrid.Data = CreateGroupItems(1); break;
            case "tabSub": m_dataGrid.Data = CreateGroupItems(2); break;
            case "tabOther": m_dataGrid.Data = CreateGroupItems(3); break;
        }
    }
}
