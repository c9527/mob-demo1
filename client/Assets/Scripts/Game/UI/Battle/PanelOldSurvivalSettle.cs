﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PanelOldSurvivalSettle : PanelOldSettle
{
    private Image m_imgRoundNum1;
    private Image m_imgRoundNum2;
    private Image m_imgRoundSuccess;
    private Image m_imgFirstRewardTips;
    private Image m_imgJoinRewardTips;
    private Image m_imgPassRewardTips;
    private Image m_imgPassRewardResetRips;
    private Text m_txtFirstRewardTimes;
    private Text m_txtJoinRewardTimes;
    private Text m_txtPassRewardTimes;
    private Image m_imgKillBossIcon;

    private GameObject m_goStarLevel;
    private const int TotalStarLevel = 3;
    private List<Image> m_lstStarLevel;

    static public int m_starLevel = -1;

    static private string m_gameType;
    static private int m_joinRewardCountUse;
    static private int m_passRewardCountUse;

    public PanelOldSurvivalSettle()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelOldSurvivalSettle");

        AddPreLoadRes("UI/Battle/PanelOldSurvivalSettle", EResType.UI);
        AddPreLoadRes("UI/Battle/SettleWeaponItemRender", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Settle", EResType.Atlas, ResTag.Forever);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);

        if (RoomModel.LastRoomInfo.rule != GameConst.GAME_RULE_WORLD_BOSS && RoomModel.LastRoomInfo.rule != GameConst.GAME_RULE_BOSS_NEWER)
            MaxPlayerCount = 5;

        m_lstStarLevel = new List<Image>(TotalStarLevel);
    }

    public override void Init()
    {
        base.Init();

        m_txtFirstRewardTimes = m_tran.Find("first_reward_times_txt").GetComponent<Text>();
        m_txtJoinRewardTimes = m_tran.Find("join_reward_times_txt").GetComponent<Text>();
        m_txtPassRewardTimes = m_tran.Find("pass_reward_times_txt").GetComponent<Text>();

        m_imgJoinRewardTips = m_tran.Find("info_canvas/join_reward_tips_img").GetComponent<Image>();
        m_imgPassRewardTips = m_tran.Find("info_canvas/pass_reward_tips_img").GetComponent<Image>();
        m_imgFirstRewardTips = m_tran.Find("info_canvas/first_reward_tips_img").GetComponent<Image>();
        m_imgPassRewardResetRips = m_tran.Find("info_canvas/pass_reward_weekly_reset_tips_img").GetComponent<Image>();
        
        m_imgRoundNum1 = m_tran.Find("player_canvas/num1").GetComponent<Image>();
        m_imgRoundNum2 = m_tran.Find("player_canvas/num2").GetComponent<Image>();
        m_imgRoundSuccess = m_tran.Find("player_canvas/success").GetComponent<Image>();

        m_goStarLevel = m_tran.Find("player_canvas/star_level").gameObject;
        for (int i = 1; i <= TotalStarLevel; ++i )
        {
            Image img = m_tran.Find(string.Format("player_canvas/star_level/star_level_{0}/star_level_{0}_icon", i)).GetComponent<Image>();
            m_lstStarLevel.Add(img);
        }

        m_imgKillBossIcon = m_tran.Find("player_canvas2/kill_boss_icon").GetComponent<Image>();
    }

	public override void OnShow()
	{
		base.OnShow();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.SolidColor;
		UIManager.m_uiCamera.backgroundColor = UnityEngine.Color.black;

	}

	public override void OnHide()
	{
		base.OnHide();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.Depth;
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		UIManager.m_uiCamera.clearFlags = UnityEngine.CameraClearFlags.Depth;
	}

    protected override void updateData()
    {
        base.updateData();
        _camp0_img.enabled = true;
        m_imgKillBossIcon.enabled = false;

        m_imgPassRewardResetRips.enabled = RoomModel.ChannelType == ChannelTypeEnum.stage_survival;

        if (RoomModel.ChannelType == ChannelTypeEnum.worldboss)
        {
            ShowJoinReward(false);
            ShowFirstReward(false);
            if (m_gameType == ChannelTypeEnum.worldboss.ToString())
            {
                m_txtPassRewardTimes.transform.localPosition = m_txtFirstRewardTimes.transform.localPosition;
                m_imgPassRewardTips.transform.localPosition = m_imgFirstRewardTips.transform.localPosition;
                ShowPassReward(true);
                var useCount = m_passRewardCountUse;
                var countMax = 1;
                m_txtFirstRewardTimes.text = string.Format("{0}/{1}", countMax - useCount, countMax);
            }
            else
                ShowPassReward(false);

            int killBossPlayerIndex = -1;
            for (int i = 0; i < settle_data.Length; ++i )
            {
                if (settle_data[i].kill > 0)
                {
                    killBossPlayerIndex = i;
                    break;
                }
            }
            if (killBossPlayerIndex != -1 && killBossPlayerIndex < _player_list.Length)
            {
                m_imgKillBossIcon.enabled = true;
                m_imgKillBossIcon.rectTransform.SetParent(_player_list[killBossPlayerIndex].transform);
                m_imgKillBossIcon.SetNativeSize();
                m_imgKillBossIcon.transform.localPosition = _player_list[killBossPlayerIndex].reportBtnPos;
            }
            return;
        }
        m_tran.Find("player_canvas/round").gameObject.TrySetActive(true);
        if (game_round < 10)
        {
            m_imgRoundNum2.enabled = false;
            m_imgRoundNum1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round));
            m_imgRoundNum1.SetNativeSize();
        }
        else
        {
            m_imgRoundNum2.enabled = true;
            m_imgRoundNum1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round / 10));
            m_imgRoundNum2.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "round_" + game_round % 10));
            m_imgRoundNum1.SetNativeSize();
            m_imgRoundNum2.SetNativeSize();
        }

        m_imgRoundSuccess.enabled = self_data != null && self_data.camp == win_camp;
        if (m_imgRoundSuccess.enabled == true)
        {
            m_tran.Find("player_canvas/round").gameObject.TrySetActive(false);
            m_imgRoundNum2.enabled = false;
            m_imgRoundNum1.enabled = false;
        }
        if (RoomModel.ChannelType == ChannelTypeEnum.survival)
        {
            ShowJoinReward(false);
            ShowPassReward(false);
            ShowFirstReward(false);
            var useCount = PlayerSystem.survival_data.daily_reward_cnt;
            var countMax = ConfigMisc.GetInt("survival_reward_daily_max");
            m_txtFirstRewardTimes.text = string.Format("{0}/{1}", countMax - useCount, countMax);
        }
        else if (RoomModel.ChannelType == ChannelTypeEnum.defend && m_gameType == ChannelTypeEnum.defend.ToString())
        {
            ShowJoinReward(true);
            ShowPassReward(true);
            ShowFirstReward(false);

            var joinRewradCountMax = ConfigMisc.GetInt("defend_join_reward_daily_max");
            var finishRewardCountMax = ConfigMisc.GetInt("defend_finish_reward_daily_max");

            m_txtJoinRewardTimes.text = string.Format("{0}/{1}", joinRewradCountMax - m_joinRewardCountUse, joinRewradCountMax);
            m_txtPassRewardTimes.text = string.Format("{0}/{1}", finishRewardCountMax - m_passRewardCountUse, finishRewardCountMax);
        }
        else
        {
            //Logger.Error("还没有收到toc_player_game_reward_times协议");
            ShowJoinReward(false);
            ShowPassReward(false);
            ShowFirstReward(false);
        }
        
        ShowStarLevel(m_starLevel != -1, m_starLevel);
    }

    private void ShowStarLevel(bool isShow, int starLevel)
    {
        if(isShow)
        {
            m_goStarLevel.SetActive(true);
            for(int i = 0; i < TotalStarLevel; ++i)
            {
                m_lstStarLevel[i].enabled = starLevel > i;
            }
        }
        else
        {
            m_goStarLevel.SetActive(false);
        }

    }

    private void ShowJoinReward(bool isShow)
    {
        m_imgJoinRewardTips.enabled = isShow;
        m_txtJoinRewardTimes.enabled = isShow;
    }

    private void ShowPassReward(bool isShow)
    {
        m_imgPassRewardTips.enabled = isShow;
        m_txtPassRewardTimes.enabled = isShow;
    }

    private void ShowFirstReward(bool isShow)
    {
        m_imgFirstRewardTips.enabled = isShow;
        m_txtFirstRewardTimes.enabled = isShow;
    }

    static void Toc_player_game_reward_times(toc_player_game_reward_times proto)
    {
        m_gameType = proto.game_type;
        m_joinRewardCountUse = proto.partake_reward;
        m_passRewardCountUse = proto.win_reward;
    }

    static void Toc_player_stage_star(toc_player_stage_star proto)
    {
        Logger.Error("Toc_player_stage_star:  proto.star=" + proto.star);
        m_starLevel = proto.star;
    }

}