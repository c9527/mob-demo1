﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/1/14 15:27:54
 * Description://战斗界面光标设置管理
 * Update History:
 *
 **************************************************************************/

public class PanelBattleCursorManager
{
    private static List<string> limitPanelName = new List<string>()
    {
        typeof (PanelBattleShop).Name,
        typeof (PanelBuy).Name,
        typeof (PanelTip).Name,
        typeof (PanelBuyBuff).Name,
        typeof (PanelBuyDotaPlayerBuff).Name,
        typeof (PanelBuyDotaBossBuff).Name,
        typeof (PanelBattleKickReason).Name,
        typeof (PanelBattleRecord).Name,
        typeof (PanelSetting).Name,
        typeof (PanelZombieSelect).Name,
        typeof (PanelFlopReward).Name
    };
    public PanelBattleCursorManager()
    {
        // 构造器
    }
    public static void lockCursor(bool isLock)
    {
        if (Screen.lockCursor != isLock)
        {
            if (isLock)
            {
                if (!checkPanelLimit())
                {
                    Screen.lockCursor = true;
                    //设置全屏
                    if (!UIManager.fullScreen)
                    {
                        UIManager.fullScreen = true;
                    }
                }
            }
            else
            {
                Screen.lockCursor = isLock;
            }
        }
    }
    /// <summary>
    /// 如果有存在限制面板 则不能隐藏光标
    /// </summary>
    public static bool checkPanelLimit()
    {
        foreach (var temp in limitPanelName)
        {
            if (UIManager.IsOpen(temp))
                return true;
        }
        return false;
    }
}
