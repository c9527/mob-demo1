﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class DamageTipsShowType
{
    public const int Damage = 1;
    public const int Cure = 2;
    public const int Damage2 = 3;
    public const int DamageCure = 4;
    public const int DamageCure2 = 5;

    public static bool IsDamageOrCure(int showType)
    {
        return showType == DamageCure || showType == DamageCure2;
    }

    public static bool IsDamage(int showType)
    {
        return showType == Damage || showType == Damage2;
    }

    public static bool IsCure(int showType)
    {
        return showType == Cure;
    }

    public static int GetObjCount(int showType)
    {
        return (showType == Damage2 || showType == DamageCure2) ? 1 : 3;
    }
}

class DamageTipsManager : MonoBehaviour
{
    private int m_maxObjectCount = 1;
    private static readonly Vector3 TipsOffSet = new Vector3(0, 1f, 0);
    private int m_iNextIndex;
    private bool m_bIsEnable;
    private int m_damageShowType;
    private GameObject m_go;
    private RectTransform m_rt;
    List<DamageTipsObject> m_listObjects = new List<DamageTipsObject>();

    public DamageTipsManager()
    {
        m_bIsEnable = false;
        GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
    }

    void Awake()
    {
        m_go = new GameObject("BattleDamgeTip");
        CanvasGroup group = m_go.AddComponent<CanvasGroup>();
        group.interactable = false;
        group.blocksRaycasts = false;
        m_rt = m_go.AddComponent<RectTransform>();
        m_rt.sizeDelta = Vector2.zero;
        m_go.AddComponent<CanvasRenderer>();
        m_go.AddComponent<SortingOrderRenderer>();
        m_go.layer = LayerMask.NameToLayer("UI");
        m_go.transform.SetParent(UIManager.GetLayer(LayerType.Tip));
        m_go.transform.localScale = Vector3.one;
        m_go.transform.localPosition = Vector3.zero;
        SortingOrderRenderer.RebuildAll();

        m_maxObjectCount = DamageTipsShowType.GetObjCount(WorldManager.singleton.gameRuleLine.DamgeShowType);
        InitTipsObjects();
        m_iNextIndex = 0;
        m_bIsEnable = true;
    }

    void OnDestroy()
    {
        m_bIsEnable = false;
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        ReleaseTips();
        GameObject.Destroy(m_go);
    }

    private void ReleaseTips()
    {
        for (int i = 0, imax = m_listObjects.Count; i < imax; ++i)
        {
            if (m_listObjects[i].tweener != null)
            m_listObjects[i].tweener.Stop();
            GameObject.Destroy(m_listObjects[i].go);
        }
        m_listObjects.Clear();
        m_listObjects = null;
    }

    private void PlayTip(Vector3 pos, string damgeValue, Color color, bool isKnight)
    {
        DamageTipsObject dtObj = GetTipsObject();

        GameObject go = dtObj.go;
        go.transform.localScale = Vector3.one;
        go.TrySetActive(true);

        Text tipText = dtObj.tipsText;
        tipText.text = damgeValue;
        tipText.color = color;
        var size = new Vector2(Math.Max(tipText.preferredWidth, 160), 30);
        tipText.rectTransform.sizeDelta = size;
        if (size.y < tipText.preferredHeight)
        {
            size = new Vector2(size.x, Math.Max(tipText.preferredHeight, 30));
            tipText.rectTransform.sizeDelta = size;
        }

        DamageTipsTweenerParams param = new DamageTipsTweenerParams();
        param.tweenTime = GameSetting.DamageTipsTweenTime;
        param.totalTime = GameSetting.DamageTipsTotalTime;
        param.pos = pos;
        if (!isKnight)
        {
            param.randomPos = new Vector2(
                 UnityEngine.Random.Range(GameSetting.DamageTipsPosRamdomX.x, GameSetting.DamageTipsPosRamdomX.y), UnityEngine.Random.Range(GameSetting.DamageTipsPosRamdomY.x, GameSetting.DamageTipsPosRamdomY.y));
        } param.scaleMin = GameSetting.DamageTipsScaleChange.x;
        param.scaleMax = GameSetting.DamageTipsScaleChange.y;
        param.onComplete = OnTweenComplete;

        DamageTipsTweener tweener = dtObj.tweener;
        tweener.Stop();
        tweener.SetData(param);
        tweener.Play();
    }

    private void OnPlayerBeDamaged(DamageData damageData)
    {
        if (!m_bIsEnable || damageData.attacker != MainPlayer.singleton
//                 || (!DamageTipsShowType.IsDamageOrCure(m_damageShowType) &&
//                        (DamageTipsShowType.IsDamage(m_damageShowType) && damageData.damage <= 0)
                        ||(damageData.damage == 0)
                        || (DamageTipsShowType.IsCure(m_damageShowType) && (damageData.damage >= 0 || damageData.attacker.Camp != damageData.victim.Camp)))
           return;
        if ((damageData.damage < 0))
        {
            damageData.bodyPart = -1;
            damageData.damage *= -1;
        }

        if (damageData.IsCrit())
            PlayTip(damageData.hitPos, "暴击", Color.red, false);
        else
            PlayTip(damageData.hitPos, damageData.damage.ToString(), GetTextColor(damageData.bodyPart), false);
    }

    private Color GetTextColor(int hitPart)
    {
        switch (hitPart)
        {
            case BasePlayer.BODY_PART_ID_HEAD:
                return Color.red;
            case BasePlayer.BODY_PART_ID_BODY:
            case BasePlayer.BODY_PART_ID_LIMBS:
                return Color.white;
            case -1:
                return Color.green;
        }
        return Color.white;
    }

    private void InitTipsObjects()
    {
        m_listObjects.Clear();
        for (int i = 0; i < m_maxObjectCount; ++i)
        {
            var go = new GameObject("damge_tips_txt");
            go.transform.SetParent(m_rt, false);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            var tipsText = new GameObject("Text").AddComponent<Text>();
            tipsText.rectTransform.SetParent(go.transform);
            tipsText.rectTransform.localScale = Vector3.one;
            tipsText.rectTransform.localPosition = new Vector3(0, 0, 0);
            tipsText.font = ResourceManager.LoadFont();
            tipsText.fontSize = GameSetting.DamageTipsFontSize;
            tipsText.alignment = TextAnchor.MiddleCenter;
            var tweener = go.AddMissingComponent<DamageTipsTweener>();
            DamageTipsObject dtObj = new DamageTipsObject();
            dtObj.go = go;
            dtObj.tipsText = tipsText;
            dtObj.tweener = tweener;

            m_listObjects.Add(dtObj);
        }
    }

    private DamageTipsObject GetTipsObject()
    {
        if (m_iNextIndex == m_maxObjectCount)
            m_iNextIndex = 0;

        DamageTipsObject dtObj = m_listObjects[m_iNextIndex++];
        return dtObj;
    }

    private void OnTweenComplete(DamageTipsTweener tween)
    {
        tween.gameObject.TrySetActive(false);
    }

    public int damgeShowType
    {
        set { m_damageShowType = value; }
    }
}

class DamageTipsObject
{
    public GameObject go;
    public Text tipsText;
    public DamageTipsTweener tweener;
}

class DamageTipsTweenerParams
{
    public float scaleMin = 0;
    public float scaleMax = 0;
    public float tweenTime = 0;
    public float totalTime = 0;
    public Vector3 pos;
    public Vector3 randomPos = Vector3.zero;
    public Action<DamageTipsTweener> onComplete;
}

/// <summary>
/// 伤害提示缓动效果
/// </summary>
class DamageTipsTweener : MonoBehaviour
{
    private Vector3 m_screenPos;
    private DamageTipsTweenerParams m_data;
    private float m_curTime;
    private bool m_bIsPlaying;

    public void SetData(DamageTipsTweenerParams data)
    {
        m_data = data;
    }

    public void Play()
    {
        m_bIsPlaying = true;
        m_curTime = 0;
        StartCoroutine(PlayProgress());
    }

    public void Stop()
    {
        m_bIsPlaying = false;
        StopCoroutine(PlayProgress());
    }

    private IEnumerator PlayProgress()
    {
        while (true)
        {
            if (SceneCamera.singleton == null)
                break;

            m_curTime += Time.deltaTime;
            if (m_curTime <= m_data.tweenTime)
            {
                Vector3 scale = Vector3.one;
                scale.y = scale.x = EaseMethod.UpdateByEaseOut(m_curTime, m_data.tweenTime, m_data.scaleMax, m_data.scaleMin);
                transform.localScale = scale;
            }
            else if (m_curTime > m_data.totalTime)
                break;
            m_screenPos = SceneCamera.singleton.WorldToScreenPoint(m_data.pos);
            transform.position = UIManager.m_uiCamera.ScreenToWorldPoint(m_screenPos) + m_data.randomPos;
            yield return null;
        }
        if (m_data.onComplete != null)
            m_data.onComplete(this);
        m_bIsPlaying = false;
    }
}
