﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemBattleShopRender : ItemRender
{
    public ConfigGameShopLine m_data;
    private Text m_txtName;
    private Text m_txtMoney;
    private Image m_imgItem;
    private Image m_imgCoin;
    private GameObject m_goBuyBtn;

    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("txtName").GetComponent<Text>();
        m_txtMoney = tran.Find("txtMoney").GetComponent<Text>();
        m_imgItem = tran.Find("imgItem").GetComponent<Image>();
        m_imgCoin = tran.Find("imgCoin").GetComponent<Image>();
        m_goBuyBtn = tran.Find("btnBuy").gameObject;
        UGUIClickHandler.Get(m_goBuyBtn).onPointerClick += OnBuyBtnClick;
    }

    private void OnBuyBtnClick(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_fight_game_shop() { shop_id = m_data.ShopId });
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ConfigGameShopLine;
        if (m_data == null)
            return;

        var item = ItemDataManager.GetItem((m_data.Param));
        if (item == null)
            return;

        m_imgCoin.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "dollar_icon"));
        m_imgCoin.SetNativeSize();

        if (!string.IsNullOrEmpty(item.Icon))
        {
            m_imgItem.SetSprite(ResourceManager.LoadIcon(item.Icon),item.SubType);
            m_imgItem.SetNativeSize();
            m_imgItem.enabled = true;
        }
        else
            m_imgItem.enabled = false;

        int price = m_data.Costs;
        m_txtMoney.text = price.ToString();
        m_txtName.text = item.DispName;
    }
}
