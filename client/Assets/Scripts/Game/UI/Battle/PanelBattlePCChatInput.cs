﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/31 13:55:31
 * Description:PC端  聊天输入
 * Update History:
 *
 **************************************************************************/

public partial class PanelBattle
{
    private GameObject m_pcChatInputGo;
    private GameObject m_pcChatSend;
    private InputField m_pcChatInputText;
    private Button m_pcChatChannel;

    public static bool isPcChatLimit = false;

    protected bool isPcChatShow
    {
        get
        {
            return m_pcChatInputGo.activeSelf;
        }
    }

    protected void showPcChatInput(bool isShow, bool isEnterPress = false)
    {
        if (isEnterPress)
        {
            if (m_pcChatInputGo.activeSelf)
            {
                //已经显示 将关闭并且发送聊天信息
                m_pcChatInputGo.TrySetActive(false);
                onClickBtnChatSend(null, null);
                //Screen.lockCursor = true;
            }
            else
            {
                m_pcChatInputGo.TrySetActive(isShow);
               // Screen.lockCursor = false;
                //m_pcChatInputText.fo
               // EventSystem.current.SetSelectedGameObject(m_pcChatInputText.gameObject);
                m_pcChatInputText.ActivateInputField();
            }

        }
        else
        {
            m_pcChatInputGo.TrySetActive(isShow);
        }
        isPcChatLimit = m_pcChatInputGo.activeSelf;
    }

    protected void onClickBtnChatSend(GameObject target, PointerEventData eventData)
    {
        m_pcChatInputGo.TrySetActive(false);
        string s = m_pcChatInputText.text;
        if (string.IsNullOrEmpty(s.Trim(' ', '\n')))
            return;
        if (s == "fps")
        {
            m_goFps.TrySetActive(!m_goFps.activeSelf);
            return;
        }
        else if (s == "fpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.AddMissingComponent<UIGmPanel>();
            p.enabled = true;
            return;
        }
        else if (s == "hidefpsdebugpanel")
        {
            UIGmPanel p = Driver.singleton.gameObject.GetComponent<UIGmPanel>();
            if (p != null)
                p.enabled = false;
            return;
        }

        m_pcChatInputText.text = "";
        if (s == "") return;
        NetLayer.Send(new tos_fight_chat() { type = m_chatType, msg = WordFiterManager.Fiter(s), audio_chat = new p_audio_chat() { content = new byte[0], duration = 0 } });

    }
}
