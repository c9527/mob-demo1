﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PassiveSkillItemRender : SkillItemRender
{
    public override void Awake()
    {
        _cd_mask_img = "buff_bg_mask";
        base.Awake();
    }

    protected override void OnSetData(object data)
    {
        if (m_renderData == null)
        {
            return;
        }

        var info = m_renderData as TDBuffInfo;

        if (info == null)
        {
            Logger.Error("Data is null");
            return;
        }

        _reverse = true;
        _isSetData = true;

        if (_last_time == -1)
        {
            frozen = true;
        }

        SetKillInfo(info.ID, info);
    }

    protected void SetKillInfo(int id, TDBuffInfo info)
    {
        _id = id;
        _cd = info.Time + 0.5f;

        _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, info.BuffIcon));
        _avatar_img.SetNativeSize();

        if (!string.IsNullOrEmpty(info.BuffMask))
        {
            _cd_mask.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, info.BuffMask));
            _cd_mask.SetNativeSize();
        }

        _cd_mask.rectTransform.anchoredPosition = info.BuffMaskOffset;
    }

}

