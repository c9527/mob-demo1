﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text;

public class BattleRoomTips : MonoBehaviour
{
    const float FightAchieveEffectY = -175f;

    ConfigGameRule m_configGameRule;

    ConfigFightAchieve m_configFightAchieve;
    ConfigPvEFightAchieve m_configPvEFightAchieve;
    Sprite[] m_numberArr;

    //UISlideText m_kSlideText;

    private float m_delayTime = 0; //特效、音效延迟时间

    bool m_bRealGameStart = false;

    private ProgressBarProp prop;
    public static bool isGoldLike = false;

    // Use this for initialization
    void Start()
    {
        m_configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        if (WorldManager.singleton.fightType.Contains("stage"))
            m_configPvEFightAchieve = ConfigManager.GetConfig<ConfigPvEFightAchieve>();
        else
            m_configFightAchieve = ConfigManager.GetConfig<ConfigFightAchieve>();
        InitNetMsg();
        //PrepairDigits();

        GameObject slideTextFrame = new GameObject("slideTextFrame");
        slideTextFrame.transform.SetParent(gameObject.transform);
        RectTransform slideRectTransform = slideTextFrame.AddMissingComponent<RectTransform>();
        Vector2 anchor = new Vector2(0.5f,0.5f );
        slideRectTransform.anchorMin = anchor;
        slideRectTransform.anchorMax = anchor;
        slideRectTransform.anchoredPosition = new Vector2( 0,120 );
        slideRectTransform.sizeDelta = new Vector2( 700,140 );
        slideRectTransform.localScale = new Vector3(1, 1, 1);


        //m_kSlideText = slideTextFrame.AddComponent<UISlideText>();
        //m_kSlideText.Init( slideTextFrame.transform );

        InitEvent();
        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state);
    }

    //void PrepairDigits()
    //{
    //    m_numberArr = new Sprite[10];
    //    m_numberArr[0] = ResourceManager.LoadSprite(AtlasName.Common, "0");
    //    m_numberArr[1] = ResourceManager.LoadSprite(AtlasName.Common, "1");
    //    m_numberArr[2] = ResourceManager.LoadSprite(AtlasName.Common, "2");
    //    m_numberArr[3] = ResourceManager.LoadSprite(AtlasName.Common, "3");
    //    m_numberArr[4] = ResourceManager.LoadSprite(AtlasName.Common, "4");
    //    m_numberArr[5] = ResourceManager.LoadSprite(AtlasName.Common, "5");
    //    m_numberArr[6] = ResourceManager.LoadSprite(AtlasName.Common, "6");
    //    m_numberArr[7] = ResourceManager.LoadSprite(AtlasName.Common, "7");
    //    m_numberArr[8] = ResourceManager.LoadSprite(AtlasName.Common, "8");
    //    m_numberArr[9] = ResourceManager.LoadSprite(AtlasName.Common, "9");
    //}


    void InitNetMsg()
    {
        

    }

    void InitEvent()
    {
        GameDispatcher.AddEventListener<toc_fight_achieve>(GameEvent.PROXY_PLAYER_FIGHT_ACHIEVE, OnPlayerAchieveDone);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.UI_GANRANSHIBAI, OnGanRanShiBai);
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<toc_fight_achieve>(GameEvent.PROXY_PLAYER_FIGHT_ACHIEVE, OnPlayerAchieveDone);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState );
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener(GameEvent.UI_GANRANSHIBAI, OnGanRanShiBai);
    }

    void OnUpdateBattleState( SBattleState data )
    {
        if (WorldManager.singleton.isFightInTurnModeOpen || data == null) 
            return;
        switch (data.state)
        {
            case GameConst.Fight_State_GameInit:
                m_bRealGameStart = true;
                break;
            case GameConst.Fight_State_GameStart:
                if (m_bRealGameStart )
                    m_bRealGameStart = false;
                BattleStart();
                BeginRound(data.round);
                break;
            case GameConst.Fight_State_RoundStart:
                break;
            case GameConst.Fight_State_RoundEnd:
                RoundOver(data.last_win );
                break;
            case GameConst.Fight_State_GameOver:
                BattleOver( data.final_win);
                break;
            default:
                break;
        }
    }

    void BeginRound( int iRound )
    {
        if( WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KILLALL)) // 歼灭站
        {
            WorldManager.ShowTextMsgInPanel(string.Format("现在开始第{0}回合！", iRound ), Color.cyan);
        }
    }

    void BattleStart()
    {
        GameDispatcher.Dispatch( GameEvent.GAME_BATTLE_ROUND_START );
    }

    void RoundOver( int winCamp )
    { 
        GameDispatcher.Dispatch(GameEvent.GAME_BATTLE_ROUND_OVER);
    }

    void BattleOver( int winCamp )
    {
        GameDispatcher.Dispatch( GameEvent.GAME_BATTLE_OVER );
    }

    ConfigFightAchieveLine getConfigFightAchieveLine(string achieveType)
    {
        if (m_configFightAchieve != null)
            return m_configFightAchieve.GetLine(achieveType);
        else if (m_configPvEFightAchieve != null)
            return m_configPvEFightAchieve.GetLine(achieveType);
        return null;
    }

    void OnPlayerAchieveDone(toc_fight_achieve data)
    {
        if (WorldManager.singleton.isViewBattleModel == true && PlayerBattleModel.Instance.battle_info.spectator_switch == true
            && FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
        {
            return;
        }
        if (WorldManager.singleton.isSurvivalTypeModeOpen) 
            return;

        var myAchieves = new List<ConfigFightAchieveLine>();
        int killerid = data.from;
        BasePlayer killer = WorldManager.singleton.GetPlayerById(killerid);
        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null)
            curPlayer = MainPlayer.singleton;
        if (killer != null )
        {
            for (int i = 0; i < data.achieve_list.Length; i++)
            {
                string achieveType = data.achieve_list[i];
                ConfigFightAchieveLine lineConfig = getConfigFightAchieveLine(achieveType);
                if (lineConfig == null)
                    continue;

                int killNum = lineConfig.KillNum;
                if (killer == curPlayer)
                {
                    myAchieves.Add(lineConfig);
                    if (lineConfig.Type == "TenKill")
                    {
                        isGoldLike = true;
                    }
                }

                if (killer == curPlayer)
                    UIManager.GetPanel<PanelBattle>().ShowMuchKillMsg(killNum);

                if (killNum <= 4) continue;


                if(QualityManager.enableUIWorldAchievement)
                {
                    NoticePanelProp prop = new NoticePanelProp();
                    //底图
                    prop.bgAltas = AtlasName.BattleNew;
                    prop.bgName = "black_bg";
                    prop.bgSize = new Vector2(382, 60);
                    //特杀图标
                    prop.imgPos = new Vector2(-142, 224);
                    //玩家名字
                    prop.msg = killer.ColorName;
                    prop.txtPos = new Vector2(-31, 219);
                    prop.textProp = new TextProp() { fontSize = 18 };
                    //贱血图标
                    prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.BattleNew, sprite = "killBlood", pos = new Vector2(52, 219) });

                    if (killNum > 4 && killNum < 10)
                    {
                        //连杀图标
                        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.BattleNew, sprite = "continue_kill_tip", pos = new Vector2(85, 217) });
                        //数字图标
                        prop.numIconList.Add(new ImgNumProp() { preName = "kill", atlas = AtlasName.Battle, align = 0, gap = -7, pos = new Vector2(52, 218), value = killNum });
                    }
                    else if (killNum > 9)
                    {
                        //超神图标
                        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.BattleNew, sprite = "oh_god_icon", pos = new Vector2(77, 217) });
                    }
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanelQueue(AtlasName.BattleNew, "big_kill_icon", 2f, false, new Vector2(-56, 222), prop, new Vector2(19, -81));
                    //string strTips = lineConfig.Tips;
                    //if (string.IsNullOrEmpty(strTips))
                    //    continue;
                    //StringBuilder sbTip = new StringBuilder(killer.ColorName);
                    //sbTip.Append(strTips);

                    //m_kSlideText.PushSlideText(sbTip.ToString(), UISlideText.SLIDE_DIRC.TOP, 0, 0, new Color(255, 156, 0), 20, 5);
                }
            }
        }

        //音效
        var configShow = GetAchieveConfig(myAchieves, false);
        if (configShow != null && !string.IsNullOrEmpty(configShow.Sound))
        {
            TimerManager.SetTimeOut(m_delayTime, () => AudioManager.PlayFightUIVoice(configShow.Sound));
            TimerManager.SetTimeOut(m_delayTime, () => AudioManager.PlayFightUISound(AudioConst.killIcon));
        }

        //特效
        var configEffect = GetAchieveConfig(myAchieves, true);
        if (configEffect != null && !string.IsNullOrEmpty(configEffect.Effect))
        {
            var effect_str = configEffect.Effect;
            if (MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
            {
                if (configEffect.Type=="FirstBlood"
                    || configEffect.Type == "LifeForLife"
                    || configEffect.Type.IndexOf("Kill") != -1 
                    || configEffect.Type.IndexOf("GodLike") != -1
                    || configEffect.Type.IndexOf("HeadShot")!=-1
                    || configEffect.Type.IndexOf("venge") != -1
                    )
                {
                    effect_str = "UI_ganran";
                }
            }

            string fightAchieveEffect = "";
            if (killer.curWeapon != null && killer.curWeapon.weaponConfigLine != null && !string.IsNullOrEmpty(killer.curWeapon.weaponConfigLine.FightAchieveEffect))
            {
                fightAchieveEffect = killer.curWeapon.weaponConfigLine.FightAchieveEffect;
            }
            TimerManager.SetTimeOut(m_delayTime, () =>  
            {
                if (!string.IsNullOrEmpty(fightAchieveEffect))
                {
                    UIEffect.ShowEffect(transform, fightAchieveEffect, 2, new Vector2(0, FightAchieveEffectY), false);
                    if(GameSetting.FightAchieveEffectDelay > 0 && GameSetting.FightAchieveEffectDelay < 2)
                    {
                        TimerManager.SetTimeOut(GameSetting.FightAchieveEffectDelay, () => { UIEffect.ShowEffect(transform, effect_str, 2 - GameSetting.FightAchieveEffectDelay, new Vector2(0, FightAchieveEffectY), false); });
                    }
                    else
                        UIEffect.ShowEffect(transform, effect_str, 2, new Vector2(0, FightAchieveEffectY), false);
                }
                else
                    UIEffect.ShowEffect(transform, effect_str, 2, new Vector2(0, FightAchieveEffectY), false);
            });
        }
        if (configShow != null && !string.IsNullOrEmpty(configShow.Sound) || configEffect != null && !string.IsNullOrEmpty(configEffect.Effect))
            m_delayTime += 2;
    }

    ConfigFightAchieveLine GetAchieveConfig(List<ConfigFightAchieveLine> myAchieves, bool effect)
    {
        var maxPriority = 0;
        ConfigFightAchieveLine configShow = null;
        for (int i = 0; i < myAchieves.Count; i++)
        {
            var lineConfig = myAchieves[i];
            if (lineConfig != null && (effect ? lineConfig.EffectPriority > maxPriority : lineConfig.SoundPriority > maxPriority))
            {
                maxPriority = effect ? lineConfig.EffectPriority : lineConfig.SoundPriority;
                configShow = lineConfig;
            }
        }
        return configShow;
    }

    void OnPlayerRevive( string eventName,BasePlayer who )
    {
        if (who == MainPlayer.singleton)
        {
            ConfigGameRuleLine rule_info = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
            if (who.GodTime > 0)
            {
                if (UIManager.IsOpen<PanelBattle>())
                {
                    if(prop == null)
                    {
                        Vector2 pos = new Vector2(0, -100);
                        prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, textProp = new TextProp() { fontSize = 18, rect = new RectTransformProp() { size = new Vector2(200, 30) } } };
                    }
                    UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "无敌时间", who.GodTime, prop, "GodTime");
                }
            }   
        }
    }

    void OnGanRanShiBai()
    {
        UIEffect.ShowEffect(transform, EffectConst.UI_GANRANSHIBAI, 2, new Vector2(0, -180), false);
    }

    void Update()
    {
        if (m_delayTime > 0)
        {
            m_delayTime -= Time.deltaTime;
            if (m_delayTime < 0)
                m_delayTime = 0;
        }
    }
}
