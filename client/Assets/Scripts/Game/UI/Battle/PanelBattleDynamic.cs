﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 这个文件主要是处理一些由代码动态添加的UI元素
/// </summary>
public partial class PanelBattle
{
    private UGUIClickHandler.PointerEvetCallBackFunc onClick;

    //公共进度条
    private UIProgressBar m_progressBar;
    private GameObject m_goProgressBar;

    //公共提示面板
    private BattleNoticePanel m_noticePanel;
    private GameObject m_goNoticePanel;
    private BattleNoticePanel m_noticePanel2;
    private GameObject m_goNoticePanel2;
    private BattleNoticePanel m_noticePanel3;
    private GameObject m_goNoticePanel3;
    private BattleNoticePanel m_noticePanel4;
    private GameObject m_goNoticePanel4;

    //生存模式相关
    private GameObject m_goRevive;//复活按钮
    private GameObject m_goBuyBuff;//买buff按钮
    private GameObject m_goBuyBuff_E;//买buff按钮快捷键E
    private GameObject m_goSurvivalBuffIcon;//buff加成图标
    private GameObject m_goSurvivalBuffIcon_E;//buff加成图标
    private GameObject m_goSurvivalBuffBg;  //buff加成底
    private GameObject m_goSurvivalBuffTxt; //buff加成文字
    private GameObject m_goBuyGun;//买gun按钮
    private GameObject m_goBuyGun_E;//买gun按钮
    private GameObject m_goBuyBullet;//买bullet按钮
    private GameObject m_goBuyBulletTxt;//买bullet按钮
    private GameObject m_goRescue;//救人按钮

    //战斗提示面板
    private GameObject m_goBattleTipBtn;//面板按钮
    private GameObject m_goBattleTipImg;//面板按钮
    private GameObject m_goBattleTipBG;//面板按钮
    private GameObject m_goBattleTipPanelBtn;//面板按钮
    private GameObject m_goBattleTipPanelImg;//面板按钮
    private GameObject m_goBattleTipPanelBG;//面板按钮
    private GameObject m_goBattleTipPanelTxt;//面板按钮
    private string m_strBattleTipTxt;
    private int m_BattleTipTextSize = 0;
    private TweenPosition m_BattleTweenPos = null;

    //爆破模式相关
    private GameObject m_goPlaceBombBtn;
    private GameObject m_goRemoveBombBtn;
    private GameObject m_goHadBombTip = null;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
    private GameObject m_goHadBombTipKey = null;
#endif

    //掉落物相关
    private GameObject m_goPickUpHander;//捡东西
    private GameObject m_goPickUpHanderImg;//捡东西图片
    private EasyImgAnimation spriteAnimation;//动画

    private GameObject m_goDropGunBtn;//扔枪按钮
    private GameObject m_goDropGunImg;//捡东西图片
    private GameObject m_PickUpGunBtn;//显示捡的枪
    private Text m_GunNameText;

    //连杀信息
    private GameObject m_goKillNum;
    private GameObject m_goMuchKillIcon;
    private GameObject m_goBloodIcon;

    //生化模式相关
    private GameObject m_goZombie2Human; //僵尸变人类按钮

    //击杀结算相关
    private GameObject m_harmListTips;
    private GameObject m_beKillGunImg;//被杀的枪
    private Text m_killerNameText;
    private Text m_harmTipGunNameText;

    #region 战斗提示面板
    public void ShowBattleTip(bool isShow = true)
    {
        if (m_goBattleTipPanelBtn != null)
            m_goBattleTipPanelBtn.TrySetActive(isShow);
        if (m_goBattleTipPanelTxt != null)
            m_goBattleTipPanelTxt.TrySetActive(isShow);
        if (m_goBattleTipBtn != null)
            ShowBattleTipBtn(false);
    }

    public void ShowBattleTipBtn(bool isShow = true)
    {
        RectTransformProp prop = m_goBattleTipBtn == null ? new RectTransformProp() { pivot = UIHelper.ONE_X_HALF_Y_VEC, max = UIHelper.ONE_X_HALF_Y_VEC, min = UIHelper.ONE_X_HALF_Y_VEC, size = new Vector2(30, 30) } : null;
        UIHelper.ShowBtn(ref m_goBattleTipBtn, "BattleTipClickBtn", new Vector2(0, -40), onClick = OnClickBattleTipBtn, isShow, prop, m_tran);
        if (m_goBattleTipImg == null && m_goBattleTipBtn != null)
        {
            if (prop == null) prop = new RectTransformProp();
            prop.reset();
            prop.rotation = new Vector3(0, 0, 180);
            UIHelper.ShowSprite(ref m_goBattleTipImg, "Img", AtlasName.Battle, "jiantou", Vector2.zero, isShow, prop, m_goBattleTipBtn.transform);
        }
        if (m_goBattleTipBG == null && m_goBattleTipBtn != null)
        {
            UIHelper.ShowSprite(ref m_goBattleTipBG, "Bg", AtlasName.Battle, "dikuang", Vector2.zero, isShow, null, m_goBattleTipBtn.transform);
        }
    }

    void OnClickBattleTipBtn(GameObject target, PointerEventData eventData)
    {
        ShowBattleTipBtn(false);
        ShowBattleTipPanel(true, true, m_strBattleTipTxt);
    }

    public void ShowBattleTipPanel(bool isShow = true, bool isTween = true, string msg = "", int textSize = -1)
    {
        if (textSize != -1)
            m_BattleTipTextSize = textSize;
        RectTransformProp prop = m_goBattleTipPanelBtn == null ? new RectTransformProp() { pivot = UIHelper.ONE_X_HALF_Y_VEC, max = UIHelper.ONE_X_HALF_Y_VEC, min = UIHelper.ONE_X_HALF_Y_VEC, size = new Vector2(30, 30) } : null;
        if (isShow) UIHelper.ShowBtn(ref m_goBattleTipPanelBtn, "BattleTipClickPanelBtn", new Vector2(0, -40), onClick = OnClickBattleTipPanelBtn, isShow, prop, m_tran);

        if (m_goBattleTipPanelImg == null && m_goBattleTipPanelBtn != null)
        {
            UIHelper.ShowSprite(ref m_goBattleTipPanelImg, "Img", AtlasName.Battle, "jiantou", Vector2.zero, isShow, null, m_goBattleTipPanelBtn.transform);
        }

        if (m_goBattleTipPanelBtn != null)
        {
            TextProp textProp = null;
            if (prop != null) prop.size = new Vector2(m_BattleTipTextSize, 30);
            if (m_goBattleTipPanelTxt == null)
            {
                textProp = new TextProp() { rect = prop, fontSize = 20, anchor = TextAnchor.MiddleLeft, color = new Color(255, 210, 128, 255) / 255, horizontalWrap = HorizontalWrapMode.Wrap };
            }
            var tipText = UIHelper.ShowText(ref m_goBattleTipPanelTxt, msg, isShow, new Vector3(-24, 0), textProp, m_goBattleTipPanelBtn.transform);
            if (prop != null && tipText != null)
            {
                prop.size = new Vector2(m_BattleTipTextSize + 8 + 24, tipText.preferredHeight + 10);
            }
            UIHelper.ShowSprite(ref m_goBattleTipPanelBG, "Bg", AtlasName.Battle, "dikuang", Vector2.zero, isShow, prop, m_goBattleTipPanelBtn.transform, false, Image.Type.Sliced);
            m_goBattleTipPanelBG.GetRectTransform().SetAsFirstSibling();
        }

        
        TweenPanel(isShow, isTween);
    }

    void TweenPanel(bool isShow, bool isEnable)
    {
        if (!isEnable)
            if (m_BattleTweenPos == null)
                return;
            else
                m_BattleTweenPos.enabled = false;

        if (m_BattleTweenPos == null && m_goBattleTipPanelBtn != null)
        {
            m_BattleTweenPos = TweenPosition.Begin(m_goBattleTipPanelBtn, 0.15f, Vector3.zero);
            m_BattleTweenPos.enabled = false;
        }
        if (m_BattleTweenPos == null || m_goBattleTipPanelBtn == null) return;
        Vector3 originPos = m_goBattleTipPanelBtn.transform.localPosition;
        Vector3 targetPos = m_goBattleTipPanelBtn.transform.localPosition;
        Vector3 from = Vector3.zero;
        Vector3 to = Vector3.zero;
        if (isShow)
        {
            to = targetPos;
            targetPos.x += m_BattleTipTextSize;
            from = targetPos;
        }
        else
        {
            from = targetPos;
            targetPos.x += m_BattleTipTextSize;
            to = targetPos;
        }
        m_BattleTweenPos.ResetToBeginning();
        m_BattleTweenPos.from = from;
        m_BattleTweenPos.to = to;
        m_BattleTweenPos.SetOnFinished(() =>
        {
            m_goBattleTipPanelBtn.TrySetActive(isShow);
            m_goBattleTipPanelBtn.transform.localPosition = originPos;
            if (!isShow) ShowBattleTipBtn(true);
        });
        m_BattleTweenPos.enabled = true;
        m_BattleTweenPos.Play(true);
    }

    public void ShowBattleTipMsg(string msg)
    {
        m_strBattleTipTxt = msg;
        if (m_goBattleTipPanelTxt != null) { m_goBattleTipPanelTxt.GetComponent<Text>().text = msg; }
    }

    void OnClickBattleTipPanelBtn(GameObject target, PointerEventData eventData)
    {
        ShowBattleTipPanel(false, true);
    }

    #endregion

    #region 生存与塔防模式相关
    public void ShowReviveBtn(bool isShow = true)
    {
        if((Driver.m_platform==EnumPlatform.pc||Driver.m_platform==EnumPlatform.web)&&GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goRevive, "ReviveBtn", AtlasName.Survival, "PC_revive_btn", new Vector2(270, -115), onClick = OnClickReviveBtn, isShow, null, m_tran);
        else
        UIHelper.ShowBtn(ref m_goRevive, "ReviveBtn", AtlasName.Survival, "revive_btn", new Vector2(270, -115), onClick = OnClickReviveBtn, isShow, null, m_tran);
    }

    void OnClickReviveBtn(GameObject target, PointerEventData eventData)
    {
        if (WorldManager.singleton.isSurvivalTypeModeOpen)
        {
            if (SurvivalBehavior.singleton == null) return;
            if (MainPlayer.singleton.isAlive == false)
                SurvivalBehavior.singleton.OnClickReviveBtn(MainPlayer.singleton.PlayerId);
            else
                SurvivalBehavior.singleton.OnClickCuteBtn();
        }
    }

    public void ShowBuyBuffBtn(bool isShow = true)
    {
        if (isShow)
        {
            pcShortCutKeyECallBack = OnClickBuyBuffBtn;
        }
        else
        {
            if (WorldManager.singleton.isDotaModeOpen)
            {
                if (UIManager.IsOpen<PanelBuyDotaBossBuff>())
                {
                    UIManager.GetPanel<PanelBuyDotaBossBuff>().HidePanel();
                }
            }
            else
            {
                if (UIManager.IsOpen<PanelBuyBuff>())
                {
                    UIManager.GetPanel<PanelBuyBuff>().HidePanel();
                }
            }
        }
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goBuyBuff, "BuyBuffBtn", AtlasName.Survival, "PC_buff2", new Vector2(-200, 0), onClick = OnClickBuyBuffBtn, isShow, null, m_tran);

        else
            UIHelper.ShowBtn(ref m_goBuyBuff, "BuyBuffBtn", AtlasName.Survival, "buff2", new Vector2(-200, 0), onClick = OnClickBuyBuffBtn, isShow, null, m_tran);
    }

    public bool isShowingBuyBuff()
    {
        if (null != m_goBuyBuff && m_goBuyBuff.activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnClickBuyBuffBtn(GameObject target, PointerEventData eventData)
    {
        pcShortCutKeyECallBack = null;

        if (WorldManager.singleton.isDotaModeOpen)
        {
            if (UIManager.IsOpen<PanelBuyDotaBossBuff>())
            {
                UIManager.GetPanel<PanelBuyDotaBossBuff>().HidePanel();
            }
            else
            {
                UIManager.PopPanel<PanelBuyDotaBossBuff>();
            }
        }
        else
        {
            if (UIManager.IsOpen<PanelBuyBuff>())
            {
                UIManager.GetPanel<PanelBuyBuff>().HidePanel();
            }
            else
            {
                UIManager.PopPanel<PanelBuyBuff>();
            }
        }
    }

    public void ShowBuyGunBtn(bool isShow = true)
    {
        if (isShow)
            pcShortCutKeyECallBack = OnClickBuyGunBtn;
        else
            if (UIManager.IsOpen<PanelBattleShop>())
            {
                UIManager.GetPanel<PanelBattleShop>().HidePanel();
            }
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goBuyGun, "BuyGunBtn", AtlasName.Survival, "PC_gun2", new Vector2(-200, 0), onClick = OnClickBuyGunBtn, isShow, null, m_tran);
        else
            UIHelper.ShowBtn(ref m_goBuyGun, "BuyGunBtn", AtlasName.Survival, "gun2", new Vector2(-200, 0), onClick = OnClickBuyGunBtn, isShow, null, m_tran);
    }

    void OnClickBuyGunBtn(GameObject target, PointerEventData eventData)
    {
        pcShortCutKeyECallBack = null;

        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData == null || MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_HERO)
        {
            TipsManager.Instance.showTips("该角色不支持购买");
            return;
        }

        if (UIManager.IsOpen<PanelBattleShop>())
        {
            UIManager.GetPanel<PanelBattleShop>().HidePanel();
        }
        else
        {
            UIManager.PopPanel<PanelBattleShop>();
        }
    }

    public void ShowBuyBulletBtn(bool isShow = true)
    {
        if (isShow)
            pcShortCutKeyECallBack = OnClickBuyBulletBtn;
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goBuyBullet, "BuyBulletBtn", AtlasName.Survival, "PC_bullet", new Vector2(-200, 0), onClick = OnClickBuyBulletBtn, isShow, null, m_tran);
        else
            UIHelper.ShowBtn(ref m_goBuyBullet, "BuyBulletBtn", AtlasName.Survival, "bullet", new Vector2(-200, 0), onClick = OnClickBuyBulletBtn, isShow, null, m_tran);
        if (m_goBuyBullet != null && m_goBuyBulletTxt == null)
        {
            TextProp prop = new TextProp() { fontSize = 18, rect = new RectTransformProp() { size = new Vector2(100, 30) }, anchor = TextAnchor.MiddleLeft, color = Color.white };
            UIHelper.ShowText(ref m_goBuyBulletTxt, ConfigMisc.GetInt("survival_buy_bullet_sp").ToString(), isShow, new Vector2(75, -12), prop, m_goBuyBullet.transform);
        }
    }

    public bool isShowingBuyBullet()
    {
        if (null != m_goBuyBullet && m_goBuyBullet.activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnClickBuyBulletBtn(GameObject target, PointerEventData eventData)
    {
        pcShortCutKeyECallBack = null;
        if (WorldManager.singleton.isSurvivalTypeModeOpen)
        {
            if (SurvivalBehavior.singleton == null) return;
            SurvivalBehavior.singleton.StartBuy(m_tran, 1);
        }
        if (WorldManager.singleton.isDotaModeOpen)
        {
            if (DotaBehavior.singleton == null) return;
            DotaBehavior.singleton.StartBuy(m_tran, 1);
        }
    }

    public void ShowBuffIcon(bool isShow, Transform parent, Vector2 pos, string msg = "")
    {
        RectTransformProp bgProp = null;
        if (m_goSurvivalBuffBg == null) bgProp = new RectTransformProp() { siblingIndex = 10 };
        UIHelper.ShowSprite(ref m_goSurvivalBuffBg, "AddBuff", AtlasName.Survival, "buff_bg", pos, isShow, bgProp, parent);
        RectTransformProp iconProp = null;
        if (m_goSurvivalBuffIcon == null) iconProp = new RectTransformProp() { scale = new Vector3(0.7f, 0.7f) };
        UIHelper.ShowSprite(ref m_goSurvivalBuffIcon, "AddBuffIcon", AtlasName.Survival, "buff_icon", new Vector2(-36, 0), isShow, iconProp, m_goSurvivalBuffBg.transform);
        TextProp textPorp = null;
        if (m_goSurvivalBuffIcon != null && m_goSurvivalBuffTxt == null)
        {
            textPorp = new TextProp() { fontSize = 20, rect = new RectTransformProp() { size = new Vector2(130, 50) }, anchor = TextAnchor.UpperCenter, color = Color.white };
        }
        if (m_goSurvivalBuffIcon != null) UIHelper.ShowText(ref m_goSurvivalBuffTxt, msg, isShow, new Vector2(10, -16), textPorp, m_goSurvivalBuffBg.transform);
    }
    #endregion

    #region 救人模式相关
    public void ShowRescueBtn(bool isShow = true)
    {
        Logger.Log("rescuebtn  = " + isShow);
        if (isShow)
            pcShortCutKeyECallBack = OnClickRescueBtn;
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goRescue, "RescueBtn", AtlasName.BattleNew, "PC_rescue_btn", new Vector2(-266, 0), onClick = OnClickRescueBtn, isShow, null, m_tran);
        else
            UIHelper.ShowBtn(ref m_goRescue, "RescueBtn", AtlasName.Battle, "rescue_btn", new Vector2(-266, 0), onClick = OnClickRescueBtn, isShow, null, m_tran);
    }

    public void CheckShowRescueBtn(int id)
    {
        pcShortCutKeyECallBack = null;
        if (RescueBehavior.singleton == null) return;
        if (RescueBehavior.singleton.beRescuedList.Contains(id))
        {
            ShowRescueBtn(false);
        }
        else
        {
            bool boo = notTimeRescue(id);
            ShowRescueBtn(boo);
        }
    }

    private bool notTimeRescue(int playerId)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(playerId);
        int teampLeftTime = 0;
        if (player != null && player.transform != null)
        {
            GameObject playerGo = player.transform.Find("goDyingGameObject").gameObject;
            if( playerGo != null )
            {
                var rescueTip = playerGo.GetComponent<UIRescueTip>();
                if( rescueTip != null )
                {
                   teampLeftTime = int.Parse(rescueTip.LeftTime().ToString("f0"));
                    int rescueTime = gameRuleLine == null ? 0 : gameRuleLine.RescueNeedTime;
                    if( teampLeftTime < rescueTime)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
        
        /*if (m_tran.Find("DirectionTip") != null && m_tran.Find("DirectionTip/Offset") != null)
        {
            if (m_tran.Find("DirectionTip/Offset").GetComponent<ImageDownWatch>() != null)
            {
                int rescueTime = gameRuleLine == null ? 0 : gameRuleLine.RescueNeedTime;
                int leftTime = int.Parse(m_tran.Find("DirectionTip/Offset").GetComponent<ImageDownWatch>().m_fTime.ToString());
                Logger.Log("leftTime  =  " + leftTime + " rescueTime =  " + rescueTime);
                if (leftTime >= rescueTime)
                {
                    return true;
                }
            }
        }
        return false;*/
    }

    void OnClickRescueBtn(GameObject target, PointerEventData eventData)
    {
        RescueBehavior.singleton.StartRescue(m_tran, m_iBeRescuedPlayerID);
        ShowRescueBtn(false);
    }

    #endregion

    #region 爆破模式相关
    public void ShowPlaceBombBtn(bool isShow = true)
    {
        if (isShow)
            pcShortCutKeyECallBack = OnClickPlaceBombBtn;
        else
            pcShortCutKeyECallBack = null;
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goPlaceBombBtn, "PlaceBomb", AtlasName.BattleNew, "PC_placebomb", new Vector2(-266, 0), onClick = OnClickPlaceBombBtn, isShow, null, m_tran);
        else
            UIHelper.ShowBtn(ref m_goPlaceBombBtn, "PlaceBomb", AtlasName.Battle, "placebomb", new Vector2(-266, 0), onClick = OnClickPlaceBombBtn, isShow, null, m_tran);
    }

    void OnClickPlaceBombBtn(GameObject target, PointerEventData eventData)
    {
        if (BurstBehavior.singleton != null)
        {
            BurstBehavior.singleton.PlaceBomb(m_tran, m_iBurstPoint);
        }
    }

    public void ShowRemoveBombBtn(bool isShow = true)
    {
        if (isShow)
            pcShortCutKeyECallBack = OnClickRemoveBombBtn;
        else
            pcShortCutKeyECallBack = null;
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
            UIHelper.ShowBtn(ref m_goRemoveBombBtn, "RemoveBomb", AtlasName.BattleNew, "PC_delbomb", new Vector2(-266, 0), onClick = OnClickRemoveBombBtn, isShow, null, m_tran);
        else
            UIHelper.ShowBtn(ref m_goRemoveBombBtn, "RemoveBomb", AtlasName.Battle, "delbomb", new Vector2(-266, 0), onClick = OnClickRemoveBombBtn, isShow, null, m_tran);
    }

    void OnClickRemoveBombBtn(GameObject target, PointerEventData eventData)
    {
        if (BurstBehavior.singleton != null)
        {
            BurstBehavior.singleton.UnPlaceBomb(m_tran, m_iBurstPoint);
        }
    }

    void ShowHoldBombTipSprite(bool isShow = true, bool clickAble = false)
    {
        UGUIClickHandler.PointerEvetCallBackFunc callBack = null;
        if (clickAble)
            callBack = OnClickDropC4;
        RectTransformProp prop = null;
        if (m_goHadBombTip == null) prop = new RectTransformProp() { max = UIHelper.ZERO_X_HALF_Y_VEC, min = UIHelper.ZERO_X_HALF_Y_VEC };
        UIHelper.ShowBtn(ref m_goHadBombTip, "HadBombTip", AtlasName.Battle, "c4", new Vector2(200, 230), onClick = callBack, isShow, prop, m_tran);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        UIHelper.ShowSprite(ref m_goHadBombTipKey, "f8_key", AtlasName.BattleNew, "f8_key", new Vector2(11, -20), isShow, null, m_goHadBombTip != null ? m_goHadBombTip.transform : m_tran);
#endif
    }

    #endregion

    #region 生化模式相关
    public void ShowZombie2HumanBtn(bool isShow = true, Vector2 pos = default(Vector2))
    {
        if (isShow)
            pcShortCutKeyECallBack = OnClickZombie2Human;
        else
            pcShortCutKeyECallBack = null;
        UIHelper.ShowBtn(ref m_goZombie2Human, "ombie2HumanBtn", AtlasName.BattleNew, "zombie2human_pill", pos, onClick = OnClickZombie2Human, isShow, null, m_tran);
    }

    void OnClickZombie2Human(GameObject target, PointerEventData eventData)
    {
        pcShortCutKeyECallBack = null;
        GameDispatcher.Dispatch(ZombieUltimateEvent.START_ZOMBIE2HUMAN);
    }
    #endregion

    #region 掉落物相关
    public void ShowPickUpHander(bool isShow = true, Sprite sprite = null)
    {
        RectTransformProp prop = null;
        if (m_goPickUpHander == null) prop = new RectTransformProp();
        UIHelper.ShowBtn(ref m_goPickUpHander, "pickuphander", new Vector2(310, 160), onClick = OnClickPickupGun, isShow, prop, m_tran);
        bool isAnimation = sprite == null;
        if (prop != null) prop.pivot = UIHelper.ZERO_X_HALF_Y_VEC;
        if (m_goPickUpHander != null) UIHelper.ShowSprite(ref m_goPickUpHanderImg, "pickuphanderImg", sprite, new Vector2(-80, 0), isShow & !isAnimation, prop, m_goPickUpHander.transform);
        if (m_goPickUpHander != null && spriteAnimation == null)
        {
            spriteAnimation = m_goPickUpHander.AddMissingComponent<EasyImgAnimation>();
            Sprite[] listSprite = { ResourceManager.LoadSprite(AtlasName.Battle, "hander"), ResourceManager.LoadSprite(AtlasName.Battle, "pickuphander") };
            spriteAnimation.SetImgList(listSprite, 6);
        }
        if (m_goPickUpHander != null) m_goPickUpHander.GetComponent<Image>().enabled = isShow & isAnimation;
        if (spriteAnimation != null) spriteAnimation.enabled = isShow & isAnimation;
    }

    void OnClickPickupGun(GameObject target, PointerEventData eventData)
    {
        DropItemManager.singleton.PickUpDropItem();
        ShowPickUpHander(false);
    }

    public void ShowDropGunBtn(bool isShow = true)
    {
        //UIHelper.ShowBtn(ref m_goDropGunBtn, "DropGunBtn", AtlasName.Battle, "rescue_btn", new Vector2(270, -115), onClick = OnClickDropGun, isShow, null, m_tran);
        if (m_goDropGunBtn != null)
        {
            m_goDropGunBtn.TrySetActive(isShow && isShowUIExcPC);
        }
        
        //Logger.Error("showdropgun: "+isShow);
    }

    public void ShowDropGunImg(bool isShow = true, Sprite sprite = null,string gunName = "")
    {
        if (m_PickUpGunBtn != null)
        {
            m_PickUpGunBtn.TrySetActive(isShow);
            m_GunNameText.text = gunName;
            // RectTransformProp prop = new RectTransformProp() { scale = new Vector3(0.7f, 0.7f, 1) };
            RectTransformProp prop = new RectTransformProp() { scale = new Vector3(1.3f, 1.3f, 1) };
            UIHelper.ShowSprite(ref m_goDropGunImg, "DropGunImg", sprite, new Vector2(0, 0), isShow, prop, m_PickUpGunBtn.transform, true);
        }
    }

    /// <summary>
    /// 捡抢
    /// </summary>
    /// <param name="target"></param>
    /// <param name="eventData"></param>
    void OnClickPickUpGun(GameObject target, PointerEventData eventData)
    {
        DropItemManager.singleton.PickUpDropItem();
    }

    void OnClickDropGun(GameObject target, PointerEventData eventData)
    {
        if (DropGunBehavior.singleton != null) DropGunBehavior.singleton.OnDropGun();
    }

    void OnClickDropC4(GameObject target, PointerEventData eventData)
    {
        if (DropGunBehavior.singleton != null) DropGunBehavior.singleton.OnDropC4();
    }
    #endregion

    #region 连杀信息
    public void ShowMuchKillMsg(int killNum)
    {
        if (m_muchKillMsg == null || m_muchKillMsg.gameObject == null) return;
        if (killNum < 2)
        {
            m_muchKillMsg.gameObject.TrySetActive(false);
            return;
        }
        m_muchKillMsg.gameObject.TrySetActive(true);
        var infect = MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE;
        if (killNum < 10)
        {
            UIHelper.ShowSprite(ref m_goBloodIcon, "BloodIcon", AtlasName.BattleNew, infect ? "infectBlood" : "killBlood", new Vector2(-6, 11), true, null, m_muchKillMsg, true);
            UIHelper.ShowSprite(ref m_goKillNum, "KillNum", AtlasName.Battle, (infect ? "infect" : "kill") + killNum, new Vector2(-6, 11), true, null, m_muchKillMsg, true);
            UIHelper.ShowSprite(ref m_goMuchKillIcon, "KillIcon", infect ? AtlasName.Battle : AtlasName.BattleNew, infect ? "infect_txt" : "continue_kill_tip", new Vector2(0, -15), true, null, m_muchKillMsg, true);
        }
        else
        {
            UIHelper.ShowSprite(ref m_goBloodIcon, "BloodIcon", AtlasName.BattleNew, infect ? "infectBlood" : "killBlood", new Vector2(-12, -1), true, null, m_muchKillMsg, true);
            UIHelper.ShowSprite(ref m_goKillNum, "KillNum", AtlasName.Battle, (infect ? "infect" : "kill") + killNum, new Vector2(-6, 11), false, null, m_muchKillMsg, true);
            UIHelper.ShowSprite(ref m_goMuchKillIcon, "KillIcon", infect ? AtlasName.Battle : AtlasName.BattleNew, infect ? "infect_txt" : "oh_god_icon", new Vector2(11, -5), true, null, m_muchKillMsg, true);
        }
    }

    public void ClearKillMsg()
    {
        if (m_goBloodIcon)
            m_goBloodIcon.TrySetActive(false);
        if (m_goKillNum)
            m_goKillNum.TrySetActive(false);
        if (m_goMuchKillIcon)
            m_goMuchKillIcon.TrySetActive(false);
    }

    #endregion

    #region 其他
    public void PlayChangeRoleNotice(BasePlayer player, int roleId)
    {
        NoticePanelProp prop = new NoticePanelProp();
        //底图
        prop.bgAltas = AtlasName.BattleNew;
        prop.bgName = "black_bg";
        prop.bgSize = new Vector2(382, 48);
        prop.txtPos = new Vector2(0, 220);
        //玩家名字
        prop.msg = player.ColorName + "变身为" + ConfigManager.GetConfig<ConfigItemRole>().GetLine(roleId).DispName;
        prop.textProp = new TextProp() { fontSize = 18 };

        PlayNoticePanelQueue(null, 3, false, new Vector2(0, 220), prop);
    }


    void UnshowSomeBtn()
    {
        ShowPlaceBombBtn(false);

        ShowRemoveBombBtn(false);

        ShowRescueBtn(false);
    }

    public void PlayProgressBar(string bgAtlas, string bg, string progressAtlas, string progress, string msg = "", float duration = 1, ProgressBarProp prop = null, string name = "",string tool = "")
    {
        name += "ProgressBar";
        if (m_goProgressBar == null)
        {
            m_goProgressBar = new GameObject(name);
            m_goProgressBar.transform.SetParent(m_tranRoomTip != null ? m_tranRoomTip : m_tran, false);
            m_progressBar = m_goProgressBar.AddMissingComponent<UIProgressBar>();
            m_goProgressBar.AddMissingComponent<RectTransform>();
        }
        m_goProgressBar.name = name;
        m_progressBar.play(bgAtlas, bg, progressAtlas, progress, msg, duration, prop, tool);
    }

    public void StopProgressBar(string name)
    {
        name += "ProgressBar";
        if (m_goProgressBar == null || m_progressBar == null || m_goProgressBar.name != name) return;
        m_progressBar.stop();
        m_goProgressBar.TrySetActive(false);

    }
    //通告面板1
    public void PlayNoticePanel(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null,Vector3 scale=default(Vector3))
    {
        if (!IsInited()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel == null)
        {
            m_goNoticePanel = new GameObject("NoticePanel");
            m_goNoticePanel.AddComponent<CanvasGroup>().blocksRaycasts = false;
            m_goNoticePanel.transform.SetParent(m_tranRoomTip != null ? m_tranRoomTip : m_tran, false);
            m_goNoticePanel.AddComponent<RectTransform>();
            m_noticePanel = m_goNoticePanel.AddMissingComponent<BattleNoticePanel>();
        }
        if (scale == default(Vector3))
        {
            m_goNoticePanel.transform.localScale = Vector3.one;
        }
        else
        {
            m_goNoticePanel.transform.localScale = scale;
        }
        m_noticePanel.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanel(string imgAtlas, string img, float duration = 3, Vector2 pos = default(Vector2), NoticePanelProp prop = null, bool voice = true, Vector3 scale = default(Vector3))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanel(sprite, duration, voice, pos, prop, scale);
    }

    public void StopNoticePanel()
    {
        if (m_noticePanel != null) m_noticePanel.stop();
    }
    //通告面板2
    public void PlayNoticePanel2(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector3 scale = default(Vector3), string headIcon = "",string roleName="")
    {
        if (!IsInited()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel2 == null)
        {
            m_goNoticePanel2 = new GameObject("NoticePanel2");
            m_goNoticePanel2.transform.SetParent(m_tranRoomTip != null ? m_tranRoomTip : m_tran, false);
            m_goNoticePanel2.AddComponent<RectTransform>();
            m_noticePanel2 = m_goNoticePanel2.AddMissingComponent<BattleNoticePanel>();
        }
        if (scale == default(Vector3))
        {
            m_goNoticePanel2.transform.localScale = Vector3.one;
        }
        else
        {
            m_goNoticePanel2.transform.localScale = scale;
        }
        m_noticePanel2.play(sprite, duration, voice, pos, prop, headIcon, roleName);
    }

    public void PlayNoticePanel2(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector3 scale = default(Vector3), string headIcon = "", string roleName="")
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanel2(sprite, duration, voice, pos, prop, scale, headIcon, roleName);
    }

    public void StopNoticePanel2()
    {
        if (m_noticePanel2 != null) m_noticePanel2.stop();
    }
    //通告面板3(加入队列)
    public void PlayNoticePanelQueue(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        if (!IsInited()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel3 == null)
        {
            m_goNoticePanel3 = new GameObject("NoticePanelQueue");
            m_goNoticePanel3.transform.SetParent(m_tranRoomTip != null ? m_tranRoomTip : m_tran, false);
            //设置通知面板组件不接受点击事件，防止组件拦截下面的操作按钮组件事件
            var canvas = m_goNoticePanel3.AddComponent<CanvasGroup>();
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            AddRectTransform(m_goNoticePanel3, rootPos);
            m_noticePanel3 = m_goNoticePanel3.AddMissingComponent<BattleNoticePanel>();
            m_noticePanel3.useQueue = true;
        }
        m_noticePanel3.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanelQueue(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanelQueue(sprite, duration, voice, pos, prop, rootPos);
    }

    public void PlayNoticePanelQueue2(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        if (!IsInited()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel4 == null)
        {
            m_goNoticePanel4 = new GameObject("NoticePanelQueue");
            m_goNoticePanel4.transform.SetParent(m_tranRoomTip != null ? m_tranRoomTip : m_tran, false);
            //设置通知面板组件不接受点击事件，防止组件拦截下面的操作按钮组件事件
            var canvas = m_goNoticePanel4.AddComponent<CanvasGroup>();
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            AddRectTransform(m_goNoticePanel4, rootPos);
            m_noticePanel4 = m_goNoticePanel4.AddMissingComponent<BattleNoticePanel>();
            m_noticePanel4.useQueue = true;
        }
        m_noticePanel4.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanelQueue2(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanelQueue2(sprite, duration, voice, pos, prop, rootPos);
    }

    public void StopNoticePanelQueue()
    {
        if (m_noticePanel3 != null) m_noticePanel3.stop();
    }

    private void AddRectTransform(GameObject go, Vector2 pos)
    {
        go.AddComponent<RectTransform>().anchoredPosition = pos;
    }

    /// <summary>
    /// 显示、隐藏伤害面板
    /// </summary>
    /// <param name="isShow"></param>
    public void ShowHarmInfoListTip(bool isShow)
    {
        m_harmListTips.TrySetActive(isShow);
        if( isShow == false )
        {
            for( int i = 0; i < m_harmTextList.Length;i++ )
            {
                m_harmTextList[i].TrySetActive(false);
            }
            m_harmTipGunNameText.text = "";
            m_killerNameText.text = "";
        }
    }

    /// <summary>
    /// 显示击杀统计数据
    /// </summary>
    /// <param name="name"></param>
    /// <param name="harm"></param>
    public void SetHarmListInfo(int index,string name, string harm,int part,int killByMyself)
    {
        m_harmTextList[index].TrySetActive(true);
        m_harmTextList[index].transform.Find("name_txt").GetComponent<Text>().text = name;
        m_harmTextList[index].transform.Find("harm_txt").GetComponent<Text>().text = harm;
        string icon_str = "";
        if( part==BasePlayer.BODY_PART_ID_HEAD )
        {
            icon_str = "killhead";
        }
        if (!string.IsNullOrEmpty(icon_str) && killByMyself == 1)
        {
            m_harmTextList[index].transform.Find("partIcon").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, icon_str));
            m_harmTextList[index].transform.Find("partIcon").gameObject.TrySetActive(true);
        }
        else
        {
            m_harmTextList[index].transform.Find("partIcon").GetComponent<Image>().SetSprite(null);
            m_harmTextList[index].transform.Find("partIcon").GetComponent<Image>().gameObject.TrySetActive(false);
        }
    }

    /// <summary>
    /// 显示被杀信息
    /// </summary>
    /// <param name="killerName"></param>
    /// <param name="weapon"></param>
    public void BeKillInfo(string killerName, int weapon)
    {
        m_killerNameText.text = killerName;
        Sprite sprite = null;
        sprite = ResourceManager.LoadWeaponIcon(weapon);
        ConfigItemWeaponLine weaponLine = ItemDataManager.GetItemWeapon(weapon);
        if (weaponLine == null)
        {
            Logger.Log("id = " + weapon + "武器配置数据为null");
        }
        m_harmTipGunNameText.text =  weaponLine.DispName;
        RectTransformProp prop = new RectTransformProp() { scale = new Vector3(1f, 1f, 1f) };
        UIHelper.ShowSprite(ref m_beKillGunImg, "beKillGunImg", sprite, new Vector2(0, 103f), sprite == null ?false:true, prop, m_harmListTips.transform, true);
    }

    #endregion
}

