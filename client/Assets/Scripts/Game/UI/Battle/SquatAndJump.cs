﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SquatAndJump : MonoBehaviour
{

    // Use this for initialization


    enum SQUAT_DEFINE
    {
        CLICK,          ///点击下蹲
        PRESS,          /// 按住下蹲

    }

    bool m_squatSwitch;
    GameObject m_goSquatBtn;
    bool flyMode;
    void Start()
    {
        m_squatSwitch = false;

        if (gameObject.name == "Jump" || gameObject.name == "LeftJump")
        {
            UGUIUpDownHandler.Get(gameObject).onPointerDown += OnJump;
            UGUIUpDownHandler.Get(gameObject).onPointerUp += OnJumpEnd;
        }
        flyMode = MainPlayer.singleton.canFly;
        if (flyMode && gameObject.name == "Jump" || gameObject.name == "LeftJump")
        {
            gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "flybtn"));
            gameObject.GetComponent<Image>().SetNativeSize();
            if (gameObject.name == "Jump")
            {
                gameObject.GetComponent<Toggle>().transition = Selectable.Transition.ColorTint;
            }
            else
            {
                gameObject.GetComponent<Button>().transition = Selectable.Transition.ColorTint;
            }
        }
        if (!flyMode && gameObject.name == "Jump" || gameObject.name == "LeftJump")
        {
            gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "jump_btn"));
            gameObject.GetComponent<Image>().SetNativeSize();
            if (gameObject.name == "Jump")
            {
                gameObject.GetComponent<Toggle>().transition = Selectable.Transition.SpriteSwap;
            }
            else
            {
                gameObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
            }
        }



        if (gameObject.name == "Squat")
        {
            UGUIUpDownHandler.Get(gameObject).onPointerDown += OnSquat;
            UGUIUpDownHandler.Get(gameObject).onPointerUp += OnStand;

            GameDispatcher.AddEventListener(GameEvent.INPUT_JUMP, OnJumpHappen);
            GameDispatcher.AddEventListener(GameEvent.INPUT_STAND, OnStandHappen);
            m_goSquatBtn = gameObject;
        }

        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener(GameEvent.PLAYER_FLY_POWER_OFF, OnJumpEndHappen);
        GameDispatcher.AddEventListener(GameEvent.PLAYER_CHECK_FLY, OnCheckFly);

    }


    private void OnCheckFly()
    {
        flyMode = MainPlayer.singleton.canFly;
        if (flyMode)
        {
            if (gameObject.name == "Jump" || gameObject.name == "LeftJump")
            {
                gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "flybtn"));
                gameObject.GetComponent<Image>().SetNativeSize();
                if (gameObject.name == "Jump")
                {
                    gameObject.GetComponent<Toggle>().transition = Selectable.Transition.ColorTint;
                }
                else
                {
                    gameObject.GetComponent<Button>().transition = Selectable.Transition.ColorTint;
                }
            }
        }
        else
        {
            if (gameObject.name == "Jump" || gameObject.name == "LeftJump")
            {
                gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "jump_btn"));
                gameObject.GetComponent<Image>().SetNativeSize();
                if (gameObject.name == "Jump")
                {
                    gameObject.GetComponent<Toggle>().transition = Selectable.Transition.SpriteSwap;
                }
                else
                {
                    gameObject.GetComponent<Button>().transition = Selectable.Transition.SpriteSwap;
                }
            }
        }
    }
    private void OnStandHappen()
    {
        if (gameObject.name == "Squat")
            gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "squat_btn"));
    }

    // Update is called once per frame
    void OnJump(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton.status.crouch)
        {
            GameDispatcher.Dispatch(GameEvent.INPUT_STAND);

        }
        else
        {
            if (!MainPlayer.singleton.skillMgr.flm.isflyup())
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_JUMP);
                if (flyMode)
                {

                    gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "flybtn_down"));
                    //GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_UP);
                }
            }
            else
            {
                GameDispatcher.Dispatch(GameEvent.INPUT_JUMP_END);
                if (flyMode)
                {
                    gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "flybtn"));
                    //GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_END);
                }
            }
        }
    }

    void OnJumpEnd(GameObject target, PointerEventData eventData)
    {
        //GameDispatcher.Dispatch(GameEvent.INPUT_JUMP_END);
    }

    void OnSquat(GameObject target, PointerEventData eventData)
    {
        GameDispatcher.Dispatch(GameEvent.INPUT_CROUCH);
        m_squatSwitch = !m_squatSwitch;
    }

    void OnStand(GameObject target, PointerEventData eventData)
    {
        if ((SQUAT_DEFINE)GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_SQUAT) == SQUAT_DEFINE.CLICK && m_squatSwitch)
        {
            Sprite pressSprite = ResourceManager.LoadSprite(AtlasName.BattleNew, "squat_down_btn");
            target.GetComponent<Image>().SetSprite(pressSprite);

        }
        else
        {
            target.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "squat_btn"));
            GameDispatcher.Dispatch(GameEvent.INPUT_STAND);
        }
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_JUMP, OnJumpHappen);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_STAND, OnStandHappen);
        GameDispatcher.RemoveEventListener(GameEvent.PLAYER_CHECK_FLY, OnCheckFly);
        GameDispatcher.RemoveEventListener(GameEvent.PLAYER_FLY_POWER_OFF, OnJumpEndHappen);
        
    }

    void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            gameObject.TrySetActive(false);
        }
    }

    void OnPlayerRevive(string eventName, BasePlayer who)
    {
        if (who == MainPlayer.singleton)
        {
            gameObject.TrySetActive(true);
            if (MainPlayer.singleton.Camp == 4)
            {
                gameObject.TrySetActive(false);
            }
            if (MainPlayer.singleton.canFly)
            {
                MainPlayer.singleton.skillMgr.flm.flyup = false;
            }
        }
    }

    void OnJumpHappen()
    {
        if (m_goSquatBtn != null)
        {
            if ((SQUAT_DEFINE)GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_SQUAT) == SQUAT_DEFINE.CLICK && m_squatSwitch)
            {
                //m_goSquatBtn.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "squat"));
                m_squatSwitch = !m_squatSwitch;
            }
        }
    }

    void OnJumpEndHappen()
    {
        if (flyMode)
        {
            if (gameObject.name == "Jump" || gameObject.name == "LeftJump")
            {
                gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "flybtn"));
            }
            return;
            //GameDispatcher.Dispatch(GameEvent.PLAYER_FLY_END);
        }
        if (gameObject.name == "Jump" || gameObject.name == "LeftJump")
        {
            gameObject.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "jump_btn"));
        }
    }
}

