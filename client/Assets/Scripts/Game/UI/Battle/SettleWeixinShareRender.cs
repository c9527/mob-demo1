﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public class SettleWeixinShareRender : ItemRender
{
    static public int MaxAdd;
    static public int MaxSub;

    SettleWeixinPlayer _data;
    Image m_headImage;
    Text m_death_kill;

    public override void Awake()
    {
        m_headImage = transform.Find("head_image").GetComponent<Image>();
        m_death_kill = transform.Find("death_kill").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        _data = data as SettleWeixinPlayer;
        m_headImage.enabled = _data != null;
        m_death_kill.enabled = _data != null;

        if (_data == null)
        {
            return;
        }
        bool isMyself = _data.uid == PlayerSystem.roleId;
        m_headImage.SetSprite(ResourceManager.LoadRoleIcon(_data.icon, _data.camp));
        m_headImage.SetNativeSize();
        string color = isMyself ? "#86fd83" : _data.camp == 1 ? "#ffca95" : "#ace5ff";
        m_death_kill.text = string.Format("<color='{0}'>{1}消灭{2}死</color>", color, _data.kill, _data.death);
        isMyself = false;
    }
}