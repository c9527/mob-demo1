﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class WeaponSkillItemRender : SkillItemRender
{
    protected override void OnSetData(object data)
    {
        if (m_renderData == null)
        {
            return;
        }

        TDWeaponAwakenProp awakenInfo = m_renderData as TDWeaponAwakenProp;
        if (awakenInfo == null)
        {
            Logger.Error("Data is null");
            return;
        }

        _isSetData = true;

        if (_last_time == -1)
        {
            frozen = true;
        }

        var info = ConfigManager.GetConfig<ConfigSkill>().GetLine(int.Parse(awakenInfo.Param));

        SetKillInfo(awakenInfo.Id, info);
    }

    public override void UseSkill()
    {
        if (isSetData != null && frozen == false)
        {
            PlayerBattleModel.Instance.TosUseWeaponSkill(skillId);
            PlayerBattleModel.Instance.bag_selected = true;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
            GameDispatcher.Dispatch<bool>(PanelGuidePC.EVENT_STEP_USESKILL, false);
#endif
        }
    }
}

