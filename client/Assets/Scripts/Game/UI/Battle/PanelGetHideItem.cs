﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;


public class PanelGetHideItem : BasePanel
{
    enum ROLLER_STATE
    {
        START,
        DOING,
        STOPING,
        SlOWING,
        STOP,
    }


    public class hideitemdata
    {
        public int id;
        public int itemType;
    }


    public class ItemHide : ItemRender
    {
        public int m_id;
        public Image m_item;
        ConfigItemRole m_Cri;
        ConfigHideBuffInfo hideBuffInfoConfig;
        public int itemType;
        public override void Awake()
        {
            m_Cri = ConfigManager.GetConfig<ConfigItemRole>();
            hideBuffInfoConfig = ConfigManager.GetConfig<ConfigHideBuffInfo>();
            m_id = 0;
            itemType = 1;
            m_item = transform.Find("Image").GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            var info = data as hideitemdata;
            itemType = info.itemType;
            setImage(info.id);
            m_id = info.id;
        }

        public void setImage(int id)
        {
            string name = "";
            if( itemType == 1 )
            {
                ConfigItemRoleLine ccl = m_Cri.GetLine(id);
                if (ccl != null)
                {
                    name = m_Cri.GetLine(id).Icon;
                    m_item.SetSprite(ResourceManager.LoadHideIcon(name));
                }
                else
                {
                    Logger.Warning("配置表中找不到该项，请检查配置表");
                }
                m_item.gameObject.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            }
            else
            {
                ConfigHideBuffInfoLine buffInfoLine = hideBuffInfoConfig.GetLine(id);
                if (buffInfoLine != null)
                {
                    name = buffInfoLine.BuffIcon;
                    m_item.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle,name));
                }
                else
                {
                    Logger.Warning("配置表中找不到该项，请检查配置表 id = "+ id);
                }
                m_item.gameObject.transform.localScale = Vector3.one;
            }
            m_item.SetNativeSize();
        }
    }

    DataGrid[] m_itemlists;
    Image[] m_timeImgs;
    int m_itemType = 1;
    GameObject m_stopbtn;
    Text m_stopbtntxt;
    GameObject m_againSpendImgGo;
    Text m_againSpendText;
    GameObject[] m_choosebtns;
    int[] m_chooseId;
    float[] m_speed;
    ROLLER_STATE[] m_rollerstate;
    int[] m_stopaims;
    float m_maxspeed;
    float m_minspeed;
    float m_addspeed;
    float m_dropspeed;
    float m_rollingtime;
    float m_starttime;
    float m_starty;
    float m_paneltime;
    int m_rollerNum;
    bool m_again;
    bool m_show;
    Image m_btnImg;
    int m_disguiseTime;
    Text m_text;
    GameObject m_text3Icon;
    int m_diamondCost;
    GameObject m_lockImg;
    private const int ITEM_COUNT = 9;
    private Text m_info1;
    private Text m_info2;
    bool m_haveCardItem;

    public PanelGetHideItem()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelGetHideItem");
        AddPreLoadRes("UI/Battle/PanelGetHideItem", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
    }

    public override void Init()
    {
        m_show = false;
        m_rollingtime = 30f;
        m_stopaims = new int[3];
        m_rollerNum = 3;
        m_rollerstate = new ROLLER_STATE[3];
        for (int i = 0; i < 3; i++)
        {
            m_rollerstate[i] = ROLLER_STATE.STOP;
        }
        m_speed = new float[3];
        m_itemlists = new DataGrid[3];
        m_timeImgs = new Image[3];
        m_choosebtns = new GameObject[3];
        m_chooseId = new int[3];
        m_text = m_tran.Find("choose3/Text").GetComponent<Text>();
        m_text3Icon = m_tran.Find("choose3/icon").gameObject;
        GameObject go = m_tran.Find("ItemHide").gameObject;
        for (int i = 0; i < 3; i++)
        {
            m_chooseId[i] = 0;
            m_timeImgs[i] = m_tran.Find("time/time" + (i + 1).ToString()).GetComponent<Image>();
            m_itemlists[i] = m_tran.Find("tigerbg/mask/roller" + (i + 1).ToString() + "/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
            m_choosebtns[i] = m_tran.Find("choose" + (i + 1).ToString()).gameObject;
            m_itemlists[i].SetItemRender(go, typeof(ItemHide));
        }
        m_stopbtn = m_tran.Find("StopOrTry").gameObject;
        m_btnImg = m_stopbtn.GetComponent<Image>();
        m_stopbtntxt = m_tran.Find("StopOrTry/Text").GetComponent<Text>();
        m_againSpendImgGo = m_tran.Find("StopOrTry/icon").gameObject;
        m_againSpendText = m_tran.Find("StopOrTry/money").GetComponent<Text>();
        m_lockImg = m_tran.Find("lock").gameObject;

        m_info1 = m_tran.Find("info1").GetComponent<Text>();
        m_info2 = m_tran.Find("info2").GetComponent<Text>();
        
        ConfigRollerSpeed rs = ConfigManager.GetConfig<ConfigRollerSpeed>();
//         m_maxspeed = rs.m_dataArr[0].MaxSpeed;
//         m_minspeed = rs.m_dataArr[0].MinSpeed;
//         m_addspeed = rs.m_dataArr[0].AddSpeed;
//         m_dropspeed = rs.m_dataArr[0].DropSpeed;

        m_maxspeed = 0.07f;
        m_minspeed = 0.004f;
        m_addspeed = 0.07f;
        m_dropspeed = 0.004f;
        m_starty = m_itemlists[0].gameObject.transform.localPosition.y;
        m_starttime = -1f;
        GetConfig();

    }


    void GetConfig()
    {
        ConfigHideDisguise configs = ConfigManager.GetConfig<ConfigHideDisguise>();
        foreach (var item in configs.m_dataArr)
        {
            if (item.DiamondCost != 0 && item.Map == SceneManager.singleton.curMapId)
            {
                m_diamondCost = item.DiamondCost;
            }
        }
    }

    void InitGrid()
    {
        hideitemdata[] items = new hideitemdata[ITEM_COUNT+3];

        for (int i = 0; i < items.Length; i++)
        {
            if (m_itemType == 1)//物件
            {
                if (i >= ITEM_COUNT)
                {
                    items[i] = new hideitemdata { id = i + 4101 - ITEM_COUNT, itemType = m_itemType };
                }
                else
                {
                    items[i] = new hideitemdata { id = i + 4101, itemType = m_itemType };
                }
                  
            }
            else//buff
            {
                if (i >= ITEM_COUNT)
                {
                    items[i] = new hideitemdata { id = i - 2, itemType = m_itemType };
                }
                    
                else
                {
                    items[i] = new hideitemdata { id = (i + 1), itemType = m_itemType };
                }
                   
            }
            
        }
        for (int i = 0; i < 3; i++)
        {
            items.Random();;
            m_itemlists[i].Data = items;
        }
    }

    void checkStopBtn(bool bo)
    {
        if (bo)
        {
            Util.SetNormalShader(m_btnImg);
        }
        else
        {
            Util.SetGrayShader(m_btnImg);
        }
    }



    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_stopbtn).onPointerClick += OnStopClick;
        UGUIClickHandler.Get(m_choosebtns[0]).onPointerClick += click;
        UGUIClickHandler.Get(m_choosebtns[1]).onPointerClick += click2;
        UGUIClickHandler.Get(m_choosebtns[2]).onPointerClick += click3;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        AddEventListener<int>(GameEvent.INPUT_KEY_NUM_DOWN, (num) =>
        {
            if (num == 1)
            {
                click(null, null);
            }
            else if (num == 2)
            {
                click2(null, null);
            }
            else if (num == 3)
            {
                click3(null, null); 
            }
        });
        //AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () => { click(null,null); });
        //AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () => { click2(null, null); });
        //AddEventListener(GameEvent.INPUT_KEY_3_DOWN, () => { click3(null, null); });
        AddEventListener(GameEvent.INPUT_KEY_R_DOWN, () =>
        {
            OnStopClick(null,null);
        });
#endif
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
        if(m_isRolling)
            AudioManager.StopUISoundLoop();
    }

    public override void OnHide()
    {
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        // if (UIManager.IsBattle())
        //Screen.lockCursor = true;
#endif
        if (UIManager.GetPanel<PanelBattle>() != null)
        {
            UIManager.GetPanel<PanelBattle>().updateHideState(true);
        }
    }

    public override void OnShow()
    {
        if (m_params != null)
        {
            object[] list = m_params[0] as object[];
            m_chooseId[0] = (int)list[0];
            m_chooseId[1] = (int)list[1];
            m_show = false;
            if (list.Length > 2)
            {
                m_chooseId[2] = (int)list[2];
            }
            m_itemType = (int)m_params[1];
        }
        InitGrid();
        ConfigHideDisguise hideDisguise = ConfigManager.GetConfig<ConfigHideDisguise>();
        int againSpend = 0;
        if (hideDisguise != null)
        {
            againSpend = hideDisguise.GetHideBuffLineByMapId(SceneManager.singleton.curMapId);
        }
        m_againSpendText.text = againSpend.ToString();
        m_info1.text = "";
        m_info2.text = "";
        m_stopbtntxt.text = "停止";
        m_againSpendImgGo.TrySetActive(false);
        m_again = false;
        canClick = true;
        checkStopBtn(true);
        ChangeView();
        if (m_rollerstate[0] == ROLLER_STATE.STOP)
        {
            m_rollerstate[0] = ROLLER_STATE.START;
            m_starttime = Time.realtimeSinceStartup;
            m_paneltime = Time.realtimeSinceStartup;
        }

        ConfigMapListLine mapListConfigLine = null;
        if ((mapListConfigLine = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString())) != null)
        {
            if( m_itemType == 1 )
            {
                m_disguiseTime = mapListConfigLine.Hide_time[1];
            }
            else
            {
                m_disguiseTime = mapListConfigLine.Hide_time[1] + mapListConfigLine.Hide_time[2] + mapListConfigLine.Hide_time[3];
            }
        }
        else
        {
            m_disguiseTime = m_itemType == 1 ? 30:130;
        }

        ConfigMiscLine cfg = ConfigManager.GetConfig<ConfigMisc>().GetLine("hide_niubi_card_number");
        int cardId = cfg.ValueInt;
        m_haveCardItem = ItemDataManager.IsDepotHasAndInTime(cardId);
        if (m_haveCardItem != false)
        {
            m_text.text = "选择";
            m_text3Icon.TrySetActive(false);
            
        }
        else
        {
            m_text.text = "2";
            m_text3Icon.TrySetActive(true);
        }
        if (m_itemType == 1)
        {
            m_again = true;
        }
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        m_tran.Find("choose1/Text").GetComponent<Text>().text = "选择(1)";
        m_tran.Find("choose2/Text").GetComponent<Text>().text = "选择(2)";
        if( m_haveCardItem != false )
        {
            m_text.text = "选择(3)";
        }
        else
        {
             m_text.text = "2      (3)";
        }
        
#endif
    }

    private void ChangeView()
    {
        bool isShow = m_itemType == 1;
        m_lockImg.TrySetActive(!isShow);
        m_itemlists[2].gameObject.TrySetActive(isShow);
        if( isShow == false )
        {
            Util.SetGoGrayShader(m_choosebtns[2]);
        }
        else
        {
            Util.SetNormalShader(m_choosebtns[2].transform);
        }
    }

    void CheckDoingState()
    {
        bool active = false;
        if (m_rollerstate[0] == ROLLER_STATE.STOP && m_rollerstate[1] == ROLLER_STATE.STOP)
        {
            active = true;
        }
        if (m_rollerstate[0] == ROLLER_STATE.DOING && m_rollerstate[1] == ROLLER_STATE.DOING)
        {
            active = true;
        }
        checkStopBtn(active);

    }

    void click(object target, PointerEventData data)
    {
        if (m_rollerstate[0] == ROLLER_STATE.STOP)
        {
            if( m_itemType == 1)
            {
                NetLayer.Send(new tos_fight_select_disguise() { index = 1 });
            }
            else
            {
                NetLayer.Send(new tos_fight_select_buff() { index = 1 });
            }
            
            PanelBattle panel = UIManager.GetPanel<PanelBattle>();
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 0;
                obj[1] = 5;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(0, 5);
            }
            if (m_itemType==1)
            {
                GameDispatcher.Dispatch<bool>(HideEvent.HIDE_FORBID_CAT, false);
            }
            
            m_show = true;
            UIManager.GetPanel<PanelBattle>().updateHideState(true);
            this.HidePanel();

        }
    }

    void click2(object target, PointerEventData data)
    {
        if (m_rollerstate[1] == ROLLER_STATE.STOP)
        {
            if (m_itemType == 1)
            {
                NetLayer.Send(new tos_fight_select_disguise() { index = 2 });
            }
            else
            {
                NetLayer.Send(new tos_fight_select_buff() { index = 2 });
            }
            
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 0;
                obj[1] = 5;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(0, 5);
            }
            if (m_itemType == 1)
            {
                GameDispatcher.Dispatch<bool>(HideEvent.HIDE_FORBID_CAT, false);
            }
            m_show = true;
            UIManager.GetPanel<PanelBattle>().updateHideState(true);
            this.HidePanel();

        }
    }

    void click3(object target, PointerEventData data)
    {
        //NetLayer.Send(new tos_fight_select_disguise() { index = 1 });
        //GameDispatcher.Dispatch<bool>(HideEvent.HIDE_FORBID_CAT, false);
        if (m_itemType == 2)
        {
            return;
        }
        if (m_rollerstate[2] == ROLLER_STATE.STOP)
        {
            if (m_haveCardItem == false && m_diamondCost > 0 && PlayerSystem.roleData.diamond < 2)
            {
                TipsManager.Instance.showTips("钻石不足");
                return;
            }
            NetLayer.Send(new tos_fight_select_disguise() { index = 3 });
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 0;
                obj[1] = 5;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(0, 5);
            }
            if (m_itemType == 1)
            {
                GameDispatcher.Dispatch<bool>(HideEvent.HIDE_FORBID_CAT, false);
            }
            m_show = true;
            UIManager.GetPanel<PanelBattle>().updateHideState(true);
            this.HidePanel();
        }
    }

    private bool canClick = true;
    void OnStopClick(object target, PointerEventData data)
    {
        if( m_itemType == 2 )
        {
            return;
        }
        for (int i = 0; i < m_rollerNum; i++)
        {
            if (m_rollerstate[0] == ROLLER_STATE.STOP && m_again == true)
            {
                canClick = true;
                m_rollerstate[0] = ROLLER_STATE.START;
                NetLayer.Send(new tos_fight_reelect_disguise());
                m_again = false;
                
            }
            if (m_rollerstate[i] == ROLLER_STATE.DOING)
            {
                m_rollerstate[i] = ROLLER_STATE.STOPING;
                
            }
        }
        if (canClick == true)
        {
            m_starttime = Time.realtimeSinceStartup;
            canClick = false;
        }
        checkStopBtn(false);
    }

    private void OnUnlockClick(GameObject obj, PointerEventData eventData)
    {

    }


    void CheckRoller2()
    {
        if (m_starttime > 0 && Time.realtimeSinceStartup - m_starttime > 0.01f)
        {
            if (m_rollerstate[0] == ROLLER_STATE.STOP)
            {
                m_rollerstate[0] = ROLLER_STATE.START;
            }
            if (m_rollerstate[1] == ROLLER_STATE.STOP)
            {
                m_rollerstate[1] = ROLLER_STATE.START;
            }
            if (m_rollerstate[2] == ROLLER_STATE.STOP)
            {
                m_rollerstate[2] = ROLLER_STATE.START;
            }
        }
    }


    void CheckSpeed(int i)
    {
        CheckRoller2();
        if (m_rollerstate[i] == ROLLER_STATE.STOP)
        {
            m_speed[i] = 0.0f;
            if (i == 1 && m_again)
            {
                if (allStop == true)
                {
                    if (m_haveCardItem != false)
                    {
                        m_againSpendText.gameObject.TrySetActive(false);
                        m_againSpendImgGo.TrySetActive(false);
                        m_stopbtntxt.text = "免费再试一次";
                    #if UNITY_WEBPLAYER || UNITY_STANDALONE
                        m_stopbtntxt.text = "免费再试一次(" + GlobalBattleParams.singleton.PcKeyToText(GlobalBattleParams.singleton.GetPcKeyParam("INPUT_KEY_R_DOWN")) + ")";
                    #endif
                    }
                    else
                    {
                        if (m_itemType == 1)
                        {
                            m_againSpendText.gameObject.TrySetActive(true);
                            m_againSpendImgGo.TrySetActive(true);
                            m_stopbtntxt.text = "再试一次";
#if UNITY_WEBPLAYER || UNITY_STANDALONE
                        m_stopbtntxt.text = "再试一次(" + GlobalBattleParams.singleton.PcKeyToText(GlobalBattleParams.singleton.GetPcKeyParam("INPUT_KEY_R_DOWN")) + ")";
#endif
                        }
                       
                    }
                }
                else
                {
                    m_againSpendText.gameObject.TrySetActive(false);
                    m_againSpendImgGo.TrySetActive(false);
                    m_stopbtntxt.text = "停止";
                    #if UNITY_WEBPLAYER || UNITY_STANDALONE
                    m_stopbtntxt.text = "停止(" + GlobalBattleParams.singleton.PcKeyToText(GlobalBattleParams.singleton.GetPcKeyParam("INPUT_KEY_R_DOWN")) + ")";
                    #endif
                }


                checkStopBtn(true);
            }
            return;
        }
        if (m_rollerstate[i] == ROLLER_STATE.START)
        {
            m_speed[i] += m_addspeed;
            if (m_speed[i] >= m_maxspeed)
            {
                m_rollerstate[i] = ROLLER_STATE.DOING;
            }
            if (i == 0)
            {
                m_stopbtntxt.text = "停止";
#if UNITY_WEBPLAYER || UNITY_STANDALONE
                m_stopbtntxt.text = "停止(" + GlobalBattleParams.singleton.PcKeyToText(GlobalBattleParams.singleton.GetPcKeyParam("INPUT_KEY_R_DOWN")) + ")";
#endif
                m_againSpendText.gameObject.TrySetActive(false);
                m_againSpendImgGo.TrySetActive(false);
                checkStopBtn(true);
            }
            return;
        }
        if (m_rollerstate[i] == ROLLER_STATE.DOING)
        {
            if (Time.realtimeSinceStartup - m_starttime > 1f)
            {
                m_rollerstate[i] = ROLLER_STATE.STOPING;
                m_starttime = -1f;
            }
            return;
        }
        if (m_rollerstate[i] == ROLLER_STATE.STOPING)
        {
            m_speed[i] -= m_dropspeed;
            if (m_speed[i] <= m_minspeed)
            {
                m_rollerstate[i] = ROLLER_STATE.SlOWING;
                m_stopaims[i] = CheckGrid(i);
            }
            return;
        }
        if (m_rollerstate[i] == ROLLER_STATE.SlOWING)
        {
            int n = m_stopaims[i] + 2;

            if (n >= ITEM_COUNT+2)
            {
                n -= ITEM_COUNT+2;
            }
            var item = new hideitemdata();
            item.id = m_chooseId[i];
            item.itemType = m_itemType;
            var a = m_itemlists[i].ItemRenders[n + 1];
            ItemHide it = a as ItemHide;
            it.itemType = m_itemType;
            it.setImage(item.id);
            var ypos = m_itemlists[i].gameObject.transform.localPosition.y;
            if (ypos > m_starty + 82.9f * n && ypos < m_starty + 82.9f * (n + 1))
            {
                m_rollerstate[i] = ROLLER_STATE.STOP;

                m_info1.text = getBuffInfo(m_chooseId[0]);
                m_info2.text = getBuffInfo(m_chooseId[1]);
                m_starttime = -1f;
            }
        }

    }

    int CheckGrid(int index)
    {
        float nowy = m_itemlists[index].gameObject.transform.localPosition.y;
        for (int i = 0; i < ITEM_COUNT+3; i++)
        {
            if (m_starty + 83f * i > nowy)
            {
                if (i == 0 || i == 1 || i == 2 || i == ITEM_COUNT+2 || i == ITEM_COUNT + 1 || i == ITEM_COUNT || i == ITEM_COUNT - 1 || i == ITEM_COUNT - 2)
                {
                    i = 2;
                }
                return i;
            }
        }
        return 0;
    }

    private bool m_isRolling;
    private bool allStop = false;
    public override void Update()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            UIManager.GetPanel<PanelBattle>().updateHideState(m_show);
        }
        if( this.IsOpen() == false)
        {
            return;
        }
        for (int i = 0; i < m_rollerNum; i++)
        {
            m_itemlists[i].verticalPos -= m_speed[i]*4;
            CheckSpeed(i);
            if (m_itemlists[i].verticalPos <= 0.0f)
            {
                m_itemlists[i].verticalPos = 1.0f;
            }
        }

        allStop = true;
        for (int i = 0; i < m_rollerstate.Length; i++)
        {
            if (m_rollerstate[i] == ROLLER_STATE.START || m_rollerstate[i] == ROLLER_STATE.DOING)
            {
                if (!m_isRolling)
                {
                    m_isRolling = true;
                    AudioManager.PlayUISoundLoop(AudioConst.hideRolling);
                }
            }

            if (m_rollerstate[i] != ROLLER_STATE.STOP)
                allStop = false;
        }
        if (allStop && m_isRolling)
        {
            m_isRolling = false;
            AudioManager.StopUISoundLoop();
        }
        if (allStop == true && m_again == false)
        {
            checkStopBtn(false);
        }
        if(allStop == true)
        {
           // m_info1.text = getBuffInfo(m_chooseId[0]);
            //m_info2.text = getBuffInfo(m_chooseId[1]);
            if (m_again == true)
            {
                m_stopbtn.TrySetActive(true);
            }
            else
            {
                m_stopbtn.TrySetActive(false);
            }
        }
        else
        {
            m_stopbtn.TrySetActive(false);
        }
        if (!m_show)
        {
            int delta =0;
            delta = m_disguiseTime - (int)Math.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
            
            if (delta <= 0)
            {
                this.HidePanel();
                GameDispatcher.Dispatch<bool>(HideEvent.HIDE_FORBID_CAT, false);
                m_show = true;
            }
            else
            {
                SetTime(delta);
            }
        }
    }

    private string getBuffInfo(int buffId)
    {
       
        if(m_itemType == 1)
        {
            return "";
        }
        string info = "";
        ConfigHideBuffInfo buffInfo = ConfigManager.GetConfig<ConfigHideBuffInfo>();
        ConfigHideBuffInfoLine buffInfoLine = null;
        if( buffInfo != null )
        {
            buffInfoLine = buffInfo.GetLine(buffId);
            if (buffInfoLine != null)
            {
                if( buffInfoLine.LifeAdd > 0 )
                {
                    info = "HP + " + buffInfoLine.LifeAdd;
                    return info;
                }
                
                if (buffInfoLine.GunBulletsAdd > 0)
                {
                    info = "手枪子弹数 + " + buffInfoLine.GunBulletsAdd;
                    return info;
                }
                   
                if (buffInfoLine.HPDeductLevel > 0)
                {
                    info = "散弹枪惩罚 - " + buffInfoLine.HPDeductLevel;
                    return info;
                }
            }
            else
            {
                Logger.Log("buffInfo is null buffid = " + buffInfo);
            }
        }
        return info;
    }

    public void SetTime(int time)
    {
        if (time >= 10)
        {
            m_timeImgs[2].gameObject.TrySetActive(true);
            m_timeImgs[2].SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "s"));
            m_timeImgs[0].SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + (time / 10).ToString()));
            m_timeImgs[1].SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + (time % 10).ToString()));
        }
        if (time < 10)
        {
            m_timeImgs[2].gameObject.TrySetActive(false);
            m_timeImgs[0].SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide" + time.ToString()));
            m_timeImgs[1].SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hide_s"));
        }
    }

    void Toc_fight_select_disguise(toc_fight_select_disguise data)
    {
        GetConfig();
        //if (m_diamondCost != 0)
        //{
        //    m_text.text = string.Format("{0}钻石", m_diamondCost);
        //}
        //else
        //{
        //    m_text.text = "选择";
        //}
        m_chooseId[0] = data.disguise_list[0];
        m_chooseId[1] = data.disguise_list[1];
        if (data.disguise_list.Length > 2)
        {
            m_chooseId[2] = data.disguise_list[2];
        }
        InitGrid();
    }

    void Toc_fight_select_buff(toc_fight_select_buff data)
    {
        int buffid = 0;
        for (int i = 0; i < data.buff_list.Length;i++ )
        {
            buffid = data.buff_list[i];
            m_chooseId[i] = buffid;
        }
        m_itemType = 2;
        InitGrid();
    }
}
