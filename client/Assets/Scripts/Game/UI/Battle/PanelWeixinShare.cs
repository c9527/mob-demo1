using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System.IO;
using System.Threading;
using System.Collections;

public class SettleWeixinPlayer
{
    public long uid;
    public string name;
    public string sceneType;
    public int camp;
    public int icon;
    public int death;
    public int kill;
    public bool isMvp;
    public bool isgGoldAce;
    public bool isSliverAce;  
}

class PanelWeixinShare:BasePanel
{
    Image m_headImage;
    Image m_playerMvp;
    Image m_playerGoldAce;
    Image m_playerSliverAce;
    //Image m_roomImage;
    Image m_logo;
    Image m_erweima;
    Image m_goType;
    Image m_goBg;

    Text m_playerName;
    //Text m_levelText;
    //Text m_killDeath;
    Text m_txtKill;
    Text m_txtDeath;
    Text m_serverName;
    Text m_roomName;
    Text m_ruleName;
    Text m_txtRank;
    Text m_txtDamage;
    Text m_txtScore;
    Text m_txtStars;
    //Text m_ruleName;
    GameObject m_rank;
    GameObject m_worldboss;
    GameObject m_zombie;
    GameObject m_pvpWin;
    GameObject m_killDeath;
    GameObject m_weixinShare;
    GameObject m_multiShare;

	Button m_captureShot;
	Button m_shareToFriend;
    Button m_shareMultiple;
    Button m_shareMultipleCancel;
	static Button m_shareToFriendLine;
    static Button m_back;

    protected const int MaxPlayerCount = 16;
	protected static int timeOutUid;

    public static SettleWeixinPlayer[] settleWeixinData;
    protected static SettleWeixinPlayer myselfData;    
    public string sceneType;
    private string m_shareType;

	private static Vector3 captureLeft;
	private static Vector3 captureRight;
    public static int fromType = 0;

    public PanelWeixinShare()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelWeixinShare");
        AddPreLoadRes("UI/Battle/PanelWeixinShare", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Login", EResType.Atlas);
        AddPreLoadRes("Atlas/WeixinShare", EResType.Atlas);
    }

    public override void Init()
    {
        m_playerMvp = m_tran.Find("pvp_win/player_mvp").GetComponent<Image>();
        m_playerGoldAce = m_tran.Find("pvp_win/player_gold_ace").GetComponent<Image>();
        m_playerSliverAce = m_tran.Find("pvp_win/player_sliver_ace").GetComponent<Image>();
        m_logo = m_tran.Find("logo").GetComponent<Image>();
        m_erweima = m_tran.Find("erweima").GetComponent<Image>();
        m_goType = m_tran.Find("go_type").GetComponent<Image>();
        m_goBg = m_tran.Find("background").GetComponent<Image>();

        m_playerName = m_tran.Find("player_bg/player_name").GetComponent<Text>();
        m_txtKill = m_tran.Find("kill_death/kill/Text").GetComponent<Text>();
        m_txtDeath = m_tran.Find("kill_death/dead/Text").GetComponent<Text>();
        
        m_serverName = m_tran.Find("player_bg/server_name").GetComponent<Text>();
        m_roomName = m_tran.Find("player_bg/room_name").GetComponent<Text>();
        m_ruleName = m_tran.Find("player_bg/rule_name").GetComponent<Text>();
        m_txtRank = m_tran.Find("pve_pass/rank/Text").GetComponent<Text>();
        m_txtDamage = m_tran.Find("pve_pass/pve_worldboss/damage/Text").GetComponent<Text>();
        m_txtScore = m_tran.Find("pve_pass/pve_zombie/score/Text").GetComponent<Text>();
        m_txtStars = m_tran.Find("pve_pass/pve_zombie/stars/Text").GetComponent<Text>();

        m_rank = m_tran.Find("pve_pass/rank").gameObject;
        m_worldboss = m_tran.Find("pve_pass/pve_worldboss").gameObject;
        m_zombie = m_tran.Find("pve_pass/pve_zombie").gameObject;
        m_pvpWin = m_tran.Find("pvp_win").gameObject;
        m_killDeath = m_tran.Find("kill_death").gameObject;
        m_weixinShare = m_tran.Find("button").gameObject;
        m_multiShare = m_tran.Find("multi_share_btn").gameObject;
        m_erweima.gameObject.TrySetActive(false);
        m_pvpWin.TrySetActive(false);
        m_weixinShare.TrySetActive(false);
        m_multiShare.TrySetActive(false);

		m_captureShot = m_tran.Find("button/capture_btn").GetComponent<Button>();
        m_shareToFriend = m_tran.Find("button/share_friend_btn").GetComponent<Button>();
        m_shareToFriendLine = m_tran.Find("button/share_line_btn").GetComponent<Button>();
        m_shareMultiple = m_tran.Find("multi_share_btn/ok_btn").GetComponent<Button>();
        m_shareMultipleCancel = m_tran.Find("multi_share_btn/cancel_btn").GetComponent<Button>();
        m_back = m_tran.Find("button/back_btn").GetComponent<Button>();

		captureLeft = m_tran.Find ("background/captureLeft").position;
		captureRight = m_tran.Find ("background/captureRight").position;
		//RectTransformUtility.WorldToScreenPoint ();   
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_back.gameObject).onPointerClick += onClose;
        UGUIClickHandler.Get(m_shareToFriend.gameObject).onPointerClick += onShareToFriend;
        UGUIClickHandler.Get(m_shareToFriendLine.gameObject).onPointerClick += onShareToFriendLine;
        UGUIClickHandler.Get(m_captureShot.gameObject).onPointerClick += onCaptureShot;
        UGUIClickHandler.Get(m_shareMultiple.gameObject).onPointerClick += onShareMultiple;
        UGUIClickHandler.Get(m_shareMultipleCancel.gameObject).onPointerClick += onShareMultipleCancel;
    }

    public void InitData(string rule)
    {
		var share_data = PanelSettle.game_result;
        var playerArray = share_data.players;
        int mvpId = share_data.mvp;
        int gaceId = share_data.gold_ace;
        int saceId = share_data.silver_ace;
        sceneType = share_data.scene_type;

        List<SettleWeixinPlayer> data = new List<SettleWeixinPlayer>();
        for (int i = 0; i < playerArray.Length; i++)
        {
            int temp = playerArray[i].actor_id;
            var sdPlayer = new SettleWeixinPlayer()
            {
                uid = playerArray[i].player_id,
                camp = playerArray[i].camp,
                icon = playerArray[i].icon,
                death = playerArray[i].death,
                kill = playerArray[i].kill,
                isMvp = temp == mvpId,
                isgGoldAce = temp == gaceId,
                isSliverAce = temp == saceId,
                name = playerArray[i].name
            };
            data.Add(sdPlayer);
        }
        settleWeixinData = data.ToArray();
        var logoName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.LOGO);
        var qrName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.GAME_SHARE_QRCODE);
        m_logo.enabled = !string.IsNullOrEmpty(logoName);        
        m_logo.SetSprite(ResourceManager.LoadSprite(AtlasName.Login, logoName));
        if(Driver.m_platform == EnumPlatform.ios)
        {
            m_erweima.enabled = !string.IsNullOrEmpty(qrName);
            m_erweima.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, qrName));
        }            
               
        if (rule == GameConst.GAME_RULE_SURVIVAL)
        {
            m_shareType = "PVE";
            fromType = 6;
            SetModeDetail(true);
            m_txtRank.text = PanelSettle.self_rank.ToString();            
            m_goType.gameObject.TrySetActive(false);
            m_goBg.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "PVE_share_bg"));
            if (PanelSettle.self_share != null)
            {
                m_txtKill.text = PanelSettle.self_share.kill.ToString();
                m_txtDeath.text = PanelSettle.self_share.death.ToString();
            }
            m_killDeath.transform.localPosition = new Vector3(145, 0, 0);
            m_killDeath.SetActive(true);
            m_rank.TrySetActive(true);
            m_worldboss.TrySetActive(false);
            m_zombie.TrySetActive(false);
            m_pvpWin.TrySetActive(false);
        }
        else if (rule == GameConst.GAME_RULE_STATE_SURVIVAL)
        {
            m_shareType = "PVE";
            fromType = 6;
            SetModeDetail(true);
            m_txtRank.text = PanelOldSettle.self_rank.ToString();
            m_goType.gameObject.TrySetActive(false);
            m_killDeath.transform.localPosition = new Vector3(145, 0, 0);
            m_goBg.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "PVE_share_bg"));
            if (PanelOldSettle.self_share != null)
            {                
                m_txtScore.text = PanelOldSettle.self_share.score.ToString();
            }
            m_txtStars.text = PanelOldSurvivalSettle.m_starLevel.ToString();
            m_killDeath.SetActive(false);
            m_rank.TrySetActive(true);
            m_worldboss.TrySetActive(false);
            m_zombie.TrySetActive(true);
            m_pvpWin.TrySetActive(false);
        }
        else if (rule == GameConst.GAME_RULE_BOSS_NEWER || rule == GameConst.GAME_RULE_WORLD_BOSS)
        {
            m_shareType = "PVE";
            fromType = 6;
            SetModeDetail(false);
            m_txtRank.text = PanelSettle.self_rank.ToString();
            m_goType.gameObject.TrySetActive(false);
            m_goBg.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "PVE_share_bg"));
            if (PanelSettle.self_share != null)
            {
                m_txtDamage.text = PanelSettle.self_share.score.ToString();                
            }
            m_txtStars.text = PanelOldSurvivalSettle.m_starLevel.ToString();
            m_killDeath.SetActive(false);
            m_rank.TrySetActive(true);
            m_worldboss.TrySetActive(true);
            m_zombie.TrySetActive(false);
            m_pvpWin.TrySetActive(false);
        }
        else
        {
            m_shareType = "PVP";
            fromType = 5;
            SetModeDetail(false);
            if (PanelSettle.self_share != null)
            {
                m_txtKill.text = PanelSettle.self_share.kill.ToString();
                m_txtDeath.text = PanelSettle.self_share.death.ToString();
                if(!PanelSettle.self_share.mvp && !PanelSettle.self_share.gold_ace)
                {
                    m_killDeath.transform.localPosition = new Vector3(145, 0, 0);
                }
            }
            m_goType.gameObject.TrySetActive(PanelSettle.self_win_result);
            m_goBg.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "PVP_share_bg"));
            m_killDeath.SetActive(true);
            m_rank.TrySetActive(false);
            m_worldboss.TrySetActive(false);
            m_zombie.TrySetActive(false);
            m_pvpWin.TrySetActive(true);
        }
        if(Driver.m_platform == EnumPlatform.ios)
        {
            m_weixinShare.TrySetActive(true);
            m_multiShare.TrySetActive(false);
        }
        else if(Driver.m_platform == EnumPlatform.android)
        {
            m_weixinShare.TrySetActive(false);
            m_multiShare.TrySetActive(true);
            if (SdkManager.CurrentSdk is AndroidSdkBase)
            {
                (SdkManager.CurrentSdk as AndroidSdkBase).sendAndroidLogToServer(fromType, 1, 0, "0");
            }
        }
    }

    private void SetModeDetail(bool isShowModeType)
    {
        var info = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.LastRoomInfo.rule);
        string modeType = null;
        if (PanelSurvivalRoom.m_levelNames != null && RoomModel.LastRoomInfo.level >= 0 && RoomModel.LastRoomInfo.level < 6)
        {
            modeType = PanelSurvivalRoom.m_levelNames[RoomModel.LastRoomInfo.level];
        }
        if (modeType != null && isShowModeType)
            m_ruleName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.LastRoomInfo.rule).Mode_Name + "(" + modeType + ")";
        else
            m_ruleName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.LastRoomInfo.rule).Mode_Name;
        m_serverName.text = PlayerSystem.GetRoleSeverName(PlayerSystem.roleId);
        m_roomName.text = ConfigManager.GetConfig<ConfigMapList>().GetLine(RoomModel.LastRoomInfo.map.ToString()).MapName; 
    }

    private void onClose(GameObject target, PointerEventData eventData)
    {
        OnHide();
        base.HidePanel();
    }

    private void onShareMultipleCancel(GameObject target, PointerEventData eventData)
    {
        OnHide();
        base.HidePanel();
    }

    private void onShareToFriend(GameObject target, PointerEventData eventData)
    {
		Logger.Log("shareToFriend---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() => {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 0;
			if(Driver.m_platform == EnumPlatform.ios)
			{
                SdkManager.WeixinShare("1", imagePath);//1 friend 2 friend line	
				//SdkManager.SaveCapture (imagePath);			
			}          
			else
			{
				handleShareToWeixin("wechat_friends", imagePath);
			}  
        }));
    }

    private void onShareToFriendLine(GameObject target, PointerEventData eventData)
    {
		Logger.Log("shareToFriendLine---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() => {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 0;
			if(Driver.m_platform == EnumPlatform.ios)
			{
                SdkManager.WeixinShare("2", imagePath);//1 friend 2 friend line
				//SdkManager.SaveCapture (imagePath);
			}
			else
			{
				handleShareToWeixin("wechat_moments", imagePath);
			}           
        }));
    }

    private void onShareMultiple(GameObject target, PointerEventData eventData)
    {
        Logger.Log("onShareMultiple---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() => {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null || m_shareType == null)
                return;
            SdkManager.m_logFrom = 0;

            if (!SdkManager.InterNationalVersion())
            {
                if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SAVE_CAPTURE) ||
                !SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
                {
                    UIManager.ShowNoThisFunc();
                    return;
                }
                else
                {
                    SdkManager.MultipleShare(m_shareType, imagePath);
                }
            }
            else
            {
                SdkManager.ShareToVkOrFBbattle("https://vk.com/ssjj.tatru", "battle record", imagePath, "ս��ʤ��");
            }
        }));
    }    

    private void onCaptureShot(GameObject target, PointerEventData eventData)
    {
		Logger.Log("onCaptureShot---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() => { 
        string imagePath = GetCaptureShotPath ();
        if (imagePath == null)
            return;

        if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SAVE_CAPTURE) ||
            !SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
        {
            UIManager.ShowNoThisFunc();
            return;
        }
        SdkManager.SaveCapture(imagePath);

        }));
        
    }

    IEnumerator CaptureCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(1f);
        action(); 
    }

    public static void handleShareToWeixin(string shareType, string imagePath)
    {
        if (!SdkManager.InterNationalVersion())
        {
            if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SAVE_CAPTURE) ||
                !SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
            {
                UIManager.ShowNoThisFunc();
                return;
            }
            else
            {
                SdkManager.WeixinShare(shareType, imagePath);//1 friend 2 friend line
                //SdkManager.SaveCapture(imagePath);
            }
        }
        else
        {
            SdkManager.ShareToVkOrFBbattle("https://vk.com/ssjj.tatru", "battle record", imagePath, "ս��ʤ��");
        }
    }




	public string GetCaptureShotPath()
	{
		Vector2 left = RectTransformUtility.WorldToScreenPoint (UIManager.m_uiCamera, captureLeft);
		Vector2 right = RectTransformUtility.WorldToScreenPoint (UIManager.m_uiCamera, captureRight);
        string path = null;
        path = DoGetCapture((float)left.x, (float)right.y, Mathf.Abs(right.x - left.x), Mathf.Abs(right.y - left.y));
        timeOutUid = TimerManager.SetTimeOut(1.0f, () =>
        {
            m_back.gameObject.TrySetActive(true);
            m_back.enabled = true;
        });
		return path;
	}

    public override void OnShow()
    {
        if(HasParams())
        {            
            var rule = m_params[0].ToString();           
            InitData(rule);
        }		
        if (settleWeixinData == null)
        {
            HidePanel();
            return;
        }
        if (settleWeixinData == null || settleWeixinData.Length == 0)
        {
            settleWeixinData = new SettleWeixinPlayer[1] { new SettleWeixinPlayer() { icon = PlayerSystem.roleData.icon } };
        }
        updateData();
    }

    public override void OnHide()
    {
        //settleWeixinData = null;
		if (timeOutUid > 0)
			TimerManager.RemoveTimeOut (timeOutUid);
    }

    public override void OnBack()
    {
        HidePanel();        
    }

    public override void OnDestroy()
    {
		if (timeOutUid > 0)
			TimerManager.RemoveTimeOut (timeOutUid);
        PanelSettle.self_share = null;
        PanelOldSettle.self_share = null;
        PanelWeixinShare.fromType = 0;
	}
	
	public override void Update()
    {
        if(Time.realtimeSinceStartup - PanelSettle.m_panelStartTime > 60)
        {
            PanelSettle.m_panelStartTime = float.MaxValue;
            if(RoomModel.IsInRoom)
            {
                HidePanel();
            }
        }
    }

    protected virtual void updateData()
    {
        var index = 0;
        if (settleWeixinData == null)
            return;
        int firstCamp = settleWeixinData[0].camp;
        myselfData = null;
        SettleWeixinPlayer[] resultSettleRed = null;
        SettleWeixinPlayer[] resultSettleYellow = null; 
        var game_rule = RoomModel.LastRoomInfo != null ? RoomModel.LastRoomInfo.rule : null;
        var line = RoomModel.LastRoomInfo != null && RoomModel.LastRoomInfo.max_player > MaxPlayerCount ? RoomModel.LastRoomInfo.max_player / 2 : 5;

        if (RoomModel.LastRoomInfo != null && RoomModel.LastRoomInfo.max_player > MaxPlayerCount)
        {  
            resultSettleRed = new SettleWeixinPlayer[RoomModel.LastRoomInfo.max_player];
            resultSettleYellow = new SettleWeixinPlayer[RoomModel.LastRoomInfo.max_player]; 
        }
        else
        {
            resultSettleRed = new SettleWeixinPlayer[MaxPlayerCount/2];
            resultSettleYellow = new SettleWeixinPlayer[MaxPlayerCount/2];
        }
        for (int i = 0; i < settleWeixinData.Length; i++)
        {
            if (settleWeixinData[i].uid == PlayerSystem.roleId)
            {
                myselfData = settleWeixinData[i];
               // index++;
            }
        }

        bool isMyself = myselfData.uid == PlayerSystem.roleId;
        if(isMyself)
        {
            m_playerName.text = PlayerSystem.GetRoleNameFull(myselfData.name, myselfData.uid);
            m_playerMvp.gameObject.TrySetActive(myselfData.isMvp);
            m_playerMvp.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "mvp"));
            m_playerMvp.SetNativeSize();
            m_playerGoldAce.gameObject.TrySetActive(myselfData.isgGoldAce);
            m_playerGoldAce.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "gold_ace"));
            m_playerGoldAce.SetNativeSize();
            m_playerSliverAce.gameObject.TrySetActive(myselfData.isSliverAce);            
            m_playerSliverAce.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "silver_ace"));
            m_playerSliverAce.SetNativeSize();
            isMyself = false;
        }       
    }

	public static string DoGetCapture(float offsetx, float offsety, float width, float height)
	{
		//yield return WaitForEndOfFrame ();
		Texture2D texture = new Texture2D((int)width, (int)height, TextureFormat.RGB24, false);
		texture.ReadPixels (new Rect(offsetx, offsety, width, height), 0, 0, true);
        var compressRatio = 0.5f;
        var targetHeight = Mathf.RoundToInt(height * compressRatio);
        var targetWidth = Mathf.RoundToInt(width * compressRatio);
        TextureScale.Point(texture, targetWidth, targetHeight);
        byte[] imageBytes = texture.EncodeToJPG();

		if (imageBytes == null) 
		{
			return null;
		}
        texture.Compress(false);
		string filePath = Application.persistentDataPath + "/screenCapture.jpg";
#if !UNITY_WEBPLAYER
		File.WriteAllBytes (filePath, imageBytes);
#endif
		return filePath;
	}
}

