﻿using UnityEngine;
using UnityEngine.UI;

public class ShowRoleName : MonoBehaviour
{
    PlayerServerData _data;
    bool _show_hp_info;

    float _last_time;

    RectTransform m_info_tran;
    Text m_name_txt;
    Image m_hp_bar;

    void Start()
    {
        var rule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        _show_hp_info = rule != null && rule.ShowAimHP == 1;
        if (_show_hp_info)
        {
            GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        }
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int,int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<BasePlayer,bool>(GameEvent.UI_SHOW_AIM_PLAYER_NAME, ShowPlayerName);
        GameDispatcher.AddEventListener(GameEvent.UI_CANCEL_AIM_PLAYER_NAME, CancleShowPlayerName);

        if (m_info_tran == null)
        {
            m_name_txt = Util.CreateText(gameObject.transform);
            m_name_txt.alignment = TextAnchor.MiddleCenter;
            m_info_tran = m_name_txt.gameObject.AddMissingComponent<RectTransform>();
            m_info_tran.gameObject.name = "showName";
            m_info_tran.localScale = Vector3.one;
            var hp_go = new GameObject("hp_bg").AddComponent<Image>();
            hp_go.color = new Color(1, 1, 1, 128f / 255);
            hp_go.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "processbk"));
            hp_go.SetNativeSize();
            hp_go.transform.SetLocation(m_info_tran, 0, -20);
            m_hp_bar = new GameObject("hp_bar").AddComponent<Image>();
            m_hp_bar.type = Image.Type.Filled;
            m_hp_bar.fillMethod = Image.FillMethod.Horizontal;
            m_hp_bar.color = new Color(1, 1, 1, 180f / 255);
            m_hp_bar.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "hp_process"));
            m_hp_bar.SetNativeSize();
            m_hp_bar.transform.SetLocation(hp_go.transform, 0, 0);
            hp_go.gameObject.TrySetActive(_show_hp_info);
        }
        CancleShowPlayerName();
    }

    void Update()
    {
        if (_data == null || _show_hp_info==false)
        {
            return;
        }
        if (Time.time >= (_last_time + 3))
        {
            _last_time = Time.time;
            m_hp_bar.fillAmount = (float)_data.life / _data.maxhp();
        }
    }

    private void OnPlayerBeDamaged(DamageData damageData)
    {
        BasePlayer victim = damageData.victim;
        if (_data == null || victim == null || victim.serverData == null || _data.id != victim.serverData.id || _show_hp_info==false)
        {
            return;
        }
        m_hp_bar.fillAmount = (float)_data.life / _data.maxhp();
    }

    private void OnPlayerRevive(string name, BasePlayer player)
    {
        if (player != MainPlayer.singleton)
            return;

        CancleShowPlayerName();
    }

    private void OnPlayerDie(BasePlayer killer, BasePlayer player, int hitPart,int weapon)
    {
        if (player != MainPlayer.singleton)
            return;

        CancleShowPlayerName();
    }

    void ShowPlayerName( BasePlayer player,bool bEnemy )
    {
        if (player == null || player.serverData == null || Camera.main == null || Camera.main.enabled == false)
        {
            return;
        }
        _last_time = Time.time;
        _data = player.serverData;
        m_info_tran.gameObject.TrySetActive(true);
        m_name_txt.color = bEnemy ? Color.red : Color.cyan;
        m_name_txt.text = _data.name;
        if (_show_hp_info)
        {
            m_hp_bar.fillAmount = (float)_data.life / _data.maxhp();
        }
        Vector2 screenPos = Camera.main.WorldToScreenPoint( player.position );

        Vector2 rectPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle( gameObject.GetComponent<RectTransform>(), screenPos, Camera.main, out rectPos);
        int screenHeight = Screen.height;
        Vector3 localPos = m_info_tran.localPosition;
        localPos.x = 0;

        int downUnit = (int)(GameSetting.AIM_NAME_HEGIHT_RATE * screenHeight);
        localPos.y = -downUnit;
        localPos.z = 0;

        m_info_tran.localPosition = localPos;
            
    }
        
    void CancleShowPlayerName( )
    {
        _data = null;
        m_info_tran.gameObject.TrySetActive(false);
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener<BasePlayer, bool>(GameEvent.UI_SHOW_AIM_PLAYER_NAME, ShowPlayerName);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CANCEL_AIM_PLAYER_NAME, CancleShowPlayerName);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int,int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
    }

}