﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UIDragable : MonoBehaviour 
{

	// Use this for initialization
    private RectTransform m_parentRectTransform;
    private Vector3 m_originalLocalPosition;
    private Vector2 m_originalPointerPosition;
    
    private Vector2 m_posLastFingle1;
    private Vector2 m_posLastFingle2;

    private float m_fAddLocalSize = 0.0f;

    bool m_bSelected = false;

    float m_fMinLocalScale = 0.2f;
    float m_fMaxLocalScale = 2.0f;
	void Start () 
    {
        m_parentRectTransform = gameObject.transform.parent as RectTransform;

        UGUIUpDownHandler.Get(gameObject).onPointerDown += OnPointerDown;
        UGUIDragHandler.Get(gameObject).onDrag += OnDrag;

	}
	
	// Update is called once per frame

    public void OnPointerDown( GameObject target, PointerEventData eventData)
    {

        PointerEventData data = eventData as PointerEventData;
        m_originalLocalPosition = gameObject.transform.localPosition; 

        RectTransformUtility.ScreenPointToLocalPointInRectangle( m_parentRectTransform, data.position, data.pressEventCamera, out m_originalPointerPosition);
    }

    public void OnDrag(GameObject target, PointerEventData data)
    {
        if ( Input.touches.Length > 1)
        {
            return;
        }

        PointerEventData eventData = data as PointerEventData;
        if (eventData.pointerDrag == null)
            return;

        

        Vector2 dragRectPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle( m_parentRectTransform, eventData.position, eventData.pressEventCamera, out dragRectPos);
        Vector3 offsetToOrigin = dragRectPos - m_originalPointerPosition;
        gameObject.transform.localPosition = m_originalLocalPosition + offsetToOrigin;

        Clamp();
    } 

    void Clamp()
    {
        RectTransform selfRectTransform = gameObject.transform as RectTransform;
        Vector3 curLocalPos = selfRectTransform.localPosition;
        float pivotX = (selfRectTransform.rect.width) / 2 * selfRectTransform.localScale.x;
        float pivotY = (selfRectTransform.rect.height) / 2 * selfRectTransform.localScale.x;
        Vector2 pivotVector = new Vector2(pivotX, pivotY);
        Vector3 minPosition = m_parentRectTransform.rect.min + pivotVector;
        Vector3 maxPosition = m_parentRectTransform.rect.max - pivotVector;

        curLocalPos.x = Mathf.Clamp(curLocalPos.x, minPosition.x, maxPosition.x);
        curLocalPos.y =  Mathf.Clamp(curLocalPos.y, minPosition.y, maxPosition.y);

        selfRectTransform.localPosition = curLocalPos;
        return;
    }

    void Update()
    {
        
        if (Input.touches.Length > 1 )
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            if ( m_bSelected )
            {
                Logger.Log("XXXXXXXXXX  touch in me XXXXXXXXXXX");
                if ( touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved )
                {
                    if( IsEnlarge( m_posLastFingle1,m_posLastFingle2,touch1.position,touch2.position ) )
                    {
                        Logger.Log("XXXXXXXXXX make to large XXXXXXXXXXX");
                        m_fAddLocalSize += 0.01f;
                    }
                    else
                    {
                        Logger.Log("XXXXXXXXXX make to small XXXXXXXXXXX");
                        m_fAddLocalSize -= 0.01f;
                    }
                }

                m_posLastFingle1 = touch1.position;
                m_posLastFingle2 = touch2.position;
            }
            

        }
    }

    void LateUpdate()
    {
        if ( Input.touchCount > 1 )
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            if ( m_bSelected )
            {
                Transform selfTransform = gameObject.transform;
                Vector3 localScale = selfTransform.localScale;
                float xScale = localScale.x + m_fAddLocalSize;
                if( xScale > m_fMaxLocalScale )
                {
                    xScale = m_fMaxLocalScale;
                }
                else if( xScale < m_fMinLocalScale )
                {
                    xScale = m_fMinLocalScale;
                }

                float yScale = localScale.y + m_fAddLocalSize;
                if (yScale > m_fMaxLocalScale)
                {
                    yScale = m_fMaxLocalScale;
                }
                else if (yScale < m_fMinLocalScale)
                {
                    yScale = m_fMinLocalScale;
                }

                float zScale = localScale.z + m_fAddLocalSize;
                if (zScale > m_fMaxLocalScale)
                {
                    zScale = m_fMaxLocalScale;
                }
                else if (zScale < m_fMinLocalScale)
                {
                    zScale = m_fMinLocalScale;
                }

                localScale.x = xScale;
                localScale.y = yScale;
                localScale.z = zScale;

                selfTransform.localScale = localScale;
                string strLog = string.Format("XXXXXXXXXX change localScale delta:{0} XXXXXXXXXXX", m_fAddLocalSize);
                Logger.Log(strLog);
            }
        }
        m_fAddLocalSize = 0.0f;
    }

    bool ScreenPointIsInSelf( Vector2 first,Vector2 second )
    {
        if ( RectTransformUtility.RectangleContainsScreenPoint( transform as RectTransform,first,Camera.main ) )
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform, second, Camera.main))
            {
                return true;
            }
        }

        return false;
    }

    bool ScreenPointIsRect( Vector2 point )
    {
        RectTransform rectTrans = transform as RectTransform;
        Vector2 topLeft = (Vector2)rectTrans.transform.position - ( rectTrans.sizeDelta / 2f );
        Rect objRect = new Rect(topLeft.x, topLeft.y, rectTrans.sizeDelta.x, rectTrans.sizeDelta.y);

        if (objRect.Contains( point ) )
        {
            return true;
        }

        return false;
    }

    bool IsEnlarge( Vector2 oP1,Vector2 op2,Vector2 nP1,Vector2 nP2 )
    {
        float oldLen = Vector2.Distance(oP1, op2);
        float newLen = Vector2.Distance(nP1, nP2);

        if( newLen > oldLen )
        {
            return true;
        }

        return false;
    }

    public bool selected
    {
        get
        {
            return m_bSelected;
        }
        set
        {
            m_bSelected = value;
        }
    }

    public float minLocalScale
    {
        get
        {
            return m_fMinLocalScale;
        }
        set
        {
            m_fMinLocalScale = value;
        }
    }

    public float maxLocalScale
    {
        get
        {
            return m_fMaxLocalScale;
        }
        set
        {
            m_fMaxLocalScale = value;
        }
    }
}

