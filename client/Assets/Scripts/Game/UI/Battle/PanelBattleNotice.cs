﻿using UnityEngine;
using System.Collections;

public class PanelBattleNotice : BasePanel
{
    //公共提示面板
    private BattleNoticePanel m_noticePanel;
    private GameObject m_goNoticePanel1;
    private BattleNoticePanel m_noticePanel2;
    private GameObject m_goNoticePanel2;
    private BattleNoticePanel m_noticeQueuePanel;
    private GameObject m_goNoticeQueuePanel1;
    private BattleNoticePanel m_noticeQueuePanel2;
    private GameObject m_goNoticeQueuePanel2;

    public PanelBattleNotice()
    {
        SetPanelPrefabPath("UI/Battle/PanelBattleNotice");
        AddPreLoadRes("UI/Battle/PanelBattleNotice", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }

    public override void Init()
    {
        if (WorldManager.singleton.gameStartTime != 0)
        {
            GameStartNotice();
        }
    }

    public override void InitEvent()
    {

    }

    public override void Update()
    {

    }

    public override void OnShow()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public void PlayNoticePanel(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector3 scale = default(Vector3))
    {
        if (!IsOpen()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel1 == null)
        {
            m_goNoticePanel1 = new GameObject("NoticePanel");
            m_goNoticePanel1.AddComponent<CanvasGroup>().blocksRaycasts = false;
            m_goNoticePanel1.transform.SetParent(m_tran, false);
            m_goNoticePanel1.AddComponent<RectTransform>();
            m_noticePanel = m_goNoticePanel1.AddMissingComponent<BattleNoticePanel>();
        }
        if (scale == default(Vector3))
        {
            m_goNoticePanel1.transform.localScale = Vector3.one;
        }
        else
        {
            m_goNoticePanel1.transform.localScale = scale;
        }
        m_noticePanel.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanel(string imgAtlas, string img, float duration = 3, Vector2 pos = default(Vector2), NoticePanelProp prop = null, bool voice = true, Vector3 scale = default(Vector3))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanel(sprite, duration, voice, pos, prop, scale);
    }

    public void StopNoticePanel()
    {
        if (m_noticePanel != null) m_noticePanel.stop();
    }
    //通告面板2
    public void PlayNoticePanel2(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector3 scale = default(Vector3))
    {
        if (!IsOpen()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticePanel2 == null)
        {
            m_goNoticePanel2 = new GameObject("NoticePanel2");
            m_goNoticePanel2.transform.SetParent(m_tran, false);
            m_goNoticePanel2.AddComponent<RectTransform>();
            m_noticePanel2 = m_goNoticePanel2.AddMissingComponent<BattleNoticePanel>();
        }
        if (scale == default(Vector3))
        {
            m_goNoticePanel2.transform.localScale = Vector3.one;
        }
        else
        {
            m_goNoticePanel2.transform.localScale = scale;
        }
        m_noticePanel2.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanel2(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector3 scale = default(Vector3))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanel2(sprite, duration, voice, pos, prop, scale);
    }

    public void StopNoticePanel2()
    {
        if (m_noticePanel2 != null) m_noticePanel2.stop();
    }
    //通告面板3(加入队列)
    public void PlayNoticePanelQueue(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        if (!IsOpen()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticeQueuePanel1 == null)
        {
            m_goNoticeQueuePanel1 = new GameObject("NoticePanelQueue");
            m_goNoticeQueuePanel1.transform.SetParent(m_tran, false);
            //设置通知面板组件不接受点击事件，防止组件拦截下面的操作按钮组件事件
            var canvas = m_goNoticeQueuePanel1.AddComponent<CanvasGroup>();
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            AddRectTransform(m_goNoticeQueuePanel1, rootPos);
            m_noticeQueuePanel = m_goNoticeQueuePanel1.AddMissingComponent<BattleNoticePanel>();
            m_noticeQueuePanel.useQueue = true;
        }
        m_noticeQueuePanel.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanelQueue(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanelQueue(sprite, duration, voice, pos, prop, rootPos);
    }

    public void StopNoticePanelQueue()
    {
        if (m_noticeQueuePanel != null) m_noticeQueuePanel.stop();
    }

    public void PlayNoticePanelQueue2(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        if (!IsOpen()) return;//panelbattle面板还未初始化完成不能使用
        if (m_goNoticeQueuePanel2 == null)
        {
            m_goNoticeQueuePanel2 = new GameObject("NoticePanelQueue");
            m_goNoticeQueuePanel2.transform.SetParent(m_tran, false);
            //设置通知面板组件不接受点击事件，防止组件拦截下面的操作按钮组件事件
            var canvas = m_goNoticeQueuePanel2.AddComponent<CanvasGroup>();
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
            AddRectTransform(m_goNoticeQueuePanel2, rootPos);
            m_noticeQueuePanel2 = m_goNoticeQueuePanel2.AddMissingComponent<BattleNoticePanel>();
            m_noticeQueuePanel2.useQueue = true;
        }
        m_noticeQueuePanel2.play(sprite, duration, voice, pos, prop);
    }

    public void PlayNoticePanelQueue2(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null, Vector2 rootPos = default(Vector2))
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        PlayNoticePanelQueue2(sprite, duration, voice, pos, prop, rootPos);
    }

    public void StopNoticePanelQueue2()
    {
        if (m_noticeQueuePanel2 != null) m_noticeQueuePanel2.stop();
    }

    private void AddRectTransform(GameObject go, Vector2 pos)
    {
        go.AddMissingComponent<RectTransform>().anchoredPosition = pos;
    }


    #region 战斗开始前提醒
    public void GameStartNotice()
    {
        int leftTime = WorldManager.singleton.gameStartTime - TimeUtil.GetNowTimeStamp();

        if (leftTime > 0 && IsOpen())
        {
            WorldManager.singleton.gameStartTime = 0;
            NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
            prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "num_o_second", pos = new Vector2(160, 145) });
            prop.numIconList.Add(new ImgNumProp() { preName = "num_o", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(140, 150) });
            PlayNoticePanel(AtlasName.Battle, "battleReadyText", leftTime, new Vector2(0, 150), prop);
        }
    }
    #endregion

}
