﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;




public class PanelFlopReward :BasePanel {

    private GameObject[] m_cardArray = new GameObject[5];
    private Image[] m_imgArray = new Image[5];
    private Text[] m_costTexts = new Text[5];
    private Image[] m_costImgs = new Image[5];
    private GameObject[] m_buyBtns = new GameObject[5];
    private GameObject[] m_buyFrees = new GameObject[5];
    private GameObject[] m_questionArr = new GameObject[5];
    private Text[] m_itemNameTexts = new Text[5];
    private GameObject m_mask;
    private GameObject m_nextBtn;
    private Text m_diamondCostTxt;
    private Text m_goldCostTxt;
    private Text m_countDownTime;
    private UIEffect m_zhuanquan;    
    private UIEffect m_zha;    
    private UIEffect m_shan;    
    private Animator m_animator;
    private const float m_halfRotateTime = 0.2f;
    private const float m_tweenPosTime = 0.4f;
    private Quaternion m_halfQua;
    private Quaternion m_fullQua;
    private static toc_player_flop_reward m_protoData;
    private static toc_player_flop_reward m_oldProtoData;
    private string[] m_colors = new string[] { "brozeCard", "silverCard", "goldenCard" };    
    private const string m_cardBg = "cardBackGround";
    public static ItemInfo[] items;
    private bool m_needHidePanel;
    private int m_allCostDiamond;
    private int m_allCostGold;
    private static float m_protoRecvTime;
    private static List<int> m_randColors = new List<int>();
    public PanelFlopReward()
    {
        SetPanelPrefabPath("UI/Battle/PanelFlopReward");
        AddPreLoadRes("UI/Battle/PanelFlopReward", EResType.UI);
        AddPreLoadRes("Atlas/FlopCard", EResType.Atlas);
        AddPreLoadRes("Effect/UI_fanpai_zha", EResType.UIEffect);
        AddPreLoadRes("Effect/UI_fanpai_shan", EResType.UIEffect);
        AddPreLoadRes("Effect/UI_fanpai_zhuanquan", EResType.UIEffect);
    }

    public override void Init()
    {
        for (int i = 0; i < 5;i++ )
        {
            Transform tran = m_tran.Find(string.Format("UI_fanpai/card{0}", i));
            m_cardArray[i] = tran.gameObject;
            m_imgArray[i] = tran.Find("item").GetComponent<Image>();
            m_costTexts[i] = tran.Find("buyBtn/count").GetComponent<Text>();
            m_costImgs[i] = tran.Find("buyBtn/GameObject").GetComponent<Image>();
            m_buyBtns[i] = tran.Find("buyBtn").gameObject;
            m_buyFrees[i] = tran.Find("buyFrre").gameObject;
            m_questionArr[i] = tran.Find("question").gameObject;
            m_itemNameTexts[i] = tran.Find("item/itemName").GetComponent<Text>();
            m_cardArray[i].AddComponent<SortingOrderRenderer>();
            SortingOrderRenderer.RebuildAll();
        }
        m_mask = m_tran.Find("mask").gameObject;
        m_mask.AddComponent<SortingOrderRenderer>();
        SortingOrderRenderer.RebuildAll();
        m_halfQua =new Quaternion(0,0,0,0);
        m_halfQua.eulerAngles = new Vector3(0, 90, 0);
        m_fullQua = new Quaternion(0,0,0,0);
        m_fullQua.eulerAngles = new Vector3(0, 359.99f, 0);
        m_zha = UIEffect.ShowEffect(m_tran.Find("UI_fanpai/zha"), "UI_fanpai_zha", Vector3.zero);
        m_zha.Hide();
        m_shan = UIEffect.ShowEffect(m_tran.Find("UI_fanpai/shan"),"UI_fanpai_shan", new Vector3(-150,0,0)); 
        m_shan.Hide();               
        m_zhuanquan = UIEffect.ShowEffect(m_tran.Find("UI_fanpai/zhuanquan"), "UI_fanpai_zhuanquan", Vector3.zero);
        m_zhuanquan.Hide();
        m_animator = m_tran.Find("UI_fanpai").GetComponent<Animator>();
        m_animator.enabled = false;
        m_nextBtn = m_tran.Find("nextBtn").gameObject;
        m_diamondCostTxt = m_tran.Find("openAll/costDiamond").GetComponent<Text>();
        m_goldCostTxt = m_tran.Find("openAll/costGold").GetComponent<Text>();
        m_countDownTime = m_tran.Find("nextBtn/countdown").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        for(int i=0;i<m_cardArray.Length;i++)
        {            
            //UGUIClickHandler.Get(m_buyBtns[i]).onPointerClick += ClickBuy;
            //UGUIClickHandler.Get(m_cardArray[i].transform.Find("buyFrre")).onPointerClick += ClickBuy;
            UGUIClickHandler.Get(m_cardArray[i],"").onPointerClick += ClickFlop;
        }        
        UGUIClickHandler.Get(m_nextBtn).onPointerClick += ClickNext;
        UGUIClickHandler.Get(m_diamondCostTxt.transform.parent).onPointerClick += ClickFlopAll;
        UGUIClickHandler.Get(m_mask).onPointerClick += ClickMaxk;
    }

    private void ClickMaxk(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        
    }

    private void ClickFlopAll(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (PlayerSystem.roleData.diamond < m_allCostDiamond)
        {
            TipsManager.Instance.showTips("钻石不足");
            return;
        }
        else if(PlayerSystem.roleData.coin<m_allCostGold)
        {
            TipsManager.Instance.showTips("金币不足");
            return;
        }
        else
        {
            for(int i=0;i<5;i++)
            {
                int color=0;
                if(TryGetData(i,ref  color)==null)
                {
                    NetLayer.Send(new tos_player_flop_reward() { pos = (i + 1) });     
                }
            }
        }
    }

    private void ClickNext(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if(m_protoData!=null&&m_protoData.color_list.Length>0&&m_protoData.item_list.Length==0)
        {
            NetLayer.Send(new tos_player_flop_reward() { pos = 1 });
            TimerManager.SetTimeOut(2f, () => { HidePanel(); });
        }
        else
        {
            HidePanel();
        }        
    }

    private void ClickFlop(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        string name = target.name;
        int index = name[name.Length - 1] - '0';
        int color = 0;

        ItemInfo card = TryGetData(index,ref color);
        if (card == null)
        {
            if(m_protoData.item_list.Length!=0)
            {
                var cfg = GetCfgByTime(m_protoData.item_list.Length + 1);
                if(cfg.MoneyType=="DIAMOND"&&PlayerSystem.roleData.diamond<cfg.Cost)
                {
                    TipsManager.Instance.showTips("钻石不足");
                    return;
                }
                else if(PlayerSystem.roleData.coin<cfg.Cost)
                {
                    TipsManager.Instance.showTips("金币不足");
                    return;
                }
            }           
            NetLayer.Send(new tos_player_flop_reward() { pos = (index + 1) });
            //Debug.LogError(string.Format("send:{0}", index + 1));
        }     
        else
        {
            AudioManager.PlayUISound(AudioConst.btnClick);
        }
    }
 
    private ConfigFlopCostLine GetCfgByTime(int time)
    {
        var arr = ConfigManager.GetConfig<ConfigFlopCost>().m_dataArr;
        for(int i=0;i<arr.Length;i++)
        {
            if (arr[i].Time == time)
                return arr[i];
        }
        return null;
    }

    public override void OnShow()
    {                
        if (HasParams()) //需要显示翻牌特效
        {
            m_shan.Hide();
            m_zhuanquan.Hide();
            m_zha.Hide();
            m_diamondCostTxt.transform.parent.gameObject.TrySetActive(false);
            m_shan.m_goEffect.transform.localPosition = new Vector3(-150, 0, 0);
            m_params = null;
            m_nextBtn.TrySetActive(false);
            
            m_mask.TrySetActive(true);            
            m_animator.enabled = true;
            for (int i = 0; i < m_protoData.color_list.Length; i++) //先显示被混淆的卡牌颜色
            {
                //int index = m_protoData.color_list[i] - 1;
                int index = m_randColors[i] - 1;
                m_cardArray[i].GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.FlopCard, m_colors[index]));
                m_buyBtns[i].TrySetActive(false);
                m_buyFrees[i].TrySetActive(false);
            }                  
            TimerManager.SetTimeOut(1f, () => //播放转圈，代码换牌
            {
                if (m_zhuanquan==null)
                    return;
                m_zhuanquan.Play();
                AudioManager.PlayUISound(AudioConst.washCard);
                for (int i = 0; i < m_cardArray.Length;i++ )
                {
                    m_cardArray[i].GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.FlopCard, m_cardBg));
                } 
            });
            TimerManager.SetTimeOut(1.9f, () => //播发炸特效
            {
                if (m_zha == null)
                    return;
                m_zha.Play();
            });
            for(int i=0;i<m_cardArray.Length;i++) //播放闪光特效
            {
                int num = i;
                TimerManager.SetTimeOut(2.4f + 0.45f * num, () =>
                {
                    if (m_shan == null)
                        return;
                    m_shan.m_goEffect.transform.localPosition += new Vector3(150, 0, 0);
                    m_shan.Play();
                });
            }
            TimerManager.SetTimeOut(4.5f, () => //动画结束
            {
                if (m_mask == null)
                    return;
                m_mask.TrySetActive(false);                
                m_nextBtn.TrySetActive(true);
                UpdateViewByData();                
                m_animator.enabled = false;
            });
        }     
        else
        {
            UpdateViewByData();
        }
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            Screen.lockCursor = false;
        }
    }

    private void UpdateViewByData()
    {
        int costIndex = m_protoData.item_list.Length;
        m_diamondCostTxt.transform.parent.gameObject.TrySetActive(true);
        for(int i=0;i<m_cardArray.Length;i++)
        {
            int color = 0;
            ItemInfo card = TryGetData(i,ref color);
            ItemInfo oldCard=TryGetOldData(i);
            GameObject cardObj = m_cardArray[i];
            GameObject question = m_questionArr[i];
            Image img = m_imgArray[i];
            GameObject freeBuy = m_buyFrees[i];
            GameObject buyBtn = m_buyBtns[i];
            Text cost = m_costTexts[i];
            Image costImg = m_costImgs[i];
            Text itemName = m_itemNameTexts[i];
            if(oldCard==null)
            {
                if (card != null)//需要显示翻牌效果
                {
                    RotateCard(cardObj, () =>
                    {
                        if(freeBuy.activeSelf) 
                            freeBuy.TrySetActive(false);
                        if(buyBtn.activeSelf) 
                            buyBtn.TrySetActive(false);
                        cardObj.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.FlopCard, m_colors[color - 1]));
                        if(!img.gameObject.activeSelf) 
                            img.gameObject.TrySetActive(true);
                        ConfigItemLine cfg = ItemDataManager.GetItem(card.ID);
                        img.SetSprite(ResourceManager.LoadIcon(card.ID), cfg!=null?cfg.SubType:"");
                        img.SetNativeSize();
                        itemName.text = GetItemName(card);
                        question.TrySetActive(false);
                    }, () =>
                    {
                        TimerManager.SetTimeOut(0.5f, () =>
                        {
                            if (m_needHidePanel)
                            {
                                m_needHidePanel = false;
                                HidePanel();
                            }
                        });
                    });
                    AudioManager.PlayUISound(AudioConst.turnCard);
                }
                else //展示待翻牌效果
                {
                    cardObj.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite( AtlasName.FlopCard, m_cardBg));
                    bool showfree = costIndex == 0 ? true : false;
                    bool showBuy = costIndex == 0 ? false : true;
                    if(freeBuy.activeSelf!=showfree)
                        freeBuy.TrySetActive(showfree);
                    if(buyBtn.activeSelf!=showBuy)
                        buyBtn.TrySetActive(showBuy);
                    if (showBuy)
                        //cost.text = m_costStrs[costIndex-1];
                        UpdateSingleCost(cost,costImg, costIndex);
                    if (img.gameObject.activeSelf)
                        img.gameObject.TrySetActive(false);
                    if (!question.activeSelf)
                        question.TrySetActive(true);
                }
            }          
        }
        UpdateAllCost();
    }


    private void UpdateSingleCost(Text txt,Image img,int index )
    {
        var arr = ConfigManager.GetConfig<ConfigFlopCost>().m_dataArr;
        for(int i=0;i<arr.Length;i++)
        {
            if(arr[i].Time==(index+1))
            {
                txt.text = arr[i].Cost.ToString();
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, (arr[i].MoneyType=="DIAMOND"? "diamond":"gold" )));
                break;
            }
        }
        img.SetNativeSize();
    }

    private void UpdateAllCost()
    {
        int len = m_protoData.item_list.Length;
        if(len==5)
        {
            m_diamondCostTxt.transform.parent.gameObject.TrySetActive(false);
        }
        m_allCostDiamond=0;
        m_allCostGold=0;
        //int color = 0;
        var arr = ConfigManager.GetConfig<ConfigFlopCost>().m_dataArr;
        for (int i = 0; i < arr.Length;i++ )
        {
            if (arr[i].Time <= m_protoData.item_list.Length)
                continue;
            if (arr[i].MoneyType == "DIAMOND")
                m_allCostDiamond += arr[i].Cost;
            else m_allCostGold += arr[i].Cost;
        }
        m_diamondCostTxt.text = m_allCostDiamond.ToString();
        m_goldCostTxt.text = m_allCostGold.ToString();
    }

    private ItemInfo TryGetData(int index,ref int color)
    {
        ItemInfo data = null;
        for (int i = 0; i < m_protoData.pos_list.Length;i++ )
        {
            if(m_protoData.pos_list[i]==(index+1))
            {
                if (i < m_protoData.item_list.Length)
                {
                    data = m_protoData.item_list[i];
                    color = m_protoData.color_list[i];
                }
                break;
            }
        }
       return data;
    }

     private ItemInfo TryGetOldData(int index)
     {
        ItemInfo data = null;
        if(m_oldProtoData==null)
            return null;
        for (int i = 0; i < m_oldProtoData.pos_list.Length;i++ )
        {
            if(m_oldProtoData.pos_list[i]==(index+1))
            {
                if (i < m_oldProtoData.item_list.Length)
                    data = m_oldProtoData.item_list[i];
                break;
            }
        }
       return data;
     }


    


    private void RotateCard(GameObject obj,Action halfCallBack,Action fullCallBack )
    {
        obj.transform.eulerAngles = new Vector3(0, 0, 0);
        TweenRotation.Begin(obj, m_halfRotateTime, m_halfQua, () =>
            {
                if(halfCallBack!=null)
                {
                    halfCallBack();                    
                }
                obj.transform.eulerAngles = new Vector3(0, 270, 0);
                TweenRotation.Begin(obj, m_halfRotateTime, m_fullQua);
            });
        //TimerManager.SetTimeOut(m_halfRotateTime ,, true);
        TimerManager.SetTimeOut(m_halfRotateTime * 2f , fullCallBack, true);
    }


    private string GetItemName(ItemInfo info)
    {
        ConfigItemLine cfg = ItemDataManager.GetItem(info.ID);
        if (cfg == null)
            return "";
        if (info.day <= 0)
            return cfg.DispName;
        else
        {
            ItemTime time = ItemDataManager.FloatToDayHourMinute(info.day);
            return string.Format("{0}{1}", cfg.DispName, time.ToString());
        }
    }

    public override void OnHide()
    {        
        m_protoData = null;
        m_oldProtoData = null;
        NetLayer.Send(new tos_player_flop_reward() { pos = 0 });
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            Screen.lockCursor = true;
        }
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        m_protoData = null;
        m_oldProtoData = null;        
    }

    public override void Update()
    {
        int time=(int)(Time.realtimeSinceStartup - m_protoRecvTime);
        m_countDownTime.text = (ConfigManager.GetConfig<ConfigMisc>().GetLine("close_delay_sec").ValueInt - time).ToString()+"s";
    }

    private static void GetRandowColors(int[] colors)
    {
        m_randColors.Clear();
        for(int i=0;i<colors.Length;i++)
        {
            m_randColors.Insert(UnityEngine.Random.Range(0, m_randColors.Count), colors[i]);
        }
    }

    static void Toc_player_flop_reward(toc_player_flop_reward proto)
    {
        var panelBattle = UIManager.GetPanel<PanelBattle>();
        if (panelBattle != null)
        {
            panelBattle.EnableMouseEvent = true;
        }
        if (proto.color_list.Length == 0)            
            return;        
        m_oldProtoData=m_protoData;
        m_protoData = proto;
        items = proto.item_list;
        var panel=UIManager.GetPanel<PanelFlopReward>() ;
        if (panel==null)
        {
            m_protoRecvTime = Time.realtimeSinceStartup;
            GetRandowColors(proto.color_list);
            UIManager.PopPanel<PanelFlopReward>(new object[] { true });
        }
        else if(panel.IsInited())
        {
            UIManager.GetPanel<PanelFlopReward>().OnShow();
        }         
        //foreach(int color in m_protoData.color_list )
        //{
        //   Logger.Error(string.Format("color:{0}", color));
        //}
        //foreach(int pos in m_protoData.pos_list)
        //{
        //   Logger.Error("pos:" + pos.ToString());
        //}
    }

}
