﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyTouch
{
    public Vector2 deltaPosition;
    public float changedTime;
    public int fingerId;
    public TouchPhase phase;
    public Vector2 position;
    //public Vector2 rawPosition;
    public int tapCount;

    public MyTouch()
    {
    }

    public void SetTouch(Touch touch)
    {
        deltaPosition = touch.deltaPosition;
        changedTime = Time.realtimeSinceStartup - touch.deltaTime;
        fingerId = touch.fingerId;
        phase = touch.phase;
        position = touch.position;
        //rawPosition = touch.rawPosition;
        tapCount = touch.tapCount;
    }

    public float deltaTime { get { return Time.realtimeSinceStartup - changedTime; } }
}

public class PanelTouch : MonoBehaviour
{
    const int MAX_COUNT = 3;

    List<MyTouch> m_touchList = new List<MyTouch>(MAX_COUNT);
    bool[] isDown = new bool[MAX_COUNT];


    void Awake()
    {
        for (int i = 0; i < MAX_COUNT; i++)
            m_touchList.Add(null);
    }

    void Update()
    {
        bool mouse = Input.GetKey(KeyCode.Mouse0);
        for (int i = 0; i < MAX_COUNT; i++)
        {
            MyTouch touch = m_touchList[i];
            if (touch != null && touch.phase == TouchPhase.Ended)
                m_touchList[i] = touch = null;

            bool key = Input.GetKey(KeyCode.Alpha1 + i);
            if (touch == null)
            {
                if (!mouse || !key)
                    continue;
                touch = new MyTouch();
                touch.fingerId = i + 100;
                touch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                touch.changedTime = Time.realtimeSinceStartup;
                touch.tapCount = 1;
                touch.phase = TouchPhase.Began;
                m_touchList[i] = touch;
                continue;
            }

            if (!key && Input.GetKeyUp(KeyCode.Alpha1 + i))
            {
                touch.deltaPosition = Vector2.zero;
                touch.changedTime = Time.realtimeSinceStartup;
                touch.phase = mouse ? TouchPhase.Stationary : TouchPhase.Ended;
                continue;
            }

            Vector2 pos;
            if (mouse && key)
                pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            else
                pos = touch.position;

            touch.deltaPosition = pos - touch.position;
            touch.position = pos;
            if (touch.deltaPosition == Vector2.zero)
            {
                touch.phase = TouchPhase.Stationary;
            }
            else
            {
                touch.phase = TouchPhase.Moved;
                touch.changedTime = Time.realtimeSinceStartup;
            }
        }
    }

    void OnGUI()
    {
        for (int i = 0; i < MAX_COUNT; i++)
        {
            MyTouch touch = m_touchList[i];
            if (touch == null)
                continue;
            GUILayout.BeginArea(new Rect(touch.position.x - 5, Screen.height - touch.position.y - 10, 100, 100));
            char c = (char)('①' + i);
            GUILayout.Label(c.ToString());
            GUILayout.EndArea();
        }
    }

    public MyTouch[] GetTouchs()
    {
        List<MyTouch> list = new List<MyTouch>();
        for (int i = 0; i < MAX_COUNT; i++)
        {
            MyTouch touch = m_touchList[i];
            if (touch != null)
                list.Add(touch);
        }
        return list.ToArray();
    }
}
