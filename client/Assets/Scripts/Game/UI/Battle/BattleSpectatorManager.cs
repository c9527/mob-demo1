﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BattleSpectatorManager : MonoBehaviour
{
    public static BattleSpectatorManager singleton;

    private GameObject m_spectatorInfoBtn;
    private Image m_spectatorInfoImg;
    private Text m_spectatorInfoText;

    private bool m_bIsPanelBattleInited;

    private bool m_released;

    #region 引擎方法
    void Awake()
    {
        singleton = this;
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.AddEventListener<long>(GameEvent.SPECTATOR_JOIN, OnSpectatorJoin);
        GameDispatcher.AddEventListener<long>(GameEvent.SPECTATOR_LEAVE, OnSpectatorLeave);


        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
            return;
        }
    }

    void OnDestory()
    {
        m_released = true;

        GameDispatcher.RemoveEventListener<long>(GameEvent.SPECTATOR_JOIN, OnSpectatorJoin);
        GameDispatcher.RemoveEventListener<long>(GameEvent.SPECTATOR_LEAVE, OnSpectatorLeave);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
    }

    #endregion

    #region 私有方法
    private void Init()
    {
        m_spectatorInfoBtn = UIManager.GetPanel<PanelBattle>().spectatorInfo;

        if (m_spectatorInfoBtn == null)
            return;
        m_spectatorInfoImg = m_spectatorInfoBtn.GetComponent<Image>();
        m_spectatorInfoText = m_spectatorInfoBtn.transform.Find("SpectatorInfoTextBG/SpectatorInfoText").GetComponent<Text>();
        UGUIClickHandler.Get(m_spectatorInfoBtn).onPointerClick += OnClickwatchInfo;
        if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
        {
            m_spectatorInfoImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Room, "dou"));
        }
        else
        {
            m_spectatorInfoImg.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "douPc"));
        }
        RefreshSpectatorInfo();
    }

    private void OnCreateBattlePanel()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        m_bIsPanelBattleInited = true;
        Init();
    }

    private void OnSceneLoaded()
    {
        if (m_released)
            return;

        if (m_bIsPanelBattleInited && m_spectatorInfoBtn == null)
            Init();
    }

    private void OnClickwatchInfo(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelBattleRecord>(new object[] { BattleRecordType.Spectator}, true);
    }

    private void OnSpectatorJoin(long pid)
    {
        RefreshSpectatorInfo();
    }

    private void OnSpectatorLeave(long pid)
    {
        RefreshSpectatorInfo();
    }

    private void RefreshSpectatorInfo()
    {
        if (m_spectatorInfoText != null && PlayerBattleModel.Instance.battle_info != null)
        {
            List<SBattleSpectator> spectators = PlayerBattleModel.Instance.battle_info.spectators;
            int spectatorCount = spectators.Count;
            if (spectatorCount == 0)
            {
                m_spectatorInfoBtn.TrySetActive(false);
            }
            else
            {
                m_spectatorInfoBtn.TrySetActive(true);
                m_spectatorInfoText.text = string.Format("{0}/50", spectatorCount);
            }
            
        }
    }
    #endregion

    #region 公有方法

    #endregion

}
