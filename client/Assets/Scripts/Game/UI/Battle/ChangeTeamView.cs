﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class ChangeTeamView : MonoBehaviour
{
    // Use this for initialization
    Transform m_Parent;
    private static GameObject m_rootTeamViewer;

    GameObject goTip;
    GameObject goChangeBtn;

    //Text m_playerName;
    //Text m_txtGunName;
    //Text m_txtUpTip;

    static int m_iSelectPlayerId = 0;
    static long m_watchedPlayerPid = -1;
    int m_selectCamp = 0;
    //ConfigItemRole m_cir;
    List<int> m_listCamps;
    int m_iCampIndex = 0;

    private GameObject m_goNameBg;
    private GameObject m_goNameTxt;

    private Button m_btnLeft;
    private Button m_btnRight;
    private UGUIClickHandler m_chLeft;
    private UGUIClickHandler m_chRight;

    private int m_dieSwitchTimer = -1;
    private bool m_bSwitchHide;
    private GameObject m_goContent;
    private bool m_bIsForceChangeTeamCamp; //强制转到队友上
    private bool m_skipPressEvent;         //跳过按钮事件

    public void Init(Transform parent)
    {
        m_Parent = parent;
        //m_cir = ConfigManager.GetConfig<ConfigItemRole>();
        m_rootTeamViewer = ResourceManager.LoadUI("UI/Battle/ViewTeamerView");
        m_rootTeamViewer.name = "TeamerView";
        m_rootTeamViewer.transform.SetParent(m_Parent, false);
        m_goContent = m_rootTeamViewer.transform.Find("content").gameObject;
        //RectTransform viewerRectTransform = m_rootTeamViewer.AddMissingComponent<RectTransform>();
        //Vector2 anchor = new Vector2(0.5f, 0);
        //viewerRectTransform.anchorMin = anchor;
        //viewerRectTransform.anchorMax = anchor;
        //viewerRectTransform.anchoredPosition = new Vector2(0, 60);
        //viewerRectTransform.localScale = new Vector3(1, 1, 1);
        ShowViewer(false);
        
        //m_playerName = m_rootTeamViewer.transform.Find("name_txt").GetComponent<Text>();
        goTip = m_rootTeamViewer.transform.Find("tip_img").gameObject;
        goChangeBtn = m_goContent.transform.Find("changeBtn").gameObject;
        goChangeBtn.TrySetActive(false);

        m_chLeft = UGUIClickHandler.Get(m_goContent.transform.Find("PreBtn"));
        m_chRight = UGUIClickHandler.Get(m_goContent.transform.Find("NextBtn"));
        m_btnLeft = m_goContent.transform.Find("PreBtn").GetComponent<Button>();
        m_btnRight = m_goContent.transform.Find("PreBtn").GetComponent<Button>();

        //m_txtGunName = m_rootTeamViewer.transform.Find("desc_txt").GetComponent<Text>();
        //m_txtUpTip = m_rootTeamViewer.transform.Find("upTip_txt").GetComponent<Text>();

        if (WorldManager.singleton.isFightInTurnModeOpen)
        {
            GameDispatcher.AddEventListener(GameEvent.PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED, OnArenaInfoUpdate);

            m_chLeft.onPointerClick += OnClickLeft;
            m_chRight.onPointerClick += OnClickRight;
        }
        else
        {
            InitEvent();
        }

        GameDispatcher.AddEventListener<int>(GameEvent.PLAYER_ACTORPROP_UPDATE, OnPlayerActorPropUpdate);
        GameDispatcher.AddEventListener<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, EnableSwitch);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.UI_SWITCH_FREE_VIEW, SwitchFreeView);
        GameDispatcher.AddEventListener(GameEvent.UI_SWITCH_PERSON_VIEW, SwitchPersonView);

        if (WorldManager.singleton.isViewBattleModel)
        {
            watchModelView();
        }
        else if (MainPlayer.singleton != null && MainPlayer.singleton.isDead)
        {
            OnPlayerStartDie();
        }
    }

    void InitEvent()
    {
        m_chLeft.onPointerClick += OnClickLeft;
        m_chRight.onPointerClick += OnClickRight;

        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.AddEventListener(GameEvent.PLAYER_HIDE_SWITCH, SwitchViewHide);
        GameDispatcher.AddEventListener(GameEvent.PlAYER_HIDE_SWITCH_BACK, SwitchHideBack);
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        GameDispatcher.AddEventListener(GameEvent.INPUT_MOUSELEFT_ACTION, shortCutKeyMouseLeft);
        GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_MOUSERIGHT_ACTION, shortCutKeyMouseRight);
        GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_LEFTARROW_UP, shortCutKeyArrowLeft);
        GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_RIGHTARROW_UP, shortCutKeyArrowRight);
#endif
    }

    private void shortCutKeyArrowLeft()
    {
        if (PanelBattle.isPcChatLimit) return;
        if (m_rootTeamViewer.activeSelf)
        {
            OnClickLeft(null, null);
        }
    }

    private void shortCutKeyArrowRight()
    {
        if (PanelBattle.isPcChatLimit) return;
        if (m_rootTeamViewer.activeSelf)
        {
            OnClickRight(null, null);
        }
    }

    private void shortCutKeyMouseLeft()
    {
        if (m_rootTeamViewer.activeSelf)
        {
            OnClickLeft(null, null);
        }
    }

    private void shortCutKeyMouseRight(bool isDown)
    {
        if (isDown)
            if (m_rootTeamViewer.activeSelf)
            {
                OnClickRight(null, null);
            }
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener<int>(GameEvent.PLAYER_ACTORPROP_UPDATE, OnPlayerActorPropUpdate);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, OnPlayerLeave);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.RemoveEventListener(GameEvent.PLAYER_HIDE_SWITCH, SwitchViewHide);
        GameDispatcher.RemoveEventListener(GameEvent.PlAYER_HIDE_SWITCH_BACK, SwitchHideBack);
        GameDispatcher.RemoveEventListener(GameEvent.PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED, OnArenaInfoUpdate);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.UI_ENABLE_SWITCH_VIEW_BTN, EnableSwitch);
        GameDispatcher.RemoveEventListener(GameEvent.UI_SWITCH_FREE_VIEW, SwitchFreeView);
        GameDispatcher.RemoveEventListener(GameEvent.UI_SWITCH_PERSON_VIEW, SwitchPersonView);
        m_iSelectPlayerId = 0;
        m_watchedPlayerPid = -1;
#if UNITY_WEBPLAYER || UNITY_STANDALONE
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_MOUSELEFT_ACTION, shortCutKeyMouseLeft);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOUSERIGHT_ACTION, shortCutKeyMouseRight);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_KEY_LEFTARROW_UP, shortCutKeyArrowLeft);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_KEY_RIGHTARROW_UP, shortCutKeyArrowRight);
#endif
    }

    void OnArenaInfoUpdate()
    {
        if (!WorldManager.singleton.isFightInTurnModeOpen || FightInTurnBehavior.singleton == null) return;
        if (FightInTurnBehavior.singleton.isSelfFighter)
        {
            ShowViewer(false);
            m_iSelectPlayerId = 0;
            m_watchedPlayerPid = -1;
        }
        else
        {
            if (WorldManager.singleton.isViewBattleModel)
            {
                watchModelView();
            }
            else if (MainPlayer.singleton != null && MainPlayer.singleton.isDead)
            {
                OnPlayerStartDie();
            }
        }
    }

    void ShowViewer(bool isShow)
    {
        //Logger.Error("show: " + isShow + " isShowBtn:" + isShowBtn);

        m_rootTeamViewer.TrySetActive(isShow);
        m_goContent.TrySetActive(isShow);
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_SWITCH_VIEW_BTN, isShow);
    }

    PlayerType GetPlayerType()
    {
        if(WorldManager.singleton.isViewBattleModel)
        {
            if (!WorldManager.singleton.isSurvivalTypeModeOpen && !WorldManager.singleton.isDotaModeOpen)
                return PlayerType.All;
            else
                return PlayerType.AllNoAI;
        }
        else
        {
            if (!WorldManager.singleton.isSurvivalTypeModeOpen && !WorldManager.singleton.isDotaModeOpen)
                return PlayerType.CampAll;
            else
                return PlayerType.CampNoAI;
        }
    }

    BasePlayer NextTeamPlayer(int dirc)
    {
        if (m_listCamps == null || m_listCamps.Count == 0)
            return null;

        if (m_listCamps.Count == 1)
        {
            BasePlayer teamer = WorldManager.singleton.GetPlayerById(m_listCamps[0]);
            if (teamer != null && teamer.gameObject != null && teamer.serverData != null && teamer.alive)
            {
                return teamer;
            }
        }
        else
        {

            int len = m_listCamps.Count;
            int oldIndex = m_iCampIndex;

            do
            {
                m_iCampIndex += dirc;
                m_iCampIndex += len;
                m_iCampIndex = m_iCampIndex % len;

                BasePlayer teamer = WorldManager.singleton.GetPlayerById(m_listCamps[m_iCampIndex]);
                //if(teamer != null) Logger.Error("next: " + teamer.PlayerName + ", isalive: " + teamer.alive);
                if (teamer != null && teamer.gameObject != null && teamer.serverData != null && teamer.alive)
                {
                    return teamer;
                }
            } while (m_iCampIndex != oldIndex);
        }
        return null;
    }

    void SwitchViewHide()
    {
        if (WorldManager.singleton.isHideModeOpen && m_rootTeamViewer != null)
        {
            if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }
            ShowViewer(true);

            m_listCamps = WorldManager.singleton.GetHideTeams(MainPlayer.singleton);
            BasePlayer hideteamer = NextTeamPlayer(1);
            if (hideteamer != null)
            {
                m_bSwitchHide = true;
                FlushPlayerInfo(hideteamer);
                GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, hideteamer.PlayerId);
            }
        }
    }

    void SwitchHideBack()
    {
        if (WorldManager.singleton.isHideModeOpen && m_rootTeamViewer != null)
        {
            m_bSwitchHide = false;
            //FlushPlayerInfo(MainPlayer.singleton);
            goTip.TrySetActive(false);
            GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_GUNLIST, false);
            ShowViewer(false);

            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, MainPlayer.singleton.PlayerId);

            if(MainPlayer.singleton.IsFrezee)
            {
                GameDispatcher.Dispatch(GameEvent.UI_SWITCH_FREE_VIEW);
            }
            else if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }
        }
    }

    void OnClickLeft(GameObject target, PointerEventData eventData)
    {
        if (m_skipPressEvent)
            return;
        
        BasePlayer curTeamer = WorldManager.singleton.GetPlayerById(m_iSelectPlayerId);
        if (curTeamer == null) return;
        if( PlayerBattleModel.Instance.battle_info.spectator_switch == true )
        {
            curTeamer.update_ESG_View = true;
        }
       
        m_watchedPlayerPid = curTeamer.Pid;
        if (WorldManager.singleton.isHideModeOpen)
        {
            if (MainPlayer.singleton != null)
            {
                if (MainPlayer.singleton.Camp == 1 || (MainPlayer.singleton.Camp == 4 && MainPlayer.singleton.isDead))
                {
                    m_listCamps = WorldManager.singleton.GetHumanTeams(curTeamer);
                }
                else
                {
                    m_listCamps = WorldManager.singleton.GetHideTeams(curTeamer);
                }
            }
            BasePlayer hideteamer = NextTeamPlayer(-1);
            if (hideteamer != null)
            {
                FlushPlayerInfo(hideteamer);
                GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, hideteamer.PlayerId);
            }
            return;
        }
        m_listCamps = WorldManager.singleton.GetCampTeams(curTeamer.Camp, GetPlayerType());

        //if(WorldManager.singleton.isSurvivalTypeModeOpen || WorldManager.singleton.isDotaModeOpen)
        //{
        //    for(int i = m_listCamps.Count - 1; i >=0 ; --i)
        //    {
        //
        //    }
        //}

        BasePlayer teamer = NextTeamPlayer(-1);
        if (teamer != null)
        {
            FlushPlayerInfo(teamer);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, teamer.PlayerId);
        }
    }

    void OnClickRight(GameObject target, PointerEventData eventData)
    {
        if (m_skipPressEvent)
            return;
        //Logger.Error("OnClickRight:" + m_iSelectPlayerId);
        BasePlayer curTeamer = WorldManager.singleton.GetPlayerById(m_iSelectPlayerId);
        if (curTeamer == null) return;
        if (PlayerBattleModel.Instance.battle_info.spectator_switch == true)
        {
            curTeamer.update_ESG_View = true;
        }
        if (WorldManager.singleton.isHideModeOpen)
        {
            if (MainPlayer.singleton != null)
            {
                if ((MainPlayer.singleton.Camp == 1) || (MainPlayer.singleton.Camp == 4 && MainPlayer.singleton.isDead))
                {
                    m_listCamps = WorldManager.singleton.GetHumanTeams(curTeamer);
                }
                else
                {
                    m_listCamps = WorldManager.singleton.GetHideTeams(curTeamer);
                }
            }
            BasePlayer hideteamer = NextTeamPlayer(1);
            if (hideteamer != null)
            {
                FlushPlayerInfo(hideteamer);
                GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, hideteamer.PlayerId);
            }
            return;
        }

        m_listCamps = WorldManager.singleton.GetCampTeams(curTeamer.Camp, GetPlayerType());
        BasePlayer teamer = NextTeamPlayer(1);
        if (teamer != null)
        {
            FlushPlayerInfo(teamer);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, teamer.PlayerId);
        }
    }


    void watchModelView()
    {
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        if (line == null || line.AllCamp.Length < 1) { Logger.Error("watchModelView, no camp---"); return; }
        //Logger.Error("watchModelView, camp: " + line.AllCamp[0]);
        m_listCamps = WorldManager.singleton.GetCampTeams(line.AllCamp[0], GetPlayerType());
        m_iCampIndex = 0;
        OnTeamerViewSwitchToNext();
        //TimerManager.SetTimeOut(0.25f, OnTeamerViewSwitchToNext);
        //Logger.Error("watchModelView");
    }

    void OnPlayerStartDie()
    {
        m_listCamps = WorldManager.singleton.GetCampTeams(MainPlayer.singleton.Camp, GetPlayerType());
        m_iCampIndex = 0;
        OnTeamerViewSwitchToNext();
    }

    void FlushPlayerInfo(BasePlayer player)
    {
        if (m_rootTeamViewer == null || player == null)
        {
            m_iSelectPlayerId = 0;
            m_watchedPlayerPid = -1;
            return;
        }
        //Logger.Error("player:" + player.RoleId +  "   player.canFirstPersonWatch:" + player.canFirstPersonWatch);
        ShowViewer(true);
        m_selectCamp = player.Camp;
        m_iSelectPlayerId = player.PlayerId;
        m_watchedPlayerPid = player.Pid;
        var prop0 = new RectTransformProp() { size = new Vector2(350, 30) };
        UIHelper.ShowSprite(ref m_goNameBg, "NameBg", AtlasName.BattleNew, "black_bg", new Vector2(-21, -70), true, prop0, m_goContent.transform, true);
        TextProp textProp = new TextProp() { horizontalWrap = HorizontalWrapMode.Overflow };
        UIHelper.ShowText(ref m_goNameTxt, player.PlayerName, true, new Vector2(0, -70), textProp, m_goContent.transform, true);

        //m_playerName.text = player.PlayerName;
        //m_txtUpTip.text = WorldManager.singleton.isViewBattleModel ? "当前为观战状态" : string.Empty;
        if (WorldManager.singleton.isHideModeOpen)
        {
            //m_txtUpTip.text = "";
            goTip.TrySetActive(false);
        }
        else
        {
            goTip.TrySetActive(true);
        }


        //Logger.Error("player:" + player.RoleId + "   player.isCancelDisplayWatchInfo:" + player.isCancelDisplayWatchInfo);
        if (player.isCancelDisplayWatchInfo)
            GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_GUNLIST, false);
        else
        {
            GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_GUNLIST, true);
            FlushWeaponInfo(m_iSelectPlayerId);
        }
    }

    void FlushWeaponInfo(int id)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(id);
        if (WorldManager.singleton.isHideModeOpen)
        {
            if (player != null && player.serverData != null && player.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                Sprite sp = ResourceManager.LoadHideIcon(player.configData.Icon);
                GameDispatcher.Dispatch<Sprite, string>(GameEvent.UI_UPDATE_GUNLIST2, sp, string.Empty);
                return;
            }
        }
        if (player != null && player.serverData != null)//显示玩家正在出战的枪支
        {
            ConfigItemWeaponLine last_weapon = null;
            if (!string.IsNullOrEmpty(player.serverData.propWeapon()))
            {
                last_weapon = ItemDataManager.GetItemWeapon(int.Parse(player.serverData.propWeapon()));
            }
            Sprite sp = null;
            string gunName = "";
            if (last_weapon != null)
            {
                sp = ResourceManager.LoadWeaponIcon(last_weapon.ID);
                gunName = last_weapon.DispName;
                //m_playerGun.SetSprite(ResourceManager.LoadWeaponIcon(last_weapon.ID));
                //m_playerGun.enabled = true;
                //m_playerGun.SetNativeSize();
                //m_txtGunName.text = last_weapon.DispName;
            }
            else
            {
                //m_playerGun.enabled = false;
                //m_txtGunName.text = "";
            }

            GameDispatcher.Dispatch<Sprite, string>(GameEvent.UI_UPDATE_GUNLIST2, sp, gunName);
        }
    }

    void OnPlayerSetState(BasePlayer player)
    {
        if (player == null || player.serverData == null)
        {
            return;
        }

        if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            return;

        if (WorldManager.singleton.isRescueModeOpen == false)
        {
            return;
        }

        if (WorldManager.singleton.isViewBattleModel || WorldManager.singleton.isRescueModeOpen
            || WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KILLALL, GameConst.GAME_RULE_BURST, GameConst.GAME_RULE_GHOST))
        {
            if (player == MainPlayer.singleton || m_iSelectPlayerId == player.PlayerId)
            {
                if (player.isDead)
                {
                    m_listCamps = WorldManager.singleton.GetCampTeams(player.Camp, GetPlayerType());
                    m_iCampIndex = 0;
                    OnTeamerViewSwitchToNext();
                    //Logger.Error("OnPlayerSetState");
                }
                else if (player.isAlive)
                {
                    GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, player.PlayerId);
                }
            }
        }
    }

    void OnPlayerDie(BasePlayer killer, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (!m_bSwitchHide && !(dier is MainPlayer) && MainPlayer.singleton != null && MainPlayer.singleton.alive)
        {
            return;
        }

        if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
        {
            if( PlayerBattleModel.Instance.battle_info.spectator_switch == false )
            {
                FreeViewPlayer.singleton.Stop();
                GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
            }
        }

        var kill_weapon = ItemDataManager.GetItemWeapon(weapon);
        if (WorldManager.singleton.isViewBattleModel || WorldManager.singleton.isRescueModeOpen || WorldManager.singleton.isHideModeOpen
            || WorldManager.singleton.isFightInTurnModeOpen
            || WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KILLALL, GameConst.GAME_RULE_BURST, GameConst.GAME_RULE_GHOST)
            || WorldManager.singleton.isZombieTypeModeOpen
            //|| (WorldManager.singleton.isZombieModeOpen && kill_weapon != null && kill_weapon.Class == WeaponManager.WEAPON_TYPE_KNIFE)
            //|| (WorldManager.singleton.isZombieHeroModeOpen && ZombieHeroBehaviour.singleton.isZombieHeroStage)
            //|| (WorldManager.singleton.isZombieUltimateModeOpen && ZombieUltimateBehaviour.singleton.isZombieHeroStage)
            || (WorldManager.singleton.isTeamHeroModeOpen && killer != null && killer.serverData != null && killer.serverData.actor_type == GameConst.ACTOR_TYPE_HERO 
            && TeamHeroBehaviour.singleton != null && TeamHeroBehaviour.singleton.GetTeamHeroInfo(killer.PlayerId) != null && TeamHeroBehaviour.singleton.GetTeamHeroInfo(killer.PlayerId).heroLevel == 3)
            || (WorldManager.singleton.isBigHeadModeOpen && BigHeadBehavior.singleton != null && BigHeadBehavior.singleton.isFinalStage)
            || (WorldManager.singleton.isDotaModeOpen && DotaBigHeadBehavior.singleton != null && DotaBigHeadBehavior.singleton.isFinalStage)
         )
        {
            if (dier == MainPlayer.singleton || m_iSelectPlayerId == dier.PlayerId)
            {
                m_listCamps = WorldManager.singleton.GetCampTeams(dier.Camp, GetPlayerType());
                m_iCampIndex = 0;
                FlushPlayerInfo(dier);

                if (dier == MainPlayer.singleton)
                {
                    if (WorldManager.singleton.isRescueModeOpen)
                        ShowViewer(false);
                    else if (m_bSwitchHide)
                        m_bSwitchHide = false;
                }
                m_skipPressEvent = true;
                m_dieSwitchTimer = TimerManager.SetTimeOut(3, BeforeSwitchToNext, dier);
            }
        }
    }

    void OnPlayerRevive(string eventName, BasePlayer who)
    {
        if (who.PlayerId == m_iSelectPlayerId && (FreeViewPlayer.singleton == null || !FreeViewPlayer.singleton.isRuning))
        {
            FlushPlayerInfo(who);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, who.PlayerId);
        }

        if (who != MainPlayer.singleton)
        {
            return;
        }

        m_iSelectPlayerId = 0;
        m_watchedPlayerPid = -1;
        ShowViewer(false);

        if (MainPlayer.singleton.serverData != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
        {
            SwitchHideBack();
        }
    }

    void BeforeSwitchToNext(object dier)
    {
        m_skipPressEvent = false;

        if (WorldManager.singleton.isRescueModeOpen)
        {
            if (dier is MainPlayer || dier is FPWatchPlayer)
            {
                if (SceneCameraController.singleton != null)
                    SceneCameraController.singleton.LookMainPlayerDeadPoint();
            }
        }
        else
        {
            OnTeamerViewSwitchToNext();
        }
    }

    void OnTeamerViewSwitchToNext()
    {
        if (!m_bSwitchHide && MainPlayer.singleton != null && MainPlayer.singleton.alive)
        {
            return;
        }
        if (MainPlayer.singleton != null && WorldManager.singleton.isHideModeOpen)
        {
            m_listCamps = WorldManager.singleton.GetHideTeams(MainPlayer.singleton);
        }
        BasePlayer teamer = NextTeamPlayer(1);

        //if(teamer != null) Logger.Error("is alive: " + teamer.alive);

        if (teamer == null && !WorldManager.singleton.isViewBattleModel)
        {
            m_bIsForceChangeTeamCamp = true;
        }
        else if(teamer != null && teamer.alive == true)
        {
            FlushPlayerInfo(teamer);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, teamer.PlayerId);
        }
    }

    void OnPlayerActorPropUpdate(int id)
    {
        if (id > 0 && m_iSelectPlayerId == id)
        {
            FlushWeaponInfo(id);
        }
    }

    void OnPlayerLeave(BasePlayer player)
    {
        if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            return;

        if (player != null && player.PlayerId == m_iSelectPlayerId)
        {
            m_listCamps = WorldManager.singleton.GetCampTeams(player.Camp, GetPlayerType());
            OnTeamerViewSwitchToNext();
        }
    }

    void OnUpdateBattleState(SBattleState data)
    {
        if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            return;

        switch (data.state)
        {
            case GameConst.Fight_State_RoundStart:
                if (m_dieSwitchTimer != -1)
                {
                    TimerManager.RemoveTimeOut(m_dieSwitchTimer);
                    m_skipPressEvent = false;
                    m_dieSwitchTimer = -1;
                }
                break;
        }

        if (!WorldManager.singleton.isViewBattleModel)
        {
            if (WorldManager.singleton.isRescueModeOpen ||
            WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_BURST, GameConst.GAME_RULE_GHOST) ||
            WorldManager.singleton.isZombieTypeModeOpen)
            {
                switch (data.state)
                {
                    case GameConst.Fight_State_RoundStart:
                        OnTeamerViewSwitchToNext();
                        break;
                    case GameConst.Fight_State_RoundEnd:
                        ShowViewer(false);
                        break;
                }
            }
        }
    }

    void OnPlayerJoin(BasePlayer player)
    {
        if (player == null || player.serverData == null)
            return;

        if (FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
            return;

        if (m_bIsForceChangeTeamCamp)
        {
            m_bIsForceChangeTeamCamp = false;
            OnTeamerViewSwitchToNext();
            return;
        }

        if (!WorldManager.singleton.isViewBattleModel)
            return;

        BasePlayer curSelectPlayer = WorldManager.singleton.GetPlayerById(m_iSelectPlayerId);
        if (player == curSelectPlayer || (curSelectPlayer == null && ((!WorldManager.singleton.isSurvivalTypeModeOpen&&!WorldManager.singleton.isDotaModeOpen) || !player.isAI))) //第一个观战的一定不能是AI
        {
            FlushPlayerInfo(player);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, player.PlayerId);
            return;
        }

        if (player.Camp != m_selectCamp)
        {
            return;
        }

        if (curSelectPlayer == null || curSelectPlayer.serverData == null || curSelectPlayer.isDead)
        {
            FlushPlayerInfo(player);

            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, player.PlayerId);
        }
    }

    void EnableSwitch(bool isEnable)
    {
        m_btnLeft.enabled = isEnable;
        m_btnRight.enabled = isEnable;
        m_chLeft.enabled = isEnable;
        m_chRight.enabled = isEnable;
    }

    void SwitchFreeView()
    {
        if (FreeViewPlayer.singleton != null)
        {
            FreeViewPlayer.singleton.Run();
        }
        ShowViewer(false);
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_SWITCH_VIEW_BTN, false);
        GameDispatcher.Dispatch<bool>(GameEvent.UI_SHOW_GUNLIST, false);
    }

    void SwitchPersonView()
    {
        m_goContent.TrySetActive(true);
        BasePlayer curSelectPlayer = WorldManager.singleton.GetPlayerById(m_iSelectPlayerId);
        if (curSelectPlayer == null || !curSelectPlayer.isAlive) 
        {
            OnTeamerViewSwitchToNext();
        }
        else
        {
            FlushPlayerInfo(curSelectPlayer);
            GameDispatcher.Dispatch<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, curSelectPlayer.PlayerId);

        }
    }

    public static int watchedPlayerID
    {
        get { return m_iSelectPlayerId; }
    }

    public static long watchedPlayerPid
    {
        get { return m_watchedPlayerPid; }
    }

    public static GameObject RootTeamViewer
    {
        get { return m_rootTeamViewer; }
    }
}
