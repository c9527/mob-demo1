﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class UIGunListNew : MonoBehaviour
{
    Text _bullet_txt;
    Text _next_bullet_txt;
    //DynamicStack _item_list;
    GameObject goLeftBtn;
    GameObject goRightBtn;
    Image imgWeapon;
    List<int> m_curWeaponList;
    private static float SWITCH_GUN_INTERVAL = 0.5f;
    private float m_fLastSwitchTime = 0f;

    void Awake()
    {
        _bullet_txt = transform.Find("bullet_txt").GetComponent<Text>();
        _next_bullet_txt = transform.Find("next_bullet_txt").GetComponent<Text>();
        goLeftBtn = transform.Find("Left").gameObject;
        goRightBtn = transform.Find("Right").gameObject;
        imgWeapon = transform.Find("Icon").GetComponent<Image>();

        //_item_list = transform.Find("List").gameObject.AddComponent<DynamicStack>();
        //_item_list.SetFrameSize(364, 101);
        //_item_list.m_textureFrame = ResourceManager.LoadSprite(AtlasName.Battle, "weaponSprite");
        //_item_list.SetSelFrame = ResourceManager.LoadSprite(AtlasName.Battle, "weaponSpritehl");

        //_name_txt.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;
        _bullet_txt.gameObject.AddComponent<CanvasGroup>().blocksRaycasts = false;

        UGUIClickHandler.Get(goRightBtn).onPointerClick += delegate { OnChangeWeapon(true); };
        UGUIClickHandler.Get(goLeftBtn).onPointerClick += delegate { OnChangeWeapon(false); };
        //UGUIClickHandler.Get(gameObject).onPointerClick += delegate { OnChangeWeapon(true); };
    }

    private void OnChangeWeapon(bool isRight)
    {
        if (WorldManager.singleton.isViewBattleModel) return;
        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData.weaponList == null || MainPlayer.singleton.serverData.weaponList.Count == 0)
        {
            return;
        }
        if (Time.realtimeSinceStartup - m_fLastSwitchTime < SWITCH_GUN_INTERVAL)
        {
            return;
        }
        m_fLastSwitchTime = Time.realtimeSinceStartup;
        string strWeaponID = MainPlayer.singleton.GetNextWeaponID(true, isRight);
        if (string.IsNullOrEmpty(strWeaponID) == false)
            GameDispatcher.Dispatch(GameEvent.INPUT_SWITCH_WEAPON, strWeaponID);
    }
    //private void onUIGunListClick(GameObject target, PointerEventData eventData)
    //{
    //    if(_item_list.isStackShow==false)
    //    {
    //        _item_list.ShowStak(true);
    //    }
    //    else
    //    {
    //        _item_list.ShowStak(false);
    //    }
    //}

    void Start()
    {
        //GameDispatcher.AddEventListener<string, int>(GameEvent.UI_GUN_SELECTED, OnSelectedChangeGun);
        GameDispatcher.AddEventListener<int, int, int>(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, OnPlayerAmmUpdate);
        GameDispatcher.AddEventListener<int>(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, OnPlayerAmmSubUpdate);
        GameDispatcher.AddEventListener<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, ShowNextBullet);
        GameDispatcher.AddEventListener<string>(GameEvent.UI_UPDATE_GUNLIST, OnPlayerSwitchGun);
        GameDispatcher.AddEventListener<Sprite, string>(GameEvent.UI_UPDATE_GUNLIST2, OnOtherPlayerSwitchGun);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_EQUIP_INFO_UPDATE, OnPlayerGunListUpdate);
        OnPlayerGunListUpdate();
        if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.curWeapon != null)
        {
            var cur_weapon = MainPlayer.singleton.curWeapon;
            OnPlayerAmmUpdate(cur_weapon.ammoNumInClip, cur_weapon.ammoNumLeft, cur_weapon.advNum);
            if (MainPlayer.singleton.subWeapon != null)
            {
                var sub_weapon = MainPlayer.singleton.subWeapon;
                OnPlayerAmmSubUpdate(sub_weapon.subAmmoNum);
            }
        }
        else
        {
            OnPlayerAmmUpdate(0, 0, 0);
            OnPlayerAmmSubUpdate(-1);
        }
    }

    //public void setEnable(bool value)
    //{
    //    _item_list.enabled = value;
    //}

    //void OnSelectedChangeGun(string strUIEvent, int index)
    //{
    //    GameDispatcher.Dispatch(GameEvent.INPUT_SWITCH_WEAPON, m_curWeaponList[index].ToString());
    //}

    public void OnDestroy()
    {
        //GameDispatcher.RemoveEventListener<string, int>(GameEvent.UI_GUN_SELECTED, OnSelectedChangeGun);
        GameDispatcher.RemoveEventListener<int, int, int>(GameEvent.MAINPLAYER_AMMOINFO_UIUPDATE, OnPlayerAmmUpdate);
        GameDispatcher.RemoveEventListener<int>(GameEvent.MAINPLAYER_AMMOINFO_SUB_UIUPDATE, OnPlayerAmmSubUpdate);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.MAINPLAYER_SWITCH_SUB_WEAPON, ShowNextBullet);
        GameDispatcher.RemoveEventListener<string>(GameEvent.UI_UPDATE_GUNLIST, OnPlayerSwitchGun);
        GameDispatcher.RemoveEventListener<Sprite, string>(GameEvent.UI_UPDATE_GUNLIST2, OnOtherPlayerSwitchGun);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_EQUIP_INFO_UPDATE, OnPlayerGunListUpdate);
    }

    void OnPlayerAmmUpdate(int inClipNum, int leftNum, int advNum)
    {
        //Logger.Error("OnPlayerAmmUpdate, incli: "+inClipNum+", left: "+leftNum+", adv: "+advNum);
        if (inClipNum > -1)
        {
            _bullet_txt.enabled = true;
            _bullet_txt.text = string.Format("{0} / {1}", inClipNum, leftNum);
        }
        else
        {
            _bullet_txt.enabled = false;
        }
    }

    void OnPlayerAmmSubUpdate(int leftNum)
    {
        if (leftNum > -1)
        {
            _next_bullet_txt.enabled = true;
            _next_bullet_txt.text = leftNum.ToString();
        }
        else
        {
            _next_bullet_txt.enabled = false;
        }
    }

    void OnOtherPlayerSwitchGun(Sprite sp, string gunName)
    {
        //Logger.Error("gunName: " + gunName);
        //OnPlayerAmmUpdate(-1, 0, 0);
        _bullet_txt.enabled = true;
        _bullet_txt.text = gunName;
        OnPlayerAmmSubUpdate(-1);
        if (sp == null)
        {
            imgWeapon.enabled = false;
            return;
        }
        imgWeapon.enabled = true;
        imgWeapon.SetSprite(sp);
    }

    void OnPlayerSwitchGun(string strWeaponId)
    {
        if (m_curWeaponList == null || string.IsNullOrEmpty(strWeaponId))
        {
            //_name_txt.text = "";
            return;
        }
        var info = ItemDataManager.GetItem(int.Parse(strWeaponId));
        if (info == null || !m_curWeaponList.Contains(info.ID)) return;
        imgWeapon.SetSprite(ResourceManager.LoadWeaponIcon(info.ID));
        //_name_txt.text = info.DispName;
        //for (int index = 0; index < m_curWeaponList.Length; ++index)
        //{
        //    if (m_curWeaponList[index] == info.ID)
        //    {
        //        _item_list.selindex = index;
        //        break;
        //    }
        //}
    }

    void OnPlayerGunListUpdate()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.serverData == null || MainPlayer.singleton.serverData.weaponList == null || MainPlayer.singleton.serverData.weaponList.Count == 0)
        {
            return;
        }
        m_curWeaponList = MainPlayer.singleton.serverData.weaponList;

        //for (int i = 0; i < m_curWeaponList.Length; i++)
        //{
        //    string weaponID = m_curWeaponList[i].ToString();
        //    if (ItemDataManager.GetItem(m_curWeaponList[i]).SubType == GameConst.WEAPON_SUBTYPE_GRENADE)
        //    {
        //        LocalWeaponData lwd = MainPlayer.singleton.localPlayerData.GetLocalWeaponData(weaponID);

        //        if (lwd == null || lwd.curClipAmmoNum == 0)
        //        {
        //            //Logger.Error("remove: " + lwd + ", id: " + m_curWeaponList[i]);
        //            //if (lwd != null)
        //            //{
        //            //    Logger.Error("clip: " + lwd.curClipAmmoNum);
        //            //}
        //            m_curWeaponList = m_curWeaponList.ArrayDelete<int>(i);
        //            break;
        //        }
        //    }
        //}

        //Sprite[] gunSprites = new Sprite[m_curWeaponList.Length];
        //for (int index = 0; index < m_curWeaponList.Length; ++index)
        //{
        //    gunSprites[index] = ResourceManager.LoadWeaponIcon(m_curWeaponList[index]);
        //    if (gunSprites[index] == null)
        //    {
        //        gunSprites[index] = ResourceManager.LoadWeaponIcon(1001);
        //    }
        //}
        //_item_list.updateStackItemList = gunSprites;
        if (MainPlayer.singleton != null && MainPlayer.singleton.serverData != null)
        {
            OnPlayerSwitchGun(MainPlayer.singleton.serverData.propWeapon());
        }
    }

    void ShowNextBullet(bool isShow)
    {
        _next_bullet_txt.enabled = isShow;
    }
}
