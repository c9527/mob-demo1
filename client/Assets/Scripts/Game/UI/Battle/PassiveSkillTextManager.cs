﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 被动技能项
/// </summary>
class PassiveSkillTextData
{
    public GameObject gameObject;
    public int timerId;
}

/// <summary>
/// 被动技能管理器
/// 跟buff管理搭配使用
/// </summary>
class PassiveSkillTextManager
{
    static public PassiveSkillTextManager singleton;
    private Dictionary<string, List<GameObject>> m_dicItemCache = new Dictionary<string, List<GameObject>>();
    private List<GameObject> m_listAllItem = new List<GameObject>();
    private LinkedList<PassiveSkillTextData> m_kPassiveSkill = new LinkedList<PassiveSkillTextData>();

    private const float ITEM_GAP_SIZE = 30f;

    public PassiveSkillTextManager()
    {
        singleton = this;
    }

    public void AddPassiveSkill(TDBuffInfo buff, float buffTime)
    {
        GameObject item = GetItem(buff);
        item.TrySetActive(true);
        PassiveSkillTextData psd = new PassiveSkillTextData();
        psd.gameObject = item;
        LinkedListNode<PassiveSkillTextData> node = m_kPassiveSkill.AddFirst(psd);
        ResetList();
        psd.timerId = TimerManager.SetTimeOut(buffTime, () => { RemovePassiveSkill(node); });
    }

    public void RemovePassiveSkill(string name)
    {
        foreach (PassiveSkillTextData ps in m_kPassiveSkill)
        {
            if (ps.gameObject.name == name)
            {
                LinkedListNode<PassiveSkillTextData> node = m_kPassiveSkill.Find(ps);
                if (node.Value.timerId != -1)
                {
                    TimerManager.RemoveTimeOut(node.Value.timerId);
                    node.Value.timerId = -1;
                }

                RemovePassiveSkill(node);
                return;
            }
        }
    }

    public void RemoveAllPassiveSkill()
    {
        foreach (PassiveSkillTextData psd in m_kPassiveSkill)
        {
            DeactiveItem(psd.gameObject);
            TimerManager.RemoveTimeOut(psd.timerId);
        }
        m_kPassiveSkill.Clear();
    }

    private void RemovePassiveSkill(LinkedListNode<PassiveSkillTextData> node)
    {
        DeactiveItem(node.Value.gameObject);
        m_kPassiveSkill.Remove(node);
        ResetList();
    }

    private void ResetList()
    {
        int index = 0;
        foreach (PassiveSkillTextData psd in m_kPassiveSkill)
        {
            psd.gameObject.transform.localPosition = new Vector3(0, index++ * ITEM_GAP_SIZE, 0);
        }
    }

    private GameObject GetItem(TDBuffInfo buff)
    {
        List<GameObject> listItem = null;
        m_dicItemCache.TryGetValue(buff.ID.ToString(), out listItem);
        if (listItem == null)
        {
            listItem = new List<GameObject>();
            m_dicItemCache.Add(buff.ID.ToString(), listItem);
        }

        GameObject item;
        int iRearIndex = listItem.Count - 1;
        if (iRearIndex != -1)
        {
            item = listItem[iRearIndex];
            listItem.RemoveAt(iRearIndex);
        }
        else
        {
            item = new GameObject(buff.ID.ToString());
            item.transform.SetParent(UIManager.GetPanel<PanelBattle>().passiveSkillText.transform);
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            Sprite sprite = ResourceManager.LoadSprite(AtlasName.Battle, buff.BuffText);
            Image image = item.AddComponent<Image>();
            image.SetSprite(sprite);
            image.SetNativeSize();
            item.gameObject.TrySetActive(false);
            m_listAllItem.Add(item);
        }

        return item;
    }

    public void DeactiveItem(GameObject item)
    {
        if (item == null)
            return;

        item.gameObject.TrySetActive(false);

        List<GameObject> listItem = null;
        m_dicItemCache.TryGetValue(item.name, out listItem);
        if (listItem == null)
        {
            listItem = new List<GameObject>();
            m_dicItemCache.Add(item.name, listItem);
        }
        listItem.Add(item);
    }

    public void Release()
    {
        for (int index = 0; index < m_listAllItem.Count; ++index)
        {
            GameObject item = m_listAllItem[index];
            GameObject.Destroy(item);
        }
        foreach (PassiveSkillTextData psd in m_kPassiveSkill)
        {
            TimerManager.RemoveTimeOut(psd.timerId);
        }

        m_dicItemCache.Clear();
        m_listAllItem.Clear();
        m_kPassiveSkill.Clear();
    }
}
