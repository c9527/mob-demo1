﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class RescueItemRender : ItemRender
{
    Text _left_txt;
    Text _right_txt;

    public override void Awake()
    {
        _left_txt = transform.Find("leftname").GetComponent<Text>();
        _right_txt = transform.Find("rightname").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        if(data is toc_fight_actor_rescued)
        {
            toc_fight_actor_rescued sdata = data as toc_fight_actor_rescued;
            BasePlayer from = WorldManager.singleton.GetPlayerById(sdata.rescuer);
            BasePlayer to = WorldManager.singleton.GetPlayerById(sdata.actor);
            if (from == null || to == null) return;
            _left_txt.text = from.PlayerName;
            _right_txt.text = to.PlayerName;
        }
    }
}
