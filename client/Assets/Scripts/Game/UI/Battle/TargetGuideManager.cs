﻿using UnityEngine;
using UnityEngine.UI;

class TargetGuideManager : MonoBehaviour
{
    public static TargetGuideManager singleton;

    private GameObject m_go;
    private Transform m_tran;
    private GameObject m_goGuide;
    private Transform m_tranGuide;

    //private GameObject m_goTarget;
    private Vector3 m_directionPoint;

    private Text m_distanceText;

    private bool m_enable;
    private bool m_released;

    private const int INTERVAL_TICK = 3;
    private int m_tick;

    private float m_maxDistance = 100f;

    private Vector3 m_guideOrginEulerAngle;

    private bool m_bIsPanelBattleInited;

    private SBattleState m_bs;
    private BasePlayer m_player;

    #region 引擎方法
    private void Awake()
    {
        singleton = this;
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchPlayer);

        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
            return;
        }
    }

    void Update()
    {
        if (m_enable == false)
            return;

        if (++m_tick == INTERVAL_TICK)
        {
            m_tick = 0;

            //处理
            if (m_goGuide == null || m_player == null || m_player.transform == null)
                return;

            float distance = Vector3.Distance(m_player.position, m_directionPoint);

            if (distance < m_maxDistance)
                Show(false);
            else
            {
                int showDistince = (int)distance;

                Vector3 dir = Vector3.Normalize(m_directionPoint - m_player.position);

                float angle = Vector3.Angle(dir, m_player.transform.forward); //夹角

                float dot = Vector3.Dot(m_player.transform.right, dir);

                if (dot > 0)
                    angle *= -1f;

                m_tranGuide.localEulerAngles = m_guideOrginEulerAngle +  new Vector3(0, 0, angle);

                m_distanceText.text = string.Format("{0}m", showDistince >= 0 ? showDistince : 0, angle);
                Show(true);
            }
        }
    }
    
    void OnDestory()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);

        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchPlayer);

        if (m_go != null)
        {
            GameObject.Destroy(m_go);
            m_go = null;
        }

        m_released = true;

        singleton = null;
    }

    #endregion


    #region 私有方法
    private void Init()
    {
        m_tran = UIManager.GetPanel<PanelBattle>().targetGuide;

        if (m_tran == null)
            return;

        m_go = m_tran.gameObject;

        m_distanceText = m_tran.Find("Distance/DistanceText").GetComponent<Text>();
        m_tranGuide = m_tran.Find("Guide");
        m_goGuide = m_tranGuide.gameObject;
        m_guideOrginEulerAngle = m_tranGuide.localEulerAngles;

        if (WorldManager.singleton.isViewBattleModel)
        {
            SwitchPlayer(ChangeTeamView.watchedPlayerID);
        }
        else
        {
            BindPlayer(MainPlayer.singleton);
        }

        Play(false);

        if (m_bs != null)
            OnUpdateBattleState(m_bs);
    }

    private void OnCreateBattlePanel()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        m_bIsPanelBattleInited = true;
        Init();
    }

    private void OnUpdateBattleState(SBattleState data)
    {
        if (m_released || data == null)
            return;

        if (data.state != GameConst.Fight_State_RoundStart)
            return;

        m_bs = data;

        if (SceneManager.singleton == null)
            return;

        //判断下有没有点

        GameObject goDirection = SceneManager.singleton.GetDireciton(m_bs.round);

        if (goDirection == null)
        {
            Play(false);
            return;
        }

        m_maxDistance = goDirection.transform.localEulerAngles.y;

        m_directionPoint = goDirection.transform.position;

        Play(true);
    }

    private void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == m_player)
        {
            Play(false);
        }
    }

    private void OnPlayerRevive(string eventName, BasePlayer who)
    {
        if (who == m_player && m_bs != null)
        {
            OnUpdateBattleState(m_bs);
        }
    }

    private void SwitchPlayer(int partnerID)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(partnerID);
        if (player != null)
        {
            BindPlayer(player);
        }
    }

    private void OnSceneLoaded()
    {
        if (m_released)
            return;

        if (m_bIsPanelBattleInited && m_go == null)
            Init();
    }

    private void Play(bool isPlay)
    {
        m_enable = isPlay;
        Show(false);
    }

    private void Show(bool isShow)
    {
        if (m_go != null)
            m_go.TrySetActive(isShow);
    }
    #endregion


    #region 公有方法
    public void BindPlayer(BasePlayer player)
    {
        m_player = player;
    }
    #endregion
}