﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettleWeaponItemRender : ItemRender
{
    const float EXP_FILL_TIME = .5f;
    const uint MAX_STATE = 3;
    const uint MAX_LEVEL = 9;
    int[] _data;

    private p_item _item;
    Image _item_img;
    Text _name_txt;
    Image[] _state_list;//state_ico
    Text _exp_txt;
    Text m_tipText;
    Image _exp_new;
    Image _exp_old;
    Text _lvl_txt;
    private Text m_txtCost;
    private Button m_btnFix;
    private Image m_imgDurability;
    private Image m_imgBroken;

    float _exp_fill_start_time = float.MaxValue;
    float _exp_fill_new_value;
    private TDStrengthenLevel upgrade_info;
    private UIEffect m_effect;

    public override void Awake()
    {
        var tran = transform;
        _item_img = tran.Find("item_img").GetComponent<Image>();
        _name_txt = tran.Find("name_txt").GetComponent<Text>();
        _exp_txt = tran.Find("exp_txt").GetComponent<Text>();
        _exp_new = tran.Find("exp_new").GetComponent<Image>();
        _exp_old = tran.Find("exp_old").GetComponent<Image>();
        _lvl_txt = tran.Find("lvl_txt").GetComponent<Text>();
        m_txtCost = tran.Find("txtCost").GetComponent<Text>();
        m_tipText = tran.Find("tips_txt").GetComponent<Text>();
        m_btnFix = tran.Find("btnFix").GetComponent<Button>();
        m_imgDurability = tran.Find("imgDurability/value").GetComponent<Image>();
        m_imgBroken = transform.Find("imgDurability/imgBroken").GetComponent<Image>();
        UGUIClickHandler.Get(m_btnFix.gameObject).onPointerClick += OnBtnFixClick;
        _state_list = new Image[3];
        for (int i = 0; i < _state_list.Length; i++)
        {
            _state_list[i] = transform.Find("state_ico_" + i).GetComponent<Image>();
        }
        
    }

    private void OnBtnFixClick(GameObject target, PointerEventData eventData)
    {
        if (_item == null || _item.durability == ItemDataManager.MAX_DURABILITY)
            return;
        NetLayer.Send(new tos_player_repair_weapons() { weapon_uids = new[] { _item.uid} });
    }

    protected override void OnSetData(object data)
    {
        _data = data as int[];
        if (_data.Length > 2 && _data[2] != 0)
        {
			_item = ItemDataManager.GetDepotItemByUid(_data[2]);
            
        }
        else
        {
            
            _item = ItemDataManager.GetDepotItem(_data[0]);
        }
        var itemId = _item.item_id;
        var info = ItemDataManager.GetItem(itemId) as ConfigItemWeaponLine;
        
        _item_img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
        _item_img.SetNativeSize();
        _name_txt.text = info.DispName;
        m_txtCost.text = _item != null ? ((ItemDataManager.MAX_DURABILITY - _item.durability) * info.RepairPrice).ToString() : "0";
        var sdata = StrengthenModel.Instance.getSStrengthenDataByCode(info.ID);
        upgrade_info = ConfigManager.GetConfig<ConfigWeaponStrengthenLevel>().getStrengthenLevelInfo(sdata.level + 1, sdata.state, info.StrengthenGroup);
        _exp_fill_new_value = upgrade_info!=null && upgrade_info.NeedExp>0 ? (float)sdata.cur_exp / upgrade_info.NeedExp : 1f;
        if (sdata.cur_exp <= _data[1])
        {
            _exp_old.fillAmount = 0;
        }
        else
        {
            _exp_old.fillAmount = upgrade_info != null && upgrade_info.NeedExp > 0 ? (float)(sdata.cur_exp - _data[1]) / upgrade_info.NeedExp : 1f;
        }
        m_tipText.text = "";
        if (sdata.cur_exp <= 0)
        {
            _exp_txt.text =  "+" + 0;
        }
        else
        {
            _exp_txt.text = "+" + _data[1];
        }

        _exp_new.fillAmount = _exp_old.fillAmount;
        if (m_effect != null)
        {
            m_effect.Hide();
        }
        if (upgrade_info != null && upgrade_info.NeedExp == 0)
        {
            if (m_effect == null)
            {
                m_effect = UIEffect.ShowEffect(m_tipText.transform, EffectConst.UI_JIESUAN_SHANGUANG, new Vector2(-3f, 1f));
            }
            else
            {
                m_effect.Play();
            }
            m_tipText.text = "可进阶";
            _exp_new.fillAmount = 1.0f;
        }
        if (sdata != null && sdata.state == MAX_STATE && sdata.level == MAX_LEVEL)//如果进阶数据配置表改最高星个数、等级这里要修改
        {
            m_tipText.text = "已满级";
            _exp_new.fillAmount = 1.0f;
            if (m_effect != null)
            {
                m_effect.Hide();
            }
        }

        m_imgDurability.fillAmount = _item != null ? 1f * _item.durability / ItemDataManager.MAX_DURABILITY : 0;
        m_imgBroken.gameObject.TrySetActive(_item != null && _item.durability == 0);

        _lvl_txt.text = String.Format("{0}级", sdata.level);
        for (int i = 0; i < _state_list.Length; i++)
        {
            _state_list[i].color = i < sdata.state ? Color.white : Color.black;
            _state_list[i].gameObject.TrySetActive(PanelStrengthenMain.IsStrengthenOpen);
        }
    }

    void Update()
    {
        if (upgrade_info == null) return;
        if (upgrade_info != null && upgrade_info.NeedExp == 0)
        {
            //可以进阶了就不要进度条缓动了
            return;
        }
       
        if (_exp_fill_start_time > Time.realtimeSinceStartup)
            _exp_fill_start_time = Time.realtimeSinceStartup;
        float dt = Time.realtimeSinceStartup - _exp_fill_start_time;
        if (dt >= EXP_FILL_TIME)
        {
            _exp_new.fillAmount = _exp_fill_new_value;
        }
        else if (dt <= 0)
        {
            _exp_new.fillAmount = _exp_old.fillAmount;
        }
        else
        {
            _exp_new.fillAmount = _exp_old.fillAmount + (_exp_fill_new_value - _exp_old.fillAmount) * dt / EXP_FILL_TIME;
        }

        if (dt >= EXP_FILL_TIME)
        {
            PanelSettle.PopWeaponLevelup();
        }
    }
}
