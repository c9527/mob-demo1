﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class BattleNoticePanel : MonoBehaviour
{
    private static Queue<QueueItem> m_panelQueue = new Queue<QueueItem>();

    public bool useQueue = false;//默认不加入队列

    private GameObject goBgImg;
    private GameObject goShowImg;
    private GameObject goShowHeadIconImg;
    private GameObject goLabelTxt;
    private GameObject goRoleNameTxt;

    private List<GameObject> m_goSymbolIcons;
    private List<UIImgNumText> m_numIcons;

    public bool isPlaying = false;

    private float duration;
    private bool voice;
    private float curTime;
    private bool isShowNum;     //是是否显示数字（只读）

    RectTransformProp bgProp = null;
    RectTransformProp imgProp = null;
    RectTransformProp headProp = null;

    public void play(string imgAtlas, string img, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null)
    {
        Sprite sprite = ResourceManager.LoadSprite(imgAtlas, img);
        play(sprite, duration, voice, pos, prop);
    }

    public void play(Sprite sprite, float duration = 3, bool voice = false, Vector2 pos = default(Vector2), NoticePanelProp prop = null,string headIcon = "",string roleName = "")
    {
        if(useQueue && isPlaying)
        {
            QueueItem item = new QueueItem() { sprite = sprite, duration = duration, voice = voice, pos = pos, prop = prop };
            m_panelQueue.Enqueue(item);
            return;
        }

        stop();

        isPlaying = true;

        gameObject.TrySetActive(true);
        this.duration = duration;
        this.voice = voice;

        Vector2 bgSize = sprite != null ? new Vector2(sprite.rect.width + 100, sprite.rect.height + 30) : Vector2.zero;
        Vector2 imgPos = pos;
        Sprite bg = ResourceManager.LoadSprite(AtlasName.Battle, "blue_bg");
        Sprite icon = ResourceManager.LoadIcon(headIcon);

        string msg = "";
        Vector2 txtPos = Vector2.zero;
        TextProp textProp = null;
        Vector2 imgScale = Vector2.one;
        Image.Type bgType = Image.Type.Simple;
        Image.Type imgType = Image.Type.Simple;
        List<SpriteProp> symbolIconList = new List<SpriteProp>();
        List<ImgNumProp> numIconList = new List<ImgNumProp>();
        if (prop != null)
        {
            bg = !prop.bgAltas.IsNullOrEmpty() && !prop.bgName.IsNullOrEmpty() ? ResourceManager.LoadSprite(prop.bgAltas, prop.bgName) : null;
            if (prop.bgSize != Vector2.zero)
            {
                bgSize = prop.bgSize;
            }
            if (prop.imgPos != Vector2.zero) imgPos = prop.imgPos;
            imgScale = prop.imgScale;
            imgType = prop.imgType;
            bgType = prop.bgType;
            symbolIconList = prop.symbolIconList;
            numIconList = prop.numIconList;
            textProp = prop.textProp;
            txtPos = prop.txtPos;
            msg = prop.msg;
        }

        if(bgProp==null) bgProp = new RectTransformProp();
        bgProp.reset();
        bgProp.size = bgSize;
        if (imgProp == null) imgProp = new RectTransformProp();
        imgProp.reset();
        imgProp.scale = imgScale;

        if (headProp == null) headProp = new RectTransformProp();
        headProp.reset();
        headProp.scale = imgScale;
        headProp.max = UIHelper.ZERO_X_ONE_Y_VEC;
        headProp.min = UIHelper.ZERO_X_ONE_Y_VEC; ;

        UIHelper.ShowSprite(ref goBgImg, "Bg", bg, pos, true, bgProp, transform, true, bgType, Image.FillMethod.Horizontal);
        UIHelper.ShowSprite(ref goShowImg, "Img", sprite, imgPos, true, imgProp, transform, true, imgType);
        UIHelper.ShowSprite(ref goShowHeadIconImg, "HeadIcon", icon, new Vector2(68, 50), true, headProp, goBgImg.transform, true, imgType);
        UIHelper.ShowText(ref goLabelTxt, msg, true, txtPos, textProp, transform, true);
        UIHelper.ShowText(ref goRoleNameTxt, roleName, true, new Vector2(90, -38), null, goShowHeadIconImg.transform, true);

        ShowSymbolIconList(symbolIconList);
        ShowNumIconList(numIconList);
    }

    public void stop()
    {
        isPlaying = false;
        curTime = 0f;
    }

    void Update()
    {
        if (isPlaying)
            playProgress();
        else
        {
            if(!useQueue)
            {
                if (gameObject != null) gameObject.TrySetActive(false);
                return;
            }
            if(m_panelQueue.Count > 0)
            {
                QueueItem item = m_panelQueue.Dequeue();
                play(item.sprite, item.duration, item.voice, item.pos, item.prop);
            }
            else
            {
                if (gameObject != null) gameObject.TrySetActive(false);
            }
        }
    }

    private void playProgress()
    {
        curTime += Time.deltaTime;
        if (curTime > duration)
        {
            stop();
            if (voice && isShowNum)
                AudioManager.PlayFightUISound(AudioConst.voiceStart);
            return;
        }
        PlayNumIcons(duration - curTime);
    }

    private void PlayNumIcons(float leftTime)
    {
        if (m_numIcons == null) return;
        int len = m_numIcons.Count;
        for (int i = 0; i < len; i++)
        {
            var time = Mathf.CeilToInt(leftTime);
            if (m_numIcons[i].isCountDown && m_numIcons[i].value != time)
            {
                m_numIcons[i].value = time;
                if (voice && isShowNum && time < 10)
                    AudioManager.PlayFightUISound("voice_" + time);
            }
        }
    }

    private void ShowNumIconList(List<ImgNumProp> list)
    {
        if (list == null || list.Count <= 0)
        {
            isShowNum = false;
            HideNumIconList();
            return;
        }
        isShowNum = true;
        if (m_numIcons == null) m_numIcons = new List<UIImgNumText>();
        int length = list.Count;
        for (int i = m_numIcons.Count; i < length; i++)
        {
            m_numIcons.Add(new UIImgNumText(transform));
        }

        int len = m_numIcons.Count;
        for (int i = 0; i < length; i++)
        {
            ShowNumIcon(i, true, list[i]);
        }
        for (int i = length; i < len; i++)
        {
            ShowNumIcon(i, false);
        }
    }

    private void ShowNumIcon(int index, bool isShow, ImgNumProp prop = null)
    {
        UIImgNumText item = m_numIcons[index];
        item.Show(isShow);
        if (isShow && prop != null)
        {
            item.setStyle(prop.preName, prop.atlas, prop.align, prop.gap, prop.pos, prop.fixBit);
            bool hasValue = prop.value > -1;
            item.isCountDown = !hasValue;
            if (hasValue) item.value = prop.value;
        }
    }

    private void HideNumIconList()
    {
        if (m_numIcons == null) return;
        int length = m_numIcons.Count;
        for (int i = 0; i < length; i++)
        {
            m_numIcons[i].Show(false);
        }
    }

    private void ShowSymbolIconList(List<SpriteProp> list)
    {
        if (list == null || list.Count <= 0)
        {
            HideSymbolIconList();
            return;
        }
        if (m_goSymbolIcons == null) m_goSymbolIcons = new List<GameObject>();
        int length = list.Count;
        for (int i = m_goSymbolIcons.Count; i < length; i++)
        {
            m_goSymbolIcons.Add(new GameObject());
        }

        int len = m_goSymbolIcons.Count;
        for (int i = 0; i < length; i++)
        {
            ShowSymbolIcon(i, true, list[i]);
        }
        for (int i = length; i < len; i++)
        {
            ShowSymbolIcon(i, false);
        }
    }

    private void ShowSymbolIcon(int index, bool isShow, SpriteProp prop = null)
    {
        if (m_goSymbolIcons == null || m_goSymbolIcons.Count <= index) return;
        Vector2 pos = Vector2.zero;
        Sprite sprite = null;
        if(prop != null)
        {
            pos = prop.pos;
            sprite = ResourceManager.LoadSprite(prop.atlas, prop.sprite);
        }
        GameObject go = m_goSymbolIcons[index];
        UIHelper.ShowSprite(ref go, "Symbol"+index, sprite, pos, isShow, null, transform, true);
    }

    private void HideSymbolIconList()
    {
        if (m_goSymbolIcons == null) return;
        int length = m_goSymbolIcons.Count;
        for (int i = 0; i < length; i++)
        {
            ShowSymbolIcon(i, false);
        }
    }

    #region 队列相关
    class QueueItem
    {
        public Sprite sprite;
        public float duration = 3;
        public bool voice = false;
        public Vector2 pos = default(Vector2);
        public NoticePanelProp prop = null;
    }
    #endregion
}

public class NoticePanelProp
{
    public string bgAltas = AtlasName.Battle;
    public string bgName = "blue_bg";
    public Vector2 bgSize = Vector2.zero;
    public Vector2 imgPos = Vector2.zero;
    public Vector2 imgScale = Vector2.one;
    public Image.Type bgType = Image.Type.Filled;
    public Image.Type imgType = Image.Type.Simple;
    public string msg = "";
    public Vector2 txtPos = Vector2.zero;
    public TextProp textProp = null;
    public List<SpriteProp> symbolIconList = new List<SpriteProp>();
    public List<ImgNumProp> numIconList = new List<ImgNumProp>();
}

public class SpriteProp
{
    public string atlas="";
    public string sprite="";
    public Vector2 pos = Vector2.zero;
}

public class ImgNumProp
{
    public string preName = "";
    public string atlas = "";
    public int align = 0;
    public int gap = 0;
    public int fixBit = 0;
    public int value = -1;
    public Vector2 pos = Vector2.zero;
}
