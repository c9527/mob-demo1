﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class BattleAddInfoRender : ItemRender
{
    private int _level;

    Image _avatar_img;
    Text _lv_txt;
    Image _value_img;
    Text _desc_txt;

    public override void Awake()
    {
        _avatar_img = transform.Find("avatar_img").GetComponent<Image>();
        _lv_txt = transform.Find("lv_txt").GetComponent<Text>();
        _value_img = transform.Find("value_img").GetComponent<Image>();
        _desc_txt = transform.Find("desc_txt").GetComponent<Text>();

        OnSetData(null);
    }

    protected override void OnSetData(object data)
    {
        var sdata = data as PlayerServerData;
        if (sdata != null)
        {
            if (sdata.fight_level > _level)
            {
                TipsManager.Instance.showTips(sdata.camp==(int)WorldManager.CAMP_DEFINE.ZOMBIE ? "僵尸等级提升，血量增加 ":"人类等级提升，伤害增加");
            }
            _level = sdata.fight_level;
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, sdata.camp == 3 ? "camp_zombie" : "camp_human"));
            _lv_txt.text = String.Format("{0}级", sdata.fight_level);
            _value_img.fillAmount = (sdata.camp == 3 ? 0.2f : 0.1f) * sdata.fight_level;
            var config = ConfigManager.GetConfig<ConfigBuff>();
            var add_life = 0;
            var add_damage_rate = 0f;
            for (int i = 0;sdata.buff_list!=null && i < sdata.buff_list.Length; i++)
            {
                var info = config.GetLine(sdata.buff_list[i].buff_id);
                if (info != null)
                {
                    add_life += info.MaxHP;
                    add_damage_rate += info.DamageRate;
                }
            }
            if (sdata.camp == 3)
            {
                _desc_txt.text = string.Format("生命+{0}", add_life);
            }
            else
            {
                _desc_txt.text = string.Format("伤害+{0}%", add_damage_rate*100);
            }
        }
        else
        {
            _level = 0;
            _avatar_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "camp_human"));
            _lv_txt.text = String.Format("{0}级", 0);
            _value_img.fillAmount = 0;
            _desc_txt.text = "";
        }
    }
}
