﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class BattleTipsHandler : MonoBehaviour
{
    //击杀提示特效
    private Vector2 m_uiPos;
    private string[] m_killEffectNames;

    private ProgressBarProp prop;

    private int m_timerId;
    
    void Awake()
    {
        m_uiPos = new Vector2(0, -(int)(GameSetting.AIM_NAME_HEGIHT_RATE * 2f * Screen.height));
        m_killEffectNames = new string[] { EffectConst.UI_KILL_NORMAL_1, EffectConst.UI_KILL_NORMAL_2, EffectConst.UI_KILL_NORMAL_3, EffectConst.UI_KILL_NORMAL_4, EffectConst.UI_KILL_NORMAL_5, EffectConst.UI_KILL_NORMAL_6, EffectConst.UI_KILL_NORMAL_GOD_LIKE};
        InitEvent();
    }

    void InitEvent()
    {
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
    }

    private int m_showTime;

    public void OnPlayerDie(BasePlayer killer, BasePlayer dier, toc_fight_actor_die proto)
    {
        int iLastHitPart = proto.part;
        int weapon = proto.weapon;
        onReviveEffectHandler(killer, dier, proto.revive_time);
        if (RoomModel.IsPvp() == true  )
        {
            if (MainPlayer.singleton != null)
            {
                //if (killer is MainPlayer)
                // {
                //   Logger.Error("iLastHitPart = " + iLastHitPart);
                setBeAttckDieStatus(killer, dier, iLastHitPart);
                // }

                if (dier is MainPlayer)//显示击杀统计数据
                {
                    showHarmPanel(killer, dier, weapon);
                }
            }
            else
            {
                PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
                if (battlePanel != null && battlePanel.watchType == GameConst.WATCH_TYPE_FIRST_PERSON_VIEW && FPWatchPlayer.singleton != null && 
                        /*FPWatchPlayer.singleton.isWatching == true &&*/ WorldManager.singleton.curPlayer.PlayerId == dier.PlayerId)
                {
                    showHarmPanel(killer, dier, weapon);
                }
            }
        }
    }

    private void showHarmPanel(BasePlayer killer, BasePlayer dier,int weapon)
    {
        if (WorldManager.singleton.isHideModeOpen == true)
        {
            return;
        }
        UIManager.GetPanel<PanelBattle>().BeKillInfo(killer != null ? killer.PlayerName : "", weapon);
        UIManager.GetPanel<PanelBattle>().ShowHarmInfoListTip(true);
        if (m_showTime != -1)
        {
            TimerManager.RemoveTimeOut(m_showTime);
        }
        m_showTime = TimerManager.SetTimeOut(3f, delegate()
        {
            if (UIManager.GetPanel<PanelBattle>() != null)
            {
                UIManager.GetPanel<PanelBattle>().ShowHarmInfoListTip(false);
            }

        });
        showHarmData();
    }

    private void setBeAttckDieStatus(BasePlayer killer, BasePlayer dier, int part)
    {
        List<HarmInfo> m_BeAttckHarmList = MainPlayer.singleton.m_BeAttckHarmList;
        for (int i = 0; i < m_BeAttckHarmList.Count; i++)
        {
            if (m_BeAttckHarmList[i].beActorId == dier.PlayerId)
            {
                m_BeAttckHarmList[i].die = true;
                if( killer is MainPlayer )
                {
                    if (m_BeAttckHarmList[i].part == -2)
                    {
                        m_BeAttckHarmList[i].part = part;
                    }
                    if (m_BeAttckHarmList[i].killByMyself == -1)
                    {
                        m_BeAttckHarmList[i].killByMyself = 1;
                    }
                    
                }
                if( killer is OtherPlayer && dier is OtherPlayer )
                {
                    m_BeAttckHarmList[i].killByMyself = 0;
                }
            }
        }
    }

    private void showHarmData()
    {
        List<HarmInfo> m_BeAttckHarmList = new List<HarmInfo>();
        if( MainPlayer.singleton != null )
        {
            m_BeAttckHarmList = MainPlayer.singleton.m_BeAttckHarmList;
        }
        else
        {
            m_BeAttckHarmList = UIManager.GetPanel<PanelBattle>().m_watchViewHarmList;
        }
        string nameStr = "";
        string harmStr = "";
        int len = m_BeAttckHarmList.Count;
        int index = 0;
        int killByMyself = -1;
        int tempHarm = 0;
        for (int k = 6; k >= 0; k--)
        {
            if (k < len &&  m_BeAttckHarmList[k] != null)
            {
                killByMyself = m_BeAttckHarmList[k].killByMyself;
                if (m_BeAttckHarmList[k].pid <= 0)
                {
                    tempHarm = m_BeAttckHarmList[k].harm > 100 ? m_BeAttckHarmList[k].harm - 100 : m_BeAttckHarmList[k].harm;
                }else
                {
                    tempHarm= m_BeAttckHarmList[k].harm ;
                }
                if (m_BeAttckHarmList[k].die == true && killByMyself == 1)
                {
                    nameStr = "<color=#f19900>" + m_BeAttckHarmList[k].beActorName + "</color>\n";
                    harmStr = "<color=#f19900>" + tempHarm + "</color>" + "\n";
                }
                else
                {
                    nameStr = "<color=#b1b1b1>" + m_BeAttckHarmList[k].beActorName + "</color>\n";
                    harmStr = "<color=#b1b1b1>" + tempHarm + "</color>\n";
                }
                UIManager.GetPanel<PanelBattle>().SetHarmListInfo(index, nameStr, harmStr, m_BeAttckHarmList[k].part, killByMyself);
                index++;
            }
        }
        m_BeAttckHarmList.Clear();
    }

    private void onReviveEffectHandler(BasePlayer killer, BasePlayer dier, float reviveTime)
    {
        if (dier != MainPlayer.singleton)
        {
            return;
        }
        ConfigGameRuleLine rule_info = ConfigManager.GetConfig<ConfigGameRule>().GetLine(WorldManager.singleton.gameRule);
        if (
               ((WorldManager.singleton.isZombieTypeModeOpen) && killer != null && killer.curWeapon != null && killer.curWeapon.weaponConfigLine.Class == (int)WeaponManager.WEAPON_TYPE_KNIFE)
            || ((WorldManager.singleton.isZombieHeroModeOpen || WorldManager.singleton.isZombieUltimateModeOpen) && (MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_HERO || killer != null && killer.serverData.actor_type == GameConst.ACTOR_TYPE_HERO))
            )
        {
            return;
        }

        if (reviveTime > 0)
        {
            Action callBack = () =>
            {
                m_timerId = -1;
                if (UIManager.IsOpen<PanelBattle>())
                {
                    if (prop == null)
                    {
                        Vector2 pos = new Vector2(0, -100);
                        prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, OnTimeOut = onTimeOut, textProp = new TextProp() { fontSize = 18, rect = new RectTransformProp() { size = new Vector2(200, 30) } } };
                    }
                    UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "复活中", reviveTime, prop, "Revive");
                }
            };

            if (rule_info.RescueWaitTime > 0)
                m_timerId = TimerManager.SetTimeOut(rule_info.RescueWaitTime, callBack);
            else
                callBack();
        }
    }

    void onTimeOut()
    {
        //MainPlayer.SendMsg("revive");
    }

    void OnPlayerRevive(string eventName, BasePlayer who)
    {
        if (who == MainPlayer.singleton)
        {
            if (m_timerId != -1)
            {
                TimerManager.RemoveTimeOut(m_timerId);
            }

            if (UIManager.IsOpen<PanelBattle>())
            {
                UIManager.GetPanel<PanelBattle>().StopProgressBar("Revive");
            }
        }
    }
}
