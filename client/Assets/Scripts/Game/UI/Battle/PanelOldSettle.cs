using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;



public class PanelOldSettle : BasePanel
{
    const float EXP_FILL_TIME = .5f;

    public static int win_camp;
    public static int avg_honor_1;
    public static int avg_honor_2;
    public static string scene_type;
    public static string scene_subtype;
    public static bool abnormal_finish;
    public static float game_time;
    public static SDSettlePlayer[] settle_data;
    public static int game_round;
    public static string gameRule;
    public static int self_rank;
    static toc_player_game_reward reward_data;
    internal static toc_fight_game_result game_result;
    public static SDSettlePlayer self_share;

    static bool data_updated;
    static Queue<object[]> weapon_lvup_queue = new Queue<object[]>();
    public static bool m_bMandatoryQuit = false;

    float _unlock_time;
    protected SDSettlePlayer self_data;
    Image _result_img;
    Image _camp1_img;
    Image _camp2_img;
    Image _exp_new;
    Image _exp_old;
    Text _lvl_txt;
    Text _exp_txt;
    Text _gold_txt;
    Text _honour_txt;
    Text _rule_txt;
    protected Image _camp0_img;
    Text _camp1_result_txt;
    Text _camp2_result_txt;
    Button _double_btn;
    Button _share_btn;
    UIEffect _double_effect;
    UIEffect _gongxi_effect;

    GameObject _weapon_canvas;
    DataGrid _weapon_list;
    private Text m_txtFixAllCost;
    private Button m_btnFixAll;

    GameObject _player_canvas;
    Text _score_lbl;
    Text m_addictionInfo;

    protected int MaxPlayerCount = 10;
    protected SettlePlayerItemRender[] _player_list;

    GameObject _popup_canvas;
    CanvasGroup _reward_group;
    Transform[] _reward_list;
    DataGrid m_playersettle;
    DataGrid m_camps;
    GridLayoutGroup m_gout;

    float _exp_fill_start_time = float.MaxValue;
    float _exp_fill_new_value;
    /// <summary>
    /// 名师段位获得积分提示
    /// </summary>
    static int masterGradePoint = -1;

    public static float m_panelStartTime = float.MaxValue;
    public static bool isMVPOrACE = false;

    public PanelOldSettle()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelSettle");
        AddPreLoadRes("UI/Battle/PanelSettle", EResType.UI);
        AddPreLoadRes("UI/Battle/SettleWeaponItemRender", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Settle", EResType.Atlas, ResTag.Forever);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        _result_img = m_tran.Find("result_img").GetComponent<Image>();
        _exp_new = m_tran.Find("exp_new").GetComponent<Image>();
        _exp_old = m_tran.Find("exp_old").GetComponent<Image>();
        _lvl_txt = m_tran.Find("lvl_txt").GetComponent<Text>();
        _exp_txt = m_tran.Find("exp_txt").GetComponent<Text>();
        _gold_txt = m_tran.Find("gold_txt").GetComponent<Text>();
        _honour_txt = m_tran.Find("honour_txt").GetComponent<Text>();
        _double_btn = m_tran.Find("double_btn").GetComponent<Button>();
        if(Driver.m_platform==EnumPlatform.ios || Driver.m_platform==EnumPlatform.android)
        {
            _share_btn = m_tran.Find("player_canvas/share_btn").GetComponent<Button>();
        }        
        _rule_txt = m_tran.Find("rule_txt").GetComponent<Text>();
        var ruleCfg = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.LastRoomInfo.rule);
        if (ruleCfg != null)
        {
            _rule_txt.text = ruleCfg.Mode_Name;
        }
        _weapon_canvas = m_tran.Find("weapon_canvas").gameObject;
        _weapon_list = _weapon_canvas.transform.Find("weapon_list/content").gameObject.AddComponent<DataGrid>();
        _weapon_list.SetItemRender(ResourceManager.LoadUIRef("UI/Battle/SettleWeaponItemRender"), typeof(SettleWeaponItemRender));
        _weapon_list.autoSelectFirst = false;

        Transform tran;
        if ((tran = m_tran.Find("weapon_canvas/Image/txtCost")) != null)
            m_txtFixAllCost = tran.GetComponent<Text>();
        if ((tran = m_tran.Find("weapon_canvas/Image/btnFixAll")) != null)
            m_btnFixAll = tran.GetComponent<Button>();

        SettlePlayerItemRender.MaxAdd = ConfigManager.GetConfig<ConfigMisc>().GetLine("win_game_honor2add_min").ValueInt;
        SettlePlayerItemRender.MaxSub = ConfigManager.GetConfig<ConfigMisc>().GetLine("lost_game_honor2del_max").ValueInt;
        if (RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_ERADICATE
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_ERADICATE2 
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_CHALLENGE
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_SURVIVAL 
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_DEFEND
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_DEFEND0
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_DEFEND2
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_SUR_SNIPE
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_WORLD_BOSS
            || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_BOSS_NEWER
            )
        {
            if (RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_WORLD_BOSS || RoomModel.LastRoomInfo.rule == GameConst.GAME_RULE_BOSS_NEWER)
            {
                _player_canvas = m_tran.Find("player_canvas2").gameObject;
                m_tran.Find("player_canvas").gameObject.SetActive(false);
            }
            else
            {
                _player_canvas = m_tran.Find("player_canvas").gameObject;
                m_tran.Find("player_canvas2").gameObject.SetActive(false);
            }
            _player_list = new SettlePlayerItemRender[MaxPlayerCount];
            for (int i = 0; i < _player_list.Length; i++)
            {
                _player_list[i] = _player_canvas.transform.Find("group/player_item_" + i).gameObject.AddComponent<SettlePlayerItemRender>();
                UGUIClickHandler.Get(_player_list[i].gameObject).onPointerClick+= ClickLookFor;
            }

        }
        else
        {
            _player_canvas = m_tran.Find("player_canvas").gameObject;
            m_camps = _player_canvas.transform.Find("campico/content").gameObject.AddComponent<DataGrid>();
            //m_camps.useLoopItems = true;
            m_gout = _player_canvas.transform.Find("campico/content").GetComponent<GridLayoutGroup>();
            _camp1_result_txt = _player_canvas.transform.Find("campico/content/camp_ico_1/camp1_result_txt").GetComponent<Text>();
            _camp2_result_txt = _player_canvas.transform.Find("campico/content/camp_ico_2/camp2_result_txt").GetComponent<Text>();
            m_playersettle = m_tran.Find("player_canvas/playerData/content").gameObject.AddComponent<DataGrid>();
            m_playersettle.useLoopItems = true;
            m_playersettle.SetItemRender(m_tran.Find("player_canvas/playerData/content/ItemPlayerSettle").gameObject, typeof(SettlePlayerItemRender));
            m_playersettle.onItemSelected = LookOverPlayer;
            m_playersettle.autoSelectFirst = false;
        }
        _camp0_img = _player_canvas.transform.Find("camp_ico_0").GetComponent<Image>();

        _score_lbl = _player_canvas.transform.Find("title/score_lbl").GetComponent<Text>();

        _popup_canvas = m_tran.Find("popup_canvas").gameObject;
        _reward_group = _popup_canvas.transform.Find("items").GetComponent<CanvasGroup>();
        _reward_list = new Transform[7];
        _popup_canvas.AddComponent<SortingOrderRenderer>();
        for (int i = 0; i < _reward_list.Length; i++)
        {
            _reward_list[i] = _popup_canvas.transform.Find("items/reward_item_" + i);
        }
        m_addictionInfo = m_tran.Find("info_canvas/addictionInfo").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(_double_btn.gameObject).onPointerClick += onDoubleBtnClick;
        if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
        {
            UGUIClickHandler.Get(_share_btn.gameObject).onPointerClick += onSubShareView;
        }    
        UGUIClickHandler.Get(m_tran.Find("next_btn")).onPointerClick += onSubViewHandler;        
        UGUIClickHandler.Get(_popup_canvas.transform.Find("HitArea")).onPointerClick += onSubViewHandler;
        if (m_btnFixAll != null)
            UGUIClickHandler.Get(m_btnFixAll.gameObject).onPointerClick += OnbtnFixClick;        
    }

    public void LookOverPlayer(object render)
    {
        SDSettlePlayer data = render as SDSettlePlayer;
        if (data == null)
        {
            return;
        }
        if (data.uid > 0&&data.uid!=PlayerSystem.roleId)
        {
            PlayerFriendData.ViewPlayer(data.uid);
        }
    }

    private void Toc_player_view_player(toc_player_view_player data)
    {
        if (!UIManager.IsOpen<PanelMainPlayerInfo>())
        {
            UIManager.PopPanel<PanelMainPlayerInfo>(new object[] { data, UIManager.CurContentPanel }, true, this, LayerType.Chat);
        }
    }

    public void ClickLookFor(GameObject target, PointerEventData eventData)
    {
        var data= target.GetComponent<SettlePlayerItemRender>();
        if (data == null)
        {
            return;
        }
        if (data._data == null)
        {
            return;
        }
        if (data._data.uid > 0 && data._data.uid != PlayerSystem.roleId)
        {
            PlayerFriendData.ViewPlayer(data._data.uid);
        }
        return;
    }




    private void OnbtnFixClick(GameObject target, PointerEventData eventData)
    {
        var fixAllCost = 0;
        var uidList = new List<int>();
        var arr = (int[][])_weapon_list.Data;
        for (int i = 0; i < arr.Length; i++)
        {
            var pitem = ItemDataManager.GetDepotItem(arr[i][0]);
            var item = ItemDataManager.GetItemWeapon(arr[i][0]);
            if (pitem == null || item == null)
                continue;

            if (pitem.durability < ItemDataManager.MAX_DURABILITY)
            {
                fixAllCost += (ItemDataManager.MAX_DURABILITY - pitem.durability) * item.RepairPrice;
                uidList.Add(pitem.uid);
            }
        }

        if (uidList.Count == 0)
        {
            TipsManager.Instance.showTips("无需修复");
            return;
        }

        UIManager.ShowTipPanel(string.Format("是否花费 {0}{1} 维修全部枪械？\n<color=#00EAFF>(维修后耐久度将回满)</color>", fixAllCost, "金币"),
            () => NetLayer.Send(new tos_player_repair_weapons() {weapon_uids = uidList.ToArray()}));
    }

    private void onDoubleBtnClick(GameObject target, PointerEventData eventData)
    {
        if (PlayerSystem.roleData.diamond >= PlayerBattleModel.Instance.double_cost)
        {
            PlayerBattleModel.Instance.TosDoubleGameReward();
        }
        else
        {
            TipsManager.Instance.showTips("钻石不足");
        }
    }

    private void onSubViewHandler(GameObject target, PointerEventData eventData)
    {
        if (_popup_canvas.activeSelf)
        {
            if (Time.realtimeSinceStartup < _unlock_time)
            {
                return;
            }
            _popup_canvas.SetActive(false);
            _player_canvas.SetActive(true);
            _weapon_canvas.SetActive(false);
            _exp_fill_start_time = Time.realtimeSinceStartup;
            if (scene_type != "stage" && scene_subtype != "stage")
            {
                if (CorpsDataManager.IsJoinCorps())
                {
                    if(PlayerSystem.roleData.level <= 15)
                        TipsManager.Instance.showTips("获得：战队贡献x3");
                    else 
                        TipsManager.Instance.showTips("获得：战队贡献x1");
                }
            }
            if (masterGradePoint > 0)
            {
                TipsManager.Instance.showTips("获得：名师积分x" + masterGradePoint);
                masterGradePoint = -1;
            }

        }
        else if (_player_canvas.activeSelf)
        {
            if (reward_data!=null && reward_data.weapon_exp_reward != null && reward_data.weapon_durability_reduce != null)
            {
                var weaponList = new List<int[]>();
                weaponList.AddRange(reward_data.weapon_exp_reward);
                for (int i = 0; i < reward_data.weapon_durability_reduce.Length; i++)
                {
                    var itemId = ItemDataManager.GetDepotItemByUid(reward_data.weapon_durability_reduce[i]).item_id;
                    var has = false;
                    for (int j = 0; j < reward_data.weapon_exp_reward.Length; j++)
                    {
                        if (itemId == reward_data.weapon_exp_reward[j][0])
                        {
                            has = true;
                            break;
                        }
                    }
                    if (!has)
                        weaponList.Add(new[] { itemId, 0 });
                }

                if (weaponList.Count == 0)
                {
                    HidePanel();
                    if (Driver.m_platform == EnumPlatform.ios)
                    {
                        if (isMVPOrACE)
                        {
                            SdkManager.appNSLog("isMVPOrACE1:" + isMVPOrACE);
                            if (SdkManager.isGoComment())
                            {
                                SdkManager.m_logFrom = 0;
                                SdkManager.getStarComment();
                            }
                            isMVPOrACE = false;
                        }
                    }					
                    return;
                }
                _popup_canvas.SetActive(false);
                _player_canvas.SetActive(false);
                _weapon_canvas.SetActive(true);

                _weapon_list.Data = weaponList.ToArray();
                UpdateAllFixCost();
            }
            else
            {
                HidePanel();
                if (Driver.m_platform == EnumPlatform.ios)
                {
                    if (isMVPOrACE)
                    {
                        SdkManager.appNSLog("isMVPOrACE2:" + isMVPOrACE);
                        if (SdkManager.isGoComment())
                        {
                            SdkManager.m_logFrom = 0;
                            SdkManager.getStarComment();
                        }
                        isMVPOrACE = false;
                    }
                }
            }
        }
        else if (_weapon_canvas.activeSelf)
        {
            HidePanel();
            if (Driver.m_platform == EnumPlatform.ios)
            {
                if (isMVPOrACE)
                {
                    SdkManager.appNSLog("isMVPOrACE3:" + isMVPOrACE);
                    if (SdkManager.isGoComment())
                    {
                        SdkManager.m_logFrom = 0;
                        SdkManager.getStarComment();
                    }
                    isMVPOrACE = false;
                }
            }
        }
    }

    private void onSubShareView(GameObject target, PointerEventData eventData)
    {
        UIManager.PopPanel<PanelWeixinShare>(new object[] { gameRule });
    }

    private void UpdateAllFixCost()
    {
        var fixAllCost = 0;
        var arr = (int[][])_weapon_list.Data;
        for (int i = 0; i < arr.Length; i++)
        {
            var pitem = ItemDataManager.GetDepotItem(arr[i][0]);
            var item = ItemDataManager.GetItemWeapon(arr[i][0]);
            if (pitem == null || item == null)
                continue;

            fixAllCost += (ItemDataManager.MAX_DURABILITY - pitem.durability) * item.RepairPrice;
        }

        m_txtFixAllCost.text = fixAllCost.ToString();
    }

    public override void OnShow()
    {
        isShowSerttle = true;
        m_panelStartTime = Time.realtimeSinceStartup;
        //if(GlobalConfig.serverValue.weixinShare_open)
        //{
            //_share_btn.enabled = true;
            //_share_btn.gameObject.SetActive(true);
            //if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
            //{
            //    if (RoomModel.LastRoomInfo != null && RoomModel.LastRoomInfo.stage != GuideManager.m_guideStageId)
            //    {
            //        var self_win = self_data != null && (self_data.camp == win_camp || win_camp == 0 && WorldManager.singleton.gameRuleLine.IsDeuceWin == 1);
            //        if (game_result != null && self_win/*  && game_result.scene_type.Equals("pvp")*/)
            //        {
            //            if (!_share_btn.gameObject.activeSelf)
            //            {
            //                _share_btn.enabled = true;
            //                _share_btn.gameObject.SetActive(true);
            //                //self_win_result = false;
            //            }
            //        }
            //    }
            //}
        //} 
        if (settle_data == null)
        {
            m_bMandatoryQuit = !WorldManager.singleton.isViewBattleModel;
            HidePanel();
            return; 
        }
        UIManager.PopRootPanel<PanelBackground>();
        PanelBackground panel = UIManager.GetPanel<PanelBackground>();
        if (panel != null)
        {
            panel.setupBG();
        }
        //UIManager.GetPanel<PanelBackground>().setupBG();
        if (settle_data == null || settle_data.Length == 0)
        {
            settle_data = new SDSettlePlayer[1] { new SDSettlePlayer() { uid = PlayerSystem.roleId, name = PlayerSystem.roleData.name, level = PlayerSystem.roleData.level, camp = 1, icon = PlayerSystem.roleData.icon } };
        }
        _popup_canvas.SetActive(reward_data != null && reward_data.item_reward != null && reward_data.item_reward.Length > 0);
        _player_canvas.SetActive(true);
        _weapon_canvas.SetActive(false);
        updateData();
        _unlock_time = 0;
        if (_popup_canvas.activeSelf)
        {
            SortingOrderRenderer.RebuildAll();
            _unlock_time = Time.realtimeSinceStartup + 2;
            if (_gongxi_effect == null)
            {
                _gongxi_effect = UIEffect.ShowEffect(_popup_canvas.transform, EffectConst.UI_SETTLE_REWARD_GONGXI, new Vector2(30, 80));
            }
            AudioManager.PlayUISound(AudioConst.battleReward);
            _reward_group.alpha = 0;
        }
        else
        {
            _exp_fill_start_time = Time.realtimeSinceStartup;
        }
        if (_double_effect == null)
        {
            _double_effect = UIEffect.ShowEffect(_double_btn.transform, EffectConst.UI_SETTLE_REWARD_DOUBLE_1);
        }
        Util.SetNormalShader(_double_btn);
        _double_btn.enabled = true;
        if(AntiAddictionManager.Instance.rewardState!=RewardState.Full)
        {
            m_addictionInfo.gameObject.SetActive(true);
            if(AntiAddictionManager.Instance.rewardState==RewardState.Half)
            {
                m_addictionInfo.text = "基础战斗收益减半";
            }
            else
            {
                m_addictionInfo.text = "基础战斗收益为零";
            }
        }
        else
            m_addictionInfo.gameObject.SetActive(false);
    }


    protected virtual void updateData()
    {

        if (scene_type == "worldboss")
        {
            //以伤害量排名
            Array.Sort(settle_data, (a, b) =>
            {
                var result = a.camp - b.camp;
                if (result == 0)
                {
                    result = b.score - a.score;
                }
                if (result == 0)
                {
                    result = b.kill - a.kill;
                }
                return result;
            });
        }
        else
        {
            //以杀人数做排名
            Array.Sort(settle_data, (a, b) =>
            {
                var result = a.camp - b.camp;
                if (result == 0)
                {
                    result = b.kill - a.kill;
                }
                if (result == 0)
                {
                    result = b.score - a.score;
                }
                return result;
            });
        }

        var index = 0;
        int firstCamp = settle_data[0].camp;
        int lastCamp = settle_data[settle_data.Length - 1].camp;
        self_data = null;
        var game_rule = RoomModel.LastRoomInfo != null ? RoomModel.LastRoomInfo.rule : null;
        var line = RoomModel.LastRoomInfo != null && RoomModel.LastRoomInfo.max_player > MaxPlayerCount ? RoomModel.LastRoomInfo.max_player / 2 : 5;
        if (RoomModel.ChannelType == ChannelTypeEnum.survival || RoomModel.ChannelType == ChannelTypeEnum.defend || RoomModel.ChannelType == ChannelTypeEnum.worldboss || RoomModel.ChannelType == ChannelTypeEnum.stage_survival)
        {
            for (int i = 0; i < _player_list.Length; i++)
            {
                var render = _player_list[i];
                if (index < settle_data.Length && (firstCamp == settle_data[index].camp || i >= line))
                {
                    render.SetData(settle_data[index]);
                    if (settle_data[index].uid == PlayerSystem.roleId)
                    {
                        self_data = settle_data[index];
                        self_share = self_data;
                        self_rank = i + 1;
                    }
                    index++;
                }
                else
                {
                    render.SetData(null);
                }
            }
        }
        else//m_gout、m_playersettle均不为空
        {
            SDSettlePlayer[] resultsettle = null;
            if (RoomModel.LastRoomInfo != null && RoomModel.LastRoomInfo.max_player > MaxPlayerCount)
            {
                resultsettle = new SDSettlePlayer[RoomModel.LastRoomInfo.max_player];
                m_gout.cellSize = new Vector2(m_gout.cellSize.x, 24f * RoomModel.LastRoomInfo.max_player);
            }
            else
            {
                resultsettle = new SDSettlePlayer[MaxPlayerCount];
                m_gout.cellSize = new Vector2(m_gout.cellSize.x, 240f);
            }
            if(game_rule == GameConst.GAME_RULE_HIDE
                || game_rule == GameConst.GAME_RULE_ZOMBIE
                || game_rule == GameConst.GAME_RULE_ZOMBIE_HERO
                || game_rule == GameConst.GAME_RULE_ZOMBIE_ULTIMATE
                || game_rule == GameConst.GAME_RULE_KING_SOLO
                || game_rule == GameConst.GAME_RULE_SOLO
            )//普通界面不分阵型
            {
                for (int i = 0; i < resultsettle.Length; i++)
                {
                    var render = resultsettle[i];
                    if (index < settle_data.Length)
                    {
                        resultsettle[i] = settle_data[index];
                        if (settle_data[index].uid == PlayerSystem.roleId)
                        {
                            self_data = settle_data[index];
                        }
                        index++;
                    }
                    else
                    {
                        resultsettle[i] = null;
                    }
                }
                m_playersettle.Data = resultsettle;
            }
            else//普通界面区分阵型
            {
                for (int i = 0; i < resultsettle.Length; i++)
                {
                    var render = resultsettle[i];
                    if (index < settle_data.Length && (firstCamp == settle_data[index].camp || i >= line))
                    {
                        resultsettle[i] = settle_data[index];
                        if (settle_data[index].uid == PlayerSystem.roleId)
                        {
                            self_data = settle_data[index];
                        }
                        index++;
                    }
                    else
                    {
                        resultsettle[i] = null;
                    }
                }
                m_playersettle.Data = resultsettle;
            }
        }
        var self_win = self_data != null && (self_data.camp == win_camp || win_camp == 0 && WorldManager.singleton.gameRuleLine.IsDeuceWin == 1);
        if (game_rule == GameConst.GAME_RULE_ERADICATE
           || game_rule == GameConst.GAME_RULE_ERADICATE2
           || game_rule == GameConst.GAME_RULE_CHALLENGE
           || game_rule == GameConst.GAME_RULE_SURVIVAL
           || game_rule == GameConst.GAME_RULE_DEFEND
           || game_rule == GameConst.GAME_RULE_DEFEND0
           || game_rule == GameConst.GAME_RULE_DEFEND2
           || game_rule == GameConst.GAME_RULE_SUR_SNIPE
           || game_rule == GameConst.GAME_RULE_WORLD_BOSS
           || game_rule == GameConst.GAME_RULE_BOSS_NEWER
           )//不分阵型，分胜负模式
        {
            _camp0_img.gameObject.SetActive(true);
        }
        else//m_gout、_camp1_result_txt、_camp2_result_txt均不为空
        {
            if (game_rule == GameConst.GAME_RULE_HIDE
            || game_rule == GameConst.GAME_RULE_ZOMBIE
            || game_rule == GameConst.GAME_RULE_ZOMBIE_HERO
            || game_rule == GameConst.GAME_RULE_ZOMBIE_ULTIMATE
            || game_rule == GameConst.GAME_RULE_KING_SOLO
            || game_rule == GameConst.GAME_RULE_SOLO
            )//不分阵型,不分胜负模式
            {
                _camp0_img.gameObject.SetActive(true);
                m_gout.gameObject.SetActive(false);
                self_win = true;
            }
            else//通常模式
            {
                _camp0_img.gameObject.SetActive(false);
                m_gout.gameObject.SetActive(true);
                _camp1_result_txt.text = (win_camp == firstCamp || win_camp == 0 && WorldManager.singleton.gameRuleLine.IsDeuceWin == 1) ? "胜利" : "失败";
                _camp2_result_txt.text = (win_camp == lastCamp || win_camp == 0 && WorldManager.singleton.gameRuleLine.IsDeuceWin == 1) ? "胜利" : "失败";
            }
        }
        if (self_win && GlobalConfig.serverValue.weixinShare_open)
        {
            if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
                _share_btn.gameObject.TrySetActive(true);        
        }
        else
            _share_btn.gameObject.TrySetActive(false);
        _result_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Settle, self_win ? "win_ico" : "lose_ico"));
        _result_img.SetNativeSize();

        if (scene_type == "match")
            _score_lbl.text = "排位积分";
        else if (scene_type == "survival" || scene_type == "worldboss")
            _score_lbl.text = "救助";
        else
            _score_lbl.text = "积分";
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            _score_lbl.text = "赞";
        }
        if (reward_data == null)
            return;

        var level_info = self_data != null ? ConfigManager.GetConfig<ConfigRoleLevel>().GetLine(PlayerSystem.roleData.level + 1) : null;
        _exp_fill_new_value = level_info != null ? (float)PlayerSystem.roleData.exp / level_info.NeedExp : 1;
        if (PlayerSystem.roleData.exp <= reward_data.exp)
            _exp_old.fillAmount = 0;
        else
            _exp_old.fillAmount = level_info != null ? (float)(PlayerSystem.roleData.exp - reward_data.exp) / level_info.NeedExp : 1;
        _exp_new.fillAmount = _exp_old.fillAmount;

        _lvl_txt.text = String.Format("{0}级", PlayerSystem.roleData.level);
        _exp_txt.text = "+" + reward_data.exp;
        _gold_txt.text = reward_data.coin.ToString();
        _honour_txt.text = reward_data.medal.ToString();
        _double_btn.transform.Find("Text").GetComponent<Text>().text = PlayerBattleModel.Instance.double_cost == 0 ? "免费" : PlayerBattleModel.Instance.double_cost.ToString();

        for (int j = 0; j < _reward_list.Length; j++)
        {
            var item = _reward_list[j];
            if (j < reward_data.item_reward.Length)
            {
                item.gameObject.SetActive(true);
                var img = item.Find("Image").GetComponent<Image>();
                var name = item.Find("Name").GetComponent<Text>();
                var num = item.Find("Num").GetComponent<Text>();
                var cfg = ItemDataManager.GetItem(reward_data.item_reward[j].ID);
                img.SetSprite(ResourceManager.LoadIcon(cfg.Icon));
                img.SetNativeSize();
                name.text = cfg.DispName;
                num.text = "x" + reward_data.item_reward[j].cnt;
                var colorValue = cfg.RareType;
                item.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(colorValue)));
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    public override void OnHide()
    {
        settle_data = null;
        if (_double_effect != null)
        {
            _double_effect.Destroy();
            _double_effect = null;
        }
        if (_gongxi_effect != null)
        {
            _gongxi_effect.Destroy();
            _gongxi_effect = null;
        }
        GameDispatcher.Dispatch(GameEvent.UI_SETTLE_END);
        isShowSerttle = false;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
        isShowSerttle = false;
        game_result = null;
    }

    public override void Update()
    {
        //结算界面呆60秒后自动离开房间
        //if (Time.realtimeSinceStartup - m_panelStartTime > 60)
        //{
        //    m_panelStartTime = float.MaxValue;
        //    if (RoomModel.IsInRoom)
        //    {
        //        NetLayer.Send(new tos_room_leave());
        //        UIManager.ShowTipPanel("您长时间未回到房间，已自动离开");
        //    }
        //}
        if (m_camps!=null)
        {
            m_camps.verticalPos = m_playersettle.verticalPos;
        }

        if (_reward_group.alpha < 1)
        {
            if (_unlock_time > 0 && Time.realtimeSinceStartup >= (_unlock_time - 1))
            {
                _reward_group.alpha += 1 / 0.5f * Time.deltaTime;
            }
        }
        float dt = Time.realtimeSinceStartup - _exp_fill_start_time;
        if (dt >= EXP_FILL_TIME)
            _exp_new.fillAmount = _exp_fill_new_value;
        else if (dt <= 0)
            _exp_new.fillAmount = _exp_old.fillAmount;
        else
            _exp_new.fillAmount = _exp_old.fillAmount + (_exp_fill_new_value - _exp_old.fillAmount) * dt / EXP_FILL_TIME;
        if (dt >= (EXP_FILL_TIME+0.25))
        {
            if (PlayerSystem.delay_levelup_effect_value > 0)
            {
                var old_value = PlayerSystem.delay_levelup_effect_value;
                PlayerSystem.delay_levelup_effect_value = 0;
                UIManager.PopPanel<PanelRoleLevelup>(new object[] { old_value, PlayerSystem.roleData.level }, false);
            }
        }
        if (data_updated == false)
        {
            return;
        }
        data_updated = false;
        updateData();
    }


    internal void Toc_player_repair_weapons(toc_player_repair_weapons data)
    {
        _weapon_list.Data = _weapon_list.Data;
        UpdateAllFixCost();
        TipsManager.Instance.showTips("修理成功");
        AudioManager.PlayUISound(AudioConst.levelUp);
    }

    public static bool isShowSerttle = false; //是否在显示奖励
    static void Toc_fight_game_result(toc_fight_game_result proto)
    {
        isShowSerttle = true;
        if (Driver.m_platform == EnumPlatform.ios || Driver.m_platform == EnumPlatform.android)
        {
            game_result = proto;
        }
        gameRule = proto.scene_type;
        toc_fight_game_result sdata = proto;
        avg_honor_1 = 0;
        avg_honor_2 = 0;
        win_camp = sdata.win_camp;
        scene_type = sdata.scene_type;
        scene_subtype = sdata.scene_subtype;
        abnormal_finish = sdata.abnormal_finish;
        game_round = sdata.game_round;
        game_time = sdata.game_time;
	
        var ary = sdata.players;
        Array.Sort(ary,(a,b)=>b.kill-a.kill);//按击杀排一次序
        int mvpid = sdata.mvp;
        int gaceid = sdata.gold_ace;
        int saceid = sdata.silver_ace;
        var camp_1_num = 0;
        var camp_2_num = 0;
        List<SDSettlePlayer> data = new List<SDSettlePlayer>();
        for (int i = 0; i < ary.Length; i++)
        {
            if (WorldManager.singleton.isSurvivalTypeModeOpen && ary[i].player_id <= 0)
                continue;
            int id = ary[i].actor_id;
            var sp = new SDSettlePlayer()
            {
                uid = ary[i].player_id,
                name = ary[i].name,
                camp = ary[i].camp,
                level = ary[i].level,
                honor = ary[i].honor,
                kill = ary[i].kill,
                death = ary[i].death,
                score = ary[i].score,
                icon = ary[i].icon,
                rank_score = ary[i].honor2add,
                mvp = id == mvpid,
                gold_ace = id == gaceid,
                silver_ace = id == saceid,
                game_time = ary[i].game_time,
                rescue_times = ary[i].rescue_times,
                revive_times = ary[i].revive_times,
                avg_type = ary[i].camp == (int)WorldManager.CAMP_DEFINE.REBEL ? 2 : 1,
                is_win = ary[i].camp == win_camp,
                mType=(MemberManager.MEMBER_TYPE)ary[i].vip_level
            };
            if (Driver.m_platform == EnumPlatform.ios)
            {
				if (ary[i].player_id == PlayerSystem.roleId && proto.scene_type.Equals("pvp") && PlayerSystem.roleData.level >= 5)
				{
					if (mvpid == id || gaceid == id || saceid == id)
					{
						isMVPOrACE = true;
					}
				}
			}          
            //对于部分模式重新计算一些值
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
            {
                sp.score = ary[i].praised_cnt;
            }
            else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_KING_SOLO))
            {
                var win_index = Mathf.FloorToInt(ary.Length / 2);
                sp.avg_type = i >= win_index ? 2 : 1;
                sp.is_win = i < win_index;
            }
            //记录运算平均分需要的数据
            if(sp.avg_type==2)
            {
                avg_honor_2 += sp.honor;
                camp_2_num++;
            }
            else
            {
                avg_honor_1 += sp.honor;
                camp_1_num++;
            }
            data.Add(sp);
        }
        if (camp_1_num > 0)
        {
            avg_honor_1 = (int)Mathf.Ceil((float)avg_honor_1 / camp_1_num);
        }
        if (camp_2_num > 0)
        {
            avg_honor_2 = (int)Mathf.Ceil((float)avg_honor_2 / camp_2_num);
        }
        settle_data = data.ToArray();
        data_updated = true;
        NetLayer.PortoCount2File();
    }

    static void Toc_player_game_reward(toc_player_game_reward proto)
    {
        reward_data = proto;
        data_updated = true;
    }

    void Toc_player_double_game_reward(toc_player_double_game_reward data)
    {
        PlayerSystem.roleData.double_reward_times = data.times;
        if (reward_data == null)
        {
            return;
        }
        reward_data.exp += data.exp;
        reward_data.coin += data.coin;
        reward_data.medal += data.medal;
        data_updated = true;
        _exp_fill_start_time = Time.realtimeSinceStartup;
        _double_btn.enabled = false;
        Util.SetGrayShader(_double_btn);
        if (_double_effect != null)
        {
            _double_effect.Destroy();
        }
        _double_effect = UIEffect.ShowEffect(_double_btn.transform, EffectConst.UI_SETTLE_REWARD_DOUBLE_2, 2);
        AudioManager.PlayUISound(AudioConst.doubleReward);
    }

    /// <summary>
    /// 名师段位积分
    /// </summary>
    /// <param name="proto"></param>
    static void Toc_master_grade_get_point(toc_master_grade_get_point proto)
    {
        masterGradePoint = proto.point;
    }



    public static void PushWeaponLevelup(object[] args)
    {
        weapon_lvup_queue.Enqueue(args);
    }

    public static bool PopWeaponLevelup()
    {
        if (weapon_lvup_queue.Count > 0 && !UIManager.IsOpen<PanelWeaponLevelup>())
        {
            UIManager.PopPanel<PanelWeaponLevelup>(weapon_lvup_queue.Dequeue());
            return true;
        }
        return false;
    }
}
