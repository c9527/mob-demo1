﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HeadList
{
    private Sprite m_liveSprite;
    private Sprite m_deadSprite;

    private List<GameObject> m_HeadList = null;

    private int m_iLiveNum = 0;

    private Transform m_transParent;

    public void Init(Transform parent, Sprite liveSprite, Sprite deadSprite, int headNum, bool bRight)
    {
        m_transParent = parent;
        m_liveSprite = liveSprite;
        m_deadSprite = deadSprite;

        if (m_HeadList == null)
        {
            m_HeadList = new List<GameObject>(headNum);
            for (int i = 0; i < headNum; i++)
            {
                GameObject newGo = new GameObject("head" + i);
                m_HeadList.Add(newGo);
            }
            InitHeadList();
            ShowAllHeadList();
        }
        else
        {
            int length = m_HeadList.Count;
            if (length > headNum)
            {
                ShowAllHeadList();
                for (int i = headNum; i < length; i++)
                {
                    m_HeadList[i].TrySetActive(false);
                }
            }
            else if (length < headNum)
            {
                for (int i = length; i < headNum; i++)
                {
                    GameObject newGo = new GameObject("head" + i);
                    m_HeadList.Add(newGo);
                }
                InitHeadList();
                ShowAllHeadList();
            }
        }
    }

    public void LiveAndDeadNum(int liveNum, int deadNum)
    {
        if (m_HeadList == null) return;
        int length = m_HeadList.Count;
        for (int i = 0; i < liveNum && i < length; i++)
        {
            GameObject go = m_HeadList[i];
            go.TrySetActive(true);
            go.GetComponent<Image>().SetSprite(m_liveSprite);
        }
        int allNum = liveNum + deadNum;
        for (int i = liveNum; i < allNum && i < length; i++)
        {
            GameObject go = m_HeadList[i];
            go.TrySetActive(true);
            go.GetComponent<Image>().SetSprite(m_deadSprite);
        }
    }

    void InitHeadList()
    {
        for (int index = 0; index < m_HeadList.Count; ++index)
        {
            GameObject go = m_HeadList[index];
            go.transform.SetParent(m_transParent);
            go.AddMissingComponent<RectTransform>().localScale = Vector3.one;
            go.AddMissingComponent<Image>().SetNativeSize();
        }
    }

    public void ResetHeadNum()
    {
        ShowAllHeadList();
    }

    void ShowAllHeadList()
    {
        if (m_HeadList == null) return;
        for (int i = 0; i < m_HeadList.Count; i++)
        {
            GameObject go = m_HeadList[i];
            go.GetComponent<Image>().SetSprite(m_liveSprite);
        }
    }
}

public class BattlingMode : MonoBehaviour
{
    // Use this for initialization
    public int m_iTraitorNum;
    public int m_iUnionNum;
    public float m_iLoopTime;
    public float m_iDownTime;
    public bool m_isDownTimer;
    private int m_iTraitorDeadTimes = 0;
    private int m_iUnionDeadTimes = 0;

    private HeadList m_unionHeadList;
    private HeadList m_traitorHeadList;

    private UIImgNumText m_kTraitorWinCount;
    private UIImgNumText m_kUnionWinCount;
    //private Image m_imgModeName;
    //private ImageDownWatch m_kDownTimer;
    private TextDownWatch m_kDownTimer;

    Image m_leftMode;
    Image m_rightMode;

    ConfigGameRule m_configGameRule;
    ConfigMapList m_configMapList;

    UIImgNumText m_kWinParam;
    float m_fLockTime = 0;

    Transform leftHeadTrans;
    Transform rightHeadTrans;

    GameObject unionBg0;
    GameObject unionBg1;
    GameObject rebelBg0;
    GameObject rebelBg1;

    GameObject m_unionKillCount;
    GameObject m_rebelKillCount;
    UIImgNumText m_unionKillCountTxt;
    UIImgNumText m_rebelKillCountTxt;

    void Start()
    {
        if (m_traitorHeadList == null)
        {
            m_traitorHeadList = new HeadList();
        }

        if (m_unionHeadList == null)
        {
            m_unionHeadList = new HeadList();
        }

        if (m_iTraitorNum > 0)
        {
            ShowTraitorList(m_iTraitorNum);
        }

        if (m_iUnionNum > 0)
        {
            ShowUnionList(m_iUnionNum);
        }
        m_configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        m_configMapList = ConfigManager.GetConfig<ConfigMapList>();
        

        Transform tempTransform = gameObject.transform;
        m_kTraitorWinCount = new UIImgNumText(tempTransform.Find("Right/rebeldeadcount"), "b_", AtlasName.Nums, 0, 3);
        m_kUnionWinCount = new UIImgNumText(tempTransform.Find("Left/uniondeadcount"), "b_", AtlasName.Nums, 0, 3);

        //m_imgModeName = tempTransform.Find("modename").gameObject.GetComponent<Image>();

        m_kDownTimer = tempTransform.Find("Middle/time").gameObject.AddMissingComponent<TextDownWatch>();
        //Sprite[] timeSpriteList = { ResourceManager.LoadSprite(AtlasName.Battle, "time0"), ResourceManager.LoadSprite(AtlasName.Battle, "time1"),
        //                    ResourceManager.LoadSprite( AtlasName.Battle,"time2"),ResourceManager.LoadSprite( AtlasName.Battle,"time3"),
        //                    ResourceManager.LoadSprite( AtlasName.Battle,"time4"),ResourceManager.LoadSprite( AtlasName.Battle,"time5"),
        //                    ResourceManager.LoadSprite( AtlasName.Battle,"time6"),ResourceManager.LoadSprite( AtlasName.Battle,"time7"),
        //                    ResourceManager.LoadSprite( AtlasName.Battle,"time8"),ResourceManager.LoadSprite( AtlasName.Battle,"time9")};
        //m_kDownTimer.SetNumSpriteList(timeSpriteList);

        m_leftMode = tempTransform.Find("Left/unionlabel").gameObject.GetComponent<Image>();
        m_rightMode = tempTransform.Find("Right/rebellabel").gameObject.GetComponent<Image>();

        leftHeadTrans = tempTransform.Find("Left/unionHead");
        rightHeadTrans = tempTransform.Find("Right/rebelHead");

        unionBg0 = tempTransform.Find("Left/unionBg0").gameObject;
        unionBg1 = tempTransform.Find("Left/unionBg1").gameObject;
        rebelBg0 = tempTransform.Find("Right/rebelBg0").gameObject;
        rebelBg1 = tempTransform.Find("Right/rebelBg1").gameObject;

        m_kWinParam = new UIImgNumText(tempTransform.Find("Middle/count"), "mode", AtlasName.Battle,0,-8);

        m_unionKillCount = tempTransform.Find("Left/unionkillcount").gameObject;
        m_rebelKillCount = tempTransform.Find("Right/rebelkillcount").gameObject;
        m_unionKillCountTxt = new UIImgNumText(tempTransform.Find("Left/unionkillcount/unionkillcounttxt"), "time", AtlasName.Battle, 0, 1);
        m_rebelKillCountTxt = new UIImgNumText(tempTransform.Find("Right/rebelkillcount/rebelkillcounttxt"), "time", AtlasName.Battle, 0, 1);

        //GameDispatcher.AddEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnBattlingStaticInfoBack);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED, OnArenaInfoUpdate);
        GameDispatcher.AddEventListener(GameEvent.PROXY_PLAYER_BIG_HEAD_FINAL_TIME_UPDATED, OnBigHeadFinalTimeUpdate);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);

        OnBattlingStaticInfoBack();
        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state);

        if(WorldManager.singleton.isFightInTurnModeOpen)  //如果是擂台战时，才调用这个方法
            OnArenaInfoUpdate();
        if (WorldManager.singleton.isBigHeadModeOpen || WorldManager.singleton.isDotaModeOpen)
            OnBigHeadFinalTimeUpdate();
    }

    
    public void Init()
    {
        m_iTraitorDeadTimes = 0;
        m_iUnionDeadTimes = 0;
    }


    public int traitornum
    {
        set
        {
            int oldNum = m_iTraitorNum;
            m_iTraitorNum = value;
            if (oldNum != m_iTraitorNum)
            {
                ShowTraitorList(m_iTraitorNum);
            }
        }
        get
        {
            return m_iTraitorNum;
        }
    }

    public int unionnum
    {
        set
        {
            int oldNum = m_iUnionNum;
            m_iUnionNum = value;

            if (oldNum != m_iUnionNum)
            {
                ShowUnionList(m_iUnionNum);
            }
        }
        get
        {
            return m_iUnionNum;
        }
    }

    

    public void SetTraitorCount(int count)
    {
        m_iTraitorDeadTimes = count;
        m_kTraitorWinCount.value = m_iTraitorDeadTimes;
    }

    public void SetUnionCount(int count)
    {
        m_iUnionDeadTimes = count;
        m_kUnionWinCount.value = m_iUnionDeadTimes;
    }

    private void ShowTraitorList(int num)
    {
        Sprite taitorSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "blue_point_2", (Sprite sprite) =>
            {
                taitorSprite = sprite;
            });

        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });
        m_traitorHeadList.Init(rightHeadTrans, taitorSprite, deadSprite, num, true);
    }

    private void ShowUnionList(int num)
    {
        Sprite unionSprite = null;

        ResourceManager.LoadSprite(AtlasName.BattleNew, "yellow_point", (Sprite sprite) =>
        {
            unionSprite = sprite;
        });

        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });
        m_unionHeadList.Init(leftHeadTrans, unionSprite, deadSprite, num, false);
    }

    private float m_tick;
    void Update()
    {
        if( m_fLockTime > 0 )
        {
            m_fLockTime -= Time.deltaTime;
        }
        if (m_kDownTimer != null)
        {
            //if (m_kDownTimer.gameObject.activeSelf == false && BurstBehavior.StartBombCD == false)
            //{
            //    m_kDownTimer.gameObject.TrySetActive(true);
            //}
            if (m_kDownTimer.gameObject.activeSelf == true && BurstBehavior.singleton != null && BurstBehavior.singleton.StartBombCD)
            {
                m_kDownTimer.SetTime = 0;
            }
        }
        m_tick += Time.deltaTime;
        
        if(m_tick >= 1)
        {
            m_tick = 0;
            OnBattlingStaticInfoBack();
        }
    }
  
    void ShowBg(bool left, bool right)
    {
        if(unionBg0 != null) unionBg0.TrySetActive(!left);
        if(unionBg1 != null) unionBg1.TrySetActive(left);
        if(rebelBg0 != null) rebelBg0.TrySetActive(!right);
        if(rebelBg1 != null) rebelBg1.TrySetActive(right);
    }

    private void OnBattlingStaticInfoBack()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        string gameRule = WorldManager.singleton.gameRule;
        if (data == null || string.IsNullOrEmpty(gameRule))
        {
            return;
        }
        List<SBattleCamp> camps = data.camps;
        int iUnionPlayers = 0;
        int iTraitorPlayers = 0;
        int liveUnionNum = 0;
        int liveRebelNum = 0;
        SBattlePlayer self_data = null;
        SBattlePlayer top_data = null; ;

        for (int i = 0; i < camps.Count; i++ )
        {
            SBattleCamp camp = camps[i];

            for(int j=0; j<camp.players.Count; j++)
            {
                SBattlePlayer splayer = camp.players[j];

                if (splayer.actor_id == WorldManager.singleton.myid)
                {
                    self_data = splayer;
                }
                if (top_data == null || splayer.round_kill > top_data.round_kill)
                {
                    top_data = splayer;
                }
                if (camp.camp == (int)WorldManager.CAMP_DEFINE.UNION)
                {
                    ++iUnionPlayers;
                    if (splayer.alive)
                    {
                        ++liveUnionNum;
                    }
                }
                else if (camp.camp == (int)WorldManager.CAMP_DEFINE.REBEL
                        || (camp.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_ZOMBIE, GameConst.GAME_RULE_ZOMBIE_HERO, GameConst.GAME_RULE_ZOMBIE_ULTIMATE))
                        || (camp.camp == (int)WorldManager.CAMP_DEFINE.CAT && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
                        )
                {
                    ++iTraitorPlayers;
                    if (splayer.alive)
                    {
                        ++liveRebelNum;
                    }
                }
            }
        }

        traitornum = iTraitorPlayers;
        unionnum = iUnionPlayers;
        m_unionHeadList.LiveAndDeadNum( liveUnionNum, iUnionPlayers - liveUnionNum );
        m_traitorHeadList.LiveAndDeadNum(liveRebelNum, iTraitorPlayers - liveRebelNum);
        int leftCount = 0;
        int rightCount = 0;
        switch (WorldManager.singleton.gameRuleLine.WinType)
        {
            case "playerkill":
                if (self_data != null)
                    leftCount = self_data.round_kill;
                if (top_data != null)
                    rightCount = top_data.round_kill;
                break;
            case "campkill":
                leftCount = camps.Count < 1 ? 0 : camps[0].round_kill;
                rightCount = camps.Count < 2 ? 0 : camps[1].round_kill;
                break;
            case "explode":
            case "killall":
            case "zombie":
            case "zombie_hero":
            case "ghost":
            case "hide":
            case "arena":
            case "big_head":
            case "team_hero":
                leftCount = camps.Count < 1 ? 0 : camps[0].win;
                rightCount = camps.Count < 2 ? 0 : camps[1].win;
                break;
            case "ow":
                if (DaggerPointBehavior.singleton != null && DaggerPointBehavior.singleton.ResoureList() != null && DaggerPointBehavior.singleton.ResoureList().Length > 0)
                {
                    leftCount = DaggerPointBehavior.singleton.ResoureList()[0];
                }
                if (DaggerPointBehavior.singleton != null && DaggerPointBehavior.singleton.ResoureList() != null&&DaggerPointBehavior.singleton.ResoureList().Length > 1)
                {
                    rightCount = DaggerPointBehavior.singleton.ResoureList()[1];
                }
                break;
        }
        //ConfigGameRuleLine line = m_configGameRule.GetLine(data.game_type);
        //if (m_imgModeName.sprite==null || m_imgModeName.sprite.name!=line.Mode_Name_ICON)
        //{
        //    m_imgModeName.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, !string.IsNullOrEmpty(line.Mode_Name_ICON) ? line.Mode_Name_ICON : "rule_solo"));
        //    m_imgModeName.SetNativeSize();
        //}
        if (data.win_param>0)
        {
            if( WorldManager.singleton.isDaggerOW )
            {
                var map = m_configMapList.GetLine(SceneManager.singleton.curMapId.ToString());
                int winParam = map.OwTarget;
                m_kWinParam.value = winParam;
            }
            else
            {
                m_kWinParam.value = data.win_param;
            }
            
        }
        else if( WorldManager.singleton.gameRuleLine.WinRound > 0 )
        {
            m_kWinParam.value = WorldManager.singleton.gameRuleLine.WinRound;
        }
        m_kWinParam.gameObject.transform.localPosition = new Vector3(0f, 0f);
        SetUnionCount(leftCount);
        SetTraitorCount(rightCount);
        m_unionKillCount.TrySetActive(false);
        m_rebelKillCount.TrySetActive(false);

        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if(curPlayer == null)
            curPlayer = MainPlayer.singleton;

        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_ZOMBIE, GameConst.GAME_RULE_ZOMBIE_HERO, GameConst.GAME_RULE_ZOMBIE_ULTIMATE))
        {
            if (m_leftMode.sprite == null || m_leftMode.sprite.name != "rule_camp_zombie_1")
            {
                m_leftMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_camp_zombie_1"));
                m_rightMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_camp_zombie_2"));
                m_leftMode.SetNativeSize();
                m_rightMode.SetNativeSize();
            }
            if (curPlayer != null)
            {
                bool right = curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE;
                Util.SetGoGrayShader(m_leftMode.gameObject, right);
                Util.SetGoGrayShader(m_rightMode.gameObject, !right);
                ShowBg(!right, right);
            }
        }
        else if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            m_kWinParam.gameObject.transform.localPosition = new Vector3(10f, 0f);
            if (m_leftMode.sprite == null || m_leftMode.sprite.name != "rule_camp_zombie_1")
            {
                m_leftMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_camp_zombie_1"));
                m_rightMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_hide_cat"));
                m_kWinParam.gameObject.transform.localPosition = new Vector3(10f, 0f);
                m_leftMode.SetNativeSize();
                m_rightMode.SetNativeSize();
            }
            if (curPlayer != null)
            {
                bool right = curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.CAT;
                Util.SetGoGrayShader(m_leftMode.gameObject, right);
                Util.SetGoGrayShader(m_rightMode.gameObject, !right);
                ShowBg(!right, right);
            }
        }
        else if(WorldManager.singleton.isTeamHeroModeOpen)
        {
            m_leftMode.rectTransform.sizeDelta = Vector2.zero;
            m_rightMode.rectTransform.sizeDelta = Vector2.zero;
            m_unionKillCount.TrySetActive(true);
            m_rebelKillCount.TrySetActive(true);
            m_unionKillCountTxt.value = camps.Count < 1 ? 0 : camps[0].round_kill;
            m_rebelKillCountTxt.value = camps.Count < 2 ? 0 : camps[1].round_kill;
            if (curPlayer != null)
            {
                bool right = curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.REBEL;
                ShowBg(!right, right);
            }
        }
        else if (iTraitorPlayers == 0 && iUnionPlayers == 0)
        {
            if (m_leftMode.sprite == null || m_leftMode.sprite.name != "rule_camp_solo_1")
            {
                m_leftMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_camp_solo_1"));
                m_rightMode.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "rule_camp_solo_2"));
                m_leftMode.SetNativeSize();
                m_rightMode.SetNativeSize();
            }
            Util.SetGoGrayShader(m_leftMode.gameObject, false);
            Util.SetGoGrayShader(m_rightMode.gameObject, false);
        }
        else
        {
            if (m_leftMode.sprite == null || m_leftMode.sprite.name != "camp_1")
            {
                m_leftMode.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "camp_1"));
                m_rightMode.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "camp_2"));
                m_leftMode.SetNativeSize();
                m_rightMode.SetNativeSize();
            }
            if (curPlayer != null)
            {
                bool right = curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.REBEL;
                Util.SetGoGrayShader(m_leftMode.gameObject, right);
                Util.SetGoGrayShader(m_rightMode.gameObject, !right);
                ShowBg(!right, right);
            }
        }
       
    }

    public void OnDestroy()
    {
        //GameDispatcher.RemoveEventListener<SBattleInfo>( GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO,OnBattlingStaticInfoBack );
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener(GameEvent.PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED, OnArenaInfoUpdate);
        GameDispatcher.RemoveEventListener(GameEvent.PROXY_PLAYER_BIG_HEAD_FINAL_TIME_UPDATED, OnBigHeadFinalTimeUpdate);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);
    }

    void OnArenaInfoUpdate()
    {
        int totaltime = ConfigMisc.GetInt("arena_time");
        var run_time = PlayerBattleModel.Instance.battle_info.round_time;
        //Logger.Error("OnArenaInfoUpdate total: " + totaltime + ", run: " + run_time);
        if (m_kDownTimer == null || totaltime - run_time < 0) return;
        m_kDownTimer.SetTime = totaltime - run_time;
    }

    void OnBigHeadFinalTimeUpdate()
    {
        if(BigHeadBehavior.singleton == null)
            return;

        int finalBeginTime = BigHeadBehavior.singleton.finalStartTime;
        int totaltime = ConfigMisc.GetInt("big_head_final_time");
        var run_time = (Time.realtimeSinceStartup - WorldManager.singleton.StartTime) - finalBeginTime / 1000f;
        if (m_kDownTimer == null || totaltime - run_time < 0) return;
        m_kDownTimer.SetTime = totaltime - run_time;
    }

    void OnUpdateBattleState(SBattleState data)
    {
        if (WorldManager.singleton.isFightInTurnModeOpen) return;
        var rule = m_configGameRule.GetLine(WorldManager.singleton.gameRule);
        var map = m_configMapList.GetLine(SceneManager.singleton.curMapId.ToString());
        float totaltime=0f;
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
        {
            for (int i = 0; i < map.Hide_time.Length; i++)
            {
                totaltime += map.Hide_time[i];
            }
        }
        var run_time = PlayerBattleModel.Instance.battle_info.round_time;
        if (FightVideoManager.isPlayFight == true)
        {
            run_time = run_time * FightVideoManager.PlaySpeed;
        }
        if (data.state == 1)
        {
            m_kDownTimer.SetTime = 0;
            m_fLockTime = rule.GodTime - run_time;
            m_unionHeadList.ResetHeadNum();
            m_traitorHeadList.ResetHeadNum();
        }
        else if (data.state==2)
        {
            m_kDownTimer.SetTime = rule.Time_play - run_time;
            m_fLockTime = rule.GodTime - run_time;
            if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
            {
                m_kDownTimer.SetTime = totaltime - run_time;
            }
        }
        else if(data.state==3)
        {
            m_kDownTimer.SetTime = -1;
        }
        else
        {
            m_kDownTimer.SetTime = 0;
        }
    }

    void OnSwitchPlayer(int playerId)
    {
        m_tick = 0;
        OnBattlingStaticInfoBack();
    }
}
