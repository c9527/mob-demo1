﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class BattleGunRoutePanel : MonoBehaviour
{
    SBattlePlayer _data;
    int _step_kill;
    TDGunKingRule[] _rules;
    TDGunKingRule _rule;

    float _end_time = 0;
    float _last_time = 0;

    Text _title_txt;
    Text _first_txt;
    Image _guide_ico;
    Image _route_pb;
    Image[] _weapon_list;

    void Awake()
    {
        _rules = ConfigManager.GetConfig<ConfigKing>().m_dataArr;

        _title_txt = gameObject.transform.Find("title_txt").GetComponent<Text>();
        _first_txt = gameObject.transform.Find("first_ico/Text").GetComponent<Text>();
        _guide_ico = gameObject.transform.Find("guide_ico").GetComponent<Image>();
        _route_pb = gameObject.transform.Find("route_pb").GetComponent<Image>();

        _weapon_list = new Image[8];
        for (int i = 0; i < 8; i++)
        {
            _weapon_list[i] = gameObject.transform.Find("weapon_img_" + i).GetComponent<Image>();
        }
    }

    public void setCD(float value)
    {
        _end_time = value < 0 ? -1 : Time.realtimeSinceStartup + value;
        _last_time = 0;
    }

    public void updateData()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data==null)
        {
            return;
        }
        _data = null;
        SBattlePlayer first_data = null;
        if (data.camps != null && data.camps.Count > 0)
        {
            int selectedPlayerId = WorldManager.singleton.isViewBattleModel ? ChangeTeamView.watchedPlayerID : WorldManager.singleton.myid;

            foreach (SBattlePlayer splayer in data.camps[0].players)
            {
                if (_data == null && splayer.actor_id == selectedPlayerId)
                {
                    _data = splayer;
                }
                if (first_data == null || splayer.kill > first_data.kill)
                {
                    first_data = splayer;
                }
            }
        }
        _rule = null;
        _step_kill = 0;
        TDGunKingRule first_rule = null;
        var first_kill = 0;
        if (_rules != null && _rules.Length>0)
        {
            foreach (var rule in _rules)
            {
                if(_data!=null)
                {
                    if (_rule == null || rule.ID == _data.king_weapon_seq)
                    {
                        _rule = rule;
                    }
                    if(rule.ID<_data.king_weapon_seq)
                    {
                        _step_kill += rule.SoloNeedKill;
                    }
                }
                if (first_data != null)
                {
                    if (first_rule == null || rule.ID == first_data.king_weapon_seq)
                    {
                        first_rule = rule;
                    }
                    if (rule.ID < first_data.king_weapon_seq)
                    {
                        first_kill += rule.SoloNeedKill;
                    }
                }
            }
        }
        var step_interal = 65f;
        if (_data != null && _rule != null)
        {
            _step_kill = _data.kill - _step_kill;
            _guide_ico.rectTransform.anchoredPosition = new Vector2(_weapon_list[_rule.ID - 1].rectTransform.localPosition.x + step_interal * _step_kill / _rule.SoloNeedKill, -20);
            _route_pb.fillAmount = 0.021f / (_step_kill % _rule.SoloNeedKill == 0 ? 1 : 2) + 0.123f * _data.kill / _rule.SoloNeedKill;
        }
        if (first_data != null && first_rule!=null)
        {
            first_kill = first_data.kill - first_kill;
            ((RectTransform)_first_txt.rectTransform.parent).anchoredPosition = new Vector2(_weapon_list[first_rule.ID-1].rectTransform.localPosition.x + step_interal * first_kill / first_rule.SoloNeedKill, -28);
            _first_txt.text = first_data.name;
        }
        _guide_ico.enabled = _data!=first_data;
        for (int j = 0; j < _weapon_list.Length; j++)
        {
            _weapon_list[j].color = _rule != null && j < (_rule.ID - 1) ? new Color(92, 230, 255) : Color.white;
        }
    }

    void Update()
    {
        if (_rule == null || _end_time == -1)
        {
            return;
        }
        if ((Time.realtimeSinceStartup - _last_time) >= 1)
        {
            _last_time = Time.realtimeSinceStartup;
            _title_txt.text = string.Format("下阶段需消灭：{0}  剩余时间：{1}", _data != null ? (_rule.SoloNeedKill - _step_kill).ToString() : "-", _end_time > 0 ? TimeUtil.FormatTime((int)(_end_time - Time.realtimeSinceStartup), 0) : "00:00");
            if (_last_time>=_end_time)
            {
                _end_time = -1;
                _last_time = 0;
            }
        }
    }
}

