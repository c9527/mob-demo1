﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 被动技能项
/// </summary>
class PassiveSkillData
{
    public PassiveSkillItemRender data;
    public int timerId;
}

/// <summary>
/// 被动技能管理器
/// 跟buff管理搭配使用
/// </summary>
class PassiveSkillManager
{
    static public PassiveSkillManager singleton;
    private Dictionary<int, List<PassiveSkillItemRender>> m_dicItemCache = new Dictionary<int, List<PassiveSkillItemRender>>();
    private List<PassiveSkillItemRender> m_listAllItem = new List<PassiveSkillItemRender>();
    private LinkedList<PassiveSkillData> m_kPassiveSkill = new LinkedList<PassiveSkillData>();

    private const float ITEM_GAP_SIZE = 50f;

    public PassiveSkillManager()
    {
        singleton = this;
    }

    public void AddPassiveSkill(TDBuffInfo buff, float buffTime)
    {
        PassiveSkillItemRender item = GetItem(buff);
        item.cd = buffTime;
        item.frozen = true;
        item.gameObject.TrySetActive(true);
        PassiveSkillData psd = new PassiveSkillData();
        psd.data = item;
        LinkedListNode<PassiveSkillData> node = m_kPassiveSkill.AddFirst(psd);
        ResetList();
        psd.timerId = TimerManager.SetTimeOut(buffTime, () => { RemovePassiveSkill(node); });
    }

    public void RemovePassiveSkill(int id)
    {
        foreach (PassiveSkillData ps in m_kPassiveSkill)
        {
            if (ps.data.skillId == id)
            {
                LinkedListNode<PassiveSkillData> node = m_kPassiveSkill.Find(ps);
                if (node.Value.timerId != -1)
                {
                    TimerManager.RemoveTimeOut(node.Value.timerId);
                    node.Value.timerId = -1;
                }

                RemovePassiveSkill(node);
                return;
            }
        }
    }

    public void RemoveAllPassiveSkill()
    {
        foreach (PassiveSkillData psd in m_kPassiveSkill)
        {
            DeactiveItem(psd.data);
            TimerManager.RemoveTimeOut(psd.timerId);
        }
        m_kPassiveSkill.Clear();
    }

    private void RemovePassiveSkill(LinkedListNode<PassiveSkillData> node)
    {
        DeactiveItem(node.Value.data);
        m_kPassiveSkill.Remove(node);
        ResetList();
    }

    private void ResetList()
    {
        int index = 0;
        foreach(PassiveSkillData psd in m_kPassiveSkill)
        {
            psd.data.transform.localPosition = new Vector3(index++ * ITEM_GAP_SIZE, 0, 0);
        }
    }

    private PassiveSkillItemRender GetItem(TDBuffInfo buff)
    {
        List<PassiveSkillItemRender> listItem = null;
        m_dicItemCache.TryGetValue(buff.ID, out listItem);
        if (listItem == null)
        {
            listItem = new List<PassiveSkillItemRender>();
            m_dicItemCache.Add(buff.ID, listItem);
        }

        PassiveSkillItemRender item;
        int iRearIndex = listItem.Count - 1;
        if (iRearIndex != -1)
        {
            item = listItem[iRearIndex];
            listItem.RemoveAt(iRearIndex);
        }
        else
        {
            item = new GameObject("PassiveSkill_" + buff.ID).AddComponent<PassiveSkillItemRender>();
            item.transform.SetParent(UIManager.GetPanel<PanelBattle>().passiveSkill.transform);
            item.transform.localScale = Vector3.one;
            item.transform.localPosition = Vector3.zero;
            item.gameObject.TrySetActive(false);
            item.SetData(buff);
            m_listAllItem.Add(item);
        }

        return item;
    }

    public void DeactiveItem(PassiveSkillItemRender item)
    {
        if (item == null)
            return;

        item.gameObject.TrySetActive(false);

        List<PassiveSkillItemRender> listItem = null;
        m_dicItemCache.TryGetValue(item.skillId, out listItem);
        if (listItem == null)
        {
            listItem = new List<PassiveSkillItemRender>();
            m_dicItemCache.Add(item.skillId, listItem);
        }
        listItem.Add(item);
    }

    public void Release()
    {
        for (int index = 0; index < m_listAllItem.Count; ++index)
        {
            PassiveSkillItemRender item = m_listAllItem[index];
            GameObject.Destroy(item);
        }
        foreach (PassiveSkillData psd in m_kPassiveSkill)
        {
            TimerManager.RemoveTimeOut(psd.timerId);
        }

        m_dicItemCache.Clear();
        m_listAllItem.Clear();
        m_kPassiveSkill.Clear();
    }
}
