﻿using UnityEngine;
using UnityEngine.UI;

public class ShowKillMsg : MonoBehaviour
{
    UIQueueObj m_killMsgQueue;
    GameObject m_killmsg_item_class;
    GameObject m_rescue_item_class;
    GameObject m_goKillerTip;

    UIThroughTheWall uiThroughTheWall = null;

    void Start()
    {
        m_killMsgQueue = gameObject.AddComponent<UIQueueObj>();
        m_killMsgQueue.m_iLayout = 2;
        m_killMsgQueue.m_maxQueueSize = 4;

        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.AddEventListener<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, OnRescuePlayer);
        GameDispatcher.AddEventListener<string, Color, int>(GameEvent.UI_TIP_INFO, OnSetMsgTips);
    }

    void OnPlayerDie(BasePlayer shooter, BasePlayer dier, toc_fight_actor_die proto)
    {
        if (!QualityManager.enableUIKillList || !GameSetting.enableUIKillList)
            return;

        if (WorldManager.singleton.isSurvivalTypeModeOpen) return;
        //if (WorldManager.singleton.isDotaModeOpen) return;

        if(WorldManager.singleton.isDotaModeOpen && (shooter.isAI || dier.isAI))
            return;

        if (m_killmsg_item_class == null)
        {
            m_killmsg_item_class = ResourceManager.LoadUIRef("UI/Battle/ViewKillInfo");
        }
        var render = m_killMsgQueue.GetGameObjectRender<KillMsgItemRender>(m_killmsg_item_class);
        render.SetData(proto);
        m_killMsgQueue.PushObj(render.gameObject);

        if (dier == MainPlayer.singleton && shooter!=null && shooter!=dier)//killer标记
        {
            if (uiThroughTheWall == null) uiThroughTheWall = UIThroughTheWall.createUI();
            ThroughTheWallProp prop = new ThroughTheWallProp()
            {
                imgProp = new RectTransformProp() { scale = new Vector3(2f, 2f, 1) },
                msg = shooter.ColorName,
                textPos = new Vector2(0, -40),
                textProp = new TextProp() { fontSize = 26, rect = new RectTransformProp() { scale = new Vector3(1.8f, 1.8f, 1) } }
            };
            uiThroughTheWall.play(AtlasName.Battle, "killer", shooter.transform, new Vector3(0, 2.2f, 0f), prop);
            TimerManager.SetTimeOut(3f, ()=> uiThroughTheWall.stop());
        }
    }

    void OnRescuePlayer(toc_fight_actor_rescued data)
    {
        if (WorldManager.singleton.isSurvivalTypeModeOpen) return;
        if (m_rescue_item_class == null)
        {
            m_rescue_item_class = ResourceManager.LoadUIRef("UI/Battle/ViewRescueInfo");
        }
        var render = m_killMsgQueue.GetGameObjectRender<RescueItemRender>(m_rescue_item_class);
        render.SetData(data);
        m_killMsgQueue.PushObj(render.gameObject);
    }

    void OnSetMsgTips( string text,Color textColor,int fontSize )
    {
        m_killMsgQueue.PushTextInfo( text,textColor,fontSize );
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.RemoveEventListener<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, OnRescuePlayer);
        GameDispatcher.RemoveEventListener<string, Color, int>(GameEvent.UI_TIP_INFO, OnSetMsgTips);
    }
}
