﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System.IO;
using System.Threading;
using System.Collections;


class PanelGodLikeShare : BasePanel
{
    Image m_logo;
    Image m_erweima;
    Image m_mvp;
    Image m_ace;

    Text m_playerName;
    Text m_server_name;
    Text m_playerKill;
    Text m_playerDead;
    Text m_roomName;
    Text m_ruleName;

    Button m_captureShot;
    Button m_shareToFriend;
    Button m_godLikeOk;
    Button m_godLikeCancel;
    static Button m_shareToFriendLine;
    static Button m_back;
    protected static int timeOutUid;

    GameObject m_shareBtn;
    GameObject m_godLike;

    private static Vector3 captureLeft;
    private static Vector3 captureRight;

    public PanelGodLikeShare()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelGodLikeShare");
        AddPreLoadRes("UI/Battle/PanelGodLikeShare", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Login", EResType.Atlas);
        AddPreLoadRes("Atlas/WeixinShare", EResType.Atlas);
    }

    public override void Init()
    {
        m_godLike = m_tran.Find("godLike_btn").gameObject;
        m_shareBtn = m_tran.Find("share_btn").gameObject;
        m_logo = m_tran.Find("logo").GetComponent<Image>();
        m_erweima = m_tran.Find("erweima").GetComponent<Image>();
        m_mvp = m_tran.Find("player_data/mvp").GetComponent<Image>();
        m_ace = m_tran.Find("player_data/ace").GetComponent<Image>();
        
        m_playerName = m_tran.Find("player_data/player_name").GetComponent<Text>();
        m_server_name = m_tran.Find("player_data/server_name").GetComponent<Text>();
        m_playerKill = m_tran.Find("player_data/kill/Text").GetComponent<Text>();
        m_playerDead = m_tran.Find("player_data/dead/Text").GetComponent<Text>();
        m_roomName = m_tran.Find("player_data/room_name").GetComponent<Text>();
        m_ruleName = m_tran.Find("player_data/rule_name").GetComponent<Text>();

        m_captureShot = m_tran.Find("share_btn/capture_btn").GetComponent<Button>();
        m_shareToFriend = m_tran.Find("share_btn/share_friend_btn").GetComponent<Button>();
        m_shareToFriendLine = m_tran.Find("share_btn/share_line_btn").GetComponent<Button>();
        m_back = m_tran.Find("share_btn/back_btn").GetComponent<Button>();
        m_godLikeOk = m_tran.Find("godLike_btn/ok_btn").GetComponent<Button>();
        m_godLikeCancel = m_tran.Find("godLike_btn/cancel_btn").GetComponent<Button>();

        captureLeft = m_tran.Find("background/captureLeft").position;
        captureRight = m_tran.Find("background/captureRight").position;

        m_godLike.TrySetActive(true);
        m_erweima.gameObject.TrySetActive(false);
        m_shareBtn.TrySetActive(false);
        m_mvp.gameObject.TrySetActive(false);
        m_ace.gameObject.TrySetActive(false);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_back.gameObject).onPointerClick += onClose;
        UGUIClickHandler.Get(m_shareToFriend.gameObject).onPointerClick += onShareToFriend;
        UGUIClickHandler.Get(m_shareToFriendLine.gameObject).onPointerClick += onShareToFriendLine;
        UGUIClickHandler.Get(m_captureShot.gameObject).onPointerClick += onCaptureShot;
        UGUIClickHandler.Get(m_godLikeOk.gameObject).onPointerClick += onGodLikeOk;
        UGUIClickHandler.Get(m_godLikeCancel.gameObject).onPointerClick += onGodLikeCancel;
    }

    private void onClose(GameObject target, PointerEventData eventData)
    {
        OnHide();
        base.HidePanel();
    }

    private void onGodLikeOk(GameObject target, PointerEventData eventData)
    {
        if(Driver.m_platform == EnumPlatform.android)
        {
            m_back.gameObject.TrySetActive(false);
            m_back.enabled = false;
            Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
            {
                string imagePath = GetCaptureShotPath();
                if (imagePath == null)
                    return;
                SdkManager.m_logFrom = 0;
                if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SAVE_CAPTURE) ||
                !SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
                {
                    UIManager.ShowNoThisFunc();
                    return;
                }
                else
                {
                    SdkManager.MultipleShare("PVP_best", imagePath);
                }
            }));
        }
        else if(Driver.m_platform == EnumPlatform.ios)
        {
            m_godLike.TrySetActive(false);
            m_erweima.gameObject.TrySetActive(true);
            m_shareBtn.TrySetActive(true);
        }
    }

    private void onGodLikeCancel(GameObject target, PointerEventData eventData)
    {
        OnHide();
        base.HidePanel();
    }

    private void onShareToFriend(GameObject target, PointerEventData eventData)
    {
        Logger.Log("shareToFriend---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 0;
            if (Driver.m_platform == EnumPlatform.ios)
            {
                SdkManager.WeixinShare("1", imagePath);//1 friend 2 friend line	
                //SdkManager.SaveCapture (imagePath);			
            }
            else
            {
                PanelWeixinShare.handleShareToWeixin("wechat_friends", imagePath);
            }
        }));
    }

    private void onShareToFriendLine(GameObject target, PointerEventData eventData)
    {
        Logger.Log("shareToFriendLine---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            SdkManager.m_logFrom = 0;
            if (Driver.m_platform == EnumPlatform.ios)
            {
                SdkManager.WeixinShare("2", imagePath);//1 friend 2 friend line
                //SdkManager.SaveCapture (imagePath);
            }
            else
            {
                PanelWeixinShare.handleShareToWeixin("wechat_moments", imagePath);
            }
        }));
    }

    private void onCaptureShot(GameObject target, PointerEventData eventData)
    {
        Logger.Log("onCaptureShot---");
        m_back.gameObject.TrySetActive(false);
        m_back.enabled = false;
        Driver.singleton.StartCoroutine(CaptureCoroutine(() =>
        {
            string imagePath = GetCaptureShotPath();
            if (imagePath == null)
                return;
            if (!SdkManager.IsSupportFunction(SdkFuncConst.GET_SAVE_CAPTURE) ||
                !SdkManager.IsSupportFunction(SdkFuncConst.GET_SHARE))
            {
                UIManager.ShowNoThisFunc();
                return;
            }
            SdkManager.SaveCapture(imagePath);
        }));
    }

    IEnumerator CaptureCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(1f);
        action();
    }

    public string GetCaptureShotPath()
    {
        Vector2 left = RectTransformUtility.WorldToScreenPoint(UIManager.m_uiCamera, captureLeft);
        Vector2 right = RectTransformUtility.WorldToScreenPoint(UIManager.m_uiCamera, captureRight);
        string path = null;
        path = PanelWeixinShare.DoGetCapture((float)left.x, (float)right.y, Mathf.Abs(right.x - left.x), Mathf.Abs(right.y - left.y));
        timeOutUid = TimerManager.SetTimeOut(1.0f, () =>
        {
            m_back.gameObject.TrySetActive(true);
            m_back.enabled = true;
        });
        return path;
    }

    public override void OnShow()
    {
        var logoName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.LOGO);
        var qrName = ConfigManager.GetConfig<ConfigPlatformContent>().GetContent(ConfigPlatformContent.GAME_SHARE_QRCODE);
        m_logo.enabled = !string.IsNullOrEmpty(logoName);
        m_erweima.enabled = !string.IsNullOrEmpty(qrName);

        m_logo.SetSprite(ResourceManager.LoadSprite(AtlasName.Login, logoName));
        m_erweima.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, qrName));

        m_server_name.text = PlayerSystem.GetRoleSeverName(PlayerSystem.roleId);
        m_playerName.text = PlayerSystem.roleData.name;
        m_ruleName.text = ConfigManager.GetConfig<ConfigGameRule>().GetLine(RoomModel.LastRoomInfo.rule).Mode_Name;
        m_roomName.text = ConfigManager.GetConfig<ConfigMapList>().GetLine(RoomModel.LastRoomInfo.map.ToString()).MapName;
        if(PanelSettle.self_share != null)
        {
            m_playerKill.text = PanelSettle.self_share.kill.ToString();
            m_playerDead.text = PanelSettle.self_share.death.ToString();
            if (PanelSettle.self_share.mvp)
            {
                m_mvp.gameObject.TrySetActive(true);
                m_mvp.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "mvp"));
                m_mvp.SetNativeSize();
            }
            if (PanelSettle.self_share.gold_ace)
            {
                m_ace.gameObject.TrySetActive(true);
                m_ace.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "gold_ace"));
                m_ace.SetNativeSize();
            }
            else if (PanelSettle.self_share.silver_ace)
            {
                m_ace.gameObject.TrySetActive(true);
                m_ace.SetSprite(ResourceManager.LoadSprite(AtlasName.WeiXinShare, "silver_ace"));
                m_ace.SetNativeSize();
            }
        }       
    }

    public override void OnHide()
    {
        //settleWeixinData = null;
        if (timeOutUid > 0)
            TimerManager.RemoveTimeOut(timeOutUid);        
    }

    public override void OnBack()
    {
        HidePanel();
    }

    public override void OnDestroy()
    {
        if (timeOutUid > 0)
            TimerManager.RemoveTimeOut(timeOutUid);
        PanelSettle.self_share = null;
    }

    public override void Update()
    {
        if (Time.realtimeSinceStartup - PanelSettle.m_panelStartTime > 60)
        {
            PanelSettle.m_panelStartTime = float.MaxValue;
            if (RoomModel.IsInRoom)
            {
                HidePanel();
            }
        }
    } 
}

