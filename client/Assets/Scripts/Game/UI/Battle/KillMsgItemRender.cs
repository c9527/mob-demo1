﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class KillMsgItemRender : ItemRender
{
    UIImgNumText _kill_txt;
    Image _kill_bg;
    Image _kill_lbl;
    Image _kill_icon;
    Image _weapon_img;
    Text _shooter_txt;
    Text _dier_txt;
    Image _sub_weapon_img;

    public override void Awake()
    {
        _kill_txt = new UIImgNumText(transform.Find("kill"), "kill", AtlasName.Battle, 0, -8);
        _kill_txt.transform.localPosition = new Vector3(-60, -1, 0);
        _kill_bg = transform.Find("kill/BG").GetComponent<Image>();
        _kill_lbl = transform.Find("kill/Label").GetComponent<Image>();
        _kill_icon = transform.Find("Icon").GetComponent<Image>();
        _weapon_img = transform.Find("gun/imgWeapon").GetComponent<Image>();
        _shooter_txt = transform.Find("leftname").GetComponent<Text>();
        _dier_txt = transform.Find("rightname").GetComponent<Text>();
        _sub_weapon_img = transform.Find("subgun").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        toc_fight_actor_die sdata = data as toc_fight_actor_die;
        if (sdata == null)
            return;

        BasePlayer killer = WorldManager.singleton.GetPlayerById(sdata.killer);
        BasePlayer target = WorldManager.singleton.GetPlayerById(sdata.actor_id);
        if (target == null)
            return;

        var icon_str = "";
        if (killer != null && killer.Camp==(int)WorldManager.CAMP_DEFINE.ZOMBIE)
        {
            _kill_txt.setStyle("infect", AtlasName.Battle, 0, -8,new Vector2(-60,-1));
            _kill_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "infectBlood"));
            _kill_lbl.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "infect_txt"));
            icon_str = "infect";
        }
        else
        {
            _kill_txt.setStyle("kill", AtlasName.Battle, 0, -8, new Vector2(-60, -1));
            _kill_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.BattleNew, "killBlood"));
            _kill_lbl.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "kill"));
            if (sdata.across_wall)
            {
                icon_str = "across_wall";
            }
            else if(sdata.part==BasePlayer.BODY_PART_ID_HEAD)
            {
                icon_str = "shothead";
            }
        }

        if (!string.IsNullOrEmpty(icon_str))
        {
            _kill_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, icon_str));
            _kill_icon.gameObject.TrySetActive(true);
        }
        else
        {
            _kill_icon.SetSprite(null);
            _kill_icon.gameObject.TrySetActive(false);
        }

        _kill_txt.value = sdata.multi_kill;
        _weapon_img.SetSprite(ResourceManager.LoadWeaponIcon(sdata.weapon));
        _sub_weapon_img.enabled = false;
        ConfigItemWeaponLine weapon = ItemDataManager.GetItemWeapon(sdata.weapon);
        if (weapon != null && weapon.SubWeapon > 0 && WeaponAtkTypeUtil.IsSubWeaponType(sdata.atk_type))
        {
            weapon = ItemDataManager.GetItemWeapon(weapon.SubWeapon);
            if (weapon != null && !string.IsNullOrEmpty(weapon.KillIcon))
            {
                _sub_weapon_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, weapon.KillIcon));
                _sub_weapon_img.enabled = true;
            }
        }
        _shooter_txt.text = killer != null ? killer.ColorName : weapon.DispName;
        _dier_txt.text = target != null ? target.ColorName : "???";
    }
}
