﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class  SettlePlayerItemRender : ItemRender
{
    static public int MaxAdd;
    static public int MaxSub;

    public SDSettlePlayer _data;
    Image _bg_vipImg;
    Image _avatar_img;
    Image _lvl_img;
    Text _name_txt;
    Text _kill_txt;
    Text _death_txt;
    Text _score_txt;
    Image _mvp_img;
    Image _ace_img;
    Image _headVip;
    Image m_titleImg;
    Button _addfriend_btn;
    Button _report_btn;
    UIEffect _headEffect;
    private DataGrid m_itemList;
    private DataGrid m_titles;


    public class TitleIcon : ItemRender
    {
        Image m_icon;
        public override void Awake()
        {
            m_icon = transform.Find("").GetComponent<Image>();
        }

        protected override void OnSetData(object data)
        {
            var info = data as p_title_result;
            m_icon.SetSprite(ResourceManager.LoadSprite("PlayerInfo", info.title));
        }
    }

    public override void Awake()
    {
        Transform tran = null;
        _avatar_img = transform.Find("avatar_img").GetComponent<Image>();
        _headVip = transform.Find("headVip").GetComponent<Image>();
        
        _bg_vipImg = transform.GetComponent<Image>();

        _lvl_img = transform.Find("lvl_img").GetComponent<Image>();
        _name_txt = transform.Find("name_txt").GetComponent<Text>();
        tran = transform.Find("kill_txt");
        if (tran != null)
            _kill_txt = tran.GetComponent<Text>();
        tran = transform.Find("death_txt");
        if (tran != null)
            _death_txt = tran.GetComponent<Text>();
        tran = transform.Find("score_txt");
        if (tran != null)
            _score_txt = tran.GetComponent<Text>();
        //_mvp_img = transform.Find("mvp_img").GetComponent<Image>();
        //_ace_img = transform.Find("ace_img").GetComponent<Image>();
        _addfriend_btn = transform.Find("addfriend_btn").GetComponent<Button>();
        _report_btn = transform.Find("report_btn").GetComponent<Button>();        

        tran = transform.Find("weapon/content/item");
        if (tran != null)
        {
            tran.gameObject.TrySetActive(false);
            m_itemList = tran.transform.parent.gameObject.AddComponent<DataGrid>();
            m_itemList.SetItemRender(tran.gameObject, typeof(RewarditemRender));
        }
        tran = transform.Find("career/content");
        
        if (tran != null)
        {
            m_titles = transform.Find("career/content").gameObject.AddComponent<DataGrid>();
            m_titles.SetItemRender(transform.Find("career/content/careerIcon").gameObject, typeof(TitleIcon));
            m_titles.Data = new object[0];
        }
        m_titleImg = transform.Find("titleIcon") == null ? null : transform.Find("titleIcon").GetComponent<Image>();
        if (m_titleImg != null)
        {
            m_titleImg.enabled = false;
        }
        
        UGUIClickHandler.Get(_addfriend_btn.gameObject).onPointerClick += onAddFriendBtnClick;
        UGUIClickHandler.Get(_report_btn.gameObject).onPointerClick += onReportBtnClick;
    }

    private void onReportBtnClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        UIManager.PopPanel<PanelReport>(new object[] { _data.name, _data.uid }, true);
    }

    private void onAddFriendBtnClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (_data.uid == 0)
        {
            TipsManager.Instance.showTips("好友申请发送成功");
            return;
        }
        if (_data != null && _data.uid > 0)
        {
            PlayerFriendData.AddFriend(_data.uid);
        }
    }

    protected override void OnSetData(object data)
    {

        _data = data as SDSettlePlayer;
        _avatar_img.enabled = _data != null;
        _lvl_img.enabled = _data != null;
        _name_txt.enabled = _data != null;
        _kill_txt.enabled = _data != null;
        if (m_titleImg != null)
        {
            m_titleImg.enabled = _data != null;
        }
        
        if (m_titles != null)
        {
            m_titles.gameObject.TrySetActive(_data != null);
        }
        if (_headVip != null)
        {
            _headVip.gameObject.TrySetActive(_data != null);
        }
        if (_bg_vipImg != null)
        {
            _bg_vipImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
        }
        if (_death_txt != null)
            _death_txt.enabled = _data != null;
        if (_score_txt != null)
            _score_txt.enabled = _data != null;
        //_mvp_img.enabled = _data != null;
        //_ace_img.enabled = _data != null;
        _addfriend_btn.gameObject.TrySetActive(false);
        _report_btn.gameObject.TrySetActive(false);
        if (m_itemList != null)
        {
            m_itemList.gameObject.TrySetActive(false);
        }
        
        if (_data == null)
        {
            return;
        }
      
        
        bool myself = _data.uid == PlayerSystem.roleId;
        _avatar_img.SetSprite(ResourceManager.LoadRoleIcon(_data.icon, _data.camp));
        _avatar_img.SetNativeSize();
        int level = myself ? PlayerSystem.roleData.level : _data.level;
        _lvl_img.SetSprite(ResourceManager.LoadArmyIcon(level));
        _lvl_img.SetNativeSize();
        string color = myself ? "#86fd83" : _data.camp == 1 ? "#ff9900" : "#43d8f7";
        _name_txt.text = string.Format("<color='{0}'>{1}</color>", color, PlayerSystem.GetRoleNameFull(_data.name, _data.uid));

        if (PanelSettle.scene_type == "worldboss")
            //_kill_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.score);
        {
            _kill_txt.text = _data.score.ToString();
        }
        else
        {
            if (_score_txt == null)
            {
                if (PanelSettle.scene_type == "boss4newer")
                {
                    _kill_txt.text = _data.score.ToString();
                }
                else
                {
                    int temp = 0;
                    temp = WorldManager.singleton.isChallengeModeOpen ? _data.score / 100 : _data.kill;
                    _kill_txt.text = string.Format("{0}/{1}", temp, _data.death);
                }
            }
            else
            {
                _kill_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.kill);
            }
        }
        if (_death_txt != null)
            _death_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.death);
        if (PanelSettle.scene_type == "match")
        {
            if (!PanelSettle.abnormal_finish && _data.game_time * 5 >= PanelSettle.game_time&&_data.game_time>=0.98f*300*0.2f)
            {
                var add_honor = _data.rank_score;
                //if (add_honor > 0)
                //{
                //    if (MaxAdd > 0)
                //    {
                //        add_honor = Mathf.Min(add_honor, MaxAdd);
                //    }
                //}
                //else if (add_honor < 0)
                //{
                //    if (MaxSub > 0)
                //    {
                //        add_honor = Mathf.Max(add_honor, -MaxSub);
                //    }
                //}
                _score_txt.text = string.Format("<color='{0}'>{1}</color><color='{3}'>({2})</color>", color, _data.honor + add_honor, (add_honor > 0 ? "+" : "") + add_honor, add_honor > 0 ? "#34e87c" : "#ff0000");
            }
            else
            {
                if (_score_txt != null)
                    _score_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.honor);
            }
        }
        else if (PanelSettle.scene_type == "survival" || PanelSettle.scene_type == "worldboss")
        {
            //_score_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.score);
            if (_score_txt != null)
                _score_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.rescue_times + _data.revive_times);
        }
        else
        {
            if (_score_txt != null)
                _score_txt.text = string.Format("<color='{0}'>{1}</color>", color, _data.score);
        }
        
        //_mvp_img.enabled = _data.mvp;
        //_ace_img.enabled = _data.silver_ace || _data.gold_ace;
        //if (_ace_img.enabled)
        //{
        //    _ace_img.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, _data.gold_ace ? "goldace" : "silverace"));
        //}
        //_addfriend_btn.gameObject.TrySetActive(_data.uid != 0 && _data.uid != PlayerSystem.roleId && !PlayerFriendData.singleton.isFriend(_data.uid));
        if (RoomModel.LastRoomInfo != null)
        {
            var gamerule = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.LastRoomInfo.channel);
            if (gamerule.Type != ChannelTypeEnum.custom && gamerule.Type != ChannelTypeEnum.stage)
                _addfriend_btn.gameObject.TrySetActive(_data.uid != PlayerSystem.roleId && !PlayerFriendData.singleton.isFriend(_data.uid));
            else
                _addfriend_btn.gameObject.TrySetActive(_data.uid != 0 && _data.uid != PlayerSystem.roleId && !PlayerFriendData.singleton.isFriend(_data.uid));
        }

        if (PanelSettle.scene_type != "survival"||PanelSettle.scene_type!="worldboss")
            _report_btn.gameObject.TrySetActive(_data.uid != PlayerSystem.roleId);
        if (PanelSettle.scene_type == "survival" || PanelSettle.scene_type == "worldboss")
        {
            _report_btn.gameObject.TrySetActive(false);
        }
        if (_bg_vipImg != null)
            if (_data.mType == MemberManager.MEMBER_TYPE.MEMBER_NORMAL)
            {
                _bg_vipImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Settle, "bg_vip"));
            }
            else if (_data.mType == MemberManager.MEMBER_TYPE.MEMBER_HIGH)
            {
                _bg_vipImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Settle, "bg_vip_super"));
            }
            else
            {
                //段位积分显示
                ConfigChannelLine channelCfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(3);
                TDScoreTitle stCfg = ConfigManager.GetConfig<ConfigScoreTitle>().getScoreTitleInfo(_data.honor);
                if (_data.level >= channelCfg.LevelMin && stCfg != null && stCfg.BatteSettleBg != "")
                {
                   _bg_vipImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Settle, stCfg.BatteSettleBg));
                }else
                    _bg_vipImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "list_bg1"));
            }
        if (m_itemList != null)
        {
            m_itemList.Data = _data.itemList;
            m_itemList.gameObject.TrySetActive(true);
        }
        if (_data != null)
        {
            if (_data.titles != null)
            {
                if (_data.titles.Count == 0)
                {
                    _name_txt.rectTransform.anchoredPosition = new Vector3(_name_txt.transform.localPosition.x, 0f, 0f);
                    if (m_titles != null)
                    {
                        m_titles.Data = new object[0];
                    }
                }
                else
                {
                    if (m_titles != null)
                    {
                        m_titles.Data = _data.titles.ToArray();
                    }
                    //_name_txt.transform.SetLocation(_name_txt.transform.parent, _name_txt.transform.localPosition.x, 10f, 0f);
                    _name_txt.rectTransform.anchoredPosition = new Vector3(_name_txt.transform.localPosition.x, 10f, 0f);
                }
            }
            else
            {
                //_name_txt.transform.SetLocation(_name_txt.transform.parent, _name_txt.transform.localPosition.x, 0f, 0f);
                _name_txt.rectTransform.anchoredPosition = new Vector3(_name_txt.transform.localPosition.x, 0f, 0f);
                if (m_titles != null)
                {
                    m_titles.Data = new object[0];
                }
            }
        }
        
        _headVip.gameObject.TrySetActive(false);
        if (_data.heroType != (int)EnumHeroCardType.none)
        {
            _headVip.gameObject.TrySetActive(true);
            if (_data.heroType == (int)EnumHeroCardType.normal)
            {
                _headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_NORMAL));
            }
            else if (_data.heroType == (int)EnumHeroCardType.super)
            {
                _headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_SUPER));
            }
            else if (_data.heroType == (int)EnumHeroCardType.legend)
            {
                _headVip.SetSprite(ResourceManager.LoadSprite(AtlasName.HeroCard, GameConst.HEAD_LEGEND));
                if (_headEffect == null)
                {
                    _headEffect = UIEffect.ShowEffect(_headVip.transform, EffectConst.UI_HEAD_HERO_CARD);
                }
            }
        }

        //装备的称号
        if (m_titleImg != null)
        {
            if (_data.playerTitleId > 0)
            {
                m_titleImg.enabled = true;
                var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
                ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(_data.playerTitleId);
                if (lineChenghao != null)
                {
                    m_titleImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
                    m_titleImg.SetNativeSize();
                }
            }
            else
            {
                m_titleImg.enabled = false;
            }
        }
    }

    int calculateAddHonor(int honor, int avg_honor, bool is_win)
    {
        var result = 0;
        var sa = is_win ? 1 : 0;
        var ea = 1.0 / (1 + Mathf.Pow(10, (avg_honor - honor) / 400f));
        var k = 32.0;
        if (honor > 2400)
        {
            k = 8;
        }
        else if (honor > 2000)
        {
            k = 152 - 0.06 * avg_honor;
        }
        result = (int)Mathf.Ceil((float)(k * (sa - ea)));
        return result;
    }

    public Vector3 reportBtnPos
    {
        get { return _report_btn != null ? _report_btn.transform.localPosition : Vector3.zero; }
    }

    public Vector3 addfriendBtnPos
    {
        get { return _addfriend_btn != null ? _addfriend_btn.transform.localPosition : Vector3.zero; }
    }
}
