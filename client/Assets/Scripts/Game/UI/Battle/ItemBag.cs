﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


public class ItemBag : ItemRender
{
    private Text m_num;
    private Image m_weapon;
    public int m_bagid;
    private Image m_imgDurability;
    private GameObject m_goDurability;
    private Image m_imgBroken;

    public override void Awake()
    {
        var tran = transform;
        m_num = tran.Find("num").GetComponent<Text>();
        m_weapon = tran.Find("Icon").GetComponent<Image>();
        m_imgDurability = tran.Find("imgDurability/value").GetComponent<Image>();
        m_goDurability = tran.Find("imgDurability").gameObject;
        m_imgBroken = tran.Find("imgDurability/imgBroken").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var bag = data as p_bag;
        m_num.text = bag.group_id.ToString();
        m_bagid = bag.group_id;
        ConfigItemWeapon KCW = ConfigManager.GetConfig<ConfigItemWeapon>();
        string type = WorldManager.singleton.ModeWeaponTYPE;

        var itemId = 0;
        if (type == "")
        {
            itemId = bag.equip_list.WEAPON1 != 0 ? bag.equip_list.WEAPON1 : bag.equip_list.WEAPON2;
        }
        else
        {
            if (type != GameConst.WEAPON_SUBTYPE_DAGGER && type != GameConst.WEAPON_SUBTYPE_GRENADE && type != "PISTOL")
            {
                if (bag.equip_list.WEAPON1 != 0)
                    itemId = KCW.GetLine(bag.equip_list.WEAPON1).WeaponType == type ? bag.equip_list.WEAPON1 : bag.equip_list.DAGGER;
                else
                    itemId = bag.equip_list.DAGGER;
            }
            else
            {
                if (type == GameConst.WEAPON_SUBTYPE_DAGGER)
                    itemId = bag.equip_list.DAGGER;
                else if (type == GameConst.WEAPON_SUBTYPE_GRENADE)
                    itemId = bag.equip_list.GRENADE != 0 ? bag.equip_list.GRENADE : bag.equip_list.DAGGER;
                else if (type == "PISTOL")
                    itemId = bag.equip_list.WEAPON2;
            }
        }

        var item = KCW.GetLine(itemId);
        var pitem = ItemDataManager.GetDepotItem(itemId);
        m_weapon.SetSprite(ResourceManager.LoadSprite("Icon", item.Icon));
        m_weapon.SetNativeSize();
        m_goDurability.TrySetActive(itemId != 0 && (item.SubType == "WEAPON1" || item.SubType == "WEAPON2"));
        m_imgDurability.fillAmount = 1f*pitem.durability/ItemDataManager.MAX_DURABILITY;
        m_imgBroken.gameObject.TrySetActive(pitem.durability == 0);
    }
}