﻿using System.Collections.Generic;
using UnityEngine;

class BattleTeamHeroInfoPanel : MonoBehaviour
{
    public int m_iLeftNum;
    public int m_iRightNum;
    //private int m_iLeftDeadTimes = 0;
    //private int m_iRightDeadTimes = 0;

    private HeadList m_rightHeadList;
    private HeadList m_leftHeadList;

    private UIImgNumText m_kRightKillCount;
    private UIImgNumText m_kLeftKillCount;
    private UIImgNumText m_kRightRoundCount;
    private UIImgNumText m_kLeftRoundCount;
    private TextDownWatch m_kDownTimer;

    UIImgNumText m_kWinParam;

    Transform m_leftHeadTrans;
    Transform m_rightHeadTrans;

    ConfigGameRule m_configGameRule;
    ConfigMapList m_configMapList;

    float m_fLockTime = 0;

    void Awake()
    {
        if (m_leftHeadList == null)
        {
            m_leftHeadList = new HeadList();
        }

        if (m_rightHeadList == null)
        {
            m_rightHeadList = new HeadList();
        }

        m_configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        m_configMapList = ConfigManager.GetConfig<ConfigMapList>();
    }
    
    void Start()
    {

        Transform tempTrans = transform.Find("Content");

        m_kRightKillCount = new UIImgNumText(tempTrans.Find("rightKillCount"), "time", AtlasName.Battle, 0, 3);
        m_kLeftKillCount = new UIImgNumText(tempTrans.Find("leftKillCount"), "time", AtlasName.Battle, 0, 3);

        m_kRightRoundCount = new UIImgNumText(tempTrans.Find("rightRoundCount"), "b_", AtlasName.Nums, 0, 3);
        m_kLeftRoundCount = new UIImgNumText(tempTrans.Find("leftRoundCount"), "b_", AtlasName.Nums, 0, 3);

        m_kDownTimer = tempTrans.Find("time").gameObject.AddMissingComponent<TextDownWatch>();

        m_leftHeadTrans = tempTrans.Find("leftHead");
        m_rightHeadTrans = tempTrans.Find("rightHead");

        m_kWinParam = new UIImgNumText(tempTrans.Find("roundCount"), "mode", AtlasName.Battle, 0, -8);

        OnBattlingStaticInfoBack();
        OnUpdateBattleState(PlayerBattleModel.Instance.battle_state);

        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);
    }

    private float m_tick;
    void Update()
    {
        if (m_fLockTime > 0)
        {
            m_fLockTime -= Time.deltaTime;
        }
        m_tick += Time.deltaTime;
        if (m_tick >= 1)
        {
            m_tick = 0;
            OnBattlingStaticInfoBack();
        }
    }

    public void Init()
    {
        //m_iLeftDeadTimes = 0;
        //m_iRightDeadTimes = 0;
    }


    public int leftNum
    {
        set
        {
            int oldNum = m_iLeftNum;
            m_iLeftNum = value;
            if (oldNum != m_iLeftNum)
            {
                ShowLeftList(m_iLeftNum);
            }
        }
        get
        {
            return m_iLeftNum;
        }
    }

    public int rightNum
    {
        set
        {
            int oldNum = m_iRightNum;
            m_iRightNum = value;

            if (oldNum != m_iRightNum)
            {
                ShowRightList(m_iRightNum);
            }
        }
        get
        {
            return m_iRightNum;
        }
    }

    public void SetLeftWinCount(int count)
    {
        //m_iLeftDeadTimes = count;
        m_kLeftRoundCount.value = count;
    }

    public void SetRightWinCount(int count)
    {
        //m_iRightDeadTimes = count;
        m_kRightRoundCount.value = count;
    }

    public void SetLeftKillCount(int count)
    {
        //m_iLeftDeadTimes = count;
        m_kLeftKillCount.value = count;
    }

    public void SetRightKillCount(int count)
    {
        //m_iRightDeadTimes = count;
        m_kRightKillCount.value = count;
    }

    private void ShowLeftList(int num)
    {
        Sprite leftSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "blue_point_2", (Sprite sprite) =>
        {
            leftSprite = sprite;
        });

        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });
        m_leftHeadList.Init(m_rightHeadTrans, leftSprite, deadSprite, num, true);
    }

    private void ShowRightList(int num)
    {
        Sprite rightSprite = null;

        ResourceManager.LoadSprite(AtlasName.BattleNew, "yellow_point", (Sprite sprite) =>
        {
            rightSprite = sprite;
        });

        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });
        m_rightHeadList.Init(m_leftHeadTrans, rightSprite, deadSprite, num, false);
    }


    void OnUpdateBattleState(SBattleState data)
    {
        if (WorldManager.singleton.isFightInTurnModeOpen) return;
        var rule = m_configGameRule.GetLine(WorldManager.singleton.gameRule);
        var map = m_configMapList.GetLine(SceneManager.singleton.curMapId.ToString());
        var run_time = PlayerBattleModel.Instance.battle_info.round_time;
        if (data.state == 1)
        {
            m_kDownTimer.SetTime = 0;
            m_fLockTime = rule.GodTime - run_time;
            m_leftHeadList.ResetHeadNum();
            m_rightHeadList.ResetHeadNum();
        }
        else if (data.state == 2)
        {
            m_kDownTimer.SetTime = rule.Time_play - run_time;
            m_fLockTime = rule.GodTime - run_time;
        }
        else if (data.state == 3)
        {
            m_kDownTimer.SetTime = -1;
        }
        else
        {
            m_kDownTimer.SetTime = 0;
        }
    }

    private void OnBattlingStaticInfoBack()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        string gameRule = WorldManager.singleton.gameRule;
        if (data == null || string.IsNullOrEmpty(gameRule))
        {
            return;
        }
        List<SBattleCamp> camps = data.camps;
        int iRightPlayers = 0;
        int iLeftPlayers = 0;
        int liveRightNum = 0;
        int liveLeftNum = 0;
        SBattlePlayer self_data = null;
        SBattlePlayer top_data = null; ;

        for (int i = 0; i < camps.Count; i++)
        {
            SBattleCamp camp = camps[i];

            for (int j = 0; j < camp.players.Count; j++)
            {
                SBattlePlayer splayer = camp.players[j];

                if (splayer.actor_id == WorldManager.singleton.myid)
                {
                    self_data = splayer;
                }
                if (top_data == null || splayer.round_kill > top_data.round_kill)
                {
                    top_data = splayer;
                }
                if (camp.camp == (int)WorldManager.CAMP_DEFINE.UNION)
                {
                    ++iRightPlayers;
                    if (splayer.alive)
                    {
                        ++liveRightNum;
                    }
                }
                else if (camp.camp == (int)WorldManager.CAMP_DEFINE.REBEL)
                {
                    ++iLeftPlayers;
                    if (splayer.alive)
                    {
                        ++liveLeftNum;
                    }
                }
            }
        }

        leftNum = iLeftPlayers;
        rightNum = iRightPlayers;
        m_rightHeadList.LiveAndDeadNum(liveRightNum, iRightPlayers - liveRightNum);
        m_leftHeadList.LiveAndDeadNum(liveLeftNum, iLeftPlayers - liveLeftNum);
        int leftCount = 0;
        int rightCount = 0;
        switch (WorldManager.singleton.gameRuleLine.WinType)
        {
            case "playerkill":
                if (self_data != null)
                    leftCount = self_data.round_kill;
                if (top_data != null)
                    rightCount = top_data.round_kill;
                break;
            case "campkill":
                leftCount = camps.Count < 1 ? 0 : camps[0].round_kill;
                rightCount = camps.Count < 2 ? 0 : camps[1].round_kill;
                break;
            case "explode":
            case "killall":
            case "zombie":
            case "zombie_hero":
            case "ghost":
            case "hide":
            case "arena":
            case "big_head":
            case "team_hero":
                leftCount = camps.Count < 1 ? 0 : camps[0].win;
                rightCount = camps.Count < 2 ? 0 : camps[1].win;
                break;
        }
        if (data.win_param > 0)
        {
            m_kWinParam.value = data.win_param;
        }
        else if (WorldManager.singleton.gameRuleLine.WinRound > 0)
        {
            m_kWinParam.value = WorldManager.singleton.gameRuleLine.WinRound;
        }
        m_kWinParam.gameObject.transform.localPosition = new Vector3(0f, 0f);
        SetLeftWinCount(leftCount);
        SetRightWinCount(rightCount);

        if(WorldManager.singleton.gameRuleLine.WinType == "team_hero")
        {
            leftCount = camps.Count < 1 ? 0 : camps[0].round_kill;
            rightCount = camps.Count < 2 ? 0 : camps[1].round_kill;
            SetLeftKillCount(leftCount);
            SetRightKillCount(rightCount);
        }
    }

    private void OnSwitchPlayer(int playerId)
    {
        m_tick = 0;
        OnBattlingStaticInfoBack();
    }

    public void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, OnSwitchPlayer);
    }

}

