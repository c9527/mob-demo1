﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MissionRewardItemRender : ItemRender {
    Image m_image;
    ItemInfo m_itemInfo;    
    Text m_num;
    Text m_name;
    public override void Awake()
    {
        m_image = transform.Find("Image").GetComponent<Image>();        
        m_num = transform.Find("Num").GetComponent<Text>();
        m_name = transform.Find("Name").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_itemInfo = data as ItemInfo;
        var config = ItemDataManager.GetItem(m_itemInfo.ID);
        m_image.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, config.Icon));
        m_image.SetNativeSize();
        m_num.text = string.Format("x{0}", m_itemInfo.cnt);
        m_name.text = config.DispName;
    }
    
}
