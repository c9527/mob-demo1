﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelMissionComplete : BasePanel {

    DataGrid m_itemGroup;
    GameObject m_rewardItem0;
    UIEffect m_effect;
    ItemInfo[] m_itemList;
	public PanelMissionComplete()
    {
        SetPanelPrefabPath("UI/Mission/PanelMissionComplete");
        AddPreLoadRes("UI/Mission/PanelMissionComplete",EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
    }

    public override void Init()
    {
        m_rewardItem0 = m_tran.Find("Icon/itemGroup/reward_item_0").gameObject;
        m_rewardItem0.TrySetActive(false);
        m_itemGroup = m_rewardItem0.transform.parent.gameObject.AddComponent<DataGrid>();
        m_itemGroup.SetItemRender(m_rewardItem0, typeof(MissionRewardItemRender));
        m_effect = UIEffect.ShowEffectBefore(m_tran.Find("Icon"), "UI_renwuwancheng_01",true);
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Icon/sure")).onPointerClick += OnSureClick;
    }

    private void OnSureClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }
    

    public override void Update()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        if(!HasParams())
        {
            HidePanel();
            return;
        }
        m_itemList=GetItems( (ItemInfo[])m_params);
        m_effect.Play();
        m_itemGroup.Data = m_itemList;
        AudioManager.PlayUISound(AudioConst.award);
    }

    public static ItemInfo[] GetItems(ItemInfo[] items)
    {
        List<ItemInfo> list = new List<ItemInfo>();        
        ConfigItemLine config;
        for(int i=0;i<items.Length;i++)
        {
            config=ItemDataManager.GetItem(items[i].ID);            
            if(config.Type==GameConst.ITEM_TYPE_CORPS)
            {
                if (CorpsDataManager.IsJoinCorps())
                    list.Add(items[i]);
            }
            else
            {
                list.Add(items[i]);
            }
        }
        return list.ToArray();
    }
}
