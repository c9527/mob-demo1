﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PanelMissionEntry : BasePanel
{
    Toggle m_toggleDailyMission;
    Toggle m_toggleAchieve;
    GameObject m_goMissionBubble;
    GameObject m_goAchieveBubble;
    public PanelMissionEntry()
    {
        SetPanelPrefabPath("UI/Mission/PanelMissionAndAchieve");
        AddPreLoadRes("UI/Mission/PanelMissionAndAchieve", EResType.UI);

        AddPreLoadRes("Atlas/Hall", EResType.Atlas);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    public override void Init()
    {
        m_toggleDailyMission = m_tran.Find("frame/dailymission").GetComponent<Toggle>();
        m_toggleAchieve = m_tran.Find("frame/achieve").GetComponent<Toggle>();
        m_goMissionBubble = m_tran.Find("frame/dailymission/bubble_tip").gameObject;
        m_goAchieveBubble = m_tran.Find("frame/achieve/bubble_tip").gameObject;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/dailymission")).onPointerClick += OnClickShowDailyMission;
        UGUIClickHandler.Get(m_tran.Find("frame/achieve")).onPointerClick += OnClickShowAchieve;
        //UGUIClickHandler.Get(m_tran.Find("achieve"));
        AddEventListener(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED, OnUpdateNotice);
    }
    public override void OnShow()
    {
        var hall = UIManager.GetPanel<PanelHall>();
        if (hall != null && hall.IsOpen())
        {
            hall.CheckChosen(hall.m_btgMission);
        } 
        if(HasParams())
        {
            string option = (string)m_params[0];
            if (option == "mission")
            {
                m_toggleDailyMission.isOn = true;
            }
        }
        if (m_toggleAchieve.isOn)
        {
            ShowDailyMission(false);
            ShowAchieve(true);
        }
        else //( m_toggleDailyMission.isOn )
        {
            ShowDailyMission(true);
            ShowAchieve(false);
        }
        OnUpdateNotice();
        if (UIManager.GetPanel<PanelPvpRoomList>() != null)
        {
            UIManager.GetPanel<PanelPvpRoomList>().m_filterRule = "";
        }
    }
    public override void OnHide()
    {
        var panelMission = UIManager.GetPanel<PanelDailyMission>();
        var panelAchievemnet = UIManager.GetPanel<PanelAchievement>();
        if (panelMission != null)
            panelMission.HidePanel();
        if (panelAchievemnet != null)
            panelAchievemnet.HidePanel();
    }
    public override void OnBack()
    {
        UIManager.ShowPanel<PanelHallBattle>();
    }
    public override void OnDestroy()
    {
    }
    public override void Update()
    {

    }

    void OnClickShowDailyMission( GameObject target, PointerEventData eventData )
    {
        ShowDailyMission(true);
        ShowAchieve(false);
    }

    void OnClickShowAchieve( GameObject target, PointerEventData eventData )
    {
        //PlayerAchieveData.SendAchieveMsg("achievement_data");
        //UIManager.ShowTipPanel("系统暂未开放");
        //m_toggleAchieve.isOn = false;
        //m_toggleDailyMission.isOn = true;
        //return;

        ShowDailyMission(false );
        ShowAchieve(true);
    }

    void ShowDailyMission( bool bShow )
    {
        if( bShow )
        {
            UIManager.PopPanel<PanelDailyMission>( null,false,this );

        }
        else
        {
            PanelDailyMission panelMission = UIManager.GetPanel<PanelDailyMission>();
            if ( panelMission != null)
            {
                panelMission.HidePanel();
            }
        }
    }

    void ShowAchieve(bool bShow)
    {
        if (bShow)
        {
            UIManager.PopPanel<PanelAchievement>(null, false, this);
        }
        else
        {
            PanelAchievement panelAchieve = UIManager.GetPanel<PanelAchievement>();
            if( panelAchieve != null )
            {
                panelAchieve.HidePanel();
            }
            
        }
    }

    protected void OnUpdateNotice()
    {
        if (PlayerMissionData.singleton.reward_num > 0)
            m_goMissionBubble.TrySetActive(true);
        else
            m_goMissionBubble.TrySetActive(false);

        if (PlayerAchieveData.m_rewardNum > 0)
            m_goAchieveBubble.TrySetActive(true);
        else
            m_goAchieveBubble.TrySetActive(false);
    }
}
