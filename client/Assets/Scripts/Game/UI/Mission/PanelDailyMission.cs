﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;


public class PanelDailyMission : BasePanel 
{
    class MissionItem:ItemRender
    {
        private GameObject m_goMission;

        Text m_txtMissionDesc;

        GameObject[] m_reward_list;

        Text m_txtCount;

        GameObject m_goEntry;

        Text m_goTxt;

        GameObject m_goGainReward;

        GameObject m_goDoneTips;

        string m_strGoSystem;

        public  BasePanel m_panelBelong;
        Image m_bg;
        Image m_imgIcon;
        SyncMission m_missionData;
        public ConfigItemLine[] m_itemCfgs = new ConfigItemLine[3];

        public override void Awake()
        {
            var tran = transform;
            
            m_txtMissionDesc = tran.Find("missioindesc").GetComponent<Text>();
            m_bg = gameObject.GetComponent<Image>();

            m_reward_list = new GameObject[3];
            for (int i = 0; i < m_reward_list.Length; i++)
            {
                m_reward_list[i] = tran.Find("rewarditem" + (i + 1)).gameObject;
            }

            m_txtCount = tran.Find("count/Text").GetComponent<Text>();

            m_goEntry = tran.Find("go").gameObject;
            m_goTxt = tran.Find("go/Text").GetComponent<Text>();
            m_goGainReward = tran.Find("gainreward").gameObject;
            m_goDoneTips = tran.Find("donetips").gameObject;

            m_imgIcon = tran.Find("icon/imgIcon").GetComponent<Image>();

            InitEvent();
        }

        protected override void OnSetData(object data)
        {
            int iMissionId = (int)data;
            SyncMission missionData = PlayerMissionData.singleton.GetMission(iMissionId);
            if (missionData == null) return;
            Flush(missionData);
            SetGoSystem(PlayerMissionData.singleton.GetGoSystem(iMissionId));
            SetMissionDesc(PlayerMissionData.singleton.GetMissionDesc(iMissionId));
            SetMissionIcon(PlayerMissionData.singleton.GetMissionIcon(iMissionId));

           string rewardID = PlayerMissionData.singleton.GetMissionRewardId(iMissionId);
           if (rewardID != null)
           {
                var cfg = ConfigManager.GetConfig<ConfigReward>().GetLine(rewardID);
                if (cfg != null)
                {
                    SetRewardItemList(cfg.ItemList);
                    Flush(PlayerMissionData.singleton.GetMission(iMissionId));
                }
           }
        }

        public void Init( GameObject go,BasePanel panelBelong )
        {
            m_goMission = go;
            var tran = go.transform;
            m_panelBelong = panelBelong;
            m_txtMissionDesc = tran.Find("missioindesc").GetComponent<Text>();
            m_bg = go.GetComponent<Image>();

            m_reward_list = new GameObject[3];
            for (int i = 0; i < m_reward_list.Length; i++)
            {
                m_reward_list[i] = tran.Find("rewarditem" + (i + 1)).gameObject;
            }
            
            m_txtCount = tran.Find("count/Text").GetComponent<Text>();

            m_goEntry = tran.Find("go").gameObject;
            m_goTxt = tran.Find("go/Text").GetComponent<Text>();
            m_goGainReward = tran.Find("gainreward").gameObject;
            m_goDoneTips = tran.Find("donetips").gameObject;

            m_imgIcon = tran.Find("icon/imgIcon").GetComponent<Image>();

            InitEvent();
        }

        
        void InitEvent()
        {
            UGUIClickHandler.Get(m_goEntry).onPointerClick += OnClickGo;
            UGUIClickHandler.Get(m_goGainReward).onPointerClick += OnClickGainReward;
            
            MiniTipsManager.get(m_reward_list[0]).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfgs[0] == null) return null;
                return new MiniTipsData() { name = m_itemCfgs[0].DispName, dec = m_itemCfgs[0].Desc };
            };
            MiniTipsManager.get(m_reward_list[1]).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfgs[1] == null) return null;
                return new MiniTipsData() { name = m_itemCfgs[1].DispName, dec = m_itemCfgs[1].Desc };
            };
            MiniTipsManager.get(m_reward_list[2]).miniTipsDataFunc = (go) =>
            {
                if (m_itemCfgs[2] == null) return null;
                return new MiniTipsData() { name = m_itemCfgs[2].DispName, dec = m_itemCfgs[2].Desc };
            };
        }
        public void Flush( SyncMission data )
        {
            m_missionData = data;
            m_txtCount.text = string.Format( "{0}/{1}", data.m_doneCount, data.m_configCount );
            if (data.m_status == SyncMission.MISSION_STATE.DOING || data.m_status == SyncMission.MISSION_STATE.ACCEPT )
            {
                m_bg.SetSprite(ResourceManager.LoadSprite("Common", "list_bg1"));
                m_txtMissionDesc.color = new Color(160 / 255f, 160f / 255f, 160f / 255f);
                string content = "";
                //if(TimeCheck(out content))
                //{
                //    Util.SetGoGrayShader()
                //}
                //else
                //{
                //    ConfigMissionLine cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(m_missionData.m_iMissionId) as ConfigMissionLine;
                //    if (cfg != null)
                //        m_goTxt.text = string.Format("{0}~{1}", cfg.TimeLimit[0], cfg.TimeLimit[1]);
                //}                
                m_goEntry.TrySetActive(true);
                bool time = TimeCheck(out content);
                m_goTxt.text = content;                
                Util.SetGoGrayShader(m_goEntry, !time);
                m_goGainReward.TrySetActive(false);
                m_goDoneTips.TrySetActive(false);
            }
            else if (data.m_status == SyncMission.MISSION_STATE.DONE )
            {
                m_bg.SetSprite(ResourceManager.LoadSprite("Mission", "missionselectbar"));
                m_txtMissionDesc.color = new Color(1f, 240f / 255f, 203f / 255f);
                m_goEntry.TrySetActive(false);
                m_goGainReward.TrySetActive(true);
                m_goDoneTips.TrySetActive(false);
                m_txtCount.text = string.Format("{0}/{1}", data.m_configCount, data.m_configCount);
            }
            else
            {
                m_bg.SetSprite(ResourceManager.LoadSprite("Common", "list_bg1"));
                m_txtMissionDesc.color = new Color(194 / 255f, 253f / 255f, 255f / 255f);
                m_goEntry.TrySetActive( false );
                m_goGainReward.TrySetActive( false );
                m_goDoneTips.TrySetActive(true);
                m_txtCount.text = string.Format("{0}/{1}", data.m_configCount, data.m_configCount);
            }
        }

        public void SetMissionDesc( string desc )
        {
            m_txtMissionDesc.text = desc;
        }

        public void SetMissionIcon(string icon)
        {
            m_imgIcon.SetSprite(ResourceManager.LoadIcon(icon));
            m_imgIcon.SetNativeSize();
        }


        DateTime GetTime(string str)
        {
            string[] arr = str.Split(':');
            DateTime now = TimeUtil.GetNowTime();
            int h = int.Parse(arr[0]);
            int m = int.Parse(arr[1]);
            DateTime time = new DateTime(now.Year, now.Month, now.Day, h, m,0);
            return time;
        }
        private int timeDis;
        bool TimeCheck(out string content)
        {
            bool result = true;
            timeDis = int.MaxValue;
            ConfigMissionLine cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(m_missionData.m_iMissionId) as ConfigMissionLine;
            content = "前往";
            if (cfg == null) return false;
            for (int i = 0; i < cfg.Timelimit.Length;i++ )
            {
                string[] str1 = cfg.Timelimit[i].Split('#');
                if(str1.Length==2)
                {
                    DateTime now = TimeUtil.GetNowTime();
                    DateTime start = GetTime(str1[0]);
                    DateTime end = GetTime(str1[1]);
                    if (now < start || now > end)
                    {
                        if (now < start)
                        {
                            int timeDis2 = Mathf.Abs((int)(now - start).TotalSeconds);
                            if (timeDis2 < timeDis)
                            {
                                content = string.Format("{0}:{1:D2}-{2}:{3:D2}", start.Hour, start.Minute, end.Hour, end.Minute);
                                timeDis = timeDis2;
                            }
                        }
                        else
                        {
                            int j = (i + 1) % cfg.Timelimit.Length;
                            string[] str = cfg.Timelimit[j].Split('#');
                            DateTime st = GetTime(str[0]);
                            DateTime et = GetTime(str[1]);
                            content = string.Format("{0}:{1:D2}-{2}:{3:D2}", st.Hour, st.Minute, et.Hour, et.Minute);
                        }
                        result = false;
                    }
                    else
                    {
                        result = true;
                        break;
                    }
                }
            }
            if(result)
            {
                content = "前往";
            }
            return result;
        }

        void OnClickGo( GameObject target, PointerEventData eventData )
        {
            string str = null;
            ConfigMissionLine cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(m_missionData.m_iMissionId) as ConfigMissionLine;
            if (cfg == null) return;
            string content = null;
            if (!TimeCheck(out content))
            {
                //UIManager.ShowTipPanel("活动还未开始");
                return;
            }
                
            if (cfg.Cond == "game_mode" || cfg.Cond == "finish_game")
                str = cfg.Params[0];                        
            if (!UIManager.GotoPanel(m_strGoSystem, str))
             {
                  UIManager.GotoPanel("battle", str);
             }            
        }

        void OnClickGainReward( GameObject target, PointerEventData eventData )
        {
            PlayerMissionData.getMissionReward(m_missionData.m_iMissionId);
        }

        public void SetGoSystem( string strGoSystem )
        {
            m_strGoSystem = strGoSystem;
        }

        public void SetRewardItemList( ItemInfo[] list )
        {
            var reward_num = 0;
            for (int i = 0; i < m_reward_list.Length; i++)
            {
                var info = i < list.Length ? ItemDataManager.GetItem(list[i].ID) : null;
                if (info != null && (info.Type != GameConst.ITEM_TYPE_CORPS || CorpsDataManager.IsJoinCorps()))
                {
                    if (i < 3)
                        m_itemCfgs[i] = info;
//                    this.GetType().GetField("m_itemCfg" + (i + 1)).SetValue(this, info);
                    
                    reward_num++;
                    m_reward_list[i].TrySetActive(true);
                    var img = m_reward_list[i].transform.Find("icon").GetComponent<Image>();
                    var lbl = m_reward_list[i].transform.Find("num").GetComponent<Text>();
                    img.SetSprite(ResourceManager.LoadIcon(info.Icon),info.SubType);
                    img.SetNativeSize();
                    if (img.sprite!=null && img.sprite.rect.height > 33)
                    {
                        img.transform.localScale = 33f / img.sprite.rect.height * Vector3.one;
                    }
                    lbl.text = list[i].cnt.ToString();
                }
                else
                {
                    m_reward_list[i].TrySetActive(false);
                }
            }
            var gap = (240 - 80 * reward_num) / (reward_num + 1);
            var start_x = -30 + gap;
            for (int j = 0; j < reward_num; j++)
            {
                //m_reward_list[j].transform.localPosition = new Vector3(start_x+j*(80+gap),0,0);
            }
        }

        public void ShowInList( Transform parent )
        {
            m_goMission.transform.SetParent(parent, false);
            m_goMission.TrySetActive( true );
        }
    }

    Dictionary<int, MissionItem> m_dicMissionItem;    
    GameObject m_goMissionItemProto;
    Transform m_dailyMissionViewList;
    Transform m_eventMissionViewList;
    GameObject m_normalMission;
    GameObject m_dailyMission;
    ConfigReward m_configReward;
    GameObject m_bubleNormalMission;
    GameObject m_bubbleEventMission;
    GameObject m_bubbleHeroMission;
    GameObject m_bubbleAchieveMission;
    int m_bubbleCount0; //日常任务
    int m_bubbleCount1; //活动任务

    DataGrid m_dataGrid;
    GameObject m_item;
    private EnumTabBtn m_curTabBtn=EnumTabBtn.normal;

    GameObject m_frame;
    public enum EnumTabBtn
    {
        normal=1,//日常任务
        events=2,//每日活动
        herocard=3,//英雄任务
        achieve = 4,
    }

    public PanelDailyMission()
    {
        SetPanelPrefabPath("UI/Mission/PanelDailyMission");
        AddPreLoadRes("UI/Mission/PanelDailyMission", EResType.UI);
        AddPreLoadRes("Atlas/Activity", EResType.Atlas);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);
    }

    public override void Init()
    {
        m_configReward = ConfigManager.GetConfig<ConfigReward>();
        //m_dicMissionItem = new Dictionary<int, MissionItem>();
        m_frame = m_tran.Find("frame").gameObject;
        m_dataGrid = m_tran.Find("frame/MissionList/ScrollView/content").GetComponent<DataGrid>();
        if (m_dataGrid == null)
        {
            m_dataGrid = m_tran.Find("frame/MissionList/ScrollView/content").gameObject.AddComponent<DataGrid>();
        }
        m_item = m_tran.Find("frame/MissionList/ScrollView/missionitem").gameObject;
        m_item.SetActive(false);
        m_dataGrid.SetItemRender(m_item,typeof(MissionItem));
        //m_normalMission = m_tran.Find("frame/normalMission").gameObject;
        //m_dailyMission = m_tran.Find("frame/dailyMission").gameObject;
        m_bubleNormalMission = m_tran.Find("toggleGrid/ToggleNormalMission/bubble").gameObject;
        m_bubbleEventMission = m_tran.Find("toggleGrid/ToggleDailyMission/bubble").gameObject;
        m_bubbleHeroMission = m_tran.Find("toggleGrid/ToggleHeroMission/bubble").gameObject;
        m_bubbleAchieveMission = m_tran.Find("toggleGrid/ToggleAchieveMission/bubble").gameObject;
    }
    public override void InitEvent()
    {        
        m_tran.Find("toggleGrid/ToggleNormalMission").GetComponent<Toggle>().onValueChanged.AddListener(NormalToggleChange);
        m_tran.Find("toggleGrid/ToggleDailyMission").GetComponent<Toggle>().onValueChanged.AddListener(DailyToggleChange);
        m_tran.Find("toggleGrid/ToggleHeroMission").GetComponent<Toggle>().onValueChanged.AddListener(HeroToggleChange);
        m_tran.Find("toggleGrid/ToggleAchieveMission").GetComponent<Toggle>().onValueChanged.AddListener(AchieveToggleChange);
        AddEventListener(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED, UpdateView);
    }

    private void AchieveToggleChange(bool On)
    {
        if (On)
        {
            showFrameList(EnumTabBtn.achieve);
        }
        else
        {
            m_frame.TrySetActive(true);
            UIManager.HidePanel<PanelAchievement>();
        }
    }

    private void NormalToggleChange(bool On)
    {
        if (On)
        {
            m_frame.TrySetActive(true);
            showFrameList(EnumTabBtn.normal);
        }
    }

    private void DailyToggleChange(bool On)
    {
        if (On)
        {
            m_frame.TrySetActive(true);
            showFrameList(EnumTabBtn.events);
        }
    }

    private void HeroToggleChange(bool On)
    {
        if (On)
        {
            if (HeroCardDataManager.getHeroMaxCardType() == EnumHeroCardType.none)
            {
                UIManager.ShowTipPanel("购买英雄卡后可以参与英雄任务？", null, () => UIManager.GotoPanel("mall"), false,
                     true, "我再想想", "前往购买");
            }
            showFrameList(EnumTabBtn.herocard);
        }
    }

    private void showFrameList(EnumTabBtn type)
    {
        m_curTabBtn = type;
        List<int> missionList = new List<int>();
        switch (type)
        {
            case EnumTabBtn.normal://日常任务
                missionList = SortMissionCanGain(EnumTabBtn.normal);
                break;
            case EnumTabBtn.events://每日活动
                missionList = SortMissionCanGain(EnumTabBtn.events);
                break;
            case EnumTabBtn.herocard:
                missionList = SortMissionCanGain(EnumTabBtn.herocard);
                break;
            case EnumTabBtn.achieve:
                m_frame.TrySetActive(false);
                UIManager.PopPanel<PanelAchievement>();
                break;
            default:
                break;
        }
        object[] arr = new object[missionList.Count];
        for (int i = 0; i < missionList.Count; i++)
        {
            arr[i] = missionList[i];
        }
        m_dataGrid.Data = arr;
    }

    public override void OnShow()
    {
        NetLayer.Send(new tos_player_mission_data());

        UpdateView();
    }

    public void UpdateView()
    {
       /* ResetMissionViewList();

        List<int> dailyMissionList = SortNormalByMissionCanGain();
        List<int> eventMissionList = SortEventByMissionCanGain();
        InitMissionItemList(dailyMissionList, m_dailyMissionViewList);
        InitMissionItemList(eventMissionList, m_eventMissionViewList);*/
        showFrameList(m_curTabBtn);
        UpdateBubble();
    }

    void UpdateBubble()
    {
        List<int> dailyMissionList = PlayerMissionData.singleton.GetDailyMissionList();
        List<int> eventMissionList = PlayerMissionData.singleton.GetDailyEventMissonList();
        List<int> heroMissionList = PlayerMissionData.singleton.GetHeroMissionList();
        m_bubbleCount0 = CalculateBubbleCount(dailyMissionList);
        m_bubbleCount1 = CalculateBubbleCount(eventMissionList);
        
        m_bubleNormalMission.TrySetActive(m_bubbleCount0 > 0);
        m_bubbleEventMission.TrySetActive(m_bubbleCount1 > 0);
        m_bubbleHeroMission.TrySetActive(CalculateBubbleCount(heroMissionList)>0);
        if (PlayerAchieveData.m_rewardNum > 0)
            m_bubbleAchieveMission.TrySetActive(true);
        else
            m_bubbleAchieveMission.TrySetActive(false);
    }

    int CalculateBubbleCount(List<int> list)
    {
        int count = 0;
        int[] arr = list.ToArray();
        for(int i=0;i<arr.Length;i++)
        {
            SyncMission mission = PlayerMissionData.singleton.GetMission(arr[i]);
            if (mission.m_status == SyncMission.MISSION_STATE.DONE)
                count++;
        }
        return count;
    }

    /* void InitMissionItemList(List<int> dailyMissionList,Transform parentTran )
     {
         for (int index = 0; index < dailyMissionList.Count; ++index)
         {
             int iMissionId = dailyMissionList[index];
             MissionItem missionItem;
             if (!m_dicMissionItem.TryGetValue(iMissionId, out missionItem))
             {
                 missionItem = new MissionItem();
                 GameObject newGoMission = UIHelper.Instantiate(m_goMissionItemProto) as GameObject;
                 newGoMission.TrySetActive(true);
                 missionItem.Init(newGoMission, this);

                 m_dicMissionItem.Add(iMissionId, missionItem);

                 missionItem.SetGoSystem(PlayerMissionData.singleton.GetGoSystem(iMissionId));
                 missionItem.SetMissionDesc(PlayerMissionData.singleton.GetMissionDesc(iMissionId));
                 missionItem.SetMissionIcon(PlayerMissionData.singleton.GetMissionIcon(iMissionId));

             }
             string rewardID = PlayerMissionData.singleton.GetMissionRewardId(iMissionId);
             if(rewardID!=null)
             {
                 var cfg=m_configReward.GetLine(rewardID);
                 if(cfg!=null)
                 {
                     missionItem.SetRewardItemList(cfg.ItemList);
                     missionItem.Flush(PlayerMissionData.singleton.GetMission(iMissionId));
                     missionItem.ShowInList(parentTran);
                 }              
             }           
         }
     }*/

    /// <summary>
    /// 排列可领取奖励列表
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    List<int> SortMissionCanGain(EnumTabBtn type)
    {
        List<int> temp = new List<int>();
        switch(type){
            case EnumTabBtn.normal:
                temp = PlayerMissionData.singleton.GetDailyMissionList();
                break;
            case EnumTabBtn.events:
                temp= PlayerMissionData.singleton.GetDailyEventMissonList();
                break;
            case EnumTabBtn.herocard:
                temp = PlayerMissionData.singleton.GetHeroMissionList();
                break;
        }
        List<int> missionList = new List<int>();
        TimeFilter(type, temp, missionList);
        SortList(missionList);
        return missionList;
    }

    /*重复代码注释掉
    List<int> SortNormalByMissionCanGain()
    {
        List<int> temp = PlayerMissionData.singleton.GetDailyMissionList();
        List<int> dailyMissionList = new List<int>();
        TimeFilter(temp, dailyMissionList);        
        SortList(dailyMissionList);        
        return dailyMissionList;
    }

    List<int> SortEventByMissionCanGain()
    {
        List<int> tempEvent = PlayerMissionData.singleton.GetDailyEventMissonList();
        List<int> dailyEventMissionList = new List<int>();        
        TimeFilter(tempEvent, dailyEventMissionList);        
        SortList(dailyEventMissionList);
        return dailyEventMissionList;
    }*/

    void SortList(List<int> dailyMissionList)
    {
        int sortIndex = 0;
        for (int index = 0; index < dailyMissionList.Count; ++index)
        {
            int iMissionId = dailyMissionList[index];
            SyncMission missionData = PlayerMissionData.singleton.GetMission(iMissionId);
            if (missionData.m_status == SyncMission.MISSION_STATE.DONE)
            {
                int tempid = dailyMissionList[sortIndex];
                dailyMissionList[sortIndex] = iMissionId;
                dailyMissionList[index] = tempid;
                ++sortIndex;
            }
        }
        for (int cnt = 0; cnt < dailyMissionList.Count; ++cnt)
        {
            int iMissionId = dailyMissionList[cnt];
            SyncMission missionData = PlayerMissionData.singleton.GetMission(iMissionId);
            if (missionData.m_status == SyncMission.MISSION_STATE.DOING || missionData.m_status == SyncMission.MISSION_STATE.ACCEPT)
            {
                int tempid = dailyMissionList[sortIndex];
                dailyMissionList[sortIndex] = iMissionId;
                dailyMissionList[cnt] = tempid;
                ++sortIndex;
            }
        }
    }

    void TimeFilter(EnumTabBtn type, List<int> temp, List<int> dailyMissionList)
    {
        //根据任务时间过滤一下
        var timestamp = TimeUtil.GetNowTimeStamp();
        var config = ConfigManager.GetConfig<ConfigMission>();
        for (int i = 0; i < temp.Count; i++)
        {
            var info = config.GetLine(temp[i]);
            SyncMission sdata = info != null ? PlayerMissionData.singleton.GetMission(info.ID) : null;
            if (info != null && sdata != null)
            {     
                if ((info.TakeStartTime == 0 && info.TakeEndTime == 0)
                    || (timestamp >= info.TakeStartTime && timestamp <= info.TakeEndTime)
                    )
                {
                    dailyMissionList.Add(info.ID);
                }
                else if(type == EnumTabBtn.herocard && TimeUtil.GetTime(timestamp).DayOfYear == TimeUtil.GetTime(info.TakeEndTime).DayOfYear)
                {
                    dailyMissionList.Add(info.ID);
                }
            }
        }
    }

    void ResetMissionViewList()
    {
        for( int index = 0; index < m_dailyMissionViewList.childCount; ++ index  )
        {
            Transform child = m_dailyMissionViewList.GetChild(index);
            child.SetParent( m_tran );
            child.gameObject.TrySetActive(false);
            index = 0;
        }
        for (int index = 0; index < m_eventMissionViewList.childCount; ++index)
        {
            Transform child = m_eventMissionViewList.GetChild(index);
            child.SetParent(m_tran);
            child.gameObject.TrySetActive(false);
            index = 0;
        }
    }
    public override void OnHide()
    {
        if (UIManager.IsOpen<PanelAchievement>())
        {
            UIManager.HidePanel<PanelAchievement>();
        }
    }
    public override void OnBack()
    {
        if (UIManager.IsOpen<PanelAchievement>())
        {
            var p = UIManager.GetPanel<PanelAchievement>();
            if (p.ModuleIsOn())
            {
                p.goBack();
                return;
            }
        }
        this.HidePanel();
        UIManager.ShowPanel<PanelHallBattle>();
    }
    public override void OnDestroy()
    {
        
    }
    public override void Update()
    {

    }

    void Toc_player_get_mission_reward(toc_player_get_mission_reward data)
    {
        //CDict missionData = data.Dict("val");
       //PlayerMissionData.singleton.ServerSyncMissiondata(missionData,true);
        int iMissionId = data.mission_id;
        ConfigRewardLine rewardLine = m_configReward.GetLine(PlayerMissionData.singleton.GetMissionRewardId(iMissionId));
        if( rewardLine != null )
        {
               UIManager.PopPanel<PanelMissionComplete>(rewardLine.ItemList, true,null,LayerType.POP,1.0f);
        }
        /*MissionItem missionItem;
        if( m_dicMissionItem.TryGetValue( iMissionId,out missionItem ) )
        {
           SyncMission syncMissionData = PlayerMissionData.singleton.GetMission(iMissionId);

           missionItem.Flush( syncMissionData );

           ConfigRewardLine rewardLine = m_configReward.GetLine(PlayerMissionData.singleton.GetMissionRewardId(iMissionId));
           if( rewardLine != null )
           {
               //PanelRewardItemTip tip = UIManager.PopPanel<PanelRewardItemTip>(null, true, this);
               //tip.SetRewardItemList( rewardLine.ItemList );
               //PanelRewardItemTipsWithEffect.DoShow(rewardLine.ItemList);
               UIManager.PopPanel<PanelMissionComplete>(rewardLine.ItemList, true,null,LayerType.POP,1.0f);
           }
        }*/
        UpdateBubble();
        GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);
    }

    void Toc_legionwar_data(toc_legionwar_data data)
    {
        UIManager.PopPanel<PanelLegionMap>();
        bool m_has = false;
        bool m_is = false;
        if (PlayerSystem.m_tagList != null)
        {
            for (int i = 0; i < PlayerSystem.m_tagList.Count; i++)
            {
                if (PlayerSystem.m_tagList[i].tag_id == (int)EnumTag.LEGION_WAR_FIRST)
                {
                    m_has = true;
                    if (PlayerSystem.m_tagList[i].value == 1)
                    {
                        m_is = true;
                    }
                }
            }
        }
        if (!m_has || !m_is)
        {
            UIManager.PopPanel<PanelLegionFirst>(null, true);
        }
    }

}
