﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

using System.Collections.Generic;

public class SyncMission
{
    public enum MISSION_STATE
    {
        CANNTDO = -1,
        ACCEPT = 0,
        DOING,
        DONE,
        GOTREWARD
    }
    public int m_iMissionId;
    public int m_configCount;
    public int m_doneCount;
    public MISSION_STATE m_status;
}

public class PlayerMissionData
{
    public enum TASKNEWHAND_TYPE
    {
        LEVEL = 1,
        OTHER
    }

    // add：新任务 del：删除任务 update：任务更新
    public const string DEALTYPE_ADD = "add";
    public const string DEALTYPE_DEL = "del";
    public const string DEALTYPE_UPDATE = "update";
    public static PlayerMissionData singleton = null;

    ConfigMission m_configMissionData;
    /// <summary>
    /// 服务端日常任务列表
    /// </summary>
    List<int> m_dailyMissionList;

    /// <summary>
    /// 服务端每日活动任务列表
    /// </summary>
    List<int> m_dailyEventMissionList;
    /// <summary>
    /// 服务端新手任务列表
    /// </summary>
    List<int> m_newHandMissionList;
    /// <summary>
    /// 英雄卡任务列表
    /// </summary>
    Dictionary<int, List<int>> m_heroCardMissionDIC;
    public int reward_num;
    public int reward_newHandNum;
    Dictionary<int, SyncMission> m_syncMissionData;

    public enum MISSION_TYPE
    {
        NONE,
        DAILY,
        NEWHAND_LEVEL,
        NEWHAND_OTHER,
        PRIMARY,
        BRANCH,
        HERONORMAL=11,
        HEROSUPER=12,
        HEROLEGEND=13
    }
    public PlayerMissionData()
    {
        if (singleton != null)
        {
            Logger.Error("PlayerMissionData is a singleton");
        }

        singleton = this;

        Init();
    }

    void Init()
    {
        m_dailyMissionList = new List<int>();
        m_dailyEventMissionList = new List<int>();
        m_newHandMissionList = new List<int>();
        m_syncMissionData = new Dictionary<int, SyncMission>();
        m_heroCardMissionDIC = new Dictionary<int, List<int>>();
        m_configMissionData = ConfigManager.GetConfig<ConfigMission>();

        //InitDailyMissionList();

        GameDispatcher.AddEventListener<toc_login_select>(GameEvent.MAIN_PLAYER_JOIN, OnMainPlayerJoin);
    }

    /// <summary>
    /// 获取任务数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public SyncMission GetMission(int id)
    {
        SyncMission mission = null;
        if (m_syncMissionData.TryGetValue(id, out mission))
        {
            return mission;
        }
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        if (missionConfigLine == null)
            return null;
        mission = new SyncMission();
        mission.m_iMissionId = id;
        mission.m_configCount = missionConfigLine.Count;
        mission.m_doneCount = 0;
        //初始标志为未能开始，任务的开始状态由后端控制
        mission.m_status = SyncMission.MISSION_STATE.CANNTDO;
        m_syncMissionData.Add(id, mission);
        return mission;
    }

    public ConfigMissionLine GetMissionCfg(int id)
    {
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        return missionConfigLine;
    }

    public string GetMissionDesc(int id)
    {
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        return missionConfigLine != null ? missionConfigLine.Desc : "";
    }

    public string GetMissionRewardId(int id)
    {
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        return missionConfigLine != null ? missionConfigLine.RewardId : "";
    }

    public string GetMissionIcon(int id)
    {
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        return missionConfigLine != null ? missionConfigLine.Icon : "";
    }

    /* void InitDailyMissionList()
     {
         m_dailyMissionList.Clear();
         for (int index = 0; index < m_configMissionData.m_dataArr.Length; ++index)
         {
             ConfigMissionLine missionLine = m_configMissionData.m_dataArr[index];
             if (missionLine.Visible && missionLine.Type == (int)MISSION_TYPE.DAILY)
             {
                 m_dailyMissionList.Add(missionLine.ID);
             }
         }
     }*/


    public bool isHasNewHandMission()
    {
        return m_newHandMissionList.Count > 0;
    }

    public List<int> GetNewHandMissionList()
    {
        return m_newHandMissionList;
    }

    public List<int> GetDailyMissionList()
    {
        return m_dailyMissionList;
    }

    /// <summary>
    /// 获得英雄卡的任务列表
    /// </summary>
    /// <returns></returns>
    public List<int> GetHeroMissionList()
    {
        List<int> temp = new List<int>();
        int type = (int)HeroCardDataManager.getHeroMaxCardType();
        if (type > 0&&m_heroCardMissionDIC!=null)
        {
            if(m_heroCardMissionDIC.ContainsKey(type))
                temp=m_heroCardMissionDIC[type];
        }
        return temp;
    }

    public List<int> GetDailyEventMissonList()
    {
        return m_dailyEventMissionList;
    }


    public string GetGoSystem(int id)
    {
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(id);
        return missionConfigLine != null ? missionConfigLine.GoSystem : "";
    }

    public static void getMissionReward(int missionId)
    {
        NetLayer.Send(new tos_player_get_mission_reward() { mission_id = missionId });
    }

    /*static void Toc_player_mission_lists(CDict data)
    {
        object[] missionList = data.Ary<object>("val");

        singleton.reward_num = 0;
        for (int index = 0; index < missionList.Length; ++index)
        {
            object[] typeList = missionList[index] as object[];
            for (int typeindex = 0; typeindex < typeList.Length; ++typeindex)
            {
                CDict missionData = typeList[typeindex] as CDict;
                singleton.ServerSyncMissiondata(missionData, false);
                singleton.reward_num += (SyncMission.MISSION_STATE)(missionData.Int("status")) == SyncMission.MISSION_STATE.DONE ? 1 : 0;
            }
        }
        GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);
    }*/

    /// <summary>
    /// 玩家任务数据返回处理
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_mission_data(toc_player_mission_data data)
    {
        singleton.m_newHandMissionList = new List<int>();
        p_mission_data_toc[] missionList = data.missions;        
        singleton.reward_num = 0;
        singleton.reward_newHandNum = 0;
        for (int index = 0; index < missionList.Length; ++index)
        {
            p_mission_data_toc tempData = missionList[index];
            singleton.ServerSyncMissiondata(tempData);
//            var cfg = ConfigManager.GetConfig<ConfigMission>().GetLine(tempData.id).Type;            
            if (isNewHandTask(tempData.id))
            {
                singleton.reward_newHandNum += (SyncMission.MISSION_STATE)(tempData.status) == SyncMission.MISSION_STATE.DONE ? 1 : 0;
            }
            else
            {
                singleton.reward_num += (SyncMission.MISSION_STATE)(tempData.status) == SyncMission.MISSION_STATE.DONE ? 1 : 0;
            }
        }
        if (singleton.reward_newHandNum > 0)
        {
            BubbleManager.SetBubble(BubbleConst.NewHandTask, true);
        }
        else
        {
            BubbleManager.SetBubble(BubbleConst.NewHandTask, false);
        }
        GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);
    }

    public static int getHeroTaskType(int missionId)
    {
        int type = -1;
        ConfigMissionLine missionConfigLine = singleton.m_configMissionData.GetLine(missionId);
        if (missionConfigLine == null)
            return type;

        switch (missionConfigLine.Type)
        {
            case (int)MISSION_TYPE.HERONORMAL:
                type = (int)EnumHeroCardType.normal;
                break;

            case (int)MISSION_TYPE.HEROSUPER:
                type = (int)EnumHeroCardType.super;
                break;

            case (int)MISSION_TYPE.HEROLEGEND:
                type = (int)EnumHeroCardType.legend;
                break;
        }
        return type;
    }

    /// <summary>
    /// 判断是否是新手任务
    /// </summary>
    /// <param name="missionId"></param>
    /// <returns></returns>
    public static bool isNewHandTask(int missionId)
    {
        ConfigMissionLine missionConfigLine = singleton.m_configMissionData.GetLine(missionId);
        if (missionConfigLine == null)
            return false;
        return (missionConfigLine.Type == (int)MISSION_TYPE.NEWHAND_LEVEL || missionConfigLine.Type == (int)MISSION_TYPE.NEWHAND_OTHER);
    }
    /*  static void Toc_player_mission(CDict data)
      {
          CDict missionData = data.Dict("val");
          singleton.ServerSyncMissiondata(missionData, true);
      }*/

    /// <summary>
    /// 单个任务数据更新处理
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_modify_mission(toc_player_modify_mission data)
    {
        singleton.ServerSyncMissiondata(data.mission, data.type, true);
    }


    /// <summary>
    /// 处理服务端的数据，并对数据进行分类
    /// </summary>
    /// <param name="missionData"></param>
    /// <param name="dealType"></param>
    /// <param name="fire_event"></param>
    public void ServerSyncMissiondata(p_mission_data_toc missionData, string dealType = DEALTYPE_ADD, bool fire_event = false)
    {
        int iMissionId = missionData.id;
        SyncMission syncData = GetMission(iMissionId);
        if (syncData == null)
            return;
        syncData.m_status = (SyncMission.MISSION_STATE)(missionData.status);
        syncData.m_doneCount = missionData.progress;
        ConfigMissionLine missionConfigLine = m_configMissionData.GetLine(iMissionId);        
        if (dealType == DEALTYPE_DEL)
        {//删除任务
            if (m_syncMissionData.ContainsKey(iMissionId))
            {
                m_syncMissionData.Remove(iMissionId);
            }
            if (missionConfigLine.Type == (int)MISSION_TYPE.DAILY)
            {
                //记录日常任务
                if (missionConfigLine.EVENT == 0)
                    m_dailyMissionList.Remove(iMissionId);
                else
                    m_dailyEventMissionList.Remove(iMissionId);
            }
            else if (isNewHandTask(iMissionId))
            {//记录新手任务
                m_newHandMissionList.Remove(iMissionId);
            }
            else
            {//英雄卡任务处理
                int heroType = getHeroTaskType(iMissionId);
                if (heroType > 0)
                {
                    if (m_heroCardMissionDIC.ContainsKey(heroType))
                    {
                        if (m_heroCardMissionDIC[heroType].IndexOf(iMissionId) != -1)
                            m_heroCardMissionDIC[heroType].Remove(iMissionId);
                    }
                }
            }
        }
        else
        {
            if (missionConfigLine.Type == (int)MISSION_TYPE.DAILY)
            {
                //记录日常任务
                if (m_dailyMissionList.IndexOf(iMissionId) == -1&&m_dailyEventMissionList.IndexOf(iMissionId)==-1)
                {
                    if (missionConfigLine.EVENT == 0)
                        m_dailyMissionList.Add(iMissionId);
                    else
                        m_dailyEventMissionList.Add(iMissionId);
                }
            }
            else if (isNewHandTask(iMissionId))
            {//记录新手任务
                if (m_newHandMissionList.IndexOf(iMissionId) == -1)
                {
                    m_newHandMissionList.Add(iMissionId);
                }
            }
            else
            {//英雄卡任务处理
                int heroType = getHeroTaskType(iMissionId);
                if (heroType > 0)
                {
                    if (!m_heroCardMissionDIC.ContainsKey(heroType))
                    {
                        m_heroCardMissionDIC.Add(heroType, new List<int>());
                    }
                    if (m_heroCardMissionDIC[heroType].IndexOf(iMissionId) == -1)
                        m_heroCardMissionDIC[heroType].Add(iMissionId);
                }
            }
        }
        if (fire_event&&dealType != DEALTYPE_DEL)
        {
            if (syncData.m_status == SyncMission.MISSION_STATE.DONE)
            {
                if (isNewHandTask(iMissionId))
                    reward_newHandNum++;
                else
                    reward_num++;
            }
            else if (syncData.m_status == SyncMission.MISSION_STATE.GOTREWARD)
            {
                if (isNewHandTask(iMissionId))
                    reward_newHandNum--;
                else
                    reward_num--;
            }
            if (singleton.reward_newHandNum > 0)
            {
                BubbleManager.SetBubble(BubbleConst.NewHandTask, true);
            }
            else
            {
                BubbleManager.SetBubble(BubbleConst.NewHandTask, false);
            }
            GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);
        }
    }

    void OnMainPlayerJoin(toc_login_select tlogin)
    {

        //SendMissionMsg("mission_lists");
        NetLayer.Send(new tos_player_mission_data());

        GlobalBattleParams.singleton.SetBattleParam(GlobalBattleParams.KEY_KNIFE_AUTOAIM, ConfigManager.GetConfig<ConfigBattleParamSetting>().GetLine(GlobalBattleParams.KEY_KNIFE_AUTOAIM).value);
    }

    public static int GetVoucherMission(List<int> missionlist)
    {
        int num = 0;
        for (int i = 0; i < missionlist.Count; i++)
        {
            var mission = ConfigManager.GetConfig<ConfigMission>().GetLine(missionlist[i]);
            var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(mission.RewardId);
            for (int j = 0; j < reward.ItemList.Length; j++)
            {
                if (reward.ItemList[j].ID == 5052)
                {
                    num += reward.ItemList[j].cnt;
                }
            }
        }
        return num;
    }

    public static int GetVoucherMissionComplete(List<int> missionlist)
    {
        int num = 0;
        for (int i = 0; i < missionlist.Count; i++)
        {
            var mission = ConfigManager.GetConfig<ConfigMission>().GetLine(missionlist[i]);
            var reward = ConfigManager.GetConfig<ConfigReward>().GetLine(mission.RewardId);
            for (int j = 0; j < reward.ItemList.Length; j++)
            {
                if (reward.ItemList[j].ID == 5052)
                {
                    var data = PlayerMissionData.singleton.GetMission(mission.ID);
                    if (data.m_status == SyncMission.MISSION_STATE.GOTREWARD)
                    {
                        num += reward.ItemList[j].cnt;
                    }
                }
            }
        }
        return num;
    }

    public static int GetAllVoucherMission()
    {
        return GetVoucherMission(PlayerMissionData.singleton.GetDailyMissionList()) +
               GetVoucherMission(PlayerMissionData.singleton.GetDailyEventMissonList()) +
               GetVoucherMission(PlayerMissionData.singleton.GetHeroMissionList());
    }

    public static int GetAllVoucherMissionComplete()
    {
        return GetVoucherMissionComplete(PlayerMissionData.singleton.GetDailyMissionList()) +
               GetVoucherMissionComplete(PlayerMissionData.singleton.GetDailyEventMissonList()) +
               GetVoucherMissionComplete(PlayerMissionData.singleton.GetHeroMissionList());
    }


}

