﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;
/// <summary>
/// 注意，此入口已废弃，请改用PanelRewardItemTipsWithEffect.DoShow()方法
/// </summary>
public class PanelRewardItemTip : BasePanel 
{
    class ItemTip
    {
        GameObject m_go;
        Image m_imgItemIcon;
        Text m_txtNum;
        Image m_imgItemIconEx;
        Text m_txtNumEx;
        Text m_txtName;
        Image m_bound;
        public bool m_itemhad;
        public int m_itemid;
        public void Init( GameObject go )
        {
            m_itemhad = true;
            m_go = go;
            m_imgItemIcon = go.transform.Find("image").GetComponent<Image>();
            m_txtNum = go.transform.Find("num").GetComponent<Text>();
            m_txtName = go.transform.Find("name").GetComponent<Text>();
            m_imgItemIconEx = go.transform.Find("imageEx").GetComponent<Image>();
            m_txtNumEx = go.transform.Find("numEx").GetComponent<Text>();
            m_bound = go.transform.Find("bound").GetComponent<Image>();
        }

        public void SetItemInfo(ItemInfo vo)
        {
            if (vo == null)
            {
                return;
            }
            if (vo is BuffInfo)
            {
                m_imgItemIcon.gameObject.TrySetActive(true);
                m_txtNum.gameObject.TrySetActive(false);
                m_txtName.gameObject.TrySetActive(true);
                m_bound.gameObject.TrySetActive(true);
                m_imgItemIconEx.gameObject.TrySetActive(false);
                m_txtNumEx.gameObject.TrySetActive(false);
                Sprite icon = null;
                var scale = Vector3.one;
                var buff_info = vo as BuffInfo;
                if (!string.IsNullOrEmpty(buff_info.icon))
                {
                    icon = ResourceManager.LoadIcon(buff_info.icon);
                    scale *= Mathf.Min((m_bound.rectTransform.rect.height - 20) / icon.rect.height, 1f);
                }
                    ConfigItemLine cfg = ItemDataManager.GetItem(buff_info.ID);
                m_imgItemIcon.transform.localScale = scale;
                m_imgItemIcon.SetSprite(icon,cfg!=null?cfg.SubType:"");
                m_imgItemIcon.SetNativeSize();
                m_txtName.text = buff_info.buff_name;
            }
            else
            {
                SetItemInfo(vo.ID, vo.cnt);
            }
        }

        public void SetItemInfo(int itemId, int num)
        {
            m_bound.gameObject.TrySetActive(false);
            ConfigItemLine itemLine = ItemDataManager.GetItem( itemId );
            if( itemLine != null )
            {
                Sprite icon = ResourceManager.LoadIcon( itemLine.Icon );
                var visible = itemLine.Type != "EQUIP";
                m_imgItemIcon.gameObject.TrySetActive(visible);
                m_txtNum.gameObject.TrySetActive(visible);
                m_txtName.gameObject.TrySetActive(visible);
                m_bound.gameObject.TrySetActive(visible);
                m_imgItemIconEx.gameObject.TrySetActive(!visible);
                m_txtNumEx.gameObject.TrySetActive(!visible);
                if (itemLine.Type != "EQUIP")
                {
                    var scale = Vector3.one * Mathf.Min(icon!=null ? (m_bound.rectTransform.rect.height - 20) / icon.rect.height : 1f, 1f);
                    m_imgItemIcon.transform.localScale = scale;
                    m_imgItemIcon.SetSprite(icon,itemLine.SubType);
                    m_imgItemIcon.SetNativeSize();
                    m_txtNum.text = string.Format( "x{0}", num);
                    m_txtName.text = itemLine.DispName;
                }
                else
                {
                    m_imgItemIconEx.SetSprite(icon, itemLine.SubType);
                    m_imgItemIconEx.SetNativeSize();
                    m_txtNumEx.text = string.Format("{0} x{1}", itemLine.DispName, num);
                    m_itemhad = ItemDataManager.GetDepotItem(itemId) != null;
                    m_itemid = itemId;
                }
            }
        }

        public void SetActive( Transform parent )
        {
            m_go.transform.SetParent(parent, false);
            m_go.TrySetActive(true);
        }

        public void Hide()
        {
            m_go.TrySetActive(false);
        }
    }

    public PanelRewardItemTip()
    {
        SetPanelPrefabPath("UI/Common/PanelRewardItemTip");
        AddPreLoadRes("UI/Common/PanelRewardItemTip", EResType.UI);
        AddPreLoadRes("Atlas/Room", EResType.Atlas);
        AddPreLoadRes("Atlas/Mission", EResType.Atlas);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
    }

    ItemInfo[] m_listItemReward;

    GameObject m_goProto;
    Transform m_transParentContent;
    Transform m_transFrame;
    private bool m_bEvent;
    private string m_tips;
    List<ItemTip> m_listItemTips;
    Text m_tips_txt;
    public override void Init()
    {
        m_listItemTips = new List<ItemTip>();

        m_goProto = m_tran.Find("frame/item").gameObject;
        m_transParentContent = m_tran.Find("frame/content");
        m_transFrame = m_tran.Find("frame");
        m_tips_txt = m_tran.Find("frame/tips_txt").GetComponent<Text>();
        
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/sure")).onPointerClick += OnClickSure;
    }
    public override void OnShow()
    {
        if( HasParams() )
        {
            m_bEvent = (bool)m_params[0];
            m_tips = m_params.Length > 1 ? m_params[1].ToString() : "";
        }
        else
        {
            m_bEvent = false;
            m_tips = "";
        }
        Flush();
        AudioManager.PlayUISound(AudioConst.award);
    }
    public override void OnHide()
    {
        
    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
        UGUIClickHandler.Get(m_tran.Find("frame/sure")).onPointerClick -= OnClickSure;
    }
    public override void Update()
    {

    }

    void OnClickSure( GameObject target, PointerEventData eventData )
    {
        //> 对活动提示做特殊处理
        if( !m_bEvent )
        {
            for (int i = 0; i < m_listItemTips.Count; i++)
            {
                if (!m_listItemTips[i].m_itemhad)
                {
                    var item = ItemDataManager.GetDepotItem(m_listItemTips[i].m_itemid);
                    if (item != null)
                    {
                        PanelChooseGroup.DoShow(new object[] { item.uid }, true);
                    }
                }
            }
        }

        if (UIManager.IsOpen<PanelCorpsLottery>())
        {
            NetLayer.Send(new tos_corps_openlottery()); 
        }
        
        DestroyPanel();
    }

    public void SetRewardItemList( ItemInfo[] list )
    {
        m_listItemReward = null;
        if (list != null)
        {
            var item_list = new List<ItemInfo>();
            for (int k = 0; k < list.Length; k++)
            {
                var info = list[k] is BuffInfo ? null : ItemDataManager.GetItem(list[k].ID);
                if (info == null || (info.Type != GameConst.ITEM_TYPE_CORPS || CorpsDataManager.IsJoinCorps()))
                {
                    item_list.Add(list[k]);
                }
            }
            m_listItemReward = item_list.ToArray();
        }
        Flush();
    }

    void DisactiveItems()
    {
        for (int index = 0; index < m_transParentContent.childCount; )
        {
            Transform child = m_transParentContent.GetChild(index);
            child.SetParent(m_transFrame, false);
            child.gameObject.TrySetActive(false);
            index = 0;
        }
    }

    void Flush()
    {
        if (m_tran == null)
        {
            return;
        }
        if (m_tips_txt != null)
        {
            m_tips_txt.text = m_tips!=null ? m_tips : "";
        }
        if( m_listItemReward != null )
        {
            for (int index = 0; index < m_listItemReward.Length; ++index)
            {
                ItemInfo item = m_listItemReward[index];
                if (index < m_listItemTips.Count)
                {
                    ItemTip tip = m_listItemTips[index];
                    tip.SetItemInfo(item);
                    tip.SetActive(m_transParentContent);
                }
                else
                {
                    GameObject goTip = UIHelper.Instantiate(m_goProto) as GameObject;
                    ItemTip newTip = new ItemTip();
                    newTip.Init(goTip);
                    newTip.SetItemInfo(item);
                    newTip.SetActive(m_transParentContent);
                    m_listItemTips.Add(newTip);
                }
            }

            for (int i = m_listItemReward.Length; i < m_listItemTips.Count; i++)
                m_listItemTips[i].Hide();
        }
    }
}
