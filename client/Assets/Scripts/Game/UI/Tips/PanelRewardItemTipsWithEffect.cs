﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PanelRewardItemTipsWithEffect :BasePanel {
    
    private UIEffect m_effect;
    private GameObject m_btn;
    private Transform m_effctTran;
    private Image m_icon;
    private Text m_count;
    private GameObject m_gotoLottery;
    private static int[] m_jintiArr = new int[] { 5040, 5041, 5042 };

    public static Queue<ItemInfo> ItemQueue = new Queue<ItemInfo>();
    public static bool isShowing = false;
    GameObject m_equipPart;
    Transform m_equipSureBtn;
    Transform m_equipCancelBtn;
    Text m_tips;
    public static bool isSkip = false;    
    public PanelRewardItemTipsWithEffect()
    {
        SetPanelPrefabPath("UI/Common/PanelRewardItemTipWithEffect");
        AddPreLoadRes("UI/Common/PanelRewardItemTipWithEffect", EResType.UI);                                                       
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
    }

    public override void Init()
    {        
        m_effctTran = m_tran.Find("effectTran");
        m_btn = m_tran.Find("Icon/sure").gameObject;
        m_icon = m_tran.Find("Icon").GetComponent<Image>();
        m_count = m_tran.Find("Icon/count").GetComponent<Text>();        
        m_equipPart = m_tran.Find("Icon/equipPart").gameObject;
        m_gotoLottery = m_tran.Find("Icon/gotoLottery").gameObject;
        m_equipSureBtn = m_equipPart.transform.Find("sure");
        m_equipCancelBtn = m_equipPart.transform.Find("cancel");
        m_tips = m_tran.Find("Icon/text").GetComponent<Text>();
        m_effect = UIEffect.ShowEffectAfter(m_icon.transform, EffectConst.UI_HUODEZUANSHI);        
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_btn).onPointerClick += OnSureClick;

        UGUIClickHandler.Get(m_equipSureBtn).onPointerClick += OnEquipAllClick;
        UGUIClickHandler.Get(m_equipCancelBtn).onPointerClick += OnEquipCancelClick;
        UGUIClickHandler.Get(m_tran.Find("Icon/gotoLottery/sure")).onPointerClick += OnSureClick;
        UGUIClickHandler.Get(m_tran.Find("Icon/gotoLottery/toLottery")).onPointerClick += OnGoLottery;
    }

    private void OnGoLottery(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
        var panel = UIManager.GetPanel<PanelEvent>();
        if (panel != null)
            panel.HidePanel();
        if (info.ID == 5040) //白晶
            UIManager.GotoPanel("lottery#baijing");
        else if (info.ID == 5041)
            UIManager.GotoPanel("lottery#lanjing");
        else if (info.ID == 5042)
            UIManager.GotoPanel("lottery#zijing");        
    }


    private void OnEquipCancelClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnEquipAllClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        var groups = ItemDataManager.GetGroupsID();
        int uid = ItemDataManager.GetDepotLastItem(info.ID).uid;
        for (int i = 0; i < groups.Length; i++)
        {
            NetLayer.Send(new tos_player_equip_item() { group = (int)groups[i], uid = uid });
        }
        HidePanel();
    }

    private void OnSureClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel(); 
    }

    public static void DoShow(ItemInfo[] items, bool addMask = false)
    {
        if (UIManager.IsBattle())
            return;
        if (isSkip)
        {
            isSkip = false;
            return;
        }
        Sort(items);
        if(items != null)
        {
            for (int i = 0; i < items.Length; i++)
            {
                bool contain = false;
                ItemInfo[] arr = ItemQueue.ToArray();
                for (int j = 0; j < arr.Length;j++ )
                {
                    if(arr[j].ID==items[i].ID)
                    {
                        contain = true;
                        break;
                    }
                }
                if (!contain)
                {
                    ItemQueue.Enqueue(items[i]);
                }
            }                
        }
        if(!isShowing)
        {
            isShowing = true;
            var iteminfo = ItemQueue.Peek();
            ConfigItemLine config = null;
            config = (iteminfo is BuffInfo) ? null : ItemDataManager.GetItem(iteminfo.ID);                
            if (iteminfo != null && config!=null&& (config.SubType == "WEAPON1" || config.SubType == "WEAPON2" || config.SubType == "DAGGER"))
            {
                UIManager.PopPanel<PanelRewardWeaponWithEffect>(null, true, null, LayerType.POP, 1.0f);
            }
            else
            {
                UIManager.PopPanel<PanelRewardItemTipsWithEffect>(null, true, null, LayerType.POP, 1.0f);
            }
        }
        else
        {
            var panel1 = UIManager.GetPanel<PanelRewardItemTipsWithEffect>();
            var panel2 = UIManager.GetPanel<PanelRewardWeaponWithEffect>();
            bool open1 = panel1 != null && panel1.IsOpen();
            bool open2 = panel2 != null && panel2.IsOpen();
            if (!open1 && !open2) //内部逻辑错误导致奖励界面未显示，重显示界面
            {
                isShowing = false;
                isShowing = true;
                var iteminfo = ItemQueue.Peek();
                ConfigItemLine config = null;
                config = (iteminfo is BuffInfo) ? null : ItemDataManager.GetItem(iteminfo.ID);
                if (iteminfo != null && config != null && (config.SubType == "WEAPON1" || config.SubType == "WEAPON2" || config.SubType == "DAGGER"))
                {
                    UIManager.PopPanel<PanelRewardWeaponWithEffect>(null, true, null, LayerType.POP, 1.0f);
                }
                else
                {
                    UIManager.PopPanel<PanelRewardItemTipsWithEffect>(null, true, null, LayerType.POP, 1.0f);
                }
            }
        }
    }

    ItemInfo info;
    public override void OnShow()
    {
        info = ItemQueue.Dequeue();        
        if(info==null)
        {
            OnHide();
            return;
        }        
        if(info is BuffInfo)
        {
            BuffInfo buffinfo = info as BuffInfo;
            m_equipPart.TrySetActive(false);
            m_btn.TrySetActive(true);
            m_tips.text = "已获得增益加成，快点去战斗吧！";
            m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, buffinfo.icon));
            m_icon.SetNativeSize();
            m_count.text = string.Format("{0}", buffinfo.buff_name);
            m_effect.Play();
            return;
        }
        var config = ItemDataManager.GetItem(info.ID);
        var configItemOther = config as ConfigItemOtherLine;
        if (config.SubType == "ARMOR" || config.SubType == "HELMET" || config.SubType == "BOOTS" || config.SubType == "GRENADE" || config.SubType == "FLASHBOMB"||config.SubType=="DECORATION")  //装备
        {
            m_equipPart.TrySetActive(true);
            m_btn.TrySetActive(false);
            m_gotoLottery.TrySetActive(false);
            m_tips.text = "装备全部背包";
        }
        //else if(ItemCheck(info.ID)) //白晶、蓝晶、紫晶
        //{
        //    m_equipPart.TrySetActive(false);
        //    m_btn.TrySetActive(false);
        //    m_tips.text = "奖励已存入仓库，请注意查收";
        //    m_gotoLottery.TrySetActive(true);            
        //}
        else //普通Item
        {
            AudioManager.PlayUISound(AudioConst.lotteryTurnCard);
            m_equipPart.TrySetActive(false);
            m_btn.TrySetActive(true);
            m_tips.text = "奖励已存入仓库，请注意查收";
            if (ItemDataManager.ItemIsHeroCard(config))
            {
                m_tips.text = "超多特权在手，还有【福利-专属英雄每日礼包】等你来领";
            }
            m_gotoLottery.TrySetActive(false);
        }
        m_gotoLottery.TrySetActive(false);
        m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.ICON, config.Icon));        
        m_icon.SetNativeSize();
        m_count.text = string.Format("{0}x{1}", config.DispName, info.cnt);
        if (ItemDataManager.ItemIsHeroCard(config))
        {
            m_count.text = string.Format("{0}", config.DispName); ;
        }
        m_effect.Play();
    }

    private static Queue<int> m_autoUseItemQueue = new Queue<int>();

    public static void AutoUseGiftBag(int id=0)
    {        
        if (id != 0)
            m_autoUseItemQueue.Enqueue(id);
        if (m_autoUseItemQueue.Count==0|| UIManager.IsOpen<PanelTip>() || UIManager.IsOpen<PanelEquipBox>()||
            UIManager.IsOpen<PanelRewardItemTipsWithEffect>()||UIManager.IsOpen<PanelRewardWeaponWithEffect>())
            return;
        int itemID=m_autoUseItemQueue.Dequeue();
        var pitem = ItemDataManager.GetDepotItem(itemID);
        var config = ItemDataManager.GetItem(itemID);
        if(pitem!=null)
        {
            //HidePanel();
            if(UIManager.IsOpen<PanelLotteryNew>()){//军火库抽奖的时候不弹这个
                return;
            }
            string content = string.Format("你刚刚领取了{0},是否立即打开？\n\n\n(也可以自己前往【仓库-其它】打开)",config.DispName);
            UIManager.ShowTipPanel(content, () =>
            {
                ItemDataManager.ResetTempAddedRecord(ItemDataManager.UPDATE_REASON_EQUIP_BOX);
                NetLayer.Send(new tos_player_use_item() { item_uid = pitem.uid });
                ConfigItemOtherLine cfg = ItemDataManager.GetItem(pitem.item_id) as ConfigItemOtherLine;
                if(cfg!=null&&cfg.EffectType.Length==2&&cfg.UseType=="use_giftbag")
                {
                    PanelRewardItemTipsWithEffect.isSkip = true;
                }
            }, () =>
            {
                m_autoUseItemQueue.Clear();
            }, false,true);            
        }
    }

    public override void OnBack()
    {
        
    }

    public override void OnHide()
    {
        if (ItemQueue.Count > 0)
        {
            //HidePanel();
            var config = ItemDataManager.GetItem(ItemQueue.Peek().ID);
            if (config.SubType == "WEAPON1" || config.SubType == "WEAPON2" || config.SubType == "DAGGER")
            {
                UIManager.PopPanel<PanelRewardWeaponWithEffect>(null, true);
            }
            else
            {
                UIManager.PopPanel<PanelRewardItemTipsWithEffect>(null, true);
            }
        }
        else
        {
            isShowing = false;
            //HidePanel();                
            AutoUseGiftBag();
        }
        /*
        if (Driver.m_platform == EnumPlatform.ios)
        {
            if (PanelSevenDayReward.m_isSecondDayGift)
            {
                SdkManager.appNSLog("PanelSevenDayReward.m_isSecondDayGift:" + PanelSevenDayReward.m_isSecondDayGift);
                if (SdkManager.isGoComment())
                {
                    SdkManager.getStarComment();
                }
                PanelSevenDayReward.m_isSecondDayGift = false;
            }
        }*/
    }

    public override void Update()
    {
        
    }

    public override void OnDestroy()
    {
        if(m_effect!=null)
        {
            m_effect.Destroy();
            m_effect = null;
        }
    }

    private static bool ItemCheck(int id)
    {
        bool contain = false;
        for(int i=0;i<m_jintiArr.Length;i++)
        {
            if(m_jintiArr[i]==id)
            {
                contain = true;
                break;
            }
        }
        return contain;
    }

    private static void Sort(ItemInfo[] itemArr)
    {
        int len=itemArr.Length;
        for(int i=0;i<len-1;i++)
        {
            if(ItemCheck(itemArr[i].ID))
            {                
                var move = itemArr[i];
                for(int j=i;j<len-1;j++)
                {
                    itemArr[j] = itemArr[j+1];
                }
                itemArr[len-1] = move;
            }
        }

    }

}
