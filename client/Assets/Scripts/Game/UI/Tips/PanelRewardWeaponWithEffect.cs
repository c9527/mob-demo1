﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelRewardWeaponWithEffect : BasePanel {
    Image m_item_img;
    UIEffect m_effect;
    DataGrid m_bagGroup;
    GameObject m_sureBtn;
    GameObject m_closeBtn;
    GameObject m_item0;
    ItemInfo m_itemInfo;
    Text desc_txt;
    Queue<ItemInfo> ItemQueue
    {
        get
        {
            return PanelRewardItemTipsWithEffect.ItemQueue;
        }
    }
    bool isShowing
    {
        get
        {
            return PanelRewardItemTipsWithEffect.isShowing;
        }
        set
        {
            PanelRewardItemTipsWithEffect.isShowing = value;
        }
    }
    public PanelRewardWeaponWithEffect()
    {
        SetPanelPrefabPath("UI/Common/PanelRewardWeaponWithEffect");

        AddPreLoadRes("UI/Common/PanelRewardWeaponWithEffect", EResType.UI);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_item_img = m_tran.Find("sp_frame/item_img").GetComponent<Image>();
        m_bagGroup = m_tran.Find("HitArea/Text/bagGroup").gameObject.AddComponent<DataGrid>();
        m_item0=m_bagGroup.transform.Find("item0").gameObject;
        m_item0.TrySetActive(false);
        m_bagGroup.SetItemRender(m_item0, typeof(BagItemRender));        
        m_sureBtn = m_tran.Find("HitArea/Text/sure").gameObject;
        m_closeBtn = m_tran.Find("HitArea/Text/close_btn2").gameObject;
        m_effect = UIEffect.ShowEffectAfter(m_item_img.transform,   EffectConst.UI_GETITEM_GETWEAPON, new Vector3(0, 142, 0));
        desc_txt = m_tran.Find("sp_frame/desc_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_sureBtn).onPointerClick += OnSureClick;
        UGUIClickHandler.Get(m_closeBtn).onPointerClick += OnCancelClick;
        m_bagGroup.onItemSelected += OnItemSelected;
    }

    int bagGroupID;
    private void OnItemSelected(object renderData)
    {
        bagGroupID = (int)renderData;
    }

    private void OnCancelClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }
    

    private void OnSureClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        int m_weaponID = ItemDataManager.GetDepotLastItem(m_itemInfo.ID).uid;
        NetLayer.Send(new tos_player_equip_item() { group = bagGroupID, uid = m_weaponID });
        HidePanel();
    }
    public override void OnShow()
    {
        m_itemInfo = ItemQueue.Dequeue();
        if (m_itemInfo == null)
        {
            HidePanel();
            return;
        }
        var cfg = ItemDataManager.GetItem(m_itemInfo.ID);
        m_item_img.SetSprite(ResourceManager.LoadIcon(m_itemInfo.ID),cfg!=null?cfg.SubType:"");        
        m_bagGroup.Data = ItemDataManager.GetGroupsID();
        m_effect.Play();
        AudioManager.PlayUISound(AudioConst.lotteryGetNbWeapon);
        ItemTime time = ItemDataManager.FloatToDayHourMinute(m_itemInfo.day);        
        desc_txt.text = string.Format("{0}{1}", ItemDataManager.GetItem(m_itemInfo.ID).DispName,time.ToString());
    }
    public override void Update()
    {

    }
    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
        if (m_effect != null)
        {
            m_effect.Destroy();
            m_effect = null;
        }
    }

    public override void OnHide()
    {
        if (ItemQueue.Count > 0)
        {
            //HidePanel();
            var config = ItemDataManager.GetItem(ItemQueue.Peek().ID);
            if (config.SubType == "WEAPON1" || config.SubType == "WEAPON2" || config.SubType == "DAGGER")
            {
                UIManager.PopPanel<PanelRewardWeaponWithEffect>(null, true, null, LayerType.POP, 1.0f);
            }
            else
            {
                UIManager.PopPanel<PanelRewardItemTipsWithEffect>(null, true, null, LayerType.POP, 1.0f);
            }            
        }
        else
        {
            isShowing = false;
            PanelRewardItemTipsWithEffect.AutoUseGiftBag();
            //HidePanel(); 
			if(Driver.m_platform == EnumPlatform.ios)
			{
				if(PanelSevenDayReward.m_isSecondDayGift)
				{
					if(SdkManager.isGoComment())
                    {
                        SdkManager.m_logFrom = 3;
						SdkManager.getStarComment();
					}
					PanelSevenDayReward.m_isSecondDayGift=false;
				}	
			}
        }       
    }
}


public class BagItemRender : ItemRender
{
    private int baggroupID;
    Image m_bagNum;
    public override void Awake()
    {
        baggroupID = -1;
        m_bagNum = transform.Find("bagNum").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        baggroupID = (int)data;
        m_bagNum.SetSprite(ResourceManager.LoadSprite(AtlasName.Event, "bag" + baggroupID.ToString()));
    }
}
        

