﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.Scripts.Game.UI;

public enum LayerType
{
    Root,
    Content,
    TV,
    POP,
    Chat,
    Laba,
    Tip,
    Loading,
    AnitAddiction,
    Guide,
}

public static class UIManager
{
    public static Camera m_uiCamera;
    public static Vector2 m_resolution;
    public static RectTransform m_rootCanvas;
    public static RectTransform m_rootLayer;
    private static RectTransform m_contentLayer;
    private static RectTransform m_tvLayer;
    private static RectTransform m_popLayer;
    private static RectTransform m_chatLayer;
    private static RectTransform m_labaLayer;
    public static RectTransform m_guideLayer;
    private static RectTransform m_tipLayer;
    public static RectTransform m_loadingLayer;
    public static RectTransform m_antiAddictionLayer;
    private static Dictionary<string, BasePanel> m_panelDic = new Dictionary<string, BasePanel>();
    private static List<BasePanel> m_panelLoopList = new List<BasePanel>();  //遍历用的列表
    private static BasePanel m_contentPanel;    //当前正在显示的主Panel
    private static BasePanel m_tvPanel;         //当前正在显示的电视Panel
    private static BasePanel m_labaPanel;         //当前正在显示的喇叭Panel
    private static BasePanel m_tipPanel;        //当前正弹出的tipPanel
    private static BasePanel m_maskPanel;       //当前正弹出的maskPanel
    private static BasePanel m_miniTipPanel;       //当前弹出的小提示窗口
    private static PanelLoading m_loadingPanel;       //当前正弹出的loadingPanel
    private static PanelLoadingWithProBar m_loadingWithProBar; //带ProBar的LoadingPanel
    private static int m_openDay;               //> 用作一些界面隔天更新数据
    private static float m_fCalcOtherDayDelta;
    private static float CALC_ORTHER_DAY_DELTA = 5f;
    public static void Init()
    {
        int ManualWildth = 960;
        int ManualHeighth = 640;
        m_resolution = new Vector2(ManualWildth, ManualHeighth);

        var eventSystemGo = new GameObject("EventSystem");
        eventSystemGo.AddComponent<StandaloneInputModule>();
        eventSystemGo.AddComponent<TouchInputModule>();
        var eventSystem = eventSystemGo.GetComponent<EventSystem>();
        eventSystem.sendNavigationEvents = false;
        //        eventSystem.pixelDragThreshold = 5;
        GameObject.DontDestroyOnLoad(eventSystemGo);

        m_uiCamera = new GameObject("UICamera").AddComponent<Camera>();
        m_uiCamera.cullingMask = GameSetting.LAYER_MASK_UI;
        m_uiCamera.orthographic = true;
        initUICameraSize(m_uiCamera);
        m_uiCamera.nearClipPlane = -100;
        m_uiCamera.farClipPlane = 100;
        m_uiCamera.depth = 10;
        m_uiCamera.useOcclusionCulling = false; //关掉遮挡剔除
        m_uiCamera.backgroundColor = Color.black;
        m_uiCamera.clearFlags = CameraClearFlags.Depth;

        GameObject.DontDestroyOnLoad(m_uiCamera.gameObject);

        var rootCanvasGo = new GameObject("UGUIRootCanvas");
        rootCanvasGo.layer = LayerMask.NameToLayer("UI");
        var canvas = rootCanvasGo.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = m_uiCamera;
        canvas.pixelPerfect = false;
        canvas.planeDistance = 0;
        var scaler = rootCanvasGo.AddComponent<CanvasScaler>();
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.referenceResolution = m_resolution;
        scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        rootCanvasGo.AddComponent<GraphicRaycaster>();
        GameObject.DontDestroyOnLoad(rootCanvasGo);
        m_rootCanvas = rootCanvasGo.GetRectTransform();

        //添加层级，不要轻易改动顺序
        m_rootLayer = AddLayer("rootLayer");
        m_contentLayer = AddLayer("contentLayer");
        m_popLayer = AddLayer("popLayer");
        m_tvLayer = AddLayer("tvLayer");
        m_chatLayer = AddLayer("chatLayer");
        m_labaLayer = AddLayer("labaLayer");
        m_tipLayer = AddLayer("tipLayer");
        m_guideLayer = AddLayer("guideLayer");
        m_antiAddictionLayer = AddLayer("antiaddiction");
        m_loadingLayer = AddLayer("loadingLayer");
        m_openDay = TimeUtil.GetNowTime().Day;
        m_fCalcOtherDayDelta = 0f;

        UISystem.Init();
    }

    //用于显示简单的规则界面
    //规则与对应的标题配置在ConfigRuleIntro中，ruleTarget
    public static void PopRulePanel(string ruleTarget)
    {
        object[] obj = new object[1];
        obj[0] = ruleTarget;
        PopPanel<PanelRuleBase>(obj, true);
    }

    public static bool fullScreen
    {
        set
        {
            if (value)
            {
                Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
            }
            else
            {
                Screen.SetResolution((int)m_resolution.x, (int)m_resolution.y, false);
            }
        }
        get
        {
            return Screen.fullScreen;
        }
    }

    static public RectTransform GetLayer(LayerType value)
    {
        RectTransform result = null;
        switch (value)
        {
            case LayerType.Root:
                result = m_rootLayer;
                break;
            case LayerType.Content:
                result = m_contentLayer;
                break;
            case LayerType.TV:
                result = m_tvLayer;
                break;
            case LayerType.Chat:
                result = m_chatLayer;
                break;
            case LayerType.Laba:
                result = m_labaLayer;
                break;
            case LayerType.Tip:
                result = m_tipLayer;
                break;
            case LayerType.POP:
                result = m_popLayer;
                break;
            case LayerType.Loading:
                result = m_loadingLayer;
                break;
            case LayerType.AnitAddiction:
                result = m_antiAddictionLayer;
                break;
            case LayerType.Guide:
                result = m_guideLayer;
                break;
        }
        return result;
    }

    public static void initUICameraSize(Camera m_camera)
    {
        var result = System.Convert.ToSingle(Screen.width) / System.Convert.ToSingle(Screen.height);
        if (result < 1.5f)
            m_camera.orthographicSize = 15.0f / result;
        else
            m_camera.orthographicSize = 10;
    }

    private static RectTransform AddLayer(string name)
    {
        var go = new GameObject(name);
        go.AddComponent<CanvasRenderer>();
        var rect = go.AddComponent<RectTransform>();
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.sizeDelta = Vector2.zero;
        rect.SetUILocation(m_rootCanvas);
        return rect;
    }

    /// <summary>
    /// 根据字符串跳转到对应系统界面xxxxx#dddd#ffff
    /// </summary>
    /// <param name="panelKeyName"></param>
    /// <param name="gameRule"></param>房间类型
    /// <returns></returns>
    public static bool GotoPanel(string panelKeyName, string gameRule = null)
    {
        string[] keyArray = panelKeyName.Split(new char[] { '#' });
        if (LuaHook.CheckHook(HookType.UI_Goto_Before, 0, keyArray, gameRule))
            return true;
        
        switch (keyArray[0])
        {
            case "battle":
                //RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.pvp, new[] { "new", "adv" });
                RoomModel.SendChannelChoose(gameRule);
               
                break;
            case "rank":
            case "rankteam":
                {
                    if (PlayerSystem.IsRankMatchOpen())
                    {
                        if (keyArray.Length <= 1)
                            UIManager.ShowPanel<PanelTeamMode>();
                        else
                        {
                            RoomModel.matchType = int.Parse(keyArray[1]);
                            RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.match);
                            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.m_channel, stage = 0 });
                        }
                    }
                    else
                    {
                        TipsManager.Instance.showTips("排位竞技15级解锁");
                    }
                    break;
                }
            case "survival":
                RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.survival);
                if (gameRule == null)
                    ShowPanel<PanelSurvivalRoomList>();
                else
                    UIManager.ShowPanel<PanelSurvivalRoomList>(new[] { gameRule });
                break;
            case "hallbattle": ShowPanel<PanelHallBattle>(); break;
            case "chat": PopChatPanel<PanelChatSmall>(); break;
            case "convert": ShowPanel<PanelStrengthenMain>(new object[] { "conver_btn" }); break;
            case "handbook": ShowPanel<PanelStrengthenMain>(new object[] { "handbook_btn" }); break;
            case "train": ShowPanel<PanelStrengthenMain>(new object[] { "train_btn" }); break;
            case "depot": ShowPanel<PanelDepot>(); break;
            case "mall": ShowPanel<PanelMall>(); break;
            case "mall_event": ShowPanel<PanelMall>(new object[] {"mall_event"}); break;
            case "corps": ShowPanel<PanelSocity>(keyArray); break;
            case "ranklist": PanelRankList.OpenPanel(); break;//排行榜
            //case "recharge": UIManager.PopPanel<PanelRecharge>(null, true, null); break;//充值
            case "recharge": SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE); break;
            //            case "lottery": UIManager.ShowPanel<PanelLottery>(null); break;//军火库
            case "lottery": UIManager.ShowPanel<PanelLotteryNew>(keyArray); break;//军火库
            case "activity": UIManager.ShowPanel<PanelLoginReward>(); break;//福利
            case "mission": UIManager.ShowPanel<PanelDailyMission>(); break;//任务
            case "role": UIManager.ShowPanel<PanelRole>(); break; //仓库-角色
            case "friend": UIManager.ShowPanel<PanelSocity>(new object[] { "friend" }); break;
            case "shareshop": UIManager.ShowPanel<PanelLoginReward>(new object[] { "shareshop" }); break;
            case "boss":
                PanelRoomBase.RoomMinimizeCheck(() =>
                {
                    if (!RoomModel.IsInRoom)
                    {
                        if (PanelWorldBoss.IsOpenEvent() == 1)
                            UIManager.ShowTipPanel("活动未开启");
                        else
                            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.worldboss), stage = 0 });
                    }
                    else
                        UIManager.ShowPanel<PanelWorldBoss>();
                });
                break;
            case "boss4newer":
                PanelRoomBase.RoomMinimizeCheck(() =>
                {
                    if (!RoomModel.IsInRoom)
                    {
                            NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.boss4newer), stage = 0 });
                    }
                    else
                        UIManager.ShowPanel<PanelWorldBoss>(new object[] { "boss4newer" });
                });
                break;
            case "defend"://直接跳转到生存模式界面
                RoomModel.m_channel = RoomModel.FindChannelByType(ChannelTypeEnum.survival);
                if (gameRule == null)
                    UIManager.ShowPanel<PanelSurvivalRoomList>();
                else
                    UIManager.ShowPanel<PanelSurvivalRoomList>(new[] { gameRule });
                break;
            case "pve":
                if (keyArray.Length == 1)
                    UIManager.ShowPanel<PanelLvlPass>();
                else
                {
                    int id = 0;
                    if (int.TryParse(keyArray[1], out id))
                    {
                        UIManager.ShowPanel<PanelLvlPass>(new object[] { id });
                    }
                }
                break;
            case "panelPVE":
                UIManager.ShowPanel<PanelPVE>();
                break;
            case "corpsEnter":
                UIManager.ShowPanel<PanelCorpsEnter>();
                break;
            case "legion":
                NetLayer.Send(new tos_legionwar_data() { });
                NetLayer.Send(new tos_legionwar_my_info() { });
                break;
            case "OnLine":
                UIManager.ShowPanel<PanelLoginReward>(new object[1]{"online"});
                break;
            case "Sign":
                UIManager.ShowPanel<PanelLoginReward>(new object[1] { "sign" });
                break;
            case "NewSign":
                UIManager.PopPanel<PanelSevenDayReward>(null, true);
                break;
            case "corpsMission":
                if (CorpsDataManager.IsJoinCorps())
                    UIManager.ShowPanel<PanelCorpsTask>();
                else
                    UIManager.ShowPanel<Assets.Scripts.Game.UI.Corps.PanelCorpsList>();
                break;
            default:
                if (LuaHook.CheckHook(HookType.UI_Goto_After, 0, keyArray, gameRule))
                    return true;
                return false;
        }

        if (LuaHook.CheckHook(HookType.UI_Goto_After, 0, keyArray, gameRule))
            return true;
        return true;
    }

    /// <summary>
    /// 设置对象的鼠标穿透
    /// </summary>
    /// <param name="go"></param>
    /// <param name="enableClick"></param>
    public static void SetClickEvent(GameObject go, bool enableClick)
    {
        var canvasGroup = go.AddMissingComponent<CanvasGroup>();
        canvasGroup.blocksRaycasts = enableClick;
    }

    #region Panel相关操作方法
    public static void HidePanel<T>() where T : BasePanel, new()
    {
        var panel = GetPanel<T>();
        if (panel != null)
            panel.HidePanel();
    }

    public static void HidePanel(string panelName)
    {
        var panel = GetPanel(panelName);
        if (panel != null)
            panel.HidePanel();
    }
    /// <summary>
    /// 切换显示主要面板
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="params">参数数组</param>
    /// <returns></returns>
    public static T ShowPanel<T>(object[] @params = null) where T : BasePanel, new()
    {
        int filter = TypeFilter(typeof(T));
        if (filter == 1)
        {
            PanelRoomBase.RoomMinimizeCheck(() =>
            {
                ShowPanel<T>(@params);
            });
            return null;
        }
        else if (filter == -1)
            return null;
        var panelName = typeof(T).Name;
        var type = typeof(T);
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }
        //if (m_panelDic[panelName].IsOpen())
        //    return m_panelDic[panelName] as T;

        if (m_contentPanel != null)
        {
            ModelDisplay.MoveOutCamera();
            m_contentPanel.HidePanel();
        }
        m_contentPanel = m_panelDic[panelName];

        var panel = m_panelDic[panelName];
        panel.SetParams(@params);
        panel.ShowPanel(m_contentLayer);
        return panel as T;
    }



    public static void ShowPanel(BasePanel panel, object[] @params = null)
    {
        if (panel.IsOpen())
            return;

        if (m_contentPanel != null)
        {
            ModelDisplay.MoveOutCamera();
            m_contentPanel.HidePanel();
        }
        m_contentPanel = panel;

        m_contentPanel.SetParams(@params);
        m_contentPanel.ShowPanel(m_contentLayer);
    }

    /// <summary>
    /// 显示根面板
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static void PopRootPanel<T>() where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_panelDic[panelName].IsOpen())
            return;

        m_panelDic[panelName].ShowPanel(m_rootLayer);
    }

    /// <summary>
    /// 在pop层弹出窗口并置顶，该层需要自己维护显示隐藏
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="params"></param>
    /// <param name="addMask"></param>
    /// <param name="parentPanel"></param>
    public static T PopPanel<T>(object[] @params = null, bool addMask = false, BasePanel parentPanel = null, LayerType layer = LayerType.POP, float maskAlpha = 0.75f) where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        var popPanel = m_panelDic[panelName];
        popPanel.SetParams(@params);
        popPanel.ShowPanel(GetLayer(layer), addMask, null, maskAlpha);
        if (parentPanel != null)
        {
            parentPanel.AddChildPanel(popPanel);
            popPanel.SetParent(parentPanel);
        }

        return popPanel as T;
    }

    /// <summary>
    /// 隐藏弹出面板
    /// </summary>
    /// <param name="panel"></param>
    public static void HidePopPanelExcept(List<string> exceptArray)
    {
        RectTransform popLayer = GetLayer(LayerType.POP);
        int childNum = popLayer.childCount;
        Transform tempTrans;
      
        string panelName;
        for (int i = 0; i < childNum; i++)
        {
            tempTrans = popLayer.GetChild(i);
            panelName = tempTrans.gameObject.name;
            if (exceptArray.IndexOf(panelName) == -1)
            {
                if (IsOpen(panelName))
                    GetPanel(panelName).HidePanel();
            }
        }
    }

    /// <summary>
    /// 在chat层弹出窗口并置顶，该层需要自己维护显示隐藏
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="params"></param>
    /// <param name="addMask"></param>
    /// <param name="parentPanel"></param>
    public static T PopChatPanel<T>(object[] @params = null, bool addMask = false, BasePanel parentPanel = null) where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        var popPanel = m_panelDic[panelName];
        popPanel.SetParams(@params);
        popPanel.ShowPanel(m_chatLayer, addMask);
        if (parentPanel != null)
        {
            parentPanel.AddChildPanel(popPanel);
            popPanel.SetParent(parentPanel);
        }

        return popPanel as T;
    }

    public static void ShowNoThisFunc()
    {
        if (!Driver.isMobilePlatform)
        {
            if (!UIManager.IsOpen<PanelBattle>())
            {
                ShowTipPanel("此平台版本不支持该功能");
            }
            else
            {
                TipsManager.Instance.showTips("此平台版本不支持该功能");
            }
        }
        else
        {
            if (!UIManager.IsOpen<PanelBattle>())
            {
                ShowTipPanel("自动更新版本不支持该功能，请到应用商店下载最新安装包");
            }
            else
            {
                TipsManager.Instance.showTips("自动更新版本不支持该功能，请到应用商店下载最新安装包");
            }
        }
    }

    /// <summary>
    /// 悬浮小提示窗
    /// </summary>
    /// <param name="message"></param>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static PanelMiniTip ShowMiniTipPanel(MiniTipsData message = null, Vector2 pos = new Vector2())
    {
        
        var panelName = typeof(PanelMiniTip).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelMiniTip());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }
        if (m_miniTipPanel != null)
        {
            m_miniTipPanel.HidePanel();
        }
        if (message == null)
        {
            if (m_miniTipPanel != null)
                m_miniTipPanel.HidePanel();
        }
        else
        {
            m_miniTipPanel = m_panelDic[panelName];
            m_miniTipPanel.SetParams(new object[] { message, pos });
            m_miniTipPanel.ShowPanel(m_tipLayer, false);
        }
        return m_miniTipPanel as PanelMiniTip;
    }

    /// <summary>
    /// 使用默认的信息提示窗
    /// </summary>
    /// <param name="message">消息内容</param>
    /// <param name="onOk">按钮回调</param>
    /// <param name="onCancel">按钮回调</param>
    /// <param name="showCloseBtn">是否显示关闭按钮</param>
    /// <param name="showCancel">是否显示取消按钮</param>
    /// <param name="okLabel">确定按钮的文字</param>
    /// <param name="cancelLabel">取消按钮的文字</param>
    /// <param name="countDownSec"></param>
    /// <param name="countDownType"></param>
    /// <param name="title"></param>
    /// <param name="canScroll"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="leftAlign"></param>
    /// <param name="topAlign"></param>
    private static T ShowTipPanel<T>(string message, Action onOk = null, Action onCancel = null,
        bool showCloseBtn = true, bool showCancel = false, string okLabel = null, string cancelLabel = null,
        float countDownSec = 0f, int countDownType = 0, string title = null, bool canScroll = false, float width = 0, float height = 0, bool leftAlign = false, bool topAlign = false) where T:PanelTip, new()
    {
        if (string.IsNullOrEmpty(message))
            return null;
        var panelName = typeof (T).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_tipPanel != null)
            m_tipPanel.HidePanel();

        var param = new TipPanelParams();
        param.title = title;
        param.message = message;
        param.leftAlign = leftAlign;
        param.topAlign = topAlign;
        param.canScroll = canScroll;
        param.width = width;
        param.height = height;
        param.onOk = onOk;
        param.onCancel = onCancel;
        param.showCancel = showCancel;
        param.showCloseBtn = showCloseBtn;
        param.okLabel = okLabel;
        param.cancelLabel = cancelLabel;
        param.countDownSec = countDownSec;
        param.countDownType = countDownType;

        m_tipPanel = m_panelDic[panelName];
        m_tipPanel.SetParams(new object[] {param});
        m_tipPanel.ShowPanel(m_tipLayer, true);

        return m_tipPanel as T;
    }

    private static T ShowTipPanelInLayer<T>(string message, RectTransform layer,Action onOk = null, Action onCancel = null,
        bool showCloseBtn = true, bool showCancel = false, string okLabel = null, string cancelLabel = null,
        float countDownSec = 0f, int countDownType = 0, string title = null, bool canScroll = false, float width = 0, float height = 0, bool leftAlign = false, bool topAlign = false) where T : PanelTip, new()
    {
        if (string.IsNullOrEmpty(message))
            return null;
        var panelName = typeof(T).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new T());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_tipPanel != null)
            m_tipPanel.HidePanel();

        var param = new TipPanelParams();
        param.title = title;
        param.message = message;
        param.leftAlign = leftAlign;
        param.topAlign = topAlign;
        param.canScroll = canScroll;
        param.width = width;
        param.height = height;
        param.onOk = onOk;
        param.onCancel = onCancel;
        param.showCancel = showCancel;
        param.showCloseBtn = showCloseBtn;
        param.okLabel = okLabel;
        param.cancelLabel = cancelLabel;
        param.countDownSec = countDownSec;
        param.countDownType = countDownType;

        m_tipPanel = m_panelDic[panelName];
        m_tipPanel.SetParams(new object[] { param });
        m_tipPanel.ShowPanel(layer, true);

        return m_tipPanel as T;
    }



    public static void SetRoleIconOrSelfIcon(this Image img, int icon, string url = "", int camp = 0)
    {
#if UNITY_ANDROID || UNITY_IPHONE
        if (!String.IsNullOrEmpty(url))
        {
            img.SetRoleIcon(url);
            return;
        }
#endif
        img.SetRoleIcon(icon,camp);
        //        img.SetNativeSize();
    }


    public static void SetRoleIcon(this Image img, int icon, int camp = 0)
    {
        img.SetSprite(ResourceManager.LoadRoleIcon(icon, camp));
//        img.SetNativeSize();
    }

    public static void SetRoleIcon(this Image img, string url)
    {
        img.rectTransform.sizeDelta = new Vector2(48, 48);
        ResourceManager.LoadCacheSprite(url, (sprite) => { img.sprite = sprite; img.material = null; });
    }

    public static void SetRoleIconBig(this Image img, int icon, int camp = 0)
    {
        img.SetSprite(ResourceManager.LoadRoleIconBig(icon, camp));
//        img.SetNativeSize();
    }

    public static void SetRoleIconBig(this Image img, string url)
    {
        img.rectTransform.sizeDelta = new Vector2(90, 90);
        ResourceManager.LoadCacheSprite(url, (sprite) => { img.sprite = sprite;img.material = null;});
    }

    public static void SetRoleBigIconOrSelfIcon(this Image img, int icon, string url = "", int camp = 0)
    {
#if UNITY_ANDROID || UNITY_IPHONE
        if (!String.IsNullOrEmpty(url))
        {
            img.SetRoleIconBig(url);
            return;
        }
#endif
        img.SetRoleIconBig(icon, camp);
        //        img.SetNativeSize();
    }

    /// <summary>
    /// 旧的入口
    /// </summary>
    public static PanelTip ShowTipPanel(string message, Action onOk, Action onCancel = null,
        bool showCloseBtn = true, bool showCancel = false, string okLabel = null, string cancelLabel = null, float countDownSec = 0f, int countDownType = 0
        )
    {
        return ShowTipPanel<PanelTip>(message, onOk, onCancel, showCloseBtn, showCancel, okLabel, cancelLabel, countDownSec, countDownType);
    }

    public static PanelTip ShowTipPanelInLayer(string message,RectTransform layer  ,string title = null)
    {
        return ShowTipPanelInLayer<PanelTip>(message,layer, null, null, false, false, null, null, 0, 0, title);
    }


    /// <summary>
    /// 少量文本提示框，不需要按钮回调，比较小，不支持文本滚动，文本居中
    /// </summary>
    public static PanelTip ShowTipPanel(string message, string title = null)
    {
        return ShowTipPanel<PanelTip>(message, null, null, false, false, null, null, 0, 0, title);
    }



    /// <summary>
    /// 大量文本信息提示框，不需要按钮回调，比较大，支持文本滚动，文本左上对齐
    /// </summary>
    public static PanelTipMulText ShowTipMulTextPanel(string message, string title = null, float width = 610, float height = 440)
    {
        return ShowTipPanel<PanelTipMulText>(message, null, null, false, false, null, null, 0, 0, title, true, width, height, true, true);
    }

    /// <summary>
    /// 隐藏tipPanel
    /// </summary>
    public static void HideTipPanel()
    {
        if (m_tipPanel != null)
            m_tipPanel.HidePanel();
    }

    private const float CLEAR_MAX_TIME = 8;
    private static float m_clearWatchDog;
    private static int m_count;
    public static void ShowMask(string message, float clearTime = CLEAR_MAX_TIME)
    {
        var panelName = typeof(PanelMask).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelMask());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_maskPanel != null)
            m_maskPanel.HidePanel();

        m_count++;
        m_clearWatchDog = clearTime;
        m_maskPanel = m_panelDic[panelName];
        m_maskPanel.SetParams(new object[] { message });
        m_maskPanel.ShowPanel(m_tipLayer, true);
    }

    public static void HideMask()
    {
        m_clearWatchDog = CLEAR_MAX_TIME;
        m_count = Mathf.Max(0, --m_count);
        if (m_count == 0 && m_maskPanel != null)
            m_maskPanel.HidePanel();
    }

    public static void ShowLoading(List<ResourceItem> resources, Action callBack = null)
    {
        var panelName = typeof(PanelLoading).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelLoading());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_loadingPanel != null)
            m_loadingPanel.HidePanel();

        m_loadingPanel = m_panelDic[panelName] as PanelLoading;
        m_loadingPanel.DontDestroyOnLoad = true;
        m_loadingPanel.SetParams(new object[] { resources.ToArray(), callBack });
        m_loadingPanel.ShowPanel(m_loadingLayer);
    }
    /// <summary>
    /// 带进度条的Loading图
    /// </summary>
    /// <param name="resoruces"></param>
    /// <param name="callBack"></param>
    public static void ShowLoadingWithProBar(List<ResourceItem> resoruces, Action callBack = null)
    {
        var panelName = typeof(PanelLoadingWithProBar).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelLoadingWithProBar());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_loadingWithProBar != null)
            m_loadingWithProBar.HidePanel();

        m_loadingWithProBar = m_panelDic[panelName] as PanelLoadingWithProBar;
        m_loadingWithProBar.DontDestroyOnLoad = true;
        m_loadingWithProBar.SetParams(new object[] { resoruces.ToArray(), callBack });
        m_loadingWithProBar.ShowPanel(m_loadingLayer, true, null, 1);

    }

    /// <summary>
    /// 上电视
    /// </summary>
    /// <param name="message"></param>
    public static void ShowTv(string message)
    {
        if (IsOpen<PanelLegionMap>())
        {
            return;
        }
        PanelTv.AddMsg(message);

        var panelName = typeof(PanelTv).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelTv());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_panelDic[panelName].IsOpen())
            return;

        if (m_tvPanel != null)
            m_tvPanel.HidePanel();
        m_tvPanel = m_panelDic[panelName];

        m_tvPanel.ShowPanel(m_tvLayer);
    }

    /// <summary>
    /// 上喇叭
    /// </summary>
    /// <param name="record"></param>
    public static void ShowLaba(ChatRecord record)
    {
        PanelLaba.AddMsg(record);

        var panelName = typeof(PanelLaba).Name;
        if (!m_panelDic.ContainsKey(panelName))
        {
            m_panelDic.Add(panelName, new PanelLaba());
            m_panelLoopList.Add(m_panelDic[panelName]);
        }

        if (m_panelDic[panelName].IsOpen())
            return;

        if (m_labaPanel != null)
            m_labaPanel.HidePanel();
        m_labaPanel = m_panelDic[panelName];

        m_labaPanel.ShowPanel(m_labaLayer);
    }

    private static RectTransform m_tranDropList;
    private static RectTransform m_tranDropListNormal;
    private static RectTransform m_tranDropListEx;
    private static RectTransform m_tranDropListPop;
    private static DataGrid m_dataGridDropList;
    private static GridLayoutGroup m_gridLayoutGroup;
    private static Action<ItemDropInfo> m_dropListClickCallBack;
    private static ItemDropInfo[] m_lastItemDropInfos;
    private static BasePanel m_lastShowPanel;
    private static object[] m_lastShowPanelParam;

    public enum Direction
    {
        Up,
        Down,
    }

    public enum DropListPattern
    {
        Normal,
        Ex,
        Pop
    }

    public static void SaveLastShowPanel(BasePanel panel, object[] param)
    {
        m_lastShowPanel = panel;
        m_lastShowPanelParam = param;
    }

    public static bool ShowLastPanel()
    {
        if (m_lastShowPanel != null)
        {
            ShowPanel(m_lastShowPanel, m_lastShowPanelParam);
            m_lastShowPanel = null;
            m_lastShowPanelParam = null;
            return true;
        }
        return false;
    }

    public static void ShowDropListUp(ItemDropInfo[] list, Action<ItemDropInfo> callBack, Transform parent, GameObject targetGameObject, float itemWidth = 90, float itemHeight = 50, DropListPattern pattern = DropListPattern.Normal)
    {
        var targetRectTran = targetGameObject.GetComponent<RectTransform>();
        var offset = new Vector2(0, targetRectTran.sizeDelta.y*0.5f);
        if (pattern == DropListPattern.Pop)
        {
            offset.y = -targetRectTran.sizeDelta.y*0.5f;
        }
        ShowDropList(list, callBack, parent, targetRectTran, offset, Direction.Up, itemWidth, itemHeight, pattern);
    }

    public static void ShowDropList(ItemDropInfo[] list, Action<ItemDropInfo> callBack, Transform parent, GameObject targetGameObject, float itemWidth = 90, float itemHeight = 50, DropListPattern pattern = DropListPattern.Normal)
    {
        var targetRectTran = targetGameObject.GetComponent<RectTransform>();
        var offset = new Vector2(0, targetRectTran.sizeDelta.y * -0.5f);
        if (pattern == DropListPattern.Pop)
        {
            offset.y = targetRectTran.sizeDelta.y * 0.5f;
        }
        ShowDropList(list, callBack, parent, targetRectTran, offset, Direction.Down, itemWidth, itemHeight, pattern);
    }

    public static void ShowDropList(ItemDropInfo[] list, Action<ItemDropInfo> callBack, Transform parent, Transform posTarget, Vector2 offset, Direction dir = Direction.Down, float itemWidth = 90, float itemHeight = 50, DropListPattern pattern = DropListPattern.Normal)
    {
        //相同数据则关闭列表
        if (list.Length == 0 || list == m_lastItemDropInfos)
        {
            HideDropList();
            m_lastItemDropInfos = null;
            return;
        }
        m_lastItemDropInfos = list;
        switch (pattern)
        {
            case DropListPattern.Normal:
                m_tranDropList = m_tranDropListNormal;
                break;
            case DropListPattern.Ex:
                m_tranDropList = m_tranDropListEx;
                break;
            case DropListPattern.Pop:
                m_tranDropList = m_tranDropListPop;
                break;
            default:
                m_tranDropList = m_tranDropListNormal;
                break;
        }

        if (m_tranDropList == null)
        {
            switch (pattern)
            {
                case DropListPattern.Normal:
                    m_tranDropListNormal = ResourceManager.LoadUI("UI/Common/DropList/DropList").GetComponent<RectTransform>();
                    m_tranDropList = m_tranDropListNormal;
                    break;
                case DropListPattern.Ex:
                    m_tranDropListEx = ResourceManager.LoadUI("UI/Common/DropList/DropListEx").GetComponent<RectTransform>();
                    m_tranDropList = m_tranDropListEx;
                    break;
                case DropListPattern.Pop:
                    m_tranDropListPop = ResourceManager.LoadUI("UI/Common/DropList/PopList").GetComponent<RectTransform>();
                    m_tranDropList = m_tranDropListPop;
                    break;
                default:
                    m_tranDropListNormal = ResourceManager.LoadUI("UI/Common/DropList/DropList").GetComponent<RectTransform>();
                    m_tranDropList = m_tranDropListNormal;
                    break;
            }

            m_gridLayoutGroup = m_tranDropList.GetComponent<GridLayoutGroup>();
            m_dataGridDropList = m_tranDropList.gameObject.AddComponent<DataGrid>();
            m_dataGridDropList.autoSelectFirst = false;

            switch (pattern)
            {
                case DropListPattern.Normal:
                    m_dataGridDropList.SetItemRender(ResourceManager.LoadUIRef("UI/Common/DropList/ItemDropListRender"), typeof(ItemDropListRender));
                    break;
                case DropListPattern.Ex:
                    m_dataGridDropList.SetItemRender(ResourceManager.LoadUIRef("UI/Common/DropList/ItemDropListRenderEx"), typeof(ItemDropListRender));
                    break;
                case DropListPattern.Pop:
                    m_dataGridDropList.SetItemRender(ResourceManager.LoadUIRef("UI/Common/DropList/ItemPop"), typeof(ItemDropListRender));
                    break;
                default:
                    m_dataGridDropList.SetItemRender(ResourceManager.LoadUIRef("UI/Common/DropList/ItemDropListRender"), typeof(ItemDropListRender));
                    break;
            }
            //m_dataGridDropList.onItemSelected = OnItemSelected;
        }

        m_dropListClickCallBack = callBack;
        m_dataGridDropList.Data = list;
        m_tranDropList.SetParent(parent, false);
        m_tranDropList.position = posTarget.position;
        m_tranDropList.anchoredPosition += offset;
        m_tranDropList.gameObject.TrySetActive(true);
        m_dataGridDropList.onItemSelected = OnItemSelected;
        m_tranDropList.SetAsLastSibling();
        if (pattern != DropListPattern.Normal)
            m_tranDropList.sizeDelta = new Vector2(itemWidth + 25, 0);
        else
            m_tranDropList.sizeDelta = new Vector2(itemWidth, 0);

        m_gridLayoutGroup.cellSize = new Vector2(itemWidth, itemHeight);
        if (pattern == DropListPattern.Ex)
        {
            m_gridLayoutGroup.padding = new RectOffset(10, 0, 10, 10);
        }

        if (dir == Direction.Up)
        {
            m_tranDropList.anchoredPosition += new Vector2(0,
                (m_gridLayoutGroup.cellSize.y + m_gridLayoutGroup.spacing.y) * list.Length - (list.Length > 0 ? m_gridLayoutGroup.spacing.y : 0));
            
        }
        if (pattern == DropListPattern.Pop)
        {
            var spriteName = dir == Direction.Up ? "poplist_bg_down" : "poplist_bg_up";
            m_tranDropList.GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Common, spriteName));
        }
            
        SortingOrderRenderer.RebuildAll();
    }

    public static void HideDropList()
    {
        m_lastItemDropInfos = null;
        if (m_tranDropList != null)
            m_tranDropList.gameObject.TrySetActive(false);
    }

    private static void OnItemSelected(object renderData)
    {
        HideDropList();
        if (m_dropListClickCallBack != null)
            m_dropListClickCallBack(renderData as ItemDropInfo);
    }

    /// <summary>
    /// 执行返回
    /// </summary>
    public static void BackPanel()
    {
        if (m_contentPanel != null)
            m_contentPanel.OnBack();
    }

    /// <summary>
    /// 判断是否存在指定UI，但不一定是打开状态（可用IsOpen方法判断）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static bool HasPanel<T>() where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        return m_panelDic.ContainsKey(panelName);
    }

    public static bool IsOpen<T>() where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        return m_panelDic.ContainsKey(panelName) && m_panelDic[panelName].IsOpen();
    }

    public static bool IsOpen(string panelName)
    {
        return m_panelDic.ContainsKey(panelName) && m_panelDic[panelName].IsOpen();
    }

    public static bool IsInited<T>() where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        return m_panelDic.ContainsKey(panelName) && m_panelDic[panelName].IsInited();
    }

    public static T GetPanel<T>() where T : BasePanel, new()
    {
        var panelName = typeof(T).Name;
        if (m_panelDic.ContainsKey(panelName))
            return m_panelDic[panelName] as T;
        return null;
    }

    public static BasePanel GetPanel(string panelName)
    {
        if (m_panelDic.ContainsKey(panelName))
            return m_panelDic[panelName];
        return null;
    }

    //    public static void DestroyPanel<T>() where T : BasePanel, new()
    //    {
    //        var panelName = typeof(T).Name;
    //        if (m_panelDic.ContainsKey(panelName))
    //        {
    //            m_panelDic[panelName].DestroyPanel();
    //            m_panelDic.Remove(panelName);
    //        }
    //    }

    public static bool IsBattle()
    {
        return IsOpen<PanelBattle>() || Application.loadedLevelName.StartsWith("map_");
    }

    public static void DestroyAllPanel()
    {
        var dontDestroyPanelList = new List<KeyValue<string, BasePanel>>();
        foreach (var kv in m_panelDic)
        {
            if (!kv.Value.DontDestroyOnLoad)
                kv.Value.DestroyPanel();
            else
                dontDestroyPanelList.Add(new KeyValue<string, BasePanel>(kv.Key, kv.Value));
        }
        m_panelDic.Clear();
        m_panelLoopList.Clear();

        for (int i = 0; i < dontDestroyPanelList.Count; i++)
        {
            var panel = dontDestroyPanelList[i];
            m_panelDic.Add(panel.key, panel.value);
            m_panelLoopList.Add(panel.value);
        }

        m_contentPanel = null;
        //        m_tvPanel = null;
        //m_tipPanel全局存在，不应该清理,否则容易造成控制权丢失，无法关掉弹出的窗口
        //m_tipPanel = null;

        AudioManager.Clear();
    }

    public static void DestroyUnusedPanel()
    {
        var list = new List<string>();
        foreach (var kv in m_panelDic)
        {
            if (kv.Value.IsOpen())
                continue;
            list.Add(kv.Key);
            kv.Value.DestroyPanel();
        }
        for (int i = 0; i < list.Count; i++)
        {
            m_panelDic.Remove(list[i]);
        }
        list.Clear();
    }

    public static void Update()
    {
        m_fCalcOtherDayDelta += Time.deltaTime;
        if (m_fCalcOtherDayDelta > CALC_ORTHER_DAY_DELTA)
        {
            int curDay = TimeUtil.GetNowTime().Day;
            if (curDay != m_openDay)
            {
                NetLayer.Send(new tos_player_event_list());
                m_openDay = curDay;
            }
            m_fCalcOtherDayDelta = 0f;
        }
        if (m_clearWatchDog > 0)
        {
            m_clearWatchDog -= Time.deltaTime;
            if (m_clearWatchDog <= 0)
            {
                m_clearWatchDog = 0;
                m_count = 0;
                HideMask();
            }
        }
        for (int i = 0; i < m_panelLoopList.Count; i++)
        {
            if (m_panelLoopList[i].IsInited())
                m_panelLoopList[i].Update();
        }
    }

    public static BasePanel CurContentPanel
    {
        get { return m_contentPanel; }
        set
        {
            m_contentPanel = value;
        }
    }


    /// <summary>
    /// 在ShowPanel之前进行特殊条件下类型过滤，根据返回值进行合适的操作
    /// </summary>
    /// <param name="type"></param>
    /// <returns>-1：终止后续执行，0：什么都不做,1最小化房间确认</returns>
    private static int TypeFilter(Type type)
    {
        int result = 0;
        PanelHall hall = UIManager.GetPanel<PanelHall>();
        ConfigChannelLine cfg = ConfigManager.GetConfig<ConfigChannel>().GetLine(RoomModel.ChannelId);
        bool minimize = (cfg != null && cfg.Minimize == 1);
        if (PanelRoomBase.IsCurrentRoomOpen() && !type.BaseType.Equals(typeof(PanelRoomBase)) && minimize && !UISystem.isNormalBack)
        {
            UISystem.isMinimizeRoom = true;
        }
        else if (UISystem.isMinimizeRoom)
        {
            if ((type.BaseType == typeof(PanelRoomListBase) || type == typeof(PanelCorpsEnter) ||
                type == typeof(PanelPVE) || type == typeof(PanelLvlPass) || type == typeof(PanelZombiePass)))
            {
                result = 1;
            }
            else if (type.BaseType == typeof(PanelRoomBase))
            {
                result = -1;
            }
        }
        UISystem.isNormalBack = false;
        return result;
    }
    #endregion
}