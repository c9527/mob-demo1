﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PanelTv : BasePanel
{
    private RectTransform m_tranMsg;
    private Text m_msg;
    private static List<string> m_msgList = new List<string>(); 

    public PanelTv()
    {
        SetPanelPrefabPath("UI/Common/PanelTv");
        AddPreLoadRes("UI/Common/PanelTv", EResType.UI);
    }

    public override void Init()
    {
        m_tranMsg = m_tran.Find("mask/Text").gameObject.GetRectTransform();
        m_msg = m_tranMsg.GetComponent<Text>();
        EnableMouseEvent = false;
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        ShowNextMsg();
    }

    public static void AddMsg(string msg)
    {
        m_msgList.Add(msg);
        if (m_msgList.Count > 10)
            m_msgList.RemoveAt(0);
    }

    private void ShowNextMsg()
    {
        if (m_msgList.Count == 0)
        {
            HidePanel();
            return;
        }
        m_msg.text = m_msgList[0];
        m_msgList.RemoveAt(0);
        m_tranMsg.anchoredPosition = new Vector2(500, 0);
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (!IsOpen())
            return;
        m_tranMsg.anchoredPosition += new Vector2(-Time.deltaTime*100, 0);
        if (m_tranMsg.anchoredPosition.x <= -500 - m_msg.preferredWidth)
            ShowNextMsg();
    }

    public static void ClearTv()
    {
        m_msgList.Clear();
    }

}