﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;




public class DynamicStack : MonoBehaviour
{
    private float m_fFrameWidth;
    private float m_fFrameHeight;
    private bool m_bShowStackList;

    private ArrayList m_textureContents;
        
    public Sprite m_textureFrame;
    public int m_iSeledIndex;
    public float m_fScale = 1;

    public Sprite[] m_textureArr;

    private ArrayList m_objStacks;
    // Use this for initialization
    const int INIT_STACK_SIZE = 3;

    private GameObject m_MainFrame;

    private Sprite m_selFrameSprite = null;

    void Awake()
    {
        m_iSeledIndex = 0;//放在Start()初始化会影响逻辑
    }

    void Start()
    {
        Init();
        m_textureContents.Clear();
        if (m_textureArr != null )
        {
            for (int n = 0; n < m_textureArr.Length; ++n)
            {
                m_textureContents.Add(m_textureArr[n]);
            }
        }
        CreateDynamicStack();
        ShowSelIndex();       
    }

    void Init()
    {
        if( m_textureContents == null )
        {
            m_textureContents = new ArrayList(INIT_STACK_SIZE);
        }

        if( m_objStacks == null )
        {
            m_objStacks = new ArrayList(INIT_STACK_SIZE);
        }

        if (m_MainFrame == null)
        {
            m_MainFrame = CreateSingleStack();
        }
    }

    public Sprite[] updateStackItemList
    {
        get
        {
            return m_textureArr;
        }

        set
        {
            m_textureArr = value;
            Init();
                
            m_textureContents.Clear();
            for (int n = 0; n < m_textureArr.Length; ++n)
            {
                m_textureContents.Add(m_textureArr[n]);
            }

            CreateDynamicStack();
            ShowSelIndex();
                
        }
    }

    GameObject CreateSingleStack(string suffix = "")
    {

        GameObject stackitem = new GameObject("stackitem" + suffix);
        Image kFrameImage = stackitem.AddComponent<Image>();
        kFrameImage.type = Image.Type.Simple;
        //stackitem.AddComponent<RectTransform>();
        RectTransform parentRect = stackitem.GetComponent<RectTransform>();
        stackitem.AddComponent<Button>();
        //parentRect.SetParent(transform, false);
        parentRect.SetParent(transform);

        parentRect.anchoredPosition = new Vector2(0, 0);
        parentRect.sizeDelta = new Vector2( m_fFrameWidth, m_fFrameHeight );
        parentRect.localScale = new Vector3(m_fScale, m_fScale, 1);

        RegisterClickEvent(stackitem);


        GameObject stackcontent = new GameObject("item");
        Image kImage = stackcontent.AddComponent<Image>();
        kImage.type = Image.Type.Simple;

        //stackcontent.AddComponent<RectTransform>();
        RectTransform contentRectTransform = stackcontent.GetComponent<RectTransform>();
        contentRectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        contentRectTransform.anchorMax = new Vector2(0.5f, 0.5f);
            
        //contentRectTransform.SetParent(parentRect, false);
        contentRectTransform.SetParent(parentRect);
        contentRectTransform.anchoredPosition = new Vector2(0, 0);
        contentRectTransform.localScale = new Vector3(m_fScale, m_fScale, 1);


        var imgBroken = new GameObject("imgBroken").AddComponent<Image>();
        imgBroken.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "broken"));
        imgBroken.rectTransform.SetParent(parentRect);
        imgBroken.rectTransform.anchoredPosition = new Vector2(-113, 26);
        imgBroken.rectTransform.localScale = new Vector3(0.7f * m_fScale, 0.7f * m_fScale, 1);
        imgBroken.SetNativeSize();
        imgBroken.enabled = false;

        var name = new GameObject("name").AddComponent<Text>();
        name.rectTransform.SetParent(parentRect);
        name.rectTransform.anchoredPosition = new Vector2(80, -26);
        name.rectTransform.localScale = Vector3.one;
        name.font = ResourceManager.LoadFont();
        name.alignment = TextAnchor.MiddleRight;
        name.horizontalOverflow = HorizontalWrapMode.Overflow;
        name.verticalOverflow = VerticalWrapMode.Overflow;
        name.fontSize = 24;

        return stackitem;
    }

    void AttachStackResource(GameObject stackitem, int index)
    {
        stackitem.GetComponent<Image>().SetSprite(m_textureFrame);

        Transform contentObj = stackitem.transform.Find("item");

        Image contentImage = contentObj.GetComponent<Image>();
        RectTransform contentRectTransform = contentObj.GetComponent<RectTransform>();

        Sprite sprite = m_textureContents[index] as Sprite;
        if( sprite != null )
        {
            
            contentImage.SetSprite(sprite);
            contentImage.SetNativeSize();
        }
        
    }

    public void SetFrameSize( float width,float height )
    {
        m_fFrameWidth = width;
        m_fFrameHeight = height;
    }

        

    void CreateDynamicStack()
    {
        if (m_textureContents.Count > m_objStacks.Count)
        {
            // increase
            int incnum = m_textureContents.Count - m_objStacks.Count;
            int baseindex = m_objStacks.Count;
            for (int n = 0; n < incnum; ++n)
            {
                GameObject newone = CreateSingleStack((n+1).ToString());
                AttachStackResource(newone, baseindex + n);

                //m_objStacks[m_textureContents.Count + n] = newone;
                m_objStacks.Add(newone);
            }
        }
        else if (m_textureContents.Count < m_objStacks.Count)
        {
            //shrink
            for (int n = m_objStacks.Count - 1, nmin = m_textureContents.Count; n >= nmin; --n)
            {
                //m_textureContents.RemoveAt(n);
                GameObject obj = m_objStacks[n] as GameObject;
                Destroy(obj);
                m_objStacks.RemoveAt(n);
            }
        }

        for (int n = 0; n < m_textureContents.Count; ++n)
        {
            GameObject obj = m_objStacks[n] as GameObject;
            AttachStackResource(obj, n);
        }
    }

    public void RemoveOne(uint index)
    {

    }

    public void TailAddOne(uint index)
    {

    }

    public int selindex
    {
        set
        {
            m_iSeledIndex = value;
            ShowSelIndex();
        }
        get
        {
            return m_iSeledIndex;
        }
    }

    public Sprite SetSelFrame
    {
        set
        {
            m_selFrameSprite = value;
        }
    }

    void ShowSelIndex()
    {
        if (m_textureContents.Count <= 0 || m_iSeledIndex >= m_textureContents.Count )
        {
            m_MainFrame.TrySetActive(false);
            return;
        }
        m_MainFrame.TrySetActive(true);
        m_MainFrame.GetComponent<Image>().SetSprite(m_textureFrame);
        Transform contentTransform = m_MainFrame.transform.Find("item");
        RectTransform recttransform = contentTransform.GetComponent<RectTransform>();

        Sprite contentSprite = m_textureContents[m_iSeledIndex] as Sprite;
        
        Image kFrameImage = contentTransform.GetComponent<Image>();
        kFrameImage.SetSprite(contentSprite);
        kFrameImage.SetNativeSize();

        var weaponList = MainPlayer.singleton.serverData.weaponList;
        var imgBroken = m_MainFrame.transform.Find("imgBroken");
        var name = m_MainFrame.transform.Find("name");
        if (weaponList != null && imgBroken != null && weaponList.Count > m_iSeledIndex)
        {
            if (imgBroken != null)
                imgBroken.GetComponent<Image>().enabled = ItemDataManager.IsBroken(weaponList[m_iSeledIndex]);
            if (name != null)
            {
                var weapon = ItemDataManager.GetItemWeapon(weaponList[m_iSeledIndex]);
                if(weapon != null)
                    name.GetComponent<Text>().text = weapon.DispName;
            }
        }
        

        ShowStak(false);
        return;
    }

    public bool isStackShow
    {
        get { return m_bShowStackList; }
    }

    public void ShowStak(bool bShow)
    {
        m_bShowStackList = bShow;
        if (bShow)
        {
            for (int n = 0; n < m_objStacks.Count; ++n)
            {
                GameObject go = m_objStacks[n] as GameObject;
                RectTransform goRectTransform = go.GetComponent<RectTransform>();
                goRectTransform.anchoredPosition = new Vector2( 0, (n + 1) * m_fFrameHeight * m_fScale );
                go.TrySetActive(true);

                Image kSelImage = go.AddMissingComponent<Image>();
                if( n == m_iSeledIndex && m_selFrameSprite != null )
                {
                        
                    kSelImage.SetSprite(m_selFrameSprite);
                    //kSelImage.SetNativeSize();
                }
                else
                {
                    kSelImage.SetSprite(m_textureFrame);
                   // kSelImage.SetNativeSize();
                }

                var weaponList = MainPlayer.singleton.serverData.weaponList;
                var imgBroken = goRectTransform.Find("imgBroken");
                var name = goRectTransform.Find("name");
                if (weaponList != null)
                {
                    if (imgBroken != null)
                        imgBroken.GetComponent<Image>().enabled = ItemDataManager.IsBroken(weaponList[n]);
                    if (name != null)
                    {
                        var weapon = ItemDataManager.GetItemWeapon(weaponList[n]);
                        if (weapon != null)
                            name.GetComponent<Text>().text = weapon.DispName;
                    }
                }

            }
        }
        else
        {
            for (int n = 0; n < m_objStacks.Count; ++n)
            {
                GameObject go = m_objStacks[n] as GameObject;
                RectTransform goRectTransform = go.GetComponent<RectTransform>();
                goRectTransform.anchoredPosition = new Vector2(0, (n + 1) * m_fFrameHeight);
                go.TrySetActive(false);
            }
        }
    }

    void RegisterClickEvent(GameObject eventObj)
    {

        UGUIClickHandler.Get(eventObj).onPointerClick += OnClickStackItem;
    }

    void OnClickStackItem(GameObject target, PointerEventData data)
    {
        if (enabled == false)
        {
            return;
        }
        PointerEventData kPointerData = data;
        if (kPointerData.pointerPress == m_MainFrame)
        {
            //观战模式下  不打开切枪列表
            if (WorldManager.singleton.isViewBattleModel || MainPlayer.singleton.IsHideView) return;
            if (MainPlayer.singleton == null || MainPlayer.singleton.serverData.state == GameConst.SERVER_ACTOR_STATUS_DEAD|| MainPlayer.singleton.serverData.weaponList == null || MainPlayer.singleton.serverData.weaponList.Count == 0)
            {
                return;
            }
            if (!m_bShowStackList)
            {
                ShowStak(true);
            }
            else
            {
                ShowStak(false);
            }

        }
        else
        {
            GameObject kSeledObj = kPointerData.pointerPress;
            int clickeindex = -1;
            for (int n = 0; n < m_objStacks.Count; ++n)
            {
                GameObject go = m_objStacks[n] as GameObject;
                if (go == kSeledObj)
                {
                    clickeindex = n;
                    break;
                }
            }

            if (clickeindex > -1)
            {
                m_iSeledIndex = clickeindex;
                ShowSelIndex();

                GameDispatcher.Dispatch(GameEvent.UI_GUN_SELECTED, GameEvent.UI_GUN_SELECTED, m_iSeledIndex);
            }
        }

        /*
        int iWeaponID = WorldManager.singleton.mainPlayer.GetNextWeaponID();
        if (iWeaponID != 0)
            GlobalUIEventDispatcher.Dispatch<string, int>(GameEvent.SWITCH_WEAPON, GameEvent.SWITCH_WEAPON, iWeaponID);
         
        */
    }
}
