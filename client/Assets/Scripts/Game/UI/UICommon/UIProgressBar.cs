﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIProgressBar : MonoBehaviour
{
    private event Action timerOverHandler=null;

    public bool isPlaying = false;

    private float to=100;
    private float from=0;
    private float duration;
    private float curTime;

    private GameObject goToolImg;
    private GameObject goBgImg;
    private GameObject goProgressImg;
    private GameObject goLabelTxt;
    private Image imgProgress;
    private Text txtLabel;

    public void play(string bgAtlas, string bg, string progressAtlas, string progress, string msg = "", float duration = 1, ProgressBarProp prop = null,string tool="")
    {
        Sprite spriteBg = ResourceManager.LoadSprite(bgAtlas, bg);
        Sprite spriteProgress = ResourceManager.LoadSprite(progressAtlas, progress);
        Sprite toolSp = ResourceManager.LoadSprite(AtlasName.Battle, tool);
        play(spriteBg, spriteProgress, msg, duration, prop, toolSp);
    }

    public void play(Sprite bg, Sprite progress, string msg = "", float duration = 1, ProgressBarProp prop = null,Sprite tool = null)
    {
        stop();
        gameObject.TrySetActive(true);

        this.duration = duration;

        Vector2 bgPos = Vector2.zero;
        Vector2 txtPos = Vector2.zero;
        Vector2 progressPos = Vector2.zero;
        Vector2 toolPos = Vector2.zero;
        TextProp textProp = null;
        RectTransformProp toolProp = null;
        RectTransformProp bgProp = null;
        RectTransformProp progressProp = null;

        if (prop != null)
        {
            this.to = prop.to;
            this.from = prop.from;
            bgPos = prop.bgPos;
            txtPos = prop.txtPos;
            progressPos = prop.progressPos;
            toolPos = prop.toolPos;
            toolProp = prop.toolProp;
            textProp = prop.textProp;
            bgProp = prop.bgProp;
            progressProp = prop.progressProp;
            this.timerOverHandler = prop.OnTimeOut;
        }

        
        UIHelper.ShowSprite(ref goBgImg, "ProgressBarBg", bg, bgPos, true, bgProp, transform, true);
        UIHelper.ShowSprite(ref goProgressImg, "ProgressBarImg", progress, progressPos, true, progressProp, transform, true, Image.Type.Filled, Image.FillMethod.Horizontal);
        UIHelper.ShowSprite(ref goToolImg, "gongjuqian", tool, toolPos, true, toolProp, goProgressImg.transform, (tool != null));
        UIHelper.ShowText(ref goLabelTxt, msg, true, txtPos, textProp, transform, true);

        if (imgProgress == null) imgProgress = goProgressImg.GetComponent<Image>();
        if (txtLabel == null) txtLabel = goLabelTxt.GetComponent<Text>();

        isPlaying = true;

        //StartCoroutine(playProgress());
    }

    public void stop()
    {
        isPlaying = false;
        //StopCoroutine(playProgress());
        curTime = 0f;
        timerOverHandler = null;
        if (goBgImg != null) goBgImg.TrySetActive(false);
        if (goLabelTxt != null) goLabelTxt.TrySetActive(false);
        if (goProgressImg != null) goProgressImg.TrySetActive(false);
        if (gameObject != null) gameObject.TrySetActive(false);
    }

    void Update()
    {
        if (isPlaying)
            playProgress();
    }

    private void playProgress()
    {
        float curProgress = from / 100;
        float tarProgress = to / 100;
        curTime += Time.deltaTime;
        curProgress = (from + (curTime / duration * (to - from))) / 100;
        if(imgProgress != null) imgProgress.fillAmount = Math.Min(curProgress, tarProgress);
        if (curTime > duration || curProgress > tarProgress)
        {
            if (timerOverHandler != null) timerOverHandler();
            stop();
        }
    }
}

public class ProgressBarProp
{
    public float from = 0;
    public float to = 100;
    public TextProp textProp = null;
    public RectTransformProp bgProp = null;
    public RectTransformProp progressProp = null;
    public RectTransformProp toolProp = null;
    public Vector2 bgPos = default(Vector2);
    public Vector2 txtPos = default(Vector2);
    public Vector2 progressPos = default(Vector2);
    public Vector2 toolPos = default(Vector2);
    public Action OnTimeOut = null;
}
