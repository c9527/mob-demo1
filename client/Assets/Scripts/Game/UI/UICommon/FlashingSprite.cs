﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class FlashingSprite : MonoBehaviour
{
    public bool isPlaying = false;

    private Image img;
    private bool isDo = true;
    private float curTime;
    private float interval;

    public static FlashingSprite AddFlashingSpriteBehaviour(ref GameObject go)
    {
        if (go == null) go = new GameObject();
        FlashingSprite behavior = go.AddMissingComponent<FlashingSprite>();
        return behavior;
    }

    public void play(ref GameObject go, string name, string atlas, string sprite, Vector2 pos, float interval = 1f, RectTransformProp prop = null, Transform parent = null,
        Image.Type spriteType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
    {
        Sprite img = ResourceManager.LoadSprite(atlas, sprite);
        play(ref go, name, img, pos, interval, prop, parent, spriteType, fillMethod);
    }

    public void play(ref GameObject go, string name, Sprite sprite, Vector2 pos, float interval = 1f, RectTransformProp prop = null, Transform parent = null,
        Image.Type spriteType = Image.Type.Simple, Image.FillMethod fillMethod = Image.FillMethod.Horizontal)
    {
        UIHelper.ShowSprite(ref go, name, sprite, pos, true, prop, parent, true, spriteType, fillMethod);
        if (isPlaying) return;
        gameObject.TrySetActive(true);
        img = go.GetComponent<Image>();
        this.interval = interval;
        isPlaying = true;
        //StartCoroutine(playProgress());
    }

    public void stop()
    {
        isPlaying = false;
        //StopCoroutine(playProgress());
        curTime = 0f;
        if (gameObject != null) gameObject.TrySetActive(false);
    }

    void Update()
    {
        if (isPlaying)
            playProgress();
    }

    private void playProgress()
    {
        //while (true)
        //{
            if (gameObject == null || img == null) return;
            curTime += Time.deltaTime;
            if(interval > 0 && curTime >= interval)
            {
                isDo = !isDo;
                curTime = 0;
            }
            if (isDo) img.enabled = true;
            else img.enabled = false;
            //yield return null;
        //}
    }
}
