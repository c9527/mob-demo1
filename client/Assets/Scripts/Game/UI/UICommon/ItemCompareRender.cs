﻿using UnityEngine.UI;

class ItemCompareRender : ItemRender
{
    private Text m_txtName;
    private Image m_imgItem;
    private Image m_imgBg;
    private Image m_imgBgBar;

    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("txtEquipName").GetComponent<Text>();
        m_imgItem = tran.Find("imgEquip").GetComponent<Image>();
        m_imgBg = tran.Find("imgBg").GetComponent<Image>();
        m_imgBgBar = tran.Find("imgBgBar").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        var item = data as ConfigItemWeaponLine;
        m_imgBg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "itembg_" + ColorUtil.GetRareColorName(item.RareType)));
        m_imgBgBar.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, ColorUtil.GetRareColorName(item.RareType) + "_bg"));

        if (!string.IsNullOrEmpty(item.Icon))
        {
            m_imgItem.SetSprite(ResourceManager.LoadIcon(item.Icon), item.SubType);
            m_imgItem.SetNativeSize();
            m_imgItem.enabled = true;
        }
        else
            m_imgItem.enabled = false;

        m_txtName.enabled = true;
        m_txtName.color = ColorUtil.GetRareColor(item.RareType);
        m_txtName.text = item.DispName;
    }
}