﻿using UnityEngine;
using UnityEngine.UI;

class PanelRuleBase : BasePanel
{
    public PanelRuleBase()
    {
        SetPanelPrefabPath("UI/Common/PanelRuleBase");
        AddPreLoadRes("UI/Common/PanelRuleBase", EResType.UI);
    }

    private Text m_title;
    private Text m_content;

    public override void Init()
    {
        m_title = m_tran.Find("RuleDesc/title").GetComponent<Text>();
        m_content = m_tran.Find("RuleDesc/content_txt").GetComponent<Text>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("ok_btn")).onPointerClick += delegate { this.HidePanel(); };
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void OnHide()
    {

    }

    public override void OnShow()
    {
        if (!HasParams())
        {
            this.HidePanel();
            return;
        }
        string name = m_params[0] as string;
        ConfigRuleIntro cri = ConfigManager.GetConfig<ConfigRuleIntro>();
        var line = cri.GetLine(name);
        if (line != null)
        {
            m_title.text = line.Title;
            m_content.text = line.Content.Replace("\\n", "\n");
        }
        else
        {
            this.HidePanel();
        }
    }

    public override void Update()
    {

    } 
}

