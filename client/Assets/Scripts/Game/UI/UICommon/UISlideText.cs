﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UISlideText : MonoBehaviour
{


    const int WINDOW_SIZE = 5;
    public enum SLIDE_DIRC
    {
        TOP = 1,
        BOTTOM = 2,
        LEFT = 3,
        RIGHT = 4
    }

    class SlideTextInfo
    {
        public GameObject m_goText;
        public float m_aliveTime = 0.0f;
        public float m_fMoveSpeed = 0.0f;
        public SLIDE_DIRC m_direction;
    }

    Transform m_Parent = null;
    List<SlideTextInfo> m_queueText;
    public void Init(Transform parent)
    {
        m_Parent = parent;
        m_queueText = new List<SlideTextInfo>();
    }

    public void PushSlideText(string text, SLIDE_DIRC direction, float xPos, float yPos, Color textColor, int fontSize = 18, float aliveTime = 1.0f)
    {
        Text slideText = Util.CreateText(m_Parent, Vector2.zero, fontSize, textColor);
//        slideText.gameObject.AddComponent<Outline>();     // Outline特别费性能

        GameObject goSlideText = slideText.gameObject;

        if (goSlideText == null)
        {
            return;
        }

        goSlideText.name = "slidetext";

        goSlideText.TrySetActive(false);
        RectTransform slideRectTransform = goSlideText.AddMissingComponent<RectTransform>();
        slideRectTransform.localPosition = new Vector3(xPos, yPos, 0);
        Vector3 scaleV = slideRectTransform.localScale;
        scaleV.x = 1;
        scaleV.y = 1;
        scaleV.z = 1;
        slideRectTransform.localScale = scaleV;

        if (direction == SLIDE_DIRC.TOP || direction == SLIDE_DIRC.BOTTOM)
        {
            Vector2 sizeV = slideRectTransform.sizeDelta;
            sizeV.x = m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.width;
            slideRectTransform.sizeDelta = sizeV;
        }
        else
        {
            Vector2 sizeV = slideRectTransform.sizeDelta;
            sizeV.y = m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.height;
            slideRectTransform.sizeDelta = sizeV;
        }
        
        slideText.alignment = TextAnchor.MiddleCenter;
        slideText.text = text;

        SlideTextInfo info = new SlideTextInfo();
        info.m_goText = goSlideText;
        info.m_aliveTime = aliveTime;

        if (direction == SLIDE_DIRC.TOP)
        {
            float len = m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.height / 2 - yPos;
            info.m_fMoveSpeed = len / aliveTime;
        }
        else if (direction == SLIDE_DIRC.BOTTOM)
        {
            float len = -m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.height / 2 - yPos;
            info.m_fMoveSpeed = len / aliveTime;
        }
        else if (direction == SLIDE_DIRC.LEFT)
        {
            float len = -m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.width / 2 - xPos;
            info.m_fMoveSpeed = len / aliveTime;
        }
        else if (direction == SLIDE_DIRC.RIGHT)
        {
            float len = m_Parent.gameObject.AddMissingComponent<RectTransform>().rect.width / 2 - xPos;
            info.m_fMoveSpeed = len / aliveTime;
        }
        info.m_direction = direction;

        m_queueText.Add(info);
    }
    // Update is called once per frame
    void Update()
    {
        int count = 0;
        if (m_queueText != null)
        {
            while (count < WINDOW_SIZE && count < m_queueText.Count)
            {

                SlideTextInfo textInfo = m_queueText[count];

                textInfo.m_aliveTime -= Time.deltaTime;

                textInfo.m_goText.TrySetActive(true);
                Transform textTransform = textInfo.m_goText.AddMissingComponent<Transform>();

                if (textInfo.m_direction == SLIDE_DIRC.TOP || textInfo.m_direction == SLIDE_DIRC.BOTTOM)
                {
                    float deltaPos = textInfo.m_fMoveSpeed * Time.deltaTime;

                    Vector3 localPos = textTransform.localPosition;
                    localPos.y += deltaPos;

                    textTransform.localPosition = localPos;
                }
                else if (textInfo.m_direction == SLIDE_DIRC.LEFT || textInfo.m_direction == SLIDE_DIRC.RIGHT)
                {
                    float deltaPos = textInfo.m_fMoveSpeed * Time.deltaTime;

                    Vector3 localPos = textTransform.localPosition;
                    localPos.x += deltaPos;

                    textTransform.localPosition = localPos;
                }

                if (count + 1 < m_queueText.Count)
                {
                    SlideTextInfo nextTextInfo = m_queueText[count + 1];
                    if (nextTextInfo.m_aliveTime - textInfo.m_aliveTime >= 1)
                    {
                        ++count;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            count = 0;
            while (count < WINDOW_SIZE)
            {
                if (m_queueText.Count > 0)
                {
                    SlideTextInfo curText = m_queueText[0];
                    if (curText.m_aliveTime <= 0)
                    {
                        curText.m_goText.TrySetActive(false);
                        curText.m_goText.transform.SetParent(null);
                        GameObject.Destroy(curText.m_goText);

                        m_queueText.RemoveAt(0);
                    }
                    else
                    {
                        break;
                    }


                }
                else if (count >= m_queueText.Count)
                {
                    break;
                }

                ++count;
            }
        }

    }
}
