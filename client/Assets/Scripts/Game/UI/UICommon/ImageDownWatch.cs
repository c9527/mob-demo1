﻿using UnityEngine;
using UnityEngine.UI;

public class ImageDownWatch : MonoBehaviour
{
    private string _num_style = "watch";
    private string _num_atlas = "Common";

    Image m_ImageMin1;
    Image m_ImageMin2;
    Image m_ImageSec1;
    Image m_ImageSec2;

    Sprite[] m_arrSprites;
    public float m_fTime = 0;
    private int m_type = 0;  //0: xx:xx ; 1: xx
    // Use this for initialization
    void Start()
    {
        m_ImageSec1 = gameObject.transform.Find("sec1").gameObject.GetComponent<Image>();
        m_ImageSec2 = gameObject.transform.Find("sec2").gameObject.GetComponent<Image>();
    }

    public void setNumStyle(string atlas,string name, int type = 0)
    {
        if (string.IsNullOrEmpty(atlas) || string.IsNullOrEmpty(name))
        {
            return;
        }
        _num_atlas = atlas;
        _num_style = name;
        m_type = type;
        m_arrSprites = null;
    }

    public void SetNumSpriteList( Sprite[] spriteList )
    {
        if (spriteList.Length < 10)
        {
            return;
        }
        m_arrSprites = spriteList;
    }

    void LoadNumSprite()
    {
        m_arrSprites = new Sprite[10];
        for (int index = 0; index < 10; ++index)
        {
            string spriteName = _num_style + index;
            ResourceManager.LoadSprite(_num_atlas, spriteName, (Sprite sprite) =>
            {
                m_arrSprites[index] = sprite;
            });
        }
    }
    public float SetTime
    {
        set
        {
            m_fTime = value;
        }
        get
        {
            return m_fTime;
        }
    }

    public bool IsFinish
    {
        get
        {
            return m_fTime <= 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_type == 0 && (m_ImageMin1 == null || m_ImageMin2 == null))
        {
            m_ImageMin1 = gameObject.transform.Find("min1").gameObject.GetComponent<Image>();
            m_ImageMin2 = gameObject.transform.Find("min2").gameObject.GetComponent<Image>();
        }

        if( m_arrSprites == null )
        {
            LoadNumSprite();
        }
        if( m_fTime>=0)
        {
            if (m_fTime > 0)
            {
                int iMin = (int)(m_fTime / 60);
                int iSec = (int)(m_fTime % 60);

                int min1 = iMin / 10;
                int min2 = iMin % 10;

                int sec1 = iSec / 10;
                int sec2 = iSec % 10;

                if (m_type == 0)
                {
                    m_ImageMin1.SetSprite(m_arrSprites[min1]);
                    m_ImageMin1.SetNativeSize();
                    m_ImageMin2.SetSprite(m_arrSprites[min2]);
                    m_ImageMin2.SetNativeSize();
                }
                
                m_ImageSec1.SetSprite(m_arrSprites[sec1]);
                m_ImageSec1.SetNativeSize();
                m_ImageSec2.SetSprite(m_arrSprites[sec2]);
                m_ImageSec2.SetNativeSize();

                m_fTime -= Time.deltaTime; 
            }
            else
            {
                m_fTime = -1;

                if (m_type == 0)
                {
                    m_ImageMin1.SetSprite(m_arrSprites[0]);
                    m_ImageMin1.SetNativeSize();
                    m_ImageMin2.SetSprite(m_arrSprites[0]);
                    m_ImageMin2.SetNativeSize();
                }
                
                m_ImageSec1.SetSprite(m_arrSprites[0]);
                m_ImageSec1.SetNativeSize();
                m_ImageSec2.SetSprite(m_arrSprites[0]);
                m_ImageSec2.SetNativeSize();
            }
        }
    }
}
