﻿using UnityEngine;
using UnityEngine.UI;

class ItemComparePropRender : ItemRender
{
    private PropRenderInfo m_data;
    private Text m_txtName;
    private Text m_txtValue;
    private Image m_imgCompare;

    public override void Awake()
    {
        var tran = transform;
        m_txtName = tran.Find("txtName").GetComponent<Text>();
        m_txtValue = tran.Find("txtValue").GetComponent<Text>();
        m_imgCompare = tran.Find("imgCompare").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as PropRenderInfo;
        if (m_data == null || m_data.value == 0)
        {
            gameObject.TrySetActive(false);
            return;
        }
        gameObject.TrySetActive(true);

        m_txtName.text = m_data.name;
        m_txtValue.text = m_data.value.ToString();
        
        if (m_data.value > m_data.valueAdd)
            m_imgCompare.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "arrow_up"));
        else if (m_data.value < m_data.valueAdd)
            m_imgCompare.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "arrow_down"));
        else
            m_imgCompare.SetSprite(ResourceManager.LoadSprite(AtlasName.Package, "arrow_equal"));
        m_imgCompare.enabled = !Mathf.Approximately(m_data.valueAdd, 0);
        m_imgCompare.SetNativeSize();
    }
}