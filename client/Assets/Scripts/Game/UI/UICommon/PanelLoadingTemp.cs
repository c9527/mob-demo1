﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PanelLoadingTemp : BasePanel
{
    public event Action OnLoadComplete;
    private Text m_content;
    private Transform m_tranLoading;

    public PanelLoadingTemp()
    {
        SetPanelPrefabPath("UI/Common/PanelLoading");
        AddPreLoadRes("UI/Common/PanelLoading", EResType.UI);
    }

    public override void Init()
    {
        m_content = m_tran.Find("Text").GetComponent<Text>();
        m_tranLoading = m_tran.Find("imgLoading");
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (IsOpen())
        {
            m_tranLoading.Rotate(0, 0, -360 * Time.deltaTime * 2, Space.Self);
        }
    }
}