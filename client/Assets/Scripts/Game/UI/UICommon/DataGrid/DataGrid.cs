﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 数据列表渲染组件，Item缓存，支持无限循环列表，即用少量的Item实现大量的列表项显示
/// </summary>
public class DataGrid : MonoBehaviour
{
    [HideInInspector]
    public bool useLoopItems = false;           //是否使用无限循环列表，对于列表项中OnDataSet方法执行消耗较大时不宜使用，因为OnDataSet方法会在滚动的时候频繁调用
    [HideInInspector]
    public bool useClickEvent = true;           //列表项是否监听点击事件
    [HideInInspector]
    public bool autoSelectFirst = true;         //创建时是否自动选中第一个对象

    public delegate void OnDataGridItemSelect(object renderData);
    public OnDataGridItemSelect onItemSelected;       //Item点击时的回调函数
    public GameObject TargetGo;//选中的对象
    private RectTransform m_content;
    private Vector2 m_lastContentPos;
    private ToggleGroup m_toggleGroup;
    private object[] m_data;
    private GameObject m_goItemRender;
    private Type m_itemRenderType;
    private readonly List<ItemRender> m_items = new List<ItemRender>();
    private object m_selectedData;
    private GridLayoutGroup m_gridLayoutGroup;
    private RectOffset m_oldPadding;
    private Canvas m_canvas;

    //下面的属性会需要父对象上有ScrollRect组件
    private ScrollRect m_scrollRect;    //父对象上的，不一定存在
    private RectTransform m_tranScrollRect;
    private int m_itemSpace;          //每个Item的空间
    private int m_viewItemCount;        //可视区域内Item的数量（向上取整）
    private bool m_isVertical;          //是否是垂直滚动方式，否则是水平滚动
    private int m_startIndex;           //数据数组渲染的起始下标
    private string m_itemClickSound = AudioConst.btnClick;

    public float verticalPos
    {
        get { return m_scrollRect.verticalNormalizedPosition; }
        set { m_scrollRect.verticalNormalizedPosition = value; }
    }

    public float horizonPos
    {
        get { return m_scrollRect.horizontalNormalizedPosition; }
        set { m_scrollRect.horizontalNormalizedPosition = value; }
    }

    //内容长度
    private float ContentSpace { get { return m_isVertical ? m_content.sizeDelta.y : m_content.sizeDelta.x; } }
    //可见区域长度
    private float ViewSpace { get { return m_isVertical ? m_tranScrollRect.sizeDelta.y : m_tranScrollRect.sizeDelta.x; } }
    //约束常量（固定的行（列）数）
    private int ConstraintCount { get { return m_gridLayoutGroup == null ? 1 : m_gridLayoutGroup.constraintCount; } }
    //数据量个数
    private int DataCount { get { return m_data == null ? 0 : m_data.Length; }}
    //缓存数量
    private int CacheCount { get { return ConstraintCount + DataCount%ConstraintCount; }}
    //缓存单元的行（列）数
    private int CacheUnitCount { get { return m_gridLayoutGroup == null ? 1 : Mathf.CeilToInt((float)CacheCount / ConstraintCount); } }
    //数据单元的行（列）数
    private int DataUnitCount { get { return m_gridLayoutGroup == null ? DataCount : Mathf.CeilToInt((float)DataCount / ConstraintCount); } }



    void Awake()
    {
        gameObject.AddMissingComponent<CanvasRenderer>();
        m_content = gameObject.GetRectTransform();
        m_toggleGroup = GetComponent<ToggleGroup>();
        m_gridLayoutGroup = GetComponent<GridLayoutGroup>();
        if (m_gridLayoutGroup != null)
            m_oldPadding = m_gridLayoutGroup.padding;

        m_scrollRect = transform.GetComponentInParent<ScrollRect>();
        if (m_scrollRect != null && m_gridLayoutGroup != null)
        {
            if (m_scrollRect.gameObject.layer != GameSetting.LAYER_VALUE_UI)
                m_scrollRect.gameObject.ApplyLayer(GameSetting.LAYER_VALUE_UI);
//            m_canvas = m_scrollRect.gameObject.AddMissingComponent<Canvas>();
//            m_canvas.overridePixelPerfect = true;
//            m_canvas.pixelPerfect = true;
            //单独一个Canvas，在滚动的时候不开启像素对齐
//            m_scrollRect.gameObject.AddMissingComponent<SortingOrderRenderer>();
//            SortingOrderRenderer.RebuildAll();

            m_scrollRect.decelerationRate = 0.2f;

            if (!Driver.isMobilePlatform)
                m_scrollRect.scrollSensitivity = 100f;
            m_tranScrollRect = m_scrollRect.GetComponent<RectTransform>();
            m_isVertical = m_scrollRect.vertical;
            var viewSpace = m_isVertical ? m_tranScrollRect.sizeDelta.y : m_tranScrollRect.sizeDelta.x;
            m_itemSpace = (int)(m_isVertical ? (m_gridLayoutGroup.cellSize.y + m_gridLayoutGroup.spacing.y) : (m_gridLayoutGroup.cellSize.x + m_gridLayoutGroup.spacing.x));
            m_viewItemCount = Mathf.CeilToInt(viewSpace / m_itemSpace);
        }
        else if (m_scrollRect == null)
        {
            if (gameObject.layer != GameSetting.LAYER_VALUE_UI)
                gameObject.ApplyLayer(GameSetting.LAYER_VALUE_UI);
//            m_canvas = gameObject.AddMissingComponent<Canvas>();
//            m_canvas.overridePixelPerfect = true;
//            m_canvas.pixelPerfect = true;
            //单独一个Canvas，在滚动的时候不开启像素对齐
//            gameObject.AddMissingComponent<SortingOrderRenderer>();
//            SortingOrderRenderer.RebuildAll();
        }
    }

    void Start()
    {
        if (m_scrollRect != null)
        {
            if (useLoopItems)
                m_scrollRect.onValueChanged.AddListener(OnScroll);
            if (m_toggleGroup != null)
                m_toggleGroup.allowSwitchOff = useLoopItems;
        }
    }

    /// <summary>
    /// 设置渲染项
    /// </summary>
    /// <param name="goItemRender"></param>
    /// <param name="itemRenderType"></param>
    public void SetItemRender(GameObject goItemRender, Type itemRenderType)
    {
        m_goItemRender = goItemRender;
        m_itemRenderType = itemRenderType.IsSubclassOf(typeof(ItemRender)) ? itemRenderType : null;
    }

    public void SetItemClickSound(string sound)
    {
        m_itemClickSound = sound;
    }

    /// <summary>
    /// 数据项
    /// </summary>
    public object[] Data
    {
        set { SetData(value, false); }
        get { return m_data; }
    }

    /// <summary>
    /// 保持当前选中项
    /// </summary>
    public object[] UpdateData
    {
        set { SetData(value, true); }
        get { return m_data; }
    }

    public void SetData(object[] value, bool keepSelected)
    {
        m_data = value;
        UpdateView();

        if (m_toggleGroup != null)
        {
            var hasSelected = false;
            if (keepSelected && m_selectedData != null)
            {
                for (int i = 0; i < m_data.Length; i++)
                {
                    if (m_data[i] == m_selectedData)
                    {
                        hasSelected = true;
                        SelectItem(m_data[i]);
                        SetToggle(i, true);
                        break;
                    }
                }
            }

            if (autoSelectFirst && m_data.Length > 0)
            {
                if (!hasSelected)
                {
                    if (m_data[0] != m_selectedData)
                        SelectItem(m_data[0]);
                    SetToggle(0, true);
                }
            }
            else if (m_data.Length == 0)
                SelectItem(null);
        }
    }

    public List<ItemRender> ItemRenders
    {
        get { return m_items; }
    }

    public void Reset()
    {
        if (m_data != null)
        {
            var allowSwitchOff = m_toggleGroup.allowSwitchOff;
            m_toggleGroup.allowSwitchOff = true;
            for (int i = 0; i < m_data.Length; i++)
            {
                SetToggle(i, false);
            }
            m_toggleGroup.allowSwitchOff = allowSwitchOff;
            SelectItem(null);
        }
    }

    /// <summary>
    /// 当前选择的数据项
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T SelectedData<T>()
    {
        return (T)m_selectedData;
    }

    public int SeletedIndex
    {
        get
        {
            for (int i = 0; i < m_data.Length; i++)
            {
                if (m_data[i] == m_selectedData)
                {
                    return i;
                }
            }
            return -1;
        }
    }

    /// <summary>
    /// 下一帧把指定项显示在最顶端并选中，这个比ResetScrollPosition保险，否则有些在UI一初始化完就执行的操作会不生效
    /// </summary>
    /// <param name="index"></param>
    public void ShowItemOnTop(int index)
    {
        TimerManager.SetTimeOut(0.01f, () =>
        {
            if (m_data.Length > index)
                SelectItem(m_data[index]);
            ResetScrollPosition(index);
        });
    }

    public void ShowItemOnTop(object data)
    {
        if (m_data == null || m_data.Length == 0)
        {
            return;
        }
        var target_index = -1;
        for (int i = 0; i < m_data.Length; i++)
        {
            if (m_data[i] == data)
            {
                target_index = i;
                break;
            }
        }
        if (target_index != -1)
        {
            TimerManager.SetTimeOut(0.01f, () =>
            {
                SelectItem(m_data[target_index]);
                ResetScrollPosition(target_index);
            });
        }
    }

    /// <summary>
    /// 重置滚动位置，如果同时还要赋值新的Data，请在赋值之前调用本方法
    /// </summary>
    public void ResetScrollPosition(int index = 0, bool force = false)
    {
        if (m_data == null)
            return;
        
        if (force)
        {
            if (m_isVertical)
                m_content.anchoredPosition = new Vector2(m_content.anchoredPosition.x, (index / ConstraintCount) * m_itemSpace);
            else
                m_content.anchoredPosition = new Vector2(-(index / ConstraintCount) * m_itemSpace, m_content.anchoredPosition.y);
            return;
        }

        var unitIndex = Mathf.Clamp(index / ConstraintCount, 0, DataUnitCount - m_viewItemCount > 0 ? DataUnitCount - m_viewItemCount : 0);
        var value = (unitIndex * m_itemSpace) / (ContentSpace - ViewSpace);
        value = Mathf.Clamp01(value);

        //特殊处理无法使指定条目置顶的情况——拉到最后
        if (unitIndex != index/ConstraintCount)
            value = 1;

        if (m_scrollRect)
        {
            if (m_isVertical)
                m_scrollRect.verticalNormalizedPosition = 1 - value;
            else
                m_scrollRect.horizontalNormalizedPosition = value;
        }

        m_startIndex = unitIndex * ConstraintCount;
        UpdateView();
    }
    /// <summary>
    /// 选择多个toggle返回的数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public List<T> SelectedMultiData<T>()
    {
        var retList = new List<T>();
        for (int i = 0; i < m_items.Count; i++)
        {
            var toggle = m_items[i].GetComponent<Toggle>();
            if (toggle)
            {
                if (toggle.isOn)
                {
                    retList.Add((T)(m_items[i].m_renderData));
                }
            }
        }
        return retList;
    }

    /// <summary>
    /// 选择多个toggle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public void ShowSelectedMultiData(bool isShow)
    {
        for (int i = 0; i < m_items.Count; i++)
        {
            var toggle = m_items[i].GetComponent<Toggle>();
            if (toggle)
            {
                if (i > 11 && i < 15)
                    toggle.isOn = false;
                else
                    toggle.isOn = isShow;
            }
        }
    }

//    private void Update()
//    {
//        //只在可滚动的情况下执行
//        if (m_canvas != null)
//        {
//            if (m_content.anchoredPosition == m_lastContentPos)
//            {
//                if (!m_canvas.pixelPerfect)
//                    m_canvas.pixelPerfect = true;
//            }
//            else
//                m_lastContentPos = m_content.anchoredPosition;
//        }
//    }

    /// <summary>
    /// 更新视图
    /// </summary>
    public void UpdateView()
    {
        if (useLoopItems)
        {
            if (m_data != null)
                m_startIndex = Mathf.Max(0, Mathf.Min(m_startIndex / ConstraintCount, DataUnitCount - m_viewItemCount - CacheUnitCount))* ConstraintCount;
            var frontSpace = m_startIndex / ConstraintCount * m_itemSpace;
            var behindSpace = Mathf.Max(0, m_itemSpace * (DataUnitCount - CacheUnitCount) - frontSpace - (m_itemSpace * m_viewItemCount));
            if (m_isVertical)
                m_gridLayoutGroup.padding = new RectOffset(m_oldPadding.left, m_oldPadding.right, frontSpace, behindSpace);
            else
                m_gridLayoutGroup.padding = new RectOffset(frontSpace, behindSpace, m_oldPadding.top, m_oldPadding.bottom);
        }
        else
            m_startIndex = 0;

        if (m_goItemRender == null || m_itemRenderType == null || m_data == null || m_content == null)
            return;

        int itemLength = useLoopItems ? m_viewItemCount * ConstraintCount + CacheCount : m_data.Length;
        itemLength = Mathf.Min(itemLength, m_data.Length);
        for (int i = itemLength; i < m_items.Count; i++)
        {
            Destroy(m_items[i].gameObject);
            m_items[i] = null;
        }
        for (int i = m_items.Count - 1; i >= 0; i--)
        {
            if (m_items[i] == null)
                m_items.RemoveAt(i);
        }
        
        for (int i = 0; i < itemLength; i++)
        {
            var index = m_startIndex + i;
            if (index >= m_data.Length || index < 0)
                continue;
            if (i < m_items.Count)
            {
                m_items[i].SetData(m_data[index]);

                if (useClickEvent || autoSelectFirst)
                    SetToggle(i, m_selectedData == m_data[index]);
            }
            else
            {
                var go = UIHelper.Instantiate(m_goItemRender) as GameObject;
                go.transform.SetParent(m_content, false);
                go.TrySetActive(true);
                var script = go.AddComponent(m_itemRenderType) as ItemRender;
                if (!go.activeInHierarchy)
                    script.Awake();
                script.SetData(m_data[index]);
                script.m_owner = this;
                if (useClickEvent)
                    UGUIClickHandler.Get(go, m_itemClickSound).onPointerClick += OnItemClick;
                if (m_toggleGroup != null)
                {
                    var toggle = go.GetComponent<Toggle>();
                    if (toggle != null)
                    {
                        toggle.group = m_toggleGroup;

                        //使用循环模式的话不能用渐变效果，否则视觉上会出现破绽
                        if (useLoopItems)
                            toggle.toggleTransition = Toggle.ToggleTransition.None;
                    }
                }
                m_items.Add(script);
            }
        }
    }

    private void OnScroll(Vector2 data)
    {
        if (m_canvas != null && m_canvas.pixelPerfect)
            m_canvas.pixelPerfect = false;
        var value = (ContentSpace - ViewSpace) * (m_isVertical ? data.y : 1-data.x);
        var start = ContentSpace - value - ViewSpace;
        var startIndex = Mathf.FloorToInt(start / m_itemSpace) * ConstraintCount;
        startIndex = Mathf.Max(0, startIndex);

        if (startIndex != m_startIndex)
        {
            m_startIndex = startIndex;
            UpdateView();
        }
    }

    private void SelectItem(object renderData)
    {
        m_selectedData = renderData;
        if (onItemSelected != null)
            onItemSelected(m_selectedData);
    }

    private void OnItemClick(GameObject target, BaseEventData baseEventData)
    {
        var renderData = target.GetComponent<ItemRender>().m_renderData;
        if (useLoopItems && renderData == m_selectedData)
        {
            var toggle = target.GetComponent<Toggle>();
            if (toggle)
                toggle.isOn = true;
        }
        TargetGo = target;
        SelectItem(renderData);
    }

    private void SetToggle(int index, bool value)
    {
        if (index < m_items.Count)
        {
            var toggle = m_items[index].GetComponent<Toggle>();
            if (toggle)
                toggle.isOn = value;
        }
    }

    

    void Destroy()
    {
        onItemSelected = null;
        m_items.Clear();
    }

    /// <summary>
    /// 选择指定项
    /// </summary>
    /// <param name="index"></param>
    public void Select(int index)
    {
        if (index >= m_data.Length || index < 0)
            return;

        if (m_data[index] != m_selectedData)
            SelectItem(m_data[index]);

        UpdateView();
    }

    /// <summary>
    /// 开启或关闭某一项的响应
    /// </summary>
    /// <param name="index"></param>
    public void Enable(int index, bool isEnable=true)
    {
        if (index < m_items.Count)
        {
            var toggle = m_items[index].GetComponent<Toggle>();
            if (toggle)
            {
                toggle.isOn = isEnable;
                toggle.enabled = isEnable;
                if (isEnable)
                {
                    UGUIClickHandler.Get(toggle.gameObject, m_itemClickSound).onPointerClick += OnItemClick;
                }
                else
                {
                    UGUIClickHandler.Get(toggle.gameObject, m_itemClickSound).RemoveAllHandler();
                }
            }
        }
    }

    /// <summary>
    /// 选择指定项
    /// </summary>
    /// <param name="renderData"></param>
    public void Select(object renderData)
    {
        if (renderData == null)
        {
            SelectItem(null);
            UpdateView();
            return;
        }
        for (int i = 0; i < m_data.Length; i++)
        {
            if (m_data[i] == renderData)
            {
                SelectItem(m_data[i]);
                UpdateView();
                break;
            }
        }
    }
}
