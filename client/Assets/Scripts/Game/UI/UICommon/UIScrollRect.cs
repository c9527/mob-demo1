﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class UIScrollRect : ScrollRect
{
    public int padding = 50;

    void Awake()
    {
        scrollSensitivity = 100;
    }

    override public void OnDrag(PointerEventData eventData)
    {
        var stop_drag = false;
        var content_rect = content.rect;
        content_rect.position = content.rect.position + (Vector2)content.localPosition;
        var offset = content_rect.position - viewRect.rect.position;
        if (horizontal)
        {
            if (offset.x >= padding)
            {
                offset.x = padding;
                stop_drag = true;
            }
            else if (offset.x <= (viewRect.rect.width - content_rect.width - padding))
            {
                offset.x = viewRect.rect.width - content_rect.width - padding;
                stop_drag = true;
            }
        }
        if (vertical)
        {
            if (offset.y >= padding)
            {
                offset.y = padding;
                stop_drag = true;
            }
            else if(offset.y <= (viewRect.rect.height - content_rect.height - padding))
            {
                offset.y = (viewRect.rect.height - content_rect.height - padding);
                stop_drag = true;
            }
        }
        if (stop_drag)
        {
            content.localPosition = (Vector3)(viewRect.rect.position + offset - content.rect.position);
        }
        else
        {
            base.OnDrag(eventData);
        }
    }
}
