﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/3 16:46:20
 * Description:达成成就提示
 * Update History:
 *
 **************************************************************************/

public class PanelTipAchieve : BasePanel
{

    private Text m_txtName;
    private Text m_txtDec;
    private List<int> achieveList;
    private float curTime;
    private float SHOW_TIME = 1.5f;
    private bool isDisappear;
    private FPSTween m_fpsTween;
    private GameObject m_tipsPanel;
    private Vector3 m_pointStart;
    private Vector3 m_pointEnd;
    private Vector3 m_pointMiddle;

    private GameObject m_ClickBg;
    public PanelTipAchieve()
    {
        // 构造器
        SetPanelPrefabPath("UI/Common/PanelTipAchieve");
        AddPreLoadRes("UI/Common/PanelTipAchieve", EResType.UI);
        DontDestroyOnLoad = true;
    }

    public override void Init()
    {
        achieveList = new List<int>();
        m_txtName = m_tran.Find("background/TextName").GetComponent<Text>();
        m_txtDec = m_tran.Find("background/TextDec").GetComponent<Text>();
        EnableMouseEvent = !UIManager.IsBattle();
        m_tipsPanel = m_tran.Find("background").gameObject;
        m_fpsTween = m_tipsPanel.AddComponent<FPSTween>();
        m_pointStart = m_tran.Find("PointBegin").position;
        m_pointEnd = m_tran.Find("PointEnd").position;
        m_pointMiddle = m_tipsPanel.transform.position;
        m_tipsPanel.transform.Find("UI_chengjiudacheng_guang").gameObject.AddComponent<SortingOrderRenderer>();
        m_ClickBg = m_tran.Find("ClickBg").gameObject;
    }
    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_ClickBg).onPointerClick += onClickBgHandler;
    }

    private void onClickBgHandler(GameObject target,PointerEventData eventData)
    {
        clickCloseDeal();
    }

    private void clickCloseDeal()
    {
        m_ClickBg.TrySetActive(false);
        achieveList.RemoveAt(0);
        m_fpsTween.TweenPosition(0.1f, m_pointEnd.x, m_pointEnd.y, m_pointEnd.z, () =>
        {
            m_tipsPanel.TrySetActive(false);
            if (achieveList.Count == 0)
            {
                HidePanel();
            }
            else
            {
                showTipsInfo();
            }
        });
    }

    public override void OnShow()
    {
        achieveList.Clear();
        if (m_params.Length == 0)
        {
            HidePanel();
            return;
        }
        else
        {
            foreach (object obj in m_params)
            {
                achieveList.Add((int)obj);
            }
            curTime = 0;
            showTipsInfo();
        }
    }
    /// <summary>
    /// 显示提示
    /// </summary>
    /// <param name="achieveId"></param>
    public void showTips(int achieveId)
    {
        if (achieveList != null)
        {
            achieveList.Add(achieveId);
        }
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {
    }
    /// <summary>
    /// 显示提示
    /// </summary>
    private void showTipsInfo()
    {
        if (achieveList.Count != 0 && m_fpsTween != null)
        {
            ConfigAchievementLine cfg = ConfigManager.GetConfig<ConfigAchievement>().GetLine(achieveList[0]) as ConfigAchievementLine;
            if (cfg != null)
            {
                m_txtName.text = cfg.Name;
                m_txtDec.text = cfg.Desc;
            }
            m_tipsPanel.transform.position = m_pointStart;
            m_tipsPanel.TrySetActive(true);
            isDisappear = false;
            AudioManager.PlayUISound(AudioConst.levelUp);
            m_fpsTween.TweenPosition(0.1f, m_pointMiddle.x, m_pointMiddle.y, m_pointMiddle.z, () =>
            {
                m_tipsPanel.transform.position = m_pointMiddle;
                if (!UIManager.IsBattle())
                {
                    m_ClickBg.TrySetActive(true);
                }
            });
        }
    }



    public override void Update()
    {
        if (achieveList != null && achieveList.Count != 0)
        {
            if (!UIManager.IsBattle())
            {
                if (!EnableMouseEvent)
                    EnableMouseEvent = true;
                return;
            }
            curTime += Time.deltaTime;
            if (curTime >= SHOW_TIME)
            {
                achieveList.RemoveAt(0);
                if (achieveList.Count == 0)
                {
                    HidePanel();
                }
                else
                {
                    showTipsInfo();
                }
                curTime = 0;
            }
            
            if (!isDisappear && SHOW_TIME - curTime <= 0.2f)
            {
                isDisappear = true;
                m_fpsTween.TweenPosition(0.1f, m_pointEnd.x, m_pointEnd.y, m_pointEnd.z, () =>
                {
                    m_tipsPanel.TrySetActive(false);
                });
            }
        }
    }
}
