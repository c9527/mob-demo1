﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelMiniTip : BasePanel
{
    private GameObject m_content;
    private Text m_text;
    private Text m_name;
    private DataGrid m_propList;
//    private Image m_imgBg;
    private const int OFFSET = 50;
    public PanelMiniTip()
    {
        SetPanelPrefabPath("UI/Common/PanelMiniTip");
        AddPreLoadRes("UI/Common/PanelMiniTip", EResType.UI);
        AddPreLoadRes("UI/Package/ItemPropRender", EResType.UI);
    }
    public override void Init()
    {
        m_content = m_tran.Find("Content").gameObject;
        m_text = m_tran.Find("Content/TextDec").GetComponent<Text>();
        m_name = m_tran.Find("Content/TextName").GetComponent<Text>();
        m_propList = m_tran.Find("Content/prop_list").gameObject.AddComponent<DataGrid>();
        m_propList.useClickEvent = false;
        m_propList.autoSelectFirst = false;
        m_propList.SetItemRender(ResourceManager.LoadUIRef("UI/Package/ItemPropRender"), typeof(ItemPropRender));
//        m_imgBg = m_tran.Find("Content/background").GetComponent<Image>();
    }
    public override void InitEvent()
    {

    }
    public override void OnShow()
    {
        
        var contentRectTran = m_content.GetRectTransform();
        contentRectTran.anchoredPosition = new Vector2(9999, 0f);
        if (m_params != null && m_params.Length != 0)
        {
            MiniTipsData data= m_params[0] as MiniTipsData;
            if(data==null) return;
            if (String.IsNullOrEmpty(data.dec))
            {
                 m_text.text = String.Empty;
                m_text.gameObject.TrySetActive(false);
            }
            else
            {
                m_text.text = data.dec;
                m_text.gameObject.TrySetActive(true);
            }
            m_name.text = data.name;
            Vector2 vec2 = (Vector2)m_params[1];
            var posVec = transform.InverseTransformPoint(new Vector3(vec2.x, vec2.y));
            
            if (data.props != null)
            {
                contentRectTran.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 246);
                m_propList.gameObject.TrySetActive(true);
                m_propList.Data = data.props;
            }
            else
            {
                contentRectTran.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 142);
                m_propList.gameObject.TrySetActive(false);
            }
            TimerManager.SetTimeOut(0.01f, () =>
            {
                Vector2 sizeVec = contentRectTran.sizeDelta;
                float xPos = posVec.x + OFFSET;
                float yPos = posVec.y;
                if (posVec.x > 0)
                {//右边点击区  左边显示
                    xPos = posVec.x - OFFSET - sizeVec.x;
                }

                if (posVec.y - sizeVec.y < (0 - Screen.height / 2))
                {
                    //超出底边界
                    yPos = posVec.y + sizeVec.y;
                }
                contentRectTran.anchoredPosition = new Vector2(xPos, yPos);
            });
           
            
        }
    }
    public override void OnHide()
    {

    }
    public override void OnBack()
    {

    }
    public override void OnDestroy()
    {

    }
    public override void Update()
    {

    }
}
