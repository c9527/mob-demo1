﻿using UnityEngine.UI;

public class ItemDropListRender: ItemRender
{
    private ItemDropInfo m_data;
    private Text m_text;
    public override void Awake()
    {
        m_text = transform.Find("Text").GetComponent<Text>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ItemDropInfo;
        if (m_data == null)
            return;
        m_text.text = m_data.label;
//        m_text.text = data.ToString();
    }
}

public class ItemDropInfo
{
    public string type;
    public string subtype;
    public string label;
    public string sublabel;
}