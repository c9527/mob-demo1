﻿using UnityEngine;
using UnityEngine.UI;

public class TextDownWatch : MonoBehaviour
{
    private float m_fTime;
    private Text m_txt;
    void Start()
    {
        m_txt = transform.GetComponent<Text>();
    }

    public float SetTime
    {
        set
        {
            m_fTime = value;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_txt == null) return;
        
        if(FightVideoManager.isPlayFight == true)
        {
            m_fTime = m_fTime - Time.deltaTime * FightVideoManager.PlaySpeed;
        }else
        {
            m_fTime -= Time.deltaTime;
        }
        m_txt.text = TimeUtil.FormatTime((int)Mathf.Max(m_fTime, 0), 1);
    }
}
