﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIRescueTip : MonoBehaviour 
{
    float _end_time = -1;

    float _offsetX = 44;
    float _offsetY = 44;

    string m_tips;

    GameObject m_uiDirectionTip = null;
    GameObject m_offSet = null;
    GameObject m_sprite = null;
    GameObject m_sec1 = null;
    GameObject m_sec2 = null;

    Sprite m_kSprite;
    ImageDownWatch m_downTimer = null;
    int m_sec = 0;
    bool m_bAroundScreen = false;//屏幕四周
    bool m_bForceHide = false;
    bool m_bShowSprites = true;
    bool m_bShowTimer = false;
    bool m_bIsTweenPos = false;
    TweenPosition m_tweenPos = null;
    Vector3 pos = Vector3.zero;
    Vector3 scale = Vector3.one;
    Vector3 pos2 = Vector3.zero;
    Vector3 pos3 = Vector3.zero;
    Vector3 scale2 = Vector3.one;
    Vector3 offsetVec = Vector3.zero;
	
    public void SetTipSprite( Sprite sprite, bool isTween = false,string tips=null)
    {
        m_kSprite = sprite;
        m_tips = tips;
        m_bIsTweenPos = isTween;
    }

    public void SetPosAndScale(Vector3 pos, Vector3 scale, Vector3 offsetVec = default(Vector3))
    {
        this.pos = pos;
        this.scale = scale;
        this.offsetVec = offsetVec;
    }

    public void SetPosAndScale2(Vector3 pos, Vector3 pos2, Vector3 scale)
    {
        this.pos2 = pos;
        this.pos3 = pos2;
        this.scale2 = scale;
    }

    public void SetLiveTime( float fLiveTime )
    {
        _end_time = Time.time + fLiveTime;
    }

	// Update is called once per frame
    //int log = 0;
	void Update () 
    {
        if (beAlive==false || transform==null)
        {
            if (m_uiDirectionTip != null && m_uiDirectionTip.activeSelf)
            {
                m_uiDirectionTip.TrySetActive(false);
            }
            return;
        }

        if (SceneCamera.singleton == null)
            return;       

        if (m_uiDirectionTip == null)
        {
            m_uiDirectionTip = new GameObject("DirectionTip");
            PanelBattle battle = UIManager.GetPanel<PanelBattle>();
            if (battle != null)
            {
                battle.AddChild(m_uiDirectionTip.transform, false);
                m_uiDirectionTip.transform.SetAsFirstSibling();
            }
            m_offSet = new GameObject("Offset");
            m_offSet.transform.SetParent(m_uiDirectionTip.transform, false);
            m_offSet.transform.localPosition = Vector3.zero;
            m_offSet.transform.localScale = Vector3.one;
            m_uiDirectionTip.ApplyLayer(GameSetting.LAYER_VALUE_UI, null, null, true);
            m_sprite = new GameObject("sprite");
            m_sprite.transform.SetParent(m_offSet.transform, false);
            m_sprite.transform.localPosition = pos;
            m_sprite.transform.localScale = scale;
            Image kImage = m_sprite.AddMissingComponent<Image>();
            kImage.SetSprite(m_kSprite);
            kImage.SetNativeSize();
        }
        if (m_bShowTimer && m_downTimer == null)
        {
            m_sec1 = new GameObject("sec1");
            m_sec2 = new GameObject("sec2");
            m_sec1.transform.SetParent(m_offSet.transform, false);
            m_sec2.transform.SetParent(m_offSet.transform, false);
            m_sec1.transform.localPosition = pos2;
            m_sec2.transform.localPosition = pos3;
            m_sec1.transform.localScale = scale2;
            m_sec2.transform.localScale = scale2;
            m_sec1.AddComponent<Image>();
            m_sec2.AddComponent<Image>();

            m_downTimer = m_offSet.AddComponent<ImageDownWatch>();
            m_downTimer.setNumStyle(AtlasName.Battle, "time", 1);
            m_downTimer.SetTime = m_sec;
        }

        Vector3 screenPos = SceneCamera.singleton.WorldToScreenPoint(transform.position + offsetVec);
        int screenHeight = Screen.height;
        int screenWidth = Screen.width;

        if (!m_bForceHide && screenPos.x >= 0 && screenPos.x <= screenWidth && screenPos.y >= 0 && screenPos.y <= screenHeight && screenPos.z >= 0)
        {
            screenPos.z = 0;
            TweenPos(m_bIsTweenPos);
            m_sprite.transform.localPosition = pos;
            m_uiDirectionTip.transform.position = UIManager.m_uiCamera.ScreenToWorldPoint(screenPos);
            m_uiDirectionTip.TrySetActive(true);

            ShowSprites(true);

            var distance = MainPlayer.singleton != null ? Math.Min((transform.position - MainPlayer.singleton.position).magnitude, 50f) : 0;
            var scale_value = Math.Max(1-distance/50f,0.35f);
            m_uiDirectionTip.transform.localScale = new Vector3(scale_value, scale_value, 1);
        }
        else if (!m_bForceHide && m_bAroundScreen)
        {
            TweenPos(false);

            m_sec1.TrySetActive(false);
            m_sec2.TrySetActive(false);
            m_sprite.TrySetActive(true);

            m_offSet.transform.localPosition = Vector3.zero;
            m_sprite.transform.localPosition = Vector3.zero;

            //var distance = MainPlayer.singleton != null ? Math.Min((transform.position - MainPlayer.singleton.position).magnitude, 50f) : 0;
            //var scale_value = Math.Max(1 - distance / 50f, 0.35f);

            //_offsetX *= scale_value;
            //_offsetY *= scale_value;

            //++log;

            //if(log > 60)
            //{
            //    Logger.Error("before pos: "+screenPos);
            //}
            if (screenPos.z >= 0)
            {
                screenPos.x = screenPos.x < _offsetX ? _offsetX : screenPos.x > screenWidth - _offsetX ? screenWidth - _offsetX : screenPos.x;
                screenPos.y = screenPos.y < _offsetY ? _offsetY : screenPos.y > screenHeight - _offsetY ? screenHeight - _offsetY : screenPos.y;
            }
            else
            {
                //if (log > 60)
                //{
                //    Logger.Error("z < 0 pos: " + screenPos);
                //}
                if (screenPos.y < _offsetY || screenPos.y > screenHeight - _offsetY || screenPos.x < _offsetX || screenPos.x > screenWidth - _offsetX)
                {
                    screenPos.x = screenPos.x < _offsetX ? screenWidth - _offsetX : screenPos.x > screenWidth - _offsetX ? _offsetX : screenWidth - screenPos.x;
                    screenPos.y = screenPos.y < _offsetY ? screenHeight - _offsetY : screenPos.y > screenHeight - _offsetY ? _offsetY : screenPos.y < screenHeight / 2 ? screenHeight - _offsetY : _offsetY;
                }
                else
                {
                    screenPos.x = screenWidth - screenPos.x;
                    screenPos.y = screenPos.y < screenHeight / 2 ? screenHeight - _offsetY : _offsetY;
                }
                screenPos.z = 0;
            }

            //if (log > 60)
            //{
            //    log = 0;
            //    Logger.Error("after pos: " + screenPos);
            //}
            
            m_uiDirectionTip.transform.position = UIManager.m_uiCamera.ScreenToWorldPoint(screenPos);
            m_uiDirectionTip.TrySetActive(true);
            
            m_uiDirectionTip.transform.localScale = new Vector3(0.8f, 0.8f, 1);
        }
        else if (m_uiDirectionTip != null && !m_bShowTimer)
        {
            m_uiDirectionTip.TrySetActive(false);
        }
        else if(m_uiDirectionTip != null)
        {
            ShowSprites(false);
        }
	}

    public void ShowSprites(bool isShow)
    {
        if(m_sec1!= null) m_sec1.TrySetActive(isShow);
        if (m_sec2 != null) m_sec2.TrySetActive(isShow);
        if (m_sprite != null) m_sprite.TrySetActive(isShow);
    }

    public void ForceHide(bool force)
    {
        m_bForceHide = force;
    }

    public void AroundScreen(bool isRound)
    {
        m_bAroundScreen = isRound;
    }

    private void TweenPos(bool isEnable)
    {
        if (m_offSet == null) return;
        if (isEnable && m_tweenPos == null)
        {
            m_tweenPos = TweenPosition.Begin(m_offSet, 1f, new Vector3(0, 40, 0));
            m_tweenPos.style = UITweener.Style.PingPong;
            m_tweenPos.method = UITweener.Method.Linear;
            m_tweenPos.enabled = isEnable;
        }
        else if (m_tweenPos != null)
            m_tweenPos.enabled = isEnable;
    }

    public void ShowDownTimer(int sec)
    {
        if (m_downTimer == null) { m_bShowTimer = true; m_sec = sec; return; }
        m_downTimer.SetTime = sec;

        TweenPos(m_bIsTweenPos);
    }

    public bool beAlive
    {
        get
        {
            return _end_time==-1 || Time.time<_end_time; 
        }
    }

    void OnEnable()
    {
        if (m_uiDirectionTip != null)
        {
            m_uiDirectionTip.TrySetActive(beAlive);
        }
    }

    void OnDisable()//UIDirectionTip因为是挂载在其他GameObject上，如果其他GameObject调用SetActive(false),则Update()不会再运行，会影响m_uiDirectionTip显示生命周期
    {
        if (m_uiDirectionTip != null)
        {
            m_uiDirectionTip.TrySetActive(false);
        }
    }

    public void Destroy()
    {
        if (m_uiDirectionTip != null)
        {
            GameObject.DestroyImmediate(m_uiDirectionTip);
            m_uiDirectionTip = null;
        }
        return;
    }

    public float LeftTime()
    {
        return m_downTimer == null ? 0 : m_downTimer.SetTime;
    }
}
