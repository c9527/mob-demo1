﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIThroughTheWall : MonoBehaviour
{
    private const float UPDATE_INTERVAL = 0f;
    private event Action timerOverHandler=null;

    public bool isPlaying = false;

    private bool isDo = true;
    private float curTime;
    private float interval;
    private float maxDistance = float.MaxValue;

    private Transform cacheTransform;

    private GameObject goRoot;
    private Transform transRoot;
    private GameObject goBgImg;
    private GameObject goLabelTxt;

    

    private Transform targetTrans;
    private Vector3 offsetPos;
    private float targetRadius;

    private TweenPosition m_tweenPos = null;
    private Text m_txtDistance;
    private GameObject m_goStarLevel;
    private List<Image> m_lstStat;

    private float m_updateTick;

    public static UIThroughTheWall createUI()
    {
        GameObject go = new GameObject("UIThroughTheWall");
        UIManager.SetClickEvent(go, false);
        go.AddComponent<RectTransform>();
        var group = go.AddMissingComponent<CanvasGroup>();
        group.interactable = false;
        group.blocksRaycasts = false;
        GameObject root = new GameObject("root");
        root.AddComponent<RectTransform>();
        root.transform.SetParent(go.transform, false);
        
        UIThroughTheWall behavior = go.AddComponent<UIThroughTheWall>();
        behavior.AttachParent();
        return behavior;
    }

    

    public void AttachParent()
    {
        PanelBattle battle = UIManager.GetPanel<PanelBattle>();
        if (battle != null && battle.IsInited())
            UIHelper.SetParent(transform, battle.transform);
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    public void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        AttachParent();
    }

    public void play(string atlas, string name, Transform target, Vector3 offset = default(Vector3), ThroughTheWallProp prop = null)
    {
        ResourceManager.LoadSprite(atlas, name, (sprite) =>
        {
            play(sprite, target, offset, prop);
        });
    }

    public void play(Sprite sprite, Transform target, Vector3 offset = default(Vector3), ThroughTheWallProp prop = null)
    {
        if (sprite == null || gameObject == null) return;

        if (cacheTransform == null) cacheTransform = transform;
        if (goRoot == null) goRoot = cacheTransform.Find("root").gameObject;
        if (transRoot == null) transRoot = goRoot.transform;

        offsetPos = offset;
        targetTrans = target;

        string msg = "";
        bool isTweenPos = false;
        bool isShowDistance = false;
        Vector2 distancePos = Vector2.zero;
        Vector2 txtPos = Vector2.zero;
        TextProp textProp = null;
        RectTransformProp imgProp = null;
        if(prop != null)
        {
            isTweenPos = prop.isTweenPos;
            maxDistance = prop.distance;
            interval = prop.interval;
            textProp = prop.textProp;
            imgProp = prop.imgProp;
            txtPos = prop.textPos;
            msg = prop.msg;
            isShowDistance = prop.isShowDistance;
            distancePos = prop.distancePos;
            targetRadius = prop.targetRadius;
        }

        gameObject.TrySetActive(true);

        UIHelper.ShowSprite(ref goBgImg, "Sprite", sprite, Vector2.zero, true, imgProp, transRoot, true);
        UIHelper.ShowText(ref goLabelTxt, msg, true, txtPos, textProp, transRoot, true);

        if (prop != null)
            ShowStarLevel(prop.starLevel, prop.starSprite, prop.starPos);
        else
            ShowStarLevel(0);

        TweenPos(isTweenPos);
        ShowDistance(isShowDistance, distancePos);
        isPlaying = true;
        //StartCoroutine(playProgress());
    }

    public void ChangeSprite(string name)
    {
        if (transRoot != null && transRoot.Find("Sprite"))
        {
            Image img = transRoot.Find("Sprite").GetComponent<Image>();
            if (img != null)
            {
                img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, name));
            }
        }
        
    }

    public void stop()
    {
        if (this == null)
            return;

        isPlaying = false;
        //StopCoroutine(playProgress());
        curTime = 0f;
        targetTrans = null;
        if (gameObject != null) gameObject.TrySetActive(false);
    }

    void Update()
    {
        m_updateTick += Time.smoothDeltaTime;
        if(m_updateTick >= UPDATE_INTERVAL)
        {
            m_updateTick = 0;
            if (isPlaying)
                playProgress();
        }
    }

    private void playProgress()
    {
        //while (true)
        //{
            curTime += Time.deltaTime;
            if(interval > 0 && curTime >= interval)
            {
                isDo = !isDo;
                curTime = 0;
            }
            if (isDo) calculatePos();
            else goRoot.TrySetActive(false);
            //yield return null;
        //}
    }

    private bool calculatePos()
    {
        if (goRoot == null || targetTrans == null || cacheTransform == null)
            return true;

        var curPlayer = WorldManager.singleton.curPlayer;

        if (curPlayer == null)
            curPlayer = MainPlayer.singleton;

        if (curPlayer == null || curPlayer.released || curPlayer.transform == null || curPlayer.serverData == null || SceneCamera.singleton == null)
        {
            goRoot.TrySetActive(false);
            return true;
        }

        Vector3 targetWorldPos = targetTrans.position + offsetPos;
        var distance = Vector3.Distance(curPlayer.transform.position, targetWorldPos);
        if (distance > maxDistance)
        {
            goRoot.TrySetActive(false);
            return true;
        }

        //显示距离
        if (m_txtDistance != null && m_txtDistance.enabled)
        {
            float showDis = (int)(distance - targetRadius);
            m_txtDistance.text = string.Format("{0}m", showDis >= 0 ? showDis : 0);
        }

        var viewDistance = Vector3.Distance(SceneCamera.singleton.position, targetWorldPos);

        Vector3 screenPos = SceneCamera.singleton.WorldToScreenPoint(targetWorldPos);
        int screenHigth = Screen.height;
        int screenWidth = Screen.width;
        if (screenPos.x >= 0 && screenPos.x <= screenWidth && screenPos.y >= 0 && screenPos.y <= screenHigth && screenPos.z >= 0)
        {
            screenPos.z = 0;
            cacheTransform.position = UIManager.m_uiCamera.ScreenToWorldPoint(screenPos);
            float scale_value = 1f;
            if (viewDistance < 5)
            {
                scale_value = Math.Min(0.324f + 14.4f / (viewDistance * viewDistance), 4.5f);
                cacheTransform.position += new Vector3(0, scale_value - 0.9f, 0);
            }
            else
                scale_value = Math.Max(1 - viewDistance / 50f, 0.35f);
            cacheTransform.localScale = new Vector3(scale_value, scale_value, 1);
            goRoot.TrySetActive(true);
        }
        else
        {
            goRoot.TrySetActive(false);
        }

        return true;
    }

    private void TweenPos(bool isEnable)
    {
        if (goRoot == null) return;
        if (isEnable && m_tweenPos == null)
        {
            m_tweenPos = TweenPosition.Begin(goRoot, 1f, new Vector3(0, 40, 0));
            m_tweenPos.style = UITweener.Style.PingPong;
            m_tweenPos.method = UITweener.Method.Linear;
            m_tweenPos.enabled = isEnable;
        }
        else if (m_tweenPos != null)
            m_tweenPos.enabled = isEnable;
    }

    private void ShowDistance(bool isShow, Vector2 offset)
    {
        if (goRoot == null) return;
        if(isShow && m_txtDistance == null)
        {
            GameObject go = null;
            UIHelper.ShowText(ref go, "", true, offset, new TextProp() { fontSize  = 45}, transRoot);
            m_txtDistance = go.GetComponent<Text>();
        }
        else if (m_txtDistance != null)
        {
            m_txtDistance.enabled = isShow;
        }
    }

    private void ShowStarLevel(int startLevel, string sprite = "", Vector2 offect = default(Vector2))
    {
        if (goRoot == null) return;
        if (startLevel > 0)
        {
            if (m_goStarLevel == null)
            {
                m_goStarLevel = new GameObject("StarLevel");
                m_goStarLevel.transform.SetParent(transRoot);
                m_goStarLevel.transform.ResetLocal();
                m_goStarLevel.AddComponent<RectTransform>().SetAsLastSibling();
                GridLayoutGroup glg = m_goStarLevel.AddComponent<GridLayoutGroup>();
                glg.cellSize = new Vector2(24f, 22f);
                glg.spacing = new Vector2(2f, 0f);
                glg.startCorner = GridLayoutGroup.Corner.UpperLeft;
                glg.startAxis = GridLayoutGroup.Axis.Horizontal;
                glg.childAlignment = TextAnchor.MiddleCenter;
                glg.constraint = GridLayoutGroup.Constraint.Flexible;
                m_lstStat = new List<Image>();
            }
            m_goStarLevel.GetComponent<RectTransform>().anchoredPosition = offect;
            int curStarCount = m_lstStat.Count;
            if (curStarCount < startLevel)
            {
                for (int i = 0; i < curStarCount; ++i)
                {
                    m_lstStat[i].gameObject.TrySetActive(true);
                }

                for (int i = curStarCount; i < startLevel; ++i)
                {
                    var img = new GameObject("star").AddComponent<Image>();
                    img.transform.SetParent(m_goStarLevel.transform);
                    img.transform.localScale = Vector3.one;
                    img.transform.localPosition = Vector3.zero;
                    img.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, sprite));
                    img.SetNativeSize();
                    m_lstStat.Add(img);
                }
            }
            else
            {
                for (int i = 0; i < curStarCount; ++i)
                {
                    m_lstStat[i].gameObject.TrySetActive(i < startLevel);
                }
            }
            m_goStarLevel.TrySetActive(true);
        }
        else if (m_goStarLevel != null)
        {
            m_goStarLevel.TrySetActive(false);
        }
        
    }

    private void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        if (m_lstStat != null)
            m_lstStat.Clear();
    }


}

public class ThroughTheWallProp
{
    public float interval = 0f;
    public string msg = "";
    public Vector2 textPos = Vector2.zero;
    public TextProp textProp = null;
    public RectTransformProp imgProp = null;
    public float distance = float.MaxValue;
    public bool isTweenPos = false;
    public bool isShowDistance = false;
    public Vector2 distancePos = Vector2.zero;
    public float targetRadius = 0f;
    public int starLevel = 0;
    public string starSprite = "pentagram_icon";
    public Vector2 starPos = Vector2.zero;
}
