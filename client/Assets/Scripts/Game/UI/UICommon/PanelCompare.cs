﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelCompare : BasePanel
{
    private Image m_imgEquip1;
    private Image m_imgEquip2;
    private Text m_txtEquipName1;
    private Text m_txtEquipName2;
    private DataGrid m_dataGridProp1;
    private DataGrid m_dataGridProp2;
    private DataGrid m_dataGrid;
    private Image m_imgWord;
    private Text m_txtTip;

    private Button m_btnRare;
    private Text m_txtRare;
    private Image m_imgRareTri;
    private Button m_btnType;
    private Text m_txtType;
    private Image m_imgTypeTri;
    private Button m_btnClose;
    private ItemDropInfo[] m_weaponClassifyArr;
    private ItemDropInfo[] m_rareTypeArr;
    private string m_selectedType;
    private int m_selectedRare;
    private Dictionary<string, int> m_classifyMappingDic; 

    public PanelCompare()
    {
        SetPanelPrefabPath("UI/Common/PanelCompare");
        AddPreLoadRes("UI/Common/PanelCompare", EResType.UI);
        AddPreLoadRes("Atlas/Package", EResType.Atlas);
    }

    public override void Init()
    {
        m_weaponClassifyArr = new[]
        {
            new ItemDropInfo { type = "WEAPON", subtype = "ALL", label = "全部" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "MACHINEGUN", label = "机枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "PISTOL", label = "手枪" },
            new ItemDropInfo { type = "WEAPON", subtype = "RIFLE", label = "步枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SHOTGUN", label = "散弹枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SNIPER", label = "狙击枪" }, 
            new ItemDropInfo { type = "WEAPON", subtype = "SUBMACHINE", label = "冲锋枪" }, 
        };

        m_rareTypeArr = new[]
        {
            new ItemDropInfo {type = "RARETYPE", subtype = "0", label = "全部"},
            new ItemDropInfo {type = "RARETYPE", subtype = "1", label = "普通"},
            new ItemDropInfo {type = "RARETYPE", subtype = "2", label = "优秀"},
            new ItemDropInfo {type = "RARETYPE", subtype = "3", label = "精英"},
            new ItemDropInfo {type = "RARETYPE", subtype = "4", label = "传说"},
            new ItemDropInfo {type = "RARETYPE", subtype = "5", label = "英雄"},
        };

        m_classifyMappingDic = new Dictionary<string, int>
        {
            {"WEAPON1", 1},
            {"WEAPON2", 1},
            {"FLASHBOMB", 2},
            {"GRENADE", 2},
            {"DAGGER", 3}
        };

        m_imgEquip1 = m_tran.Find("left/imgEquip1").GetComponent<Image>();
        m_imgEquip2 = m_tran.Find("left/imgEquip2").GetComponent<Image>();
        m_txtEquipName1 = m_tran.Find("left/txtEquipName1").GetComponent<Text>();
        m_txtEquipName2 = m_tran.Find("left/txtEquipName2").GetComponent<Text>();
        m_dataGridProp1 = m_tran.Find("left/proplist1/content").gameObject.AddComponent<DataGrid>();
        m_dataGridProp1.SetItemRender(m_tran.Find("left/proplist1/content/item").gameObject, typeof(ItemComparePropRender)); ;
        m_dataGridProp1.useClickEvent = false;
        m_dataGridProp1.autoSelectFirst = false;
        m_dataGridProp2 = m_tran.Find("left/proplist2/content").gameObject.AddComponent<DataGrid>();
        m_dataGridProp2.SetItemRender(m_tran.Find("left/proplist1/content/item").gameObject, typeof(ItemComparePropRender)); ;
        m_dataGridProp2.useClickEvent = false;
        m_dataGridProp2.autoSelectFirst = false;
        m_imgWord = m_tran.Find("left/imgWord").GetComponent<Image>();
        m_txtTip = m_tran.Find("left/txtTip").GetComponent<Text>();

        m_btnRare = m_tran.Find("right/btnRare").GetComponent<Button>();
        m_txtRare = m_tran.Find("right/btnRare/Text").GetComponent<Text>();
        m_imgRareTri = m_tran.Find("right/btnRare/Image").GetComponent<Image>();
        m_btnType = m_tran.Find("right/btnType").GetComponent<Button>();
        m_txtType = m_tran.Find("right/btnType/Text").GetComponent<Text>();
        m_imgTypeTri = m_tran.Find("right/btnType/Image").GetComponent<Image>();
        m_btnClose = m_tran.Find("right/btnClose").GetComponent<Button>();
        m_dataGrid = m_tran.Find("right/ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_dataGrid.SetItemRender(m_tran.Find("right/ScrollDataGrid/content/ItemCompareRender").gameObject, typeof(ItemCompareRender));
        m_dataGrid.useLoopItems = true;
        m_dataGrid.autoSelectFirst = false;
    }

    public override void InitEvent()
    {
        m_dataGrid.onItemSelected += OnItemSelected;
        UGUIClickHandler.Get(m_btnRare.gameObject).onPointerClick += OnBtnRareClick;
        UGUIClickHandler.Get(m_btnType.gameObject).onPointerClick += OnBtnTypeClick;
        UGUIClickHandler.Get(m_btnClose.gameObject).onPointerClick += delegate { HidePanel(); };
    }

    public override void OnShow()
    {
        m_txtTip.enabled = true;
        m_imgWord.enabled = true;
        m_txtEquipName2.text = "";
        m_imgEquip2.enabled = false;
        m_dataGridProp1.Data = new object[0];
        m_dataGridProp2.Data = new object[0];

        if (!HasParams())
            return;

        m_selectedType = "ALL";
        m_selectedRare = 0;
        m_txtType.text = "类型";
        m_txtRare.text = "品质";

        var item1 = ItemDataManager.GetItemWeapon((int)m_params[0]);
        m_txtEquipName1.text = item1.DispName;
        m_imgEquip1.enabled = true;
        m_imgEquip1.SetSprite(ResourceManager.LoadIcon(item1.Icon));
        m_imgEquip1.SetNativeSize();

        m_dataGrid.Data = CreateItems();
        m_dataGrid.ResetScrollPosition();

        m_dataGridProp1.Data = ItemDataManager.GetPropRenderInfos(item1);

        if (m_classifyMappingDic.ContainsKey(item1.SubType) && (m_classifyMappingDic[item1.SubType] == 2 || m_classifyMappingDic[item1.SubType] == 3))
        {
            m_btnType.interactable = false;
            m_txtType.color = new Color32(100, 100, 100, 255);
            m_imgTypeTri.color = new Color32(100, 100, 100, 255);
        }
        else
        {
            m_btnType.interactable = true;
            m_txtType.color = new Color32(158, 158, 158, 255);
            m_imgTypeTri.color = Color.white;
        }
    }

    private void OnItemSelected(object renderData)
    {
        var item2 = renderData as ConfigItemWeaponLine;
        m_txtTip.enabled = false;
        m_imgWord.enabled = false;

        m_txtEquipName2.text = item2.DispName;
        m_imgEquip2.enabled = true;
        m_imgEquip2.SetSprite(ResourceManager.LoadIcon(item2.Icon));
        m_imgEquip2.SetNativeSize();

        var item1 = ItemDataManager.GetItemWeapon((int)m_params[0]);
        var prop1 = ItemDataManager.GetPropRenderInfos(item1);
        var prop2 = ItemDataManager.GetPropRenderInfos(item2);
        for (int i = 0; i < prop1.Length; i++)
        {
            prop1[i].valueAdd = prop2[i].value;
            prop2[i].valueAdd = prop1[i].value;
        }
        m_dataGridProp1.Data = prop1;
        m_dataGridProp2.Data = prop2;
    }

    private ConfigItemWeaponLine[] CreateItems()
    {
        var resultList = new List<ConfigItemWeaponLine>();
        var item1 = ItemDataManager.GetItemWeapon((int)m_params[0]);
        if (!m_classifyMappingDic.ContainsKey(item1.SubType))
            return new ConfigItemWeaponLine[0];

        var conigWeapon = ConfigManager.GetConfig<ConfigItemWeapon>();
        if (m_params.Length > 1 && m_params[1].ToString() == "depot")
        {
            var arr = ItemDataManager.m_items;
            for (int i = 0; i < arr.Length; i++)
            {
                var item = conigWeapon.GetLine(arr[i].item_id);
                if (item == null)
                    continue;
                if (!m_classifyMappingDic.ContainsKey(item.SubType))
                    continue;
                if (m_classifyMappingDic[item1.SubType] != m_classifyMappingDic[item.SubType])
                    continue;
                if (m_selectedRare != 0 && m_selectedRare != item.RareType)
                    continue;
                if (m_selectedType != "ALL" && m_selectedType != item.WeaponType)
                    continue;
                resultList.Add(item);
            }
        }
        else
        {

            var arr = ConfigManager.GetConfig<ConfigMall>().GetMallLineByLang();
            var dic = new HashSet<int>();

            for (int i = 0; i < arr.Length; i++)
            {
                if (!ItemDataManager.IsSale(arr[i]))
                    continue;
                var item = conigWeapon.GetLine(arr[i].ItemId);
                if (item == null)
                    continue;
                if (!m_classifyMappingDic.ContainsKey(item.SubType))
                    continue;
                if (m_classifyMappingDic[item1.SubType] != m_classifyMappingDic[item.SubType])
                    continue;
                if (m_selectedRare != 0 && m_selectedRare != item.RareType)
                    continue;
                if (m_selectedType != "ALL" && m_selectedType != arr[i].SubType)
                    continue;
                if (!dic.Contains(arr[i].ItemId))
                {
                    dic.Add(arr[i].ItemId);
                    resultList.Add(item);
                }
            }
        }
        
        resultList.Sort((x, y) => y.RareType - x.RareType);
        return resultList.ToArray();
    }

    private void OnBtnTypeClick(GameObject target, PointerEventData eventdata)
    {
        if (!m_btnType.interactable)
            return;
        UIManager.ShowDropList(m_weaponClassifyArr, OnDropListItemSelected, m_tran, target, 240);
    }

    private void OnBtnRareClick(GameObject target, PointerEventData eventdata)
    {
        if (!m_btnRare.interactable)
            return;
        UIManager.ShowDropList(m_rareTypeArr, OnDropListItemSelected, m_tran, target, 240);
    }

    private void OnDropListItemSelected(ItemDropInfo data)
    {
        m_dataGrid.ResetScrollPosition();
        if (data.type == "RARETYPE")
        {
            m_selectedRare = int.Parse(data.subtype);
            m_dataGrid.Data = CreateItems();
            m_txtRare.text = data.subtype == "0" ? "品质" : data.label;
        }
        else
        {
            m_selectedType = data.subtype;
            m_dataGrid.Data = CreateItems();
            m_txtType.text = data.subtype == "ALL" ? "类型" : data.label;
        }
    }

    public override void OnHide()
    {
        
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
    }
}