﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIImgNumText
{
    public RectTransform transform;
    public GameObject gameObject;

    private int _value = -1;
    //0-居中，1-左对齐，2-右对齐
    public int gap = 0;
    public int align = 0;
    public int fixed_bit = 0;
    public string atlas_name;
    public string sprite_prefix;
    private Sprite[] _sprite_list;
    private bool _style_updated;

    private List<Image> _all_bit_list = new List<Image>();

    public UIImgNumText(Transform parent, string name = null, string atlas = null, int align = 0, int gap = 0, Vector2 pos = default(Vector2), int fixed_bit = 0)
    {
        gameObject = new GameObject("UIImgNumText");
        transform = gameObject.AddComponent<RectTransform>();
        transform.sizeDelta = new Vector2(30, 30);
        if (parent != null)
        {
            transform.SetParent(parent, false);
            transform.localScale = Vector3.one;
        }
        setStyle(name, atlas, align, gap, pos, fixed_bit);
    }

    public int value
    {
        get { return _value; }
        set 
        {
            if (value < 0) return;
            if (_value != value || _style_updated)
            {
                _value = value; 
                Update();
            }
        }
    }
    //true: 就是进行倒计时
    public bool isCountDown
    {
        get;
        set;
    }

    public void Show(bool isShow = true)
    {
        gameObject.TrySetActive(isShow);
    }

    public void setStyle(string name = null, string atlas = null, int align = 0, int gap = 0, Vector2 pos = default(Vector2), int fixed_bit = 0)
    {
        if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(atlas)) return;
        transform.anchoredPosition = pos;
        var old_prefix = sprite_prefix;
        var old_atlas = atlas_name;
        sprite_prefix = string.IsNullOrEmpty(name) ? sprite_prefix : name;
        atlas_name = string.IsNullOrEmpty(atlas) ? atlas_name : atlas;
        this.align = align;
        this.gap = gap;
        this.fixed_bit = fixed_bit;
        if (sprite_prefix != old_prefix || atlas_name != old_atlas || _sprite_list==null)
        {
            _sprite_list = new Sprite[10];
            for (int i = 0; i < _sprite_list.Length; i++)
            {
                _sprite_list[i] = ResourceManager.LoadSprite(atlas_name, sprite_prefix + i.ToString());
            }
        }
        _style_updated = true;
    }

    void Update()
    {
        _style_updated = false;
        if (string.IsNullOrEmpty(atlas_name) || string.IsNullOrEmpty(sprite_prefix))
        {
            return;
        }
        //设置显示数据
        var value_str = value.ToString();
        int count = _all_bit_list.Count;
        int len = Mathf.Max(value_str.Length, fixed_bit);
        //Logger.Error(", val: " + value + ", old: "+(old_list==null ? 0 : old_list.Length)+", new: "+_bit_list.Length);
        for (int i = 0; i < len; i++)
        {
            var img = i >= count ? null : _all_bit_list[i];
            if (img == null)
            {
                img = new GameObject("bit_" + i).AddComponent<Image>();
                img.rectTransform.SetParent(transform, false);
                img.rectTransform.localScale = Vector3.one;
                _all_bit_list.Add(img);
            }
            img.gameObject.TrySetActive(true);
            int bit_value = i >= value_str.Length ? 0 : int.Parse(value_str[i].ToString());
            img.SetSprite(_sprite_list[bit_value]);
            img.SetNativeSize();
        }
        count = _all_bit_list.Count;
        //隐藏多余的对象
        for (int k = len; k < count; k++)
        {
            _all_bit_list[k].gameObject.TrySetActive(false);
        }
        //重新设置位置
        var bit_gap = _all_bit_list[0].rectTransform.sizeDelta.x + gap;
        var offset = Vector2.zero;
        if (align == 0)
        {
            offset.x = -bit_gap * (len - 1) / 2;
        }
        else if (align == 2)
        {
            offset.x = -bit_gap * (len - 1);
        }
        for (int j = 0; j < len; j++)
        {
            _all_bit_list[j].rectTransform.anchoredPosition = offset;
            offset.x += bit_gap;
        }
    }
}
