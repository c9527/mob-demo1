﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQueueObj : MonoBehaviour
{
    class QueueItem
    {
        public GameObject m_go;
        public float m_liveTime;
    }

    class ItemPosInfo
    {
        public Vector2 anchor;
        public Vector2 pivot;
        public Vector2 anchoredPosition;
        public Vector2 sizeDelta;
        public Vector3 localScale;
    }

    public int m_maxQueueSize = 5;
    public int m_staySeconds = 3;
    public int m_iLayout = 0;   //0 左对齐，1：中对齐，2：右对齐 
    private List<QueueItem> m_queueObjList;
    private GameObject        m_goTopFrame;
    private Dictionary<string, Queue<GameObject>> _object_pool;

    private List<ItemPosInfo> m_itemsPosInfo;

    public UIQueueObj()
    {
        _object_pool = new Dictionary<string, Queue<GameObject>>();
    }

    public T GetGameObjectRender<T>(GameObject prototype) where T : ItemRender
    {
        Queue<GameObject> list = null;
        _object_pool.TryGetValue(typeof(T).Name,out list);
        if (list == null)
        {
            list = new Queue<GameObject>();
            _object_pool.Add(typeof(T).Name, list);
        }
        GameObject render = null;
        if (list.Count == 0)
        {
            render = UIHelper.Instantiate(prototype) as GameObject;
            render.AddComponent<Canvas>();
        }
        else
        {
            render = list.Dequeue();
        }
        if (render.activeSelf == false)
        {
            render.TrySetActive(true);
        }
        return render.AddMissingComponent<T>();
    }

    public void Start()
    {
        m_itemsPosInfo = new List<ItemPosInfo>();
        m_queueObjList = new List<QueueItem>();
        m_goTopFrame = gameObject;

        QueueSpaceBuild();
    }

    void QueueSpaceBuild()
    {
        RectTransform frameRectTransform = m_goTopFrame.GetComponent<RectTransform>();
        if( frameRectTransform == null )
        {
            frameRectTransform = m_goTopFrame.AddComponent<RectTransform>();
        }

        float height = frameRectTransform.sizeDelta.y;

        float itemHeight = height / m_maxQueueSize;

        if (m_itemsPosInfo.Count > m_maxQueueSize )
        {
            int delta = m_itemsPosInfo.Count - m_maxQueueSize;
            m_itemsPosInfo.RemoveRange(m_itemsPosInfo.Count - delta, delta );
        }

        Vector2 anchor = new Vector2();
        Vector2 pivot = new Vector2();
        if( m_iLayout == 0 )
        {
            anchor.x = 0;
            anchor.y = 1;
            pivot.x = 0;
            pivot.y = 1;
        }
        else if (m_iLayout == 1 )
        {
            anchor.x = 0.5f;
            anchor.y = 1;
            pivot.x = 0.5f;
            pivot.y = 1;
            
        }
        else if( m_iLayout == 2 )
        {
            anchor.x = 1;
            anchor.y = 1;
            pivot.x = 1;
            pivot.y = 1;
        }

        int itemposInfoSize = m_itemsPosInfo.Count;
        for( int index = 0; index < m_maxQueueSize; ++ index )
        {
            ItemPosInfo itemRectTransform = null;
            if (index < itemposInfoSize)
            {
                itemRectTransform = m_itemsPosInfo[index];
            }
            else
            {
                itemRectTransform = new ItemPosInfo();
            }
                
            itemRectTransform.anchor = anchor;

            itemRectTransform.pivot = pivot;

            itemRectTransform.anchoredPosition = new Vector2(0, -(index * itemHeight));

            itemRectTransform.localScale = new Vector3(1, 1, 1);

            if( index >= itemposInfoSize )
            {
                m_itemsPosInfo.Add( itemRectTransform );
            }
        }
    }

    public int maxqueuesize
    {
        get
        {
            return m_maxQueueSize;
        }
        set
        {
            int oldSize = m_maxQueueSize;
            m_maxQueueSize = value;

            if( oldSize != m_maxQueueSize )
            {
                QueueSpaceBuild();
                if( m_queueObjList.Count > m_maxQueueSize )
                {
                    PopObjs(m_queueObjList.Count - m_maxQueueSize);
                }
                else
                {
                    ShowQueue(false);
                }
            }
        }
    }

    public GameObject framegameobject
    {
        set
        {
            m_goTopFrame = value;
            QueueSpaceBuild();

            if (m_queueObjList.Count > m_maxQueueSize)
            {
                PopObjs(m_queueObjList.Count - m_maxQueueSize);
            }
            else
            {
                ShowQueue(false);
            }
        }
    }

    public void PushObj( GameObject go )
    {
        if( m_queueObjList.Count >= m_maxQueueSize )
        {
            int delta = m_queueObjList.Count - m_maxQueueSize + 1;
            PopObjs( delta );

            QueueItem newItem = new QueueItem();
            newItem.m_go = go;
            newItem.m_liveTime = m_staySeconds;

            m_queueObjList.Add(newItem);

            ShowQueue(true);
        }
        else
        {
            QueueItem newItem = new QueueItem();
            newItem.m_go = go;
            newItem.m_liveTime = m_staySeconds;

            m_queueObjList.Add(newItem);
            ShowQueue( true );
        }
    }

    public void PushTextInfo(string strInfo, Color textColor, int fontSize = 18)
    {
        var go = new GameObject("Info");
        var setText = Util.CreateText(go.transform, new Vector2(-80, 0), fontSize, textColor);
        setText.horizontalOverflow = HorizontalWrapMode.Overflow;
        setText.alignment = TextAnchor.MiddleRight;
        setText.text = strInfo;
        PushObj(go);
    }

    void PopObjs( int num )
    {
        while (num > 0)
        {
            if (m_queueObjList.Count > 0)
            {
                RecycleGameObject(m_queueObjList[0]);
                m_queueObjList.RemoveAt(0);
            }
            --num;
        }
        ShowQueue( false );
    }

    private void RecycleGameObject(QueueItem item)
    {
        if (item==null || item.m_go == null)
        {
            return;
        }
        //item.m_go.TrySetActive(false);
        item.m_go.layer = GameSetting.LAYER_VALUE_DEFAULT;
        var render = item.m_go.GetComponent<ItemRender>();
        if (render != null)
        {
            Queue<GameObject> list = null; 
            _object_pool.TryGetValue(render.GetType().Name, out list);
            if (list != null && list.Count<10)
            {
                list.Enqueue(item.m_go);
            }
            else
            {
                GameObject.Destroy(item.m_go);
            }
        }
        else
        {
            GameObject.Destroy(item.m_go);
        }
        item.m_go = null;
        item.m_liveTime = 0f;
    }

    void ShowQueue( bool bAppend )
    {
        if (!bAppend)
        {
            int index = 0;
            foreach ( QueueItem item in m_queueObjList)
            {
                GameObject go = item.m_go;
                go.layer = GameSetting.LAYER_VALUE_UI;
                if (go.activeSelf == false)
                {
                    go.TrySetActive(true);
                }
                if(go.transform.parent!=m_goTopFrame.transform)
                {
                    
                    go.transform.SetParent(m_goTopFrame.transform);
                }
                RectTransform goRectTransform = go.GetComponent<RectTransform>();
                if (goRectTransform == null)
                {
                    goRectTransform = go.AddComponent<RectTransform>();
                }
                ItemPosInfo indexRectTransform = m_itemsPosInfo[index];
                goRectTransform.anchorMin = indexRectTransform.anchor;
                goRectTransform.anchorMax = indexRectTransform.anchor;
                goRectTransform.pivot = indexRectTransform.pivot;
                goRectTransform.anchoredPosition = indexRectTransform.anchoredPosition;
                goRectTransform.localScale = indexRectTransform.localScale;
                ++index;
            }
        }
        else
        {
            int lastIndex = m_queueObjList.Count - 1;
            GameObject lastGo = m_queueObjList[m_queueObjList.Count - 1].m_go;
            ItemPosInfo lastRectTransform = m_itemsPosInfo[lastIndex];
            lastGo.layer = GameSetting.LAYER_VALUE_UI;
            if (lastGo.activeSelf == false)
            {
                lastGo.TrySetActive(true);
            }
            if (lastGo.transform.parent != m_goTopFrame.transform)
            {
                lastGo.transform.SetParent(m_goTopFrame.transform);
            }
            RectTransform goRectTransform = lastGo.AddMissingComponent<RectTransform>();
            goRectTransform.anchorMin = lastRectTransform.anchor;
            goRectTransform.anchorMax = lastRectTransform.anchor;
            if(goRectTransform.pivot != lastRectTransform.pivot)
                goRectTransform.pivot = lastRectTransform.pivot;
            if (goRectTransform.anchoredPosition != lastRectTransform.anchoredPosition)
                goRectTransform.anchoredPosition = lastRectTransform.anchoredPosition;
            if (goRectTransform.localScale != lastRectTransform.localScale)
                goRectTransform.localScale = lastRectTransform.localScale;
        }
    }

    void Update()
    {
        int expireCount = 0;
        if (m_queueObjList != null)
        {
            foreach (QueueItem item in m_queueObjList)
            {
                item.m_liveTime -= Time.deltaTime;
                if (item.m_liveTime <= 0.0f)
                {
                    ++expireCount;
                }
            }

            if (expireCount > 0)
            {
                PopObjs(expireCount);
            }
        }
    }
}
