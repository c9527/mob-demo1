﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 根据图片列表，按顺序循环播放图片，达到动画的效果
/// </summary>
public class EasyImgAnimation : MonoBehaviour 
{
    public Sprite[] m_listAnimationSprite;
    public int m_limitFrameRate;
	// Use this for initialization
    Image m_imgSpriteContainer;

    int m_iSpriteIndex;
    int m_iSpriteCount;

    float m_fPerFrameTime;
    float m_fElapsFrameTime;
	void Start () 
    {
        m_imgSpriteContainer = gameObject.AddMissingComponent<Image>();
        m_imgSpriteContainer.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        if( m_listAnimationSprite != null )
        {
            SetImgList(m_listAnimationSprite, m_limitFrameRate );
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        m_fElapsFrameTime -= Time.deltaTime;
        if( m_fElapsFrameTime <= 0 )
        {
            int curIndex = (++m_iSpriteIndex) % m_iSpriteCount;
            m_imgSpriteContainer.SetSprite(m_listAnimationSprite[curIndex]);
            m_fElapsFrameTime = m_fPerFrameTime;
        }
        
	}

    public void SetImgList( Sprite[] listSprite,int frameRate )
    {
        m_limitFrameRate = frameRate;
        m_fPerFrameTime = 1.0f / frameRate;
        m_fElapsFrameTime = m_fPerFrameTime;
        m_listAnimationSprite = listSprite;
        if( listSprite.Length > 0 )
        {
            if ( m_imgSpriteContainer == null )
            {
                m_imgSpriteContainer = gameObject.AddMissingComponent<Image>();
            }
            m_imgSpriteContainer.SetSprite(listSprite[0]);
            m_imgSpriteContainer.SetNativeSize();
            m_iSpriteIndex = 0;
            m_iSpriteCount = listSprite.Length;
        }
    }
}
