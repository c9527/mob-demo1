﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class QueueText : MonoBehaviour
{
    public int MaxList = 5;
    public float PopTime = 3;

    Queue<string> m_msgList = new Queue<string>();

    float m_waitTime = float.MaxValue;

    Text m_text = null;

    // Use this for initialization
    void Awake()
    {
        m_text = GetComponent<Text>();
    }
    void Start()
    {
        
        m_text.text = "";
    }

    void ShowText()
    {
        if (m_msgList.Count > MaxList)
        {
            m_waitTime = Time.time + PopTime;
            while (m_msgList.Count > MaxList)
                m_msgList.Dequeue();
        }
        m_text.text = string.Join("\n", m_msgList.ToArray());
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.time;
        if (t >= m_waitTime)
        {
            if (m_msgList.Count > 0)
                m_msgList.Dequeue();
            if (m_msgList.Count > 0)
                m_waitTime += PopTime;
            else
                m_waitTime = float.MaxValue;
            ShowText();
        }
    }

    public void PushMsg(string msg, params object[] args)
    {
        if (args.Length > 0)
            msg = string.Format(msg, args);

        m_msgList.Enqueue(msg);

        if (m_waitTime >= float.MaxValue)
            m_waitTime = Time.time + PopTime;

        ShowText();
    }
}
