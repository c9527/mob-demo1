﻿using UnityEngine;

public class UITween : MonoBehaviour
{
    public Vector3 m_posTarget;
    public float m_fTweenTime;
    private Vector3 m_posSource;
    private float m_fEslapeTime = 0.0f;
    // Use this for initialization
    void Start()
    {
        m_posSource = gameObject.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        m_fEslapeTime += Time.deltaTime;
        float rate = m_fEslapeTime / m_fTweenTime;

        gameObject.transform.localPosition = Vector3.Lerp(m_posSource, m_posTarget, rate);

        if (rate >= 1.0f)
        {
            m_fEslapeTime = 0.0f;
            Vector3 source = m_posSource;
            m_posSource = m_posTarget;
            m_posTarget = source;
        }
    }

    public void TweenTo(Vector3 targetPos, float fTime)
    {
        m_posTarget = targetPos;
        m_fTweenTime = fTime;
    }
}