﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class ImgDigits : MonoBehaviour
{
    Sprite[] m_listImgSprite;
    int m_iSpaceLayout;
    List<GameObject> m_goHadCreate;

    int m_iNumOfDigit = 0;
	public void Init( Sprite[] digitImgList,int space )
    {
        m_listImgSprite = digitImgList;
        m_iSpaceLayout = space;
        m_goHadCreate = new List<GameObject>();
    }

    public void SetDigit( int digit )
    {
        for (int index = 0; index < m_goHadCreate.Count; ++index)
        {
            GameObject go = m_goHadCreate[index];
            go.TrySetActive(false);
        }

        m_iNumOfDigit = 0;

        while( digit / 10 != 0 )
        {
            int baseDigit = digit % 10;
            ++m_iNumOfDigit;
            GenerateNewDigit( baseDigit );
            digit = digit / 10;
        }

        ++m_iNumOfDigit;
        GenerateNewDigit( digit );

        int posOffset = 0;
        for (int index = m_iNumOfDigit - 1; index >= 0; --index)
        {
            GameObject goDigit = m_goHadCreate[index];
            RectTransform rectTransform = goDigit.AddMissingComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(( posOffset * m_iSpaceLayout ), 0 );
            ++posOffset;
            goDigit.TrySetActive( true );
        }
    }

    void GenerateNewDigit( int num )
    {
        Sprite numSprite = m_listImgSprite[num];
        if( m_iNumOfDigit > m_goHadCreate.Count )
        {
            GameObject goNewDigit = new GameObject("num");
            goNewDigit.transform.SetParent(gameObject.transform);
            goNewDigit.transform.localScale = new Vector3(1, 1, 1);
            Image img = goNewDigit.AddMissingComponent<Image>();
            img.SetSprite(numSprite);
            img.SetNativeSize();
            goNewDigit.TrySetActive(false);
            m_goHadCreate.Add(goNewDigit);
        }
        else
        {
            GameObject go = m_goHadCreate[m_iNumOfDigit - 1];
            Image img = go.AddMissingComponent<Image>();
            img.SetSprite(numSprite);
            img.SetNativeSize();
            go.TrySetActive(false);
        }
        
    }

    void OnDestroy()
    {
        for( int index = 0; index < m_goHadCreate.Count; ++ index )
        {
            GameObject goDestory = m_goHadCreate[index];
            goDestory.TrySetActive(false);
            GameObject.DestroyImmediate(goDestory);
        }
        m_goHadCreate.Clear();
    }
}
