﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class PanelPauseMask : BasePanel
{
    static private PanelPauseMask _instance;

    public PanelPauseMask()
    {
        SetPanelPrefabPath("UI/Common/PanelPauseMask");

        AddPreLoadRes("UI/Common/PanelPauseMask", EResType.UI);
    }

    static public void Show()
    {
        if (_instance == null || !_instance.IsOpen())
        {
            _instance = UIManager.PopPanel<PanelPauseMask>(null, false, null, LayerType.Tip);
        }
    }

    static public void Hide()
    {
        if (_instance != null && _instance.IsOpen())
        {
            _instance.HidePanel();
        }
    }

    public override void Init()
    {
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {

    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (UIManager.fullScreen && IsOpen())
        {
            HidePanel();
        }
    }
}
