﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelTip : BasePanel
{
    private TipPanelParams m_data;
    private Text m_content;
    private Text m_txtTitle;
    private RectTransform m_tranOk;
    private RectTransform m_tranCancel;
    private RectTransform m_tranBtnClose;
    private RectTransform m_tranBg;
    private Image m_imgNoCloseBg;
    private Text m_txtOk;
    private Text m_txtCancel;
    private ScrollRect m_scrollRect;
    private Mask m_mask;
    private Image m_maskImage;

    private string m_strDefaultOk;
    private string m_strDefaultCancel;

    private float m_countDownSec;
    private Vector2 m_originSize;

    public PanelTip()
    {
        SetPanelPrefabPath("UI/Common/PanelTip");
        AddPreLoadRes("UI/Common/PanelTip", EResType.UI);
    }

    public override void Init()
    {
        m_content = m_tran.Find("background/TextExp/Text").GetComponent<Text>();
        m_txtTitle = m_tran.Find("background/txtTitle").GetComponent<Text>();
        m_tranOk = m_tran.Find("background/btnlist/btnOk").GetComponent<RectTransform>();
        m_tranCancel = m_tran.Find("background/btnlist/btnCancel").GetComponent<RectTransform>();
        m_tranBtnClose = m_tran.Find("background/btnClose").GetComponent<RectTransform>();
        m_tranBg = m_tran.Find("background").GetComponent<RectTransform>();
        m_imgNoCloseBg = m_tran.Find("background/imgNoCloseBg").GetComponent<Image>();
        m_txtOk = m_tran.Find("background/btnlist/btnOk/Text").GetComponent<Text>();
        m_txtCancel = m_tran.Find("background/btnlist/btnCancel/Text").GetComponent<Text>();

        m_scrollRect = m_tran.Find("background/TextExp").GetComponent<ScrollRect>();
        m_mask = m_tran.Find("background/TextExp").GetComponent<Mask>();
        m_maskImage = m_tran.Find("background/TextExp").GetComponent<Image>();

        m_strDefaultOk = m_txtOk.text;
        m_strDefaultCancel = m_txtCancel.text;
        m_originSize = m_tranBg.sizeDelta;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tranOk).onPointerClick += OnOkClick;
        UGUIClickHandler.Get(m_tranCancel).onPointerClick += OnCancelClick;
        UGUIClickHandler.Get(m_tranBtnClose).onPointerClick += delegate { UIManager.HideTipPanel();};
    }

    private void OnOkClick(GameObject target, PointerEventData eventData)
    {
        if (m_data == null)
            return;
        if (m_data.onOk != null)
            m_data.onOk();
        UIManager.HideTipPanel();
    }

    private void OnCancelClick(GameObject target, PointerEventData eventdata)
    {
        if (m_data == null)
            return;
        if (m_data.onCancel != null)
            m_data.onCancel();
        UIManager.HideTipPanel();
    }

    public override void OnShow()
    {
        if (m_params == null || m_params.Length == 0 || !(m_params[0] is TipPanelParams))
        {
            m_content.text = "";
            m_txtTitle.text = "信息提示";
            return;
        }
        m_data = m_params[0] as TipPanelParams;
        m_countDownSec = m_data.countDownSec;
        m_txtTitle.text = !string.IsNullOrEmpty(m_data.title) ? m_data.title : "信息提示";
        m_tranBg.sizeDelta = new Vector2(m_data.width != 0 ? m_data.width : m_originSize.x, m_data.height != 0 ? m_data.height : m_originSize.y);

        m_content.text = m_data.message;
        m_content.alignment = m_data.leftAlign ? TextAnchor.MiddleLeft : TextAnchor.MiddleCenter;
        m_content.rectTransform.pivot = new Vector2(0.5f, m_data.topAlign ? 1 : 0.5f);
        m_scrollRect.enabled = m_data.canScroll;
        m_mask.enabled = m_data.canScroll;
        m_maskImage.enabled = m_data.canScroll;
        m_tranCancel.gameObject.TrySetActive(m_data.showCancel);
        m_tranBtnClose.gameObject.TrySetActive(m_data.showCloseBtn);
        m_imgNoCloseBg.gameObject.TrySetActive(!m_data.showCloseBtn);

        m_txtOk.text = m_data.okLabel ?? m_strDefaultOk;
        m_txtCancel.text = m_data.cancelLabel ?? m_strDefaultCancel;
        UpdateCountDownSec();
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            if (UIManager.IsBattle())
            {
                Screen.lockCursor = false;
            }
        }
    }

    public override void OnHide()
    {
        m_params = null;
        m_data = null;
        m_countDownSec = 0;
        DontDestroyOnLoad = false;
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            if (UIManager.IsBattle())
            {
                Screen.lockCursor = true;
            }
        }
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (m_countDownSec > 0)
        {
            m_countDownSec -= Time.deltaTime;
            UpdateCountDownSec();
        }
    }

    private void UpdateCountDownSec()
    {
        if (m_data == null)
            return;

        if (m_countDownSec < 0)
            m_countDownSec = 0;
        if (m_data.countDownType == 1)
        {
            m_txtOk.text = m_data.okLabel ?? m_strDefaultOk + "(" + Mathf.CeilToInt(m_countDownSec) + ")";
            if (m_countDownSec <= 0)
                OnOkClick(null, null);
        }
        else if (m_data.countDownType == 2)
        {
            m_txtCancel.text = m_data.cancelLabel ?? m_strDefaultCancel + "(" + Mathf.CeilToInt(m_countDownSec) + ")";
            if (m_countDownSec <= 0)
                OnCancelClick(null, null);
        }
        else if (m_data.countDownType == 3)
            m_content.text = m_data.message + "\n\n(" + Mathf.CeilToInt(m_countDownSec) + ")";
    }
}

public class TipPanelParams
{
    public string message;
    public string title;
    public bool leftAlign;
    public bool topAlign;
    public float width;
    public float height;
    public bool canScroll;
    public Action onOk;
    public Action onCancel;
    public bool showCancel;
    public bool showCloseBtn;
    public string okLabel;
    public string cancelLabel;
    public float countDownSec; //倒计时秒
    public int countDownType; //倒计时类型  1确定倒计时 2取消倒计时 3无操作倒计时
}