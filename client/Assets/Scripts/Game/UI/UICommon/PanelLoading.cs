﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PanelLoading : BasePanel
{
    private Text m_content;
    private Transform m_tranLoading;
    private static int m_loadingCount = 0;

    public PanelLoading()
    {
        SetPanelPrefabPath("UI/Common/PanelLoading");
        AddPreLoadRes("UI/Common/PanelLoading", EResType.UI);
    }

    public override void Init()
    {
        m_content = m_tran.Find("Text").GetComponent<Text>();
        m_tranLoading = m_tran.Find("imgLoading");
    }

    public override void InitEvent()
    {
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        var callBack = (Action) m_params[1];
        m_loadingCount++;
        ResourceManager.LoadMulti((ResourceItem[])m_params[0], null, () =>
        {
            m_loadingCount--;
            if (m_loadingCount <= 0)
                TimerManager.SetTimeOut(0.5f, HidePanel);
            if (callBack != null)
                callBack();
        });
    }

    public override void OnHide()
    {
        m_params = null;
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }

    public override void Update()
    {
        if (IsOpen())
        {
            m_tranLoading.Rotate(0, 0, -360 * Time.deltaTime * 2, Space.Self);
        }
    }
}