﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIRoleIconVipBg : MonoBehaviour
{
    public void SetVipInfo(int lv)
    {
        string vipBg = "vipBgFrame";
        GameObject vipBgGO;
        Image bgImg;
        if (gameObject.transform.Find("vipBgFrame") == null)
        {
            vipBgGO = new GameObject(vipBg);
            vipBgGO.transform.SetParent(gameObject.transform);
            bgImg = vipBgGO.AddComponent<Image>();
            bgImg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "partSubIcon"));
            bgImg.SetNativeSize();
        }
        else
        {
            vipBgGO = gameObject.transform.Find(vipBg).gameObject;
            bgImg = vipBgGO.GetComponent<Image>();
            gameObject.transform.Find(vipBg).gameObject.TrySetActive(true);
        }
        RectTransform rtform = bgImg.rectTransform;
        rtform.anchoredPosition3D = Vector3.zero;
        rtform.localScale = Vector3.one;
        bgImg.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public static UIRoleIconVipBg Get(GameObject go)
    {
        UIRoleIconVipBg script = go.GetComponent<UIRoleIconVipBg>();
        if (script == null)
        {
            script = go.AddComponent<UIRoleIconVipBg>();
        }
        return script;
    }
}
