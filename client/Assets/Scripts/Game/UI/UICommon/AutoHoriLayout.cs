﻿using UnityEngine;
using System.Collections;

public class AutoHoriLayout : MonoBehaviour 
{

	// Use this for initialization
    public float m_fObjInterval;
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public float interval
    {
        get
        {
            return m_fObjInterval;
        }
        set
        {
            m_fObjInterval = value;
        }
    }
    void OnEnable()
    {
        float leftLen = 0;
        Vector2 anchor = new Vector2(0, 0.5f);
        for( int index = 0; index < transform.childCount; ++ index )
        {
            RectTransform rectChild = transform.GetChild(index).gameObject.AddMissingComponent<RectTransform>();
            rectChild.anchorMin = anchor;
            rectChild.anchorMax = anchor;

            Vector2 pos = rectChild.anchoredPosition;
            pos.x = leftLen + rectChild.sizeDelta.x / 2 + index * m_fObjInterval;
            rectChild.anchoredPosition = pos;

            leftLen += rectChild.sizeDelta.x;
        }
        ///父窗口的宽度自适应
        RectTransform parentRectTrans = gameObject.GetComponent<RectTransform>();
        Vector2 size = parentRectTrans.sizeDelta;
        size.x = leftLen + 2;
        parentRectTrans.sizeDelta = size;
    }

    public void HoriLayout()
    {
        OnEnable();
    }

}
