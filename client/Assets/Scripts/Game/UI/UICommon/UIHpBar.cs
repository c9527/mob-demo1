﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 头顶血条UI
/// </summary>
public class UIHpBar : MonoBehaviour
{
    private bool m_isPlaying;
    private bool m_isDo = true;
    
    private Image m_imgHpBar;
    private BasePlayer m_kPlayer;
    private PlayerServerData m_kServerData;

    private float curTime;
    private float interval;
    private float maxDistance = float.MaxValue;

    private GameObject m_goRoot;
    private Transform m_transRoot;
    private Transform m_cacheTransform;

    private Transform m_targetTrans;
    private Vector3 m_offsetPos;

    private Sprite m_spRedHp;
    private Sprite m_spGreenHp;
    private Sprite m_spYellowHp;

    private int m_hpBarType = -1;

    void Awake()
    {
        m_cacheTransform = transform;
        m_goRoot = m_cacheTransform.Find("root").gameObject;
        m_transRoot = m_goRoot.transform;
        m_goRoot.TrySetActive(false);

        GameObject bgGo = new GameObject("HpBarBg");
        bgGo.transform.SetParent(m_transRoot);
        bgGo.transform.ResetLocal();
        Image bg = bgGo.AddComponent<Image>();
        bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "life_bar_bg"));
        bg.SetNativeSize();
        
        GameObject hpGo = new GameObject("Hp");
        hpGo.transform.SetParent(bgGo.transform);
        hpGo.transform.ResetLocal();
        m_imgHpBar = hpGo.AddComponent<Image>();
        m_imgHpBar.type = Image.Type.Filled;
        m_imgHpBar.fillMethod = Image.FillMethod.Horizontal;
        m_imgHpBar.fillOrigin = (int)Image.OriginHorizontal.Left;
        m_spGreenHp = ResourceManager.LoadSprite(AtlasName.Battle, "life_bar_green");
        m_imgHpBar.SetSprite(m_spGreenHp);
        m_imgHpBar.SetNativeSize();
    }

    public void BindPlayer(BasePlayer basePlayer)
    {
        m_kPlayer = basePlayer;
        if (basePlayer != null)
            m_kServerData = basePlayer.serverData;
        else
            m_kServerData = null;
    }

    public static UIHpBar Create()
    {
        GameObject go = new GameObject("UIHpBar");
        UIManager.SetClickEvent(go, false);
        go.AddComponent<RectTransform>();
        var group = go.AddMissingComponent<CanvasGroup>();
        group.interactable = false;
        group.blocksRaycasts = false;
        GameObject root = new GameObject("root");
        root.AddComponent<RectTransform>();
        root.transform.SetParent(go.transform, false);

        UIHpBar behavior = go.AddComponent<UIHpBar>();
        behavior.AttachParent();
        return behavior;
    }

    public void AttachParent()
    {
        PanelBattle battle = UIManager.GetPanel<PanelBattle>();
        if (battle != null && battle.IsInited())
            UIHelper.SetParent(transform, battle.transform);
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    public void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        AttachParent();
    }

    public void Play(Transform target, Vector3 offset = default(Vector3))
    {
        if(gameObject == null) 
            return;

        if (m_cacheTransform == null) 
            m_cacheTransform = transform;
        if (m_goRoot == null) m_goRoot = 
            m_cacheTransform.Find("root").gameObject;
        if (m_transRoot == null) 
            m_transRoot = m_goRoot.transform;

        m_offsetPos = offset;
        m_targetTrans = target;

        gameObject.TrySetActive(true);
        m_isPlaying = true;
    }

    public void Stop()
    {
        if (this == null)
            return;

        m_isPlaying = false;
        curTime = 0f;
        m_targetTrans = null;
        if (gameObject != null) 
            gameObject.TrySetActive(false);
    }

    void Update()
    {
        if (m_isPlaying)
            playProgress();
    }

    private void playProgress()
    {
        if (interval == 0 || (curTime += Time.deltaTime) >= interval)
        {
            curTime = 0;
            //if(m_kPlayer.isRendererVisible)
            //{
            //    if (!m_goRoot.activeSelf)
            //        m_goRoot.TrySetActive(true);
            //    calculatePos();
            //    UpdateHp();
            //}
            //else
            //{
            //    if (m_goRoot.activeSelf)
            //        m_goRoot.TrySetActive(false);
            //}
            calculatePos();
            UpdateHp();
        }
    }

    private void UpdateHp()
    {
        if (m_kServerData == null)
            return;
        float hpScale = (float)m_kServerData.life / m_kServerData.maxhp();
        int hpType = -1;
        Sprite spHp;

        if (hpScale > GameConst.YELLOW_MAX_HP) //绿血
        {
            hpType = GameConst.GREEN_HP;
            if (m_spGreenHp == null)
                m_spGreenHp = ResourceManager.LoadSprite(AtlasName.Battle, "life_bar_green");
            spHp = m_spGreenHp;
        }
        else if (hpScale > GameConst.RED_MAX_HP) //黄血
        {
            hpType = GameConst.YELLOW_HP;
            if (m_spYellowHp == null)
                m_spYellowHp = ResourceManager.LoadSprite(AtlasName.Battle, "life_bar_yellow");
            spHp = m_spYellowHp;
        }
        else //红血
        {
            hpType = GameConst.RED_HP;
            if (m_spRedHp == null)
                m_spRedHp = ResourceManager.LoadSprite(AtlasName.Battle, "life_bar_red");
            spHp = m_spRedHp;
        }

        if(m_hpBarType != hpType)
        {
            m_imgHpBar.SetSprite(spHp);
            m_imgHpBar.SetNativeSize();
            m_hpBarType = hpType;
        }
        m_imgHpBar.fillAmount = hpScale;
    }

    private bool calculatePos()
    {
        if (m_goRoot == null || m_targetTrans == null || m_cacheTransform == null)
            return true;

        var curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null || curPlayer.released || curPlayer.transform == null || curPlayer.serverData == null || SceneCamera.singleton == null)
        {
            m_goRoot.TrySetActive(false);
            return true;
        }

        Vector3 targetWorldPos = m_targetTrans.position + m_offsetPos;
        var distance = Vector3.Distance(curPlayer.transform.position, targetWorldPos);
        if (distance > maxDistance)
        {
            m_goRoot.TrySetActive(false);
            return true;
        }

        Vector3 screenPos = SceneCamera.singleton.WorldToScreenPoint(targetWorldPos);
        int screenHigth = Screen.height;
        int screenWidth = Screen.width;
        if (screenPos.x >= 0 && screenPos.x <= screenWidth && screenPos.y >= 0 && screenPos.y <= screenHigth && screenPos.z >= 0)
        {
            screenPos.z = 0;
            m_cacheTransform.position = UIManager.m_uiCamera.ScreenToWorldPoint(screenPos);
            float scale_value = 1f;
            if (distance < 5)
                scale_value = Math.Min(0.324f + 14.4f / (distance * distance), 4.5f);
            else
                scale_value = Math.Max(1 - distance / 50f, 0.35f);
            m_cacheTransform.localScale = new Vector3(scale_value, scale_value, 1);
            m_goRoot.TrySetActive(true);
        }
        else
        {
            m_goRoot.TrySetActive(false);
        }
        return true;
    }

    public bool isPlaying
    {
        get { return m_isPlaying; }
    }

    public Vector3 offsetPos
    {
        set { m_offsetPos = value; }
    }

}
