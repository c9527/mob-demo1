﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// 图文混排组件，支持不同大小的图片
/// </summary>
[RequireComponent(typeof(Text))]
public class MotionText : MonoBehaviour
{
    static private Sprite m_motion_sprite;
    static private Sprite m_motion_vip_sprite;
    static private ConfigMiscLine m_motion_info;
    private Text m_text;
    private float m_occupyPixelWidth;
    private int m_maxImageSize;
    private readonly List<string> m_motionList = new List<string>();
    private const string OCCUPY_CHAR = "_";
    private const string OCCUPY_PRE_STR = "<color='#00000000'>";
    private const string OCCUPY_SUF_STR = "</color>";
    private const string REGEX_MATCH_LINK = @"\[.*?\]";
    private const string REGEX_MATCH_OCCUPY = "(?<=" + OCCUPY_PRE_STR + "\n*)" + OCCUPY_CHAR + "+(?=\n*" + OCCUPY_SUF_STR + ")";
    private readonly List<GameObject> m_imageList = new List<GameObject>();
    private readonly List<KeyValue<string, List<GameObject>>> m_link_list = new List<KeyValue<string, List<GameObject>>>();

    private Action<string> m_OnLinkCallBack;
    private string m_userName;  //显示这个混排图文的拥有者

    public int MaxImageSize{ set { m_maxImageSize = value; } get { return m_maxImageSize; }}

    public void Create(Action<string> link_callback=null, string userName = null)
    {
        m_userName = userName;
        ClearLinks();
        ClearImages();
        m_motionList.Clear();

        m_OnLinkCallBack = link_callback;

        m_text = GetComponent<Text>();
        var g = m_text.cachedTextGeneratorForLayout;
        var gs = m_text.GetGenerationSettings(m_text.rectTransform.sizeDelta);
        m_occupyPixelWidth = g.GetPreferredWidth(OCCUPY_CHAR, gs);

        //将表情标记替换为占位符
        var content_str = m_text.text;        
        content_str = Regex.Replace(content_str, @"(?<!')#(\d\d)", OnMatchMotionKey);
        //将链接更新为显示格式
        content_str = Regex.Replace(content_str, @"<a=?(.*?)>(.*?)</a>", OnMatchLinkKey);
        m_text.text = content_str;
        g.Populate(m_text.text, gs);

        //处理边界换行问题，判断表情占位符是否跨行。而且必须在处理完一个需要换行的表情后，重新排版文本再继续处理
        var match = Regex.Match(m_text.text, REGEX_MATCH_OCCUPY);
        var offset = 0; //每插入一个\n，偏移量需要+1，因为实际的文本因插入\n而变化，但正则的文本是不变的
        IList<UILineInfo> lines;
        while (match.Success)
        {
            var index = match.Index + offset;
            lines = m_text.cachedTextGeneratorForLayout.lines;
            for (int i = 0; i < lines.Count; i++)
            {
                var startIndex = lines[i].startCharIdx;
                if (startIndex > index && startIndex <= index + match.Value.Length)
                {
                    m_text.text = m_text.text.Insert(index - OCCUPY_PRE_STR.Length, "\n");
                    offset++;
                    g.Populate(m_text.text, gs);
                }
            }
            match = match.NextMatch();
        }

        //处理行高
        var lineHeightDic = new Dictionary<int, float>();
        var motionIndex = 0;
        match = Regex.Match(m_text.text, REGEX_MATCH_OCCUPY);
        lines = m_text.cachedTextGeneratorForLayout.lines;
        while (match.Success)
        {
            var index = GetLineIndex(lines, match.Index);
            if (!lineHeightDic.ContainsKey(index))
                lineHeightDic[index] = 0;

            var motionSprite = GetMotionSprite(m_motionList[motionIndex]);
            if (motionSprite != null && motionSprite.rect.height > lineHeightDic[index])
                lineHeightDic[index] = motionSprite.rect.height;

            motionIndex++;
            match = match.NextMatch();
        }
        for (int i = lines.Count - 1; i >= 0; i--)
        {
            if (lineHeightDic.ContainsKey(i))
            {
                //当插入的位置是起始位置或者当前位置已经有\n(上一步处理边界问题插入的，只会有一个，玩家是不允许输入\n的)存在，则少输出一个\n
                var index = lines[i].startCharIdx;
                var countOffset = index == 0 || m_text.text[index - 1] == '\n' ? -1 : 0;
                m_text.text = m_text.text.Insert(index, GetEnterByHeight(lineHeightDic[i], countOffset));
            }
        }
        g.Populate(m_text.text, gs);

        //创建表情
        match = Regex.Match(m_text.text, REGEX_MATCH_OCCUPY);
        motionIndex = 0;
        while (match.Success)
        {
            var left = g.characters[match.Index].cursorPos / m_text.pixelsPerUnit;
            var right = g.characters[match.Index + match.Value.Length - 1].cursorPos / m_text.pixelsPerUnit;
            CreateMotion(m_motionList[motionIndex], new Vector2(0.5f * (left.x + right.x + m_occupyPixelWidth), left.y - m_text.fontSize));
            match = match.NextMatch();
            motionIndex++;
        }

        //创建链接
        if (m_OnLinkCallBack != null)
        {
            match = Regex.Match(m_text.text, REGEX_MATCH_LINK);
            lines = g.lines;
            var link_index = 0;
            var text_w = m_text.rectTransform.sizeDelta.x;
            var offset_w = text_w * m_text.rectTransform.pivot.x;
            while (match.Success && link_index < m_link_list.Count)
            {
                var link_data = m_link_list[link_index];
                if (!string.IsNullOrEmpty(link_data.key))//有效url
                {
                    var start_line_index = GetLineIndex(lines, match.Index);
                    var end_line_index = GetLineIndex(lines, match.Index + match.Value.Length - 1);
                    var spos = g.characters[match.Index].cursorPos / m_text.pixelsPerUnit;
                    var epos = g.characters[match.Index + match.Value.Length - 1].cursorPos / m_text.pixelsPerUnit;
                    for (int i = start_line_index; i <= end_line_index; i++)
                    {
                        var link_img = new GameObject("link_" + link_index).AddComponent<Image>();
                        link_data.value.Add(link_img.gameObject);
                        link_img.color = new Color(0, 0, 0, 0);
                        var rectTransform = link_img.rectTransform;
                        rectTransform.SetParent(transform, false);
                        rectTransform.anchorMin = new Vector2(0.5f, 1f);
                        rectTransform.anchorMax = new Vector2(0.5f, 1f);
                        rectTransform.pivot = new Vector2(0, 1f);
                        var link_w = text_w;
                        if (i == start_line_index)
                        {
                            link_w -= offset_w + spos.x;
                        }
                        if (i == end_line_index)
                        {
                            link_w -= text_w - (offset_w + epos.x);
                        }
                        rectTransform.sizeDelta = new Vector2(Mathf.Max(link_w,38),Mathf.Max(lines[i].height,m_text.fontSize));
                        rectTransform.localPosition = spos;
                        UGUIClickHandler.Get(link_img.gameObject).onPointerClick += OnLinkClick;
                        spos = new Vector2(-offset_w, spos.y - lines[i].height);
                    }
                }
                match = match.NextMatch();
                link_index++;
            }
        }
    }

    private void OnLinkClick(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        Logger.Log("点击文本超链接：" + target.name);
        if (m_OnLinkCallBack != null)
        {
            var index = int.Parse(target.name.Substring(target.name.LastIndexOf("_") + 1));
            if (index < m_link_list.Count && !string.IsNullOrEmpty(m_link_list[index].key))
            {
                m_OnLinkCallBack(m_link_list[index].key);
            }
        }
    }

    private void ClearLinks()
    {
        foreach (var link in m_link_list)
        {
            foreach(var link_obj in link.value)
            {
                Destroy(link_obj.gameObject);
            }
            link.value.Clear();
        }
        m_link_list.Clear();
        m_OnLinkCallBack = null;
    }

    private string OnMatchLinkKey(Match match)
    {
        var url = match.Groups[1].Value;
        m_link_list.Add(new KeyValue<string, List<GameObject>>(url, new List<GameObject>()));
        return "[" + match.Groups[2].Value + "]";
    }

    private string OnMatchMotionKey(Match match)
    {
        var motionKey = match.Groups[1].Value;
        var motionSprite = GetMotionSprite(motionKey);
        if (motionSprite != null)
        {
            m_motionList.Add(motionKey);
            return GetOccupyByWidth(motionSprite.rect.width);
        }
        return match.Groups[0].Value;
    }

    /// <summary>
    /// 获取表情
    /// </summary>
    /// <param name="motionKey"></param>
    /// <returns></returns>
    private Sprite GetMotionSprite(string motionKey)
    {
        if (m_motion_sprite == null)
        {
            m_motion_sprite = ResourceManager.LoadSprite(AtlasName.Common, "motion");
        }
        if (m_motion_info == null)
        {
            m_motion_info = ConfigManager.GetConfig<ConfigMisc>().GetLine("chat_motion");
        }
        var key_value = 0;
        int.TryParse(motionKey, out key_value);
        var row_value = (int)(key_value / 10);
        var col_value = key_value % 10;
        if(row_value < 6)
        {
            if (m_motion_sprite != null)
            {
                /*var key_value = 0;
                int.TryParse(motionKey, out key_value);
                var row_value = (int)(key_value / 10);
                var col_value = key_value % 10;*/
                if ((row_value * 8 + col_value) >= m_motion_info.ValueInt)
                {
                    row_value = 0;
                    col_value = 0;
                }
                var rect = new Rect(col_value * 32, row_value * 32, 32, 32);
                rect.position += m_motion_sprite.textureRect.position;
                if (rect.xMax > m_motion_sprite.textureRect.xMax)
                    return null;
                return Sprite.Create(m_motion_sprite.texture, rect, Vector2.one / 2);
            }
        }
        else
        {
            if (!RightManager.Has(m_userName, RightManager.RIGHT_MOTION_DOU_WA))
                return null;
            m_motion_vip_sprite = ResourceManager.LoadSprite(AtlasName.Common, "motion_hero_card" + row_value.ToString() + col_value.ToString());
            return m_motion_vip_sprite;
        }
        return null;
    }

    /// <summary>
    /// 在指定位置创建表情
    /// </summary>
    /// <param name="motionKey"></param>
    /// <param name="pos"></param>
    private void CreateMotion(string motionKey, Vector2 pos)
    {
        var go = new GameObject("motion" + motionKey);
        var rect = go.AddComponent<RectTransform>();
        rect.SetParent(transform, false);
        rect.pivot = new Vector2(0.5f, 0);
        rect.localPosition = pos;
        var image = go.AddComponent<Image>();
        image.SetSprite(GetMotionSprite(motionKey));
        if (m_maxImageSize>0)
        {
            var height = image.sprite.rect.height;
            var width = image.sprite.rect.width;
            if (height > width)
            {
                if (height > m_maxImageSize)
                    image.rectTransform.sizeDelta = new Vector2(width * m_maxImageSize / height, m_maxImageSize);
                else
                    image.SetNativeSize();
            }
            else
            {
                if (width > m_maxImageSize)
                    image.rectTransform.sizeDelta = new Vector2(m_maxImageSize, height * m_maxImageSize / width);
                else
                    image.SetNativeSize();
            }
        }
        else
            image.SetNativeSize();
        m_imageList.Add(go);
    }

    /// <summary>
    /// 使用透明的_符号，而不用空格，因为空格会导致异常的自动换行
    /// </summary>
    /// <param name="spriteWidth"></param>
    /// <returns></returns>
    public string GetOccupyByWidth(float spriteWidth)
    {
        if (m_maxImageSize != 0)
            spriteWidth = Mathf.Min(spriteWidth, m_maxImageSize);
        var occupyCount = Mathf.CeilToInt(spriteWidth / (m_occupyPixelWidth / m_text.pixelsPerUnit));
        if (occupyCount > 0)
        {
            var occupy = OCCUPY_PRE_STR;
            for (int i = 0; i < occupyCount; i++)
            {
                occupy += OCCUPY_CHAR;
            }
            occupy += OCCUPY_SUF_STR;
            return occupy;
        }
        return "";
    }

    /// <summary>
    /// 根据图片高度返回换行符
    /// </summary>
    /// <param name="spriteHeight"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    private string GetEnterByHeight(float spriteHeight, int offset)
    {
        if (m_maxImageSize != 0)
            spriteHeight = Mathf.Min(spriteHeight, m_maxImageSize);
        var enterCount = Mathf.RoundToInt(spriteHeight/(m_text.fontSize));
        if (enterCount > 0)
        {
            var enter = "";
            for (int i = 0; i < enterCount + offset; i++)
            {
                enter += "\n";
            }
            return enter;
        }
        return "";
    }

    /// <summary>
    /// 获取字符所在行
    /// </summary>
    /// <param name="lines"></param>
    /// <param name="charIndex"></param>
    /// <returns></returns>
    private int GetLineIndex(IList<UILineInfo> lines, int charIndex)
    {
        if (lines.Count == 0)
            return 0;
        for (int i = 0; i < lines.Count-1; i++)
        {
            if (charIndex >= lines[i].startCharIdx && charIndex < lines[i + 1].startCharIdx)
                return i;
        }
        return lines.Count - 1;
    }

    private void ClearImages()
    {
        for (int i = 0; i < m_imageList.Count; i++)
        {
            Destroy(m_imageList[i]);
        }
    }
}