﻿using UnityEngine;
using System.Collections;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：技术统计面板伤害数据
//创建者：hwl
//创建日期: 2016/3/02
///////////////////////////////////////////
public class HarmInfo
{
    public int beActorId;
    public long pid;
    public int harm;
    public string beActorName;
    public bool die;
    public int part = -2;//击杀部位
    public int killByMyself = -1;// -1：为赋值  0：被他杀  1：被我杀
    public HarmInfo()
    {

    }
}
