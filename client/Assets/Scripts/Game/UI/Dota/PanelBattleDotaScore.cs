﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PanelBattleDotaScore : BasePanel
{
    public DataGrid bossDataGridRight;
    public static List<OtherPlayer> listBossRight = new List<OtherPlayer>();
    public DataGrid bossDataGridLeft;
    public static List<OtherPlayer> listBossLeft = new List<OtherPlayer>();

    protected TextDownWatch m_kDownTimerRight;
    protected TextDownWatch m_kDownTimerLeft;

    protected Image m_bossIconRight;
    protected Image m_bossIconLeft;

    protected GameObject m_bossReviveRight;
    protected GameObject m_bossReviveLeft;

    protected Vector2 pickupPos = new Vector2(28, 0);
    protected Vector2 monsterPos = new Vector2(50, 0);

    protected ImageDownWatch m_kDownTimer;
    protected UIImgNumText m_roundNumTxt;

    //protected Text m_revivecoin;
    protected Text m_supplypoint;
    protected Text m_killCount;

    protected bool m_showTipBtn = false;
    //private TextDownWatch m_bossDownTimer;

    protected float m_curRoundTotalTime;
    protected int m_configRound;
    protected string m_winType; //胜利条件（配表）
    protected DotaBehavior m_behavior;

    protected Transform leftHeadTrans;
    protected Transform rightHeadTrans;
    private HeadList m_leftHeadList;
    private HeadList m_rightHeadList;
    public int m_rightNum;
    public int m_leftNum;

    protected GameObject m_buyPlayerBuffBtn;
    protected GameObject m_buyPlayerBuffEffect;

    //以下为基地
    protected GameObject m_goBaseInfoRight;
    private BasePlayer m_basePlayerRight;
    private Image m_baseHpRight;
    private Image m_baseHpBarRight;
    private float m_lastTimeRight;

    protected GameObject m_goBaseInfoLeft;
    private BasePlayer m_basePlayerLeft;
    private Image m_baseHpLeft;
    private Image m_baseHpBarLeft;
    private float m_lastTimeLeft;


    private GameObject m_beingAttackTips;
    private int m_beingAttackTipsTimer = -1;
    


    public PanelBattleDotaScore()
    {
        m_pixelPrefect = false;
        SetPanelPrefabPath("UI/Battle/PanelBattleDotaScore");

        AddPreLoadRes("UI/Battle/PanelBattleDotaScore", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
        AddPreLoadRes("Atlas/Survival", EResType.Atlas);
    }

    public override void Init()
    {
        if (m_leftHeadList == null)
        {
            m_leftHeadList = new HeadList();
        }
        if (m_rightHeadList == null)
        {
            m_rightHeadList = new HeadList();
        }

        m_kDownTimer = m_tran.Find("TopBar/time").gameObject.AddMissingComponent<ImageDownWatch>();
        Sprite[] timeSpriteList = { ResourceManager.LoadSprite(AtlasName.Battle, "time0"), ResourceManager.LoadSprite(AtlasName.Battle, "time1"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time2"),ResourceManager.LoadSprite( AtlasName.Battle,"time3"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time4"),ResourceManager.LoadSprite( AtlasName.Battle,"time5"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time6"),ResourceManager.LoadSprite( AtlasName.Battle,"time7"),
                            ResourceManager.LoadSprite( AtlasName.Battle,"time8"),ResourceManager.LoadSprite( AtlasName.Battle,"time9")};
        m_kDownTimer.SetNumSpriteList(timeSpriteList);

        m_roundNumTxt = new UIImgNumText(m_tran.Find("TopBar/RoundNum"), "gray_", AtlasName.Common, 0, -3);



        //m_revivecoin = m_tran.Find("TopBar/CoinNum").GetComponent<Text>();
        m_supplypoint = m_tran.Find("TopBar/SupplyPoint/dollarNum").GetComponent<Text>();
        m_killCount = m_tran.Find("TopBar/BattleInfo/KillCount").GetComponent<Text>();

        //m_bossDownTimer = m_tran.Find("TopBar/bosstime").gameObject.AddMissingComponent<TextDownWatch>();

        leftHeadTrans = m_tran.Find("TopBar/LeftHead");
        rightHeadTrans = m_tran.Find("TopBar/RightHead");

        m_buyPlayerBuffBtn = m_tran.Find("BuyPlayerBuff").gameObject;
        m_buyPlayerBuffEffect = m_tran.Find("BuyPlayerBuff/Effect").gameObject;
        m_buyPlayerBuffEffect.TrySetActive(false);
        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web))
        {
            var img = m_buyPlayerBuffBtn.GetComponent<Image>();
            img.SetSprite(ResourceManager.LoadSprite(AtlasName.Survival, "add_skill_icon_pc"));
            img.SetNativeSize();
            m_buyPlayerBuffEffect.GetRectTransform().localPosition += new Vector3(0, 6f, 0);
        }
        

        Transform bossRenderRight = m_tran.Find("Right/bossHPGrid/hpBar");
        bossDataGridRight = bossRenderRight.transform.parent.gameObject.AddComponent<DataGrid>();
        bossDataGridRight.SetItemRender(bossRenderRight.gameObject, typeof(DotaBossHPItemRender));

        Transform bossRenderLeft = m_tran.Find("Left/bossHPGrid/hpBar");
        bossDataGridLeft = bossRenderLeft.transform.parent.gameObject.AddComponent<DataGrid>();
        bossDataGridLeft.SetItemRender(bossRenderLeft.gameObject, typeof(DotaBossHPItemRender));

        m_bossReviveRight = m_tran.Find("Right/bossHPGrid/revive").gameObject;
        m_bossReviveLeft = m_tran.Find("Left/bossHPGrid/revive").gameObject;

        m_kDownTimerRight = m_tran.Find("Right/bossHPGrid/revive/reviveTime").gameObject.AddMissingComponent<TextDownWatch>();
        m_kDownTimerLeft = m_tran.Find("Left/bossHPGrid/revive/reviveTime").gameObject.AddMissingComponent<TextDownWatch>();

        m_bossIconRight = m_tran.Find("Right/bossHPGrid/revive/icon").GetComponent<Image>();
        m_bossIconLeft = m_tran.Find("Left/bossHPGrid/revive/icon").GetComponent<Image>();

        Util.SetGoGrayShader(m_bossIconRight.gameObject, true);
        Util.SetGoGrayShader(m_bossIconLeft.gameObject, true);

        //if (WorldManager.singleton.isDefendModeOpen)
        //{
        //    bossDataGridRight.transform.localPosition -= new Vector3(0, 70, 0);
        //}

        //以下是基地相关信息
        m_goBaseInfoRight = m_tran.Find("Right/BaseInfo").gameObject;
        m_goBaseInfoRight.TrySetActive(true);
        m_baseHpRight = m_tran.Find("Right/BaseInfo/HpBg/Hp").GetComponent<Image>();
        m_baseHpBarRight = m_tran.Find("Right/BaseInfo/HpBg").GetComponent<Image>();

        m_goBaseInfoLeft = m_tran.Find("Left/BaseInfo").gameObject;
        m_goBaseInfoLeft.TrySetActive(true);
        m_baseHpLeft = m_tran.Find("Left/BaseInfo/HpBg/Hp").GetComponent<Image>();
        m_baseHpBarLeft = m_tran.Find("Left/BaseInfo/HpBg").GetComponent<Image>();

        
        m_beingAttackTips = m_tran.Find("BeingAttackedTips").gameObject;
        m_beingAttackTips.TrySetActive(false);

        InitBaseHpBarRight();
        InitBaseHpBarLeft();
    }

    public override void InitEvent()
    {
        GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        UGUIClickHandler.Get(m_buyPlayerBuffBtn).onPointerClick += OnBuyPlayerBuff;

        if ((Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web) && GlobalBattleParams.singleton.IsPcKeyAutoDefine(GameEvent.INPUT_KEY_E_DOWN))
        {
            AddEventListener(GameEvent.INPUT_KEY_E_DOWN, () =>
            {
                if (UIManager.IsOpen<PanelBattle>())
                {
                    if (UIManager.GetPanel<PanelBattle>().isShowingBuyBuff() || UIManager.GetPanel<PanelBattle>().isShowingBuyBullet())
                        return;
                }
                if (m_buyPlayerBuffBtn.activeSelf)
                    OnBuyPlayerBuff(null, null);
            });
        }
    }


    public override void OnShow()
    {
        UpdateRound(PlayerBattleModel.Instance.battle_state.round, m_configRound, m_winType, m_curRoundTotalTime);
        if (null != m_leftHeadList && null != m_rightHeadList)
        {
            if (m_leftNum > 0)
            {
                ShowLeftList(m_leftNum);
            }
            if (m_rightNum > 0)
            {
                ShowRightList(m_rightNum);
            }

            m_leftHeadList.ResetHeadNum();
            m_rightHeadList.ResetHeadNum();
        }
        if (WorldManager.singleton.isViewBattleModel)
            m_buyPlayerBuffBtn.TrySetActive(false);
    }

    public override void OnHide()
    {

    }
    public override void OnBack()
    {
    }
    public override void OnDestroy()
    {
        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);
        m_basePlayerRight = null;
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
    }

    private float m_tick;
    public override void Update()
    {
        if (m_kDownTimer != null)
        {
            var run_time = PlayerBattleModel.Instance.battle_info.round_time;
            if (FightVideoManager.isPlayFight == true)
            {
                run_time = run_time * FightVideoManager.PlaySpeed;
            }
            float left_time = m_curRoundTotalTime - run_time;
            m_kDownTimer.SetTime = left_time;
        }

        //if (null != m_bossDownTimer)
        //{
        //    if (m_bossDownTimer.gameObject.activeSelf == true && BurstBehavior.singleton != null && BurstBehavior.singleton.StartBombCD)
        //    {
        //        m_bossDownTimer.SetTime = 0;
        //    }
        //}
        m_tick += Time.deltaTime;
        if (m_tick >= 1)
        {
            m_tick = 0;
            OnUpdatePlayersInfo();
        }

        //显示Hp信息
        if (m_basePlayerRight != null && m_basePlayerRight.serverData != null && m_baseHpRight != null)
        {
            if (Time.time >= (m_lastTimeRight + 0.5f))
            {
                m_lastTimeRight = Time.time;
                m_baseHpRight.fillAmount = (float)m_basePlayerRight.serverData.life / m_basePlayerRight.serverData.maxhp();
            }
        }

        if (m_basePlayerLeft != null && m_basePlayerLeft.serverData != null && m_baseHpLeft != null)
        {
            if (Time.time >= (m_lastTimeLeft + 0.5f))
            {
                m_lastTimeLeft = Time.time;
                m_baseHpLeft.fillAmount = (float)m_basePlayerLeft.serverData.life / m_basePlayerLeft.serverData.maxhp();
            }
        }
    }


    virtual public void BindBehavior(DotaBehavior behavior)
    {
        m_behavior = behavior;
    }

    public void UpdateBossTime(int camp, int bosstime)
    {
        float leftTime = bosstime / 1000f - (Time.realtimeSinceStartup - WorldManager.singleton.StartTime);

        if (leftTime <= 0)
            return;

        Logger.Log(string.Format("camp:{0} left_time:{1}", camp, leftTime));
        if (camp == (int)WorldManager.CAMP_DEFINE.UNION)
        {
            if (m_bossReviveRight != null)
            {
                m_bossReviveRight.TrySetActive(true);
                m_kDownTimerRight.SetTime = leftTime;
            }
        }
        else
        {
            if (m_bossReviveLeft != null)
            {
                m_bossReviveLeft.TrySetActive(true);
                m_kDownTimerLeft.SetTime = leftTime;
            }
        }
        //m_bossDownTimer.SetTime = left_time;
        //m_bossDownTimer.SetTime = bosstime;
    }

    public void UpdateMsgTipInfo(int status)
    {
        return;
        if (-1 == status) return;
        string msg = "";
        switch (status)
        {
            case 1:
                msg = "消灭敌方巨石";
                break;
            case 2:
                msg = "保护已方巨石";
                break;
            case 3:
                msg = "消灭敌人";
                break;
            case 4:
                msg = "阻止敌方巨石";
                break;
        }
        if (UIManager.IsOpen<PanelBattle>())
        {
            if (!m_showTipBtn)//第一次show
            {
                UIManager.GetPanel<PanelBattle>().ShowBattleTipPanel(true, false, msg, 180);
                m_showTipBtn = true;
            }
            UIManager.GetPanel<PanelBattle>().ShowBattleTipMsg(msg);
        }
    }

    public void UpdateInfo(int coint, int point, int totalbox, int remainbox, int round, int stageLv, int monster)
    {
        //if (m_revivecoin != null && coint != -1) m_revivecoin.text = coint.ToString();
        if (m_supplypoint != null && point != -1) m_supplypoint.text = point.ToString();
        if (m_buyPlayerBuffEffect != null)
        {
            bool enable = point >= 2000;
            m_buyPlayerBuffEffect.TrySetActive(enable);
        }
    }

    public void UpdateRound(int gameRound, int configRound, string winType, float roundTime)
    {
        m_curRoundTotalTime = roundTime;
        m_winType = winType;
        m_configRound = configRound;
        if (string.IsNullOrEmpty(m_winType))
            return;

        if (m_roundNumTxt != null)
            m_roundNumTxt.value = gameRound;

        if (m_behavior != null)
            UpdateInfo(m_behavior.revivecoin, m_behavior.supplypoint, m_behavior.total_box, m_behavior.remain_box,
                m_behavior.total_round, m_behavior.stage_level, m_behavior.monster);
        if (bossDataGridRight != null)
            bossDataGridRight.Data = listBossRight.ToArray();
        if (bossDataGridLeft != null)
            bossDataGridLeft.Data = listBossLeft.ToArray();
        //TryPlayBossMusic();
    }

    public static void TryAddBossHPBar(OtherPlayer player)
    {
        PanelBattleDotaScore panel = UIManager.GetPanel<PanelBattleDotaScore>();
        if (panel == null)
            panel = UIManager.GetPanel<PanelBattleDotaDefendScore>();
        //TryPlayBossMusic();
        if (player != null)
        {
            if (player.Camp == (int)WorldManager.CAMP_DEFINE.UNION)
            {
                if (!listBossRight.Contains(player))
                {
                    listBossRight.Add(player);
                }
                if (panel != null && panel.IsInited())
                {
                    panel.bossDataGridRight.Data = listBossRight.ToArray();
                    if (panel.bossReviveRight != null)
                        panel.bossReviveRight.TrySetActive(false);
                }
                
            }
            else if (player.Camp == (int)WorldManager.CAMP_DEFINE.REBEL)
            {
                if (!listBossLeft.Contains(player))
                {
                    listBossLeft.Add(player);
                }
                if (panel != null && panel.IsInited())
                {
                    panel.bossDataGridLeft.Data = listBossLeft.ToArray();
                    if (panel.bossReviveLeft != null)
                        panel.bossReviveLeft.TrySetActive(false);
                }
            }
        }
        
    }

    public static void TryPlayBossMusic()
    {
        if (UIManager.IsBattle() && AudioManager.GetBGMusicName() != AudioConst.zombieBoss && listBossRight.Count != 0)
        {
            AudioManager.PlayBgMusic(AudioConst.zombieBoss);
        }
    }

    public static void TryDeleteBossHPBar(OtherPlayer player, bool all = false)
    {
        PanelBattleDotaScore panel = UIManager.GetPanel<PanelBattleDotaScore>();
        if (panel == null)
            panel = UIManager.GetPanel<PanelBattleDotaDefendScore>();
        if (all)
        {
            listBossRight.Clear();
            listBossLeft.Clear();
        }
        else
        {
            if (player != null)
            {
                if (player.Camp == (int)WorldManager.CAMP_DEFINE.UNION)
                    listBossRight.Remove(player);
                else
                    listBossLeft.Remove(player);
            }
        }
        if (panel != null && player != null)
        {
            if (player.Camp == (int)WorldManager.CAMP_DEFINE.UNION)
            {
                if (panel.bossDataGridRight != null)
                    panel.bossDataGridRight.Data = listBossRight.ToArray();
            }
            else if (player.Camp == (int)WorldManager.CAMP_DEFINE.REBEL)
            {
                if (panel.bossDataGridLeft!= null)
                    panel.bossDataGridLeft.Data = listBossLeft.ToArray();
            }
        }
    }

    public int rightnum
    {
        set
        {
            int oldNum = m_rightNum;
            m_rightNum = value;
            if (oldNum != m_rightNum)
            {
                ShowRightList(m_rightNum);
            }
        }
        get
        {
            return m_rightNum;
        }
    }

    public int leftnum
    {
        set
        {
            int oldNum = m_leftNum;
            m_leftNum = value;

            if (oldNum != m_leftNum)
            {
                ShowLeftList(m_leftNum);
            }
        }
        get
        {
            return m_leftNum;
        }
    }

    public GameObject bossReviveRight { get { return m_bossReviveRight; } }
    public GameObject bossReviveLeft { get { return m_bossReviveLeft; } }

    private void OnUpdatePlayersInfo()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        string gameRule = WorldManager.singleton.gameRule;
        if (data == null || string.IsNullOrEmpty(gameRule) || null == MainPlayer.singleton)
        {
            return;
        }
        List<SBattleCamp> camps = data.camps;
        int iUnionPlayers = 0;
        int iTraitorPlayers = 0;
        int liveUnionNum = 0;
        int liveRebelNum = 0;

        for (int i = 0; i < camps.Count; i++)
        {
            SBattleCamp camp = camps[i];
            SBattlePlayer splayer;
            for (int j = 0; j < camp.players.Count; j++)
            {
                splayer = camp.players[j];
                if (splayer.actor_type != GameConst.ACTOR_TYPE_PERSON)
                {
                    continue;
                }

                //if (camp.camp == (int)WorldManager.CAMP_DEFINE.UNION)
                if (camp.camp == MainPlayer.singleton.Camp)
                {
                    ++iUnionPlayers;
                    if (splayer.alive)
                    {
                        ++liveUnionNum;
                    }
                }
                //else if (camp.camp == (int)WorldManager.CAMP_DEFINE.REBEL
                //        || (camp.camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_ZOMBIE, GameConst.GAME_RULE_ZOMBIE_HERO, GameConst.GAME_RULE_ZOMBIE_ULTIMATE))
                //        || (camp.camp == (int)WorldManager.CAMP_DEFINE.CAT && WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_HIDE))
                //        )
                else
                {
                    ++iTraitorPlayers;
                    if (splayer.alive)
                    {
                        ++liveRebelNum;
                    }
                }
            }
        }

        leftnum = iUnionPlayers;
        rightnum = iTraitorPlayers;
        m_leftHeadList.LiveAndDeadNum(liveUnionNum, iUnionPlayers - liveUnionNum);
        m_rightHeadList.LiveAndDeadNum(liveRebelNum, iTraitorPlayers - liveRebelNum);

        int leftCount = 0;
        int rightCount = 0;
        rightCount = camps.Count < 1 ? 0 : camps[0].round_kill;
        leftCount = camps.Count < 2 ? 0 : camps[1].round_kill;
        m_killCount.text = string.Format("{0}:{1}", rightCount, leftCount);
    }

    private void ShowLeftList(int num)
    {
        Sprite unionSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "yellow_point", (Sprite sprite) =>
        {
            unionSprite = sprite;
        });

        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });

        m_leftHeadList.Init(leftHeadTrans, unionSprite, deadSprite, num, false);
    }

    private void ShowRightList(int num)
    {
        Sprite taitorSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "blue_point_2", (Sprite sprite) =>
        {
            taitorSprite = sprite;
        });
        Sprite deadSprite = null;
        ResourceManager.LoadSprite(AtlasName.BattleNew, "dead_point", (Sprite sprite) =>
        {
            deadSprite = sprite;
        });

        m_rightHeadList.Init(rightHeadTrans, taitorSprite, deadSprite, num, true);
    }

    public void BindBasePlayerRight(BasePlayer basePlayer)
    {
        m_basePlayerRight = basePlayer;
    }

    public void BindBasePlayerLeft(BasePlayer basePlayer)
    {
        m_basePlayerLeft= basePlayer;
    }

    private void OnPlayerBeDamaged(DamageData damageData)
    {
        BasePlayer curBase = null;
        if (WorldManager.singleton.curPlayer != null)
        {
            if(WorldManager.singleton.curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.UNION)
                curBase = m_basePlayerRight;
            else if(WorldManager.singleton.curPlayer.Camp == (int)WorldManager.CAMP_DEFINE.REBEL)
                curBase = m_basePlayerLeft;
        }
        if (curBase != null && damageData.victim == curBase)
        {
            PlayBaseBeingAttackedTips();
        }
    }

    private void PlayBaseBeingAttackedTips()
    {
        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);

        m_beingAttackTips.TrySetActive(true);

        m_beingAttackTipsTimer = TimerManager.SetTimeOut(2f, () =>
        {
            m_beingAttackTips.TrySetActive(false);
            m_beingAttackTipsTimer = -1;
        });
    }

    public void InitBaseHpBarRight()
    {
        if (m_baseHpBarRight == null || m_behavior == null || null == m_basePlayerRight)
            return;

        ConfigDefenseBattleLine myBaseCfg = ConfigManager.GetConfig<ConfigDefenseBattle>().GetLine(m_basePlayerRight.Camp);
        if (null != myBaseCfg)
        {
            Sprite sprite = ResourceManager.LoadSprite(AtlasName.Survival, myBaseCfg.CrystalIcon);
            m_baseHpBarRight.SetSprite(sprite);
            m_baseHpBarRight.SetNativeSize();
            m_baseHpBarRight.rectTransform.sizeDelta = new Vector2(290, 40);
        }
    }

    public void InitBaseHpBarLeft()
    {
        if (m_baseHpBarLeft == null || m_behavior == null || null == m_basePlayerLeft)
            return;

        ConfigDefenseBattleLine myBaseCfg = ConfigManager.GetConfig<ConfigDefenseBattle>().GetLine(m_basePlayerLeft.Camp);
        if (null != myBaseCfg)
        {
            Sprite sprite = ResourceManager.LoadSprite(AtlasName.Survival, myBaseCfg.CrystalIcon);
            m_baseHpBarLeft.SetSprite(sprite);
            m_baseHpBarLeft.SetNativeSize();
            m_baseHpBarLeft.rectTransform.sizeDelta = new Vector2(290, 40);
        }
    }

    //打开商店
    private void OnBuyPlayerBuff(GameObject target, PointerEventData eventData)
    {
        if (MainPlayer.singleton != null)
        {
            if (UIManager.IsOpen<PanelBuyDotaPlayerBuff>())
            {
                UIManager.GetPanel<PanelBuyDotaPlayerBuff>().HidePanel();
            }
            else
            {
                UIManager.PopPanel<PanelBuyDotaPlayerBuff>();
            }
        }
    }

    private void OnPlayerDie(BasePlayer shooter, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (WorldManager.singleton.isViewBattleModel || MainPlayer.singleton == null || MainPlayer.singleton.serverData == null) return;
        if (dier == MainPlayer.singleton)
        {
            //if (UIManager.IsOpen<PanelBuyDotaPlayerBuff>())
            //{
            //    UIManager.GetPanel<PanelBuyDotaPlayerBuff>().HidePanel();
            //}
        }
    }
}
