﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 守护模式的面板（基于SurvivalScore添加base血量 显示）
/// </summary>
class PanelBattleDotaDefendScore : PanelBattleDotaScore
{
    private int m_beingAttackTipsTimer = -1;
    private GameObject m_beingAttackTips;

    private BasePlayer m_basePlayer;
    private Image m_baseHp;
    private Image m_baseHpBar;

    private float m_lastTime;

    public PanelBattleDotaDefendScore()
    {

    }

    public override void Init()
    {
        base.Init();

        m_goBaseInfoRight.TrySetActive(true);

        m_baseHp = m_tran.Find("BaseInfo/HpBg/Hp").GetComponent<Image>();
        m_baseHpBar = m_tran.Find("BaseInfo/HpBg").GetComponent<Image>();

        m_beingAttackTips = m_tran.Find("BaseInfo/BeingAttackedTips").gameObject;
        var tsBeingAttackTips = m_beingAttackTips.AddMissingComponent<TweenScale>();
        tsBeingAttackTips.style = UITweener.Style.PingPong;
        tsBeingAttackTips.from = Vector3.one;
        tsBeingAttackTips.to = new Vector3(1.2f, 1.2f, 1.2f);
        tsBeingAttackTips.duration = 0.5f;
        m_beingAttackTips.TrySetActive(false);

        InitBaseHpBar();
    }

    public override void InitEvent()
    {
        base.InitEvent();
        GameDispatcher.AddEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
    }


    public override void OnDestroy()
    {
        base.OnDestroy();

        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);
        m_basePlayer = null;
        GameDispatcher.RemoveEventListener<DamageData>(GameEvent.PLAYER_BE_DAMAGE, OnPlayerBeDamaged);
    }

    public override void Update()
    {
        base.Update();

        //显示Hp信息
        if (m_basePlayer != null && m_basePlayer.serverData != null && m_baseHp != null)
        {
            if (Time.time >= (m_lastTime + 0.5f))
            {
                m_lastTime = Time.time;
                m_baseHp.fillAmount = (float)m_basePlayer.serverData.life / m_basePlayer.serverData.maxhp();
            }
        }
    }

    public override void OnShow()
    {
        base.OnShow();
    }

    public void BindBasePlayer(BasePlayer basePlayer)
    {
        m_basePlayer = basePlayer;
    }

    private void OnPlayerBeDamaged(DamageData damageData)
    {
        if (damageData.victim == m_basePlayer)
        {
            PlayBaseBeingAttackedTips();
        }
    }

    private void PlayBaseBeingAttackedTips()
    {
        if (m_beingAttackTipsTimer != -1)
            TimerManager.RemoveTimeOut(m_beingAttackTipsTimer);

        m_beingAttackTips.TrySetActive(true);

        m_beingAttackTipsTimer = TimerManager.SetTimeOut(2f, () =>
        {
            m_beingAttackTips.TrySetActive(false);
            m_beingAttackTipsTimer = -1;
        });
    }

    public void InitBaseHpBar()
    {
        //if (m_baseHpBar == null || m_behavior == null || m_baseHpBar.sprite.name == m_behavior.stageConfig.Icon)
        //    return;

        //Sprite sprite = ResourceManager.LoadSprite(AtlasName.Survival, m_behavior.stageConfig.Icon);
        //m_baseHpBar.SetSprite(sprite);
        //m_baseHpBar.SetNativeSize();
    }

}
