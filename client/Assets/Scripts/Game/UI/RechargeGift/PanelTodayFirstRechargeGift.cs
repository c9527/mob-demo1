﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PanelTodayFirstRechargeGift : BasePanel
{
    private int m_id;
    private TODAY_FIRST_RECHARGE_GIFT_GET_TYPE m_enumGetType;

    private Image m_imgGetGift;
    private GameObject m_goProto;
    private Transform m_transParentContent;
    private Text m_time_txt;

    private GameObject m_goGetBtn;
    private GameObject m_goGetted;

    public PanelTodayFirstRechargeGift()
    {
        SetPanelPrefabPath("UI/RechargeGift/PanelTodayFirstRechargeGift");
        
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/RechargeGift", EResType.Atlas);
        AddPreLoadRes("UI/RechargeGift/PanelTodayFirstRechargeGift", EResType.UI);
    }

    public override void Init()
    {
        m_imgGetGift = m_tran.Find("get_btn/title").GetComponent<Image>();
        m_goProto = m_tran.Find("item").gameObject;
        m_transParentContent = m_tran.Find("content");
        m_time_txt = m_tran.Find("time_txt").GetComponent<Text>();

        m_goGetBtn = m_tran.Find("get_btn").gameObject;
        m_goGetted = m_tran.Find("gift_getted").gameObject;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("get_btn")).onPointerClick += OnClkGetGift;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        var now_date = TimeUtil.GetNowTime();
        var start_date = new DateTime(now_date.Year, now_date.Month, now_date.Day);
        var end_date = start_date.AddMinutes(24 * 60 + 10);
        m_time_txt.text = string.Format("活动时间:{0}-{1}", start_date.ToString("M月d日HH:mm"), end_date.ToString("M月d日HH:mm"));

        toc_player_first_recharge_data proto = m_params[0] as toc_player_first_recharge_data;
        m_enumGetType = (TODAY_FIRST_RECHARGE_GIFT_GET_TYPE)proto.tag;
        if (proto == null || proto.tag == null)
        {
            HidePanel();
            TipsManager.Instance.showTips("网络延迟");
            return;
        }
        m_id = proto.id;
        UpdateData(m_enumGetType, m_id);
        
    }

    private void SetItem( GameObject go,ItemInfo info )
    {
        ConfigItemLine itemLine = ItemDataManager.GetItem( info.ID );
        if( itemLine == null )
            return;
        go.transform.FindChild("icon").GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(itemLine.Icon),itemLine.SubType);
        go.transform.FindChild("icon").GetComponent<Image>().SetNativeSize();
        go.transform.FindChild("num").GetComponent<Text>().text = "X" + info.cnt;

        if (itemLine.Type == GameConst.ITEM_TYPE_EQUIP)
        {
            Text txt=go.transform.FindChild("name").GetComponent<Text>();
            //txt.text=itemLine.DispName + "(1天)";
            ItemTime time = ItemDataManager.FloatToDayHourMinute(info.day);
            txt.text = string.Format("{0}{1}", itemLine.DispName, time.ToString());
        }            
        else
            go.transform.FindChild("name").GetComponent<Text>().text = itemLine.DispName;
    }

    private void UpdateData(TODAY_FIRST_RECHARGE_GIFT_GET_TYPE tag, int id)
    {
        m_enumGetType = tag;
        m_id = id;
        if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.NOT_RECHARGE)
        {
            m_goGetBtn.TrySetActive(true);
            m_goGetted.TrySetActive(false);
            m_imgGetGift.SetSprite(ResourceManager.LoadSprite(AtlasName.RechargeGift, "recharge_get"));
        }
        else if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.NOT_GET_GIFT)
        {
            m_goGetBtn.TrySetActive(true);
            m_goGetted.TrySetActive(false);
            m_imgGetGift.SetSprite(ResourceManager.LoadSprite(AtlasName.RechargeGift, "gift_get"));
        }
        else if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.GETTED_GIFT)
        {
            m_goGetBtn.TrySetActive(false);
            m_goGetted.TrySetActive(true);
        }
        
        var config1 = ConfigManager.GetConfig<ConfigFirstRecharge>();
        ConfigFirstRechargeLine line = config1.GetLine(m_id);
        if (line == null)
            return;

        var config2 = ConfigManager.GetConfig<ConfigGiftBag>();
        ConfigGiftBagLine giftLine = config2.GetLine(line.WelfareId);
        if (giftLine == null)
            return;

       
        //foreach (Transform tran in m_transParentContent)
        //{
        //    GameObject.Destroy(tran.gameObject);
        //}

        for (int i = 0; i < giftLine.GiftList.Length; ++i)
        {
            GameObject goNew = m_transParentContent.FindChild(string.Format("item{0}",(i+1).ToString() )).gameObject;
            SetItem(goNew, giftLine.GiftList[i]);
            //goNew.TrySetActive(true);
            //goNew.transform.SetParent(m_transParentContent, false);
        }
        
        
    }

    public override void OnHide()
    {
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    protected void OnClkGetGift(GameObject target, PointerEventData eventData)
    {
        if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.NOT_RECHARGE)
        {
            DestroyPanel();
            PanelEvent parent = UIManager.GetPanel<PanelEvent>();
            if (parent != null)
                parent.HidePanel();

            SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
        }
        else if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.NOT_GET_GIFT)
        {
            NetLayer.Send(new tos_player_get_first_recharge() { id = m_id });
            m_goGetBtn.TrySetActive(false);
            m_goGetted.TrySetActive(true);
            PanelEvent panel = UIManager.GetPanel<PanelEvent>();
            //if (panel != null)
            //    panel.OnGetTodayFirstGift();
            NetLayer.Send(new tos_player_first_recharge_data());

            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(/*new object[] {true}*/null, true, null);
            var config1 = ConfigManager.GetConfig<ConfigFirstRecharge>();
            ConfigFirstRechargeLine line = config1.GetLine(m_id);
            var config2 = ConfigManager.GetConfig<ConfigGiftBag>();
            ConfigGiftBagLine giftLine = config2.GetLine(line.WelfareId);
            //panelItemTip.SetRewardItemList(giftLine.GiftList);
            PanelRewardItemTipsWithEffect.DoShow(giftLine.GiftList);
        }
        else if (m_enumGetType == TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.GETTED_GIFT)
        {
            UIManager.ShowTipPanel("你今天已经领取过了");
        }
        //
    }

}
