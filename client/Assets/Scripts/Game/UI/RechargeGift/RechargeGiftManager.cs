﻿using UnityEngine;
using System.Collections;

public enum ACCUMULATIVE_RECHARGE_STATE
{
    ACC_NOT_ENOUGH = 0,
    ACC_HAS_GIFT = 1,
    ACC_GETTED_GIFT = 2
}

public enum TODAY_FIRST_RECHARGE_GIFT_GET_TYPE
{
    NOT_RECHARGE = 1,
    GETTED_GIFT = 2,
    NOT_GET_GIFT = 3
};

public class RechargeGiftManager
{
    internal static toc_player_first_recharge_data m_todayFirstRecharge;
    internal static toc_player_recharge_welfare_list m_accumultiveRecharge;
    public static bool m_bFirstPay = false;
    private static int STAGE_NEW = 1;
    public static void RequestFirstRechargeGift()
    {
        NetLayer.Send(new tos_player_first_recharge_welfare() );
    }

    private static void Toc_player_first_recharge_welfare( toc_player_first_recharge_welfare proto ) 
    {
        //UIManager.PopPanel<PanelFirstRechargeGift>(new object[] { proto.id }, true, null);
        //m_bFirstPay = true;
        //GameDispatcher.Dispatch(GameEvent.UI_FIRST_PAY);
    }

    //private static void Toc_player_first_recharge_data( toc_player_first_recharge_data proto )
    //{
    //    UIManager.PopPanel<PanelTodayFirstRechargeGift>(new object[] { proto.tag,proto.id }, true, null);
    //}

    //private static void Toc_player_recharge_welfare_list(toc_player_recharge_welfare_list proto)
    //{
    //    UIManager.ShowPanel<PanelActivity>( new object[] { proto});
    //}

    private static void Toc_player_first_recharge_data(toc_player_first_recharge_data proto)
    {
        m_todayFirstRecharge = proto;
        GameDispatcher.Dispatch(GameEvent.UI_BUBBLE_CHANGE);
    }

    public static bool HasTodayFirstRechargeGift()
    {
        if (m_todayFirstRecharge == null)
            return false;

        if ( m_todayFirstRecharge.tag == (int)TODAY_FIRST_RECHARGE_GIFT_GET_TYPE.NOT_GET_GIFT)
            return true;

        return false;
    }
    private static void Toc_player_recharge_welfare_list(toc_player_recharge_welfare_list proto)
    {
        m_accumultiveRecharge = proto;
        GameDispatcher.Dispatch(GameEvent.UI_BUBBLE_CHANGE);
		
    }

    public static bool HasAccummultiveRechargeGift()
    {
        if (m_accumultiveRecharge == null)
            return false;
        if (m_accumultiveRecharge.welfares.Length > 0)
            return true;

        return false;
    }

    public static bool IsInAccumulativeNew()
    {

        if (m_accumultiveRecharge == null)
            return false;

        return m_accumultiveRecharge.stage == STAGE_NEW;
    }
}
