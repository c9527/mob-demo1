﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

public class PanelFirstRecharge : BasePanel
{
    private GameObject m_goRenderTexture;
    private Camera m_weaponCamera;
    private Transform m_tranWeapon;
    private string m_lastWeaponModeName;
    private float m_rotationCenter = 3000.12f; //模型旋转的中心

    private UIEffect m_effect;

    public PanelFirstRecharge()
    {
        SetPanelPrefabPath("UI/RechargeGift/PanelFirstRecharge");
        AddPreLoadRes("UI/RechargeGift/PanelFirstRecharge", EResType.UI);
        AddPreLoadRes("UI/Strengthen/WeaponTexture", EResType.RenderTexture);
        AddPreLoadRes("Materials/" + GameConst.MAT_TUJIAN_WEAPON, EResType.Material);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/RechargeGift", EResType.Atlas);
    }

    public override void Init()
    {
        m_goRenderTexture = m_tran.Find("weaponImage").gameObject;

        m_weaponCamera = new GameObject("WeaponCamera").AddComponent<Camera>();
        m_weaponCamera.orthographic = false;
        m_weaponCamera.orthographicSize = 0.51f;
        m_weaponCamera.fieldOfView = 35;
        m_weaponCamera.farClipPlane = 5;
        m_weaponCamera.targetTexture = ResourceManager.LoadRenderTexture("UI/Strengthen/WeaponTexture");
        m_weaponCamera.transform.SetLocation(null, m_rotationCenter, 0f, 1.4f);
        m_weaponCamera.transform.localRotation = Quaternion.Euler(0, 180f, 0);
    }

    private int m_rotateDir = 1;
    public override void Update()
    {
        if (m_tranWeapon != null)
        {
            var r = m_tranWeapon.localRotation.eulerAngles.y;
            if (r > 120 || r < 60)
                m_rotateDir *= -1;
            m_tranWeapon.RotateAround(new Vector3(m_rotationCenter, 0, 0), Vector3.up*m_rotateDir, Time.deltaTime*5);
        }
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close")).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("btn_recharge")).onPointerClick += OnClkRecharge;
//        UGUIDragHandler.Get(m_goRenderTexture).onDrag += OnDrag;
    }

    public override void OnShow()
    {

        UpdateMode();
        if (m_weaponCamera)
            m_weaponCamera.gameObject.TrySetActive(true);
        if (m_tranWeapon)
            m_tranWeapon.gameObject.TrySetActive(true);
//        if (m_effect == null)
//            m_effect = UIEffect.ShowEffectAfter(m_goRenderTexture.transform, EffectConst.UI_EVENT_FIRST_RECHARGE, true);
    }

    public override void OnHide()
    {
        if (m_effect != null)
        {
            m_effect.Destroy();
            m_effect = null;
        }
        if (m_weaponCamera)
            m_weaponCamera.gameObject.TrySetActive(false);
        if (m_tranWeapon)
            m_tranWeapon.gameObject.TrySetActive(false);
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    protected void OnClkRecharge(GameObject target, PointerEventData eventData)
    {
        HidePanel();
        SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
    }

//    private void OnDrag(GameObject target, PointerEventData eventData)
//    {
//        if (m_tranWeapon == null)
//            return;
//        m_tranWeapon.RotateAround(new Vector3(m_rotationCenter, 0, 0), Vector3.up, -eventData.delta.x * 0.2f);
//    }

    private void UpdateMode()
    {
        var _data = ItemDataManager.GetItemWeapon(1004);
        if (_data != null)
        {
            if (m_lastWeaponModeName != _data.MPModel)
            {
                m_lastWeaponModeName = _data.MPModel;
                ResourceManager.LoadModel("Weapons/" + m_lastWeaponModeName, (go) =>
                {
                    if (!m_go)
                    {
                        Object.Destroy(go);
                        return;
                    }

                    if (m_tranWeapon)
                        Object.DestroyImmediate(m_tranWeapon.gameObject);

                    m_goRenderTexture.TrySetActive(true);
                    m_tranWeapon = go.transform;
                    m_tranWeapon.localPosition = new Vector3(3000, 0, 0);
                    m_tranWeapon.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));

                    // 排除特效节点
                    ExtendFuncHelper.CallBackBool<GameObject> funVisit = (GameObject child) =>
                    {
                        string name = child.name.ToLower();
                        if (name.ToLower().StartsWith(ModelConst.WEAPON_EFFECT))
                            return false;

                        Renderer render = child.GetComponent<Renderer>();
                        if (render != null)
                        {
                            var texture = render.sharedMaterial.mainTexture;
                            if (m_tranWeapon.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
                                render.material = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON_CRYSTAL);
                            else
                                render.material = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON);
                            render.material.mainTexture = texture;
                        }
                        return true;
                    };

                    m_tranWeapon.VisitChild(funVisit, true);

                }, true);
            }
            else
            {
                if (m_goRenderTexture)
                    m_goRenderTexture.TrySetActive(true);
            }

        }
    }
}
