﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ItemAccumulativeRecharge : ItemRender
{
    private AccumulativeRechargeGift m_data;
    private Text m_txtLabel;
    private GameObject m_goEquip;
    private GameObject m_goItem;
    private Text m_txtTitle;
    private Text m_txtRecharge;
    private GameObject m_goBtnGet;
    private GameObject m_goBtnRecharge;
    private GameObject m_goBtnGetted;

    public override void Awake()
    {
        m_txtLabel = transform.Find("btn_get/Label").GetComponent<Text>();
        m_goEquip = transform.Find("equip_list").gameObject;
        m_goItem = transform.Find("item_list").gameObject;
        m_txtTitle = transform.Find("title").GetComponent<Text>();
        m_txtRecharge = transform.Find("recharge").GetComponent<Text>();
        m_goBtnGet = transform.Find("btn_get").gameObject;
        m_goBtnRecharge = transform.Find("btn_recharge").gameObject;
        m_goBtnGetted = transform.Find("btn_getted").gameObject;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as AccumulativeRechargeGift;
        if( m_data.State == ACCUMULATIVE_RECHARGE_STATE.ACC_HAS_GIFT )
        {
            m_goBtnGet.TrySetActive(true);
            m_goBtnRecharge.TrySetActive(false);
            m_goBtnGetted.TrySetActive(false);
            UGUIClickHandler.Get(transform.Find("btn_get")).onPointerClick += GetGift;
            m_txtLabel.text = "领取";
        }
        else if( m_data.State == ACCUMULATIVE_RECHARGE_STATE.ACC_GETTED_GIFT )
        {
            m_goBtnGet.TrySetActive(false);
            m_goBtnRecharge.TrySetActive(false);
            m_goBtnGetted.TrySetActive(true);
            //m_txtLabel.text = "已领取";
        }
        else if( m_data.State == ACCUMULATIVE_RECHARGE_STATE.ACC_NOT_ENOUGH )
        {
            m_goBtnGet.TrySetActive(false);
            m_goBtnRecharge.TrySetActive(true);
            m_goBtnGetted.TrySetActive(false);
            UGUIClickHandler.Get(m_goBtnRecharge).onPointerClick += GoRecharge;
            //m_txtLabel.text = "条件不足";
        }

        m_txtTitle.text = m_data.EventDisp;
        int rechargeMoney = 0;
        if (m_data.RechargeSum >= m_data.Recharge)
            rechargeMoney = m_data.Recharge;
        else
            rechargeMoney = m_data.RechargeSum;

        m_txtRecharge.text = rechargeMoney.ToString() + "/" + m_data.Recharge.ToString();

        var config = ConfigManager.GetConfig<ConfigGiftBag>();
        ConfigGiftBagLine giftLine = config.GetLine(m_data.WelfareId);
        if (giftLine == null)
            return;

        if (giftLine.GiftList.Length == 0)
            return;

        var info = ItemDataManager.GetItem(giftLine.GiftList[0].ID);
        if( info.Type == GameConst.ITEM_TYPE_EQUIP )
        {
            m_goEquip.TrySetActive(true);
            m_goItem.TrySetActive(false);
            var cfg = ItemDataManager.GetItem(giftLine.GiftList[0].ID);
            m_goEquip.transform.FindChild("img_equip").GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(giftLine.GiftList[0].ID),cfg!=null?cfg.SubType:"");
            m_goEquip.transform.FindChild("img_equip").GetComponent<Image>().SetNativeSize();
            m_goEquip.transform.FindChild("name").GetComponent<Text>().text = m_data.Name;
        }
        else
        {
            for( int i = 0; i < giftLine.GiftList.Length; ++i )
            {
                string icon = string.Format("item{0}/icon", (i+1).ToString());
                string num = string.Format("item{0}/num", (i+1).ToString());
                string name = string.Format("item{0}/name", (i+1).ToString());
                string discription = string.Format("item{0}/discription", (i+1).ToString());
                var cfg = ItemDataManager.GetItem(giftLine.GiftList[i].ID);
                m_goItem.transform.FindChild(icon).GetComponent<Image>().SetSprite(ResourceManager.LoadIcon(giftLine.GiftList[i].ID), cfg != null ? cfg.SubType : "");
                m_goItem.transform.FindChild(icon).GetComponent<Image>().SetNativeSize();
                m_goItem.transform.FindChild(num).GetComponent<Text>().text = "X" + giftLine.GiftList[i].cnt;
                m_goItem.transform.FindChild(name).GetComponent<Text>().text = m_data.Name;
                m_goItem.transform.FindChild(discription).GetComponent<Text>().text = m_data.Txt;
            }

            if( giftLine.GiftList.Length >= 2 )
            {
                m_goItem.transform.FindChild("add").gameObject.TrySetActive(true);
                m_goItem.transform.FindChild("item2").gameObject.TrySetActive(true);
            }
        }
    }

    protected void GetGift(GameObject target, PointerEventData eventData)
    {
        NetLayer.Send(new tos_player_get_recharge_welfare() { id = m_data.ID });
        //m_txtLabel.text = "已领取";
        m_goBtnGet.TrySetActive(false);
        m_goBtnRecharge.TrySetActive(false);
        m_goBtnGetted.TrySetActive(true);
        NetLayer.Send(new tos_player_recharge_welfare_list());

        var config = ConfigManager.GetConfig<ConfigGiftBag>();
        ConfigGiftBagLine giftLine = config.GetLine(m_data.WelfareId);
        if(giftLine != null)
        {
            //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(new object[]{true}, true, null);
            //panelItemTip.SetRewardItemList(giftLine.GiftList);
            PanelRewardItemTipsWithEffect.DoShow(giftLine.GiftList);
        }
       
    }

    protected void GoRecharge(GameObject target, PointerEventData eventData)
    {
        PanelAccumulativeRecharge panel = UIManager.GetPanel<PanelAccumulativeRecharge>();
        if (panel != null)
            panel.DestroyPanel();

        PanelEvent parent = UIManager.GetPanel<PanelEvent>();
        if (parent != null)
            parent.HidePanel();

        SceneManager.singleton.OpenRecharge(PanelRechargeMember.VIEW_RECHARGE);
    }
}
