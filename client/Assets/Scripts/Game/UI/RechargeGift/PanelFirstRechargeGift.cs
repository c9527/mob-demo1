﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PanelFirstRechargeGift : BasePanel
{
    //private Image m_imgEquip;
    private Text m_txtEquipName;
    private UIEffect m_effect;
    public PanelFirstRechargeGift()
    {
        SetPanelPrefabPath("UI/RechargeGift/PanelFirstRechargeGift");
        AddPreLoadRes("UI/RechargeGift/PanelFirstRechargeGift", EResType.UI);

        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/RechargeGift", EResType.Atlas);
    }

    public override void Init()
    {
        //m_imgEquip = m_tran.Find("background/imgEquip").GetComponent<Image>();
        m_txtEquipName = m_tran.Find("background/txtName").GetComponent<Text>();
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("background/btnOK")).onPointerClick += OnClkSure;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        int itemId = (int)m_params[0];
         ConfigItemLine itemLine = ItemDataManager.GetItem( itemId );
         if (itemLine != null)
         {
             //Sprite icon = ResourceManager.LoadIcon(itemLine.Icon);
             //m_imgEquip.SetSprite(icon);
             //m_imgEquip.SetNativeSize();
             m_txtEquipName.text = itemLine.DispName + "(永久)";
             //m_txtEquipName.text = string.Format("{0} ({1})", itemLine.DispName, TimeUtil.GetRemainTimeString(itemLine.time_limit));
         }

        if( m_effect == null )
        {
            m_effect = UIEffect.ShowEffect(m_tran, EffectConst.UI_FIRST_RECHARGE_GIFT, new Vector2(23, 56) );
        }
    }

    public override void OnHide()
    {
        if( m_effect != null )
        {
            m_effect.Destroy();
            m_effect = null;
        }
    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    protected void OnClkSure( GameObject target, PointerEventData eventData )
    {
        DestroyPanel();
    }
}
