﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class AccumulativeRechargeGift
{
    public int ID;
    public int WelfareId;
    public string EventDisp;
    public ACCUMULATIVE_RECHARGE_STATE State;
    public int Recharge;
    public int CurRecharge;
    public string Name;
    public int RechargeSum;
    public string Txt;
}

public class PanelAccumulativeRecharge : BasePanel
{
    private List<AccumulativeRechargeGift> m_lstGift;
    private DataGrid m_giftDataGrid;
    private Text m_eventTime;
    public static int STAGE_NEW = 1;
    public PanelAccumulativeRecharge()
    {
        SetPanelPrefabPath("UI/Event/PanelAccumulativeRecharge");
        AddPreLoadRes("UI/Event/PanelAccumulativeRecharge", EResType.UI);
        AddPreLoadRes("UI/Event/ItemAccumulativeRecharge", EResType.UI);
        AddPreLoadRes("Atlas/Icon", EResType.Atlas);
        AddPreLoadRes("Atlas/Event", EResType.Atlas);
    }

    public override void Init()
    {
        m_lstGift = new List<AccumulativeRechargeGift>();

        m_giftDataGrid = m_tran.Find("ScrollDataGrid/content").gameObject.AddComponent<DataGrid>();
        m_giftDataGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Event/ItemAccumulativeRecharge"), typeof(ItemAccumulativeRecharge));
        //m_giftDataGrid.useLoopItems = true;
        m_eventTime = m_tran.Find("time_txt").GetComponent<Text>();
      
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        //UGUIClickHandler.Get(m_tran.Find("btn_get_gift")).onPointerClick += OnClkGetGift;
    }

    public override void OnShow()
    {
        //NetLayer.Send(new tos_player_recharge_welfare_list());
        if (!HasParams())
            return;

        toc_player_recharge_welfare_list proto = m_params[0] as toc_player_recharge_welfare_list;
        UpdateView(proto);
     }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
       
    }

    private void UpdateView(toc_player_recharge_welfare_list proto)
    {
        List<int> lstHasGift = new List<int>();
        lstHasGift.AddRange(proto.welfares);
        List<int> lstGettedGift = new List<int>();
        lstGettedGift.AddRange(proto.received_welfares);

        m_lstGift.Clear();
        var config = ConfigManager.GetConfig<ConfigAccumulativeRecharge>();

        int iStartDays = 0;
        int iEndDays = 0;
        bool bCalcDays = true;
        int n = 0;
        int oldEventId = 0;
        while (bCalcDays)
        {
            int idx = (n++) % config.m_dataArr.Length;
            if (proto.stage == STAGE_NEW)
            {
                if (config.m_dataArr[idx].Stage != STAGE_NEW || config.m_dataArr[idx].EventId == oldEventId )
                    continue;
            }
            else
            {
                if (config.m_dataArr[idx].Stage == STAGE_NEW || config.m_dataArr[idx].EventId == oldEventId )
                    continue;
            }

            iStartDays += config.m_dataArr[idx].Days;
            iEndDays += config.m_dataArr[idx].Days;
            oldEventId = config.m_dataArr[idx].EventId;
            if (iEndDays >= proto.elapse_days)
            {
                bCalcDays = false;
                iStartDays -= config.m_dataArr[idx].Days;
            }
            
        }
        DateTime utcTime = TimeUtil.GetTime(proto.utc_time);
        DateTime initTime = new DateTime(utcTime.Year, utcTime.Month, utcTime.Day);
        DateTime startTime = initTime.AddDays( iStartDays );
        DateTime endTime = initTime.AddDays(iEndDays - 1);
        m_eventTime.text = string.Format("活动时间：{0}00:00-{1}24:00", startTime.ToString("M月d日"), endTime.ToString("M月d日"));

        for (int i = 0; i < config.m_dataArr.Length; ++i)
        {
            if (config.m_dataArr[i].Stage == proto.stage && config.m_dataArr[i].EventId == proto.event_id)
            {
                AccumulativeRechargeGift gift = new AccumulativeRechargeGift();
                gift.ID = config.m_dataArr[i].ID;
                gift.WelfareId = config.m_dataArr[i].WelfareId;
                gift.EventDisp = config.m_dataArr[i].EventDisp;
                gift.State = ACCUMULATIVE_RECHARGE_STATE.ACC_NOT_ENOUGH;
                gift.Recharge = config.m_dataArr[i].Recharge;
                gift.CurRecharge = 0;
                gift.Name = config.m_dataArr[i].Name;
                gift.RechargeSum = proto.recharge_money;
                gift.Txt = config.m_dataArr[i].Txt;

                if (lstHasGift.IndexOf(config.m_dataArr[i].ID) > -1)
                {
                    gift.State = ACCUMULATIVE_RECHARGE_STATE.ACC_HAS_GIFT;
                }

                if (lstGettedGift.IndexOf(config.m_dataArr[i].ID) > -1)
                {
                    gift.State = ACCUMULATIVE_RECHARGE_STATE.ACC_GETTED_GIFT;
                }

                m_lstGift.Add(gift);
            }
        }

        m_giftDataGrid.Data = m_lstGift.ToArray();
    }
}
