﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：
//创建者：HWL
//创建时间: 2016/11/24 14:12:17
///////////////////////////

public class ESGRankItem : ItemRender
{
    private Text m_NameText;
    private Text m_KillNumText;
    private Image m_rankIcon;
    private Image m_HpProcess;
    private Transform m_rankIcon2;
    private Image m_rangIcon0;
    private Image m_rangIcon1;
    public override void Awake()
    {
        m_NameText = transform.Find("NameText").GetComponent<Text>();
        m_KillNumText = transform.Find("KillNumText").GetComponent<Text>();
        m_rankIcon = transform.Find("RankIcon").GetComponent<Image>();
        m_HpProcess = transform.Find("HpProcess/up").GetComponent<Image>();
        m_rankIcon2 = transform.Find("RankIcon2");
        m_rangIcon0 = m_rankIcon2.Find("RankIcon0").GetComponent<Image>();
        m_rangIcon1 = m_rankIcon2.Find("RankIcon1").GetComponent<Image>();
    }
    protected override void OnSetData(object data)
    {
        KillNumRankData info = data as KillNumRankData;
        if (info == null)
        {
            m_NameText.text = "";
            m_KillNumText.text ="";
            m_rankIcon.gameObject.TrySetActive(false);
            m_HpProcess.fillAmount = 0f;
            transform.gameObject.TrySetActive(false);
            return;
        }
        if (info.rank < 10)
        {
            m_rankIcon.gameObject.TrySetActive(true);
            m_rankIcon2.gameObject.TrySetActive(false);
            m_rankIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "yellow_" + info.rank));
        }
        else
        {
            m_rankIcon.gameObject.TrySetActive(false);
            m_rankIcon2.gameObject.TrySetActive(true);
            m_rangIcon0.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "yellow_" + info.rank/10));
            m_rangIcon1.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "yellow_" + info.rank % 10));
        }
        
        transform.gameObject.TrySetActive(true);
        
        m_NameText.text = info.name;
        m_KillNumText.text = info.killNum.ToString();
        
        m_HpProcess.fillAmount = info.curHpPercent;
    }
}