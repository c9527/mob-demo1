﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：ESG比赛战绩面板
//创建者：HWL
//创建时间: 2016/11/23 17:44:34
///////////////////////////
public class PanelESGkillRank : BasePanel {

    private GameObject m_hideBtnGo;
    private ScrollRect m_scrollContent;
    private DataGrid m_campRankDataGrid1;
    private DataGrid m_campRankDataGrid2;
    private GameObject m_camp1;
    private GameObject m_camp2;
    private float m_tick;
    private bool m_isShowCampIcon = false;

    public PanelESGkillRank()
    {
        SetPanelPrefabPath("UI/ESG/PanelESGKillRank");
        AddPreLoadRes("UI/ESG/PanelESGKillRank", EResType.UI);
    }
   
    public override void Init()
    {
        m_hideBtnGo = m_tran.Find("hideBtn").gameObject;
        m_scrollContent = m_tran.Find("ScrollContent").GetComponent<ScrollRect>();
        m_campRankDataGrid1 = m_tran.Find("ScrollContent/ESGItemCampList0").gameObject.AddComponent<DataGrid>();
        m_campRankDataGrid1.SetItemRender(m_tran.Find("ScrollContent/ESGItemCampList0/ESGItem").gameObject, typeof(ESGRankItem));

        m_campRankDataGrid2 = m_tran.Find("ScrollContent/ESGItemCampList1").gameObject.AddComponent<DataGrid>();
        m_campRankDataGrid2.SetItemRender(m_tran.Find("ScrollContent/ESGItemCampList1/ESGItem").gameObject, typeof(ESGRankItem));

        m_camp1 = m_tran.Find("camp1").gameObject;
        m_camp2 = m_tran.Find("camp2").gameObject;
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_hideBtnGo).onPointerClick += OnHideBtnClick;
    }

    public override void OnShow()
    {
        isShow = true;
        OnUpdate();
        showCampIcon(m_isShowCampIcon);
        resetGridPosition();
    }

    private void resetGridPosition()
    {
        List<SBattleCamp> camps = PlayerBattleModel.Instance.battle_info.camps;
        if( camps.Count > 1 )
        {
            m_campRankDataGrid2.gameObject.GetRectTransform().anchoredPosition = new Vector3(-6, -208, 0);
        }else
        {
            m_campRankDataGrid2.gameObject.GetRectTransform().anchoredPosition = new Vector3(-6, -174, 0);
        }
    }

    public override void Update()
    {
        m_tick += Time.deltaTime;
        if (m_tick >= 1.0f)
        {
            m_tick = 0;
            OnUpdate();
        }
    }

    private int m_count = 0;
    public void OnUpdate()
    {
        if (!IsInited() || IsOpen() == false)
            return;
        List<SBattleCamp> camps = PlayerBattleModel.Instance.battle_info.camps;
        List<KillNumRankData> dataList1 = null;
        List<KillNumRankData> dataList2 = null;
        KillNumRankData[] data1 = new KillNumRankData[5];
        KillNumRankData[] data2 = new KillNumRankData[5];
        if (camps.Count == 1)
        {
            m_isShowCampIcon = false;
            m_count = 0;
            dataList1 = PlayerBattleModel.Instance.GetKillNumRankDataList(camps[0].camp);
            dataList1.Sort((a, b) => { return b.killNum != a.killNum ? b.killNum - a.killNum : a.id - b.id; });
            for (int i = 0; i < dataList1.Count; i++)
            {
                if (dataList1[i].id > 0 && m_count < 10)
                {
                    dataList1[i].rank = m_count + 1;
                    if (m_count < 5)
                    {
                        data1[m_count] = dataList1[i];
                    }
                    else
                    {
                        data2[m_count % 5] = dataList1[i];
                    }
                    m_count++;
                }
            }
        }
        else
        {
            m_isShowCampIcon = true;
            m_count = 0;
            dataList1 = PlayerBattleModel.Instance.GetKillNumRankDataList(camps[0].camp);
            dataList2 = PlayerBattleModel.Instance.GetKillNumRankDataList(camps[1].camp);
            dataList1.Sort((a, b) => { return b.killNum != a.killNum ? b.killNum - a.killNum : a.id - b.id; });
            dataList2.Sort((a, b) => { return b.killNum != a.killNum ? b.killNum - a.killNum : a.id - b.id; });
            for( int i = 0; i < dataList1.Count; i++ )
            {
                if (dataList1[i].id > 0 && m_count < 5)
                {
                    dataList1[i].rank = m_count + 1;
                    data1[m_count] = dataList1[i];
                    m_count++;
                }
                
            }
            m_count = 0;
            for (int i = 0; i < dataList2.Count; i++)
            {
                if (dataList2[i].id > 0 && m_count < 5)
                {
                    dataList2[i].rank = m_count + 1;
                    data2[m_count] = dataList2[i];
                    m_count++;
                }
            }
        }
        m_campRankDataGrid1.Data = data1;
        m_campRankDataGrid2.Data = data2;
    }

    private void showCampIcon(bool isShow)
    {
        m_camp1.TrySetActive(isShow);
        m_camp2.TrySetActive(isShow);
    }

    bool isShow = true;
    private void OnHideBtnClick(GameObject target, PointerEventData eventData)
    {
        isShow = m_scrollContent.gameObject.activeSelf;
        m_scrollContent.gameObject.TrySetActive(!isShow);
        showCampIcon(!isShow && m_isShowCampIcon);
        if (isShow == true)
        {
            m_hideBtnGo.GetRectTransform().anchoredPosition = new Vector3(-24, -62, 0);
            m_hideBtnGo.GetRectTransform().localScale = new Vector3(-1, -1, -1);
        }
        else
        {
            m_hideBtnGo.GetRectTransform().anchoredPosition = new Vector3(-175, -62, 0);
            m_hideBtnGo.GetRectTransform().localScale = new Vector3(1, 1, 1);
        }
    }


    public override void OnHide()
    {
    }

    public override void OnBack()
    {
    }

    public override void OnDestroy()
    {
    }
}
