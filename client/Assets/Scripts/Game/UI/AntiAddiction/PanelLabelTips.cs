﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelLabelTips : BasePanel {
    private RectTransform m_image;
    private Text m_text;
    private RectTransform m_textTran;
    private float m_startShowTime;
    private const float m_showTime = 5f;
    private bool m_startUpdate = false;
    public PanelLabelTips()
    {
        SetPanelPrefabPath("UI/AntiAddiction/PanelLabelTips");
        AddPreLoadRes("UI/AntiAddiction/PanelLabelTips", EResType.UI);
        AddPreLoadRes("Atlas/Battle", EResType.Atlas);
    }


    public override void Init()
    {
        m_image = m_tran.Find("Image").GetComponent<RectTransform>();
        m_text = m_tran.Find("Image/Text").GetComponent<Text>();
        m_textTran = m_text.GetComponent<RectTransform>();
    }

    public override void InitEvent()
    {
        
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }


    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        if(!HasParams())
        {
            HidePanel();
        }
        string content = (string)m_params[0];
        m_text.text = content;        
        m_image.sizeDelta = new Vector2(m_text.preferredWidth+150, m_image.sizeDelta.y);
        m_startShowTime = Time.realtimeSinceStartup;
        m_startUpdate = true;
    }

    public override void Update()
    {
        if (!m_startUpdate)
            return;
        if ((Time.time - m_startShowTime) > m_showTime)
        {
            m_startUpdate = false;
            HidePanel();
        }
    }
}
