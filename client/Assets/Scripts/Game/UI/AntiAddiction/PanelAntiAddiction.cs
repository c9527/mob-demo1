﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class PanelAntiAddiction:BasePanel
{

    public PanelAntiAddiction()
    {
        SetPanelPrefabPath("UI/AntiAddiction/PanelAntiAddiction");
        AddPreLoadRes("UI/AntiAddiction/PanelAntiAddiction", EResType.UI);
        AddPreLoadRes("Atlas/Common", EResType.Atlas);
    }

    private InputField m_nameInput;
    private InputField m_idInput;
    public override void Init()
    {
        m_nameInput = m_tran.Find("name").GetComponent<InputField>();
        m_idInput = m_tran.Find("id").GetComponent<InputField>();
        m_content = m_tran.Find("PanelTip/background/Text").GetComponent<Text>();
        m_tranOk = m_tran.Find("PanelTip/background/btnOk").GetComponent<RectTransform>();
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("Button")).onPointerClick += ClickSure;
        UGUIClickHandler.Get(m_tran.Find("CloseBtn")).onPointerClick += ClickClose;
        UGUIClickHandler.Get(m_tranOk).onPointerClick += OnOkClick;
    }

    private void OnOkClick(GameObject target, PointerEventData eventData)
    {
        m_tran.Find("PanelTip").gameObject.TrySetActive(false);
    }

    private void ClickClose(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        HidePanel();
    }

    private void ClickSure(GameObject target, UnityEngine.EventSystems.PointerEventData eventData)
    {
        if(m_nameInput.text.Length==0)
        {
            ShowTipPanel("名字不能为空");
            return;
        }
        Regex regexName = new Regex(@"[^\u3e00-\u9fff]{2,8}");
        bool matchName = regexName.IsMatch(m_nameInput.text.Trim());
        if (matchName)
        {
            ShowTipPanel("名字格式错误");
            return;
        }
        if(m_idInput.text.Trim().Length!=18)
        {
            ShowTipPanel("身份证长度必须为18");
            return;
        }
        Regex regexID = new Regex(@"[0-9]{17}[0-9|X|x]{1}");
        bool matchID = regexID.IsMatch(m_idInput.text.Trim());
        if(!matchID)
        {
            ShowTipPanel("您填写的信息无效，请确认后再填写");
            return;
        }
        bool idcheck=IDCheck(m_idInput.text.Trim());
        if(!idcheck)
        {
            ShowTipPanel("您填写的信息无效，请确认后再填写");
            return;
        }
        NetLayer.Send(new tos_player_anti_addiction() { player_name = m_nameInput.text.Trim(), id = m_idInput.text.Trim() });
        HidePanel();
        //Logger.Log("playerName:" + m_nameInput.text + "id:" + m_idInput.text);
    }

    private bool IDCheck(string id)
    {        
        int year = int.Parse(id.Substring(6, 4));
        int mouth = int.Parse(id.Substring(10, 2));
        int day = int.Parse(id.Substring(12, 2));
        if ((year >= 1900 && (mouth > 0 && mouth < 13) && (day > 0 && day < 32)))
        {            
            return true;
        }
        else
        {
            return false;
        }
        //DateTime birthday = new DateTime(year, mouth, day);
        //DateTime now = TimeUtil.GetNowTime();                

    }

    private void Toc_player_anti_addiction(toc_player_anti_addiction toc_data)
    {
        //Logger.Log("anti_addiction:" + toc_data.anti_addiction);
        if (toc_data.anti_addiction == 1)
        {
            AntiAddictionManager.Instance.state = AntiAddictionState.Pass;
            AntiAddictionManager.AddTimer();
            ShowTipPanel("您的账号已通过防沉迷系统验证");
        } 
        else if(toc_data.anti_addiction==0)
        {
            AntiAddictionManager.Instance.state = AntiAddictionState.NotPass;
            ShowTipPanel("您的账号是未成年账号，请合理安排游戏时间");
        }
        else
        {
            HidePanel();
        }
    }

    private void Toc_player_err_msg(toc_player_err_msg proto)
    {
        var errMsg = Util.GetErrMsg(proto.err_code, proto.err_msg);
        ShowTipPanel(errMsg);
    }

    private Text m_content;
    private RectTransform m_tranOk;
    private void ShowTipPanel(string msg)
    {
        m_tran.Find("PanelTip").gameObject.TrySetActive(true);
        m_content.text = msg;
    }

    public override void OnBack()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShow()
    {
        
    }

    public override void Update()
    {
        
    }
    
}
