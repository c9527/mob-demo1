﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System;


public class AntiAddictionManager  {

    private static AntiAddictionManager m_instance;
    public static AntiAddictionManager Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = new AntiAddictionManager();
            return m_instance;
        }
    }

    /// <summary>
    /// 在线1小时、2小时、2小时55分、3小时计时器Key数组
    /// </summary>
    private List<int> m_timerIdList = new List<int>(); 
    public bool isFirstLoading = true;
    public AntiAddictionState state = AntiAddictionState.Pass;
    public float gameTime;
    public RewardState rewardState = RewardState.Full;
    private static void Toc_player_game_time_today(toc_player_game_time_today tocData)
    {
        if (Driver.isMobilePlatform)
            return;
        var instance = AntiAddictionManager.Instance;
        instance.state = AntiAddictionState.NotPass + tocData.anti_addiction;
        instance.gameTime = tocData.game_time;
        AddTimer();
    }


    private static void ResetTimer()
    {
        AntiAddictionManager.Instance.gameTime = 0f;
        AddTimer();
    }

    public static void AddTimer()
    {
        var instance=AntiAddictionManager.Instance;
        instance.rewardState = RewardState.Full;
        for (int i = 0; i <instance.m_timerIdList.Count; i++)
        {
            if (instance.m_timerIdList[i] > 0)
                TimerManager.RemoveTimeOut(instance.m_timerIdList[i]);
        }
        instance.m_timerIdList.Clear();
        if (instance.state == AntiAddictionState.Pass)
            return;

        const string normal = "累计在线已满{0}";
        const string inbattleHalf = "累计在线已满{0}，收益减半";
        const string outbattleHalf = "累计在线已满{0}，继续游戏收益将减半，请您下线休息，做适当身体运动。";
        const string inbattleZero = "累计在线已满{0}，收益为0，累计下线5小时后恢复正常";
        const string outbattleZero = "累计在线已满{0}，已进入不健康游戏时间，请下线休息。如继续游戏，您的游戏收益将为0。累计下线5小时后，游戏收益恢复正常。";
        Add(1, normal, normal);
        Add(2, normal, normal);
        Add(3, inbattleHalf, outbattleHalf, RewardState.Half);
        Add(3.5f, inbattleHalf, outbattleHalf, RewardState.Half);
        Add(4, inbattleHalf, outbattleHalf, RewardState.Half);
        Add(4.5f, inbattleHalf, outbattleHalf, RewardState.Half);
        Add(5, inbattleZero, outbattleZero, RewardState.Zero);
        Add(5.25f, inbattleZero, outbattleZero, RewardState.Zero);
        Add(5.5f, inbattleZero, outbattleZero, RewardState.Zero);
        Add(5.75f, inbattleZero, outbattleZero, RewardState.Zero);
        Add(6f, inbattleZero, outbattleZero, RewardState.Zero);

        //超过6小时
        string time;
        var hour = instance.gameTime/3600;
        var min = (int)(hour - (int)hour) * 60;
        if (min > 0)
            time = (int)hour + "小时" + min + "分";
        else
            time = (int)hour + "小时";
        if (instance.gameTime >= 3600 * 3 && instance.gameTime < 3600*5)
            TimerFun(new AntiAddictionTimerInfo() { inBattle = string.Format(inbattleHalf, time), outBattle = string.Format(outbattleHalf, time), rewardState = RewardState.Half });
        else if (instance.gameTime >= 3600*6)
            TimerFun(new AntiAddictionTimerInfo() { inBattle = string.Format(inbattleZero, time), outBattle = string.Format(outbattleZero, time), rewardState = RewardState.Zero });

        DateTime nowData = TimeUtil.GetNowTime();
        DateTime nextDate = new DateTime(nowData.Year, nowData.Month, nowData.Date.Day+1, 0, 0, 0);
        TimerManager.SetTimeOut((float)(nextDate - nowData).TotalSeconds, ResetTimer, true);
    }

    private static void Add(float hour, string inBattle, string outBattle, RewardState rewardState = RewardState.Full)
    {
        string time;
        var min = (int)(hour - (int) hour)*60;
        if (min > 0)
            time = (int) hour + "小时" + min + "分";
        else
            time = (int)hour + "小时";

        inBattle = string.Format(inBattle, time);
        outBattle = string.Format(outBattle, time);
        var info = new AntiAddictionTimerInfo() {inBattle = inBattle, outBattle = outBattle, rewardState = rewardState};
        if (Instance.gameTime < 3600f*hour)
            Instance.m_timerIdList.Add(TimerManager.SetTimeOut(3600f * hour - Instance.gameTime, TimerFun, info, true));
    }

    private static void TimerFun(object param)
    {
        var info = (AntiAddictionTimerInfo)param;
        if (UIManager.IsOpen<PanelBattle>())
            UIManager.PopPanel<PanelLabelTips>(new object[] { info.inBattle });
        else
            UIManager.ShowTipPanel(info.outBattle);

        if (info.rewardState != RewardState.Full)
        {
            AntiAddictionManager.Instance.rewardState = info.rewardState;
            NetLayer.Send(new tos_player_set_anti_addiction_rate());
            PanelHall.TryUpdateAntiAddiction();
        }
    }
}

public struct AntiAddictionTimerInfo
{
    public string inBattle;
    public string outBattle;
    public RewardState rewardState;
}


public enum AntiAddictionState
{
    NotPass,  //已经登记但未满18岁
    Pass, //满18岁
    NotCheck, //未验证登陆
}

public enum RewardState
{
    Full,
    Half,
    Zero,
}
