﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：作战指示
//创建者：hwl
//创建日期: 2016/8/24
///////////////////////////////////////////
public class FightDirectionPanel : MonoBehaviour
{
    private Toggle m_ToggleDef;
    private Toggle m_ToggleAttck;
    private Toggle m_ToggleCommunicate;
    private int m_type = 1;
    private Text[] m_contentList = new Text[4];
    private float m_lastTime = 0f;
    private GameObject m_bg1;
    private GameObject m_bg2;
    private GameObject m_numListGo1;
    private GameObject m_numListGo2;
    private GameObject m_toggleText1;
    private GameObject m_toggleText2;
    private GameObject m_toggleText3;
    enum FightDirectionEnum
    {
        def = 1,
        attck = 2,
        communication =3
    }
    void Start()
    {
        Transform tran = gameObject.transform;
        m_numListGo1 = tran.Find("NumList1").gameObject;
        m_numListGo2 = tran.Find("NumList2").gameObject;
        m_numListGo1.TrySetActive(false);
        m_numListGo2.TrySetActive(false);
        m_bg1 = tran.Find("Bg1").gameObject;
        m_bg2 = tran.Find("Bg2").gameObject;
        m_bg1.TrySetActive(false);
        m_bg2.TrySetActive(false);
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            m_bg2.TrySetActive(false);
            m_bg1.TrySetActive(true);
            m_numListGo1.TrySetActive(true);
            m_numListGo2.TrySetActive(false);
        }
        m_ToggleDef = tran.Find("fram/ToggleGroup/ToggleDef").GetComponent<Toggle>();
        m_ToggleAttck = tran.Find("fram/ToggleGroup/ToggleAttack").GetComponent<Toggle>();
        m_ToggleCommunicate = tran.Find("fram/ToggleGroup/ToggleCommunicate").GetComponent<Toggle>();
        m_toggleText1 = tran.Find("fram/ToggleGroup/ToggleDef/Label").gameObject;
        m_toggleText2 = tran.Find("fram/ToggleGroup/ToggleAttack/Label").gameObject;
        m_toggleText3 = tran.Find("fram/ToggleGroup/ToggleCommunicate/Label").gameObject;
        UGUIClickHandler.Get(m_ToggleDef.gameObject).onPointerClick += onToggleClick;
        UGUIClickHandler.Get(m_ToggleAttck.gameObject).onPointerClick += onToggleClick;
        UGUIClickHandler.Get(m_ToggleCommunicate.gameObject).onPointerClick += onToggleClick;
        UGUIClickHandler.Get(tran.Find("fram/clickarea").gameObject).onPointerClick += onCloseHandle;
        Text text;
        for( int i = 0; i < 4; i++ )
        {
            text =  tran.Find("fram/content/content" + i).GetComponent<Text>();
            UGUIClickHandler.Get(text.gameObject).onPointerClick += SendInfoHandle;
            m_contentList[i] = text;
        }
        updateContent(m_type);
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () =>
            {
                if (gameObject == null || gameObject.activeSelf == false) return;
                if (m_bg1.activeSelf == true && m_bg2.activeSelf == false)
                {
                    m_ToggleDef.isOn = true;
                    updateContent((int)FightDirectionEnum.def);
                    showNumListGo();
                    initNumPosition();
                    return;
                }
                
                SendInfoHandle(m_contentList[3].gameObject, null);
            });
            GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () =>
            {
                if (gameObject == null || gameObject.activeSelf == false) return;
                if (m_bg1.activeSelf == true && m_bg2.activeSelf == false)
                {
                    
                    m_ToggleAttck.isOn = true;
                    updateContent((int)FightDirectionEnum.attck);
                    showNumListGo();
                    initNumPosition();
                    return;
                }
               
                SendInfoHandle(m_contentList[2].gameObject, null);
            });
            GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_3_DOWN, () =>
            {
                if (gameObject== null || gameObject.activeSelf == false) return;
                if (m_bg1.activeSelf == true && m_bg2.activeSelf == false)
                {
                    m_ToggleCommunicate.isOn = true;
                    updateContent((int)FightDirectionEnum.communication);
                    showNumListGo();
                    initNumPosition();
                    return;
                }
                
                SendInfoHandle(m_contentList[1].gameObject, null);
            });
            GameDispatcher.AddEventListener(GameEvent.INPUT_KEY_4_DOWN, () =>
            {
                if (gameObject == null || gameObject.activeSelf == false) return;
                if (m_bg1.activeSelf == true && m_bg2.activeSelf == false)
                {
                    return;
                }
                SendInfoHandle(m_contentList[0].gameObject, null);
            });
        }
        initPosition();
    }

    private void showNumListGo()
    {
        m_bg2.TrySetActive(true);
        m_bg1.TrySetActive(false);
        m_numListGo1.TrySetActive(false);
        m_numListGo2.TrySetActive(true);
    }

    private void initNumPosition()
    {
        Image[] list = m_numListGo1.GetComponentsInChildren<Image>();
        Quaternion quaternion = Quaternion.Euler(0, 0, 0);
        if (m_ShowType == GameConst.FIGHTDIRECTIONBTN_TOP)//左上
        {
            quaternion = Quaternion.Euler(0, 0, 180);
        }
        else if (m_ShowType == GameConst.FIGHTDIRECTIONBTN_BOTTOM)
        {
            quaternion = Quaternion.Euler(0, 0, 0);
        }
        for (int i = 0; i < list.Length; i++)
        {
            list[i].gameObject.GetRectTransform().localRotation = quaternion;
        }
        list = m_numListGo2.GetComponentsInChildren<Image>();
        for (int i = 0; i < list.Length; i++)
        {
            list[i].gameObject.GetRectTransform().localRotation = quaternion;
        }
    }
    float m_ShowType;
    private void initPosition()
    {
        m_ShowType = GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_FIGHT_DIRECTION);
        if (m_ShowType == GameConst.FIGHTDIRECTIONBTN_TOP)//左上
        {
            gameObject.GetRectTransform().anchoredPosition = new Vector2(112,-222);
            gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            gameObject.GetRectTransform().anchorMax = UIHelper.ZERO_X_ONE_Y_VEC;
            gameObject.GetRectTransform().anchorMin = UIHelper.ZERO_X_ONE_Y_VEC;
            for( int i = 0; i< m_contentList.Length;i++ )
            {
                m_contentList[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            }
            m_toggleText1.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            m_toggleText2.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            m_toggleText3.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            Image[] list = m_numListGo1.GetComponentsInChildren<Image>();
            for( int i = 0; i < list.Length;i++ )
            {
                list[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            }
            list = m_numListGo2.GetComponentsInChildren<Image>();
            for (int i = 0; i < list.Length; i++)
            {
                list[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 180);
            }

        }
        else if (m_ShowType == GameConst.FIGHTDIRECTIONBTN_BOTTOM)//右下
        {
            gameObject.GetRectTransform().anchoredPosition = new Vector2(112, 0);
            gameObject.GetRectTransform().anchorMax = new Vector2(1f, 0f);
            gameObject.GetRectTransform().anchorMin = new Vector2(1f, 0f);
            gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            for (int i = 0; i < m_contentList.Length; i++)
            {
                m_contentList[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            }
            m_toggleText1.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            m_toggleText2.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            m_toggleText3.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            Image[] list = m_numListGo1.GetComponentsInChildren<Image>();
            for (int i = 0; i < list.Length; i++)
            {
                list[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            }
            list = m_numListGo2.GetComponentsInChildren<Image>();
            for (int i = 0; i < list.Length; i++)
            {
                list[i].gameObject.GetRectTransform().localRotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }

    void Update()
    {
        if (m_ShowType == GameConst.FIGHTDIRECTIONBTN_BOTTOM)
        {
            if (gameObject.GetRectTransform().anchoredPosition.x > -100 && gameObject.GetRectTransform().anchoredPosition.y < 212)
            {
                gameObject.GetRectTransform().anchoredPosition = new Vector3(gameObject.GetRectTransform().anchoredPosition.x - Time.deltaTime * 1000, gameObject.GetRectTransform().anchoredPosition.y + Time.deltaTime * 1000, 0);
            }
        }
    }
    //发送语音
    private void SendInfoHandle(GameObject target, PointerEventData eventData)
    {

        if (Time.realtimeSinceStartup - m_lastTime < 10)
        {
            TipsManager.Instance.showTips("每隔10秒才能发送一次无线电消息");
            gameObject.TrySetActive(false);
            return;
        }

        m_lastTime = Time.realtimeSinceStartup;
        Text label = target.GetComponent<Text>();
        string content = label.text;
        NetLayer.Send(new tos_fight_chat()
        {
            type = 1,
            msg = WordFiterManager.Fiter(content),
            command_id = int.Parse(label.name),
            audio_chat = new p_audio_chat() { content = new byte[0], duration = 0 }
        });
       gameObject.TrySetActive(false);
    }


    private void onCloseHandle(GameObject target, PointerEventData eventData)
    {
        gameObject.GetRectTransform().anchoredPosition = new Vector3(112, 0, 0);
        gameObject.TrySetActive(false);

    }

    private void onToggleClick(GameObject target, PointerEventData eventData)
    {
        if( target.name == m_ToggleDef.name )
        {
            m_type = (int)FightDirectionEnum.def;
        }else if( target.name == m_ToggleAttck.name )
        {
            m_type = (int)FightDirectionEnum.attck;
        }
        else
        {
            m_type = (int)FightDirectionEnum.communication;
        }
        target.transform.SetAsFirstSibling();
        updateContent(m_type);
    }

    private void updateContent(int type)
    {
        ConfigFightDirection config = ConfigManager.GetConfig<ConfigFightDirection>();
        if( config != null )
        {
            List<ConfigFightDirectionLine> list = config.GetFightDirectionLineListByType(type);
            for( int i = 0; i < m_contentList.Length; i++ )
            {
                if( i < list.Count )
                {
                    m_contentList[i].text = list[i].Content;
                    m_contentList[i].name = list[i].ID.ToString();
                }
                else
                {
                    m_contentList[i].text = "";
                }
            }
        }
    }

    public void Init()
    {
        initPosition();
        m_ToggleDef.isOn = true;
        m_type = (int)FightDirectionEnum.def;
        updateContent(m_type);
        if( Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web )
        {
            m_bg2.TrySetActive(false);
            m_bg1.TrySetActive(true);
            m_numListGo1.TrySetActive(true);
            m_numListGo2.TrySetActive(false);
        }
    }

    void OnDisable()
    {
        if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            m_bg2.TrySetActive(false);
            m_bg1.TrySetActive(true);
            m_numListGo1.TrySetActive(true);
            m_numListGo2.TrySetActive(false);
        }
    }
}
