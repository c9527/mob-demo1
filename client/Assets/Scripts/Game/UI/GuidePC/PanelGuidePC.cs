﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/1/7 16:18:53
 * Description:PC版新手指引
 * Update History:
 *
 **************************************************************************/

public class PanelGuidePC : BasePanel
{
    /// <summary>
    /// 引导步骤展示顺序
    /// </summary>
    private List<int> stepList;
    public const int SHOW_LEVEL = 8;
    public enum GUIDESTEP
    {
        SETP1_MOVE = 1,//移动提示wasd
        SETP2_SELECTWEAPON,//选择武器 1234
        SETP3_SWITCHWEAPON,//指引切枪R
        SETP4_FIRE,//开火
        SETP5_LOADBULLET,//装弹
        SETP6_SQUAT,//蹲下
        SETP7_JUMP,//跳跃
        SETP8_TABRECORD//积分版
    }

    public const string KEY_1 = "1";
    public const string KEY_2 = "2";
    public const string KEY_3 = "3";
    public const string KEY_4 = "4";

    public const string KEY_A = "A";
    public const string KEY_S = "S";
    public const string KEY_D = "D";
    public const string KEY_W = "W";

    public const string KEY_Q = "Q";
    public const string KEY_F = "F";
    public const string KEY_SPACE = "SPACE";
    public const string KEY_R = "R";
    public const string KEY_TAB = "TAB";
    public const string KEY_SHIFT = "SHIFT";
    public const string KEY_MOUSELEFT = "MOUSELEFT";

    /// <summary>
    /// 按键操作所对应的步骤
    /// </summary>
    private Dictionary<string, int> keyToStep;

    public int guideStep = 0;

    public GameObject m_stepUseSkill;
    /// <summary>
    /// 使用技能指引提示
    /// </summary>
    public const string EVENT_STEP_USESKILL = "PC_EVENT_STEP_USESKILL";
    public PanelGuidePC()
    {
        SetPanelPrefabPath("UI/GuidePC/PanelGuidePC");
        AddPreLoadRes("UI/GuidePC/PanelGuidePC", EResType.UI);
        // 构造器
        keyToStep = new Dictionary<string, int>();
        keyToStep.Add(KEY_A, (int)GUIDESTEP.SETP1_MOVE);
        keyToStep.Add(KEY_S, (int)GUIDESTEP.SETP1_MOVE);
        keyToStep.Add(KEY_D, (int)GUIDESTEP.SETP1_MOVE);
        keyToStep.Add(KEY_W, (int)GUIDESTEP.SETP1_MOVE);

        keyToStep.Add(KEY_1, (int)GUIDESTEP.SETP2_SELECTWEAPON);
        keyToStep.Add(KEY_2, (int)GUIDESTEP.SETP2_SELECTWEAPON);
        keyToStep.Add(KEY_3, (int)GUIDESTEP.SETP2_SELECTWEAPON);
        keyToStep.Add(KEY_4, (int)GUIDESTEP.SETP2_SELECTWEAPON);

        keyToStep.Add(KEY_Q, (int)GUIDESTEP.SETP3_SWITCHWEAPON);

        keyToStep.Add(KEY_MOUSELEFT, (int)GUIDESTEP.SETP4_FIRE);
        keyToStep.Add(KEY_R, (int)GUIDESTEP.SETP5_LOADBULLET);
        keyToStep.Add(KEY_SHIFT, (int)GUIDESTEP.SETP6_SQUAT);
        keyToStep.Add(KEY_SPACE, (int)GUIDESTEP.SETP7_JUMP);
        keyToStep.Add(KEY_TAB, (int)GUIDESTEP.SETP8_TABRECORD);



        // keyToStep.Add();
    }
    public override void Init()
    {
        m_stepUseSkill = m_tran.Find("Step_useSkill").gameObject;
        m_stepUseSkill.TrySetActive(false);
    }
    public override void InitEvent()
    {
        AddEventListener(GameEvent.INPUT_KEY_1_DOWN, () => { dealKey(KEY_1); });
        AddEventListener(GameEvent.INPUT_KEY_2_DOWN, () => { dealKey(KEY_2); });
        AddEventListener(GameEvent.INPUT_KEY_3_DOWN, () => { dealKey(KEY_3); });
        AddEventListener(GameEvent.INPUT_KEY_4_DOWN, () => { dealKey(KEY_4); });

        AddEventListener(GameEvent.INPUT_KEY_A_DOWN, () => { dealKey(KEY_A); });
        AddEventListener(GameEvent.INPUT_KEY_S_DOWN, () => { dealKey(KEY_S); });
        AddEventListener(GameEvent.INPUT_KEY_D_DOWN, () => { dealKey(KEY_D); });
        AddEventListener(GameEvent.INPUT_KEY_W_DOWN, () => { dealKey(KEY_W); });

        AddEventListener(GameEvent.INPUT_KEY_Q_DOWN, () => { dealKey(KEY_Q); });
        AddEventListener(GameEvent.INPUT_KEY_R_DOWN, () => { dealKey(KEY_R); });
        AddEventListener(GameEvent.INPUT_MOUSELEFT_ACTION, () => { dealKey(KEY_MOUSELEFT); });

        AddEventListener<bool>(GameEvent.INPUT_KEY_SHIFT, (isDown) =>
        {
            if (isDown)
                dealKey(KEY_SHIFT);
        });
        AddEventListener(GameEvent.INPUT_KEY_SPACE_DOWN, () => { dealKey(KEY_SPACE); });
        AddEventListener<bool>(GameEvent.INPUT_KEY_TAB_DOWN, (isDown) => { if (isDown) dealKey(KEY_TAB); });
        AddEventListener<bool>(EVENT_STEP_USESKILL, (isShow) =>
        {
            if (isShow)
            {
                if (m_stepUseSkill.activeSelf != isShow)
                    m_stepUseSkill.TrySetActive(isShow);
            }
            else
            {
                //指引完关闭指引面板
                m_stepUseSkill.TrySetActive(false);
                HidePanel();
            }
        });
    }
    public override void OnShow()
    {
        for(int i=0;i<m_tran.childCount;i++){
            m_tran.GetChild(i).gameObject.TrySetActive(false);
        }

        var gameType = WorldManager.singleton.gameRuleLine.GameType;
        if (gameType == GameConst.GAME_RULE_SOLO
            || gameType == GameConst.GAME_RULE_KILLALL
            || gameType == GameConst.GAME_RULE_PVP
            || gameType == GameConst.GAME_RULE_BURST
            || gameType == GameConst.GAME_RULE_EXPLODE_REVIVE
            || gameType == GameConst.GAME_RULE_ERADICATE
            || gameType == GameConst.GAME_RULE_ERADICATE2
            || gameType == GameConst.GAME_RULE_CHALLENGE
            || gameType == GameConst.GAME_RULE_DEFEND
            || gameType == GameConst.GAME_RULE_DEFEND0
            || gameType == GameConst.GAME_RULE_DEFEND2
            || gameType == GameConst.GAME_RULE_SUR_SNIPE
            || gameType == GameConst.GAME_RULE_FIGHT_IN_TURN)
        {
            stepList = new List<int>() { (int)GUIDESTEP.SETP1_MOVE, (int)GUIDESTEP.SETP2_SELECTWEAPON, (int)GUIDESTEP.SETP3_SWITCHWEAPON, (int)GUIDESTEP.SETP4_FIRE, (int)GUIDESTEP.SETP5_LOADBULLET, (int)GUIDESTEP.SETP6_SQUAT, (int)GUIDESTEP.SETP7_JUMP, (int)GUIDESTEP.SETP8_TABRECORD };
            showNextGuide(0);
        }
        else if (gameType == GameConst.GAME_RULE_KING_SOLO
            || gameType == GameConst.GAME_RULE_KING_CAMP
            || gameType == GameConst.GAME_RULE_SNIPE)
        {
            stepList = new List<int>() { (int)GUIDESTEP.SETP1_MOVE, (int)GUIDESTEP.SETP3_SWITCHWEAPON, (int)GUIDESTEP.SETP4_FIRE, (int)GUIDESTEP.SETP5_LOADBULLET, (int)GUIDESTEP.SETP6_SQUAT, (int)GUIDESTEP.SETP7_JUMP, (int)GUIDESTEP.SETP8_TABRECORD };
            showNextGuide(0);
        }
        else
        {
            stepList = new List<int>();
            showNextGuide(-1);
        }
    }

    private void dealKey(string key)
    {
        if (keyToStep.ContainsKey(key))
        {
            if (guideStep >= 0 && guideStep < stepList.Count)
                if (keyToStep[key] == stepList[guideStep])
                {
                    showNextGuide(guideStep + 1);
                }
        }
    }

    private void showNextGuide(int step)
    {
        if (step < stepList.Count)
        {
            guideStep = step;
            bool isShow = false;
            for (int i = 0; i < stepList.Count; i++)
            {
                if (i == guideStep)
                {
                    isShow = true;
                }
                else
                {
                    isShow = false;
                }
                m_tran.Find("Step_" + stepList[i]).gameObject.TrySetActive(isShow);
            }
        }
        else
        {
            //指引显示完关闭面板
            HidePanel();
        }
    }

    public override void OnHide() { }
    public override void OnBack() { }
    public override void OnDestroy() { }
    public override void Update() { }
}
