﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class PanelAchievement : BasePanel
{
    private GameObject m_goOverView;
    private GameObject m_goModule;
    private DataGrid m_teamTitleGrid;
    private DataGrid m_teamContentGrid;
    private DataGrid m_badgeGrid;
    private List<AchieveBadge> m_lstBadges;
    //private Text m_txtAllProcessProb;
    //private Text m_txtAllProcessNum;
    //private Image m_imgAllProcessSchedule;
    //private Text m_txtWeaponProcess;
    //private Text m_txtCompetitionProcess;
    //private Text m_txtMapProcess;
    //private Text m_txtChallengeProcess;
    //private Text m_txtCareerProcess;

    private int m_curTeamId;
    private string m_curModule;

    //private Toggle m_toggleOverView;
    //private Toggle m_toggleWeapon;
    //private Toggle m_toggleCompetition;
    //private Toggle m_toggleMap;
    //private Toggle m_toggleChallenge;
    //private Toggle m_toggleCareer;

    //private GameObject m_goWeaponBubble;
    //private GameObject m_goCompetitionBubble;
    //private GameObject m_goMapBubble;
    //private GameObject m_goChallengeBubble;
    //private GameObject m_goCareerBubble;

    private Image m_weaponbar;
    private Image m_weaponicon;
    private Image m_mapbar;
    private Image m_mapicon;
    //private Image m_pvpbar;
    //private Image m_pvpicon;
    //private Image m_challengebar;
    //private Image m_challengeicon;
    private Image m_careerbar;
    private Image m_careericon;


    private Text m_acnum;
    private Text m_gpnum;
    private Text m_biggpnum;
    private Image m_teamIcon;


    private Text m_desc;
    private Image m_acbar;


    private GameObject m_gp_gift;
    private Text m_gp_gift_txt;


    private GameObject m_madel_gift;
    private Image m_medal_icon;
    private Text m_medal_txt;

    private GameObject m_title_gift;
    private Image m_title_icon;
    private Text m_title_txt;

    private int ac_id;

    ConfigAchievementIcon configAchieveIcon;

    GameObject m_btnget;
    Text m_btntxt;
    GameObject m_get;

    GameObject m_weaponbubble;
    GameObject m_careerbubble;
    GameObject m_mapbubble;
    public PanelAchievement()
    {
        SetPanelPrefabPath("UI/Achievement/PanelAchievement");
        AddPreLoadRes("UI/Achievement/PanelAchievement", EResType.UI);
        AddPreLoadRes("UI/Achievement/ItemAchieveBadge", EResType.UI);
        AddPreLoadRes("UI/Achievement/ItemTeamTitle", EResType.UI);
        AddPreLoadRes("UI/Achievement/ItemAchievement", EResType.UI);

        AddPreLoadRes("Atlas/Achievement", EResType.Atlas);
    }

    public override void Init()
    {
        //m_dicAchievementData = new Dictionary<int, Dictionary<int, List<AchivementData>>>();
        //m_dicAchieveTeamTitle = new Dictionary<int, List<AchiveTeamTitle>>();
        //m_dicSumModuleCount = new Dictionary<int, int>();
        //m_dicSumModuleGetCount = new Dictionary<int, int>();
        m_curTeamId = 0;
        m_curModule = "";
        //m_sumAchieveCount = 0;
        m_lstBadges = new List<AchieveBadge>();

        m_goOverView = m_tran.Find("overview").gameObject;
        m_goModule = m_tran.Find("module").gameObject;

        m_badgeGrid = m_tran.Find("overview/badges/content").gameObject.AddComponent<DataGrid>();
        m_badgeGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Achievement/ItemAchieveBadge"), typeof(ItemAchieveBadge));

        m_teamTitleGrid = m_tran.Find("module/team_title/content").gameObject.AddComponent<DataGrid>();
        m_teamTitleGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Achievement/ItemTeamTitle"), typeof(ItemTeamTitle));
        m_teamTitleGrid.onItemSelected += OnSelTeamTitle;

        m_teamContentGrid = m_tran.Find("module/team_content/content").gameObject.AddComponent<DataGrid>();
        m_teamContentGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Achievement/ItemAchievement"), typeof(ItemAchievement));

        //m_toggleOverView = m_tran.Find("title/overview").GetComponent<Toggle>();
        //m_toggleWeapon = m_tran.Find("title/weapon").GetComponent<Toggle>();
        //m_goWeaponBubble = m_tran.Find("title/weapon/bubble_tip").gameObject;
        //m_toggleCompetition = m_tran.Find("title/competition").GetComponent<Toggle>();
        //m_goCompetitionBubble = m_tran.Find("title/competition/bubble_tip").gameObject;
        //m_toggleMap = m_tran.Find("title/map").GetComponent<Toggle>();
        //m_goMapBubble = m_tran.Find("title/map/bubble_tip").gameObject;
        //m_toggleChallenge = m_tran.Find("title/challenge").GetComponent<Toggle>();
        //m_goChallengeBubble = m_tran.Find("title/challenge/bubble_tip").gameObject;
        //m_toggleCareer = m_tran.Find("title/career").GetComponent<Toggle>();
        //m_goCareerBubble = m_tran.Find("title/career/bubble_tip").gameObject;

        //m_txtAllProcessProb = m_tran.Find("overview/schedule_all/schedule_kuang/schedule_prob").GetComponent<Text>();
        //m_txtAllProcessNum = m_tran.Find("overview/schedule_all/schedule_num").GetComponent<Text>();
        //m_imgAllProcessSchedule = m_tran.Find("overview/schedule_all/schedule_process").GetComponent<Image>();
        //m_txtWeaponProcess = m_tran.Find("overview/schedule_weapon/process").GetComponent<Text>();
        //m_txtCompetitionProcess = m_tran.Find("overview/schedule_competition/process").GetComponent<Text>();
        //m_txtMapProcess = m_tran.Find("overview/schedule_map/process").GetComponent<Text>();
        //m_txtChallengeProcess = m_tran.Find("overview/schedule_challenge/process").GetComponent<Text>();
        //m_txtCareerProcess = m_tran.Find("overview/schedule_career/process").GetComponent<Text>();

        //InitAchieveData();

        m_weaponbar = m_tran.Find("overview/schedule_weapon/circle").GetComponent<Image>();
        m_weaponicon = m_tran.Find("overview/schedule_weapon/circle_back").GetComponent<Image>();
        m_mapbar = m_tran.Find("overview/schedule_map/circle").GetComponent<Image>();
        m_mapicon = m_tran.Find("overview/schedule_map/circle_back").GetComponent<Image>();
        //m_pvpbar = m_tran.Find("overview/schedule_pvp/circle").GetComponent<Image>();
        //m_pvpicon = m_tran.Find("overview/schedule_pvp/circle_back").GetComponent<Image>();
        //m_challengebar = m_tran.Find("overview/schedule_challenge/circle").GetComponent<Image>();
        //m_challengeicon = m_tran.Find("overview/schedule_challenge/circle_back").GetComponent<Image>();
        m_careerbar = m_tran.Find("overview/schedule_career/circle").GetComponent<Image>();
        m_careericon = m_tran.Find("overview/schedule_career/circle_back").GetComponent<Image>(); 

        m_acnum = m_tran.Find("module/Title/acnum").GetComponent<Text>();
        m_gpnum = m_tran.Find("module/Title/gpnum").GetComponent<Text>();
        m_biggpnum = m_tran.Find("overview/GP/num").GetComponent<Text>();
        m_teamIcon = m_tran.Find("module/Title/icon").GetComponent<Image>();

        m_acbar = m_tran.Find("module/info/bar/fill").GetComponent<Image>();
        m_desc = m_tran.Find("module/info/desc").GetComponent<Text>();

        m_gp_gift = m_tran.Find("module/info/ScrollDataGrid/content/GP").gameObject;
        m_gp_gift_txt = m_tran.Find("module/info/ScrollDataGrid/content/GP/num").GetComponent<Text>();

        m_madel_gift = m_tran.Find("module/info/ScrollDataGrid/content/medal").gameObject;
        m_medal_icon = m_tran.Find("module/info/ScrollDataGrid/content/medal/icon").GetComponent<Image>();
        m_medal_txt = m_tran.Find("module/info/ScrollDataGrid/content/medal/Text").GetComponent<Text>();

        m_title_gift = m_tran.Find("module/info/ScrollDataGrid/content/title").gameObject;
        m_title_icon = m_tran.Find("module/info/ScrollDataGrid/content/title/icon").GetComponent<Image>();
        m_title_txt = m_tran.Find("module/info/ScrollDataGrid/content/title/Text").GetComponent<Text>();

        configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        ac_id = 0;

        m_get = m_tran.Find("module/info/get").gameObject;
        m_btnget = m_tran.Find("module/info/Button").gameObject;
        m_btntxt = m_tran.Find("module/info/Button/Text").GetComponent<Text>();

        m_weaponbubble = m_tran.Find("overview/schedule_weapon/bubble").gameObject;
        m_careerbubble = m_tran.Find("overview/schedule_career/bubble").gameObject;
        m_mapbubble = m_tran.Find("overview/schedule_map/bubble").gameObject;
    }

    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("overview/schedule_weapon")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("overview/schedule_pvp")).onPointerClick += null;
        UGUIClickHandler.Get(m_tran.Find("overview/schedule_map")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("overview/schedule_challenge")).onPointerClick += null;
        UGUIClickHandler.Get(m_tran.Find("overview/schedule_career")).onPointerClick += OnClickModule;
        m_teamContentGrid.onItemSelected += onAchieveSel;
        UGUIClickHandler.Get(m_btnget).onPointerClick += GetAc;
        //UGUIClickHandler.Get(m_tran.Find("title/overview")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("title/weapon")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("title/competition"));//.onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("title/map")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("title/challenge"));//.onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("title/career")).onPointerClick += OnClickModule;
        //UGUIClickHandler.Get(m_tran.Find("overview/test")).onPointerClick += OnClickTest;
    }


    void GetAc(GameObject target, PointerEventData eventData)
    {
            tos_player_get_achieve_reward getMsg = new tos_player_get_achieve_reward();
            getMsg.achieve_id = ac_id;
            NetLayer.Send(getMsg);
    }
    void onAchieveSel(object itemRender)
    {
        var info = itemRender as AchivementData;
        m_desc.text = info.desc;
        m_acbar.rectTransform.sizeDelta = new Vector2((189f * (float)info.finishedCount / (float)info.count), m_acbar.rectTransform.sizeDelta.y);
        ac_id = info.id;
        ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(info.badge);
        if(lineBadge != null)
        {
            m_madel_gift.TrySetActive(true);
            m_medal_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
            m_medal_icon.SetNativeSize();
        }
        else
        {
            m_madel_gift.TrySetActive(false);
        }

        ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(info.achieveTitle);
        if (lineChenghao != null)
        {
            m_title_gift.TrySetActive(true);
            m_title_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
            m_title_icon.SetNativeSize();
        }
        else
        {
            m_title_gift.TrySetActive(false);
        }

        if (info.gp == 0)
        {
            m_gp_gift.TrySetActive(false);
        }
        else
        {
            m_gp_gift.TrySetActive(true);
            m_gp_gift_txt.text = info.gp.ToString();
        }

        if (info.stat == Achieve_Stat.AS_GETTED)
        {
            m_btnget.TrySetActive(false);
            m_get.TrySetActive(true);
        }
        else if(info.stat == Achieve_Stat.AS_UN_GET)
        {
            m_btnget.TrySetActive(true);
            m_get.TrySetActive(false);
            m_btntxt.text = "可领取";
            Util.SetNormalShader(m_btnget.GetComponent<Button>());
        }
        else
        {
            m_btnget.TrySetActive(true);
            m_get.TrySetActive(false);
            m_btntxt.text = "未达成";
            Util.SetGrayShader(m_btnget.GetComponent<Button>());
        }


    }

    public override void OnShow()
    {
        ShowOverview();
        UpdateModuleBubble();
        //if (m_toggleOverView.isOn)
        //{
        //    ShowOverview();
        //}
        //else if (m_toggleWeapon.isOn)
        //{
        //    ShowAchievement("weapon");
        //}
        //else if (m_toggleCompetition.isOn)
        //{
        //    ShowAchievement("competition");
        //}
        //else if (m_toggleMap.isOn)
        //{
        //    ShowAchievement("map");
        //}
        //else if (m_toggleChallenge.isOn)
        //{
        //    ShowAchievement("challenge");
        //}
        //else if (m_toggleCareer.isOn)
        //{
        //    ShowAchievement("career");
        //}
    }

  
    public override void OnHide()
    {

    }

    public override void OnBack()
    {
        if (m_goModule.activeSelf == true)
        {
            m_goModule.TrySetActive(false);
            m_goOverView.TrySetActive(true);
        }
    }

    public override void OnDestroy()
    {
    }

    protected void OnClickModule(GameObject target, PointerEventData eventData)
    {
            ShowAchievement(target.name);
            
    }

    protected void ShowOverview()
    {
        m_curModule = "overview";
        m_goOverView.TrySetActive(true);
        m_goModule.TrySetActive(false);
        float fAllProcess = (float)PlayerAchieveData.m_sumGetAchievePoint / (float)PlayerAchieveData.m_sumAchievePoint;
        //m_imgAllProcessSchedule.fillAmount = fAllProcess;
        int iAllProcess = (int)(fAllProcess * 100f);
        //m_txtAllProcessProb.text = iAllProcess.ToString() + "%";
        //m_txtAllProcessNum.text = PlayerAchieveData.m_sumGetAchievePoint.ToString() + "/" + PlayerAchieveData.m_sumAchievePoint.ToString();

        float fWeaponProcess = (float)GetModuleGettedPoint(Achieve_Module.AM_WEAPON) / (float)PlayerAchieveData.m_dicSumModulePoint[(int)Achieve_Module.AM_WEAPON];
        int iWeaponProcess = (int)(fWeaponProcess * 100f);
        m_weaponbar.fillAmount = fWeaponProcess;

        if (iWeaponProcess >= 100)
        {
            m_weaponicon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, "weapon_finish"));
        }
        //m_txtWeaponProcess.text = iWeaponProcess.ToString() + "%";

        float fMapProcess = (float)GetModuleGettedPoint(Achieve_Module.AM_MAP) / (float)PlayerAchieveData.m_dicSumModulePoint[(int)Achieve_Module.AM_MAP];
        int iMapProcess = (int)(fMapProcess * 100f);
        m_mapbar.fillAmount = fWeaponProcess;
        if (iMapProcess >= 100)
        {
            m_mapicon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, "map_finish"));
        }
        //m_txtMapProcess.text = iMapProcess.ToString() + "%";

        //m_pvpbar.fillAmount = 0f;
        //m_challengebar.fillAmount = 0f;
        //float fCompetitionProcess = (float)GetModuleGettedCount(Achieve_Module.AM_COMPETITION) / (float)m_dicSumModuleCount[(int)Achieve_Module.AM_COMPETITION];
        //int iCompetitionProcess = (int)(fCompetitionProcess * 100f);
        ////m_txtCompetitionProcess.text = iCompetitionProcess.ToString() + "%";

        //float fChallengeProcess = (float)GetModuleGettedCount(Achieve_Module.AM_CHALLENGE ) / (float)m_dicSumModuleCount[(int)Achieve_Module.AM_CHALLENGE];
        //int iChallengeProcess = (int)(fChallengeProcess * 100f);
        ////m_txtChallengeProcess.text = iChallengeProcess.ToString() + "%";
        
        float fCareerProcess = (float)GetModuleGettedPoint(Achieve_Module.AM_CAREER) / (float)PlayerAchieveData.m_dicSumModulePoint[(int)Achieve_Module.AM_CAREER];
        int iCareerProcess = (int)(fCareerProcess * 100f);
        m_careerbar.fillAmount = fCareerProcess;
        if (iCareerProcess >= 100)
        {
            m_careericon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, "career_finish"));
        }
        //m_txtCareerProcess.text = iCareerProcess.ToString() + "%";
        

        m_lstBadges.Clear();
        var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        foreach(int badgeId in PlayerAchieveData.m_lstBadge)
        {
            ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(badgeId);
            if (lineBadge != null)
            {
                AchieveBadge acBadgeItem = new AchieveBadge();
                acBadgeItem.badge = lineBadge.Name;
                m_lstBadges.Add(acBadgeItem);
            }
        }
        if (m_lstBadges.Count < PlayerAchieveData.MAX_BADGE_IN_ROW)
            AddXBadge(PlayerAchieveData.MAX_BADGE_IN_ROW - m_lstBadges.Count);

        m_badgeGrid.Data = m_lstBadges.ToArray();
        m_gpnum.text = PlayerAchieveData.m_achievePoint.ToString();
        m_biggpnum.text = PlayerAchieveData.m_achievePoint.ToString();
        
    }

    protected int GetModuleGettedPoint( Achieve_Module module )
    {
        int key = (int)module;
        if (PlayerAchieveData.m_dicSumModuleGetPoint.ContainsKey(key))
            return PlayerAchieveData.m_dicSumModuleGetPoint[key];
        else
            return 0;
    }
    protected void ShowAchievement(string module)
    {
        m_curModule = module;
        m_goOverView.TrySetActive(false);
        m_goModule.TrySetActive(true);

        var config = ConfigManager.GetConfig<ConfigAchieveTagName>();
        for (int i = 0; i < config.m_dataArr.Length; ++i)
        {
            ConfigAchieveTagNameLine line = config.m_dataArr[i];
            if ("schedule_" + line.AchieveClassTitle == module)
            {
                m_teamIcon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, line.AchieveClassTitle));
                ShowModule(line.Tag);
                return;
            }
        }
    }

    protected void ShowModule(int module)
    {
        if (!PlayerAchieveData.m_dicAchieveTeamTitle.ContainsKey(module))
            return;

        List<AchiveTeamTitle> lstTeamTitle = PlayerAchieveData.m_dicAchieveTeamTitle[module];
        m_teamTitleGrid.Data = lstTeamTitle.ToArray();
        if(lstTeamTitle.Count > 0 )
        {
            ShowTeam(module, lstTeamTitle[0].teamId);
        }
    }

    protected void ShowTeam(int module,int team)
    {
        if (!PlayerAchieveData.m_dicAchievementData.ContainsKey(module))
            return;
        
        Dictionary<int, List<AchivementData>> dicTeamAchieve = PlayerAchieveData.m_dicAchievementData[module];
        if (!dicTeamAchieve.ContainsKey(team))
            return;
        
        int getnum = 0;
        int allnum = 0;

        foreach (var item in dicTeamAchieve)
        {
            for (int i = 0; i < item.Value.Count; i++)
            {
                allnum++;
                if (item.Value[i].stat == Achieve_Stat.AS_GETTED)
                {
                    getnum++;
                }
            }
        }




        m_curTeamId = team;
        List<AchivementData> lstAchieveData = dicTeamAchieve[team];
       
        var arr=lstAchieveData.ToArray();
        List<AchivementData> canGetted = new List<AchivementData>();
        List<AchivementData> notGet = new List<AchivementData>();
        List<AchivementData> getted = new List<AchivementData>(); 

        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i].stat == Achieve_Stat.AS_GETTED)
                getted.Add(arr[i]);
            else if (arr[i].stat == Achieve_Stat.AS_UN_GET)
                canGetted.Add(arr[i]);
            else
                notGet.Add(arr[i]);
        }
       
        

        canGetted.AddRange(notGet);
        canGetted.AddRange(getted);
        notGet.AddRange(getted);
        m_teamContentGrid.Data = canGetted.ToArray();
        //m_teamContentGrid.Data = lstAchieveData.ToArray();
        m_acnum.text = string.Format("{0}/{1}", getnum, allnum);
        if (PlayerAchieveData.m_dicSumModuleGetPoint.ContainsKey(module))
        {
            m_gpnum.text = PlayerAchieveData.m_dicSumModuleGetPoint[module].ToString();
        }
        else
        {
            m_gpnum.text = "0";
        }
        int teamId = m_teamTitleGrid.SelectedData<AchiveTeamTitle>().teamId;
        for( int i = 0; i < m_teamTitleGrid.ItemRenders.Count; ++i)
        {
            ItemTeamTitle item = m_teamTitleGrid.ItemRenders[i] as ItemTeamTitle;
            if (item.GetTeamId() == teamId )
                item.UpdateView();
        }
    }
    private void OnSelTeamTitle(object renderData)
    {
        var data = renderData as AchiveTeamTitle;
        ShowTeam(data.moduleId, data.teamId);
        List<ItemRender> lstRender = m_teamTitleGrid.ItemRenders;
        foreach(ItemTeamTitle render in lstRender )
        {
            if(render != null)
            {
                if(render.data.moduleId == data.moduleId && render.data.teamId == data.teamId)
                {
                    render.SetNameColorWhite(true);
                }
                else
                {
                    render.SetNameColorWhite(false);
                }
            }
        }
    }

    private void AddXBadge( int num )
    {
        for(int i = 0; i < num; ++i)
        {
            AchieveBadge acBadge = new AchieveBadge();
            acBadge.badge = "x_badge";
            m_lstBadges.Add(acBadge);
        }
    }

    protected void OnClickTest(GameObject target, PointerEventData eventData)
    {
       
    }

    public void UpdateView()
    {
        UpdateModuleBubble();
        if (m_curModule == "overview")
            ShowOverview();
        else
        {
            var config = ConfigManager.GetConfig<ConfigAchieveTagName>();
            for (int i = 0; i < config.m_dataArr.Length; ++i)
            {
                ConfigAchieveTagNameLine line = config.m_dataArr[i];
                if ("schedule_"+line.AchieveClassTitle == m_curModule)
                {
                    ShowTeam(line.Tag,m_curTeamId);
                    return;
                }
            }
        }
    }

    public bool ModuleIsOn()
    {
        if (m_goModule.activeSelf)
        {
            return true;
        }
        return false;
    }

    public void goBack()
    {
        m_goModule.TrySetActive(false);
        m_goOverView.TrySetActive(true);
        ShowOverview();
    }

    void Toc_player_achieve_update(toc_player_achieve_update data)
    {
        UpdateView();
    }

    protected void UpdateModuleBubble()
    {
        for (int module = (int)Achieve_Module.AM_WEAPON; module < 6; module++)
        {
            GameObject goBubble = null;
            if (module == (int)Achieve_Module.AM_WEAPON)
                goBubble = m_weaponbubble;
            else if (module == (int)Achieve_Module.AM_MAP)
                goBubble = m_mapbubble;
            //else if (module == (int)Achieve_Module.AM_COMPETITION)
            //    goBubble = m_goCompetitionBubble;
            //else if (module == (int)Achieve_Module.AM_CHALLENGE)
            //    goBubble = m_goChallengeBubble;
            else if (module == (int)Achieve_Module.AM_CAREER)
                goBubble = m_careerbubble;

            if (goBubble == null)
                continue;

            AchieveModuleData moduleData = PlayerAchieveData.GetModuleData(module);
            if (moduleData == null)
                goBubble.TrySetActive(false);
            else
            {
                if (moduleData.un_get_num <= 0)
                    goBubble.TrySetActive(false);
                else
                    goBubble.TrySetActive(true);
            }
        }     
    }
}
