﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemTeamTitle : ItemRender
{
    private AchiveTeamTitle m_data;
    private Text m_txtName;
    //private Text m_txtProcess;
    //private Image m_imgBadge;
    private GameObject m_goBubble;
    public override void Awake()
    {
        m_txtName = transform.Find("name").GetComponent<Text>();
        //m_txtProcess = transform.Find("zd_process").GetComponent<Text>();
        //m_imgBadge = transform.Find("badge").GetComponent<Image>();
        m_goBubble = transform.Find("bubble_tip").gameObject;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as AchiveTeamTitle;
        m_txtName.text = m_data.name;
        var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(m_data.badge);
        //if(lineBadge != null)
        //{
        //    m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
        //    m_imgBadge.SetNativeSize();
        //}
            

        UpdateView();
        //float fProcess = (float)m_data.gettedPoint / (float)m_data.sumPoint;
        //int iProcess = (int)(fProcess * 100f);
        //m_txtProcess.text = iProcess.ToString() + "%";
    }

    public void SetNameColorWhite( bool bWhite )
    {
        if(bWhite)
        {
            m_txtName.color = new Color32(255, 255, 255, 255);
            //m_txtProcess.color = new Color32(255, 255, 255, 255);
        }
        else
        {
            m_txtName.color = new Color32(255, 255, 255, 255);
            //m_txtName.color = new Color32(165, 77, 12, 255);
            //m_txtProcess.color = new Color32(165, 77, 12, 255);
        }
    }

    public AchiveTeamTitle data
    {
        get { return m_data; }
    }

    public int GetTeamId()
    {
        return m_data.teamId;
    }

    public void UpdateView()
    {
        float fProcess = (float)m_data.gettedPoint / (float)m_data.sumPoint;
        int iProcess = (int)(fProcess * 100f);
        //m_txtProcess.text = iProcess.ToString() + "%";
        if (m_data.un_get_num > 0)
            m_goBubble.TrySetActive(true);
        else
            m_goBubble.TrySetActive(false);
    }
}
