﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PanelGetAchieveReward : BasePanel
{
    private int m_iAchieveId;
    private GameObject m_goChenghao;
    private Image m_imgChenghao;
    private GameObject m_goHead;
    private Image m_imgHead;
    private GameObject m_goGp;
    private Text m_txtGp;
    private GameObject m_goBadge;
    private Image m_imgBadge;
    private UIEffect m_gongxiEffect;
    public PanelGetAchieveReward()
    {
        SetPanelPrefabPath("UI/Achievement/PanelGetAchieveReward");
        AddPreLoadRes("UI/Achievement/PanelGetAchieveReward", EResType.UI);
       

        AddPreLoadRes("Atlas/Achievement", EResType.Atlas);
    }

    public override void Init()
    {
        m_goChenghao = m_tran.Find("click_screen/reward/chenghao").gameObject;
        m_imgChenghao = m_tran.Find("click_screen/reward/chenghao/icon").GetComponent<Image>();
        m_goHead = m_tran.Find("click_screen/reward/head").gameObject;
        m_imgHead = m_tran.Find("click_screen/reward/head/icon").GetComponent<Image>();
        m_goBadge = m_tran.Find("click_screen/reward/badge").gameObject;
        m_imgBadge = m_tran.Find("click_screen/reward/badge/icon").GetComponent<Image>();
        m_goGp = m_tran.Find("click_screen/reward/gp").gameObject;
        m_txtGp = m_tran.Find("click_screen/reward/gp/num").GetComponent<Text>();
    }

     public override void Update()
    {
    }

     public override void InitEvent()
     {
         UGUIClickHandler.Get(m_tran.Find("click_screen").gameObject).onPointerClick += OnClkContinue;
     }

     public override void OnShow()
     {
         if (!HasParams())
             return;

         m_iAchieveId = (int)m_params[0];
         var config = ConfigManager.GetConfig<ConfigAchievement>();
         ConfigAchievementLine line = config.GetLine(m_iAchieveId);
         if (line == null)
             return;

         m_goChenghao.TrySetActive(false);
         m_goHead.TrySetActive(false);
         m_goBadge.TrySetActive(false);
         m_goGp.TrySetActive(true);

         var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();

         if(line.AchieveType == (int)Achieve_Type.AT_NORMAL)
         {
             m_txtGp.text = "X" + line.Point;
         }
         else if(line.AchieveType == (int)Achieve_Type.AT_HAS_CHENGHAO)
         {
             m_goChenghao.TrySetActive(true);
             ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(line.AchieveTitle);
             if (lineChenghao != null)
             {
                 m_imgChenghao.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
                 m_imgChenghao.SetNativeSize();
             }
                 
         }
         else if(line.AchieveType == (int)Achieve_Type.AT_HAS_HEAD)
         {
             m_goHead.TrySetActive(true);
             m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(line.AchieveIcon));
             m_imgHead.SetNativeSize();
         }
         else if(line.AchieveType == (int)Achieve_Type.AT_TEAM)
         {
             m_goBadge.TrySetActive(true);
             ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(line.Badge);
             if (lineBadge != null)
             {
                 m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
                 m_imgBadge.SetNativeSize();
             }
         }


         if (m_gongxiEffect == null)
         {
             m_gongxiEffect = UIEffect.ShowEffect(m_tran, EffectConst.UI_GONGXI_DACHENG_CHENGJIU, new Vector2(22, 138));
         }

         AudioManager.PlayUISound(AudioConst.lotteryGetNbWeapon);
     }

     public override void OnHide()
     {
         if (m_gongxiEffect != null)
         {
             m_gongxiEffect.Destroy();
             m_gongxiEffect = null;
         }
     }

     public override void OnBack()
     {

     }

     public override void OnDestroy()
     {
     }

     protected void OnClkContinue(GameObject target, PointerEventData eventData)
     {
         HidePanel();
     }
}
