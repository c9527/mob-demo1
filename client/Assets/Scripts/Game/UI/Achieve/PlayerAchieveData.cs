﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Achieve_Stat
{
    AS_UN_START = 0,        //> 未开始
    AS_IN_PROGRESS = 1,     //> 进行中
    AS_UN_GET = 2,          //> 完成未领取
    AS_GETTED = 3           //> 完成并领取
}

public enum Achieve_Type
{
    AT_NORMAL = 1,              //> 普通成就
    AT_HAS_CHENGHAO = 2,        //> 含有称号的成就
    AT_HAS_HEAD = 3,            //> 含有头像的成就
    AT_TEAM = 4,                //> 一个组的总结成就
}

public enum Achieve_Module
{
    AM_WEAPON = 1,              //> 武器模块   
    AM_MAP = 2,                 //> 地图模块
    AM_COMPETITION = 3,         //> 竞技模块
    AM_CHALLENGE = 4,           //> 挑战模块
    AM_CAREER = 5,              //> 生涯模块
    AM_MAX
}

public class AchieveModuleData
{
    public int un_get_num;          //> 已成功未领取的成就数目
}

public class AchiveTeamTitle
{
    public int moduleId;            //> 模块id
    public int teamId;              //> 组id
    public string name;             //> 成就组名称
    public int badge;               //> 徽章
    public int gettedPoint;         //> 已经领取的成就点
    public int sumPoint;            //> 总的成就点
    public int un_get_num;          //> 已成功未领取的成就数目
}

public class AchivementData
{
    public int id;                          //> 成就id
    public string name;                     //> 成就名称
    public Achieve_Type achieveType;        //> 成就类型
    public string desc;                     //> 成就描述
    public int finishedCount;               //> 已经完成的成就条件值
    public int count;                       //> 成就条件值
    public int gp;                          //> 成就点
    public int achieveTitle;                //> 成就称号
    public int achieveIcon;                 //> 成就头像
    public int preAchieveId;                //> 前置成就id
    public Achieve_Stat stat;               //> 成就状态
    public int badge;                       //> 徽章
}

public class AchieveBadge
{
    public string badge;
}
public class PlayerAchieveData
{
    public static Dictionary<int, p_achievement> m_dicAchievement = new Dictionary<int, p_achievement>();
    public static List<int> m_lstBadge = new List<int>();
    public static List<int> m_lstChenghao = new List<int>();
    public static List<int> m_lstHeadIcon = new List<int>();
    public static int m_achievePoint = 0;

    public static Dictionary<int, Dictionary<int, List<AchivementData>>> m_dicAchievementData = new Dictionary<int, Dictionary<int, List<AchivementData>>>();
    public static Dictionary<int, List<AchiveTeamTitle>> m_dicAchieveTeamTitle = new Dictionary<int, List<AchiveTeamTitle>>();
    public static Dictionary<int, AchieveModuleData> m_dicModuleData = new Dictionary<int, AchieveModuleData>();

    public static int m_sumAchievePoint = 0;                      //> 总的成就Point值
    public static Dictionary<int, int> m_dicSumModulePoint = new Dictionary<int, int>();   //> 各个模块总的Point值
    public static int m_sumGetAchievePoint;                   //> 已经领取的成就Point值
    public static Dictionary<int, int> m_dicSumModuleGetPoint = new Dictionary<int, int>();//> 各个模块已经领取的成就Point值
    public static int MAX_BADGE_IN_ROW = 6;

    public static Dictionary<int, List<int>> m_dicChenghaoByModule = new Dictionary<int, List<int>>();
    public static int m_rewardNum = 0;
    public static void RequestAchievement()
    {
        NetLayer.Send(new tos_player_achievement_data());
    }
    private static void Toc_player_achievement_data(toc_player_achievement_data proto)
    {
        m_lstBadge.Clear();
        m_lstBadge.AddRange(proto.achieve_badge_list);
        m_lstChenghao.Clear();
        m_lstChenghao.AddRange(proto.achieve_chenghao_list);
        m_lstHeadIcon.Clear();
        m_lstHeadIcon.AddRange(proto.achieve_head_icon_list);

        m_dicAchievement.Clear();
        for (int i = 0; i < proto.achieve_list.Length; ++i)
        {
            p_achievement achieve = new p_achievement();
            achieve.id = proto.achieve_list[i].id;
            achieve.val = proto.achieve_list[i].val;
            achieve.stat = proto.achieve_list[i].stat;
            m_dicAchievement[achieve.id] = achieve;
            if (proto.achieve_list[i].stat == (int)Achieve_Stat.AS_UN_GET)
                m_rewardNum++;
        }

        GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);
        m_achievePoint = proto.achieve_point;

        InitAchieveData();
    }

    protected static void InitAchieveData()
    {
        m_dicAchievementData.Clear();
        m_dicAchieveTeamTitle.Clear();
        m_dicChenghaoByModule.Clear();
        m_dicModuleData.Clear();

        var config = ConfigManager.GetConfig<ConfigAchievement>();
        if (config == null)
            return;
        for (int i = 0; i < config.m_dataArr.Length; ++i)
        {
            ConfigAchievementLine line = config.m_dataArr[i];

            m_sumAchievePoint += line.Point;
            if (m_dicSumModulePoint.ContainsKey(line.ModuleTag))
            {
                m_dicSumModulePoint[line.ModuleTag] = m_dicSumModulePoint[line.ModuleTag] + line.Point;
            }
            else
            {
                m_dicSumModulePoint[line.ModuleTag] = line.Point;
            }
            AchiveTeamTitle teamTitle = GetTeamTitle(line.ModuleTag, line.TeamTag);
            if (teamTitle == null)
            {
                teamTitle = new AchiveTeamTitle();
                teamTitle.moduleId = line.ModuleTag;
                teamTitle.teamId = line.TeamTag;
                teamTitle.name = line.TeamTagName;
                teamTitle.gettedPoint = 0;
                teamTitle.sumPoint = 0;
                teamTitle.un_get_num = 0;
                AddTeamTitle(line.ModuleTag, teamTitle);
            }

            if (line.AchieveType == (int)Achieve_Type.AT_TEAM)
            {
                teamTitle.badge = line.Badge;
            }
            teamTitle.sumPoint += line.Point;


            if (line.PreAchieve != 0)
            {
                p_achievement syncPreAchieve = PlayerAchieveData.GetSyncAchieve(line.PreAchieve);
                if (syncPreAchieve == null || syncPreAchieve.stat != (int)Achieve_Stat.AS_GETTED)
                    continue;
            }

            AchivementData achieveData = new AchivementData();
            achieveData.id = line.ID;
            achieveData.name = line.Name;
            achieveData.achieveType = (Achieve_Type)line.AchieveType;
            achieveData.desc = line.Desc;
            achieveData.count = line.Count;
            achieveData.gp = line.Point;
            achieveData.achieveTitle = line.AchieveTitle;
            achieveData.achieveIcon = line.AchieveIcon;
            achieveData.preAchieveId = line.PreAchieve;
            achieveData.badge = line.Badge;

            p_achievement syncAchieve = PlayerAchieveData.GetSyncAchieve(achieveData.id);
            if (syncAchieve != null)
            {
                achieveData.finishedCount = syncAchieve.val;
                achieveData.stat = (Achieve_Stat)syncAchieve.stat;
                if (achieveData.stat == Achieve_Stat.AS_GETTED)
                {
                    teamTitle.gettedPoint += line.Point;
                    m_sumGetAchievePoint += line.Point;
                    if (m_dicSumModuleGetPoint.ContainsKey(line.ModuleTag))
                        m_dicSumModuleGetPoint[line.ModuleTag] += line.Point;
                    else
                        m_dicSumModuleGetPoint[line.ModuleTag] = line.Point;

                    if (line.AchieveType == (int)Achieve_Type.AT_HAS_CHENGHAO)
                    {
                        if (!m_dicChenghaoByModule.ContainsKey(line.ModuleTag))
                            m_dicChenghaoByModule[line.ModuleTag] = new List<int>();

                        List<int> lstChenghao = m_dicChenghaoByModule[line.ModuleTag];
                        lstChenghao.Add(line.AchieveTitle);
                    }
                }
                else if (achieveData.stat == Achieve_Stat.AS_UN_GET)
                {
                    teamTitle.un_get_num++;
                    AchieveModuleData moduleData = GetModuleData(line.ModuleTag);
                    moduleData.un_get_num++;
                }
            }
            else
            {
                achieveData.finishedCount = 0;
                achieveData.stat = Achieve_Stat.AS_UN_START;
            }

            Dictionary<int, List<AchivementData>> moduleAchieveData = null;
            if (!m_dicAchievementData.ContainsKey(line.ModuleTag))
            {
                m_dicAchievementData[line.ModuleTag] = new Dictionary<int, List<AchivementData>>();
            }

            moduleAchieveData = m_dicAchievementData[line.ModuleTag];
            List<AchivementData> teamAchieveData = null;
            if (!moduleAchieveData.ContainsKey(line.TeamTag))
            {
                moduleAchieveData[line.TeamTag] = new List<AchivementData>();
            }
            teamAchieveData = moduleAchieveData[line.TeamTag];

            teamAchieveData.Add(achieveData);
        }
    }

    public static AchieveModuleData GetModuleData(int module)
    {
        if (!m_dicModuleData.ContainsKey(module))
        {
            m_dicModuleData[module] = new AchieveModuleData();
            m_dicModuleData[module].un_get_num = 0;
        }

        return m_dicModuleData[module];
    }
    protected static AchiveTeamTitle GetTeamTitle(int module, int team)
    {
        if (m_dicAchieveTeamTitle.ContainsKey(module))
        {
            List<AchiveTeamTitle> lstTeamTitle = m_dicAchieveTeamTitle[module];
            foreach (AchiveTeamTitle teamTitle in lstTeamTitle)
            {
                if (teamTitle.teamId == team)
                    return teamTitle;
            }
        }
        return null;
    }

    protected static void AddTeamTitle(int module, AchiveTeamTitle teamTitle)
    {
        List<AchiveTeamTitle> lstTeamTitle = null;
        if (m_dicAchieveTeamTitle.ContainsKey(module))
            lstTeamTitle = m_dicAchieveTeamTitle[module];
        else
        {
            lstTeamTitle = new List<AchiveTeamTitle>();
            m_dicAchieveTeamTitle[module] = lstTeamTitle;
        }
        lstTeamTitle.Add(teamTitle);
    }

    protected static void UpdateAchievement(p_achievement achieve)
    {
        var config = ConfigManager.GetConfig<ConfigAchievement>();
        ConfigAchievementLine line_up = config.GetLine(achieve.id);
        if (line_up == null)
            return;

        if (!m_dicAchievementData.ContainsKey(line_up.ModuleTag))
            return;

        AchiveTeamTitle teamTitle = GetTeamTitle(line_up.ModuleTag, line_up.TeamTag);

        Dictionary<int, List<AchivementData>> moduleAchieveData = m_dicAchievementData[line_up.ModuleTag];
        //moduleAchieveData = m_dicAchievementData[line_up.ModuleTag];

        if (!moduleAchieveData.ContainsKey(line_up.TeamTag))
            return;
        List<AchivementData> teamAchieveData = moduleAchieveData[line_up.TeamTag];

        if (achieve.stat != (int)Achieve_Stat.AS_GETTED)
        {
            if (achieve.stat == (int)Achieve_Stat.AS_UN_GET)
            {
                teamTitle.un_get_num++;
                AchieveModuleData moduleData = GetModuleData(line_up.ModuleTag);
                moduleData.un_get_num++;
            }

            foreach (AchivementData item in (teamAchieveData))
            {
                if (item.id == achieve.id)
                {
                    item.stat = (Achieve_Stat)achieve.stat;
                    item.finishedCount = achieve.val;
                }
            }
        }
        else
        {
            m_sumGetAchievePoint += line_up.Point;
            if (teamTitle != null)
            {
                teamTitle.gettedPoint += line_up.Point;
                teamTitle.un_get_num--;

                AchieveModuleData moduleData = GetModuleData(line_up.ModuleTag);
                moduleData.un_get_num--;
            }

            if (m_dicSumModuleGetPoint.ContainsKey(line_up.ModuleTag))
                m_dicSumModuleGetPoint[line_up.ModuleTag] += line_up.Point;
            else
                m_dicSumModuleGetPoint[line_up.ModuleTag] = line_up.Point;

            if (line_up.AchieveType == (int)Achieve_Type.AT_HAS_CHENGHAO)
            {
                if (!m_dicChenghaoByModule.ContainsKey(line_up.ModuleTag))
                    m_dicChenghaoByModule[line_up.ModuleTag] = new List<int>();

                m_dicChenghaoByModule[line_up.ModuleTag].Add(line_up.AchieveTitle);
            }

            teamAchieveData.Clear();
            for (int i = 0; i < config.m_dataArr.Length; ++i)
            {
                ConfigAchievementLine line_achieve = config.m_dataArr[i];
                if (line_achieve.ModuleTag != line_up.ModuleTag || line_achieve.TeamTag != line_up.TeamTag)
                    continue;

                if (line_achieve.PreAchieve != 0)
                {
                    p_achievement syncPreAchieve = GetSyncAchieve(line_achieve.PreAchieve);
                    if (syncPreAchieve == null || syncPreAchieve.stat != (int)Achieve_Stat.AS_GETTED)
                        continue;
                }

                AchivementData achieveData = new AchivementData();
                achieveData.id = line_achieve.ID;
                achieveData.name = line_achieve.Name;
                achieveData.achieveType = (Achieve_Type)line_achieve.AchieveType;
                achieveData.desc = line_achieve.Desc;
                achieveData.count = line_achieve.Count;
                achieveData.gp = line_achieve.Point;
                achieveData.achieveTitle = line_achieve.AchieveTitle;
                achieveData.achieveIcon = line_achieve.AchieveIcon;
                achieveData.preAchieveId = line_achieve.PreAchieve;
                achieveData.badge = line_achieve.Badge;

                p_achievement syncAchieve = GetSyncAchieve(achieveData.id);
                if (syncAchieve != null)
                {
                    achieveData.finishedCount = syncAchieve.val;
                    achieveData.stat = (Achieve_Stat)syncAchieve.stat;
                }
                else
                {
                    achieveData.finishedCount = 0;
                    achieveData.stat = Achieve_Stat.AS_UN_START;
                }
                teamAchieveData.Add(achieveData);
            }
        }

    }

    private static void Toc_player_achieve_update(toc_player_achieve_update proto)
    {
        p_achievement achieve = proto.achieve;

        bool bChange = true;
        if (achieve.stat == (int)Achieve_Stat.AS_UN_GET)
            m_rewardNum++;
        else if (achieve.stat == (int)Achieve_Stat.AS_GETTED)
        {
            m_rewardNum--;
            if (m_rewardNum < 0)
                m_rewardNum = 0;
        }
        else
            bChange = false;

        if (bChange)
            GameDispatcher.Dispatch(GameEvent.PROXY_DAILY_MISSION_DATA_UPDATED);

        if (m_dicAchievement.ContainsKey(achieve.id))
        {
            m_dicAchievement[achieve.id].stat = achieve.stat;
            m_dicAchievement[achieve.id].val = achieve.val;
        }
        else
        {
            p_achievement newAchieve = new p_achievement();
            newAchieve.id = achieve.id;
            newAchieve.val = achieve.val;
            newAchieve.stat = achieve.stat;
            m_dicAchievement[newAchieve.id] = newAchieve;
        }

        UpdateAchievement(achieve);

        PanelAchievement panel = UIManager.GetPanel<PanelAchievement>();
        if (panel != null && panel.IsOpen())
        {
            panel.UpdateView();
        }

        if (achieve.stat == (int)Achieve_Stat.AS_GETTED)
        {
            UIManager.PopPanel<PanelGetAchieveReward>(new object[] { achieve.id }, false, null);
        }
        else if (achieve.stat == (int)Achieve_Stat.AS_UN_GET)
        {
            if (UIManager.IsOpen<PanelTipAchieve>())
            {
                UIManager.GetPanel<PanelTipAchieve>().showTips(achieve.id);
            }
            else
                UIManager.PopPanel<PanelTipAchieve>(new object[] { achieve.id }, false, null, LayerType.Tip);
        }

        ConfigAchievementLine config = ConfigManager.GetConfig<ConfigAchievement>().GetLine(proto.achieve.id);
        if (config.AchieveIcon != 0 && achieve.stat == (int)Achieve_Stat.AS_GETTED)
        {
            RequestAchievement();
        }

    }

    public static p_achievement GetSyncAchieve(int id)
    {
        if (m_dicAchievement.ContainsKey(id))
        {
            return m_dicAchievement[id];
        }
        else
            return null;
    }

    private static void Toc_player_achieve_add(toc_player_achieve_add proto)
    {
        for (int i = 0; i < proto.item_add_list.Length; ++i)
        {
            p_achieve_add_item item = proto.item_add_list[i];
            if (item.tag == "achieve_point")
            {
                m_achievePoint += item.val;
            }
            else if (item.tag == "chenghao")
            {
                m_lstChenghao.Add(item.val);
                var config = ConfigManager.GetConfig<ConfigAchievement>();

                for (int j = 0; j < config.m_dataArr.Length; ++j)
                {

                }
            }
            else if (item.tag == "head")
            {
                m_lstHeadIcon.Add(item.val);
            }
            else if (item.tag == "badge")
            {
                m_lstBadge.Add(item.val);
            }
        }


        PanelAchievement panel = UIManager.GetPanel<PanelAchievement>();
        if (panel != null && panel.IsOpen())
            panel.UpdateView();
    }

    private static void Toc_player_get_achieve_reward(toc_player_get_achieve_reward proto)
    {
        var config = ConfigManager.GetConfig<ConfigAchievement>();
        ConfigAchievementLine lineGet = config.GetLine(proto.achieve_id);
        if (lineGet == null)
            return;

        m_achievePoint += lineGet.Point;
        if (lineGet.AchieveType == (int)Achieve_Type.AT_HAS_CHENGHAO)
        {
            m_lstChenghao.Add(lineGet.AchieveTitle);
        }
        else if (lineGet.AchieveType == (int)Achieve_Type.AT_HAS_HEAD)
        {
            m_lstHeadIcon.Add(lineGet.AchieveIcon);
        }
        else if (lineGet.AchieveType == (int)Achieve_Type.AT_TEAM)
        {
            m_lstBadge.Add(lineGet.Badge);
        }

        PanelAchievement panel = UIManager.GetPanel<PanelAchievement>();
        if (panel != null && panel.IsOpen())
            panel.UpdateView();
    }

}