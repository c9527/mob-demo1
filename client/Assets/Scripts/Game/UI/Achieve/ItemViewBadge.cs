﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemViewBadge : ItemRender
{
    private ViewBadgeData m_data;
    private Image m_imgBadge;
    public override void Awake()
    {
        m_imgBadge = transform.FindChild("icon").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as ViewBadgeData;

        var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(m_data.badge);
        if (lineBadge != null)
        {
            m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
            m_imgBadge.SetNativeSize();
        }
    }

}
