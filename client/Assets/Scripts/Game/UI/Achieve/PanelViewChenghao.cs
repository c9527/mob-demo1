﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
public class PanelViewChenghao : BasePanel
{
    //private GameObject m_goModuleNameProto;
    private GameObject m_goChenghaoProto;
    private GameObject m_goModuleProto;
    private Transform m_tranData;
    private static int MODULE_NAME_HEIGHT = 100;
    private int m_selChenghao;

    public PanelViewChenghao()
    {
        SetPanelPrefabPath("UI/Achievement/PanelViewChenghao");
        AddPreLoadRes("UI/Achievement/PanelViewChenghao", EResType.UI);
        
        AddPreLoadRes("Atlas/Achievement", EResType.Atlas);
    }

    public override void Init()
    {
        m_selChenghao = 0;
        //m_goModuleNameProto = m_tran.Find("module_name").gameObject;
        m_goChenghaoProto = m_tran.Find("chenghao").gameObject;
        m_tranData = m_tran.Find("all_data/data").transform;
        m_goModuleProto = m_tran.Find("all_data/data/module").gameObject;
        //m_goChenghaoProto = m_tran.Find("all_data/data/module/content/chenghao").gameObject;
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("ok").gameObject).onPointerClick += OnClkOk;
    }

    public override void OnShow()
    {
        if (!HasParams())
            return;

        int curChenghao = (int) m_params[0];
        foreach (Transform tran in m_tranData)
        {
            if (tran.gameObject == m_goModuleProto)
                continue;

            Object.Destroy(tran.gameObject);
        }

        var cfgAchieveTagName = ConfigManager.GetConfig<ConfigAchieveTagName>();
        var cfgAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();

        int colNum = 0;
        float yModuleBegin = 0f;
        int dataHeight = 0;
        for( int i = 1; i < (int)Achieve_Module.AM_MAX;++i )
        {
            if (!PlayerAchieveData.m_dicChenghaoByModule.ContainsKey(i))
                continue;
            List<int> lstChenghao = PlayerAchieveData.m_dicChenghaoByModule[i];
            if (lstChenghao.Count == 0)
                continue;

            GameObject goModule = UIHelper.Instantiate(m_goModuleProto) as GameObject;
            Vector2 anchorPos = goModule.GetComponent<RectTransform>().anchoredPosition;
           
            if (colNum != 0)
            {
                anchorPos.y = yModuleBegin - ( MODULE_NAME_HEIGHT + 10 * (3 * colNum - 1) );
            }
            yModuleBegin = anchorPos.y;

            goModule.GetComponent<RectTransform>().anchoredPosition = anchorPos;

            goModule.TrySetActive(true);
            ConfigAchieveTagNameLine acTagLine = cfgAchieveTagName.GetLine(i);
            goModule.name = acTagLine.AchieveClassTitle;  
            goModule.transform.FindChild("module_name/name").GetComponent<Text>().text = acTagLine.AchieveClassName;
            goModule.transform.SetParent(m_tranData, false);

            Transform tranContent = goModule.transform.FindChild( "content" );
            GridLayoutGroup grid = goModule.transform.FindChild("content").GetComponent<GridLayoutGroup>();
            grid.enabled = true;

            for( int j = 0; j < lstChenghao.Count; ++j)
            {
                GameObject goChenghao = UIHelper.Instantiate(m_goChenghaoProto) as GameObject;
                goChenghao.transform.SetParent(tranContent, false);
                goChenghao.TrySetActive(true);
                ConfigAchievementIconLine lineChenghao = cfgAchieveIcon.GetLine(lstChenghao[j]);
                if( lineChenghao != null )
                {
                    goChenghao.transform.FindChild("icon").GetComponent<Image>().SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
                    goChenghao.transform.FindChild("icon").GetComponent<Image>().SetNativeSize();
                    goChenghao.transform.FindChild("id").GetComponent<Text>().text = lstChenghao[j].ToString();
                }

                if (curChenghao == lstChenghao[j])
                    goChenghao.GetComponent<Toggle>().isOn = true;

                UGUIClickHandler.Get(goChenghao).onPointerClick += OnChenghaoClick;
            }

            colNum = lstChenghao.Count / 4;
            if (lstChenghao.Count % 4 != 0)
                colNum++;

            dataHeight += ( MODULE_NAME_HEIGHT + 10 * (3 * colNum - 1) );
        }

        Vector2 dataSize = m_tran.Find("all_data/data").GetComponent<RectTransform>().sizeDelta;
        dataSize.y = (float)dataHeight;
        m_tran.Find("all_data/data").GetComponent<RectTransform>().sizeDelta = dataSize;

    }

    public override void OnHide()
    {

    }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }

    private void OnChenghaoClick(GameObject target, PointerEventData eventData)
    {
        m_selChenghao = int.Parse( target.transform.FindChild("id").GetComponent<Text>().text );
    }

    protected void OnClkOk(GameObject target, PointerEventData eventData)
    {
        if( m_selChenghao != 0)
        {
            NetLayer.Send(new tos_player_set_chenghao() { chenghao = m_selChenghao });
            HidePanel();
        }
        else
        {
            TipsManager.Instance.showTips("请选择一个称号!");
        }  
    }
}
