﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemAchieveBadge : ItemRender
{
    private AchieveBadge m_data;
    private Image m_imgBadge;

    public override void Awake()
    {
        m_imgBadge = transform.Find("badge").GetComponent<Image>();
    }

    protected override void OnSetData(object data)
    {
        m_data = data as AchieveBadge;
        m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, m_data.badge));
        m_imgBadge.SetNativeSize();
    }

}
