﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemAchievement : ItemRender
{
    private AchivementData m_data;
    private Text m_txtName;

    private GameObject m_lvl;
    private Text m_level;
    private Image m_icon;
    private GameObject m_goGet;
    private GameObject m_bubble;
    //private Button m_btnGet;
    //private Text m_txtGet;
    //private GameObject m_goGetted;

    //private GameObject m_goBtnCompletion;
    //private GameObject m_goBtnSpecial;

    //private GameObject m_goCondition;
    //private Text m_txtCondition;

    //private GameObject m_goSpecial;
    //private GameObject m_goHead;
    //private Image m_imgHead;
    //private GameObject m_goBadge;
    //private Image m_imgBadge;
    //private GameObject m_goChenghao;
    //private Image m_imgChenghao;
    //private Image m_imgProcess;
    //private Text m_txtProcess;

    public override void Awake()
    {
        m_txtName = transform.Find("name").GetComponent<Text>();
        m_lvl = transform.Find("lvl").gameObject;
        m_level = transform.Find("lvl/Text").GetComponent<Text>();
        m_icon = transform.Find("icon").GetComponent<Image>();
        m_bubble = transform.Find("bubble").gameObject;
        m_bubble.SetActive(false);
        //m_goGet = transform.Find("btn_get").gameObject;
        //m_btnGet = transform.Find("btn_get").GetComponent<Button>();
        //m_txtGet = transform.Find("btn_get/Label").GetComponent<Text>();
        //m_goGetted = transform.Find("btn_getted").gameObject;

        //m_goBtnCompletion = transform.Find("btn_completion").gameObject;
        //m_goBtnSpecial = transform.Find("btn_special").gameObject;

        //m_goCondition = transform.Find("condition").gameObject;
        //m_txtCondition = transform.Find("condition").GetComponent<Text>();

        //m_goSpecial = transform.Find("special").gameObject;
        //m_goHead = transform.Find("special/head").gameObject;
        //m_imgHead = transform.Find("special/head/head").GetComponent<Image>();

        //m_goBadge = transform.Find("special/badge").gameObject;
        //m_imgBadge = transform.Find("special/badge").GetComponent<Image>();

        //m_goChenghao = transform.Find("special/chenghao").gameObject;
        //m_imgChenghao = transform.Find("special/chenghao/chenghao").GetComponent<Image>();

        //m_txtProcess = transform.Find("schedule/process_num").GetComponent<Text>();
        //m_imgProcess = transform.Find("schedule/process").GetComponent<Image>();

        //UGUIClickHandler.Get(m_goGet).onPointerClick += OnClickGet;
        //UGUIClickHandler.Get(m_goBtnCompletion).onPointerClick += OnClickCompletion;
        //UGUIClickHandler.Get(m_goBtnSpecial).onPointerClick += OnClickSpecial;
        //UGUIClickHandler.Get(transform.Find("test").gameObject).onPointerClick += OnGMFinishAchievement;
    }

    protected override void OnSetData(object data)
    {
        m_data = data as AchivementData;

        m_txtName.text = m_data.name;
        var names = m_data.name.Split('·');
        if (m_data.name.Contains("级"))
        {
            m_lvl.SetActive(true);
            if (names.Length >= 2)
            {
                m_level.text = names[1];
            }
            m_txtName.text = names[0];
        }
        else
        {
            m_lvl.SetActive(false);
        }

        if (m_data.stat == Achieve_Stat.AS_GETTED)
        {
            Util.SetGoGrayShader(transform.gameObject);
        }
        else
        {
            Util.SetNormalShader(transform);
        }
        var cfg = ConfigManager.GetConfig<ConfigAchievement>();
        var info = cfg.GetLine(m_data.id);
        var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();
        ConfigAchievementIconLine lineBadge1 = configAchieveIcon.GetLine(m_data.badge);



        if (lineBadge1 != null)
        {
            m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge1.Name));
            m_icon.SetNativeSize();
        }
        else
        {
            var list = PlayerAchieveData.m_dicAchieveTeamTitle[info.ModuleTag];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].teamId == info.TeamTag)
                {
                    ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(list[i].badge);
                    if (lineBadge != null)
                    {
                        m_icon.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
                        m_icon.SetNativeSize();
                    }
                }
            }
        }

        if (m_data.stat == Achieve_Stat.AS_UN_GET)
        {
            m_bubble.SetActive(true);
        }
        else
        {
            m_bubble.SetActive(false);
        }



        //m_txtCondition.text = m_data.desc;
        //if(m_data.stat == Achieve_Stat.AS_UN_START || m_data.stat == Achieve_Stat.AS_IN_PROGRESS)
        //{
        //    m_goGet.TrySetActive(true);
        //    m_goGetted.TrySetActive(false);
        //    Util.SetGrayShader(m_btnGet);
        //    m_txtGet.text = "未达成";
        //}
        //else if (m_data.stat == Achieve_Stat.AS_UN_GET)
        //{
        //    m_goGet.TrySetActive(true);
        //    m_goGetted.TrySetActive(false);
        //    Util.SetNormalShader(m_btnGet);
        //    m_txtGet.text = "可领取";
        //}
        //else if(m_data.stat == Achieve_Stat.AS_GETTED)
        //{
        //    m_goGet.TrySetActive(false);
        //    m_goGetted.TrySetActive(true);
        //}

        //if(m_data.achieveType == Achieve_Type.AT_NORMAL)
        //{
        //    m_goBtnCompletion.TrySetActive(false);
        //    m_goBtnSpecial.TrySetActive(false);
        //    m_goCondition.TrySetActive(true);
        //    m_goSpecial.TrySetActive(false);
        //}
        //else
        //{
        //    m_goBtnCompletion.TrySetActive(false);
        //    m_goBtnSpecial.TrySetActive(true);
        //    m_goCondition.TrySetActive(true);

        //    var configAchieveIcon = ConfigManager.GetConfig<ConfigAchievementIcon>();

        //    if(m_data.achieveType == Achieve_Type.AT_HAS_HEAD)
        //    {
        //        m_goHead.TrySetActive(true);
        //        m_goChenghao.TrySetActive(false);
        //        m_goBadge.TrySetActive(false);
        //        m_imgHead.SetSprite(ResourceManager.LoadRoleIcon(m_data.achieveIcon));
        //        m_imgHead.SetNativeSize();
        //    }
        //    else if(m_data.achieveType == Achieve_Type.AT_HAS_CHENGHAO)
        //    {
        //        m_goHead.TrySetActive(false);
        //        m_goChenghao.TrySetActive(true);
        //        m_goBadge.TrySetActive(false);
                
        //        ConfigAchievementIconLine lineChenghao = configAchieveIcon.GetLine(m_data.achieveTitle);
        //        if(lineChenghao != null)
        //        {
        //            m_imgChenghao.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineChenghao.Name));
        //            m_imgChenghao.SetNativeSize();
        //        }
                    
        //    }
        //    else if(m_data.achieveType == Achieve_Type.AT_TEAM)
        //    {
        //        m_goHead.TrySetActive(false);
        //        m_goChenghao.TrySetActive(false);
        //        m_goBadge.TrySetActive(true);
        //        ConfigAchievementIconLine lineBadge = configAchieveIcon.GetLine(m_data.badge);
        //        if(lineBadge != null)
        //        {
        //            m_imgBadge.SetSprite(ResourceManager.LoadSprite(AtlasName.Achievement, lineBadge.Name));
        //            m_imgBadge.SetNativeSize();
        //        }
        //    }
        //}

        //m_txtProcess.text = m_data.finishedCount.ToString() + "/" + m_data.count.ToString();
        //m_imgProcess.fillAmount = (float)m_data.finishedCount / (float)m_data.count;
    }

    protected void OnClickCompletion(GameObject target, PointerEventData eventData)
    {
        //m_goBtnCompletion.TrySetActive(false);
        //m_goBtnSpecial.TrySetActive(true);

        //m_goCondition.TrySetActive(true);
        //m_goSpecial.TrySetActive(false);
    }

    protected void OnClickSpecial(GameObject target, PointerEventData eventData)
    {
        //m_goBtnCompletion.TrySetActive(true);
        //m_goBtnSpecial.TrySetActive(false);

        //m_goCondition.TrySetActive(false);
        //m_goSpecial.TrySetActive(true);
    }

    protected void OnClickGet(GameObject target, PointerEventData eventData)
    {
        if (m_data.stat == Achieve_Stat.AS_UN_GET)
        {
            tos_player_get_achieve_reward getMsg = new tos_player_get_achieve_reward();
            getMsg.achieve_id = m_data.id;
            NetLayer.Send(getMsg);
        }
    }

    protected void OnGMFinishAchievement(GameObject target, PointerEventData eventData)
    {
        //NetLayer.Send(new tos_player_achieve_test() { achieve_id = m_data.id });
    }
}
