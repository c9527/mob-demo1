﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ViewBadgeData
{
    public int badge;
}
public class PanelViewBadge : BasePanel
{
    private DataGrid m_badgeGrid;
    private List<ViewBadgeData> m_lstBadge;
    public PanelViewBadge()
    {
        SetPanelPrefabPath("UI/Achievement/PanelViewBadge");
        AddPreLoadRes("UI/Achievement/PanelViewBadge", EResType.UI);
        AddPreLoadRes("UI/Achievement/ItemViewBadge", EResType.UI);

        AddPreLoadRes("Atlas/Achievement", EResType.Atlas);
    }

    public override void Init()
    {
        m_badgeGrid = m_tran.Find("badges/content").gameObject.AddComponent<DataGrid>();
        m_badgeGrid.SetItemRender(ResourceManager.LoadUIRef("UI/Achievement/ItemViewBadge"), typeof(ItemViewBadge));

        m_lstBadge = new List<ViewBadgeData>();
    }


    public override void Update()
    {
    }

    public override void InitEvent()
    {
        UGUIClickHandler.Get(m_tran.Find("close").gameObject).onPointerClick += OnClkClose;
        UGUIClickHandler.Get(m_tran.Find("ok").gameObject).onPointerClick += OnClkClose;
    }

    public override void OnShow()
    {
        m_lstBadge.Clear();
        List<int> lstBadge = null;
        if (HasParams())
            lstBadge = (List<int>)m_params[0];
        else
            lstBadge = PlayerAchieveData.m_lstBadge;

        for (int i = 0; i < lstBadge.Count; ++i)
        {
            ViewBadgeData badgeData = new ViewBadgeData();
            badgeData.badge = lstBadge[i];
            m_lstBadge.Add(badgeData);
        }
        m_badgeGrid.Data = m_lstBadge.ToArray();
    }

     public override void OnHide()
     {

     }

    public override void OnBack()
    {

    }

    public override void OnDestroy()
    {
    }

    protected void OnClkClose(GameObject target, PointerEventData eventData)
    {
        HidePanel();
    }
}
