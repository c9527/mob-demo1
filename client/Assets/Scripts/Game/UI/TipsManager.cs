﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class TweenParams
{
    public float time = 0;
    public RectTransform target = null;
    public float duration = 0;
    public Vector4 param_pos = Vector4.zero;
    public Vector2 param_scale = Vector2.zero;
    public Vector2 param_rotate = Vector2.zero;
    public Vector2 param_alpha = Vector2.zero;
    public Action<TipsTweener> onComplete;
}

class PosAndScale
{
    public Vector4 pos;
    public Vector2 scale;
}

class TipsManager : MonoBehaviour
{
    static TipsManager _instance;

    static int _MAX_PLAYING_NUM = 2;
    static float _MIN_REPEAT_INTERVAL = 0.5f;
    static float _MIN_INTERVAL = 0.75f;
    const int _DEFALUT_MAX_LEFT_COUNT = 3;

    float _last_time = 0f;
    float _last_play_time = 0f;
    int _playing_num;
    List<string> _waiting_list = new List<string>();
    Dictionary<string, PosAndScale> _posAndScaleDic = new Dictionary<string, PosAndScale>();

    Stack<GameObject> _object_pool = new Stack<GameObject>();

    TipsTweener _lastTween;
    int _last_max_left_count = 0;

    public static TipsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameObject("TipsManager").AddComponent<TipsManager>();
                _instance.gameObject.AddComponent<RectTransform>();
                _instance.gameObject.AddComponent<CanvasRenderer>();
                _instance.gameObject.AddComponent<SortingOrderRenderer>();
                _instance.gameObject.layer = LayerMask.NameToLayer("UI");
                _instance.transform.SetParent(UIManager.GetLayer(LayerType.Tip));
                _instance.transform.localScale = Vector3.one;
                _instance.transform.localPosition = Vector3.zero;
                SortingOrderRenderer.RebuildAll();
            }
            return _instance;
        }
    }

    public void showTips(string content, Vector4 pos = default(Vector4), Vector2 scale = default(Vector2), int maxLeftCount = _DEFALUT_MAX_LEFT_COUNT)
    {
        if (string.IsNullOrEmpty(content))
            return;
        bool ignore = false;
        _last_max_left_count = maxLeftCount;
        int waitCount = _waiting_list.Count;
        if (waitCount > 0 && _waiting_list[waitCount - 1] == content && (Time.time - _last_time) < _MIN_REPEAT_INTERVAL)
        {
            ignore = true;
        }
        if (!ignore)
        {
            _last_time = Time.time;
            _waiting_list.Add(content);
            if (pos != default(Vector4) || scale != default(Vector2))
            {
                if (!_posAndScaleDic.ContainsKey(content)) _posAndScaleDic.Add(content, new PosAndScale() { pos = pos, scale = scale });
                else _posAndScaleDic[content] = new PosAndScale() { pos = pos, scale = scale };
            }
            bool force = waitCount > _last_max_left_count;
            if (force && _lastTween != null)
                _lastTween.Finish();
            playNext(force);
        }
    }

    private void playNext(bool force = false)
    {
        if (!force && (Time.time - _last_play_time) < _MIN_INTERVAL)
        {
            return;
        }

        int waitCount = _waiting_list.Count;

        if (force || (_playing_num < _MAX_PLAYING_NUM && waitCount > 0))
        {
            if (waitCount - 1 <= _last_max_left_count)
                _last_max_left_count = _DEFALUT_MAX_LEFT_COUNT;

            _last_play_time = Time.time;
            _playing_num++;
            var str = _waiting_list[0];
            _waiting_list.RemoveAt(0);

            var trans = genTipsObject(str);

            var tween_pos = new Vector4(0, 50, 0, 100);
            var tween_scale = new Vector2(0, 1);
            if(_posAndScaleDic.ContainsKey(str))
            {
                Vector4 pos = _posAndScaleDic[str].pos;
                Vector2 scale = _posAndScaleDic[str].scale;
                if (pos != default(Vector4))
                {
                    tween_pos.x = pos.x;
                    tween_pos.y = pos.y;
                    tween_pos.z = pos.z;
                    tween_pos.w = pos.w;
                }
                if (scale != default(Vector2))
                {
                    tween_scale.x = scale.x;
                    tween_scale.y = scale.y;
                }
                _posAndScaleDic.Remove(str);
            }

            _lastTween = trans.gameObject.AddMissingComponent<TipsTweener>();
            _lastTween.setData(new TweenParams() { time = Time.time, target = trans, duration = 1f, param_pos = tween_pos, param_scale = tween_scale, onComplete = onTweenComplete });
        }
    }

    private void onTweenComplete(TipsTweener tween)
    {
        tween.gameObject.TrySetActive(false);
        if (_object_pool.Count < 5)
        {
            _object_pool.Push(tween.gameObject);
        }
        else
        {
            GameObject.Destroy(tween.gameObject);
        }
        _playing_num--;

        playNext(_waiting_list.Count > _last_max_left_count);
    }

    private RectTransform genTipsObject(string content)
    {
        var go = _object_pool.Count > 0 ? _object_pool.Pop() as GameObject : null;
        Text tip_txt = null;
        Image tip_bg = null;
        if (go == null)
        {
            go = new GameObject("tips_txt");
            tip_bg = go.AddComponent<Image>();
            tip_bg.gameObject.layer = LayerMask.NameToLayer("UI");
            tip_bg.rectTransform.SetParent(_instance.transform);
            tip_bg.rectTransform.SetAsFirstSibling();
            tip_bg.rectTransform.localScale = Vector3.one;
            tip_bg.rectTransform.localPosition = Vector3.zero;
            tip_bg.SetSprite(ResourceManager.LoadSprite(AtlasName.Common, "view_bg_4"));
            tip_bg.type = Image.Type.Sliced;
            tip_txt = new GameObject("Text").AddComponent<Text>();
            tip_txt.rectTransform.SetParent(tip_bg.rectTransform);
            tip_txt.rectTransform.localScale = Vector3.one;
            tip_txt.rectTransform.localPosition = new Vector3(-2,0,0);
            tip_txt.font = ResourceManager.LoadFont();
            tip_txt.fontSize = 22;
            tip_txt.alignment = TextAnchor.MiddleCenter;
            //tip_txt.fontStyle = FontStyle.Bold;
        }
        else
        {
            go.TrySetActive(true);
            tip_bg = go.transform.GetComponent<Image>();
            tip_txt = go.transform.GetComponentInChildren<Text>();
        }
        tip_txt.rectTransform.parent.gameObject.TrySetActive(true);
        tip_txt.text = content;
        var size = new Vector2(Math.Max(tip_txt.preferredWidth, 160), 30);
        tip_txt.rectTransform.sizeDelta = size;
        if(size.y<tip_txt.preferredHeight)
        {
            size = new Vector2(size.x,Math.Max(tip_txt.preferredHeight,30));
            tip_txt.rectTransform.sizeDelta = size;
        }
        size.x += 8;
        size.y += 8;
        tip_bg.rectTransform.sizeDelta = size;
        return go.transform as RectTransform;
    }
}

class TipsTweener : MonoBehaviour
{
    TweenParams _data;

    int _step = -1;

    Image _bg;
    Text _txt;

    public void setData(TweenParams data)
    {
        _data = data;

        _step = 0;
        _bg = transform.GetComponent<Image>();
        _txt = transform.GetComponentInChildren<Text>();
        _bg.color = new Color(1, 1, 1, 1);
        _txt.color = new Color(1, 1, 1, 1);
        if (_data != null && transform != null)
        {
            transform.localScale = new Vector2(_data.param_scale.x, _data.param_scale.x);
            transform.localPosition = new Vector2(_data.param_pos.x, _data.param_pos.y);
        }
    }

    void Update()
    {
        if (_step < 0 || _data==null)
        {
            return;
        }
        float d_t = Time.time - _data.time;
        if (_step == 0)//scale 0->1
        {
            Vector3 scale = Vector3.one;
            scale.y = scale.x = EaseMethod.UpdateByEaseOut(d_t, 0.5f, _data.param_scale.y, _data.param_scale.x);
            transform.localScale = scale;
            if (d_t >= 0.5)
            {
                _step = 1;
            }
        }
        else if (_step == 1)//move pos by define && alpha 1->0
        {
            d_t -= 0.5f;
            Vector2 pos = Vector2.zero;
            pos.x = EaseMethod.UpdateByEase(d_t, _data.duration, _data.param_pos.z - _data.param_pos.x, _data.param_pos.x);
            pos.y = EaseMethod.UpdateByEase(d_t, _data.duration, _data.param_pos.w - _data.param_pos.y, _data.param_pos.y);
            transform.localPosition = pos;
            if (d_t >= _data.duration / 2)
            {
                float alpha = EaseMethod.UpdateByEase(d_t - _data.duration / 2, _data.duration / 2, -1, 1);
                var temp_color = _bg.color;
                temp_color.a = alpha;
                _bg.color = temp_color;
                temp_color = _txt.color;
                temp_color.a = alpha;
                _txt.color = temp_color;
            }
            if (d_t >= _data.duration)
            {
                _step = -1;
            }
        }
        if(_step==-1)
        {
            if (_data.onComplete != null)
            {
                _data.onComplete.Invoke(this);
            }
        }
    }

    public void Finish()
    {
        _step = -1;
        if (_data.onComplete != null)
        {
            _data.onComplete.Invoke(this);
        }
    }
}

