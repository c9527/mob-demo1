﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

static class WeaponDisplay
{
    private static Camera m_weaponCamera;
    private static Transform m_tranWeapon;
    private static RenderTexture m_renderTexture;
    private static RenderTexture m_renderTextureBig;
    private static float m_rotationCenter = 2000.12f; //模型旋转的中心
    private static ConfigItemWeaponLine m_configWeapon;
    private static bool m_visible;

    public static RenderTexture RenderTexture
    {
        get
        {
            Init();
            if (m_renderTexture == null)
                m_renderTexture = new RenderTexture(512, 300, 16);
            m_weaponCamera.targetTexture = m_renderTexture;
            return m_renderTexture;
        }
    }

    public static RenderTexture RenderTextureBig
    {
        get
        {
            Init();
            if (m_renderTextureBig == null)
                m_renderTextureBig = new RenderTexture(700, 400, 16);
            m_weaponCamera.targetTexture = m_renderTextureBig;
            return m_renderTextureBig;
        }
    }

    public static void Init()
    {
        if (m_weaponCamera)
            return;
        m_weaponCamera = new GameObject("WeaponCamera").AddComponent<Camera>();
        m_weaponCamera.orthographic = true;
        m_weaponCamera.orthographicSize = 0.5f;
        m_weaponCamera.fieldOfView = 35;
        m_weaponCamera.farClipPlane = 5;
        m_weaponCamera.transform.SetLocation(null, m_rotationCenter, 0f, 1.4f);
        m_weaponCamera.transform.localRotation = Quaternion.Euler(0, 180f, 0);
    }

    public static bool Visible
    {
        set
        {
            Init();
            m_visible = value;
            if (m_weaponCamera)
                m_weaponCamera.enabled = value;
        }
        get { return m_visible; }
    }

    public static void UpdateMode(ConfigItemWeaponLine configWeapon)
    {
        m_configWeapon = configWeapon;
        if (m_configWeapon == null)
            return;
        string weaponModel = m_configWeapon.TujianModel.IsNullOrEmpty() ? m_configWeapon.MPModel : m_configWeapon.TujianModel;
        var weaponModelChanged = m_tranWeapon == null || m_tranWeapon.name != weaponModel;

        if (!weaponModelChanged) 
            return;
        ResourceManager.LoadModelRef("Weapons/" + weaponModel, (resWeapon) =>
        {
            if (WorldManager.singleton != null && WorldManager.singleton.fightEntered)
                return;

            if (m_tranWeapon)
                Object.DestroyImmediate(m_tranWeapon.gameObject);

            m_weaponCamera.orthographicSize = Mathf.Approximately(m_configWeapon.DetailSize, 0) ? 0.5f : m_configWeapon.DetailSize;
            m_tranWeapon = (Object.Instantiate(resWeapon) as GameObject).transform;
            m_tranWeapon.name = resWeapon.name;
            m_tranWeapon.localPosition = m_configWeapon.DetailPos == Vector3.zero ? new Vector3(2000, 0, 0) : m_configWeapon.DetailPos;
            m_tranWeapon.localRotation = Quaternion.Euler(m_configWeapon.DetailRotation == Vector3.zero ? new Vector3(0, 90, 0) : m_configWeapon.DetailRotation);

            // 排除特效节点
            ExtendFuncHelper.CallBackBool<GameObject> funVisit = (GameObject child) =>
            {
                string name = child.name.ToLower();
                if (name.ToLower().StartsWith(ModelConst.WEAPON_EFFECT))
                    return false;

                Renderer render = child.GetComponent<Renderer>();
                if (render != null)
                {
                    Material newMat = null;
                    Material oldMat = render.material;

                    if (m_tranWeapon.name.IndexOf("Crystal", StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        newMat = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON_CRYSTAL);
                        if (ResourceManager.m_loadMode == ELoadMode.AssetBundle)
                            newMat = Object.Instantiate(newMat) as Material;
                    }
                    else
                    {
                        newMat = ResourceManager.LoadMaterial(GameConst.MAT_TUJIAN_WEAPON);
                        if (ResourceManager.m_loadMode == ELoadMode.AssetBundle)
                            newMat = Object.Instantiate(newMat) as Material;

                        newMat.CopyPropTexFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWTEX);
                        newMat.CopyPropColorFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWTEX_COLOR);
                        newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWDIR_X);
                        newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWDIR_Y);
                        newMat.CopyPropFloatFrom(oldMat, GameConst.SHADER_PROPERTY_FLOWSPEED);

                        ConfigItemWeaponLine wl = GlobalConfig.configWeapon.GetLine(m_configWeapon.ID);
                        if (wl != null && wl.FlowTexColor.Length == 3)
                        {
                            Color flowColor = new Color(wl.FlowTexColor[0] / 255, wl.FlowTexColor[1] / 255, wl.FlowTexColor[2] / 255);
                            if (flowColor != Color.white && newMat.HasProperty(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR))
                            {
                                newMat.SetColor(GameConst.SHADER_PROPERTY_FLOWTEX_COLOR, flowColor);
                            }
                        }
                    }

                    newMat.mainTexture = oldMat.mainTexture;
                    render.material = newMat;
                }
                return true;
            };

            m_tranWeapon.VisitChild(funVisit, true);

        }, false);
    }

    public static void Rotation(float dx)
    {
        if (m_tranWeapon)
            m_tranWeapon.RotateAround(new Vector3(m_rotationCenter, 0, 0), Vector3.up, -dx * 0.3f);
    }

    public static void Destroy()
    {
        if (m_weaponCamera)
            Object.Destroy(m_weaponCamera.gameObject);
        if (m_tranWeapon)
            Object.Destroy(m_tranWeapon.gameObject);
    }
}