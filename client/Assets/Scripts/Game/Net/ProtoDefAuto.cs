using System.IO;

static partial class NetLayer
{
  static NetLayer()
  {
    mCreater[0x10001] = () => new toc_login_verify_old();
    mCreater[0x10002] = () => new toc_login_create();
    mCreater[0x10003] = () => new toc_login_select();
    mCreater[0x10004] = () => new toc_login_ping();
    mCreater[0x10005] = () => new toc_login_verify();
    mCreater[0x10006] = () => new toc_login_random_name();

    mCreater[0x20001] = () => new toc_player_exe_lua();
    mCreater[0x20002] = () => new toc_player_view_player();
    mCreater[0x20003] = () => new toc_player_err_msg();
    mCreater[0x20004] = () => new toc_player_item_equip_data();
    mCreater[0x20005] = () => new toc_player_buy_item();
    mCreater[0x20006] = () => new toc_player_equip_item();
    mCreater[0x20007] = () => new toc_player_buy_equip_bag();
    mCreater[0x20008] = () => new toc_player_set_default_role();
    mCreater[0x20009] = () => new toc_player_equip_bag();
    mCreater[0x2000a] = () => new toc_player_chat();
    mCreater[0x2000b] = () => new toc_player_chat_msg();
    mCreater[0x2000c] = () => new toc_player_audio();
    mCreater[0x2000d] = () => new toc_player_rank_list();
    mCreater[0x2000e] = () => new toc_player_notice();
    mCreater[0x2000f] = () => new toc_player_weapon_strengthen_data();
    mCreater[0x20010] = () => new toc_player_weapon_train_info();
    mCreater[0x20011] = () => new toc_player_start_weapon_train();
    mCreater[0x20012] = () => new toc_player_stop_weapon_train();
    mCreater[0x20013] = () => new toc_player_weapon_awaken();
    mCreater[0x20014] = () => new toc_player_upgrade_weapon_strengthen_state();
    mCreater[0x20015] = () => new toc_player_win_stage();
    mCreater[0x20016] = () => new toc_player_mail_list();
    mCreater[0x20018] = () => new toc_player_del_mail();
    mCreater[0x20019] = () => new toc_player_new_mail();
    mCreater[0x2001b] = () => new toc_player_send_mail_gift();
    mCreater[0x2001c] = () => new toc_player_use_exchange_code();
    mCreater[0x2001d] = () => new toc_player_my_ranks();
    mCreater[0x2001e] = () => new toc_player_friend_ranklist();
    mCreater[0x2001f] = () => new toc_player_ranklist_reward();
    mCreater[0x20020] = () => new toc_player_room_invitelist();
    mCreater[0x20021] = () => new toc_player_signin_data();
    mCreater[0x20022] = () => new toc_player_sign_in();
    mCreater[0x20023] = () => new toc_player_welfare_data();
    mCreater[0x20024] = () => new toc_player_get_welfare_reward();
    mCreater[0x20025] = () => new toc_player_gm();
    mCreater[0x20026] = () => new toc_player_weapon_strengthen_info();
    mCreater[0x20027] = () => new toc_player_set_icon();
    mCreater[0x20028] = () => new toc_player_game_reward();
    mCreater[0x20029] = () => new toc_player_double_game_reward();
    mCreater[0x2002a] = () => new toc_player_default_fight_roles();
    mCreater[0x2002c] = () => new toc_player_critical_msg();
    mCreater[0x2002f] = () => new toc_player_rename();
    mCreater[0x20030] = () => new toc_player_use_item();
    mCreater[0x20031] = () => new toc_player_buff_list();
    mCreater[0x20032] = () => new toc_player_compound();
    mCreater[0x20033] = () => new toc_player_room_list();
    mCreater[0x20034] = () => new toc_player_test_accessory_meterial();
    mCreater[0x20035] = () => new toc_player_equip_weapon_accessory();
    mCreater[0x20036] = () => new toc_player_unequip_weapon_accessory();
    mCreater[0x20037] = () => new toc_player_buyback_weapon_accessory();
    mCreater[0x20038] = () => new toc_player_first_recharge_welfare();
    mCreater[0x20039] = () => new toc_player_first_recharge_data();
    mCreater[0x2003b] = () => new toc_player_recharge_welfare_list();
    mCreater[0x2003c] = () => new toc_player_exchange_list();
    mCreater[0x2003d] = () => new toc_player_exchange();
    mCreater[0x2003e] = () => new toc_player_exchange_num();
    mCreater[0x20040] = () => new toc_player_recharge_info();
    mCreater[0x20041] = () => new toc_player_recharge_associator_info();
    mCreater[0x20042] = () => new toc_player_recharge_switch();
    mCreater[0x20043] = () => new toc_player_lottery();
    mCreater[0x20044] = () => new toc_player_get_lottery_rank();
    mCreater[0x20045] = () => new toc_player_lottery_message();
    mCreater[0x20046] = () => new toc_player_new_lottery();
    mCreater[0x20047] = () => new toc_player_new_exchange_lottery();
    mCreater[0x20048] = () => new toc_player_channel_list();
    mCreater[0x20049] = () => new toc_player_exchange_points();
    mCreater[0x2004a] = () => new toc_player_event_list();
    mCreater[0x2004c] = () => new toc_player_cash_gain_info();
    mCreater[0x2004d] = () => new toc_player_friend_rcmnd_list();
    mCreater[0x2004e] = () => new toc_player_friend_data();
    mCreater[0x2004f] = () => new toc_player_friend_msg();
    mCreater[0x20052] = () => new toc_player_add_friend();
    mCreater[0x20053] = () => new toc_player_del_friend();
    mCreater[0x20054] = () => new toc_player_agree_fan();
    mCreater[0x20055] = () => new toc_player_refuse_fan();
    mCreater[0x20056] = () => new toc_player_add_black();
    mCreater[0x20057] = () => new toc_player_del_black();
    mCreater[0x20058] = () => new toc_player_getdata();
    mCreater[0x20059] = () => new toc_player_repair_weapons();
    mCreater[0x2005a] = () => new toc_player_game_reward_times();
    mCreater[0x2005b] = () => new toc_player_blackmarket_notice();
    mCreater[0x2005c] = () => new toc_player_blackmarket_list();
    mCreater[0x2005e] = () => new toc_player_mall_record();
    mCreater[0x20060] = () => new toc_player_get_zone_info();
    mCreater[0x20061] = () => new toc_player_edit_zone();
    mCreater[0x20062] = () => new toc_player_achievement_data();
    mCreater[0x20063] = () => new toc_player_achieve_update();
    mCreater[0x20064] = () => new toc_player_achieve_add();
    mCreater[0x20065] = () => new toc_player_get_achieve_reward();
    mCreater[0x20066] = () => new toc_player_set_chenghao();
    mCreater[0x20067] = () => new toc_player_trigger_guide();
    mCreater[0x20068] = () => new toc_player_server_value();
    mCreater[0x20069] = () => new toc_player_survival_data();
    mCreater[0x2006a] = () => new toc_player_eventlist();
    mCreater[0x2006b] = () => new toc_player_take_event_reward();
    mCreater[0x2006c] = () => new toc_player_client_value();
    mCreater[0x2006e] = () => new toc_player_mission_data();
    mCreater[0x2006f] = () => new toc_player_modify_mission();
    mCreater[0x20070] = () => new toc_player_get_mission_reward();
    mCreater[0x20071] = () => new toc_player_offline_chat_msg();
    mCreater[0x20073] = () => new toc_player_is_oldplayer();
    mCreater[0x20075] = () => new toc_player_get_oldplayer_gift_info();
    mCreater[0x20076] = () => new toc_player_use_giftbag();
    mCreater[0x20078] = () => new toc_player_stage_survival_data();
    mCreater[0x20079] = () => new toc_player_stage_star();
    mCreater[0x2007b] = () => new toc_player_game_time_today();
    mCreater[0x2007c] = () => new toc_player_anti_addiction();
    mCreater[0x2007e] = () => new toc_player_options();
    mCreater[0x20083] = () => new toc_player_tag_list();
    mCreater[0x20084] = () => new toc_player_update_tag();
    mCreater[0x20086] = () => new toc_player_item_transform();
    mCreater[0x20087] = () => new toc_player_flop_reward();
    mCreater[0x20088] = () => new toc_player_new_lottery_show();
    mCreater[0x20089] = () => new toc_player_close_agent();
    mCreater[0x2008a] = () => new toc_player_hero_card_data();
    mCreater[0x2008b] = () => new toc_player_take_hero_card_reward();
    mCreater[0x2008c] = () => new toc_player_set_titles();
    mCreater[0x2008d] = () => new toc_player_subtype_room_list();
    mCreater[0x2008e] = () => new toc_player_diamond2eventcoin_002();
    mCreater[0x2008f] = () => new toc_player_available_channel();
    mCreater[0x20090] = () => new toc_player_timelimit_gift();
    mCreater[0x20091] = () => new toc_player_timelimit_gift_buy();
    mCreater[0x20092] = () => new toc_player_timelimit_gift_time();
    mCreater[0x20093] = () => new toc_player_foundation_data();
    mCreater[0x20094] = () => new toc_player_get_foundation_reward();
    mCreater[0x20095] = () => new toc_player_set_hero_sex();
    mCreater[0x20096] = () => new toc_player_weapon_accessory_data();
    mCreater[0x20097] = () => new toc_player_weapon_accessories();
    mCreater[0x20098] = () => new toc_player_phased_purchase_data();
    mCreater[0x20099] = () => new toc_player_phased_item_update();
    mCreater[0x2009a] = () => new toc_player_phased_item_new();
    mCreater[0x2009b] = () => new toc_player_phased_item_del();
    mCreater[0x2009c] = () => new toc_player_unequip_decoration();
    mCreater[0x2009d] = () => new toc_player_icon_url_data();
    mCreater[0x2009e] = () => new toc_player_add_icon_url();
    mCreater[0x2009f] = () => new toc_player_set_icon_url();
    mCreater[0x200a0] = () => new toc_player_del_icon_url();
    mCreater[0x200a1] = () => new toc_player_match_punish_data();
    mCreater[0x200a3] = () => new toc_player_social_join_media();
    mCreater[0x200a5] = () => new toc_player_social_invitor_name();
    mCreater[0x200a6] = () => new toc_player_social_bind_invite_code();
    mCreater[0x200a7] = () => new toc_player_phone_register();
    mCreater[0x200a8] = () => new toc_player_phone_verify();
    mCreater[0x200a9] = () => new toc_player_phone_code();
    mCreater[0x200aa] = () => new toc_player_phone_info();
    mCreater[0x200ab] = () => new toc_player_social_invite_friend();

    mCreater[0x30001] = () => new toc_item_new();
    mCreater[0x30002] = () => new toc_item_update();
    mCreater[0x30003] = () => new toc_item_del();

    mCreater[0x40001] = () => new toc_fight_base();
    mCreater[0x40002] = () => new toc_fight_msg();
    mCreater[0x40003] = () => new toc_fight_leave();
    mCreater[0x40004] = () => new toc_fight_move();
    mCreater[0x40006] = () => new toc_fight_equip_info();
    mCreater[0x40007] = () => new toc_fight_fire();
    mCreater[0x40009] = () => new toc_fight_del_drop_item();
    mCreater[0x4000a] = () => new toc_fight_add_drop_item();
    mCreater[0x4000b] = () => new toc_fight_drop_list();
    mCreater[0x4000c] = () => new toc_fight_start_set_bomb();
    mCreater[0x4000d] = () => new toc_fight_stop_set_bomb();
    mCreater[0x4000e] = () => new toc_fight_set_bomb();
    mCreater[0x4000f] = () => new toc_fight_start_defuse_bomb();
    mCreater[0x40010] = () => new toc_fight_stop_defuse_bomb();
    mCreater[0x40011] = () => new toc_fight_defuse_bomb();
    mCreater[0x40012] = () => new toc_fight_bomb_burst();
    mCreater[0x40014] = () => new toc_fight_atk();
    mCreater[0x40015] = () => new toc_fight_reload();
    mCreater[0x40017] = () => new toc_fight_jump();
    mCreater[0x40018] = () => new toc_fight_stand();
    mCreater[0x40019] = () => new toc_fight_crouch();
    mCreater[0x4001a] = () => new toc_fight_fixbullet();
    mCreater[0x4001b] = () => new toc_fight_enter();
    mCreater[0x4001c] = () => new toc_fight_game_info();
    mCreater[0x4001d] = () => new toc_fight_join();
    mCreater[0x4001e] = () => new toc_fight_actorprop();
    mCreater[0x4001f] = () => new toc_fight_actorstate();
    mCreater[0x40020] = () => new toc_fight_use_skill();
    mCreater[0x40021] = () => new toc_fight_maul_heavily();
    mCreater[0x40022] = () => new toc_fight_actor_die();
    mCreater[0x40023] = () => new toc_fight_state();
    mCreater[0x40024] = () => new toc_fight_stat_info();
    mCreater[0x40025] = () => new toc_fight_chat();
    mCreater[0x40026] = () => new toc_fight_hit();
    mCreater[0x40028] = () => new toc_fight_join_spectator();
    mCreater[0x40029] = () => new toc_fight_leave_spectator();
    mCreater[0x4002a] = () => new toc_fight_can_vote_kick();
    mCreater[0x4002b] = () => new toc_fight_begin_vote_kick();
    mCreater[0x4002c] = () => new toc_fight_vote_kick();
    mCreater[0x4002d] = () => new toc_fight_vote_kick_result();
    mCreater[0x4002e] = () => new toc_fight_start_rescue();
    mCreater[0x4002f] = () => new toc_fight_stop_rescue();
    mCreater[0x40030] = () => new toc_fight_achieve();
    mCreater[0x40031] = () => new toc_fight_actor_rescued();
    mCreater[0x40032] = () => new toc_fight_survival_use_revivecoin();
    mCreater[0x40033] = () => new toc_fight_survival_revive();
    mCreater[0x40034] = () => new toc_fight_survival_need_revivecoin();
    mCreater[0x40035] = () => new toc_fight_survival_state();
    mCreater[0x40036] = () => new toc_fight_survival_expenditure();
    mCreater[0x40038] = () => new toc_fight_hide_state();
    mCreater[0x40039] = () => new toc_fight_select_disguise();
    mCreater[0x4003a] = () => new toc_fight_praise();
    mCreater[0x4003b] = () => new toc_fight_atk_wrong();
    mCreater[0x4003d] = () => new toc_fight_scene_items();
    mCreater[0x4003e] = () => new toc_fight_total_scene_items();
    mCreater[0x4003f] = () => new toc_fight_game_result();
    mCreater[0x40040] = () => new toc_fight_throw_weapon();
    mCreater[0x40041] = () => new toc_fight_arena_begin_info();
    mCreater[0x40042] = () => new toc_fight_arena_end_info();
    mCreater[0x40044] = () => new toc_fight_bomb_info();
    mCreater[0x40045] = () => new toc_fight_score();
    mCreater[0x40046] = () => new toc_fight_king_camp();
    mCreater[0x40047] = () => new toc_fight_king_solo();
    mCreater[0x40048] = () => new toc_fight_weapon_motion();
    mCreater[0x4004a] = () => new toc_fight_game_shop();
    mCreater[0x4004c] = () => new toc_fight_defend_state();
    mCreater[0x4004d] = () => new toc_fight_weapon_awaken_skill();
    mCreater[0x4004e] = () => new toc_fight_whether2human();
    mCreater[0x4004f] = () => new toc_fight_start_zombie2human();
    mCreater[0x40050] = () => new toc_fight_select_equip();
    mCreater[0x40051] = () => new toc_fight_select_zombie();
    mCreater[0x40052] = () => new toc_fight_actor_life();
    mCreater[0x40053] = () => new toc_fight_actor_god_end();
    mCreater[0x40054] = () => new toc_fight_actor_buff_list();
    mCreater[0x40055] = () => new toc_fight_actor_pos();
    mCreater[0x40056] = () => new toc_fight_actor_fight_level();
    mCreater[0x40057] = () => new toc_fight_change_role();
    mCreater[0x40058] = () => new toc_fight_actor_immune();
    mCreater[0x40059] = () => new toc_fight_actor_type();
    mCreater[0x4005a] = () => new toc_fight_update_actor_type();
    mCreater[0x4005b] = () => new toc_fight_actor2big_head_king();
    mCreater[0x4005c] = () => new toc_fight_final_time();
    mCreater[0x4005d] = () => new toc_fight_ai_action();
    mCreater[0x4005e] = () => new toc_fight_attack_failed();
    mCreater[0x4005f] = () => new toc_fight_select_role();
    mCreater[0x40060] = () => new toc_fight_hero_time();
    mCreater[0x40061] = () => new toc_fight_ghost_jump();
    mCreater[0x40062] = () => new toc_fight_team_hero_level();
    mCreater[0x40063] = () => new toc_fight_rush_done();
    mCreater[0x40064] = () => new toc_fight_ai_skill();
    mCreater[0x40065] = () => new toc_fight_dota_camp_info();
    mCreater[0x40066] = () => new toc_fight_dota_state();
    mCreater[0x40067] = () => new toc_fight_energy();
    mCreater[0x40068] = () => new toc_fight_round_best();
    mCreater[0x4006a] = () => new toc_fight_play_animation();
    mCreater[0x4006b] = () => new toc_fight_game_start_time();
    mCreater[0x4006c] = () => new toc_fight_role_select_list();
    mCreater[0x4006d] = () => new toc_fight_record();
    mCreater[0x4006e] = () => new toc_fight_wait_time();
    mCreater[0x4006f] = () => new toc_fight_select_buff();
    mCreater[0x40071] = () => new toc_fight_fixpos();
    mCreater[0x40072] = () => new toc_fight_throw_cfour();
    mCreater[0x40073] = () => new toc_fight_occupy();
    mCreater[0x40074] = () => new toc_fight_halo_list();
    mCreater[0x40075] = () => new toc_fight_poweron();
    mCreater[0x40076] = () => new toc_fight_poweroff();
    mCreater[0x40078] = () => new toc_fight_change_spectator();
    mCreater[0x40079] = () => new toc_fight_spectator_record();


    mCreater[0x6000d] = () => new toc_room_chat_msg();
    mCreater[0x6000f] = () => new toc_room_invite();
    mCreater[0x60010] = () => new toc_room_info();
    mCreater[0x60015] = () => new toc_room_survival_leader();
    mCreater[0x60016] = () => new toc_room_leave_room();
    mCreater[0x60019] = () => new toc_room_clientstop();
    mCreater[0x6001b] = () => new toc_room_match_time();
    mCreater[0x6001c] = () => new toc_room_corpsmatch_give_up();
    mCreater[0x6001d] = () => new toc_room_notice_leave();
    mCreater[0x6001f] = () => new toc_room_match_punished();

    mCreater[0x70001] = () => new toc_corps_create_corps();
    mCreater[0x70004] = () => new toc_corps_data();
    mCreater[0x70005] = () => new toc_corps_get_list();
    mCreater[0x70006] = () => new toc_corps_apply_enter();
    mCreater[0x70007] = () => new toc_corps_agree_apply();
    mCreater[0x70008] = () => new toc_corps_refuse_apply();
    mCreater[0x70009] = () => new toc_corps_quit();
    mCreater[0x7000a] = () => new toc_corps_change_icon();
    mCreater[0x7000b] = () => new toc_corps_update_notice();
    mCreater[0x7000c] = () => new toc_corps_change_member_type();
    mCreater[0x7000d] = () => new toc_corps_set_leader();
    mCreater[0x7000e] = () => new toc_corps_kick_member();
    mCreater[0x7000f] = () => new toc_corps_dismiss();
    mCreater[0x70010] = () => new toc_corps_message();
    mCreater[0x70011] = () => new toc_corps_view_info();
    mCreater[0x70012] = () => new toc_corps_notify_apply();
    mCreater[0x70013] = () => new toc_corps_notify_members();
    mCreater[0x70014] = () => new toc_corps_notify_infos();
    mCreater[0x70015] = () => new toc_corps_notify_logs();
    mCreater[0x70016] = () => new toc_corps_donate_tribute();
    mCreater[0x70017] = () => new toc_corps_upgrade_level();
    mCreater[0x70018] = () => new toc_corps_getlotterycounts();
    mCreater[0x70019] = () => new toc_corps_openlottery();
    mCreater[0x7001a] = () => new toc_corps_uselottery();
    mCreater[0x7001b] = () => new toc_corps_buylotterybox();
    mCreater[0x7001c] = () => new toc_corps_donate_info();
    mCreater[0x7001d] = () => new toc_corps_enter_mall();
    mCreater[0x7001e] = () => new toc_corps_buymallbox();
    mCreater[0x7001f] = () => new toc_corps_mallbid();
    mCreater[0x70020] = () => new toc_corps_mission_data();
    mCreater[0x70021] = () => new toc_corps_notify_mission();
    mCreater[0x70022] = () => new toc_corps_get_activeness_reward();
    mCreater[0x70023] = () => new toc_corps_open_instance();
    mCreater[0x70026] = () => new toc_corps_recmd_list();
    mCreater[0x70027] = () => new toc_corps_get_hurt_list();
    mCreater[0x70028] = () => new toc_corps_change_name();

    mCreater[0x80001] = () => new toc_corpsmatch_score_info();
    mCreater[0x80002] = () => new toc_corpsmatch_stage();
    mCreater[0x80003] = () => new toc_corpsmatch_group_score();
    mCreater[0x80005] = () => new toc_corpsmatch_is_registered();
    mCreater[0x80006] = () => new toc_corpsmatch_register_num();
    mCreater[0x80007] = () => new toc_corpsmatch_timeline();
    mCreater[0x80008] = () => new toc_corpsmatch_vote();
    mCreater[0x80009] = () => new toc_corpsmatch_vote_result();
    mCreater[0x8000a] = () => new toc_corpsmatch_matchtable();
    mCreater[0x8000b] = () => new toc_corpsmatch_last_result();
    mCreater[0x8000c] = () => new toc_corpsmatch_match_info();
    mCreater[0x8000d] = () => new toc_corpsmatch_room();
    mCreater[0x8000e] = () => new toc_corpsmatch_record();
    mCreater[0x8000f] = () => new toc_corpsmatch_last_record();
    mCreater[0x80010] = () => new toc_corpsmatch_match_list();
    mCreater[0x80011] = () => new toc_corpsmatch_result();
    mCreater[0x80012] = () => new toc_corpsmatch_full();
    mCreater[0x80013] = () => new toc_corpsmatch_top16();

    mCreater[0x90002] = () => new toc_master_agree_apply();
    mCreater[0x90003] = () => new toc_master_apprentice_info();
    mCreater[0x90004] = () => new toc_master_kick_master();
    mCreater[0x90005] = () => new toc_master_rcmnd_master_list();
    mCreater[0x90006] = () => new toc_master_apply_master_list();
    mCreater[0x90007] = () => new toc_master_notice();
    mCreater[0x90008] = () => new toc_master_master_welfare();
    mCreater[0x90009] = () => new toc_master_get_master_welfare();
    mCreater[0x9000a] = () => new toc_master_has_welfare_apply();
    mCreater[0x9000b] = () => new toc_master_tree();
    mCreater[0x9000c] = () => new toc_master_double_exp();
    mCreater[0x9000d] = () => new toc_master_grade();
    mCreater[0x9000e] = () => new toc_master_grade_drawgif();
    mCreater[0x9000f] = () => new toc_master_grade_get_point();
    mCreater[0x90010] = () => new toc_master_grade_isdraw();
    mCreater[0x90011] = () => new toc_master_grade_has_welfare();
    mCreater[0x90012] = () => new toc_master_put_weapon_to_arsenal();
    mCreater[0x90013] = () => new toc_master_borrow_weapon();
    mCreater[0x90014] = () => new toc_master_arsenal();

    mCreater[0xa0001] = () => new toc_legion_create_legion();
    mCreater[0xa0004] = () => new toc_legion_data();
    mCreater[0xa0005] = () => new toc_legion_get_list();
    mCreater[0xa0006] = () => new toc_legion_get_member_list();
    mCreater[0xa0007] = () => new toc_legion_apply_enter();
    mCreater[0xa0008] = () => new toc_legion_agree_apply();
    mCreater[0xa0009] = () => new toc_legion_refuse_apply();
    mCreater[0xa000a] = () => new toc_legion_quit();
    mCreater[0xa000b] = () => new toc_legion_change_icon();
    mCreater[0xa000c] = () => new toc_legion_update_notice();
    mCreater[0xa000d] = () => new toc_legion_change_corps_post();
    mCreater[0xa000e] = () => new toc_legion_set_leader();
    mCreater[0xa000f] = () => new toc_legion_kick_corps();
    mCreater[0xa0010] = () => new toc_legion_dismiss();
    mCreater[0xa0011] = () => new toc_legion_message();
    mCreater[0xa0012] = () => new toc_legion_view_info();
    mCreater[0xa0013] = () => new toc_legion_notify_apply();
    mCreater[0xa0014] = () => new toc_legion_notify_corps();
    mCreater[0xa0015] = () => new toc_legion_notify_infos();
    mCreater[0xa0016] = () => new toc_legion_notify_logs();
    mCreater[0xa0017] = () => new toc_legion_enlarge_capacity();

    mCreater[0xb0001] = () => new toc_legionwar_data();
    mCreater[0xb0002] = () => new toc_legionwar_fighter_ranks();
    mCreater[0xb0003] = () => new toc_legionwar_set_mass_order();
    mCreater[0xb0004] = () => new toc_legionwar_my_info();
    mCreater[0xb0005] = () => new toc_legionwar_get_reward();
    mCreater[0xb0006] = () => new toc_legionwar_my_territory_info();
    mCreater[0xb0007] = () => new toc_legionwar_start_match();
    mCreater[0xb0008] = () => new toc_legionwar_territory_info();
    mCreater[0xb0009] = () => new toc_legionwar_legion_officers();
    mCreater[0xb000a] = () => new toc_legionwar_eminentors();
    mCreater[0xb000b] = () => new toc_legionwar_corps_ranks();

    mCreater[0xc0001] = () => new toc_family_add_family();
    mCreater[0xc0002] = () => new toc_family_agree_family();
    mCreater[0xc0003] = () => new toc_family_del_family();
    mCreater[0xc0004] = () => new toc_family_family_list();
    mCreater[0xc0005] = () => new toc_family_update_family();
    mCreater[0xc0006] = () => new toc_family_family_apply_info();
    mCreater[0xc0007] = () => new toc_family_open_family_pos();

    mCreater[0xd0001] = () => new toc_record_history_list();
    mCreater[0xd0007] = () => new toc_record_message();

    mCreater[0xe0001] = () => new toc_microblog_my_data();
    mCreater[0xe0002] = () => new toc_microblog_view_player();
    mCreater[0xe0003] = () => new toc_microblog_blog_list();
    mCreater[0xe0004] = () => new toc_microblog_feed_list();
    mCreater[0xe0005] = () => new toc_microblog_comment_list();
    mCreater[0xe0006] = () => new toc_microblog_add_blog();
    mCreater[0xe0007] = () => new toc_microblog_del_blog();
    mCreater[0xe0008] = () => new toc_microblog_praise_blog();
    mCreater[0xe0009] = () => new toc_microblog_cancel_praise();
    mCreater[0xe000a] = () => new toc_microblog_comment_blog();
    mCreater[0xe000b] = () => new toc_microblog_del_comment();
    mCreater[0xe000c] = () => new toc_microblog_add_follow();
    mCreater[0xe000d] = () => new toc_microblog_del_follow();
    mCreater[0xe000e] = () => new toc_microblog_add_black();
    mCreater[0xe000f] = () => new toc_microblog_del_black();

    Init();
  }
}

partial class toc_login_verify_old
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "verify_old"; } }
  public override string Name { get { return "toc_login_verify_old"; } }
  public override int Id { get { return 0x10001; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_login_verify_old
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "verify_old"; } }
  public override string Name { get { return "tos_login_verify_old"; } }
  public override int Id { get { return 0x10001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_login_create
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "create"; } }
  public override string Name { get { return "toc_login_create"; } }
  public override int Id { get { return 0x10002; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_login_create
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "create"; } }
  public override string Name { get { return "tos_login_create"; } }
  public override int Id { get { return 0x10002; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    bw.Write(this.sex);
    bw.Write(this.sid);
  }
}

partial class toc_login_select
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "select"; } }
  public override string Name { get { return "toc_login_select"; } }
  public override int Id { get { return 0x10003; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    RoleData cls1 = new RoleData();
    cls1.name = LoadString(br);
    cls1.level = br.ReadInt32();
    cls1.icon = br.ReadInt32();
    cls1.exp = br.ReadInt32();
    cls1.honor = br.ReadInt32();
    cls1.vip_level = br.ReadInt32();
    cls1.sex = br.ReadInt32();
    cls1.diamond = br.ReadInt32();
    cls1.coin = br.ReadInt32();
    cls1.medal = br.ReadInt32();
    cls1.cash = br.ReadInt32();
    cls1.power = br.ReadInt32();
    cls1.stage = br.ReadInt32();
    cls1.double_reward_times = br.ReadInt32();
    cls1.first_pay_time = br.ReadInt32();
    cls1.corps_coin = br.ReadInt32();
    cls1.chenghao = br.ReadInt32();
    cls1.blue_stone = br.ReadInt32();
    cls1.purple_stone = br.ReadInt32();
    cls1.white_stone = br.ReadInt32();
    cls1.eventcoin_002 = br.ReadInt32();
    cls1.active_coin = br.ReadInt32();
    cls1.last_logout_time = br.ReadInt32();
    cls1.honor_joke = br.ReadInt32();
    cls1.create_time = br.ReadInt32();
    data = cls1;
    times = br.ReadInt32();
    reserve = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_login_select
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "select"; } }
  public override string Name { get { return "tos_login_select"; } }
  public override int Id { get { return 0x10003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.reserve);
    bw.Write(this.sid);
  }
}

partial class toc_login_ping
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "ping"; } }
  public override string Name { get { return "toc_login_ping"; } }
  public override int Id { get { return 0x10004; } }

  public override void Load(BinaryReader br)
  {
    client_time = br.ReadSingle();
    server_time = br.ReadSingle();
    sid = br.ReadUInt32();
  }
}

partial class tos_login_ping
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "ping"; } }
  public override string Name { get { return "tos_login_ping"; } }
  public override int Id { get { return 0x10004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.client_time);
    SaveString(bw, CJsonHelper.Save(this.info));
    bw.Write(this.sid);
  }
}

partial class toc_login_verify
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "verify"; } }
  public override string Name { get { return "toc_login_verify"; } }
  public override int Id { get { return 0x10005; } }

  public override void Load(BinaryReader br)
  {
    uid = LoadString(br);
    int len1 = br.ReadUInt16();
    toc_login_verify.RoleInfo[] ary2 = new toc_login_verify.RoleInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_login_verify.RoleInfo cls4 = new toc_login_verify.RoleInfo();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.level = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    oauth_info = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_login_verify
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "verify"; } }
  public override string Name { get { return "tos_login_verify"; } }
  public override int Id { get { return 0x10005; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.sdkplat);
    bw.Write(this.platform);
    SaveString(bw, this.ext_info);
    SaveString(bw, this.plat_uid);
    SaveString(bw, this.account);
    SaveString(bw, CJsonHelper.Save(this.other_info));
    bw.Write(this.sid);
  }
}

partial class toc_login_random_name
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "random_name"; } }
  public override string Name { get { return "toc_login_random_name"; } }
  public override int Id { get { return 0x10006; } }

  public override void Load(BinaryReader br)
  {
    name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_login_random_name
{
  public override string Typ { get { return "login"; } }
  public override string Key { get { return "random_name"; } }
  public override string Name { get { return "tos_login_random_name"; } }
  public override int Id { get { return 0x10006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sex);
    bw.Write(this.sid);
  }
}


partial class toc_player_exe_lua
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exe_lua"; } }
  public override string Name { get { return "toc_player_exe_lua"; } }
  public override int Id { get { return 0x20001; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    code = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_exe_lua
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exe_lua"; } }
  public override string Name { get { return "tos_player_exe_lua"; } }
  public override int Id { get { return 0x20001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.ok);
    SaveString(bw, this.msg);
    bw.Write(this.sid);
  }
}

partial class toc_player_view_player
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "view_player"; } }
  public override string Name { get { return "toc_player_view_player"; } }
  public override int Id { get { return 0x20002; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    icon = br.ReadInt32();
    icon_url = LoadString(br);
    level = br.ReadInt32();
    rank_val = br.ReadInt32();
    corps_name = LoadString(br);
    vip_level = br.ReadInt32();
    sex = br.ReadInt32();
    name = LoadString(br);
    int len1 = br.ReadUInt16();
    toc_player_view_player.EquipGroup[] ary2 = new toc_player_view_player.EquipGroup[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_view_player.EquipGroup cls4 = new toc_player_view_player.EquipGroup();
      cls4.group_id = br.ReadInt32();
      toc_player_view_player.EquipInfo cls5 = new toc_player_view_player.EquipInfo();
      cls5.item_id = br.ReadInt32();
      cls5.stage = br.ReadInt32();
      cls5.time_limit = br.ReadInt32();
      int len6 = br.ReadUInt16();
      accessory[] ary7 = new accessory[len6];
      for (int idx8 = 0; idx8 < len6; idx8++)
      {
        accessory cls9 = new accessory();
        cls9.pos = br.ReadInt32();
        cls9.item_id = br.ReadInt32();
        int len10 = br.ReadUInt16();
        System.Int32[] ary11 = new System.Int32[len10];
        for (int idx12 = 0; idx12 < len10; idx12++)
        {
          ary11[idx12] = br.ReadInt32();
        }
        cls9.accessory_values = ary11;
        ary7[idx8] = cls9;
      }
      cls5.accessory_list = ary7;
      cls4.WEAPON1 = cls5;
      toc_player_view_player.EquipInfo cls13 = new toc_player_view_player.EquipInfo();
      cls13.item_id = br.ReadInt32();
      cls13.stage = br.ReadInt32();
      cls13.time_limit = br.ReadInt32();
      int len14 = br.ReadUInt16();
      accessory[] ary15 = new accessory[len14];
      for (int idx16 = 0; idx16 < len14; idx16++)
      {
        accessory cls17 = new accessory();
        cls17.pos = br.ReadInt32();
        cls17.item_id = br.ReadInt32();
        int len18 = br.ReadUInt16();
        System.Int32[] ary19 = new System.Int32[len18];
        for (int idx20 = 0; idx20 < len18; idx20++)
        {
          ary19[idx20] = br.ReadInt32();
        }
        cls17.accessory_values = ary19;
        ary15[idx16] = cls17;
      }
      cls13.accessory_list = ary15;
      cls4.WEAPON2 = cls13;
      toc_player_view_player.EquipInfo cls21 = new toc_player_view_player.EquipInfo();
      cls21.item_id = br.ReadInt32();
      cls21.stage = br.ReadInt32();
      cls21.time_limit = br.ReadInt32();
      int len22 = br.ReadUInt16();
      accessory[] ary23 = new accessory[len22];
      for (int idx24 = 0; idx24 < len22; idx24++)
      {
        accessory cls25 = new accessory();
        cls25.pos = br.ReadInt32();
        cls25.item_id = br.ReadInt32();
        int len26 = br.ReadUInt16();
        System.Int32[] ary27 = new System.Int32[len26];
        for (int idx28 = 0; idx28 < len26; idx28++)
        {
          ary27[idx28] = br.ReadInt32();
        }
        cls25.accessory_values = ary27;
        ary23[idx24] = cls25;
      }
      cls21.accessory_list = ary23;
      cls4.HELMET = cls21;
      toc_player_view_player.EquipInfo cls29 = new toc_player_view_player.EquipInfo();
      cls29.item_id = br.ReadInt32();
      cls29.stage = br.ReadInt32();
      cls29.time_limit = br.ReadInt32();
      int len30 = br.ReadUInt16();
      accessory[] ary31 = new accessory[len30];
      for (int idx32 = 0; idx32 < len30; idx32++)
      {
        accessory cls33 = new accessory();
        cls33.pos = br.ReadInt32();
        cls33.item_id = br.ReadInt32();
        int len34 = br.ReadUInt16();
        System.Int32[] ary35 = new System.Int32[len34];
        for (int idx36 = 0; idx36 < len34; idx36++)
        {
          ary35[idx36] = br.ReadInt32();
        }
        cls33.accessory_values = ary35;
        ary31[idx32] = cls33;
      }
      cls29.accessory_list = ary31;
      cls4.ARMOR = cls29;
      toc_player_view_player.EquipInfo cls37 = new toc_player_view_player.EquipInfo();
      cls37.item_id = br.ReadInt32();
      cls37.stage = br.ReadInt32();
      cls37.time_limit = br.ReadInt32();
      int len38 = br.ReadUInt16();
      accessory[] ary39 = new accessory[len38];
      for (int idx40 = 0; idx40 < len38; idx40++)
      {
        accessory cls41 = new accessory();
        cls41.pos = br.ReadInt32();
        cls41.item_id = br.ReadInt32();
        int len42 = br.ReadUInt16();
        System.Int32[] ary43 = new System.Int32[len42];
        for (int idx44 = 0; idx44 < len42; idx44++)
        {
          ary43[idx44] = br.ReadInt32();
        }
        cls41.accessory_values = ary43;
        ary39[idx40] = cls41;
      }
      cls37.accessory_list = ary39;
      cls4.BOOTS = cls37;
      toc_player_view_player.EquipInfo cls45 = new toc_player_view_player.EquipInfo();
      cls45.item_id = br.ReadInt32();
      cls45.stage = br.ReadInt32();
      cls45.time_limit = br.ReadInt32();
      int len46 = br.ReadUInt16();
      accessory[] ary47 = new accessory[len46];
      for (int idx48 = 0; idx48 < len46; idx48++)
      {
        accessory cls49 = new accessory();
        cls49.pos = br.ReadInt32();
        cls49.item_id = br.ReadInt32();
        int len50 = br.ReadUInt16();
        System.Int32[] ary51 = new System.Int32[len50];
        for (int idx52 = 0; idx52 < len50; idx52++)
        {
          ary51[idx52] = br.ReadInt32();
        }
        cls49.accessory_values = ary51;
        ary47[idx48] = cls49;
      }
      cls45.accessory_list = ary47;
      cls4.DAGGER = cls45;
      toc_player_view_player.EquipInfo cls53 = new toc_player_view_player.EquipInfo();
      cls53.item_id = br.ReadInt32();
      cls53.stage = br.ReadInt32();
      cls53.time_limit = br.ReadInt32();
      int len54 = br.ReadUInt16();
      accessory[] ary55 = new accessory[len54];
      for (int idx56 = 0; idx56 < len54; idx56++)
      {
        accessory cls57 = new accessory();
        cls57.pos = br.ReadInt32();
        cls57.item_id = br.ReadInt32();
        int len58 = br.ReadUInt16();
        System.Int32[] ary59 = new System.Int32[len58];
        for (int idx60 = 0; idx60 < len58; idx60++)
        {
          ary59[idx60] = br.ReadInt32();
        }
        cls57.accessory_values = ary59;
        ary55[idx56] = cls57;
      }
      cls53.accessory_list = ary55;
      cls4.GRENADE = cls53;
      toc_player_view_player.EquipInfo cls61 = new toc_player_view_player.EquipInfo();
      cls61.item_id = br.ReadInt32();
      cls61.stage = br.ReadInt32();
      cls61.time_limit = br.ReadInt32();
      int len62 = br.ReadUInt16();
      accessory[] ary63 = new accessory[len62];
      for (int idx64 = 0; idx64 < len62; idx64++)
      {
        accessory cls65 = new accessory();
        cls65.pos = br.ReadInt32();
        cls65.item_id = br.ReadInt32();
        int len66 = br.ReadUInt16();
        System.Int32[] ary67 = new System.Int32[len66];
        for (int idx68 = 0; idx68 < len66; idx68++)
        {
          ary67[idx68] = br.ReadInt32();
        }
        cls65.accessory_values = ary67;
        ary63[idx64] = cls65;
      }
      cls61.accessory_list = ary63;
      cls4.FLASHBOMB = cls61;
      int len69 = br.ReadUInt16();
      toc_player_view_player.Decoration[] ary70 = new toc_player_view_player.Decoration[len69];
      for (int idx71 = 0; idx71 < len69; idx71++)
      {
        toc_player_view_player.Decoration cls72 = new toc_player_view_player.Decoration();
        cls72.part = br.ReadInt32();
        cls72.item_id = br.ReadInt32();
        ary70[idx71] = cls72;
      }
      cls4.decorations = ary70;
      ary2[idx3] = cls4;
    }
    equip_groups = ary2;
    toc_player_view_player.FightStatdata cls73 = new toc_player_view_player.FightStatdata();
    cls73.kill_cnt = br.ReadInt32();
    cls73.knife_kill = br.ReadInt32();
    cls73.death_cnt = br.ReadInt32();
    cls73.headshoot = br.ReadInt32();
    cls73.fight_cnt = br.ReadInt32();
    cls73.win_cnt = br.ReadInt32();
    cls73.grenade_kill = br.ReadInt32();
    cls73.mvp_cnt = br.ReadInt32();
    cls73.gold_ace = br.ReadInt32();
    cls73.silver_ace = br.ReadInt32();
    cls73.qualifying_win = br.ReadInt32();
    cls73.qualifying_kill = br.ReadInt32();
    cls73.max_multi_kill = br.ReadInt32();
    cls73.assist_cnt = br.ReadInt32();
    int len74 = br.ReadUInt16();
    toc_player_view_player.GameStatdata[] ary75 = new toc_player_view_player.GameStatdata[len74];
    for (int idx76 = 0; idx76 < len74; idx76++)
    {
      toc_player_view_player.GameStatdata cls77 = new toc_player_view_player.GameStatdata();
      cls77.game_type = LoadString(br);
      cls77.kill_cnt = br.ReadInt32();
      cls77.death_cnt = br.ReadInt32();
      cls77.assist_cnt = br.ReadInt32();
      cls77.win_cnt = br.ReadInt32();
      cls77.fight_cnt = br.ReadInt32();
      ary75[idx76] = cls77;
    }
    cls73.game_stats = ary75;
    fight_stat = cls73;
    int len78 = br.ReadUInt16();
    toc_player_view_player.FightStatdata[] ary79 = new toc_player_view_player.FightStatdata[len78];
    for (int idx80 = 0; idx80 < len78; idx80++)
    {
      toc_player_view_player.FightStatdata cls81 = new toc_player_view_player.FightStatdata();
      cls81.kill_cnt = br.ReadInt32();
      cls81.knife_kill = br.ReadInt32();
      cls81.death_cnt = br.ReadInt32();
      cls81.headshoot = br.ReadInt32();
      cls81.fight_cnt = br.ReadInt32();
      cls81.win_cnt = br.ReadInt32();
      cls81.grenade_kill = br.ReadInt32();
      cls81.mvp_cnt = br.ReadInt32();
      cls81.gold_ace = br.ReadInt32();
      cls81.silver_ace = br.ReadInt32();
      cls81.qualifying_win = br.ReadInt32();
      cls81.qualifying_kill = br.ReadInt32();
      cls81.max_multi_kill = br.ReadInt32();
      cls81.assist_cnt = br.ReadInt32();
      int len82 = br.ReadUInt16();
      toc_player_view_player.GameStatdata[] ary83 = new toc_player_view_player.GameStatdata[len82];
      for (int idx84 = 0; idx84 < len82; idx84++)
      {
        toc_player_view_player.GameStatdata cls85 = new toc_player_view_player.GameStatdata();
        cls85.game_type = LoadString(br);
        cls85.kill_cnt = br.ReadInt32();
        cls85.death_cnt = br.ReadInt32();
        cls85.assist_cnt = br.ReadInt32();
        cls85.win_cnt = br.ReadInt32();
        cls85.fight_cnt = br.ReadInt32();
        ary83[idx84] = cls85;
      }
      cls81.game_stats = ary83;
      ary79[idx80] = cls81;
    }
    mode_stats = ary79;
    int len86 = br.ReadUInt16();
    System.Int32[] ary87 = new System.Int32[len86];
    for (int idx88 = 0; idx88 < len86; idx88++)
    {
      ary87[idx88] = br.ReadInt32();
    }
    badge_list = ary87;
    int len89 = br.ReadUInt16();
    p_title_info[] ary90 = new p_title_info[len89];
    for (int idx91 = 0; idx91 < len89; idx91++)
    {
      p_title_info cls92 = new p_title_info();
      cls92.title = LoadString(br);
      cls92.cnt = br.ReadInt32();
      cls92.type = br.ReadInt32();
      ary90[idx91] = cls92;
    }
    general_titles = ary90;
    int len93 = br.ReadUInt16();
    p_title_info[] ary94 = new p_title_info[len93];
    for (int idx95 = 0; idx95 < len93; idx95++)
    {
      p_title_info cls96 = new p_title_info();
      cls96.title = LoadString(br);
      cls96.cnt = br.ReadInt32();
      cls96.type = br.ReadInt32();
      ary94[idx95] = cls96;
    }
    qualifying_titles = ary94;
    int len97 = br.ReadUInt16();
    p_title_info[] ary98 = new p_title_info[len97];
    for (int idx99 = 0; idx99 < len97; idx99++)
    {
      p_title_info cls100 = new p_title_info();
      cls100.title = LoadString(br);
      cls100.cnt = br.ReadInt32();
      cls100.type = br.ReadInt32();
      ary98[idx99] = cls100;
    }
    set_titles = ary98;
    hero_type = br.ReadInt32();
    int len101 = br.ReadUInt16();
    toc_player_view_player.RoleInfo[] ary102 = new toc_player_view_player.RoleInfo[len101];
    for (int idx103 = 0; idx103 < len101; idx103++)
    {
      toc_player_view_player.RoleInfo cls104 = new toc_player_view_player.RoleInfo();
      cls104.camp = br.ReadInt32();
      cls104.item_id = br.ReadInt32();
      ary102[idx103] = cls104;
    }
    roles = ary102;
    exp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_view_player
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "view_player"; } }
  public override string Name { get { return "tos_player_view_player"; } }
  public override int Id { get { return 0x20002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.friend_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_err_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "err_msg"; } }
  public override string Name { get { return "toc_player_err_msg"; } }
  public override int Id { get { return 0x20003; } }

  public override void Load(BinaryReader br)
  {
    proto_type = LoadString(br);
    proto_key = LoadString(br);
    err_msg = LoadString(br);
    err_code = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_item_equip_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "item_equip_data"; } }
  public override string Name { get { return "toc_player_item_equip_data"; } }
  public override int Id { get { return 0x20004; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_item[] ary2 = new p_item[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_item cls4 = new p_item();
      cls4.uid = br.ReadInt32();
      cls4.item_id = br.ReadInt32();
      cls4.item_cnt = br.ReadInt32();
      cls4.create_time = br.ReadUInt32();
      cls4.time_limit = br.ReadUInt32();
      int len5 = br.ReadUInt16();
      System.Int32[] ary6 = new System.Int32[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadInt32();
      }
      cls4.accessory_values = ary6;
      cls4.durability = br.ReadInt32();
      cls4.is_arsenal = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    item_bag = ary2;
    int len8 = br.ReadUInt16();
    p_bag[] ary9 = new p_bag[len8];
    for (int idx10 = 0; idx10 < len8; idx10++)
    {
      p_bag cls11 = new p_bag();
      cls11.group_id = br.ReadInt32();
      p_group_equip_list cls12 = new p_group_equip_list();
      cls12.WEAPON1 = br.ReadInt32();
      cls12.WEAPON2 = br.ReadInt32();
      cls12.HELMET = br.ReadInt32();
      cls12.ARMOR = br.ReadInt32();
      cls12.BOOTS = br.ReadInt32();
      cls12.DAGGER = br.ReadInt32();
      cls12.GRENADE = br.ReadInt32();
      cls12.FLASHBOMB = br.ReadInt32();
      cls11.equip_list = cls12;
      int len13 = br.ReadUInt16();
      p_group_decoration[] ary14 = new p_group_decoration[len13];
      for (int idx15 = 0; idx15 < len13; idx15++)
      {
        p_group_decoration cls16 = new p_group_decoration();
        cls16.part = br.ReadInt32();
        cls16.decoration = br.ReadInt32();
        ary14[idx15] = cls16;
      }
      cls11.decorations = ary14;
      ary9[idx10] = cls11;
    }
    equip_groups = ary9;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_item_equip_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "item_equip_data"; } }
  public override string Name { get { return "tos_player_item_equip_data"; } }
  public override int Id { get { return 0x20004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_buy_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buy_item"; } }
  public override string Name { get { return "toc_player_buy_item"; } }
  public override int Id { get { return 0x20005; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_player_buy_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buy_item"; } }
  public override string Name { get { return "tos_player_buy_item"; } }
  public override int Id { get { return 0x20005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.mall_id);
    bw.Write(this.days);
    bw.Write(this.in_blackmarket);
    bw.Write(this.sid);
  }
}

partial class toc_player_equip_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "equip_item"; } }
  public override string Name { get { return "toc_player_equip_item"; } }
  public override int Id { get { return 0x20006; } }

  public override void Load(BinaryReader br)
  {
    p_bag cls1 = new p_bag();
    cls1.group_id = br.ReadInt32();
    p_group_equip_list cls2 = new p_group_equip_list();
    cls2.WEAPON1 = br.ReadInt32();
    cls2.WEAPON2 = br.ReadInt32();
    cls2.HELMET = br.ReadInt32();
    cls2.ARMOR = br.ReadInt32();
    cls2.BOOTS = br.ReadInt32();
    cls2.DAGGER = br.ReadInt32();
    cls2.GRENADE = br.ReadInt32();
    cls2.FLASHBOMB = br.ReadInt32();
    cls1.equip_list = cls2;
    int len3 = br.ReadUInt16();
    p_group_decoration[] ary4 = new p_group_decoration[len3];
    for (int idx5 = 0; idx5 < len3; idx5++)
    {
      p_group_decoration cls6 = new p_group_decoration();
      cls6.part = br.ReadInt32();
      cls6.decoration = br.ReadInt32();
      ary4[idx5] = cls6;
    }
    cls1.decorations = ary4;
    val = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_equip_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "equip_item"; } }
  public override string Name { get { return "tos_player_equip_item"; } }
  public override int Id { get { return 0x20006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.group);
    bw.Write(this.uid);
    bw.Write(this.sid);
  }
}

partial class toc_player_buy_equip_bag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buy_equip_bag"; } }
  public override string Name { get { return "toc_player_buy_equip_bag"; } }
  public override int Id { get { return 0x20007; } }

  public override void Load(BinaryReader br)
  {
    p_bag cls1 = new p_bag();
    cls1.group_id = br.ReadInt32();
    p_group_equip_list cls2 = new p_group_equip_list();
    cls2.WEAPON1 = br.ReadInt32();
    cls2.WEAPON2 = br.ReadInt32();
    cls2.HELMET = br.ReadInt32();
    cls2.ARMOR = br.ReadInt32();
    cls2.BOOTS = br.ReadInt32();
    cls2.DAGGER = br.ReadInt32();
    cls2.GRENADE = br.ReadInt32();
    cls2.FLASHBOMB = br.ReadInt32();
    cls1.equip_list = cls2;
    int len3 = br.ReadUInt16();
    p_group_decoration[] ary4 = new p_group_decoration[len3];
    for (int idx5 = 0; idx5 < len3; idx5++)
    {
      p_group_decoration cls6 = new p_group_decoration();
      cls6.part = br.ReadInt32();
      cls6.decoration = br.ReadInt32();
      ary4[idx5] = cls6;
    }
    cls1.decorations = ary4;
    bag = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_buy_equip_bag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buy_equip_bag"; } }
  public override string Name { get { return "tos_player_buy_equip_bag"; } }
  public override int Id { get { return 0x20007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_set_default_role
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_default_role"; } }
  public override string Name { get { return "toc_player_set_default_role"; } }
  public override int Id { get { return 0x20008; } }

  public override void Load(BinaryReader br)
  {
    camp = br.ReadInt32();
    role_uid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_default_role
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_default_role"; } }
  public override string Name { get { return "tos_player_set_default_role"; } }
  public override int Id { get { return 0x20008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.camp);
    bw.Write(this.role_uid);
    bw.Write(this.sid);
  }
}

partial class toc_player_equip_bag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "equip_bag"; } }
  public override string Name { get { return "toc_player_equip_bag"; } }
  public override int Id { get { return 0x20009; } }

  public override void Load(BinaryReader br)
  {
    p_bag cls1 = new p_bag();
    cls1.group_id = br.ReadInt32();
    p_group_equip_list cls2 = new p_group_equip_list();
    cls2.WEAPON1 = br.ReadInt32();
    cls2.WEAPON2 = br.ReadInt32();
    cls2.HELMET = br.ReadInt32();
    cls2.ARMOR = br.ReadInt32();
    cls2.BOOTS = br.ReadInt32();
    cls2.DAGGER = br.ReadInt32();
    cls2.GRENADE = br.ReadInt32();
    cls2.FLASHBOMB = br.ReadInt32();
    cls1.equip_list = cls2;
    int len3 = br.ReadUInt16();
    p_group_decoration[] ary4 = new p_group_decoration[len3];
    for (int idx5 = 0; idx5 < len3; idx5++)
    {
      p_group_decoration cls6 = new p_group_decoration();
      cls6.part = br.ReadInt32();
      cls6.decoration = br.ReadInt32();
      ary4[idx5] = cls6;
    }
    cls1.decorations = ary4;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_chat
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "chat"; } }
  public override string Name { get { return "toc_player_chat"; } }
  public override int Id { get { return 0x2000a; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    pid = br.ReadInt64();
    err_code = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_chat
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "chat"; } }
  public override string Name { get { return "tos_player_chat"; } }
  public override int Id { get { return 0x2000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    SaveString(bw, this.msg);
    bw.Write(this.pid);
    var ary1 = this.audio_chat;
    Assert(ary1 != null, "null value: this.audio_chat");
    bw.Write(ary1.audio_id);
    bw.Write(ary1.duration);
    var ary2 = ary1.content;
    Assert(ary2 != null, "null value: ary1.content");
    Assert(ary2.Length <= short.MaxValue, "ary1.content length out of range:", ary2.Length);
    bw.Write((ushort)ary2.Length);
    bw.Write(ary2, 0, ary2.Length);
    bw.Write(this.log_type);
    SaveString(bw, this.cost_type);
    bw.Write(this.sid);
  }
}

partial class toc_player_chat_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "chat_msg"; } }
  public override string Name { get { return "toc_player_chat_msg"; } }
  public override int Id { get { return 0x2000b; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    from = br.ReadInt64();
    msg = LoadString(br);
    icon = br.ReadInt32();
    name = LoadString(br);
    sex = br.ReadInt32();
    level = br.ReadInt32();
    vip_level = br.ReadInt32();
    p_audio_chat cls1 = new p_audio_chat();
    cls1.audio_id = br.ReadInt32();
    cls1.duration = br.ReadInt32();
    int len2 = br.ReadUInt16();
    cls1.content = br.ReadBytes(len2);
    audio_chat = cls1;
    area = LoadString(br);
    hero_type = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_audio
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "audio"; } }
  public override string Name { get { return "toc_player_audio"; } }
  public override int Id { get { return 0x2000c; } }

  public override void Load(BinaryReader br)
  {
    p_audio_chat cls1 = new p_audio_chat();
    cls1.audio_id = br.ReadInt32();
    cls1.duration = br.ReadInt32();
    int len2 = br.ReadUInt16();
    cls1.content = br.ReadBytes(len2);
    audio_chat = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_audio
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "audio"; } }
  public override string Name { get { return "tos_player_audio"; } }
  public override int Id { get { return 0x2000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.audio_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_rank_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "rank_list"; } }
  public override string Name { get { return "toc_player_rank_list"; } }
  public override int Id { get { return 0x2000d; } }

  public override void Load(BinaryReader br)
  {
    name = LoadString(br);
    page = br.ReadInt32();
    start = br.ReadInt32();
    size = br.ReadInt32();
    int len1 = br.ReadUInt16();
    toc_player_rank_list.RankData[] ary2 = new toc_player_rank_list.RankData[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_rank_list.RankData cls4 = new toc_player_rank_list.RankData();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.corps_name = LoadString(br);
      cls4.val = br.ReadInt32();
      cls4.ext = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_rank_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "rank_list"; } }
  public override string Name { get { return "tos_player_rank_list"; } }
  public override int Id { get { return 0x2000d; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}

partial class toc_player_notice
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "notice"; } }
  public override string Name { get { return "toc_player_notice"; } }
  public override int Id { get { return 0x2000e; } }

  public override void Load(BinaryReader br)
  {
    msg = LoadString(br);
    type = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_weapon_strengthen_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_strengthen_data"; } }
  public override string Name { get { return "toc_player_weapon_strengthen_data"; } }
  public override int Id { get { return 0x2000f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_weapon_strength_info_toc[] ary2 = new p_weapon_strength_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_weapon_strength_info_toc cls4 = new p_weapon_strength_info_toc();
      cls4.weapon_id = br.ReadInt32();
      cls4.state = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.cur_exp = br.ReadInt32();
      p_awaken_data cls5 = new p_awaken_data();
      cls5.level = br.ReadInt32();
      int len6 = br.ReadUInt16();
      System.Int32[] ary7 = new System.Int32[len6];
      for (int idx8 = 0; idx8 < len6; idx8++)
      {
        ary7[idx8] = br.ReadInt32();
      }
      cls5.props = ary7;
      cls5.times = br.ReadInt32();
      cls4.awaken_data = cls5;
      ary2[idx3] = cls4;
    }
    strengthen_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_weapon_strengthen_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_strengthen_data"; } }
  public override string Name { get { return "tos_player_weapon_strengthen_data"; } }
  public override int Id { get { return 0x2000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_weapon_train_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_train_info"; } }
  public override string Name { get { return "toc_player_weapon_train_info"; } }
  public override int Id { get { return 0x20010; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_weapon_train_info.train_info[] ary2 = new toc_player_weapon_train_info.train_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_weapon_train_info.train_info cls4 = new toc_player_weapon_train_info.train_info();
      cls4.index = br.ReadInt32();
      cls4.weapon = br.ReadInt32();
      cls4.ratio = br.ReadSingle();
      cls4.need_time = br.ReadInt32();
      cls4.start_time = br.ReadInt32();
      cls4.total_exp = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    train_list = ary2;
    quicken_times = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_weapon_train_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_train_info"; } }
  public override string Name { get { return "tos_player_weapon_train_info"; } }
  public override int Id { get { return 0x20010; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_start_weapon_train
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "start_weapon_train"; } }
  public override string Name { get { return "toc_player_start_weapon_train"; } }
  public override int Id { get { return 0x20011; } }

  public override void Load(BinaryReader br)
  {
    index = br.ReadInt32();
    weapon = br.ReadInt32();
    ratio = br.ReadSingle();
    need_time = br.ReadInt32();
    start_time = br.ReadInt32();
    total_exp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_start_weapon_train
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "start_weapon_train"; } }
  public override string Name { get { return "tos_player_start_weapon_train"; } }
  public override int Id { get { return 0x20011; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.weapon_uid);
    bw.Write(this.time_opt);
    bw.Write(this.ratio_opt);
    bw.Write(this.sid);
  }
}

partial class toc_player_stop_weapon_train
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "stop_weapon_train"; } }
  public override string Name { get { return "toc_player_stop_weapon_train"; } }
  public override int Id { get { return 0x20012; } }

  public override void Load(BinaryReader br)
  {
    index = br.ReadInt32();
    gain_exp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_stop_weapon_train
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "stop_weapon_train"; } }
  public override string Name { get { return "tos_player_stop_weapon_train"; } }
  public override int Id { get { return 0x20012; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.quicken);
    bw.Write(this.sid);
  }
}

partial class toc_player_weapon_awaken
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_awaken"; } }
  public override string Name { get { return "toc_player_weapon_awaken"; } }
  public override int Id { get { return 0x20013; } }

  public override void Load(BinaryReader br)
  {
    ret_code = br.ReadBoolean();
    weapon_id = br.ReadInt32();
    level = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    weapon_prop = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_weapon_awaken
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_awaken"; } }
  public override string Name { get { return "tos_player_weapon_awaken"; } }
  public override int Id { get { return 0x20013; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weapon_uid);
    bw.Write(this.sid);
  }
}

partial class toc_player_upgrade_weapon_strengthen_state
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "upgrade_weapon_strengthen_state"; } }
  public override string Name { get { return "toc_player_upgrade_weapon_strengthen_state"; } }
  public override int Id { get { return 0x20014; } }

  public override void Load(BinaryReader br)
  {
    weapon_id = br.ReadInt32();
    state = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_upgrade_weapon_strengthen_state
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "upgrade_weapon_strengthen_state"; } }
  public override string Name { get { return "tos_player_upgrade_weapon_strengthen_state"; } }
  public override int Id { get { return 0x20014; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weapon_uid);
    bw.Write(this.sid);
  }
}

partial class toc_player_win_stage
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "win_stage"; } }
  public override string Name { get { return "toc_player_win_stage"; } }
  public override int Id { get { return 0x20015; } }

  public override void Load(BinaryReader br)
  {
    firsttime = br.ReadBoolean();
    stageid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_mail_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mail_list"; } }
  public override string Name { get { return "toc_player_mail_list"; } }
  public override int Id { get { return 0x20016; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_mail_list.mail[] ary2 = new toc_player_mail_list.mail[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_mail_list.mail cls4 = new toc_player_mail_list.mail();
      cls4.mail_id = br.ReadInt32();
      cls4.from_id = br.ReadInt64();
      cls4.from_name = LoadString(br);
      cls4.title = LoadString(br);
      cls4.content = LoadString(br);
      int len5 = br.ReadUInt16();
      toc_player_mail_list.attachment[] ary6 = new toc_player_mail_list.attachment[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        toc_player_mail_list.attachment cls8 = new toc_player_mail_list.attachment();
        cls8.item_id = br.ReadInt32();
        cls8.item_cnt = br.ReadInt32();
        cls8.days = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.attachment = ary6;
      cls4.create_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_mail_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mail_list"; } }
  public override string Name { get { return "tos_player_mail_list"; } }
  public override int Id { get { return 0x20016; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_read_mail
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "read_mail"; } }
  public override string Name { get { return "tos_player_read_mail"; } }
  public override int Id { get { return 0x20017; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.mail_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_del_mail
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_mail"; } }
  public override string Name { get { return "toc_player_del_mail"; } }
  public override int Id { get { return 0x20018; } }

  public override void Load(BinaryReader br)
  {
    mail_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_new_mail
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_mail"; } }
  public override string Name { get { return "toc_player_new_mail"; } }
  public override int Id { get { return 0x20019; } }

  public override void Load(BinaryReader br)
  {
    toc_player_mail_list.mail cls1 = new toc_player_mail_list.mail();
    cls1.mail_id = br.ReadInt32();
    cls1.from_id = br.ReadInt64();
    cls1.from_name = LoadString(br);
    cls1.title = LoadString(br);
    cls1.content = LoadString(br);
    int len2 = br.ReadUInt16();
    toc_player_mail_list.attachment[] ary3 = new toc_player_mail_list.attachment[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      toc_player_mail_list.attachment cls5 = new toc_player_mail_list.attachment();
      cls5.item_id = br.ReadInt32();
      cls5.item_cnt = br.ReadInt32();
      cls5.days = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.attachment = ary3;
    cls1.create_time = br.ReadInt32();
    mail = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_read_all_mail
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "read_all_mail"; } }
  public override string Name { get { return "tos_player_read_all_mail"; } }
  public override int Id { get { return 0x2001a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_send_mail_gift
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "send_mail_gift"; } }
  public override string Name { get { return "toc_player_send_mail_gift"; } }
  public override int Id { get { return 0x2001b; } }

  public override void Load(BinaryReader br)
  {
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_send_mail_gift
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "send_mail_gift"; } }
  public override string Name { get { return "tos_player_send_mail_gift"; } }
  public override int Id { get { return 0x2001b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.to_pid);
    bw.Write(this.mall_id);
    bw.Write(this.index);
    SaveString(bw, this.title);
    SaveString(bw, this.content);
    bw.Write(this.sid);
  }
}

partial class toc_player_use_exchange_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "use_exchange_code"; } }
  public override string Name { get { return "toc_player_use_exchange_code"; } }
  public override int Id { get { return 0x2001c; } }

  public override void Load(BinaryReader br)
  {
    exchange_code = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_use_exchange_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "use_exchange_code"; } }
  public override string Name { get { return "tos_player_use_exchange_code"; } }
  public override int Id { get { return 0x2001c; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.exchange_code);
    bw.Write(this.sid);
  }
}

partial class toc_player_my_ranks
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "my_ranks"; } }
  public override string Name { get { return "toc_player_my_ranks"; } }
  public override int Id { get { return 0x2001d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_my_ranks.rank_info[] ary2 = new toc_player_my_ranks.rank_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_my_ranks.rank_info cls4 = new toc_player_my_ranks.rank_info();
      cls4.name = LoadString(br);
      cls4.rank = br.ReadInt32();
      cls4.val = br.ReadInt32();
      cls4.ext = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    ranks = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_my_ranks
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "my_ranks"; } }
  public override string Name { get { return "tos_player_my_ranks"; } }
  public override int Id { get { return 0x2001d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_friend_ranklist
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_ranklist"; } }
  public override string Name { get { return "toc_player_friend_ranklist"; } }
  public override int Id { get { return 0x2001e; } }

  public override void Load(BinaryReader br)
  {
    name = LoadString(br);
    int len1 = br.ReadUInt16();
    toc_player_friend_ranklist.rank_info[] ary2 = new toc_player_friend_ranklist.rank_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_friend_ranklist.rank_info cls4 = new toc_player_friend_ranklist.rank_info();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.corps_name = LoadString(br);
      cls4.rank = br.ReadInt32();
      cls4.val = br.ReadInt32();
      cls4.ext = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_friend_ranklist
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_ranklist"; } }
  public override string Name { get { return "tos_player_friend_ranklist"; } }
  public override int Id { get { return 0x2001e; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    bw.Write(this.sid);
  }
}

partial class toc_player_ranklist_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "ranklist_reward"; } }
  public override string Name { get { return "toc_player_ranklist_reward"; } }
  public override int Id { get { return 0x2001f; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_player_room_invitelist
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "room_invitelist"; } }
  public override string Name { get { return "toc_player_room_invitelist"; } }
  public override int Id { get { return 0x20020; } }

  public override void Load(BinaryReader br)
  {
    stage = br.ReadInt32();
    is_friend = br.ReadBoolean();
    int len1 = br.ReadUInt16();
    toc_player_room_invitelist.PlayerInfo[] ary2 = new toc_player_room_invitelist.PlayerInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_room_invitelist.PlayerInfo cls4 = new toc_player_room_invitelist.PlayerInfo();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.level = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    stage_type = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_room_invitelist
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "room_invitelist"; } }
  public override string Name { get { return "tos_player_room_invitelist"; } }
  public override int Id { get { return 0x20020; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.stage);
    bw.Write(this.is_friend);
    SaveString(bw, this.stage_type);
    bw.Write(this.sid);
  }
}

partial class toc_player_signin_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "signin_data"; } }
  public override string Name { get { return "toc_player_signin_data"; } }
  public override int Id { get { return 0x20021; } }

  public override void Load(BinaryReader br)
  {
    p_signin_data_toc cls1 = new p_signin_data_toc();
    cls1.last_sginin_time = br.ReadInt32();
    cls1.signined_times = br.ReadInt32();
    cls1.supplement_times = br.ReadInt32();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.rewarded = ary3;
    monthly_signin = cls1;
    p_signin_data_toc cls5 = new p_signin_data_toc();
    cls5.last_sginin_time = br.ReadInt32();
    cls5.signined_times = br.ReadInt32();
    cls5.supplement_times = br.ReadInt32();
    int len6 = br.ReadUInt16();
    System.Int32[] ary7 = new System.Int32[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      ary7[idx8] = br.ReadInt32();
    }
    cls5.rewarded = ary7;
    cumulate_signin = cls5;
    p_signin_data_toc cls9 = new p_signin_data_toc();
    cls9.last_sginin_time = br.ReadInt32();
    cls9.signined_times = br.ReadInt32();
    cls9.supplement_times = br.ReadInt32();
    int len10 = br.ReadUInt16();
    System.Int32[] ary11 = new System.Int32[len10];
    for (int idx12 = 0; idx12 < len10; idx12++)
    {
      ary11[idx12] = br.ReadInt32();
    }
    cls9.rewarded = ary11;
    day_signin = cls9;
    p_signin_data_toc cls13 = new p_signin_data_toc();
    cls13.last_sginin_time = br.ReadInt32();
    cls13.signined_times = br.ReadInt32();
    cls13.supplement_times = br.ReadInt32();
    int len14 = br.ReadUInt16();
    System.Int32[] ary15 = new System.Int32[len14];
    for (int idx16 = 0; idx16 < len14; idx16++)
    {
      ary15[idx16] = br.ReadInt32();
    }
    cls13.rewarded = ary15;
    cumulate_signin_new = cls13;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_signin_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "signin_data"; } }
  public override string Name { get { return "tos_player_signin_data"; } }
  public override int Id { get { return 0x20021; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_sign_in
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "sign_in"; } }
  public override string Name { get { return "toc_player_sign_in"; } }
  public override int Id { get { return 0x20022; } }

  public override void Load(BinaryReader br)
  {
    type = br.ReadInt32();
    supplement = br.ReadBoolean();
    p_signin_data_toc cls1 = new p_signin_data_toc();
    cls1.last_sginin_time = br.ReadInt32();
    cls1.signined_times = br.ReadInt32();
    cls1.supplement_times = br.ReadInt32();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.rewarded = ary3;
    data = cls1;
    flag = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_sign_in
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "sign_in"; } }
  public override string Name { get { return "tos_player_sign_in"; } }
  public override int Id { get { return 0x20022; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.type);
    bw.Write(this.supplement);
    bw.Write(this.flag);
    bw.Write(this.sid);
  }
}

partial class toc_player_welfare_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "welfare_data"; } }
  public override string Name { get { return "toc_player_welfare_data"; } }
  public override int Id { get { return 0x20023; } }

  public override void Load(BinaryReader br)
  {
    p_welfare_data_toc cls1 = new p_welfare_data_toc();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.welfare_list = ary3;
    cls1.last_online_time = br.ReadInt32();
    cls1.total_online_time = br.ReadInt32();
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_welfare_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "welfare_data"; } }
  public override string Name { get { return "tos_player_welfare_data"; } }
  public override int Id { get { return 0x20023; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_get_welfare_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_welfare_reward"; } }
  public override string Name { get { return "toc_player_get_welfare_reward"; } }
  public override int Id { get { return 0x20024; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    p_welfare_data_toc cls1 = new p_welfare_data_toc();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.welfare_list = ary3;
    cls1.last_online_time = br.ReadInt32();
    cls1.total_online_time = br.ReadInt32();
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_welfare_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_welfare_reward"; } }
  public override string Name { get { return "tos_player_get_welfare_reward"; } }
  public override int Id { get { return 0x20024; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_gm
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "gm"; } }
  public override string Name { get { return "toc_player_gm"; } }
  public override int Id { get { return 0x20025; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_gm.gm_def[] ary2 = new toc_player_gm.gm_def[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_gm.gm_def cls4 = new toc_player_gm.gm_def();
      cls4.name = LoadString(br);
      int len5 = br.ReadUInt16();
      System.String[] ary6 = new System.String[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = LoadString(br);
      }
      cls4.cmds = ary6;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_gm
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "gm"; } }
  public override string Name { get { return "tos_player_gm"; } }
  public override int Id { get { return 0x20025; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    SaveString(bw, this.cmd);
    bw.Write(this.sid);
  }
}

partial class toc_player_weapon_strengthen_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_strengthen_info"; } }
  public override string Name { get { return "toc_player_weapon_strengthen_info"; } }
  public override int Id { get { return 0x20026; } }

  public override void Load(BinaryReader br)
  {
    p_weapon_strength_info_toc cls1 = new p_weapon_strength_info_toc();
    cls1.weapon_id = br.ReadInt32();
    cls1.state = br.ReadInt32();
    cls1.level = br.ReadInt32();
    cls1.cur_exp = br.ReadInt32();
    p_awaken_data cls2 = new p_awaken_data();
    cls2.level = br.ReadInt32();
    int len3 = br.ReadUInt16();
    System.Int32[] ary4 = new System.Int32[len3];
    for (int idx5 = 0; idx5 < len3; idx5++)
    {
      ary4[idx5] = br.ReadInt32();
    }
    cls2.props = ary4;
    cls2.times = br.ReadInt32();
    cls1.awaken_data = cls2;
    info = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_set_icon
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_icon"; } }
  public override string Name { get { return "toc_player_set_icon"; } }
  public override int Id { get { return 0x20027; } }

  public override void Load(BinaryReader br)
  {
    icon = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_icon
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_icon"; } }
  public override string Name { get { return "tos_player_set_icon"; } }
  public override int Id { get { return 0x20027; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.icon);
    bw.Write(this.sid);
  }
}

partial class toc_player_game_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "game_reward"; } }
  public override string Name { get { return "toc_player_game_reward"; } }
  public override int Id { get { return 0x20028; } }

  public override void Load(BinaryReader br)
  {
    exp = br.ReadInt32();
    coin = br.ReadInt32();
    medal = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Int32[][] ary2 = new System.Int32[len1][];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      int len4 = br.ReadUInt16();
      System.Int32[] ary5 = new System.Int32[len4];
      for (int idx6 = 0; idx6 < len4; idx6++)
      {
        ary5[idx6] = br.ReadInt32();
      }
      ary2[idx3] = ary5;
    }
    weapon_exp_reward = ary2;
    int len7 = br.ReadUInt16();
    ItemInfo[] ary8 = new ItemInfo[len7];
    for (int idx9 = 0; idx9 < len7; idx9++)
    {
      ItemInfo cls10 = new ItemInfo();
      cls10.ID = br.ReadInt32();
      cls10.cnt = br.ReadInt32();
      cls10.day = br.ReadSingle();
      ary8[idx9] = cls10;
    }
    item_reward = ary8;
    int len11 = br.ReadUInt16();
    System.Int32[] ary12 = new System.Int32[len11];
    for (int idx13 = 0; idx13 < len11; idx13++)
    {
      ary12[idx13] = br.ReadInt32();
    }
    weapon_durability_reduce = ary12;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_double_game_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "double_game_reward"; } }
  public override string Name { get { return "toc_player_double_game_reward"; } }
  public override int Id { get { return 0x20029; } }

  public override void Load(BinaryReader br)
  {
    times = br.ReadInt32();
    exp = br.ReadInt32();
    coin = br.ReadInt32();
    medal = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_double_game_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "double_game_reward"; } }
  public override string Name { get { return "tos_player_double_game_reward"; } }
  public override int Id { get { return 0x20029; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_default_fight_roles
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "default_fight_roles"; } }
  public override string Name { get { return "toc_player_default_fight_roles"; } }
  public override int Id { get { return 0x2002a; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_default_fight_roles.role[] ary2 = new toc_player_default_fight_roles.role[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_default_fight_roles.role cls4 = new toc_player_default_fight_roles.role();
      cls4.camp = br.ReadInt32();
      cls4.uid = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    roles = ary2;
    hero_sex = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_default_fight_roles
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "default_fight_roles"; } }
  public override string Name { get { return "tos_player_default_fight_roles"; } }
  public override int Id { get { return 0x2002a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_verify_crc
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "verify_crc"; } }
  public override string Name { get { return "tos_player_verify_crc"; } }
  public override int Id { get { return 0x2002b; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.list;
    Assert(ary1 != null, "null value: this.list");
    Assert(ary1.Length <= short.MaxValue, "this.list length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      SaveString(bw, ary3.path);
      bw.Write(ary3.crc);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_critical_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "critical_msg"; } }
  public override string Name { get { return "toc_player_critical_msg"; } }
  public override int Id { get { return 0x2002c; } }

  public override void Load(BinaryReader br)
  {
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_log_event
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "log_event"; } }
  public override string Name { get { return "tos_player_log_event"; } }
  public override int Id { get { return 0x2002d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.event_id);
    bw.Write(this.sid);
  }
}

partial class tos_player_complain
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "complain"; } }
  public override string Name { get { return "tos_player_complain"; } }
  public override int Id { get { return 0x2002e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.complain_type);
    SaveString(bw, this.content);
    bw.Write(this.sid);
  }
}

partial class toc_player_rename
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "rename"; } }
  public override string Name { get { return "toc_player_rename"; } }
  public override int Id { get { return 0x2002f; } }

  public override void Load(BinaryReader br)
  {
    name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_rename
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "rename"; } }
  public override string Name { get { return "tos_player_rename"; } }
  public override int Id { get { return 0x2002f; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    bw.Write(this.sid);
  }
}

partial class toc_player_use_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "use_item"; } }
  public override string Name { get { return "toc_player_use_item"; } }
  public override int Id { get { return 0x20030; } }

  public override void Load(BinaryReader br)
  {
    item_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_use_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "use_item"; } }
  public override string Name { get { return "tos_player_use_item"; } }
  public override int Id { get { return 0x20030; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.item_uid);
    bw.Write(this.sid);
  }
}

partial class toc_player_buff_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buff_list"; } }
  public override string Name { get { return "toc_player_buff_list"; } }
  public override int Id { get { return 0x20031; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_buff_list.buff[] ary2 = new toc_player_buff_list.buff[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_buff_list.buff cls4 = new toc_player_buff_list.buff();
      cls4.buff_id = br.ReadInt32();
      cls4.time_limit = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_buff_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buff_list"; } }
  public override string Name { get { return "tos_player_buff_list"; } }
  public override int Id { get { return 0x20031; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_compound
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "compound"; } }
  public override string Name { get { return "toc_player_compound"; } }
  public override int Id { get { return 0x20032; } }

  public override void Load(BinaryReader br)
  {
    item_des_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_compound
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "compound"; } }
  public override string Name { get { return "tos_player_compound"; } }
  public override int Id { get { return 0x20032; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.item_src_id);
    bw.Write(this.item_des_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_room_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "room_list"; } }
  public override string Name { get { return "toc_player_room_list"; } }
  public override int Id { get { return 0x20033; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_room_info[] ary2 = new p_room_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_room_info cls4 = new p_room_info();
      cls4.id = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.leader_id = br.ReadInt64();
      cls4.leader = LoadString(br);
      cls4.leader_hero = br.ReadInt32();
      cls4.leader_device = LoadString(br);
      cls4.map = br.ReadInt32();
      cls4.rule = LoadString(br);
      cls4.player_count = br.ReadInt32();
      cls4.max_player = br.ReadInt32();
      cls4.spectators_count = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.password = br.ReadBoolean();
      cls4.cur_round = br.ReadInt32();
      cls4.max_round = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.state = br.ReadInt32();
      cls4.level_limit = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_room_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "room_list"; } }
  public override string Name { get { return "tos_player_room_list"; } }
  public override int Id { get { return 0x20033; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    bw.Write(this.sid);
  }
}

partial class toc_player_test_accessory_meterial
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "test_accessory_meterial"; } }
  public override string Name { get { return "toc_player_test_accessory_meterial"; } }
  public override int Id { get { return 0x20034; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    meterial_list = ary2;
    int len4 = br.ReadUInt16();
    p_item[] ary5 = new p_item[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      p_item cls7 = new p_item();
      cls7.uid = br.ReadInt32();
      cls7.item_id = br.ReadInt32();
      cls7.item_cnt = br.ReadInt32();
      cls7.create_time = br.ReadUInt32();
      cls7.time_limit = br.ReadUInt32();
      int len8 = br.ReadUInt16();
      System.Int32[] ary9 = new System.Int32[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        ary9[idx10] = br.ReadInt32();
      }
      cls7.accessory_values = ary9;
      cls7.durability = br.ReadInt32();
      cls7.is_arsenal = br.ReadBoolean();
      ary5[idx6] = cls7;
    }
    accessory_list = ary5;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_test_accessory_meterial
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "test_accessory_meterial"; } }
  public override string Name { get { return "tos_player_test_accessory_meterial"; } }
  public override int Id { get { return 0x20034; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.meterial_list;
    Assert(ary1 != null, "null value: this.meterial_list");
    Assert(ary1.Length <= short.MaxValue, "this.meterial_list length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_equip_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "equip_weapon_accessory"; } }
  public override string Name { get { return "toc_player_equip_weapon_accessory"; } }
  public override int Id { get { return 0x20035; } }

  public override void Load(BinaryReader br)
  {
    weapon_id = br.ReadInt32();
    accessory_uid = br.ReadInt32();
    pos = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_equip_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "equip_weapon_accessory"; } }
  public override string Name { get { return "tos_player_equip_weapon_accessory"; } }
  public override int Id { get { return 0x20035; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weapon_id);
    bw.Write(this.accessory_uid);
    bw.Write(this.pos);
    bw.Write(this.sid);
  }
}

partial class toc_player_unequip_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "unequip_weapon_accessory"; } }
  public override string Name { get { return "toc_player_unequip_weapon_accessory"; } }
  public override int Id { get { return 0x20036; } }

  public override void Load(BinaryReader br)
  {
    weapon_id = br.ReadInt32();
    pos = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_unequip_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "unequip_weapon_accessory"; } }
  public override string Name { get { return "tos_player_unequip_weapon_accessory"; } }
  public override int Id { get { return 0x20036; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weapon_id);
    bw.Write(this.pos);
    bw.Write(this.sid);
  }
}

partial class toc_player_buyback_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buyback_weapon_accessory"; } }
  public override string Name { get { return "toc_player_buyback_weapon_accessory"; } }
  public override int Id { get { return 0x20037; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    accessorys = ary2;
    coin = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_buyback_weapon_accessory
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "buyback_weapon_accessory"; } }
  public override string Name { get { return "tos_player_buyback_weapon_accessory"; } }
  public override int Id { get { return 0x20037; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.accessorys;
    Assert(ary1 != null, "null value: this.accessorys");
    Assert(ary1.Length <= short.MaxValue, "this.accessorys length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_first_recharge_welfare
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "first_recharge_welfare"; } }
  public override string Name { get { return "toc_player_first_recharge_welfare"; } }
  public override int Id { get { return 0x20038; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_first_recharge_welfare
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "first_recharge_welfare"; } }
  public override string Name { get { return "tos_player_first_recharge_welfare"; } }
  public override int Id { get { return 0x20038; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_first_recharge_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "first_recharge_data"; } }
  public override string Name { get { return "toc_player_first_recharge_data"; } }
  public override int Id { get { return 0x20039; } }

  public override void Load(BinaryReader br)
  {
    tag = br.ReadInt32();
    id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_first_recharge_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "first_recharge_data"; } }
  public override string Name { get { return "tos_player_first_recharge_data"; } }
  public override int Id { get { return 0x20039; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_get_first_recharge
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_first_recharge"; } }
  public override string Name { get { return "tos_player_get_first_recharge"; } }
  public override int Id { get { return 0x2003a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_recharge_welfare_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_welfare_list"; } }
  public override string Name { get { return "toc_player_recharge_welfare_list"; } }
  public override int Id { get { return 0x2003b; } }

  public override void Load(BinaryReader br)
  {
    stage = br.ReadInt32();
    event_id = br.ReadInt32();
    utc_time = br.ReadInt32();
    elapse_days = br.ReadInt32();
    recharge_money = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    welfares = ary2;
    int len4 = br.ReadUInt16();
    System.Int32[] ary5 = new System.Int32[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      ary5[idx6] = br.ReadInt32();
    }
    received_welfares = ary5;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_recharge_welfare_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_welfare_list"; } }
  public override string Name { get { return "tos_player_recharge_welfare_list"; } }
  public override int Id { get { return 0x2003b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_exchange_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_list"; } }
  public override string Name { get { return "toc_player_exchange_list"; } }
  public override int Id { get { return 0x2003c; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_exchange_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_list"; } }
  public override string Name { get { return "tos_player_exchange_list"; } }
  public override int Id { get { return 0x2003c; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.type);
    bw.Write(this.sid);
  }
}

partial class toc_player_exchange
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange"; } }
  public override string Name { get { return "toc_player_exchange"; } }
  public override int Id { get { return 0x2003d; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_exchange
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange"; } }
  public override string Name { get { return "tos_player_exchange"; } }
  public override int Id { get { return 0x2003d; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.type);
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_exchange_num
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_num"; } }
  public override string Name { get { return "toc_player_exchange_num"; } }
  public override int Id { get { return 0x2003e; } }

  public override void Load(BinaryReader br)
  {
    exchange_num = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_exchange_num
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_num"; } }
  public override string Name { get { return "tos_player_exchange_num"; } }
  public override int Id { get { return 0x2003e; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.type);
    bw.Write(this.sid);
  }
}

partial class tos_player_get_recharge_welfare
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_recharge_welfare"; } }
  public override string Name { get { return "tos_player_get_recharge_welfare"; } }
  public override int Id { get { return 0x2003f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_recharge_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_info"; } }
  public override string Name { get { return "toc_player_recharge_info"; } }
  public override int Id { get { return 0x20040; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    recharge_id_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_recharge_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_info"; } }
  public override string Name { get { return "tos_player_recharge_info"; } }
  public override int Id { get { return 0x20040; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_recharge_associator_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_associator_info"; } }
  public override string Name { get { return "toc_player_recharge_associator_info"; } }
  public override int Id { get { return 0x20041; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_recharge_associator_info[] ary2 = new p_recharge_associator_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_recharge_associator_info cls4 = new p_recharge_associator_info();
      cls4.associator_type = br.ReadInt32();
      cls4.associator_guarantee = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    recharge_associator_info = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_recharge_switch
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_switch"; } }
  public override string Name { get { return "toc_player_recharge_switch"; } }
  public override int Id { get { return 0x20042; } }

  public override void Load(BinaryReader br)
  {
    is_open = br.ReadBoolean();
    callback_info = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_recharge_switch
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "recharge_switch"; } }
  public override string Name { get { return "tos_player_recharge_switch"; } }
  public override int Id { get { return 0x20042; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.item_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "lottery"; } }
  public override string Name { get { return "toc_player_lottery"; } }
  public override int Id { get { return 0x20043; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    reward_id_list = ary2;
    p_lottery_player cls4 = new p_lottery_player();
    cls4.uid = br.ReadInt64();
    cls4.name = LoadString(br);
    cls4.reward_id = br.ReadInt32();
    cls4.award_amount = br.ReadInt32();
    top_player = cls4;
    award_amount = br.ReadInt32();
    free_times = br.ReadInt32();
    free_cd = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "lottery"; } }
  public override string Name { get { return "tos_player_lottery"; } }
  public override int Id { get { return 0x20043; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.lottery_type);
    bw.Write(this.sid);
  }
}

partial class toc_player_get_lottery_rank
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_lottery_rank"; } }
  public override string Name { get { return "toc_player_get_lottery_rank"; } }
  public override int Id { get { return 0x20044; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_lottery_player[] ary2 = new p_lottery_player[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_lottery_player cls4 = new p_lottery_player();
      cls4.uid = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.reward_id = br.ReadInt32();
      cls4.award_amount = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    player_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_lottery_rank
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_lottery_rank"; } }
  public override string Name { get { return "tos_player_get_lottery_rank"; } }
  public override int Id { get { return 0x20044; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_lottery_message
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "lottery_message"; } }
  public override string Name { get { return "toc_player_lottery_message"; } }
  public override int Id { get { return 0x20045; } }

  public override void Load(BinaryReader br)
  {
    p_lottery_player cls1 = new p_lottery_player();
    cls1.uid = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.reward_id = br.ReadInt32();
    cls1.award_amount = br.ReadInt32();
    player = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_new_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_lottery"; } }
  public override string Name { get { return "toc_player_new_lottery"; } }
  public override int Id { get { return 0x20046; } }

  public override void Load(BinaryReader br)
  {
    lottery_type = br.ReadInt32();
    time = br.ReadInt32();
    int len1 = br.ReadUInt16();
    ItemInfo[] ary2 = new ItemInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ItemInfo cls4 = new ItemInfo();
      cls4.ID = br.ReadInt32();
      cls4.cnt = br.ReadInt32();
      cls4.day = br.ReadSingle();
      ary2[idx3] = cls4;
    }
    items = ary2;
    int len5 = br.ReadUInt16();
    System.Int32[] ary6 = new System.Int32[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt32();
    }
    his_items = ary6;
    award_amount = br.ReadInt32();
    point = br.ReadInt32();
    int len8 = br.ReadUInt16();
    System.String[] ary9 = new System.String[len8];
    for (int idx10 = 0; idx10 < len8; idx10++)
    {
      ary9[idx10] = LoadString(br);
    }
    lucky_list = ary9;
    int len11 = br.ReadUInt16();
    System.String[] ary12 = new System.String[len11];
    for (int idx13 = 0; idx13 < len11; idx13++)
    {
      ary12[idx13] = LoadString(br);
    }
    recent_list = ary12;
    free_time = br.ReadInt32();
    int len14 = br.ReadUInt16();
    System.Boolean[] ary15 = new System.Boolean[len14];
    for (int idx16 = 0; idx16 < len14; idx16++)
    {
      ary15[idx16] = br.ReadBoolean();
    }
    exists = ary15;
    buy_cnt = br.ReadInt32();
    discount_one_count = br.ReadInt32();
    discount_ten_count = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_new_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_lottery"; } }
  public override string Name { get { return "tos_player_new_lottery"; } }
  public override int Id { get { return 0x20046; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.lottery_type);
    bw.Write(this.time);
    bw.Write(this.sid);
  }
}

partial class toc_player_new_exchange_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_exchange_lottery"; } }
  public override string Name { get { return "toc_player_new_exchange_lottery"; } }
  public override int Id { get { return 0x20047; } }

  public override void Load(BinaryReader br)
  {
    exchange_code = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_new_exchange_lottery
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_exchange_lottery"; } }
  public override string Name { get { return "tos_player_new_exchange_lottery"; } }
  public override int Id { get { return 0x20047; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.exchange_code);
    bw.Write(this.sid);
  }
}

partial class toc_player_channel_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "channel_list"; } }
  public override string Name { get { return "toc_player_channel_list"; } }
  public override int Id { get { return 0x20048; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_channel_info[] ary2 = new p_channel_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_channel_info cls4 = new p_channel_info();
      cls4.id = br.ReadInt32();
      cls4.typ = LoadString(br);
      cls4.subtyp = LoadString(br);
      cls4.name = LoadString(br);
      cls4.player_cnt = br.ReadInt32();
      cls4.player_max = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    channels = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_channel_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "channel_list"; } }
  public override string Name { get { return "tos_player_channel_list"; } }
  public override int Id { get { return 0x20048; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_exchange_points
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_points"; } }
  public override string Name { get { return "toc_player_exchange_points"; } }
  public override int Id { get { return 0x20049; } }

  public override void Load(BinaryReader br)
  {
    op_code = br.ReadInt32();
    points = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_exchange_points
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "exchange_points"; } }
  public override string Name { get { return "tos_player_exchange_points"; } }
  public override int Id { get { return 0x20049; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.op_code);
    bw.Write(this.sid);
  }
}

partial class toc_player_event_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "event_list"; } }
  public override string Name { get { return "toc_player_event_list"; } }
  public override int Id { get { return 0x2004a; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_event[] ary2 = new p_event[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_event cls4 = new p_event();
      cls4.id = br.ReadInt32();
      cls4.status = br.ReadInt32();
      cls4.extend = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    events = ary2;
    int len5 = br.ReadUInt16();
    ItemInfo[] ary6 = new ItemInfo[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ItemInfo cls8 = new ItemInfo();
      cls8.ID = br.ReadInt32();
      cls8.cnt = br.ReadInt32();
      cls8.day = br.ReadSingle();
      ary6[idx7] = cls8;
    }
    items = ary6;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_event_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "event_list"; } }
  public override string Name { get { return "tos_player_event_list"; } }
  public override int Id { get { return 0x2004a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_get_event
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_event"; } }
  public override string Name { get { return "tos_player_get_event"; } }
  public override int Id { get { return 0x2004b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_cash_gain_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "cash_gain_info"; } }
  public override string Name { get { return "toc_player_cash_gain_info"; } }
  public override int Id { get { return 0x2004c; } }

  public override void Load(BinaryReader br)
  {
    total_gain = br.ReadInt32();
    before_gain = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_cash_gain_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "cash_gain_info"; } }
  public override string Name { get { return "tos_player_cash_gain_info"; } }
  public override int Id { get { return 0x2004c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_friend_rcmnd_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_rcmnd_list"; } }
  public override string Name { get { return "toc_player_friend_rcmnd_list"; } }
  public override int Id { get { return 0x2004d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_friend_data[] ary2 = new p_friend_data[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_friend_data cls4 = new p_friend_data();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.channel = br.ReadInt32();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.logout_time = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_hero_card_info[] ary6 = new p_hero_card_info[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_hero_card_info cls8 = new p_hero_card_info();
        cls8.hero_type = br.ReadInt32();
        cls8.end_time = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.hero_cards = ary6;
      int len9 = br.ReadUInt16();
      p_title_info[] ary10 = new p_title_info[len9];
      for (int idx11 = 0; idx11 < len9; idx11++)
      {
        p_title_info cls12 = new p_title_info();
        cls12.title = LoadString(br);
        cls12.cnt = br.ReadInt32();
        cls12.type = br.ReadInt32();
        ary10[idx11] = cls12;
      }
      cls4.set_titles = ary10;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_friend_rcmnd_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_rcmnd_list"; } }
  public override string Name { get { return "tos_player_friend_rcmnd_list"; } }
  public override int Id { get { return 0x2004d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_friend_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_data"; } }
  public override string Name { get { return "toc_player_friend_data"; } }
  public override int Id { get { return 0x2004e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_friend_data[] ary2 = new p_friend_data[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_friend_data cls4 = new p_friend_data();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.channel = br.ReadInt32();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.logout_time = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_hero_card_info[] ary6 = new p_hero_card_info[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_hero_card_info cls8 = new p_hero_card_info();
        cls8.hero_type = br.ReadInt32();
        cls8.end_time = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.hero_cards = ary6;
      int len9 = br.ReadUInt16();
      p_title_info[] ary10 = new p_title_info[len9];
      for (int idx11 = 0; idx11 < len9; idx11++)
      {
        p_title_info cls12 = new p_title_info();
        cls12.title = LoadString(br);
        cls12.cnt = br.ReadInt32();
        cls12.type = br.ReadInt32();
        ary10[idx11] = cls12;
      }
      cls4.set_titles = ary10;
      ary2[idx3] = cls4;
    }
    friend_list = ary2;
    int len13 = br.ReadUInt16();
    p_friend_data[] ary14 = new p_friend_data[len13];
    for (int idx15 = 0; idx15 < len13; idx15++)
    {
      p_friend_data cls16 = new p_friend_data();
      cls16.id = br.ReadInt64();
      cls16.name = LoadString(br);
      cls16.sex = br.ReadInt32();
      cls16.icon = br.ReadInt32();
      cls16.honor = br.ReadInt32();
      cls16.vip_level = br.ReadInt32();
      cls16.level = br.ReadInt32();
      cls16.handle = br.ReadInt32();
      cls16.channel = br.ReadInt32();
      cls16.room = br.ReadInt32();
      cls16.team = br.ReadInt32();
      cls16.mvp = br.ReadInt32();
      cls16.gold_ace = br.ReadInt32();
      cls16.silver_ace = br.ReadInt32();
      cls16.fight = br.ReadBoolean();
      cls16.logout_time = br.ReadInt32();
      int len17 = br.ReadUInt16();
      p_hero_card_info[] ary18 = new p_hero_card_info[len17];
      for (int idx19 = 0; idx19 < len17; idx19++)
      {
        p_hero_card_info cls20 = new p_hero_card_info();
        cls20.hero_type = br.ReadInt32();
        cls20.end_time = br.ReadInt32();
        ary18[idx19] = cls20;
      }
      cls16.hero_cards = ary18;
      int len21 = br.ReadUInt16();
      p_title_info[] ary22 = new p_title_info[len21];
      for (int idx23 = 0; idx23 < len21; idx23++)
      {
        p_title_info cls24 = new p_title_info();
        cls24.title = LoadString(br);
        cls24.cnt = br.ReadInt32();
        cls24.type = br.ReadInt32();
        ary22[idx23] = cls24;
      }
      cls16.set_titles = ary22;
      ary14[idx15] = cls16;
    }
    fans_list = ary14;
    int len25 = br.ReadUInt16();
    p_friend_data[] ary26 = new p_friend_data[len25];
    for (int idx27 = 0; idx27 < len25; idx27++)
    {
      p_friend_data cls28 = new p_friend_data();
      cls28.id = br.ReadInt64();
      cls28.name = LoadString(br);
      cls28.sex = br.ReadInt32();
      cls28.icon = br.ReadInt32();
      cls28.honor = br.ReadInt32();
      cls28.vip_level = br.ReadInt32();
      cls28.level = br.ReadInt32();
      cls28.handle = br.ReadInt32();
      cls28.channel = br.ReadInt32();
      cls28.room = br.ReadInt32();
      cls28.team = br.ReadInt32();
      cls28.mvp = br.ReadInt32();
      cls28.gold_ace = br.ReadInt32();
      cls28.silver_ace = br.ReadInt32();
      cls28.fight = br.ReadBoolean();
      cls28.logout_time = br.ReadInt32();
      int len29 = br.ReadUInt16();
      p_hero_card_info[] ary30 = new p_hero_card_info[len29];
      for (int idx31 = 0; idx31 < len29; idx31++)
      {
        p_hero_card_info cls32 = new p_hero_card_info();
        cls32.hero_type = br.ReadInt32();
        cls32.end_time = br.ReadInt32();
        ary30[idx31] = cls32;
      }
      cls28.hero_cards = ary30;
      int len33 = br.ReadUInt16();
      p_title_info[] ary34 = new p_title_info[len33];
      for (int idx35 = 0; idx35 < len33; idx35++)
      {
        p_title_info cls36 = new p_title_info();
        cls36.title = LoadString(br);
        cls36.cnt = br.ReadInt32();
        cls36.type = br.ReadInt32();
        ary34[idx35] = cls36;
      }
      cls28.set_titles = ary34;
      ary26[idx27] = cls28;
    }
    black_list = ary26;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_friend_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_data"; } }
  public override string Name { get { return "tos_player_friend_data"; } }
  public override int Id { get { return 0x2004e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_friend_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "friend_msg"; } }
  public override string Name { get { return "toc_player_friend_msg"; } }
  public override int Id { get { return 0x2004f; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    p_friend_data cls1 = new p_friend_data();
    cls1.id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.sex = br.ReadInt32();
    cls1.icon = br.ReadInt32();
    cls1.honor = br.ReadInt32();
    cls1.vip_level = br.ReadInt32();
    cls1.level = br.ReadInt32();
    cls1.handle = br.ReadInt32();
    cls1.channel = br.ReadInt32();
    cls1.room = br.ReadInt32();
    cls1.team = br.ReadInt32();
    cls1.mvp = br.ReadInt32();
    cls1.gold_ace = br.ReadInt32();
    cls1.silver_ace = br.ReadInt32();
    cls1.fight = br.ReadBoolean();
    cls1.logout_time = br.ReadInt32();
    int len2 = br.ReadUInt16();
    p_hero_card_info[] ary3 = new p_hero_card_info[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_hero_card_info cls5 = new p_hero_card_info();
      cls5.hero_type = br.ReadInt32();
      cls5.end_time = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.hero_cards = ary3;
    int len6 = br.ReadUInt16();
    p_title_info[] ary7 = new p_title_info[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_title_info cls9 = new p_title_info();
      cls9.title = LoadString(br);
      cls9.cnt = br.ReadInt32();
      cls9.type = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.set_titles = ary7;
    friend_data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_view_player_by_name
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "view_player_by_name"; } }
  public override string Name { get { return "tos_player_view_player_by_name"; } }
  public override int Id { get { return 0x20050; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.friend_name);
    bw.Write(this.sid);
  }
}

partial class tos_player_add_friend_by_name
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_friend_by_name"; } }
  public override string Name { get { return "tos_player_add_friend_by_name"; } }
  public override int Id { get { return 0x20051; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.friend_name);
    bw.Write(this.sid);
  }
}

partial class toc_player_add_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_friend"; } }
  public override string Name { get { return "toc_player_add_friend"; } }
  public override int Id { get { return 0x20052; } }

  public override void Load(BinaryReader br)
  {
    friend_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_add_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_friend"; } }
  public override string Name { get { return "tos_player_add_friend"; } }
  public override int Id { get { return 0x20052; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.friend_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_del_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_friend"; } }
  public override string Name { get { return "toc_player_del_friend"; } }
  public override int Id { get { return 0x20053; } }

  public override void Load(BinaryReader br)
  {
    friend_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_del_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_friend"; } }
  public override string Name { get { return "tos_player_del_friend"; } }
  public override int Id { get { return 0x20053; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.friend_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_agree_fan
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "agree_fan"; } }
  public override string Name { get { return "toc_player_agree_fan"; } }
  public override int Id { get { return 0x20054; } }

  public override void Load(BinaryReader br)
  {
    fan_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_agree_fan
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "agree_fan"; } }
  public override string Name { get { return "tos_player_agree_fan"; } }
  public override int Id { get { return 0x20054; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.fan_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_refuse_fan
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "refuse_fan"; } }
  public override string Name { get { return "toc_player_refuse_fan"; } }
  public override int Id { get { return 0x20055; } }

  public override void Load(BinaryReader br)
  {
    fan_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_refuse_fan
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "refuse_fan"; } }
  public override string Name { get { return "tos_player_refuse_fan"; } }
  public override int Id { get { return 0x20055; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.fan_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_add_black
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_black"; } }
  public override string Name { get { return "toc_player_add_black"; } }
  public override int Id { get { return 0x20056; } }

  public override void Load(BinaryReader br)
  {
    black_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_add_black
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_black"; } }
  public override string Name { get { return "tos_player_add_black"; } }
  public override int Id { get { return 0x20056; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.black_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_del_black
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_black"; } }
  public override string Name { get { return "toc_player_del_black"; } }
  public override int Id { get { return 0x20057; } }

  public override void Load(BinaryReader br)
  {
    black_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_del_black
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_black"; } }
  public override string Name { get { return "tos_player_del_black"; } }
  public override int Id { get { return 0x20057; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.black_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_getdata
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "getdata"; } }
  public override string Name { get { return "toc_player_getdata"; } }
  public override int Id { get { return 0x20058; } }

  public override void Load(BinaryReader br)
  {
    req = CJsonHelper.Load(LoadString(br)) as CDict;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_getdata
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "getdata"; } }
  public override string Name { get { return "tos_player_getdata"; } }
  public override int Id { get { return 0x20058; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, CJsonHelper.Save(this.ret));
    bw.Write(this.sid);
  }
}

partial class toc_player_repair_weapons
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "repair_weapons"; } }
  public override string Name { get { return "toc_player_repair_weapons"; } }
  public override int Id { get { return 0x20059; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_player_repair_weapons
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "repair_weapons"; } }
  public override string Name { get { return "tos_player_repair_weapons"; } }
  public override int Id { get { return 0x20059; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.weapon_uids;
    Assert(ary1 != null, "null value: this.weapon_uids");
    Assert(ary1.Length <= short.MaxValue, "this.weapon_uids length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_game_reward_times
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "game_reward_times"; } }
  public override string Name { get { return "toc_player_game_reward_times"; } }
  public override int Id { get { return 0x2005a; } }

  public override void Load(BinaryReader br)
  {
    partake_reward = br.ReadInt32();
    win_reward = br.ReadInt32();
    game_type = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_player_blackmarket_notice
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "blackmarket_notice"; } }
  public override string Name { get { return "toc_player_blackmarket_notice"; } }
  public override int Id { get { return 0x2005b; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_player_blackmarket_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "blackmarket_list"; } }
  public override string Name { get { return "toc_player_blackmarket_list"; } }
  public override int Id { get { return 0x2005c; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    item_list = ary2;
    refresh_time = br.ReadInt32();
    refresh_count = br.ReadInt32();
    refresh_cost = br.ReadInt32();
    show_count = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_blackmarket_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "blackmarket_list"; } }
  public override string Name { get { return "tos_player_blackmarket_list"; } }
  public override int Id { get { return 0x2005c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_blackmarket_refresh
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "blackmarket_refresh"; } }
  public override string Name { get { return "tos_player_blackmarket_refresh"; } }
  public override int Id { get { return 0x2005d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_mall_record
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mall_record"; } }
  public override string Name { get { return "toc_player_mall_record"; } }
  public override int Id { get { return 0x2005e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_mall_record[] ary2 = new p_mall_record[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_mall_record cls4 = new p_mall_record();
      cls4.mall_id = br.ReadInt32();
      cls4.amount = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    p_record = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_mall_record
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mall_record"; } }
  public override string Name { get { return "tos_player_mall_record"; } }
  public override int Id { get { return 0x2005e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_load_done
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "load_done"; } }
  public override string Name { get { return "tos_player_load_done"; } }
  public override int Id { get { return 0x2005f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.step);
    bw.Write(this.sid);
  }
}

partial class toc_player_get_zone_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_zone_info"; } }
  public override string Name { get { return "toc_player_get_zone_info"; } }
  public override int Id { get { return 0x20060; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_zone_info_item[] ary2 = new p_zone_info_item[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_zone_info_item cls4 = new p_zone_info_item();
      cls4.key = LoadString(br);
      cls4.value = LoadString(br);
      ary2[idx3] = cls4;
    }
    p_zone_info = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_zone_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_zone_info"; } }
  public override string Name { get { return "tos_player_get_zone_info"; } }
  public override int Id { get { return 0x20060; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.owner_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_edit_zone
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "edit_zone"; } }
  public override string Name { get { return "toc_player_edit_zone"; } }
  public override int Id { get { return 0x20061; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_edit_zone
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "edit_zone"; } }
  public override string Name { get { return "tos_player_edit_zone"; } }
  public override int Id { get { return 0x20061; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.p_zone_info;
    Assert(ary1 != null, "null value: this.p_zone_info");
    Assert(ary1.Length <= short.MaxValue, "this.p_zone_info length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      SaveString(bw, ary3.key);
      SaveString(bw, ary3.value);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_achievement_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "achievement_data"; } }
  public override string Name { get { return "toc_player_achievement_data"; } }
  public override int Id { get { return 0x20062; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_achievement[] ary2 = new p_achievement[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_achievement cls4 = new p_achievement();
      cls4.id = br.ReadInt32();
      cls4.val = br.ReadInt32();
      cls4.stat = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    achieve_list = ary2;
    int len5 = br.ReadUInt16();
    System.Int32[] ary6 = new System.Int32[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt32();
    }
    achieve_badge_list = ary6;
    int len8 = br.ReadUInt16();
    System.Int32[] ary9 = new System.Int32[len8];
    for (int idx10 = 0; idx10 < len8; idx10++)
    {
      ary9[idx10] = br.ReadInt32();
    }
    achieve_chenghao_list = ary9;
    int len11 = br.ReadUInt16();
    System.Int32[] ary12 = new System.Int32[len11];
    for (int idx13 = 0; idx13 < len11; idx13++)
    {
      ary12[idx13] = br.ReadInt32();
    }
    achieve_head_icon_list = ary12;
    achieve_point = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_achievement_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "achievement_data"; } }
  public override string Name { get { return "tos_player_achievement_data"; } }
  public override int Id { get { return 0x20062; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_achieve_update
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "achieve_update"; } }
  public override string Name { get { return "toc_player_achieve_update"; } }
  public override int Id { get { return 0x20063; } }

  public override void Load(BinaryReader br)
  {
    p_achievement cls1 = new p_achievement();
    cls1.id = br.ReadInt32();
    cls1.val = br.ReadInt32();
    cls1.stat = br.ReadInt32();
    achieve = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_achieve_add
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "achieve_add"; } }
  public override string Name { get { return "toc_player_achieve_add"; } }
  public override int Id { get { return 0x20064; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_achieve_add_item[] ary2 = new p_achieve_add_item[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_achieve_add_item cls4 = new p_achieve_add_item();
      cls4.tag = LoadString(br);
      cls4.val = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    item_add_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_get_achieve_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_achieve_reward"; } }
  public override string Name { get { return "toc_player_get_achieve_reward"; } }
  public override int Id { get { return 0x20065; } }

  public override void Load(BinaryReader br)
  {
    achieve_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_achieve_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_achieve_reward"; } }
  public override string Name { get { return "tos_player_get_achieve_reward"; } }
  public override int Id { get { return 0x20065; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.achieve_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_set_chenghao
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_chenghao"; } }
  public override string Name { get { return "toc_player_set_chenghao"; } }
  public override int Id { get { return 0x20066; } }

  public override void Load(BinaryReader br)
  {
    chenghao = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_chenghao
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_chenghao"; } }
  public override string Name { get { return "tos_player_set_chenghao"; } }
  public override int Id { get { return 0x20066; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.chenghao);
    bw.Write(this.sid);
  }
}

partial class toc_player_trigger_guide
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "trigger_guide"; } }
  public override string Name { get { return "toc_player_trigger_guide"; } }
  public override int Id { get { return 0x20067; } }

  public override void Load(BinaryReader br)
  {
    is_trigger = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_trigger_guide
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "trigger_guide"; } }
  public override string Name { get { return "tos_player_trigger_guide"; } }
  public override int Id { get { return 0x20067; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_server_value
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "server_value"; } }
  public override string Name { get { return "toc_player_server_value"; } }
  public override int Id { get { return 0x20068; } }

  public override void Load(BinaryReader br)
  {
    dict = CJsonHelper.Load(LoadString(br)) as CDict;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_survival_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "survival_data"; } }
  public override string Name { get { return "toc_player_survival_data"; } }
  public override int Id { get { return 0x20069; } }

  public override void Load(BinaryReader br)
  {
    daily_reward_cnt = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_survival_type_data[] ary2 = new p_survival_type_data[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_survival_type_data cls4 = new p_survival_type_data();
      cls4.type = LoadString(br);
      cls4.top_level = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    types_data = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_eventlist
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "eventlist"; } }
  public override string Name { get { return "toc_player_eventlist"; } }
  public override int Id { get { return 0x2006a; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_player_event[] ary2 = new p_player_event[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_player_event cls4 = new p_player_event();
      cls4.id = br.ReadInt32();
      cls4.open = br.ReadBoolean();
      cls4.progress = br.ReadInt32();
      cls4.total = br.ReadInt32();
      cls4.reward = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_take_event_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "take_event_reward"; } }
  public override string Name { get { return "toc_player_take_event_reward"; } }
  public override int Id { get { return 0x2006b; } }

  public override void Load(BinaryReader br)
  {
    event_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_take_event_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "take_event_reward"; } }
  public override string Name { get { return "tos_player_take_event_reward"; } }
  public override int Id { get { return 0x2006b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.event_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_client_value
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "client_value"; } }
  public override string Name { get { return "toc_player_client_value"; } }
  public override int Id { get { return 0x2006c; } }

  public override void Load(BinaryReader br)
  {
    dict = CJsonHelper.Load(LoadString(br)) as CDict;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_client_value_update
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "client_value_update"; } }
  public override string Name { get { return "tos_player_client_value_update"; } }
  public override int Id { get { return 0x2006d; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, CJsonHelper.Save(this.dict));
    bw.Write(this.sid);
  }
}

partial class toc_player_mission_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mission_data"; } }
  public override string Name { get { return "toc_player_mission_data"; } }
  public override int Id { get { return 0x2006e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_mission_data_toc[] ary2 = new p_mission_data_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_mission_data_toc cls4 = new p_mission_data_toc();
      cls4.id = br.ReadInt32();
      cls4.status = br.ReadInt32();
      cls4.progress = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    missions = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_mission_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "mission_data"; } }
  public override string Name { get { return "tos_player_mission_data"; } }
  public override int Id { get { return 0x2006e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_modify_mission
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "modify_mission"; } }
  public override string Name { get { return "toc_player_modify_mission"; } }
  public override int Id { get { return 0x2006f; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    p_mission_data_toc cls1 = new p_mission_data_toc();
    cls1.id = br.ReadInt32();
    cls1.status = br.ReadInt32();
    cls1.progress = br.ReadInt32();
    mission = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_get_mission_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_mission_reward"; } }
  public override string Name { get { return "toc_player_get_mission_reward"; } }
  public override int Id { get { return 0x20070; } }

  public override void Load(BinaryReader br)
  {
    mission_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_mission_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_mission_reward"; } }
  public override string Name { get { return "tos_player_get_mission_reward"; } }
  public override int Id { get { return 0x20070; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.mission_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_offline_chat_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "offline_chat_msg"; } }
  public override string Name { get { return "toc_player_offline_chat_msg"; } }
  public override int Id { get { return 0x20071; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_offline_chat_msg.offline_msg[] ary2 = new toc_player_offline_chat_msg.offline_msg[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_offline_chat_msg.offline_msg cls4 = new toc_player_offline_chat_msg.offline_msg();
      cls4.channel = br.ReadInt32();
      cls4.from = br.ReadInt64();
      cls4.msg = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.area = LoadString(br);
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_comfirm_offline_msg
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "comfirm_offline_msg"; } }
  public override string Name { get { return "tos_player_comfirm_offline_msg"; } }
  public override int Id { get { return 0x20072; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_is_oldplayer
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "is_oldplayer"; } }
  public override string Name { get { return "toc_player_is_oldplayer"; } }
  public override int Id { get { return 0x20073; } }

  public override void Load(BinaryReader br)
  {
    is_oldplayer = br.ReadInt32();
    is_invited = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_oldplayer_invite
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "oldplayer_invite"; } }
  public override string Name { get { return "tos_player_oldplayer_invite"; } }
  public override int Id { get { return 0x20074; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_get_oldplayer_gift_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_oldplayer_gift_info"; } }
  public override string Name { get { return "toc_player_get_oldplayer_gift_info"; } }
  public override int Id { get { return 0x20075; } }

  public override void Load(BinaryReader br)
  {
    invite_gift_cnt = br.ReadInt32();
    pop_gift_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_oldplayer_gift_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_oldplayer_gift_info"; } }
  public override string Name { get { return "tos_player_get_oldplayer_gift_info"; } }
  public override int Id { get { return 0x20075; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_use_giftbag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "use_giftbag"; } }
  public override string Name { get { return "toc_player_use_giftbag"; } }
  public override int Id { get { return 0x20076; } }

  public override void Load(BinaryReader br)
  {
    giftbag_id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_item_info_toc[] ary2 = new p_item_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_item_info_toc cls4 = new p_item_info_toc();
      cls4.item_id = br.ReadInt32();
      cls4.item_cnt = br.ReadInt32();
      cls4.days = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    item_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_check_online_time_target
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "check_online_time_target"; } }
  public override string Name { get { return "tos_player_check_online_time_target"; } }
  public override int Id { get { return 0x20077; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_stage_survival_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "stage_survival_data"; } }
  public override string Name { get { return "toc_player_stage_survival_data"; } }
  public override int Id { get { return 0x20078; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_stage_survival_data.stage_survival_info[] ary2 = new toc_player_stage_survival_data.stage_survival_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_stage_survival_data.stage_survival_info cls4 = new toc_player_stage_survival_data.stage_survival_info();
      cls4.stage_id = br.ReadInt32();
      cls4.best_star = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    max_stage = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_stage_survival_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "stage_survival_data"; } }
  public override string Name { get { return "tos_player_stage_survival_data"; } }
  public override int Id { get { return 0x20078; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_stage_star
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "stage_star"; } }
  public override string Name { get { return "toc_player_stage_star"; } }
  public override int Id { get { return 0x20079; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    stage = br.ReadInt32();
    star = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_refresh_online_time
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "refresh_online_time"; } }
  public override string Name { get { return "tos_player_refresh_online_time"; } }
  public override int Id { get { return 0x2007a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_game_time_today
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "game_time_today"; } }
  public override string Name { get { return "toc_player_game_time_today"; } }
  public override int Id { get { return 0x2007b; } }

  public override void Load(BinaryReader br)
  {
    game_time = br.ReadInt32();
    anti_addiction = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_anti_addiction
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "anti_addiction"; } }
  public override string Name { get { return "toc_player_anti_addiction"; } }
  public override int Id { get { return 0x2007c; } }

  public override void Load(BinaryReader br)
  {
    anti_addiction = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_anti_addiction
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "anti_addiction"; } }
  public override string Name { get { return "tos_player_anti_addiction"; } }
  public override int Id { get { return 0x2007c; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.player_name);
    SaveString(bw, this.id);
    bw.Write(this.sid);
  }
}

partial class tos_player_set_anti_addiction_rate
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_anti_addiction_rate"; } }
  public override string Name { get { return "tos_player_set_anti_addiction_rate"; } }
  public override int Id { get { return 0x2007d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_options
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "options"; } }
  public override string Name { get { return "toc_player_options"; } }
  public override int Id { get { return 0x2007e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    option[] ary2 = new option[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      option cls4 = new option();
      cls4.option_type = LoadString(br);
      cls4.option_val = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    options = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_option
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_option"; } }
  public override string Name { get { return "tos_player_set_option"; } }
  public override int Id { get { return 0x2007f; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.option_type);
    bw.Write(this.option_val);
    bw.Write(this.sid);
  }
}

partial class tos_player_del_item
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_item"; } }
  public override string Name { get { return "tos_player_del_item"; } }
  public override int Id { get { return 0x20080; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.uid);
    bw.Write(this.sid);
  }
}

partial class tos_player_quick_join_action
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "quick_join_action"; } }
  public override string Name { get { return "tos_player_quick_join_action"; } }
  public override int Id { get { return 0x20081; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_report
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "report"; } }
  public override string Name { get { return "tos_player_report"; } }
  public override int Id { get { return 0x20082; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.be_report_id);
    SaveString(bw, this.be_report_name);
    bw.Write(this.report_type);
    SaveString(bw, this.content);
    SaveString(bw, this.proof);
    SaveString(bw, this.proof2);
    SaveString(bw, this.proof3);
    bw.Write(this.sid);
  }
}

partial class toc_player_tag_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "tag_list"; } }
  public override string Name { get { return "toc_player_tag_list"; } }
  public override int Id { get { return 0x20083; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_player_tag_list.p_tag[] ary2 = new toc_player_tag_list.p_tag[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_player_tag_list.p_tag cls4 = new toc_player_tag_list.p_tag();
      cls4.tag_id = br.ReadInt32();
      cls4.value = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    tag_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_tag_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "tag_list"; } }
  public override string Name { get { return "tos_player_tag_list"; } }
  public override int Id { get { return 0x20083; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_update_tag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "update_tag"; } }
  public override string Name { get { return "toc_player_update_tag"; } }
  public override int Id { get { return 0x20084; } }

  public override void Load(BinaryReader br)
  {
    tag_id = br.ReadInt32();
    value = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_update_tag
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "update_tag"; } }
  public override string Name { get { return "tos_player_update_tag"; } }
  public override int Id { get { return 0x20084; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.tag_id);
    bw.Write(this.value);
    bw.Write(this.sid);
  }
}

partial class tos_player_log_misc
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "log_misc"; } }
  public override string Name { get { return "tos_player_log_misc"; } }
  public override int Id { get { return 0x20085; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.log_typ);
    bw.Write(this.log_n1);
    bw.Write(this.log_n2);
    SaveString(bw, this.log_s1);
    SaveString(bw, this.log_s2);
    bw.Write(this.sid);
  }
}

partial class toc_player_item_transform
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "item_transform"; } }
  public override string Name { get { return "toc_player_item_transform"; } }
  public override int Id { get { return 0x20086; } }

  public override void Load(BinaryReader br)
  {
    ItemInfo cls1 = new ItemInfo();
    cls1.ID = br.ReadInt32();
    cls1.cnt = br.ReadInt32();
    cls1.day = br.ReadSingle();
    from = cls1;
    ItemInfo cls2 = new ItemInfo();
    cls2.ID = br.ReadInt32();
    cls2.cnt = br.ReadInt32();
    cls2.day = br.ReadSingle();
    to = cls2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_flop_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "flop_reward"; } }
  public override string Name { get { return "toc_player_flop_reward"; } }
  public override int Id { get { return 0x20087; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    ItemInfo[] ary2 = new ItemInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ItemInfo cls4 = new ItemInfo();
      cls4.ID = br.ReadInt32();
      cls4.cnt = br.ReadInt32();
      cls4.day = br.ReadSingle();
      ary2[idx3] = cls4;
    }
    item_list = ary2;
    int len5 = br.ReadUInt16();
    System.Int32[] ary6 = new System.Int32[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt32();
    }
    pos_list = ary6;
    int len8 = br.ReadUInt16();
    System.Int32[] ary9 = new System.Int32[len8];
    for (int idx10 = 0; idx10 < len8; idx10++)
    {
      ary9[idx10] = br.ReadInt32();
    }
    color_list = ary9;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_flop_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "flop_reward"; } }
  public override string Name { get { return "tos_player_flop_reward"; } }
  public override int Id { get { return 0x20087; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pos);
    bw.Write(this.sid);
  }
}

partial class toc_player_new_lottery_show
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_lottery_show"; } }
  public override string Name { get { return "toc_player_new_lottery_show"; } }
  public override int Id { get { return 0x20088; } }

  public override void Load(BinaryReader br)
  {
    type = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    lucky_list = ary2;
    int len4 = br.ReadUInt16();
    System.String[] ary5 = new System.String[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      ary5[idx6] = LoadString(br);
    }
    recent_list = ary5;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_new_lottery_show
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "new_lottery_show"; } }
  public override string Name { get { return "tos_player_new_lottery_show"; } }
  public override int Id { get { return 0x20088; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.type);
    bw.Write(this.sid);
  }
}

partial class toc_player_close_agent
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "close_agent"; } }
  public override string Name { get { return "toc_player_close_agent"; } }
  public override int Id { get { return 0x20089; } }

  public override void Load(BinaryReader br)
  {
    reason = br.ReadInt32();
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_player_hero_card_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "hero_card_data"; } }
  public override string Name { get { return "toc_player_hero_card_data"; } }
  public override int Id { get { return 0x2008a; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_hero_card_info[] ary2 = new p_hero_card_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_hero_card_info cls4 = new p_hero_card_info();
      cls4.hero_type = br.ReadInt32();
      cls4.end_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    cards = ary2;
    reward_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_hero_card_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "hero_card_data"; } }
  public override string Name { get { return "tos_player_hero_card_data"; } }
  public override int Id { get { return 0x2008a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_take_hero_card_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "take_hero_card_reward"; } }
  public override string Name { get { return "toc_player_take_hero_card_reward"; } }
  public override int Id { get { return 0x2008b; } }

  public override void Load(BinaryReader br)
  {
    reward_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_take_hero_card_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "take_hero_card_reward"; } }
  public override string Name { get { return "tos_player_take_hero_card_reward"; } }
  public override int Id { get { return 0x2008b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_set_titles
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_titles"; } }
  public override string Name { get { return "toc_player_set_titles"; } }
  public override int Id { get { return 0x2008c; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_title_info[] ary2 = new p_title_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_title_info cls4 = new p_title_info();
      cls4.title = LoadString(br);
      cls4.cnt = br.ReadInt32();
      cls4.type = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    titles = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_titles
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_titles"; } }
  public override string Name { get { return "tos_player_set_titles"; } }
  public override int Id { get { return 0x2008c; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.titles;
    Assert(ary1 != null, "null value: this.titles");
    Assert(ary1.Length <= short.MaxValue, "this.titles length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      SaveString(bw, ary3.title);
      bw.Write(ary3.cnt);
      bw.Write(ary3.type);
    }
    bw.Write(this.sid);
  }
}

partial class toc_player_subtype_room_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "subtype_room_list"; } }
  public override string Name { get { return "toc_player_subtype_room_list"; } }
  public override int Id { get { return 0x2008d; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_room_info[] ary2 = new p_room_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_room_info cls4 = new p_room_info();
      cls4.id = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.leader_id = br.ReadInt64();
      cls4.leader = LoadString(br);
      cls4.leader_hero = br.ReadInt32();
      cls4.leader_device = LoadString(br);
      cls4.map = br.ReadInt32();
      cls4.rule = LoadString(br);
      cls4.player_count = br.ReadInt32();
      cls4.max_player = br.ReadInt32();
      cls4.spectators_count = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.password = br.ReadBoolean();
      cls4.cur_round = br.ReadInt32();
      cls4.max_round = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.state = br.ReadInt32();
      cls4.level_limit = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_subtype_room_list
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "subtype_room_list"; } }
  public override string Name { get { return "tos_player_subtype_room_list"; } }
  public override int Id { get { return 0x2008d; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.subtype);
    bw.Write(this.sid);
  }
}

partial class toc_player_diamond2eventcoin_002
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "diamond2eventcoin_002"; } }
  public override string Name { get { return "toc_player_diamond2eventcoin_002"; } }
  public override int Id { get { return 0x2008e; } }

  public override void Load(BinaryReader br)
  {
    count = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_diamond2eventcoin_002
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "diamond2eventcoin_002"; } }
  public override string Name { get { return "tos_player_diamond2eventcoin_002"; } }
  public override int Id { get { return 0x2008e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.count);
    bw.Write(this.sid);
  }
}

partial class toc_player_available_channel
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "available_channel"; } }
  public override string Name { get { return "toc_player_available_channel"; } }
  public override int Id { get { return 0x2008f; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_timelimit_gift
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift"; } }
  public override string Name { get { return "toc_player_timelimit_gift"; } }
  public override int Id { get { return 0x20090; } }

  public override void Load(BinaryReader br)
  {
    gift_id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_timelimit_item[] ary2 = new p_timelimit_item[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_timelimit_item cls4 = new p_timelimit_item();
      cls4.reward_id = LoadString(br);
      cls4.buy = br.ReadInt32();
      cls4.price = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    item_list = ary2;
    second_left = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_timelimit_gift
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift"; } }
  public override string Name { get { return "tos_player_timelimit_gift"; } }
  public override int Id { get { return 0x20090; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_timelimit_gift_buy
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift_buy"; } }
  public override string Name { get { return "toc_player_timelimit_gift_buy"; } }
  public override int Id { get { return 0x20091; } }

  public override void Load(BinaryReader br)
  {
    reward_id = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_timelimit_gift_buy
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift_buy"; } }
  public override string Name { get { return "tos_player_timelimit_gift_buy"; } }
  public override int Id { get { return 0x20091; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.gift_id);
    SaveString(bw, this.reward_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_timelimit_gift_time
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift_time"; } }
  public override string Name { get { return "toc_player_timelimit_gift_time"; } }
  public override int Id { get { return 0x20092; } }

  public override void Load(BinaryReader br)
  {
    timestamp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_timelimit_gift_time
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "timelimit_gift_time"; } }
  public override string Name { get { return "tos_player_timelimit_gift_time"; } }
  public override int Id { get { return 0x20092; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_foundation_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "foundation_data"; } }
  public override string Name { get { return "toc_player_foundation_data"; } }
  public override int Id { get { return 0x20093; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_foundation_info[] ary2 = new p_foundation_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_foundation_info cls4 = new p_foundation_info();
      cls4.foundation_id = br.ReadInt32();
      cls4.rewarded = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    data = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_foundation_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "foundation_data"; } }
  public override string Name { get { return "tos_player_foundation_data"; } }
  public override int Id { get { return 0x20093; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_get_foundation_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_foundation_reward"; } }
  public override string Name { get { return "toc_player_get_foundation_reward"; } }
  public override int Id { get { return 0x20094; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_get_foundation_reward
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "get_foundation_reward"; } }
  public override string Name { get { return "tos_player_get_foundation_reward"; } }
  public override int Id { get { return 0x20094; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_player_set_hero_sex
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_hero_sex"; } }
  public override string Name { get { return "toc_player_set_hero_sex"; } }
  public override int Id { get { return 0x20095; } }

  public override void Load(BinaryReader br)
  {
    sex = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_hero_sex
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_hero_sex"; } }
  public override string Name { get { return "tos_player_set_hero_sex"; } }
  public override int Id { get { return 0x20095; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sex);
    bw.Write(this.sid);
  }
}

partial class toc_player_weapon_accessory_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_accessory_data"; } }
  public override string Name { get { return "toc_player_weapon_accessory_data"; } }
  public override int Id { get { return 0x20096; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_weapon_accessory_info[] ary2 = new p_weapon_accessory_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_weapon_accessory_info cls4 = new p_weapon_accessory_info();
      cls4.weapon = br.ReadInt32();
      int len5 = br.ReadUInt16();
      accessory[] ary6 = new accessory[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        accessory cls8 = new accessory();
        cls8.pos = br.ReadInt32();
        cls8.item_id = br.ReadInt32();
        int len9 = br.ReadUInt16();
        System.Int32[] ary10 = new System.Int32[len9];
        for (int idx11 = 0; idx11 < len9; idx11++)
        {
          ary10[idx11] = br.ReadInt32();
        }
        cls8.accessory_values = ary10;
        ary6[idx7] = cls8;
      }
      cls4.accessories = ary6;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_weapon_accessory_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_accessory_data"; } }
  public override string Name { get { return "tos_player_weapon_accessory_data"; } }
  public override int Id { get { return 0x20096; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_weapon_accessories
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "weapon_accessories"; } }
  public override string Name { get { return "toc_player_weapon_accessories"; } }
  public override int Id { get { return 0x20097; } }

  public override void Load(BinaryReader br)
  {
    weapon = br.ReadInt32();
    int len1 = br.ReadUInt16();
    accessory[] ary2 = new accessory[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      accessory cls4 = new accessory();
      cls4.pos = br.ReadInt32();
      cls4.item_id = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Int32[] ary6 = new System.Int32[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadInt32();
      }
      cls4.accessory_values = ary6;
      ary2[idx3] = cls4;
    }
    accessories = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_phased_purchase_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phased_purchase_data"; } }
  public override string Name { get { return "toc_player_phased_purchase_data"; } }
  public override int Id { get { return 0x20098; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_phased_purchase_item[] ary2 = new p_phased_purchase_item[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_phased_purchase_item cls4 = new p_phased_purchase_item();
      cls4.item_id = br.ReadInt32();
      cls4.item_uid = br.ReadInt32();
      cls4.phased_number = br.ReadInt32();
      cls4.total_phased_number = br.ReadInt32();
      cls4.mall_id = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    phased_purchase_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_player_phased_purchase_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phased_purchase_data"; } }
  public override string Name { get { return "tos_player_phased_purchase_data"; } }
  public override int Id { get { return 0x20098; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_phased_item_update
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phased_item_update"; } }
  public override string Name { get { return "toc_player_phased_item_update"; } }
  public override int Id { get { return 0x20099; } }

  public override void Load(BinaryReader br)
  {
    p_phased_purchase_item cls1 = new p_phased_purchase_item();
    cls1.item_id = br.ReadInt32();
    cls1.item_uid = br.ReadInt32();
    cls1.phased_number = br.ReadInt32();
    cls1.total_phased_number = br.ReadInt32();
    cls1.mall_id = br.ReadInt32();
    item = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_phased_item_new
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phased_item_new"; } }
  public override string Name { get { return "toc_player_phased_item_new"; } }
  public override int Id { get { return 0x2009a; } }

  public override void Load(BinaryReader br)
  {
    p_phased_purchase_item cls1 = new p_phased_purchase_item();
    cls1.item_id = br.ReadInt32();
    cls1.item_uid = br.ReadInt32();
    cls1.phased_number = br.ReadInt32();
    cls1.total_phased_number = br.ReadInt32();
    cls1.mall_id = br.ReadInt32();
    item = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_player_phased_item_del
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phased_item_del"; } }
  public override string Name { get { return "toc_player_phased_item_del"; } }
  public override int Id { get { return 0x2009b; } }

  public override void Load(BinaryReader br)
  {
    item_uid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_player_unequip_decoration
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "unequip_decoration"; } }
  public override string Name { get { return "toc_player_unequip_decoration"; } }
  public override int Id { get { return 0x2009c; } }

  public override void Load(BinaryReader br)
  {
    group = br.ReadInt32();
    part = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_unequip_decoration
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "unequip_decoration"; } }
  public override string Name { get { return "tos_player_unequip_decoration"; } }
  public override int Id { get { return 0x2009c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.group);
    bw.Write(this.part);
    bw.Write(this.sid);
  }
}

partial class toc_player_icon_url_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "icon_url_data"; } }
  public override string Name { get { return "toc_player_icon_url_data"; } }
  public override int Id { get { return 0x2009d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    url_list = ary2;
    cur_index = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_icon_url_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "icon_url_data"; } }
  public override string Name { get { return "tos_player_icon_url_data"; } }
  public override int Id { get { return 0x2009d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_add_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_icon_url"; } }
  public override string Name { get { return "toc_player_add_icon_url"; } }
  public override int Id { get { return 0x2009e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    url_list = ary2;
    cur_index = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_add_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "add_icon_url"; } }
  public override string Name { get { return "tos_player_add_icon_url"; } }
  public override int Id { get { return 0x2009e; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.icon_url);
    bw.Write(this.sid);
  }
}

partial class toc_player_set_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_icon_url"; } }
  public override string Name { get { return "toc_player_set_icon_url"; } }
  public override int Id { get { return 0x2009f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    url_list = ary2;
    cur_index = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_set_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "set_icon_url"; } }
  public override string Name { get { return "tos_player_set_icon_url"; } }
  public override int Id { get { return 0x2009f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.new_index);
    bw.Write(this.sid);
  }
}

partial class toc_player_del_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_icon_url"; } }
  public override string Name { get { return "toc_player_del_icon_url"; } }
  public override int Id { get { return 0x200a0; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    url_list = ary2;
    cur_index = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_del_icon_url
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "del_icon_url"; } }
  public override string Name { get { return "tos_player_del_icon_url"; } }
  public override int Id { get { return 0x200a0; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.del_index);
    bw.Write(this.sid);
  }
}

partial class toc_player_match_punish_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "match_punish_data"; } }
  public override string Name { get { return "toc_player_match_punish_data"; } }
  public override int Id { get { return 0x200a1; } }

  public override void Load(BinaryReader br)
  {
    clear_time = br.ReadInt32();
    daily_dec_time = br.ReadInt32();
    punish_value = br.ReadInt32();
    release_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_match_punish_data
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "match_punish_data"; } }
  public override string Name { get { return "tos_player_match_punish_data"; } }
  public override int Id { get { return 0x200a1; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_social_joingroup
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_joingroup"; } }
  public override string Name { get { return "tos_player_social_joingroup"; } }
  public override int Id { get { return 0x200a2; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.platform);
    bw.Write(this.sid);
  }
}

partial class toc_player_social_join_media
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_join_media"; } }
  public override string Name { get { return "toc_player_social_join_media"; } }
  public override int Id { get { return 0x200a3; } }

  public override void Load(BinaryReader br)
  {
    join_reward = br.ReadBoolean();
    share_reward = br.ReadBoolean();
    bind_invite_code = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_social_join_media
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_join_media"; } }
  public override string Name { get { return "tos_player_social_join_media"; } }
  public override int Id { get { return 0x200a3; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_player_social_share_game
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_share_game"; } }
  public override string Name { get { return "tos_player_social_share_game"; } }
  public override int Id { get { return 0x200a4; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_social_invitor_name
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_invitor_name"; } }
  public override string Name { get { return "toc_player_social_invitor_name"; } }
  public override int Id { get { return 0x200a5; } }

  public override void Load(BinaryReader br)
  {
    invitor_name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_player_social_invitor_name
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_invitor_name"; } }
  public override string Name { get { return "tos_player_social_invitor_name"; } }
  public override int Id { get { return 0x200a5; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.sid);
  }
}

partial class toc_player_social_bind_invite_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_bind_invite_code"; } }
  public override string Name { get { return "toc_player_social_bind_invite_code"; } }
  public override int Id { get { return 0x200a6; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_social_bind_invite_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_bind_invite_code"; } }
  public override string Name { get { return "tos_player_social_bind_invite_code"; } }
  public override int Id { get { return 0x200a6; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    SaveString(bw, this.device_code);
    bw.Write(this.sid);
  }
}

partial class toc_player_phone_register
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_register"; } }
  public override string Name { get { return "toc_player_phone_register"; } }
  public override int Id { get { return 0x200a7; } }

  public override void Load(BinaryReader br)
  {
    state = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_phone_register
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_register"; } }
  public override string Name { get { return "tos_player_phone_register"; } }
  public override int Id { get { return 0x200a7; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.phone_num);
    bw.Write(this.sid);
  }
}

partial class toc_player_phone_verify
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_verify"; } }
  public override string Name { get { return "toc_player_phone_verify"; } }
  public override int Id { get { return 0x200a8; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_player_phone_verify
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_verify"; } }
  public override string Name { get { return "tos_player_phone_verify"; } }
  public override int Id { get { return 0x200a8; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.phone_num);
    SaveString(bw, this.code);
    bw.Write(this.sid);
  }
}

partial class toc_player_phone_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_code"; } }
  public override string Name { get { return "toc_player_phone_code"; } }
  public override int Id { get { return 0x200a9; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_player_phone_code
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_code"; } }
  public override string Name { get { return "tos_player_phone_code"; } }
  public override int Id { get { return 0x200a9; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.phone_num);
    bw.Write(this.sid);
  }
}

partial class toc_player_phone_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_info"; } }
  public override string Name { get { return "toc_player_phone_info"; } }
  public override int Id { get { return 0x200aa; } }

  public override void Load(BinaryReader br)
  {
    phone_num = LoadString(br);
    reg_time = br.ReadUInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_phone_info
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "phone_info"; } }
  public override string Name { get { return "tos_player_phone_info"; } }
  public override int Id { get { return 0x200aa; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_player_social_invite_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_invite_friend"; } }
  public override string Name { get { return "toc_player_social_invite_friend"; } }
  public override int Id { get { return 0x200ab; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class tos_player_social_invite_friend
{
  public override string Typ { get { return "player"; } }
  public override string Key { get { return "social_invite_friend"; } }
  public override string Name { get { return "tos_player_social_invite_friend"; } }
  public override int Id { get { return 0x200ab; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.friend_id);
    bw.Write(this.sid);
  }
}


partial class toc_item_new
{
  public override string Typ { get { return "item"; } }
  public override string Key { get { return "new"; } }
  public override string Name { get { return "toc_item_new"; } }
  public override int Id { get { return 0x30001; } }

  public override void Load(BinaryReader br)
  {
    p_item cls1 = new p_item();
    cls1.uid = br.ReadInt32();
    cls1.item_id = br.ReadInt32();
    cls1.item_cnt = br.ReadInt32();
    cls1.create_time = br.ReadUInt32();
    cls1.time_limit = br.ReadUInt32();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.accessory_values = ary3;
    cls1.durability = br.ReadInt32();
    cls1.is_arsenal = br.ReadBoolean();
    item = cls1;
    reason = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_item_update
{
  public override string Typ { get { return "item"; } }
  public override string Key { get { return "update"; } }
  public override string Name { get { return "toc_item_update"; } }
  public override int Id { get { return 0x30002; } }

  public override void Load(BinaryReader br)
  {
    p_item cls1 = new p_item();
    cls1.uid = br.ReadInt32();
    cls1.item_id = br.ReadInt32();
    cls1.item_cnt = br.ReadInt32();
    cls1.create_time = br.ReadUInt32();
    cls1.time_limit = br.ReadUInt32();
    int len2 = br.ReadUInt16();
    System.Int32[] ary3 = new System.Int32[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadInt32();
    }
    cls1.accessory_values = ary3;
    cls1.durability = br.ReadInt32();
    cls1.is_arsenal = br.ReadBoolean();
    item = cls1;
    reason = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_item_del
{
  public override string Typ { get { return "item"; } }
  public override string Key { get { return "del"; } }
  public override string Name { get { return "toc_item_del"; } }
  public override int Id { get { return 0x30003; } }

  public override void Load(BinaryReader br)
  {
    uid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}


partial class toc_fight_base
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "base"; } }
  public override string Name { get { return "toc_fight_base"; } }
  public override int Id { get { return 0x40001; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_msg
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "msg"; } }
  public override string Name { get { return "toc_fight_msg"; } }
  public override int Id { get { return 0x40002; } }

  public override void Load(BinaryReader br)
  {
    message = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_leave
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "leave"; } }
  public override string Name { get { return "toc_fight_leave"; } }
  public override int Id { get { return 0x40003; } }

  public override void Load(BinaryReader br)
  {
    val = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_move
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "move"; } }
  public override string Name { get { return "toc_fight_move"; } }
  public override int Id { get { return 0x40004; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int16[] ary2 = new System.Int16[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt16();
    }
    pos = ary2;
    anglePitch = br.ReadByte();
    angleY = br.ReadByte();
    moveState = br.ReadByte();
    moveSpeed = br.ReadByte();
    clientSpeed = br.ReadByte();
    time = br.ReadInt16();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_move
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "move"; } }
  public override string Name { get { return "tos_fight_move"; } }
  public override int Id { get { return 0x40004; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.pos;
    Assert(ary1 != null, "null value: this.pos");
    Assert(ary1.Length <= short.MaxValue, "this.pos length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    bw.Write(this.anglePitch);
    bw.Write(this.angleY);
    bw.Write(this.moveState);
    bw.Write(this.clientSpeed);
    bw.Write(this.time);
    bw.Write(this.timeStamp);
    bw.Write(this.sid);
  }
}

partial class tos_fight_switch_weapon
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "switch_weapon"; } }
  public override string Name { get { return "tos_fight_switch_weapon"; } }
  public override int Id { get { return 0x40005; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.weapon);
    bw.Write(this.atk_type);
    bw.Write(this.sid);
  }
}

partial class toc_fight_equip_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "equip_info"; } }
  public override string Name { get { return "toc_fight_equip_info"; } }
  public override int Id { get { return 0x40006; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_fight_equip_info.EquipInfo[] ary2 = new toc_fight_equip_info.EquipInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_fight_equip_info.EquipInfo cls4 = new toc_fight_equip_info.EquipInfo();
      cls4.type = LoadString(br);
      cls4.itemid = br.ReadInt32();
      cls4.cur_count = br.ReadInt32();
      cls4.ext_count = br.ReadInt32();
      cls4.adv_count = br.ReadInt32();
      cls4.sub_count = br.ReadInt32();
      cls4.max_count = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Int32[] ary6 = new System.Int32[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadInt32();
      }
      cls4.awaken_props = ary6;
      ary2[idx3] = cls4;
    }
    list = ary2;
    weapon = LoadString(br);
    weaponVer = br.ReadInt32();
    unswitch = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_fire
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "fire"; } }
  public override string Name { get { return "toc_fight_fire"; } }
  public override int Id { get { return 0x40007; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_hit_info_toc[] ary2 = new p_hit_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_hit_info_toc cls4 = new p_hit_info_toc();
      cls4.actorId = br.ReadInt32();
      cls4.bodyPart = br.ReadInt32();
      cls4.life = br.ReadInt32();
      cls4.armor = br.ReadInt32();
      cls4.die = br.ReadBoolean();
      cls4.hurtType = br.ReadByte();
      ary2[idx3] = cls4;
    }
    hitList = ary2;
    int len5 = br.ReadUInt16();
    System.Int16[] ary6 = new System.Int16[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt16();
    }
    cameraRot = ary6;
    UnityEngine.Vector3 cls8 = new UnityEngine.Vector3();
    cls8.x = br.ReadSingle();
    cls8.y = br.ReadSingle();
    cls8.z = br.ReadSingle();
    hitPos = cls8;
    notPlayAni = br.ReadByte();
    atkType = br.ReadInt32();
    int len9 = br.ReadUInt16();
    System.Int16[] ary10 = new System.Int16[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      ary10[idx11] = br.ReadInt16();
    }
    firePos = ary10;
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_fire
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "fire"; } }
  public override string Name { get { return "tos_fight_fire"; } }
  public override int Id { get { return 0x40007; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.hitList;
    Assert(ary1 != null, "null value: this.hitList");
    Assert(ary1.Length <= short.MaxValue, "this.hitList length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      Assert(ary3.Length <= short.MaxValue, "ary1[idx2] length out of range:", ary3.Length);
      bw.Write((ushort)ary3.Length);
      for (int idx4 = 0; idx4 < ary3.Length; idx4++)
      {
        var ary5 = ary3[idx4];
        Assert(ary5 != null, "null value: ary3[idx4]");
        bw.Write(ary5.matType);
        bw.Write(ary5.actorId);
        bw.Write(ary5.bodyPart);
        var ary6 = ary5.pos;
        bw.Write(ary6.x);
        bw.Write(ary6.y);
        bw.Write(ary6.z);
      }
    }
    var ary7 = this.cameraRot;
    Assert(ary7 != null, "null value: this.cameraRot");
    Assert(ary7.Length <= short.MaxValue, "this.cameraRot length out of range:", ary7.Length);
    bw.Write((ushort)ary7.Length);
    for (int idx8 = 0; idx8 < ary7.Length; idx8++)
    {
      bw.Write(ary7[idx8]);
    }
    var ary9 = this.hitPos;
    bw.Write(ary9.x);
    bw.Write(ary9.y);
    bw.Write(ary9.z);
    bw.Write(this.notPlayAni);
    bw.Write(this.isZoom);
    bw.Write(this.weaponVer);
    bw.Write(this.bulletCount);
    bw.Write(this.relyBulletCount);
    bw.Write(this.atkType);
    var ary10 = this.firePos;
    bw.Write(ary10.x);
    bw.Write(ary10.y);
    bw.Write(ary10.z);
    bw.Write(this.timeStamp);
    bw.Write(this.sid);
  }
}

partial class tos_fight_pickup_drop_item
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "pickup_drop_item"; } }
  public override string Name { get { return "tos_fight_pickup_drop_item"; } }
  public override int Id { get { return 0x40008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.uid);
    bw.Write(this.sid);
  }
}

partial class toc_fight_del_drop_item
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "del_drop_item"; } }
  public override string Name { get { return "toc_fight_del_drop_item"; } }
  public override int Id { get { return 0x40009; } }

  public override void Load(BinaryReader br)
  {
    uid = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_add_drop_item
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "add_drop_item"; } }
  public override string Name { get { return "toc_fight_add_drop_item"; } }
  public override int Id { get { return 0x4000a; } }

  public override void Load(BinaryReader br)
  {
    DropInfo cls1 = new DropInfo();
    cls1.uid = br.ReadInt32();
    int len2 = br.ReadUInt16();
    System.Single[] ary3 = new System.Single[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadSingle();
    }
    cls1.pos = ary3;
    cls1.drop_time = br.ReadDouble();
    cls1.drop_id = br.ReadInt32();
    drop = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_drop_list
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "drop_list"; } }
  public override string Name { get { return "toc_fight_drop_list"; } }
  public override int Id { get { return 0x4000b; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    DropInfo[] ary2 = new DropInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      DropInfo cls4 = new DropInfo();
      cls4.uid = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Single[] ary6 = new System.Single[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadSingle();
      }
      cls4.pos = ary6;
      cls4.drop_time = br.ReadDouble();
      cls4.drop_id = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    drop_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_start_set_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_set_bomb"; } }
  public override string Name { get { return "toc_fight_start_set_bomb"; } }
  public override int Id { get { return 0x4000c; } }

  public override void Load(BinaryReader br)
  {
    point = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_start_set_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_set_bomb"; } }
  public override string Name { get { return "tos_fight_start_set_bomb"; } }
  public override int Id { get { return 0x4000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.point);
    bw.Write(this.x);
    bw.Write(this.y);
    bw.Write(this.z);
    bw.Write(this.sid);
  }
}

partial class toc_fight_stop_set_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_set_bomb"; } }
  public override string Name { get { return "toc_fight_stop_set_bomb"; } }
  public override int Id { get { return 0x4000d; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_stop_set_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_set_bomb"; } }
  public override string Name { get { return "tos_fight_stop_set_bomb"; } }
  public override int Id { get { return 0x4000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_set_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "set_bomb"; } }
  public override string Name { get { return "toc_fight_set_bomb"; } }
  public override int Id { get { return 0x4000e; } }

  public override void Load(BinaryReader br)
  {
    set_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_start_defuse_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_defuse_bomb"; } }
  public override string Name { get { return "toc_fight_start_defuse_bomb"; } }
  public override int Id { get { return 0x4000f; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_start_defuse_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_defuse_bomb"; } }
  public override string Name { get { return "tos_fight_start_defuse_bomb"; } }
  public override int Id { get { return 0x4000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_stop_defuse_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_defuse_bomb"; } }
  public override string Name { get { return "toc_fight_stop_defuse_bomb"; } }
  public override int Id { get { return 0x40010; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_stop_defuse_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_defuse_bomb"; } }
  public override string Name { get { return "tos_fight_stop_defuse_bomb"; } }
  public override int Id { get { return 0x40010; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_defuse_bomb
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "defuse_bomb"; } }
  public override string Name { get { return "toc_fight_defuse_bomb"; } }
  public override int Id { get { return 0x40011; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_bomb_burst
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "bomb_burst"; } }
  public override string Name { get { return "toc_fight_bomb_burst"; } }
  public override int Id { get { return 0x40012; } }

  public override void Load(BinaryReader br)
  {
    point = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_fireg
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "fireg"; } }
  public override string Name { get { return "tos_fight_fireg"; } }
  public override int Id { get { return 0x40013; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.hitList;
    Assert(ary1 != null, "null value: this.hitList");
    Assert(ary1.Length <= short.MaxValue, "this.hitList length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      bw.Write(ary3.id);
      var ary4 = ary3.pos;
      bw.Write(ary4.x);
      bw.Write(ary4.y);
      bw.Write(ary4.z);
    }
    var ary5 = this.fromPos;
    bw.Write(ary5.x);
    bw.Write(ary5.y);
    bw.Write(ary5.z);
    bw.Write(this.index);
    var ary6 = this.firePos;
    bw.Write(ary6.x);
    bw.Write(ary6.y);
    bw.Write(ary6.z);
    bw.Write(this.sid);
  }
}

partial class toc_fight_atk
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "atk"; } }
  public override string Name { get { return "toc_fight_atk"; } }
  public override int Id { get { return 0x40014; } }

  public override void Load(BinaryReader br)
  {
    firePose = br.ReadInt32();
    UnityEngine.Vector3 cls1 = new UnityEngine.Vector3();
    cls1.x = br.ReadSingle();
    cls1.y = br.ReadSingle();
    cls1.z = br.ReadSingle();
    fireDir = cls1;
    index = br.ReadInt32();
    atkType = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_atk
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "atk"; } }
  public override string Name { get { return "tos_fight_atk"; } }
  public override int Id { get { return 0x40014; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.firePose);
    var ary1 = this.fireDir;
    bw.Write(ary1.x);
    bw.Write(ary1.y);
    bw.Write(ary1.z);
    bw.Write(this.index);
    bw.Write(this.atkType);
    bw.Write(this.sid);
  }
}

partial class toc_fight_reload
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "reload"; } }
  public override string Name { get { return "toc_fight_reload"; } }
  public override int Id { get { return 0x40015; } }

  public override void Load(BinaryReader br)
  {
    reloadTime = br.ReadSingle();
    reloadConut = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_reload
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "reload"; } }
  public override string Name { get { return "tos_fight_reload"; } }
  public override int Id { get { return 0x40015; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.reloadTime);
    bw.Write(this.reloadConut);
    bw.Write(this.sid);
  }
}

partial class tos_fight_reload_done
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "reload_done"; } }
  public override string Name { get { return "tos_fight_reload_done"; } }
  public override int Id { get { return 0x40016; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weaponVer);
    bw.Write(this.onlyOne);
    bw.Write(this.sid);
  }
}

partial class toc_fight_jump
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "jump"; } }
  public override string Name { get { return "toc_fight_jump"; } }
  public override int Id { get { return 0x40017; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    pos = ary2;
    type = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_jump
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "jump"; } }
  public override string Name { get { return "tos_fight_jump"; } }
  public override int Id { get { return 0x40017; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.pos;
    Assert(ary1 != null, "null value: this.pos");
    Assert(ary1.Length <= short.MaxValue, "this.pos length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    bw.Write(this.type);
    bw.Write(this.sid);
  }
}

partial class toc_fight_stand
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stand"; } }
  public override string Name { get { return "toc_fight_stand"; } }
  public override int Id { get { return 0x40018; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_stand
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stand"; } }
  public override string Name { get { return "tos_fight_stand"; } }
  public override int Id { get { return 0x40018; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_crouch
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "crouch"; } }
  public override string Name { get { return "toc_fight_crouch"; } }
  public override int Id { get { return 0x40019; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_crouch
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "crouch"; } }
  public override string Name { get { return "tos_fight_crouch"; } }
  public override int Id { get { return 0x40019; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_fixbullet
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "fixbullet"; } }
  public override string Name { get { return "toc_fight_fixbullet"; } }
  public override int Id { get { return 0x4001a; } }

  public override void Load(BinaryReader br)
  {
    weaponVer = br.ReadInt32();
    bulletCountD = br.ReadInt32();
    relyBulletCountD = br.ReadInt32();
    atkType = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_enter
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "enter"; } }
  public override string Name { get { return "toc_fight_enter"; } }
  public override int Id { get { return 0x4001b; } }

  public override void Load(BinaryReader br)
  {
    myid = br.ReadInt32();
    rhd = br.ReadUInt32();
    rid = br.ReadUInt32();
    type = LoadString(br);
    map = br.ReadInt32();
    rule = LoadString(br);
    auto_aim = br.ReadBoolean();
    server_addr = LoadString(br);
    server_port = br.ReadInt32();
    has_zombie_cloth = br.ReadBoolean();
    has_zombie_blade = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_game_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "game_info"; } }
  public override string Name { get { return "toc_fight_game_info"; } }
  public override int Id { get { return 0x4001c; } }

  public override void Load(BinaryReader br)
  {
    now = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_fight_role[] ary2 = new p_fight_role[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_fight_role cls4 = new p_fight_role();
      cls4.id = br.ReadInt32();
      cls4.pid = br.ReadInt64();
      cls4.icon = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Single[] ary6 = new System.Single[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadSingle();
      }
      cls4.pos = ary6;
      cls4.life = br.ReadInt32();
      cls4.last_armor = br.ReadInt32();
      cls4.last_damage_time = br.ReadInt32();
      cls4.last_act_time = br.ReadInt32();
      cls4.god_end = br.ReadInt32();
      cls4.state = br.ReadInt32();
      cls4.camp = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.immune = br.ReadInt32();
      cls4.actor_type = br.ReadInt32();
      p_fight_prop cls8 = new p_fight_prop();
      cls8.MaxHP = br.ReadInt32();
      cls8.StandMoveSpeed = br.ReadSingle();
      cls8.CrouchMoveSpeed = br.ReadSingle();
      cls8.WeaponId = LoadString(br);
      int len9 = br.ReadUInt16();
      System.String[] ary10 = new System.String[len9];
      for (int idx11 = 0; idx11 < len9; idx11++)
      {
        ary10[idx11] = LoadString(br);
      }
      cls8.DispPart = ary10;
      cls8.ClipSize = br.ReadInt32();
      cls8.ReloadTime = br.ReadSingle();
      cls8.Decelerate = br.ReadSingle();
      cls8.DecelerateTime = br.ReadSingle();
      cls8.AntiDecelerate = br.ReadSingle();
      cls8.MaxArmor = br.ReadInt32();
      cls8.ArmorRecoverWait = br.ReadInt32();
      cls8.ArmorRecoverSpeed = br.ReadInt32();
      cls8.HPRecoverWait = br.ReadInt32();
      cls8.HPRecoverSpeed = br.ReadInt32();
      cls8.Impulse = br.ReadSingle();
      cls8.ImpulseSpeed = br.ReadSingle();
      cls8.HitBackSpeed = br.ReadSingle();
      cls8.HitBackTime = br.ReadSingle();
      cls8.Weight = br.ReadSingle();
      cls8.FireRate = br.ReadSingle();
      cls8.ZoomFireRate = br.ReadSingle();
      cls8.StandSpreadMax = br.ReadSingle();
      cls8.CrouchSpreadMax = br.ReadSingle();
      cls8.RecoilPitchMin = br.ReadSingle();
      cls8.RecoilPitchMax = br.ReadSingle();
      cls8.RecoilYawMin = br.ReadSingle();
      cls8.RecoilYawMax = br.ReadSingle();
      cls8.JumpHeightAdd = br.ReadSingle();
      cls8.PveExtraWeight = br.ReadSingle();
      cls8.ZombieExtraWeight = br.ReadSingle();
      cls8.MaxHitCount = br.ReadInt32();
      cls8.RecoilCountMax = br.ReadInt32();
      cls8.StandSpreadInitial = br.ReadSingle();
      cls8.StandSpreadFireAdd = br.ReadSingle();
      cls8.CrouchSpreadInitial = br.ReadSingle();
      cls8.CrouchSpreadFireAdd = br.ReadSingle();
      cls8.SpreadMovementMax = br.ReadSingle();
      cls8.SpreadJumpMax = br.ReadSingle();
      cls8.SpreadSightsFactor = br.ReadSingle();
      cls8.SpreadDispMin = br.ReadSingle();
      cls4.prop = cls8;
      int len12 = br.ReadUInt16();
      p_actor_buff_toc[] ary13 = new p_actor_buff_toc[len12];
      for (int idx14 = 0; idx14 < len12; idx14++)
      {
        p_actor_buff_toc cls15 = new p_actor_buff_toc();
        cls15.buff_id = br.ReadInt32();
        cls15.start_time = br.ReadInt32();
        cls15.end_time = br.ReadInt32();
        ary13[idx14] = cls15;
      }
      cls4.buff_list = ary13;
      cls4.praised = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.kill = br.ReadInt32();
      cls4.round_kill = br.ReadInt32();
      cls4.death = br.ReadInt32();
      cls4.score = br.ReadInt32();
      cls4.king_cur_seq = br.ReadInt32();
      cls4.king_cur_weapon_kill = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.sacred_weapon_id = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    int len16 = br.ReadUInt16();
    p_fight_spectator[] ary17 = new p_fight_spectator[len16];
    for (int idx18 = 0; idx18 < len16; idx18++)
    {
      p_fight_spectator cls19 = new p_fight_spectator();
      cls19.player_id = br.ReadInt64();
      cls19.name = LoadString(br);
      cls19.icon = br.ReadInt32();
      cls19.corps_id = br.ReadInt32();
      cls19.corps_name = LoadString(br);
      cls19.level = br.ReadInt32();
      ary17[idx18] = cls19;
    }
    spectator_list = ary17;
    p_fight_state cls20 = new p_fight_state();
    cls20.state = br.ReadInt32();
    cls20.round = br.ReadInt32();
    cls20.last_win = br.ReadInt32();
    cls20.final_win = br.ReadInt32();
    state = cls20;
    round_time = br.ReadSingle();
    int len21 = br.ReadUInt16();
    p_camp_info[] ary22 = new p_camp_info[len21];
    for (int idx23 = 0; idx23 < len21; idx23++)
    {
      p_camp_info cls24 = new p_camp_info();
      cls24.camp = br.ReadInt32();
      cls24.win = br.ReadInt32();
      cls24.kill = br.ReadInt32();
      cls24.round_begin_kill = br.ReadInt32();
      cls24.king_camp_score = br.ReadInt32();
      cls24.king_camp_cur_kill = br.ReadInt32();
      ary22[idx23] = cls24;
    }
    camps = ary22;
    king_camp_last_win = br.ReadInt32();
    spectator_switch = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_join
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "join"; } }
  public override string Name { get { return "toc_fight_join"; } }
  public override int Id { get { return 0x4001d; } }

  public override void Load(BinaryReader br)
  {
    p_fight_role cls1 = new p_fight_role();
    cls1.id = br.ReadInt32();
    cls1.pid = br.ReadInt64();
    cls1.icon = br.ReadInt32();
    int len2 = br.ReadUInt16();
    System.Single[] ary3 = new System.Single[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      ary3[idx4] = br.ReadSingle();
    }
    cls1.pos = ary3;
    cls1.life = br.ReadInt32();
    cls1.last_armor = br.ReadInt32();
    cls1.last_damage_time = br.ReadInt32();
    cls1.last_act_time = br.ReadInt32();
    cls1.god_end = br.ReadInt32();
    cls1.state = br.ReadInt32();
    cls1.camp = br.ReadInt32();
    cls1.name = LoadString(br);
    cls1.mvp = br.ReadInt32();
    cls1.gold_ace = br.ReadInt32();
    cls1.silver_ace = br.ReadInt32();
    cls1.immune = br.ReadInt32();
    cls1.actor_type = br.ReadInt32();
    p_fight_prop cls5 = new p_fight_prop();
    cls5.MaxHP = br.ReadInt32();
    cls5.StandMoveSpeed = br.ReadSingle();
    cls5.CrouchMoveSpeed = br.ReadSingle();
    cls5.WeaponId = LoadString(br);
    int len6 = br.ReadUInt16();
    System.String[] ary7 = new System.String[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      ary7[idx8] = LoadString(br);
    }
    cls5.DispPart = ary7;
    cls5.ClipSize = br.ReadInt32();
    cls5.ReloadTime = br.ReadSingle();
    cls5.Decelerate = br.ReadSingle();
    cls5.DecelerateTime = br.ReadSingle();
    cls5.AntiDecelerate = br.ReadSingle();
    cls5.MaxArmor = br.ReadInt32();
    cls5.ArmorRecoverWait = br.ReadInt32();
    cls5.ArmorRecoverSpeed = br.ReadInt32();
    cls5.HPRecoverWait = br.ReadInt32();
    cls5.HPRecoverSpeed = br.ReadInt32();
    cls5.Impulse = br.ReadSingle();
    cls5.ImpulseSpeed = br.ReadSingle();
    cls5.HitBackSpeed = br.ReadSingle();
    cls5.HitBackTime = br.ReadSingle();
    cls5.Weight = br.ReadSingle();
    cls5.FireRate = br.ReadSingle();
    cls5.ZoomFireRate = br.ReadSingle();
    cls5.StandSpreadMax = br.ReadSingle();
    cls5.CrouchSpreadMax = br.ReadSingle();
    cls5.RecoilPitchMin = br.ReadSingle();
    cls5.RecoilPitchMax = br.ReadSingle();
    cls5.RecoilYawMin = br.ReadSingle();
    cls5.RecoilYawMax = br.ReadSingle();
    cls5.JumpHeightAdd = br.ReadSingle();
    cls5.PveExtraWeight = br.ReadSingle();
    cls5.ZombieExtraWeight = br.ReadSingle();
    cls5.MaxHitCount = br.ReadInt32();
    cls5.RecoilCountMax = br.ReadInt32();
    cls5.StandSpreadInitial = br.ReadSingle();
    cls5.StandSpreadFireAdd = br.ReadSingle();
    cls5.CrouchSpreadInitial = br.ReadSingle();
    cls5.CrouchSpreadFireAdd = br.ReadSingle();
    cls5.SpreadMovementMax = br.ReadSingle();
    cls5.SpreadJumpMax = br.ReadSingle();
    cls5.SpreadSightsFactor = br.ReadSingle();
    cls5.SpreadDispMin = br.ReadSingle();
    cls1.prop = cls5;
    int len9 = br.ReadUInt16();
    p_actor_buff_toc[] ary10 = new p_actor_buff_toc[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      p_actor_buff_toc cls12 = new p_actor_buff_toc();
      cls12.buff_id = br.ReadInt32();
      cls12.start_time = br.ReadInt32();
      cls12.end_time = br.ReadInt32();
      ary10[idx11] = cls12;
    }
    cls1.buff_list = ary10;
    cls1.praised = br.ReadInt32();
    cls1.vip_level = br.ReadInt32();
    cls1.level = br.ReadInt32();
    cls1.kill = br.ReadInt32();
    cls1.round_kill = br.ReadInt32();
    cls1.death = br.ReadInt32();
    cls1.score = br.ReadInt32();
    cls1.king_cur_seq = br.ReadInt32();
    cls1.king_cur_weapon_kill = br.ReadInt32();
    cls1.honor = br.ReadInt32();
    cls1.sacred_weapon_id = br.ReadInt32();
    role = cls1;
    effect_id = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actorprop
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actorprop"; } }
  public override string Name { get { return "toc_fight_actorprop"; } }
  public override int Id { get { return 0x4001e; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    toc_fight_actorprop.prop_info[] ary2 = new toc_fight_actorprop.prop_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_fight_actorprop.prop_info cls4 = new toc_fight_actorprop.prop_info();
      cls4.atkType = br.ReadInt32();
      p_fight_prop cls5 = new p_fight_prop();
      cls5.MaxHP = br.ReadInt32();
      cls5.StandMoveSpeed = br.ReadSingle();
      cls5.CrouchMoveSpeed = br.ReadSingle();
      cls5.WeaponId = LoadString(br);
      int len6 = br.ReadUInt16();
      System.String[] ary7 = new System.String[len6];
      for (int idx8 = 0; idx8 < len6; idx8++)
      {
        ary7[idx8] = LoadString(br);
      }
      cls5.DispPart = ary7;
      cls5.ClipSize = br.ReadInt32();
      cls5.ReloadTime = br.ReadSingle();
      cls5.Decelerate = br.ReadSingle();
      cls5.DecelerateTime = br.ReadSingle();
      cls5.AntiDecelerate = br.ReadSingle();
      cls5.MaxArmor = br.ReadInt32();
      cls5.ArmorRecoverWait = br.ReadInt32();
      cls5.ArmorRecoverSpeed = br.ReadInt32();
      cls5.HPRecoverWait = br.ReadInt32();
      cls5.HPRecoverSpeed = br.ReadInt32();
      cls5.Impulse = br.ReadSingle();
      cls5.ImpulseSpeed = br.ReadSingle();
      cls5.HitBackSpeed = br.ReadSingle();
      cls5.HitBackTime = br.ReadSingle();
      cls5.Weight = br.ReadSingle();
      cls5.FireRate = br.ReadSingle();
      cls5.ZoomFireRate = br.ReadSingle();
      cls5.StandSpreadMax = br.ReadSingle();
      cls5.CrouchSpreadMax = br.ReadSingle();
      cls5.RecoilPitchMin = br.ReadSingle();
      cls5.RecoilPitchMax = br.ReadSingle();
      cls5.RecoilYawMin = br.ReadSingle();
      cls5.RecoilYawMax = br.ReadSingle();
      cls5.JumpHeightAdd = br.ReadSingle();
      cls5.PveExtraWeight = br.ReadSingle();
      cls5.ZombieExtraWeight = br.ReadSingle();
      cls5.MaxHitCount = br.ReadInt32();
      cls5.RecoilCountMax = br.ReadInt32();
      cls5.StandSpreadInitial = br.ReadSingle();
      cls5.StandSpreadFireAdd = br.ReadSingle();
      cls5.CrouchSpreadInitial = br.ReadSingle();
      cls5.CrouchSpreadFireAdd = br.ReadSingle();
      cls5.SpreadMovementMax = br.ReadSingle();
      cls5.SpreadJumpMax = br.ReadSingle();
      cls5.SpreadSightsFactor = br.ReadSingle();
      cls5.SpreadDispMin = br.ReadSingle();
      cls4.prop = cls5;
      ary2[idx3] = cls4;
    }
    props = ary2;
    int len9 = br.ReadUInt16();
    System.Int32[] ary10 = new System.Int32[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      ary10[idx11] = br.ReadInt32();
    }
    awaken_props = ary10;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actorstate
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actorstate"; } }
  public override string Name { get { return "toc_fight_actorstate"; } }
  public override int Id { get { return 0x4001f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_actor_state_toc[] ary2 = new p_actor_state_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_actor_state_toc cls4 = new p_actor_state_toc();
      cls4.id = br.ReadInt32();
      cls4.life = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Single[] ary6 = new System.Single[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadSingle();
      }
      cls4.pos = ary6;
      cls4.state = br.ReadInt32();
      cls4.camp = br.ReadInt32();
      cls4.god_end = br.ReadInt32();
      cls4.immune = br.ReadInt32();
      cls4.actor_type = br.ReadInt32();
      cls4.fight_level = br.ReadInt32();
      int len8 = br.ReadUInt16();
      p_actor_buff_toc[] ary9 = new p_actor_buff_toc[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        p_actor_buff_toc cls11 = new p_actor_buff_toc();
        cls11.buff_id = br.ReadInt32();
        cls11.start_time = br.ReadInt32();
        cls11.end_time = br.ReadInt32();
        ary9[idx10] = cls11;
      }
      cls4.buff_list = ary9;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_use_skill
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "use_skill"; } }
  public override string Name { get { return "toc_fight_use_skill"; } }
  public override int Id { get { return 0x40020; } }

  public override void Load(BinaryReader br)
  {
    skill_id = br.ReadInt32();
    tauntedPlayerId = br.ReadInt64();
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    pos = ary2;
    int len4 = br.ReadUInt16();
    System.Int32[] ary5 = new System.Int32[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      ary5[idx6] = br.ReadInt32();
    }
    target_ids = ary5;
    skill_param = LoadString(br);
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_use_skill
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "use_skill"; } }
  public override string Name { get { return "tos_fight_use_skill"; } }
  public override int Id { get { return 0x40020; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.skill_id);
    bw.Write(this.tauntedPlayerId);
    var ary1 = this.pos;
    Assert(ary1 != null, "null value: this.pos");
    Assert(ary1.Length <= short.MaxValue, "this.pos length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    var ary3 = this.target_ids;
    Assert(ary3 != null, "null value: this.target_ids");
    Assert(ary3.Length <= short.MaxValue, "this.target_ids length out of range:", ary3.Length);
    bw.Write((ushort)ary3.Length);
    for (int idx4 = 0; idx4 < ary3.Length; idx4++)
    {
      bw.Write(ary3[idx4]);
    }
    SaveString(bw, this.skill_param);
    bw.Write(this.sid);
  }
}

partial class toc_fight_maul_heavily
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "maul_heavily"; } }
  public override string Name { get { return "toc_fight_maul_heavily"; } }
  public override int Id { get { return 0x40021; } }

  public override void Load(BinaryReader br)
  {
    actor_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_die
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_die"; } }
  public override string Name { get { return "toc_fight_actor_die"; } }
  public override int Id { get { return 0x40022; } }

  public override void Load(BinaryReader br)
  {
    actor_id = br.ReadInt32();
    killer = br.ReadInt32();
    weapon = br.ReadInt32();
    part = br.ReadInt32();
    multi_kill = br.ReadInt32();
    stage = br.ReadInt32();
    across_wall = br.ReadBoolean();
    UnityEngine.Vector3 cls1 = new UnityEngine.Vector3();
    cls1.x = br.ReadSingle();
    cls1.y = br.ReadSingle();
    cls1.z = br.ReadSingle();
    killerPos = cls1;
    UnityEngine.Vector3 cls2 = new UnityEngine.Vector3();
    cls2.x = br.ReadSingle();
    cls2.y = br.ReadSingle();
    cls2.z = br.ReadSingle();
    diePos = cls2;
    revive_time = br.ReadInt32();
    atk_type = br.ReadInt32();
    timeStamp = br.ReadSingle();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_state
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "state"; } }
  public override string Name { get { return "toc_fight_state"; } }
  public override int Id { get { return 0x40023; } }

  public override void Load(BinaryReader br)
  {
    p_fight_state cls1 = new p_fight_state();
    cls1.state = br.ReadInt32();
    cls1.round = br.ReadInt32();
    cls1.last_win = br.ReadInt32();
    cls1.final_win = br.ReadInt32();
    state = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_stat_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stat_info"; } }
  public override string Name { get { return "toc_fight_stat_info"; } }
  public override int Id { get { return 0x40024; } }

  public override void Load(BinaryReader br)
  {
    game_type = LoadString(br);
    win_type = LoadString(br);
    win_param = br.ReadInt32();
    win_round = br.ReadInt32();
    game_time = br.ReadSingle();
    round_time = br.ReadSingle();
    round = br.ReadInt32();
    first_blood = br.ReadInt32();
    last_blood = br.ReadInt32();
    bomb_holder = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    bomb_point = ary2;
    int len4 = br.ReadUInt16();
    stat_info_camp[] ary5 = new stat_info_camp[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      stat_info_camp cls7 = new stat_info_camp();
      cls7.camp = br.ReadInt32();
      cls7.win = br.ReadInt32();
      cls7.kill = br.ReadInt32();
      cls7.round_kill = br.ReadInt32();
      cls7.king_camp_score = br.ReadInt32();
      cls7.king_camp_cur_kill = br.ReadInt32();
      cls7.king_camp_last_win = br.ReadInt32();
      int len8 = br.ReadUInt16();
      stat_info_actor[] ary9 = new stat_info_actor[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        stat_info_actor cls11 = new stat_info_actor();
        cls11.id = br.ReadInt32();
        cls11.name = LoadString(br);
        cls11.icon = br.ReadInt32();
        cls11.vip_level = br.ReadInt32();
        cls11.alive = br.ReadBoolean();
        cls11.level = br.ReadInt32();
        cls11.kill = br.ReadInt32();
        cls11.round_kill = br.ReadInt32();
        cls11.death = br.ReadInt32();
        cls11.score = br.ReadInt32();
        cls11.king_cur_seq = br.ReadInt32();
        cls11.king_cur_weapon_kill = br.ReadInt32();
        ary9[idx10] = cls11;
      }
      cls7.players = ary9;
      cls7.arena_score = br.ReadInt32();
      ary5[idx6] = cls7;
    }
    camps = ary5;
    int len12 = br.ReadUInt16();
    stat_info_actor[] ary13 = new stat_info_actor[len12];
    for (int idx14 = 0; idx14 < len12; idx14++)
    {
      stat_info_actor cls15 = new stat_info_actor();
      cls15.id = br.ReadInt32();
      cls15.name = LoadString(br);
      cls15.icon = br.ReadInt32();
      cls15.vip_level = br.ReadInt32();
      cls15.alive = br.ReadBoolean();
      cls15.level = br.ReadInt32();
      cls15.kill = br.ReadInt32();
      cls15.round_kill = br.ReadInt32();
      cls15.death = br.ReadInt32();
      cls15.score = br.ReadInt32();
      cls15.king_cur_seq = br.ReadInt32();
      cls15.king_cur_weapon_kill = br.ReadInt32();
      ary13[idx14] = cls15;
    }
    spectators = ary13;
    round_total_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_chat
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "chat"; } }
  public override string Name { get { return "toc_fight_chat"; } }
  public override int Id { get { return 0x40025; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt64();
    type = br.ReadInt32();
    msg = LoadString(br);
    command_id = br.ReadInt32();
    p_audio_chat cls1 = new p_audio_chat();
    cls1.audio_id = br.ReadInt32();
    cls1.duration = br.ReadInt32();
    int len2 = br.ReadUInt16();
    cls1.content = br.ReadBytes(len2);
    audio_chat = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_chat
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "chat"; } }
  public override string Name { get { return "tos_fight_chat"; } }
  public override int Id { get { return 0x40025; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.type);
    SaveString(bw, this.msg);
    bw.Write(this.command_id);
    var ary1 = this.audio_chat;
    Assert(ary1 != null, "null value: this.audio_chat");
    bw.Write(ary1.audio_id);
    bw.Write(ary1.duration);
    var ary2 = ary1.content;
    Assert(ary2 != null, "null value: ary1.content");
    Assert(ary2.Length <= short.MaxValue, "ary1.content length out of range:", ary2.Length);
    bw.Write((ushort)ary2.Length);
    bw.Write(ary2, 0, ary2.Length);
    bw.Write(this.sid);
  }
}

partial class toc_fight_hit
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "hit"; } }
  public override string Name { get { return "toc_fight_hit"; } }
  public override int Id { get { return 0x40026; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_hit_info_toc[] ary2 = new p_hit_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_hit_info_toc cls4 = new p_hit_info_toc();
      cls4.actorId = br.ReadInt32();
      cls4.bodyPart = br.ReadInt32();
      cls4.life = br.ReadInt32();
      cls4.armor = br.ReadInt32();
      cls4.die = br.ReadBoolean();
      cls4.hurtType = br.ReadByte();
      ary2[idx3] = cls4;
    }
    hitList = ary2;
    hitType = br.ReadInt32();
    atkIdx = br.ReadInt32();
    skillId = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_spectate
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "spectate"; } }
  public override string Name { get { return "tos_fight_spectate"; } }
  public override int Id { get { return 0x40027; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.sid);
  }
}

partial class toc_fight_join_spectator
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "join_spectator"; } }
  public override string Name { get { return "toc_fight_join_spectator"; } }
  public override int Id { get { return 0x40028; } }

  public override void Load(BinaryReader br)
  {
    p_fight_spectator cls1 = new p_fight_spectator();
    cls1.player_id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.icon = br.ReadInt32();
    cls1.corps_id = br.ReadInt32();
    cls1.corps_name = LoadString(br);
    cls1.level = br.ReadInt32();
    spectator = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_leave_spectator
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "leave_spectator"; } }
  public override string Name { get { return "toc_fight_leave_spectator"; } }
  public override int Id { get { return 0x40029; } }

  public override void Load(BinaryReader br)
  {
    player_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_can_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "can_vote_kick"; } }
  public override string Name { get { return "toc_fight_can_vote_kick"; } }
  public override int Id { get { return 0x4002a; } }

  public override void Load(BinaryReader br)
  {
    can_vote = br.ReadBoolean();
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_can_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "can_vote_kick"; } }
  public override string Name { get { return "tos_fight_can_vote_kick"; } }
  public override int Id { get { return 0x4002a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_begin_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "begin_vote_kick"; } }
  public override string Name { get { return "toc_fight_begin_vote_kick"; } }
  public override int Id { get { return 0x4002b; } }

  public override void Load(BinaryReader br)
  {
    sponsor_name = LoadString(br);
    sponsor_id = br.ReadInt32();
    kicked_player_name = LoadString(br);
    kicked_player_id = br.ReadInt32();
    reason = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_begin_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "begin_vote_kick"; } }
  public override string Name { get { return "tos_fight_begin_vote_kick"; } }
  public override int Id { get { return 0x4002b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.kicked_player_id);
    SaveString(bw, this.reason);
    bw.Write(this.sid);
  }
}

partial class toc_fight_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "vote_kick"; } }
  public override string Name { get { return "toc_fight_vote_kick"; } }
  public override int Id { get { return 0x4002c; } }

  public override void Load(BinaryReader br)
  {
    pro = br.ReadInt32();
    con = br.ReadInt32();
    remainder_sec = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_vote_kick
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "vote_kick"; } }
  public override string Name { get { return "tos_fight_vote_kick"; } }
  public override int Id { get { return 0x4002c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pro_or_con);
    bw.Write(this.sid);
  }
}

partial class toc_fight_vote_kick_result
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "vote_kick_result"; } }
  public override string Name { get { return "toc_fight_vote_kick_result"; } }
  public override int Id { get { return 0x4002d; } }

  public override void Load(BinaryReader br)
  {
    is_kicked = br.ReadBoolean();
    kicked_player_name = LoadString(br);
    kicked_player_id = br.ReadInt32();
    pro = br.ReadInt32();
    con = br.ReadInt32();
    reason = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_start_rescue
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_rescue"; } }
  public override string Name { get { return "toc_fight_start_rescue"; } }
  public override int Id { get { return 0x4002e; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    actor = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_start_rescue
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_rescue"; } }
  public override string Name { get { return "tos_fight_start_rescue"; } }
  public override int Id { get { return 0x4002e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.actor);
    bw.Write(this.sid);
  }
}

partial class toc_fight_stop_rescue
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_rescue"; } }
  public override string Name { get { return "toc_fight_stop_rescue"; } }
  public override int Id { get { return 0x4002f; } }

  public override void Load(BinaryReader br)
  {
    actor = br.ReadInt32();
    remain_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_stop_rescue
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "stop_rescue"; } }
  public override string Name { get { return "tos_fight_stop_rescue"; } }
  public override int Id { get { return 0x4002f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_achieve
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "achieve"; } }
  public override string Name { get { return "toc_fight_achieve"; } }
  public override int Id { get { return 0x40030; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.String[] ary2 = new System.String[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = LoadString(br);
    }
    achieve_list = ary2;
    from = br.ReadInt32();
    to = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_rescued
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_rescued"; } }
  public override string Name { get { return "toc_fight_actor_rescued"; } }
  public override int Id { get { return 0x40031; } }

  public override void Load(BinaryReader br)
  {
    rescuer = br.ReadInt32();
    actor = br.ReadInt32();
    times = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_survival_use_revivecoin
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_use_revivecoin"; } }
  public override string Name { get { return "toc_fight_survival_use_revivecoin"; } }
  public override int Id { get { return 0x40032; } }

  public override void Load(BinaryReader br)
  {
    actor = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_survival_use_revivecoin
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_use_revivecoin"; } }
  public override string Name { get { return "tos_fight_survival_use_revivecoin"; } }
  public override int Id { get { return 0x40032; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.actor);
    bw.Write(this.sid);
  }
}

partial class toc_fight_survival_revive
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_revive"; } }
  public override string Name { get { return "toc_fight_survival_revive"; } }
  public override int Id { get { return 0x40033; } }

  public override void Load(BinaryReader br)
  {
    reviver = br.ReadInt32();
    actor = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_survival_need_revivecoin
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_need_revivecoin"; } }
  public override string Name { get { return "toc_fight_survival_need_revivecoin"; } }
  public override int Id { get { return 0x40034; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_survival_state
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_state"; } }
  public override string Name { get { return "toc_fight_survival_state"; } }
  public override int Id { get { return 0x40035; } }

  public override void Load(BinaryReader br)
  {
    total_round = br.ReadInt32();
    stage_level = br.ReadInt32();
    revivecoin = br.ReadInt32();
    supplypoint = br.ReadInt32();
    survival_buff = br.ReadInt32();
    total_box = br.ReadInt32();
    remain_box = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_survival_expenditure
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_expenditure"; } }
  public override string Name { get { return "toc_fight_survival_expenditure"; } }
  public override int Id { get { return 0x40036; } }

  public override void Load(BinaryReader br)
  {
    type = br.ReadInt32();
    item_id = br.ReadInt32();
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_survival_expenditure
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_expenditure"; } }
  public override string Name { get { return "tos_fight_survival_expenditure"; } }
  public override int Id { get { return 0x40036; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.type);
    bw.Write(this.item_id);
    bw.Write(this.sid);
  }
}

partial class tos_fight_survival_abort_expenditure
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "survival_abort_expenditure"; } }
  public override string Name { get { return "tos_fight_survival_abort_expenditure"; } }
  public override int Id { get { return 0x40037; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.type);
    bw.Write(this.item_id);
    bw.Write(this.sid);
  }
}

partial class toc_fight_hide_state
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "hide_state"; } }
  public override string Name { get { return "toc_fight_hide_state"; } }
  public override int Id { get { return 0x40038; } }

  public override void Load(BinaryReader br)
  {
    state = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_select_disguise
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_disguise"; } }
  public override string Name { get { return "toc_fight_select_disguise"; } }
  public override int Id { get { return 0x40039; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    disguise_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_select_disguise
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_disguise"; } }
  public override string Name { get { return "tos_fight_select_disguise"; } }
  public override int Id { get { return 0x40039; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.sid);
  }
}

partial class toc_fight_praise
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "praise"; } }
  public override string Name { get { return "toc_fight_praise"; } }
  public override int Id { get { return 0x4003a; } }

  public override void Load(BinaryReader br)
  {
    pid = br.ReadInt64();
    praised = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_praise
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "praise"; } }
  public override string Name { get { return "tos_fight_praise"; } }
  public override int Id { get { return 0x4003a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pid);
    bw.Write(this.sid);
  }
}

partial class toc_fight_atk_wrong
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "atk_wrong"; } }
  public override string Name { get { return "toc_fight_atk_wrong"; } }
  public override int Id { get { return 0x4003b; } }

  public override void Load(BinaryReader br)
  {
    life = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_atk_wrong
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "atk_wrong"; } }
  public override string Name { get { return "tos_fight_atk_wrong"; } }
  public override int Id { get { return 0x4003b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_fight_reelect_disguise
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "reelect_disguise"; } }
  public override string Name { get { return "tos_fight_reelect_disguise"; } }
  public override int Id { get { return 0x4003c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_scene_items
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "scene_items"; } }
  public override string Name { get { return "toc_fight_scene_items"; } }
  public override int Id { get { return 0x4003d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_scene_item_toc[] ary2 = new p_scene_item_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_scene_item_toc cls4 = new p_scene_item_toc();
      cls4.id = br.ReadInt32();
      int len5 = br.ReadUInt16();
      System.Single[] ary6 = new System.Single[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadSingle();
      }
      cls4.pos = ary6;
      int len8 = br.ReadUInt16();
      System.Single[] ary9 = new System.Single[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        ary9[idx10] = br.ReadSingle();
      }
      cls4.scale = ary9;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_total_scene_items
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "total_scene_items"; } }
  public override string Name { get { return "toc_fight_total_scene_items"; } }
  public override int Id { get { return 0x4003e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_game_result
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "game_result"; } }
  public override string Name { get { return "toc_fight_game_result"; } }
  public override int Id { get { return 0x4003f; } }

  public override void Load(BinaryReader br)
  {
    first_blood = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_result_player[] ary2 = new p_result_player[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_result_player cls4 = new p_result_player();
      cls4.death = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.mvp = br.ReadInt32();
      cls4.score = br.ReadInt32();
      cls4.player_id = br.ReadInt64();
      cls4.praised_cnt = br.ReadInt32();
      cls4.game_time = br.ReadSingle();
      cls4.actor_id = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.camp = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.kill = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.rescue_times = br.ReadInt32();
      cls4.revive_times = br.ReadInt32();
      cls4.honor2add = br.ReadInt32();
      int len5 = br.ReadUInt16();
      ItemInfo[] ary6 = new ItemInfo[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ItemInfo cls8 = new ItemInfo();
        cls8.ID = br.ReadInt32();
        cls8.cnt = br.ReadInt32();
        cls8.day = br.ReadSingle();
        ary6[idx7] = cls8;
      }
      cls4.item_list = ary6;
      cls4.hero_type = br.ReadInt32();
      cls4.chenghao = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    players = ary2;
    abnormal_finish = br.ReadBoolean();
    silver_ace = br.ReadInt32();
    gold_ace = br.ReadInt32();
    game_time = br.ReadSingle();
    last_blood = br.ReadInt32();
    win_camp = br.ReadInt32();
    mvp = br.ReadInt32();
    scene_subtype = LoadString(br);
    game_type = LoadString(br);
    int len9 = br.ReadUInt16();
    p_camp_result[] ary10 = new p_camp_result[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      p_camp_result cls12 = new p_camp_result();
      cls12.death = br.ReadInt32();
      cls12.camp = br.ReadInt32();
      cls12.kill = br.ReadInt32();
      cls12.win = br.ReadInt32();
      ary10[idx11] = cls12;
    }
    camp_info = ary10;
    scene_type = LoadString(br);
    win_type = LoadString(br);
    game_round = br.ReadInt32();
    int len13 = br.ReadUInt16();
    p_title_result[] ary14 = new p_title_result[len13];
    for (int idx15 = 0; idx15 < len13; idx15++)
    {
      p_title_result cls16 = new p_title_result();
      cls16.title = LoadString(br);
      cls16.player_id = br.ReadInt32();
      ary14[idx15] = cls16;
    }
    titles = ary14;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_throw_weapon
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "throw_weapon"; } }
  public override string Name { get { return "toc_fight_throw_weapon"; } }
  public override int Id { get { return 0x40040; } }

  public override void Load(BinaryReader br)
  {
    drop_uid = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_throw_weapon
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "throw_weapon"; } }
  public override string Name { get { return "tos_fight_throw_weapon"; } }
  public override int Id { get { return 0x40040; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.pos;
    bw.Write(ary1.x);
    bw.Write(ary1.y);
    bw.Write(ary1.z);
    bw.Write(this.sid);
  }
}

partial class toc_fight_arena_begin_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "arena_begin_info"; } }
  public override string Name { get { return "toc_fight_arena_begin_info"; } }
  public override int Id { get { return 0x40041; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_arena_player_info[] ary2 = new p_arena_player_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_arena_player_info cls4 = new p_arena_player_info();
      cls4.actor_id = br.ReadInt32();
      cls4.camp = br.ReadInt32();
      cls4.order = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    a_info = ary2;
    int len5 = br.ReadUInt16();
    p_arena_player_info[] ary6 = new p_arena_player_info[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_arena_player_info cls8 = new p_arena_player_info();
      cls8.actor_id = br.ReadInt32();
      cls8.camp = br.ReadInt32();
      cls8.order = br.ReadInt32();
      ary6[idx7] = cls8;
    }
    b_info = ary6;
    a_index = br.ReadInt32();
    b_index = br.ReadInt32();
    time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_arena_end_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "arena_end_info"; } }
  public override string Name { get { return "toc_fight_arena_end_info"; } }
  public override int Id { get { return 0x40042; } }

  public override void Load(BinaryReader br)
  {
    camp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_res_load_done
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "res_load_done"; } }
  public override string Name { get { return "tos_fight_res_load_done"; } }
  public override int Id { get { return 0x40043; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_bomb_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "bomb_info"; } }
  public override string Name { get { return "toc_fight_bomb_info"; } }
  public override int Id { get { return 0x40044; } }

  public override void Load(BinaryReader br)
  {
    bomb_holder = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    bomb_point = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_score
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "score"; } }
  public override string Name { get { return "toc_fight_score"; } }
  public override int Id { get { return 0x40045; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_king_score[] ary2 = new p_king_score[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_king_score cls4 = new p_king_score();
      cls4.actor_id = br.ReadInt32();
      cls4.score = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_king_camp
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "king_camp"; } }
  public override string Name { get { return "toc_fight_king_camp"; } }
  public override int Id { get { return 0x40046; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_king_camp_info[] ary2 = new p_king_camp_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_king_camp_info cls4 = new p_king_camp_info();
      cls4.camp = br.ReadInt32();
      cls4.king_camp_score = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    camps = ary2;
    king_camp_last_win = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_king_solo
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "king_solo"; } }
  public override string Name { get { return "toc_fight_king_solo"; } }
  public override int Id { get { return 0x40047; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_king_solo_info[] ary2 = new p_king_solo_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_king_solo_info cls4 = new p_king_solo_info();
      cls4.actor_id = br.ReadInt32();
      cls4.king_cur_seq = br.ReadInt32();
      cls4.king_cur_weapon_kill = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_weapon_motion
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "weapon_motion"; } }
  public override string Name { get { return "toc_fight_weapon_motion"; } }
  public override int Id { get { return 0x40048; } }

  public override void Load(BinaryReader br)
  {
    weapon_id = br.ReadInt32();
    motion = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_weapon_motion
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "weapon_motion"; } }
  public override string Name { get { return "tos_fight_weapon_motion"; } }
  public override int Id { get { return 0x40048; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.weapon_id);
    bw.Write(this.motion);
    bw.Write(this.sid);
  }
}

partial class tos_fight_enter_deathbox
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "enter_deathbox"; } }
  public override string Name { get { return "tos_fight_enter_deathbox"; } }
  public override int Id { get { return 0x40049; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_fight_game_shop
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "game_shop"; } }
  public override string Name { get { return "toc_fight_game_shop"; } }
  public override int Id { get { return 0x4004a; } }

  public override void Load(BinaryReader br)
  {
    shop_id = br.ReadInt32();
    ret = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_game_shop
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "game_shop"; } }
  public override string Name { get { return "tos_fight_game_shop"; } }
  public override int Id { get { return 0x4004a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.shop_id);
    bw.Write(this.sid);
  }
}

partial class tos_fight_cancel_shop
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "cancel_shop"; } }
  public override string Name { get { return "tos_fight_cancel_shop"; } }
  public override int Id { get { return 0x4004b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_defend_state
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "defend_state"; } }
  public override string Name { get { return "toc_fight_defend_state"; } }
  public override int Id { get { return 0x4004c; } }

  public override void Load(BinaryReader br)
  {
    revivecoin = br.ReadInt32();
    supplypoint = br.ReadInt32();
    defend_buff = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_weapon_awaken_skill
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "weapon_awaken_skill"; } }
  public override string Name { get { return "toc_fight_weapon_awaken_skill"; } }
  public override int Id { get { return 0x4004d; } }

  public override void Load(BinaryReader br)
  {
    awaken_id = br.ReadInt32();
    skill_id = br.ReadInt32();
    life = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    pos = ary2;
    int len4 = br.ReadUInt16();
    System.Int32[] ary5 = new System.Int32[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      ary5[idx6] = br.ReadInt32();
    }
    target_ids = ary5;
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_weapon_awaken_skill
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "weapon_awaken_skill"; } }
  public override string Name { get { return "tos_fight_weapon_awaken_skill"; } }
  public override int Id { get { return 0x4004d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.awaken_id);
    var ary1 = this.pos;
    Assert(ary1 != null, "null value: this.pos");
    Assert(ary1.Length <= short.MaxValue, "this.pos length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      bw.Write(ary1[idx2]);
    }
    var ary3 = this.target_ids;
    Assert(ary3 != null, "null value: this.target_ids");
    Assert(ary3.Length <= short.MaxValue, "this.target_ids length out of range:", ary3.Length);
    bw.Write((ushort)ary3.Length);
    for (int idx4 = 0; idx4 < ary3.Length; idx4++)
    {
      bw.Write(ary3[idx4]);
    }
    bw.Write(this.sid);
  }
}

partial class toc_fight_whether2human
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "whether2human"; } }
  public override string Name { get { return "toc_fight_whether2human"; } }
  public override int Id { get { return 0x4004e; } }

  public override void Load(BinaryReader br)
  {
    time_limit = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_start_zombie2human
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_zombie2human"; } }
  public override string Name { get { return "toc_fight_start_zombie2human"; } }
  public override int Id { get { return 0x4004f; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_start_zombie2human
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "start_zombie2human"; } }
  public override string Name { get { return "tos_fight_start_zombie2human"; } }
  public override int Id { get { return 0x4004f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_select_equip
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_equip"; } }
  public override string Name { get { return "toc_fight_select_equip"; } }
  public override int Id { get { return 0x40050; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_bag[] ary2 = new p_bag[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_bag cls4 = new p_bag();
      cls4.group_id = br.ReadInt32();
      p_group_equip_list cls5 = new p_group_equip_list();
      cls5.WEAPON1 = br.ReadInt32();
      cls5.WEAPON2 = br.ReadInt32();
      cls5.HELMET = br.ReadInt32();
      cls5.ARMOR = br.ReadInt32();
      cls5.BOOTS = br.ReadInt32();
      cls5.DAGGER = br.ReadInt32();
      cls5.GRENADE = br.ReadInt32();
      cls5.FLASHBOMB = br.ReadInt32();
      cls4.equip_list = cls5;
      int len6 = br.ReadUInt16();
      p_group_decoration[] ary7 = new p_group_decoration[len6];
      for (int idx8 = 0; idx8 < len6; idx8++)
      {
        p_group_decoration cls9 = new p_group_decoration();
        cls9.part = br.ReadInt32();
        cls9.decoration = br.ReadInt32();
        ary7[idx8] = cls9;
      }
      cls4.decorations = ary7;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_select_equip
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_equip"; } }
  public override string Name { get { return "tos_fight_select_equip"; } }
  public override int Id { get { return 0x40050; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.bagid);
    bw.Write(this.sid);
  }
}

partial class toc_fight_select_zombie
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_zombie"; } }
  public override string Name { get { return "toc_fight_select_zombie"; } }
  public override int Id { get { return 0x40051; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    zombies = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_select_zombie
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_zombie"; } }
  public override string Name { get { return "tos_fight_select_zombie"; } }
  public override int Id { get { return 0x40051; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.sid);
  }
}

partial class toc_fight_actor_life
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_life"; } }
  public override string Name { get { return "toc_fight_actor_life"; } }
  public override int Id { get { return 0x40052; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    life = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_god_end
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_god_end"; } }
  public override string Name { get { return "toc_fight_actor_god_end"; } }
  public override int Id { get { return 0x40053; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    god_end = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_buff_list
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_buff_list"; } }
  public override string Name { get { return "toc_fight_actor_buff_list"; } }
  public override int Id { get { return 0x40054; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_actor_buff_toc[] ary2 = new p_actor_buff_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_actor_buff_toc cls4 = new p_actor_buff_toc();
      cls4.buff_id = br.ReadInt32();
      cls4.start_time = br.ReadInt32();
      cls4.end_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    buff_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_pos
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_pos"; } }
  public override string Name { get { return "toc_fight_actor_pos"; } }
  public override int Id { get { return 0x40055; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Single[] ary2 = new System.Single[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadSingle();
    }
    pos = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_fight_level
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_fight_level"; } }
  public override string Name { get { return "toc_fight_actor_fight_level"; } }
  public override int Id { get { return 0x40056; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    fight_level = br.ReadInt32();
    is_level_up = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_change_role
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "change_role"; } }
  public override string Name { get { return "toc_fight_change_role"; } }
  public override int Id { get { return 0x40057; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    role = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_immune
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_immune"; } }
  public override string Name { get { return "toc_fight_actor_immune"; } }
  public override int Id { get { return 0x40058; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    immune = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_actor_type
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor_type"; } }
  public override string Name { get { return "toc_fight_actor_type"; } }
  public override int Id { get { return 0x40059; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    type = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_update_actor_type
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "update_actor_type"; } }
  public override string Name { get { return "toc_fight_update_actor_type"; } }
  public override int Id { get { return 0x4005a; } }

  public override void Load(BinaryReader br)
  {
    actor_type = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_update_actor_type
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "update_actor_type"; } }
  public override string Name { get { return "tos_fight_update_actor_type"; } }
  public override int Id { get { return 0x4005a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.actor_type);
    bw.Write(this.sid);
  }
}

partial class toc_fight_actor2big_head_king
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "actor2big_head_king"; } }
  public override string Name { get { return "toc_fight_actor2big_head_king"; } }
  public override int Id { get { return 0x4005b; } }

  public override void Load(BinaryReader br)
  {
    actor = br.ReadInt32();
    king_index = br.ReadInt32();
    max_heros = br.ReadInt32();
    hero_need_king = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_final_time
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "final_time"; } }
  public override string Name { get { return "toc_fight_final_time"; } }
  public override int Id { get { return 0x4005c; } }

  public override void Load(BinaryReader br)
  {
    final_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_ai_action
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "ai_action"; } }
  public override string Name { get { return "toc_fight_ai_action"; } }
  public override int Id { get { return 0x4005d; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    action = br.ReadInt32();
    UnityEngine.Vector3 cls1 = new UnityEngine.Vector3();
    cls1.x = br.ReadSingle();
    cls1.y = br.ReadSingle();
    cls1.z = br.ReadSingle();
    pos = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_attack_failed
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "attack_failed"; } }
  public override string Name { get { return "toc_fight_attack_failed"; } }
  public override int Id { get { return 0x4005e; } }

  public override void Load(BinaryReader br)
  {
    from_id = br.ReadInt32();
    actor_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_select_role
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_role"; } }
  public override string Name { get { return "toc_fight_select_role"; } }
  public override int Id { get { return 0x4005f; } }

  public override void Load(BinaryReader br)
  {
    index = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_select_role
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_role"; } }
  public override string Name { get { return "tos_fight_select_role"; } }
  public override int Id { get { return 0x4005f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.sid);
  }
}

partial class toc_fight_hero_time
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "hero_time"; } }
  public override string Name { get { return "toc_fight_hero_time"; } }
  public override int Id { get { return 0x40060; } }

  public override void Load(BinaryReader br)
  {
    hero_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_ghost_jump
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "ghost_jump"; } }
  public override string Name { get { return "toc_fight_ghost_jump"; } }
  public override int Id { get { return 0x40061; } }

  public override void Load(BinaryReader br)
  {
    visible = br.ReadBoolean();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_ghost_jump
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "ghost_jump"; } }
  public override string Name { get { return "tos_fight_ghost_jump"; } }
  public override int Id { get { return 0x40061; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.visible);
    bw.Write(this.sid);
  }
}

partial class toc_fight_team_hero_level
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "team_hero_level"; } }
  public override string Name { get { return "toc_fight_team_hero_level"; } }
  public override int Id { get { return 0x40062; } }

  public override void Load(BinaryReader br)
  {
    actor_id = br.ReadInt32();
    level = br.ReadInt32();
    soul = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_rush_done
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "rush_done"; } }
  public override string Name { get { return "toc_fight_rush_done"; } }
  public override int Id { get { return 0x40063; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_rush_done
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "rush_done"; } }
  public override string Name { get { return "tos_fight_rush_done"; } }
  public override int Id { get { return 0x40063; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_ai_skill
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "ai_skill"; } }
  public override string Name { get { return "toc_fight_ai_skill"; } }
  public override int Id { get { return 0x40064; } }

  public override void Load(BinaryReader br)
  {
    from = br.ReadInt32();
    skill = br.ReadInt32();
    int len1 = br.ReadUInt16();
    System.Single[][] ary2 = new System.Single[len1][];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      int len4 = br.ReadUInt16();
      System.Single[] ary5 = new System.Single[len4];
      for (int idx6 = 0; idx6 < len4; idx6++)
      {
        ary5[idx6] = br.ReadSingle();
      }
      ary2[idx3] = ary5;
    }
    targets = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_dota_camp_info
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "dota_camp_info"; } }
  public override string Name { get { return "toc_fight_dota_camp_info"; } }
  public override int Id { get { return 0x40065; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_dota_camp_info[] ary2 = new p_dota_camp_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_dota_camp_info cls4 = new p_dota_camp_info();
      cls4.camp = br.ReadInt32();
      cls4.boss_buff_level = br.ReadInt32();
      cls4.boss_revive_time = br.ReadInt32();
      cls4.boss_revive_date = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    infos = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_dota_state
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "dota_state"; } }
  public override string Name { get { return "toc_fight_dota_state"; } }
  public override int Id { get { return 0x40066; } }

  public override void Load(BinaryReader br)
  {
    actor_id = br.ReadInt32();
    survival_buff = br.ReadInt32();
    attack_buff = br.ReadInt32();
    supplypoint = br.ReadInt32();
    revivecoin = br.ReadInt32();
    cure_buff = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_energy
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "energy"; } }
  public override string Name { get { return "toc_fight_energy"; } }
  public override int Id { get { return 0x40067; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_king_score[] ary2 = new p_king_score[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_king_score cls4 = new p_king_score();
      cls4.actor_id = br.ReadInt32();
      cls4.score = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_round_best
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "round_best"; } }
  public override string Name { get { return "toc_fight_round_best"; } }
  public override int Id { get { return 0x40068; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_round_best[] ary2 = new p_round_best[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_round_best cls4 = new p_round_best();
      cls4.title = LoadString(br);
      cls4.name = LoadString(br);
      cls4.score = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    round_bests = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_player_check_win
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "player_check_win"; } }
  public override string Name { get { return "tos_fight_player_check_win"; } }
  public override int Id { get { return 0x40069; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_play_animation
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "play_animation"; } }
  public override string Name { get { return "toc_fight_play_animation"; } }
  public override int Id { get { return 0x4006a; } }

  public override void Load(BinaryReader br)
  {
    actor_id = br.ReadInt32();
    animation = LoadString(br);
    play_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_game_start_time
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "game_start_time"; } }
  public override string Name { get { return "toc_fight_game_start_time"; } }
  public override int Id { get { return 0x4006b; } }

  public override void Load(BinaryReader br)
  {
    time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_role_select_list
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "role_select_list"; } }
  public override string Name { get { return "toc_fight_role_select_list"; } }
  public override int Id { get { return 0x4006c; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_record
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "record"; } }
  public override string Name { get { return "toc_fight_record"; } }
  public override int Id { get { return 0x4006d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_data_record[] ary2 = new p_data_record[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_data_record cls4 = new p_data_record();
      cls4.pass_time = br.ReadSingle();
      cls4.data = LoadString(br);
      ary2[idx3] = cls4;
    }
    scene_data_record = ary2;
    int len5 = br.ReadUInt16();
    p_data_record[][] ary6 = new p_data_record[len5][];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      int len8 = br.ReadUInt16();
      p_data_record[] ary9 = new p_data_record[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        p_data_record cls11 = new p_data_record();
        cls11.pass_time = br.ReadSingle();
        cls11.data = LoadString(br);
        ary9[idx10] = cls11;
      }
      ary6[idx7] = ary9;
    }
    broadcast_records = ary6;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_wait_time
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "wait_time"; } }
  public override string Name { get { return "toc_fight_wait_time"; } }
  public override int Id { get { return 0x4006e; } }

  public override void Load(BinaryReader br)
  {
    time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_select_buff
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_buff"; } }
  public override string Name { get { return "toc_fight_select_buff"; } }
  public override int Id { get { return 0x4006f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    buff_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_select_buff
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "select_buff"; } }
  public override string Name { get { return "tos_fight_select_buff"; } }
  public override int Id { get { return 0x4006f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.sid);
  }
}

partial class tos_fight_reelect_buff
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "reelect_buff"; } }
  public override string Name { get { return "tos_fight_reelect_buff"; } }
  public override int Id { get { return 0x40070; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_fight_fixpos
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "fixpos"; } }
  public override string Name { get { return "toc_fight_fixpos"; } }
  public override int Id { get { return 0x40071; } }

  public override void Load(BinaryReader br)
  {
    UnityEngine.Vector3 cls1 = new UnityEngine.Vector3();
    cls1.x = br.ReadSingle();
    cls1.y = br.ReadSingle();
    cls1.z = br.ReadSingle();
    pos = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_throw_cfour
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "throw_cfour"; } }
  public override string Name { get { return "toc_fight_throw_cfour"; } }
  public override int Id { get { return 0x40072; } }

  public override void Load(BinaryReader br)
  {
    drop_uid = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_throw_cfour
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "throw_cfour"; } }
  public override string Name { get { return "tos_fight_throw_cfour"; } }
  public override int Id { get { return 0x40072; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.pos;
    bw.Write(ary1.x);
    bw.Write(ary1.y);
    bw.Write(ary1.z);
    bw.Write(this.sid);
  }
}

partial class toc_fight_occupy
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "occupy"; } }
  public override string Name { get { return "toc_fight_occupy"; } }
  public override int Id { get { return 0x40073; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    resources = ary2;
    int len4 = br.ReadUInt16();
    p_occupy_info[] ary5 = new p_occupy_info[len4];
    for (int idx6 = 0; idx6 < len4; idx6++)
    {
      p_occupy_info cls7 = new p_occupy_info();
      cls7.id = br.ReadInt32();
      cls7.owner = br.ReadInt32();
      int len8 = br.ReadUInt16();
      System.Int32[] ary9 = new System.Int32[len8];
      for (int idx10 = 0; idx10 < len8; idx10++)
      {
        ary9[idx10] = br.ReadInt32();
      }
      cls7.scores = ary9;
      cls7.occupyMember1 = br.ReadInt32();
      cls7.occupyMember2 = br.ReadInt32();
      ary5[idx6] = cls7;
    }
    occupy_infos = ary5;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_halo_list
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "halo_list"; } }
  public override string Name { get { return "toc_fight_halo_list"; } }
  public override int Id { get { return 0x40074; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_halo_info[] ary2 = new p_halo_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_halo_info cls4 = new p_halo_info();
      cls4.actor_id = br.ReadInt32();
      cls4.halo_id = br.ReadInt32();
      cls4.camp = br.ReadInt32();
      UnityEngine.Vector3 cls5 = new UnityEngine.Vector3();
      cls5.x = br.ReadSingle();
      cls5.y = br.ReadSingle();
      cls5.z = br.ReadSingle();
      cls4.pos = cls5;
      cls4.end_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    halo_infos = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_fight_poweron
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "poweron"; } }
  public override string Name { get { return "toc_fight_poweron"; } }
  public override int Id { get { return 0x40075; } }

  public override void Load(BinaryReader br)
  {
    timeStamp = br.ReadSingle();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_poweron
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "poweron"; } }
  public override string Name { get { return "tos_fight_poweron"; } }
  public override int Id { get { return 0x40075; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.timeStamp);
    bw.Write(this.sid);
  }
}

partial class toc_fight_poweroff
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "poweroff"; } }
  public override string Name { get { return "toc_fight_poweroff"; } }
  public override int Id { get { return 0x40076; } }

  public override void Load(BinaryReader br)
  {
    timeStamp = br.ReadSingle();
    firePose = br.ReadInt32();
    UnityEngine.Vector3 cls1 = new UnityEngine.Vector3();
    cls1.x = br.ReadSingle();
    cls1.y = br.ReadSingle();
    cls1.z = br.ReadSingle();
    fireDir = cls1;
    index = br.ReadInt32();
    atkType = br.ReadInt32();
    from = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_poweroff
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "poweroff"; } }
  public override string Name { get { return "tos_fight_poweroff"; } }
  public override int Id { get { return 0x40076; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.firePose);
    var ary1 = this.fireDir;
    bw.Write(ary1.x);
    bw.Write(ary1.y);
    bw.Write(ary1.z);
    bw.Write(this.index);
    bw.Write(this.atkType);
    bw.Write(this.timeStamp);
    bw.Write(this.sid);
  }
}

partial class tos_fight_bowfire
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "bowfire"; } }
  public override string Name { get { return "tos_fight_bowfire"; } }
  public override int Id { get { return 0x40077; } }

  public override void Save(BinaryWriter bw)
  {
    var ary1 = this.hitList;
    Assert(ary1 != null, "null value: this.hitList");
    Assert(ary1.Length <= short.MaxValue, "this.hitList length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      var ary3 = ary1[idx2];
      Assert(ary3 != null, "null value: ary1[idx2]");
      bw.Write(ary3.id);
      var ary4 = ary3.pos;
      bw.Write(ary4.x);
      bw.Write(ary4.y);
      bw.Write(ary4.z);
      bw.Write(ary3.bodyPart);
    }
    var ary5 = this.fromPos;
    bw.Write(ary5.x);
    bw.Write(ary5.y);
    bw.Write(ary5.z);
    bw.Write(this.index);
    var ary6 = this.firePos;
    bw.Write(ary6.x);
    bw.Write(ary6.y);
    bw.Write(ary6.z);
    bw.Write(this.sid);
  }
}

partial class toc_fight_change_spectator
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "change_spectator"; } }
  public override string Name { get { return "toc_fight_change_spectator"; } }
  public override int Id { get { return 0x40078; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_fight_change_spectator
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "change_spectator"; } }
  public override string Name { get { return "tos_fight_change_spectator"; } }
  public override int Id { get { return 0x40078; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.actor_id);
    bw.Write(this.sid);
  }
}

partial class toc_fight_spectator_record
{
  public override string Typ { get { return "fight"; } }
  public override string Key { get { return "spectator_record"; } }
  public override string Name { get { return "toc_fight_spectator_record"; } }
  public override int Id { get { return 0x40079; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_spectator_record[] ary2 = new p_spectator_record[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_spectator_record cls4 = new p_spectator_record();
      cls4.actor_id = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_spectator_detail[] ary6 = new p_spectator_detail[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_spectator_detail cls8 = new p_spectator_detail();
        cls8.damage = br.ReadInt32();
        cls8.hit_position = br.ReadInt32();
        cls8.killed_by_who = br.ReadInt32();
        cls8.damage_name = LoadString(br);
        cls8.player_id = br.ReadInt64();
        ary6[idx7] = cls8;
      }
      cls4.detail_info = ary6;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}


partial class tos_joinroom_create
{
  public override string Typ { get { return "joinroom"; } }
  public override string Key { get { return "create"; } }
  public override string Name { get { return "tos_joinroom_create"; } }
  public override int Id { get { return 0x50001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    bw.Write(this.stage);
    bw.Write(this.sid);
  }
}

partial class tos_joinroom_join
{
  public override string Typ { get { return "joinroom"; } }
  public override string Key { get { return "join"; } }
  public override string Name { get { return "tos_joinroom_join"; } }
  public override int Id { get { return 0x50002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    bw.Write(this.room);
    SaveString(bw, this.password);
    bw.Write(this.spectator);
    bw.Write(this.sid);
  }
}

partial class tos_joinroom_quick_join
{
  public override string Typ { get { return "joinroom"; } }
  public override string Key { get { return "quick_join"; } }
  public override string Name { get { return "tos_joinroom_quick_join"; } }
  public override int Id { get { return 0x50003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    SaveString(bw, this.rule);
    var ary1 = this.rulelist;
    Assert(ary1 != null, "null value: this.rulelist");
    Assert(ary1.Length <= short.MaxValue, "this.rulelist length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      SaveString(bw, ary1[idx2]);
    }
    bw.Write(this.source);
    bw.Write(this.sid);
  }
}

partial class tos_joinroom_accept_invite
{
  public override string Typ { get { return "joinroom"; } }
  public override string Key { get { return "accept_invite"; } }
  public override string Name { get { return "tos_joinroom_accept_invite"; } }
  public override int Id { get { return 0x50004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    bw.Write(this.room);
    bw.Write(this.sid);
  }
}

partial class tos_joinroom_follow
{
  public override string Typ { get { return "joinroom"; } }
  public override string Key { get { return "follow"; } }
  public override string Name { get { return "tos_joinroom_follow"; } }
  public override int Id { get { return 0x50005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.roleid);
    SaveString(bw, this.password);
    bw.Write(this.spectator);
    bw.Write(this.sid);
  }
}


partial class tos_room_leave
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "leave"; } }
  public override string Name { get { return "tos_room_leave"; } }
  public override int Id { get { return 0x60001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_name
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "name"; } }
  public override string Name { get { return "tos_room_name"; } }
  public override int Id { get { return 0x60002; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.name);
    bw.Write(this.sid);
  }
}

partial class tos_room_password
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "password"; } }
  public override string Name { get { return "tos_room_password"; } }
  public override int Id { get { return 0x60003; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.password);
    bw.Write(this.sid);
  }
}

partial class tos_room_auto_aim
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "auto_aim"; } }
  public override string Name { get { return "tos_room_auto_aim"; } }
  public override int Id { get { return 0x60004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.auto_aim);
    bw.Write(this.sid);
  }
}

partial class tos_room_rule
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "rule"; } }
  public override string Name { get { return "tos_room_rule"; } }
  public override int Id { get { return 0x60005; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.rule);
    bw.Write(this.map);
    bw.Write(this.max_player);
    bw.Write(this.level);
    var ary1 = this.rule_list;
    Assert(ary1 != null, "null value: this.rule_list");
    Assert(ary1.Length <= short.MaxValue, "this.rule_list length out of range:", ary1.Length);
    bw.Write((ushort)ary1.Length);
    for (int idx2 = 0; idx2 < ary1.Length; idx2++)
    {
      SaveString(bw, ary1[idx2]);
    }
    bw.Write(this.sid);
  }
}

partial class tos_room_side
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "side"; } }
  public override string Name { get { return "tos_room_side"; } }
  public override int Id { get { return 0x60006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.side);
    bw.Write(this.sid);
  }
}

partial class tos_room_change_spectator
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "change_spectator"; } }
  public override string Name { get { return "tos_room_change_spectator"; } }
  public override int Id { get { return 0x60007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.spectator);
    bw.Write(this.sid);
  }
}

partial class tos_room_ready
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "ready"; } }
  public override string Name { get { return "tos_room_ready"; } }
  public override int Id { get { return 0x60008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.ready);
    bw.Write(this.sid);
  }
}

partial class tos_room_start_game
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "start_game"; } }
  public override string Name { get { return "tos_room_start_game"; } }
  public override int Id { get { return 0x60009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_join_game
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "join_game"; } }
  public override string Name { get { return "tos_room_join_game"; } }
  public override int Id { get { return 0x6000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_start_match
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "start_match"; } }
  public override string Name { get { return "tos_room_start_match"; } }
  public override int Id { get { return 0x6000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_chat
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "chat"; } }
  public override string Name { get { return "tos_room_chat"; } }
  public override int Id { get { return 0x6000c; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.msg);
    bw.Write(this.sid);
  }
}

partial class toc_room_chat_msg
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "chat_msg"; } }
  public override string Name { get { return "toc_room_chat_msg"; } }
  public override int Id { get { return 0x6000d; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    name = LoadString(br);
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_room_kick_player
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "kick_player"; } }
  public override string Name { get { return "tos_room_kick_player"; } }
  public override int Id { get { return 0x6000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_room_invite
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "invite"; } }
  public override string Name { get { return "toc_room_invite"; } }
  public override int Id { get { return 0x6000f; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    type = LoadString(br);
    subtype = LoadString(br);
    room = br.ReadInt32();
    stage = br.ReadInt32();
    name = LoadString(br);
    rule = LoadString(br);
    inviter = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_room_invite
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "invite"; } }
  public override string Name { get { return "tos_room_invite"; } }
  public override int Id { get { return 0x6000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_room_info
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "info"; } }
  public override string Name { get { return "toc_room_info"; } }
  public override int Id { get { return 0x60010; } }

  public override void Load(BinaryReader br)
  {
    channel = br.ReadInt32();
    stage = br.ReadInt32();
    name = LoadString(br);
    map = br.ReadInt32();
    id = br.ReadInt32();
    leader = br.ReadInt64();
    rule = LoadString(br);
    int len1 = br.ReadUInt16();
    p_player_info[] ary2 = new p_player_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_player_info cls4 = new p_player_info();
      cls4.id = br.ReadInt64();
      cls4.pos = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.vip_level = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      cls4.ready = br.ReadBoolean();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_title_info[] ary6 = new p_title_info[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_title_info cls8 = new p_title_info();
        cls8.title = LoadString(br);
        cls8.cnt = br.ReadInt32();
        cls8.type = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.titles = ary6;
      cls4.hero_type = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    players = ary2;
    int len9 = br.ReadUInt16();
    p_spectator_info[] ary10 = new p_spectator_info[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      p_spectator_info cls12 = new p_spectator_info();
      cls12.id = br.ReadInt64();
      cls12.name = LoadString(br);
      cls12.icon = br.ReadInt32();
      cls12.level = br.ReadInt32();
      cls12.fight = br.ReadBoolean();
      cls12.ready = br.ReadBoolean();
      ary10[idx11] = cls12;
    }
    spectators = ary10;
    fight = br.ReadBoolean();
    password = br.ReadBoolean();
    auto_aim = br.ReadBoolean();
    max_player = br.ReadInt32();
    level = br.ReadInt32();
    int len13 = br.ReadUInt16();
    p_round_info[] ary14 = new p_round_info[len13];
    for (int idx15 = 0; idx15 < len13; idx15++)
    {
      p_round_info cls16 = new p_round_info();
      cls16.id = br.ReadInt32();
      cls16.times = br.ReadInt32();
      ary14[idx15] = cls16;
    }
    round = ary14;
    state = br.ReadInt32();
    level_limit = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_room_leader
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "leader"; } }
  public override string Name { get { return "tos_room_leader"; } }
  public override int Id { get { return 0x60011; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class tos_room_robot
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "robot"; } }
  public override string Name { get { return "tos_room_robot"; } }
  public override int Id { get { return 0x60012; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.index);
    bw.Write(this.pos);
    bw.Write(this.sid);
  }
}

partial class tos_room_quick_robot
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "quick_robot"; } }
  public override string Name { get { return "tos_room_quick_robot"; } }
  public override int Id { get { return 0x60013; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_kick_robot
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "kick_robot"; } }
  public override string Name { get { return "tos_room_kick_robot"; } }
  public override int Id { get { return 0x60014; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_room_survival_leader
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "survival_leader"; } }
  public override string Name { get { return "toc_room_survival_leader"; } }
  public override int Id { get { return 0x60015; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    level = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_room_leave_room
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "leave_room"; } }
  public override string Name { get { return "toc_room_leave_room"; } }
  public override int Id { get { return 0x60016; } }

  public override void Load(BinaryReader br)
  {
    reason = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_room_join_team
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "join_team"; } }
  public override string Name { get { return "tos_room_join_team"; } }
  public override int Id { get { return 0x60017; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class tos_room_create_room
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "create_room"; } }
  public override string Name { get { return "tos_room_create_room"; } }
  public override int Id { get { return 0x60018; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.type);
    bw.Write(this.typeid);
    SaveString(bw, this.password);
    bw.Write(this.sid);
  }
}

partial class toc_room_clientstop
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "clientstop"; } }
  public override string Name { get { return "toc_room_clientstop"; } }
  public override int Id { get { return 0x60019; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_room_cancel_match
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "cancel_match"; } }
  public override string Name { get { return "tos_room_cancel_match"; } }
  public override int Id { get { return 0x6001a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_room_match_time
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "match_time"; } }
  public override string Name { get { return "toc_room_match_time"; } }
  public override int Id { get { return 0x6001b; } }

  public override void Load(BinaryReader br)
  {
    match_time = br.ReadSingle();
    id = br.ReadInt64();
    name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_room_corpsmatch_give_up
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "corpsmatch_give_up"; } }
  public override string Name { get { return "toc_room_corpsmatch_give_up"; } }
  public override int Id { get { return 0x6001c; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_room_notice_leave
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "notice_leave"; } }
  public override string Name { get { return "toc_room_notice_leave"; } }
  public override int Id { get { return 0x6001d; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_room_notice_leave
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "notice_leave"; } }
  public override string Name { get { return "tos_room_notice_leave"; } }
  public override int Id { get { return 0x6001d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_room_level_limit
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "level_limit"; } }
  public override string Name { get { return "tos_room_level_limit"; } }
  public override int Id { get { return 0x6001e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.level_limit);
    bw.Write(this.sid);
  }
}

partial class toc_room_match_punished
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "match_punished"; } }
  public override string Name { get { return "toc_room_match_punished"; } }
  public override int Id { get { return 0x6001f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_room_match_punished.punish_info[] ary2 = new toc_room_match_punished.punish_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_room_match_punished.punish_info cls4 = new toc_room_match_punished.punish_info();
      cls4.player_id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.release_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_room_match_change_channel
{
  public override string Typ { get { return "room"; } }
  public override string Key { get { return "match_change_channel"; } }
  public override string Name { get { return "tos_room_match_change_channel"; } }
  public override int Id { get { return 0x60020; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.channel);
    bw.Write(this.sid);
  }
}


partial class toc_corps_create_corps
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "create_corps"; } }
  public override string Name { get { return "toc_corps_create_corps"; } }
  public override int Id { get { return 0x70001; } }

  public override void Load(BinaryReader br)
  {
    p_corps_data_toc cls1 = new p_corps_data_toc();
    cls1.corps_id = br.ReadInt64();
    cls1.legion_id = br.ReadInt64();
    cls1.level = br.ReadInt32();
    cls1.tribute = br.ReadInt32();
    cls1.name = LoadString(br);
    cls1.notice = LoadString(br);
    cls1.create_time = br.ReadInt32();
    cls1.creator_id = br.ReadInt64();
    cls1.creator_name = LoadString(br);
    cls1.leader_id = br.ReadInt64();
    cls1.leader_name = LoadString(br);
    int len2 = br.ReadUInt16();
    p_corps_member_toc[] ary3 = new p_corps_member_toc[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_corps_member_toc cls5 = new p_corps_member_toc();
      cls5.id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.icon = br.ReadInt32();
      cls5.level = br.ReadInt32();
      cls5.vip_level = br.ReadInt32();
      cls5.logout_time = br.ReadInt32();
      cls5.qualifying_win = br.ReadInt32();
      cls5.member_type = br.ReadInt32();
      cls5.fighting = br.ReadInt32();
      cls5.week_fighting = br.ReadInt32();
      cls5.handle = br.ReadInt32();
      cls5.sex = br.ReadInt32();
      cls5.hero_type = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.member_list = ary3;
    int len6 = br.ReadUInt16();
    p_corps_apply_toc[] ary7 = new p_corps_apply_toc[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_corps_apply_toc cls9 = new p_corps_apply_toc();
      cls9.id = br.ReadInt64();
      cls9.name = LoadString(br);
      cls9.icon = br.ReadInt32();
      cls9.level = br.ReadInt32();
      cls9.vip_level = br.ReadInt32();
      cls9.logout_time = br.ReadInt32();
      cls9.qualifying_win = br.ReadInt32();
      cls9.handle = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.apply_list = ary7;
    cls1.max_member = br.ReadInt32();
    cls1.fighting = br.ReadInt32();
    cls1.logo_pannel = br.ReadInt32();
    cls1.logo_icon = br.ReadInt32();
    cls1.logo_color = br.ReadInt32();
    cls1.enter_level = br.ReadInt32();
    int len10 = br.ReadUInt16();
    p_corps_log_toc[] ary11 = new p_corps_log_toc[len10];
    for (int idx12 = 0; idx12 < len10; idx12++)
    {
      p_corps_log_toc cls13 = new p_corps_log_toc();
      cls13.log_type = LoadString(br);
      cls13.opr_name = LoadString(br);
      cls13.opr_type = br.ReadInt32();
      cls13.mbr_name = LoadString(br);
      cls13.log_time = br.ReadInt32();
      cls13.param = br.ReadInt32();
      ary11[idx12] = cls13;
    }
    cls1.log_list = ary11;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_create_corps
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "create_corps"; } }
  public override string Name { get { return "tos_corps_create_corps"; } }
  public override int Id { get { return 0x70001; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.corps_name);
    bw.Write(this.pannel);
    bw.Write(this.icon);
    bw.Write(this.color);
    bw.Write(this.money_type);
    bw.Write(this.sid);
  }
}

partial class tos_corps_search_name
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "search_name"; } }
  public override string Name { get { return "tos_corps_search_name"; } }
  public override int Id { get { return 0x70002; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.corps_name);
    bw.Write(this.sid);
  }
}

partial class tos_corps_get_data
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_data"; } }
  public override string Name { get { return "tos_corps_get_data"; } }
  public override int Id { get { return 0x70003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_data
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "data"; } }
  public override string Name { get { return "toc_corps_data"; } }
  public override int Id { get { return 0x70004; } }

  public override void Load(BinaryReader br)
  {
    p_corps_data_toc cls1 = new p_corps_data_toc();
    cls1.corps_id = br.ReadInt64();
    cls1.legion_id = br.ReadInt64();
    cls1.level = br.ReadInt32();
    cls1.tribute = br.ReadInt32();
    cls1.name = LoadString(br);
    cls1.notice = LoadString(br);
    cls1.create_time = br.ReadInt32();
    cls1.creator_id = br.ReadInt64();
    cls1.creator_name = LoadString(br);
    cls1.leader_id = br.ReadInt64();
    cls1.leader_name = LoadString(br);
    int len2 = br.ReadUInt16();
    p_corps_member_toc[] ary3 = new p_corps_member_toc[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_corps_member_toc cls5 = new p_corps_member_toc();
      cls5.id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.icon = br.ReadInt32();
      cls5.level = br.ReadInt32();
      cls5.vip_level = br.ReadInt32();
      cls5.logout_time = br.ReadInt32();
      cls5.qualifying_win = br.ReadInt32();
      cls5.member_type = br.ReadInt32();
      cls5.fighting = br.ReadInt32();
      cls5.week_fighting = br.ReadInt32();
      cls5.handle = br.ReadInt32();
      cls5.sex = br.ReadInt32();
      cls5.hero_type = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.member_list = ary3;
    int len6 = br.ReadUInt16();
    p_corps_apply_toc[] ary7 = new p_corps_apply_toc[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_corps_apply_toc cls9 = new p_corps_apply_toc();
      cls9.id = br.ReadInt64();
      cls9.name = LoadString(br);
      cls9.icon = br.ReadInt32();
      cls9.level = br.ReadInt32();
      cls9.vip_level = br.ReadInt32();
      cls9.logout_time = br.ReadInt32();
      cls9.qualifying_win = br.ReadInt32();
      cls9.handle = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.apply_list = ary7;
    cls1.max_member = br.ReadInt32();
    cls1.fighting = br.ReadInt32();
    cls1.logo_pannel = br.ReadInt32();
    cls1.logo_icon = br.ReadInt32();
    cls1.logo_color = br.ReadInt32();
    cls1.enter_level = br.ReadInt32();
    int len10 = br.ReadUInt16();
    p_corps_log_toc[] ary11 = new p_corps_log_toc[len10];
    for (int idx12 = 0; idx12 < len10; idx12++)
    {
      p_corps_log_toc cls13 = new p_corps_log_toc();
      cls13.log_type = LoadString(br);
      cls13.opr_name = LoadString(br);
      cls13.opr_type = br.ReadInt32();
      cls13.mbr_name = LoadString(br);
      cls13.log_time = br.ReadInt32();
      cls13.param = br.ReadInt32();
      ary11[idx12] = cls13;
    }
    cls1.log_list = ary11;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_get_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_list"; } }
  public override string Name { get { return "toc_corps_get_list"; } }
  public override int Id { get { return 0x70005; } }

  public override void Load(BinaryReader br)
  {
    page = br.ReadInt32();
    pages = br.ReadInt32();
    start = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_corps_info_toc[] ary2 = new p_corps_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_info_toc cls4 = new p_corps_info_toc();
      cls4.corps_id = br.ReadInt64();
      cls4.corps_name = LoadString(br);
      cls4.leader_id = br.ReadInt64();
      cls4.leader_name = LoadString(br);
      cls4.member_cnt = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.max_member = br.ReadInt32();
      cls4.fighting = br.ReadInt32();
      cls4.enter_level = br.ReadInt32();
      cls4.logo_pannel = br.ReadInt32();
      cls4.logo_icon = br.ReadInt32();
      cls4.logo_color = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_get_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_list"; } }
  public override string Name { get { return "tos_corps_get_list"; } }
  public override int Id { get { return 0x70005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}

partial class toc_corps_apply_enter
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "apply_enter"; } }
  public override string Name { get { return "toc_corps_apply_enter"; } }
  public override int Id { get { return 0x70006; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_apply_enter
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "apply_enter"; } }
  public override string Name { get { return "tos_corps_apply_enter"; } }
  public override int Id { get { return 0x70006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.corps_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_agree_apply
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "toc_corps_agree_apply"; } }
  public override int Id { get { return 0x70007; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_agree_apply
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "tos_corps_agree_apply"; } }
  public override int Id { get { return 0x70007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.apply_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_refuse_apply
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "refuse_apply"; } }
  public override string Name { get { return "toc_corps_refuse_apply"; } }
  public override int Id { get { return 0x70008; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_refuse_apply
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "refuse_apply"; } }
  public override string Name { get { return "tos_corps_refuse_apply"; } }
  public override int Id { get { return 0x70008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.apply_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_quit
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "quit"; } }
  public override string Name { get { return "toc_corps_quit"; } }
  public override int Id { get { return 0x70009; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_quit
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "quit"; } }
  public override string Name { get { return "tos_corps_quit"; } }
  public override int Id { get { return 0x70009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_change_icon
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_icon"; } }
  public override string Name { get { return "toc_corps_change_icon"; } }
  public override int Id { get { return 0x7000a; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_change_icon
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_icon"; } }
  public override string Name { get { return "tos_corps_change_icon"; } }
  public override int Id { get { return 0x7000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pannel);
    bw.Write(this.icon);
    bw.Write(this.money_type);
    bw.Write(this.sid);
  }
}

partial class toc_corps_update_notice
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "update_notice"; } }
  public override string Name { get { return "toc_corps_update_notice"; } }
  public override int Id { get { return 0x7000b; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_update_notice
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "update_notice"; } }
  public override string Name { get { return "tos_corps_update_notice"; } }
  public override int Id { get { return 0x7000b; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.notice);
    bw.Write(this.sid);
  }
}

partial class toc_corps_change_member_type
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_member_type"; } }
  public override string Name { get { return "toc_corps_change_member_type"; } }
  public override int Id { get { return 0x7000c; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_change_member_type
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_member_type"; } }
  public override string Name { get { return "tos_corps_change_member_type"; } }
  public override int Id { get { return 0x7000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.member_id);
    bw.Write(this.member_type);
    bw.Write(this.sid);
  }
}

partial class toc_corps_set_leader
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "set_leader"; } }
  public override string Name { get { return "toc_corps_set_leader"; } }
  public override int Id { get { return 0x7000d; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_set_leader
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "set_leader"; } }
  public override string Name { get { return "tos_corps_set_leader"; } }
  public override int Id { get { return 0x7000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.leader_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_kick_member
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "kick_member"; } }
  public override string Name { get { return "toc_corps_kick_member"; } }
  public override int Id { get { return 0x7000e; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_kick_member
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "kick_member"; } }
  public override string Name { get { return "tos_corps_kick_member"; } }
  public override int Id { get { return 0x7000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.member_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_dismiss
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "dismiss"; } }
  public override string Name { get { return "toc_corps_dismiss"; } }
  public override int Id { get { return 0x7000f; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_dismiss
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "dismiss"; } }
  public override string Name { get { return "tos_corps_dismiss"; } }
  public override int Id { get { return 0x7000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_message
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "message"; } }
  public override string Name { get { return "toc_corps_message"; } }
  public override int Id { get { return 0x70010; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    pid = br.ReadInt64();
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_view_info
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "view_info"; } }
  public override string Name { get { return "toc_corps_view_info"; } }
  public override int Id { get { return 0x70011; } }

  public override void Load(BinaryReader br)
  {
    corps_id = br.ReadInt64();
    corps_name = LoadString(br);
    create_time = br.ReadInt32();
    member_cnt = br.ReadInt32();
    level = br.ReadInt32();
    max_member = br.ReadInt32();
    notice = LoadString(br);
    logo_pannel = br.ReadInt32();
    logo_icon = br.ReadInt32();
    logo_color = br.ReadInt32();
    enter_level = br.ReadInt32();
    leader_name = LoadString(br);
    int len1 = br.ReadUInt16();
    toc_corps_view_info.manager[] ary2 = new toc_corps_view_info.manager[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_corps_view_info.manager cls4 = new toc_corps_view_info.manager();
      cls4.member_id = br.ReadInt64();
      cls4.member_type = br.ReadInt32();
      cls4.member_name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.sex = br.ReadInt32();
      cls4.hero_type = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    managers = ary2;
    tribute = br.ReadInt32();
    fighting = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_view_info
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "view_info"; } }
  public override string Name { get { return "tos_corps_view_info"; } }
  public override int Id { get { return 0x70011; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.corps_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_notify_apply
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "notify_apply"; } }
  public override string Name { get { return "toc_corps_notify_apply"; } }
  public override int Id { get { return 0x70012; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    p_corps_apply_toc cls1 = new p_corps_apply_toc();
    cls1.id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.icon = br.ReadInt32();
    cls1.level = br.ReadInt32();
    cls1.vip_level = br.ReadInt32();
    cls1.logout_time = br.ReadInt32();
    cls1.qualifying_win = br.ReadInt32();
    cls1.handle = br.ReadInt32();
    apply = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_notify_members
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "notify_members"; } }
  public override string Name { get { return "toc_corps_notify_members"; } }
  public override int Id { get { return 0x70013; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_corps_notify_members.notify_member[] ary2 = new toc_corps_notify_members.notify_member[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_corps_notify_members.notify_member cls4 = new toc_corps_notify_members.notify_member();
      cls4.type = LoadString(br);
      p_corps_member_toc cls5 = new p_corps_member_toc();
      cls5.id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.icon = br.ReadInt32();
      cls5.level = br.ReadInt32();
      cls5.vip_level = br.ReadInt32();
      cls5.logout_time = br.ReadInt32();
      cls5.qualifying_win = br.ReadInt32();
      cls5.member_type = br.ReadInt32();
      cls5.fighting = br.ReadInt32();
      cls5.week_fighting = br.ReadInt32();
      cls5.handle = br.ReadInt32();
      cls5.sex = br.ReadInt32();
      cls5.hero_type = br.ReadInt32();
      cls4.member = cls5;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_notify_infos
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "notify_infos"; } }
  public override string Name { get { return "toc_corps_notify_infos"; } }
  public override int Id { get { return 0x70014; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_corps_notify_infos.notify_info[] ary2 = new toc_corps_notify_infos.notify_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_corps_notify_infos.notify_info cls4 = new toc_corps_notify_infos.notify_info();
      cls4.key = LoadString(br);
      cls4.str_val = LoadString(br);
      cls4.int_val = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_notify_logs
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "notify_logs"; } }
  public override string Name { get { return "toc_corps_notify_logs"; } }
  public override int Id { get { return 0x70015; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corps_log_toc[] ary2 = new p_corps_log_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_log_toc cls4 = new p_corps_log_toc();
      cls4.log_type = LoadString(br);
      cls4.opr_name = LoadString(br);
      cls4.opr_type = br.ReadInt32();
      cls4.mbr_name = LoadString(br);
      cls4.log_time = br.ReadInt32();
      cls4.param = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_donate_tribute
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "donate_tribute"; } }
  public override string Name { get { return "toc_corps_donate_tribute"; } }
  public override int Id { get { return 0x70016; } }

  public override void Load(BinaryReader br)
  {
    donate_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_donate_tribute
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "donate_tribute"; } }
  public override string Name { get { return "tos_corps_donate_tribute"; } }
  public override int Id { get { return 0x70016; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.donate_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_upgrade_level
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "upgrade_level"; } }
  public override string Name { get { return "toc_corps_upgrade_level"; } }
  public override int Id { get { return 0x70017; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_upgrade_level
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "upgrade_level"; } }
  public override string Name { get { return "tos_corps_upgrade_level"; } }
  public override int Id { get { return 0x70017; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_getlotterycounts
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "getlotterycounts"; } }
  public override string Name { get { return "toc_corps_getlotterycounts"; } }
  public override int Id { get { return 0x70018; } }

  public override void Load(BinaryReader br)
  {
    total_counts = br.ReadInt32();
    cur_counts = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_getlotterycounts
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "getlotterycounts"; } }
  public override string Name { get { return "tos_corps_getlotterycounts"; } }
  public override int Id { get { return 0x70018; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_openlottery
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "openlottery"; } }
  public override string Name { get { return "toc_corps_openlottery"; } }
  public override int Id { get { return 0x70019; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corps_lotterybox[] ary2 = new p_corps_lotterybox[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_lotterybox cls4 = new p_corps_lotterybox();
      cls4.box_id = br.ReadInt32();
      cls4.lottery_id = br.ReadInt32();
      cls4.can_use = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_openlottery
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "openlottery"; } }
  public override string Name { get { return "tos_corps_openlottery"; } }
  public override int Id { get { return 0x70019; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_uselottery
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "uselottery"; } }
  public override string Name { get { return "toc_corps_uselottery"; } }
  public override int Id { get { return 0x7001a; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    box_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_uselottery
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "uselottery"; } }
  public override string Name { get { return "tos_corps_uselottery"; } }
  public override int Id { get { return 0x7001a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_buylotterybox
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "buylotterybox"; } }
  public override string Name { get { return "toc_corps_buylotterybox"; } }
  public override int Id { get { return 0x7001b; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_buylotterybox
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "buylotterybox"; } }
  public override string Name { get { return "tos_corps_buylotterybox"; } }
  public override int Id { get { return 0x7001b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.box_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_donate_info
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "donate_info"; } }
  public override string Name { get { return "toc_corps_donate_info"; } }
  public override int Id { get { return 0x7001c; } }

  public override void Load(BinaryReader br)
  {
    donated_diamond = br.ReadInt32();
    donated_coin = br.ReadInt32();
    next_diamond_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_donate_info
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "donate_info"; } }
  public override string Name { get { return "tos_corps_donate_info"; } }
  public override int Id { get { return 0x7001c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_enter_mall
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "enter_mall"; } }
  public override string Name { get { return "toc_corps_enter_mall"; } }
  public override int Id { get { return 0x7001d; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corps_mallbox[] ary2 = new p_corps_mallbox[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_mallbox cls4 = new p_corps_mallbox();
      cls4.box_id = br.ReadInt32();
      cls4.mall_id = br.ReadInt32();
      cls4.item_count = br.ReadInt32();
      cls4.can_use = br.ReadBoolean();
      cls4.highest_bidder_id = br.ReadInt32();
      cls4.highest_bidder_name = LoadString(br);
      cls4.highest_bid = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    next_flush_timestamp = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_enter_mall
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "enter_mall"; } }
  public override string Name { get { return "tos_corps_enter_mall"; } }
  public override int Id { get { return 0x7001d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_buymallbox
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "buymallbox"; } }
  public override string Name { get { return "toc_corps_buymallbox"; } }
  public override int Id { get { return 0x7001e; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_buymallbox
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "buymallbox"; } }
  public override string Name { get { return "tos_corps_buymallbox"; } }
  public override int Id { get { return 0x7001e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.box_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_mallbid
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "mallbid"; } }
  public override string Name { get { return "toc_corps_mallbid"; } }
  public override int Id { get { return 0x7001f; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_mallbid
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "mallbid"; } }
  public override string Name { get { return "tos_corps_mallbid"; } }
  public override int Id { get { return 0x7001f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.box_id);
    bw.Write(this.mall_id);
    bw.Write(this.bid_price);
    bw.Write(this.sid);
  }
}

partial class toc_corps_mission_data
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "mission_data"; } }
  public override string Name { get { return "toc_corps_mission_data"; } }
  public override int Id { get { return 0x70020; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corps_mission[] ary2 = new p_corps_mission[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_mission cls4 = new p_corps_mission();
      cls4.id = br.ReadInt32();
      cls4.type = br.ReadInt32();
      cls4.status = br.ReadInt32();
      cls4.progress = br.ReadInt32();
      cls4.partake = br.ReadInt32();
      cls4.finisher = LoadString(br);
      ary2[idx3] = cls4;
    }
    mission_list = ary2;
    activeness = br.ReadInt32();
    int len5 = br.ReadUInt16();
    System.Int32[] ary6 = new System.Int32[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt32();
    }
    rewarded_list = ary6;
    toc_corps_mission_data.member_info cls8 = new toc_corps_mission_data.member_info();
    cls8.id = br.ReadInt64();
    cls8.name = LoadString(br);
    cls8.icon = br.ReadInt32();
    last_star = cls8;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_mission_data
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "mission_data"; } }
  public override string Name { get { return "tos_corps_mission_data"; } }
  public override int Id { get { return 0x70020; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_notify_mission
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "notify_mission"; } }
  public override string Name { get { return "toc_corps_notify_mission"; } }
  public override int Id { get { return 0x70021; } }

  public override void Load(BinaryReader br)
  {
    p_corps_mission cls1 = new p_corps_mission();
    cls1.id = br.ReadInt32();
    cls1.type = br.ReadInt32();
    cls1.status = br.ReadInt32();
    cls1.progress = br.ReadInt32();
    cls1.partake = br.ReadInt32();
    cls1.finisher = LoadString(br);
    mission = cls1;
    activeness = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_corps_get_activeness_reward
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_activeness_reward"; } }
  public override string Name { get { return "toc_corps_get_activeness_reward"; } }
  public override int Id { get { return 0x70022; } }

  public override void Load(BinaryReader br)
  {
    reward_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_get_activeness_reward
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_activeness_reward"; } }
  public override string Name { get { return "tos_corps_get_activeness_reward"; } }
  public override int Id { get { return 0x70022; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.reward_id);
    bw.Write(this.sid);
  }
}

partial class toc_corps_open_instance
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "open_instance"; } }
  public override string Name { get { return "toc_corps_open_instance"; } }
  public override int Id { get { return 0x70023; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_open_instance
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "open_instance"; } }
  public override string Name { get { return "tos_corps_open_instance"; } }
  public override int Id { get { return 0x70023; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_corps_start_instance
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "start_instance"; } }
  public override string Name { get { return "tos_corps_start_instance"; } }
  public override int Id { get { return 0x70024; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class tos_corps_set_enter_level
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "set_enter_level"; } }
  public override string Name { get { return "tos_corps_set_enter_level"; } }
  public override int Id { get { return 0x70025; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.level);
    bw.Write(this.sid);
  }
}

partial class toc_corps_recmd_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "recmd_list"; } }
  public override string Name { get { return "toc_corps_recmd_list"; } }
  public override int Id { get { return 0x70026; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corps_info_toc[] ary2 = new p_corps_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corps_info_toc cls4 = new p_corps_info_toc();
      cls4.corps_id = br.ReadInt64();
      cls4.corps_name = LoadString(br);
      cls4.leader_id = br.ReadInt64();
      cls4.leader_name = LoadString(br);
      cls4.member_cnt = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.max_member = br.ReadInt32();
      cls4.fighting = br.ReadInt32();
      cls4.enter_level = br.ReadInt32();
      cls4.logo_pannel = br.ReadInt32();
      cls4.logo_icon = br.ReadInt32();
      cls4.logo_color = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_recmd_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "recmd_list"; } }
  public override string Name { get { return "tos_corps_recmd_list"; } }
  public override int Id { get { return 0x70026; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_get_hurt_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_hurt_list"; } }
  public override string Name { get { return "toc_corps_get_hurt_list"; } }
  public override int Id { get { return 0x70027; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_corps_get_hurt_list.p_hurt_info[] ary2 = new toc_corps_get_hurt_list.p_hurt_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_corps_get_hurt_list.p_hurt_info cls4 = new toc_corps_get_hurt_list.p_hurt_info();
      cls4.player_id = br.ReadInt64();
      cls4.hurt = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_get_hurt_list
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "get_hurt_list"; } }
  public override string Name { get { return "tos_corps_get_hurt_list"; } }
  public override int Id { get { return 0x70027; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corps_change_name
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_name"; } }
  public override string Name { get { return "toc_corps_change_name"; } }
  public override int Id { get { return 0x70028; } }

  public override void Load(BinaryReader br)
  {
    pannel = br.ReadInt32();
    icon = br.ReadInt32();
    color = br.ReadInt32();
    corps_name = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class tos_corps_change_name
{
  public override string Typ { get { return "corps"; } }
  public override string Key { get { return "change_name"; } }
  public override string Name { get { return "tos_corps_change_name"; } }
  public override int Id { get { return 0x70028; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pannel);
    bw.Write(this.icon);
    bw.Write(this.color);
    SaveString(bw, this.corps_name);
    bw.Write(this.sid);
  }
}


partial class toc_corpsmatch_score_info
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "score_info"; } }
  public override string Name { get { return "toc_corpsmatch_score_info"; } }
  public override int Id { get { return 0x80001; } }

  public override void Load(BinaryReader br)
  {
    player_score = br.ReadInt32();
    corps_score = br.ReadInt32();
    remain_rounds = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_score_info
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "score_info"; } }
  public override string Name { get { return "tos_corpsmatch_score_info"; } }
  public override int Id { get { return 0x80001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_stage
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "stage"; } }
  public override string Name { get { return "toc_corpsmatch_stage"; } }
  public override int Id { get { return 0x80002; } }

  public override void Load(BinaryReader br)
  {
    stage = br.ReadInt32();
    week = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_stage
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "stage"; } }
  public override string Name { get { return "tos_corpsmatch_stage"; } }
  public override int Id { get { return 0x80002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_group_score
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "group_score"; } }
  public override string Name { get { return "toc_corpsmatch_group_score"; } }
  public override int Id { get { return 0x80003; } }

  public override void Load(BinaryReader br)
  {
    score = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_register
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "register"; } }
  public override string Name { get { return "tos_corpsmatch_register"; } }
  public override int Id { get { return 0x80004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_is_registered
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "is_registered"; } }
  public override string Name { get { return "toc_corpsmatch_is_registered"; } }
  public override int Id { get { return 0x80005; } }

  public override void Load(BinaryReader br)
  {
    is_registered = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_is_registered
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "is_registered"; } }
  public override string Name { get { return "tos_corpsmatch_is_registered"; } }
  public override int Id { get { return 0x80005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_register_num
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "register_num"; } }
  public override string Name { get { return "toc_corpsmatch_register_num"; } }
  public override int Id { get { return 0x80006; } }

  public override void Load(BinaryReader br)
  {
    register_num = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_register_num
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "register_num"; } }
  public override string Name { get { return "tos_corpsmatch_register_num"; } }
  public override int Id { get { return 0x80006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_timeline
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "timeline"; } }
  public override string Name { get { return "toc_corpsmatch_timeline"; } }
  public override int Id { get { return 0x80007; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_timeline
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "timeline"; } }
  public override string Name { get { return "tos_corpsmatch_timeline"; } }
  public override int Id { get { return 0x80007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_vote
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "vote"; } }
  public override string Name { get { return "toc_corpsmatch_vote"; } }
  public override int Id { get { return 0x80008; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_vote
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "vote"; } }
  public override string Name { get { return "tos_corpsmatch_vote"; } }
  public override int Id { get { return 0x80008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_vote_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "vote_result"; } }
  public override string Name { get { return "toc_corpsmatch_vote_result"; } }
  public override int Id { get { return 0x80009; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    System.Int32[][] ary2 = new System.Int32[len1][];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      int len4 = br.ReadUInt16();
      System.Int32[] ary5 = new System.Int32[len4];
      for (int idx6 = 0; idx6 < len4; idx6++)
      {
        ary5[idx6] = br.ReadInt32();
      }
      ary2[idx3] = ary5;
    }
    votes_num = ary2;
    int len7 = br.ReadUInt16();
    System.Int32[][] ary8 = new System.Int32[len7][];
    for (int idx9 = 0; idx9 < len7; idx9++)
    {
      int len10 = br.ReadUInt16();
      System.Int32[] ary11 = new System.Int32[len10];
      for (int idx12 = 0; idx12 < len10; idx12++)
      {
        ary11[idx12] = br.ReadInt32();
      }
      ary8[idx9] = ary11;
    }
    corps_num = ary8;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_vote_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "vote_result"; } }
  public override string Name { get { return "tos_corpsmatch_vote_result"; } }
  public override int Id { get { return 0x80009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_matchtable
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "matchtable"; } }
  public override string Name { get { return "toc_corpsmatch_matchtable"; } }
  public override int Id { get { return 0x8000a; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_matchtable
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "matchtable"; } }
  public override string Name { get { return "tos_corpsmatch_matchtable"; } }
  public override int Id { get { return 0x8000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_last_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "last_result"; } }
  public override string Name { get { return "toc_corpsmatch_last_result"; } }
  public override int Id { get { return 0x8000b; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_last_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "last_result"; } }
  public override string Name { get { return "tos_corpsmatch_last_result"; } }
  public override int Id { get { return 0x8000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_match_info
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "match_info"; } }
  public override string Name { get { return "toc_corpsmatch_match_info"; } }
  public override int Id { get { return 0x8000c; } }

  public override void Load(BinaryReader br)
  {
    my_id = br.ReadInt64();
    other_id = br.ReadInt64();
    int len1 = br.ReadUInt16();
    System.Int32[] ary2 = new System.Int32[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ary2[idx3] = br.ReadInt32();
    }
    mode = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_match_info
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "match_info"; } }
  public override string Name { get { return "tos_corpsmatch_match_info"; } }
  public override int Id { get { return 0x8000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_room
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "room"; } }
  public override string Name { get { return "toc_corpsmatch_room"; } }
  public override int Id { get { return 0x8000d; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_room
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "room"; } }
  public override string Name { get { return "tos_corpsmatch_room"; } }
  public override int Id { get { return 0x8000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_record
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "record"; } }
  public override string Name { get { return "toc_corpsmatch_record"; } }
  public override int Id { get { return 0x8000e; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corpsmatch_record[] ary2 = new p_corpsmatch_record[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corpsmatch_record cls4 = new p_corpsmatch_record();
      cls4.name = LoadString(br);
      cls4.win = br.ReadInt32();
      cls4.day = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    records = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_record
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "record"; } }
  public override string Name { get { return "tos_corpsmatch_record"; } }
  public override int Id { get { return 0x8000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_last_record
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "last_record"; } }
  public override string Name { get { return "toc_corpsmatch_last_record"; } }
  public override int Id { get { return 0x8000f; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corpsmatch_view[] ary2 = new p_corpsmatch_view[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corpsmatch_view cls4 = new p_corpsmatch_view();
      cls4.day = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_corpsmatch_team[] ary6 = new p_corpsmatch_team[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_corpsmatch_team cls8 = new p_corpsmatch_team();
        cls8.icon = br.ReadInt32();
        cls8.name = LoadString(br);
        cls8.pos = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.teams = ary6;
      ary2[idx3] = cls4;
    }
    records = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_last_record
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "last_record"; } }
  public override string Name { get { return "tos_corpsmatch_last_record"; } }
  public override int Id { get { return 0x8000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.is_last);
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_match_list
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "match_list"; } }
  public override string Name { get { return "toc_corpsmatch_match_list"; } }
  public override int Id { get { return 0x80010; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corpsmatch_room[] ary2 = new p_corpsmatch_room[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corpsmatch_room cls4 = new p_corpsmatch_room();
      cls4.room_id = br.ReadInt32();
      cls4.team1 = LoadString(br);
      cls4.icon1 = br.ReadInt32();
      cls4.team2 = LoadString(br);
      cls4.icon2 = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_match_list
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "match_list"; } }
  public override string Name { get { return "tos_corpsmatch_match_list"; } }
  public override int Id { get { return 0x80010; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "result"; } }
  public override string Name { get { return "toc_corpsmatch_result"; } }
  public override int Id { get { return 0x80011; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corpsmatch_resultteam[] ary2 = new p_corpsmatch_resultteam[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corpsmatch_resultteam cls4 = new p_corpsmatch_resultteam();
      cls4.name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.panel = br.ReadInt32();
      cls4.color = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    team = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_result
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "result"; } }
  public override string Name { get { return "tos_corpsmatch_result"; } }
  public override int Id { get { return 0x80011; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_corpsmatch_full
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "full"; } }
  public override string Name { get { return "toc_corpsmatch_full"; } }
  public override int Id { get { return 0x80012; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_corpsmatch_top16
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "top16"; } }
  public override string Name { get { return "toc_corpsmatch_top16"; } }
  public override int Id { get { return 0x80013; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_corpsmatch_member[] ary2 = new p_corpsmatch_member[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_corpsmatch_member cls4 = new p_corpsmatch_member();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      ary2[idx3] = cls4;
    }
    members = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_corpsmatch_top16
{
  public override string Typ { get { return "corpsmatch"; } }
  public override string Key { get { return "top16"; } }
  public override string Name { get { return "tos_corpsmatch_top16"; } }
  public override int Id { get { return 0x80013; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}


partial class tos_master_add_master
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "add_master"; } }
  public override string Name { get { return "tos_master_add_master"; } }
  public override int Id { get { return 0x90001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_master_agree_apply
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "toc_master_agree_apply"; } }
  public override int Id { get { return 0x90002; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_agree_apply
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "tos_master_agree_apply"; } }
  public override int Id { get { return 0x90002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.agree);
    bw.Write(this.sid);
  }
}

partial class toc_master_apprentice_info
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "apprentice_info"; } }
  public override string Name { get { return "toc_master_apprentice_info"; } }
  public override int Id { get { return 0x90003; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_master_base_info_toc[] ary2 = new p_master_base_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_master_base_info_toc cls4 = new p_master_base_info_toc();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    masters = ary2;
    int len5 = br.ReadUInt16();
    p_master_base_info_toc[] ary6 = new p_master_base_info_toc[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_master_base_info_toc cls8 = new p_master_base_info_toc();
      cls8.id = br.ReadInt64();
      cls8.name = LoadString(br);
      cls8.sex = br.ReadInt32();
      cls8.icon = br.ReadInt32();
      cls8.honor = br.ReadInt32();
      cls8.vip_level = br.ReadInt32();
      cls8.level = br.ReadInt32();
      cls8.handle = br.ReadInt32();
      cls8.room = br.ReadInt32();
      cls8.team = br.ReadInt32();
      cls8.mvp = br.ReadInt32();
      cls8.gold_ace = br.ReadInt32();
      cls8.silver_ace = br.ReadInt32();
      cls8.fight = br.ReadBoolean();
      ary6[idx7] = cls8;
    }
    apprentices = ary6;
    kick_master_time = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_apprentice_info
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "apprentice_info"; } }
  public override string Name { get { return "tos_master_apprentice_info"; } }
  public override int Id { get { return 0x90003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_kick_master
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "kick_master"; } }
  public override string Name { get { return "toc_master_kick_master"; } }
  public override int Id { get { return 0x90004; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_kick_master
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "kick_master"; } }
  public override string Name { get { return "tos_master_kick_master"; } }
  public override int Id { get { return 0x90004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.kick_master);
    SaveString(bw, this.reason);
    bw.Write(this.sid);
  }
}

partial class toc_master_rcmnd_master_list
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "rcmnd_master_list"; } }
  public override string Name { get { return "toc_master_rcmnd_master_list"; } }
  public override int Id { get { return 0x90005; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_master_base_info_toc[] ary2 = new p_master_base_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_master_base_info_toc cls4 = new p_master_base_info_toc();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    recommends = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_master_rcmnd_master_list
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "rcmnd_master_list"; } }
  public override string Name { get { return "tos_master_rcmnd_master_list"; } }
  public override int Id { get { return 0x90005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_apply_master_list
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "apply_master_list"; } }
  public override string Name { get { return "toc_master_apply_master_list"; } }
  public override int Id { get { return 0x90006; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_master_base_info_toc[] ary2 = new p_master_base_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_master_base_info_toc cls4 = new p_master_base_info_toc();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.sex = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.honor = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.level = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.room = br.ReadInt32();
      cls4.team = br.ReadInt32();
      cls4.mvp = br.ReadInt32();
      cls4.gold_ace = br.ReadInt32();
      cls4.silver_ace = br.ReadInt32();
      cls4.fight = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    applies = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_master_apply_master_list
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "apply_master_list"; } }
  public override string Name { get { return "tos_master_apply_master_list"; } }
  public override int Id { get { return 0x90006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_notice
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "notice"; } }
  public override string Name { get { return "toc_master_notice"; } }
  public override int Id { get { return 0x90007; } }

  public override void Load(BinaryReader br)
  {
    login = br.ReadBoolean();
    master = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_master_master_welfare
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "master_welfare"; } }
  public override string Name { get { return "toc_master_master_welfare"; } }
  public override int Id { get { return 0x90008; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_master_welfare_toc[] ary2 = new p_master_welfare_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_master_welfare_toc cls4 = new p_master_welfare_toc();
      cls4.player_id = br.ReadInt64();
      int len5 = br.ReadUInt16();
      System.Int32[] ary6 = new System.Int32[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        ary6[idx7] = br.ReadInt32();
      }
      cls4.welfare_ids = ary6;
      cls4.gold = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    welfares = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_master_master_welfare
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "master_welfare"; } }
  public override string Name { get { return "tos_master_master_welfare"; } }
  public override int Id { get { return 0x90008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_get_master_welfare
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "get_master_welfare"; } }
  public override string Name { get { return "toc_master_get_master_welfare"; } }
  public override int Id { get { return 0x90009; } }

  public override void Load(BinaryReader br)
  {
    welfare_id = br.ReadInt32();
    gold = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_get_master_welfare
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "get_master_welfare"; } }
  public override string Name { get { return "tos_master_get_master_welfare"; } }
  public override int Id { get { return 0x90009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.welfare_id);
    bw.Write(this.is_gold);
    bw.Write(this.sid);
  }
}

partial class toc_master_has_welfare_apply
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "has_welfare_apply"; } }
  public override string Name { get { return "toc_master_has_welfare_apply"; } }
  public override int Id { get { return 0x9000a; } }

  public override void Load(BinaryReader br)
  {
    has_master = br.ReadBoolean();
    has_apprentice = br.ReadBoolean();
    apply = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class toc_master_tree
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "tree"; } }
  public override string Name { get { return "toc_master_tree"; } }
  public override int Id { get { return 0x9000b; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    master_tree_toc[] ary2 = new master_tree_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      master_tree_toc cls4 = new master_tree_toc();
      cls4.master = br.ReadInt64();
      cls4.apprentice = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.sex = br.ReadInt32();
      cls4.level = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    masters = ary2;
    int len5 = br.ReadUInt16();
    master_tree_toc[] ary6 = new master_tree_toc[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      master_tree_toc cls8 = new master_tree_toc();
      cls8.master = br.ReadInt64();
      cls8.apprentice = br.ReadInt64();
      cls8.name = LoadString(br);
      cls8.icon = br.ReadInt32();
      cls8.sex = br.ReadInt32();
      cls8.level = br.ReadInt32();
      ary6[idx7] = cls8;
    }
    mates = ary6;
    int len9 = br.ReadUInt16();
    master_tree_toc[] ary10 = new master_tree_toc[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      master_tree_toc cls12 = new master_tree_toc();
      cls12.master = br.ReadInt64();
      cls12.apprentice = br.ReadInt64();
      cls12.name = LoadString(br);
      cls12.icon = br.ReadInt32();
      cls12.sex = br.ReadInt32();
      cls12.level = br.ReadInt32();
      ary10[idx11] = cls12;
    }
    apprentice = ary10;
    int len13 = br.ReadUInt16();
    master_tree_toc[] ary14 = new master_tree_toc[len13];
    for (int idx15 = 0; idx15 < len13; idx15++)
    {
      master_tree_toc cls16 = new master_tree_toc();
      cls16.master = br.ReadInt64();
      cls16.apprentice = br.ReadInt64();
      cls16.name = LoadString(br);
      cls16.icon = br.ReadInt32();
      cls16.sex = br.ReadInt32();
      cls16.level = br.ReadInt32();
      ary14[idx15] = cls16;
    }
    second_apprentice = ary14;
    sid = br.ReadUInt32();
  }
}

partial class tos_master_tree
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "tree"; } }
  public override string Name { get { return "tos_master_tree"; } }
  public override int Id { get { return 0x9000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_double_exp
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "double_exp"; } }
  public override string Name { get { return "toc_master_double_exp"; } }
  public override int Id { get { return 0x9000c; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_master_double_exp_toc[] ary2 = new p_master_double_exp_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_master_double_exp_toc cls4 = new p_master_double_exp_toc();
      cls4.id = br.ReadInt64();
      cls4.rest_num = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    masters = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_master_double_exp
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "double_exp"; } }
  public override string Name { get { return "tos_master_double_exp"; } }
  public override int Id { get { return 0x9000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_grade
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade"; } }
  public override string Name { get { return "toc_master_grade"; } }
  public override int Id { get { return 0x9000d; } }

  public override void Load(BinaryReader br)
  {
    point = br.ReadInt32();
    grade_id = br.ReadInt32();
    todayPoint = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_grade
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade"; } }
  public override string Name { get { return "tos_master_grade"; } }
  public override int Id { get { return 0x9000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_master_grade_drawgif
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_drawgif"; } }
  public override string Name { get { return "toc_master_grade_drawgif"; } }
  public override int Id { get { return 0x9000e; } }

  public override void Load(BinaryReader br)
  {
    result = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_grade_drawgif
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_drawgif"; } }
  public override string Name { get { return "tos_master_grade_drawgif"; } }
  public override int Id { get { return 0x9000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.item_id);
    bw.Write(this.sid);
  }
}

partial class toc_master_grade_get_point
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_get_point"; } }
  public override string Name { get { return "toc_master_grade_get_point"; } }
  public override int Id { get { return 0x9000f; } }

  public override void Load(BinaryReader br)
  {
    point = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class toc_master_grade_isdraw
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_isdraw"; } }
  public override string Name { get { return "toc_master_grade_isdraw"; } }
  public override int Id { get { return 0x90010; } }

  public override void Load(BinaryReader br)
  {
    isdraw = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_grade_isdraw
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_isdraw"; } }
  public override string Name { get { return "tos_master_grade_isdraw"; } }
  public override int Id { get { return 0x90010; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.grade_id);
    bw.Write(this.sid);
  }
}

partial class toc_master_grade_has_welfare
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "grade_has_welfare"; } }
  public override string Name { get { return "toc_master_grade_has_welfare"; } }
  public override int Id { get { return 0x90011; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class toc_master_put_weapon_to_arsenal
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "put_weapon_to_arsenal"; } }
  public override string Name { get { return "toc_master_put_weapon_to_arsenal"; } }
  public override int Id { get { return 0x90012; } }

  public override void Load(BinaryReader br)
  {
    uid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_put_weapon_to_arsenal
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "put_weapon_to_arsenal"; } }
  public override string Name { get { return "tos_master_put_weapon_to_arsenal"; } }
  public override int Id { get { return 0x90012; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.uid);
    bw.Write(this.sid);
  }
}

partial class toc_master_borrow_weapon
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "borrow_weapon"; } }
  public override string Name { get { return "toc_master_borrow_weapon"; } }
  public override int Id { get { return 0x90013; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    uid = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_borrow_weapon
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "borrow_weapon"; } }
  public override string Name { get { return "tos_master_borrow_weapon"; } }
  public override int Id { get { return 0x90013; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.uid);
    bw.Write(this.sid);
  }
}

partial class toc_master_arsenal
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "arsenal"; } }
  public override string Name { get { return "toc_master_arsenal"; } }
  public override int Id { get { return 0x90014; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    weapon_attr[] ary2 = new weapon_attr[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      weapon_attr cls4 = new weapon_attr();
      cls4.id = br.ReadInt32();
      cls4.uid = br.ReadInt32();
      cls4.expire_time = br.ReadInt32();
      cls4.status = br.ReadInt32();
      cls4.apprentice_name = LoadString(br);
      cls4.is_borrowed = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    arsenal = ary2;
    is_master = br.ReadBoolean();
    sid = br.ReadUInt32();
  }
}

partial class tos_master_arsenal
{
  public override string Typ { get { return "master"; } }
  public override string Key { get { return "arsenal"; } }
  public override string Name { get { return "tos_master_arsenal"; } }
  public override int Id { get { return 0x90014; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.is_master);
    bw.Write(this.sid);
  }
}


partial class toc_legion_create_legion
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "create_legion"; } }
  public override string Name { get { return "toc_legion_create_legion"; } }
  public override int Id { get { return 0xa0001; } }

  public override void Load(BinaryReader br)
  {
    p_legion_data_toc cls1 = new p_legion_data_toc();
    cls1.legion_id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.logo_icon = br.ReadInt32();
    cls1.notice = LoadString(br);
    cls1.create_time = br.ReadInt32();
    cls1.capacity = br.ReadInt32();
    cls1.creator_cid = br.ReadInt64();
    cls1.creator_pid = br.ReadInt64();
    cls1.creator_name = LoadString(br);
    cls1.leader_cid = br.ReadInt64();
    cls1.leader_name = LoadString(br);
    int len2 = br.ReadUInt16();
    p_legion_corps_toc[] ary3 = new p_legion_corps_toc[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_legion_corps_toc cls5 = new p_legion_corps_toc();
      cls5.corps_id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.leader_id = br.ReadInt64();
      cls5.leader_name = LoadString(br);
      cls5.leader_icon = br.ReadInt32();
      cls5.member_cnt = br.ReadInt32();
      cls5.logo_pannel = br.ReadInt32();
      cls5.logo_icon = br.ReadInt32();
      cls5.logo_color = br.ReadInt32();
      cls5.enter_time = br.ReadInt32();
      cls5.legion_post = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.corps_list = ary3;
    int len6 = br.ReadUInt16();
    p_legion_apply_toc[] ary7 = new p_legion_apply_toc[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_legion_apply_toc cls9 = new p_legion_apply_toc();
      cls9.corps_id = br.ReadInt64();
      cls9.name = LoadString(br);
      cls9.leader_id = br.ReadInt64();
      cls9.leader_name = LoadString(br);
      cls9.member_cnt = br.ReadInt32();
      cls9.fighting = br.ReadInt32();
      cls9.logo_pannel = br.ReadInt32();
      cls9.logo_icon = br.ReadInt32();
      cls9.logo_color = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.apply_list = ary7;
    cls1.activeness = br.ReadInt32();
    int len10 = br.ReadUInt16();
    p_legion_log_toc[] ary11 = new p_legion_log_toc[len10];
    for (int idx12 = 0; idx12 < len10; idx12++)
    {
      p_legion_log_toc cls13 = new p_legion_log_toc();
      cls13.log_type = LoadString(br);
      cls13.opr_name = LoadString(br);
      cls13.opr_post = br.ReadInt32();
      cls13.name = LoadString(br);
      cls13.log_time = br.ReadInt32();
      cls13.param = br.ReadInt32();
      ary11[idx12] = cls13;
    }
    cls1.log_list = ary11;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_create_legion
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "create_legion"; } }
  public override string Name { get { return "tos_legion_create_legion"; } }
  public override int Id { get { return 0xa0001; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.legion_name);
    bw.Write(this.icon);
    bw.Write(this.money_type);
    bw.Write(this.sid);
  }
}

partial class tos_legion_search_name
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "search_name"; } }
  public override string Name { get { return "tos_legion_search_name"; } }
  public override int Id { get { return 0xa0002; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.legion_name);
    bw.Write(this.sid);
  }
}

partial class tos_legion_get_data
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "get_data"; } }
  public override string Name { get { return "tos_legion_get_data"; } }
  public override int Id { get { return 0xa0003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legion_data
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "data"; } }
  public override string Name { get { return "toc_legion_data"; } }
  public override int Id { get { return 0xa0004; } }

  public override void Load(BinaryReader br)
  {
    p_legion_data_toc cls1 = new p_legion_data_toc();
    cls1.legion_id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.logo_icon = br.ReadInt32();
    cls1.notice = LoadString(br);
    cls1.create_time = br.ReadInt32();
    cls1.capacity = br.ReadInt32();
    cls1.creator_cid = br.ReadInt64();
    cls1.creator_pid = br.ReadInt64();
    cls1.creator_name = LoadString(br);
    cls1.leader_cid = br.ReadInt64();
    cls1.leader_name = LoadString(br);
    int len2 = br.ReadUInt16();
    p_legion_corps_toc[] ary3 = new p_legion_corps_toc[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_legion_corps_toc cls5 = new p_legion_corps_toc();
      cls5.corps_id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.leader_id = br.ReadInt64();
      cls5.leader_name = LoadString(br);
      cls5.leader_icon = br.ReadInt32();
      cls5.member_cnt = br.ReadInt32();
      cls5.logo_pannel = br.ReadInt32();
      cls5.logo_icon = br.ReadInt32();
      cls5.logo_color = br.ReadInt32();
      cls5.enter_time = br.ReadInt32();
      cls5.legion_post = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.corps_list = ary3;
    int len6 = br.ReadUInt16();
    p_legion_apply_toc[] ary7 = new p_legion_apply_toc[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_legion_apply_toc cls9 = new p_legion_apply_toc();
      cls9.corps_id = br.ReadInt64();
      cls9.name = LoadString(br);
      cls9.leader_id = br.ReadInt64();
      cls9.leader_name = LoadString(br);
      cls9.member_cnt = br.ReadInt32();
      cls9.fighting = br.ReadInt32();
      cls9.logo_pannel = br.ReadInt32();
      cls9.logo_icon = br.ReadInt32();
      cls9.logo_color = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.apply_list = ary7;
    cls1.activeness = br.ReadInt32();
    int len10 = br.ReadUInt16();
    p_legion_log_toc[] ary11 = new p_legion_log_toc[len10];
    for (int idx12 = 0; idx12 < len10; idx12++)
    {
      p_legion_log_toc cls13 = new p_legion_log_toc();
      cls13.log_type = LoadString(br);
      cls13.opr_name = LoadString(br);
      cls13.opr_post = br.ReadInt32();
      cls13.name = LoadString(br);
      cls13.log_time = br.ReadInt32();
      cls13.param = br.ReadInt32();
      ary11[idx12] = cls13;
    }
    cls1.log_list = ary11;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_get_list
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "get_list"; } }
  public override string Name { get { return "toc_legion_get_list"; } }
  public override int Id { get { return 0xa0005; } }

  public override void Load(BinaryReader br)
  {
    page = br.ReadInt32();
    pages = br.ReadInt32();
    start = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legion_info_toc[] ary2 = new p_legion_info_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legion_info_toc cls4 = new p_legion_info_toc();
      cls4.legion_id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.leader_name = LoadString(br);
      cls4.member_cnt = br.ReadInt32();
      cls4.activeness = br.ReadInt32();
      cls4.corps_cnt = br.ReadInt32();
      cls4.capacity = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_get_list
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "get_list"; } }
  public override string Name { get { return "tos_legion_get_list"; } }
  public override int Id { get { return 0xa0005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}

partial class toc_legion_get_member_list
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "get_member_list"; } }
  public override string Name { get { return "toc_legion_get_member_list"; } }
  public override int Id { get { return 0xa0006; } }

  public override void Load(BinaryReader br)
  {
    page = br.ReadInt32();
    pages = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legion_member_toc[] ary2 = new p_legion_member_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legion_member_toc cls4 = new p_legion_member_toc();
      cls4.id = br.ReadInt64();
      cls4.icon = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.level = br.ReadInt32();
      cls4.vip_level = br.ReadInt32();
      cls4.corps_id = br.ReadInt64();
      cls4.corps_name = LoadString(br);
      cls4.member_type = br.ReadInt32();
      cls4.handle = br.ReadInt32();
      cls4.logout_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_get_member_list
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "get_member_list"; } }
  public override string Name { get { return "tos_legion_get_member_list"; } }
  public override int Id { get { return 0xa0006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}

partial class toc_legion_apply_enter
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "apply_enter"; } }
  public override string Name { get { return "toc_legion_apply_enter"; } }
  public override int Id { get { return 0xa0007; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_apply_enter
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "apply_enter"; } }
  public override string Name { get { return "tos_legion_apply_enter"; } }
  public override int Id { get { return 0xa0007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.legion_id);
    bw.Write(this.sid);
  }
}

partial class toc_legion_agree_apply
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "toc_legion_agree_apply"; } }
  public override int Id { get { return 0xa0008; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_agree_apply
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "agree_apply"; } }
  public override string Name { get { return "tos_legion_agree_apply"; } }
  public override int Id { get { return 0xa0008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.apply_id);
    bw.Write(this.sid);
  }
}

partial class toc_legion_refuse_apply
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "refuse_apply"; } }
  public override string Name { get { return "toc_legion_refuse_apply"; } }
  public override int Id { get { return 0xa0009; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_refuse_apply
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "refuse_apply"; } }
  public override string Name { get { return "tos_legion_refuse_apply"; } }
  public override int Id { get { return 0xa0009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.apply_id);
    bw.Write(this.sid);
  }
}

partial class toc_legion_quit
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "quit"; } }
  public override string Name { get { return "toc_legion_quit"; } }
  public override int Id { get { return 0xa000a; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_quit
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "quit"; } }
  public override string Name { get { return "tos_legion_quit"; } }
  public override int Id { get { return 0xa000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legion_change_icon
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "change_icon"; } }
  public override string Name { get { return "toc_legion_change_icon"; } }
  public override int Id { get { return 0xa000b; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_change_icon
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "change_icon"; } }
  public override string Name { get { return "tos_legion_change_icon"; } }
  public override int Id { get { return 0xa000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.icon);
    bw.Write(this.sid);
  }
}

partial class toc_legion_update_notice
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "update_notice"; } }
  public override string Name { get { return "toc_legion_update_notice"; } }
  public override int Id { get { return 0xa000c; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_update_notice
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "update_notice"; } }
  public override string Name { get { return "tos_legion_update_notice"; } }
  public override int Id { get { return 0xa000c; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.notice);
    bw.Write(this.sid);
  }
}

partial class toc_legion_change_corps_post
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "change_corps_post"; } }
  public override string Name { get { return "toc_legion_change_corps_post"; } }
  public override int Id { get { return 0xa000d; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_change_corps_post
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "change_corps_post"; } }
  public override string Name { get { return "tos_legion_change_corps_post"; } }
  public override int Id { get { return 0xa000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.corps_id);
    bw.Write(this.post_type);
    bw.Write(this.sid);
  }
}

partial class toc_legion_set_leader
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "set_leader"; } }
  public override string Name { get { return "toc_legion_set_leader"; } }
  public override int Id { get { return 0xa000e; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_set_leader
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "set_leader"; } }
  public override string Name { get { return "tos_legion_set_leader"; } }
  public override int Id { get { return 0xa000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.leader_cid);
    bw.Write(this.sid);
  }
}

partial class toc_legion_kick_corps
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "kick_corps"; } }
  public override string Name { get { return "toc_legion_kick_corps"; } }
  public override int Id { get { return 0xa000f; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_kick_corps
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "kick_corps"; } }
  public override string Name { get { return "tos_legion_kick_corps"; } }
  public override int Id { get { return 0xa000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.corps_id);
    bw.Write(this.sid);
  }
}

partial class toc_legion_dismiss
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "dismiss"; } }
  public override string Name { get { return "toc_legion_dismiss"; } }
  public override int Id { get { return 0xa0010; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_dismiss
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "dismiss"; } }
  public override string Name { get { return "tos_legion_dismiss"; } }
  public override int Id { get { return 0xa0010; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legion_message
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "message"; } }
  public override string Name { get { return "toc_legion_message"; } }
  public override int Id { get { return 0xa0011; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    pid = br.ReadInt64();
    cid = br.ReadInt64();
    msg = LoadString(br);
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_view_info
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "view_info"; } }
  public override string Name { get { return "toc_legion_view_info"; } }
  public override int Id { get { return 0xa0012; } }

  public override void Load(BinaryReader br)
  {
    legion_id = br.ReadInt64();
    name = LoadString(br);
    logo_icon = br.ReadInt32();
    notice = LoadString(br);
    capacity = br.ReadInt32();
    member_cnt = br.ReadInt32();
    activeness = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legion_manager_toc[] ary2 = new p_legion_manager_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legion_manager_toc cls4 = new p_legion_manager_toc();
      cls4.id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.legion_post = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    managers = ary2;
    int len5 = br.ReadUInt16();
    p_legion_corps_toc[] ary6 = new p_legion_corps_toc[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_legion_corps_toc cls8 = new p_legion_corps_toc();
      cls8.corps_id = br.ReadInt64();
      cls8.name = LoadString(br);
      cls8.leader_id = br.ReadInt64();
      cls8.leader_name = LoadString(br);
      cls8.leader_icon = br.ReadInt32();
      cls8.member_cnt = br.ReadInt32();
      cls8.logo_pannel = br.ReadInt32();
      cls8.logo_icon = br.ReadInt32();
      cls8.logo_color = br.ReadInt32();
      cls8.enter_time = br.ReadInt32();
      cls8.legion_post = br.ReadInt32();
      ary6[idx7] = cls8;
    }
    corps_list = ary6;
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_view_info
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "view_info"; } }
  public override string Name { get { return "tos_legion_view_info"; } }
  public override int Id { get { return 0xa0012; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.legion_id);
    bw.Write(this.sid);
  }
}

partial class toc_legion_notify_apply
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "notify_apply"; } }
  public override string Name { get { return "toc_legion_notify_apply"; } }
  public override int Id { get { return 0xa0013; } }

  public override void Load(BinaryReader br)
  {
    type = LoadString(br);
    p_legion_apply_toc cls1 = new p_legion_apply_toc();
    cls1.corps_id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.leader_id = br.ReadInt64();
    cls1.leader_name = LoadString(br);
    cls1.member_cnt = br.ReadInt32();
    cls1.fighting = br.ReadInt32();
    cls1.logo_pannel = br.ReadInt32();
    cls1.logo_icon = br.ReadInt32();
    cls1.logo_color = br.ReadInt32();
    apply = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_notify_corps
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "notify_corps"; } }
  public override string Name { get { return "toc_legion_notify_corps"; } }
  public override int Id { get { return 0xa0014; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_legion_notify_corps.notify_corps[] ary2 = new toc_legion_notify_corps.notify_corps[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_legion_notify_corps.notify_corps cls4 = new toc_legion_notify_corps.notify_corps();
      cls4.type = LoadString(br);
      p_legion_corps_toc cls5 = new p_legion_corps_toc();
      cls5.corps_id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.leader_id = br.ReadInt64();
      cls5.leader_name = LoadString(br);
      cls5.leader_icon = br.ReadInt32();
      cls5.member_cnt = br.ReadInt32();
      cls5.logo_pannel = br.ReadInt32();
      cls5.logo_icon = br.ReadInt32();
      cls5.logo_color = br.ReadInt32();
      cls5.enter_time = br.ReadInt32();
      cls5.legion_post = br.ReadInt32();
      cls4.corps = cls5;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_notify_infos
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "notify_infos"; } }
  public override string Name { get { return "toc_legion_notify_infos"; } }
  public override int Id { get { return 0xa0015; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    toc_legion_notify_infos.notify_info[] ary2 = new toc_legion_notify_infos.notify_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_legion_notify_infos.notify_info cls4 = new toc_legion_notify_infos.notify_info();
      cls4.key = LoadString(br);
      cls4.str_val = LoadString(br);
      cls4.int_val = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_notify_logs
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "notify_logs"; } }
  public override string Name { get { return "toc_legion_notify_logs"; } }
  public override int Id { get { return 0xa0016; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_legion_log_toc[] ary2 = new p_legion_log_toc[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legion_log_toc cls4 = new p_legion_log_toc();
      cls4.log_type = LoadString(br);
      cls4.opr_name = LoadString(br);
      cls4.opr_post = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.log_time = br.ReadInt32();
      cls4.param = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class toc_legion_enlarge_capacity
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "enlarge_capacity"; } }
  public override string Name { get { return "toc_legion_enlarge_capacity"; } }
  public override int Id { get { return 0xa0017; } }

  public override void Load(BinaryReader br)
  {
    sid = br.ReadUInt32();
  }
}

partial class tos_legion_enlarge_capacity
{
  public override string Typ { get { return "legion"; } }
  public override string Key { get { return "enlarge_capacity"; } }
  public override string Name { get { return "tos_legion_enlarge_capacity"; } }
  public override int Id { get { return 0xa0017; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}


partial class toc_legionwar_data
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "data"; } }
  public override string Name { get { return "toc_legionwar_data"; } }
  public override int Id { get { return 0xb0001; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_legionwar_legion_info[] ary2 = new p_legionwar_legion_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_legion_info cls4 = new p_legionwar_legion_info();
      cls4.id = br.ReadInt32();
      cls4.battle = br.ReadInt32();
      cls4.death = br.ReadInt32();
      cls4.win = br.ReadInt32();
      cls4.capture = br.ReadInt32();
      cls4.cur_fighter = br.ReadInt32();
      cls4.cur_battles = br.ReadInt32();
      cls4.mass_order = br.ReadInt32();
      cls4.mass_order_time = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    legion_list = ary2;
    int len5 = br.ReadUInt16();
    p_legionwar_territory[] ary6 = new p_legionwar_territory[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_legionwar_territory cls8 = new p_legionwar_territory();
      cls8.id = br.ReadInt32();
      cls8.owner = br.ReadInt32();
      int len9 = br.ReadUInt16();
      System.Int32[] ary10 = new System.Int32[len9];
      for (int idx11 = 0; idx11 < len9; idx11++)
      {
        ary10[idx11] = br.ReadInt32();
      }
      cls8.legions = ary10;
      ary6[idx7] = cls8;
    }
    territory_list = ary6;
    int len12 = br.ReadUInt16();
    p_legionwar_log[] ary13 = new p_legionwar_log[len12];
    for (int idx14 = 0; idx14 < len12; idx14++)
    {
      p_legionwar_log cls15 = new p_legionwar_log();
      cls15.time = br.ReadInt32();
      cls15.type = br.ReadInt32();
      cls15.name1 = LoadString(br);
      cls15.name2 = LoadString(br);
      cls15.param1 = br.ReadInt32();
      cls15.param2 = br.ReadInt32();
      cls15.param3 = br.ReadInt32();
      ary13[idx14] = cls15;
    }
    battle_logs = ary13;
    cur_season = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_data
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "data"; } }
  public override string Name { get { return "tos_legionwar_data"; } }
  public override int Id { get { return 0xb0001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_fighter_ranks
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "fighter_ranks"; } }
  public override string Name { get { return "toc_legionwar_fighter_ranks"; } }
  public override int Id { get { return 0xb0002; } }

  public override void Load(BinaryReader br)
  {
    page = br.ReadInt32();
    start = br.ReadInt32();
    size = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legionwar_fighter[] ary2 = new p_legionwar_fighter[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_fighter cls4 = new p_legionwar_fighter();
      cls4.player_id = br.ReadInt64();
      cls4.legion_id = br.ReadInt32();
      cls4.name = LoadString(br);
      cls4.level = br.ReadInt32();
      cls4.icon = br.ReadInt32();
      cls4.corps_name = LoadString(br);
      cls4.contribution = br.ReadInt32();
      cls4.position = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_fighter_ranks
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "fighter_ranks"; } }
  public override string Name { get { return "tos_legionwar_fighter_ranks"; } }
  public override int Id { get { return 0xb0002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_set_mass_order
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "set_mass_order"; } }
  public override string Name { get { return "toc_legionwar_set_mass_order"; } }
  public override int Id { get { return 0xb0003; } }

  public override void Load(BinaryReader br)
  {
    territory_id = br.ReadInt32();
    legion_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_set_mass_order
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "set_mass_order"; } }
  public override string Name { get { return "tos_legionwar_set_mass_order"; } }
  public override int Id { get { return 0xb0003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.territory_id);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_my_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "my_info"; } }
  public override string Name { get { return "toc_legionwar_my_info"; } }
  public override int Id { get { return 0xb0004; } }

  public override void Load(BinaryReader br)
  {
    legion_id = br.ReadInt32();
    battle = br.ReadInt32();
    win = br.ReadInt32();
    kill = br.ReadInt32();
    death = br.ReadInt32();
    contribution = br.ReadInt32();
    cur_position = br.ReadInt32();
    int len1 = br.ReadUInt16();
    toc_legionwar_my_info.HistoryPosition[] ary2 = new toc_legionwar_my_info.HistoryPosition[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      toc_legionwar_my_info.HistoryPosition cls4 = new toc_legionwar_my_info.HistoryPosition();
      cls4.position = br.ReadInt32();
      cls4.times = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    history_position = ary2;
    have_reward = br.ReadBoolean();
    daily_battle = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_my_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "my_info"; } }
  public override string Name { get { return "tos_legionwar_my_info"; } }
  public override int Id { get { return 0xb0004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_get_reward
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "get_reward"; } }
  public override string Name { get { return "toc_legionwar_get_reward"; } }
  public override int Id { get { return 0xb0005; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    ItemInfo[] ary2 = new ItemInfo[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      ItemInfo cls4 = new ItemInfo();
      cls4.ID = br.ReadInt32();
      cls4.cnt = br.ReadInt32();
      cls4.day = br.ReadSingle();
      ary2[idx3] = cls4;
    }
    item_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_get_reward
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "get_reward"; } }
  public override string Name { get { return "tos_legionwar_get_reward"; } }
  public override int Id { get { return 0xb0005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_my_territory_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "my_territory_info"; } }
  public override string Name { get { return "toc_legionwar_my_territory_info"; } }
  public override int Id { get { return 0xb0006; } }

  public override void Load(BinaryReader br)
  {
    territory_id = br.ReadInt32();
    battle = br.ReadInt32();
    win = br.ReadInt32();
    kill = br.ReadInt32();
    death = br.ReadInt32();
    contribution = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_my_territory_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "my_territory_info"; } }
  public override string Name { get { return "tos_legionwar_my_territory_info"; } }
  public override int Id { get { return 0xb0006; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.territory_id);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_start_match
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "start_match"; } }
  public override string Name { get { return "toc_legionwar_start_match"; } }
  public override int Id { get { return 0xb0007; } }

  public override void Load(BinaryReader br)
  {
    territory_id = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_start_match
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "start_match"; } }
  public override string Name { get { return "tos_legionwar_start_match"; } }
  public override int Id { get { return 0xb0007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.territory_id);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_territory_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "territory_info"; } }
  public override string Name { get { return "toc_legionwar_territory_info"; } }
  public override int Id { get { return 0xb0008; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt32();
    owner = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legionwar_territory_legion[] ary2 = new p_legionwar_territory_legion[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_territory_legion cls4 = new p_legionwar_territory_legion();
      cls4.legion = br.ReadInt32();
      cls4.occupy = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_legionwar_officer[] ary6 = new p_legionwar_officer[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_legionwar_officer cls8 = new p_legionwar_officer();
        cls8.player_id = br.ReadInt64();
        cls8.name = LoadString(br);
        cls8.icon = br.ReadInt32();
        cls8.contribution = br.ReadInt32();
        cls8.position = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.bestors = ary6;
      cls4.battle = br.ReadInt32();
      cls4.death = br.ReadInt32();
      cls4.win = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    legions = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_territory_info
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "territory_info"; } }
  public override string Name { get { return "tos_legionwar_territory_info"; } }
  public override int Id { get { return 0xb0008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.territory_id);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_legion_officers
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "legion_officers"; } }
  public override string Name { get { return "toc_legionwar_legion_officers"; } }
  public override int Id { get { return 0xb0009; } }

  public override void Load(BinaryReader br)
  {
    legion_id = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legionwar_officer[] ary2 = new p_legionwar_officer[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_officer cls4 = new p_legionwar_officer();
      cls4.player_id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.contribution = br.ReadInt32();
      cls4.position = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    officers = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_legion_officers
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "legion_officers"; } }
  public override string Name { get { return "tos_legionwar_legion_officers"; } }
  public override int Id { get { return 0xb0009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.legion_id);
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_eminentors
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "eminentors"; } }
  public override string Name { get { return "toc_legionwar_eminentors"; } }
  public override int Id { get { return 0xb000a; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_legionwar_eminentor[] ary2 = new p_legionwar_eminentor[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_eminentor cls4 = new p_legionwar_eminentor();
      cls4.player_id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.icon = br.ReadInt32();
      cls4.lead_times = br.ReadInt32();
      cls4.corps_name = LoadString(br);
      int len5 = br.ReadUInt16();
      p_hero_card_info[] ary6 = new p_hero_card_info[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_hero_card_info cls8 = new p_hero_card_info();
        cls8.hero_type = br.ReadInt32();
        cls8.end_time = br.ReadInt32();
        ary6[idx7] = cls8;
      }
      cls4.hero_cards = ary6;
      ary2[idx3] = cls4;
    }
    eminentors = ary2;
    int len9 = br.ReadUInt16();
    p_hero_card_info[] ary10 = new p_hero_card_info[len9];
    for (int idx11 = 0; idx11 < len9; idx11++)
    {
      p_hero_card_info cls12 = new p_hero_card_info();
      cls12.hero_type = br.ReadInt32();
      cls12.end_time = br.ReadInt32();
      ary10[idx11] = cls12;
    }
    hero_cards = ary10;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_eminentors
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "eminentors"; } }
  public override string Name { get { return "tos_legionwar_eminentors"; } }
  public override int Id { get { return 0xb000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_legionwar_corps_ranks
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "corps_ranks"; } }
  public override string Name { get { return "toc_legionwar_corps_ranks"; } }
  public override int Id { get { return 0xb000b; } }

  public override void Load(BinaryReader br)
  {
    page = br.ReadInt32();
    start = br.ReadInt32();
    size = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_legionwar_corps[] ary2 = new p_legionwar_corps[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_legionwar_corps cls4 = new p_legionwar_corps();
      cls4.corps_id = br.ReadInt64();
      cls4.name = LoadString(br);
      cls4.leader = LoadString(br);
      cls4.member_cnt = br.ReadInt32();
      cls4.legion_id = br.ReadInt32();
      cls4.contribution = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_legionwar_corps_ranks
{
  public override string Typ { get { return "legionwar"; } }
  public override string Key { get { return "corps_ranks"; } }
  public override string Name { get { return "tos_legionwar_corps_ranks"; } }
  public override int Id { get { return 0xb000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.page);
    bw.Write(this.sid);
  }
}


partial class toc_family_add_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "add_family"; } }
  public override string Name { get { return "toc_family_add_family"; } }
  public override int Id { get { return 0xc0001; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_family_add_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "add_family"; } }
  public override string Name { get { return "tos_family_add_family"; } }
  public override int Id { get { return 0xc0001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    SaveString(bw, this.from_relative);
    SaveString(bw, this.to_relative);
    bw.Write(this.sid);
  }
}

partial class toc_family_agree_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "agree_family"; } }
  public override string Name { get { return "toc_family_agree_family"; } }
  public override int Id { get { return 0xc0002; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_family_agree_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "agree_family"; } }
  public override string Name { get { return "tos_family_agree_family"; } }
  public override int Id { get { return 0xc0002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.agree);
    bw.Write(this.sid);
  }
}

partial class toc_family_del_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "del_family"; } }
  public override string Name { get { return "toc_family_del_family"; } }
  public override int Id { get { return 0xc0003; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_family_del_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "del_family"; } }
  public override string Name { get { return "tos_family_del_family"; } }
  public override int Id { get { return 0xc0003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}

partial class toc_family_family_list
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "family_list"; } }
  public override string Name { get { return "toc_family_family_list"; } }
  public override int Id { get { return 0xc0004; } }

  public override void Load(BinaryReader br)
  {
    max_family_cnt = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_family_data[] ary2 = new p_family_data[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_family_data cls4 = new p_family_data();
      cls4.relative = LoadString(br);
      p_friend_data cls5 = new p_friend_data();
      cls5.id = br.ReadInt64();
      cls5.name = LoadString(br);
      cls5.sex = br.ReadInt32();
      cls5.icon = br.ReadInt32();
      cls5.honor = br.ReadInt32();
      cls5.vip_level = br.ReadInt32();
      cls5.level = br.ReadInt32();
      cls5.handle = br.ReadInt32();
      cls5.channel = br.ReadInt32();
      cls5.room = br.ReadInt32();
      cls5.team = br.ReadInt32();
      cls5.mvp = br.ReadInt32();
      cls5.gold_ace = br.ReadInt32();
      cls5.silver_ace = br.ReadInt32();
      cls5.fight = br.ReadBoolean();
      cls5.logout_time = br.ReadInt32();
      int len6 = br.ReadUInt16();
      p_hero_card_info[] ary7 = new p_hero_card_info[len6];
      for (int idx8 = 0; idx8 < len6; idx8++)
      {
        p_hero_card_info cls9 = new p_hero_card_info();
        cls9.hero_type = br.ReadInt32();
        cls9.end_time = br.ReadInt32();
        ary7[idx8] = cls9;
      }
      cls5.hero_cards = ary7;
      int len10 = br.ReadUInt16();
      p_title_info[] ary11 = new p_title_info[len10];
      for (int idx12 = 0; idx12 < len10; idx12++)
      {
        p_title_info cls13 = new p_title_info();
        cls13.title = LoadString(br);
        cls13.cnt = br.ReadInt32();
        cls13.type = br.ReadInt32();
        ary11[idx12] = cls13;
      }
      cls5.set_titles = ary11;
      cls4.data = cls5;
      cls4.value = br.ReadInt32();
      ary2[idx3] = cls4;
    }
    family_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_family_family_list
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "family_list"; } }
  public override string Name { get { return "tos_family_family_list"; } }
  public override int Id { get { return 0xc0004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_family_update_family
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "update_family"; } }
  public override string Name { get { return "toc_family_update_family"; } }
  public override int Id { get { return 0xc0005; } }

  public override void Load(BinaryReader br)
  {
    del = br.ReadBoolean();
    p_family_data cls1 = new p_family_data();
    cls1.relative = LoadString(br);
    p_friend_data cls2 = new p_friend_data();
    cls2.id = br.ReadInt64();
    cls2.name = LoadString(br);
    cls2.sex = br.ReadInt32();
    cls2.icon = br.ReadInt32();
    cls2.honor = br.ReadInt32();
    cls2.vip_level = br.ReadInt32();
    cls2.level = br.ReadInt32();
    cls2.handle = br.ReadInt32();
    cls2.channel = br.ReadInt32();
    cls2.room = br.ReadInt32();
    cls2.team = br.ReadInt32();
    cls2.mvp = br.ReadInt32();
    cls2.gold_ace = br.ReadInt32();
    cls2.silver_ace = br.ReadInt32();
    cls2.fight = br.ReadBoolean();
    cls2.logout_time = br.ReadInt32();
    int len3 = br.ReadUInt16();
    p_hero_card_info[] ary4 = new p_hero_card_info[len3];
    for (int idx5 = 0; idx5 < len3; idx5++)
    {
      p_hero_card_info cls6 = new p_hero_card_info();
      cls6.hero_type = br.ReadInt32();
      cls6.end_time = br.ReadInt32();
      ary4[idx5] = cls6;
    }
    cls2.hero_cards = ary4;
    int len7 = br.ReadUInt16();
    p_title_info[] ary8 = new p_title_info[len7];
    for (int idx9 = 0; idx9 < len7; idx9++)
    {
      p_title_info cls10 = new p_title_info();
      cls10.title = LoadString(br);
      cls10.cnt = br.ReadInt32();
      cls10.type = br.ReadInt32();
      ary8[idx9] = cls10;
    }
    cls2.set_titles = ary8;
    cls1.data = cls2;
    cls1.value = br.ReadInt32();
    family_data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_family_family_apply_info
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "family_apply_info"; } }
  public override string Name { get { return "toc_family_family_apply_info"; } }
  public override int Id { get { return 0xc0006; } }

  public override void Load(BinaryReader br)
  {
    id = br.ReadInt64();
    from_relative = LoadString(br);
    to_relative = LoadString(br);
    p_friend_data cls1 = new p_friend_data();
    cls1.id = br.ReadInt64();
    cls1.name = LoadString(br);
    cls1.sex = br.ReadInt32();
    cls1.icon = br.ReadInt32();
    cls1.honor = br.ReadInt32();
    cls1.vip_level = br.ReadInt32();
    cls1.level = br.ReadInt32();
    cls1.handle = br.ReadInt32();
    cls1.channel = br.ReadInt32();
    cls1.room = br.ReadInt32();
    cls1.team = br.ReadInt32();
    cls1.mvp = br.ReadInt32();
    cls1.gold_ace = br.ReadInt32();
    cls1.silver_ace = br.ReadInt32();
    cls1.fight = br.ReadBoolean();
    cls1.logout_time = br.ReadInt32();
    int len2 = br.ReadUInt16();
    p_hero_card_info[] ary3 = new p_hero_card_info[len2];
    for (int idx4 = 0; idx4 < len2; idx4++)
    {
      p_hero_card_info cls5 = new p_hero_card_info();
      cls5.hero_type = br.ReadInt32();
      cls5.end_time = br.ReadInt32();
      ary3[idx4] = cls5;
    }
    cls1.hero_cards = ary3;
    int len6 = br.ReadUInt16();
    p_title_info[] ary7 = new p_title_info[len6];
    for (int idx8 = 0; idx8 < len6; idx8++)
    {
      p_title_info cls9 = new p_title_info();
      cls9.title = LoadString(br);
      cls9.cnt = br.ReadInt32();
      cls9.type = br.ReadInt32();
      ary7[idx8] = cls9;
    }
    cls1.set_titles = ary7;
    data = cls1;
    sid = br.ReadUInt32();
  }
}

partial class toc_family_open_family_pos
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "open_family_pos"; } }
  public override string Name { get { return "toc_family_open_family_pos"; } }
  public override int Id { get { return 0xc0007; } }

  public override void Load(BinaryReader br)
  {
    pos = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_family_open_family_pos
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "open_family_pos"; } }
  public override string Name { get { return "tos_family_open_family_pos"; } }
  public override int Id { get { return 0xc0007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.pos);
    bw.Write(this.sid);
  }
}

partial class tos_family_add_family_relation
{
  public override string Typ { get { return "family"; } }
  public override string Key { get { return "add_family_relation"; } }
  public override string Name { get { return "tos_family_add_family_relation"; } }
  public override int Id { get { return 0xc0008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.sid);
  }
}


partial class toc_record_history_list
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "history_list"; } }
  public override string Name { get { return "toc_record_history_list"; } }
  public override int Id { get { return 0xd0001; } }

  public override void Load(BinaryReader br)
  {
    game_type = LoadString(br);
    level = br.ReadInt32();
    page = br.ReadInt32();
    total_page = br.ReadInt32();
    platform = LoadString(br);
    int len1 = br.ReadUInt16();
    p_record_history[] ary2 = new p_record_history[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_record_history cls4 = new p_record_history();
      cls4.id = br.ReadInt64();
      cls4.win_camp = br.ReadInt32();
      cls4.schedule = br.ReadInt32();
      cls4.game_type = LoadString(br);
      cls4.map = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.watch_times = br.ReadInt32();
      int len5 = br.ReadUInt16();
      p_team_info[] ary6 = new p_team_info[len5];
      for (int idx7 = 0; idx7 < len5; idx7++)
      {
        p_team_info cls8 = new p_team_info();
        cls8.name = LoadString(br);
        cls8.corps_name = LoadString(br);
        cls8.icon = br.ReadInt32();
        cls8.logo_pannel = br.ReadInt32();
        cls8.logo_icon = br.ReadInt32();
        cls8.logo_color = br.ReadInt32();
        cls8.camp = br.ReadInt32();
        cls8.win = br.ReadInt32();
        cls8.kill = br.ReadInt32();
        int len9 = br.ReadUInt16();
        p_team_role[] ary10 = new p_team_role[len9];
        for (int idx11 = 0; idx11 < len9; idx11++)
        {
          p_team_role cls12 = new p_team_role();
          cls12.player_id = br.ReadInt64();
          cls12.icon = br.ReadInt32();
          cls12.level = br.ReadInt32();
          cls12.name = LoadString(br);
          cls12.corps_id = br.ReadInt32();
          cls12.corps_name = LoadString(br);
          cls12.honor = br.ReadInt32();
          ary10[idx11] = cls12;
        }
        cls8.roles = ary10;
        ary6[idx7] = cls8;
      }
      cls4.teams = ary6;
      ary2[idx3] = cls4;
    }
    list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_record_history_list
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "history_list"; } }
  public override string Name { get { return "tos_record_history_list"; } }
  public override int Id { get { return 0xd0001; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.game_type);
    bw.Write(this.level);
    bw.Write(this.page);
    bw.Write(this.by_center);
    SaveString(bw, this.platform);
    bw.Write(this.sid);
  }
}

partial class tos_record_watch_record
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "watch_record"; } }
  public override string Name { get { return "tos_record_watch_record"; } }
  public override int Id { get { return 0xd0002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.id);
    bw.Write(this.by_center);
    bw.Write(this.sid);
  }
}

partial class tos_record_stop_watch
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "stop_watch"; } }
  public override string Name { get { return "tos_record_stop_watch"; } }
  public override int Id { get { return 0xd0003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.by_center);
    bw.Write(this.sid);
  }
}

partial class tos_record_start_watch
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "start_watch"; } }
  public override string Name { get { return "tos_record_start_watch"; } }
  public override int Id { get { return 0xd0004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.by_center);
    bw.Write(this.sid);
  }
}

partial class tos_record_set_speed
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "set_speed"; } }
  public override string Name { get { return "tos_record_set_speed"; } }
  public override int Id { get { return 0xd0005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.speed);
    bw.Write(this.by_center);
    bw.Write(this.sid);
  }
}

partial class tos_record_send_message
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "send_message"; } }
  public override string Name { get { return "tos_record_send_message"; } }
  public override int Id { get { return 0xd0006; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.msg);
    bw.Write(this.by_center);
    bw.Write(this.sid);
  }
}

partial class toc_record_message
{
  public override string Typ { get { return "record"; } }
  public override string Key { get { return "message"; } }
  public override string Name { get { return "toc_record_message"; } }
  public override int Id { get { return 0xd0007; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_record_message[] ary2 = new p_record_message[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_record_message cls4 = new p_record_message();
      cls4.pass_time = br.ReadSingle();
      cls4.player_id = br.ReadInt32();
      cls4.hero_type = br.ReadInt32();
      cls4.msg = LoadString(br);
      ary2[idx3] = cls4;
    }
    messages = ary2;
    sid = br.ReadUInt32();
  }
}


partial class toc_microblog_my_data
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "my_data"; } }
  public override string Name { get { return "toc_microblog_my_data"; } }
  public override int Id { get { return 0xe0001; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_microblog_content[] ary2 = new p_microblog_content[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_microblog_content cls4 = new p_microblog_content();
      cls4.id = br.ReadInt64();
      cls4.player_id = br.ReadInt64();
      cls4.msg = LoadString(br);
      cls4.praise_cnt = br.ReadInt32();
      cls4.comment_cnt = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.is_praised = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    blog_list = ary2;
    int len5 = br.ReadUInt16();
    System.Int64[] ary6 = new System.Int64[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      ary6[idx7] = br.ReadInt64();
    }
    follow_list = ary6;
    int len8 = br.ReadUInt16();
    System.Int64[] ary9 = new System.Int64[len8];
    for (int idx10 = 0; idx10 < len8; idx10++)
    {
      ary9[idx10] = br.ReadInt64();
    }
    fans_list = ary9;
    int len11 = br.ReadUInt16();
    System.Int64[] ary12 = new System.Int64[len11];
    for (int idx13 = 0; idx13 < len11; idx13++)
    {
      ary12[idx13] = br.ReadInt64();
    }
    black_list = ary12;
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_my_data
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "my_data"; } }
  public override string Name { get { return "tos_microblog_my_data"; } }
  public override int Id { get { return 0xe0001; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.sid);
  }
}

partial class toc_microblog_view_player
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "view_player"; } }
  public override string Name { get { return "toc_microblog_view_player"; } }
  public override int Id { get { return 0xe0002; } }

  public override void Load(BinaryReader br)
  {
    player_id = br.ReadInt64();
    name = LoadString(br);
    level = br.ReadInt32();
    icon = br.ReadInt32();
    icon_url = LoadString(br);
    fans_cnt = br.ReadInt32();
    int len1 = br.ReadUInt16();
    p_microblog_content[] ary2 = new p_microblog_content[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_microblog_content cls4 = new p_microblog_content();
      cls4.id = br.ReadInt64();
      cls4.player_id = br.ReadInt64();
      cls4.msg = LoadString(br);
      cls4.praise_cnt = br.ReadInt32();
      cls4.comment_cnt = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.is_praised = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    blog_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_view_player
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "view_player"; } }
  public override string Name { get { return "tos_microblog_view_player"; } }
  public override int Id { get { return 0xe0002; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.view_pid);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_blog_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "blog_list"; } }
  public override string Name { get { return "toc_microblog_blog_list"; } }
  public override int Id { get { return 0xe0003; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_microblog_content[] ary2 = new p_microblog_content[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_microblog_content cls4 = new p_microblog_content();
      cls4.id = br.ReadInt64();
      cls4.player_id = br.ReadInt64();
      cls4.msg = LoadString(br);
      cls4.praise_cnt = br.ReadInt32();
      cls4.comment_cnt = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.is_praised = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    blog_list = ary2;
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_blog_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "blog_list"; } }
  public override string Name { get { return "tos_microblog_blog_list"; } }
  public override int Id { get { return 0xe0003; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.player_id);
    bw.Write(this.last_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_feed_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "feed_list"; } }
  public override string Name { get { return "toc_microblog_feed_list"; } }
  public override int Id { get { return 0xe0004; } }

  public override void Load(BinaryReader br)
  {
    int len1 = br.ReadUInt16();
    p_microblog_content[] ary2 = new p_microblog_content[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_microblog_content cls4 = new p_microblog_content();
      cls4.id = br.ReadInt64();
      cls4.player_id = br.ReadInt64();
      cls4.msg = LoadString(br);
      cls4.praise_cnt = br.ReadInt32();
      cls4.comment_cnt = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.is_praised = br.ReadBoolean();
      ary2[idx3] = cls4;
    }
    list = ary2;
    int len5 = br.ReadUInt16();
    p_microblog_author_info[] ary6 = new p_microblog_author_info[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_microblog_author_info cls8 = new p_microblog_author_info();
      cls8.player_id = br.ReadInt64();
      cls8.name = LoadString(br);
      cls8.level = br.ReadInt32();
      cls8.icon = br.ReadInt32();
      cls8.icon_url = LoadString(br);
      cls8.sex = br.ReadInt32();
      ary6[idx7] = cls8;
    }
    author_list = ary6;
    remain = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_feed_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "feed_list"; } }
  public override string Name { get { return "tos_microblog_feed_list"; } }
  public override int Id { get { return 0xe0004; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.last_id);
    bw.Write(this.refresh);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_comment_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "comment_list"; } }
  public override string Name { get { return "toc_microblog_comment_list"; } }
  public override int Id { get { return 0xe0005; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    int len1 = br.ReadUInt16();
    p_microblog_comment_info[] ary2 = new p_microblog_comment_info[len1];
    for (int idx3 = 0; idx3 < len1; idx3++)
    {
      p_microblog_comment_info cls4 = new p_microblog_comment_info();
      cls4.id = br.ReadInt32();
      cls4.time = br.ReadInt32();
      cls4.pid = br.ReadInt64();
      cls4.comment = LoadString(br);
      ary2[idx3] = cls4;
    }
    comment_list = ary2;
    int len5 = br.ReadUInt16();
    p_microblog_author_info[] ary6 = new p_microblog_author_info[len5];
    for (int idx7 = 0; idx7 < len5; idx7++)
    {
      p_microblog_author_info cls8 = new p_microblog_author_info();
      cls8.player_id = br.ReadInt64();
      cls8.name = LoadString(br);
      cls8.level = br.ReadInt32();
      cls8.icon = br.ReadInt32();
      cls8.icon_url = LoadString(br);
      cls8.sex = br.ReadInt32();
      ary6[idx7] = cls8;
    }
    author_list = ary6;
    praise_cnt = br.ReadInt32();
    comment_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_comment_list
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "comment_list"; } }
  public override string Name { get { return "tos_microblog_comment_list"; } }
  public override int Id { get { return 0xe0005; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    bw.Write(this.last_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_add_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_blog"; } }
  public override string Name { get { return "toc_microblog_add_blog"; } }
  public override int Id { get { return 0xe0006; } }

  public override void Load(BinaryReader br)
  {
    p_microblog_content cls1 = new p_microblog_content();
    cls1.id = br.ReadInt64();
    cls1.player_id = br.ReadInt64();
    cls1.msg = LoadString(br);
    cls1.praise_cnt = br.ReadInt32();
    cls1.comment_cnt = br.ReadInt32();
    cls1.time = br.ReadInt32();
    cls1.is_praised = br.ReadBoolean();
    content = cls1;
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_add_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_blog"; } }
  public override string Name { get { return "tos_microblog_add_blog"; } }
  public override int Id { get { return 0xe0006; } }

  public override void Save(BinaryWriter bw)
  {
    SaveString(bw, this.content);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_del_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_blog"; } }
  public override string Name { get { return "toc_microblog_del_blog"; } }
  public override int Id { get { return 0xe0007; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_del_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_blog"; } }
  public override string Name { get { return "tos_microblog_del_blog"; } }
  public override int Id { get { return 0xe0007; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_praise_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "praise_blog"; } }
  public override string Name { get { return "toc_microblog_praise_blog"; } }
  public override int Id { get { return 0xe0008; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    praise_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_praise_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "praise_blog"; } }
  public override string Name { get { return "tos_microblog_praise_blog"; } }
  public override int Id { get { return 0xe0008; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_cancel_praise
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "cancel_praise"; } }
  public override string Name { get { return "toc_microblog_cancel_praise"; } }
  public override int Id { get { return 0xe0009; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    praise_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_cancel_praise
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "cancel_praise"; } }
  public override string Name { get { return "tos_microblog_cancel_praise"; } }
  public override int Id { get { return 0xe0009; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_comment_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "comment_blog"; } }
  public override string Name { get { return "toc_microblog_comment_blog"; } }
  public override int Id { get { return 0xe000a; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    p_microblog_comment_info cls1 = new p_microblog_comment_info();
    cls1.id = br.ReadInt32();
    cls1.time = br.ReadInt32();
    cls1.pid = br.ReadInt64();
    cls1.comment = LoadString(br);
    comment = cls1;
    praise_cnt = br.ReadInt32();
    comment_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_comment_blog
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "comment_blog"; } }
  public override string Name { get { return "tos_microblog_comment_blog"; } }
  public override int Id { get { return 0xe000a; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    SaveString(bw, this.comment);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_del_comment
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_comment"; } }
  public override string Name { get { return "toc_microblog_del_comment"; } }
  public override int Id { get { return 0xe000b; } }

  public override void Load(BinaryReader br)
  {
    blog_id = br.ReadInt64();
    comment_id = br.ReadInt32();
    praise_cnt = br.ReadInt32();
    comment_cnt = br.ReadInt32();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_del_comment
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_comment"; } }
  public override string Name { get { return "tos_microblog_del_comment"; } }
  public override int Id { get { return 0xe000b; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.blog_id);
    bw.Write(this.comment_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_add_follow
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_follow"; } }
  public override string Name { get { return "toc_microblog_add_follow"; } }
  public override int Id { get { return 0xe000c; } }

  public override void Load(BinaryReader br)
  {
    follow_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_add_follow
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_follow"; } }
  public override string Name { get { return "tos_microblog_add_follow"; } }
  public override int Id { get { return 0xe000c; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.follow_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_del_follow
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_follow"; } }
  public override string Name { get { return "toc_microblog_del_follow"; } }
  public override int Id { get { return 0xe000d; } }

  public override void Load(BinaryReader br)
  {
    follow_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_del_follow
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_follow"; } }
  public override string Name { get { return "tos_microblog_del_follow"; } }
  public override int Id { get { return 0xe000d; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.follow_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_add_black
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_black"; } }
  public override string Name { get { return "toc_microblog_add_black"; } }
  public override int Id { get { return 0xe000e; } }

  public override void Load(BinaryReader br)
  {
    black_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_add_black
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "add_black"; } }
  public override string Name { get { return "tos_microblog_add_black"; } }
  public override int Id { get { return 0xe000e; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.black_id);
    bw.Write(this.sid);
  }
}

partial class toc_microblog_del_black
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_black"; } }
  public override string Name { get { return "toc_microblog_del_black"; } }
  public override int Id { get { return 0xe000f; } }

  public override void Load(BinaryReader br)
  {
    black_id = br.ReadInt64();
    sid = br.ReadUInt32();
  }
}

partial class tos_microblog_del_black
{
  public override string Typ { get { return "microblog"; } }
  public override string Key { get { return "del_black"; } }
  public override string Name { get { return "tos_microblog_del_black"; } }
  public override int Id { get { return 0xe000f; } }

  public override void Save(BinaryWriter bw)
  {
    bw.Write(this.black_id);
    bw.Write(this.sid);
  }
}


