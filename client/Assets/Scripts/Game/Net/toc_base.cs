﻿using System;
using System.IO;
using System.Text;

[AttributeUsage(AttributeTargets.Field, AllowMultiple=false)]
class DefaultValueAttribute : Attribute
{
    string mValue;

    public DefaultValueAttribute(object v, bool luacode = false)
    {
        if (!luacode && v is string)
            mValue = @"""" + v + @"""";
        else
            mValue = v.ToString();
    }

    public string Value { get { return mValue; } }
}

abstract class toc_base
{
    public uint sid = 0;

    public virtual void Load(BinaryReader br)
    {
        throw new NotImplementedException();
    }

    protected static string LoadString(BinaryReader br)
    {
        int len = br.ReadUInt16();
        byte[] buf = new byte[len];
        br.Read(buf, 0, len);
        return Encoding.UTF8.GetString(buf);
    }

    public virtual string Typ { get { return null; } }
    public virtual string Key { get { return null; } }
    public virtual string Name { get { return null; } }
    public virtual int Id { get { return 0; } }
}

