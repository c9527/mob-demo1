﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using UnityEngine;

static partial class NetLayer
{
    class MethodInfoObj
    {
        public MethodInfo methodInfo;
        public object obj;
    }

    public enum ConnectErrorType
    {
        None,
        SocketConnectFail,      //Socket连接失败
        SocketTimeout,          //Socket连接超时
        ServerRefuse,           //服务器主动拒绝
        ReissueFail,            //补发失败
        ResponsePacketTimeout,  //响应包超时
        ResponsePacketEmpty,    //响应包为空
        PingTimeError,          //Ping时间错误
        StartupTimeTooLong,     //启动时间超长(30天)
        SendQueueOverflow,      //发送队列溢出
        PingTimeout,            //Ping超时
        ConnectShutdown,        //连接被断开
        SendError               //发送数据出错
    }

    const int MAX_SEND_QUEUE = 100;

    public static bool SHOW_PROTO = false;

    static Socket msUdp = null;
//    static IPEndPoint msLastUdpAddr = null;
    static string msLastUdpHost;
    static int msLastUdpPort;
    static byte[][] msUdpQueue = new byte[100][];
    static int msUdpReadIndex = 0;
    static int msUdpRecvIndex = 0;
    static int msUdpReadOff = 0;
    static int msUdpReadSize = 0;

    static int msSession = 0;
    static CNetPacker msPacker = null;
    static ConnectErrorType msLastConnectErrorType; //最近一次的连接错误类型
    static bool msEnableForwarding = false;     //是否启用中转连接
    static Queue<byte[]> msSendQueue = new Queue<byte[]>();
    static byte[] msRecvBuf = new byte[0x10000];
    static int msSendId = 0;
    static int msRecvId = 0;
    static int msErrorId = 0;
    static string msServerHost;
    static int msServerPort;
    static int msConnectId = -1;
    static string msConnectToken;

    static uint msSessionId = 0;

    static bool msPingSend = false;
    static float msLastPingTime = 0;
    static float msPingSendTime = 0;
    static float msLastClientTime = 0;
    static float msLastServerTime = 0;

    static Ping msPingOut = null;
    static Ping msPingServer = null;
    static int msPingOutTime = 0;
    static int msPingServerTime = 0;

    static float msReconectTime = 0;

    static float msTimeOutTime = 60;

    static Dictionary<string, List<MethodInfoObj>> msProtoHandler = new Dictionary<string, List<MethodInfoObj>>();

    static Dictionary<int, Func<toc_base>> mCreater = new Dictionary<int, Func<toc_base>>();

    static List<ErrorMsgCallBack> mErrorMsgCallBackList = new List<ErrorMsgCallBack>();

    class ErrorMsgCallBack
    {
        public string msgName;
        public uint sid;
        public Action<toc_player_err_msg> action;
    }

    class NetCount
    {
        public int times;
        public long packSize;
    }

    static Dictionary<string, NetCount> mProtoCount = new Dictionary<string, NetCount>();

    static void SetProtoCount(string name, long size)
    {
        if (mProtoCount.ContainsKey(name))
        {
            NetCount netCount = mProtoCount[name];
            netCount.times = netCount.times + 1;
            netCount.packSize = netCount.packSize + size;
            mProtoCount[name] = netCount;
        }
        else
        {
            NetCount netCount = new NetCount();
            netCount.times = 1;
            netCount.packSize = size;
            mProtoCount[name] = netCount;
        }
    }

    public static void CleanProtoCount()
    {
        mProtoCount.Clear();
    }

    public static void PortoCount2File()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("name\tcount\tsize");

        foreach (string key in mProtoCount.Keys)
        {
            NetCount netCount = mProtoCount[key];
            sb.AppendFormat("{0}\t{1}\t{2}\r\n", key, netCount.times, netCount.packSize);
        }

        try
        {
#if !UNITY_WEBPLAYER
            File.WriteAllText(Application.persistentDataPath + "/ProtoCount.txt", sb.ToString());
#endif
        }
        catch (IOException)
        {

        }
    }

    static float GetCurTime()
    {
        DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
        DateTime nowTime = DateTime.Now;
        return (float)Math.Round((nowTime - startTime).TotalMilliseconds, MidpointRounding.AwayFromZero);
    }

    static bool Init()
    {
        for (int i = 0; i < msUdpQueue.Length; i++)
            msUdpQueue[i] = new byte[1024];
        return true;
    }

    static CDict WaitConnectPack()
    {
        if (!msPacker.WaitRead(3000))
        {
            WriteLine("响应包超时！");
            msLastConnectErrorType = ConnectErrorType.ResponsePacketTimeout;
            return null;
        }
        int len;
        if (!msPacker.TryRecvPack(out len))
        {
            WriteLine("未收到响应包！");
            msLastConnectErrorType = ConnectErrorType.ResponsePacketEmpty;
            return null;
        }
        string json = Encoding.UTF8.GetString(msPacker.RecvBuf, 0, len);
        WriteLine("响应包：{0}", json);
        CDict proto = CJsonHelper.Load(json) as CDict;
        if (!proto.Bool("ok"))
        {
            WriteLine("响应包错误：{0}", proto.Str("msg"));
            msLastConnectErrorType = ConnectErrorType.ServerRefuse;
        }
        return proto;
    }

    static bool SendConnectPack(CDict proto)
    {
        string json = CJsonHelper.Save(proto);
        WriteLine("SendConnectPack:\t{0}", json);
        byte[] buf = Encoding.UTF8.GetBytes(json);
        return msPacker.Send(buf);
    }

    static Socket CreateSocket(string host, int port, bool tcp)
    {
        if (msEnableForwarding)
        {
            Driver.SendLogServer(1201, "启用BGP转发[{0}:{1}]", msServerHost, msServerPort);
            var cfg = GlobalConfig.addressMapping.GetLine(host + ":" + port);
            if (cfg != null)
            {
                var arr = cfg.DestAddress.Split(':');
                host = arr[0];
                if (arr.Length > 1)
                    port = int.Parse(arr[1]);
            }
        }

        IPAddress ip;
        if (!IPAddress.TryParse(host, out ip))
        {
            try
            {
                ip = Dns.GetHostEntry(host).AddressList[0];
            }
            catch (Exception e)
            {
                if (msEnableForwarding)
                    Driver.SendLogServer(1202, "BGP域名解析失败：{0}  error:{1}", host, e.Message);
                return null;
            }
        }

        Socket sk;
		if (tcp)
			sk = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
		else
			sk = new Socket(ip.AddressFamily, SocketType.Dgram, ProtocolType.Unspecified);

        try
        {
            sk.Connect(ip, port);
            return sk;
        }
        catch (Exception e)
        {
            if (msEnableForwarding)
                Driver.SendLogServer(1203, "BGP连接失败：{0}:{1}  error:{2}", ip, port, e.Message);
        }
        sk.Close();
        return null;
    }

    static bool DoConnect()
    {
        Close();

        float time = Time.realtimeSinceStartup;

        Socket sk = null;
        var t = new Thread(() => sk = CreateSocket(msServerHost, msServerPort, true));
        t.Start();

        if (!t.Join(3000))
        {
            WriteLine("连接超时1：{0}", msServerHost);
            msLastConnectErrorType = ConnectErrorType.SocketTimeout;
            return false;
        }

        if (sk == null)
        {
            WriteLine("建立socket失败：{0}", msServerHost);
            msLastConnectErrorType = ConnectErrorType.SocketConnectFail;
            return false;
        }

        while (!sk.Connected)
        {
            if (Time.realtimeSinceStartup > time + 3)
            {
				try
				{
					WriteLine("连接超时2：{0}", sk.RemoteEndPoint);
				}
				catch(Exception e)
				{
				}
                sk.Close();
                msLastConnectErrorType = ConnectErrorType.SocketTimeout;
                return false;
            }
            Thread.Sleep(1);
        }

        sk.NoDelay = true;
        msPacker = new CNetPacker(sk);
        WriteLine("tcp conneted:{0}", sk.Connected);
		try
		{
			WriteLine("成功建立socket：{0}", sk.RemoteEndPoint);
		}
		catch(Exception e)
		{
		}

        msLastClientTime = Time.realtimeSinceStartup;
        msLastServerTime = 0;
        //float tempNow = GetCurTime();
        msPingSend = false;
        msLastConnectErrorType = ConnectErrorType.None;
        return true;
    }

    public static int Connect(string host, int port)
    {
        msSendId = 0;
        msRecvId = 0;
        msErrorId = 0;
        ClearConnectId();

        msServerHost = host;
        msServerPort = port;

        if (!DoConnect())
            return 2;  //错误码2

        SendConnectPack(new CDict());

        CDict proto = WaitConnectPack();
        if (proto == null)
            return 1;  //错误码1
        if (msLastConnectErrorType == ConnectErrorType.ServerRefuse)
            return 5;   //响应包错误

        msConnectId = proto.Int("id");
        msConnectToken = proto.Str("token");

        return 0;
    }

    public static bool Reconnect()
    {
        if (!CanReconnect)
        {
            WriteLine("连接已废弃，不可重连");
            return false;
        }

        if (!DoConnect())
        {
            WriteLine("重连无法连接目标网络，重试。。。");
            if (msLastConnectErrorType == ConnectErrorType.SocketConnectFail)
                Driver.SendLogServer(1301, "创建Socket失败 Host:{0} Port:{1}", msServerHost, msServerPort);
            else if (msLastConnectErrorType == ConnectErrorType.SocketTimeout)
                Driver.SendLogServer(1301, "连接Socket超时 Host:{0} Port:{1}", msServerHost, msServerPort);
            return false;
        }

        byte[][] ary = msSendQueue.ToArray();
        int lostId = msSendId - ary.Length;  // 小于等于此id的包无法补发

        CDict proto = new CDict();
        proto["sid"] = lostId;
        proto["rid"] = msRecvId;
        proto["id"] = msConnectId;
        proto["token"] = msConnectToken;

        SendConnectPack(proto);

        proto = WaitConnectPack();
        if (proto == null)
        {
            WriteLine("重连无法收取数据，重试。。。");
            Close();
            Driver.SendLogServer(1302, "响应包超时或未收到 Host:{0} Port:{1}", msServerHost, msServerPort);
            return false;
        }

        if (!proto.Bool("ok"))
        {
            msConnectId = -1;
            Close();
            WriteLine("连接废弃，需要重登");
            msLastConnectErrorType = ConnectErrorType.ServerRefuse;
            Driver.SendLogServer(1302, "响应包被拒绝 Host:{0} Port:{1} Msg:{2}", msServerHost, msServerPort, proto.Str("msg"));
            return false;
        }

        for (int i = proto.Int("rid") - lostId; i < ary.Length; i++)
        {
            if (!msPacker.Send(ary[i]))
            {
                WriteLine("补发失败 {0}/{1}", i, ary.Length);
                Close();
                msLastConnectErrorType = ConnectErrorType.ReissueFail;
                Driver.SendLogServer(1302, string.Format("协议补发失败 Host:{0} Port:{1} Length:{2}/{3}", msServerHost, msServerPort, i, ary.Length));
                return false;
            }
        }

        msLastServerTime = 0;
        msPingSend = false;
        if (WorldManager.singleton.fightEntered)
        {
            ReconnectUDP();
        }
        WriteLine("重连成功");
        msLastConnectErrorType = ConnectErrorType.None;
        return true;
    }

    public static void ReconnectUDP()
    {
        if (msLastUdpHost != null)
        {
            ConnectUDP(msLastUdpHost, msLastUdpPort);
            WorldManager.SendUdp(new tos_fight_spectate() { player_id = PlayerSystem.roleId });
        }
    }

    public static void Close()
    {
        if (msPacker != null)
        {
            msPacker.Close();
            msPacker = null;
        }
    }

    public static void CloseUdp()
    {
        if (msUdp != null)
        {
            Socket sk = msUdp;
            msUdp = null;
            sk.Close();
        }
        msLastUdpHost = null;
        msLastUdpPort = 0;
    }

    /// <summary>
    /// 清除后就不会重连了
    /// </summary>
    public static void ClearConnectId()
    {
        msConnectId = -1;
        msConnectToken = null;
        msSendQueue.Clear();
    }

    public static void ConnectUDP(string host, int port)
    {
        if (string.IsNullOrEmpty(host))
            host = msServerHost;

        CloseUdp();
        msLastUdpHost = host;
        msLastUdpPort = port;
        Socket udp = CreateSocket(host, port, false);
        if (udp == null)
            return;
        msUdp = udp;
        msUdpReadIndex = msUdpRecvIndex = 0;
        msUdpReadOff = 0;
        Thread th = new Thread(UdpReciver);
        th.IsBackground = true;
        th.Start(udp);
    }

    static bool TryRecv(out int len)
    {
        if (!Connected)
        {
            msUdpReadIndex = msUdpRecvIndex;
            msUdpReadOff = 0;
            len = 0;
            return false;
        }
        if (msUdpReadIndex != msUdpRecvIndex)
        {
            byte[] buf = msUdpQueue[msUdpReadIndex];
            if (msUdpReadOff <= 0)
            {
                msUdpReadSize = buf[0] + (buf[1] << 8) + 2;
                msUdpReadOff = 2;
            }
            len = buf[msUdpReadOff] + (buf[msUdpReadOff + 1] << 8) - 4;
            if (msUdpReadOff + len > msUdpReadSize)
            {
                Logger.Error(string.Format("udp part error len: {0} + {1} > {2}", msUdpReadOff, len, msUdpReadSize));
                msUdpReadOff = msUdpReadSize;
                len = 0;
            }
            else
            {
                uint id = buf[msUdpReadOff + 2] + ((uint)buf[msUdpReadOff + 3] << 8) + ((uint)buf[msUdpReadOff + 4] << 16) + ((uint)buf[msUdpReadOff + 5] << 24);
                msUdpReadOff += 6;
                MaskBuf(buf, msRecvBuf, id, len, msUdpReadOff);
                msUdpReadOff += len;
            }
            if (msUdpReadOff >= msUdpReadSize)
            {
                int index = msUdpReadIndex + 1; // 先用临时变量，一次性赋给msUdpReadIndex，避免多线程冲突
                if (index >= msUdpQueue.Length)
                    index = 0;
                msUdpReadIndex = index;
                msUdpReadOff = 0;
            }
        }
        else if (msPacker != null && msPacker.TryRecvPack(out len))
        {
            MaskBuf(msPacker.RecvBuf, msRecvBuf, (uint)++msRecvId, len);
        }
        else
        {
            len = 0;
            return false;
        }

        uint crc = msRecvBuf[0] + ((uint)msRecvBuf[1] << 8) + ((uint)msRecvBuf[2] << 16) + ((uint)msRecvBuf[3] << 24);
        uint _crc = Adler32(msRecvBuf, 4, len);
        if (crc == _crc)
            return true;

        Debug.LogWarning(string.Format("recv crc error: {0:x}!={1:x}", crc, _crc));
        return false;
    }

    static void UdpReciver(object obj)
    {
        if( FightVideoManager.isPlayFight == true )
        {
            return;
        }
        Socket udp = obj as Socket;
        DateTime nextLogTime = DateTime.MinValue;
        while (msUdp == udp)
        {
            int index = msUdpRecvIndex + 1;
            if (index >= msUdpQueue.Length)
                index = 0;
            if (index == msUdpReadIndex)
            {
                DateTime now = DateTime.Now;
                if (now > nextLogTime)
                {
                    Debug.LogWarning("udp overflow!");
                    nextLogTime = now.AddSeconds(10);
                }
                Thread.Sleep(1);
                continue;
            }

            byte[] buf = msUdpQueue[msUdpRecvIndex];
            int len;
            try { len = udp.Receive(buf, 2, buf.Length - 2, SocketFlags.None); }
            catch (Exception e)
            {
                if (msUdp != udp)
                    break;
                Debug.LogError(e.Message);
                continue;
            }
            if (len < 12 || len > 1024)
            {
                Debug.LogError("udp recv len error: " + len);
                continue;
            }
            buf[0] = (byte)len;
            buf[1] = (byte)(len >> 8);
            msUdpRecvIndex = index;
        }
        Debug.Log("udp recv exit.");
    }

    public static bool Update()
    {
        int len;
        while (TryRecv(out len))
        {
            string name = string.Empty;
            object proto = null;
            if (msRecvBuf[4] == '{' && msRecvBuf[5] == '\"')
            {
                string json = Encoding.UTF8.GetString(msRecvBuf, 4, len - 4);
                proto = CJsonHelper.Load(json);
                CDict dic = proto as CDict;

                name = string.Format("{0}{1}{2}{3}", "toc_", dic.Str("typ"), "_", dic.Str("key"));
                if (SHOW_PROTO)
                    WriteLine("\tRecv[{0}_{1}]:\t{2}\n", dic.Str("typ"), dic.Str("key"), json);
            }
            else
            {
                using (BinaryReader br = new BinaryReader(new MemoryStream(msRecvBuf)))
                {
                    br.BaseStream.Position = 4;
                    int id = br.ReadInt32();
                    toc_base ptc = mCreater[id]();
                    name = ptc.Name;
                    proto = ptc;
                    if (SHOW_PROTO)
                        WriteLine("\tRecv[{0}]...\n", name);
                    try
                    {
                        ptc.Load(br);
                    }
                    catch (Exception exp)
                    {
                        Debug.LogError("Proto Error, " + name + " : " + exp.Message.ToString());
                        continue;
                    }
                    if (LuaHook.CheckHook(HookType.Net_TcpRecv, 0, ptc))
                        proto = null;
                }
            }
            // 统计协议接受次数和大小
            SetProtoCount(name, len);
            if (proto != null)
                HandleProto(name, proto);
        }

        float now = Time.realtimeSinceStartup;
        if (now < 0 || now > 3600 * 24 * 30)
        {
            Debug.LogError("time error! " + now);
            Close();
            ClearConnectId();
            msLastConnectErrorType = ConnectErrorType.StartupTimeTooLong;
            Driver.SendLogServer(1308, "time error! " + now);
            return false;
        }

        if (msPacker != null)
        {
            if (msPacker.Connected)
            {
                if (!msPingSend)
                {
                    if (now >= msLastClientTime + 10)
                    {
                        CDict info = new CDict();
                        info["fps"] = Driver.LastFps;
                        info["delay"] = Driver.MaxDelayTime;
                        info["tcp_ping"] = PingTime;
                        info["server_ping"] = PingServerTime;
                        info["out_ping"] = PingOutTime;
                        bool fight = UIManager.IsBattle();
                        info["fight"] = fight;
                        if (fight && UDPping.Instance != null)
                            info["udp_ping"] = UDPping.Instance.ping * 1000;
                        info["mem"] = Profiler.GetTotalAllocatedMemory();
                        Send(new tos_login_ping() { client_time = now, info = info });
                        Driver.MaxDelayTime = 0;
                        msPingSendTime = now;
                        msPingSend = true;
                        msPingOut = new Ping("qq.com");
                        msPingServer = new Ping(msServerHost);
                    }
                    return true;
                }

                if (now < msLastClientTime + msTimeOutTime)
                    return true;

                WriteLine("ping超时！");
                msLastConnectErrorType = ConnectErrorType.PingTimeout;
                Driver.SendLogServer(1310, msTimeOutTime+"s");
            }
            else
            {
                WriteLine("连接断开！");
                msLastConnectErrorType = ConnectErrorType.ConnectShutdown;
                Driver.SendLogServer(1311);
            }
            msPacker.Close();
            msPacker = null;
        }

        //交由外部决定重连时机
//        if (ConnectToServer.LoginVerifySuccess && now > msReconectTime + 3)
//        {
//            msReconectTime = now;
//            if (Reconnect())
//                return true;
//        }

        return false;
    }

    public static void SendPack(CDict proto)
    {
        proto["sid"] = ++msSession;
        string json = CJsonHelper.Save(proto);
        string name = "toc_" + proto.Str("typ") + "_" + proto.Str("key");
        SendTcp(name, json);
    }

    public static void SendMsg(string typ, string key, object value = null)
    {
        CDict proto = new CDict();
        proto["typ"] = typ;
        proto["key"] = key;
        if (value != null)
            proto["val"] = value;
        SendPack(proto);
    }

    public static void Send(tos_base proto)
    {
        proto.sid = ++msSessionId;
        SendTcp(proto.Name, proto);
    }

    public static void Send(tos_base proto, Action<toc_player_err_msg> uniqueCallBack)
    {
        proto.sid = ++msSessionId;
        SendTcp(proto.Name, proto);
        if (uniqueCallBack != null)
        {
            var msgName = proto.Name;
            var has = false;
            for (int i = 0; i < mErrorMsgCallBackList.Count; i++)
            {
                if (mErrorMsgCallBackList[i].msgName == msgName)
                {
                    mErrorMsgCallBackList[i].sid = proto.sid;
                    mErrorMsgCallBackList[i].action = uniqueCallBack;
                    has = true;
                    break;
                }
            }
            if (!has)
            {
                mErrorMsgCallBackList.Add(new ErrorMsgCallBack() { msgName = proto.Name, sid = proto.sid, action = uniqueCallBack });
            }
        }
    }

    public static void TryLaunchErrorCallBack(toc_player_err_msg proto)
    {
        for (int i = 0; i < mErrorMsgCallBackList.Count; i++)
        {
            if (mErrorMsgCallBackList[i].sid == proto.sid)
            {
                var action = mErrorMsgCallBackList[i].action;
                mErrorMsgCallBackList[i].action = null;
                if (action != null)
                    action(proto);
            }
        }
    }

    static void MaskBuf(byte[] buf1, byte[] buf2, uint id, int size, int off1 = 0, int off2 = 0)
    {
        uint msk = (id * 0xabcd4399u) ^ 0x12345678u;
        for (int i = 0; i < size; i++)
            buf2[i + off2] = (byte)(buf1[i + off1] ^ (msk >> (8 * (i & 0x3))));
    }

    static uint Adler32(byte[] buf, int offset = 0, int end = -1)
    {
        uint s1 = 1, s2 = 0;
        if (end < 0)
            end = buf.Length;
        for (int i = offset; i < end; i++)
        {
            s1 = (s1 + (uint)buf[i]) % 65521;
            s2 = (s2 + s1) % 65521;
        }
        return (s2 << 16) + s1;
    }

    static void SendTcp(string name, object proto)
    {
        if (LuaHook.CheckHook(HookType.Net_TcpSend, 0, proto))
            return;
        if (SHOW_PROTO)
            WriteLine("Send[{0}]:\t{1}\n", name, proto.ToString());
        byte[] buf;
        using (MemoryStream mem = new MemoryStream())
        using (BinaryWriter bw = new BinaryWriter(mem))
        {
            mem.Position += 4;  // 校验占位
            if (proto is tos_base)
            {
                tos_base tos = proto as tos_base;
                bw.Write(tos.Id);   // 协议ID
                tos.Save(bw);   // 协议内容
            }
            else
            {
                bw.Write(Encoding.UTF8.GetBytes(proto as string));
            }
            bw.Flush();
            uint crc = Adler32(mem.GetBuffer(), 4, (int)mem.Length);
            mem.Position = 0;
            bw.Write(crc);  // 校验
            bw.Flush();

            // 统计协议次数和大小
            SetProtoCount(name, mem.Length);

            buf = mem.ToArray();
        }

        MaskBuf(buf, buf, (uint)++msSendId, buf.Length);
        msSendQueue.Enqueue(buf);
        while (msSendQueue.Count > MAX_SEND_QUEUE)
            msSendQueue.Dequeue();

        if (!Connected)
        {
            if (msErrorId <= 0)
                msErrorId = msSendId;
            if (msSendId >= msErrorId + MAX_SEND_QUEUE)
            {
                ClearConnectId();
                WriteLine("连接废弃，需要重登");
                msLastConnectErrorType = ConnectErrorType.SendQueueOverflow;
                Driver.SendLogServer(1309, "sendId:{0} errorId:{1}", msSendId, msErrorId);
            }
            else
            {
                WriteLine("连接异常，需要重连");
            }
        }
        else if (!msPacker.Send(buf))
        {
            msErrorId = msSendId;
            Close();
            msLastConnectErrorType = ConnectErrorType.SendError;
            Driver.SendLogServer(1312);
        }
    }

    public static bool SendUdp(uint rhd, uint rid, tos_base proto)
    {
        if (msUdp == null)
        {
            Debug.LogError("SendUdp err, udp already closed.");
            return false;
        }

        if (!Connected)
            return false;

        proto.sid = ++msSessionId;

        using (MemoryStream mem = new MemoryStream())
        using (BinaryWriter bw = new BinaryWriter(mem))
        {
            bw.Write(proto.sid);
            mem.Position += 4;  // 校验占位
            bw.Write(rhd);
            bw.Write(rid);
            bw.Write(proto.Id); // 协议ID
            proto.Save(bw); // 协议内容
            bw.Flush();
            int len = (int)mem.Length;
            byte[] buf = mem.GetBuffer();
            uint crc = Adler32(buf, 8, len);
            mem.Position = 4;
            bw.Write(crc);  // 校验
            bw.Flush();

            // 统计协议次数和大小
            SetProtoCount(proto.Name, len);

            MaskBuf(buf, buf, proto.sid ^ (uint)len, len - 4, 4, 4);
            return msUdp.Send(buf, len, SocketFlags.None) == len;
        }
    }

    static bool HandleProto(string name, object proto)
    {
        List<MethodInfoObj> list;
        if (!msProtoHandler.TryGetValue(name, out list))
            return false;
        for (int i = 0; i < list.Count; i++)
        {
            MethodInfo mi = list[i].methodInfo;
            try
            { mi.Invoke(list[i].obj, new object[] { proto }); }
            catch (TargetInvocationException ex)
            { throw ex.InnerException; }
        }

        return true;
    }

    /// <summary>
    /// 注册所有类中的静态私有响应函数
    /// </summary>
    public static void AddAllHandler()
    {
        var typeArr = Assembly.GetCallingAssembly().GetTypes();
        for (int i = 0; i < typeArr.Length; i++)
        {
            AddStaticHandler(typeArr[i]);
        }
    }

    private static void AddStaticHandler(Type type)
    {
        AddHandler(type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic), null);
    }

    /// <summary>
    /// 注册实例对象上的消息响应函数
    /// </summary>
    /// <param name="obj"></param>
    public static void AddHandler(object obj)
    {
        AddHandler(obj.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic), obj);
    }

    private static void AddHandler(MethodInfo[] methodInfos, object obj)
    {
        for (int index = 0; index < methodInfos.Length; index++)
        {
            var mi = methodInfos[index];
            if (!mi.Name.StartsWith("Toc_"))
                continue;
            var isObsolete = false;
            var attrArr = mi.GetCustomAttributes(typeof(ObsoleteAttribute), false);
            for (int i = 0; i < attrArr.Length; i++)
            {
                var attr = attrArr[i] as ObsoleteAttribute;
                if (attr != null)
                {
                    isObsolete = true;
                    break;
                }
            }
            if (!isObsolete)
            {
                var key = mi.Name.ToLower();
                if (!msProtoHandler.ContainsKey(key))
                    msProtoHandler[key] = new List<MethodInfoObj>();
                var miList = msProtoHandler[key];
                var isHas = false;
                for (int i = 0; i < miList.Count; i++)
                {
                    if (miList[i].obj == obj && miList[i].methodInfo == mi)
                    {
                        isHas = true;
                        break;
                    }
                }
                if (!isHas)
                    msProtoHandler[key].Add(new MethodInfoObj { obj = obj, methodInfo = mi });
            }
        }
    }

    /// <summary>
    /// 移除对象实例响应函数的注册
    /// </summary>
    /// <param name="obj"></param>
    public static void RemoveHandler(object obj)
    {
        foreach (var mi in obj.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic))
        {
            var key = mi.Name.ToLower();
            if (!msProtoHandler.ContainsKey(key))
                continue;
            for (int i = msProtoHandler[key].Count - 1; i >= 0; i--)
            {
                if (msProtoHandler[key][i].obj == obj)
                    msProtoHandler[key].RemoveAt(i);
            }
        }
    }

    static void WriteLine(string msg, params object[] args)
    {
        if (args.Length > 0)
            msg = string.Format(msg, args);

        if (msg.IndexOf("login_ping", System.StringComparison.Ordinal) == -1)
            Debug.LogWarning("[net]" + msg);
    }

    static void Toc_login_ping(toc_login_ping proto)
    {
        float now = Time.realtimeSinceStartup;

        if (msLastServerTime != 0 && proto.server_time != 0)
        {
            float mdt = now - proto.client_time + msLastPingTime;
            float dt = (now - msLastClientTime) - (proto.server_time - msLastServerTime);
            if (dt > mdt + .1f || dt < -.1f)
            {
                Debug.LogError("ping time error! " + dt);
                Close();
                ClearConnectId();
                msLastConnectErrorType = ConnectErrorType.PingTimeError;
                Driver.SendLogServer(1307, "ping time error! " + dt);
            }
        }

        msPingSend = false;
        msLastPingTime = now - proto.client_time;
        msLastClientTime = proto.client_time;
        msLastServerTime = proto.server_time;
    }

    static int _getPing(Ping ping, ref int time)
    {
        if (ping == null)
            return time;
        if (ping.isDone)
            return time = ping.time;
        return time = Math.Max(time, (int)((Time.realtimeSinceStartup - msPingSendTime) * 1000));
    }

    public static bool Connected { get { return msPacker != null && msPacker.Connected; } }

    public static bool UDPConnected { get { return msUdp != null; } }
    public static bool CanReconnect { get { return msConnectId >= 0; } }
    public static uint SessionId { get { return msSessionId; } }
    public static int PingTime
    {
        get
        {
            if (!msPingSend)
                return (int)(msLastPingTime * 1000);
            return (int)(Mathf.Max(msLastPingTime, Time.realtimeSinceStartup - msPingSendTime) * 1000);
        }
    }
    public static int PingOutTime { get { return _getPing(msPingOut, ref msPingOutTime); } }
    public static int PingServerTime { get { return _getPing(msPingServer, ref msPingServerTime); } }
    public static float TimeOutTime { set { msTimeOutTime = value; } }

    public static bool EnableForwarding
    {
        set
        {
            msEnableForwarding = value;
        }
        get
        {
            return msEnableForwarding;
        }
    }

    public static ConnectErrorType LastConnectErrorType { get { return msLastConnectErrorType; } }
}
