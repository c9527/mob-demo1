﻿using System;
using System.Net.Sockets;

class CNetPacker
{
    const int PackHeadSize = 2;

    protected byte[] m_recvBuf = new byte[1 << (PackHeadSize * 8)];
    protected int m_bufSize = PackHeadSize;
    protected int m_recvLen = 0;
    protected bool m_recvHead = true;
    protected Socket m_sk;

    public bool Connected { get { return m_sk.Connected; } }
    public byte[] RecvBuf { get { return m_recvBuf; } }

    public CNetPacker(Socket socket)
    {
        m_sk = socket;
    }

    public void Close()
    {
        try
        {
            if (m_sk.Connected)
                m_sk.Shutdown(SocketShutdown.Both);
        }
        catch { }
        m_sk.Close();
    }

    public bool TryRecvPack(out int len)
    {
        len = 0;

        if (m_recvHead)
        {
            if (!_RecvBuf())
                return false;

            int size = 0;
            for (int i = 0; i < PackHeadSize; i++)
                size = (size << 8) + m_recvBuf[i];
            m_bufSize = size;
            m_recvLen = 0;
            m_recvHead = false;
        }

        if (!_RecvBuf())
            return false;

        len = m_bufSize;
        m_bufSize = PackHeadSize;
        m_recvLen = 0;
        m_recvHead = true;
        return true;
    }

    public bool Send(byte[] buf)
    {
        if (!m_sk.Connected)
            return false;

        int l = buf.Length;
        byte[] sendBuf = new byte[PackHeadSize + buf.Length];
        for (int i = PackHeadSize - 1; i >= 0; i--)
        {
            sendBuf[i] = (byte)l;
            l >>= 8;
        }
        buf.CopyTo(sendBuf, PackHeadSize);

        try
        {
            m_sk.Send(sendBuf);
        }
        catch (SocketException)
        {
            return false;
        }

        return true;
    }

    protected bool _RecvBuf()
    {
        int restLen = m_bufSize - m_recvLen;
        if (restLen <= 0)
            return true;

        if (!m_sk.Connected)
            return false;

        if (!m_sk.Poll(0, SelectMode.SelectRead))
            return false;

        int len = m_sk.Available;
        if (len <= 0)   // 收到断开请求
        {
            m_sk.Close();
            return false;
        }

        m_recvLen += m_sk.Receive(m_recvBuf, m_recvLen, Math.Min(len, restLen), SocketFlags.None);

        return m_recvLen >= m_bufSize;
    }

    public bool WaitRead(int ms)
    {
        return m_sk.Poll(ms * 1000, SelectMode.SelectRead);
    }
}
