﻿using System.Collections.Generic;
using System;
using UnityEngine;

#pragma warning disable 0219,0649

#region 登录相关

[Obsolete("旧登录协议")]
partial class tos_login_verify_old : tos_base
{
}

[Obsolete("旧登录协议")]
partial class toc_login_verify_old : toc_base
{
}

partial class tos_login_create : tos_base
{
    public string name;
    public int sex;         /// 0:男，1:女
}

partial class toc_login_create : toc_base
{
    public long id;
    public string name;
}

partial class tos_login_select : tos_base
{
    public long id;
    public int reserve; // 预留字段
}

public class p_recharge_associator_info
{
    public int associator_type;         //> 1:普通会员,2:搞基会员
    public int associator_guarantee;    //> 会员到期时间
}

public class RoleData
{
    public string name;
    public int level;
    public int icon;
    public int exp;
    public int honor;
    public int vip_level;
    public int sex;
    public int diamond;
    public int coin;
    public int medal;
    public int cash;
    public int power;
    public int stage;
    public int double_reward_times;  // 每天领取双倍经验奖励的次数
    public int first_pay_time;
    public int corps_coin;
    public int chenghao;            //> 称号id
    public int blue_stone;
    public int purple_stone;
    public int white_stone;
    public int eventcoin_002;       //兑换币
    [DefaultValue(0, false)]
	public int active_coin;
    [DefaultValue(0, false)]
    public int last_logout_time;	// 上次下线时间
    [DefaultValue(1000, false)]
    public int honor_joke;          // 娱乐排位积分
    public int create_time;         // 创角时间戳
}

partial class toc_login_select : toc_base
{
    public long id;
    public RoleData data;
    public int times;   // 服务器时间
    public int reserve; // 预留字段
}

partial class tos_login_ping : tos_base
{
    public float client_time;
    public CDict info;
}

partial class toc_login_ping : toc_base
{
    public float client_time;
    public float server_time;
}

partial class tos_login_verify : tos_base
{
    public string sdkplat;  // SDK接入平台 none fn ios
    public int platform;    // 具体平台ID 4399为1, ...
    public string ext_info; // 验证的json串
    public string plat_uid; // 平台唯一ID，多个平台可能重复
    public string account;  // 平台帐号
    public CDict other_info;    // 包括设备信息在内的其它信息
}

partial class toc_login_verify : toc_base
{
    public class RoleInfo
    {
        public long id;
        public string name;
        public int level;
    }

    public string uid;
    public RoleInfo[] list;
    [DefaultValue("", false)]
    public string oauth_info;
}

partial class tos_login_random_name : tos_base
{
    public int sex; // 0:男，1:女
}

partial class toc_login_random_name : toc_base
{
    public string name;
}

#endregion


#region player相关

public class p_hero_card_info
{
    public int hero_type;   // 1：普通英雄卡  2：超级英雄卡 3：传奇英雄卡
    public int end_time;    // 对用hero_type的截止时间戳
}

partial class toc_player_exe_lua : toc_base
{
    public int id;
    public string code;
}
partial class tos_player_exe_lua : tos_base
{
    public int id;
    public bool ok;
    public string msg;
}

public class p_title_info
{
    public string title;
    public int cnt;
    public int type;
}


partial class toc_player_view_player : toc_base
{
    public class GameStatdata
    {
        [DefaultValue("", false)]
        public string game_type;
        [DefaultValue(0, false)]
        public int kill_cnt;
        [DefaultValue(0, false)]
        public int death_cnt;
        [DefaultValue(0, false)]
        public int assist_cnt;
        [DefaultValue(0, false)]
        public int win_cnt;
        [DefaultValue(0, false)]
        public int fight_cnt;
    }

    public class FightStatdata
    {
        [DefaultValue(0, false)]
        public int kill_cnt;
        [DefaultValue(0, false)]
        public int knife_kill;
        [DefaultValue(0, false)]
        public int death_cnt;
        [DefaultValue(0, false)]
        public int headshoot;
        [DefaultValue(0, false)]
        public int fight_cnt;
        [DefaultValue(0, false)]
        public int win_cnt;
        [DefaultValue(0, false)]
        public int grenade_kill;
        [DefaultValue(0, false)]
        public int mvp_cnt;
        [DefaultValue(0, false)]
        public int gold_ace;
        [DefaultValue(0, false)]
        public int silver_ace;
        [DefaultValue(0, false)]
        public int qualifying_win;  // 排位胜场
        [DefaultValue(0, false)]
        public int qualifying_kill; // 排位击杀
        [DefaultValue(0, false)]
        public int max_multi_kill;  // 最高连杀数
        [DefaultValue(0, false)]
        public int assist_cnt;	//助攻数
        [DefaultValue("{}", true)]
        public GameStatdata[] game_stats;
    }

    public class EquipInfo
    {
        public int item_id; // 道具ID
        public int stage;   // 星阶
        public int time_limit;	// 有效期
        [DefaultValue("{}", true)]
        public accessory[] accessory_list;  // 配件
    }

    public class Decoration
    {
        public int part;
        public int item_id;
    }

    public class EquipGroup
    {
        public int group_id;
        public EquipInfo WEAPON1;
        public EquipInfo WEAPON2;
        public EquipInfo HELMET;
        public EquipInfo ARMOR;
        public EquipInfo BOOTS;
        public EquipInfo DAGGER;
        public EquipInfo GRENADE;
        public EquipInfo FLASHBOMB;
        public Decoration[] decorations;
    }

	public class RoleInfo
	{
		public int camp;
		public int item_id;
	}
	
    public long id;
    public int icon;
    [DefaultValue("")]
    public string icon_url;
    public int level;
    public int rank_val;
    public string corps_name;
    public int vip_level;
    public int sex;
    public string name;
    
    public EquipGroup[] equip_groups;
    public FightStatdata fight_stat;
    [DefaultValue("{}", true)]
    public FightStatdata[] mode_stats;
    [DefaultValue("{}", true)]
    public int[] badge_list;
    [DefaultValue("{}", true)]
    public p_title_info[] general_titles;
    [DefaultValue("{}", true)]
    public p_title_info[] qualifying_titles;
    [DefaultValue("{}", true)]
    public p_title_info[] set_titles;
    [DefaultValue(0, false)]
    public int hero_type;
	[DefaultValue("{}", true)]
	public RoleInfo[] roles;
    public int exp;
}

partial class toc_player_err_msg : toc_base
{
    public string proto_type;   // 协议主类型(临时)
    public string proto_key;    // 协议子类型(临时)
    public string err_msg;      // 错误信息
    public int err_code;        // 错误码
}

public class accessory
{
    public int pos;
    public int item_id;
    public int[] accessory_values; // 武器附加的加成值，在道具为武器附件时生效
}

public class p_item
{
    public int uid;
    public int item_id;
    public int item_cnt;
    public uint create_time;
    public uint time_limit;
    [DefaultValue("{}", true)]
    public int[] accessory_values; // 武器附加的加成值，在道具为武器附件时生效
//    [DefaultValue("{}", true)]
//    public accessory[] accessory_list;   // 附加列表，道具为主武器或副武器时生效
    [DefaultValue(0, false)]
    public int durability;
    public bool is_arsenal;             // true:在导师武器库； false:不在
}

public class p_group_equip_list
{
    [DefaultValue(0)]
    public int WEAPON1;
    [DefaultValue(0)]
    public int WEAPON2;
    [DefaultValue(0)]
    public int HELMET;
    [DefaultValue(0)]
    public int ARMOR;
    [DefaultValue(0)]
    public int BOOTS;
    [DefaultValue(0)]
    public int DAGGER;
    [DefaultValue(0)]
    public int GRENADE;
    [DefaultValue(0)]
    public int FLASHBOMB;
}

public class p_group_decoration
{
    public int part;
    public int decoration;  // uid
}

public class p_bag
{
    public int group_id;
    public p_group_equip_list equip_list;
    [DefaultValue("{}", true)]
    public p_group_decoration[] decorations;
}

partial class tos_player_item_equip_data : tos_base  //查询背包和仓库数据
{
}
partial class toc_player_item_equip_data : toc_base
{
    public p_item[] item_bag;
    public p_bag[] equip_groups;
}

partial class toc_item_new : toc_base
{
    public p_item item;
    public int reason;  //神兽武器：1 ; 其他:0
}

partial class toc_item_update : toc_base
{
    public p_item item;
    public int reason;  //神兽武器：1 ; 其他:0
}

partial class toc_item_del : toc_base
{
    public int uid;
}

partial class tos_player_buy_item : tos_base    //购买道具
{
    public int mall_id;
    public int days;
    public bool in_blackmarket;
}
partial class toc_player_buy_item : toc_base
{

}


partial class tos_player_equip_item : tos_base  //装备道具到指定背包
{
    public int group;
    public int uid;
}
partial class toc_player_equip_item : toc_base
{
    public p_bag val;
}

partial class tos_player_buy_equip_bag : tos_base   //开背包
{
}
partial class toc_player_buy_equip_bag : toc_base
{
    public p_bag bag;
}

partial class tos_player_set_default_role : tos_base    // 设置默认角色
{
    public int camp;
    public int role_uid;
}

partial class toc_player_set_default_role : toc_base
{
    public int camp;
    public int role_uid;
}

partial class toc_player_equip_bag : toc_base
{
    public p_bag data;
}

public class p_audio_chat
{
    public int audio_id;
    public int duration;
    public byte[] content;
}

partial class tos_player_chat : tos_base
{
    public int channel;
    public string msg;
    public long pid;
    public p_audio_chat audio_chat;
    public int log_type;//0-写日志 1-不写日志
    public string cost_type;//world_invite - 世界邀请消息
}

partial class toc_player_chat_msg : toc_base
{
    public int channel;
    public long from;
    public string msg;
    public int icon;
    public string name;
    public int sex;
    public int level;
    public int vip_level;
    public p_audio_chat audio_chat;
    public string area;
    [DefaultValue(0, false)]
    public int hero_type;
}

partial class tos_player_audio : tos_base
{
    public int audio_id;
}

partial class toc_player_audio : toc_base
{
    public p_audio_chat audio_chat;
}

partial class tos_player_rank_list : tos_base
{
    public string name;
    public int page;
}

partial class toc_player_rank_list : toc_base
{
    public class RankData
    {
        public long id;
        public string name;
        public string corps_name;
        public int val;
        public int ext;
    }

    public string name;
    public int page;
    public int start;
    public int size;    // 排行榜总大小
    public RankData[] list;
}

partial class toc_player_notice : toc_base
{
    public string msg;
    [DefaultValue(2)]
    public int type;    // 2:顶部滚动 4:战斗内公告
}

partial class tos_player_weapon_strengthen_data : tos_base { }

partial class toc_player_weapon_strengthen_data : toc_base
{
    public p_weapon_strength_info_toc[] strengthen_list;
}

public class p_awaken_data
{
    public int level;
    public int[] props;
    public int times;
}
public class p_weapon_strength_info_toc
{
    public int weapon_id;
    public int state;   // 星阶 从0开始
    public int level;   // 等级 从1开始
    public int cur_exp;
    public p_awaken_data awaken_data;
}

partial class tos_player_weapon_train_info : tos_base { }

partial class toc_player_weapon_train_info : toc_base
{
    public class train_info
    {
        public int index;
        public int weapon;
        public float ratio;
        public int need_time;
        public int start_time;
        public int total_exp;
    }
    public train_info[] train_list;
    public int quicken_times;
}

partial class tos_player_start_weapon_train : tos_base
{
    public int index;       // 训练位置1~4
    public int weapon_uid;  // 武器唯一ID
    public int time_opt;    // 时间选择（配置表中的ID项）
    public int ratio_opt;   // 效率选择（配置表中的ID项）
}

partial class toc_player_start_weapon_train : toc_base
{
    public int index;
    public int weapon;
    public float ratio;
    public int need_time;
    public int start_time;
    public int total_exp;
}

partial class tos_player_stop_weapon_train : tos_base
{
    public int index;       // 训练位置1~4
    public int quicken;     // 是否加速 0：否 1：是
}

partial class toc_player_stop_weapon_train : toc_base
{
    public int index;
    public int gain_exp;    // 最终获得经验
}

partial class tos_player_weapon_awaken : tos_base
{
    public long weapon_uid;
}

partial class toc_player_weapon_awaken : toc_base
{
    public bool ret_code;
    public int weapon_id;
    public int level;
    public int[] weapon_prop;
}

partial class tos_player_upgrade_weapon_strengthen_state : tos_base
{
    public int weapon_uid;
}

partial class toc_player_upgrade_weapon_strengthen_state : toc_base
{
    public int weapon_id;
    public int state;   // 星阶 将等级置0，经验清0
}

partial class toc_player_win_stage : toc_base
{
    public bool firsttime;
    public int stageid;
}

partial class tos_player_mail_list : tos_base { }

partial class toc_player_mail_list : toc_base
{
    public class attachment
    {
        public int item_id;
        public int item_cnt;
        public int days;
    }
    public class mail
    {
        public int mail_id;
        public long from_id;
        public string from_name;
        public string title;
        public string content;
        public attachment[] attachment;
        public int create_time;
    }
    public mail[] list;
}

partial class tos_player_read_mail : tos_base
{
    public int mail_id;
}

partial class toc_player_del_mail : toc_base
{
    public int mail_id;
}
partial class toc_player_new_mail : toc_base
{
    public toc_player_mail_list.mail mail;
}

partial class tos_player_read_all_mail : tos_base { }

partial class tos_player_send_mail_gift : tos_base
{
    public long to_pid;
    public int mall_id;
    public int index;
    public string title;
    public string content;
}

partial class toc_player_send_mail_gift : toc_base
{
    public string msg;
}

partial class tos_player_use_exchange_code : tos_base
{
    public string exchange_code;
}

partial class toc_player_use_exchange_code : toc_base
{
    public string exchange_code;
}

partial class tos_player_my_ranks : tos_base { }

partial class toc_player_my_ranks : toc_base
{
    public class rank_info
    {
        public string name; // 排行榜名字
        public int rank;    // 名次
        public int val;  // 分值
        public int ext;
    }
    public rank_info[] ranks;
}

partial class tos_player_friend_ranklist : tos_base
{
    public string name; // 排行榜名字
}

partial class toc_player_friend_ranklist : toc_base
{
    public class rank_info
    {
        public long id;      // 玩家ID
        public string name; // 玩家名字
        public string corps_name;   // 战队名
        public int rank;    // 名次
        public int val;  // 分值
        public int ext;
    }
    public string name;     // 排行榜名字
    public rank_info[] list;
}

partial class toc_player_ranklist_reward : toc_base
{

}

partial class tos_player_room_invitelist : tos_base
{
    public int stage;       // 邀请哪一个关卡的。0表示无关卡需求。
    public bool is_friend;  // true为好友、false为公会成员
    public string stage_type;   // 关卡类型，空串表示非关卡  stage:普通关卡  stage_survival:生存闯关
}

partial class toc_player_room_invitelist : toc_base
{
    public class PlayerInfo
    {
        public long id;
        public string name;
        public int level;
        public int icon;
        public int vip_level;
        public bool fight;  // 是否在战斗中
        public int room;    // pvp,pve
        public int team;    // 排位赛队伍
        public int handle;  //大于0就在线
        public int mvp;
        public int gold_ace;
        public int silver_ace;
    }
    public int stage;
    public bool is_friend;  // true为好友、false为公会成员
    public PlayerInfo[] list;
    public string stage_type;
}

partial class tos_player_signin_data : tos_base { } // 签到数据

partial class toc_player_signin_data : toc_base
{
    public p_signin_data_toc monthly_signin;    // 每月签到
    public p_signin_data_toc cumulate_signin;   // 累计签到
    public p_signin_data_toc day_signin; //新人七天签到
    public p_signin_data_toc cumulate_signin_new;//
}

public class p_signin_data_toc
{
    public int last_sginin_time;    // 上一次签到时间
    public int signined_times;      // 已签到次数
    public int supplement_times;    // 补签次数
    public int[] rewarded;
}

partial class tos_player_sign_in : tos_base     // 签到
{
    public int type;    // 1：每月签到；  2：累计签到
    public bool supplement; // 是否为补签
    public int flag; // 标记签哪一个
}

partial class toc_player_sign_in : toc_base
{
    public int type;
    public bool supplement;
    public p_signin_data_toc data;  // 对应签到数据
    public int flag;
}

partial class tos_player_welfare_data : tos_base { }    // 福利数据

partial class toc_player_welfare_data : toc_base
{
    public p_welfare_data_toc data;
}

partial class tos_player_get_welfare_reward : tos_base
{
    public int id;  // 福利ID
}

partial class toc_player_get_welfare_reward : toc_base
{
    public int id;
    public p_welfare_data_toc data;
}

public class p_welfare_data_toc
{
    public int[] welfare_list;      // 福利状态列表   0：未达成； 1：已达成； 2：已领奖
    public int last_online_time;    // 玩家上一次领取在线奖励时的总在线时间
    public int total_online_time;   // 玩家总在线时间
}


partial class toc_player_gm : toc_base
{
    public class gm_def
    {
        public string name;
        public string[] cmds;
    }
    public gm_def[] list;
}

partial class tos_player_gm : tos_base
{
    public string name;
    public string cmd;
}

partial class toc_player_weapon_strengthen_info : toc_base
{
    public p_weapon_strength_info_toc info;
}

partial class tos_player_set_icon : tos_base    // 玩家修改头像
{
    public int icon;
}

partial class toc_player_set_icon : toc_base
{
    public int icon;
}

partial class toc_player_game_reward : toc_base
{
    public int exp;
    public int coin;
    public int medal;
    public int[][] weapon_exp_reward;       //本次战斗内获得经验的武器及相应的经验，itemId
    public ItemInfo[] item_reward;
    public int[] weapon_durability_reduce;  //本次战斗内耐久减少的武器，uid
}

partial class tos_player_double_game_reward : tos_base { }

partial class toc_player_double_game_reward : toc_base
{
    public int times;   // 双倍奖励次数
    public int exp;
    public int coin;
    public int medal;
}

partial class tos_player_default_fight_roles : tos_base { }

partial class toc_player_default_fight_roles : toc_base
{
    public class role
    {
        public int camp;
        public int uid;
    }
    public role[] roles;
    [DefaultValue(0, false)]
    public int hero_sex;
}

partial class toc_player_chat : toc_base
{
    public int channel;
    public long pid;
    public int err_code;    // 0：成功 1：对方离线
}

partial class tos_player_verify_crc : tos_base
{
    public class info
    {
        public string path;
        public uint crc;
    }

    public info[] list;
}

partial class toc_player_critical_msg : toc_base
{
    public string msg;      // 错误信息
}

partial class tos_player_log_event : tos_base
{
    public int event_id;    // 预先定义好的事件ID
}

partial class tos_player_complain : tos_base
{
    public int complain_type;    // 投诉类型(11='bug',12='投诉',13='建议',10='其他', 15='咨询')
    public string content;       // 投诉内容
}

partial class tos_player_rename : tos_base
{
    public string name;
}

partial class toc_player_rename : toc_base
{
    public string name;
}
partial class tos_player_use_item : tos_base
{
    public int item_uid;
}

partial class toc_player_use_item : toc_base
{
    public int item_id;
}

partial class tos_player_buff_list : tos_base { }

partial class toc_player_buff_list : toc_base
{
    public class buff
    {
        public int buff_id;
        public int time_limit;
    }
    public buff[] list;
}

partial class tos_player_compound : tos_base
{
    public int item_src_id;
    public int item_des_id;
}

partial class toc_player_compound : toc_base
{
    public int item_des_id;
}

public class p_channel_info
{
    public int id;
    public string typ;
    public string subtyp;
    public string name;
    public int player_cnt;
    public int player_max;
}

partial class tos_player_room_list : tos_base
{
    public int channel;
}



public class p_room_info
{
    public int id;
    public string name;
    public long leader_id;
    public string leader;
    public int leader_hero; // 房主英雄卡
    public string leader_device;
    public int map;
    public string rule;
    public int player_count;
    public int max_player;
    public int spectators_count;
    public bool fight;
    public bool password;
    public int cur_round;
    public int max_round;
    public int level;
    public int state;
    public int level_limit;
}

partial class toc_player_room_list : toc_base
{
    public p_room_info[] list;
}

partial class tos_player_test_accessory_meterial : tos_base     // 武器配件材料鉴定
{
    public int[] meterial_list;
}

partial class toc_player_test_accessory_meterial : toc_base
{
    public int[] meterial_list;
    public p_item[] accessory_list;    // 鉴定出的附件
}

partial class tos_player_equip_weapon_accessory : tos_base
{
    public int weapon_id;
    public int accessory_uid;
    public int pos;    // 成功装备的位置
}

partial class toc_player_equip_weapon_accessory : toc_base
{
    public int weapon_id;
    public int accessory_uid;
    public int pos;    // 成功装备的位置
}

partial class tos_player_unequip_weapon_accessory : tos_base
{
    public int weapon_id;
    public int pos;
}

partial class toc_player_unequip_weapon_accessory : toc_base
{
    public int weapon_id;
    public int pos;
}

partial class tos_player_buyback_weapon_accessory : tos_base
{
    public int[] accessorys;
}

partial class toc_player_buyback_weapon_accessory : toc_base
{
    public int[] accessorys;
    public int coin;    // 回购获得的总金币
}

// 充值活动
partial class tos_player_first_recharge_welfare : tos_base
{
}
partial class toc_player_first_recharge_welfare : toc_base
{
    public int id;
}
// 每日首充礼包
partial class tos_player_first_recharge_data : tos_base { }
partial class toc_player_first_recharge_data : toc_base
{
    public int tag;     // 1:未获得 2:已领取  3:可领取
    public int id;
}
// 领取每日首充礼包
partial class tos_player_get_first_recharge : tos_base
{
    public int id; // FirstRecharge配置表中对应的ID
}

// 累计充值礼包
partial class tos_player_recharge_welfare_list : tos_base { }
partial class toc_player_recharge_welfare_list : toc_base
{
    public int stage;
    public int event_id;
    public int utc_time;    //> 创角时间戳或开服时间戳
    public int elapse_days; //> 创角或开服经过的天数
    public int recharge_money;  //> 本阶段的充值总数
    public int[] welfares;
    public int[] received_welfares;
}

partial class tos_player_exchange_list : tos_base
{
    public string type; //兑换类型
}
partial class toc_player_exchange_list : toc_base
{
    public int[] list; //已领取的兑换列表
}

partial class tos_player_exchange : tos_base
{
    public string type; //兑换类型
    public int id; //兑换ID
}

partial class toc_player_exchange : toc_base
{
    public string type; //兑换类型
    public int id; //兑换ID
}

partial class tos_player_exchange_num : tos_base
{
    public string type; //兑换类型
}

partial class toc_player_exchange_num : toc_base
{
    public int exchange_num;
}
// 领取累计充值礼包
partial class tos_player_get_recharge_welfare : tos_base
{
    public int id;  // AccumulativeRecharge配置表中对应的ID 
}

partial class tos_player_recharge_info : tos_base
{
}

partial class toc_player_recharge_info : toc_base
{
    public int[] recharge_id_list;    // 充值购买过的物品Id数组
}

partial class toc_player_recharge_associator_info : toc_base
{
    public p_recharge_associator_info[] recharge_associator_info;    // 会员信息
}

partial class tos_player_recharge_switch : tos_base
{
    public int item_id;
}

partial class toc_player_recharge_switch : toc_base
{
    public bool is_open;    // 充值是否开启
    public string callback_info;
}

public class p_lottery_player
{
    public long uid;
    public string name;
    public int reward_id;
    public int award_amount;
}

partial class tos_player_lottery : tos_base
{
    public int lottery_type;//0：查询信息，1：普通抽奖，2：10连抽
}

partial class toc_player_lottery : toc_base
{
    public int[] reward_id_list;
    public p_lottery_player top_player;
    public int award_amount;
    public int free_times;
    public int free_cd;
}

partial class tos_player_get_lottery_rank : tos_base
{
}

partial class toc_player_get_lottery_rank : toc_base
{
    public p_lottery_player[] player_list;
}

partial class toc_player_lottery_message : toc_base
{
    public p_lottery_player player;
}

// 改版抽奖
partial class tos_player_new_lottery : tos_base
{
    public int lottery_type;
    public int time;
}

partial class toc_player_new_lottery : toc_base
{
    public int lottery_type;
    public int time;
    public ItemInfo[] items;	// 奖励内容
    public int[] his_items;		// 历史奖励
    public int award_amount;	// 奖池数据
    public int point;
    public string[] lucky_list;
    public string[] recent_list;
    public int free_time;		// 免费抽奖时间
    public bool[] exists;
    public int buy_cnt;
	public int discount_one_count;	// 单次折扣次数
	public int discount_ten_count;	// 十次折扣次数
}

// 改版兑换
partial class tos_player_new_exchange_lottery : tos_base
{
    public int exchange_code;	// 配置对应
}

partial class toc_player_new_exchange_lottery : toc_base
{
    public int exchange_code;	// 配置对应
}

partial class tos_player_channel_list : tos_base
{
}

partial class toc_player_channel_list : toc_base
{
    public p_channel_info[] channels;
}

partial class tos_player_exchange_points : tos_base
{
    public int op_code;//0-查询，1-抽奖触发，2(or 物品ID)-兑换
}

partial class toc_player_exchange_points : toc_base
{
    public int op_code;//抽奖类型，对应LotteryExchange的Id字段，NewLotteryCost的Type字段
    public int points;
}

// 活动相关
public class p_event
{
    public int id;
    public int status;
    public int extend;      //> 扩展字段
}
partial class tos_player_event_list : tos_base
{

}
partial class toc_player_event_list : toc_base
{
    public p_event[] events;
    public ItemInfo[] items;
}

partial class tos_player_get_event : tos_base
{
    public int id;
}

partial class tos_player_cash_gain_info : tos_base { }

partial class toc_player_cash_gain_info : toc_base
{
    public int total_gain;  // 总共获得的现金券
    public int before_gain; // 今天之前获得的现金券
}

public class p_friend_data
{
    public long id;
    public string name;
    public int sex;
    public int icon;
    public int honor;
    public int vip_level;
    public int level;
    public int handle;
    public int channel;
    public int room;
    public int team;
    public int mvp;
    public int gold_ace;
    public int silver_ace;
    public bool fight;
    public int logout_time;
    [DefaultValue("{}", true)]
    public p_hero_card_info[] hero_cards;
    [DefaultValue("{}", true)]
    public p_title_info[] set_titles;
}

partial class tos_player_friend_rcmnd_list : tos_base
{

}

partial class toc_player_friend_rcmnd_list : toc_base
{
    public p_friend_data[] list;
}

partial class tos_player_friend_data : tos_base
{
}

partial class toc_player_friend_data : toc_base
{
    public p_friend_data[] friend_list;
    public p_friend_data[] fans_list;
    public p_friend_data[] black_list;
}

partial class toc_player_friend_msg : toc_base
{
    public string type;
    public p_friend_data friend_data;
}

partial class tos_player_view_player : tos_base
{
    public long friend_id;
}

partial class tos_player_view_player_by_name : tos_base
{
    public string friend_name;
}

partial class tos_player_add_friend_by_name : tos_base
{
    public string friend_name;
}

partial class tos_player_add_friend : tos_base
{
    public long friend_id;
}

partial class toc_player_add_friend : toc_base
{
    public long friend_id;
}

partial class tos_player_del_friend : tos_base
{
    public long friend_id;
}

partial class toc_player_del_friend : toc_base
{
    public long friend_id;
}

partial class tos_player_agree_fan : tos_base
{
    public long fan_id;
}

partial class toc_player_agree_fan : toc_base
{
    public long fan_id;
}

partial class tos_player_refuse_fan : tos_base
{
    public long fan_id;
}

partial class toc_player_refuse_fan : toc_base
{
    public long fan_id;
}

partial class tos_player_add_black : tos_base
{
    public long black_id;
}

partial class toc_player_add_black : toc_base
{
    public long black_id;
}

partial class tos_player_del_black : tos_base
{
    public long black_id;
}

partial class toc_player_del_black : toc_base
{
    public long black_id;
}

partial class toc_player_getdata : toc_base
{
    public CDict req;
}

partial class tos_player_getdata : tos_base
{
    public CDict ret;
}

partial class tos_player_repair_weapons : tos_base  //> 修理枪械
{
    public int[] weapon_uids;
}

partial class toc_player_repair_weapons : toc_base  //> 修理枪械成功返回
{
}

partial class toc_player_game_reward_times : toc_base
{
    public int partake_reward; // 参与奖励次数
    public int win_reward;     // 胜利奖励次数
    public string game_type;    // defend，worldboss
}

partial class toc_player_blackmarket_notice : toc_base
{
}

partial class tos_player_blackmarket_list : tos_base
{
}

partial class toc_player_blackmarket_list : toc_base
{
    public int[] item_list;
    public int refresh_time;    // 自动刷新剩余时间（秒）
    public int refresh_count;   // 当天付费刷新次数
    public int refresh_cost;    // 下次付费刷新消耗，-1表示不可刷新
    public int show_count;      // 可以购买的格子数
}

partial class tos_player_blackmarket_refresh : tos_base
{
}

partial class tos_player_mall_record : tos_base
{
}

partial class p_mall_record
{
    public int mall_id;
    public int amount;
}

partial class toc_player_mall_record : toc_base
{
    public p_mall_record[] p_record;
}

partial class tos_player_load_done : tos_base
{
    public int step;
}

partial class tos_player_get_zone_info : tos_base
{
    public long owner_id;
}


public class p_zone_info_item
{
    public string key;// player_id(long) icon(int) vip_level(int) name(string) description(string) region(string) gender(int 0:男 1:女) online_time(string) birthday(string) qq(string) 
    public string value;
}
partial class toc_player_get_zone_info : toc_base
{
    public p_zone_info_item[] p_zone_info;
}

partial class tos_player_edit_zone : tos_base
{
    public p_zone_info_item[] p_zone_info;
}

partial class toc_player_edit_zone : toc_base
{
    public int result; // 0:成功 1:含有敏感字 9:其他错误
}

public class p_achievement
{
    public int id;              //> 成就id
    public int val;             //> 成就值
    public int stat;            //> 成就状态 

}

partial class tos_player_achievement_data : tos_base
{
}

partial class toc_player_achievement_data : toc_base        //> 成就数据,是
{
    public p_achievement[] achieve_list;                    //> 已经开始的成就的状态
    public int[] achieve_badge_list;                        //> 已经获得的徽章
    public int[] achieve_chenghao_list;                     //> 已经获得的称号
    public int[] achieve_head_icon_list;                    //> 已经获得的头像
    public int achieve_point;                               //> 已经获得的成就点
}

partial class toc_player_achieve_update : toc_base          //> 更新单个成就
{
    public p_achievement achieve;
}

public class p_achieve_add_item
{
    public string tag;
    public int val;
}
partial class toc_player_achieve_add : toc_base             //> 成就数据的增加,主要是achieve_point,chenghao,head,badge
{
    public p_achieve_add_item[] item_add_list;
}
partial class tos_player_get_achieve_reward : tos_base
{
    public int achieve_id;
}

partial class toc_player_get_achieve_reward : toc_base
{
    public int achieve_id;
}

partial class tos_player_set_chenghao : tos_base    // 玩家修改称号
{
    public int chenghao;
}

partial class toc_player_set_chenghao : toc_base
{
    public int chenghao;
}

partial class tos_player_trigger_guide : tos_base
{

}
partial class toc_player_trigger_guide : toc_base
{
    public bool is_trigger;     // true:触发引导; false: 不触发
}

partial class toc_player_server_value : toc_base
{
    public CDict dict;
}

public class p_survival_type_data
{
    public string type;
    public int top_level;
}

partial class toc_player_survival_data : toc_base
{
    public int daily_reward_cnt;                // 每日奖励次数
    public p_survival_type_data[] types_data;   // 各模式数据
}

// 活动相关
public class p_player_event
{
    public int id;
    public bool open;

    // 个人任务类型需要
    [DefaultValue(0)]
    public int progress;    // 任务进度（progress>=total时表示任务达成）
    [DefaultValue(0)]
    public int total;       // 任务总数
    [DefaultValue(0)]
    public int reward;      // 已得奖次数
}

partial class toc_player_eventlist : toc_base
{
    public p_player_event[] list;
}

partial class tos_player_take_event_reward : tos_base
{
    public int event_id;
}

partial class toc_player_take_event_reward : toc_base
{
    public int event_id;
}

partial class toc_player_client_value : toc_base
{
    public CDict dict;
}

partial class tos_player_client_value_update : tos_base
{
    public CDict dict;
}

partial class tos_player_mission_data : tos_base { }

public class p_mission_data_toc
{
    public int id;
    public int status;      // 状态 0:未完成 1:进行中 2:已完成 3:已领奖
    public int progress;    // 进度，未完成时生效
}

partial class toc_player_mission_data : toc_base
{
    public p_mission_data_toc[] missions;
}

partial class toc_player_modify_mission : toc_base
{
    public string type;     // add：新任务 del：删除任务 update：任务更新
    public p_mission_data_toc mission;
}

partial class tos_player_get_mission_reward : tos_base
{
    public int mission_id;
}

partial class toc_player_get_mission_reward : toc_base
{
    public int mission_id;
}

partial class toc_player_offline_chat_msg : toc_base
{
    public class offline_msg
    {
        public int channel;
        public long from;
        public string msg;
        public int icon;
        public string name;
        public int sex;
        public int level;
        public int vip_level;
        public int time;
        public string area;
    }
    public offline_msg[] list;
}


partial class tos_player_comfirm_offline_msg : tos_base
{
    public long player_id;
}

partial class toc_player_is_oldplayer : toc_base
{
    public int is_oldplayer;        // 0:表示非老玩家 1:表示是老玩家
    public int is_invited;          // 0:表示未邀请 1：表示已邀请
}

partial class tos_player_oldplayer_invite : tos_base
{
    public long player_id;
}

partial class tos_player_get_oldplayer_gift_info : tos_base
{
}

partial class toc_player_get_oldplayer_gift_info : toc_base
{
    public int invite_gift_cnt;     // 好人礼包个数
    public int pop_gift_cnt;        // 人缘帝礼包个数
}

class p_item_info_toc
{
    public int item_id;
    public int item_cnt;
    public int days;
}

partial class toc_player_use_giftbag : toc_base
{
    public int giftbag_id;  // 使用的礼包id
    public p_item_info_toc[] item_list;   // 获得的道具
}

partial class tos_player_check_online_time_target : tos_base
{
}

partial class tos_player_stage_survival_data : tos_base { }

partial class toc_player_stage_survival_data : toc_base
{
    public class stage_survival_info
    {
        public int stage_id;    // 关卡ID
        public int best_star;   // 最高星数
    }
    public stage_survival_info[] list;
    public int max_stage;       // 通关的最大关卡id
}

partial class toc_player_stage_star : toc_base
{
    public string type;     // 关卡类型 stage_survival
    public int stage;       // 关卡id
    public int star;        // 获得星数
}

partial class tos_player_refresh_online_time : tos_base
{
}

partial class toc_player_game_time_today : toc_base
{
    public int game_time;
    public int anti_addiction;
}

partial class tos_player_anti_addiction : tos_base
{
    public string player_name;
    public string id;
}

partial class toc_player_anti_addiction : toc_base
{
    public int anti_addiction;
}

partial class tos_player_set_anti_addiction_rate : tos_base
{
}

public class option
{
    public string option_type;
    public int option_val;
}
partial class toc_player_options : toc_base
{
    public option[] options;
}

partial class tos_player_set_option : tos_base
{
    public string option_type;
    public int option_val;
}


partial class tos_player_del_item : tos_base
{
    public int uid;
}

partial class tos_player_quick_join_action : tos_base
{
}

partial class tos_player_report : tos_base
{
    public long be_report_id;   // 被举报玩家ID
    public string be_report_name;
    public int report_type;     // 举报类型
    public string content;      // 举报内容
    public string proof;
    public string proof2;
    public string proof3;
}

partial class tos_player_tag_list : tos_base { }    // 玩家标记列表

partial class toc_player_tag_list : toc_base
{
    public class p_tag
    {
        public int tag_id;
        public int value;
    }
    public p_tag[] tag_list;
}

partial class tos_player_update_tag : tos_base      // 更新玩家标记
{
    public int tag_id;
    public int value;
}

partial class toc_player_update_tag : toc_base      // 玩家标记更新
{
    public int tag_id;
    public int value;
}

partial class tos_player_log_misc : tos_base
{
    public int log_typ;//类型0好评(log_n1:0结算1军火库2充值3第二天签到,log_n2: 0赏好评1吐槽2下次再说); 1微信(log_n1: 0结算1军火库,log_n2: 0朋友1朋友圈)
    public int log_n1;
    public int log_n2;
    public string log_s1;
    public string log_s2;
}

partial class toc_player_item_transform : toc_base
{
    public ItemInfo from;
    public ItemInfo to;
}

partial class toc_player_flop_reward : toc_base
{
    public ItemInfo[] item_list;
    public int[] pos_list;
    public int[] color_list;
}

partial class tos_player_flop_reward : tos_base
{
    public int pos;
}

// 军火库幸运榜、实施公告展示
partial class tos_player_new_lottery_show : tos_base
{
    public int type;
}

partial class toc_player_new_lottery_show : toc_base
{
    public int type;
    public string[] lucky_list;
    public string[] recent_list;
}

partial class toc_player_close_agent : toc_base
{
    public int reason;
    public string msg;
}

partial class tos_player_hero_card_data : tos_base { }

partial class toc_player_hero_card_data : toc_base
{
    public p_hero_card_info[] cards;
    public int reward_time;     // 上次领取礼包的时间戳，用于判断今天可不可以领取礼包
}

partial class tos_player_take_hero_card_reward : tos_base { }

partial class toc_player_take_hero_card_reward : toc_base
{
    public int reward_time;
}

partial class tos_player_set_titles : tos_base
{
    public p_title_info[] titles;
}

partial class toc_player_set_titles : toc_base
{
    public p_title_info[] titles;
}

partial class tos_player_subtype_room_list : tos_base
{
    public string subtype;
}

partial class toc_player_subtype_room_list : toc_base
{
    public int channel;
    public p_room_info[] list;
}

partial class tos_player_diamond2eventcoin_002 : tos_base
{
    public int count;   // 点券数量
}

partial class toc_player_diamond2eventcoin_002 : toc_base
{
    public int count;   // 点券数量
}

partial class toc_player_available_channel : toc_base
{
    public int channel;
}

public class p_timelimit_item
{
    public string reward_id;
    public int buy; //0没买过, 1买过
    public int price;
}

partial class tos_player_timelimit_gift : tos_base
{
}

partial class toc_player_timelimit_gift : toc_base
{
    public int gift_id;
    public p_timelimit_item[] item_list;
    public int second_left;
}

partial class tos_player_timelimit_gift_buy : tos_base
{
    public int gift_id;
    public string reward_id;
}

partial class toc_player_timelimit_gift_buy : toc_base
{
    public string reward_id;
}

partial class tos_player_timelimit_gift_time : tos_base
{
}

partial class toc_player_timelimit_gift_time : toc_base
{
    public int timestamp;
}

public class p_foundation_info
{
    public int foundation_id;
    public bool rewarded;
}

partial class tos_player_foundation_data : tos_base { }

partial class toc_player_foundation_data : toc_base
{
    public p_foundation_info[] data;
}

partial class tos_player_get_foundation_reward : tos_base
{
    public int id;
}

partial class toc_player_get_foundation_reward : toc_base
{
    public int id;
}

partial class tos_player_set_hero_sex : tos_base
{
    public int sex; // 0：男; 1：女
}

partial class toc_player_set_hero_sex : toc_base
{
    public int sex;
}

partial class tos_player_weapon_accessory_data : tos_base { }

public class p_weapon_accessory_info
{
    public int weapon;              // 武器ID
    public accessory[] accessories; // 配件列表
}

partial class toc_player_weapon_accessory_data : toc_base
{
    public p_weapon_accessory_info[] list;
}

partial class toc_player_weapon_accessories : toc_base  // 某个武器的配件更新
{
    public int weapon;              // 武器ID
    public accessory[] accessories; // 配件列表
}

partial class tos_player_phased_purchase_data : tos_base
{
}

partial class toc_player_phased_purchase_data : toc_base
{
    public p_phased_purchase_item[] phased_purchase_list;
}

public class p_phased_purchase_item
{
    public int item_id; //物品的id
    public int item_uid; //物品的uid
    public int phased_number; //当前第几期
    public int total_phased_number; //总共有多少期
    public int mall_id;
}

partial class toc_player_phased_item_update : toc_base
{
    public p_phased_purchase_item item;
}

partial class toc_player_phased_item_new : toc_base
{
    public p_phased_purchase_item item;
}

partial class toc_player_phased_item_del : toc_base
{
    public int item_uid;
}

partial class tos_player_unequip_decoration : tos_base
{
    public int group;
    public int part;
}

partial class toc_player_unequip_decoration : toc_base
{
    public int group;
    public int part;
}

partial class tos_player_icon_url_data : tos_base { }

partial class toc_player_icon_url_data : toc_base
{
    public string[] url_list;
    public int cur_index;
}

partial class tos_player_add_icon_url : tos_base
{
    public string icon_url;
}

partial class toc_player_add_icon_url : toc_player_icon_url_data { }

partial class tos_player_set_icon_url : tos_base
{
    public int new_index;
}

partial class toc_player_set_icon_url : toc_player_icon_url_data { }

partial class tos_player_del_icon_url : tos_base
{
    public int del_index;
}

partial class toc_player_del_icon_url : toc_player_icon_url_data { }

partial class tos_player_match_punish_data : tos_base { }

partial class toc_player_match_punish_data : toc_base
{
    public int clear_time;      // 重置时间
    public int daily_dec_time;  // 每日恢复时间
    public int punish_value;    // 惩罚值（递增）
    public int release_time;    // 解禁时间
}

partial class tos_player_social_joingroup : tos_base 
{ 
    public string platform; //社交平台
}

partial class tos_player_social_join_media : tos_base
{

}

partial class toc_player_social_join_media : toc_base
{
    public bool join_reward; //是否加入VK或者FB已经点赞相关奖励发放标志
    public bool share_reward; //每日分享奖励是否发送
    public bool bind_invite_code; //是否已经绑定了邀请码
}

//分享游戏
partial class tos_player_social_share_game : tos_base 
{

}


//被邀请人发送邀请码
partial class tos_player_social_invitor_name : tos_base
{
    public long player_id; //邀请人的邀请码
}

partial class toc_player_social_invitor_name : toc_base
{
    public string invitor_name; //邀请人的名字
}

partial class tos_player_social_bind_invite_code : tos_base
{
    public long player_id; //邀请人的邀请码
    public string device_code; //被邀请人的唯一标识
}

partial class toc_player_social_bind_invite_code : toc_base
{
    public int result; // 0表示邀请码不存在，1表示设备号重复，2表示已经达到了上限 3表示成功
}

//绑定手机请求
partial class tos_player_phone_register : tos_base
{
    public string phone_num;
}

partial class toc_player_phone_register : toc_base
{
    public int state; //0首次绑定,1非首次绑定，2 CD中
}

//验证码验证
partial class tos_player_phone_verify : tos_base
{
    public string phone_num;
    public string code;
}

partial class toc_player_phone_verify : toc_base
{
}

//获取验证码请求
partial class tos_player_phone_code : tos_base
{
    public string phone_num;
}

partial class toc_player_phone_code : toc_base
{
}

partial class tos_player_phone_info : tos_base
{
}

partial class toc_player_phone_info : toc_base
{
    public string phone_num;
    public uint reg_time;
}

partial class tos_player_social_invite_friend : tos_base
{
    public int friend_id;
}

partial class toc_player_social_invite_friend : toc_base
{
    public bool result;
}

#endregion


#region fight相关

partial class toc_fight_base : toc_base // 含来源的fight协议基类
{
    public int from;
}

partial class toc_fight_msg : toc_base
{
    public string message;
}

partial class toc_fight_leave : toc_base
{
    public int val;
}

partial class tos_fight_move : tos_base
{
    public short[] pos;
    public byte anglePitch;
    public byte angleY;
    public byte moveState;
    public byte clientSpeed;
    public short time;
    public float timeStamp;
}

partial class toc_fight_move : toc_fight_base
{
    public short[] pos;
    public byte anglePitch;
    public byte angleY;
    public byte moveState;
    public byte moveSpeed;
    public byte clientSpeed;
    public short time;
}

partial class tos_fight_switch_weapon : tos_base
{
    public string weapon;
    public int atk_type;
    //public bool advbullet;  // 开启了穿甲弹相当于切到穿甲枪，所以跟切枪使用同一协议
}

partial class toc_fight_equip_info : toc_base   // 只有自己收到
{
    public class EquipInfo
    {
        public string type;
        public int itemid;
        public int cur_count;   // 武器当前弹夹中剩余子弹数
        public int ext_count;   // 武器当前弹夹之外的子弹数
        public int adv_count;   // 穿甲弹数量
        public int sub_count;   // 子武器子弹数量
        public int max_count;   // 最大子弹数量
        [DefaultValue("{}", true)]
        public int[] awaken_props;  // 当前武器觉醒属性列表
    }

    public EquipInfo[] list;
    public string weapon;   // 当前武器类型，主武器，副武器，刀具什么的
    public int weaponVer;
    public bool unswitch;   // 客户端是否“不执行”切枪动作
}

class p_hit_info
{
    public int matType = 0;
    public int actorId = 0;
    public int bodyPart = 0;
    public Vector3 pos;
}

partial class tos_fight_fire : tos_base
{
    public p_hit_info[][] hitList;
    public short[] cameraRot;
    public Vector3 hitPos;
    public byte notPlayAni = 0x00; // 用于区别是不是从同一次射击发射出来的，如果是，则收到这些消息后，只播放一次动画
    public bool isZoom = false;     // 是否已开瞄准镜（部分枪支开镜后伤害不同）
    public int weaponVer;
    public int bulletCount;     // atkType剩余子弹数
    public int relyBulletCount; // atkType依赖子弹数
    public int atkType;    // 攻击方式 1：主武器默认 2：主武器穿甲 3：主武器第二攻击 4：副武器...
    public Vector3 firePos;
    public float timeStamp;
}

class p_hit_info_toc
{
    public int actorId;
    public int bodyPart;
    public int life;
    public int armor;
    public bool die;
    public byte hurtType;
}

partial class toc_fight_fire : toc_fight_base
{
    public p_hit_info_toc[] hitList;
    public short[] cameraRot;
    public Vector3 hitPos;
    public byte notPlayAni = 0x00;
    public int atkType;
    public short[] firePos;
}

//拾取掉落物
partial class tos_fight_pickup_drop_item : tos_base
{
    public int uid;
}

partial class toc_fight_del_drop_item : toc_fight_base
{
    public int uid;
}

class DropInfo
{
    public int uid;
    public float[] pos;
    public double drop_time;
    public int drop_id;
}
partial class toc_fight_add_drop_item : toc_base
{
    public DropInfo drop;
}

partial class toc_fight_drop_list : toc_base
{
    public DropInfo[] drop_list;
}

partial class tos_fight_start_set_bomb : tos_base { public int point; public float x; public float y; public float z; }   // 开始装炸弹
partial class toc_fight_start_set_bomb : toc_base { public int point; }

partial class tos_fight_stop_set_bomb : tos_base { }   // 停止装炸弹
partial class toc_fight_stop_set_bomb : toc_base { }

partial class toc_fight_set_bomb : toc_base { public int set_time; }   // 装好炸弹的时间

partial class tos_fight_start_defuse_bomb : tos_base { }    // 开始拆炸弹
partial class toc_fight_start_defuse_bomb : toc_base { }

partial class tos_fight_stop_defuse_bomb : tos_base { }    // 停止拆炸弹
partial class toc_fight_stop_defuse_bomb : toc_base { }

partial class toc_fight_defuse_bomb : toc_base { }      // 拆完炸弹

partial class toc_fight_bomb_burst : toc_base { public int point; }   // 炸弹爆炸






class p_hitg_info_toc
{
    public int id;
    public Vector3 pos;
}

partial class tos_fight_fireg : tos_base
{
    public p_hitg_info_toc[] hitList;
    public Vector3 fromPos;
    public int index;   // 投掷武器属性序号
    public Vector3 firePos;
}

partial class tos_fight_atk : tos_base
{
    public int firePose;    // 攻击动作index
    public Vector3 fireDir;
    public int index;       // 投掷武器属性保存序号
    public int atkType;    // 投掷武器攻击类型
}

partial class toc_fight_atk : toc_fight_base
{
    public int firePose;    // 攻击动作index
    public Vector3 fireDir;
    public int index;       // 投掷武器属性保存序号
    public int atkType;    // 投掷武器攻击类型
}

partial class tos_fight_reload : tos_base
{
    public float reloadTime;
    public int reloadConut;
}

partial class toc_fight_reload : toc_fight_base
{
    public float reloadTime;
    public int reloadConut;
}

partial class tos_fight_reload_done : tos_base
{
    public int weaponVer;
    public bool onlyOne;    // 是否只加了一发（如散弹枪）
}

partial class tos_fight_jump : tos_base
{
    public float[] pos;
    public int type;
}


partial class toc_fight_jump : toc_fight_base
{
    public float[] pos;
    public int type;
}

partial class tos_fight_stand : tos_base
{ }

partial class toc_fight_stand : toc_fight_base
{ }

partial class tos_fight_crouch : tos_base
{ }

partial class toc_fight_crouch : toc_fight_base
{ }

partial class toc_fight_fixbullet : toc_base
{
    public int weaponVer;
    public int bulletCountD;
    public int relyBulletCountD;
    public int atkType;
}

class p_fight_spectator
{
    public long player_id;
    public string name;
    public int icon;
    public int corps_id;
    public string corps_name;
    public int level;
}

partial class toc_fight_enter : toc_base
{
    public int myid;
    public uint rhd;
    public uint rid;
    public string type;
    public int map;
    public string rule;
    public bool auto_aim;

    public string server_addr;
    public int server_port;
    public bool has_zombie_cloth;   // 是否配备生化服，生化模式生效
    public bool has_zombie_blade;   // 是否配备生化刃，生化模式生效
}

partial class toc_fight_game_info : toc_base
{
    public int now;
    public p_fight_role[] list;
    public p_fight_spectator[] spectator_list;
    public p_fight_state state;
    // new add
    public float round_time;    // 本局已开始时间
    public p_camp_info[] camps;
    public int king_camp_last_win;
    public bool spectator_switch;
}

class p_camp_info
{
    public int camp;
    public int win;                 // 赢了几回合
    public int kill;                // 击杀总数
    public int round_begin_kill;    // 这回合之前的击杀总数
    public int king_camp_score;     // 团队枪王
    public int king_camp_cur_kill;
}

partial class toc_fight_join : toc_base
{
    public p_fight_role role;
    public string effect_id;
}

partial class toc_fight_actorprop : toc_base
{
    public class prop_info
    {
        public int atkType;
        public p_fight_prop prop;
    }
    public int id;
    public prop_info[] props;
    [DefaultValue("{}", true)]
    public int[] awaken_props;  // 当前武器觉醒属性列表

}

public class p_actor_buff_toc
{
    public int buff_id;
    public int start_time;  // 以战斗开始时间为基准的毫秒数
    public int end_time;    // 同上  
}

class p_actor_state_toc
{
    public int id;
    public int life;
    public float[] pos;
    public int state;
    public int camp;
    public int god_end;     // 无敌结束时间（以战斗开始时间为基准的毫秒时间）
    public int immune;      // 免疫次数
    public int actor_type; // 生化类型 0：人类 1：母体 1：精英 2：小兵
    public int fight_level;
    public p_actor_buff_toc[] buff_list;
}

partial class toc_fight_actorstate : toc_base
{
    public p_actor_state_toc[] list;
}

class p_fight_role
{
    public int id;
    public long pid; // player_id 机器人为0
    public int icon;
    public float[] pos;
    public int life;
    public int last_armor;
    public int last_damage_time;
    public int last_act_time;
    public int god_end; // 无敌结束时间（以战斗开始时间为基准的毫秒时间）
    public int state;
    public int camp;
    public string name;
    public int mvp;
    public int gold_ace;
    public int silver_ace;
    public int immune;      // 免疫次数
    public int actor_type; // 生化类型 0：人类 1：母体 1：精英 2：小兵
    public p_fight_prop prop;
    public p_actor_buff_toc[] buff_list;
    public int praised; // 被赞次数

    // ready to add      
    public int vip_level;
    public int level;
    public int kill;
    public int round_kill;
    public int death;
    public int score;
    public int king_cur_seq;
    public int king_cur_weapon_kill;
    [DefaultValue(1000, false)]
    public int honor;

    public int sacred_weapon_id;
}

class p_fight_prop
{
    public int MaxHP;
    public EptFloat StandMoveSpeed;
    public EptFloat CrouchMoveSpeed;
    public string WeaponId;
    public string[] DispPart;
    public int ClipSize;
    public EptFloat ReloadTime;
    public EptFloat Decelerate;
    public EptFloat DecelerateTime;
    public EptFloat AntiDecelerate;    // 反减速百分比
    public int MaxArmor;
    public int ArmorRecoverWait;
    public int ArmorRecoverSpeed;
    public int HPRecoverWait;
    public int HPRecoverSpeed;
    public EptFloat Impulse;
    public EptFloat ImpulseSpeed;
    public EptFloat HitBackSpeed;      //击退速度
    public EptFloat HitBackTime;       //击退时间
    public EptFloat Weight;
    public EptFloat FireRate;
    public EptFloat ZoomFireRate;

    public EptFloat StandSpreadMax;
    public EptFloat CrouchSpreadMax;
    public EptFloat RecoilPitchMin;
    public EptFloat RecoilPitchMax;
    public EptFloat RecoilYawMin;
    public EptFloat RecoilYawMax;
    public EptFloat JumpHeightAdd;     // 跳跃高度加成（数值）
    public EptFloat PveExtraWeight;    // PVE额外重量
    public EptFloat ZombieExtraWeight; // 生化僵尸额外加成

    public EptInt MaxHitCount;
    public EptInt RecoilCountMax;
    public EptFloat StandSpreadInitial;
    public EptFloat StandSpreadFireAdd;
    public EptFloat CrouchSpreadInitial;
    public EptFloat CrouchSpreadFireAdd;
    public EptFloat SpreadMovementMax;
    public EptFloat SpreadJumpMax;
    public EptFloat SpreadSightsFactor;
    public EptFloat SpreadDispMin;
}

// 战斗状态
class p_fight_state
{
    public int state;       // 
    public int round;
    public int last_win;
    public int final_win;
}

partial class tos_fight_use_skill : tos_base
{
    public int skill_id;
    public long tauntedPlayerId;
    //[DefaultValue("",false)]
    public float[] pos;
    public int[] target_ids;
    public string skill_param = "";
}

partial class toc_fight_use_skill : toc_fight_base
{
    public int skill_id;
    public long tauntedPlayerId;
    public float[] pos;
    public int[] target_ids;
    public string skill_param = "";
}

partial class toc_fight_maul_heavily : toc_base // 重创变异体
{
    public int actor_id;    // 被打的生化人
}

partial class toc_fight_actor_die : toc_base    // actor死亡消息
{
    public int actor_id;    // 死亡者
    public int killer;      // 击杀者
    public int weapon;      // 击杀武器
    public int part;        // 击杀部位
    public int multi_kill;  // 连杀数
    public int stage;       // 武器星阶
    public bool across_wall;//是否穿墙
    public Vector3 killerPos;
    public Vector3 diePos;
    public int revive_time;	//复活时间
    public int atk_type;
    public float timeStamp; // 用于区别同一次死亡发两次
}

partial class toc_fight_state : toc_base
{
    public p_fight_state state;
}

partial class toc_fight_stat_info : toc_base
{
    public string game_type;    // done
    public string win_type;     // done
    public int win_param;       // done
    public int win_round;       // done
    public float game_time;     // done
    public float round_time;    // done
    public int round;           // done   
    public int first_blood;     // done
    public int last_blood;      // done
    public int bomb_holder;     // done
    public float[] bomb_point;  // done
    public stat_info_camp[] camps;  // done
    public stat_info_actor[] spectators;    // done
    public int round_total_time;    // done
}

partial class stat_info_camp
{
    public int camp;
    public int win;                     // done 赢了几回合，回合胜利消息里有
    public int kill;                    // no use
    public int round_kill;              // done 每回合击杀数，算上退出的队友
    public int king_camp_score;         // done 新加了协议, 还未接
    public int king_camp_cur_kill;      // done
    public int king_camp_last_win;      // done
    public stat_info_actor[] players;   // done
    public int arena_score;             // no use
}

partial class stat_info_actor
{
    public int id;
    public string name;
    public int icon;
    public int vip_level;
    public bool alive;
    public int level;                   // no use
    public int kill;
    public int round_kill;
    public int death;
    public int score;
    public int king_cur_seq;
    public int king_cur_weapon_kill;
}

partial class tos_fight_chat : tos_base
{
    public int type;    // 类型 0：所有人(战斗玩家) 1:队伍 2:观战(暂未实现)
    public string msg;  // 消息长度字节数不能超过100
    public int command_id;
    public p_audio_chat audio_chat;
}

partial class toc_fight_chat : toc_base
{
    public long from;    // player_id
    public int type;
    public string msg;
    public int command_id;
    public p_audio_chat audio_chat;
}

// 通用伤害，目前用于手雷
partial class toc_fight_hit : toc_fight_base
{
    public p_hit_info_toc[] hitList;
    public int hitType; // 1、手雷，2、技能
    public int atkIdx;  // >0延迟伤害保存属性的索引
    public int skillId;
}

partial class tos_fight_spectate : tos_base
{
    public long player_id;
}

partial class toc_fight_join_spectator : toc_base
{
    public p_fight_spectator spectator;
}

partial class toc_fight_leave_spectator : toc_base
{
    public long player_id;
}

partial class tos_fight_can_vote_kick : tos_base
{
}

partial class toc_fight_can_vote_kick : toc_base
{
    public bool can_vote;
    public string msg;
}

partial class tos_fight_begin_vote_kick : tos_base
{
    public int kicked_player_id; // 被踢人id actorId
    public string reason;
}

partial class toc_fight_begin_vote_kick : toc_base
{
    public string sponsor_name; // 投票发起人
    public int sponsor_id;  //actorId
    public string kicked_player_name; // 被踢人
    public int kicked_player_id;  //actorId
    public string reason;
}

partial class tos_fight_vote_kick : tos_base
{
    public int pro_or_con; // 1:同意 2:反对
}

partial class toc_fight_vote_kick : toc_base
{
    public int pro; // 同意人数
    public int con; // 反对人数
    public int remainder_sec; // 剩余秒数
}

partial class toc_fight_vote_kick_result : toc_base
{
    public bool is_kicked;
    public string kicked_player_name; // 被踢人
    public int kicked_player_id;  //actorId
    public int pro; // 同意人数
    public int con; // 反对人数
    public string reason;
}

partial class tos_fight_start_rescue : tos_base
{
    public int actor;   // 被救人的actor id
}

partial class toc_fight_start_rescue : toc_base
{
    public int from;    // 救人的actor id
    public int actor;   // 被救人的actor id
}

partial class tos_fight_stop_rescue : tos_base { }

partial class toc_fight_stop_rescue : toc_base
{
    public int actor;
    public int remain_time; // 剩余可营救时间
}

partial class toc_fight_achieve : toc_base
{
    public string[] achieve_list;
    public int from;
    public int to;
}

partial class toc_fight_actor_rescued : toc_base
{
    public int rescuer; // 救治者actor id
    public int actor;   // 被救治者actor id
    public int times;   // 被救治次数
}

partial class tos_fight_survival_use_revivecoin : tos_base
{
    public int actor;
}

partial class toc_fight_survival_use_revivecoin : toc_fight_base
{
    public int actor;
}

partial class toc_fight_survival_revive : toc_base
{
    public int reviver; // 使用复活币的actor id
    public int actor;   // 被使用复活币的actor id
}

partial class toc_fight_survival_need_revivecoin : toc_base
{
}

partial class toc_fight_survival_state : toc_base
{
    public int total_round;   // 总回合数
    public int stage_level;   // 副本难度
    public int revivecoin;    // 复活币
    public int supplypoint;   // 补给点
    public int survival_buff; // 当前最大BuffId
    public int total_box;     // 生化箱总数
    public int remain_box;    // 生化箱剩余
}

partial class tos_fight_survival_expenditure : tos_base
{
    public int type;    // 1:弹药 2:枪械 3:buff
    public int item_id; //
}

partial class tos_fight_survival_abort_expenditure : tos_base
{
    public int type;    // 1:弹药 2:枪械 3:buff
    public int item_id; //
}

partial class toc_fight_survival_expenditure : toc_base
{
    public int type;    // 1:弹药 2:枪械 3:buff
    public int item_id; //
    public int result;  // 1:成功 0：失败
}

// 躲猫猫新增协议
partial class toc_fight_hide_state : toc_base
{
    public int state;   // 0:分配阵营 1:选择物件躲藏 2:人类开始寻找 3:二次变身 4:人类扫荡
}

partial class toc_fight_select_disguise : toc_base
{
    public int[] disguise_list; // 可选物件列表 道具ID数组
}

partial class tos_fight_select_disguise : tos_base
{
    public int index;   // 选中第几个物件
}

partial class tos_fight_praise : tos_base   // 点赞
{
    public long pid;   // 被赞的玩家player_id
}

partial class toc_fight_praise : toc_fight_base
{
    public long pid;
    public int praised; // 被赞次数
}

partial class tos_fight_atk_wrong : tos_base { }

partial class toc_fight_atk_wrong : toc_fight_base
{
    public int life; //血量
}

partial class tos_fight_reelect_disguise : tos_base { }

class p_scene_item_toc
{
    public int id;
    public float[] pos;
    public float[] scale;
}

partial class toc_fight_scene_items : toc_base
{
    public p_scene_item_toc[] list;
}

partial class toc_fight_total_scene_items : toc_base
{
    public int[] list;
}

public class p_result_player
{
    public int death;
    public string name;
    public int mvp;
    public int score;
    public long player_id;
    public int praised_cnt;
    public float game_time;
    public int actor_id;
    public int level;
    public int camp;
    public int silver_ace;
    public int honor;
    public int icon;
    public int gold_ace;
    public int kill;
    public int vip_level;
    [DefaultValue(0, false)]
    public int rescue_times;    // 救人次数
    [DefaultValue(0, false)]
    public int revive_times;    // 复活别人次数
    public int honor2add;       // 排位赛有效
    public ItemInfo[] item_list;
    [DefaultValue(0, false)]
    public int hero_type;
    public int chenghao;//装备的称号
}

public class p_camp_result
{
    public int death;
    public int camp;
    public int kill;
    public int win;
}

public class p_title_result
{
    public string title;
    public int player_id;
}

partial class toc_fight_game_result : toc_base
{
    public int first_blood;
    public p_result_player[] players;
    public bool abnormal_finish;
    public int silver_ace;
    public int gold_ace;
    public float game_time;
    public int last_blood;
    public int win_camp;
    public int mvp;
    public string scene_subtype;
    public string game_type;
    public p_camp_result[] camp_info;
    public string scene_type;
    public string win_type;
    public int game_round;
    [DefaultValue("{}", true)]
    public p_title_result[] titles;
    
}

partial class tos_fight_throw_weapon : tos_base
{
    public Vector3 pos;
}

partial class toc_fight_throw_weapon : toc_fight_base
{
    public int drop_uid;    // 掉落枪的uid
}

// 擂台赛相关协议
public class p_arena_player_info
{
    public int actor_id;
    public int camp;
    public int order;
}
partial class toc_fight_arena_begin_info : toc_base
{
    public p_arena_player_info[] a_info;    // 小回合A队信息
    public p_arena_player_info[] b_info;    // 小回合B队信息
    public int a_index;                     // A队当前出战玩家在a_info中的索引
    public int b_index;                     // B队当前出战玩家在b_info中的索引
    public int time;                        // 小回合时间
}

partial class toc_fight_arena_end_info : toc_base
{
    public int camp;
}

// 擂台赛相关协议 end

partial class tos_fight_res_load_done : tos_base { }

partial class toc_fight_bomb_info : toc_base
{
    public int bomb_holder;
    public float[] bomb_point;
}

partial class toc_fight_score : toc_base
{
    public p_king_score[] list;
}

class p_king_score
{
    public int actor_id;
    public int score;
}

partial class toc_fight_king_camp : toc_base
{
    public p_king_camp_info[] camps;
    public int king_camp_last_win;
}

partial class toc_fight_king_solo : toc_base
{
    public p_king_solo_info[] list;
}

class p_king_solo_info
{
    public int actor_id;
    public int king_cur_seq;
    public int king_cur_weapon_kill;
}

class p_king_camp_info
{
    public int camp;
    public int king_camp_score;
}

partial class tos_fight_weapon_motion : tos_base   // 武器动作，客户端定义
{
    public int weapon_id;
    public int motion;
}

partial class toc_fight_weapon_motion : toc_fight_base
{
    public int weapon_id;
    public int motion;
}

partial class tos_fight_enter_deathbox : tos_base
{
    public int id;
}

partial class tos_fight_game_shop : tos_base
{
    public int shop_id;
}

partial class toc_fight_game_shop : toc_base
{
    public int shop_id;
    public int ret;     // 0:成功 -1:资源不足 -2:重复购买 -3:无需购买 -4:正在购买中(频繁操作)
}

partial class tos_fight_cancel_shop : tos_base { }

partial class toc_fight_defend_state : toc_base
{
    public int revivecoin;
    public int supplypoint;
    public int defend_buff;   // 玩家当前购买到了第几个buff
}

partial class tos_fight_weapon_awaken_skill : tos_base
{
    public int awaken_id;   // 当前武器觉醒属性id
    public float[] pos;
    public int[] target_ids;
}

partial class toc_fight_weapon_awaken_skill : toc_fight_base
{
    public int awaken_id;   // 当前武器觉醒属性id
    public int skill_id;    // 技能id
    public int life;        // 玩家血量，使用技能可能会扣除一定血量
    public float[] pos;
    public int[] target_ids;
}

partial class toc_fight_whether2human : toc_base    // 是否选择变成人类
{
    public int time_limit;  // 变身有效截止时间戳(s)
}

partial class tos_fight_start_zombie2human : tos_base { }   // 选择变人类

partial class toc_fight_start_zombie2human : toc_fight_base { }

partial class toc_fight_select_equip : toc_base
{
    public p_bag[] list;
}

partial class tos_fight_select_equip : tos_base
{
    public int bagid;
}

partial class toc_fight_select_zombie : toc_base
{
    public int[] zombies;
}

partial class tos_fight_select_zombie : tos_base
{
    public int index;
}

partial class toc_fight_actor_life : toc_base
{
    public int id;
    public int life;
}

partial class toc_fight_actor_god_end : toc_base
{
    public int id;
    public int god_end;
}

partial class toc_fight_actor_buff_list : toc_base
{
    public int id;
    public p_actor_buff_toc[] buff_list;
}

partial class toc_fight_actor_pos : toc_base
{
    public int id;
    public float[] pos;
}

partial class toc_fight_actor_fight_level : toc_base  // 生化模式中使用
{
    public int id;
    public int fight_level;
    public bool is_level_up;
}

partial class toc_fight_change_role : toc_base  // 战斗中角色变更
{
    public int id;      // actor
    public int role;    // 角色id
}

partial class toc_fight_actor_immune : toc_base // 剩余免疫次数，生化模式中使用
{
    public int id;
    public int immune;
}

partial class toc_fight_actor_type : toc_base   // 
{
    public int id;
    public int type;    // GameConst actor_type
}

partial class tos_fight_update_actor_type : tos_base
{
    public int actor_type;
}

partial class toc_fight_update_actor_type : toc_base
{
    public int actor_type;
}

partial class toc_fight_actor2big_head_king : toc_base
{
    public int actor;
    public int king_index;  // 变成大头王的序号，从1开始
    public int max_heros;   // 各阵营最多可变多少个英雄
    public int hero_need_king; // 各阵营变英雄所需要的大头王数量
}

partial class toc_fight_final_time : toc_base
{
    public int final_time;  // 终极时刻，以战斗开始时间为基准的毫秒数
}

/* 技能伤害采用 toc_fight_hit 协议 */
partial class toc_fight_ai_action : toc_base     // 用于怪执行AI动作
{
    public int from;    // 技能施放者 actor id
    public int action;  // 动作id
    public Vector3 pos; // 目标位置
}

partial class toc_fight_attack_failed : toc_base    // 攻击失败（生化感染失败）
{
    public int from_id;     // 攻击方
    public int actor_id;    // 被攻击方
}

partial class tos_fight_select_role : tos_base
{
    public int index;  // 位置从1开始
}

partial class toc_fight_select_role : toc_base
{
    public int index;
}

partial class toc_fight_hero_time : toc_base
{
    public int hero_time;   // 英雄时刻，可以变英雄的时间
}

partial class tos_fight_ghost_jump : tos_base
{
    public bool visible;
}
partial class toc_fight_ghost_jump : toc_fight_base
{
    public bool visible;
}

partial class toc_fight_team_hero_level : toc_base
{
    public int actor_id;    // 英雄actor
    public int level;       // 英雄等级
    public int soul;        // 当前魂值
}

partial class tos_fight_rush_done : tos_base
{ }

partial class toc_fight_rush_done : toc_fight_base
{ }

partial class toc_fight_ai_skill : toc_base
{
    public int from;    // 技能执行者
    public int skill;   // 技能ID
    public float[][] targets;  // 目标点({x,y,z,r} r代表半径) 技能目标为实体对象时，该数组为空
}

class p_dota_camp_info
{
    public int camp;
    public int boss_buff_level;
    public int boss_revive_time;
    public int boss_revive_date;
}

partial class toc_fight_dota_camp_info : toc_base
{
    public p_dota_camp_info[] infos;
}

partial class toc_fight_dota_state : toc_base
{
    public int actor_id;
    public int survival_buff;
    public int attack_buff;
    public int supplypoint;
    public int revivecoin;
    [DefaultValue(0, false)]
    public int cure_buff;
}

partial class toc_fight_energy : toc_base
{
    public p_king_score[] list;
}

public class p_round_best
{
    public string title;
    public string name;
    public int score;
}

partial class toc_fight_round_best : toc_base
{
    public p_round_best[] round_bests;
}

partial class tos_fight_player_check_win : tos_base { }

partial class toc_fight_play_animation : toc_base
{
    public int actor_id;
    public string animation;
    public int play_time;
}

partial class toc_fight_game_start_time : toc_base
{
    public int time;    // 战斗开始时间戳
}

partial class toc_fight_role_select_list : toc_base
{
    public int[] list;
}

partial class p_data_record
{
	public float pass_time;
	public string data;
}

partial class toc_fight_record: toc_base
{
	public p_data_record[] scene_data_record;
	public p_data_record[][] broadcast_records;
}

partial class toc_fight_wait_time: toc_base
{
	public int time;
}

partial class toc_fight_select_buff : toc_base
{
    public int[] buff_list;
}

partial class tos_fight_select_buff : tos_base
{
    public int index;
}

partial class tos_fight_reelect_buff : tos_base { }

partial class toc_fight_fixpos: toc_base
{
	public Vector3 pos;
}

partial class tos_fight_throw_cfour : tos_base
{
    public Vector3 pos;
}

partial class toc_fight_throw_cfour : toc_fight_base 
{
    public int drop_uid;//c4
}

partial class p_occupy_info
{
	public int id;
	public int owner;
	public int[] scores;
    public int occupyMember1;
    public int occupyMember2;
}

partial class toc_fight_occupy: toc_base
{
	public int[] resources;
	public p_occupy_info[] occupy_infos;
}

public class p_halo_info
{
	public int actor_id;
	public int halo_id;
	public int camp;
	public Vector3 pos;
	public int end_time;
}

partial class toc_fight_halo_list: toc_base
{
	public p_halo_info[] halo_infos;
}

partial class tos_fight_poweron : tos_base
{
    public float timeStamp;
}

partial class toc_fight_poweron : toc_fight_base
{
    public float timeStamp;
}

partial class tos_fight_poweroff : tos_base
{
    public int firePose;    // 攻击动作index
    public Vector3 fireDir;
    public int index;       // 投掷武器属性保存序号
    public int atkType;    // 投掷武器攻击类型
    public float timeStamp;
}

partial class toc_fight_poweroff : toc_fight_base
{
    public float timeStamp;
    public int firePose;    // 攻击动作index
    public Vector3 fireDir;
    public int index;       // 投掷武器属性保存序号
    public int atkType;    // 投掷武器攻击类型
}

class p_bowhit_info_toc
{
    public int id;
    public Vector3 pos;
    public int bodyPart = 0;
}
partial class tos_fight_bowfire : tos_base
{
    public p_bowhit_info_toc[] hitList;
    public Vector3 fromPos;
    public int index;   // 投掷武器属性序号
    public Vector3 firePos;
}

partial class tos_fight_change_spectator : tos_base
{
    public int actor_id;
}

partial class toc_fight_change_spectator : toc_base
{

}

partial class p_spectator_detail
{
    public int damage;
    public int hit_position;
    public int killed_by_who;   //击杀者的actor_id
    public string damage_name; //被攻击者的名字
    public long player_id; //击杀者的player_id
}

partial class p_spectator_record
{
    public int actor_id; //被攻击者的id
    public p_spectator_detail[] detail_info;
}

partial class toc_fight_spectator_record : toc_base
{
    public p_spectator_record[] list;
}

#endregion


#region joinroom

partial class tos_joinroom_create : tos_base
{
    public int channel;
    public int stage;
}

partial class tos_joinroom_join : tos_base
{
    public int channel;
    public int room;
    public string password;
    public bool spectator;
}

partial class tos_joinroom_quick_join : tos_base
{
    public int channel;
    public string rule;
    public string[] rulelist;
    public int source;  // 0:表示频道内的快速开始 1:表示大厅的快速开始
}

partial class tos_joinroom_accept_invite : tos_base
{
    public int channel;
    public int room;
}

partial class tos_joinroom_follow : tos_base
{
    public long roleid;
    public string password;
    public bool spectator;
}


#endregion


#region room相关

partial class tos_room_leave : tos_base
{
}

partial class tos_room_name : tos_base
{
    public string name;
}

partial class tos_room_password : tos_base
{
    public string password;
}

partial class tos_room_auto_aim : tos_base
{
    public bool auto_aim;
}

partial class tos_room_rule : tos_base
{
    public string rule;
    public int map;
    public int max_player;
    public int level;
    public string[] rule_list;
}

partial class tos_room_side : tos_base
{
    public int side;
}

partial class tos_room_change_spectator : tos_base
{
    public bool spectator;
}

partial class tos_room_ready : tos_base
{
    public bool ready;
}

partial class tos_room_start_game : tos_base
{

}

partial class tos_room_join_game : tos_base
{

}

partial class tos_room_start_match : tos_base
{

}

partial class tos_room_chat : tos_base
{
    public string msg;
}
partial class toc_room_chat_msg : toc_base
{
    public long id;
    public string name;
    public string msg;
}

partial class tos_room_kick_player : tos_base
{
    public long id;
}

partial class tos_room_invite : tos_base
{
    public long id;
}

partial class toc_room_invite : toc_base
{
    public int channel;
    public string type;
    public string subtype;
    public int room;
    public int stage;
    public string name;
    public string rule;
    public string inviter;
}

public class p_player_info
{
    public long id;
    public int pos;
    public string name;
    public int vip_level;
    public int icon;
    public int level;
    public bool fight;
    public bool ready;
    public int mvp;
    public int gold_ace;
    public int silver_ace;
    [DefaultValue(1000, false)]
    public int honor;
    [DefaultValue("{}", true)]
    public p_title_info[] titles;
    [DefaultValue(0, false)]
    public int hero_type;
}

public class p_spectator_info
{
    public long id;
    public string name;
    public int icon;
    public int level;
    public bool fight;
    public bool ready;
}

public class p_round_info
{
    public int id;
    public int times;
}

partial class toc_room_info : toc_base
{
    public int channel;
    public int stage;
    public string name;
    public int map;
    public int id;
    public long leader;
    public string rule;
    public p_player_info[] players;
    public p_spectator_info[] spectators;
    public bool fight;
    public bool password;
    public bool auto_aim;
    public int max_player;
    public int level;
    public p_round_info[] round;
    public int state;
    public int level_limit;
}

partial class tos_room_leader : tos_base
{
    public long id;
}

partial class tos_room_robot : tos_base
{
    public int index;
    public int pos;
}

partial class tos_room_quick_robot : tos_base
{

}

partial class tos_room_kick_robot : tos_base
{
    public long id;
}
partial class toc_room_survival_leader : toc_base
{
    public long id;
    public int level;
}


//---------------------------------------萌萌哒分割线------------------------------------------
class p_room_robot
{
    public int id;
    public string type;
    public string name;
    public int camp;
}

partial class toc_room_leave_room : toc_base
{
    public string reason;   // 目前有：""（主动离开）、"kicked"（被踢）、"finished"（关卡完成）
}

partial class tos_room_join_team : tos_base
{
    public int id;
}

partial class tos_room_create_room : tos_base
{
    public string type;     // pvp、stage
    public int typeid;      // pvp模式下无意义，stage模式下填关卡ID
    public string password = "";
}

partial class toc_room_clientstop : toc_base
{ }

partial class tos_room_cancel_match : tos_base
{

}

partial class toc_room_match_time : toc_base
{
    public float match_time;
    public long id;
    public string name;
}

partial class toc_room_corpsmatch_give_up : toc_base
{

}

partial class toc_room_notice_leave : toc_base
{
}

partial class tos_room_notice_leave : tos_base
{
}

partial class tos_room_level_limit : tos_base
{
    public int level_limit;
}

partial class toc_room_match_punished : toc_base
{
    public class punish_info
    {
        public long player_id;
        public string name;     // 被禁赛的玩家名
        public int release_time;    // 解禁时间戳
    }
    public punish_info[] list;
}

partial class tos_room_match_change_channel : tos_base
{
    public int channel;
}

#endregion


#region corps战队相关

public class p_corps_member_toc
{
    public long id;
    public string name;
    public int icon;
    public int level;
    public int vip_level;
    public int logout_time;     // 上一次下线时间
    public int qualifying_win;  // 排位赛胜利数
    public int member_type;
    public int fighting;        // 成员贡献度
	[DefaultValue(0, false)]
	public int week_fighting;
    public int handle;
	public int sex;
	public int hero_type;
}

public class p_corps_apply_toc
{
    public long id;
    public string name;
    public int icon;
    public int level;
    public int vip_level;
    public int logout_time;
    public int qualifying_win;
    public int handle;
}

public class p_corps_log_toc
{
    public string log_type; // agree_aply; kick; quit; change_member_type,update_notice,change_icon,set_leader,
    public string opr_name; // 操作人名字
    public int opr_type;    // 操作人职位ID
    public string mbr_name; // 被操作人名字，当无被操作人时，该字段等于opr_name直接忽略
    public int log_time;    // 日志记录时间
    public int param;       // 该字段在设置成员职位时生效，正数表示升职，负值表示降级
}

class p_corps_data_toc
{
    public long corps_id;
    public long legion_id;   // 所在军团
    [DefaultValue(1, false)]
    public int level;
    [DefaultValue(0, false)]
    public int tribute;      // 建设值
    public string name;
    public string notice;
    public int create_time;
    public long creator_id;
    public string creator_name;
    public long leader_id;
    public string leader_name;
    public p_corps_member_toc[] member_list;// 成员列表
    public p_corps_apply_toc[] apply_list;  // 申请列表
    public int max_member;  // 战队成员容量
    public int fighting;    // 战斗力
    public int logo_pannel; // 战队旗面
    public int logo_icon;   // 战队标志
    public int logo_color;  // 旗面颜色
    [DefaultValue(1, false)]
    public int enter_level;
    public p_corps_log_toc[] log_list;    // 日志列表
    //public int max_legion_id;       //> 当前最大的军团id
}

partial class tos_corps_create_corps : tos_base
{
    public string corps_name;
    public int pannel;
    public int icon;
    public int color;
    public int money_type;
    //public int legion_id;
}

partial class toc_corps_create_corps : toc_base
{
    public p_corps_data_toc data;
}

partial class tos_corps_search_name : tos_base  // 暂未实现
{
    public string corps_name;
}

partial class tos_corps_get_data : tos_base { }


partial class toc_corps_data : toc_base
{
    public p_corps_data_toc data;
}

partial class tos_corps_get_list : tos_base
{
    public int page;
}

class p_corps_info_toc
{
    public long corps_id;
    public string corps_name;
    public long leader_id;
    public string leader_name;
    public int member_cnt;
    public int level;
    public int max_member;
    public int fighting;    // 战斗力
    [DefaultValue(1, false)]
    public int enter_level;
    //public int legion_id;
	public int logo_pannel;
	public int logo_icon;
	public int logo_color;
}

partial class toc_corps_get_list : toc_base
{
    public int page;    // 当前页
    public int pages;   // 总页数
    public int start;   // 排名起点
    public p_corps_info_toc[] list;
}

partial class tos_corps_apply_enter : tos_base
{
    public long corps_id;
}

partial class toc_corps_apply_enter : toc_base { }

partial class tos_corps_agree_apply : tos_base
{
    public long apply_id;
}

partial class toc_corps_agree_apply : toc_base { }

partial class tos_corps_refuse_apply : tos_base
{
    public long apply_id;
}

partial class toc_corps_refuse_apply : toc_base { }

partial class tos_corps_quit : tos_base { }

partial class toc_corps_quit : toc_base { }

partial class tos_corps_change_icon : tos_base
{
    public int pannel;
    public int icon;
    public int money_type;
}

partial class toc_corps_change_icon : toc_base { }

partial class tos_corps_update_notice : tos_base
{
    public string notice;
}

partial class toc_corps_update_notice : toc_base { }

partial class tos_corps_change_member_type : tos_base
{
    public long member_id;
    public int member_type;
}

partial class toc_corps_change_member_type : toc_base { }

partial class tos_corps_set_leader : tos_base
{
    public long leader_id;
}

partial class toc_corps_set_leader : toc_base { }


partial class tos_corps_kick_member : tos_base
{
    public long member_id;
}

partial class toc_corps_kick_member : toc_base { }

partial class tos_corps_dismiss : tos_base { }

partial class toc_corps_dismiss : toc_base { }

partial class toc_corps_message : toc_base
{
    public string type; // 消息类型，有：kick; enter; quit; dismiss; login; logout;
    public long pid;     // 操作的玩家Id
    public string msg;
}

partial class tos_corps_view_info : tos_base
{
    public long corps_id;
}

partial class toc_corps_view_info : toc_base
{
    public class manager
    {
        public long member_id;
        public int member_type;
        public string member_name;
		public int icon;
		public int level;
		public int sex;
		public int hero_type;
    }
    public long corps_id;
    public string corps_name;
    public int create_time;
    public int member_cnt;
    public int level;
    public int max_member;
    public string notice;
    public int logo_pannel;
    public int logo_icon;
    public int logo_color;
    [DefaultValue(1, false)]
    public int enter_level;
    public string leader_name;
    public manager[] managers;
	public int tribute;
	public int fighting;
}

partial class toc_corps_notify_apply : toc_base     // 战队申请变更
{
    public string type;     // add:增加一个申请  del:删除一个申请
    public p_corps_apply_toc apply;   // 申请者信息
}

partial class toc_corps_notify_members : toc_base    // 战队成员信息变更
{
    public class notify_member
    {
        public string type;     // login:上线 logout:下线 add:增加 del:删除 change_type:改变身份
        public p_corps_member_toc member;   // 成员信息
    }
    public notify_member[] list;
}

partial class toc_corps_notify_infos : toc_base     // 战队基础信息变更
{
    public class notify_info
    {
        public string key;      // 变更属性 fighting, notice, logo_pannel, logo_icon,level,tribute(建设值)
        public string str_val;  // 字符串类型属性值
        public int int_val;     // 整数类型属性值
    }
    public notify_info[] list;
}

partial class toc_corps_notify_logs : toc_base  // 战队日志
{
    public p_corps_log_toc[] list;    // 日志列表
}

partial class tos_corps_donate_tribute : tos_base   // 战队捐献
{
    public int donate_id;       // 捐献ID，CfgCorpsDonate键
}

partial class toc_corps_donate_tribute : toc_base
{
    public int donate_id;
}

partial class tos_corps_upgrade_level : tos_base { }

partial class toc_corps_upgrade_level : toc_base { }

// 战队抽奖（签到）
class p_corps_lotterybox
{
    public int box_id;           // 盒子id
    public int lottery_id;   // 奖品id
    public bool can_use;     // 是否启用
}

partial class tos_corps_getlotterycounts : tos_base  // 获取抽奖次数
{
}

partial class toc_corps_getlotterycounts : toc_base
{
    public int total_counts;
    public int cur_counts;
}

partial class tos_corps_openlottery : tos_base  // 进入抽奖界面
{
}

partial class toc_corps_openlottery : toc_base
{
    public p_corps_lotterybox[] list;
}

partial class tos_corps_uselottery : tos_base  // 抽奖
{
}

partial class toc_corps_uselottery : toc_base
{
    public int result;     // 0:可以抽奖 1:抽奖次数不够 2:战队币不够
    public int box_id; // 不能正常抽奖时返回-1
}


partial class tos_corps_buylotterybox : tos_base  // 购买抽奖盒
{
    public int box_id;
}

partial class toc_corps_buylotterybox : toc_base
{
    public int result;             // 0:购买成功 1:等级不够 2:战队币不够
}

partial class tos_corps_donate_info : tos_base { }  // 战队捐献信息

partial class toc_corps_donate_info : toc_base
{
    public int donated_diamond; // 捐献的钻石数
    public int donated_coin;    // 捐献的金币数
    public int next_diamond_id; // 下一个可捐献的id
}

// 战队商店
class p_corps_mallbox
{
    public int box_id;                  // 盒子id
    public int mall_id;                 // 物品id
    public int item_count;              // 物品数量
    public bool can_use;                // 是否启用
    public int highest_bidder_id;       // 最高价出价玩家ID
    public string highest_bidder_name;  // 最高价出价玩家昵称
    public int highest_bid;             // 最高价
}
partial class tos_corps_enter_mall : tos_base { }  // 进入战队商店

partial class toc_corps_enter_mall : toc_base
{
    public p_corps_mallbox[] list;
    public int next_flush_timestamp;
}

partial class tos_corps_buymallbox : tos_base  // 购买战队商店盒子
{
    public int box_id;
}

partial class toc_corps_buymallbox : toc_base
{
    public int result;             // 0:购买成功 1:等级不够 2:战队币不够
}

partial class tos_corps_mallbid : tos_base  // 战队商店竞价
{
    public int box_id;
    public int mall_id;
    public int bid_price;
}

partial class toc_corps_mallbid : toc_base
{
    public int result;             // 0:竞价成功 1:竞价过低 2:战队币不够 3:竞价失败
}

partial class tos_corps_mission_data : tos_base { }

class p_corps_mission
{
    public int id;
    public int type;        // 类型 1：个人 2：多人
    public int status;
    public int progress;
    public int partake;     // 玩家自己的参与度
    public string finisher;    // 完成者，个人任务生效
}

partial class toc_corps_mission_data : toc_base
{
    public class member_info
    {
        public long id;
        public string name;
        public int icon;
    }
    public p_corps_mission[] mission_list;
    public int activeness;      // 累积的活跃度
    public int[] rewarded_list; // 已经领奖的记录
    public member_info last_star;      // 昨日之星
}

partial class toc_corps_notify_mission : toc_base
{
    public p_corps_mission mission;
    [DefaultValue(0, false)]
    public int activeness;
}

partial class tos_corps_get_activeness_reward : tos_base
{
    public int reward_id;
}

partial class toc_corps_get_activeness_reward : toc_base
{
    public int reward_id;
}

partial class tos_corps_open_instance : tos_base { }    // 预约开启战队副本

partial class toc_corps_open_instance : toc_base { }

partial class tos_corps_start_instance : tos_base { }   // 进入战队副本

partial class tos_corps_set_enter_level : tos_base
{
    public int level;   // -1:关闭申请  0:需要审核  >0:达到level等级的玩家直接加入  
}


partial class tos_corpsmatch_score_info : tos_base
{

}
partial class toc_corpsmatch_score_info : toc_base
{
    public int player_score;    // 个人积分
    public int corps_score;     // 战队积分
    public int remain_rounds;   // 剩余场次
}

partial class tos_corpsmatch_stage : tos_base
{

}

partial class toc_corpsmatch_stage : toc_base
{
    public int stage; // 0: 小组混赛，1：十六强之后比赛
    public int week;  // 周几
}

partial class toc_corpsmatch_group_score : toc_base
{
    public int score; // 每场混赛获得积分
}

partial class tos_corps_recmd_list : tos_base { }

partial class toc_corps_recmd_list : toc_base
{
    public p_corps_info_toc[] list;
}

partial class tos_corps_get_hurt_list : tos_base
{
}

partial class toc_corps_get_hurt_list : toc_base
{
	public class p_hurt_info
	{
		public long player_id;
		public int hurt;
	}
	public p_hurt_info[] list;
}

partial class tos_corps_change_name : tos_base
{
    public int pannel;
    public int icon;
    public int color;
    public string corps_name;
}

partial class toc_corps_change_name : toc_base 
{
    public int pannel;
    public int icon;
    public int color;
    public string corps_name;
}
#endregion

#region 师徒相关协议
partial class tos_master_add_master : tos_base
{
    public long id;   // 师傅ID
}
partial class tos_master_agree_apply : tos_base
{
    public long id;      // 申请者ID
    public bool agree;  // true: 同意，false: 拒绝
}
partial class toc_master_agree_apply : toc_base
{
    public long id;      // 申请者ID
}
// 玩家的师徒信息
partial class tos_master_apprentice_info : tos_base
{
}
public class p_master_base_info_toc
{
    public long id;
    public string name;
    public int sex;
    public int icon;
    public int honor;
    public int vip_level;
    public int level;
    public int handle;
    public int room;
    public int team;
    public int mvp;
    public int gold_ace;
    public int silver_ace;
    public bool fight;

}
partial class toc_master_apprentice_info : toc_base
{
    public p_master_base_info_toc[] masters;
    public p_master_base_info_toc[] apprentices;
    [DefaultValue(0, false)]
    public int kick_master_time;	// 解除师徒关系时间
}
partial class tos_master_kick_master : tos_base
{
    public long id;       // 师傅/徒弟ID
    public bool kick_master;    // true：踢师傅  false: 踢徒弟
    public string reason;
}
partial class toc_master_kick_master : toc_base
{
    public long id;       // 师傅/徒弟ID
}
partial class tos_master_rcmnd_master_list : tos_base { }
partial class toc_master_rcmnd_master_list : toc_base
{
    public p_master_base_info_toc[] recommends;
}
partial class tos_master_apply_master_list : tos_base { }
partial class toc_master_apply_master_list : toc_base
{
    public p_master_base_info_toc[] applies;
}

partial class toc_master_notice : toc_base
{
    public bool login;      //true：上线通知， false：下线通知
    public bool master;     //true： 导师，false：　徒弟
}

public class p_master_welfare_toc
{
    public long player_id;   // 玩家ID
    public int[] welfare_ids;  // 礼包ID
    public int gold;        //金币
}

partial class tos_master_master_welfare : tos_base
{
}
partial class toc_master_master_welfare : toc_base
{
    public p_master_welfare_toc[] welfares;        //礼包ID列表
}
partial class tos_master_get_master_welfare : tos_base
{
    public long player_id;       // 玩家ID
    public int welfare_id;      // 礼包ID
    public bool is_gold;        // true: 金币，false: 礼包
}
partial class toc_master_get_master_welfare : toc_base
{
    public int welfare_id;
    public int gold;
}
partial class toc_master_has_welfare_apply : toc_base
{
    public bool has_master;   // 有导师礼包
    public bool has_apprentice; // 有徒弟礼包
    public bool apply;        // 申请者
}
partial class tos_master_tree : tos_base
{

}
public class master_tree_toc
{
    public long master;              //> 
    public long apprentice;
    public string name;
    public int icon;
    public int sex;
    public int level;
}
partial class toc_master_tree : toc_base
{
    public master_tree_toc[] masters;               // 师傅以及师叔伯
    public master_tree_toc[] mates;                  // 同门师兄
    public master_tree_toc[] apprentice;            // 徒弟
    public master_tree_toc[] second_apprentice;    // 徒孙
}

public class p_master_double_exp_toc
{
    public long id;
    public int rest_num;
}


partial class tos_master_double_exp : tos_base
{

}


partial class toc_master_double_exp : toc_base
{
    public p_master_double_exp_toc[] masters;
}


partial class tos_master_grade : tos_base
{

}

partial class toc_master_grade : toc_base
{
    public int point;//当前段位积分
    public int grade_id;//段位ID 
    public int todayPoint;//今日获得积分
}

partial class tos_master_grade_drawgif : tos_base
{
    public int item_id;//领取礼包ID
}

partial class toc_master_grade_drawgif : toc_base
{
    public int result;//领取成功：0
}

partial class toc_master_grade_get_point : toc_base
{
    public int point;//师徒战斗获得多少积分
}


partial class tos_master_grade_isdraw : tos_base
{
    public int grade_id;//段位ID
}

partial class toc_master_grade_isdraw : toc_base
{
    public int isdraw;//已领取1，未领取0
}


partial class toc_master_grade_has_welfare : toc_base
{

}


// 师徒枪械库
partial class tos_master_put_weapon_to_arsenal : tos_base
{
    public int uid; //武器ID
}

partial class toc_master_put_weapon_to_arsenal : toc_base
{
    public int uid; //武器ID
}

partial class tos_master_borrow_weapon : tos_base
{
    public int id; //武器ID
    public int uid;
}

partial class toc_master_borrow_weapon : toc_base
{
    public int id; //武器ID
    public int uid;
}

public class weapon_attr
{
    public int id;                  // 武器ID
    public int uid;
    public int expire_time;         // 武器过期时间
    public int status;              // 武器状态 0 在库； 1：借出
    public string apprentice_name;  // 借用人
    public bool is_borrowed;        //玩家是否已借用
}
partial class tos_master_arsenal : tos_base
{
    public bool is_master; // 标记位： true: 查询导师武器库； false:查询自己武器库
}

partial class toc_master_arsenal : toc_base
{
    public weapon_attr[] arsenal;
    public bool is_master;// 标记位： true: 查询导师武器库； false:查询自己武器库
}

#endregion

#region legion 军团相关

public class p_legion_member_toc
{
    public long id;
    public int icon;
    public string name;
    public int level;
    public int vip_level;
    public long corps_id;
    public string corps_name;
    public int member_type;         // 战队成员的type
    public int handle;              // 不为0时表示在线
    public int logout_time;         // 上次下线时间
}

public class p_legion_corps_toc
{
    public long corps_id;
    public string name;
    public long leader_id;
    public string leader_name;
    public int leader_icon;
    public int member_cnt;
    public int logo_pannel;
    public int logo_icon;
    public int logo_color;
    public int enter_time;
    public int legion_post; // 该战队队长在军团中的职位，跟战队职位一致
}

public class p_legion_apply_toc
{
    public long corps_id;
    public string name;
    public long leader_id;
    public string leader_name;
    public int member_cnt;
    public int fighting;
    public int logo_pannel;
    public int logo_icon;
    public int logo_color;
}

//log_type            opr_name    opr_post    name                param

//level_up            忽略        忽略        忽略                等级
//agree_apply         操作者名字  操作者职位  加入的军团名        忽略
//quit                忽略        忽略        退出的军团名        忽略
//enlarge_capacity    操作者名字  操作者职位  忽略                扩容后的军团容量
//change_icon         操作者名字  操作者职位  忽略                忽略
//update_notice       操作者名字  操作者职位  忽略                忽略
//kick                操作者名字  操作者职位  被踢的军团名        忽略
//change_post_type    操作者名字  操作者职位  被授予职位的玩家名  职位(负数表示降职)
//set_leader          原军团长名  军团长职位  新军团长名          忽略

public class p_legion_log_toc
{
    public string log_type; // agree_apply; kick; quit; change_corps_post,update_notice,change_icon,set_leader,
    public string opr_name; // 操作人名字
    public int opr_post;    // 操作人职位ID
    public string name;     // 被操作者（战队或成员）名字，当无被操作战队时，该字段为空字符，直接忽略
    public int log_time;    // 日志记录时间
    public int param;       // 该字段在设置成员职位时生效，正数表示升职，负值表示降级
}

class p_legion_data_toc
{
    public long legion_id;
    public string name;
    public int logo_icon;
    public string notice;
    public int create_time;
    public int capacity;    // 战队数量容量
    public long creator_cid;
    public long creator_pid;
    public string creator_name;
    public long leader_cid;
    public string leader_name;
    public p_legion_corps_toc[] corps_list;  // 战队列表
    public p_legion_apply_toc[] apply_list;  // 申请列表
    public int activeness;  // 建设值
    public p_legion_log_toc[] log_list;    // 日志列表
}

partial class tos_legion_create_legion : tos_base
{
    public string legion_name;
    public int icon;
    public int money_type;
}

partial class toc_legion_create_legion : toc_base
{
    public p_legion_data_toc data;
}

partial class tos_legion_search_name : tos_base  // 暂未实现
{
    public string legion_name;
}

partial class tos_legion_get_data : tos_base { }


partial class toc_legion_data : toc_base
{
    public p_legion_data_toc data;
}

partial class tos_legion_get_list : tos_base
{
    public int page;
}

public class p_legion_info_toc
{
    public long legion_id;
    public string name;
    public string leader_name;
    public int member_cnt;    // 这里显示总人数
    public int activeness;    // 战斗力
    public int corps_cnt;     // 军团数
    public int capacity;      // 战队数量容量
}

partial class toc_legion_get_list : toc_base
{
    public int page;    // 当前页
    public int pages;   // 总页数
    public int start;   // 排名起点
    public p_legion_info_toc[] list;
}

partial class tos_legion_get_member_list : tos_base
{
    public int page;
}

partial class toc_legion_get_member_list : toc_base
{
    public int page;    // 当前页
    public int pages;   // 总页数
    public p_legion_member_toc[] list;
}

partial class tos_legion_apply_enter : tos_base
{
    public long legion_id;
}

partial class toc_legion_apply_enter : toc_base { }

partial class tos_legion_agree_apply : tos_base
{
    public long apply_id;
}

partial class toc_legion_agree_apply : toc_base { }

partial class tos_legion_refuse_apply : tos_base
{
    public long apply_id;
}

partial class toc_legion_refuse_apply : toc_base { }

partial class tos_legion_quit : tos_base { }

partial class toc_legion_quit : toc_base { }

partial class tos_legion_change_icon : tos_base
{
    public int icon;
}

partial class toc_legion_change_icon : toc_base { }

partial class tos_legion_update_notice : tos_base
{
    public string notice;
}

partial class toc_legion_update_notice : toc_base { }

partial class tos_legion_change_corps_post : tos_base
{
    public long corps_id;
    public int post_type;
}

partial class toc_legion_change_corps_post : toc_base { }

partial class tos_legion_set_leader : tos_base
{
    public long leader_cid;      // 这个是战队的ID
}

partial class toc_legion_set_leader : toc_base { }


partial class tos_legion_kick_corps : tos_base
{
    public long corps_id;
}

partial class toc_legion_kick_corps : toc_base { }

partial class tos_legion_dismiss : tos_base { }

partial class toc_legion_dismiss : toc_base { }

partial class toc_legion_message : toc_base
{
    public string type; // 消息类型，有：kick; quit; dismiss; refuse_apply; agree_apply, change_post_type; set_leader
    public long pid;     // 操作的玩家Id
    public long cid;     // 被操作战队Id
    public string msg;
}

partial class tos_legion_view_info : tos_base
{
    public long legion_id;
}

class p_legion_manager_toc
{
    public long id;
    public string name;
    public int icon;        //> 头像id
    public int legion_post; //> 1:军团长,2:副军团长,7:普通成员
}

partial class toc_legion_view_info : toc_base
{
    public long legion_id;
    public string name;
    public int logo_icon;
    public string notice;
    public int capacity;    // 战队数量容量
    public int member_cnt;  // 总人数
    public int activeness;  // 建设值
    public p_legion_manager_toc[] managers;  // 管理层 团长和副团长
    public p_legion_corps_toc[] corps_list;  // 战队列表
}

partial class toc_legion_notify_apply : toc_base     // 战队申请变更
{
    public string type;     // add:增加一个申请  del:删除一个申请
    public p_legion_apply_toc apply;   // 申请者信息
}

partial class toc_legion_notify_corps : toc_base    // 战队成员信息变更
{
    public class notify_corps
    {
        public string type;     // add:增加 del:删除 change_post:改变身份
        public p_legion_corps_toc corps;   // 战队信息
    }
    public notify_corps[] list;
}

partial class toc_legion_notify_infos : toc_base     // 战队基础信息变更
{
    public class notify_info
    {
        public string key;      // 变更属性 activeness, notice, icon,capacity
        public string str_val;  // 字符串类型属性值
        public int int_val;     // 整数类型属性值
    }
    public notify_info[] list;
}

partial class toc_legion_notify_logs : toc_base  // 战队日志
{
    public p_legion_log_toc[] list;    // 日志列表
}

partial class tos_legion_enlarge_capacity : tos_base { }

partial class toc_legion_enlarge_capacity : toc_base { }

#endregion

#region corpsmatch 战队联赛
partial class tos_corpsmatch_register : tos_base
{
}


partial class tos_corpsmatch_is_registered : tos_base
{

}

partial class toc_corpsmatch_is_registered : toc_base
{
    public bool is_registered;
}

partial class tos_corpsmatch_register_num : tos_base
{

}

partial class toc_corpsmatch_register_num : toc_base
{
    public int register_num;
}

partial class tos_corpsmatch_timeline : tos_base
{

}



partial class toc_corpsmatch_timeline : toc_base
{

}

partial class tos_corpsmatch_vote : tos_base
{
    public int id;
}

partial class toc_corpsmatch_vote : toc_base
{

}

partial class tos_corpsmatch_vote_result : tos_base
{

}

partial class toc_corpsmatch_vote_result : toc_base
{
    public int[][] votes_num;
    public int[][] corps_num;
}

partial class tos_corpsmatch_matchtable : tos_base
{
}

partial class toc_corpsmatch_matchtable : toc_base
{
    int[] match_num;
    int register_num;
}

partial class tos_corpsmatch_last_result : tos_base
{

}

partial class toc_corpsmatch_last_result : toc_base
{

}

partial class tos_corpsmatch_match_info : tos_base
{

}

partial class toc_corpsmatch_match_info : toc_base
{
    public long my_id;
    public long other_id;
    public int[] mode;
}

partial class tos_corpsmatch_room : tos_base
{

}

partial class toc_corpsmatch_room : toc_base
{
    public int id;
}

public class p_corpsmatch_record
{
    public string name;
    public int win;
    public int day;
    public int icon;
}

partial class tos_corpsmatch_record : tos_base
{

}

partial class toc_corpsmatch_record : toc_base
{
    public p_corpsmatch_record[] records;
}

public class p_corpsmatch_team
{
    public int icon;
    public string name;
    public int pos;
}


public class p_corpsmatch_view
{
    public int day;
    public p_corpsmatch_team[] teams;
}

partial class tos_corpsmatch_last_record : tos_base
{
    public bool is_last;
}

partial class toc_corpsmatch_last_record : toc_base
{
    public p_corpsmatch_view[] records;
}

public class p_corpsmatch_room
{
    public int room_id;
    public string team1;
    public int icon1;
    public string team2;
    public int icon2;
}

partial class tos_corpsmatch_match_list : tos_base
{

}

partial class toc_corpsmatch_match_list : toc_base
{
    public p_corpsmatch_room[] list;
}

public class p_corpsmatch_resultteam
{
    public string name;
    public int icon;
    public int panel;
    public int color;
}

partial class tos_corpsmatch_result : tos_base
{

}

partial class toc_corpsmatch_result : toc_base
{
    public p_corpsmatch_resultteam[] team;
}

partial class toc_corpsmatch_full : toc_base
{

}

partial class tos_corpsmatch_top16 : tos_base
{

}

public class p_corpsmatch_member
{
    public long id;
    public string name;
}

partial class toc_corpsmatch_top16 : toc_base
{
    public p_corpsmatch_member[] members;
}

#endregion


#region legionwar 军团战相关

// 领土信息
public class p_legionwar_territory
{
    public int id;      // 领土id
    public int owner;   // 所属军团
    public int[] legions;  // 各军团占领度
}

// 军团信息
public class p_legionwar_legion_info
{
    public int id;          // 军团id
    public int battle;      // 参战人次
    public int death;       // 死亡人次
    public int win;         // 胜利场次
    public int capture;     // 攻陷领土
    public int cur_fighter; // 当前参战人数？？？
    public int cur_battles; // 进行中的战队
    public int mass_order;  // 集结令位置
    public int mass_order_time; // 集结令设置时间
}

public class p_legionwar_log
{
    public int time;        // 记录时间
    public int type;        // 记录类型  1:xx军团在xx区战胜xx军团 2：xx军团占领xx区  3：xx地图处于交战状态 4：xx担任了xx军团长  5：xx军团在xx区取得胜利
    public string name1;    //玩家名字
    public string name2;
    public int param1;      //军团1 id
    public int param2;      //领土 id
    public int param3;      //军团2 id
}

partial class tos_legionwar_data : tos_base { }
partial class toc_legionwar_data : toc_base
{
    public p_legionwar_legion_info[] legion_list;
    public p_legionwar_territory[] territory_list;
    public p_legionwar_log[] battle_logs;
    public int cur_season;
}

public class p_legionwar_fighter
{
    public long player_id;
    public int legion_id;
    public string name;
    public int level;
    public int icon;
    public string corps_name;
    public int contribution;
    public int position;        // 职位
}

partial class tos_legionwar_fighter_ranks : tos_base
{
    public int page;
}

partial class toc_legionwar_fighter_ranks : toc_base
{
    public int page;    // 当前页
    public int start;   // 起始排名
    public int size;    // 排行榜总大小
    public p_legionwar_fighter[] list;
}

// 设置集结令
partial class tos_legionwar_set_mass_order : tos_base
{
    public int territory_id;
}

partial class toc_legionwar_set_mass_order : toc_base
{
    public int territory_id;
    public int legion_id;
}

partial class tos_legionwar_my_info : tos_base { }

partial class toc_legionwar_my_info : toc_base
{
    public class HistoryPosition
    {
        public int position;
        public int times;
    }
    public int legion_id;
    public int battle;
    public int win;
    public int kill;
    public int death;
    public int contribution;
    public int cur_position;
    public HistoryPosition[] history_position;
    public bool have_reward;
    public int daily_battle;
}

partial class tos_legionwar_get_reward : tos_base { }

partial class toc_legionwar_get_reward : toc_base
{
    public ItemInfo[] item_list;    // TODO 暂定
}

partial class tos_legionwar_my_territory_info : tos_base
{
    public int territory_id;
}

partial class toc_legionwar_my_territory_info : toc_base
{
    public int territory_id;
    public int battle;
    public int win;
    public int kill;
    public int death;
    public int contribution;
}

partial class tos_legionwar_start_match : tos_base
{
    public int territory_id;
}

partial class toc_legionwar_start_match : toc_base
{
    public int territory_id;
}

partial class tos_legionwar_territory_info : tos_base
{
    public int territory_id;
}

// 领土详细信息
public class p_legionwar_officer
{
    public long player_id;
    public string name;
    public int icon;
    public int contribution;
    public int position;        // 职位
}

// 单个领土上各军团的信息
public class p_legionwar_territory_legion
{
    public int legion;      // 军团ID
    public int occupy;      // 占领度
    public p_legionwar_officer[] bestors;     // 最佳士官
    public int battle;      // 参战人次
    public int death;       // 死亡人次
    public int win;         // 胜利场次
}
partial class toc_legionwar_territory_info : toc_base
{
    public int id;      // 领土id
    public int owner;   // 所属军团
    public p_legionwar_territory_legion[] legions;  // 各军团信息
}

// 军团军团列表
partial class tos_legionwar_legion_officers : tos_base
{
    public int legion_id;
}

partial class toc_legionwar_legion_officers : toc_base
{
    public int legion_id;
    public p_legionwar_officer[] officers;      // 军官表
}

// 名人榜
partial class tos_legionwar_eminentors : tos_base { }

public class p_legionwar_eminentor
{
    public long player_id;
    public string name;
    public int icon;
    public int lead_times;      // 当军团长次数
    public string corps_name;
    [DefaultValue("{}", true)]
    public p_hero_card_info[] hero_cards;
}

partial class toc_legionwar_eminentors : toc_base
{
    public p_legionwar_eminentor[] eminentors;  // 名人堂
    [DefaultValue("{}", true)]
    public p_hero_card_info[] hero_cards;
}

public class p_legionwar_corps
{
    public long corps_id;
    public string name;
    public string leader;
    public int member_cnt;
    public int legion_id;
    public int contribution;
}

partial class tos_legionwar_corps_ranks : tos_base
{
    public int page;
}

partial class toc_legionwar_corps_ranks : toc_base
{
    public int page;    // 当前页
    public int start;   // 起始排名
    public int size;    // 排行榜总大小
    public p_legionwar_corps[] list;
}

#endregion

#region family家庭相关

partial class tos_family_add_family: tos_base
{
	public long id;
	public string from_relative;	// 我的名号
	public string to_relative;	// 对方名号
}

partial class toc_family_add_family: toc_base
{
	public long id;
}

partial class tos_family_agree_family: tos_base
{
	public long id;
	public bool agree;
}

partial class toc_family_agree_family: toc_base
{
	public long id;
}

partial class tos_family_del_family: tos_base
{
	public long id;
}

partial class toc_family_del_family: toc_base
{
	public long id;
}

partial class p_family_data
{
	public string relative;
    public p_friend_data data;
	public int value;
}

partial class tos_family_family_list: tos_base
{
}

partial class toc_family_family_list: toc_base
{
	public int max_family_cnt;
	public p_family_data[] family_list;
}

partial class toc_family_update_family : toc_base
{
    public bool del;
    public p_family_data family_data;
}

partial class toc_family_family_apply_info: toc_base
{
	public long id;
	public string from_relative;	// 邀请者名号
	public string to_relative;	// 被邀请者名号
	public p_friend_data data;
}

partial class tos_family_open_family_pos: tos_base
{
	public int pos;
}

partial class toc_family_open_family_pos: toc_base
{
	public int pos;
}

partial class tos_family_add_family_relation : tos_base
{
	public long id;
}
#endregion

#region record 录像相关
partial class p_team_role
{
	public long player_id;
	public int icon;
    public int level;
    public string name;
	public int corps_id;
	public string corps_name;
	public int honor;//段位积分
}

partial class p_team_info
{
	public string name;	// 名称
	public string corps_name;	// 战队名
	public int icon;	// 图标
	public int logo_pannel;
	public int logo_icon;
	public int logo_color;
	public int camp;
    public int win;                 // 赢了几回合
    public int kill;                // 击杀总数
	public p_team_role[] roles;
}

partial class p_record_history
{
	public long id;		// 比赛编号
	public int win_camp;	// 胜利方
	public int schedule;	// 进程
	public string game_type;	// 模式
	public int map;		// 地图
	public int time;	// 时间
	public int watch_times;	//观战次数
	public p_team_info[] teams;	
}

partial class tos_record_history_list: tos_base
{
    public string game_type;	// 战报类型
	public int level;	// 段位
	public int page;	// 第几页
	public bool by_center;
	public string platform;	// 平台
}

partial class toc_record_history_list: toc_base
{
    public string game_type;	// 战报类型
	public int level;	// 段位
	public int page;	// 第几页
	public int total_page;	// 总页数
	public string platform;	// 平台
	public p_record_history[] list;
}

partial class tos_record_watch_record: tos_base
{
	public long id;
	public bool by_center;
}

partial class tos_record_stop_watch: tos_base
{
	public bool by_center;
}

partial class tos_record_start_watch: tos_base
{
	public bool by_center;
}

partial class tos_record_set_speed: tos_base
{
	public float speed;	//播放速度
	public bool by_center;
}

partial class tos_record_send_message: tos_base
{
	public string msg;		//文字内容
	public bool by_center;
}

partial class p_record_message
{
	public float pass_time;	//时间
	public int player_id;	// 玩家id
	public int hero_type;	//英雄卡类型
	public string msg;		//文字内容
}

partial class toc_record_message: toc_base
{
	public p_record_message[] messages;
}

#endregion

#region microblog 朋友圈相关
public class p_microblog_content
{
    public long id;
    public long player_id; 
    public string msg;
    public int praise_cnt;
    public int comment_cnt;
    public int time;
    [DefaultValue(false)]
    public bool is_praised; // 我是否点过赞
}

public class p_microblog_author_info
{
    public long player_id;
    public string name;
    public int level;
    public int icon;
    public string icon_url;
    public int sex;
}

partial class tos_microblog_my_data : tos_base { }

partial class toc_microblog_my_data : toc_base
{
    public p_microblog_content[] blog_list;
    public long[] follow_list;
    public long[] fans_list;
    public long[] black_list;
}

partial class tos_microblog_view_player : tos_base
{
    public long view_pid;
}

partial class toc_microblog_view_player : toc_base
{
    public long player_id;
    public string name;
    public int level;
    public int icon;
    public string icon_url;
    public int fans_cnt;
    public p_microblog_content[] blog_list;
}

partial class tos_microblog_blog_list : tos_base    // TODO
{
    public long player_id;
    public long last_id;
}

partial class toc_microblog_blog_list : toc_base
{
    public p_microblog_content[] blog_list;
}

partial class tos_microblog_feed_list : tos_base    // 请求动态
{
    public long last_id;
    public bool refresh;    // 是否为刷新请求最新动态
}

partial class toc_microblog_feed_list : toc_base    // TODO
{
    public p_microblog_content[] list;
    public p_microblog_author_info[] author_list;
    public int remain;  // 剩余未读数
}

partial class tos_microblog_comment_list : tos_base
{
    public long blog_id;
    public int last_id;
}

public class p_microblog_comment_info
{
    public int id;
    public int time;
    public long pid;
    public string comment;
}

partial class toc_microblog_comment_list : toc_base
{
    public long blog_id;
    public p_microblog_comment_info[] comment_list;
    public p_microblog_author_info[] author_list;
    public int praise_cnt;
    public int comment_cnt;
}

partial class tos_microblog_add_blog : tos_base
{
    public string content;
}

partial class toc_microblog_add_blog : toc_base
{
    public p_microblog_content content;
}

partial class tos_microblog_del_blog : tos_base
{
    public long blog_id;
}

partial class toc_microblog_del_blog : toc_base
{
    public long blog_id;
}

partial class tos_microblog_praise_blog : tos_base
{
    public long blog_id;
}

partial class toc_microblog_praise_blog : toc_base
{
    public long blog_id;
    public int praise_cnt;
}

partial class tos_microblog_cancel_praise : tos_base
{
    public long blog_id;
}

partial class toc_microblog_cancel_praise : toc_base
{
    public long blog_id;
    public int praise_cnt;
}

partial class tos_microblog_comment_blog : tos_base
{
    public long blog_id;
    public string comment;
}

partial class toc_microblog_comment_blog : toc_base
{
    public long blog_id;
    public p_microblog_comment_info comment;
    public int praise_cnt;
    public int comment_cnt;
}

partial class tos_microblog_del_comment : tos_base
{
    public long blog_id;
    public int comment_id;
}

partial class toc_microblog_del_comment : toc_base
{
    public long blog_id;
    public int comment_id;
    public int praise_cnt;
    public int comment_cnt;
}

partial class tos_microblog_add_follow : tos_base   // 关注
{
    public long follow_id;
}

partial class toc_microblog_add_follow : toc_base
{
    public long follow_id;
}

partial class tos_microblog_del_follow : tos_base
{
    public long follow_id;
}

partial class toc_microblog_del_follow : toc_base
{
    public long follow_id;
}

partial class tos_microblog_add_black : tos_base
{
    public long black_id;
}

partial class toc_microblog_add_black : toc_base
{
    public long black_id;
}

partial class tos_microblog_del_black : tos_base
{
    public long black_id;
}

partial class toc_microblog_del_black : toc_base
{
    public long black_id;
}

#endregion
