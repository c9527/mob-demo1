﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;

abstract class tos_base
{
    public uint sid;

    protected static void Assert(bool con, string msg = "empty", object obj = null)
    {
        if (!con)
            throw new Exception("assert failed:" + msg + (obj ?? ""));
    }

    public virtual void Save(BinaryWriter bw)
    {
        throw new NotImplementedException();
    }

    protected static void SaveString(BinaryWriter bw, string str)
    {
        byte[] buf = Encoding.UTF8.GetBytes(str);
        Assert(buf.Length <= short.MaxValue, "string length out of range:", buf.Length);
        bw.Write((ushort)buf.Length);
        bw.Write(buf);
    }

    public virtual string Typ { get { return null; } }
    public virtual string Key { get { return null; } }
    public virtual string Name { get { return null; } }
    public virtual int Id { get { return 0; } }
}
