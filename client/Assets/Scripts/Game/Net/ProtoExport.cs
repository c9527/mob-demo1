﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

static public class ProtoExport
{
    class SPInfo
    {
        public int id;
        public Type toc = null;
        public Type tos = null;

        public string idstr(MPInfo mp)
        {
            int n = (mp.id << 16) | id;
            return string.Format("0x{0:x}", n);
        }
    }

    class MPInfo
    {
        public int id;
        public int mspid = 0;
        public Dictionary<string, SPInfo> sps = new Dictionary<string, SPInfo>();
    }

    static void Assert(bool con, string msg = "empty", object obj = null)
    {
        if (!con)
            throw new Exception("assert failed:" + msg + (obj ?? ""));
    }

    static int msRandIndex = 0;

    static Dictionary<string, MPInfo> GetAllProto()
    {
        int mmpid = 0;
        Dictionary<string, MPInfo> pdic = new Dictionary<string, MPInfo>();
        foreach (Type type in Assembly.GetCallingAssembly().GetTypes())
        {
            if (!type.IsClass || type.IsAbstract)
                continue;
            if (!type.IsSubclassOf(typeof(tos_base)) && !type.IsSubclassOf(typeof(toc_base)))
                continue;
            string name = type.Name;
            int i = name.IndexOf('_');
            Assert(i > 0);
            i++;
            int j = name.IndexOf('_', i);
            Assert(j > 0);
            string mname = name.Substring(i, j - i);
            string sname = name.Substring(j + 1);

            MPInfo mp;
            if (!pdic.TryGetValue(mname, out mp))
            {
                mp = new MPInfo();
                mp.id = ++mmpid;
                pdic.Add(mname, mp);
            }
            SPInfo sp;
            if (!mp.sps.TryGetValue(sname, out sp))
            {
                sp = new SPInfo();
                sp.id = ++mp.mspid;
                mp.sps.Add(sname, sp);
            }

            if (type.IsSubclassOf(typeof(tos_base)))
                sp.tos = type;
            else
                sp.toc = type;
        }
        return pdic;
    }

    public static void Export()
    {
        Dictionary<string, MPInfo> pdic = GetAllProto();
        WriteClientDef(pdic, "Assets/Scripts/Game/Net/ProtoDefAuto.cs");
        WriteServerDef(pdic, "../server/cfg/proto_def.config");
    }

    public static void Clear()
    {
#if UNITY_WEBPLAYER
        Logger.Error("WebPlayer平台不支持File.WriteAllText");
#else
        File.WriteAllText("Assets/Scripts/Game/Net/ProtoDefAuto.cs", "");
#endif
    }

    static void WriteClientDef(Dictionary<string, MPInfo> pdic, string path)
    {
#if UNITY_WEBPLAYER
        Logger.Error("WebPlayer平台不支持File.CreateText");
#else
        StreamWriter sw = File.CreateText(path);

        sw.WriteLine("using System.IO;");
        sw.WriteLine();
        sw.WriteLine("static partial class NetLayer");
        sw.WriteLine("{");
        sw.WriteLine("  static NetLayer()");
        sw.WriteLine("  {");
        foreach (MPInfo mp in pdic.Values)
        {
            foreach (SPInfo sp in mp.sps.Values)
            {
                if (sp.toc == null)
                    continue;
                sw.WriteLine("    mCreater[{0}] = () => new {1}();", sp.idstr(mp), sp.toc.Name);
            }
            sw.WriteLine();
        }
        sw.WriteLine("    Init();");
        sw.WriteLine("  }");
        sw.WriteLine("}");
        sw.WriteLine();

        foreach (string mname in pdic.Keys)
        {
            MPInfo mp = pdic[mname];
            foreach (string sname in mp.sps.Keys)
            {
                SPInfo sp = mp.sps[sname];
                if (sp.toc != null)
                {
                    msRandIndex = 0;
                    sw.WriteLine("partial class {0}", sp.toc.Name);
                    sw.WriteLine("{");
                    sw.WriteLine("  public override string Typ {{ get {{ return \"{0}\"; }} }}", mname);
                    sw.WriteLine("  public override string Key {{ get {{ return \"{0}\"; }} }}", sname);
                    sw.WriteLine("  public override string Name {{ get {{ return \"{0}\"; }} }}", sp.toc.Name);
                    sw.WriteLine("  public override int Id {{ get {{ return {0}; }} }}", sp.idstr(mp));
                    sw.WriteLine();
                    sw.WriteLine("  public override void Load(BinaryReader br)");
                    sw.WriteLine("  {");
                    foreach (FieldInfo fi in sp.toc.GetFields(BindingFlags.Instance | BindingFlags.Public))
                        WriteLoadBlock(sw, fi.FieldType, fi.Name, "    ");
                    sw.WriteLine("  }");
                    sw.WriteLine("}");
                    sw.WriteLine();
                }
                if (sp.tos != null)
                {
                    msRandIndex = 0;
                    sw.WriteLine("partial class {0}", sp.tos.Name);
                    sw.WriteLine("{");
                    sw.WriteLine("  public override string Typ {{ get {{ return \"{0}\"; }} }}", mname);
                    sw.WriteLine("  public override string Key {{ get {{ return \"{0}\"; }} }}", sname);
                    sw.WriteLine("  public override string Name {{ get {{ return \"{0}\"; }} }}", sp.tos.Name);
                    sw.WriteLine("  public override int Id {{ get {{ return {0}; }} }}", sp.idstr(mp));
                    sw.WriteLine();
                    sw.WriteLine("  public override void Save(BinaryWriter bw)");
                    sw.WriteLine("  {");
                    WriteSaveBlock(sw, sp.tos, "this", "    ");
                    sw.WriteLine("  }");
                    sw.WriteLine("}");
                    sw.WriteLine();
                }
            }
            sw.WriteLine();
        }

        sw.Close();
#endif
    }

    static void WriteLoadBlock(StreamWriter sw, Type type, string fieldName, string space)
    {
        Assert(!type.IsEnum, "unsupported enum:", type);

        // 基元类型 or 加密float、int
        if (type.IsPrimitive || type == typeof(EptFloat) || type == typeof(EptInt))
        {
            string funName = null;
            if (type == typeof(bool))
                funName = "br.ReadBoolean";
            else if (type == typeof(byte))
                funName = "br.ReadByte";
            else if (type == typeof(int) || type == typeof(EptInt))
                funName = "br.ReadInt32";
            else if (type == typeof(short))
                funName = "br.ReadInt16";
            else if (type == typeof(uint))
                funName = "br.ReadUInt32";
            else if (type == typeof(long))
                funName = "br.ReadInt64";
            else if (type == typeof(float) || type == typeof(EptFloat))
                funName = "br.ReadSingle";
            else if (type == typeof(double))
                funName = "br.ReadDouble";
            Assert(funName != null, "unsupported PrimitiveType:" + type);
            sw.WriteLine("{0}{1} = {2}();", space, fieldName, funName);
            return;
        }

        Assert(!type.IsInterface, "unsupported Interface:", type);

        // 字符串
        if (type == typeof(string))
        {
            sw.WriteLine("{0}{1} = LoadString(br);", space, fieldName);
            return;
        }

        // CDict存为json
        if (type == typeof(CDict))
        {
            sw.WriteLine("{0}{1} = CJsonHelper.Load(LoadString(br)) as CDict;", space, fieldName);
            return;
        }

        // data
        if (type == typeof(byte[]))
        {
            string lenName = GetRandName("len");
            sw.WriteLine("{0}int {1} = br.ReadUInt16();", space, lenName);
            sw.WriteLine("{0}{1} = br.ReadBytes({2});", space, fieldName, lenName);
            return;
        }

        // 数组
        if (type.IsArray)
        {
            string lenName = GetRandName("len");
            string aryName = GetRandName("ary");
            string idxName = GetRandName("idx");
            string aryType = GetTypeName(type.GetElementType());
            string newType;
            if (type.GetElementType().IsArray)
            {
                int i = aryType.IndexOf('[');
                newType = aryType.Substring(0, i) + string.Format("[{0}]", lenName) + aryType.Substring(i);
            }
            else
            {
                newType = string.Format("{0}[{1}]", aryType, lenName);
            }
            sw.WriteLine("{0}int {1} = br.ReadUInt16();", space, lenName);
            sw.WriteLine("{0}{1}[] {2} = new {3};", space, aryType, aryName, newType);
            sw.WriteLine("{0}for (int {1} = 0; {1} < {2}; {1}++)", space, idxName, lenName);
            sw.WriteLine("{0}{{", space);
            WriteLoadBlock(sw, type.GetElementType(), string.Format("{0}[{1}]", aryName, idxName), space + "  ");
            sw.WriteLine("{0}}}", space);
            sw.WriteLine("{0}{1} = {2};", space, fieldName, aryName);
            return;
        }

        Assert(!type.IsSubclassOf(typeof(System.Collections.IEnumerable)), "unsupported IEnumerable:", type);

        // 结构体or一般自定义类
        string clsName = GetRandName("cls");
        sw.WriteLine("{0}{1} {2} = new {1}();", space, GetTypeName(type), clsName);
        foreach (FieldInfo fi in type.GetFields(BindingFlags.Instance | BindingFlags.Public))
            WriteLoadBlock(sw, fi.FieldType, string.Format("{0}.{1}", clsName, fi.Name), space);
        sw.WriteLine("{0}{1} = {2};", space, fieldName, clsName);
    }

    static void WriteSaveBlock(StreamWriter sw, Type type, string fieldName, string space)
    {
        Assert(!type.IsEnum, "unsupported enum:", type);

        // 基元类型
        if (type.IsPrimitive)
        {
            sw.WriteLine("{0}bw.Write({1});", space, fieldName);
            return;
        }
        
        // 加密float、int
        if (type == typeof(EptFloat) || type == typeof(EptInt))
        {
            sw.WriteLine("{0}bw.Write({1}.val);", space, fieldName);
            return;
        }

        Assert(!type.IsInterface, "unsupported Interface:", type);

        // CDict存为json
        if (type == typeof(CDict))
        {
            sw.WriteLine("{0}SaveString(bw, CJsonHelper.Save({1}));", space, fieldName);
            return;
        }

        // 字符串
        if (type == typeof(string))
        {
            sw.WriteLine("{0}SaveString(bw, {1});", space, fieldName);
            return;
        }

        // 复杂类型，先提取短变量名
        string valName = fieldName;
        if (valName.IndexOfAny(new char[] { '.', '[' }) >= 0)
        {
            valName = GetRandName("ary");
            sw.WriteLine("{0}var {1} = {2};", space, valName, fieldName);
        }

        if (!type.IsValueType && fieldName != "this")
            sw.WriteLine("{0}Assert({1} != null, \"null value: {2}\");", space, valName, fieldName);

        // data
        if (type == typeof(byte[]))
        {
            sw.WriteLine("{0}Assert({1}.Length <= short.MaxValue, \"{2} length out of range:\", {1}.Length);", space, valName, fieldName);
            sw.WriteLine("{0}bw.Write((ushort){1}.Length);", space, valName);
            sw.WriteLine("{0}bw.Write({1}, 0, {1}.Length);", space, valName);
            return;
        }

        // 数组
        if (type.IsArray)
        {
            string idxName = GetRandName("idx");
            sw.WriteLine("{0}Assert({1}.Length <= short.MaxValue, \"{2} length out of range:\", {1}.Length);", space, valName, fieldName);
            sw.WriteLine("{0}bw.Write((ushort){1}.Length);", space, valName);
            sw.WriteLine("{0}for (int {1} = 0; {1} < {2}.Length; {1}++)", space, idxName, valName);
            sw.WriteLine("{0}{{", space);
            WriteSaveBlock(sw, type.GetElementType(), string.Format("{0}[{1}]", valName, idxName), space + "  ");
            sw.WriteLine("{0}}}", space);
            return;
        }

        // 结构体or一般自定义类
        if (type.Namespace != "UnityEngine")
            Assert(type.Namespace == null, "unsupported type:", type);   // 复杂类不支持（如Dictionary、List）

        foreach (FieldInfo fi in type.GetFields(BindingFlags.Instance | BindingFlags.Public))
            WriteSaveBlock(sw, fi.FieldType, string.Format("{0}.{1}", valName, fi.Name), space);
    }

    static string GetRandName(string perfix)
    {
        return perfix + (++msRandIndex);
    }

    static string GetTypeName(Type typ)
    {
        return typ.FullName.Replace('+', '.');
    }

    static void WriteServerDef(Dictionary<string, MPInfo> pdic, string path)
    {
#if UNITY_WEBPLAYER
        Logger.Error("WebPlayer平台不支持File.CreateText");
#else
        StreamWriter sw = File.CreateText(path);

        sw.WriteLine("toc = {");
        foreach (string mname in pdic.Keys)
        {
            MPInfo mp = pdic[mname];
            sw.WriteLine("  {0} = {{", mname);
            foreach (string sname in mp.sps.Keys)
            {
                SPInfo sp = mp.sps[sname];
                if (sp.toc == null)
                    continue;
                sw.WriteLine("    {0} = {{id={1}, name=\"{2}\", def={3}}},", sname, sp.idstr(mp), sp.toc.Name, SaveType(sp.toc, "    "));
            }
            sw.WriteLine("  },");
        }
        sw.WriteLine("}");
        sw.WriteLine();

        sw.WriteLine("tos = {");
        foreach (string mname in pdic.Keys)
        {
            MPInfo mp = pdic[mname];
            bool empty = true;
            foreach (string sname in mp.sps.Keys)
            {
                SPInfo sp = mp.sps[sname];
                if (sp.tos == null)
                    continue;
                empty = false;
                sw.WriteLine("  [{0}] = {{typ=\"{1}\", key=\"{2}\", name=\"{3}\", def={4}}},", sp.idstr(mp), mname, sname, sp.tos.Name, SaveType(sp.tos, "  "));
            }
            if (!empty)
                sw.WriteLine();
        }
        sw.WriteLine("}");
        sw.WriteLine();

        sw.WriteLine("group = {");
        foreach (string mname in pdic.Keys)
        {
            sw.WriteLine("    [0x{0:x}] = \"{1}\",", pdic[mname].id, mname);
            sw.WriteLine("    {0} = 0x{1:x},", mname, pdic[mname].id);
        }
        sw.WriteLine("}");
        sw.WriteLine();

        sw.Close();
#endif
    }

    static string SaveType(Type type, string sp)
    {
        if (type == typeof(byte[]))
            return "\"string\"";

        if (type.IsArray)
            return string.Format("{{array={0}}}", SaveType(type.GetElementType(), sp));

        Assert(!type.IsEnum);

        if (type.IsPrimitive || type == typeof(string))
            return string.Format("\"{0}\"", type.Name.ToLower());
        if (type == typeof(EptFloat))
            return "\"single\"";
        if (type == typeof(EptInt))
            return "\"int32\"";
        if (type == typeof(CDict))
            return "\"json\"";

        //bool istoc = type.IsSubclassOf(typeof(toc_base));

        string nsp = sp + "  ";
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("{");
        foreach (FieldInfo fi in type.GetFields(BindingFlags.Instance | BindingFlags.Public))
        {
            string dv = "nil";
            foreach (Attribute attr in fi.GetCustomAttributes(false))
            {
                if (attr is DefaultValueAttribute)
                    dv = (attr as DefaultValueAttribute).Value;
            }
            sb.AppendFormat("{0}{{\"{1}\", {2}, {3}}},\r\n", nsp, fi.Name, SaveType(fi.FieldType, nsp), dv);
        }
        sb.AppendFormat("{0}}}", sp);
        return sb.ToString();
    }
}
