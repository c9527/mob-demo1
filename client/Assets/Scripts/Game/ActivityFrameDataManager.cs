﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 15:09:47
 * Description: 活动框架数据维护
 * Update History:
 *
 **************************************************************************/

public static class ActivityFrameDataManager
{
    /// <summary>
    /// 未完成
    /// </summary>
    public const int EVENT_STATUS_NO_FINISH = 0;
    /// <summary>
    /// 完成了 可以领取
    /// </summary>
    public const int EVENT_STATUS_CAN_DRAW = 1;
    /// <summary>
    /// 已完成  已领取
    /// </summary>
    public const int EVENT_STATUS_FINISH_DRAW = -1;

    /// <summary>
    /// 老玩家回归活动  玩家数据
    /// </summary>
    public static bool oldPlayerInfo_isOld;
    public static bool oldPlayerInfo_isInvite;
    public static bool newPlayerInfo_isNew;

    public static Dictionary<int, p_player_event> m_EventDic = new Dictionary<int, p_player_event>();
    public const string ACTIVITYFRAME_UPDATE_EVENT = "ACTIVITYFRAME_UPDATE_EVENT";

    public const string EVENTTHEME_PAGETTYPE_EXCHANGE = "Exchange";

    /// <summary>
    /// 在线类型的活动
    /// </summary>
    private static Dictionary<int, int> onLineTimeDic = new Dictionary<int, int>();
    private static float timeTick;

    public static void ActivityFrameDataManagerTestData()
    {
        m_EventDic = new Dictionary<int, p_player_event>();
        toc_player_eventlist data = new toc_player_eventlist();
        data.list = new p_player_event[] { };
        p_player_event temp = new p_player_event();
        temp.id = 1001;
        temp.open = true;
        temp.progress = 1;
        temp.reward = 0;
        temp.total = 1;
        data.list = data.list.ArrayConcat(new[] { temp });
        temp = new p_player_event();
        temp.id = 1007;
        temp.open = true;
        temp.progress = 1;
        temp.reward = 0;
        temp.total = 2;
        data.list = data.list.ArrayConcat(new[] { temp });
        Toc_player_eventlist(data);
    }

    /// <summary>
    /// 礼包领取通用提示
    /// </summary>
    /// <param name="data"></param>
    static void Toc_player_take_event_reward(toc_player_take_event_reward data)
    {
        ConfigEventListLine eventListLine = ConfigManager.GetConfig<ConfigEventList>().GetLine(data.event_id) as ConfigEventListLine;
        ConfigRewardLine rewardLine = ConfigManager.GetConfig<ConfigReward>().GetLine(eventListLine.Reward);
        //弹窗显示礼包信息
        //var panelItemTip = UIManager.PopPanel<PanelRewardItemTip>(null, true, null);
        //panelItemTip.SetRewardItemList(rewardLine.ItemList);
        PanelRewardItemTipsWithEffect.DoShow(rewardLine.ItemList, true);
    }


    static void Toc_player_is_oldplayer(toc_player_is_oldplayer data)
    {
        oldPlayerInfo_isOld = data.is_oldplayer == 1;
        oldPlayerInfo_isInvite = data.is_invited == 1;
    }

    /// <summary>
    /// 活动
    /// </summary>
    /// <param name="data"></param>

    static void Toc_player_eventlist(toc_player_eventlist data)
    {
        int lngth = data.list.Length;
        p_player_event tempEvent;
        ConfigEventListLine cfg;
        bool isDispatchEvent = false;
        int status;
        for (int i = 0; i < lngth; i++)
        {
            tempEvent = data.list[i];
            status = getEventSatus(tempEvent);
            if (m_EventDic.ContainsKey(tempEvent.id))
            {
                m_EventDic[tempEvent.id] = tempEvent;
            }
            else
            {
                m_EventDic.Add(tempEvent.id, tempEvent);
            }
            cfg = ConfigManager.GetConfig<ConfigEventList>().GetLine(tempEvent.id) as ConfigEventListLine;
            if (cfg != null && cfg.Target == "online_time")
            {
                if (status == EVENT_STATUS_NO_FINISH && tempEvent.open)
                {
                    if (onLineTimeDic.ContainsKey(cfg.Id))
                        onLineTimeDic[cfg.Id] = tempEvent.progress * 60;
                    else
                        onLineTimeDic.Add(cfg.Id, tempEvent.progress * 60);
                }
                else
                {//没有开启 或者已经完成  不添加到倒计时列表
                    if (onLineTimeDic.ContainsKey(cfg.Id))
                        onLineTimeDic.Remove(cfg.Id);
                }
            }

            if (i == lngth - 1)
            {
                //在处理最后一个活动列表数据才会派发更新界面处理
                isDispatchEvent = true;
            }
            if (status == EVENT_STATUS_CAN_DRAW)
            {
                BubbleManager.SetBubble(tempEvent.id + "", isNeedBubble(tempEvent.id), isDispatchEvent);
            }
            else
            {
                BubbleManager.SetBubble(tempEvent.id + "", false, isDispatchEvent);
            }
        }
        GameDispatcher.Dispatch(ActivityFrameDataManager.ACTIVITYFRAME_UPDATE_EVENT);
    }

    /// <summary>
    /// 判断是否需要小红点  目前过滤掉兑换类型的活动
    /// </summary>
    public static bool isNeedBubble(int eventId)
    {
        ConfigEventListLine eventListCfg = ConfigManager.GetConfig<ConfigEventList>().GetLine(eventId) as ConfigEventListLine;
        if (eventListCfg == null)
            return false;
        ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(eventListCfg.ThemeId) as ConfigEventThemeLine;
        if (cfg == null)
            return false;
        if (cfg.PageType == EVENTTHEME_PAGETTYPE_EXCHANGE)//兑换类型的活动不设置小红点
            return false;
        if (Array.IndexOf(cfg.EventList, eventId) == -1)
            return false;
        return true;
    }

    public static p_player_event getEventData(int id)
    {
        if (m_EventDic.ContainsKey(id))
        {
            return m_EventDic[id];
        }
        return null;
    }

    /// <summary>
    /// 判断任务的完成状态
    /// </summary>
    /// <param name="tempEvent"></param>
    /// <returns></returns> 0：表示未完成  1：表示完成未领取  -1：表示已完成已领取
    public static int checkEventSatus(int id)
    {
        if (m_EventDic.ContainsKey(id))
        {
            return getEventSatus(m_EventDic[id]);
        }
        return 0;
    }
    /// <summary>
    /// 判断活动数据 该活动是否开启
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static bool isEventOpen(int id)
    {
        bool isOpen = false;
        if (m_EventDic.ContainsKey(id))
        {
            isOpen = m_EventDic[id].open;
        }
        return isOpen;
    }

    /// <summary>
    /// 是否有任务数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static bool isHasEvent(int id)
    {
        bool isOpen = false;
        if (m_EventDic.ContainsKey(id))
        {
            isOpen =true;
        }
        return isOpen;
    }
    /// <summary>
    /// 判断任务的完成状态
    /// </summary>
    /// <param name="tempEvent"></param>
    /// <returns></returns> 0：表示未完成  1：表示完成未领取  -1：表示已完成已领取
    public static int getEventSatus(p_player_event tempEvent)
    {
        int result = 0;
        if (tempEvent == null)
            return result;
        if (!tempEvent.open)
            return result;
        if (tempEvent.progress >= tempEvent.total)
        {
            ConfigEventListLine cfg = ConfigManager.GetConfig<ConfigEventList>().GetLine(tempEvent.id) as ConfigEventListLine;
            if (cfg == null)
            {
                return 0;
            }
            if (cfg.RewardTimes == 0)
            {
                result = 0;
            }
            else if (cfg.RewardTimes > tempEvent.reward)
            {
                result = 1;
            }
            else
            {
                result = -1;
            }
        }
        return result;
    }

    /// <summary>
    /// 判断活动主题是否有小红点
    /// </summary>
    /// <param name="themeId"></param>
    /// <returns></returns>
    public static bool activityThemeHasBubble(int themeId)
    {
        bool isHas = false;
        ConfigEventThemeLine cfg = ConfigManager.GetConfig<ConfigEventTheme>().GetLine(themeId) as ConfigEventThemeLine;
        if (cfg != null)
        {
            if (cfg.EventList != null && cfg.EventList.Length != 0)
            {
                int lngth = cfg.EventList.Length;
                for (int i = 0; i < lngth; i++)
                {
                    if (BubbleManager.HasBubble(cfg.EventList[i] + ""))
                    {
                        isHas = true;
                        break;
                    }
                }
            }
        }
        return isHas;
    }

    /// <summary>
    ///更新时间处理
    /// </summary>
    public static void Update()
    {
        timeTick += Time.deltaTime;
        if (timeTick > 1)
        {
            timeTick = 0;
            int count = onLineTimeDic.Keys.Count;
            if (count == 0) return;
            int[] keyAry = new int[count];
            int sec=0;
            p_player_event pevent;
            onLineTimeDic.Keys.CopyTo(keyAry, 0);
            for (int i = 0; i < count; i++)
            {
                sec=onLineTimeDic[keyAry[i]]+1;
                onLineTimeDic[keyAry[i]] = sec;
                pevent = getEventData(keyAry[i]);
                if (pevent != null)
                {
                    if (pevent.progress < Math.Floor(sec / 60f))
                    {
                        pevent.progress = pevent.progress + 1;
                        if (pevent.progress > pevent.total)
                        {
                            pevent.progress = pevent.total;
                        }
                    }
                    if (pevent.total * 60 < sec)
                    {
                        NetLayer.Send(new tos_player_refresh_online_time());
                        //倒计时时间满
                        onLineTimeDic.Remove(pevent.id);
                    }
                }
            }
        }
    }
}
