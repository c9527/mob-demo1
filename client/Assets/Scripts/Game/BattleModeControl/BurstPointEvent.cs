﻿using UnityEngine;
using System.Collections;

public class BurstPointEvent : MonoBehaviour
{

	// Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        GameObject goEnter = other.gameObject;

        GameDispatcher.Dispatch< GameObject, GameObject >(GameEvent.GAME_OBJ_ENTER_BURST_POINT,gameObject ,goEnter);
        
    }

    void OnTriggerExit(Collider other)
    {
        GameObject goLeaver = other.gameObject;

        GameDispatcher.Dispatch<GameObject, GameObject>(GameEvent.GAME_OBJ_LEAVE_BURST_POINT, gameObject,goLeaver);
        
    }

    void  OnTriggerStay(Collider other)
    {
        GameObject goLeaver = other.gameObject;

        GameDispatcher.Dispatch<GameObject, GameObject>(GameEvent.GAME_OBJ_STAYIN_BURST_POINT, gameObject,goLeaver);
        
    }
}
