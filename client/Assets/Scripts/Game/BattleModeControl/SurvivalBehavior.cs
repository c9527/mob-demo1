﻿using System;
using System.Collections.Generic;
using UnityEngine;

class SurvivalWinType
{
    public const string KILLALL = "killall";
    public const string PICKUP = "pickup_box";
    public const string KILLCNT = "killcnt";
    public const string PICKKILL = "pickkill";
    public const string KILLBOSS = "killboss";
}


class SurvivalBehavior : MonoBehaviour
{
    static public SurvivalBehavior singleton;

    protected GameObject m_sceneGO;

    protected int m_fillBulletShopId;
    protected int m_startBuffShopId;

    protected int m_buyBulletTime;

    protected bool isBuying = false;
    protected SBattleState m_bs;

    protected int m_gameRound;
    protected int m_configRound;
    protected string m_winType;
    protected string m_winParam;
    protected string m_taskTip;

    protected ConfigSurvivalStageLine m_stageConfig;

    protected toc_fight_survival_state data;

    protected PanelBattleSurvival m_panelSurvival;
    protected PanelBattleSurvivalScore m_panelScore;

    protected bool m_reNotice = false;
    protected bool m_rePlayStory = false;

    private UIEffect m_transmitEffect;

    public SurvivalBehavior()
    {
        singleton = this;

        m_fillBulletShopId = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_FILL_BULLET);

        m_buyBulletTime = ConfigManager.GetConfig<ConfigGameShop>().GetLine(m_fillBulletShopId).Param;
        
        m_startBuffShopId = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVPBUFF); 
    }

    virtual protected void Awake()
    {
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);
        GameDispatcher.AddEventListener<int, bool>(GameEvent.GAME_BATTLE_ROUND_UPDATE, UpdateRound);
        NetLayer.AddHandler(this);
    }

    virtual protected void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);
        GameDispatcher.RemoveEventListener<int, bool>(GameEvent.GAME_BATTLE_ROUND_UPDATE, UpdateRound);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);

        if (m_transmitEffect != null)
            m_transmitEffect.Destroy();

        NetLayer.RemoveHandler(this);
    }

    virtual protected void OnSceneLoaded()
    {
        m_sceneGO = SceneManager.singleton.curSceneGo;
        PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
        if (battlePanel != null && battlePanel.IsInited())
            Init();
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    virtual protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        Init();
    }

    virtual protected void Init()
    {
        m_stageConfig = ConfigManager.GetConfig<ConfigSurvivalStage>().GetConfig(WorldManager.singleton.gameRule, SceneManager.singleton.curMapId, stage_level);
        m_panelSurvival = UIManager.PopPanel<PanelBattleSurvival>(null, false, null, LayerType.POP);
        m_panelSurvival.BindBehavior(this);
        if (m_panelScore == null)
            m_panelScore = UIManager.PopPanel<PanelBattleSurvivalScore>();
        m_panelScore.BindBehavior(this);
        if (m_bs != null)
        {
            UpdateRound(m_bs.round, m_rePlayStory);
            OnUpdateBattleState(m_bs);
            InitSceneEffect(m_bs.round);
        }
        else if(m_configRound != 0)
        {
            UpdateRound(m_configRound, m_rePlayStory);
        }

        if(data != null)
        {
            m_panelScore.UpdateInfo(data.revivecoin, data.supplypoint, data.total_box, data.remain_box, data.total_round, data.stage_level, monster);
        }
    }

    protected void GenerateMapBuyPoints(string path, string atlas, string sprite, string type)
    {
        Transform bpp = m_sceneGO.transform.FindChild(path);

        if (bpp != null)
        {
            for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
            {

                GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                if (goChildBp != null)
                {
                    ThroughTheWallProp prop = new ThroughTheWallProp() 
                    { 
                        imgProp = new RectTransformProp() { scale = new Vector3(1.6f, 1.6f, 1) },
                        distance = WorldManager.singleton.gameRuleLine.MapIconVisibleRange <= 0 ? float.MaxValue : WorldManager.singleton.gameRuleLine.MapIconVisibleRange,
                    };
                    UIThroughTheWall.createUI().play(atlas, sprite, goChildBp.transform, new Vector3(0, 1.2f, 0), prop);
                }

                if (goChildBp != null)
                {
                    SphereCollider collider = goChildBp.AddMissingComponent<SphereCollider>();
                    collider.radius = 1;
                    collider.isTrigger = true;
                    goChildBp.AddMissingComponent<PointTriggerEvent>().type = type;
                    goChildBp.TrySetActive(true);

                    ResourceManager.LoadBattlePrefab("Effect/baopodian_1", (go, crc) =>
                    {
                        if (go == null || goChildBp == null) return;
                        go.transform.SetLocation(goChildBp.transform, 0, 0.05f);
                    });
                }
            }
        }
    }

    protected void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            StopBuy();
        }
        else if (WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.Camp != who.Camp)
        {
            if (m_panelScore != null && data != null)
                m_panelScore.UpdateInfo(data.revivecoin, data.supplypoint, data.total_box, data.remain_box, data.total_round, data.stage_level, monster);
        }
    }

    public void OnClickReviveBtn(int playerId)
    {
        if (revivecoin > 0)
        {
            NetLayer.Send(new tos_fight_survival_use_revivecoin() { actor = playerId });
        }
        else
        {
            UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetMallLine(5007) });
        }
    }

    virtual public void OnClickCuteBtn()
    {
        
    }

    public int GetReviveCoinCure(long pid)
    {
        long robotId = pid * -1;
        int result = -1;

        if (robotId < 0)
            return -1;

        ConfigRobotLine robot = ConfigManager.GetConfig<ConfigRobot>().GetLine((int)robotId);

        if (robot == null || (result = robot.ReviveCoinCure) == 0)
            return -1;

        return result;
    }

    public void StartBuy(Transform parent, int playerId)
    {
        if (isBuying) return;
        isBuying = true;
        ControlProcess(parent, true, "弹药补充中...", () => { isBuying = false; });
        NetLayer.Send(new tos_fight_game_shop() { shop_id = m_fillBulletShopId });
    }

    public void StopBuy(bool send = true)
    {
        if (isBuying)
        {
            ControlProcess(null, false);
            if (send) NetLayer.Send(new tos_fight_cancel_shop() { });
            isBuying = false;
        }
    }


    private ProgressBarProp prop;
    private int m_timerId;
    protected void ControlProcess(Transform parent, bool isShow, string msg = "", Action callBack = null)
    {
        if (m_buyBulletTime <= 0)
        {
            if(callBack != null)
                callBack();
        }
        else
        {

            TimerManager.RemoveTimeOut(m_timerId);

            m_timerId = TimerManager.SetTimeOut(0.1f, () =>
            {
                if (UIManager.IsOpen<PanelBattle>())
                {
                    if (!isShow) { UIManager.GetPanel<PanelBattle>().StopProgressBar("Survival"); return; }
                    if (prop == null)
                    {
                        Vector2 pos = new Vector2(0, -115);
                        prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } }, OnTimeOut = callBack };
                    }
                    UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", msg, m_buyBulletTime, prop, "Survival");
                }
            });
        }
    }

    protected void OnDelDropItemInScene(DropItem dropItem)
    {
        if (dropItem.dropID == 19050)
        {
            if (UIManager.IsOpen<PanelBattle>())
            {
                TipsManager.Instance.showTips("获取病毒箱");
                UIManager.GetPanel<PanelBattle>().StopProgressBar("DropItem1023");
            }
        }
    }

    protected virtual void OnUpdateBattleState(SBattleState data)
    {
        if (data == null)
            return;

        if (m_bs == null)
            InitSceneEffect(data.round);

        m_bs = data;

        if (m_bs.state == GameConst.Fight_State_RoundStart)
        {
            if (StoryManager.singleton != null)
                StoryManager.singleton.EndStoryForce();

            ConfigSurvivalRoundLine roundLine = ConfigManager.GetConfig<ConfigSurvivalRound>().GetLine(m_configRound);

            if (roundLine == null)
                return;

            if (SceneManager.singleton != null)
            {
                SceneManager.singleton.ShowSceneEffect(roundLine.RoundStartOpenEffect, true);
            }

            if(roundLine.TransmitPlayer > 0)
            {
                PlayTransmit(roundLine.TransmitPlayer, roundLine.TransmitEffect);
            }
        }
        else if(m_bs.state == GameConst.Fight_State_RoundEnd)
        {
            if(SceneManager.singleton != null)
            {
                ConfigSurvivalRoundLine roundLine = ConfigManager.GetConfig<ConfigSurvivalRound>().GetLine(m_configRound);
                if (roundLine == null)
                {
                    return;
                }
                SceneManager.singleton.ShowSceneEffect(roundLine.RoundEndCloseEffect, false);
            }
        }
        
    }

    protected void InitSceneEffect(int round)
    {
        if (SceneManager.singleton != null || m_configRound > 0)
        {
            ConfigSurvivalRoundLine roundLine = null;

            for (int i = 1; i < round; ++i)
            {
                roundLine = ConfigManager.GetConfig<ConfigSurvivalRound>().GetLine(m_configRound - i);
                if (roundLine == null)
                    continue;

                SceneManager.singleton.ShowSceneEffect(roundLine.RoundStartOpenEffect, true);
                SceneManager.singleton.ShowSceneEffect(roundLine.RoundEndCloseEffect, false);
            }
        }
    }

    protected virtual void UpdateRound(int round, bool playStory = false)
    {
        if (m_panelScore == null || m_stageConfig == null || m_bs == null)
        {
            m_configRound = round;
            m_rePlayStory = playStory;
            return;
        }

        m_configRound = m_stageConfig.RoundList.Length >= round ? m_stageConfig.RoundList[round -1] : -1;
        if (m_configRound == -1)
            return;
        var winParamLine =  ConfigManager.GetConfig<ConfigSurvivalRound>().GetLine(m_configRound);

        if (playStory && winParamLine.StoryID != 0 && m_bs.state != GameConst.Fight_State_RoundStart)
        {
            if (StoryManager.singleton != null)
                StoryManager.singleton.PlayStory(winParamLine.StoryID);
            m_rePlayStory = false;
        }

        m_winParam = winParamLine.WinParam;
        m_winType = winParamLine.WinType;
        m_taskTip = winParamLine.TaskTipsText;
        m_panelScore.UpdateRound(round, m_configRound, m_winType, winParamLine.PlayTime, m_taskTip);
    }

    protected virtual void PlayNoticePanel()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
            if (run_time < 10)
            {
                NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "num_o_second", pos = new Vector2(230, -75) });
                prop.numIconList.Add(new ImgNumProp() { preName = "num_o", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(210, -70) });
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Survival, "next_round_tip", 10 - run_time, new Vector2(0, -70), prop);
            }
        }
    }

    protected virtual void PlayTransmit(float transmitTime, string effect)
    {
        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);

        Logger.Log("PlayTransmit run_time=" + run_time + "  transmitTime=" + transmitTime);

        if (run_time < transmitTime)
        {
            float leftTime = transmitTime - run_time;
            if (UIManager.IsOpen<PanelBattle>())
            {

                if (!string.IsNullOrEmpty(effect))
                {
                    m_transmitEffect = UIEffect.ShowEffect(UIManager.GetPanel<PanelBattle>().transform, effect, leftTime, null, null);
                }

                if (prop == null)
                {
                    Vector2 pos = new Vector2(0, -100);
                    prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, OnTimeOut = TransmitDone, textProp = new TextProp() { fontSize = 18, rect = new RectTransformProp() { size = new Vector2(200, 30) } } };
                }
                UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "传送中", leftTime, prop, "Transmit");
            }
        }
    }

    protected virtual void TransmitDone()
    {
        if (m_transmitEffect != null)
            m_transmitEffect.Destroy();
    }

    protected virtual void PlaySelfNoticePanel(int round = 0, int duration = 6)
    {
        if (data == null)
        {
            m_configRound = round;
            m_reNotice = true;
            return;
        }

        int trueRound = ConfigManager.GetConfig<ConfigSurvivalStage>().GetRoundId(WorldManager.singleton.gameRule, SceneManager.singleton.curMapId, stage_level, round);
        ConfigSurvivalRoundLine line = ConfigManager.GetConfig<ConfigSurvivalRound>().GetLine(trueRound);
        if (line == null) return;
        ConfigSurvivalStageLine stageLine = ConfigManager.GetConfig<ConfigSurvivalStage>().GetConfig(WorldManager.singleton.gameRule, SceneManager.singleton.curMapId, stage_level);
        string pic = line.TipsType;
        if (UIManager.IsOpen<PanelBattle>() && !string.IsNullOrEmpty(pic))
        {
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.Survival, pic, duration, false, new Vector2(0, 50), null, default(Vector3), stageLine.HeadIcon, stageLine.RoleName);
        }
    }

    public int GetAllBuffEffect(int index)
    {
        float all = 0;
        ConfigBuff configBuff = ConfigManager.GetConfig<ConfigBuff>();
        var shopList = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopList(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVPBUFF);

        for (int i = 0; i <= index; i++)
        {
            all += configBuff.GetLine(shopList[i].Param).DamageRate;
        }
        return (int)(all * 100);
    }

    protected int CaculateRemainZombie()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        int killZombie = 0;
        if (!int.TryParse(m_winParam, out killZombie))
            return -1;
        if (data == null) return -1;
        if (killZombie <= 0) return -1;
        int length = data.camps.Count;
        int round_kill = 0;
        for (int i = 0; i < length; ++i)
        {
            if (data.camps[i].camp == (int)WorldManager.CAMP_DEFINE.UNION)
            {
                round_kill = data.camps[i].round_kill;
                break;
            }
        }
        return killZombie - round_kill;
    }

    protected int CaculateMapZombie()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return -1;
        if (data.m_allPlayers == null) return -1;
        int result  = 0;
        foreach (var player in data.m_allPlayers.Values)
        {
            if (player.camp != (int)WorldManager.CAMP_DEFINE.UNION && player.alive)
            {
                ++result;
            }
        }
        return result;
    }

    protected int CaculateRemainBoss()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return -1;
        if (data.m_allPlayers == null) return -1;
        int result = 0;
        foreach (var player in data.m_allPlayers.Values)
        {
            if (player.camp != (int)WorldManager.CAMP_DEFINE.UNION && player.actor_type == GameConst.ACTOR_TYPE_BOSS && player.alive)
            {
                ++result;
            }
        }
        return result;
    }

    virtual protected void ReNotice()
    {
        PlaySelfNoticePanel(m_configRound);
    }

    virtual public int buffShopId
    {
        get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVPBUFF, buffId); }
    }

    virtual public int startBuffShopId
    {
        get { return m_startBuffShopId; }
    }

    virtual public int total_round
    {
        get { return data != null ? data.total_round : 0; }
    }
    virtual public int stage_level
    {
        get { return data != null ? data.stage_level : 0; }
    }
    virtual public int revivecoin
    {
        get { return data != null ? data.revivecoin : 0; }
    }
    virtual public int supplypoint
    {
        get { return data != null ? data.supplypoint : 0; }
    }

    virtual public int buffId
    {
        get { return data != null ? data.survival_buff : 0; }
    }

    virtual public int total_box
    {
        get { return data != null ? data.total_box : 0; }
    }

    virtual public int remain_box
    {
        get { return data != null ? data.remain_box : 0; }
    }

    virtual public int remain_zombie
    {
        get
        {
            return CaculateRemainZombie();
        }
    }

    virtual public int map_zombie
    {
        get
        {
            return CaculateMapZombie();
        }
    }

    virtual public int remain_boss
    {
        get
        {
            return CaculateRemainBoss();
        }
    }

    virtual public int monster
    {
        get
        {
            if(m_winType == SurvivalWinType.PICKKILL)
                return map_zombie;
            else if(m_winType == SurvivalWinType.KILLBOSS)
                return remain_boss;
            else
                return remain_zombie;
        }
    }

    public ConfigSurvivalStageLine stageConfig
    {
        get
        {
            return m_stageConfig;
        }
    }

    protected void Toc_fight_game_shop(toc_fight_game_shop proto)
    {
        int shopId = proto.shop_id;
        var config = ConfigManager.GetConfig<ConfigGameShop>().GetLine(shopId);
        switch (config.Type)
        {
            case GameConst.SHOP_ITEM_TYPE_PVPBUFF:
                if (proto.ret == 0)
                {
                    int allbuffEffect = GetAllBuffEffect(buffShopId - startBuffShopId);
                    if (UIManager.IsOpen<PanelBattle>())
                    {
                        string msg = "+" + allbuffEffect + "%";
                        UIManager.GetPanel<PanelBattle>().ShowBuffIcon(true, m_panelScore.transform, new Vector2(-250, 302), msg);
                    }
                    if (UIManager.IsOpen<PanelBuyBuff>())
                    {
                        UIManager.GetPanel<PanelBuyBuff>().Refresh();
                    }
                    TipsManager.Instance.showTips("升级成功");
                }
                break;
            case GameConst.SHOP_ITEM_TYPE_FILL_BULLET:
                if (proto.ret == 0)
                    break;
                if (proto.ret == -1)
                {
                    TipsManager.Instance.showTips("补给点不足");
                }
                else if (proto.ret == -3)
                {
                    TipsManager.Instance.showTips("弹药已满");
                }
                else
                {
                    TipsManager.Instance.showTips("失败");
                }
                StopBuy(true);
                break;
            case GameConst.SHOP_ITEM_TYPE_WEAPON:
                if (proto.ret == 0) //资源不足 -2:重复购买 -3:无需购买 -4:正在购买中(频繁操作)
                {
                    TipsManager.Instance.showTips("购买成功");
                }
                else if (proto.ret == -1)
                {
                    TipsManager.Instance.showTips("补给点不足");
                }
                else if (proto.ret == -4)
                {
                    TipsManager.Instance.showTips("频繁操作");
                }
                else
                {
                    TipsManager.Instance.showTips("购买失败");
                }
                break;
        }
    }

    virtual protected Vector4 pos { get { return new Vector4(-80, 250, -80, 270); } }
    virtual protected Vector2 scale { get { return new Vector2(0, 0.7f); } }

    protected void Toc_fight_survival_state(toc_fight_survival_state data)
    {
        int supplyPointAdd = data.supplypoint - supplypoint;
        this.data = data;
        if (m_panelScore != null)
        {
            // 注释掉，优化性能
            //if (supplyPointAdd != 0) TipsManager.Instance.showTips("补给点  " + (supplyPointAdd > 0 ? "+" : "") + supplyPointAdd, pos, scale);
            m_panelScore.UpdateInfo(data.revivecoin, data.supplypoint, data.total_box, data.remain_box, data.total_round, data.stage_level, monster);
        }
        if(m_reNotice)
        {
            m_reNotice = false;
            ReNotice();
        }
    }

    protected void Toc_fight_survival_use_revivecoin(toc_fight_survival_use_revivecoin proto)
    {
        if (m_panelSurvival != null)
        {
            m_panelSurvival.OnUseRebornCoinRebornPlayer(proto);
        }
        GameDispatcher.Dispatch<toc_fight_survival_use_revivecoin>(GameEvent.PLAYER_USE_REVIVECOIN, proto);
    }
}