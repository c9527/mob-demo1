﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GhostJumpRecord
{
    /// <summary>
    /// 鬼跳有效触发时间范围
    /// </summary>
    private static float timeRange;
    /// <summary>
    /// 操作间隔
    /// </summary>
    private static float timeInterval;
    private static int times;

    private List<string> m_operationList = new List<string>();
    private int m_invisibleTimer =-1;
    private int m_ghostJumpTimer = -1;
    private int m_returnInvisibleTimer = -1;
    private GhostJumpState m_state = GhostJumpState.Visible;


    public int leftTimes;
    public bool isJumping = false;
    public bool isMoving = false;
    public bool isVisible = true;
    public GhostJumpRecord( )
    {
        this.leftTimes = times;
    }

    public void ResetStatus()
    {
        DelTimer(ref m_invisibleTimer);
        DelTimer(ref m_ghostJumpTimer);
        DelTimer(ref m_returnInvisibleTimer);
        m_state = GhostJumpState.Visible;
        isJumping = false;
        isMoving = false;
        isVisible = true;
    }


    public void GhostJump(string operation)
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.Camp != 2)
            return;
        //if (leftTimes == 0||isJumping)
        //    return;        
        if (m_state == GhostJumpState.Visible)
        {
            if(operation==PlayerStatus.STATUS_STAND&&m_invisibleTimer>0)
                return;
            AddTimer(ref m_invisibleTimer, 2f, () =>
            {
                m_state = GhostJumpState.Invisible;
                m_invisibleTimer = -1;
                GhostJump(PlayerStatus.STATUS_STAND);
            });
        }
        else if(m_state==GhostJumpState.Invisible)
        {
            if(operation==PlayerStatus.STATUS_MOVE||operation==PlayerStatus.STATUS_JUMP||operation==PlayerStatus.STATUS_FIRE)
            {
                m_state = GhostJumpState.Visible;
                if(!isVisible)
                {
                    isVisible = true;
                    NetLayer.Send(new tos_fight_ghost_jump() { visible = isVisible });
                    //Logger.Error("send visible:true");
                }
                GhostJump(PlayerStatus.STATUS_STAND);
            }
            else if(m_ghostJumpTimer<0)
            {
                AddTimer(ref m_ghostJumpTimer, timeRange, () =>
                    {
                        m_state = GhostJumpState.GhostJump;
                        m_ghostJumpTimer = -1;
                        NetLayer.Send(new tos_fight_ghost_jump() { visible = false });
                        //Logger.Error("send visible:false");
                        isVisible = false;
                    });
            }
        }
        else if(m_state==GhostJumpState.GhostJump)
        {
            if(operation==PlayerStatus.STATUS_FIRE)
            {
                DelTimer(ref m_returnInvisibleTimer);
                NetLayer.Send(new tos_fight_ghost_jump() { visible = true });
                m_state = GhostJumpState.Visible;
                //Logger.Error("send visible:true");
            }
            else if(!isJumping &&operation==PlayerStatus.STATUS_MOVE&&m_returnInvisibleTimer<0)
            {
                AddTimer(ref m_returnInvisibleTimer, timeInterval, () =>
                    {
                        m_state = GhostJumpState.Invisible;
                        m_returnInvisibleTimer = -1;
                        if (!isMoving)
                            GhostJump(PlayerStatus.STATUS_STAND);
                    });
            }
            else if(operation==PlayerStatus.STATUS_JUMP&&!isJumping)
            {
                isJumping = true;
                if (m_returnInvisibleTimer > 0)
                    DelTimer(ref m_returnInvisibleTimer);
            }
            else if(operation=="jump_end")
            {
                m_state=GhostJumpState.Invisible;
                isJumping = false;
                GhostJump(PlayerStatus.STATUS_STAND);
            }
        }
    }

    private void AddTimer(ref int timer,float delay,Action callBack)
    {
        DelTimer(ref timer);
        timer= TimerManager.SetTimeOut(delay,callBack, true);
    }

    private void DelTimer(ref int timer)
    {
        if(timer>0)
        {
            TimerManager.RemoveTimeOut(timer);
            timer = -1;
        }
    }
   

    public static void InitConfigData()
    {
        ConfigMiscLine cfgRange = ConfigManager.GetConfig<ConfigMisc>().GetLine("ghost_jump_time_range");
        ConfigMiscLine cfgInterval = ConfigManager.GetConfig<ConfigMisc>().GetLine("ghost_jump_time_interval");
        //string[] val = cfgRange.ValueStr.Split(';');
        timeRange = float.Parse(cfgRange.ValueStr);
        //timeRange[1] = float.Parse(val[1]);
        timeInterval = float.Parse(cfgInterval.ValueStr);
        times = ConfigManager.GetConfig<ConfigMisc>().GetLine("ghost_jump_times").ValueInt;
    }   
}

public enum GhostJumpState
{    
    Visible,
    Invisible,
    GhostJump,
}

