﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
public class BurstBehavior : MonoBehaviour
{
    public bool StartBombCD;

    static string[] ALARM_SOUND_PATH = { "c4_beep1", "c4_beep2", "c4_beep3", "c4_beep4", "c4_beep5" };

    static public BurstBehavior singleton;

    ConfigMapList m_configMapList;

    private GameObject m_sceneGO;

    bool isShowPlaceBombProgressBar = false;
    bool isShowUnPlaceBombProgressBar = false;

    float m_fPlaceBombTime;
    float m_fDelBombTime;
    float m_fBombBurstTime;

    float m_fBombSetTime = -1;
    private bool m_bIsPanelBattleInited;

    GameObject m_goC4Bomb = null;
    Effect c4_effect = null;
    ImageDownWatch m_downWatcher;
    Image m_downWatcherBG;
    UISlideText m_kPlaceBombTxtTip;

    int m_iPlacedPoint = 0;

    public int bomb_placed_point
    {
        get { return m_iPlacedPoint; }
    }
    public float[] bomb_drop_point;
    int m_iBehavior = 0;
    int m_configBombAlarmCount = 0;
    int m_curBombAlarmCount = 0;
    int m_curBombSoundIndex = - 1;
    int useEffect;
    bool m_item;

    private List<Vector3> m_bombPointPosList = new List<Vector3>();
    private float m_to = 0;
    private float m_totalTime;

    public BurstBehavior()
    {
        singleton = this;

        if (m_configMapList == null)
        {
            m_configMapList = ConfigManager.GetConfig<ConfigMapList>();
        }

        useEffect = 0;
        m_item = ItemDataManager.IsDepotHasAndInTime(GameConst.BAOPO_TOOLID);
        if (m_item == true)
        {
            ConfigItemOther itemOtherConfig = ConfigManager.GetConfig<ConfigItemOther>();
            ConfigItemOtherLine itemOtherConfigLine = null;
            if (itemOtherConfig != null)
            {
                itemOtherConfigLine = itemOtherConfig.GetLine(GameConst.BAOPO_TOOLID);
            }
            useEffect = itemOtherConfigLine == null ? 4 : itemOtherConfigLine.UseEffect;
        }
        
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        m_to = useEffect / line.Place_Bomb_Time * 100;
        m_totalTime = line.Place_Bomb_Time;
        m_fPlaceBombTime = useEffect > 0 ? useEffect : line.Place_Bomb_Time - 0.5f;
        m_fDelBombTime = useEffect > 0 ? useEffect : line.Del_Bomb_Time;
        m_fBombBurstTime = line.Bomb_Burst_Time;
    }

    void Awake()
    {
        GameDispatcher.AddEventListener(GameEvent.GAME_EXIT_BATTLE_SCENE, OnBattleSceneDestory);
        GameDispatcher.AddEventListener(GameEvent.GAME_BOMB_POINT, OnSetBombPos);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);

        NetLayer.AddHandler(this);
    }

    void Start()
    {
        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
            return;
        }
    }


    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_EXIT_BATTLE_SCENE, OnBattleSceneDestory);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BOMB_POINT, OnSetBombPos);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);

        NetLayer.RemoveHandler(this);
        CancelInvoke("PlayerBombAlarmSound");

        singleton = null;
    }

    private void OnSceneLoaded()
    {
        m_sceneGO = SceneManager.singleton.curSceneGo;

        OnSetBombPos();

        GenerateMapBurstPoints();

        OnUpdateBattleState(m_bs);
    }

    public List<Vector3> BombPointList
    {
        get
        {
            return m_bombPointPosList;
        }
    }

    void GenerateMapBurstPoints()
    {
        Transform bpp =m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_BOMBPOINT_PATH);
        
        if (bpp != null)
        {
            for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
            {
                GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                Transform tranBombTip = goChildBp.transform.FindChild("BombPointTip");

                ConfigMapListLine mapLine = m_configMapList.GetLine(SceneManager.singleton.curMapId.ToString());
                float radius =  mapLine.BurstDistance != 0.0f ? mapLine.BurstDistance : 1;

                if( tranBombTip != null )
                {
                    m_bombPointPosList.Add(new Vector3(tranBombTip.position.x, tranBombTip.position.z, int.Parse(goChildBp.name)));
                    ThroughTheWallProp prop = new ThroughTheWallProp() 
                    { 
                        imgProp = new RectTransformProp() { scale = new Vector3(2, 2, 1) },
                        isShowDistance = true,
                        distancePos = new Vector2(0, 60),
                        targetRadius = radius,
                        distance = WorldManager.singleton.gameRuleLine.MapIconVisibleRange <= 0 ? float.MaxValue : WorldManager.singleton.gameRuleLine.MapIconVisibleRange,
                    };
                    UIThroughTheWall.createUI().play(AtlasName.Battle, (goChildBp.name == "1" ?  "a" : "b"), tranBombTip, Vector3.zero, prop);
                }
                
                if (mapLine != null)
                {
                    SphereCollider collider = goChildBp.GetComponent<SphereCollider>();
                    if (collider == null)
                    {
                        collider = goChildBp.AddComponent<SphereCollider>();
                        collider.radius = radius;
                    }
                    collider.isTrigger = true;
                    goChildBp.AddMissingComponent<PointTriggerEvent>().type = "Burst";
                    goChildBp.TrySetActive(false);

                    ResourceManager.LoadBattlePrefab("Effect/baopodian_1", (go, crc) =>
                    {
                        if (go == null || goChildBp==null) return;
                        go.transform.SetParent(goChildBp.transform, false);
                    });

                }
            }
        }
    }

    void OnBattleSceneDestory()
    {

    }

    public void PlaceBomb(Transform parent, int iBurstPoint)
    {
        isShowPlaceBombProgressBar = !isShowPlaceBombProgressBar;

        if (isShowPlaceBombProgressBar) ShowProgress(true, m_fPlaceBombTime, PlaceBombTimeOver);
        else ShowProgress(false);

        if (isShowPlaceBombProgressBar)
        {
            Vector3 curPlayerPos = MainPlayer.singleton.position;
            Vector3 forwardDirct = MainPlayer.singleton.transform.forward;

            Vector3 Bombpos = (curPlayerPos + forwardDirct) ;
            //播放放置炸弹动画
            NetLayer.Send(new tos_fight_start_set_bomb() { point = iBurstPoint, x = Bombpos.x, y = Bombpos.y, z = Bombpos.z });
            MainPlayer.singleton.SwitchBomb();

            m_iBehavior = 1;
            Logger.Log("Send tos_fight_start_set_bomb");
        }
        else
        {
            //取消放置炸弹动画
            MainPlayer.singleton.SwitchPreWeapon();
            NetLayer.Send(new tos_fight_stop_set_bomb());
            m_iBehavior = 0;
            isShowPlaceBombProgressBar = false;
            Logger.Log("Send tos_fight_stop_set_bomb");
        }
    }

    public void UnPlaceBomb(Transform parent, int iBurstPoint)
    {
        isShowUnPlaceBombProgressBar = !isShowUnPlaceBombProgressBar;
        if (isShowUnPlaceBombProgressBar) ShowProgress(true, m_fDelBombTime, UnplaceBombTimeOver);
        else ShowProgress(false);

        /// 发送开始拆弹协议
        if (isShowUnPlaceBombProgressBar)
        {
            m_iBehavior = 2;
            NetLayer.Send(new tos_fight_start_defuse_bomb());
        }
        else
        {
            m_iBehavior = 0;
            NetLayer.Send(new tos_fight_stop_defuse_bomb());
            isShowUnPlaceBombProgressBar = false;
        }
    }

    void ForceCanelBombProcess()
    {
        isShowPlaceBombProgressBar = false;
        isShowUnPlaceBombProgressBar = false;

        if (m_iBehavior != 0)
        {
            if (MainPlayer.singleton !=null && MainPlayer.singleton.isBombHolder)
            {
                MainPlayer.singleton.SwitchPreWeapon();
            }
            ShowProgress(false);
        }
        if (m_iBehavior == 1)
        {
            NetLayer.Send(new tos_fight_stop_set_bomb());
        }
        else if (m_iBehavior == 2)
        {
            NetLayer.Send(new tos_fight_stop_defuse_bomb());
        }
        m_iBehavior = 0;
    }

    ProgressBarProp prop = null;
    void ShowProgress(bool isShow, float duration=1f, Action onTimeOut=null)
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            if (!isShow) { UIManager.GetPanel<PanelBattle>().StopProgressBar("Burst"); return; }
            string toolName = "";
            if (prop == null)
            {
                Vector2 pos = Vector2.zero;
                RectTransformProp toolProp = null;
                
                if (useEffect > 0)
                {
                    float x = 278 * 1.0f * (useEffect / m_totalTime);
                    pos = new Vector2(x, 0f);
                    toolProp = new RectTransformProp()
                    {
                        min = UIHelper.ZERO_X_HALF_Y_VEC,
                        max = UIHelper.ZERO_X_HALF_Y_VEC,
                        scale = new Vector3(0.5f,0.5f,0.5f),
                        rotation = new Vector3(0f,0f,270f)
                    };
                    toolName = "gongjuqian";
                }
                prop = new ProgressBarProp() { toolProp = toolProp,to = (m_to > 0 ? m_to : 100), toolPos = pos, OnTimeOut = onTimeOut };
            }
            else
            {
                if (m_item == true)
                {
                    toolName = "gongjuqian";
                }
            }
            UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "burstrocessbk", AtlasName.Battle, "burstprocessfore", "", duration, prop, "Burst", toolName);
        }
    }

    void Update()
    {
        if( m_downWatcher != null)
        {
            if (m_downWatcher.IsFinish)
            {
                StartBombCD = false;
                m_downWatcher.gameObject.TrySetActive(false); 
            }
            else if (m_downWatcherBG!=null)
            {
                var color = m_downWatcherBG.color;
                color.g += (color.r==1 ? -1 : 1) * Time.deltaTime;
                color.b = color.g;
                if (color.g < 0)
                {
                    color.r = 2;
                }
                else if (color.g > 1)
                {
                    color.r = 1;
                }
                m_downWatcherBG.color = color;
            }
        }
    }

    void PlaceBombTimeOver()
    {
        ///放置炸弹对象在地上，同时启动爆炸倒计时
        isShowPlaceBombProgressBar = false;
        m_iBehavior = 0;
        if (UIManager.IsOpen<PanelBattle>()) UIManager.GetPanel<PanelBattle>().ShowPlaceBombBtn(false);
    }

    void UnplaceBombTimeOver()
    {
        isShowUnPlaceBombProgressBar = false;
        m_iBehavior = 0;
    }

    void OnSetBombPos()
    {
        if (m_sceneGO == null)
        {
            return;
        }
        if (m_goC4Bomb == null)
        {
            float[] burstPointInfo = PlayerBattleModel.Instance.battle_info.bomb_point;
            if (burstPointInfo != null && burstPointInfo.Length >= 4)
            {
                int iBurstPoint = (int)burstPointInfo[0];
                if (iBurstPoint > 0) // c4炸弹放置点
                {
                    m_iPlacedPoint = iBurstPoint;
                    Transform bpp = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_BOMBPOINT_PATH);
                    Transform placeTrans = bpp.FindChild(iBurstPoint.ToString());
                    if (placeTrans != null )
                    {
                        ResourceManager.LoadBattlePrefab("Weapons/c4", (go, crc) =>
                        {
                            if (m_sceneGO == null) return;

                            m_goC4Bomb = go;
                            go.transform.position = new Vector3(burstPointInfo[1], burstPointInfo[2], burstPointInfo[3]);
                            go.transform.SetParent(m_sceneGO.transform, true);
                            Vector3 effectPos = new Vector3(0, 0, 0); ;
                            EffectManager.singleton.GetEffect<Effect>(EffectConst.C4_EFFECT, (effect) =>
                            {
                                c4_effect = effect;
                                effect.Attach(effectPos, Vector3.zero, Space.Self, m_goC4Bomb.transform);
                                effect.Play();
                            });
                        });
                    }

                }
            }
        }
    }

    private SBattleState m_bs; 
    void OnUpdateBattleState(SBattleState data)
    {
        m_bs = data;

        if (m_sceneGO == null || m_bs==null)
            return;

        switch (m_bs.state)
        {
            case GameConst.Fight_State_GameInit:
                
                break;
            case GameConst.Fight_State_GameStart:
                ShowBurstPointInMap();
                //ResetDownTimeTips();                
                //BombDestory();                
                //CancelInvoke("PlayerBombAlarmSound");
                break;
            case GameConst.Fight_State_RoundStart:
                ShowBurstPointInMap();
                break;
            case GameConst.Fight_State_RoundEnd:
                ResetDownTimeTips();
                BombDestory();
                CancelInvoke("PlayerBombAlarmSound");
                break;
            case GameConst.Fight_State_GameOver:
                ResetDownTimeTips();
                CancelInvoke("PlayerBombAlarmSound");
                break;
            default:
                break;
        }
    }

    void ShowBurstPointInMap()
    {
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_BURST, GameConst.GAME_RULE_GHOST,GameConst.GAME_RULE_EXPLODE_REVIVE))
        {
            Transform bpp = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_BOMBPOINT_PATH);
            if (bpp != null)
            {
                for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
                {
                    GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                    goChildBp.TrySetActive(true);
                }
            }
        }
    }

    void ResetDownTimeTips()
    {
        StartBombCD = false;
        m_fBombSetTime = -1;
        if (m_downWatcher != null )
        {
            m_downWatcher.gameObject.TrySetActive(false);
        }
    }

    public bool IsC4BombPlaced()
    {
        return m_goC4Bomb != null;
    }

    void Toc_fight_bomb_burst(toc_fight_bomb_burst data)
    {
        if (m_iPlacedPoint != 0 && m_sceneGO != null)
        {
            Transform bpp = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_BOMBPOINT_PATH);
            Transform burstPoint = bpp.FindChild(m_iPlacedPoint.ToString());
            EffectManager.singleton.GetEffect<EffectTimeout>("baozha_da", (kEffect) =>
                {
                    kEffect.Attach(burstPoint);
                    kEffect.Play();

                    AudioManager.PlayFightUISound("c4_explode1");
                }
                );

            BombDestory();
        }
    }

    void Toc_fight_defuse_bomb(toc_fight_defuse_bomb data)
    {
        BombDestory();
        AudioManager.PlayFightUIVoice("bombdef");
        PushSlideText("炸弹已被拆除！");
    }

    void OnPlayerDoAnyThing()
    {
        ForceCanelBombProcess();
    }

    void BombDestory()
    {
        bomb_drop_point = null;
        if (m_goC4Bomb != null)
        {
            GameObject.Destroy(m_goC4Bomb);
            m_goC4Bomb = null;
        }
        if( c4_effect != null )
        {
            EffectManager.singleton.Reclaim(c4_effect);
            c4_effect = null;
        }
    }


    void Toc_fight_start_set_bomb(toc_fight_start_set_bomb data)
    {
        if (WorldManager.singleton.isViewBattleModel || !MainPlayer.singleton.isBombHolder)
        {
            BasePlayer placeBombEnemy = WorldManager.singleton.GetPlayerById(PlayerBattleModel.Instance.bombHolder);
            if (placeBombEnemy != null)
            {
                placeBombEnemy.SwitchBomb();
            }
        }
    }

    void Toc_fight_stop_set_bomb( toc_fight_stop_set_bomb data )
    {
        if (WorldManager.singleton.isViewBattleModel || !MainPlayer.singleton.isBombHolder)
        {
            BasePlayer placeBombEnemy = WorldManager.singleton.GetPlayerById(PlayerBattleModel.Instance.bombHolder);
            if (placeBombEnemy != null)
            {
                placeBombEnemy.SwitchPreWeapon();
            }
        }
    }

    float GetBombSoundPlayTime(  )
    {
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        return line.Bomb_Sound[0];
    }

    int GetBombSoundPlayerCount( int index )
    {
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        return (int)line.Bomb_Sound[ index ];
    }
    /// 炸弹安装好
    void Toc_fight_set_bomb( toc_fight_set_bomb data )
    {
        m_fBombSetTime = data.set_time;
        SetBombTime();
    }

    void SetBombTime()
    {
        m_curBombSoundIndex = 1;
        m_configBombAlarmCount = GetBombSoundPlayerCount(m_curBombSoundIndex);
        m_curBombAlarmCount = 0;
        if (m_downWatcher == null)
        {
            ResourceManager.LoadUI("UI/Common/DownWatch", (GameObject go) =>
            {
                if (gameObject == null || m_downWatcher != null || !m_bIsPanelBattleInited) { return; }
                var timer_trans = go.AddMissingComponent<RectTransform>();
                timer_trans.name = "BurstDownTimer";
                UIManager.GetPanel<PanelBattle>().AddChild(timer_trans, false);
                timer_trans.anchorMin = new Vector2(0.5f, 1f);
                timer_trans.anchorMax = new Vector2(0.5f, 1f);
                timer_trans.anchoredPosition = new Vector2(0, -66);

                m_downWatcherBG = new GameObject("bg").AddComponent<Image>();
                m_downWatcherBG.transform.SetParent(timer_trans);
                m_downWatcherBG.transform.localScale = Vector3.one;
                m_downWatcherBG.transform.localPosition = new Vector3(0, -50, 0);
                m_downWatcherBG.SetSprite(ResourceManager.LoadSprite(AtlasName.Battle, "c4bomb"));
                m_downWatcherBG.SetNativeSize();

                StartBombCD = true;
                m_downWatcher = timer_trans.gameObject.AddMissingComponent<ImageDownWatch>();
                m_downWatcher.setNumStyle(AtlasName.Battle, "time");
                m_downWatcher.gameObject.TrySetActive(true);
                m_downWatcher.SetTime = m_fBombBurstTime - ((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) - m_fBombSetTime / 1000f);

                if (!m_downWatcher.IsFinish)
                {
                    InvokeRepeating("PlayerBombAlarmSound", 0, GetBombSoundPlayTime());
                    AudioManager.PlayFightUIVoice("bombpl");
                    PushSlideText("请注意，炸弹已被放置在" + (m_iPlacedPoint == 1 ? "A" : "B") + "点！");
                }
            });
        }
        else
        {
            StartBombCD = true;
            m_downWatcher.gameObject.TrySetActive(true);
            m_downWatcher.SetTime = m_fBombBurstTime - ((Time.realtimeSinceStartup - WorldManager.singleton.StartTime) - m_fBombSetTime / 1000f);
            m_downWatcherBG.color = Color.white;
            if (!m_downWatcher.IsFinish)
            {
                InvokeRepeating("PlayerBombAlarmSound", 0, GetBombSoundPlayTime());
                AudioManager.PlayFightUIVoice("bombpl");
                PushSlideText("请注意，炸弹已被放置在" + (m_iPlacedPoint == 1 ? "A" : "B") + "点！");
            }
        }
    }


    void PlayerBombAlarmSound( )
    {
        ++m_curBombAlarmCount;

        AudioManager.PlayFightUISound(ALARM_SOUND_PATH[m_curBombSoundIndex - 1]);

        if (m_curBombAlarmCount >= m_configBombAlarmCount )
        {
            ++m_curBombSoundIndex;
            if( m_curBombSoundIndex <= ALARM_SOUND_PATH.Length )
            {
                m_configBombAlarmCount = GetBombSoundPlayerCount(m_curBombSoundIndex);
                m_curBombAlarmCount = 0;
            }
            else
            {
                CancelInvoke("PlayerBombAlarmSound");
            }
        }
    }

    void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart,int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            ForceCanelBombProcess();
        }
    }

    protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        m_bIsPanelBattleInited = true;
        if (m_fBombSetTime != -1 && !StartBombCD)
        {
            SetBombTime();
        }    
    }

    void PushSlideText( string info )
    {
        if (m_sceneGO == null)
            return;
        if( m_kPlaceBombTxtTip == null )
        {
            m_kPlaceBombTxtTip = m_sceneGO.AddMissingComponent<UISlideText>();
            m_kPlaceBombTxtTip.Init( UIManager.GetPanel<PanelBattle>().getRectTransform as Transform );
        }

        m_kPlaceBombTxtTip.PushSlideText(info, UISlideText.SLIDE_DIRC.TOP, 0, 0, new Color(255, 156, 0), 16, 3);
    }
}
    
