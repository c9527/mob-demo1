﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


class SurvivalChallengeBehavior : SurvivalDefendBehavior
{
    override protected Vector4 pos {get{ return new Vector4(-100, 250, -100, 270);}}

    protected override void Init()
    {
        if (m_panelScore == null)
            m_panelScore = UIManager.PopPanel<PanelBattleSurvivalChallengeScore>();

        base.Init();

        for (int i = 0; i < 50; i++ )
        {
            Transform missionPoint = m_sceneGO.transform.FindChild("Config/MissionPoint" + i);
            if(missionPoint != null)
            {
                foreach(Transform child in missionPoint)
                {
                    MainPlayerColliderTrigger mpTrigger = child.gameObject.AddMissingComponent<MainPlayerColliderTrigger>();
                    mpTrigger.Init(child.localEulerAngles.y, MainPlayerEnterMissionPoint, null);
                }
            }
        }
    }

    protected override void PlayNoticePanel()
    {
        //base.PlayNoticePanel();
    }

    private void MainPlayerEnterMissionPoint()
    {
        NetLayer.Send(new tos_fight_player_check_win());
        Logger.Log("send tos_fight_player_check_win");
    }

    static private void Toc_fight_play_animation(toc_fight_play_animation proto)
    {
        Logger.Log("receive Toc_fight_play_animation");

        BasePlayer npc = WorldManager.singleton.GetPlayerById(proto.actor_id);
        if (npc == null || npc.isDead || npc.released)
            return;

        npc.playerAniController.CrossFade(proto.animation);
        npc.playerAniController.SetLooping(true);
        Logger.Log("Toc_fight_play_animation 播放动画 " + proto.animation + " " + proto.play_time);
        TimerManager.SetTimeOut(proto.play_time, () =>
        {
            Logger.Log("Toc_fight_play_animation timeout");
            if(npc != null && npc.alive && npc.released == false)
            {
                npc.playerAniController.SetLooping(false);
                npc.playerAniController.CrossFade(AniConst.stand);
                Logger.Log("Toc_fight_play_animation 停止播放动画");
            }
        });
    }

}
