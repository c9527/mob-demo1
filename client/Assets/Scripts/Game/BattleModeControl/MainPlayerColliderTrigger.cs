﻿using UnityEngine;
using System.Collections;
using System;

public class MainPlayerColliderTrigger : MonoBehaviour {

    private Action m_enterCallBack;
    //private Action m_exitCallBack;
    

    void Awake()
    {
        // 只跟主角有物理交互
        for (int layerValue = 0; layerValue <= 31; layerValue++)
        {
            Physics.IgnoreLayerCollision(GameSetting.LAYER_VALUE_MAINPLAYER_TRIGGER, layerValue, layerValue != GameSetting.LAYER_VALUE_MAIN_PLAYER);
        }
    }

    public void Init(float triggerRadius, Action enterCallBack, Action exitCallBack)
    {
        m_enterCallBack = enterCallBack;
        //m_exitCallBack = exitCallBack;
        SphereCollider collider = gameObject.AddMissingComponent<SphereCollider>();
        collider.isTrigger = true;
        collider.radius = triggerRadius;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(m_enterCallBack != null && collider.gameObject.layer == GameSetting.LAYER_VALUE_MAIN_PLAYER && collider is CharacterController)
        {
            m_enterCallBack();
        }
    }

    //private void OnTriggerExit(Collider collider)
    //{
    //    if (m_exitCallBack != null && collider.gameObject.layer == GameSetting.LAYER_VALUE_MAIN_PLAYER && collider is CharacterController)
    //    {
    //        m_exitCallBack();
    //    }
    //}
	


}
