﻿using UnityEngine;

class SurvivalEradicateBehavior : SurvivalBehavior
{
    public SurvivalEradicateBehavior()
    {

    }

    protected override void Init()
    {
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_GUNPOINT_PATH, AtlasName.Survival, "gun1", "Gun");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BUFFPOINT_PATH, AtlasName.Survival, "buff1", "Buff");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BULLETPOINT_PATH, AtlasName.Battle, "logo_ammo_box", "Bullet");

        base.Init();
    }

    protected override void OnUpdateBattleState(SBattleState data)
    {
        base.OnUpdateBattleState(data);

        //if (WorldManager.singleton.isViewBattleModel) return;
        if (m_sceneGO == null || m_bs == null)
            return;
        switch (m_bs.state)
        {
            //case GameConst.Fight_State_GameInit:
            //    break;
            //case GameConst.Fight_State_GameStart:
            //    break;
            case GameConst.Fight_State_RoundStart:
                    PlaySelfNoticePanel(m_bs.round, 6);
                    PlayNoticePanel();
                break;
            //case GameConst.Fight_State_RoundEnd:
            //    break;
            //case GameConst.Fight_State_GameOver:
            //    break;
            //default:
            //    break;
        }
    }

}