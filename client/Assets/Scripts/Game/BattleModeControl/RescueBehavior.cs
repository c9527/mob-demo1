﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class RescueBehavior : MonoBehaviour
{
    static public RescueBehavior singleton;
    private readonly int RESCUE_NEED_TIME_SEC;

    bool isRescuing = false;
    public bool isRescueRevive = false;

    public List<int> beRescuedList;

    public RescueBehavior()
    {
        singleton = this;
        beRescuedList = new List<int>();
        ConfigGameRule configGameRule = ConfigManager.GetConfig<ConfigGameRule>();
        ConfigGameRuleLine line = configGameRule.GetLine(WorldManager.singleton.gameRule);
        RESCUE_NEED_TIME_SEC = line.RescueNeedTime;
    }

    void Awake()
    {
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        //GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        NetLayer.AddHandler(this);
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        //GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_BE_CONTROLLED, OnPlayerDoAnyThing);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        NetLayer.RemoveHandler(this);
        beRescuedList.Clear();
        beRescuedList = null;
        singleton = null;
    }

    private ProgressBarProp prop;
    void ControlProcess(Transform parent, bool isShow, string msg = "")
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            if (!isShow) { UIManager.GetPanel<PanelBattle>().StopProgressBar("Rescue"); return; }
            if (prop == null)
            {
                Vector2 pos = new Vector2(0, -115);
                prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = new Vector2(0, -85), textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } } };
            }
            UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", msg, RESCUE_NEED_TIME_SEC, prop, "Rescue");
        }
    }

    void Update()
    {
    }

    //void OnPlayerDoAnyThing()
    //{
    //    StopRescue();
    //}

    private void OnPlayerSetState(BasePlayer player)
    {
        if (player != null && player.serverData != null && (player.serverData.state == GameConst.SERVER_ACTOR_STATUS_DEAD || player.serverData.state == GameConst.SERVER_ACTOR_STATUS_LIVE))
        {
            if (beRescuedList.Contains(player.PlayerId)) beRescuedList.Remove(player.PlayerId);
        }
    }

    protected virtual void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            StopRescue();
        }
    } 

    virtual public void StartRescue(Transform parent, int playerId)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(playerId);
        if (player == null) return;
        isRescuing = true;
        string msg = string.Format("<color='{0}'>{1}</color><color='{2}'>{3}</color>", "#ffffff", "正在救助", "#ff8a00", player.PlayerName);
        ControlProcess(parent, true, msg);
        NetLayer.Send(new tos_fight_start_rescue() { actor = playerId });
        //TimerManager.SetTimeOut(0.5f, () => { canStopRescuing = true; });
    }

    public void StopRescue()
    {
        if (isRescuing)// && canStopRescuing)
        {
            ControlProcess(null, false, null);
            NetLayer.Send(new tos_fight_stop_rescue());
            isRescuing = false;
            //canStopRescuing = false;
        }
    }

    void Toc_fight_start_rescue(toc_fight_start_rescue data)
    {
        int from = data.from;
        int to = data.actor;
        beRescuedList.Add(to);
        //Logger.Error("Toc_fight_start_rescue: to: " + to + ", from: " + from);
        int selectedPlayerId = WorldManager.singleton.isViewBattleModel ? ChangeTeamView.watchedPlayerID : MainPlayer.singleton.PlayerId;
        BasePlayer me = WorldManager.singleton.GetPlayerById(selectedPlayerId);
        BasePlayer player = WorldManager.singleton.GetPlayerById(to);
        BasePlayer fromPlayer = WorldManager.singleton.GetPlayerById(from);
        if (me == null || player == null || fromPlayer == null || me.Camp != player.Camp) return;
        string msg = string.Empty;        
        if (me.PlayerId == to)
        {
            msg = string.Format("<color='{0}'>{1}</color><color='{2}'>{3}</color>", "#ff8a00", fromPlayer.PlayerName, "#ffffff", "正在救助你");
            PanelBattle battle = UIManager.GetPanel<PanelBattle>();
            if (battle != null)
            {
                ControlProcess(battle.transform, true, msg);
            }
            DeadPlayer.Instance.ShowDyingDrectionTip(false);
        }
        else
        {
            if (player is OtherPlayer)
                (player as OtherPlayer).ShowDyingDrectionTip(false);
            else
                DeadPlayer.Instance.ShowDyingDrectionTip(false);
        }
    }

    void Toc_fight_stop_rescue(toc_fight_stop_rescue data)
    {
        int to = data.actor;
        int sec = data.remain_time;
        if (beRescuedList.Contains(to)) beRescuedList.Remove(to);
        //Logger.Error("Toc_fight_stop_rescue: to: " + to + ", sec: " + sec);
        int selectedPlayerId = WorldManager.singleton.isViewBattleModel ? ChangeTeamView.watchedPlayerID : MainPlayer.singleton.PlayerId;
        BasePlayer me = WorldManager.singleton.GetPlayerById(selectedPlayerId);
        BasePlayer player = WorldManager.singleton.GetPlayerById(to);
        if (me == null || player == null || me.Camp != player.Camp) return;
        if (me.PlayerId == to)
        {
            PanelBattle battle = UIManager.GetPanel<PanelBattle>();
            if (battle != null)
            {
                ControlProcess(battle.transform, false, null);
            }
            DeadPlayer.Instance.ShowDyingDrectionTip(true, sec);
        }
        else
        {
            if (player is OtherPlayer)
                (player as OtherPlayer).ShowDyingDrectionTip(true, sec);
            else
                DeadPlayer.Instance.ShowDyingDrectionTip(true, sec);
        }
    }

    void Toc_fight_actor_rescued(toc_fight_actor_rescued data)
    {
        isRescueRevive = MainPlayer.singleton != null ? data.actor == MainPlayer.singleton.PlayerId : false;
        GameDispatcher.Dispatch<toc_fight_actor_rescued>(GameEvent.GAME_BATTLE_RESCUE_SOMEONE, data);
    }
}