﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GhostModeBehavior : MonoBehaviour
{
    //private int frame = 0;
    private Dictionary<int, int> timerDic = new Dictionary<int, int>();
    private int GHOST_MODE_DISTANCE = ConfigMisc.GetInt("ghost_invisible_disctance");
    private float m_minDis = 20;//幽灵和人类最小距离
    private int m_count = 0;
    private int m_curEffectType = 1;
    private int m_lastEffectType;
    private Vector3 m_effectPos = new Vector3(0f, -190f, 0f);

    enum EffectType
    {
        Safety = 1,
        Warn = 2,
        Danger =3
    }
    private GhostJumpRecord m_ghostJumpRecord
    {
        get
        {
            if (WorldManager.singleton.curPlayer == null)
                return null;
            return WorldManager.singleton.curPlayer.ghostJumpRecord;
        }
    }    
    void Start()
    {        
        GameDispatcher.AddEventListener<bool>(GameEvent.WEAPON_C4_SWITCH, OnC4Switch);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_ATK, OnPlayerAtk);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_KILL_CURPLAYER, OnPlayerKillCurPlayer);
        GameDispatcher.AddEventListener<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, OnMainPlayerAtk);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_ROUND_START, RoundStart);        
        //各种输入状态
        GameDispatcher.AddEventListener<bool>(GameEvent.INPUT_MOVE, Move);
        GameDispatcher.AddEventListener(GameEvent.INPUT_JUMP, Jump);
        GameDispatcher.AddEventListener(GameEvent.INPUT_STAND, Stand);
        
        //GameDispatcher.AddEventListener(GameEvent.);

        GhostJumpRecord.InitConfigData();
        if(m_ghostJumpRecord!=null)
            m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_STAND);
    }


    private void Stand()
    {
        if(m_ghostJumpRecord!=null)
            m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_STAND);
    }

    private void Jump()
    {
        if (m_ghostJumpRecord != null)
            m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_JUMP);
    }

    private void Move(bool keyDown)
    {
        if (m_ghostJumpRecord == null)
            return;
        m_ghostJumpRecord.isMoving = keyDown;
        if (keyDown)
            m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_MOVE);
        else
            m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_STAND);
    }

    private void RoundStart()
    {
        if (!WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GHOST))
            return;
        BasePlayer[] arr = WorldManager.singleton.allPlayer.Values.ToArray<BasePlayer>();
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i].ghostJumpRecord.ResetStatus();
        }
        //m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_STAND);
        BasePlayer curPlayer = WorldManager.singleton.curPlayer;
        if (curPlayer == null)
            return;
        CancelTimer(curPlayer.PlayerId);
        if (curPlayer.Camp == 2)
        {
            float time = curPlayer.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
            curPlayer.SetVisableDegree(GameConst.GHOST_MODE_ARM_ALPHA, time);
        }
            
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<bool>(GameEvent.WEAPON_C4_SWITCH, OnC4Switch);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_ATK, OnPlayerAtk);
        GameDispatcher.RemoveEventListener<BasePlayer, bool>(GameEvent.MAINPLAYER_ATK, OnMainPlayerAtk);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_KILL_CURPLAYER, OnPlayerKillCurPlayer);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_ROUND_UPDATE, RoundStart);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.INPUT_MOVE, Move);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_JUMP, Jump);
        GameDispatcher.RemoveEventListener(GameEvent.INPUT_STAND, Stand);
        timerDic.Clear();
        timerDic = null;        
    }
    
    void Update()
     {
        //if (frame++>=5)
        //{
        //    frame = 0;
         if (m_ghostJumpRecord != null && m_ghostJumpRecord.isMoving)
             m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_MOVE);
         BasePlayer me = WorldManager.singleton.curPlayer;
         if (me == null)
             return;

         Dictionary<int, BasePlayer> dicAllPlayer = WorldManager.singleton.allPlayer;
         foreach (KeyValuePair<int, BasePlayer> kvp in dicAllPlayer)
         {
             m_count++;
             BasePlayer player = kvp.Value;
             if (player != null && player.alive && !player.isIgnoreTransparent)
             {
                 if (player.Camp == 2)
                 {
                     float time = player.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
                     float alpha = GameConst.GHOST_MODE_BODY_ALPHA;
                     if (player is MainPlayer || player is FPWatchPlayer)
                     {
                         alpha = GameConst.GHOST_MODE_ARM_ALPHA;
                         player.canPlayHeartBeat = true;
                         SetAlphaSetMat(player, alpha, time);
                     }
                     else
                     {
                         if (me.Camp == 2) //自己也是幽灵方
                         {
                             player.canPlayHeartBeat = false;
                             player.SetVisableDegree(alpha, time);
                             continue;
                         }
                         float visibleDisPercent = me.configData.HideDisPercent;
                         float temp = visibleDisPercent == 0 ? 0 : visibleDisPercent;
                         float dis = Vector3.Distance(me.position, player.position);
                         float distance = GHOST_MODE_DISTANCE * 1.0f;
                         distance = distance + (distance * temp);
                         if (dis > distance)
                         {
                             player.canPlayHeartBeat = false;
                             player.SetVisableDegree(0f, time);
                         }
                         else
                         {
                             player.canPlayHeartBeat = true;
                             SetAlphaSetMat(player, alpha, time);
                         }
                     }
                 }
                 else
                 {
                     if (me.Camp == 2 && me.alive == true ) //自己是幽灵方
                     {
                         if( MainPlayer.singleton == null && ( FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching == false))
                         {
                             resetEffect();
                             return;
                         }
                         float dis = Vector3.Distance(me.position, player.position);
                         if (dis < m_minDis)
                         {
                             m_minDis = dis;
                         }
                         if (m_count >= dicAllPlayer.Count)
                         {
                             disHadPlayer(m_minDis, me.isVisible);
                             m_count = 0;
                             m_minDis = 20;
                         }
                     }
                 }
             }
         }
        //}
    }
    
    private void SetAlphaSetMat(BasePlayer player, float alpha,float time)
    {
        if (!player.ghostJumpRecord.isVisible)
        {
            player.SetVisableDegree(0f, time);
            return;
        }
        if (player.visableDegree > 0f && player.visableDegree != alpha)
        {
            player.SetVisableDegree(alpha, time);
        }
        if (player.status.move || player.status.jump)
        //if (player.justMoveNotJump)
        {
            if(m_ghostJumpRecord!=null && m_ghostJumpRecord.isVisible)
            {
                CancelTimer(player.PlayerId);
                player.SetVisableDegree(alpha, time);
            }
            else
            {
                player.SetVisableDegree(0f, time);                
            }
        }
        else if (!player.status.move)
        {
            if (!timerDic.ContainsKey(player.PlayerId))
                timerDic.Add(player.PlayerId, 0);
            int timer = timerDic[player.PlayerId];
            if (timer <= 0)
            {
                timer = TimerManager.SetTimeOut(GameConst.GHOST_MODE_STAND_TIME, () => { player.SetVisableDegree(0f, time); });
                timerDic[player.PlayerId] = timer;               
            }     
        }
    }

    private UIEffect effect1 = null;
    private UIEffect effect2 = null;
    private UIEffect effect3 = null;
    private void disHadPlayer(float dis, bool visableDegree)
    {
        if (dis >= 20)
        {
            m_curEffectType = (int)EffectType.Safety;
            createEffect(effect1, EffectConst.UI_TANCEQI_LV);
            if (effect1 != null && effect1.m_hide)
            {
                effect1.Play();
            }
            
            if (effect2 != null)
            {
                effect2.Hide();
            }
            if (effect3 != null)
            {
                effect3.Hide();
            }
        }
        else
        {
            if (visableDegree)
            {
                //没隐身
                m_curEffectType = (int)EffectType.Danger;
                createEffect(effect3, EffectConst.UI_TANCEQI_HONG);
                if (effect3 != null && effect3.m_hide)
                {
                    effect3.Play();
                }
                if (effect1 != null)
                {
                    effect1.Hide();
                }
                if (effect2 != null)
                {
                    effect2.Hide();
                }
            }
            else
            {
                //隐身
                m_curEffectType = (int)EffectType.Warn;
                createEffect(effect2, EffectConst.UI_TANCEQI_HUANG);
                if (effect2 != null && effect2.m_hide)
                {
                    effect2.Play();
                }
                if (effect1 != null)
                {
                    effect1.Hide();
                }
                if (effect3 != null)
                {
                    effect3.Hide();
                }
            }
        }
    }

    private void createEffect(UIEffect effect, string effectName)
    {
        if (m_curEffectType != m_lastEffectType)
        {
            m_lastEffectType = m_curEffectType;
            var panel = UIManager.GetPanel<PanelBattle>();
            if (panel != null && effect == null)
            {
                if (effectName == EffectConst.UI_TANCEQI_LV)
                {
                    effect1 = effect1 == null ? UIEffect.ShowEffect(panel.transform, effectName, m_effectPos) : effect1;
                }
                else if (effectName == EffectConst.UI_TANCEQI_HUANG)
                {
                    effect2 = effect2 == null ? UIEffect.ShowEffect(panel.transform, effectName, m_effectPos) : effect2;
                }
                else if (effectName == EffectConst.UI_TANCEQI_HONG)
                {
                    effect3 = effect3 == null ? UIEffect.ShowEffect(panel.transform, effectName, m_effectPos) : effect3;
                }
            }
        }
    }

    private void resetEffect()
    {
        if( effect1 != null && effect1.m_hide == false)
        {
            effect1.Hide();
        }
        if (effect2!= null && effect2.m_hide == false)
        {
            effect2.Hide();
        }
        if (effect3 != null && effect3.m_hide == false)
        {
            effect3.Hide();
        }
    }

    private void CancelTimer(int PlayerId)
    {
        if (timerDic!=null&&timerDic.ContainsKey(PlayerId))
        {
            int timer = timerDic[PlayerId];
            TimerManager.RemoveTimeOut(timer);
            timerDic[PlayerId] = 0;
        }
    }

    private void OnPlayerAtk(BasePlayer player)
    {
        if (player != null && player.Camp == 2 && player.alive&&!player.ghostJumpRecord.isJumping)
        {
            CancelTimer(player.PlayerId);
            float time = player.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
            player.SetVisableDegree(GameConst.GHOST_MODE_BODY_ALPHA, time);
        }
    }

    private void OnPlayerKillCurPlayer(BasePlayer player)
    {
        if (player != null && player.Camp == 2 && player.alive && !player.ghostJumpRecord.isJumping)
        {
            CancelTimer(player.PlayerId);
            player.isIgnoreTransparent = true;
            float time = player.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
            player.SetVisableDegree(GameConst.GHOST_MODE_BODY_ALPHA, time);
            TimerManager.SetTimeOut(3f, () => 
            {
                if (player != null)
                    player.isIgnoreTransparent = false; 
            });
        }
    }

    private void OnMainPlayerAtk(BasePlayer player, bool isDown)
    {
        if (player != null && player.Camp == 2 && player.alive)
        {
            CancelTimer(player.PlayerId);
            player.isIgnoreTransparent = isDown;
            float time = player.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
            player.SetVisableDegree(GameConst.GHOST_MODE_ARM_ALPHA, time);
            if (m_ghostJumpRecord != null)
                m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_FIRE);
        }
    }

    private void OnPlayerDie(BasePlayer shooter, BasePlayer dier, toc_fight_actor_die proto)
    {
        if (dier != null && dier.Camp == 2)
        {
            CancelTimer(dier.PlayerId);
            float time = dier.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
            dier.SetVisableDegree(1f, time);
            if (dier is MainPlayer)
            {
                resetEffect();
            }
            if (m_ghostJumpRecord != null)
                m_ghostJumpRecord.ResetStatus();
        }
    }

    private void OnC4Switch(bool isBegin)
    {
        int playerId = PlayerBattleModel.Instance.bombHolder;
        BasePlayer player = WorldManager.singleton.GetPlayerById(playerId);
        if (player != null && player.Camp == 2 && player.alive)
        {
            CancelTimer(player.PlayerId);
            player.isIgnoreTransparent = isBegin;
            float alpha = GameConst.GHOST_MODE_BODY_ALPHA;
            if (player is MainPlayer || player is FPWatchPlayer)
            {
                alpha = GameConst.GHOST_MODE_ARM_ALPHA;
            }
            if (player.visableDegree != alpha&&!player.ghostJumpRecord.isJumping)
            {
                float time = player.configData.HideDisappearTimePercent * GameSetting.PLAYER_SET_ALPHA_TIME;
                player.SetVisableDegree(alpha, time);
            }
            if (m_ghostJumpRecord != null)
                m_ghostJumpRecord.GhostJump(PlayerStatus.STATUS_FIRE);
        }
    }

    private static void Toc_fight_ghost_jump(toc_fight_ghost_jump data )
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(data.from);
       // player.ghostJumpRecord.isJumping = data.status == (int)GhostJumpStatus.Start ? true : false;        
        player.ghostJumpRecord.isVisible = data.visible;
        //Logger.Error(string.Format("rece  visible:{0}", data.visible));
    }
    
    

}
