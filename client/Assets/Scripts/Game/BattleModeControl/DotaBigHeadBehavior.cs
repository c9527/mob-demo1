﻿using System.Collections.Generic;
using UnityEngine;

public class DotaBigHeadBehavior : MonoBehaviour 
{
    public static DotaBigHeadBehavior singleton;

    private PanelBattleDotaBigHead panelBattleBigHead;
    //private ConfigBigHeadLevel m_bigHeadLevelConfig;
    private bool m_can2Hero;
    private int m_maxHeroCount;
    private int m_needKingCount;
    private int m_curHeroCount;
    private bool m_bIsFinalStage;
    private bool m_bIsPanelBattleInited;
    private int m_finalStartTime;
    
    protected SBattleState m_bs;
    protected readonly int DOTA_MAX_SCORE;
    protected readonly int DOTA_MAX_SCALE;

    private BasePlayer m_kPlayer;

    public DotaBigHeadBehavior()
    {
        singleton = this;

        DOTA_MAX_SCORE = ConfigMisc.GetInt("dota_max_score");
        DOTA_MAX_SCALE = ConfigMisc.GetInt("dota_max_scale");
        //DOTA_MAX_SCORE = 1000;
        //DOTA_MAX_SCALE = 3;
    }

    protected void Awake()
    {
        //m_bigHeadLevelConfig = ConfigManager.GetConfig<ConfigBigHeadLevel>();
        
        NetLayer.AddHandler(this);
        InitEvent();
    }

    protected void Start()
    {
        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited())
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
            return;
        }
    }

    protected void OnSceneLoaded()
    {
        if (m_bIsPanelBattleInited)
            Init();
    }

    protected void Init()
    {
        panelBattleBigHead = UIManager.PopPanel<PanelBattleDotaBigHead>();
        panelBattleBigHead.SetProgressStage(DOTA_MAX_SCORE - 1);
        if (m_bs != null)
        {
            OnUpdateBattleState(m_bs);
        }

        if(WorldManager.singleton.isViewBattleModel)
        {
            SwitchPlayer(ChangeTeamView.watchedPlayerID);
            //panelBattleBigHead.ShowCountTips(false);
        }
        else
        {
            BindPlayer(MainPlayer.singleton);
            //panelBattleBigHead.ShowCountTips(true);
            m_curHeroCount = GetActorTypeCount(m_kPlayer.Camp, GameConst.ACTOR_TYPE_BIG_HEAD_HERO);
            int curKingCount = GetActorTypeCount(m_kPlayer.Camp, GameConst.ACTOR_TYPE_BIG_HEAD_KING);
            //panelBattleBigHead.SetCountTips(curKingCount, m_maxHeroCount);
        }
        OnPlayerJoin(m_kPlayer);
    }

    protected void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.RemoveEventListener<toc_fight_energy>(GameEvent.PLAYER_ENERGY_UPDATE, UpdateScore);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_ACTOR_TYPE_UPDATE, PlayerActorTypeUpdate);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchPlayer);
        GameDispatcher.RemoveEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        NetLayer.RemoveHandler(this);
    }

    private void InitEvent()
    {
        GameDispatcher.AddEventListener<string, BasePlayer>(GameEvent.PLAYER_REVIVE, OnPlayerRevive);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<toc_fight_energy>(GameEvent.PLAYER_ENERGY_UPDATE, UpdateScore);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_JOIN, OnPlayerJoin);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_ACTOR_TYPE_UPDATE, PlayerActorTypeUpdate);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchPlayer);
    }

    protected void BindPlayer(BasePlayer player)
    {
        m_kPlayer = player;
    }

    protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        Init();
        if (m_bIsFinalStage)
        {
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.BigHead, "last_time_befall", 3, false, new Vector2(0, 120));
            GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_BIG_HEAD_FINAL_TIME_UPDATED);
        }
        m_bIsPanelBattleInited = true;
    }

    private void UpdateScore(BasePlayer player, int score)
    {
        if (player == null)
            return;

        if (player is OtherPlayer)
        {
            float bigHeadScale = score >= DOTA_MAX_SCORE ? DOTA_MAX_SCALE : 1;
            player.lastBigHeadScale = bigHeadScale;
            player.SetHeadScale(bigHeadScale);
        }

        //Logger.Log(string.Format("toc_fight_score: {0}, 名字：{1}", score, player.PlayerName));

        if (player == m_kPlayer && panelBattleBigHead != null)
        {
            panelBattleBigHead.SetBigHeadFillAmount(score);

            //var curLevel = m_bigHeadLevelConfig.GetBigHeadConfig(score);
            //if (score >= DOTA_MAX_SCORE) //大头王
            //{
            //    panelBattleBigHead.SetBigHeadFillAmount(score);
            //}
            //else
            //{
            //    var nextLevel = m_bigHeadLevelConfig.GetLine(curLevel.Level + 1);
            //    float nextLevelRatio =(float) (score - curLevel.Score) / (nextLevel.Score - curLevel.Score);
            //    panelBattleBigHead.SetBigHeadProgress(curLevel.Level - 1 + nextLevelRatio);
            //}
        }
    }

    private void UpdateScore(toc_fight_energy proto)
    {
        p_king_score[] list = proto.list;
        for (int i = 0; i < list.Length; ++i)
        {
            BasePlayer player = WorldManager.singleton.GetPlayerById(list[i].actor_id);
            if(player == null)
                continue;
            UpdateScore(player, list[i].score);
        }
    }

    protected void OnUpdateBattleState(SBattleState data)
    {
        m_bs = data;

        if (m_bs == null || panelBattleBigHead == null || !m_bIsPanelBattleInited)
            return;

        switch (m_bs.state)
        {
            case GameConst.Fight_State_RoundStart:
                m_can2Hero = false;
                m_bIsFinalStage = false;
                m_curHeroCount = 0;
                panelBattleBigHead.ShowBecomeHeroTip(false);
                panelBattleBigHead.ShowHeroTip(false);
                panelBattleBigHead.ShowProgressEffect(false);
                panelBattleBigHead.ShowProgress(true);
                //if (!WorldManager.singleton.isViewBattleModel)
                //    panelBattleBigHead.ShowCountTips(true);
                break;
        }
    }

    protected void OnPlayerJoin(BasePlayer player)
    {
        if (player == null)
            return;

        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return;
        if (data.m_allPlayers == null) return;
        if (!data.m_allPlayers.ContainsKey(player.PlayerId))
            return;
        UpdateScore(player, data.m_allPlayers[player.PlayerId].fight_energy);
    }


    private void Toc_fight_final_time(toc_fight_final_time proto)
    {
        m_bIsFinalStage = true;

        if (m_kPlayer == MainPlayer.singleton)
        {
            if(!m_kPlayer.isAlive)
                panelBattleBigHead.ShowBecomeHeroTip(false);
            else if(m_can2Hero && m_kPlayer.serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_KING)
                panelBattleBigHead.ShowBecomeHeroTip(true);
        }
        
        m_finalStartTime = proto.final_time;
        if (!m_bIsPanelBattleInited)
        {
            return;
        }
        UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.BigHead, "last_time_befall", 3, false, new Vector2(0, 140));
        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_BIG_HEAD_FINAL_TIME_UPDATED);
        AudioManager.PlayFightUISound(AudioConst.finalMoment);
    }

    private void Toc_fight_actor2big_head_king(toc_fight_actor2big_head_king proto)
    {
        //Logger.Log(string.Format("toc_fight_actor2big_head_king:"));
        //Logger.Log(string.Format("actor:{0}, king_index:{1}, max_heros:{2}, hero_need_king:{3}", proto.actor, proto.king_index, proto.max_heros, proto.hero_need_king));

        m_maxHeroCount = proto.max_heros;
        m_needKingCount = proto.hero_need_king;

        if (panelBattleBigHead == null)
            return;

        BasePlayer player = WorldManager.singleton.GetPlayerById(proto.actor);

        if (player != null)
        {
            if (player.isAlive && player.Camp == m_kPlayer.Camp)
            {
                int curKingCount = GetActorTypeCount(player.Camp, GameConst.ACTOR_TYPE_BIG_HEAD_KING);
                if (player == m_kPlayer && proto.king_index <= m_maxHeroCount)
                {
                    //显示特效
                    panelBattleBigHead.ShowProgressEffect(m_kPlayer == MainPlayer.singleton);
                    m_can2Hero = true;
                    if(m_kPlayer == MainPlayer.singleton && (curKingCount >= m_needKingCount || m_bIsFinalStage))
                        panelBattleBigHead.ShowBecomeHeroTip(true);
                    else if (curKingCount < m_needKingCount)
                        UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.BigHead, "become_big_head_king", 3, new Vector2(0, 120));
                }
                else
                {
                    if (( m_bIsFinalStage || curKingCount >= m_needKingCount) && m_can2Hero && m_kPlayer == MainPlayer.singleton)
                        panelBattleBigHead.ShowBecomeHeroTip(true);
                }
            }
        }
        //panelBattleBigHead.SetCountTips(GetActorTypeCount(m_kPlayer.Camp, GameConst.ACTOR_TYPE_BIG_HEAD_KING), m_maxHeroCount);
    }

    private void PlayerActorTypeUpdate(BasePlayer player)
    {
        if (panelBattleBigHead == null)
            return;

        if (player.Camp == m_kPlayer.Camp && player.serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_HERO)
        {
            if (player == m_kPlayer)
            {
                m_can2Hero = false;
                panelBattleBigHead.ShowBecomeHeroTip(false);
                panelBattleBigHead.ShowProgressEffect(false);
                panelBattleBigHead.ShowProgress(false);
                //panelBattleBigHead.ShowCountTips(false);
                if (m_kPlayer is MainPlayer && m_kPlayer.isAlive)
                    panelBattleBigHead.ShowHeroTip(true);
            }
            ++m_curHeroCount;
            if(m_curHeroCount == m_maxHeroCount)
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.BigHead, "big_head_hero_all_befall", 3, new Vector2(0, 0));
        }
    }

    void OnPlayerDie(BasePlayer killer, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (panelBattleBigHead == null)
            return;

        if (dier != m_kPlayer || !isFinalStage)
        {
            return;
        }

        m_can2Hero = false;
        panelBattleBigHead.ShowBecomeHeroTip(false);
        panelBattleBigHead.ShowProgressEffect(false);
        panelBattleBigHead.ShowHeroTip(false);
    }

    void OnPlayerRevive(string eventName, BasePlayer who)
    {
        if (who == MainPlayer.singleton)
        {
            BindPlayer(who);
            OnPlayerJoin(who);
            if(isFinalStage && m_can2Hero && m_kPlayer.serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_KING)
            {
                panelBattleBigHead.ShowBecomeHeroTip(true);
            }
        }
        else if (WorldManager.singleton.isViewBattleModel && who == m_kPlayer)
        {
            BindPlayer(who);
            OnPlayerJoin(who);
        }

        //复活时强制设置大头比例
        SBattlePlayer sp = PlayerBattleModel.Instance.battle_info.GetPlayer(who.PlayerId);
        if( sp != null)
            UpdateScore(who, sp.fight_energy);
    }

    private void SwitchPlayer(int partnerID)
    {
        BasePlayer player = WorldManager.singleton.GetPlayerById(partnerID);
        if (player != null)
        {
            BindPlayer(player);
            OnPlayerJoin(player);
            if (player.serverData.actor_type == GameConst.ACTOR_TYPE_BIG_HEAD_HERO)
                panelBattleBigHead.ShowProgress(false);
            else
                panelBattleBigHead.ShowProgress(true);

            //观战时把这些UI关闭
            panelBattleBigHead.ShowBecomeHeroTip(false);
            panelBattleBigHead.ShowProgressEffect(false);
            panelBattleBigHead.ShowHeroTip(false);
        }
    }

    private int GetActorTypeCount(int camp, int type)
    {
        int result = 0;
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data != null)
        {
            SBattleCamp sc = data.GetCamp(camp);
            foreach (var sp in sc.players)
            {
                if (sp.actor_type == type)
                    ++result;
            }
        }
        return result;
    }


    #region get set

    public bool isFinalStage
    {
        get { return m_bIsFinalStage; }
    }

    public int finalStartTime
    {
        get { return m_finalStartTime; }
    }
    #endregion


}
