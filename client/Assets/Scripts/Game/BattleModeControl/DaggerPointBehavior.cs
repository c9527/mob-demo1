﻿using UnityEngine;
using System.Collections;
///////////////////////////////////////////
//Copyright (C): 4399 FPS studio
//All rights reserved
//文件描述：刀锋据点模式
//创建者：hwl
//创建日期: 2016/11/03
///////////////////////////////////////////
public class DaggerPointBehavior : RescueBehavior
{

    private GameObject m_sceneGO;
    static public DaggerPointBehavior singleton;
    private PanelDaggerOW m_panelDaggerOW;
    private toc_fight_occupy m_fight_occupy;
    private ConfigMapListLine m_mapListLine;
    private Transform m_jdPoint;
    public int m_PoiontNum = 0;

    public DaggerPointBehavior()
    {
        singleton = this;
    }

    void Awake()
    {
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        NetLayer.AddHandler(this);
        m_mapListLine = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString());
        m_PoiontNum = m_mapListLine.StrongPointDistance.Split(';').Length;
    }

    private void OnSceneLoaded()
    {
        m_sceneGO = SceneManager.singleton.curSceneGo;
        m_jdPoint = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_OWPOINT_PATH);
        initPointEffect();
        PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
        if (battlePanel != null && battlePanel.IsInited())
            Init();
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    private void Init()
    {
        OnUpdateBattleState(m_bs);
        if (m_panelDaggerOW == null)
            m_panelDaggerOW = UIManager.PopPanel<PanelDaggerOW>();
        m_panelDaggerOW.BindBehavior(this);
    }

    private void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        Init();

    }

    private SBattleState m_bs;
    void OnUpdateBattleState(SBattleState data)
    {
        m_bs = data;

        if (m_sceneGO == null || m_bs == null)
            return;
        switch (m_bs.state)
        {
            case GameConst.Fight_State_GameStart:
            case GameConst.Fight_State_RoundStart:
                string pic = "tip";
                if (UIManager.IsOpen<PanelBattle>() && !string.IsNullOrEmpty(pic))
                {
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.DaggerOW, pic, 5, false, new Vector2(0, 50), null, default(Vector3), "", "");
                }
                ShowOwPointInMap();
                break;
            default:
                break;
        }
    }

    private void ShowOwPointInMap()
    {
        if (WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_DAGGER_OW_TAG))
        {
            Transform bpp = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_OWPOINT_PATH);
            if (bpp != null)
            {
                for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
                {
                    GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                    goChildBp.TrySetActive(true);
                   // SceneManager.singleton.m_daggerEffectList[childIndex].Play();
                }
            }
        }
    }

    private void initPointEffect()
    {
        string[] radiusList = m_mapListLine.StrongPointDistance.Split(';');
        string[] effectList = m_mapListLine.StrongPointEffect.Split(';');
        Transform bpp = m_sceneGO.transform.FindChild(SceneConst.SCENE_CHILD_OWPOINT_PATH);
        if (bpp != null)
        {
            string effectName = "";
            
            for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
            {
                float radius = float.Parse(radiusList[childIndex]);
                int initEffect = int.Parse(effectList[childIndex]);
                GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                for (int i = 0; i < 3;i++ )
                {
                    if (initEffect == i)
                    {
                        continue;
                    }
                    effectName = getDaggerPointEffectName(i.ToString());
                    if (goChildBp.transform.Find(effectName) == null)
                    {
                        EffectManager.singleton.GetEffect<EffectTimeout>(effectName, (g) =>
                        {
                            if (g == null || goChildBp == null) return;
                            g.Attach(goChildBp.transform);
                            float r = radius / 2;
                            g.transform.localScale = new Vector3(r, r, r);
                            g.transform.localPosition = new Vector3(0f, -0.3f, 0f);
                            SceneManager.singleton.m_daggerEffectList.Add(g);
                        });
                    }
                }
                
            }
        }
    }

    private string getDaggerPointEffectName(string value)
    {
        switch (value)
        {
            case "0":
                return EffectConst.DAGGEROW_GRAY;
            case "1":
                return EffectConst.DAGGEROW_RED;
            case "2":
                return EffectConst.DAGGEROW_BLUE;
        }
        return EffectConst.DAGGEROW_GRAY;
    }

    private void Toc_fight_occupy(toc_fight_occupy fight_occupy)
    {
        m_fight_occupy = fight_occupy;
        if (m_panelDaggerOW != null && m_panelDaggerOW.IsOpen())
        {
            m_panelDaggerOW.UpdateProgress();
        }
        updateEffectState();
    }

    private void updateEffectState()
    {
        p_occupy_info info = null;
        for (int i = 0; i < m_fight_occupy.occupy_infos.Length; i++)
        {
            info = m_fight_occupy.occupy_infos[i];
            if (info == null)
            {
                Logger.Log("据点数据为空 i = " + i);
            }
            GameObject goChildBp = null;
            if (m_jdPoint == null || m_jdPoint.GetChild(i) == null)
            {
                continue;
            }
            goChildBp = m_jdPoint.GetChild(i).gameObject;
            if (goChildBp.transform.Find(getDaggerPointEffectName("0")) == null || goChildBp.transform.Find(getDaggerPointEffectName("1")) ==null || goChildBp.transform.Find(getDaggerPointEffectName("2")) == null)
                return;

            GameObject grayEffect = goChildBp.transform.Find(getDaggerPointEffectName("0")).gameObject;
            GameObject redEffect = goChildBp.transform.Find(getDaggerPointEffectName("1")).gameObject;
            GameObject blueEffect = goChildBp.transform.Find(getDaggerPointEffectName("2")).gameObject;
            if (info.owner == 0)
            {
                grayEffect.TrySetActive(true);
                redEffect.TrySetActive(false);
                blueEffect.TrySetActive(false);
                SceneManager.singleton.m_daggerEffectList[i].Play();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum + i].Stop();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum *2+ i].Stop();
            }
            else if (info.owner == 1)
            {
                grayEffect.TrySetActive(false);
                redEffect.TrySetActive(true);
                blueEffect.TrySetActive(false);
                SceneManager.singleton.m_daggerEffectList[i].Stop();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum + i].Play();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum *2 + i].Stop();
               
            }
            else
            {
                grayEffect.TrySetActive(false);
                redEffect.TrySetActive(false);
                blueEffect.TrySetActive(true);
                SceneManager.singleton.m_daggerEffectList[i].Stop();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum + i].Stop();
                SceneManager.singleton.m_daggerEffectList[m_PoiontNum*2 + i].Play();
                
            }
        }
    }

    protected override void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        base.OnPlayerDie(killer, who, iLastHitPart, weapon);
        if (m_panelDaggerOW != null && m_panelDaggerOW.IsInited())
        {
            m_panelDaggerOW.ReSetBar();
        }
    }

    public int[] ResoureList()
    {
        if (m_fight_occupy == null)
        {
            return null;
        }
        return m_fight_occupy.resources;
    }

    internal p_occupy_info[] GetOccupyInfoList()
    {
        if (m_fight_occupy == null)
            return null;
        return m_fight_occupy.occupy_infos;
    }

    internal p_occupy_info GetOccupyInfo(int id)
    {
        if (m_fight_occupy == null)
        {
            return null;
        }
        for( int i = 0; i <  m_fight_occupy.occupy_infos.Length;i++)
        {
            if( id ==  m_fight_occupy.occupy_infos[i].id)
            {
                return m_fight_occupy.occupy_infos[i];
            }
        }
        return null;
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);

        NetLayer.RemoveHandler(this);
        SceneManager.singleton.m_daggerEffectList.Clear();
        singleton = null;
    }
}
