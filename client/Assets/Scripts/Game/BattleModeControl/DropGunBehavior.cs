﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public class DropGunBehavior : MonoBehaviour
{
    static public DropGunBehavior singleton;
    private Dictionary<int, int> uIdDic = new Dictionary<int, int>();
    private Dictionary<int, TweenPosition> dropingItmeDic = new Dictionary<int, TweenPosition>();

    private readonly float FLY_TIME;

    public DropGunBehavior()
    {
        singleton = this;
        FLY_TIME = (float)ConfigMisc.GetInt("drop_gun_fly_time") / 1000f;
    }

    void Awake()
    {
        GameDispatcher.AddEventListener<int>(GameEvent.DROPSYS_ADD_DROP, OnAddDropItem);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        GameDispatcher.AddEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);

        NetLayer.AddHandler(this);
    }

    void OnDestroy()
    {
        GameDispatcher.RemoveEventListener<int>(GameEvent.DROPSYS_ADD_DROP, OnAddDropItem);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_SET_STATE, OnPlayerSetState);
        GameDispatcher.RemoveEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);
        NetLayer.RemoveHandler(this);
        uIdDic.Clear();
        uIdDic = null;
        dropingItmeDic.Clear();
        dropingItmeDic = null;
        singleton = null;
    }

    void Update()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            bool isNotAlive = MainPlayer.singleton == null || !MainPlayer.singleton.isAlive;
            UIManager.GetPanel<PanelBattle>().ShowDropGunBtn(!isNotAlive && MainPlayer.singleton.canDropWeapon);
        }
    }

    public Vector3 findTargetPoint()
    {
        if (MainPlayer.singleton == null || !MainPlayer.singleton.isAlive) return Vector3.zero;
        Vector3 fromPos = MainPlayer.singleton.headPos;
        Vector3 centerPos = MainPlayer.singleton.transform.position;
        Vector3 toPos = centerPos + MainPlayer.singleton.transform.forward * 2.5f;
        Vector3 dir = toPos - fromPos;
        RaycastHit kRH;
        Vector3 pos;
        if (Physics.Raycast(fromPos, dir, out kRH, 100, GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER | GameSetting.LAYER_MASK_AIR_WALL))
        {
            Vector3 hitPos = kRH.point;
            pos = hitPos -MainPlayer.singleton.transform.forward * 0.3f;
        }
        else
        {
            pos = centerPos;  
        }

        pos.y += 1f;

        return pos;
    }

    public void OnDropGun()
    {
        PlayerBattleModel.Instance.bag_selected = true;
        Vector3 targetPos = findTargetPoint();
        targetPos = SceneManager.GetGroundPoint(targetPos);
        NetLayer.Send(new tos_fight_throw_weapon() { pos = targetPos });
        //if (DropItemManager.singleton.IsDropItemMainWeapon) DropItemManager.singleton.PickUpDropItem();
    }

    public void OnDropC4()
    {
        Vector3 targetPos = findTargetPoint();
        targetPos = SceneManager.GetGroundPoint(targetPos);
        NetLayer.Send(new tos_fight_throw_cfour() { pos = targetPos });
    }

    TweenPosition tweenPos = null;
    void OnAddDropItem(BasePlayer player, DropItem item)
    {
        if (player == null || player.serverData == null || !player.isAlive || player.released || player.gameObject == null || item == null || item.released) return;
        int itemId = item.dropID;
        GameObject itemGo = item.gameObject;
        if ((DropItemManager.singleton.IsWeapon(itemId) == false && DropItemManager.singleton.IsBomb(itemId) == false) || itemGo == null)
        {
            Logger.Error("OnAddDropItem, no item, id: "+itemId+", go: "+itemGo);
            return;
        }
        
        //Logger.Error("flying item, uid: " + item.uID + ", dropId: " + item.dropID + ", name: " + itemGo.name);
        Vector3 targetPos = itemGo.transform.position;
        Vector3 rotation0 = player.transform.localEulerAngles;
        Vector3 rotation1 = itemGo.transform.localEulerAngles;
        rotation1.y = rotation0.y;
        itemGo.transform.localEulerAngles = rotation1;
        Vector3 from = player.headPos;
        from.y = from.y - player.height / 2 + 0.7f;
        from += player.transform.forward * 0.2f;
        //Logger.Error("from: " + from + ", to: " + targetPos);

        //if (tweenPos == null)
        //{
        tweenPos = TweenPosition.Begin(itemGo, FLY_TIME, targetPos);
            tweenPos.worldSpace = true;
        //}
            dropingItmeDic.Add(itemId, tweenPos);
        item.isMoving = true;
        tweenPos.ResetToBeginning();
        tweenPos.from = from;
        tweenPos.to = targetPos;
        tweenPos.SetOnFinished(() => { item.isMoving = false; dropingItmeDic.Remove(itemId);});
        tweenPos.Play(true);
    }

    void OnDelDropItemInScene(DropItem item)
    {
        int itemId = item.dropID;
        if (dropingItmeDic.TryGetValue(itemId, out tweenPos))
        {
            dropingItmeDic.Remove(itemId);
        }
    }

    void OnPlayerSetState(BasePlayer player)
    {
        if (player == null) return;
        if (player == MainPlayer.singleton)
        {
            if (!player.isAlive)
            {
                if(UIManager.IsOpen<PanelBattle>())
                {
                    UIManager.GetPanel<PanelBattle>().ShowDropGunBtn(false);
                    UIManager.GetPanel<PanelBattle>().ShowDropGunImg(false);
                }
            }
        }
    }

    void OnAddDropItem(int uId)
    {
        if (uIdDic.ContainsKey(uId))
        {
            int from = uIdDic[uId];
            DropItem item = DropItemManager.singleton.GetDropItemByUID(uId);
            if (item == null || !item.isInited) { Logger.Error("again---- no drop item, uid: " + uId + ", form: " + from); return; }

            BasePlayer player = WorldManager.singleton.GetPlayerById(from);
            if (player == null || player.serverData == null || !player.isAlive) { Logger.Error("again---- player not alive, id: " + from); return; }
            OnAddDropItem(player, item);

            uIdDic.Remove(uId);
        }
    }

    void ThrowItem(int from, int uid)
    {
        //Logger.Error("from: "+from+", uid: "+uid);
        DropItem item = DropItemManager.singleton.GetDropItemByUID(uid);
        if (item == null || !item.isInited)
        {
            //Logger.Error("Toc_fight_throw_weapon no drop item, uid: " + uid + ", form: " + from);
            uIdDic.Add(uid, from);
            return;
        }
        BasePlayer player = WorldManager.singleton.GetPlayerById(from);
        if (player == null || player.serverData == null || !player.isAlive) { Logger.Error("Toc_fight_throw_weapon player not alive, id: " + from); return; }
        OnAddDropItem(player, item);
    }

    void Toc_fight_throw_weapon(toc_fight_throw_weapon data)
    {
        int from = data.from;
        int uid = data.drop_uid;
        ThrowItem(from, uid);
    }

    void Toc_fight_throw_cfour(toc_fight_throw_cfour data)
    {
        int from = data.from;
        int uid = data.drop_uid;
        ThrowItem(from, uid);
    }

}


