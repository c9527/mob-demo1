﻿using System;
using System.Collections.Generic;
using UnityEngine;

class DotaWinType
{
    public const string KILLALL = "killall";
    public const string PICKUP = "pickup_box";
    public const string KILLCNT = "killcnt";
    public const string PICKKILL = "pickkill";
    public const string KILLBOSS = "killboss";
}

class DotaState
{
    public BasePlayer player;
    public DotaBuffType topBuffType;
    public int topBuffLevel;
    public int rank;
    static int maxRank;

    public DotaState()
    {
        topBuffType = DotaBuffType.DotaBuff_NONE;
    }

    public void AddRank()
    {
        rank = maxRank++;
    }

}

class DotaBehavior : MonoBehaviour
{
    static public DotaBehavior singleton;

    protected GameObject m_sceneGO;

    protected int m_fillBulletShopId;

    protected int m_pveBuffShopIdStart;
    protected int m_pvpBuffShopIdStart;
    protected int m_bossBuffShopIdStart;
    protected int m_bossReviveBuffShopIdStart;
    protected int m_cureBuffShopIdStart;
    protected p_dota_camp_info m_myCampInfo;

    protected readonly int BUY_BULLET_NEED_TIME_SEC;

    protected bool isBuying = false;
    protected SBattleState m_bs;

    protected int m_gameRound;
    protected int m_configRound;
    protected string m_winType;
    protected int m_winParam;

    //protected ConfigSurvivalStageLine m_stageConfig;

    protected toc_fight_dota_state data;
    private p_dota_camp_info[] m_dotaCampList = null;

    protected PanelBattleDota panelSurvival;
    protected PanelBattleDotaScore panelScore;

    protected bool m_reNotice = false;

    protected Dictionary<int, DotaState> m_allDotaState = new Dictionary<int,DotaState>();

    public DotaBehavior()
    {
        singleton = this;

        BUY_BULLET_NEED_TIME_SEC = ConfigMisc.GetInt("survival_buy_bullet_wait_time");
        m_fillBulletShopId = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_FILL_BULLET);

        m_pveBuffShopIdStart = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVEKBUFF);
        m_pvpBuffShopIdStart = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVPBUFF);
        m_bossBuffShopIdStart = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_BOSSBUFF);
        m_bossReviveBuffShopIdStart = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_BOSSREVIVEBUFF);
        m_cureBuffShopIdStart = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopId(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_CUREBUFF);
        Logger.Log(string.Format("DotaBehavior: m_attackBuffShopIdStart:{0}", m_pveBuffShopIdStart));
        Logger.Log(string.Format("DotaBehavior: m_bloodBuffShopIdStart:{0}", m_pvpBuffShopIdStart));
        Logger.Log(string.Format("DotaBehavior: m_bossBuffShopIdStart:{0}", m_bossBuffShopIdStart));
        Logger.Log(string.Format("DotaBehavior: m_bossReviveBuffShopIdStart:{0}", m_bossReviveBuffShopIdStart));
        Logger.Log(string.Format("DotaBehavior: m_cureBuffShopIdStart:{0}", m_cureBuffShopIdStart));
    }

    protected void Awake()
    {
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.AddEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);
        GameDispatcher.AddEventListener<int, bool>(GameEvent.GAME_BATTLE_ROUND_UPDATE, UpdateRound);
        GameDispatcher.AddEventListener(GameEvent.BATTLE_MODEL_CAMP_UPDATE, UpdateCamp);
        NetLayer.AddHandler(this);
    }

    protected void OnDestroy()
    {
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnSceneLoaded);
        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
        GameDispatcher.RemoveEventListener<DropItem>(GameEvent.DROPSYS_DEL_DROP, OnDelDropItemInScene);
        GameDispatcher.RemoveEventListener<int, bool>(GameEvent.GAME_BATTLE_ROUND_UPDATE, UpdateRound);
        GameDispatcher.RemoveEventListener(GameEvent.BATTLE_MODEL_CAMP_UPDATE, UpdateCamp);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        NetLayer.RemoveHandler(this);
    }

    protected void OnSceneLoaded()
    {
        m_sceneGO = SceneManager.singleton.curSceneGo;
        PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
        if (battlePanel != null && battlePanel.IsInited())
            Init();
        else
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
    }

    protected void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        Init();
    }

    protected void UpdateCamp()
    {
        //Logger.Log("DotaBehavior---UpdateCamp");
        if (null != panelScore)
        {
            panelScore.UpdateMsgTipInfo(msgtips_status);
        }
    }

    virtual protected void Init()
    {
        //dota模式下的购买点
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BUFFPOINT_PATH, AtlasName.Survival, "buff1", "Buff");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BULLETPOINT_PATH, AtlasName.Battle, "logo_ammo_box", "Bullet");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BUFFPOINT1_PATH, AtlasName.Survival, "buff1", "Buff");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BULLETPOINT1_PATH, AtlasName.Battle, "logo_ammo_box", "Bullet");

        panelSurvival = UIManager.PopPanel<PanelBattleDota>();
        panelSurvival.BindBehavior(this);
        if (panelScore == null)
            panelScore = UIManager.PopPanel<PanelBattleDotaScore>();

        var basePlayerRight = GetBasePlayer((int)WorldManager.CAMP_DEFINE.UNION);
        if (basePlayerRight != null)
            panelScore.BindBasePlayerRight(basePlayerRight);

        var basePlayerLeft = GetBasePlayer((int)WorldManager.CAMP_DEFINE.REBEL);
        if (basePlayerLeft != null)
            panelScore.BindBasePlayerLeft(basePlayerLeft);

        panelScore.BindBehavior(this);
        if (m_bs != null)
        {
            OnUpdateBattleState(m_bs);
            UpdateRound(m_bs.round);
        }
        else if (m_configRound != 0)
        {
            UpdateRound(m_configRound);
        }

        panelScore.InitBaseHpBarRight();
        panelScore.InitBaseHpBarLeft();
    }

    private BasePlayer GetBasePlayer(int camp)
    {
        foreach (var player in WorldManager.singleton.allPlayer.Values)
        {
            if (null != player && player.Camp == camp && null != player.configData
                && player.configData.SubType == GameConst.ROLE_SUBTYPE_BASE)
                return player;
        }
        return null;
    }

    protected void GenerateMapBuyPoints(string path, string atlas, string sprite, string type)
    {
        Transform bpp = m_sceneGO.transform.FindChild(path);

        if (bpp != null)
        {
            for (int childIndex = 0; childIndex < bpp.childCount; ++childIndex)
            {

                GameObject goChildBp = bpp.GetChild(childIndex).gameObject;
                if (goChildBp != null)
                {
                    ThroughTheWallProp prop = new ThroughTheWallProp() 
                    { 
                        imgProp = new RectTransformProp() { scale = new Vector3(1.6f, 1.6f, 1) } ,
                        distance = WorldManager.singleton.gameRuleLine.MapIconVisibleRange <= 0 ? float.MaxValue : WorldManager.singleton.gameRuleLine.MapIconVisibleRange,
                    };
                    UIThroughTheWall.createUI().play(atlas, sprite, goChildBp.transform, new Vector3(0, 1.2f, 0), prop);
                }

                if (goChildBp != null)
                {
                    SphereCollider collider = goChildBp.AddMissingComponent<SphereCollider>();
                    collider.radius = 1;
                    collider.isTrigger = true;
                    goChildBp.AddMissingComponent<PointTriggerEvent>().type = type;
                    goChildBp.TrySetActive(true);

                    ResourceManager.LoadBattlePrefab("Effect/baopodian_1", (go, crc) =>
                    {
                        if (go == null || goChildBp == null) return;
                        go.transform.SetLocation(goChildBp.transform, 0, 0.05f);
                    });
                }
            }
        }
    }

    protected void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            StopBuy();
        }
        else if (WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.Camp != who.Camp)
        {
            if (panelScore != null && data != null)
                panelScore.UpdateInfo(data.revivecoin, data.supplypoint, 1, 1, 1, 1, monster);
        }
    }

    public void OnClickReviveBtn(int playerId)
    {
        if (revivecoin > 0)
        {
            NetLayer.Send(new tos_fight_survival_use_revivecoin() { actor = playerId });
        }
        else
        {
            UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetMallLine(5007) });
        }
    }

    public void StartBuy(Transform parent, int playerId)
    {
        if (isBuying) return;
        isBuying = true;
        ControlProcess(parent, true, "弹药补充中...", () => { isBuying = false; });
        //Logger.Log(string.Format("发送购买子弹协议, shop_id:{0}", m_fillBulletShopId));
        NetLayer.Send(new tos_fight_game_shop() { shop_id = m_fillBulletShopId });
    }

    public void StopBuy(bool send = true)
    {
        if (isBuying)
        {
            ControlProcess(null, false);
            if (send) NetLayer.Send(new tos_fight_cancel_shop() { });
            isBuying = false;
        }
    }


    private ProgressBarProp prop;
    private int m_timerId;
    protected void ControlProcess(Transform parent, bool isShow, string msg = "", Action callBack = null)
    {
        TimerManager.RemoveTimeOut(m_timerId);

        m_timerId = TimerManager.SetTimeOut(0.1f, () =>
        {
            if (UIManager.IsOpen<PanelBattle>())
            {
                if (!isShow) { UIManager.GetPanel<PanelBattle>().StopProgressBar("Survival"); return; }
                if (prop == null)
                {
                    Vector2 pos = new Vector2(0, -115);
                    prop = new ProgressBarProp() { bgPos = pos, progressPos = pos, txtPos = pos, textProp = new TextProp() { fontSize = 22, rect = new RectTransformProp() { size = new Vector2(200, 40) } }, OnTimeOut = callBack };
                }
                UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", msg, BUY_BULLET_NEED_TIME_SEC, prop, "Survival");
            }
        });
    }

    protected void OnDelDropItemInScene(DropItem dropItem)
    {
        if (dropItem.dropID == 19050)
        {
            if (UIManager.IsOpen<PanelBattle>())
            {
                TipsManager.Instance.showTips("获取病毒箱");
                UIManager.GetPanel<PanelBattle>().StopProgressBar("DropItem1023");
            }
        }
    }

    protected virtual void OnUpdateBattleState(SBattleState data)
    {
        m_bs = data;
        if (m_bs == null || m_bs.state != GameConst.Fight_State_RoundStart)
            return;

        if (StoryManager.singleton != null)
           StoryManager.singleton.EndStoryForce();

        BasePlayer basePlayerRight = null;
        BasePlayer basePlayerLeft = null;
        if (panelScore != null)
        {
            basePlayerRight = GetBasePlayer((int)WorldManager.CAMP_DEFINE.UNION);
            if (basePlayerRight != null && panelScore != null)
                panelScore.BindBasePlayerRight(basePlayerRight);

            basePlayerLeft = GetBasePlayer((int)WorldManager.CAMP_DEFINE.REBEL);
            if (basePlayerLeft != null && panelScore != null)
                panelScore.BindBasePlayerLeft(basePlayerLeft);
        }
        if (basePlayerRight == null || basePlayerLeft == null)
        {
            Logger.Log("获取基地玩家失败，2秒后重新获取!!");
            TimerManager.SetTimeOut(2f, () => { OnUpdateBattleState(m_bs); });
        }

        PlaySelfNoticePanel();
        //PlayNoticePanel();

        //以下是否需要
        //if (m_sceneGO == null || m_bs == null)
        //    return;
        //switch (m_bs.state)
        //{
        //    case GameConst.Fight_State_RoundStart:
        //        PlaySelfNoticePanel(m_bs.round, 6);
        //        PlayNoticePanel();
        //        break;
        //}
    }

    protected virtual void UpdateRound(int round, bool playStory = false)
    {
        if (panelScore == null || m_bs == null)
        {
            m_configRound = round;
            return;
        }
        m_winType = DotaWinType.KILLALL;
        if (null != WorldManager.singleton.gameRuleLine)
        {
            panelScore.UpdateRound(round, m_configRound, m_winType, WorldManager.singleton.gameRuleLine.Time_play);
        }
    }

    protected void PlayNoticePanel()
    {
        if (UIManager.IsOpen<PanelBattle>())
        {
            var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
            if (run_time < 10)
            {
                NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "num_o_second", pos = new Vector2(230, -75) });
                prop.numIconList.Add(new ImgNumProp() { preName = "num_o", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(210, -70) });
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Survival, "next_round_tip", 10 - run_time, new Vector2(0, -70), prop);
            }
        }
    }

    protected virtual void PlaySelfNoticePanel(int duration = 6)
    {
        if (data == null)
        {
            m_reNotice = true;
            return;
        }

        string pic = "dota_game_tips";

        if (UIManager.IsOpen<PanelBattle>())
        {

            if (Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.Survival, pic, duration, false, new Vector2(-600, 50), null, new Vector3(0.5f, 0.5f, 0.5f));
            else
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(AtlasName.Survival, pic, duration, false, new Vector2(0, 50), null);
        }
    }

    /// <summary>
    /// 1，双方BOSS都存活时：消灭敌方巨石
    /// 2，已方BOSS存活而敌方BOSS死亡时：保护已方巨石
    /// 3，双方BOSS都死亡时：消灭敌人
    /// 4，已方BOSS死亡而敌方BOSS存活时：阻止敌方巨石
    /// </summary>
    /// <returns></returns>
    protected int CaculateMsgTipsStatus()
    {
        if (MainPlayer.singleton == null) return -1;
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return -1;
        if (data.m_allPlayers == null) return -1;
        int myBoss = 0;
        int otherBoss = 0;
        foreach (var player in data.m_allPlayers.Values)
        {
            if (player.actor_type == GameConst.ACTOR_TYPE_BOSS && player.alive)
            {
                if (player.camp == MainPlayer.singleton.Camp)
                    ++myBoss;
                else
                    ++otherBoss;
            }
        }
        if (myBoss > 0 && otherBoss > 0)
            return 1;
        else if (myBoss > 0 && 0 == otherBoss)
            return 2;
        else if (0 == myBoss && 0 == otherBoss)
            return 3;
        else
            return 4;
    }

    protected int CaculateRemainZombie()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;

        if (data == null) return -1;
        if (m_winParam <= 0) return -1;
        int length = data.camps.Count;
        int round_kill = 0;
        for (int i = 0; i < length; ++i)
        {
            if (data.camps[i].camp == (int)WorldManager.CAMP_DEFINE.UNION)
            {
                round_kill = data.camps[i].round_kill;
                break;
            }
        }
        return m_winParam - round_kill;
    }

    protected int CaculateMapZombie()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return -1;
        if (data.m_allPlayers == null) return -1;
        int result = 0;
        foreach (var player in data.m_allPlayers.Values)
        {
            if (player.camp != (int)WorldManager.CAMP_DEFINE.UNION && player.alive)
            {
                ++result;
            }
        }
        return result;
    }

    protected int CaculateRemainBoss()
    {
        SBattleInfo data = PlayerBattleModel.Instance.battle_info;
        if (data == null) return -1;
        if (data.m_allPlayers == null) return -1;
        int result = 0;
        foreach (var player in data.m_allPlayers.Values)
        {
            if (player.camp != (int)WorldManager.CAMP_DEFINE.UNION && player.actor_type == GameConst.ACTOR_TYPE_BOSS && player.alive)
            {
                ++result;
            }
        }
        return result;
    }

    virtual protected void ReNotice()
    {
        PlaySelfNoticePanel(m_configRound);
    }

    virtual public int total_round
    {
        get { return data != null ? 1 : 0; }
    }
    virtual public int stage_level
    {
        get { return data != null ? 1 : 0; }
    }
    virtual public int revivecoin
    {
        get { return data != null ? data.revivecoin : 0; }
    }
    virtual public int supplypoint
    {
        get { return data != null ? data.supplypoint : 0; }
    }

    virtual public int pveBuffShopIdStart
    {
        get { return m_pveBuffShopIdStart; }
    }
    virtual public int pvpBuffShopIdStart
    {
        get { return m_pvpBuffShopIdStart; }
    }
    virtual public int bossReviveBuffShopIdStart
    {
        get { return m_bossReviveBuffShopIdStart; }
    }

    virtual public int cureBuffShopIdStart
    {
        get { return m_cureBuffShopIdStart; }
    }

    virtual public int pveId
    {
        get { return data != null ? data.attack_buff : 0; }
    }

    virtual public int pveBuffShopIdSrv
    {
        get { return GetBuffShopId(GameConst.SHOP_ITEM_TYPE_PVEKBUFF, pveId); }
        //get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVEKBUFF, pveId); }
    }

    virtual public int pvpId
    {
        get { return data != null ? data.survival_buff : 0; }
    }
   

    virtual public int pvpBuffShopIdSrv
    {
        get { return GetBuffShopId(GameConst.SHOP_ITEM_TYPE_PVPBUFF, pvpId); }
        //get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_PVPBUFF, pvpId); }
    }

    virtual public int cureBuffId
    {
        get { return data != null ? data.cure_buff : 0; }
    }

    virtual public int cureBuffShopIdSrv
    {
        get { return GetBuffShopId(GameConst.SHOP_ITEM_TYPE_CUREBUFF, cureBuffId); }
        //get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_CUREBUFF, cureBuffId); }
    }

    //virtual public int bossId
    //{
    //    get { return m_myCampInfo != null ? m_myCampInfo.boss_buff_level : 0; }
    //}

    //virtual public int bossBuffShopIdSrv
    //{
    //    get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_BOSSBUFF, bossId); }
    //}

    virtual public int bossReviveId
    {
        get { return data != null ? m_bossReviveBuffShopIdStart : 0; }
    }

    virtual public int bossReviveBuffShopIdSrv
    {
        get { return GetBuffShopId(GameConst.SHOP_ITEM_TYPE_BOSSREVIVEBUFF, bossReviveId); }
        //get { return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, GameConst.SHOP_ITEM_TYPE_BOSSREVIVEBUFF, bossReviveId); }
    }

    public int GetBuffShopId(int buffType, int buffId)
    {
        return ConfigManager.GetConfig<ConfigGameShop>().GetGameShopIdByParam(WorldManager.singleton.gameRule, buffType, buffId);
    }

    virtual public int total_box
    {
        get { return data != null ? 1 : 0; }
    }

    virtual public int remain_box
    {
        get { return data != null ? 1 : 0; }
    }

    virtual public int msgtips_status
    {
        get
        {
            return CaculateMsgTipsStatus();
        }
    }

    virtual public int remain_zombie
    {
        get
        {
            return CaculateRemainZombie();
        }
    }

    virtual public int map_zombie
    {
        get
        {
            return CaculateMapZombie();
        }
    }

    virtual public int remain_boss
    {
        get
        {
            return CaculateRemainBoss();
        }
    }

    virtual public int monster
    {
        get
        {
            if (m_winType == DotaWinType.PICKKILL)
                return map_zombie;
            else if (m_winType == DotaWinType.KILLBOSS)
                return remain_boss;
            else
                return remain_zombie;
        }
    }

    virtual public List<DotaState> allDotaState
    {
        get
        {
            return m_allDotaState.Values.ToList<DotaState>();
        }
    }

    public int GetAllBuffEffect(int index, int type)
    {
        Logger.Log(string.Format("GetAllBuffEffect, index:{0}, type:{1}", index, type));
        float all = 0;
        ConfigBuff configBuff = ConfigManager.GetConfig<ConfigBuff>();
        var shopList = ConfigManager.GetConfig<ConfigGameShop>().GetGameShopList(WorldManager.singleton.gameRule, type);

        for (int i = 0; i <= index; i++)
        {
            all += configBuff.GetLine(shopList[i].Param).DamageRate;
        }
        return (int)(all * 100);
    }

    protected void Toc_fight_game_shop(toc_fight_game_shop proto)
    {
        int shopId = proto.shop_id;
        var config = ConfigManager.GetConfig<ConfigGameShop>().GetLine(shopId);
        switch (config.Type)
        {
            case GameConst.SHOP_ITEM_TYPE_BOSSREVIVEBUFF:
                {
                    if (proto.ret == 0)
                    {
                        TipsManager.Instance.showTips("领主已成功复活");
                    }
                }
                break;
            //case GameConst.SHOP_ITEM_TYPE_PVEKBUFF:
            //    if (proto.ret == 0)
            //    {
            //        int allbuffEffect = GetAllBuffEffect(pveBuffShopIdSrv - attackBuffShopIdStart, config.Type);
            //        if (UIManager.IsOpen<PanelBattle>())
            //        {
            //            string msg = "+" + allbuffEffect + "%";
            //            UIManager.GetPanel<PanelBattle>().ShowBuffIcon(true, panelScore.transform, new Vector2(-250, 302), msg);
            //        }
            //        if (UIManager.IsOpen<PanelBuyDotaPlayerBuff>())
            //        {
            //            UIManager.GetPanel<PanelBuyDotaPlayerBuff>().Refresh();
            //        }
            //        TipsManager.Instance.showTips("升级成功");
            //    }
            //    break;
            case GameConst.SHOP_ITEM_TYPE_PVEKBUFF:
            //case GameConst.SHOP_ITEM_TYPE_BOSSBUFF:
            case GameConst.SHOP_ITEM_TYPE_PVPBUFF:
            case GameConst.SHOP_ITEM_TYPE_CUREBUFF:
                if (proto.ret == 0)
                {
                    if (UIManager.IsOpen<PanelBuyDotaPlayerBuff>())
                    {
                        UIManager.GetPanel<PanelBuyDotaPlayerBuff>().Refresh();
                    }
                    TipsManager.Instance.showTips("升级成功");
                }
                break;
            case GameConst.SHOP_ITEM_TYPE_FILL_BULLET:
                if (proto.ret == 0)
                    break;
                if (proto.ret == -1)
                {
                    TipsManager.Instance.showTips("补给点不足");
                }
                else if (proto.ret == -3)
                {
                    TipsManager.Instance.showTips("弹药已满");
                }
                else
                {
                    TipsManager.Instance.showTips("失败");
                }
                StopBuy(true);
                break;
            case GameConst.SHOP_ITEM_TYPE_WEAPON:
                if (proto.ret == 0) //资源不足 -2:重复购买 -3:无需购买 -4:正在购买中(频繁操作)
                {
                    TipsManager.Instance.showTips("购买成功");
                }
                else if (proto.ret == -1)
                {
                    TipsManager.Instance.showTips("补给点不足");
                }
                else if (proto.ret == -4)
                {
                    TipsManager.Instance.showTips("频繁操作");
                }
                else
                {
                    TipsManager.Instance.showTips("购买失败");
                }
                break;
        }
    }

    Vector4 pos = new Vector4(-100, 250, -100, 270);
    Vector2 scale = new Vector2(0, 0.7f);
    protected void Toc_fight_dota_state(toc_fight_dota_state data)
    {
        BasePlayer bp = WorldManager.singleton.GetPlayerById(data.actor_id);
        Logger.Log(string.Format("toc_fight_dota_state, bp={0} pve:{1},pvo:{2},cure:{3}", (bp == null ? data.actor_id.ToString() : bp.ColorName), data.attack_buff, data.survival_buff, data.cure_buff));

        BasePlayer curPlayer = WorldManager.singleton.curPlayer != null ? WorldManager.singleton.curPlayer : MainPlayer.singleton;

        if (bp == curPlayer)
        {
            int supplyPointAdd = data.supplypoint - supplypoint;
            this.data = data;
            if (panelScore != null)
            {
                if (supplyPointAdd != 0) TipsManager.Instance.showTips("补给点  " + (supplyPointAdd > 0 ? "+" : "") + supplyPointAdd, pos, scale);
                panelScore.UpdateInfo(data.revivecoin, data.supplypoint, 1, 1, 1, 1, monster);
            }
            if (m_reNotice)
            {
                m_reNotice = false;
                ReNotice();
            }
        }

        if (bp == null)
        {
            Logger.Log("Toc_fight_dota_state  actor_id=" + data.actor_id + " is null");
            return;
        }

        if (curPlayer == null || curPlayer.Camp != bp.Camp)
        {
            return;
        }

        DotaState dotaState;
        if (!m_allDotaState.TryGetValue(data.actor_id, out dotaState))
        {
            dotaState = new DotaState();
            dotaState.player = bp;
            dotaState.AddRank();
            m_allDotaState.Add(data.actor_id, dotaState);
        }

        int pvpLevel = GetBuffShopId(GameConst.SHOP_ITEM_TYPE_PVPBUFF, data.survival_buff) - m_pvpBuffShopIdStart + 1;
        int pveLevel = GetBuffShopId(GameConst.SHOP_ITEM_TYPE_PVEKBUFF, data.attack_buff) - m_pveBuffShopIdStart + 1;
        int cureLevel = GetBuffShopId(GameConst.SHOP_ITEM_TYPE_CUREBUFF, data.cure_buff) - m_cureBuffShopIdStart + 1;

        if (pvpLevel > 0 && pvpLevel > dotaState.topBuffLevel)
        {
            dotaState.topBuffLevel = pvpLevel;
            dotaState.topBuffType = DotaBuffType.DotaBuff_PVP;
        }
        if (pveLevel > 0 && pveLevel > dotaState.topBuffLevel)
        {
            dotaState.topBuffLevel = pveLevel;
            dotaState.topBuffType = DotaBuffType.DotaBuff_PVE;
        }
        if (cureLevel > 0 && cureLevel > dotaState.topBuffLevel)
        {
            dotaState.topBuffLevel = cureLevel;
            dotaState.topBuffType = DotaBuffType.DotaBuff_CURE;
        }
    }

    //protected void Toc_fight_survival_use_revivecoin(toc_fight_survival_use_revivecoin proto)
    //{
    //    if (panelSurvival != null)
    //    {
    //        panelSurvival.OnUseRebornCoinRebornPlayer(proto);
    //    }
    //}

    protected void Toc_fight_dota_camp_info(toc_fight_dota_camp_info data)
    {
        m_dotaCampList = data.infos;
        m_myCampInfo = GetCampInfo(true);

        for (int i = 0, imax = m_dotaCampList.Length; i < imax; ++i)
        {
            var campInfo = m_dotaCampList[i];
            if (null != campInfo)
            {
                Logger.Log(string.Format("camp {0} boss buff level: {1}, revive time: {2}, revive date: {3}", campInfo.camp, campInfo.boss_buff_level, campInfo.boss_revive_time, campInfo.boss_revive_date));
                if (null != panelScore)
                {
                    panelScore.UpdateBossTime(campInfo.camp, campInfo.boss_revive_date);
                }
            }
        }
    }

    public p_dota_camp_info GetCampInfo(bool isMy = true)
    {
        if (null == m_dotaCampList || 0 == m_dotaCampList.Length || null == MainPlayer.singleton)
            return null;
        for (int i = 0; i < m_dotaCampList.Length; i++)
        {
            if (isMy)
            {
                if (m_dotaCampList[i].camp == MainPlayer.singleton.Camp)
                {
                    return m_dotaCampList[i];
                }
            }
            else
            {
                if (m_dotaCampList[i].camp != MainPlayer.singleton.Camp)
                {
                    return m_dotaCampList[i];
                }
            }
        }
        return null;
    }
}