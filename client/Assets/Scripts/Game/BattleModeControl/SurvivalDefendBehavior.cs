﻿using UnityEngine;


//生存塔房
class SurvivalDefendBehavior : SurvivalBehavior
{
    protected BasePlayer m_basePlayer;

    public SurvivalDefendBehavior()
    {
    }

    protected override void Awake()
    {
        base.Awake();
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_ATTACH, TryAttachBase);
        GameDispatcher.AddEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, TryDetachBase);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_ATTACH, TryAttachBase);
        GameDispatcher.RemoveEventListener<BasePlayer>(GameEvent.PLAYER_LEAVE, TryDetachBase);
    }

    protected override void Init()
    {
        if (m_panelScore == null)
            m_panelScore = UIManager.PopPanel<PanelBattleSurvivalDefendScore>();
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_GUNPOINT_PATH, AtlasName.Survival, "gun1", "DefendGun");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BUFFPOINT_PATH, AtlasName.Survival, "buff1", "DefendBuff");
        GenerateMapBuyPoints(SceneConst.SCENE_CHILD_BULLETPOINT_PATH, AtlasName.Battle, "logo_ammo_box", "DefendBullet");

        base.Init();

        if (m_basePlayer != null)
            m_panelScore.BindBasePlayer(m_basePlayer);
    }

    protected override void OnUpdateBattleState(SBattleState data)
    {
        base.OnUpdateBattleState(data);

        //if (WorldManager.singleton.isViewBattleModel) return;
        if (m_sceneGO == null || m_bs == null)
            return;
        
        switch (m_bs.state)
        {
            //case GameConst.Fight_State_GameInit:
            //    break;
            //case GameConst.Fight_State_GameStart:
            //    break;
            case GameConst.Fight_State_RoundStart:
                if (m_basePlayer != null && m_panelScore != null)
                    m_panelScore.BindBasePlayer(m_basePlayer);
                if (m_bs.round == 8)
                {
                    m_isShowTips = true;
                }
                PlaySelfNoticePanel(m_bs.round, 6);
                PlayNoticePanel();
                break;
            //case GameConst.Fight_State_RoundEnd:
            //    break;
            //case GameConst.Fight_State_GameOver:
            //    break;
            //default:
            //    break;
        }
    }

    public void TryAttachBase(BasePlayer basePlayer)
    {
        if (basePlayer != null && basePlayer.configData != null && basePlayer.configData.SubType == GameConst.PLAYER_SUBTYPE_BASE)
        {
            m_basePlayer = basePlayer;
            if (m_panelScore != null && m_panelScore != null)
                m_panelScore.BindBasePlayer(m_basePlayer);
        }
    }

    public void TryDetachBase(BasePlayer basePlayer)
    {
        if(basePlayer == m_basePlayer)
        {
            m_basePlayer = null;
            if (m_panelScore != null && m_panelScore != null)
                m_panelScore.BindBasePlayer(null);
        }
    }

    private bool m_isShowTips = true;
    public override void OnClickCuteBtn()
    {
        if (m_basePlayer == null)
            return;

        int reviveCoinCure = GetReviveCoinCure(m_basePlayer.Pid);

        if (reviveCoinCure < 0)
            return;

        if (revivecoin > 0)
        {
            if (WorldManager.singleton.isChallengeModeOpen)
            {
                if (m_isShowTips)
                {
                    UIManager.ShowTipPanel(string.Format("是否消耗一个复活币，为守护目标增加{0}血量", reviveCoinCure), () => { NetLayer.Send(new tos_fight_survival_use_revivecoin() { actor = m_basePlayer.PlayerId }); }, null, true, true);
                    m_isShowTips = false;
                }
                else
                {
                    NetLayer.Send(new tos_fight_survival_use_revivecoin() { actor = m_basePlayer.PlayerId }); 
                }
            }
            else
            {
                UIManager.ShowTipPanel(string.Format("是否消耗一个复活币，为守护目标增加{0}血量", reviveCoinCure), () => { NetLayer.Send(new tos_fight_survival_use_revivecoin() { actor = m_basePlayer.PlayerId }); }, null, true, true);
            }
            
        }
        else
        {
            UIManager.PopPanel<PanelBuy>(new object[] { ConfigManager.GetConfig<ConfigMall>().GetMallLine(5007) });
        }
    }


    override public int total_box
    {
        get { return -1; }
    }

    override public int remain_box
    {
        get { return -1; }
    }

}