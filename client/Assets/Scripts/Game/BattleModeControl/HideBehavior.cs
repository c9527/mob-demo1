﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 躲猫猫状态枚举
/// </summary>
public enum HideStateEnum
{
    /// <summary>
    /// 分配阵营
    /// </summary>
    HideStateInit = 0,
    /// <summary>
    /// 选择物件躲藏
    /// </summary>
    HideStateDisguise = 1,
    /// <summary>
    /// 人类开始寻找
    /// </summary>
    HideStateSeek = 2,
    /// <summary>
    /// 二次变身
    /// </summary>
    HideStateReDisguise = 3,
    /// <summary>
    /// 人类二次寻找
    /// </summary>
    HideStateReSeek = 4,
    /// <summary>
    /// 人类扫荡
    /// </summary>
    HideStateSweep = 5,
}

/// <summary>
/// 躲猫猫事件
/// </summary>
public class HideEvent
{
    public const string HIDE_FORBID_CAT = "hide_forbid_cat";       //禁止猫猫移动
    public const string HIDE_FORBID_HUMAN = "hide_forbid_human";   //禁止人类移动
}

public class HideBehavior : MonoBehaviour
{
    static public HideBehavior singleton;
    ConfigGameRuleLine m_ConfigGameRule;
    ConfigMapListLine m_ConfigMap;
    int m_iNoticeTimerId = -1;
    int m_voiceTimerId = -1;
    private bool m_bReNotice = false;
    private bool m_bIsPanelBattleInited = false;
    private bool m_bRePopSelectPanel = false;
    public static HideStateEnum m_eHideState;
    private Dictionary<HideStateEnum, Action> m_dicHideStateFunction;
    private toc_fight_select_disguise m_selectDisguiseData;
    public static TDSKillInfo tauntSkill = null;
    private int m_RoundNum;
    private int m_itemType = 1; //1:物件  2：buff
    string effect_str = EffectConst.UI_HIDE_BEIJISHAFANG;
    private GameObject m_autoFireGo;
    void Awake()
    {
        singleton = this;

        NetLayer.AddHandler(this);
        
        m_ConfigGameRule = ConfigManager.GetConfig<ConfigGameRule>().GetLine(GameConst.GAME_RULE_HIDE);

        //注册函数
        m_dicHideStateFunction = new Dictionary<HideStateEnum, Action>();
        m_dicHideStateFunction.Add(HideStateEnum.HideStateInit, DealHideStateInit);
        m_dicHideStateFunction.Add(HideStateEnum.HideStateDisguise, DealHideStateDisguise);
        m_dicHideStateFunction.Add(HideStateEnum.HideStateSeek, DealHideStateSeek);
        m_dicHideStateFunction.Add(HideStateEnum.HideStateReDisguise, DealHideStateReDisguise);
        m_dicHideStateFunction.Add(HideStateEnum.HideStateReSeek, DealHideStateReSeek);
        m_dicHideStateFunction.Add(HideStateEnum.HideStateSweep, DealHideStateSweep);

        //注册事件
        GameDispatcher.AddEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_INIT_DONE, DealHideState);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_WRONG_ATK, OnWrongAtk);
        GameDispatcher.AddEventListener(GameEvent.GAME_EXIT_BATTLE_SCENE, RemoveNotice);
        GameDispatcher.AddEventListener<bool>(HideEvent.HIDE_FORBID_CAT, ForbidCat);
        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, ReDealHideState);
        GameDispatcher.AddEventListener<bool>(GameEvent.MAINPLAYER_SETFREEZE, OnMainPlayerSetFreeze);
        GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_GAME_START_DIE, OnPlayerStartDie);
        tauntSkill = ConfigManager.GetConfig<ConfigSkill>().GetLine(630001);
    }

    private void OnPlayerStartDie()
    {
        if (MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel != null)
            {
                UIEffect.ShowEffect(battlePanel.TranRoomTip, effect_str, 2, new Vector2(0, -150), false);
            }
        }
    }

    void OnDestroy()
    {
        singleton = null;
        RemoveNotice();
        m_dicHideStateFunction.Clear();
        m_dicHideStateFunction = null;

        NetLayer.RemoveHandler(this);

        GameDispatcher.RemoveEventListener<SBattleState>(GameEvent.PROXY_PLAYER_FIGHT_STATE, OnUpdateBattleState);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_INIT_DONE, DealHideState);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_WRONG_ATK, OnWrongAtk);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_EXIT_BATTLE_SCENE, RemoveNotice);
        GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, ReDealHideState);
        GameDispatcher.RemoveEventListener<bool>(HideEvent.HIDE_FORBID_CAT, ForbidCat);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.MAINPLAYER_SETFREEZE, OnMainPlayerSetFreeze);
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_GAME_START_DIE, OnPlayerStartDie);
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        //GameDispatcher.RemoveEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnFightStatInfoUpdate);
    }

    private void OnUpdateBattleState(SBattleState arg)
    {
        m_RoundNum = arg.round;
    }

    void Start()
    {
        m_bIsPanelBattleInited = false;
        if (UIManager.IsOpen<PanelBattle>())
        {
            PanelBattle battlePanel = UIManager.GetPanel<PanelBattle>();
            if (battlePanel.IsInited()) 
                m_bIsPanelBattleInited = true;
        }

        if (!m_bIsPanelBattleInited)
        {
            m_bReNotice = true;
            GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
            return;
        }
        var configMapListLine = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString());
        //if (!PlayerBattleModel.Instance.battle_info.is_set_once)
        //{
        //    m_bReNotice = true;
        //    GameDispatcher.AddEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnFightStatInfoUpdate);
        //}
    }

    /// <summary>
    /// 躲猫猫状态处理
    /// </summary>
    private void DealHideState()
    {
        RemoveNotice();
        m_bReNotice = false;
        
        if(!SceneManager.singleton.battleSceneLoaded)
        {
            m_bReNotice = true;
            return;
        }
        if (m_ConfigMap == null)
        {
            var configMapList = ConfigManager.GetConfig<ConfigMapList>();
                m_ConfigMap = configMapList.GetLine(SceneManager.singleton.curMapId.ToString());
        }
        m_dicHideStateFunction[m_eHideState]();
    }

    /// <summary>
    /// 初始状态
    /// </summary>
    private void DealHideStateInit()
    {        
        PanelBattle panel = UIManager.GetPanel<PanelBattle>();
        if (MainPlayer.singleton != null)
        {
            ForbidCat(true);
        }

        if (m_bIsPanelBattleInited)
        {
            UIManager.GetPanel<PanelBattle>().StopNoticePanel();			
        }

        if (UIManager.IsOpen<PanelGetHideItem>())
        {
            UIManager.GetPanel<PanelGetHideItem>().HidePanel();
        }
        panel.OnShowPanelTaunt(false);
        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
        float initTime = 30;
        if (m_ConfigGameRule != null)
        {
            initTime = m_ConfigGameRule.Time_init;
        }
        float readyTime = initTime - run_time;
        readyTime = readyTime > initTime ? initTime : readyTime;
        if( m_RoundNum == 1)
        {
            NoticePanelProp prop1 = new NoticePanelProp();
            prop1.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(200, 118) });
            prop1.numIconList.Add(new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(186, 122) });
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "hide_init_camp", readyTime, new Vector2(0, 120), prop1);
        }
    }

    /// <summary>
    /// 一次躲藏
    /// </summary>
    private void DealHideStateDisguise()
    {
        if (MainPlayer.singleton != null)
        {
            bool isCat = MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT;
            string tipsName = "";
            NoticePanelProp prop = new NoticePanelProp();
            if (isCat)
            {
                ForbidCat(!MainPlayer.singleton.isDmmModel);
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(272, 132) });
                prop.numIconList.Add(new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(258, 135) });
                tipsName = "cathide";
            }
            else
            {
                ForbidHuman(true);
                MainPlayer.singleton.ForbidSceneCam = true;
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(145, 115) });
                prop.numIconList.Add(new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(130, 120) });
                tipsName = "hide_enter_game";
            }
            if (!m_bIsPanelBattleInited) return;
            UIManager.GetPanel<PanelBattle>().OnShowPanelTaunt(false);
            var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
            int seekTime = m_ConfigMap.Hide_time[1];
            float playTime = seekTime - run_time;
            playTime = playTime > m_ConfigMap.Hide_time[1] ? m_ConfigMap.Hide_time[1] : playTime;
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, tipsName, playTime, new Vector2(0, 120), prop);
        }
    }

    /// <summary>
    /// 一次寻找
    /// </summary>
    private void DealHideStateSeek()
    {
        if (MainPlayer.singleton != null )
        {
            if( MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT )
            {
                ForbidHuman(false);
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "findcat", 4, new Vector2(0, 120));
            }
            else
            {
                if (WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
                    UIManager.GetPanel<PanelBattle>().OnShowPanelTaunt(true);
            }
                
        }

        if (!m_bIsPanelBattleInited) return;
        bool isCat = (MainPlayer.singleton == null ||  MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT);
        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
        int reDisguiseTime = m_ConfigMap.Hide_time[1] + m_ConfigMap.Hide_time[2];
        float reDisguiseTimeLeft = reDisguiseTime - run_time;
        string titleName = "";

        SpriteProp spriteProp = new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(229, 128) };
        ImgNumProp numProp = new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(214, 133) };
        if (isCat == true)
        {
            titleName = "hide_redisguise";
            spriteProp.pos = new Vector2(205,115);
            numProp.pos = new Vector2(190,120);
        }
        else
        {
            titleName = "hide_redisguise_human";
        }

        NoticePanelProp prop = new NoticePanelProp();
        prop.symbolIconList.Add(spriteProp);
        prop.numIconList.Add(numProp);
        if (reDisguiseTimeLeft - 5 > 0)
        {
            m_iNoticeTimerId = TimerManager.SetTimeOut(reDisguiseTimeLeft - 5, () =>
                {
                    float cd = reDisguiseTime - Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
                    UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, titleName, cd, new Vector2(0, 120), prop);
                });
        }
        else
        {
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, titleName, reDisguiseTimeLeft, new Vector2(0, 120), prop);
        }
    }

    /// <summary>
    /// 二次躲藏
    /// </summary>
    private void DealHideStateReDisguise()
    {
        if (MainPlayer.singleton != null)
        {
            bool isCat = MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT;
            if (isCat)
            {
                if (MainPlayer.singleton.alive)
                {
                    
                    MainPlayer.singleton.SetFreeze(false);
                    if(FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning)
                    {
                        FreeViewPlayer.singleton.Stop();
                        GameDispatcher.Dispatch(GameEvent.UI_RESET_WATCH_TYPE);
                    }
                    MainPlayer.singleton.AttachSceneCamera();
                    MainPlayer.singleton.IsHideView = false;
                }
                else
                {
                    MainPlayer.singleton.IsHideView = true;
                }
            }
            else
            {
                UIManager.GetPanel<PanelBattle>().OnShowPanelTaunt(false);
                ForbidHuman(true);
                MainPlayer.singleton.ForbidSceneCam = true;
            }
        }

        if (!m_bIsPanelBattleInited) return;

        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
        int reSeekTime = m_ConfigMap.Hide_time[1] + m_ConfigMap.Hide_time[2] + m_ConfigMap.Hide_time[3];
        float playTime = reSeekTime - run_time;
        playTime = playTime > m_ConfigMap.Hide_time[4] ? m_ConfigMap.Hide_time[4] : playTime;

        bool isCat2 = (MainPlayer.singleton == null || MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT);
        Sprite sprite = null;
        Vector2 bgSize = Vector2.zero;
        float xPos = 0;
        xPos = 187;
        if (isCat2)
	    {
            sprite = ResourceManager.LoadSprite(AtlasName.Battle, "hide_human_awake");
        }
        else
        {
            sprite = ResourceManager.LoadSprite(AtlasName.Battle, "hide_human_awake1");
            bgSize = new Vector2(553f, 45f);
        }
        NoticePanelProp prop = new NoticePanelProp();
        prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(xPos+15, 115) });
        prop.numIconList.Add(new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(xPos, 120) });
        prop.bgSize = bgSize;
        UIManager.GetPanel<PanelBattle>().PlayNoticePanel(sprite, playTime, true, new Vector2(0, 120), prop);
        if (playTime > 10f)
            AudioManager.PlayFightUISound(AudioConst.voiceBeginCountdown);
    }

    /// <summary>
    /// 二次寻找
    /// </summary>
    private void DealHideStateReSeek()
    {
        string title = "";
        SpriteProp spriteProp = new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(162, 115) };
       ImgNumProp numProp = new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(147, 120) };
        if (MainPlayer.singleton != null && MainPlayer.singleton.Camp != (int)WorldManager.CAMP_DEFINE.CAT)
        {
            title = "hide_sweep_is_coming_human";
            spriteProp.pos = new Vector2(162, 131);
            numProp.pos = new Vector2(147, 135);
            ForbidHuman(false);
        }
        else
        {
            title = "hide_sweep_is_coming";
            if (WorldManager.singleton.curPlayer != null && WorldManager.singleton.curPlayer.isDmmModel == false)
            {
                UIManager.GetPanel<PanelBattle>().OnShowPanelTaunt(true);
            }
        }
        if (!m_bIsPanelBattleInited) return;
        
        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
        int sweepTime = m_ConfigMap.Hide_time[1] + m_ConfigMap.Hide_time[2] + m_ConfigMap.Hide_time[3] + m_ConfigMap.Hide_time[4];
        float sweepTimeLeft = sweepTime - run_time;
        NoticePanelProp prop = new NoticePanelProp();
        prop.symbolIconList.Add(spriteProp);
        prop.numIconList.Add(numProp);
        if (sweepTimeLeft > 5)
            m_iNoticeTimerId = TimerManager.SetTimeOut(sweepTimeLeft - 5, () => 
            {
                var temp = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, title, sweepTime - temp, new Vector2(0, 120), prop);
            });
        else
            UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, title, sweepTimeLeft, new Vector2(0, 120), prop);
    }

    /// <summary>
    /// 扫荡
    /// </summary>
    private void DealHideStateSweep()
    {
        if (!m_bIsPanelBattleInited) return;

        var run_time = Mathf.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
        UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "hide_sweep_has_comed", m_ConfigGameRule.Time_play - run_time, new Vector2(0, 120));
        
    }

    public static bool AutoFire = true;
    private int m_count;
    void Update()
    {
        bool isCat = (MainPlayer.singleton != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT);
        if (m_eHideState != null && isCat == false && m_eHideState == HideStateEnum.HideStateSweep)
        {
            if (GlobalBattleParams.singleton.GetBattleParam(GlobalBattleParams.KEY_AUTO_FIRE) > 0.1f) //自动开火
            {
                GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, true);
                m_count = 0;
            }
            else
            {
                if (AutoFire == false && m_count == 0)
                {
                    m_count++;
                    GameDispatcher.Dispatch<bool>(GameEvent.INPUT_FIRE, false);
                }
                
            }
            if (m_autoFireGo !=null)
                m_autoFireGo.TrySetActive(true);
        }
        else
        {
            if (m_autoFireGo != null)
                m_autoFireGo.TrySetActive(false);
        }
        if (isCat == true && MainPlayer.singleton != null)
        {
           if(MainPlayer.singleton.characterControler != null )
           {
               if (MainPlayer.singleton.alive == true && MainPlayer.singleton.characterControler.enabled == false)
                   MainPlayer.singleton.characterControler.enabled = true;
                   Logger.Log("MainPlayer.singleton.characterControler is false");
           }else
           {
               Logger.Log("MainPlayer.singleton.characterControler is null");
           }
        }
    }

    /// <summary>
    /// 重新处理状态
    /// </summary>
    private void ReDealHideState()
    {
        if (m_bReNotice && PlayerBattleModel.Instance.battle_state.state == GameConst.Fight_State_RoundStart)
        {
            DealHideState();
        }
    }

    /// <summary>
    /// 移除提示
    /// </summary>
    private void RemoveNotice()
    {
        if (m_iNoticeTimerId != -1)
        {
            TimerManager.RemoveTimeOut(m_iNoticeTimerId);
        }

        if (m_voiceTimerId != -1)
            TimerManager.RemoveTimeOut(m_voiceTimerId);
    }


    //事件监听
    private void OnWrongAtk()
    {
        if(m_eHideState != HideStateEnum.HideStateSweep)
        {
            NetLayer.Send(new tos_fight_atk_wrong());
        }
    }

    private void OnCreateBattlePanel()
    {
        m_bIsPanelBattleInited = true;
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnCreateBattlePanel);
        ReDealHideState();
        var panel = UIManager.GetPanel<PanelBattle>();
        if (panel != null)
        {
            m_autoFireGo = panel.m_autoFire;
            if (m_autoFireGo !=null)
                m_autoFireGo.TrySetActive(false);
        }
        //重新打开转盘
        if (m_bRePopSelectPanel && m_eHideState == HideStateEnum.HideStateDisguise && m_selectDisguiseData != null)
        {
            m_bRePopSelectPanel = false;
            Toc_fight_select_disguise(m_selectDisguiseData);
            m_selectDisguiseData = null;
        }
    }

    //private void OnFightStatInfoUpdate(SBattleInfo data)
    //{
    //    if (data == null)
    //        return;

    //    if (data.is_set_once)
    //    {
    //        GameDispatcher.RemoveEventListener<SBattleInfo>(GameEvent.PROXY_PLAYER_FIGHT_STAT_INFO, OnFightStatInfoUpdate);
    //        ReDealHideState();
    //    }
    //}

    private void OnMainPlayerSetFreeze(bool isFreeze)
    {
        SceneCameraController.singleton.SetEnableDrag(isFreeze);
        if (isFreeze)
        {
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 1;
                obj[1] = 2;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(1, 2);
            }
            if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                UIManager.GetPanel<PanelBattle>().SetAimShootImg("Aim1", "hideLock");
                
            }
        }
        else
        {
            if (!UIManager.IsOpen<PanelHideMsg>())
            {
                object[] obj = new object[2];
                obj[0] = 2;
                obj[1] = 2;
                UIManager.PopPanel<PanelHideMsg>(obj);
            }
            else
            {
                UIManager.GetPanel<PanelHideMsg>().updateview(2, 2);
            }
            if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                UIManager.GetPanel<PanelBattle>().SetAimShootImg("Aim1", "hideUnlock");
                
            }
        }
    }

    /// <summary>
    /// 禁止人类行动
    /// </summary>
    /// <param name="isForbid"></param>
    private void ForbidHuman(bool isForbid)
    {
        MainPlayer.singleton.ForbidMainCam = false;
        MainPlayer.singleton.ForbidSceneCam = false;
        MainPlayer.singleton.ForbidMove = isForbid;
        MainPlayer.singleton.ForbidFire = isForbid;
        MainPlayer.singleton.ForbidJump = isForbid;
        MainPlayer.singleton.ForbidLookAround = isForbid;
        if (MainPlayer.singleton.curWeapon != null && isForbid)
            MainPlayer.singleton.curWeapon.StopFire();
    }

    /// <summary>
    /// 禁止猫猫行动
    /// </summary>
    /// <param name="isForbid"></param>
    private void ForbidCat(bool isForbid)
    {
        MainPlayer.singleton.ForbidMainCam = false;
        MainPlayer.singleton.ForbidSceneCam = false;
        MainPlayer.singleton.ForbidMove = isForbid;
        MainPlayer.singleton.ForbidFire = isForbid;
        MainPlayer.singleton.ForbidJump = isForbid;
        MainPlayer.singleton.ForbidLookAround = isForbid;
    }

    //协议处理
    void Toc_fight_hide_state(toc_fight_hide_state data)
    {
        m_eHideState = (HideStateEnum)data.state;
        DealHideState();
    }

    void Toc_fight_select_disguise(toc_fight_select_disguise data)
    {        
        PanelBattle panel = UIManager.GetPanel<PanelBattle>();
        object[] obj = new object[data.disguise_list.Length];
        for (int i = 0; i < data.disguise_list.Length; i++)
        {
            obj[i] = data.disguise_list[i];
        }
        m_itemType = 1;
        if (m_bIsPanelBattleInited)
        {
            UIManager.PopPanel<PanelGetHideItem>( new object[]{ obj,m_itemType}, true, null);
            if (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.CAT)
            {
                ConfigMapListLine mapListConfigLine = null;
                var disguiseTime = 0;
                if ((mapListConfigLine = ConfigManager.GetConfig<ConfigMapList>().GetLine(SceneManager.singleton.curMapId.ToString())) != null)
                {
                    disguiseTime = mapListConfigLine.Hide_time[1];
                }
                else
                {
                    disguiseTime = 30;
                }
                int delta = disguiseTime - (int)Math.Max(PlayerBattleModel.Instance.battle_info.round_time, 0);
                NoticePanelProp prop = new NoticePanelProp();
                prop.symbolIconList.Add(new SpriteProp() { atlas = AtlasName.Battle, sprite = "hide_s", pos = new Vector2(262, 115) });
                prop.numIconList.Add(new ImgNumProp() { preName = "hide", atlas = AtlasName.Battle, align = 2, gap = -7, pos = new Vector2(254, 121) });
                UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.Battle, "hide_cat", delta, new Vector2(0, 120), prop);
            }
            if(panel!=null)
            {
                panel.OnShowPanelTaunt(false);
            }
            
            ForbidCat(true);
        }
        else
        {
            m_selectDisguiseData = data;
            m_bRePopSelectPanel = true;
            if (panel != null)
                panel.OnShowPanelTaunt(false);
        }
    }

    void Toc_fight_select_buff(toc_fight_select_buff data)
    {
        m_itemType = 2;
        PanelBattle panel = UIManager.GetPanel<PanelBattle>();
        object[] obj = new object[data.buff_list.Length];
        for (int i = 0; i < data.buff_list.Length; i++)
        {
            obj[i] = data.buff_list[i];
        }
        UIManager.PopPanel<PanelGetHideItem>(new object[] { obj, m_itemType }, true, null);
    }

    /// <summary>
    /// 人类是否开始找
    /// </summary>
    /// <returns></returns>
    public bool PlayerStartFind()
    {
        bool isFind = false;
        if(m_eHideState == HideStateEnum.HideStateSeek || m_eHideState == HideStateEnum.HideStateReSeek
            || m_eHideState == HideStateEnum.HideStateSweep)
        {
            isFind = true;
        }

        return isFind;
    }
}
