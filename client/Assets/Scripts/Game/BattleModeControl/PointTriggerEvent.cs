﻿using UnityEngine;
using System.Collections;

public class PointTriggerEvent : MonoBehaviour
{
    public string type = string.Empty;
    public int id = 0;
	// Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        GameObject goEnter = other.gameObject;

        GameDispatcher.Dispatch< GameObject, GameObject, string, int >(GameEvent.GAME_OBJ_ENTER_BURST_POINT,gameObject ,goEnter, type, id);
    }

    void OnTriggerExit(Collider other)
    {
        GameObject goLeaver = other.gameObject;

        GameDispatcher.Dispatch<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_LEAVE_BURST_POINT, gameObject, goLeaver, type, id);
    }

    void  OnTriggerStay(Collider other)
    {
        GameObject goLeaver = other.gameObject;

        GameDispatcher.Dispatch<GameObject, GameObject, string, int>(GameEvent.GAME_OBJ_STAYIN_BURST_POINT, gameObject, goLeaver, type, id);
    }
}
