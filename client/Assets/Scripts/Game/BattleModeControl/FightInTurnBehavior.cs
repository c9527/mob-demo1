﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

class FightInTurnBehavior : MonoBehaviour
{
    static public FightInTurnBehavior singleton;
    public readonly int BEFORE_START_SEC;
    public readonly int BEFORE_END_SEC;
    public readonly int NOTICE_BEFORE_END_SEC;

    private toc_fight_arena_begin_info m_data;
    private PanelBattleFightInTurnBegin panelBegin;
    private PanelBattleFightInTurnView panelView;
    private int timer = -1;
    public FightInTurnBehavior()
    {
        singleton = this;
        BEFORE_START_SEC = ConfigMisc.GetInt("arena_begin_time");
        BEFORE_END_SEC = ConfigMisc.GetInt("arena_end_time");
        NOTICE_BEFORE_END_SEC = ConfigMisc.GetInt("arena_visible_notice_time");
    }
    void Awake()
    {
        NetLayer.AddHandler(this);
        GameDispatcher.AddEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
    }

    void OnDestroy()
    {
        NetLayer.RemoveHandler(this);
        singleton = null;
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, toc_fight_actor_die>(GameEvent.PLAYER_KILLED_EX, OnPlayerDie);
    }

    void OnPanelBattleCreate()
    {
        GameDispatcher.RemoveEventListener(GameEvent.UI_CREATE_PANEL_BATTLE, OnPanelBattleCreate);
        if (isSelfFighter) return;
        var battle = UIManager.GetPanel<PanelBattle>();
        if (battle != null && battle.IsInited()) battle.ShowBattleSubInfo(false);
    }

    void ShowUIThroughTheWall(bool isPlay = true)
    {
        timer = -1;
        if (isPlay&&UIManager.IsBattle())
        {
            TipsManager.Instance.showTips("敌人已被标记，请注意屏幕中的红心");
        }
        BasePlayer leftFighter = WorldManager.singleton.GetPlayerById(leftFighterId);
        BasePlayer rightFighter = WorldManager.singleton.GetPlayerById(rightFighterId);
        ShowPosFlashingUI(leftFighter, isPlay);
        ShowPosFlashingUI(rightFighter, isPlay);
    }

    void ShowPosFlashingUI(BasePlayer fighter, bool isPlay)
    {
        //Logger.Error("trans: " + fighter.transform);
        if (fighter is OtherPlayer && fighter.transform != null)
        {
            (fighter as OtherPlayer).ShowPosFlashingUI(isPlay);
        }
    }

    void OnPlayerDie(BasePlayer shooter, BasePlayer dier, toc_fight_actor_die proto)
    {
        if (shooter == null || shooter.serverData == null || proto == null || proto.multi_kill <= 0) return;
        string msg = shooter.PlayerName + "已经一挑" + Util.ToBigNum(proto.multi_kill);
        //Logger.Error("---------------------"+msg);
        Vector2 pos = new Vector2(0, -100);
        NoticePanelProp prop = new NoticePanelProp() { msg = msg, txtPos = pos, bgSize = new Vector2(600, 50) };
        if (UIManager.IsOpen<PanelBattle>()) UIManager.GetPanel<PanelBattle>().PlayNoticePanel2(null, BEFORE_END_SEC, false, pos, prop);
    }

    public bool isSelfFighter
    {
        get { return leftFighterId == WorldManager.singleton.myid || rightFighterId == WorldManager.singleton.myid; }
    }

    public toc_fight_arena_begin_info beginData
    {
        get { return m_data; }
    }

    public int leftIndex
    {
        get { return m_data != null ? m_data.a_index - 1: -1; }
    }

    public int rightIndex
    {
        get { return m_data != null ? m_data.b_index - 1: -1; }
    }

    public int leftFighterId
    {
        get{return m_data != null && m_data.a_info.Length > 0 ? m_data.a_info[m_data.a_index-1].actor_id : 0;}
    }

    public int rightFighterId
    {
        get{return m_data != null && m_data.b_info.Length > 0 ? m_data.b_info[m_data.b_index-1].actor_id : 0;}
    }

    void Toc_fight_arena_begin_info(toc_fight_arena_begin_info data)
    {
        //Logger.Error("Toc_fight_arena_begin_info, a-len: " + data.a_info.Length + ", aIdx: " + data.a_index + ", b-len: " + data.b_info.Length + ", bIdx: "+data.b_index+", time: "+data.time);
        if(data == null || data.a_info.Length < data.a_index || data.b_info.Length < data.b_index)
        {
            Logger.Error("index out of array, ------- a-len: " + data.a_info.Length + ", aIdx: " + data.a_index + ", b-len: " + data.b_info.Length + ", bIdx: " + data.b_index);
            return;
        }
        m_data = data;

        GameDispatcher.Dispatch(GameEvent.PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED);

        if (timer != -1) TimerManager.RemoveTimeOut(timer);
        ShowUIThroughTheWall(false);
        timer = TimerManager.SetTimeOut(data.time - NOTICE_BEFORE_END_SEC, () => ShowUIThroughTheWall(true));
        
        if (panelView != null) panelView.HidePanel();

        var battle = UIManager.GetPanel<PanelBattle>();
        if (battle != null && battle.IsInited()) battle.ShowBattleSubInfo(isSelfFighter);

        panelBegin = UIManager.PopPanel<PanelBattleFightInTurnBegin>();
        TimerManager.SetTimeOut(BEFORE_START_SEC, () =>
            {
                if (UIManager.HasPanel<PanelBattleFightInTurnBegin>()) UIManager.GetPanel<PanelBattleFightInTurnBegin>().HidePanel();
                if (!isSelfFighter) panelView = UIManager.PopPanel<PanelBattleFightInTurnView>();
            });
        if (panelBegin != null) panelBegin.OnUpdate(data);
        if (panelView != null) panelView.OnUpdate(data);
    }

    void Toc_fight_arena_end_info(toc_fight_arena_end_info data)
    {
        //Logger.Error("Toc_fight_arena_end_info: " + data.camp);
        if (!UIManager.IsOpen<PanelBattle>() || MainPlayer.singleton == null) return;
        bool isWin = MainPlayer.singleton.Camp == data.camp;
        string spriteName = isWin ? "youwin" : "youlose";
        if (!isSelfFighter) spriteName = (isWin ? "campwin" + data.camp : "camplose" + MainPlayer.singleton.Camp);
        NoticePanelProp prop = new NoticePanelProp() { bgAltas = "" };
        UIManager.GetPanel<PanelBattle>().PlayNoticePanel(AtlasName.FightInTurn, spriteName, BEFORE_END_SEC, Vector2.zero, prop);
    }

}
