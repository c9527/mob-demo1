﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class AntiCheatManager
{
    static public bool Enable = true;

    public const int ErrorCode_CrouchStandHeightRadio = 1;
    //public const int ErrorCode_SceneCameraY = 2;
    //public const int ErrorCode_SceneCameraX = 3;
    //public const int ErrorCode_SceneCameraZ = 4;
    public const int ErrorCode_KnifeFireRate = 5;
    public const int ErrorCode_LastFireTime = 6;
    public const int ErrorCode_Speed = 7;
    public const int ErrorCode_MPCCMove = 8;
    public const int ErrorCode_Pos = 9;
    public const int ErrorCode_JumpMaxHeight = 10;
    public const int ErrorCode_JumpStartSpeed = 11;
    public const int ErrorCode_JumpStatus = 12;
	public const int ErrorCode_HitPos = 13;
	public const int ErrorCode_PCCheatProcess = 14;
    public const int ErrorCode_MPCCMoveDis = 15;
    public const int ErrorCode_TimeScale = 16;
    public const int ErrorCode_SceneCollider = 17;
    public const int ErrorCode_SceneColliderGO = 18;
    public const int ErrorCode_MPScale = 19;
    public const int ErrorCode_ColliderInfo = 20;
    public const int ErrorCode_FireDir = 21;
    //public const int ErrorCode_SceneShaderTransparent = 22;


    public const int ErrorCode_CheatApp = 30;
    public const int ErrorCode_ChaCtr_Height = 31;
    public const int ErrorCode_ChaCtr_Radius = 32;
    public const int ErrorCode_ChaCtr_Center = 33;
    public const int ErrorCode_ChaCtr_Slope = 34;
    public const int ErrorCode_CapCol_Height = 35;
    public const int ErrorCode_CapCol_Radius = 36;
    public const int ErrorCode_CapCol_Center = 37;

    public const int ErrorCode_SceneCamera_LocalPos = 40;
    public const int ErrorCode_SceneCameraSlot_LocalPos = 41;
    public const int ErrorCode_MPPosY = 42;
    public const int ErrorCode_SceneCamera_NearPlane = 43;
    public const int ErrorCode_SceneCamera_Occlusion = 44;



    static private Dictionary<string, string> DicCheatApp;

    static private EptFloat m_mpCCMinHeight;
    static private EptFloat m_mpCCMaxHeight;
    static private EptFloat m_mpCCMinRadius;
    static private EptFloat m_mpCCMaxRadius;
    static private EptFloat m_mpCCCenterMinY;
    static private EptFloat m_mpCCCenterMaxY;
    static private EptFloat m_mpSlopeLimit;

    static private VtfFloatCls m_tickBattle = new VtfFloatCls();
    static private VtfFloatCls m_tickCheatApp = new VtfFloatCls();
    static private VtfBoolCls m_dialogShow = new VtfBoolCls();
    static private VtfFloatCls m_tickCameraError = new VtfFloatCls();
    static private VtfFloatCls m_tickCameraLocalError = new VtfFloatCls();

    static private string m_initData = "";

    static public void Init()
    {
        ConfigItemRoleLine[] arrRole = ConfigManager.GetConfig<ConfigItemRole>().m_dataArr;
        ConfigItemRoleLine role = arrRole[0];

        for(int i=1; i<arrRole.Length; i++)
        {
            role = arrRole[i];

            if(i==1)
            {
                m_mpCCMinHeight = m_mpCCMaxHeight = role.HeightStand;
                m_mpCCMinRadius = m_mpCCMaxRadius = role.MPRadius;
                if (role.HeightStand > 0)
                    m_mpCCCenterMinY = m_mpCCCenterMaxY = role.HeightStand * 0.5f + GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET;
            }
            else
            {
                if(role.HeightStand > 0)
                {
                    if (m_mpCCMinHeight == 0 || role.HeightStand < m_mpCCMinHeight)
                        m_mpCCMinHeight = role.HeightStand;
                    if (m_mpCCMaxHeight == 0 || role.HeightStand > m_mpCCMaxHeight)
                        m_mpCCMaxHeight = role.HeightStand;

                    float ccY = role.HeightStand * 0.5f + GameSetting.PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET;
                    if (m_mpCCCenterMinY == 0 || ccY < m_mpCCCenterMinY)
                        m_mpCCCenterMinY = ccY;
                    if (m_mpCCCenterMaxY == 0 || ccY > m_mpCCCenterMaxY)
                        m_mpCCCenterMaxY = ccY;
                }
               
                if(role.MPRadius > 0)
                {
                    if (m_mpCCMinRadius == 0 || role.MPRadius < m_mpCCMinRadius)
                        m_mpCCMinRadius = role.MPRadius;
                    if (m_mpCCMaxRadius == 0 || role.MPRadius > m_mpCCMaxRadius)
                        m_mpCCMaxRadius = role.MPRadius;
                }
            }            
        }

        m_mpSlopeLimit = GlobalConfig.gameSetting.PlayerColliderSlope;
        m_initData = "mpCCMinHeight = " + m_mpCCMinHeight +
                   "\nmpCCMaxHeight = " + m_mpCCMaxHeight +
                   "\nCCMinRadius = " + m_mpCCMinRadius +
                   "\nCCMaxRadius = " + m_mpCCMaxRadius +
                   "\nCCMinCenterY = " + m_mpCCCenterMinY +
                   "\nCCMaxCenterY = " + m_mpCCCenterMaxY +
                   "\nSlopeLimit = " + m_mpSlopeLimit;
           

        //m_mpCCMinHeight = m_mpCCMinHeight;
        m_mpCCMaxHeight = m_mpCCMaxHeight + 0.5f;    // 范围放大一点，小数比较有问题
        //m_mpCCMinRadius = m_mpCCMinRadius;
        m_mpCCMaxRadius = m_mpCCMaxRadius + 0.5f;
        //m_mpCCCenterMinY = m_mpCCCenterMinY;
        m_mpCCCenterMaxY = m_mpCCCenterMaxY + 0.5f;
        m_mpSlopeLimit = GlobalConfig.gameSetting.PlayerColliderSlope;

        DicCheatApp = new Dictionary<string, string>();
        DicCheatApp.Add("huluxia.gametools", "葫芦侠");
        DicCheatApp.Add("sbtools.gamehack", "烧饼修改器");
        DicCheatApp.Add("paojiao.youxia", "泡椒修改器");
        DicCheatApp.Add("xxAssistant", "XX助手");
        DicCheatApp.Add("bamenshenqi", "八门神器");
    }


    //static public void FindBattleCheat(string msg, bool findCheatApp = false)
    //{
    //   Logger.Error("CheatMsg : " + msg + " InitData : \n" + m_initData);

    //    if (findCheatApp == false)
    //    {
    //        m_dialogShow.val = true;

    //        int timerID = TimerManager.SetTimeOut(15.0f, () =>
    //        {
    //            SendExitBattleMsg(msg);
    //        });

    //        UIManager.ShowTipPanel("检测到数据异常，请关闭具有游戏修改功能的App后再尝试（若使用外挂则封号）。如有问题可联系QQ800152012，把这个提示截图下来，找客服MM解决问题哦！\n" + msg, () =>
    //        {
    //            SendExitBattleMsg(msg);
    //            TimerManager.RemoveTimeOut(timerID);
    //        }, null, false, false, "确定");
    //    }
    //}

   

    //static private void SendExitBattleMsg(string msg)
    //{
    //    m_dialogShow.val = false;

    //    NetLayer.Send(new tos_room_leave());
    //    RoomModel.SetCustomRoomLeaveType(RoomLeaveType.Force);

    //   Logger.Error("Find Cheat Quit Battle \n" + msg);
    //}

    static public void Update()
    {

#if UNITY_EDITOR
        if (Enable == false)
            return;
#endif
        if (m_dialogShow.val)
            return;

        CheckCheatApp();

        UpdateInBattle();
    }

    static private void UpdateInBattle()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.released || MainPlayer.singleton.isAlive == false)
            return;

        if(Time.timeSinceLevelLoad - m_tickBattle.val > 3.0f)
        {
            m_tickBattle.val = Time.timeSinceLevelLoad;
            CheckMPScale();
            CheckMPCC();
            CheckMPCapsuleCollider();
            CheckSceneCamera();
            CheckSceneCameraCulling();
            CheckTimeScale();
            CheckSceneCollider();
            //CheckSceneShader();
            CheckMPPosY();
        }        
    }

    static public void CheckMPScale()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.released || MainPlayer.singleton.transform == null)
            return;

        if(MainPlayer.singleton.transform.localScale != Vector3.one)
        {
            CloseUDP(ErrorCode_MPScale, MainPlayer.singleton.transform.localScale.ToStrFloat());
        }
    }

    static public void CheckMPCC()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.characterControler == null)
            return;

        CharacterController cc = MainPlayer.singleton.characterControler;

        if (cc.height < m_mpCCMinHeight || cc.height > m_mpCCMaxHeight)
        {
            CloseUDP(ErrorCode_ChaCtr_Height, cc.height);
            return;
        }

        if (cc.radius < m_mpCCMinRadius || cc.radius > m_mpCCMaxRadius)
        {
            CloseUDP(ErrorCode_ChaCtr_Radius, cc.radius);
            return;
        }

        Vector3 ccCenter = cc.center;
        if (ccCenter.x != 0 || ccCenter.z != 0 || ccCenter.y < m_mpCCCenterMinY || ccCenter.y > m_mpCCCenterMaxY)
        {
            CloseUDP(ErrorCode_ChaCtr_Center, ccCenter.ToString());
            return;
        }

        if (Mathf.Abs(cc.slopeLimit - m_mpSlopeLimit) > 1)
        {
            CloseUDP(ErrorCode_ChaCtr_Slope, cc.slopeLimit);
            return;
        }
    }

    static public void CheckMPCapsuleCollider()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.characterControler == null)
            return;

        CapsuleCollider cc = MainPlayer.singleton.capsuleCollider;

        if (cc.height < m_mpCCMinHeight || cc.height > m_mpCCMaxHeight)
        {
            CloseUDP(ErrorCode_CapCol_Height, cc.height);
            return;
        }

        if (cc.radius < m_mpCCMinRadius || cc.radius > m_mpCCMaxRadius)
        {
            CloseUDP(ErrorCode_CapCol_Radius, cc.radius);
            return;
        }

        Vector3 ccCenter = cc.center;
        if (Mathf.Abs(ccCenter.x) > 1 || Mathf.Abs(ccCenter.z) >= 1 || ccCenter.y < m_mpCCCenterMinY || ccCenter.y > m_mpCCCenterMaxY)
        {
            CloseUDP(ErrorCode_CapCol_Center, ccCenter.ToString());
            return;
        }
    }

    static public void CheckSceneCamera()
    {
        if (MainPlayer.singleton == null || MainPlayer.singleton.isAlive == false || MainPlayer.singleton.isDmmModel == true || MainPlayer.singleton.gameObject == null ||
            SceneCamera.singleton.parent == null || MainPlayer.singleton.sceneCameraSlot == null || SceneCamera.singleton.parent != MainPlayer.singleton.sceneCameraSlot)
            return;

        if (SceneCamera.singleton.cameraShake != null && SceneCamera.singleton.cameraShake.isShake)
            return;


        if(Driver.m_platform == EnumPlatform.pc || Driver.m_platform == EnumPlatform.web)
        {
            if (SceneCamera.singleton.useOcclusionCulling == false)
            {
                CloseUDP(ErrorCode_SceneCamera_Occlusion, "sco", false);
                return;
            }
        }

        Vector3 slotLocalPos = MainPlayer.singleton.sceneCameraSlot.localPosition;
        Vector3 standPos = GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS;
        standPos.y *= MainPlayer.singleton.scale;

        if (MathTool.InRange(slotLocalPos, standPos, new Vector3(0.3f, 1f, 0.15f)) == false)
        {
            if (m_tickCameraError.val == 0)
                m_tickCameraError.val = Time.timeSinceLevelLoad;
            else if(Time.timeSinceLevelLoad - m_tickCameraError.val >= 6.0f)
            {
                m_tickCameraError.val = 0;
                CloseUDP(ErrorCode_SceneCameraSlot_LocalPos, slotLocalPos.ToStrFloat() + " " + GameSetting.MAIN_PLAYER_SCENECAMERA_LOCALPOS + " " + (int)Time.timeSinceLevelLoad + " EC:" + PanelBattle.ms_lastECall + " " 
                    + MainPlayer.singleton.scale + " " + MainPlayer.singleton.position + " " + (MainPlayer.singleton.sceneCameraSlot.parent == null ? "null" : MainPlayer.singleton.sceneCameraSlot.parent.name));
            }
        }

        if((SceneCamera.singleton.localPosition).sqrMagnitude >= 1.0f)
        {
            if (m_tickCameraLocalError.val == 0)
                m_tickCameraLocalError.val = Time.timeSinceLevelLoad;
            else if (Time.timeSinceLevelLoad - m_tickCameraLocalError.val >= 10.0f)
             {
                 m_tickCameraLocalError.val = 0;
                 CloseUDP(ErrorCode_SceneCamera_LocalPos, SceneCamera.singleton.localPosition.ToStrFloat() + " " + SceneCamera.singleton.localPosition.sqrMagnitude);
             }
        }
    }

    static public void CheckSceneCameraCulling()
    {
        if (SceneCamera.singleton == null)
            return;

        Camera camera = SceneCamera.singleton.camera;

        if (camera == null)
            return;

        if(camera.nearClipPlane > 1.0f)
        {
            CloseUDP(ErrorCode_SceneCamera_NearPlane, camera.nearClipPlane, false);
        }
    }

    static public void CheckTimeScale()
    {
        if(Time.timeScale > 1.01f)
        {
            CloseUDP(ErrorCode_TimeScale, (Time.timeScale * 100 + 1111).ToString());
        }   
    }

    static public void CheckSceneCollider()
    {
        int sceneID = SceneManager.singleton.curMapId;

        List<Collider> colliders = SceneManager.singleton.sceneCollider;
        if(colliders != null)
        {
            for(int i=0; i<colliders.Count; i++)
            {
                if(colliders[i].enabled == false)
                {
                    CloseUDP(ErrorCode_SceneCollider, sceneID + ";" + colliders[i].name);
                    return;
                }
            }
        }

        List<GameObject> gos = SceneManager.singleton.sceneColliderGO;
        if(gos != null)
        {
            for(int j=0; j<gos.Count; j++)
            {
                if(gos[j].activeInHierarchy == false)
                {
                    CloseUDP(ErrorCode_SceneColliderGO, sceneID + ";" + gos[j].name);
                    return;
                }
            }
        }
    }

    //static public void CheckSceneShader()
    //{
    //    if (SceneManager.singleton == null || SceneManager.singleton.sceneRenderer == null)
    //        return;

    //    Renderer[] arrRenderers = SceneManager.singleton.sceneRenderer;
    //    int cnt = 0;
    //    for(int i=0; i<arrRenderers.Length; i++)
    //    {
    //        if (arrRenderers[i].sharedMaterial.shader.name == GameConst.SHADER_LIGHTMAP)
    //            cnt++;
    //    }

    //    if (cnt < SceneManager.singleton.lightmapSceneObjCnt)
    //        CloseUDP(ErrorCode_SceneShaderTransparent, cnt + " " + SceneManager.singleton.lightmapSceneObjCnt, false);

    //}

    static public void CheckCheatApp()
    {
        m_tickCheatApp.val += Time.deltaTime;
        if (m_tickCheatApp.val < 8)
            return;

        m_tickCheatApp.val = 0;

        string name = GetCheatAppName();
        if (name != "")
            FoundCheatApp(name);
    }

    static public string GetCheatAppName()
    {
        string[] arrInfo = Util.GetAllProcessName();
        if(arrInfo != null)
        {
            for(int i=0; i<arrInfo.Length; i++)
            {
                foreach(KeyValuePair<string, string> kvp in DicCheatApp)
                {
                    if(arrInfo[i].Contains(kvp.Key))
                    {
                        return kvp.Value;
                    }
                }
            }
        }

        return "";
    }

    static public void CheckMPPosY()
    {
        if (GlobalConfig.serverValue.checkMPPosY && MainPlayer.singleton != null && !MainPlayer.singleton.released && MainPlayer.singleton.transform != null && MainPlayer.singleton.transform.parent != SceneManager.singleton.reclaimRoot)
        {
            if (SceneManager.singleton.curMapItemLine != null && SceneManager.singleton.curMapItemLine.maxY > 0 && MainPlayer.singleton.transform.position.y >= SceneManager.singleton.curMapItemLine.maxY)
            {
                NetLayer.Send(new tos_fight_enter_deathbox() { id = MainPlayer.singleton.PlayerId });
                UIManager.ShowTipPanel("数据异常，请重新开始战斗(可截图反馈客服QQ800152012) Msg = " + ErrorCode_MPPosY + " | " + SceneManager.singleton.curMapItemLine.maxY * 100 + " " + SceneManager.singleton.curMapItemLine.maxY * 100);
            }
        }
    }

    static public bool CheatCollider(BasePlayer kBP, Collider collider, Vector3 rayHitPos)
    {
        if (kBP == null || collider == null)
            return false;
        if (kBP.isAI)
            return false;
        // Check whethere the fire direction is right
        var tmpRayHitPos = new Vector3(rayHitPos.x, rayHitPos.y, rayHitPos.z);
        if (GlobalConfig.serverValue.checkFireDir && kBP.transform != null && kBP.gameObjectScale <= 1)
        {
            tmpRayHitPos.y = 0;
            Vector3 playerPos = kBP.transform.position;
            playerPos.y = 0;
            if (Vector3.SqrMagnitude(tmpRayHitPos - playerPos) >= 10 * 10)
            {
                GameObject root = collider.transform.root.gameObject;
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_FireDir, kBP.modelName + " " + root.name + " " + root.layer + " " + rayHitPos + " " + playerPos);
                return true;
            }
        }

        // Check whethere the center or size of collider is modified
        BoxCollider boxCollider = collider as BoxCollider;
        OtherPlayer op = kBP as OtherPlayer;
        if(GlobalConfig.serverValue.checkOPColliderInfo && boxCollider != null && op != null)
        {
            List<Vector3> list;
            op.colliderInfo.TryGetValue(boxCollider.name, out list);
            if(list != null)
            {
                if (Util.IsEqual(boxCollider.center, list[0], Vector3.one * 0.1f) == false || Util.IsEqual(boxCollider.size, list[1], Vector3.one * 0.1f) == false)
               {
                   AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_ColliderInfo, list[0] + " " + boxCollider.center.ToStrFloat() + " " + list[1] + " " + boxCollider.size.ToStrFloat() + " " + boxCollider.name + " " + kBP.gameObject.name);
                   return true;
               }
            }
        }
        
        // Check whethere the position of collider is modified
        Vector3 hitColliderPos = collider.transform.position;
        float dis = Vector3.SqrMagnitude(kBP.centerPos - hitColliderPos);
        float disSqrt = 7 * 7;
        if (kBP != null && kBP.scale > 1)
            disSqrt *= kBP.scale * kBP.scale;

        if (dis > disSqrt)
        {
            AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_HitPos, dis.ToString());
            return true;
        }

        // Check whethere the position of collider is modified
        hitColliderPos = collider.transform.position + collider.transform.TransformVector(boxCollider.center);
        if (!(WorldManager.singleton.isBigHeadModeOpen || WorldManager.singleton.isHideModeOpen) && collider.name == "Bip_Head")
        {
            dis = Vector3.SqrMagnitude(rayHitPos - hitColliderPos);
            disSqrt = Vector3.SqrMagnitude(boxCollider.size);

            if (kBP != null && kBP.scale > 1)
                disSqrt *= kBP.scale * kBP.scale;

            if (dis > disSqrt)
            {
                AntiCheatManager.CloseUDP(AntiCheatManager.ErrorCode_HitPos, dis );
                return true;
            }
        }
        


        return false;
    }

    static public void CloseUDP(int cheatErrorCode, string errorValue, bool showPanel = true)
    {
#if UNITY_EDITOR
        if (Enable == false)
            return;
#endif
        if (Driver.m_platform == EnumPlatform.ios)
            return;

        string msg = cheatErrorCode + " | " + errorValue;
        Logger.Error(msg);

        if (NetLayer.UDPConnected)
        {
#if !UNITY_EDITOR
            NetLayer.CloseUdp();
#endif
            if (showPanel)
                UIManager.ShowTipPanel("数据异常，请重新开始战斗(可截图反馈客服QQ800152012) Msg = " + msg);
        }
    }

    static public void CloseUDP(int cheatErrorCode, float errorValue, bool showPanel = true)
    {
        CloseUDP(cheatErrorCode, errorValue.ToString(), showPanel);
    }

    static private void FoundCheatApp(string appName)
    {
        Debug.LogError("FindCheatApp : " + appName);

        if (GlobalConfig.serverValue.cheatAppRelogin)
            CloseUDP(ErrorCode_CheatApp, "检测到具有游戏修改功能的App在运行，请关闭\" " + appName);
    }

}
