﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DropItemManager 
{
    static ConfigDropItem m_configDropItem = null;

    public static DropItemManager singleton = null;

    Dictionary<int, DropItem> m_dicSceneDropItems;//每个掉落物都有一个唯一的uid
    Dictionary<int, List<DropItem>> m_dicCacheDropItems; //缓存掉落物
    List<DropItem> m_listAllDropItems; //缓存掉落物

    private DropItem m_pickable_item; //当前可以捡的东西
    private ProgressBarProp prop = null;
 
    public DropItemManager()
    {
        if (singleton != null)
        {
            Logger.Error("DropItemManager is singleton!");
        }

        singleton = this;
        m_dicSceneDropItems = new Dictionary<int, DropItem>();
        m_dicCacheDropItems = new Dictionary<int, List<DropItem>>();
        m_listAllDropItems = new List<DropItem>();

        GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, OnEnterBattleScene);
        GameDispatcher.AddEventListener(GameEvent.GAME_EXIT_BATTLE_SCENE, OnExitBattleScene);

        GameDispatcher.AddEventListener<GameObject, DropItem>(GameEvent.DROPITEM_GAEMOBJECT_ENTER_PICK_UP_DISTANCE, OnEnterDropItemDistance);
        GameDispatcher.AddEventListener<GameObject, DropItem>(GameEvent.DROPITEM_GAEMOBJECT_LEAVE_PICK_UP_DISTANCE, OnLeaveDropItemDistance);

        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
    }


    #region 捡掉落物处理
    void OnEnterDropItemDistance(GameObject goEnter, DropItem which)
    {
        if (canPickUp(goEnter, which))
        {
            var info = ConfigManager.GetConfig<ConfigDropItem>().GetLine(which.dropID);

            if (info == null) 
            {
                Logger.Error("OnEnterDropItemDistance, no config, id: "+which.dropID);
                return;
            }

            m_pickable_item = which;

            Action callback = () =>
            {
                if (info != null && info.AutoPickUp == 1)
                {
                    //Logger.Error("OnEnterDropItemDistance: id: " + which.uID);
                    if (m_pickable_item != null && !m_pickable_item.isMoving) PickUpDropItem();
                }
                else
                {
                    if (!WorldManager.singleton.isDropGunModeOpen) return;
                    if (UIManager.IsOpen<PanelBattle>())
                    {
                        if (MainPlayer.singleton == null || !MainPlayer.singleton.isAlive) return;
                        if (MainPlayer.singleton.hasMainWeapon)
                        {
                            //if (MainPlayer.singleton.isHoldMainWeapon)
                            //{
                                if(IsPickable)
                                {
                                    //显示丢枪图标
                                    Sprite sprite = null;
                                    if (which.dropItemConfigLine != null && which.dropItemConfigLine.DropType == "weapon")
                                    {
                                        sprite = ResourceManager.LoadWeaponIcon(which.dropID);
                                    }
                                    ConfigItemWeapon itemWeapon = ConfigManager.GetConfig<ConfigItemWeapon>();
                                    string weaponName = "";
                                    if( itemWeapon != null )
                                    {
                                        var weaponLine = itemWeapon.GetLine(which.dropID);
                                        weaponName = weaponLine == null ? "" : weaponLine.DispName;
                                    }
                                    UIManager.GetPanel<PanelBattle>().ShowDropGunBtn(true);
                                    UIManager.GetPanel<PanelBattle>().ShowDropGunImg(true, sprite, weaponName);
                                }
                            //}
                        }
                        else
                        {
                            if (IsPickable) PickUpDropItem();
                        }
                        
                    }
                }
            };

            if (info.CD > 0)
            {
                if (UIManager.IsOpen<PanelBattle>())
                {
                    Vector2 pos = new Vector2(0, -115);
                    if (prop == null) prop = new ProgressBarProp() { OnTimeOut = callback, bgPos = pos, progressPos = pos, txtPos = pos };
                    UIManager.GetPanel<PanelBattle>().PlayProgressBar(AtlasName.Battle, "battle_pb_bg", AtlasName.Battle, "battle_pb_value", "收集中", info.CD, prop, "DropItem" + which.dropID);
                }
                return;
            }

            if (callback != null)
            {
                callback();
            }
        }
    }

    public void PickUpDropItem()
    {
        if (m_pickable_item != null)
        {
            GameDispatcher.Dispatch<DropItem>(GameEvent.DROPSYS_PRE_PICKUP, m_pickable_item);
            NetLayer.Send(new tos_fight_pickup_drop_item() { uid = PickableItemId });
        }
        m_pickable_item = null;
        if (UIManager.IsOpen<PanelBattle>()) UIManager.GetPanel<PanelBattle>().ShowDropGunImg(false);
    }

    private int PickableItemId
    {
        get { return m_pickable_item != null ? m_pickable_item.uID : -1; }
    }

    public bool IsPickable
    {
        get { return IsDropItemMainWeapon && !m_pickable_item.isMoving; }
    }

    public bool IsDropItemMainWeapon
    {
        get 
        {
            if (m_pickable_item == null) return false;
            ConfigItemWeaponLine wl = ItemDataManager.GetItemWeapon(m_pickable_item.dropID);
            if (wl == null) return false;
            return wl.SubType == GameConst.WEAPON_SUBTYPE_WEAPON1;
        }
    }

    public bool IsWeapon(int id)
    {
        var info = ConfigManager.GetConfig<ConfigDropItem>().GetLine(id);
        return info != null && info.DropType == "weapon";
    }

    public bool IsBomb(int id)
    {
        var info = ConfigManager.GetConfig<ConfigDropItem>().GetLine(id);
        return info != null && info.DropType == "bomb";
    }

    void OnLeaveDropItemDistance(GameObject goEnter, DropItem which)
    {
        if (MainPlayer.singleton == null)
        {
            return;
        }
        if (goEnter == MainPlayer.singleton.gameObject && m_pickable_item != null)
        {
            if (UIManager.IsOpen<PanelBattle>())
            {
                UIManager.GetPanel<PanelBattle>().ShowPickUpHander(false);
                UIManager.GetPanel<PanelBattle>().StopProgressBar("DropItem" + (m_pickable_item.dropID.ToString()));
                UIManager.GetPanel<PanelBattle>().ShowDropGunImg(false);
                m_pickable_item = null;
            }
        }
    }

    protected void OnPlayerDie(BasePlayer killer, BasePlayer who, int iLastHitPart, int weapon)
    {
        if (who == MainPlayer.singleton)
        {
            if (UIManager.IsOpen<PanelBattle>() && m_pickable_item != null)
            {
                UIManager.GetPanel<PanelBattle>().ShowPickUpHander(false);
                UIManager.GetPanel<PanelBattle>().StopProgressBar("DropItem" + (m_pickable_item.dropID.ToString()));
                UIManager.GetPanel<PanelBattle>().ShowDropGunImg(false);
                m_pickable_item = null;
            }
        }
    }

    bool canPickUp(GameObject goEnter, DropItem which)
    {
        return !(WorldManager.singleton.CheckGameTags(GameConst.GAME_RULE_GHOST) && MainPlayer.singleton != null && MainPlayer.singleton.Camp == 2 && which.dropID != 1901)
             && MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.alive
            && goEnter == MainPlayer.singleton.gameObject && which.canPickup(MainPlayer.singleton.Camp) && MainPlayer.singleton.isHero == false;
    }

    #endregion

    public void DropItems( int dropID,int uId,Vector3 dropPos, double dropTime=0 )
    {
        if(  m_configDropItem == null )
        {
            m_configDropItem = ConfigManager.GetConfig<ConfigDropItem>();
        }

        ConfigDropItemLine dropLine = m_configDropItem.GetLine( dropID );
        if (dropLine == null)
        {
            return;
        }

        int nowTime = TimeUtil.GetNowTimeStamp();
        int duration = nowTime - (int)dropTime;
        //Logger.Error("droptime: " + dropTime + ", now: " + nowTime + ", duration:　"+duration);
        if (dropTime > 0 && duration >= dropLine.ValidTime - 1) return;

        string[] itemModelPathList = dropLine.ModePath;
        for (int index = 0; index < itemModelPathList.Length; ++index)
        {
            DropItem dropItem = GetDropItem(dropID);
            dropItem.Init(uId, dropID, dropPos, itemModelPathList[index], dropLine);
            
            if( !m_dicSceneDropItems.ContainsKey(uId) )
            {
                m_dicSceneDropItems.Add(uId, dropItem);
            }
            else
                m_dicSceneDropItems[uId] = dropItem;
        }
        if (dropID == GameConst.DROP_ITEM_ID_C4)
        {
            if (BurstBehavior.singleton != null)
            {
                BurstBehavior.singleton.bomb_drop_point = dropPos.ToArray();
            }
        }
    }

    public DropItem GetDropItemByUID(int uId)
    {
        if (m_dicSceneDropItems.ContainsKey(uId))
        {
            DropItem dropItem = m_dicSceneDropItems[uId];
            return dropItem != null && !dropItem.IsDeactive() ? dropItem : null;
        }
        return null;
    }

    private DropItem GetDropItem(int dropID)
    {
        List<DropItem> listDrop = null;
        m_dicCacheDropItems.TryGetValue(dropID, out listDrop);
        if (listDrop == null)
        {
            listDrop = new List<DropItem>();
            m_dicCacheDropItems.Add(dropID, listDrop);
        }

        DropItem dropItem;
        int iRearIndex = listDrop.Count - 1;
        if (iRearIndex != -1)
        {
            dropItem = listDrop[iRearIndex];
            listDrop.RemoveAt(iRearIndex);
        }
        else
        {
            dropItem = new DropItem();
            m_listAllDropItems.Add(dropItem);
        }

        return dropItem;
    }

    public void DeactiveItem(DropItem dropItem)
    {
        if (dropItem == null)
            return;

        dropItem.enabled = false;

        List<DropItem> listDrop = null;
        m_dicCacheDropItems.TryGetValue(dropItem.dropID, out listDrop);
        if (listDrop == null)
        {
            listDrop = new List<DropItem>();
            m_dicCacheDropItems.Add(dropItem.dropID, listDrop);
        }

        listDrop.Add(dropItem);
        dropItem.Release();
    }

    List<DropItem> listToRemove = new List<DropItem>();
    public void Update()
    {
        listToRemove.Clear();
        foreach (var item in m_dicSceneDropItems.Values)
        {
            DropItem dropItem = item;
            listToRemove.Add(dropItem);
        }
        for (int i = 0; i < listToRemove.Count; i++)
        {
            DropItem dropItem = listToRemove[i];
            dropItem.Update();
            if (dropItem.IsDeactive())
            {
                DeactiveItem(dropItem);
                m_dicSceneDropItems.Remove(dropItem.uID);
            }
        }
    }

    internal void PickUpDropItem(toc_fight_del_drop_item proto)
    {
        int uId = proto.uid;

        if( !m_dicSceneDropItems.ContainsKey( uId ) )
            return;

        DropItem dropItem = m_dicSceneDropItems[uId];
        GameDispatcher.Dispatch<DropItem>(GameEvent.DROPSYS_DEL_DROP, dropItem);
        DeactiveItem(dropItem);
        m_dicSceneDropItems.Remove( uId );

        if (dropItem.dropItemConfigLine.DropType != "weapon" && dropItem.dropItemConfigLine.DropType != "bomb")
            AudioManager.PlayFightUISound(AudioConst.levelUp);

        if(uId == PickableItemId)
        {
            m_pickable_item = null;
            if (UIManager.IsOpen<PanelBattle>()) UIManager.GetPanel<PanelBattle>().ShowDropGunImg(false);
        }

        if (dropItem.dropID == GameConst.DROP_ITEM_ID_C4 && proto.from != 0)
        {
            if (BurstBehavior.singleton != null)
            {
                BurstBehavior.singleton.bomb_drop_point = null;
            }
            PlayerBattleModel.Instance.bombHolder = proto.from;
            Logger.Log("PickUpDropItem bombHolder = " + proto.from);
        }
    }

    void OnEnterBattleScene()
    {
        //DestoryDropItems();
        ReLoadDropItems();
    }

    void OnExitBattleScene()
    {
        DestoryDropItems();
    }

    void ReLoadDropItems()
    {
        foreach (var item in m_dicSceneDropItems.Values)
        {
            DropItem dropItem = item;
            dropItem.ReloadRes();
        }
    }

    public void ClearDropItems()
    {
        foreach (var item in m_dicSceneDropItems.Values)
        {
            DropItem dropItem = item;
            DeactiveItem(dropItem);
        }
        m_dicSceneDropItems.Clear();
    }

    public void DestoryDropItems()
    {
        for (int index = 0; index < m_listAllDropItems.Count; ++index)
        {
            DropItem dropItem = m_listAllDropItems[index];
            dropItem.Destory();
        }
        m_listAllDropItems.Clear();
        m_dicCacheDropItems.Clear();
        m_dicSceneDropItems.Clear();
    }

    static void Toc_fight_drop_list( toc_fight_drop_list dropList )
    {
        for (int index = 0; index < dropList.drop_list.Length; ++index)
        {
            DropInfo dropInfo = dropList.drop_list[index];
            //Logger.Error("listadd, id: " + dropList.drop_list[index].drop_id + ", uid: " + dropList.drop_list[index].uid);
            singleton.DropItems(dropInfo.drop_id, dropInfo.uid, new Vector3() { x = dropInfo.pos[0], y = dropInfo.pos[1] + 0.05f, z = dropInfo.pos[2] }, dropInfo.drop_time);
        }
    }

    static void Toc_fight_add_drop_item( toc_fight_add_drop_item addDropItem )
    {
        //Logger.Error("add, dropID: "+addDropItem.drop.drop_id+", uid: "+addDropItem.drop.uid);
        singleton.DropItems(addDropItem.drop.drop_id, addDropItem.drop.uid, new Vector3() { x = addDropItem.drop.pos[0], y = addDropItem.drop.pos[1] + 0.05f, z = addDropItem.drop.pos[2] });
    }

    static void Toc_fight_del_drop_item( toc_fight_del_drop_item delDropItem )
    {
        //Logger.Error("del, dropID: " + 0 + ", uid: " + delDropItem.uid);
        singleton.PickUpDropItem(delDropItem);
        PlayerBattleModel.Instance.bag_selected = true;
    }
}
