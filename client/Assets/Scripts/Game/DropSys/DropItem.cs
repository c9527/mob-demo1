﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropItem 
{
    private GameObject m_go;
    private Transform m_transform;
    private Transform m_transLogo;
    private SphereCollider m_collider;

    private Effect m_kEffect;

    private ConfigDropItemLine m_dropItemLine;

    private int m_uId;
    private int m_dropID;
    private Vector3 m_worldPos;
    private string m_modelPath;

    private float m_fActiveTime;
    private float m_lastTime;
    private bool m_update;
    private bool m_startLoad;
    private bool m_released;

    public bool isMoving = false;
    public bool isInited = false;

    private UIThroughTheWall uiThroughTheWall = null;

    public void Init(int uID, int dropID, Vector3 pos, string modePath, ConfigDropItemLine dropConfigLine)
    {
        m_uId = uID;
        m_dropID = dropID;
        m_worldPos = pos;
        m_modelPath = modePath;

        m_dropItemLine = dropConfigLine;
        m_fActiveTime = dropConfigLine.ValidTime;

        m_released = false;
        m_lastTime = 0;
        isMoving = false;
        
        if(m_go == null)
        {
            //GameDispatcher.AddEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, LoadRes);
            LoadRes();
        }
        else
        {
            if (m_go.GetComponent<DropItemEvent>() != null )
            {
                m_go.GetComponent<DropItemEvent>().enabled = true;
            }
            
            if (m_collider != null)
                m_collider.enabled = true;
            InitPos();
            AddEffect();
            OnInit();
        }
    }

    public void Destory()
    {
        Release();
        GameObject.Destroy(m_go);
        GameObject.Destroy(uiThroughTheWall);
        uiThroughTheWall = null;
        m_go = null;
    }

    public void Release()
    {
        m_released = true;
        m_update = false;
        isInited = false;

        RemoveEffect();

        //GameObject.Destroy(m_goLogo);
        //GameObject.Destroy(m_go);
        playUIThroughTheWall(false);
        //m_goLogo = null;
        //m_go = null;

        Reclaim();
        if (m_go != null && m_go.GetComponent<DropItemEvent>() != null)
        {
            m_go.GetComponent<DropItemEvent>().enabled = false;
        }

        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_INIT_DONE, MainPlayerInit);
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchCamera);
        //GameDispatcher.RemoveEventListener(GameEvent.GAME_BATTLE_SCENE_LOADED, LoadRes);
    }

    /// <summary>
    /// 场景没有加载完可能造成掉落物没有刷出来， 所以在场景加载完毕后重新检查一遍
    /// </summary>
    public void ReloadRes()
    {
        LoadRes();
    }

    private void LoadRes()
    {
        if (m_startLoad || SceneManager.singleton.battleSceneLoaded == false || m_released)
            return;

        m_startLoad = true;
        ResourceManager.LoadBattlePrefab(m_modelPath, (go, crc) =>
        {
            m_go = go;
            m_transform = go.transform;

            if (m_released)
            {
                Release();
                return;
            }
            //Logger.Error("load name: "+go.name);
            m_go.AddComponent<DropItemEvent>().SetDropItem(this);
            m_go.GetComponent<DropItemEvent>().enabled = true;
            SphereCollider collider = m_go.AddComponent<SphereCollider>();
            collider.isTrigger = true;
            collider.radius = m_dropItemLine.Radius;
            collider.enabled = true;

            m_collider = collider;

            InitPos();
            AddEffect();
            OnInit();
            m_update = true;           
        });
    }

    private void InitPos()
    {
        m_transform.parent = null;
        m_transform.position = SceneManager.GetGroundPoint(m_worldPos);
        m_transform.localPosition += new Vector3(0, m_dropItemLine.Height, 0);

        if (m_dropItemLine.EulerAngles.Length >= 3)
            m_transform.eulerAngles = new Vector3 { x = m_dropItemLine.EulerAngles[0], y = m_dropItemLine.EulerAngles[1], z = m_dropItemLine.EulerAngles[2] };
    }

    private void OnInit()
    {
        isInited = true;
        GameDispatcher.Dispatch<int>(GameEvent.DROPSYS_ADD_DROP, uID);
    }

    public bool canPickup(int camp)
    {
        var result = false;
         
        for (int i = 0; i < m_dropItemLine.Camps.Length; i++)
        {
            if (m_dropItemLine.Camps[i] == camp)
            {
                result = true;
                break;
            }
        }
        return result;
    }

    private void AddEffect()
    {
        if (WorldManager.singleton.curPlayer == null)
        {
            if (WorldManager.singleton.isViewBattleModel)
            {
                GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchCamera);
                
            }
            else if (MainPlayer.singleton == null || MainPlayer.singleton.gameObject == null)
            {
                GameDispatcher.AddEventListener(GameEvent.MAINPLAYER_ATTACH_SCENE_CAMERA, MainPlayerInit);
            }
            return;
        }

        if ((!string.IsNullOrEmpty(m_dropItemLine.EffectShowCamp) && m_dropItemLine.EffectShowCamp.IndexOf(WorldManager.singleton.curPlayer.Camp.ToString()) == -1))
            return;

        int index = -1;
        for (int i = 0; i < dropItemConfigLine.ModePath.Length; i++ )
        {
            if (m_modelPath == dropItemConfigLine.ModePath[i])
            {
                index = i;
                break;
            }
        }
        if (dropItemConfigLine.Effect.Length >= index + 1)
        {
            string effectPath = dropItemConfigLine.Effect[index];
            if(effectPath.IsNullOrEmpty() == false)
            {
                EffectManager.singleton.GetEffect<Effect>(effectPath, (kEffect) =>
                {
                    if (m_go == null)
                    {
                        EffectManager.singleton.Reclaim(m_kEffect);
                        m_kEffect = null;
                        return;
                    }

                    m_kEffect = kEffect;
                    kEffect.Attach(m_transform);
                    kEffect.Play();
                });
            }
        }
    }

    private void MainPlayerInit()
    {
        if (m_released)
            return;
        GameDispatcher.RemoveEventListener(GameEvent.MAINPLAYER_ATTACH_SCENE_CAMERA, MainPlayerInit);
        AddEffect();
    }

    private void SwitchCamera(int id)
    {
        if (m_released)
            return;
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchCamera);
        AddEffect();
    }

    private void RemoveEffect()
    {
        if (m_kEffect != null)
        {
            EffectManager.singleton.Reclaim(m_kEffect);
            m_kEffect = null;
        }
    }

    private void Reclaim()
    {
        if (m_go != null && SceneManager.singleton != null)
        {
            m_transform.SetParent(SceneManager.singleton.reclaimRoot);
            m_transform.transform.localPosition = Vector3.zero;
        }
    }

    public void Update( )
    {
        if (m_fActiveTime <= 0) return;
            m_fActiveTime -= Time.deltaTime;
        if (m_update == true && m_dropItemLine!=null)
        {
            m_update = false;
            playUIThroughTheWall(true);
        }
        if (uiThroughTheWall != null)
        {
            if ((Time.time - m_lastTime)>=3)
            {
                m_lastTime = Time.time;
                if (
                    MainPlayer.singleton == null
                    || m_dropItemLine == null
                    || (m_dropItemLine.UIiconRange > 0 && MainPlayer.singleton != null && (m_transform.position - MainPlayer.singleton.position).sqrMagnitude > Mathf.Pow(m_dropItemLine.UIiconRange, 2))
                    || (m_dropItemLine.DropId == 1020 && MainPlayer.singleton!=null && MainPlayer.singleton.serverData!=null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE)
                    || (m_dropItemLine.DropType == "zombie" && MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && (MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.ZOMBIE || MainPlayer.singleton.serverData.actor_type == GameConst.ACTOR_TYPE_HERO))
                    || (m_dropItemLine.DropId == GameConst.DROP_ITEM_ID_C4 && MainPlayer.singleton != null && MainPlayer.singleton.serverData != null && MainPlayer.singleton.Camp == (int)WorldManager.CAMP_DEFINE.UNION)
                    )
                {
                    playUIThroughTheWall(false);
                }   
                else
                {
                    playUIThroughTheWall(true);
                }
            }
        }
    }

    private void playUIThroughTheWall(bool isShow)
    {
        if (m_dropItemLine == null || string.IsNullOrEmpty(m_dropItemLine.LogoName) || (!string.IsNullOrEmpty(m_dropItemLine.LogoShowCamp) && WorldManager.singleton.curPlayer != null && m_dropItemLine.LogoShowCamp.IndexOf(WorldManager.singleton.curPlayer.Camp.ToString()) == -1) || !isShow)
        {
            if (uiThroughTheWall != null) uiThroughTheWall.stop();
            return;
        }
        if (isShow)
        {
            string logoName = m_dropItemLine.LogoName;
            if(uiThroughTheWall == null) uiThroughTheWall = UIThroughTheWall.createUI();
            ThroughTheWallProp prop = new ThroughTheWallProp() 
            { 
                imgProp = new RectTransformProp() { scale = m_dropItemLine.LogoScale != Vector3.zero ? m_dropItemLine.LogoScale : Vector3.one }, 
                starLevel = m_dropItemLine.LogoStarLevel, 
                starPos = new Vector2(0, -22f),
                distance = WorldManager.singleton.gameRuleLine.MapIconVisibleRange <= 0 ? float.MaxValue : WorldManager.singleton.gameRuleLine.MapIconVisibleRange,
            };
            uiThroughTheWall.play(AtlasName.Battle, logoName, m_go.transform, m_dropItemLine.LogoOffSet != Vector3.zero ? m_dropItemLine.LogoOffSet : new Vector3(0, 1.2f, 0f), prop);
        }
    }

    public bool IsDeactive()
    {
        if (m_fActiveTime <= 0)
        {
            return true;
        }
            
        return false;
    }

    public int uID
    {
        get
        {
            return m_uId;
        }
    }

    public int dropID
    {
        get
        {
            return m_dropID;
        }
    }

    public ConfigDropItemLine dropItemConfigLine
    {
        get { return m_dropItemLine; }
    }

    public GameObject gameObject
    {
        get{    return m_go;  }
    }

    public bool enabled
    {
        set
        {
            if (m_collider != null)
                m_collider.enabled = value;
        }
    }

    public Vector3 scene_pos
    {
        get { return m_worldPos; }
    }

    public bool released
    {
        get { return m_released; }
    }

}
