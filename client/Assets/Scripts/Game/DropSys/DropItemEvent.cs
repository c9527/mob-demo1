﻿using UnityEngine;
using System.Collections;

public class DropItemEvent : MonoBehaviour 
{
    DropItem m_ownerDropItem;
    bool isEnter = false;
    GameObject m_go;
    void OnTriggerEnter( Collider other )
    {
        m_go = other.gameObject;
        if (m_go.layer == LayerMask.NameToLayer(GameSetting.LAYER_NAME_MAIN_PLAYER) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<GameObject, DropItem>(GameEvent.DROPITEM_GAEMOBJECT_ENTER_PICK_UP_DISTANCE, m_go, m_ownerDropItem);
            isEnter = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        m_go = other.gameObject;
        if (m_go.layer == LayerMask.NameToLayer(GameSetting.LAYER_NAME_MAIN_PLAYER) && (other is CharacterController))
        {
            GameDispatcher.Dispatch<GameObject, DropItem>(GameEvent.DROPITEM_GAEMOBJECT_LEAVE_PICK_UP_DISTANCE, m_go, m_ownerDropItem);
            isEnter = false;
            
        }
    }

    void OnDisable()
    {
        if (isEnter == true)
        {
            if (m_go.layer == LayerMask.NameToLayer(GameSetting.LAYER_NAME_MAIN_PLAYER) && ((m_go.GetComponent<Collider>()) is CharacterController))
            {
                GameDispatcher.Dispatch<GameObject, DropItem>(GameEvent.DROPITEM_GAEMOBJECT_LEAVE_PICK_UP_DISTANCE, m_go, m_ownerDropItem);
            }
        }
        isEnter = false;
    }

    public void SetDropItem( DropItem dropItem )
    {
        m_ownerDropItem = dropItem;
    }
}
