﻿using UnityEngine;
using System.Collections;

public class CSVFileReceive : MonoBehaviour 
{

	// Use this for initialization
    void Start () 
    {
	}
	
    static void Toc_csv_receive(CDict proto)
    {
        string filename = proto.Str("filename");
        string filetext = proto.Str("filetext");
        // 根据不同的配置文件使用不同的事件通知
        if (filename == "ConfigWeapon")
        {
            ConfigItemWeapon cw = ConfigManager.GetConfig<ConfigItemWeapon>();
            cw.Load(filetext);
            GameDispatcher.Dispatch(GameEvent.TOOL_UPDATE_WEAPON_CONFIG_REFERENCE);
        }
        // 文件传输完成后显示提示消息
        GameDispatcher.Dispatch(GameEvent.UI_TIP_INFO, "配置表更新完毕", Color.blue, 20);
    }
}
