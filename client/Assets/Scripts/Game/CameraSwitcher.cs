﻿using UnityEngine;
using System.Collections.Generic;


public class CameraSwitcher
{
    private PlayerDeadCameraSwitchData m_kData;
    private float m_focusKillerTick = -1;
    private bool m_bStart;

    private Vector3 m_cameraTargetPos;
    private Vector3 m_cameraSourcePos;
    private Vector3 m_cameraRotCenterPos;

    private bool m_bIsArriveTarget;

    private bool m_bIsFirstPersonView;

    private int m_kPartnerID;

    private bool m_bIsShake;

    public CameraSwitcher(bool deadPlayer=true)
    {
        Init(deadPlayer);
    }

    private void Init(bool deadPlayer)
    {
        GameDispatcher.AddEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchCameraToPartner);
        GameDispatcher.AddEventListener<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, ChangeViewMode);
    }

    public void Release()
    {
        GameDispatcher.RemoveEventListener<int>(GameEvent.GAME_SWITCH_CAMERA_TO_PARTNER, SwitchCameraToPartner);
        GameDispatcher.RemoveEventListener<bool>(GameEvent.GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW, ChangeViewMode);
        DeadPlayer.Instance.Release();
    }

    public void StopSwitching()
    {
        m_bStart = false;
        m_focusKillerTick = -1;

        if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
            FPWatchPlayer.singleton.EndWatch();
    }

    public void PlayMainPlayerDeadSwitch(PlayerDeadCameraSwitchData kData)
    {
        //if (SceneManager.singleton.battleSceneLoaded == false)
        //    return;

        if (!FPWatchPlayer.singleton.isWatching && (MainPlayer.singleton == null || MainPlayer.singleton.transform == null))
            return;

        m_kData = kData;

        Vector3 selfPos = kData.pos;
        Vector3 killerDir;
        if (kData.killerPos == Vector3.zero)
            killerDir = kData.face;
        else
            killerDir = VectorHelper.NormalizeXZ(kData.killerPos - selfPos);

        SceneCamera.singleton.forward = killerDir;

        // Get Camera Vertical Height 
        RaycastHit kRH1;
        float cameraHeight = GameSetting.configGameSettingLine.WatchDeadCameraHeight;
        if (Physics.Raycast(selfPos, Vector3.up, out kRH1, cameraHeight, GameSetting.LAYER_MASK_BUILDING))
        {
            cameraHeight = kRH1.distance - GameSetting.CAMERA_RAYTEST_OFFSET;
            cameraHeight = cameraHeight <= 0 ? 0 : cameraHeight;
        }

        // Get Camera Horizontal Dis
        RaycastHit kRH2;
        Vector3 heightPos = selfPos + Vector3.up * cameraHeight;
        float cameraDis = GameSetting.configGameSettingLine.WatchDeadCameraDis;
        if (Physics.Raycast(heightPos, -killerDir, out kRH2, cameraDis, GameSetting.LAYER_MASK_BUILDING))
        {
            cameraDis = kRH2.distance - GameSetting.CAMERA_RAYTEST_OFFSET;
            cameraDis = cameraDis <= 0 ? 0 : cameraDis;
        }

        m_cameraRotCenterPos = heightPos;
        m_cameraSourcePos = heightPos;
        m_cameraTargetPos = heightPos - killerDir * cameraDis;

        if (!kData.isShowModel)
        {
            SceneCameraController.singleton.LookKiller(m_cameraTargetPos, m_kData.killer);
        }
        else
        {
            m_focusKillerTick = 0;
            DeadPlayer.Instance.SetData(kData);
            DeadPlayer.Instance.Show(null);

            m_bStart = true;
            m_bIsShake = kData.isShake;
            m_bIsArriveTarget = false;
        }

        // 如果killer == null 是不会设置相机位置的，这里补一下
        if(m_kData.killer == null)
        {
            SceneCamera.singleton.position = m_cameraTargetPos;
            SceneCamera.singleton.LookAt(selfPos + m_kData.face * 5.0f);
            if (m_bIsShake)
                SceneCamera.singleton.cameraShake.ReShake();
        }
    }    
    
    private void SwitchCameraToPartner(int partnerID)
    {
        m_kPartnerID = partnerID;

        //如果主角活着又不是躲猫猫模式，则退出
        if (MainPlayer.singleton != null && MainPlayer.singleton.alive && !WorldManager.singleton.isHideModeOpen)
            return;

        //StopSwitching();
        m_bStart = false;
        m_focusKillerTick = -1;
        
        BasePlayer kPlayer = WorldManager.singleton.GetPlayerById(partnerID);
        if (kPlayer != null && kPlayer.alive)
        {
            if (kPlayer is MainPlayer)
            {
                if (FPWatchPlayer.singleton != null && FPWatchPlayer.singleton.isWatching)
                    FPWatchPlayer.singleton.EndWatch();
                MainPlayer.singleton.AttachSceneCamera();
            }
            else
            {
                if (m_bIsFirstPersonView)
                {
                    FPWatchPlayer.singleton.StartWatch(kPlayer);
                }
                else
                {
                    if (kPlayer is FPWatchPlayer)
                    {
                        kPlayer = FPWatchPlayer.singleton.relatePlayer;
                        FPWatchPlayer.singleton.EndWatch();
                        kPlayer.AttachSceneCamera();
                    }
                    else
                    {
                        if( FreeViewPlayer.singleton != null && FreeViewPlayer.singleton.isRuning == false )
                        {
                            kPlayer.AttachSceneCamera();
                        }
                    }
                }
            }
        }
    }

    private void ChangeViewMode(bool isFirstPersonView)
    {
        m_bIsFirstPersonView = isFirstPersonView;
        SwitchCameraToPartner(m_kPartnerID);
    }

    public void Update()
    {
        if (m_bStart == false)
            return;

        m_focusKillerTick += Time.deltaTime;

        if(m_focusKillerTick >= GameSetting.configGameSettingLine.WatchDeadCameraFollowKillerTime)
        {
            m_focusKillerTick = -1;
            m_bStart = false;
            m_bIsShake = false;
            SceneCameraController.singleton.Stop();
        }

        if (m_kData.killer != null && m_kData.killer.serverData != null && m_kData.killer.alive && m_kData.killer.transform != null)
        {
            if (!m_bIsArriveTarget)
            {
                //照相机移动到目标点
                if (m_focusKillerTick <= GameSetting.configGameSettingLine.WatchDeadCameraMoveBackTime)
                {
                    SceneCamera.singleton.position = Vector3.Lerp(m_cameraSourcePos, m_cameraTargetPos, m_focusKillerTick / GameSetting.configGameSettingLine.WatchDeadCameraMoveBackTime);
                }
                else
                {
                    if (m_bIsShake)
                        SceneCamera.singleton.cameraShake.ReShake();
                    m_bIsArriveTarget = true;
                    SceneCamera.singleton.position = m_cameraTargetPos;
                    SceneCameraController.singleton.LookKiller(m_cameraRotCenterPos, m_kData.killer);
                }
                
            }
            
        }
    }
}