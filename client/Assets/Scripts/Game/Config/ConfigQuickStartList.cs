﻿using System;


/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/2 17:26:39
 * Description:
 * Update History:
 *
 **************************************************************************/
[Serializable]
public class ConfigQuickStartListLine
{
    public int Id;
    public string Name;
    public int IsHot;
    public int Type;
    public string GameRule;
    public string Icon;
    public ChannelTypeEnum ChannelType;
    public string[] ChannelSubType;
}
public class ConfigQuickStartList : ConfigBase<ConfigQuickStartListLine>
{
    private ConfigQuickStartList()
        : base("Id")
    {

    }
}