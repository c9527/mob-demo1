﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigLegionTerritoryLine
{
    public int ID;
    public int Legion;
    public int Occupy;
    public int[] NearTerritory;
    public string TerritoryName;
    public int Resource;
    public bool IsBase;
    public string TerritoryWorldName;
}

public class ConfigLegionTerritory : ConfigBase<ConfigLegionTerritoryLine>
{
    private ConfigLegionTerritory()
        : base("ID")
    {
    }
}
