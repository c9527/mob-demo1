﻿using System;

[Serializable]
public class ConfigItemArmorLine : ConfigItemLine
{
}

public class ConfigItemArmor : ConfigBase<ConfigItemArmorLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemArmor() : base("ID")
    {
    }
}
