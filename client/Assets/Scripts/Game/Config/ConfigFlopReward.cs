﻿using UnityEngine;
using System.Collections;
using System;


[Serializable]
public class ConfigFlopRewardLine
{
    public int ID;
    public string Rule;
    public int Round;
    public int Level;
    public string FreeReward;
    public string CostReward;
    public string[] RewardList;
    public string Name;
    public string RuleName;
}

public class ConfigFlopReward : ConfigBase<ConfigFlopRewardLine> {

	public ConfigFlopReward():base("ID")
    {

    }
}
