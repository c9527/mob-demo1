﻿using System;
using System.Collections.Generic;


[Serializable]
class TDStrengthenLevel
{
    public int ID;
    public int Group;
    public int State;
    public int Level;
    public int NeedExp;
    public string Consume;
    public int DispDamage;
    public int DispShootSpeed;
    public int DispWeight;
    public int DispAccuracy;
    public int DispRecoil;
    public int DispFireRange;
    public int DispPierce;
    public int DispClipCap;

    private KeyValue<int, int>[] _wakeup_conds;

    public KeyValue<int, int>[] getWakeupConds()
    {
        if (_wakeup_conds == null && !string.IsNullOrEmpty(Consume))
        {
            var temp = Consume.Split(';');
            _wakeup_conds = new KeyValue<int,int>[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                var values = temp[i].Split('#');
                _wakeup_conds.SetValue(new KeyValue<int,int>(){ key=int.Parse(values[0]),value=int.Parse(values[1]) }, i);
            }
        }
        return _wakeup_conds;
    }
}

class ConfigWeaponStrengthenLevel : ConfigBase<TDStrengthenLevel>
{
    public ConfigWeaponStrengthenLevel()
        : base("ID")
    {

    }

    //如果level超过当前星阶上限，会自动返回下一星阶第一等级信息
    public TDStrengthenLevel getStrengthenLevelInfo(int level, int state, int group = 1)
    {
        TDStrengthenLevel result = null;
        if (level < 1 || state < 0 || group< 1)
        {
            return null;
        }
        var info = getStateLevelInfo(state, group);
        if (info != null && info.Length>0)
        {
            if (level > info[info.Length - 1].Level)//越阶了，重新设置查找数据
            {
                level = 1;
                state += 1;
                info = getStateLevelInfo(state, group);
            }
            for (int i = 0; i < info.Length; i++)
            {
                if (info[i].Level == level)
                {
                    result = info[i];
                    break;
                }
            }
        }
        return result;
    }

    public TDStrengthenLevel[] getStateLevelInfo(int state, int group = 1)
    {
        List<TDStrengthenLevel> list = new List<TDStrengthenLevel>();
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (state == m_dataArr[i].State && m_dataArr[i].Group==group)
            {
                list.Add(m_dataArr[i]);
            }
        }
        return list.ToArray();
    }
}