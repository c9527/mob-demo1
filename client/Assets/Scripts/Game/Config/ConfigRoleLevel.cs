﻿using System;
using System.Collections.Generic;

[Serializable]

public class ConfigRoleLevelLine
{
    public int Level;
    public int NeedExp;        // 模式名字    
    public string Title;       // 等级称号               
    public string ArmyIcon;    // 军衔等级图标
    public string openFun;      //开启功能

    public string[] GetOpenFunList()
    {
        if (openFun == "")
        {
            return null;
        }
        return openFun.Split('|');
    }
}

public class ConfigRoleLevel : ConfigBase<ConfigRoleLevelLine>
{
    private ConfigRoleLevel()
        : base("Level")
    {

    }

    public ConfigRoleLevelLine GetLineMax()
    {
        if (m_dataArr.Length > 0 )
            return m_dataArr[m_dataArr.Length - 1];
        return  null;
    }

   
}