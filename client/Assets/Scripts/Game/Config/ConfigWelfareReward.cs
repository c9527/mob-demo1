﻿using System;

[Serializable]

public class ConfigWelfareRewardLine
{
    public int ID;
    public string RewardCond;
    public int CondParam;
    public int PreWelfare;
    public string CondDesc;             /// 奖励条件描述
    public PanelSubWelfare.WelfareTag Tag;                     /// 分类
    public string Reward;               /// 奖励的物品列表
    
}

public class ConfigWelfareReward : ConfigBase<ConfigWelfareRewardLine>
{
    private ConfigWelfareReward()
        : base("ID")
    {

    }
}