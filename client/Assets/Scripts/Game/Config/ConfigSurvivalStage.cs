﻿using UnityEngine;
using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]

public class ConfigSurvivalStageLine
{
    public int Id;
    public string Type;
    public int MapId;
    public int Level;
    //public string RewardId;
    public int[] RoundList;
    public string Icon;
    public string HeadIcon;
    public string RoleName;
}

public class ConfigSurvivalStage : ConfigBase<ConfigSurvivalStageLine>
{
    private Dictionary<string, ConfigSurvivalStageLine> m_stageData = new Dictionary<string, ConfigSurvivalStageLine>();

    private ConfigSurvivalStage()
        : base("Id")
    {
    }

    public ConfigSurvivalStageLine GetConfig(string type, int mapId, int level)
    {
        ConfigSurvivalStageLine result;
        string key = type + "_" + mapId + "_" + level;
        if (!m_stageData.TryGetValue(key, out result))
        {
            if (m_stageData.Count == 0)
            {
                for (int i = 0, imax = m_dataArr.Length; i < imax; ++i)
                {
                    ConfigSurvivalStageLine line = m_dataArr[i];
                    string _key = line.Type + "_" + line.MapId + "_" + line.Level;
                    m_stageData.Add(_key, line);
                }
                m_stageData.TryGetValue(key, out result);
            }
        }
        return result;
    }

    public int GetRoundId(string type, int mapId, int level, int round)
    {
        ConfigSurvivalStageLine stage = GetConfig(type, mapId, level);
        if (stage == null || stage.RoundList == null || round <= 0 || round > stage.RoundList.Length)
            return -1;
        return stage.RoundList[round - 1];
    }
}

