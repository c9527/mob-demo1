﻿using UnityEngine;
using System;
using System.Collections;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：
//创建者：HWL
//创建时间: 2016/3/26 11:07:45
///////////////////////////
[Serializable]

public class ConfigLegionPositionLine
{
    public int ID;
    public string PositionName;
}
public class ConfigLegionPosition : ConfigBase<ConfigLegionPositionLine>
{
    private ConfigLegionPosition()
        : base("ID")
    {

    }
}
