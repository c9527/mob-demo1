﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TDWeaponAwaken
{
    public int Id;
    public int WeaponId;
    public int AwakenLevel;
    public int SuperAttr;
    public int PromoteAttr;
    public int[] PromoteCon;
    public string Desc;
    public string PromoteDesc;
    public int Open;

    private TDWeaponAwakenGroup _group;
    private TDWeaponAwakenRandom _random;

    public TDWeaponAwakenGroup group
    {
        get 
        {
            if (_group == null)
            {
                _group = ConfigManager.GetConfig<ConfigWeaponAwakenGroup>().GetLine(WeaponId);
            }
            return _group; 
        }
    }

    public TDWeaponAwakenRandom random
    {
        get 
        {
            if (_random == null && group != null)
            {
                _random = ConfigManager.GetConfig<ConfigWeaponAwakenRandom>().GetAwakenRandom(group.GroupId, AwakenLevel);
            }
            return _random; 
        }
    }

    public int GetRandomValue(int times)
    {
        return random != null ? random.GetRandomValue(times) : 0;
    }

    public int GetMaxRandom()
    {
        return random != null ? random.GetMaxRandom() : 0;
    }
}

class ConfigWeaponAwaken : ConfigBase<TDWeaponAwaken>
{
    private Dictionary<int, List<TDWeaponAwaken>> _data_map = new Dictionary<int, List<TDWeaponAwaken>>();

    public ConfigWeaponAwaken()
        : base("Id")
    {

    }

    public TDWeaponAwaken[] GetWakeupData(int code)
    {
        List<TDWeaponAwaken> list = null;
        if (code > 0)
        {
            if (_data_map.ContainsKey(code))
            {
                list = _data_map[code];
            }
            else if(m_dataArr!=null && m_dataArr.Length>0)
            {
                foreach (var info in m_dataArr)
                {
                    if (info.WeaponId==code)
                    {
                        if (list == null)
                        {
                            list = _data_map[code] = new List<TDWeaponAwaken>();
                        }
                        list.Add(info);
                    }
                }
            }
        }
        return list != null ? list.ToArray() : null;
    }

    public TDWeaponAwaken GetWakeupLevelData(int code, int level)
    {
        TDWeaponAwaken result = null;
        var list = GetWakeupData(code);
        for (var i = 0; list != null && i < list.Length;i++ )
        {
            if (list[i].AwakenLevel == level)
            {
                result = list[i];
                break;
            }
        }
        return result;
    }
}
