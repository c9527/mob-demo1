﻿using System;
using System.Collections.Generic;

[Serializable]
class ConfigAccessoryCostLine
{
    public int Quality;
    public int UnequipCoin;
    public int BuybackCoin;
}

class ConfigAccessoryCost : ConfigBase<ConfigAccessoryCostLine>
{
    public ConfigAccessoryCost()
        : base("Quality")
    {

    }
}
