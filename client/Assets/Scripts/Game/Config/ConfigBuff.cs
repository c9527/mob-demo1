﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TDBuffInfo
{
    public int ID;
    public float DamageRate;
    public int MaxHP;
    public float Time;
    public int EffectType;
    public string MPEffectArg;
    public string OPEffectArg;
    public int IsCancelHitBack;
    public string Desc;
    public int IconValue;
    public string BuffIcon;
    public string BuffMask;
    public Vector2 BuffMaskOffset;
    public string BuffText;
    public string Name;
    public float Fov;
    public float DotaPveDamageRate;
    public float DotaPvpDamageRate;
    public float TeammateCure;
    public int LimitState;
}

public class ConfigBuff : ConfigBase<TDBuffInfo>
{
    public ConfigBuff()
        : base("ID")
    {

    }
}
