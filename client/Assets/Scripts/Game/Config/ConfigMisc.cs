﻿using System;

[Serializable]

public class ConfigMiscLine
{
    public string Key;
    public int ValueInt;
    public string ValueStr;
}

public class ConfigMisc : ConfigBase<ConfigMiscLine>
{
    static ConfigMisc msCfg;

    private ConfigMisc()
        : base("Key")
    {
    }

    public static void Load()
    {
        msCfg = ConfigManager.GetConfig<ConfigMisc>();
    }

    public static int GetInt(string key)
    {
        return msCfg.GetLine(key).ValueInt;
    }

    public static string GetValue(string key)
    {
        return msCfg.GetLine(key).ValueStr;
    }

    public static ItemInfo GetItemInfo(string key)
    {
        var str =  msCfg.GetLine(key).ValueStr;
        if (!string.IsNullOrEmpty(str))
        {
            var item = new ItemInfo();
            char splitChar = SPLIT_SUB_ARRAY;
            if (str.IndexOf(SPLIT_SUB_ARRAY) == -1)
                splitChar = SPLIT_ARRAY;
            var arr = str.Split(splitChar);
            if (arr.Length >= 2)
            {
                int.TryParse(arr[0], out item.ID);
                int.TryParse(arr[1], out item.cnt);
                if (arr.Length > 2)
                {
                    float.TryParse(arr[2], out item.day);
                }
            }
            return item;
        }
        return null;
    }
}
