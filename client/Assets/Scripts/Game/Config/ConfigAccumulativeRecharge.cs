﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigAccumulativeRechargeLine
{
    public int ID;
    public int Stage;
    public int EventId;
    public int Recharge;
    public int WelfareId;
    public int Days;
    public string EventDisp;
    public string StageDisp;
    public string Name;
    public string Txt;
}


public class ConfigAccumulativeRecharge : ConfigBase<ConfigAccumulativeRechargeLine>
{
    private ConfigAccumulativeRecharge()
        : base("ID")
    {
    }
}

	
