﻿using System;

[Serializable]

public class ConfigBattleParamSettingLine
{
    public string Key;
    public float value;        // 模式名字                       

}

public class ConfigBattleParamSetting : ConfigBase<ConfigBattleParamSettingLine>
{
    private ConfigBattleParamSetting()
        : base("Key")
    {

    }
}