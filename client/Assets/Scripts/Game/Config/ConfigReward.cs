﻿using System;

[Serializable]

public class ConfigRewardLine
{
    public string ID;
    public ItemInfo[] ItemList;     // 奖励的物品列表
    public string RewardName;//奖励的名字
    public int[] Probability; //物品概率
}

public class ConfigReward : ConfigBase<ConfigRewardLine>
{
    private ConfigReward()
        : base("ID")
    {

    }
}