﻿using UnityEngine;
using System.Collections;
using System;
/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：限时礼包数据
//创建者：HWL
//创建时间: 2016/7/02 11:07:39
///////////////////////////
[Serializable]

public class ConfigLimitGiftLine
{
    public string Id;
    public string GiftName;
    public string GiftList;
}
public class ConfigLimitGift : ConfigBase<ConfigLimitGiftLine>
{
    public ConfigLimitGift()
        : base("Id")
    {

    }
}
