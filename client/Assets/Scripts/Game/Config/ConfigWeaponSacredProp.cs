﻿using System;
using UnityEngine;

[Serializable]

public class ConfigWeaponSacredPropLine
{
    public int Id;
    public int ExtraCoin;
    public int ExtraExp;
    public int ExtraMedal;
    public string HeadIcon;
    public string KillIcon;
    public string[] PropDesc;
}

public class ConfigWeaponSacredProp : ConfigBase<ConfigWeaponSacredPropLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigWeaponSacredProp()
        : base("Id")
    {
    }
}
