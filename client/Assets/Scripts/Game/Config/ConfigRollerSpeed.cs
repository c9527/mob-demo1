﻿using System;
using System.Collections.Generic;

[Serializable]

public class ConfigRollerSpeedLine
{
    public int ID;
    public float MaxSpeed;
    public float AddSpeed;
    public float DropSpeed;
    public float MinSpeed;
    public float RollingTime;
}

class ConfigRollerSpeed : ConfigBase<ConfigRollerSpeedLine>
{
    public ConfigRollerSpeed()
        : base("ID")
    {

    }

}
