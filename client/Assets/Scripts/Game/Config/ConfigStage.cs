﻿using System;
using UnityEngine;

[Serializable]


public class ConfigStageLine
{
    public int StageId;
    public int MapId;
    public string GameRule;
    public int MyCamp;
    public RobotInfo[] MyRobot;
    public RobotInfo[] OtherRobot;
    public string FirstRewardID;
    public string RewardID;
    public string MissionName;
    public bool IsHard;
    /*
     * 描述位置样式：0右下角，1右上角，2左上角，3左下角
     * */
    public int DescStyle;
    public Vector2 Pos;
}
public class ConfigStage : ConfigBase<ConfigStageLine>
{
    private ConfigStage()
        : base("StageId")
    {
    }
}
