﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigMemberPrivilegeLine
{
    public int ID;
    public int Coin;
    public int Medal;
    public int Exp;
    public int TrainHour;
    public int MaxTrain;
    public int Diamond;
}
public class ConfigMemberPrivilege : ConfigBase<ConfigMemberPrivilegeLine>
{
    private ConfigMemberPrivilege()
        : base("ID")
    {
    }
}
