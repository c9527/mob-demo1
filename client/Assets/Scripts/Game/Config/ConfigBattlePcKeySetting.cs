﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/2/14 11:36:08
 * Description:
 * Update History:
 *
 **************************************************************************/
[Serializable]
public class ConfigBattlePcKeySettingLine
{
    public string Key;
    public string Value;
    public int Type;
    public string Name;
}
public class ConfigBattlePcKeySetting : ConfigBase<ConfigBattlePcKeySettingLine>
{
    public ConfigBattlePcKeySetting()
        : base("Key")
    {
        // 构造器
    }
}
