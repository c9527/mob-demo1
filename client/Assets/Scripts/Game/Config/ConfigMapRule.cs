﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigMapRuleLine
{
    public int Id;
    public int MapId;
    public float JumpMaxHeight;     //跳跃高度
    public float Jump2MaxHeight;    //二段跃高度
    public int[] CanJump2Camps;     //可以二段跳跃阵营
    public string[] CanUsingMode;   //地图对应的能开放的模式
    public int UseSpring;           //是否使用弹簧床
    public int UseStrongPoint;      //是否使用据点
    public int Overtime;
}

public class ConfigMapRule : ConfigBase<ConfigMapRuleLine>
{
    private Dictionary<string, ConfigMapRuleLine> m_data = new Dictionary<string,ConfigMapRuleLine>();

    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigMapRule()
        : base("Id")
    {

    }

    public ConfigMapRuleLine GetRule(int mapId, string gameRule)
    {
        ConfigMapRuleLine result = null;
        string key = mapId + gameRule;
        if (!m_data.TryGetValue(key, out result) && m_data.Count == 0)
        {
            for (int i = 0; i < m_dataArr.Length; ++i)
            {
                string[] CanUsingMode = m_dataArr[i].CanUsingMode;
                if (CanUsingMode != null && CanUsingMode.Length != 0)
                {
                    string _key = "";
                    for (int j = 0; j < CanUsingMode.Length; j++)
                    {
                        _key = m_dataArr[i].MapId + m_dataArr[i].CanUsingMode[j];
                        m_data.Add(_key, m_dataArr[i]);
                    }
                }
            }
            m_data.TryGetValue(key, out result);
        }
        return result;
    }

}
