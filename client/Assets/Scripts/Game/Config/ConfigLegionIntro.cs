﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigLegionIntroLine
{
    public string TabName;
    public string Content;
}

public class ConfigLegionIntro : ConfigBase<ConfigLegionIntroLine>
{
    private ConfigLegionIntro()
        : base("TabName")
    {
    }
}