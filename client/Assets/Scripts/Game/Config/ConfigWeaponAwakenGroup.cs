﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDWeaponAwakenGroup
{
    public int Id;
    public int WeaponId;
    public int ItemNum;
    public int GroupId;
}

class ConfigWeaponAwakenGroup : ConfigBase<TDWeaponAwakenGroup>
{
    public ConfigWeaponAwakenGroup()
        : base("WeaponId")
    {

    }
}
