﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDWeaponAwakenRandom
{
    public int Id;
    public int GroupId;
    public int AwakenLevel;
    public int[] Random;

    public int upper_times
    {
        get { return Random != null && Random.Length > 0 ? Random.Length : int.MaxValue; }
    }

    public int GetMaxRandom()
    {
        return Random != null && Random.Length>0 ? Random.Length - 1 : 0;
    }

    public int GetRandomValue(int times)
    {
        var result = 0;
        if (Random != null && Random.Length > 0)
        {
            result = times < Random.Length ? Random[times] : 100;
        }
        return result;
    }
}

class ConfigWeaponAwakenRandom : ConfigBase<TDWeaponAwakenRandom>
{
    private Dictionary<int, TDWeaponAwakenRandom[]> _data_map = new Dictionary<int, TDWeaponAwakenRandom[]>();

    public ConfigWeaponAwakenRandom()
        : base("Id")
    {
    }

    public TDWeaponAwakenRandom[] GetAwakenGroupRandom(int group)
    {
        TDWeaponAwakenRandom[] result = null;
        if (group > 0)
        {
            if (!_data_map.ContainsKey(group))
            {
                var temp = new List<TDWeaponAwakenRandom>();
                for (var i = 0;m_dataArr!=null && i < m_dataArr.Length;i++ )
                {
                    if (m_dataArr[i].GroupId==group)
                    {
                        temp.Add(m_dataArr[i]);
                    }
                }
                result = _data_map[group] = temp.ToArray();
            }
            else
            {
                result = _data_map[group];
            }
        }
        return result;
    }

    public TDWeaponAwakenRandom GetAwakenRandom(int group, int level)
    {
        TDWeaponAwakenRandom result = null;
        var list = GetAwakenGroupRandom(group);
        if (list != null && list.Length > 0)
        {
            foreach (var info in list)
            {
                if (info.AwakenLevel == level)
                {
                    result = info;
                    break;
                }
            }
        }
        return result;
    }
}
