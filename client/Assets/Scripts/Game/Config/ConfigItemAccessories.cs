﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigItemAccessoriesLine : ConfigItemLine
{
    public int Suit;
    public int Part;
    public string[] AddProps;
    public string[] DispProps;
    private ConfigAccessoryQualityLine[] _add_prop_data;

    public ConfigAccessoryQualityLine[] GetAddProp()
    {
        if (_add_prop_data == null)
        {
            var temp = new List<ConfigAccessoryQualityLine>();
            if(AddProps!=null && AddProps.Length>0)
            {
                var config = ConfigManager.GetConfig<ConfigAccessoryQuality>();
                for (var i = 0; i < AddProps.Length; i++)
                {
                    temp.Add(config.GetAddProp(ID, AddProps[i]));//null的时候也加入，方便AddProps和DispProps形成对应
                }
            }
            _add_prop_data = temp.ToArray();
        }
        return _add_prop_data;
    }

    public int GetQuality(int value,int index=0)
    {
        var result = 0;
        index = Math.Max(index, 0);
        var prop_data = GetAddProp();
        if (prop_data != null && index<prop_data.Length && prop_data[index] != null)
        {
            result = prop_data[index].GetQuality(value);
        }
        return result;
    }

    public string partTypeName
    {
        get
        {
            return ConfigItemAccessories.GetPartTypeName(Part);
        }
    }
}

public class ConfigItemAccessories : ConfigBase<ConfigItemAccessoriesLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemAccessories() : base("ID")
    {
    }

    static public string GetPartTypeName(int part)
    {
        var result = "";
        switch (part)
        {
            case 1:
                result = "枪口";
                break;
            case 2:
                result = "前握把";
                break;
            case 3:
                result = "弹夹";
                break;
            case 4:
                result = "瞄具";
                break;
        }
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="code"></param>
    /// <param name="value"></param>
    /// <returns>0-未鉴定的物品或者数值不在控制内，1-灰,2-绿,3-蓝,4-紫,5-黄</returns>
    public int GetQuality(int code,int value,int prop_index=0)
    {
        var info = GetLine(code);
        return info!=null ? info.GetQuality(value,prop_index) : 0;
    }
}
