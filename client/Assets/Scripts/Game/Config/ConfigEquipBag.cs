﻿using System;

[Serializable]
public class ConfigEquipBagLine
{
    public int BagId;
    public EnumMoneyType MoneyType;
    public int Price;
}

public class ConfigEquipBag : ConfigBase<ConfigEquipBagLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigEquipBag() : base("BagId")
    {
    }
}
