﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigAISkillLine
{
    public int ID;
    public int CD;
    public int[] ActionList;
    public string ActionLoop;
    public int TargetType;
    public int TargetParam;
    public int TargetNum;
    public int[] TargetRange;
    public int[] SkewingRange;
    public int EnemyInView;
    public int RandTarget;
    public string AlertType;
    public int AlertHeightType;
    public int ClientTime;
    public string Effect;
    public float[] AlertParam;
    public int CullingMode;
    public int OverWall;
    public Vector3 AlertEulerAngles;
    public int UseLaser;
}

public class ConfigAISkill : ConfigBase<ConfigAISkillLine>
{
    public ConfigAISkill()
        : base("ID")
    {

    }
}
