﻿using System;
using UnityEngine;

[Serializable]
public class ConfigMapItemLine
{
    public int ID;
    public int[] DMMItemID;
    public Vector3[] DMMItemPos;
    public Vector3[] DMMItemRot;
    public Vector3[] DMMItemScale;
    public string[] GameRules;
    public Vector2 posMin;
    public Vector2 posMax;
    public Vector2 size;
    public float maxY;
}

public class ConfigMapItem : ConfigBase<ConfigMapItemLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigMapItem()
        : base("ID")
    {

    }
}
