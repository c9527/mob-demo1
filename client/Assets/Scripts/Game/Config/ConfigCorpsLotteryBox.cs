﻿using System;
using System.Collections.Generic;

[Serializable]
class TDCorpsLotteryBox
{
    public int ID;
    public int CorpsLevel;
    public int Tribute;//解锁消耗
}

class ConfigCorpsLotteryBox : ConfigBase<TDCorpsLotteryBox>
{
    public ConfigCorpsLotteryBox()
        : base("ID")
    {

    }
}
