﻿using System;
using UnityEngine;

[Serializable]
public class ConfigSceneItemLine
{
    public int ID;
    public string Model;
    public int Material;
    public int Type;

}

public class ConfigSceneItem : ConfigBase<ConfigSceneItemLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigSceneItem()
        : base("ID")
    {

    }
}
