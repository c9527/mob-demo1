﻿using System;

[Serializable]

public class ConfigAchieveTagNameLine
{
    public int Tag;
    public string AchieveClassName;     /// 模块的名称
    public string AchieveClassTitle;    /// 模块title,在代码中使用
}

public class ConfigAchieveTagName : ConfigBase<ConfigAchieveTagNameLine>
{
    private ConfigAchieveTagName()
        : base("Tag")
    {

    }
}