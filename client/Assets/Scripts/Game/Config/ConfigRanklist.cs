﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigRanklistLine
{
    public string Name;
    public int Max;
    public int Realtime;
    public string RefreshType;
    public int[] RefreshTime;
    public int Special;
    public string DispName;
    public string DataName;
    public string ShortInfo;
    public string LongInfo;
}

public class ConfigRanklist : ConfigBase<ConfigRanklistLine>
{
    private ConfigRanklist()
        : base("Name")
    {
    }
}
