﻿using System;
using System.Collections.Generic;

[Serializable]
class TDCorpsLottery
{
    public int ID;
    public string RewardId;
    public string ItemName;
    public string Icon;
    /// <summary>
    /// Type: item or buff
    /// </summary>
    public string Type;
}

class ConfigCorpsLottery : ConfigBase<TDCorpsLottery>
{
    public ConfigCorpsLottery()
        : base("ID")
    {

    }
}
