﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigDefenseBattleLine
{
    public int ID;
    public string Type;
    public int MapId;
    public int Camp;
    public int CrystalId;
    public float[] CrystalPos;
    public string CrystalIcon;
    public int MonsterId;
    public float[] BornPos;
    public int Reborn;
}

public class ConfigDefenseBattle : ConfigBase<ConfigDefenseBattleLine>
{
    public ConfigDefenseBattle()
        : base("Camp")
    {

    }
}
