﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigAnnouncementLine
{
    public int Id;
    public string Name;
    public string Content;
}
public class ConfigAnnouncement : ConfigBase<ConfigAnnouncementLine>
{
    private ConfigAnnouncement()
        : base("Id")
    {
    }
}
