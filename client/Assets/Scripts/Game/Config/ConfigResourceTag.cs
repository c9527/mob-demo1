﻿using System;

[Serializable]
public class ConfigResourceTagLine
{
    public string Path;
    public EResType ResType;
    public bool IsSaveAssetbundle;
    public bool IsClearAfterLoaded;
    public int Tag;
    public bool IsCalcCrc;
}

public class ConfigResourceTag : ConfigBase<ConfigResourceTagLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigResourceTag() : base()
    {
    }
}
