﻿using System;

[Serializable]

public class ConfigGameRuleLine
{
    public string Type; 
    public string GameType; 
    public string[] Tags;   //分类标记组
    public string ParentGameRule; 
    public int[] AllCamp;           // 阵营数组
    public int CampPlayersMax;      // 阵营最大人数上限
    public int CampPlayersMin; //每阵营最小人数（开游戏后）
    public int CampPlayersDif;      // 开始游戏阵营最大相差人数
    public string WinType;          // 战场模式
    public int WinParam;         // 战场胜负判断参数
    public int WinRound;            // 胜利轮数
    public int MaxRound;            // 最大轮数
    public float ReviveTime;        // 死亡后复活时间
    public float GodTime;           // 复活后的无敌时长                                
    public float Time_init;         // 战场初始化时间                      
    public float Time_begin;        // 倒计时开始时间
    public int Time_play;         // 回合进行时间                                
    public float Time_done;         // 回合结束等待
    public float Time_over;         // 游戏结束等待
    public string Special_Item;     
    public float Place_Bomb_Time;
    public float Del_Bomb_Time;
    public float Bomb_Burst_Time;
    public float[] Bomb_Sound;        // 炸弹警示音效
    public int Mode_Class;          // 战斗模式分类，普遍模式，特殊模式
    public int ModeSort;
    public int Open;                // 是否对外开放，0：关闭模式，1：模式开放
    public string[] HideInPanels;        //某些界面上隐藏  
    public string Mode_Name;        // 模式名字      
    public string Mode_Name_ICON;   /// 模式名字对应的图片 
    public string[] Music;
    public string WeaponType;
    public int RescueWaitTime;
    public int RescueNeedTime;
    public int MaxRescueTimes;
    public int ShowAimHP;
    public int[] Channels;
    public int SurvivalLevelMax;
    public int SurvivalId;
    public int IsDropGunOpen;
    public int DamgeShowType;
    public int CanAutoShoot;
    public int ShowHp;
    public int UseWeaponDecelerate;
    public float KickOutTime;
    public int WatchFreeViewMode;
    public string[] PreloadRes;
    public int IsDeuceWin;
    public string DivideType;
    public int ModeType;   //1：PVE，2：PVP，3：其他/生化
    public string ModeSubIcon;
    public float MapIconVisibleRange;

    public int AllPlayersMax()
    {
        return CampPlayersMax*AllCamp.Length;
    }

    public bool IsShow(string panelName = null, int channel = 0)
    {
        if (Open == 0)
            return false;
        
        return IsCanUse(panelName, channel);
    }

    public bool IsCanUse(string panelName = null, int channel = 0)
    {
        if (panelName != null)
        {
            for (int i = 0; i < HideInPanels.Length; i++)
            {
                if (HideInPanels[i] == panelName)
                    return false;
            }
        }
        if (channel != 0)
        {
            var has = false;
            for (int i = 0; i < Channels.Length; i++)
            {
                if (Channels[i] == channel)
                {
                    has = true;
                    break;
                }
            }
            if (!has)
                return false;
        }
        return true;
    }

    public bool CheckGameTags(params string[] checkTags)
    {
        var result = false;
        var curTags = Tags;
        for (int i = 0; i < curTags.Length; i++)
        {
            for (int j = 0; j < checkTags.Length; j++)
            {
                if (curTags[i] == checkTags[j])
                {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
}

public class ConfigGameRule :  ConfigBase<ConfigGameRuleLine>
{
    private ConfigGameRule() : base("Type")
    {

    }

    public ConfigGameRuleLine GetLineByGameRule(string gameType)
    {
        for (int i = 0; i < this.m_dataArr.Length; i++)
        {
            if (m_dataArr[i].GameType == gameType)
            {
                return m_dataArr[i];
            }
        }
        return null;
    }
}