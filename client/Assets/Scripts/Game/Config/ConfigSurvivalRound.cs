﻿using UnityEngine;
using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]

public class ConfigSurvivalRoundLine
{
    public int Round;
    public string WinType;
    public string WinParam;
    public int PlayTime;
    public int MaxMonster;
    public int StoryID;
    public string TipsType;
    public string TaskTipsText;
    public string[] RoundEndCloseEffect;
    public string[] RoundStartOpenEffect;
    public float TransmitPlayer;
    public string TransmitEffect;
}

public class ConfigSurvivalRound : ConfigBase<ConfigSurvivalRoundLine>
{

    private ConfigSurvivalRound()
        : base("Round")
    {
    }

    public int GetRoundTotalTime(int round)
    {
        if (round < 0 || m_dataArr.Length <= round)
            return 0;
        return m_dataArr[round].PlayTime;
    }

}

