﻿using System;

[Serializable]

public class ConfigMasterRankGradeLine
{
    public int Id;
    public int Score;
    public string Grade;
    public int Gift;
}

public class ConfigMasterRankGrade : ConfigBase<ConfigMasterRankGradeLine>
{
    private ConfigMasterRankGrade()
        : base("Id")
    {

    }

    /// <summary>
    /// 获取配置最后一条数据
    /// </summary>
    /// <returns></returns>
    public ConfigMasterRankGradeLine GetLineMax()
    {
        if (m_dataArr.Length > 0)
            return m_dataArr[m_dataArr.Length - 1];
        return null;
    }
}