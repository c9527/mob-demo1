﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class TDLottery
{
    public int ID;
    public string ItemId;
    public string ItemName;
    public int IsRarity;
    /// <summary>
    /// 此数据中的extend字段用于标记抽奖信息中对用的奖励ID-ItemId
    /// </summary>
    private CDItemData _main_item;
    public int StartTime;
    public int EndTime;

    private void InitMainItem()
    {
        var info = ConfigManager.GetConfig<ConfigReward>().GetLine(ItemId).ItemList[0];
        _main_item = new CDItemData(info.ID);
        _main_item.sdata.item_cnt = info.cnt;
        _main_item.extend = ItemId;
        _main_item.overrideName = ItemName;
    }

    public CDItemData GetMainItem()
    {
        if (_main_item == null)
        {
            InitMainItem();
        }
        return _main_item;
    }

    public string GetMainName()
    {
        if (_main_item == null)
        {
            InitMainItem();
        }
        return _main_item.info != null ? _main_item.info.DispName : "???";
    }

    public string GetMainIcon()
    {
        if (_main_item == null)
        {
            InitMainItem();
        }
        return _main_item.info != null ? _main_item.info.Icon : null;
    }

    public bool InTime(int timestamp)
    {
        var start_stamp = StartTime > 0 ? StartTime : 0;
        var end_stamp = EndTime > 0 ? EndTime : int.MaxValue;
        return timestamp >= start_stamp && timestamp <= end_stamp;
    }
}

class ConfigLottery : ConfigBase<TDLottery>
{
    public ConfigLottery()
        : base("ID")
    {

    }
}
