﻿using UnityEngine;
using System.Collections;
using System;

public class ConfigExchangeLine
{
    public int Id;
    public string Type;
    public ItemInfo[] Item;
    public string ItemType;
    public int Times;
    public int Sign;
    public int Condition;
    public string ExchangeIcon;
}
public class ConfigExchange : ConfigBase<ConfigExchangeLine>
{
    private ConfigExchange()
        : base("Id")
    {

    }
}
