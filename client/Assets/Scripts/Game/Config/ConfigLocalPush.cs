﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigLocalPushLine
{
    public string ID;
    public string type;
    public string title;
    public string content;
    public string month;
    public string day;
    public string hour;
    public string minute;
    public string isActive;
}

public class ConfigLocalPush : ConfigBase<ConfigLocalPushLine>
{
    public ConfigLocalPush()
        : base("ID")
    {

    }
}