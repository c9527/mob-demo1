﻿using System;

[Serializable]

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 10:48:30
 * Description:
 * Update History:
 *
 **************************************************************************/

public class ConfigEventListLine
{
    public int Id;
    public string Name;
    public string VisibleStart;
    public string VisibleEnd;
    public string TimeStart;
    public string TimeEnd;
    public int[] WeekLimit;
    public string EventType;
    public string EventParam;
    public string Target;
    public string TargetParam;
    public string Reward;
    public string RewardName;
    public string RewardIcon;
    public int RewardTimes;
    public string ConditionDec;
    public string ProcessDec;
    public int ThemeId;
    public string GoPage;
    public string GoPageImg;
    public int[] GoPagePos;
    public int[] GoPageProTxtPos;
    public string TimeLimit;
    public string GoURL;
    public int[] ServerLimit;  
}

public class ConfigEventList : ConfigBase<ConfigEventListLine>
{
    private ConfigEventList()
        : base("Id")
    {

    }

    /// <summary>
    /// 获取配置最后一条数据
    /// </summary>
    /// <returns></returns>
    public ConfigEventListLine GetLineMax()
    {
        if (m_dataArr.Length > 0)
            return m_dataArr[m_dataArr.Length - 1];
        return null;
    }

    public bool IsInLotteryEvent()
    {
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].EventType == "lottery_discount")
            {
                var time = TimeUtil.GetNowTimeStamp();
                if (time >= int.Parse(m_dataArr[i].TimeStart) && time <= int.Parse(m_dataArr[i].TimeEnd))
                {
                    return true;
                }
            }
        }


        return false;
    }

}
