﻿using System;
using UnityEngine;

[Serializable]


public class ConfigStageSurvivalLine
{
    public int StageId;
    public int MapId;
    public string GameRule;
    public string MissionName;
    public bool IsHard;
    /*
     * 描述位置样式：0右下角，1右上角，2左上角，3左下角
     * */
    public int DescStyle;
    public Vector2 Pos;
    public string[] StarReward;//星数礼包
    public string[] ConDec;//条件达成信息
    public string RoomDec;//房间描述
}
public class ConfigStageSurvival : ConfigBase<ConfigStageSurvivalLine>
{
    private ConfigStageSurvival()
        : base("StageId")
    {
    }
}
