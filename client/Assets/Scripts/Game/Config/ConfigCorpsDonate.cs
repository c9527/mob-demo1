﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigCorpsDonateLine
{
    public int ID;
    public int Diamond;
    public int Coin;
    public int CorpsCoin;
    public int Tribute;
    public string Disp;
}

public class ConfigCorpsDonate : ConfigBase<ConfigCorpsDonateLine>
{
    private ConfigCorpsDonate()
        : base("ID")
    {
    }
}
