﻿using System;

[Serializable]
public class ConfigBattleUIControlPosLine
{
    public string key;
    public float anchorX;
    public float anchorY;
    public float fLocalScale;
    public float fAnchorMinX;
    public float fAnchorMinY;
    public float fAnchorMaxX;
    public float fAnchorMaxY;
    public float fWidth;
    public float fHeight;
    public float minLocalScale;
    public float maxLocalScale;
}

public class ConfigBattleUIControlPos : ConfigBase<ConfigBattleUIControlPosLine>
{
    //请使用ScriptableObject.CreateInstance创建对象
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigBattleUIControlPos()
        : base("key")
    {
    }
}
