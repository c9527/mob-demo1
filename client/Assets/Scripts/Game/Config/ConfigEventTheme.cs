﻿using System;

[Serializable]
/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2015/12/16 14:30:36
 * Description:
 * Update History:
 *
 **************************************************************************/
public class ConfigEventThemeLine
{
    public int ThemeId;
    public string Name;
    public string Status;
    public string PageType;
    public string AdvertPic;
    public int[] EventList;
    public string EndTime;
    public string EndTimeDec;
    public int[] SkipChannel;
    public string Device;
}


public class ConfigEventTheme : ConfigBase<ConfigEventThemeLine>
{
    public const string PAGETYPE_LIST = "List";
    public const string PAGETYPE_EXCHANGE = "Exchange";
    public const string PAGETYPE_PAGE = "Page";
    public const string PAGETYPE_INVITEBIND = "InviteBind";
    public const string PAGETYPE_INVITEBACK = "InviteBack";


    private ConfigEventTheme()
        : base("ThemeId")
    {

    }

    /// <summary>
    /// 获取配置最后一条数据
    /// </summary>
    /// <returns></returns>
    public ConfigEventThemeLine GetLineMax()
    {
        if (m_dataArr.Length > 0)
            return m_dataArr[m_dataArr.Length - 1];
        return null;
    }
}
