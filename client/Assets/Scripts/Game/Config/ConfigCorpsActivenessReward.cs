﻿using System;
using System.Collections.Generic;

[Serializable]
class TDCorpsActivenessReward
{
    public int ID;
    public int Activeness;
    public string Reward;
    public string Name;
}

class ConfigCorpsActivenessReward : ConfigBase<TDCorpsActivenessReward>
{
    public ConfigCorpsActivenessReward()
        : base("ID")
    {

    }
}
