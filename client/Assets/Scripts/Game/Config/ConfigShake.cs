﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigShakeLine
{
    public int ID;
    public float IncidenceRadius;
    public float Time;
    public float HorizontalLevel;
    public float HorizontalDecay;
    public float VerticalLevel;
    public float VerticalDecay;
    public float Frequency;
    public float MainCameraLevelScale;
    public int HorizontalRandom;
    public int VerticalRandom;
}

public class ConfigShake : ConfigBase<ConfigShakeLine>
{
    public ConfigShake()
        : base("ID")
    {

    }
}
