﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TDSKillInfo
{
    public int ID;
    public int CD;
    public string ICON;
    public string Effect;
    public string Param;
    public string ClientParam;
    public int CDClearType;
    public string[] UsableGameType;
    public int[] UnlimitedChannel;
    public string Mask;
    public Vector2 MaskOffset;
}

public class ConfigSkill : ConfigBase<TDSKillInfo>
{
    public ConfigSkill()
        : base("ID")
    {

    }
}
