﻿using System;

[Serializable]

public class ConfigPhasedPurchaseLine
{
    public int ID;
    public int HeroCardType;
    public int ReservedTime;
}

public class ConfigPhasedPurchase : ConfigBase<ConfigPhasedPurchaseLine>
{
    private ConfigPhasedPurchase()
        : base("ID")
    {

    }
}

 