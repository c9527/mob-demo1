﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigSkillEffectLine
{
    public int ID;
    public int Type;
    public float[] Range;
    public float HitBackDiatance;
    public float HitBackSpeed;
    public float Decelerate;
    public float DecelerateTime;
    public float Durance;
    public int PenetrateWall;
    public string[] Action;
    public string Effect;
    public float EffectDelay;
    public float EffectTime;
    public int EffectSlotType;
    public int Directional;
    public int Camp;
}

public class ConfigSkillEffect : ConfigBase<ConfigSkillEffectLine>
{
    public ConfigSkillEffect()
        : base("ID")
    {

    }
}
