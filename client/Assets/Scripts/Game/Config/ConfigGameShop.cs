﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigGameShopLine
{
    public int ShopId;
    public string GameType;
    public int Type;
    public int Param;
    public int Costs;
    public int Sort;
    public int Order;
}

class ConfigGameShop : ConfigBase<ConfigGameShopLine>
{
    //缓存
    static private Dictionary<string, List<ConfigGameShopLine>> s_CacheShopData = new Dictionary<string, List<ConfigGameShopLine>>();
    static private Dictionary<string, Dictionary<int, int>> s_CacheBuffData = new Dictionary<string, Dictionary<int, int>>();

    public ConfigGameShop()
        : base("ShopId")
    {

    }

    public int GetGameShopIdByParam(string GameType, int Type, int Param)
    {
        Dictionary<int, int> buffData;
        string key = GameType + Type;
        if (!s_CacheBuffData.TryGetValue(key, out buffData))
        {
            buffData = new Dictionary<int, int>();
            for (int i = 0; i < m_dataArr.Length; i++)
            {
                if (m_dataArr[i].GameType == GameType && (m_dataArr[i].Type == Type))
                {
                    buffData.Add(m_dataArr[i].Param, m_dataArr[i].ShopId);
                }
            }
            s_CacheBuffData.Add(key, buffData);
        }
        int shopId;
        if (buffData.TryGetValue(Param, out shopId))
        {
            return shopId;
        }
        return -1;
    }

    public int GetGameShopId(string GameType, int Type)
    {
        List<ConfigGameShopLine> result = GetGameShopList(GameType, Type);
        if (result != null && result.Count == 0)
            return -1;
        return result[0].ShopId;
    }

    public List<ConfigGameShopLine> GetGameShopList(string GameType, int Type = 0, int Sort = 0)
    {
        List<ConfigGameShopLine> result;
        string key = GameType + Type + Sort;
        if (s_CacheShopData.TryGetValue(key, out result))
            return result;

        result = new List<ConfigGameShopLine>();
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].GameType == GameType && (Type == 0|| m_dataArr[i].Type == Type) && (Sort == 0 ||  m_dataArr[i].Sort == Sort))
            {
                result.Add(m_dataArr[i]);
            }
        }

        s_CacheShopData.Add(key, result);

        return result;
    }

}
