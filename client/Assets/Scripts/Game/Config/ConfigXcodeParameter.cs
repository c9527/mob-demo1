﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigXcodeParameterLine
{
    public string Key;
    public string Content;
}

public class ConfigXcodeParameter : ConfigBase<ConfigXcodeParameterLine>
{
    static ConfigXcodeParameter xpCfg;
    public ConfigXcodeParameter()
        : base("Key")
    {

    }
}