﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDGameDrop
{
    public int ID;
    public string GameType;
    public int Time;
    public int DropId;
    public int DropPoints;
    public int Repeat;
}

class ConfigGameDrop : ConfigBase<TDGameDrop>
{
    public ConfigGameDrop()
        : base("ID")
    {

    }

    public TDGameDrop GetGameDropInfo(string rule, int drop_id)
    {
        TDGameDrop result = null;
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].GameType == rule && m_dataArr[i].DropId == drop_id)
            {
                result = m_dataArr[i];
                break;
            }
        }
        return result;
    }
}
