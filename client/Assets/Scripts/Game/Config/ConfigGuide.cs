﻿using System;
using UnityEngine;

[Serializable]
public class ConfigGuideLine
{
    public int GuideId;
    public int LimitLevel;
    public string ChapterNameStart;
    public string[] ChapterComplete;
    public int EndId;
    public GuideType Type;
    public bool ForbidMove;
    public bool ForbidFire;
    public bool ForbidSwitch;
    public bool ForbidLookAround;
    public GuideCompleteType CompleteType;
    public float MaskAlpha;
    public string Body;
    public string Layer;
    public float Time;
    public string Target;
    public Vector3 Position;
    public Vector3 Rotation;
    public Vector3 Scale;
    public Vector2 AnchorMin;
    public Vector2 AnchorMax;
    public bool ShowAward;
    public bool ShowSkipButton;
    public string SkipToChapterName;
    public string[] ClosePanel;
    public string Title;
    public string Content;
    public bool TryLeaveRoom;
    public int LogEventId;
}

public class ConfigGuide : ConfigBase<ConfigGuideLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigGuide() : base("GuideId")
    {
    }
}
