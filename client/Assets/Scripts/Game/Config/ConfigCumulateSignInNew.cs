﻿using UnityEngine;
using System.Collections;
using System;

public class ConfigCumulateSignInNewLine
{
    public int ID;
    public string Reward;
    public string Icon;
    public string Tip;
}
public class ConfigCumulateSignInNew : ConfigBase<ConfigCumulateSignInNewLine>
{
    private ConfigCumulateSignInNew()
        : base("ID")
    {

    }
}
