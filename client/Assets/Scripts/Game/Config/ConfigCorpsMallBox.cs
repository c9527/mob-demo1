﻿using System;
using System.Collections.Generic;

[Serializable]
class TDCorpsMallBox
{
    public int ID;
    public int CorpsLevel;
    public int Tribute;
}

class ConfigCorpsMallBox : ConfigBase<TDCorpsMallBox>
{
    public ConfigCorpsMallBox()
        : base("ID")
    {

    }
}
