﻿using System;
using UnityEngine;

[Serializable]
public class ConfigRoleConstLine
{
    public string key;
    //todo:定义配置字段

    public float autoAimTargetHeight;
    public float autoAimColliderRadius;
    public float autoAimColliderHeight;
    public float autoAimBackCenterDelay;
    public float autoAimBackCenterSpeed;
    public float autoAimPlayerRotateSpeed;
    public float autoAimCrosshairSpeed;
    public float autoAimTraceTime;
    public float autoAimCoolDownTime;
}

public class ConfigRoleConst : ConfigBase<ConfigRoleConstLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigRoleConst()
        : base()
    {
    }
}
