﻿using System;
using UnityEngine;

[Serializable]
public class ConfigQualityLine
{
    public string key;

    public float CullDisNear;
    public float CullDisFar;
    public float CullDisBuilding;
    public float CullDisSkyBox;
    public float CullDisPlayer;
    public float CullDisBattleEffect;
    public float CullDisSceneEffect;

    public int OrnamentVisible;
    public int SceneEffectMaxNum;

    public int LowBoneWeightDis;
}

public class ConfigQuality : ConfigBase<ConfigQualityLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigQuality()
        : base()
    {

    }
}
