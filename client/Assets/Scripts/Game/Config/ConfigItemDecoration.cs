﻿using System;
using UnityEngine;

[Serializable]
public class ConfigItemDecorationLine : ConfigItemLine
{
    public string Part;
    public string SlotPath;
    public Vector3 BoyPos = Vector3.zero;
    public Vector3 BoyRot = Vector3.zero;
    public Vector3 GirlPos = Vector3.zero;
    public Vector3 GirlRot = Vector3.zero;
    public string BoyModel;
    public string GirlModel;
}

public class ConfigItemDecoration : ConfigBase<ConfigItemDecorationLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemDecoration()
        : base("ID")
    {
    }
}
