﻿//using System;

/// <summary>
/// 道具配置基类
/// </summary>
public class ConfigItemLine
{
    public int ID;
    public string Type;
    public string SubType;
    public EnumItemTag[] Tags;
    public int RareType;
    public string DispName;
    public string Desc;
    public string Introduction;
    public string Icon;
    public string SmallIcon;
    public float IconRotation;
    public bool CanDel;
    public int AddRule;
    public string UseType;

    public string DispPart; //ItemRole ItemWeapon
    public int RoleCamp;    //ItemRole
    public string Param;    //ItemOther
    public string MPModel;  // 第一人称显示的模型
    public string OPModel;  // 第三人称显示的模型
    public string GetMethodGoto;    //获取途径链接
    public string GetMethodDesc;    //获取途径说明

    public bool CheckTags(EnumItemTag[] tags)
    {
        for (int i = 0; i < Tags.Length; i++)
        {
            for (int j = 0; j < tags.Length; j++)
            {
                if (Tags[i] == tags[j])
                    return true;
            }
        }
        return false;
    }
}

//public class ConfigItem : ConfigBase<ConfigItemLine>
//{
//    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
//    private ConfigItem() : base("ID")
//    {
//    }
//}
