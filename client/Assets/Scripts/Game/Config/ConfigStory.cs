﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigStoryLine
{
    public int ID;
    public int StoryID;
    public string Action;
    public string ActionParam;
    public string Sound;
}

public class ConfigStory : ConfigBase<ConfigStoryLine>
{
    //private static Dictionary<int, Queue<ConfigStoryLine>> m_dicStoryCommand = new Dictionary<int, Queue<ConfigStoryLine>>();

    public ConfigStory()
        : base("ID")
    {

    }


    public Queue<ConfigStoryLine> GetStoryCommand(int storyID)
    {
        Queue<ConfigStoryLine> result;

        //if (m_dicStoryCommand.TryGetValue(storyID, out result))
        //{
        //    return result;
        //}

        result = new Queue<ConfigStoryLine>();
        for (int i = 0, imax = m_dataArr.Length; i < imax; ++i )
        {
            ConfigStoryLine d = m_dataArr[i];
            if(d.StoryID == storyID)
            {
                result.Enqueue(d);
            }
        }

        //m_dicStoryCommand.Add(storyID, result);
        return result;
    }

}
