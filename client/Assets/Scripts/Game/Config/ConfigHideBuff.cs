﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigHideBuffLine
{
    public int ID;
    public int DiamondCost;
    public int Map;
    
}

public class ConfigHideBuff : ConfigBase<ConfigHideBuffLine>
{

    public ConfigHideBuff()
        : base("ID")
    {

    }
}
