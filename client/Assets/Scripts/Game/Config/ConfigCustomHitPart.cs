﻿using System;

[Serializable]
public class ConfigCustomHitPartLine
{
    public string Collider;
    public int Part;
}

public class ConfigCustomHitPart : ConfigBase<ConfigCustomHitPartLine>
{
    public ConfigCustomHitPart()
        : base("Collider")
    {

    }
}
