﻿using System;

[Serializable]
public class ConfigRoleNameLine
{
    public string firstName;
    public string secondName;
    public string thirdName;
}

public class ConfigRoleName : ConfigBase<ConfigRoleNameLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigRoleName() : base()
    {
    }
}
