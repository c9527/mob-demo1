﻿using System;
using System.Collections.Generic;

[Serializable]


class ConfigJokeModeGroupLine
{
    public int GroupId;
    public string[] ModeGroup;
    public string GroupName;
}

class ConfigJokeModeGroup: ConfigBase<ConfigJokeModeGroupLine>
{
    public ConfigJokeModeGroup()
        : base("ID")
    {

    }
}
