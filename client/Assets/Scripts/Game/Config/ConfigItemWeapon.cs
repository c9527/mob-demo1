﻿using System;
using UnityEngine;

[Serializable]
public class ConfigItemWeaponLine : ConfigItemLine
{
    public int Class;
    public int SlotType;
    public int CanPickup;
    public int CanDrop;
    public int RepairPrice;
    public EnumMoneyType RepairMoneyType;
    public int BattleMode;
    public int Droppable;
    public int Repairable;
    public int Weight;
    public float RecoilDis;
    public float RecoilRecoverSpeed;
    public Vector3[] MPPos;
    public Vector3[] MPRot;
    public Vector3[] OPBoyPos;
    public Vector3[] OPBoyRot;
    public Vector3[] OPGirlPos;
    public Vector3[] OPGirlRot;
    public Vector3[] HallPos;
    public Vector3[] HallRot;
    public Vector3[] HallGrilPos;
    public Vector3[] HallGrilRot;
    public Vector3 DetailPos;
    public Vector3 DetailRotation;
    public float DetailSize;
    public string CameraIdleAni;
    public string CameraWalkAni;
    public string MPIdleAni;
    public string MPFireBeginAni;
    public string MPFireEndAni;
    public string[] MPFireAni;
    public string[] MPReloadAni;
    public string MPSwitchAni;
    public string MPPowerOnAni;
    public string[] MPFireMountEffect;
    public string[] ReloadAni;
    public string[] FireBeginAni;
    public string[] FireEndAni;
    public string[] PowerOnAni;
    public string[] FireAni;
    public string[] SwitchAni;
    public string[] IdleAni;
    public string OPIdleAni;
    public string[] OPFireAni;
    public string OPReloadAni;
    public string OPSwitchAni;
    public string[] OPFireMountEffect;
    public string FireMountEffect;
    public string[] FireSound;
    public float[] FireSoundProbability;
    public float[] FireSoundSpawn;
    public string[] ReloadSound;
    public string[] SwitchSound;
    public string[] FireEmptySound;
    public string[] OpWeaponFire;
    public string[] OpWeaponSwitch;
    public string[] OpWeaponIdle;


    public int AmmoID;
    public int InitClipSize;
    public int ClipSize;
    public int ExtraAmmoNum;
    public int FireMode;
    public int BurstNum;
    public string AmmoTypes;    
    public int FireRate;
    public int BurstRate;
    public float FireDelay; //开火（前）延迟
    public float FireAfterDelay; //开火后延迟
    public float Damage;
    public float ReloadTime;
    public float SwitchTime;
    public float Impulse;
    public float ImpulseSpeed;
    public float DamageDelay;
    public float DamageDuration;
    public float[] DamageRange;
    public int Pellets;
    public float DamageDecayStartDis;
    public float DamageDecayEndDis;
    public float DamageDecayMax;
    public float TeamHealRate;
    public float[] HitEffectRot;
    public string[] FireEffect;
    public string[] FireEffect2;
    public string CartridgeEffect;
    public string CartridgeEffect2;
    public string BulletModel;
    public string BulletModel2;
    public string BulletEffect;
    public string BulletEffect2;
    public float BulletEffectSpeed;
    public float BulletEffectSpeed2;
    public float BulletEffectTime1;
    public float BulletEffectTime2;
    public int isAttachPlayer;
    public int BulletEffectFrequency;
    public int ContinuousSniper;
    public int ZoomMode;
    public float ZoomInTime;
    public float ZoomOutTime;
    public float ZoomFactor;
    public int ZoomFireRate;
    public float ZoomDamage;
    public float ZoomMoveFactor;

    // 武器击中特效
    public string MPHitEffect;
    public float MPHitEffectTime;
    public int[] OPHitEffect;
    public float OPHitEffectTime;

    //
    public string FlashBombScreenPic;

    // Outlook
    public string FlowTex;
    public int[] FlowTexColor;

    //后座力
    public float[] MinPitch;
    public float[] MaxPitch;
    public float[] MinYaw;
    public float[] MaxYaw;
    public int RecoilCountMax;
    public float RecoilCountDecaySpeed;
    public float RecoilKickAcc;
    public float RecoilReturnAccFactor;
    public float RecoilPitchMin;
    public float RecoilPitchMax;
    public float RecoilYawMin;
    public float RecoilYawMax;
    public float RecoilSightsFactor;

    //散射
    public float StandSpreadInitial;
    public float StandSpreadFireAdd;
    public float StandSpreadMax;
    public float StandSpreadMinFactor;
    public float StandSpreadDecaySpeed;
    public float CrouchSpreadInitial;
    public float CrouchSpreadFireAdd;
    public float CrouchSpreadMax;
    public float CrouchSpreadMinFactor;
    public float CrouchSpreadDecaySpeed;
    public float SpreadPitchFactor;
    public float SpreadYawFactor;
    public float SpreadMovementFactor;
    public float SpreadMovementMax;
    public float SpreadMovementDecaySpeed;
    public float SpreadJumpMax;
    public float SpreadJumpDecaySpeed;
    public float SpreadSightsFactor;
    public float SpreadDispMin;

    public int Score;
    public string[] HallAnimation;
    public string[] HallWeaponAnimation;
    public int DispDamage;
    public int DispShootSpeed;
    public int DispWeight;
    public int DispAccuracy;
    public int DispRecoil;
    public int DispFireRange;
    public int DispPierce;
    public int DispClipCap;
    public int DispNearDamage;
    public int DispAttackSpeed;
    public int DispAttackArea;
    public int DispAttackForce;
    public int DispDamageArea;
    public int DispBulletproof;
    public int DispBlastproof;
    public int DispMoveSpeed;
    public int DispDurability;
    public int StrengthenGroup = 1;//使用的强化组别
    public int Dispbook;

    public string WeaponType;
    public int SubWeapon;

    public string MPModel2;
    public string OPModel2;
    public string TujianModel;

    public int DieAniType;
    public int[] SpecialAtt;
    public int SniperUI;//1：低级瞄准镜  2：高级瞄准镜

    public int MaxHitCount;


    public string LevelTag;
    public int HallModelCheck = 0;
    public float HallModelScale = 1;

    public string FightAchieveEffect;

    public string KillIcon;
    public int ReloadType;

    public float AutoAimDistance;
    public int AutoAimType;
    public int SkillA;
    public int SkillB;

    //蓄力相关
    public float MinPowerOnTime;
    public float MaxPowerOnTime;
    public float MaxDamage;
}

public class ConfigItemWeapon : ConfigBase<ConfigItemWeaponLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemWeapon() : base("ID")
    {
    }
}
