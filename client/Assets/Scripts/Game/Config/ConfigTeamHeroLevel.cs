﻿using System;
using System.Collections.Generic;

[Serializable]
public class CongigTeamHeroLevelLine
{
    public int Level;
    public int Soul;
    public int Halo;
    public int[] Skills;
    public string Tip;
}

public class ConfigTeamHeroLevel : ConfigBase<CongigTeamHeroLevelLine>
{
    public ConfigTeamHeroLevel()
        : base("Level")
    {

    }
}
