﻿using System;
using System.Collections.Generic;

[Serializable]


public class ConfigTitleLine
{
    public string Type;
    public string Name;
    public string[] GameType;
    public string tips;
    public int[] ShowType;
    public string[] icon;
}

public class ConfigTitle : ConfigBase<ConfigTitleLine>
{
    public ConfigTitle()
        : base("Type")
    {

    }

    public static string GetIcon(string title, int type)
    {
        ConfigTitleLine cfg = ConfigManager.GetConfig<ConfigTitle>().GetLine(title);
        if (cfg.icon != null && cfg.icon.Length > 0)
        {
            return cfg.icon[type - 1];
        }
        return title;
    }
}

