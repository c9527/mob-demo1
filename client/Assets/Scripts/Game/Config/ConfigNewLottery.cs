﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/17 14:40:36
 * Description:
 * Update History:
 *
 **************************************************************************/
[Serializable]
public class ConfigNewLotteryLine
{

    public enum TYPE_HIGHGRADE
    {
        level0 = 0,
        level1,
        level2,
        level3
    }

    public enum TYPE_LIMIT
    {
        NONE = 0,
        BIG_PRIZE,
        LIMIT,
    }
    public int ID;
    public float[] ItemId;
    public int MoneyType;
    public int ShowWeight;
    public int HighGrade;
    public int RareType;
    public int IsLimit;
}

public class ConfigNewLottery : ConfigBase<ConfigNewLotteryLine>
{
    public ConfigNewLottery()
        : base("ID")
    {

    }
}
