﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
class ConfigFamilyTitleLine
{
    public int ID;
    public string Name;
    public string[] FromTitle;
    public string[] ToTitle;
}

class ConfigFamilyTitle : ConfigBase<ConfigFamilyTitleLine>
{
    private Dictionary<string, string> _longName2ShortName; 
    public ConfigFamilyTitle()
        : base("ID")
    {

    }

    public string GetShortName(string longName)
    {
        if (_longName2ShortName == null)
        {
            _longName2ShortName =  new Dictionary<string, string>();
            for (int i = 0; i < m_dataArr.Length; i++)
            {
                var cfgline = m_dataArr[i] as ConfigFamilyTitleLine;
                _longName2ShortName[cfgline.FromTitle[0]] = cfgline.FromTitle[1];
                _longName2ShortName[cfgline.ToTitle[0]] = cfgline.ToTitle[1];
            }
        }
        if (_longName2ShortName.ContainsKey(longName))
        {
            return _longName2ShortName[longName];
        }
        return String.Empty;
    }
}
