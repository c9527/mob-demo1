﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigWeaponSpecialAttrLine
{
    public int ID;
    public string Name;
    public string Icon;
}

public class ConfigWeaponSpecialAttr :ConfigBase<ConfigWeaponSpecialAttrLine>
{
    public ConfigWeaponSpecialAttr():base("ID")
    {
        
    }

}
