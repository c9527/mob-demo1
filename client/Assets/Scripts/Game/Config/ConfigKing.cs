﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDGunKingRule
{
    public int ID;
    public string WeaponType;
    public int WeaponTypeInt;
    public int DefaultWeaponId;
    public int SoloRound;
    public int SoloNeedKill;
    public int CampRound;
    public int CampNeedKill;
    public int CampScore;
}

class ConfigKing : ConfigBase<TDGunKingRule>
{
    public ConfigKing()
        : base("ID")
    {

    }

    public TDGunKingRule getNextStepInfo(int id)
    {
        return GetLine(id + 1);
    }
}
