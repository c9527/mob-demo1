﻿using System;
using System.Collections.Generic;

[Serializable]
class TDStrengthenTrainQuicken
{
    public int Times;
    public int Diamond;
    public int ReqVip;
}

class ConfigWeaponTrainQuicken : ConfigBase<TDStrengthenTrainQuicken>
{
    public ConfigWeaponTrainQuicken()
        : base("Times")
    {

    }

    public TDStrengthenTrainQuicken GetQuickenUpper(int level=0)
    {
        TDStrengthenTrainQuicken result = null;
        for (int i = m_dataArr.Length-1; i >=0; i--)
        {
            if (level == m_dataArr[i].ReqVip)
            {
                result = m_dataArr[i];
                break;
            }
        }
        return result;
    }

    public int GetTotalFreeQuickenTimes(int level = 0)
    {
        var result = 0;
        for (int i =0; i<m_dataArr.Length; i++)
        {
            if (level == m_dataArr[i].ReqVip && m_dataArr[i].Diamond==0)
            {
                result++;
                break;
            }
        }
        return result;
    }

    public int GetMaxQuickenTimes(int level)
    {
        int max = 0;
        for (int i =0; i <m_dataArr.Length; i++)
        {
            if (m_dataArr[i].Times>max&&m_dataArr[i].ReqVip==level)
            {
                max = m_dataArr[i].Times;                
            }
        }
        max += 2;
        return max;
    }

}

