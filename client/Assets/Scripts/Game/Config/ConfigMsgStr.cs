﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

class ConfigMsgStrLine
{
    public int ID;
    public string zh;
    public string en;
    public string ru;
    public string ko;

}


class ConfigMsgStr:ConfigBase<ConfigMsgStrLine>
{
    


    private ConfigMsgStr()
        : base("ID")
    {
    }

    public string GetMsgStr(int code)
    {
        var cfgline = GetLine(code);
        if (cfgline != null)
        {
            var fieldInfo = typeof(ConfigMsgStrLine).GetField(SdkManager.Lang, BindingFlags.Instance | BindingFlags.Public);
            return (string)(fieldInfo.GetValue(cfgline));
        }
        return String.Empty;
    }
}