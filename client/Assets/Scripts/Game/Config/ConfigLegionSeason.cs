﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigLegionSeasonLine
{
    public int Season;
    public int StartTime;
    public int EndTime;
}

public class ConfigLegionSeason : ConfigBase<ConfigLegionSeasonLine>
{
    private ConfigLegionSeason()
        : base("Season")
    {
    }
}
