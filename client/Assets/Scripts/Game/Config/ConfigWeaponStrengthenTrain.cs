﻿using System;
using System.Collections.Generic;


[Serializable]
class ConfigWeaponStrengthenTrainLine
{
    public int ID;
    public int Time;
    public float Ratio;
    public int Diamond;
    public int Coin;
    public int Medal;
    public int ReqVip;
    public int Exp;
    public int NeedItem;
}


class ConfigWeaponStrengthenTrain : ConfigBase<ConfigWeaponStrengthenTrainLine>
{
    public ConfigWeaponStrengthenTrain()
        : base("ID")
    {

    }
}

