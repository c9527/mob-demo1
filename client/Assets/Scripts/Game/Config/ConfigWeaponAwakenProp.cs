﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDWeaponAwakenProp
{
    public int Id;
    public string Name;
    public string Desc;
    public string Effect;
    public int Type;
    public string Param;
    public float HurtRate;
    public float DamageRate;
    public float ZombieDamageRate;
    public float PveDamageRate;
    public float PvpKillCure;
    public float PveKillCure;
    public float HeadHurtFactor;
    public float Decelerate;
    public float AntiDecelerate;
}

class ConfigWeaponAwakenProp : ConfigBase<TDWeaponAwakenProp>
{
    public ConfigWeaponAwakenProp()
        : base("Id")
    {

    }
}
