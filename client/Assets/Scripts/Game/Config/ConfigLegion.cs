﻿using UnityEngine;
using System;
using System.Collections;

/////////////////////////
//Copyright(C): 4399 FPS studio
//功能描述：
//创建者：HWL
//创建时间: 2016/3/25 16:18:16
///////////////////////////
[Serializable]

public class ConfigLegionLine
{
    public int LegionID;
    public string LegionName;
}

public class ConfigLegion : ConfigBase<ConfigLegionLine>
{
    private ConfigLegion()
        : base("LegionID")
    {
    }
}
