﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDPropInfo
{
    public string ID;
    public string Name;
}

public class ConfigPropDef : ConfigBase<TDPropInfo>
{
    Dictionary<string,string> _disp_prop_map;

    public ConfigPropDef()
        : base("ID")
    {
        _disp_prop_map = new Dictionary<string,string>();
        _disp_prop_map["DispDamage"] = "伤害";
        _disp_prop_map["DispShootSpeed"] = "射速";
        _disp_prop_map["DispWeight"] = "便携";
        _disp_prop_map["DispAccuracy"] = "精准";
        _disp_prop_map["DispRecoil"] = "稳定";
        _disp_prop_map["DispFireRange"] = "射程";
        _disp_prop_map["DispPierce"] = "穿甲";
        _disp_prop_map["DispNearDamage"] = "伤害";
        _disp_prop_map["DispAttackSpeed"] = "攻速";
        _disp_prop_map["DispAttackArea"] = "范围";
        _disp_prop_map["DispAttackForce"] = "伤害";
        _disp_prop_map["DispDamageArea"] = "范围";
        _disp_prop_map["DispBulletproof"] = "防弹";
        _disp_prop_map["DispBlastproof"] = "防爆";
        _disp_prop_map["DispMoveSpeed"] = "移速";
        _disp_prop_map["DispClipCap"] = "载弹";
        _disp_prop_map["DispDurability"] = "耐久";
        _disp_prop_map["DispReloadTime"] = "上弹速度";
    }

    public String GetDispPropName(string type)
    {
        var result = "";
        if(!string.IsNullOrEmpty(type) && _disp_prop_map.TryGetValue(type,out result))
        {
            return result;
        }
        else
        {
            return "";
        }
    }
}

