﻿using System;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/2 17:23:02
 * Description:
 * Update History:
 *
 **************************************************************************/
[Serializable]

public class ConfigQuickStartTypeLine
{
    public int Type;
    public string Name;
}
public class ConfigQuickStartType : ConfigBase<ConfigQuickStartTypeLine>
{
    private ConfigQuickStartType()
        : base("Type")
    {

    }
}
