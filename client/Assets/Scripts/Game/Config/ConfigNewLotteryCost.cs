﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/18 10:06:12
 * Description:
 * Update History:
 *
 **************************************************************************/

public class ConfigNewLotteryCostLine
{
    public int ID;
    public int Type;
    public int Time;
    public string MoneyType;
    public int[] Cost;
    public ItemInfo[] Items;
    public int DiscountCost;
    public int DiscountTime;
    public string DiscountTimeRange;
}

public class ConfigNewLotteryCost : ConfigBase<ConfigNewLotteryCostLine>
{
    public ConfigNewLotteryCost()
        : base("ID")
    {
        // 构造器
    }

    public ConfigNewLotteryCostLine GetbyCountAndType(int count, string moneyType)
    {
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].MoneyType == moneyType)
            {
                if (m_dataArr[i].Time == count)
                {
                    if (m_dataArr[i].DiscountCost != 0)
                    {
                        return m_dataArr[i];
                    }
                }
            }
        }
        return null;
    }
}
