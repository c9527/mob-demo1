﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigEventWelfareLine
{
    public int Id;
    public ItemInfo[] Item;
    public int Times;
    public int Sign;
    public int[] Condition;
}
public class ConfigEventWelfare : ConfigBase<ConfigEventWelfareLine>
{
    private ConfigEventWelfare()
        : base("Id")
    {
    }
}
