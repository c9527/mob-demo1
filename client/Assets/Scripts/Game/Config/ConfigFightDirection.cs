﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]

public class ConfigFightDirectionLine
{
    public int ID;
    public int type;
    public string Content;
    public string Sound;
    public string[] CanUsingMode;
}
public class ConfigFightDirection : ConfigBase<ConfigFightDirectionLine>
{
    private List<ConfigFightDirectionLine> contentList = new List<ConfigFightDirectionLine>();
    private ConfigFightDirection()
        : base("ID")
    {
    }

    /// <summary>
    /// 获取同一类型的数据
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<ConfigFightDirectionLine> GetFightDirectionLineListByType(int type)
    {
        contentList.Clear();
        for( int i = 0; i < m_dataArr.Length;i++ )
        {
            if( type ==  m_dataArr[i].type)
            {
                if (WorldManager.singleton.gameRuleLine != null && m_dataArr[i].CanUsingMode.IndexOf(WorldManager.singleton.gameRuleLine.Type) > -1)
                {
                    contentList.Add(m_dataArr[i]);
                }
            }
        }
        return contentList;
    }
}
