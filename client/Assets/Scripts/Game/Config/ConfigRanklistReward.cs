﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigRanklistRewardLine
{
    public int ID;
    public string Name;
    public int BeginRank;
    public int EndRank;
    public ItemInfo[] RewardList;
}

public class ConfigRanklistReward : ConfigBase<ConfigRanklistRewardLine>
{
    private ConfigRanklistReward()
        : base("ID")
    {
    }
}
