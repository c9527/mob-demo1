﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
class TDCorpsMission
{
    public int ID;
    public int Type;
    public int Count;
    public string Desc;
    public string GoSystem;
    public string Icon;
    public int CorpsCoin;
    public int Activeness;
    public bool Visible;
    public int Limits;
    public int[] Level;
}

class ConfigCorpsMission : ConfigBase<TDCorpsMission>
{
    public ConfigCorpsMission()
        : base("ID")
    {

    }
}
