﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigPvEFightAchieve : ConfigBase<ConfigFightAchieveLine>
{
    private ConfigPvEFightAchieve()
        : base("Type")
    {

    }
}