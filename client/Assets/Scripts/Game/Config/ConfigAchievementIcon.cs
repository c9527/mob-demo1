﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigAchievementIconLine
{
    public int ID;
    public string Name;
}
public class ConfigAchievementIcon : ConfigBase<ConfigAchievementIconLine>
{
    private ConfigAchievementIcon()
        : base("ID")
    {

    }
}
