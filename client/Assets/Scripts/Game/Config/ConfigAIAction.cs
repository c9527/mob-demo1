﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigAIActionLine
{
    public int ID;
    public string Type;
    public int ClientTime;
    public float[] Params;
    public string Effect;
    public int EffectAttach;
    public string Animation;
    public string AniEffect;
    public string[] ActionSound;
    public float ActionSoundRange;
    public int ShakeID;
}

public class ConfigAIAction : ConfigBase<ConfigAIActionLine>
{
    public ConfigAIAction()
        : base("ID")
    {

    }
}
