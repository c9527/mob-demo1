﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigItemRoleLine : ConfigItemLine
{
    public int ModelSex;
    public float BackMoveFactor;
    public float SideMoveFactor;
    public int Fov;
    public string[] Skills;
    public int open;
    public string[] DeadSound;
    public string[] DeadHeadSound;
    public float DeadSoundRange;
    public float HeightStand;
    public float HeightCrouch;
    public float MPRadius;
    public float OPRadius;
    public Vector3 MPCenter;
    public Vector3 OPCenter;
    public float FollowHeight;
    public float FollowDistance;
    public int MaxHP;
    public int[] Weapon;
    public string DieEffect;
    public int CanFirstPersonWatch;        //能否第一人称视角观战
    public int IsCancelDisplayWatchInfo;     //是否取消显示观战信息
    public float MoveSlowSpeedFactor;//慢走时是正常移动速度的比例
    public int IsCancelPlayStepSound;
    public string[] LodModel;
    public float HPAlert;
    public string ChooseImage;
    public float DeadDisappearTime;  //死亡消失时间
    public float HideDisPercent;//幽灵模式距离百分比
    public float HideDisappearTimePercent;//幽灵模式消失百分比

    public int RealFov
    {
        get { return !Driver.isMobilePlatform ? 60 : Fov; }
    }
}

public class ConfigItemRole : ConfigBase<ConfigItemRoleLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemRole()
        : base("ID")
    {
    }

    public ConfigItemRoleLine[] getRoleData(int camp = -1)
    {
        var list = new List<ConfigItemRoleLine>();
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].open == 1 && (m_dataArr[i].RoleCamp == camp || camp == -1))
            {
                list.Add(m_dataArr[i]);
            }
        }
        return list.ToArray();
    }

    public ConfigItemRoleLine[] getZombieList()
    {
        return getRoleData(3);
    }
}
