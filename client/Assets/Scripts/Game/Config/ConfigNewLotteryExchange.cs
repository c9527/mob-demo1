﻿using System;
using System.Collections.Generic;
using UnityEngine;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/3/17 16:37:34
 * Description:军火库货币兑换消耗
 * Update History:
 *
 **************************************************************************/
[Serializable]
public class ConfigNewLotteryExchangeLine
{
    public int ID;
    public int FromMoneyId
    {
        get
        {
            if (FromMoney == "DIAMOND")
                return 5003;
            else if (FromMoney == "BLUE_STONE")
                return 5041;
            else if (FromMoney == "PURPLE_STONE")
                return 5042;
            return 0;
        }
    }
    public string FromMoney;

    public int FromAmount;
    public string ToMoney;
    public int ToMoneyId
    {
        get
        {
            if (ToMoney == "DIAMOND")
                return 5003;
            else if (ToMoney == "BLUE_STONE")
                return 5041;
            else if (ToMoney == "PURPLE_STONE")
                return 5042;
            else if (ToMoney == "WHITE_STONE")
                return 5040;
            return 0;
        }
    }
    public int ToAmount;
}
public class ConfigNewLotteryExchange : ConfigBase<ConfigNewLotteryExchangeLine>
{
    public ConfigNewLotteryExchange()
        : base("ID")
    {
        // 构造器
    }
}
