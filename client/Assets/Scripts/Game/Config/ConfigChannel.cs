﻿using System;

[Serializable]


public class ConfigChannelLine
{
    public int Id;
    public int SortIndex;
    public ChannelTypeEnum Type;
    public string SubType;
    public EnumPlatform[] EnablePlatforms;
    public string Name;
    public bool ByCenter;   //是否跨服
    public bool IsRoleServerName;   //跨服玩家是否显示服名
    public bool IsAIFakeServerName;   //跨服AI是否显示假服名
    public bool Display;
    public string DisplayName;
    public int LevelMin;
    public int LevelMax;
    public int Maxplayer;
    public int DisplayColor;
    public int DisableWorldInvite;
    public int Minimize;
    public bool PlatDivide;
    public bool IsPve;
    public bool isBase;
    /// <summary>
    /// 该频道是否支持当前平台
    /// </summary>
    /// <returns></returns>
    public bool IsPlatformEnable()
    {
        var enable = false;
        for (int j = 0; j < EnablePlatforms.Length; j++)
        {
            if (Driver.m_platform == EnablePlatforms[j])
            {
                enable = true;
                break;
            }
        }

        return enable;
    }
}

public class ConfigChannel : ConfigBase<ConfigChannelLine>
{
    private ConfigChannel(): base("Id")
    {

    }
}