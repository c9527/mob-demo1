﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigFoundationRewardLine
{
    public int ID;
    public int[] LevelRange;
    public string Reward;
    public int ItemId;
    public int TargetLevel;
    public string DisplayName;
    public string[] Tag;
    public string Desc;
    public string Icon;
    public string Bubble;
}

public class ConfigFoundationReward : ConfigBase<ConfigFoundationRewardLine>
{
    public ConfigFoundationReward()
        : base("ID")
    {

    }

    public ConfigFoundationRewardLine[] GetBaseReward()
    {
        List<ConfigFoundationRewardLine> list = new List<ConfigFoundationRewardLine>();
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].ID%100==0)
            {
                list.Add(m_dataArr[i]);
            }
        }
        return list.ToArray();
    }

    public bool GetAllBubble()
    {
        bool bo = false;
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            bo |= BubbleManager.HasBubble(m_dataArr[i].Bubble);
        }


        return bo;
    }

    public string[] GetAllConst()
    {
        List<string> bubbles = new List<string>();
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (!bubbles.Contains(m_dataArr[i].Bubble))
            {
                bubbles.Add(m_dataArr[i].Bubble);
            }
        }
        return bubbles.ToArray();
    }

}

