﻿using System;

[Serializable]

public class ConfigAchievementLine
{
    public int ID;                      /// 成就ID
    public int AchieveType;             /// 成就类型,1:普通成就,2:带有前置条件的成就,3:一组内最终的成就
    public int ModuleTag;               /// 成就所属的模块
    public int TeamTag;                 /// 小组标签 
    public string TeamTagName;          /// 小组标签名称
    public string Name;                 /// 成就名字
    public string Desc;                 /// 成就描述
    public int Count;                   /// 计数
    public int Point;                   /// 成就点
    public int AchieveTitle;            /// 成就称号
    public int AchieveIcon;             /// 成就头像
    public int Badge;                   /// 徽章
    public int PreAchieve;              /// 前置成就
  

}

public class ConfigAchievement : ConfigBase<ConfigAchievementLine>
{
    private ConfigAchievement()
        : base("ID")
    {

    }
}