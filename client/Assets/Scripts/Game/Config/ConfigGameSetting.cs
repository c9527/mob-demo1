﻿using System;
using UnityEngine;

[Serializable]
public class ConfigGameSettingLine
{
    public string key;

    public Vector3 SceneCameraPos;
    public Vector3 SceneCameraRot;
    //public float SceneCameraNearClip;
    //public float SceneCameraFarClip;
    public float OPPitchUpMaxAngle;
    public float OPPitchDownMaxAngle;
    public float OPPitchSpeed;
    public float OPPitchChestFactor;

    public Vector3 MainPlayerCameraPos;
    public Vector3 MainPlaerCameraRot;

    public float EffectPosZOffset;

    public float MPColliderRadius;
    public float MPStepOffset;
    public float OPColliderRadius;
    public float PlayerColliderSlope;

    public float GRAVITY;
    public float GRAVITY_JUMPUP;

    //public float WatchDeadCameraMaxRotAngle;
    public float WatchDeadCameraHeight;
    public float WatchDeadCameraDis;
    public float WatchDeadCameraMoveBackTime;
    public float WatchDeadCameraFollowKillerTime;

    // 同步相关
    public float SendPosInterval;
    public float TweenTime;
    public float AITweenTime;
    public float WatchDieDelay;
    public float OPPosDelayTime;
    public float MPPosDelayTime;
    public int PosQueueMaxNum;
    public int AIPosQueueMaxNum;
    public int PosQueueNormalNum;
    public float TweenJumpTime;
    public float TweenJumpGroundTime;
    public float TweenFakeJumpSpeed;

    public float TweenSpeedUpFactor;
    public float AITweenSpeedUpFactor;
    public float TweenSlowDownFactor;

    public float MPCCCenterOffset;
    public float OPGroundCheckHeightOffset;

    public int MaxBloodEffectNum;
    public int OPHitBuildingInterval;
    public int HitBuildingEffectDis;
    public int OptimizeRoleColliderNum;

    public float FollowPlayerDefaultVerticalRotate;

    public float DamageTipsTotalTime;
    public float DamageTipsTweenTime;
    public int DamageTipsFontSize;
    public Vector2 DamageTipsPosRamdomX;
    public Vector2 DamageTipsPosRamdomY;
    public Vector2 DamageTipsScaleChange;

    public float JumpMoveSlowTime;
    public float JumpMaxHeight;
    //public string[] CheatAppName;

    //public int MinMemMode;
    public int MinFreeMemory;
    public int MaxUsedMemory;
    public int SmallMemory;

    public float[] IodLevelDistance;

    public float FightAchieveEffectDelay;
}

public class ConfigGameSetting : ConfigBase<ConfigGameSettingLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigGameSetting() : base()
    {

    }
}
