﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigRuleIntroLine
{
    public string RuleTarget;
    public string Content;
    public string Title;
}

public class ConfigRuleIntro : ConfigBase<ConfigRuleIntroLine>
{
    private ConfigRuleIntro()
        : base("RuleTarget")
    {
    }
}
