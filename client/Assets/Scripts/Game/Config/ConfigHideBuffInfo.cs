﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigHideBuffInfoLine
{
    public int ID;
    public string BuffIcon;
    public int LifeAdd;
    public int GunBulletsAdd;
    public int HPDeductLevel;
	
}

public class ConfigHideBuffInfo : ConfigBase<ConfigHideBuffInfoLine>
{

    public ConfigHideBuffInfo()
        : base("ID")
    {

    }

}