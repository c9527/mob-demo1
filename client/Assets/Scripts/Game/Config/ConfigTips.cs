﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class ConfigTipsLine
{
    public int ID;
    public int[] Level;
    /// <summary>
    /// 普通平台
    /// </summary>
    public string[] Icon1;
    /// <summary>
    /// 应用宝
    /// </summary>
    public string[] Icon2;
    public string Type;
    public string Content;
}

public class ConfigTips:ConfigBase<ConfigTipsLine>  {
    static bool m_firstBattle = true; //第一次进入战斗
    public ConfigTips():base("ID")
    {

    }

    public static ConfigTipsLine GetRandomTips()
    {
        ConfigTipsLine result = null;
        List<ConfigTipsLine> list =new List<ConfigTipsLine>();
        int lv = 0;
        RoleData role = PlayerSystem.roleData;
        string gameRule=WorldManager.singleton.gameRule;
        if (GlobalBattleParams.singleton.GetBattleParam("FirstLoading") < 0.1f)
        {
            gameRule = "FirstLoading";
            GlobalBattleParams.singleton.SetBattleParam("FirstLoading", 1.0f);
        }
        else if(!gameRule.IsNullOrEmpty()&&m_firstBattle)
        {
            m_firstBattle = false;
            gameRule = "FirstBattle";
        }
        if (role != null)
        {
            lv = role.level;
        }
        foreach(ConfigTipsLine item in  ConfigManager.GetConfig<ConfigTips>().m_dataArr )
        {
            if(lv>=item.Level[0]&&lv<=item.Level[1])
            {
                if (gameRule.IsNullOrEmpty() && item.Type == "0")
                {
                    list.Add(item);
                }
                else if(gameRule==item.Type)
                {
                    list.Add(item);
                }
            }
        }
        if (list.Count == 0)
            return null;
        int index = new System.Random().Next(0, list.Count);
        result = list[index];
        return result;
    }
}
