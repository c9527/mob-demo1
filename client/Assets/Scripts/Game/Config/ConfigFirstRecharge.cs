﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigFirstRechargeLine
{
    public int ID;
    public int EventId;
    public int WelfareId;
}

public class ConfigFirstRecharge : ConfigBase<ConfigFirstRechargeLine>
{
    private ConfigFirstRecharge()
        : base("ID")
    {
    }
}

