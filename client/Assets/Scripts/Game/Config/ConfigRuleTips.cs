﻿using UnityEngine;
using System.Collections;

public class ConfigRuleTipeLine
{
    public string ruleName;
    public string ruleTips;
    public ChannelTypeEnum channel;
}

public class ConfigRuleTips : ConfigBase<ConfigRuleTipeLine> {

    public ConfigRuleTips()
        : base("ruleName")
    {

    }
}
