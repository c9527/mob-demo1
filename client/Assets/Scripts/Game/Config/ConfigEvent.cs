﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigEventLine
{
    public int Id;
    public int EventId;
    public int EventType;
    public string Name;
    public string Title;
    public int StartTime;
    public int EndTime;
    public int[] Welfare;
    public int ViewStartTime;
    public int ViewEndTime;
    public int State;
    public string[] Params;
}
public class ConfigEvent : ConfigBase<ConfigEventLine> 
{
    private ConfigEvent()
        : base("Id")
    {
    }
}
