﻿using System;

[Serializable]

public class ConfigMasterWelfareLine
{
    public int ID;
    public bool IsMaster;
    public int Level;               
    public ItemInfo[] Reward;        
}

public class ConfigMasterWelfare : ConfigBase<ConfigMasterWelfareLine>
{
    private ConfigMasterWelfare()
        : base("ID")
    {

    }
}