﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigAccessoryQualityLine
{
    public int ID;
    public int Accessory;
    public string Prop;
    public int AddType;
    public Vector2[] PropScopes;

    public int GetQuality(int value)
    {
        value = Mathf.Abs(value);
        var result = 0;
        if (PropScopes != null)
        {
            for (int i = 0; i < PropScopes.Length; i++)
            {
                if (value < Mathf.Abs(PropScopes[i].x))
                {
                    break;
                }
                result++;
            }
        }
        return result;
    }
}

public class ConfigAccessoryQuality : ConfigBase<ConfigAccessoryQualityLine>
{
    public ConfigAccessoryQuality()
    {

    }

    public ConfigAccessoryQualityLine GetAddProp(int code, string prop)
    {
        ConfigAccessoryQualityLine info = null;
        if (m_dataArr != null && code > 0 && !string.IsNullOrEmpty(prop))
        {
            for (var i = 0; i < m_dataArr.Length; i++)
            {
                if (m_dataArr[i].Accessory == code && m_dataArr[i].Prop == prop)
                {
                    info = m_dataArr[i];
                    break;
                }
            }
        }
        return info;
    }

    public int GetQuality(int code, string prop,int value)
    {
        var info = GetAddProp(code, prop);
        return info != null ? info.GetQuality(value) : 0;
    }
}

