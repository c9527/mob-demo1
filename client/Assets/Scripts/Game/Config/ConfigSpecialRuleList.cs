﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigSpecialRuleListLine
{
    public string MatchRule;
    public string RuleName;
    public int RuleType;
}

public class ConfigSpecialRuleList : ConfigBase<ConfigSpecialRuleListLine>
{
	public ConfigSpecialRuleList():base("MatchRule")
    {

    }
}
