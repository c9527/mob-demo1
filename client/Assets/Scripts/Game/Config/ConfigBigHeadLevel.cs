﻿using System;

[Serializable]
public class ConfigBigHeadLevelLine
{
    public int Level;
    public int Score;
    public int Buff;
    public float Head;
}

public class ConfigBigHeadLevel : ConfigBase<ConfigBigHeadLevelLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigBigHeadLevel()
        : base("Level")
    {
    }

    public ConfigBigHeadLevelLine GetBigHeadConfig(int score)
    {
        int left = 0;
        int right = m_dataArr.Length - 1;
        int middle = 0;
        while (left <= right)
        {
            middle = left + ((right - left) >> 1);
            if (m_dataArr[middle].Score > score)
                right = middle -1;
            else if (m_dataArr[middle].Score < score)
                left = middle + 1;
            else
                return m_dataArr[middle];
        }

        if (m_dataArr[right].Score > score)
            return m_dataArr[right - 1];
        else
            return m_dataArr[right];
    }
}
