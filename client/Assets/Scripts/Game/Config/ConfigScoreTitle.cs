﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

[Serializable]
public class TDScoreTitle
{
    public int ID;
    public int range;
    public string Name;
    public int ScoreLower;
    public string IconKey;
    public string RankRoomBG;
    public string BattleRecordBg;
    public string BatteRoomBg;
    public string BatteRoom8v8Bg;
    public string BatteSettleBg;
    public int Level;
}

class ConfigScoreTitle : ConfigBase<TDScoreTitle>
{
    private List<TDScoreTitle> _prefixTitleList; 
    public ConfigScoreTitle()
        : base("ID")
    {

    }

    public TDScoreTitle getScoreTitleInfo(int value)
    {
        TDScoreTitle result = null;
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (value<m_dataArr[i].ScoreLower)
            {
                return result;
            }
            result = m_dataArr[i];
        }
        return m_dataArr[m_dataArr.Length-1];
    }

    public string getScoreTitleName(int range)
    {
        string name = "";
        var regex = new Regex(@"[0-9]");
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].range == range)
            {
                name = m_dataArr[i].Name;
                return regex.Replace(name, String.Empty);
            }
        }
        return "";
    }

    public List<TDScoreTitle> GetAllScoreTitleList()
    {
        if (_prefixTitleList == null)
        {
            _prefixTitleList = new List<TDScoreTitle>();
            int prevRange = 0;
            for (int i = 0; i < m_dataArr.Length; i++)
            {
                if (prevRange != m_dataArr[i].range)
                {
                    _prefixTitleList.Add(m_dataArr[i]);
                    prevRange = m_dataArr[i].range;
                }
            }
        }
        return _prefixTitleList;
    }

    public TDScoreTitle getScoreTitleInfoById(int id)
    {
        if (id >= m_dataArr.Length)
            return null;
        return m_dataArr[id];
    }
}

