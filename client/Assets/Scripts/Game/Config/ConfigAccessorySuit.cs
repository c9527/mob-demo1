﻿using System;
using System.Collections.Generic;

[Serializable]
class ConfigAccessorySuitLine
{
    public int Suit;
    public int Parts;
    public string[] AddProps;
    public string Desc;
}

class ConfigAccessorySuit : ConfigBase<ConfigAccessorySuitLine>
{
    public ConfigAccessorySuit()
    {

    }

    public ConfigAccessorySuitLine[] GetSuitGroupAddPropData(int code)
    {
        var list = new List<ConfigAccessorySuitLine>();
        if (m_dataArr != null && code>0)
        {
            for (var i = 0; i < m_dataArr.Length; i++)
            {
                if (m_dataArr[i].Suit == code)
                {
                    list.Add(m_dataArr[i]);
                }
            }
        }
        list.Sort((a, b) => a.Parts - b.Parts);
        return list.ToArray();
    }

    public ConfigAccessorySuitLine GetSuitAddPropData(int code,int part=0)
    {
        ConfigAccessorySuitLine result = null;
        var list = GetSuitGroupAddPropData(code);
        if (part > 0 && list != null && list.Length > 0)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Parts>part)
                {
                    break;
                }
                result = list[i];
            }
        }
        return result;
    }
}
