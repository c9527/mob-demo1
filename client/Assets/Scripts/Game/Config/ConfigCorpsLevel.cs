﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using System;

public class ConfigCorpsLevelLine
{
    public int Level;
    public int Activeness;
    public int MaxMember;
    public int Tribute;
    public int MallItemCnt;
    public string Disp;
}

public class ConfigCorpsLevel : ConfigBase<ConfigCorpsLevelLine>
{
    private ConfigCorpsLevel()
        : base("Level")
    {
    }
}
