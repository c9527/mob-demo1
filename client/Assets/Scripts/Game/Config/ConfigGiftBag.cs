﻿using System;
using UnityEngine;

[Serializable]

public class ConfigGiftBagLine
{
    public int ID;                      /// 礼包ID
    public string Name;                 /// 礼包名字
    public ItemInfo[] GiftList;                 /// 礼包名字
}

public class ConfigGiftBag : ConfigBase<ConfigGiftBagLine>
{
    private ConfigGiftBag()
        : base("ID")
    {

    }
}