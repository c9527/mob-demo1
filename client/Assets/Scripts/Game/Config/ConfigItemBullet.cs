﻿using System;

[Serializable]
public class ConfigItemBulletLine : ConfigItemLine
{
    public string DamageType;
    public float Speed;
    public float SpeedDecay;
    public float Gravity;
    public int Resilient;
    public float FlyTime;
    public float GroundTime;
    public float IgnoreArmorRate;
    public float Decelerate;
    public float DecelerateTime;
    public string TailEffect;
    public float TailEffectSize;
    public float TailEffectShowTime;
    public string ExplodeEffect;
    public string[] Explodesound;
    public float DamageMinR;
    public float DamageMaxR;
    public float SafeDistance;
    public int ExplodeType;
    public int ShakeID;
    public float ThrowAngle;
    public float MaxSpeed;
    public bool GoThroughTeammate;
    public float HeadShotRange;
}

public class ConfigItemBullet : ConfigBase<ConfigItemBulletLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemBullet() : base("ID")
    {
    }
}
