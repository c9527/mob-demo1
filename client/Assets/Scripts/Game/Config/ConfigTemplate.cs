﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigTemplateLine
{
    public string key;
    public bool boolField;
    public float floatField;
    public string stringField;
    public float[] floatArrayField;
    public Vector2[] vector2Field;
    public Vector3[] vector3Field;
}

public class ConfigTemplate : ConfigBase<ConfigTemplateLine>
{
    //请使用ScriptableObject.CreateInstance创建对象
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigTemplate() : base("key")
    {
    }
}
