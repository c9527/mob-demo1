﻿using System;
using UnityEngine;

[Serializable]

public class ConfigMapListLine
{
    public string MapId;
    public string MapName;        // 模式名字              
    public bool PackageOutside;   //包外加载
    public Vector3 SightPos;
    public Vector3 SightRot;
    public float BurstDistance;
    public string StrongPointDistance;//刀锋据点半径
    public string StrongPointEffect;
    public int Open;
    public int[] Hide_time;
    public float[] PitchRange;
    public string LightProbe;  //使用灯光探针
    public bool HideInUserDefine;
    public string[] CanUsingMode;  /// 地图对应的能开放的模式
    public int OwTarget;                           
                                   
    public bool isCanUse(string mode=null)
    {
        if (Open == 0 || string.IsNullOrEmpty(mode))
            return false;
        if(CanUsingMode!=null)
        {
            for (int i = 0; i < CanUsingMode.Length; i++)
            {
                if (CanUsingMode[i] == mode)
                    return true;
            }
        }
        return false;
    }
}

public class ConfigMapList : ConfigBase<ConfigMapListLine>
{
    private ConfigMapList()
        : base("MapId")
    {

    }
   
}