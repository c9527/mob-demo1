﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigAICondLine
{
    public int ID;
    public string Type;
    public int[] Params;
    public int[] ActionList;
    //public int IndieCD;
    //public int GlobalCD;
}

public class ConfigAICond : ConfigBase<ConfigAICondLine>
{
    public ConfigAICond()
        : base("ID")
    {

    }
}
