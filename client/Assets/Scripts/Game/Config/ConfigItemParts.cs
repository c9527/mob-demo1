﻿using System;

/***************************************************************************
 * Author: fujiangshan
 * Create Time: 2016/4/28 15:27:24
 * Description:
 * Update History:
 *
 **************************************************************************/
[Serializable]
public class ConfigItemPartsLine:ConfigItemLine
{
    public int MergeItem;//合成的武器
    public int MergeCost;//合成时消耗
}
public class ConfigItemParts : ConfigBase<ConfigItemPartsLine>
{
    private ConfigItemParts()
        : base("ID")
    {
    }
}
