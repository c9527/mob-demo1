﻿using System;

[Serializable]

public class ConfigMonthlySignInLine
{
    public int ID;
    public string Reward;               /// 奖励的物品列表
    public string VipDoubleIcon;        /// vip 翻倍图标 
}

public class ConfigMonthlySignIn : ConfigBase<ConfigMonthlySignInLine>
{
    private ConfigMonthlySignIn()
        : base("ID")
    {

    }
}