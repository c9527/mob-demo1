﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDRecharge
{
    public int ID;
    public string Name;
    public bool Sell;
    public bool Display;
    public bool DisplayInIOS;
    public float Cost;
    public float AppStoreCost;
    public string AppStoreId;
    public int AdditionalDiamond;
    public int GiftBagId;
    public int FirstbuyAdd;
    public int Recommend;
    public string Tag;
    public string Icon;
    public int Associator;
    public int Guarantee;
    public string ProductID;

    public float PlatformCost
    {
        get
        {
            if (SdkManager.IsSDKIOS)
                return AppStoreCost;
            else
                return Cost;
        }
    }
}

public class ConfigRecharge : ConfigBase<TDRecharge>
{
    public ConfigRecharge()
        : base("ID")
    {

    }
}

