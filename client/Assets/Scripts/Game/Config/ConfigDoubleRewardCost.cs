﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigDoubleRewardCostLine
{
    public int Times;
    public int Diamond;
}

public class ConfigDoubleRewardCost : ConfigBase<ConfigDoubleRewardCostLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigDoubleRewardCost()
        : base("Times")
    {
    }

    public ConfigDoubleRewardCostLine getCostData(int times)
    {
        if (times >= m_dataArr.Length)
            times = m_dataArr.Length - 1;
        return m_dataArr[times];
    }

}
