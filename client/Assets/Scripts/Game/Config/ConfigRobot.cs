﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigRobotLine
{
    public int ID;
    public string Type;
    public string Name;
    public int Level;
    public int Rank;
    public int DispPart1;
    public int DispPart2;
    public int DispPart3;
    public int ReviveCoinCure;
}
public class ConfigRobot : ConfigBase<ConfigRobotLine>
{
    private ConfigRobot()
        : base("ID")
    {
    }
}
