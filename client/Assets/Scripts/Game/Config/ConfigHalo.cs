﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
class ConfigHaloLine
{
    public int Id;
    public int Radius;
    public int OwnerBuff;
    public int TeammateBuff;
    public int EnemyBuff;
    public string Effect;
}

class ConfigHalo : ConfigBase<ConfigHaloLine>
{
    public ConfigHalo()
        : base("Id")
    {

    }
}

