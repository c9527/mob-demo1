﻿using System;

[Serializable]
public class ConfigDisplayPartLine
{
    public string key;
    //todo:定义配置字段

    public string ID;
    public string MPModel;
    public string OPModel;
}

public class ConfigDisplayPart : ConfigBase<ConfigDisplayPartLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigDisplayPart() : base("ID")
    {

    }
}
