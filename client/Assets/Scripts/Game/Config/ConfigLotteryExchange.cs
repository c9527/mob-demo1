﻿using System;
using System.Collections.Generic;

[Serializable]
public class TDLotteryExchange
{
    public int Id;
    public int ITEM;
    public int ExchangePoints;
}

class ConfigLotteryExchange : ConfigBase<TDLotteryExchange>
{
    public ConfigLotteryExchange()
        : base("Id")
    {
        
    }
}

