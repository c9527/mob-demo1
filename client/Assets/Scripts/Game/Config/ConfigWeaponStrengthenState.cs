﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
class ConfigWeaponStrengthenStateLine
{
    public int ID;
    public int Weapon;
    public int State;
    public int DisplayStuff;
    public ItemInfo[] Consume;
}

class ConfigWeaponStrengthenState : ConfigBase<ConfigWeaponStrengthenStateLine>
{
    public ConfigWeaponStrengthenState()
        : base("ID")
    {

    }

    public ConfigWeaponStrengthenStateLine getUpgradeInfo(int weapon_id, int state)
    {
        ConfigWeaponStrengthenStateLine result = null;
        ConfigWeaponStrengthenStateLine max = null;
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].Weapon == weapon_id )
            {
                if(m_dataArr[i].State==state)
                {
                    result = m_dataArr[i];
                    break;
                }
                else if(m_dataArr[i].State==state-1)
                {
                    max = m_dataArr[i];
                }
            }
        }
        return result == null ? max : result;
    }
}
