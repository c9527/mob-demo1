﻿using System;

[Serializable]
public class ConfigMaterialLine
{
    public string key;
    //todo:定义配置字段

    public int ID;
    public string HitEffect;
    public string HitEffectOP;
    public float HitEffectMin;
    public float HitEffectMax;
    public float HitEffectRange;
    public float HitEffectTime;
    public string ExpEffect;
    public float ExpEffectTime;
    public string KnifeEffect;
    public float KnifeEffectTime;
    public string KnifeHitSound;
    public string HitSound;
    public string ExpSound;
    public string WalkSound;
    public string JumpSound;
}

public class ConfigMaterial : ConfigBase<ConfigMaterialLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigMaterial() : base("ID")
    {
    }
}
