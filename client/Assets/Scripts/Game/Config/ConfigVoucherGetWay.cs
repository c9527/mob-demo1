﻿using System;

[Serializable]


public class ConfigVoucherGetWayLine
{
    public int ID;
    public string Name;
    public string Key;
    public string ToPanel;

}

public class ConfigVoucherGetWay : ConfigBase<ConfigVoucherGetWayLine>
{
    private ConfigVoucherGetWay()
        : base("ID")
    {

    }
}

