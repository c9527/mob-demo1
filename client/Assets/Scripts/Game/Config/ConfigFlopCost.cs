﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ConfigFlopCostLine
{
    public int ID;
    public string Rule;
    public string MoneyType;
    public int Time;
    public int Cost;
    public int RewardType;
}

public class ConfigFlopCost:ConfigBase<ConfigFlopCostLine>  {
    public ConfigFlopCost():base("ID")
    {

    }
	
}
