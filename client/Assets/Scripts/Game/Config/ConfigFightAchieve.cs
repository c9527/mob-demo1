﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]

public class ConfigFightAchieveLine
{
    public string Type;
    
    public string Tips;           // 连杀提示
    public int SoundPriority;          // 提示音优先级
    public string Sound;          // 提示音
    public int EffectPriority;         // 提示特效优先级
    public string Effect;         // 提示特效
    public int Honor;             // 荣誉
    public int KillNum;             // 击杀数

}

public class ConfigFightAchieve : ConfigBase<ConfigFightAchieveLine>
{
    private ConfigFightAchieve()
        : base("Type")
    {

    }
}