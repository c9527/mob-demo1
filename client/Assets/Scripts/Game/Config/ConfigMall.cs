﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConfigMallLine
{
    public int ID;
    public int ItemId;
    public string Name;
    public int UnlockLevel;
    public int UnlockVipLevel;
    public int TopIndex;
    public int NormalIndex;
    public EnumMoneyType MoneyType;
    public int ShopType;
    public string Type;
    public string SubType;
    public Vector2 IconOffset;
    public ItemMallPrice[] Price;
    public int CheckStrengthenLevel;
    public float Discount;
    public int DiscountStartTime;
    public int DiscountEndTime;
    public int HotStartTime;
    public int HotEndTime;
    public int SaleStartTime;
    public int SaleEndTime;
    public int LimitAmount;
    public int[] ShowLevel;
    public int CanSend;
    public int LimitTime;
    public string Lang;
}

public class ConfigMall : ConfigBase<ConfigMallLine>
{
    private Dictionary<string, ConfigMallLine[]> m_lang2MallLines = new Dictionary<string, ConfigMallLine[]>(); 

    //商城中物品可能会重复，所以不能用ItemId作为关键字
    private ConfigMall() : base("ID")
    {
    }

    /// <summary>
    /// 默认从普通商店找，shop_type==-1时会忽略shop_type，取配置表中第一个找到的数据
    /// </summary>
    /// <param name="code"></param>
    /// <param name="shop_type"></param>
    /// <returns></returns>
    public ConfigMallLine GetMallLine(int code, int shop_type=0)
    {
        ConfigMallLine result = null;
        var dataArr = GetMallLineByLang(SdkManager.Lang);
        for (int i = 0; i < dataArr.Length; i++)
        {
            if (dataArr[i].ItemId == code && (shop_type == -1 || dataArr[i].ShopType == shop_type))
            {
                result = dataArr[i];
                break;
            }
        }
        return result;
    }


    public ConfigMallLine[] GetMallLineByLang()
    {
        return GetMallLineByLang(SdkManager.Lang);
    }

    public ConfigMallLine[] GetMallLineByLang(string lang)
    {
        if (String.IsNullOrEmpty(lang))
        {
            return m_dataArr;
        }
        if (!m_lang2MallLines.ContainsKey(lang))
        {
            List<ConfigMallLine> mallLines = new List<ConfigMallLine>();
            for (int i = 0; i < m_dataArr.Length; i++)
            {
                if (String.IsNullOrEmpty(m_dataArr[i].Lang) || m_dataArr[i].Lang.Equals(lang))
                {
                    mallLines.Add(m_dataArr[i]);
                }
            }
            m_lang2MallLines[lang] = mallLines.ToArray();
        }
        return m_lang2MallLines[lang];
    }

}