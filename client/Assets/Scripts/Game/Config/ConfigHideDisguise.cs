﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class HideDisguiseLine
{
    public int ID;
    public int Map;
    public int DiamondCost;
    public int AgainSpend;
}

public class ConfigHideDisguise : ConfigBase<HideDisguiseLine>
{

    public ConfigHideDisguise()
        : base("ID")
    {

    }
    public int GetHideBuffLineByMapId(int mapId)
    {
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            if (m_dataArr[i].Map == mapId)
            {
                return m_dataArr[i].AgainSpend;
            }
        }
        return 0;
    }
}
