﻿using System;

[Serializable]

public class ConfigMissionLine
{
    public int ID;              // 任务id
    public string Desc;         // 任务内容描述                       
    public int Type;            // 任务类型 1:每日任务2:新手等级任务3:新手常规任务4:主线任务5:支线任务6:活动任务
    public int Count;           // 任务计数
    public string RewardId;     // 奖励id
    public string Icon;         // Icon
    public string GoSystem;     // 直接跳转到的系统
    public bool Visible;     //可见
    public int TakeStartTime;
    public int TakeEndTime;
    public int Index;//显示顺序
    public string Cond;         //条件类型
    public string[] Params;     //条件参数
    public string[] Timelimit; //时间限制
    public int EVENT; //是否是活动任务
    public string[] Lang;
}

public class ConfigMission : ConfigBase<ConfigMissionLine>
{
    private ConfigMission()
        : base("ID")
    {

    }

    public static bool CheckLang(string lang,ConfigMissionLine line)
    {
        if (line.Lang.Length == 0)
        {
            return false;
        }
        for (int i = 0; i < line.Lang.Length; i++)
        {
            if (line.Lang[i] == SdkManager.Lang)
            {
                return true;
            }
        }
        return false;
    }
}