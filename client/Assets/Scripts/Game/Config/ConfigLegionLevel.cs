﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]

public class ConfigLegionLevelLine
{
    public int Level;
    public int Activeness;
    public int MaxCorps;
    public string Disp;
}

public class ConfigLegionLevel : ConfigBase<ConfigLegionLevelLine>
{
    private ConfigLegionLevel()
        : base("Level")
    {
    }
}

