﻿using System;

[Serializable]
public class ConfigItemOtherLine : ConfigItemLine
{
    public string DispShortName;
    public int ConvertType;
    public string[] EffectType;
    public int[] VirtualItemList;
    public string UseParam;
    public string GuideData;
    public int AutoUseGift;
    public int UseEffect;
}

public class ConfigItemOther : ConfigBase<ConfigItemOtherLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigItemOther() : base("ID")
    {
    }
}
