﻿using System;
using UnityEngine;

[Serializable]

public class ConfigDropItemLine
{
    public int DropId;
    
    public float ValidTime;                                /// 
    public string DropType;
    public string[] ModePath;       /// 模型列表，和掉落物品列表一一对应
    public string[] Effect;         // 模型物资
    public string EffectShowCamp;   // 特效显示阵营
    public float Height;            // 模型高度
    public float Radius;
    public float[] EulerAngles;
    public int[] Camps;               //可拾取阵营0表示不限阵营;1警;2匪;3生化人  
    public string DropList;
    public int UIiconRange;
    public string LogoName;
    public Vector3 LogoScale;
    public int LogoStarLevel;
    public Vector3 LogoOffSet;
    public string LogoShowCamp;
    public int AutoPickUp;         //自动获取
    public int CD;                 //进度条显示时间

    private KeyValue<int, int>[] _item_list;

    public KeyValue<int, int>[] GetItemList()
    {
        if (_item_list == null)
        {
            var temp = DropList.Split(';');
            _item_list = new KeyValue<int, int>[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                var strs = temp[i].Split('#');
                _item_list[i] = new KeyValue<int, int>();
                if (strs.Length == 1)
                {
                    _item_list[i].key = int.Parse(strs[0]);
                    _item_list[i].value = int.Parse(strs[0]);
                }
                else if (strs.Length == 2)
                {
                    _item_list[i].key = int.Parse(strs[0]);
                    _item_list[i].value = int.Parse(strs[1]);
                }
            }
        }
        return _item_list;
    }
}

public class ConfigDropItem : ConfigBase<ConfigDropItemLine>
{
    private ConfigDropItem()
        : base("DropId")
    {

    }
}