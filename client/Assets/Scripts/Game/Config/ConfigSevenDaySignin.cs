﻿using UnityEngine;
using System.Collections;

public class ConfigSevenDaySigninLine
{
    public int ID;
    public string Reward;
    public string VipReward;
}

public class ConfigSevenDaySignin : ConfigBase<ConfigSevenDaySigninLine> {

	private ConfigSevenDaySignin():base("ID")
    {

    }
}
