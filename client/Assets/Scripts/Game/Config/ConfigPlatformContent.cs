﻿using System;
using UnityEngine;

[Serializable]
public class ConfigPlatformContentLine
{
    public string GroupName;
    public int PlatformId;
    public string Device;
    public string Content;
}

public class ConfigPlatformContent : ConfigBase<ConfigPlatformContentLine>
{
    public const string LOGO = "Logo";
    public const string GAME_SHARE_QRCODE = "GameShareQRCode";
    public const string CUSTOMER_SERVICE = "CustomerService";
    public const string EXIT_TIP = "ExitTip";
    public const string SHOW_CHUSHOUTV= "ShowChuShouTVButton";

    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigPlatformContent() : base()
    {
    }

    public string GetContent(string groupName)
    {
        return GetContent(groupName, SdkManager.PlatformForSeparate, Driver.m_platform.ToString());
    }

    private string GetContent(string groupName, int pid, string device)
    {
        var lastIndex = -1; //默认项，如果一个组内找不到对应的，则使用最后一个作为默认项
        for (int i = 0; i < m_dataArr.Length; i++)
        {
            var item = m_dataArr[i];
            if (item.GroupName != groupName) 
                continue;

            lastIndex = i;
            if ((item.PlatformId == 0 || item.PlatformId == pid) &&
                (string.IsNullOrEmpty(item.Device) || item.Device == device))
                return item.Content;
        }

        if (lastIndex != -1)
            return m_dataArr[lastIndex].Content;
        return "";
    }
}
