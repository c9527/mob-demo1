﻿using System;
using System.Collections.Generic;

[Serializable]
public class ConfigRoleIconLine
{
    public int ID;
    public int Sex;
    public string Name;
    public int DisplayIndex;
    public int Source;//头像来源：0-默认，>0-仓库中物品
    public string IconGetDesc;
}

public class ConfigRoleIcon : ConfigBase<ConfigRoleIconLine>
{
    //base可根据需要指定字段名作为主键来创建字典对象，方便读取
    private ConfigRoleIcon() : base("ID")
    {
    }
}
