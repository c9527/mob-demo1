﻿class GuidePanelTrain : BaseGuide
{
    private BasePanel m_panel;

    protected override void DoGuide()
    {
        m_panel = UIManager.PopPanel<PanelGuideTrain>(new object[] { this }, true, null, LayerType.POP, m_config.MaskAlpha);
    }

    protected override void EndGuide()
    {
        if (m_panel != null)
            m_panel.HidePanel();
        m_panel = null;
    }

    protected override void Update()
    {
    }
}