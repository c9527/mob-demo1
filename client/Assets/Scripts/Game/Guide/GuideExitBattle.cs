﻿using UnityEngine;

class GuideExitBattle : BaseGuide
{
    private bool m_enable;
    private const float m_interval = 0.5f;
    private float m_nextTime;

    protected override void DoGuide()
    {
        GuideManager.m_isEnterBattle = false;
        m_nextTime = Time.time + m_interval;
        m_enable = true;

//        CDict proto = new CDict();
//        proto["typ"] = "room";
//        proto["key"] = "leave_room";
//        NetLayer.SendPack(proto);
    }

    protected override void EndGuide()
    {
        GuideManager.ResetTag();
        m_enable = false;
    }

    protected override void Update()
    {
        if (!m_enable || SceneManager.singleton == null)
            return;
        if (Time.time < m_nextTime)
            return;
        m_nextTime += m_interval;

        if (UIManager.IsOpen<PanelHall>())
            CompleteGuide();
    }
}