﻿using UnityEngine;
using UnityEngine.UI;

class GuideUIEffect : BaseGuide
{
    private UIEffect m_uiEffect;
    private Transform m_tranTarget;

    protected override void DoGuide()
    {
        m_tranTarget = GetTarget();
        //如果当前没有指定对象，则在Update里一直查找
        if (m_tranTarget != null)
            m_uiEffect = UIEffect.ShowEffect(m_tranTarget, m_config.Body, Mathf.Max(0, m_config.Time), m_config.Position);
    }

    protected override void EndGuide()
    {
        if (m_uiEffect != null)
            m_uiEffect.Destroy();
        m_uiEffect = null;
    }

    protected override void Update()
    {
        if (m_tranTarget == null)
        {
            m_tranTarget = GetTarget();
            if (m_tranTarget != null)
                m_uiEffect = UIEffect.ShowEffect(m_tranTarget, m_config.Body, Mathf.Max(0, m_config.Time), m_config.Position);
        }
    }
}