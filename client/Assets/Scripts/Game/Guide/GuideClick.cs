﻿using UnityEngine;
using UnityEngine.EventSystems;

class GuideClick : BaseGuide
{
    private Transform m_tranTarget;

    protected override void DoGuide()
    {
        if (m_config == null)
            return;
        m_tranTarget = GetTarget();

        //如果当前没有指定对象，则在Update里一直查找
        if (m_tranTarget != null)
            AddClickEvent();
    }

    private void AddClickEvent()
    {
        if (m_config.CompleteType == GuideCompleteType.MouseClick)
            UGUIClickHandler.Get(m_tranTarget).onPointerClick += OnTargetClick;
        else if (m_config.CompleteType == GuideCompleteType.MouseDown)
            UGUIUpDownHandler.Get(m_tranTarget).onPointerDown += OnTargetClick;
    }

    private void OnTargetClick(GameObject target, PointerEventData eventData)
    {
        CompleteGuide();
    }

    protected override void EndGuide()
    {
        if (m_tranTarget != null && (m_config.CompleteType == GuideCompleteType.MouseClick))
        {
            //如果事件清0了，就销毁Handler。防止残留点击处理
            var handler = UGUIClickHandler.Get(m_tranTarget);
            handler.onPointerClick -= OnTargetClick;
            if (handler.IsEmpty())
                handler.RemoveAllHandler();
        }
        if (m_tranTarget != null && (m_config.CompleteType == GuideCompleteType.MouseDown))
            UGUIUpDownHandler.Get(m_tranTarget).onPointerDown -= OnTargetClick;

        m_tranTarget = null;
    }

    protected override void Update()
    {
        if (m_tranTarget == null)
        {
            m_tranTarget = GetTarget();
            if (m_tranTarget != null)
                AddClickEvent();
        }
    }
}