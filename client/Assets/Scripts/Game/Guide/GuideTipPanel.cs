﻿using UnityEngine;
using UnityEngine.UI;

class GuideTipPanel : BaseGuide
{
    private GameObject m_goRoot;
    private Transform m_tranTarget;

    protected override void DoGuide()
    {
        ResourceManager.LoadSprite(AtlasName.Guide, "guideSmallPanelBg", OnLoaded);
    }

    private void OnLoaded(Sprite sprite)
    {
        if (m_config == null)
            return;

        m_tranTarget = GetTarget();
        m_goRoot = new GameObject("imgGuide_" + m_config.GuideId);
        var imgBg = m_goRoot.AddComponent<Image>();
        imgBg.SetSprite(sprite);

        //如果当前没有指定对象，则在Update里一直查找
        if (m_tranTarget != null)
            imgBg.rectTransform.SetParent(m_tranTarget, false);
        else
        {
            m_goRoot.transform.SetParent(UIManager.m_rootCanvas, false);
            m_goRoot.TrySetActive(false);
        }

        imgBg.SetNativeSize();
        imgBg.rectTransform.anchorMin = m_config.AnchorMin;
        imgBg.rectTransform.anchorMax = m_config.AnchorMax;
        imgBg.rectTransform.anchoredPosition = new Vector2(m_config.Position.x, m_config.Position.y);

        var txtContent = Util.CreateText(imgBg.rectTransform, new Vector2(80, -20), 20);
        txtContent.gameObject.name = "txtContent";
        txtContent.rectTransform.anchorMin = Vector2.zero;
        txtContent.rectTransform.anchorMax = Vector2.one;
        txtContent.rectTransform.sizeDelta = new Vector2(-199, -101);
        txtContent.alignment = TextAnchor.MiddleLeft;
        txtContent.text = m_config.Content;
    }

    protected override void EndGuide()
    {
        if (m_goRoot != null)
            GameObject.Destroy(m_goRoot);
        m_goRoot = null;
        m_tranTarget = null;
    }

    protected override void Update()
    {
        if (m_goRoot == null)
            return;
        if (m_tranTarget == null)
        {
            m_tranTarget = GetTarget();
            if (m_tranTarget != null)
            {
                m_goRoot.transform.SetParent(m_tranTarget, false);
                m_goRoot.TrySetActive(true);
            }
        }
    }
}