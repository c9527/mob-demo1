﻿using UnityEngine;
using UnityEngine.UI;

class GuideKill : BaseGuide
{
    protected override void DoGuide()
    {
        GameDispatcher.AddEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
    }

    private void OnPlayerDie(BasePlayer shooter, BasePlayer dier, int iLastHitPart, int weapon)
    {
        if (shooter != MainPlayer.singleton)
            return;

        TimerManager.SetTimeOut(2, CompleteGuide);
    }

    protected override void EndGuide()
    {
        GameDispatcher.RemoveEventListener<BasePlayer, BasePlayer, int, int>(GameEvent.PLAYER_KILLED, OnPlayerDie);
    }

    protected override void Update()
    {
    }
}