﻿class GuideCheck : BaseGuide
{
    private int[] m_checkList; 

    protected override void DoGuide()
    {
        var arr = m_config.Target.Split(';');
        m_checkList = new int[arr.Length];
        for (int i = 0; i < arr.Length; i++)
        {
            m_checkList[i] = int.Parse(arr[i]);
        }
    }

    protected override void EndGuide()
    {
    }

    protected override void Update()
    {
        var allChecked = true;
        for (int i = 0; i < m_checkList.Length; i++)
        {
            var complete = false;
            for (int j = 0; j < m_completeGuideIdList.Count; j++)
            {
                if (m_checkList[i] == m_completeGuideIdList[j])
                {
                    complete = true;
                    break;
                }
            }

            if (!complete)
            {
                allChecked = false;
                break;
            }
        }

        if (allChecked)
            CompleteGuide();
    }
}