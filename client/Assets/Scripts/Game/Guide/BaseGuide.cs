﻿using System.Collections.Generic;
using UnityEngine;

abstract class BaseGuide
{
    private static ConfigGuide m_configGuide;
    private static List<BaseGuide> m_guideList;
    protected static List<int> m_completeGuideIdList; 
    private static int m_curGuideId = 0;    

    protected ConfigGuideLine m_config;
    protected abstract void DoGuide();
    protected abstract void EndGuide();
    protected abstract void Update();

    public ConfigGuideLine Config
    {
        get { return m_config; }
    }

    public static void Init()
    {
        m_configGuide = ConfigManager.GetConfig<ConfigGuide>();
        m_guideList = new List<BaseGuide>();
        m_completeGuideIdList = new List<int>();
    }

    public static void StartGuideFrom(int guideId)
    {
        if (LuaHook.CheckHook(HookType.Guide_Start, 0, guideId))
            return;
        DestroyGuide(false);
        m_curGuideId = guideId-1;
        if (m_guideList == null)
            m_guideList = new List<BaseGuide>();
        else
            m_guideList.Clear();

        if (m_completeGuideIdList == null)
            m_completeGuideIdList = new List<int>();
        else
            m_completeGuideIdList.Clear();
        DoNextGuide();
    }

    public static void StartChapterName(string name)
    {
        for (int i = 0; i < m_configGuide.m_dataArr.Length; i++)
        {
            var config = m_configGuide.m_dataArr[i];
            if (config.ChapterNameStart == name)
            {
                GlobalConfig.clientValue.guideChapterName = name;
                GlobalConfig.SaveClientValue("guideChapterName");
                Logger.Log("保存指引进度：" + GlobalConfig.clientValue.guideChapterName);
                StartGuideFrom(config.GuideId);
                break;
            }
        }
    }

    public static void DestroyGuide(bool sendLog = true)
    {
        //离开新手
        if (sendLog)
        {
            NetLayer.Send(new tos_player_log_event() { event_id = GuideManager.m_guideEventLogId });
            Logger.Log("新手指引步骤id:" + GuideManager.m_guideEventLogId);
        }
        if (m_guideList != null)
        {
            for (int i = 0; i < m_guideList.Count; i++)
            {
                m_guideList[i].EndGuide();
            }
        }
        m_guideList = null;
        m_completeGuideIdList = null;
        ResetTag();
    }

    public static bool IsGuiding()
    {
        if (m_guideList != null && m_guideList.Count > 0)
            return true;
        if (UIManager.IsOpen<PanelGuideAsk>())
            return true;
        return false;
    }

    public static void ResetTag()
    {
        GuideManager.m_isEnterBattle = false;
        if (MainPlayer.singleton != null)
        {
            MainPlayer.singleton.ForbidMove = false;
            MainPlayer.singleton.ForbidFire = false;
            MainPlayer.singleton.ForbidLookAround = false;
        }
    }

    private void TryLimitOperation()
    {
        if (m_config == null)
            return;
        GameDispatcher.Dispatch(GameEvent.UI_HIDE_MOVE_ICON, m_config.ForbidMove);
        GameDispatcher.Dispatch(GameEvent.UI_HIDE_SWITCH_ICON, m_config.ForbidSwitch);
        GameDispatcher.Dispatch(GameEvent.INPUT_MOVE, false);
        GameDispatcher.Dispatch(GameEvent.INPUT_FIRE, false);

        if (MainPlayer.singleton != null)
        {
            MainPlayer.singleton.ForbidFire = m_config.ForbidFire;
            MainPlayer.singleton.ForbidMove = m_config.ForbidMove;
            MainPlayer.singleton.ForbidLookAround = m_config.ForbidLookAround;
        }
    }

    public void CompleteGuide()
    {
        if (m_config == null)
            return;
        if (m_config.Time > 0)
            TimerManager.RemoveTimeOut(CompleteGuide);
        if (m_config.LogEventId != 0)
        {
            NetLayer.Send(new tos_player_log_event() {event_id = m_config.LogEventId});
            Logger.Log("新手指引步骤id:" + m_config.LogEventId);
        }
        if (m_config.TryLeaveRoom)
        {
            if (RoomModel.IsInRoom)
            {
                NetLayer.Send(new tos_room_leave());
                if (UIManager.IsOpen<PanelHall>())
                    UIManager.GetPanel<PanelHall>().SetNormalBack();
            }
        }

        //当前章节结束，并更改当前所在章节为指定的下一章节名
        if (m_config.ChapterComplete.Length > 0 && !GuideManager.IsGuideFinish())
        {
            if (GlobalConfig.clientValue.guideChapterName == m_config.ChapterComplete[0])
                GlobalConfig.clientValue.guideChapterName = m_config.ChapterComplete[1];
            GlobalConfig.SaveClientValue("guideChapterName");
            Logger.Log("保存指引进度：" + GlobalConfig.clientValue.guideChapterName);
        }

        var time = m_config.Time;
        var guideId = m_config.GuideId;
        EndGuide(guideId);

        m_completeGuideIdList.Add(guideId);

        if (this is GuideAudio && time < 0)
            return;
        //防止非当前指引对象推动指引进程
        if (guideId == m_curGuideId)
            DoNextGuide();
    }

    public static void UpdateDriver()
    {
        if (m_guideList == null)
            return;
        for (int i = m_guideList.Count - 1; i >= 0; i--)
        {
            if (i < m_guideList.Count)
            {
                for (int j = 0; j < m_guideList[i].Config.ClosePanel.Length; j++)
                {
                    UIManager.HidePanel(m_guideList[i].Config.ClosePanel[j]);
                }
                m_guideList[i].Update();
            }
        }
    }

    /// <summary>
    /// 尝试继续指引，检查角色等级够不够
    /// </summary>
    public static void TryContinueGuide()
    {
        if (IsGuiding())
            return;
        m_curGuideId--;
        DoNextGuide();
    }

    private static void DoNextGuide()
    {
        m_curGuideId++;
        if (LuaHook.CheckHook(HookType.Guide_DoNext, 0, m_curGuideId))
            return;

        var config = m_configGuide.GetLine(m_curGuideId);
        if (config == null)
        {
            DestroyGuide();
            return;
        }
        TryEndGuide();

        if (PlayerSystem.roleData.level < config.LimitLevel)
            return;

        switch (config.Type)
        {
            case GuideType.Panel: DoGuide<GuidePanel>(config); break;
            case GuideType.PanelTrain: DoGuide<GuidePanelTrain>(config); break;
            case GuideType.Image: DoGuide<GuideImage>(config); break;
            case GuideType.UIEffect: DoGuide<GuideUIEffect>(config); break;
            case GuideType.Tip: DoGuide<GuideTip>(config); break;
            case GuideType.TipPanel: DoGuide<GuideTipPanel>(config); break;
            case GuideType.Arrow: DoGuide<GuideArrow>(config); break;
            case GuideType.CreateObject: DoGuide<GuideCreate>(config); break;
            case GuideType.AimTarget: DoGuide<GuideAimTarget>(config); break;
            case GuideType.AimTargetMulti: DoGuide<GuideAimTargetMulti>(config); break;
            case GuideType.Trigger: DoGuide<GuideTrigger>(config); break;
            case GuideType.Kill: DoGuide<GuideKill>(config); break;
            case GuideType.EnterBattle: DoGuide<GuideEnterBattle>(config); break;
            case GuideType.ExitBattle: DoGuide<GuideExitBattle>(config); break;
            case GuideType.Wait: DoGuide<GuideWait>(config); break;
            case GuideType.Audio: DoGuide<GuideAudio>(config); break;
            case GuideType.End: DoGuide<GuideEnd>(config); break;
            case GuideType.GotoChapter: DoGuide<GuideGotoChapter>(config); break;
            case GuideType.Click: DoGuide<GuideClick>(config); break;
            case GuideType.Check: DoGuide<GuideCheck>(config); break;
            case GuideType.Setting: DoGuide<GuideSetting>(config); break;
            default:
                Logger.Warning("未识别的指引类型：" + config.Type);
                break;
        }
    }

    private static void DoGuide<T>(ConfigGuideLine config) where T : BaseGuide, new()
    {
        for (int i = 0; i < m_guideList.Count; i++)
        {
            if (m_guideList[i].m_config.GuideId == config.GuideId)
                return;
        }

        var guide = new T { m_config = config };
        m_guideList.Add(guide);
        guide.DoGuide();
        guide.TryLimitOperation();
        if (config.Time > 0)
            TimerManager.SetTimeOut(config.Time, guide.CompleteGuide);
        else if (config.Time < 0)
            DoNextGuide();
    }

    private static void TryEndGuide()
    {
        var config = m_configGuide.GetLine(m_curGuideId);
        for (int i = 1; i < m_curGuideId; i++)
        {
            var preConfig = m_configGuide.GetLine(i);
            if (preConfig.EndId != 0 && config.GuideId >= preConfig.GuideId + preConfig.EndId)
            {
                EndGuide(preConfig.GuideId);
            }
        }
    }

    private static void EndGuide(int guideId)
    {
        for (int i = 0; i < m_guideList.Count; i++)
        {
            var guide = m_guideList[i];
            if (guide.m_config.GuideId == guideId)
            {
                m_guideList.RemoveAt(i);
                guide.EndGuide();
                guide.m_config = null;
                return;
            }
        }
    }

    protected Transform GetTarget()
    {
        var targetGo = GameObject.Find(m_config.Target);
        var tranTarget = targetGo != null ? targetGo.transform : null;
        if (tranTarget != null && !tranTarget.gameObject.activeInHierarchy)
            tranTarget = null;
        if (tranTarget == null)
        {
            var arr = m_config.Target.Split('/');
            targetGo = GameObject.Find("UGUIRootCanvas/loadingLayer/Guide_mask/" + arr[arr.Length - 1]);
            tranTarget = targetGo != null ? targetGo.transform : null;
        }
        return tranTarget;
    }
}

public enum GuideType
{
    Panel,
    PanelTrain,
    Image,
    UIEffect,
    Arrow,
    Tip,
    TipPanel,
    CreateObject,
    Trigger,
    AimTarget,
    AimTargetMulti,
    Kill,
    EnterBattle,
    ExitBattle,
    Wait,
    Audio,
    End,
    GotoChapter,
    Click,
    Check,
    Setting,
}

public enum GuideCompleteType
{
    Normal,
    MouseClick,
    MaskMouseClick,
    MouseDown,
    MaskMouseDown,
}