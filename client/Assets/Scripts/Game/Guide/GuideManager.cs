﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GuideManager
{
    public const int m_guideStageId = 10000;
    public const int m_guideEventLogId = 60;
    public static bool m_isEnterBattle;

    public static void Init()
    {
        BaseGuide.Init();
    }

    public static void StartGuideId(int guideId)
    {
        BaseGuide.StartGuideFrom(guideId);
    }

    public static void StartChapterName(string name)
    {
        BaseGuide.StartChapterName(name);
    }

    public static void TryContinueGuide()
    {
        BaseGuide.TryContinueGuide();
    }

    public static void DestroyGuide(bool sendLog = true)
    {
        BaseGuide.DestroyGuide(sendLog);
    }

    public static void ResetTag()
    {
        BaseGuide.ResetTag();
    }

    public static bool IsGuiding()
    {
        return BaseGuide.IsGuiding();
    }

    public static void Update()
    {
        BaseGuide.UpdateDriver();
    }

    public static bool IsGuideFinish()
    {
        return GlobalConfig.clientValue.guideChapterName == "Finish";
    }
}