﻿class GuideSetting : BaseGuide
{
    private BasePanel m_panel;

    protected override void DoGuide()
    {
        UIManager.PopPanel<PanelGuideSetting>(new object[]{this}, true, null, LayerType.POP, m_config.MaskAlpha);
    }

    protected override void EndGuide()
    {
        if(m_panel != null)
        {
            m_panel.HidePanel();            
        }
        m_panel = null;
    }

    protected override void Update()
    {
        
    }
}