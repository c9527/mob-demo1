﻿using UnityEngine;

class GuideGotoChapter : BaseGuide
{
    private const float m_interval = 0.5f;
    private float m_nextTime;

    protected override void DoGuide()
    {
        if (Config.Title == "IfWeaponMaxExp")
        {
            if (StrengthenModel.Instance.isAccordUpgrade(int.Parse(Config.Content)))
                TimerManager.SetTimeOut(0.01f, ()=> StartChapterName(Config.Target));
            else
                TimerManager.SetTimeOut(0.01f, CompleteGuide);//不能再DoGuide里调用CompleteGuide
        }
    }

    protected override void EndGuide()
    {
    }

    protected override void Update()
    {
        if (Config == null)
            return;
        if (Time.time < m_nextTime)
            return;
        m_nextTime += m_interval;

        if (Config.Title == "WhenLevelGE")
        {
            if (PlayerSystem.roleData.level >= int.Parse(Config.Content))
                StartChapterName(Config.Target);
            else
                CompleteGuide();
        }
    }
}