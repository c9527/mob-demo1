﻿using UnityEngine;

class GuideAimTargetMulti : BaseGuide
{
    private bool m_enable;
    private const float m_interval = 0.5f;
    private float m_nextTime;

    protected override void DoGuide()
    {
        m_nextTime = Time.time+m_interval;
        m_enable = true;
    }

    protected override void EndGuide()
    {
        m_enable = false;
    }

    protected override void Update()
    {
        if (!m_enable || SceneManager.singleton == null)
            return;
        if (Time.time < m_nextTime)
            return;
        m_nextTime += m_interval;

        if (SceneCamera.singleton.camera == null)
            return;

        Ray kRay = AutoAim.CurAimRay;
        RaycastHit kRH;

        if (Physics.Raycast(kRay, out kRH, GameSetting.FIRE_RAYTEST_LENGTH,
            GameSetting.LAYER_MASK_OTHER_PLAYER_FRIEND | GameSetting.LAYER_MASK_OTHER_PLAYER_ENEMY | GameSetting.LAYER_MASK_BUILDING | GameSetting.LAYER_MASK_DMM_PLAYER))
        {
            if (kRH.collider.gameObject.layer == GameSetting.LAYER_VALUE_BUILDING)
                return;
            
            if (kRH.collider.transform.root.name == m_config.Target)
                CompleteGuide();
        }
    }
}