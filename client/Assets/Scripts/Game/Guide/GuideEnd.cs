﻿class GuideEnd : BaseGuide
{
    protected override void DoGuide()
    {
        GuideManager.DestroyGuide();
    }

    protected override void EndGuide()
    {
    }

    protected override void Update()
    {
    }
}