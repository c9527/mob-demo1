﻿using UnityEngine;

class GuideTrigger : BaseGuide
{
    private bool m_enable;
    private const float m_interval = 0f;
    private float m_nextTime;

    protected override void DoGuide()
    {
        m_nextTime = Time.time+m_interval;
        m_enable = true;
    }

    protected override void EndGuide()
    {
        m_enable = false;
    }

    protected override void Update()
    {
        if (!m_enable || MainPlayer.singleton == null || MainPlayer.singleton.transform == null)
            return;
        if (Time.time < m_nextTime)
            return;
        m_nextTime += m_interval;

        var arr = Physics.OverlapSphere(MainPlayer.singleton.transform.position, 1);
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i].transform.root.name == m_config.Target)
            {
                CompleteGuide();
                return;
            }
        }
    }
}