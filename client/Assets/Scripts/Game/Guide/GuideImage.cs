﻿using UnityEngine;
using UnityEngine.UI;

class GuideImage : BaseGuide
{
    private GameObject m_go;
    private Image m_imgMask;

    protected override void DoGuide()
    {
        ResourceManager.LoadSprite(AtlasName.Guide, m_config.Body, OnLoaded);
    }

    private void OnLoaded(Sprite sprite)
    {
        if (m_config == null)
            return;

        m_go = new GameObject("imgGuide_" + m_config.GuideId);
        var img = m_go.AddComponent<Image>();
        img.SetSprite(sprite);
        var parent = GameObject.Find(m_config.Target).transform;
        if (parent == null)
        {
            Logger.Error("指引目标不存在");
            return;
        }
        img.rectTransform.SetParent(parent, false);
        img.SetNativeSize();
        img.rectTransform.anchorMin = m_config.AnchorMin;
        img.rectTransform.anchorMax = m_config.AnchorMax;
        img.rectTransform.anchoredPosition = new Vector2(m_config.Position.x, m_config.Position.y);

        if (m_config.CompleteType == GuideCompleteType.MouseClick)
            UGUIClickHandler.Get(m_go).onPointerClick += delegate { CompleteGuide(); };
        else if (m_config.CompleteType == GuideCompleteType.MaskMouseClick)
        {
            UGUIClickHandler.Get(m_go).onPointerClick += delegate { CompleteGuide(); };
            if (m_imgMask == null)
            {
                m_imgMask = new GameObject("Guide_mask").AddComponent<Image>();
                m_imgMask.rectTransform.SetParent(UIManager.m_guideLayer, false);
                m_imgMask.gameObject.layer = GameSetting.LAYER_VALUE_UI;
                m_imgMask.rectTransform.SetAsFirstSibling();
                m_imgMask.rectTransform.anchorMin = Vector2.zero;
                m_imgMask.rectTransform.anchorMax = Vector2.one;
                m_imgMask.rectTransform.anchoredPosition = Vector2.zero;
                m_imgMask.rectTransform.sizeDelta = Vector2.zero;
                m_imgMask.color = new Color(0, 0, 0, m_config.MaskAlpha);
            }

            m_imgMask.rectTransform.SetParent(parent, true);
            img.rectTransform.SetParent(m_imgMask.rectTransform, true);
        }
    }

    protected override void EndGuide()
    {
        if (m_imgMask != null)
            GameObject.Destroy(m_imgMask.gameObject);
        if (m_go != null)
            GameObject.Destroy(m_go);
        m_go = null;
    }

    protected override void Update()
    {
    }
}