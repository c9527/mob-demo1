﻿using UnityEngine;

class GuideCreate : BaseGuide
{
    private GameObject m_go;

    protected override void DoGuide()
    {
        ResourceManager.LoadBattlePrefab(m_config.Body, OnLoaded, EResType.Battle, true);
    }

    private void OnLoaded(GameObject go, uint crc)
    {
        if (m_config == null)
            return;

        m_go = go;
        if (!string.IsNullOrEmpty(m_config.Layer))
            m_go.ApplyLayer(LayerMask.NameToLayer(m_config.Layer));
        m_go.transform.localPosition = m_config.Position;
        m_go.transform.localEulerAngles = m_config.Rotation;
        m_go.transform.localScale = m_config.Scale;
    }

    protected override void EndGuide()
    {
        if (m_go != null)
            GameObject.Destroy(m_go);
        m_go = null;
    }

    protected override void Update()
    {
    }
}