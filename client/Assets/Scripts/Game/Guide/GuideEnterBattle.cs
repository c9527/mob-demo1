﻿using UnityEngine;

class GuideEnterBattle : BaseGuide
{
    private bool m_enable;
    private const float m_interval = 0f;
    private float m_nextTime;
    private int m_count;

    protected override void DoGuide()
    {
        m_nextTime = Time.time + m_interval;
        m_enable = true;
        NetLayer.AddHandler(this);
        m_count = 0;
        GuideManager.m_isEnterBattle = true;
        NetLayer.AddHandler(this);
        NetLayer.Send(new tos_joinroom_create() { channel = RoomModel.FindChannelByType(ChannelTypeEnum.stage), stage = GuideManager.m_guideStageId });
        Logger.Warning("Guide_1");
    }

    protected override void EndGuide()
    {
        Logger.Warning("Guide_5");
        NetLayer.RemoveHandler(this);
        m_enable = false;
    }

    protected override void Update()
    {
        if (!m_enable)
            return;

        if (UIManager.IsInited<PanelBattle>())
        {
            Logger.Warning("Guide_2");
            MainPlayer.singleton.SendSwitchWeaponMsg("WEAPON2");
            CompleteGuide();
        }

        if (Time.time < m_nextTime)
            return;
        m_nextTime += m_interval;
    }

    private void Toc_room_room_info(toc_room_info data)
    {
        if (ConfigManager.GetConfig<ConfigChannel>().GetLine(data.channel).Type == ChannelTypeEnum.stage)
        {
            if (data.stage == GuideManager.m_guideStageId)
            {
                NetLayer.Send(new tos_room_start_game());
                NetLayer.RemoveHandler(this);
            }
        }
    }
}