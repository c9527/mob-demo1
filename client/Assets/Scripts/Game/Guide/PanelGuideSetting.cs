﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelGuideSetting : BasePanel
{
    private GameObject m_moveImage;
    private GameObject m_moveShop;
    private GameObject m_nomoveShop;

    private GameObject m_nomoveCheck;
    private GameObject m_moveCheck;

    private GameObject m_btnReturn;

    Dictionary<string, int> m_dicDefaultKeyToIndex;
    Dictionary<string, int> m_dicUserKeyToIndex;
    private ConfigBattleParamSetting m_defaultParamSetting;
    private ConfigBattleParamSetting m_userParamSetting;
    

    int movingTime = 0;
    Vector3 initpos;
    int upordown = -1;

    public PanelGuideSetting()
    {
        SetPanelPrefabPath("UI/Setting/PanelGuideSetting");
        AddPreLoadRes("UI/Setting/PanelGuideSetting", EResType.UI);
        AddPreLoadRes("Atlas/Setting", EResType.Atlas);
    }

    public override void Init()
    {
        m_defaultParamSetting = ConfigManager.GetConfig<ConfigBattleParamSetting>();
        m_userParamSetting = ScriptableObject.CreateInstance<ConfigBattleParamSetting>();
        ReadUserBattleParamSetting();
        m_nomoveShop = m_tran.Find("frame/shotSetGroup/noMovingshot").gameObject;
        m_moveShop = m_tran.Find("frame/shotSetGroup/Movingshot").gameObject;

        m_moveCheck = m_tran.Find("frame/shotSetGroup/Movingshot/Image").gameObject;
        m_nomoveCheck = m_tran.Find("frame/shotSetGroup/noMovingshot/Image").gameObject;
        m_moveImage = m_tran.Find("frame/shotSetGroup/Movingshot/image/right/Image").gameObject;

        m_btnReturn = m_tran.Find("frame/return").gameObject;
        initpos = m_moveImage.transform.localPosition;

        m_dicDefaultKeyToIndex = new Dictionary<string, int>();
        m_dicUserKeyToIndex = new Dictionary<string, int>();

        BuildKeyToIndex(m_userParamSetting, m_dicUserKeyToIndex);
        BuildKeyToIndex(m_defaultParamSetting, m_dicDefaultKeyToIndex);
    }

    public override void InitEvent()
    {

        UGUIClickHandler.Get(m_btnReturn).onPointerClick += delegate { HidePanel(); };

        UGUIClickHandler.Get(m_moveShop).onPointerClick += OnClickShotTog;
        UGUIClickHandler.Get(m_nomoveShop).onPointerClick += OnClickShotTog;
        UGUIClickHandler.Get(m_tran.Find("frame/shotSetGroup/noMovingshot/Button")).onPointerClick += delegate { OnClickShotTog(m_nomoveShop, null); };
        UGUIClickHandler.Get(m_tran.Find("frame/shotSetGroup/Movingshot/Button")).onPointerClick += delegate { OnClickShotTog(m_moveShop, null); };
    }

    void OnClickShotTog(GameObject target, PointerEventData eventData)
    {
        if (target.name == "Movingshot")
        {
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 0.0f);
            m_nomoveShop.GetComponent<Toggle>().isOn = false;
            m_moveShop.GetComponent<Toggle>().isOn = true;
            m_nomoveCheck.TrySetActive(false);
            m_moveCheck.TrySetActive(true);
        }
        else if (target.name == "noMovingshot")
        {
            if (UIManager.IsBattle())
            {
                GameDispatcher.Dispatch(GameEvent.UI_CLICK_STABLE);
            }
            SetUserParamValueByKey(GlobalBattleParams.KEY_FIRE, 1.0f);
            m_nomoveShop.GetComponent<Toggle>().isOn = true;
            m_moveShop.GetComponent<Toggle>().isOn = false;
            m_nomoveCheck.TrySetActive(true);
            m_moveCheck.TrySetActive(false);
        }
    }

    void SetUserParamValueByKey(string key, float newValue)
    {
        int index = m_dicUserKeyToIndex[key];

        if (index >= m_userParamSetting.m_dataArr.Length)
        {
            return;
        }

        ConfigBattleParamSettingLine lineInfo = m_userParamSetting.m_dataArr[index];
        lineInfo.value = newValue;

        GlobalBattleParams.singleton.SetBattleParam(key, newValue);
    }

    void BuildKeyToIndex(ConfigBattleParamSetting source, Dictionary<string, int> to)
    {
        for (int index = 0; index < source.m_dataArr.Length; ++index)
        {
            to[source.m_dataArr[index].Key] = index;
        }
    }

    public override void OnShow()
    {
        
    }

    public override void Update()
    {
        if (movingTime >= 300)
        {
            movingTime = 0;
            upordown *= -1;
        }
        movingTime++;
        m_moveImage.transform.localPosition += new Vector3(0.15f, 0.15f, 0) * upordown;
    }

    void ReadUserBattleParamSetting()
    {
        m_userParamSetting.Load(m_defaultParamSetting);
        m_userParamSetting.m_dataArr = GlobalBattleParams.singleton.SaveArray();
    }

    public override void OnBack()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnDestroy()
    {
        
    }

}
