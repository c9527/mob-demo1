﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class GuideArrow : BaseGuide
{
    private GameObject m_goRoot;
    private Transform m_tranTarget;
    private Image m_imgArrow;
    private Image m_imgMask;

    private Transform m_oldTargetParent;
    private bool m_oldTargetParentGridLayoutGroupEnable;
    private bool m_oldTargetParentContentSizeFitterEnable;
    private int m_oldSiblingIndex;
    private GridLayoutGroup m_targetParentGridLayoutGroup;
    private ContentSizeFitter m_targetParentContentSizeFitter;

    protected override void DoGuide()
    {
        ResourceManager.LoadSprite(AtlasName.Guide, "guideArrow", OnLoaded);
    }

    private void OnLoaded(Sprite sprite)
    {
        if (m_config == null)
            return;
        m_tranTarget = GetTarget();
        m_goRoot = new GameObject("imgGuide_" + m_config.GuideId);

        //如果当前没有指定对象，则在Update里一直查找
        if (m_tranTarget != null)
            m_goRoot.transform.SetParent(m_tranTarget, false);
        else
        {
            m_goRoot.transform.SetParent(UIManager.m_rootCanvas, false);
            m_goRoot.TrySetActive(false);
        }

        m_goRoot.AddComponent<RectTransform>().anchoredPosition = new Vector2(m_config.Position.x, m_config.Position.y);;
        m_goRoot.transform.localScale = m_config.Scale;

        m_imgArrow = new GameObject("imgArrow").AddComponent<Image>();
        m_imgArrow.rectTransform.SetParent(m_goRoot.transform, false);
        m_imgArrow.SetSprite(sprite);
        m_imgArrow.SetNativeSize();
        m_imgArrow.rectTransform.anchorMin = m_config.AnchorMin;
        m_imgArrow.rectTransform.anchorMax = m_config.AnchorMax;
        m_imgArrow.rectTransform.localEulerAngles = m_config.Rotation;

        var txtTip = Util.CreateText(m_goRoot.transform, new Vector2(0, 23));
        txtTip.gameObject.name = "txtTip";
        txtTip.rectTransform.anchorMin = Vector2.zero;
        txtTip.rectTransform.anchorMax = Vector2.one;
        txtTip.rectTransform.anchoredPosition = new Vector2(0, 23);
        txtTip.rectTransform.sizeDelta = Vector2.zero;
        txtTip.alignment = TextAnchor.UpperCenter;
        txtTip.text = m_config.Content;
        txtTip.horizontalOverflow = HorizontalWrapMode.Overflow;

        if (m_tranTarget != null)
            AddClickEvent();
    }

    private void AddClickEvent()
    {
        if (m_tranTarget.parent.GetComponent<GridLayoutGroup>())
        {
            //如果目标的父对象上有GridLayoutGroup组件，要延迟一帧等他排好位置后再处理
            TimerManager.SetTimeOut(0.001f, () =>
            {
                m_targetParentGridLayoutGroup = m_tranTarget.parent.GetComponent<GridLayoutGroup>();
                if (m_targetParentGridLayoutGroup != null)
                {
                    m_oldTargetParentGridLayoutGroupEnable = m_targetParentGridLayoutGroup.enabled;
                    m_targetParentGridLayoutGroup.enabled = false;
                }

                m_targetParentContentSizeFitter = m_tranTarget.parent.GetComponent<ContentSizeFitter>();
                if (m_targetParentContentSizeFitter != null)
                {
                    m_oldTargetParentContentSizeFitterEnable = m_targetParentContentSizeFitter.enabled;
                    m_targetParentContentSizeFitter.enabled = false;
                }
                AddClickEventDirect();
            });
        }
        else
            AddClickEventDirect();
    }

    private void AddClickEventDirect()
    {
        if (m_imgMask != null)
            m_imgMask.gameObject.TrySetActive(false);

        if (m_config.CompleteType == GuideCompleteType.MouseClick)
            UGUIClickHandler.Get(m_tranTarget).onPointerClick += OnTargetClick;
        else if (m_config.CompleteType == GuideCompleteType.MouseDown)
            UGUIUpDownHandler.Get(m_tranTarget).onPointerDown += OnTargetClick;
        else if (m_config.CompleteType == GuideCompleteType.MaskMouseClick || m_config.CompleteType == GuideCompleteType.MaskMouseDown)
        {
            if (m_config.CompleteType == GuideCompleteType.MaskMouseClick)
                UGUIClickHandler.Get(m_tranTarget).onPointerClick += OnTargetClick;
            else if (m_config.CompleteType == GuideCompleteType.MaskMouseDown)
                UGUIUpDownHandler.Get(m_tranTarget).onPointerDown += OnTargetClick;
            if (m_imgMask == null)
            {
                m_imgMask = new GameObject("Guide_mask").AddComponent<Image>();
                m_imgMask.rectTransform.SetParent(UIManager.m_guideLayer, false);
                m_imgMask.gameObject.layer = GameSetting.LAYER_VALUE_UI;
                m_imgMask.rectTransform.SetAsFirstSibling();
                m_imgMask.rectTransform.anchorMin = Vector2.zero;
                m_imgMask.rectTransform.anchorMax = Vector2.one;
                m_imgMask.rectTransform.anchoredPosition = Vector2.zero;
                m_imgMask.rectTransform.sizeDelta = Vector2.zero;
                m_imgMask.color = new Color(0, 0, 0, m_config.MaskAlpha);
                var canvas = m_imgMask.gameObject.AddMissingComponent<Canvas>();
                canvas.overridePixelPerfect = true;
                canvas.pixelPerfect = true;
                m_imgMask.gameObject.AddComponent<SortingOrderRenderer>();
                m_imgMask.gameObject.TrySetActive(false);
            }

            m_oldSiblingIndex = m_tranTarget.GetSiblingIndex();
            m_oldTargetParent = m_tranTarget.parent;
            m_imgMask.gameObject.TrySetActive(true);
            m_imgMask.rectTransform.anchoredPosition = Vector2.zero;
            //延迟一帧改变target的位置，避免后面的指引也用到这个target造成找不到
            TimerManager.SetTimeOut(0.001f, () =>
            {
                m_tranTarget.SetParent(m_imgMask.rectTransform, true);
                SortingOrderRenderer.RebuildAll();
            });
            
        }
    }

    private void OnTargetClick(GameObject target, PointerEventData eventData)
    {
        CompleteGuide();
    }

    protected override void EndGuide()
    {
        if (m_tranTarget != null && (m_config.CompleteType == GuideCompleteType.MouseClick || m_config.CompleteType == GuideCompleteType.MaskMouseClick))
        {
            //UGUIClickHandler.Get(m_tranTarget).onPointerClick -= OnTargetClick;
            //如果事件清0了，就销毁Handler。防止残留点击处理
            var handler = UGUIClickHandler.Get(m_tranTarget);
            handler.onPointerClick -= OnTargetClick;
            if (handler.IsEmpty())
            {
                handler.RemoveAllHandler();
            }
        }
        if (m_tranTarget != null && (m_config.CompleteType == GuideCompleteType.MouseDown || m_config.CompleteType == GuideCompleteType.MaskMouseDown))
            UGUIUpDownHandler.Get(m_tranTarget).onPointerDown -= OnTargetClick;

        if (m_goRoot != null)
            GameObject.Destroy(m_goRoot);

        if (m_oldTargetParent != null && m_tranTarget != null)
        {
            m_tranTarget.SetParent(m_oldTargetParent, true);
            m_tranTarget.SetSiblingIndex(m_oldSiblingIndex);
        }
        if (m_targetParentGridLayoutGroup != null)
            m_targetParentGridLayoutGroup.enabled = m_oldTargetParentGridLayoutGroupEnable;
        if (m_targetParentContentSizeFitter != null)
            m_targetParentContentSizeFitter.enabled = m_oldTargetParentContentSizeFitterEnable;

        if (m_imgMask != null)
            GameObject.Destroy(m_imgMask.gameObject);

        m_imgMask = null;
        m_oldTargetParent = null;
        m_goRoot = null;
        m_tranTarget = null;
    }

    protected override void Update()
    {
        if (m_imgArrow)
        {
            if (m_config.Rotation.z == 0 || m_config.Rotation.z == 180)
                m_imgArrow.rectTransform.anchoredPosition = new Vector2(0, 10*Mathf.Sin(Time.time*8));
            else
                m_imgArrow.rectTransform.anchoredPosition = new Vector2(10*Mathf.Sin(Time.time*8), 0);

            if (m_tranTarget == null)
            {
                m_tranTarget = GetTarget();
                if (m_tranTarget != null)
                {
                    m_goRoot.transform.SetParent(m_tranTarget, false);
                    m_goRoot.TrySetActive(true);
                    AddClickEvent();
                }
            }
            else if (m_imgMask && !m_tranTarget.gameObject.activeSelf)
                m_imgMask.gameObject.TrySetActive(false);
        }
        else if (m_imgArrow == null && !ReferenceEquals(m_imgArrow, null))
        {
            //当arrow挂在其他界面里，被销毁后会重建
            m_imgArrow = null;  //防止重复进入该条件内
            DoGuide();
        }
    }
}