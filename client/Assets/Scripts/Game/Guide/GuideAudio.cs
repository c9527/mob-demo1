﻿class GuideAudio : BaseGuide
{
    protected override void DoGuide()
    {
        AudioManager.PlayGuideVoice(Config.Body);
        CompleteGuide();
    }

    protected override void EndGuide()
    {
    }

    protected override void Update()
    {
    }
}