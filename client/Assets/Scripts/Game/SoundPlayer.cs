﻿using UnityEngine;

public class SoundPlayer 
{
    private AudioSource m_kAS;

    private bool m_playingGunFireSound;
    private float m_GFtime;
    private float m_GFnextTime;
    private string[] m_GFstrSoundNames;
    private float[] m_GFprobabilitys;
    private float m_GFminSpawn;
    private float m_GFmaxSpawn;
    private float m_GFVolume;

	public SoundPlayer()
    {

    }

    public void Release()
    {
        // TODO:释放音频文件， 考虑游戏与UI音效分开
    }

    public void SetAudioSource(AudioSource kAS)
    {
        m_kAS = kAS;
        m_kAS.spread = 180;
    }

    public void PlayOneShot(string strSoundName, float fSoundVolume = 1.0f)
    {
        if (string.IsNullOrEmpty(strSoundName))
            return;

        AudioManager.PlayOneShot(m_kAS, strSoundName, fSoundVolume);
    }

    public void Stop()
    {
        if (m_kAS.isPlaying)
            m_kAS.Stop();
    }

    public void BeginGunFireSound(string[] strSoundNames, float[] probabilitys, float minSpawn, float maxSpawn, float fSoundVolume = 1.0f)
    {
        if (m_playingGunFireSound)
            return;
        m_playingGunFireSound = true;

        m_GFstrSoundNames = strSoundNames;
        m_GFminSpawn = minSpawn;
        m_GFmaxSpawn = maxSpawn;
        m_GFVolume = fSoundVolume;
        m_GFprobabilitys = probabilitys;

        m_GFtime = Time.time;
        m_GFnextTime = m_GFtime + Random.Range(m_GFminSpawn, m_GFmaxSpawn);

        PlayGunFireSound();
    }

    public void EndGunFireSound()
    {
        m_playingGunFireSound = false;
    }

    private void PlayGunFireSound()
    {
        if (m_GFstrSoundNames.Length == 0)
            return;
//        var r = Random.value;
//        var clipName = m_GFstrSoundNames[0];
//        for (int i = 1; i < m_GFprobabilitys.Length; i++)
//        {
//            if (r > m_GFprobabilitys[i - 1] && r <= m_GFprobabilitys[i])
//                clipName = m_GFstrSoundNames[i - 1];
//        }

        AudioManager.PlayOneShot(m_kAS, m_GFstrSoundNames[0], m_GFVolume, true);
//        ResourceManager.LoadAudio(PathHelper.GetAudioClipPath(m_GFstrSoundNames[0]), clip =>
//        {
//            m_kAS.Stop();
//            m_kAS.PlayOneShot(clip, m_GFVolume);
//        });
    }

    public void Update()
    {
        if (m_playingGunFireSound)
        {
            m_GFtime += Time.deltaTime;
            if (m_GFtime >= m_GFnextTime)
            {
                PlayGunFireSound();
                m_GFnextTime = m_GFnextTime + Random.Range(m_GFminSpawn, m_GFmaxSpawn);
            }
        }
    }

    public float radius
    {
        set
        {
            if (m_kAS != null)
                m_kAS.maxDistance = value;
        }
    }
}
