﻿public static class AudioSourceConst
{
    public const string SOUND_UI = "sound_ui";
    public const string SOUND_UI_LOOP = "sound_ui_loop";
    public const string SOUND_GUIDE_UI = "sound_guide_ui";
    public const string SOUND_FIGHT_2D = "sound_fight_2d";
    public const string SOUND_FIGHT_VOICE = "sound_fight_voice";
    public const string MUSIC_BACKGROUND1 = "music_background1";
    public const string MUSIC_BACKGROUND2 = "music_background2";
}