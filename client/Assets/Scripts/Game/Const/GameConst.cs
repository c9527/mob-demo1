﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameConst
{

    #region Game Rule
    public const string GAME_RULE_GUIDE_LEVEL = "guide_level";
    public const string GAME_RULE_SOLO = "solo";//个人竞技
    public const string GAME_RULE_KILLALL = "killall";//歼灭
    public const string GAME_RULE_PVP20 = "team_pvp20";//团队竞技
    public const string GAME_RULE_PVP40 = "team_pvp40";//团队竞技
    public const string GAME_RULE_PVP = "team_pvp";//团队竞技
    public const string GAME_RULE_BURST = "explode";//爆破
    public const string GAME_RULE_SNIPE = "snipe";     // 狙击战
    public const string GAME_RULE_DAGGER = "dagger";   // 刀战
    public const string GAME_RULE_GRENADE = "grenade"; // 手雷战
    public const string GAME_RULE_HANDGUN = "handgun"; // 手枪战
    public const string GAME_RULE_ZOMBIE = "zombie";//生化模式
    public const string GAME_RULE_ZOMBIE_HERO = "zombie_hero";//生化英雄模式
    public const string GAME_RULE_ZOMBIE_ULTIMATE = "zombie_ultimate"; //终极生化模式
    public const string GAME_RULE_KING_SOLO = "king_solo";//枪王模式
    public const string GAME_RULE_KING_CAMP = "king_camp";//团队枪王模式
    public const string GAME_RULE_GHOST = "ghost";//幽灵
    public const string GAME_RULE_DAGGER_REVIVE = "dagger_revive";//刀锋模式
    public const string GAME_RULE_EXPLODE_REVIVE = "explode_revive";//生死爆破
    public const string GAME_RULE_ERADICATE = "eradicate";//生存杀灭模式
    public const string GAME_RULE_ERADICATE2 = "eradicate2";//生存杀灭模式
    public const string GAME_RULE_DEFEND = "defend";//生存塔防模式
    public const string GAME_RULE_DEFEND0 = "defend0";//战队副本模式
    public const string GAME_RULE_DEFEND2 = "defend2";//战队副本模式
    public const string GAME_RULE_HIDE = "hide"; //躲猫猫模式
    public const string GAME_RULE_FIGHT_IN_TURN = "arena"; //擂台赛模式
    public const string GAME_RULE_SURVIVAL = "survival";   //生存New模式
    public const string GAME_RULE_BIG_HEAD = "big_head";   //大头模式
    public const string GAME_RULE_WORLD_BOSS = "world_boss"; //世界BOSS
    public const string GAME_RULE_BOSS_NEWER = "boss4newer"; //世界BOSS
    public const string GAME_RULE_TEAM_HEAO = "team_hero";  //团队英雄
    public const string GAME_RULE_SUR_SNIPE = "sur_snipe";  //生化阻击
    public const string GAME_RULE_DOTA = "dota";  //遗迹守护战
    public const string GAME_RULE_CHALLENGE = "challenge";  //生存挑战
    public const string GAME_RULE_STATE_SURVIVAL = "stage_survival";  //末日挑战模式
    public const string GAME_RULE_DAGGER_OW = "dagger_ow";  //刀锋据点模式
    public const string GAME_RULE_DAGGER_OW_TAG = "ow";  //刀锋据点模式
    #endregion

    #region Item Type
    static public string ITEM_TYPE_EQUIP = "EQUIP";
    static public string ITEM_TYPE_ROLE = "ROLE";
    static public string ITEM_TYPE_MONEY = "MONEY";
    static public string ITEM_TYPE_EXP = "EXP";
    static public string ITEM_TYPE_CORPS = "CORPS";
    static public string ITEM_TYPE_MATERIAL = "MATERIAL";
    static public string ITEM_TYPE_ITEMS = "ITEMS";
    #endregion

    #region Item Id
    static public int ITEM_ID_LABA = 5015;
    static public int ITEM_ID_VOUCHER = 390;
    public const string ITEM_ID_ANA_WEAPON_ID = "2108";
    #endregion

    #region Role SubType
    static public string ROLE_SUBTYPE_ROLE = "ROLE";
    static public string ROLE_SUBTYPE_BASE = "BASE";
    static public string ROLE_SUBTYPE_BOSS = "BOSS";
    static public string ROLE_SUBTYPE_OBJECT = "OBJECT";
    #endregion

    #region Weapon SubType
    static public string WEAPON_SUBTYPE_WEAPON1 = "WEAPON1";
    static public string WEAPON_SUBTYPE_WEAPON2 = "WEAPON2";
    static public string WEAPON_SUBTYPE_DAGGER = "DAGGER";
    static public string WEAPON_SUBTYPE_GRENADE = "GRENADE";
    static public string WEAPON_SUBTYPE_FLASHBOMB = "FLASHBOMB";
    static public string WEAPON_SUBTYPE_BOM = "BOM";
    static public string WEAPON_SUBTYPE_BULLET = "BULLET";
    static public string WEAPON_SUBTYPE_ARMOR = "ARMOR";
    static public string WEAPON_SUBTYPE_HELMET = "HELMET";
    static public string WEAPON_SUBTYPE_BOOTS = "BOOTS";
    static public string WEAPON_SUBTYPE_ACCESSORY = "ACCESSORY";
    static public string WEAPON_SUBTYPE_SKILLWEAPON = "SKILLWEAPON";
    #endregion

    #region Weapon AtkType
    public const int WEAPON_ATKTYPE_NORMAL = 1;
    public const int WEAPON_ATKTYPE_USEADVAMMO = 2;
    public const int WEAPON_ATKTYPE_SUBWEAPON = 3;
    #endregion

    #region Weapon ReloadType
    public const int WeaponReloadTypeWholly = 0;
    public const int WeaponReloadTypeOneByOne = 1;
    #endregion

    #region Prop SubType
    static public string ITEMS_SUBTYPE_RENAMECARD = "RENAMECARD";
    static public string ITEMS_SUBTYPE_BUFFCARD = "BUFFCARD";
    static public string ITEMS_SUBTYPE_OBJECT = "OBJECT";
    static public string ITEMS_SUBTYPE_MERGE = "MERGE";
    static public string ITEMS_SUBTYPE_HERO = "HERO";
    #endregion

    #region Battle

    public const float PLAYER_HIT_BACK_SPEED = 6.0f;

    public const float GRENADE_DECELERATE = 10f;
    public const float GRANDE_DECELERATE_TIME = 1.0f;

    public const float GHOST_MODE_BODY_ALPHA = 0.2f;
    public const float GHOST_MODE_ARM_ALPHA = 0.4f;
    public const float GHOST_MODE_STAND_TIME = 1f;

    public const byte MOVE_STATE_WALK = 1;
    public const byte MOVE_STATE_JUMP = 2;

    public const int JUMP_STAGE_START = 1;
    public const int JUMP_STAGE_TOP = 2;
    public const int JUMP_STAGE_GROUND = 3;

    public const int BAOPO_TOOLID = 66611;

    #endregion

    #region 同步
    public const int ACT_TYPE_MOVE = 1;
    public const int ACT_TYPE_FIRE = 2;
    public const int ACT_TYPE_RELOAD = 3;
    public const int ACT_TYPE_WEAPON_MOTION = 4;

    static public Vector3 Vector4399 = new Vector3(4399, 4399, 4399);
    static public Vector3 SetPosOffset = new Vector3(0, 0.1f, 0);
    #endregion

    #region DropItem

    static public string DROP_ITEM_ITEM = "item";
    static public string DROP_ITEM_WEAPON = "weapon";
    static public string DROP_ITEM_BOMB = "bomb";
    static public string DROP_ITEM_ZOMBIE = "zombie";

    public const int DROP_ITEM_ID_C4 = 1901;

    #endregion

    # region Server Msg Value

    public const int Fight_State_GameInit = 0;    //游戏初始化
    public const int Fight_State_GameStart = 1;   //游戏开始
    public const int Fight_State_RoundStart = 2;  //回合开始
    public const int Fight_State_RoundEnd = 3;    //回合结束
    public const int Fight_State_GameOver = 4;    //游戏结束

    public const int SERVER_ACTOR_STATUS_LOCK = 0;
    public const int SERVER_ACTOR_STATUS_LIVE = 1;
    public const int SERVER_ACTOR_STATUS_DEAD = 2;
    public const int SERVER_ACTOR_STATUS_DYING = 3;//濒死

    public const int PLAYER_STATUS_ALIVE = 1;

    #endregion

    #region Const String

    public const string MAT_WEAPON_LP_SPEC = "mat_weapon_LP_Spec";
    #endregion

    #region Material Name

    public const string MAT_HALL_PLAYER = "mat_hall_player";
    public const string MAT_HALL_WEAPON = "mat_hall_weapon";
    public const string MAT_HALL_WEAPON_CRYSTAL = "mat_hall_weapon_crystal";

    public const string MAT_TUJIAN_WEAPON = "mat_tujian_weapon";
    public const string MAT_TUJIAN_WEAPON_CRYSTAL = "mat_tujian_weapon_crystal";

    public const string MAT_ALPHASET_ONESIDE = "mat_alphaset_oneside";

    #endregion

    #region Shader Const

    public static List<string> SHADER_LIST_PARTICLE = new List<string> { SHADER_ADDITIVE, SHADER_ADDITIVE_TINTCOLOR, SHADER_ALPHABLEND, SHADER_ALPHABLEND_TINTCOLOR, SHADER_MULTIPLY, SHADER_TRANSPARENT };

    public const string SHADER_DIFFUSE = "FPS/Unity/Unity_Diffuse";
    public const string SHADER_LIGHTMAP = "FPS/Texture_Lightmap";
    public const string SHADER_LIGHTPROBE_TEX = "FPS/LightProbe_Texture";
    public const string SHADER_TEXTURE = "FPS/Texture";
    public const string SHADER_TRANSPARENT = "FPS/Transparent";
    public const string SHADER_ALPHASET = "FPS/Transparent_AlphaSet_OneSide";
    public const string SHADER_SKYBOX = "FPS/SkyBox";

    public const string SHADER_ADDITIVE = "FPS/Additive";
    public const string SHADER_ADDITIVE_TINTCOLOR = "FPS/Additive_TintColor";
    public const string SHADER_ALPHABLEND = "FPS/AlphaBlend";
    public const string SHADER_ALPHABLEND_TINTCOLOR = "FPS/AlphaBlend_TintColor";
    public const string SHADER_MULTIPLY = "FPS/Multiply";

    public const string SHADER_MP_WEAPON = "FPS/LightProbe_Spec";
    public const string SHADER_MP_WEAPON_FLOW = "FPS/LightProbe_Spec_Flow";
    public const string SHADER_MP_WEAPON_FLOW_TRANSPARENT = " FPS/LightProbe_Spec_Flow_Transparent";
    public const string SHADER_MP_WEAPON_CRYSTAL = "FPS/LightProbe_Spec_Flow_Transparent";

    public const string SHADER_OP_WEAPON = "FPS/Texture_Brightness";
   

    public const string SHADER_UI_DEFAULT = "UI/Default";
    public const string SHADER_UI_SPRITE_ETC = "UI/SpriteETC";
    public const string SHADER_UI_SPRITE_GRAY_ETC = "UI/SpriteGrayETC";
    public const string SHADER_FPS_UI_SHANGUANG = "FPS/UI_ShanGuang";
    public const string SHADER_FPS_UI_SHANGUANG_ETC = "FPS/UI_ShanGuangETC";

    //public const string SHADER_FLASHBOMB_EFFECT = "FPS/FlashBomb_Effect";

    public const string SHADER_NAME_LIGHTPROBE = "LightProbe";
    public const string SHADER_NAME_TRANSPARENT = "Transparent";

    public const string SHADER_PROPERTY_COLOR = "_Color";
    public const string SHADER_PROPERTY_TintColor = "_TintColor";
    public const string SHADER_PROPERTY_ALPHA = "_Alpha";
    public const string SHADER_PROPERTY_FLOWTEX = "_FlowTex";
    public const string SHADER_PROPERTY_FLOWTEX_COLOR = "_FlowColor";
    public const string SHADER_PROPERTY_FLOWDIR_X = "_FlowDir_X";
    public const string SHADER_PROPERTY_FLOWDIR_Y = "_FlowDir_Y";
    public const string SHADER_PROPERTY_FLOWSPEED = "_FlowSpeed";
    public const string SHADER_PROPERTY_SPECBRIGHTNESS = "_SpecBrightness";


    public const string SHADER_KEYWORD_LIGHTPROBE_ON = "LIGHTPROBE_ON";
    public const string SHADER_KEYWORD_LIGHTPROBE_OFF = "LIGHTPROBE_OFF";

    #endregion

    #region actor_type
    //0人，1母体，2精英，3小兵，100生化英雄
    public const int ACTOR_TYPE_PERSON = 0;             //人类
    public const int ACTOR_TYPE_MATRIX = 1;      //生化母体
    public const int ACTOR_TYPE_ELITE = 2;       //生化精英
    public const int ACTOR_TYPE_GENERAL = 3;     //生化小兵
    public const int ACTOR_TYPE_ULTIMATE_MATRIX = 4;    //终极母体
    public const int ACTOR_TYPE_HERO = 100;      //生化英雄
    public const int ACTOR_TYPE_BIG_HEAD_KING = 1001;   //大头王
    public const int ACTOR_TYPE_BIG_HEAD_HERO = 1002;   //大头英雄
    public const int ACTOR_TYPE_BIG_HEAD_DOTA = 1003;   //dota大头英雄
    public const int ACTOR_TYPE_BOSS = 200;             //生存模式BOSS
    public const int ACTOR_TYPE_MONSTER = 201;          //小怪
    public const int ACTOR_TYPE_TOWER = 202;            //dota中的塔
    public const int ACTOR_TYPE_STORY = -1;             //剧情怪
    #endregion

    #region Model Type
    public const int MODEL_TYPE_ROLE = 0;
    public const int MODEL_TYPE_DMM = 1;
    public const int MODEL_TYPE_STORY = 2;
    #endregion

    #region Game Const Value
    public const int GameSceneMaxPlayerCount = 100;
    #endregion

    #region Buff

    public const int TRANSFORM_BUFF_ID = 806001; // 变身id

    #endregion

    #region GAME_SHOP_ITEM_TYPE
    public const int SHOP_ITEM_TYPE_FILL_BULLET = 1;
    public const int SHOP_ITEM_TYPE_WEAPON = 2;
    public const int SHOP_ITEM_TYPE_PVPBUFF = 3; //DOTA模式下，是加血
    public const int SHOP_ITEM_TYPE_BOSSREVIVEBUFF = 4; //DOTA模式下，boss复活
    public const int SHOP_ITEM_TYPE_BOSSBUFF = 5; //DOTA模式下，boss血量增加
    public const int SHOP_ITEM_TYPE_PVEKBUFF = 6; //DOTA模式下，增加PVE伤害
    public const int SHOP_ITEM_TYPE_CUREBUFF = 5; //DOTA模式下，治愈buff
    #endregion

    #region GAME_SKILL_EFFECT
    public const string SKILL_EFFECT_ADDBUFF = "add_buff";           //添加buff
    public const string SKILL_EFFECT_USEWEAPON = "use_weapon";       //武器攻击
    public const string SKILL_EFFECT_SCOUT = "scout";                //索敌
    public const string SKILL_EFFECT_REVIVE = "revive";              //复活
    public const string SKILL_EFFECT_RUSH = "rush";                  //冲刺
    public const string SKILL_EFFECT_ACTIVE_SKILL = "active_skill";  //主动技能
    public const string SKILL_EFFECT_TRANSFORM = "transform";  //变身
    public const string SKILL_EFFECT_JETPACK = "jetpack";
    #endregion

    #region WeaponAwaken DEFINE
    public const string AWAKEN_WEAPON_EFFECT_FIRE = "weapon_effect_fire";        //枪火特效
    public const string AWAKEN_WEAPON_EFFECT_FLOW = "weapon_effect_flow";        //流光特效
    public const string AWAKEN_ACTOR_PROPS = "actor_props";
    public const string AWAKEN_ACTOR_SKILL = "actor_skill";

    public const int AWAKEN_SKILL_PASSIVE = 0;  //被动
    public const int AWAKEN_SKILL_ACTIVE = 1;   //主动
    #endregion

    #region EFFECT_CONST
    public const int EFFECT_CONST_GANRAN_ID = 800025;
    public const int EFFECT_CONST_GANRAN_TIME = 1000;

    #endregion

    #region ScoutSkillState
    public const int SCOUT_SKILL_STATE_NODEFINE = 0;
    public const int SCOUT_SKILL_STATE_START = 1;
    public const int SCOUT_SKILL_STATE_END = 2;
    #endregion

    #region cd减除类型
    public const int CDCLEARTYPE_ROUNDEND = 0;
    public const int CDCLEARTYPE_BATTLEEND = 1;
    #endregion

    #region 状态机
    public const string OTHERPLAYER_MOVE_STATE_NAME = "move";
    public const string OTHERPLAYER_WALKWAGGLE_LAYER_NAME = "WalkWaggle";
    public const int PLAYER_WALKWAGGLE_LAYER_INDEX = 2;
    public const int PLAYER_UPBODY_LAYER_INDEX = 1;
    #endregion

    #region 死亡动画类型
    public const int WEAPON_DIE_ANI_TYPE_LIGHT = 1;   //轻武器
    public const int WEAPON_DIE_ANI_TYPE_HEAVY = 2;   //重武器
    public const int WEAPON_DIE_ANI_TYPE_GRENADE = 3;   //榴弹
    #endregion

    #region 血条类型
    public const int SHOWHP_NONE = 0;
    public const int SHOWHP_FRIEND = 1;
    public const int SHOWHP_ENEMY = 2;
    public const int SHOWHP_ALL = 3;

    public const float YELLOW_MAX_HP = 0.75f;
    public const float RED_MAX_HP = 0.2f;

    public const int GREEN_HP = 0;
    public const int YELLOW_HP = 1;
    public const int RED_HP = 2;
    #endregion

    #region 头顶UI挂点挂载高度
    public const float HEAD_SLOT_HEIGHT_BIG_HEAD_KING = 1f;
    public const float HEAD_SLOT_HEIGHT_BIG_HEAD_HERO = 1f;
    public const float HEAD_SLOT_HEIGHT_HP_BAR = 0.13f;

    #endregion

    #region AIAction
    public const string AIACTION_USE_SKILL = "use_skill";      //使用技能
    public const string AIACTION_ALERT_RECTANGLE = "alert_rectangle";  //长方型预警
    public const string AIACTION_ALERT_CIRCLE = "alert_circle";        //圆型预警
    public const string AIACTION_ATTACK = "attack";            //攻击
    public const string AIACTION_ALERT = "alert";            //蓄力攻击，第一动作
    public const string AIACTION_JUMP = "jump";            //跳砸，第二动作
    public const string AIACTION_MOVE_FORWARD = "move_forward";            //冲刺，第二动作
    public const string AIACTION_MOVE2TARGET = "move2target";            //收招，第三动作
    public const string AIACTION_DAMAGE_AREA = "damage_area";            //撼地波
    public const int AIACTION_EFFECT_ATTACH_TARGET_PLAYER = 0;           //AI特效挂载在目标玩家（被施放者）上
    public const int AIACTION_EFFECT_ATTACH_TARGET_POINT = 1;            //AI特效挂载在目标点上
    public const int AIACTION_EFFECT_ATTACH_ATTACK_PLAYER = 2;           //AI特效挂载在攻击玩家（施放者）上

    public const int AISKILL_ALERT_HEIGHT_TYPE_PLAYER_GROUND = 0;        //AI技能预警高度-施法者落地点
    public const int AISKILL_ALERT_HEIGHT_TYPE_TARGET_GROUND = 1;        //AI技能预警高度-目标落地点
    public const int AISKILL_ALERT_HEIGHT_TYPE_PLAYER_CENTER = 2;        //AI技能预警高度-施法者高度
    public const int AISKILL_ALERT_HEIGHT_TYPE_PLAYER_POS = 3;        //AI技能预警高度-施法者脚底高度

    public const float AISKILL_ALERT_TEST_GROUND_POINT_FIXED_HEIGHT = 1; //AI技能测试高度时添加的固定高度
    #endregion

    #region 人物模型
    public const string PLAYER_MP_BOY_DEFAULT_MODEL = "arm";
    public const string PLAYER_MP_GIRL_DEFAULT_MODEL = "arm_girl";
    public const string PLAYER_MP_ELITE_ZOMBIE_DEFAULT_MODEL = "arm_shenghuayouling";
    public const string PLAYER_MP_MATRIX_ZOMBIE_DEFAULT_MODEL = "arm_sishen";
    public const string PLAYER_MP_ULTIMATE_ZOMBIE_DEFAULT_MODEL = "arm_queen";

    #endregion

    #region 设置相关（和服务器通讯的）
    public const string PARAMSETTING_AREA_VISIBLE = "area";//显示城市
    #endregion

    #region 持枪方式
    public const int PLAYER_WEAPON_HOLD_HADN_RIGHT = 1;
    public const int PLAYER_wEAPON_HOLD_HAND_LEFT = 0;

    #endregion

    #region 准星类型、摇杆、重刀类型
    public const float CROSS_POINT = 0.1f;//带红星的
    public const float CROSS_NO_POION = 0.2f;//不带红星的
    public const float CROSS_CIRCLE = 0.3f;//圆圈

    public const float JOYSTICK1 = 0.1f;
    public const float JOYSTICK2 = 0.2f;
    public const float JOYSTICK3 = 0.3f;

    public const float FIGHTDIRECTIONBTN_TOP = 0.1f;//左上
    public const float FIGHTDIRECTIONBTN_BOTTOM = 0.2f;//右下
    public const float FIGHTDIRECTIONBTN_HIDE = 0.3f;//关闭

    public const float JOYSTICK_MOVE1 = 0.1f;
    public const float JOYSTICK_MOVE2 = 0.2f;

    public const float NORMAL_HEAVYKNIFE = 0f;
    public const float CHANGE_HEAVY_KNIFE = 1f;

    public const float KNIFE_AUTOAIM_CLOSE = 0f;
    public const float KNIFE_AUTOAIM_OPEN = 1f;

    public const float ZOMBIE_AUTOAIM_CLOSE = 0f;
    public const float ZOMBIE_AUTOAIM_OPEN = 1f;

    public const float AUTOAIM_CLOSE = 0f;
    public const float AUTOAIM_OPEN = 1f;
    #endregion

    #region 外挂检测相关阈值
    // ！！！ 不要存在变量里了，内存会被修改
    //public const float PLAYER_MAX_SPEED = 15.0f;                 //玩家最大速度
    //public const float PLAYER_MOVE_CHECK_MAX_DISTANCE_SQRT = 3 * 3f; //玩家位移检测最大距离的平方
    //public const float PLAYER_POS_CHECK_MAX_DISTANCE_SQRT = 4 * 4;  //玩家坐标检测最大距离的平方
    //public const float JUMP_HEIGHTADD_MAX = 1.0f;
    //public const float JUMP_HEIGHTMAP_MAX = 5.0f;
    //public const float PLAYER_HIT_POS_MAX_DISTANCE_SQRT = 7 * 7f; //玩家击点偏移中心点最大距离的平方
    //public const float JUMP_MAX = 1f;
    #endregion

    #region GameRule Tags相关
    public const string ZOMBIE_MODE = "zombie_mode"; //生化类模式
    public const string SURVIVAL_MODE = "survivalMode"; //生存类模式
    #endregion

    #region XcodeParameter
    public const string FIVE_STARS_COMMENT_ADDRESS = "fiveStarsCommentAddress";
    #endregion

    #region 英雄卡
    public const string HEAD_NORMAL = "head_normal";
    public const string HEAD_SUPER = "head_super";
    public const string HEAD_LEGEND = "head_legend";

    public const string BUBBLE_NORMAL = "bubble_purple";
    public const string BUBBLE_SUPER = "bubble_yellow";
    public const string BUBBLE_LEGEND = "bubble_red";

    public const string ROOM_NORMAL = "room_purple";
    public const string ROOM_SUPER = "room_yellow";
    public const string ROOM_LEGEND = "room_red";
    
    public const string INFO_LINE_NORMAL = "info_purple_line";
    public const string INFO_LINE_SUPER = "info_yellow_line";
    public const string INFO_LINE_LEGEND = "info_red_line";

    #endregion

    #region 角色subType
    public const string PLAYER_SUBTYPE_BOSS = "BOSS";
    public const string PLAYER_SUBTYPE_OBJECT = "OBJECT";
    public const string PLAYER_SUBTYPE_BASE = "BASE";
    #endregion

    #region 家族卡

    public const int FAMILY_CARD_ID = 5038;

    #endregion

    #region 观战类似
    public const int WATCH_TYPE_TOTAL_COUNT = 3;
    public const int WATCH_TYPE_THIRD_PERSON_VIEW = 0;
    public const int WATCH_TYPE_FIRST_PERSON_VIEW = 1;
    public const int WATCH_TYPE_GOD_VIEW = 2;
    
    #endregion

    #region 位置修正
    public const float PLAYER_POSITION_FIXED_HIEGHT = 0.0001f;
    #endregion

    #region 语言版本
    public const string CHN = "zh";
    public const string RUS = "ru";
    public const string EN = "en";
    #endregion

}
