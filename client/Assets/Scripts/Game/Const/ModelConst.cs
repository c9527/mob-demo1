﻿public class ModelConst
{

#region Player Bip Name

    //public const string BIP_NAME_RIGHT_HAND = "Bip_RightHand";

    //public const string BIP_NAME_LEFT_HAND = "Bip_LeftHand";

    //public const string BIP_NAME_HEAD = "Bip_Head";

    public const string BIP_NAME_RIGHT_HAND_CENTER = "Bip_RightHandCenter";

    public const string BIP_NAME_LEFT_HAND_CENTER = "Bip_LeftHandCenter";

    public const string BIP_NAME_HEAD = "Bip_Head";

    public const string BIP_NAME_PEVIS = "Bip_Hips";

    public const string BIP_SPINE = "Bip_Spine";

    public const string BIP_CHEST = "Bip_Chest";

    public const string BIP_HEAD = "Bip_Head";

    public const string BIP_PREFIX = "Bip_";
    #endregion

#region Player IK Target Name

    public const string IK_TARGET_NAME_LEFT_HAND = "IKTargetPoint_LeftHand";

    public const string IK_TARGET_NAME_RIGHT_HAND = "IKTargetPoint_RightHand";

    public const string IK_TARGET_NAME_HEAD = "IKTargetPoint_Head";
#endregion

#region Player Slot

    public const string PLAYER_SLOT_RIGHT_HAND = "Slot_RightHand";

    public const string PLAYER_SLOT_LEFT_HAND = "Slot_LeftHand";

    public const string SLOT_HEAD = "Slot_Head";
#endregion

#region Camera Slot Point

    public const string SLOT_SCENE_CAMERA = "Slot_SceneCamera";

    public const string SLOT_MAINPLAYER_CAMERA = "Slot_MainPlayerCamera";
#endregion

#region Weapon Mount Type

    public const int WEAPON_MOUNT_TYPE_RIGHTHAND = 1;

    public const int WEAPON_MOUNT_TYPE_LEFTHAND = 2;

    public const int WEAPON_MOUNT_TYPE_BOTHHANDS = 3;
#endregion

#region Weapon

    public const string WEAPON_BIP_ROOT = "b_root";
    public const string WEAPON_SLOT_FIREPOINT = "Slot_Fire";

    public const string WEAPON_SLOT_CARTRIDGE = "Slot_Cartridge";

    public const string WEAPON_SLOT_RIFLEGRENADE = "Slot_Liudan";

    public const string WEAPON_EFFECT = "effect";
    public const string WEAPON_EFFECT1 = "effect1";     // 主角武器上，需要显示的特效组， 一般为左侧的特效
    public const string WEAPON_EFFECT2 = "effect2";     // 主角武器上，不需要显示的特效组,一般为右侧的特效，主角看不到
#endregion

#region Player Body Part

    // 字母都小写
    public const string PLAYER_BODY_PART_NAME_HEAD = "head";
    public const string PLAYER_BODY_PART_NAME_ARM = "arm";
    public const string PLAYER_BODY_PART_NAME_HAND = "hand";
    public const string PLAYER_BODY_PART_NAME_LEG = "leg";
    public const string PLAYER_BODY_PART_NAME_FOOT = "foot";
    public const string PLAYER_BODY_PART_NAME_CHEST = "chest";
    public const string PLAYER_BODY_PART_NAME_SHIELD = "shield";
#endregion

#region Model Name Releated
    public const string MODEL_PLAYER_BOY_CHAR = "nan";
    public const string MODEL_PLAYER_GIRL_CHAR = "nv";

    public const string MODEL_MAIN_PLAYER_GIRL_CHAR = "girl";

    public const string WEAPON_CRYSTAL = "crystal";
#endregion

#region Modle Sex
    //模型性别 1:男 2:女
    public const int MODEL_PLAYER_BOY_SEX = 1;
    public const int MODEL_PLAYER_GIRL_SEX = 2;
#endregion

#region 简单死亡动作模型
    public const string MODEL_YAZHUO_CHAR = "yazhou";
    public const string MODEL_OUMEI_CHAR = "oumei";
#endregion

#region 躲猫猫
    public const string MODEL_DMM_CHAR = "dmm";
#endregion

#region 
    public const string MODEL_MAINPLAYER_CC_GO = "CCGO";
#endregion

}