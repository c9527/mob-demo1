﻿public static class GameEvent
{
    #region 游戏逻辑事件
    public const string GAME_BATTLE_SCENE_LOADED = "game_battle_scene_loaded";
    public const string GAME_EXIT_BATTLE_SCENE = "game_exit_battle_scene";

    public const string GAME_HALL_SCENE_LOADED = "game_hall_scene_loaded";

    public const string GAME_SWITCH_CAMERA_TO_PARTNER = "game_switch_camera_to_partner";    //切换摄像机到队友身上
    public const string GAME_SWITCH_CAMERA_TO_PARTNER_NEXT = "game_switch_camera_to_partner_next";
    public const string GAME_SWITCH_CAMERA_CHANGE_FIRST_PERSON_VIEW = "game_switch_camera_change_first_person_view"; //改变第一人称第三人称视角

    public const string GAME_SHOW_GUNSCOPE = "game_show_gunscope";

    public const string GAME_OBJ_ENTER_BURST_POINT = "game_obj_enter_burst_point";    //游戏对象进入爆破点范围
    public const string GAME_OBJ_STAYIN_BURST_POINT = "game_obj_stayin_burst_point";  //游戏对象停留在爆破点范围
    public const string GAME_OBJ_LEAVE_BURST_POINT = "game_obj_leave_burst_point";    //游戏对象离开爆破点范围

    public const string GAME_BATTLE_ROUND_START = "game_battle_round_start";          ///  游戏回合开始
    public const string GAME_BATTLE_ROUND_OVER = "game_battle_round_over";            ///  游戏回合结束
    public const string GAME_BATTLE_OVER = "game_battle_over";                        ///  整个战斗结束。 
    public const string GAME_BATTLE_ROUND_UPDATE = "battle_round_update";                                                          ///  

    public const string WEAPON_C4_SWITCH = "weapon_c4_switch";                        ///  c4切换。 
    public const string GAME_BOMB_POINT = "game_bomb_point";
      
    public const string GAME_BATTLE_RESCUE_SOMEONE = "game_battle_rescue_someone";    ///  救人。 
                                                                                      ///  
    public const string LOGIN_VERIFY = "login_verify";    //登录验证账号
    public const string LOGIN_PLATFORM_COMPLETE = "login_platform_complete";    //平台登陆完成

    public const string LOGIN_ENABLE_BTN = "login_enable_btn";    //登录按钮启用

    #endregion

    #region 主角事件
    public const string MAINPLAYER_INIT_DONE = "mainplayer_init_done";  // 主角对象初始化完毕
    public const string MAINPLAYER_BEHIT = "mainplayer_behit";          //主角被击中（角度）

    public const string MAINPLAYER_ATK = "mainplayer_atk";              //主角攻击

    public const string MAINPLAYER_WRONG_ATK = "mainplayer_wrong_atk";  //主角攻击错误
    
    public const string MAINPLAYER_AMMOINFO_UIUPDATE = "mainplayer_ammoinfo_update";
    public const string MAINPLAYER_AMMOINFO_SUB_UIUPDATE = "mainplayer_ammoinfo_sub_uiupdate";

    public const string MAINPLAYER_RELOADBULLET_CD_MASK = "mainplayer_reloadbullet_cd_mask";

    public const string LOGIN_SUCCESS = "login_sucess";     // 主角登录成功事件
    public const string MAIN_PLAYER_JOIN = "main_player_join"; // 主角玩家进入游戏事件
    public const string MAINPLAYER_INFO_CHANGE = "mainplayer_info_change"; // 主角玩家本身属性变更，通知各个UI更新（如货币、等级）
    public const string MAINPLAYER_LEVEL_UP = "mainplayer_level_up"; // 主角升级
    
    // Equip Related
    public const string MAINPLAYER_EQUIP_INFO_UPDATE = "mainplayer_equipinfo_update";

    public const string MAINPLAYER_BE_CONTROLLED = "mainplayer_be_controlled";

    public const string MAINPLAYER_READY_JOIN_ROOM = "mainplayer_ready_join_room";

    public const string MAINPLAYER_GAME_START_DIE = "mainplayer_game_start_die";

    public const string MAINPLAYER_CLIMB = "mainplayer_climb";

    public const string MAINPLAYER_SETFREEZE = "mainplayer_setfreeze";

    public const string MAINPLAYER_SWITCH_SUB_WEAPON = "mainplayer_switch_sub_weapon";

    public const string MAINPLAYER_BOUNCE = "mainplayer_bounce";   //弹起事件

    //主角自动开火相关
    public const string MAINPLAYER_SET_IS_CAN_AUTOFIRE = "mainplayer_set_is_can_autofire"; //主角，设置能否自动开火
    public const string MAINPLAYER_SWITCH_AUTOFIRE = "mainplayer_switch_autofire"; //主角，切换自动开火
    public const string MAINPLAYER_STOP_AUTOFIRE = "mainplayer_stop_autofire"; //主角，停止自动开火

    public const string MAINPLAYER_ATTACH_SCENE_CAMERA = "mainplayer_attach_scene_camera";

    public const string MAINPLAYER_HEROCARD_UPDATE = "mainplayer_herocard_update";//英雄卡更新信息
    #endregion

    #region 所有角色事件
    public const string PLAYER_KILLED = "player_killed";    // 击杀事件(角色死亡)（攻击角色，被击杀角色，命中部位）
    public const string PLAYER_KILLED_EX = "player_killed_ex";    // 击杀事件(角色死亡)（攻击角色，被击杀角色，击杀数据）
    public const string PLAYER_SET_STATE = "player_set_state";    // 玩家状态改变
    public const string PLAYER_REVIVE = "player_revive";    // 角色復活（事件名，复活角色）
    public const string PLAYER_JOIN = "player_join";        // 玩家加入（加入角色）
    public const string PLAYER_LEAVE = "player_leave";      // 玩家离开战场
    public const string PLAYER_BE_DAMAGE = "player_be_damage";  // 主角受到伤害（血量）
    public const string PLAYER_ACTORPROP_UPDATE = "player_actorprop_update";  // 属性变化
    public const string PLAYER_HIDE_SWITCH = "player_hide_switch";
    public const string PlAYER_HIDE_SWITCH_BACK = "player_hide_switch_back";
    public const string PLAYER_ATK = "player_atk";  // 玩家攻击
    public const string PLAYER_KILL_CURPLAYER = "player_kill_curplayer";
    public const string PLAYER_SCORE_UPDATE = "player_score_update"; //玩家积分发生变化
    public const string PLAYER_ENERGY_UPDATE = "player_energy_update"; //玩家能量值发生变化
    public const string PLAYER_ACTOR_TYPE_UPDATE = "player_actor_type_update"; //玩家类型发生变化
    public const string PLAYER_ATTACH = "player_attach";
    public const string PLAYER_DETACH = "player_detach";
    public const string PLAYER_USE_REVIVECOIN = "player_use_revivecoin";
    public const string PLAYER_LEVEL_UP = "player_level_up";//玩家升级
    public const string PLAYER_FLY_POWER_OFF = "player_fly_power_off";
    public const string PLAYER_FLY_UP = "player_fly_up";//飞行开始
    public const string PLAYER_FLY_END = "player_fly_end";//飞行结束
    public const string PLAYER_CHECK_FLY = "player_check_fly";
    #endregion

    #region 服务端消息的代理事件（把服务端的消息转发给其他本地模块处理）
    public const string PROXY_PLAYER_FIGHT_ACHIEVE = "proxy_player_fight_achieve";   // 战斗成就达成消息
    public const string PROXY_PLAYER_FIGHT_STAT_INFO = "proxy_player_fight_stat_info";// 战斗统计信息消息
    public const string PROXY_PLAYER_FIGHT_STATE = "proxy_player_fight_state";        // 战斗状态消息                                                              
    public const string PROXY_PLAYER_ATTRIBUTE_UPDATE = "proxy_player_attribute_update"; // 主角属性数据更新事件
    public const string PROXY_PLAYER_STAT_OF_FIGHT = "proxy_player_fight_stat";
    public const string PROXY_PLAYER_ACTOR_STATE = "proxy_player_actor_state";  // 主角角色状态数据变化
    public const string PROXY_PLAYER_DEFAULT_ROLE_UPDATED = "proxy_player_default_role_updated";//装备的角色更新
    public const string PROXY_PLAYER_ARENA_ROUND_INFO_UPDATED = "proxy_player_arena_round_info_updated";//擂台赛回合数据更新
    public const string PROXY_PLAYER_BIG_HEAD_FINAL_TIME_UPDATED = "proxy_player_big_head_final_time_updated";//大头模式终于时刻时间更新
 
    //========================任务相关
    public const string PROXY_DAILY_MISSION_DATA_UPDATED = "proxy_daily_mission_data_updated";

    //===========================活动相关
    public const string PROXY_EVENT_SHARE_DATA_UPDATED = "proxy_event_share_data_updated";

    #endregion

    #region UI事件
    public const string UI_SHOW_FIRE_AIM_POINT = "ui_show_fire_aim_point";          //显示准星（是否显示）
    public const string UI_SHAKE_FIRE_AIM_POINT = "ui_shake_fire_aim_point";        //震动准星（未实现）
    public const string UI_SHOW_AIM_PLAYER_NAME = "ui_show_aim_player_name";        //瞄准目标显示名字（瞄准的角色，是否敌方）
    public const string UI_CANCEL_AIM_PLAYER_NAME = "ui_cancel_aim_player_name";    //取消显示目标名字
    public const string UI_SHOW_AUTO_AIM = "ui_show_auto_aim";                      //自动瞄准
    public const string UI_TIP_INFO = "ui_tip_info";                                //显示消息（内容，文本颜色，字体大小）
    public const string UI_GUN_SELECTED = "ui_gun_selected";                        //选枪（事件名，index）
    public const string UI_ROOM_MODE_SELECTED = "ui_room_mode_selected";            // 创建房间后，选取的模式事件
    public const string UI_SETTLE_END = "ui_settle_end";                            //> 关闭结算界面
    public const string UI_SETTING_OPEN = "ui_setting_open";                        //> 打开设置界面
    public const string UI_SEETING_CLOSE = "ui_setting_close";                      //> 关闭设置界面
    public const string UI_UPDATE_GUNLIST = "ui_update_gunlist";
    public const string UI_UPDATE_GUNLIST2 = "ui_update_gunlist2";
    public const string UI_SHOW_GUNLIST = "ui_show_gunlist";                        //> 显示枪列表
    public const string UI_SETTING_UPDATE = "ui_setting_update";                    //> 设置更新
    public const string UI_USER_DEFINE_CONTROL_POS = "ui_user_define_control_pos";
    public const string UI_GET_CHAT_MSG = "ui_get_chat_msg";                        //> 大厅收到聊天消息
    public const string UI_UPDATE_CHAT_MSG = "ui_update_chat_msg";                  //> 更新聊天界面的消息
    public const string UI_NEW_CHAT_MSG = "ui_new_chat_msg";                  //> 有新的聊天信息
    public const string UI_HIDE_MOVE_ICON = "ui_hide_move_icon";                        //隐藏移动图标
    public const string UI_HIDE_SWITCH_ICON = "ui_hide_switch_icon";                        //隐藏换枪图标
    public const string UI_SELECT_MAP = "ui_select_map";                        //选择地图

    public const string UI_EQUIP_UPDATE = "ui_equip_update";
    public const string UI_SHOW_GUNSCOPE_BUTTON = "ui_show_gunscope_button";
    //public const string UI_GUNSCOPE_BUTTON_RESET = "ui_gunscrope_button_reset";
    public const string UI_SET_GUNSCOPE_BUTTON_STATUS = "ui_set_gunscope_button_status";
    public const string UI_CANCLE_FIRE = "ui_cancle_fire";
    public const string UI_SHOW_CANCLE_FIRE_BTN = "ui_show_cancle_fire_btn";

    public const string UI_NEW_MSG_PLAYER_NUM = "ui_new_msg_player_num";
    public const string UI_NEW_MSG_PLAYER_NUM_PRIVATE = "ui_new_msg_player_num_private";
    public const string UI_GUESTS_UPDATE = "ui_guests_update";

    public const string UI_BUBBLE_CHANGE = "ui_bubble_change";                      //气泡数据变更
    public const string UI_EVENT_DATA_CHANGE = "ui_event_data_change";                        //> 活动数据变更
    public const string UI_MEMBER_CHANGE = "ui_member_change";                      //> 会员身份变更
    public const string UI_LOGIN_SERVER_CHANGE = "ui_login_server_change";  //服务器改变
    public const string UI_FIRST_PAY = "ui_first_pay";                              //> 发生首充

    //> 获取新消息的玩家个数
    public const string UI_ROBOT_SELECT = "ui_robot_select";                        //> 选择机器人

    public const string UI_ROLE_ITEM_SELECTED = "ui_role_item_selected";
    public const string UI_ROLE_ICON_CHANGED = "ui_role_icon_changed";              //> 主角头像修改

    public const string UI_SHOW_ZOMBIEHERO_INFO = "ui_show_zombiehero_info";

    public const string UI_CREATE_PANEL_BATTLE = "ui_create_panel_battle";   //创建战斗面板

    public const string UI_CHAT_NEW_UNREAD_MSG = "ui_chat_new_unread_msg";   //增加一条新的未读消息
    public const string UI_CHAT_ALL_MSG_READED = "ui_chat_all_msg_readed";   //所有消息已读

    public const string UI_SHOW_SWITCH_VIEW_BTN = "ui_show_switch_view_btn";      //切换视角按钮
    public const string UI_ENABLE_SWITCH_VIEW_BTN = "ui_enable_switch_view_btn";  //打开视角按钮

    public const string UI_UPDATE_WEAPON_SKILL = "ui_update_weapon_skill";    //更新武器技能

    public const string UI_SHOW_WEAPON_SKILL = "ui_show_weapon_skill";        //是否显示武器技能

    public const string UI_REFRESH_WEAPON_SKILL = "ui_refresh_weapon_skill";    //刷新武器技能

    public const string UI_REFRESH_SKILL = "ui_refresh_skill";    //刷新技能
    public const string UI_REFRESH_SKILL_WITHPARAM = "ui_refresh_skill_withParam";    //刷新技能,带参数
    public const string UI_DOUBLE_CLICK_ROOMLIST = "ui_double_click_roomlist";//房间列表双击事件
    public const string UI_DOUBLE_CLICK_CORPSLIST = "ui_double_click_corpslist";//战队列表双击事件
    public const string UI_DOUBLE_CLICK_DEPOTWEAPON = "ui_double_click_depotweapon";//仓库武器双击事件
    public const string UI_CHANGE_CORPS = "ui_change_corps";//战队改名

    public const string UI_GANRANSHIBAI = "ui_ganranshibai";

    public const string UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION = "UI_BATTLEPARAM_SETTING_FIGHTDIRECTION_POSITION";//设置作战指示按钮位置
    public const string UI_BATTLEPARAM_SETTING_CROSS_TYPE = "UI_BATTLEPARAM_SETTING_CROSS_TYPE";//设置准星
    public const string UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE = "UI_BATTLEPARAM_SETTING_JOYSTICK_TYPE";//设置摇杆
    public const string UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE = "UI_BATTLEPARAM_SETTING_JOYSTICK_MOVE_TYPE";//设置摇杆移动类型
    public const string UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE = "UI_BATTLEPARAM_SETTING_HEAVY_KNIFE_TYPE";//设置重刀
    public const string UI_BATTLEPARAM_SETTING_KNIFE_AUTOAIM = "UI_BATTLEPARAM_SETTING_KNIFE_AUTOAIM";//刀自瞄
    public const string UI_BATTLEPARAM_SETTING_AUTOAIM = "UI_BATTLEPARAM_SETTING_AUTOAIM";//刀自瞄
    public const string UI_BATTLEPARAM_SETTING_ZOMBIE_AUTOAIM = "UI_BATTLEPARAM_SETTING_ZOMBIE_AUTOAIM";//僵尸自瞄
    public const string UI_FOCUS_CHANGE = "ui_focus_change";    //焦点变化
    public const string UI_BATTLEPARAM_SETTING_HAND_TYPE = "UI_BATTLEPARAM_SETTING_HAND_TYPE";//设置左右手
    public const string UI_SWITCH_FREE_VIEW = "ui_hide_chage_view";
    public const string UI_SWITCH_PERSON_VIEW = "ui_switch_person_view";
    public const string UI_RESET_WATCH_TYPE = "ui_reset_watch_type";
    public const string UI_CLICK_STABLE = "ui_click_stable";
    public const string UI_SETCROSSHAIRVISIBLE = "ui_setcrosshairvisible";
    public const string UI_SELECT_EXTERN_PIC = "ui_selectexternpic";
    public const string UI_CROP_PIC = "ui_croppic";
    public const string UI_UPLOAD_PIC = "ui_uploadpic";
    public const string UI_SELF_ICON_URL_RETRIVED = "ui_selficonurlretrived";
    public const string UI_MICROBLOG_PLAYER_ZONE = "ui_microblogplayerzone";
    public const string UI_MICROBLOG_FOLLOW = "ui_microblogfollow";
    public const string UI_MICROBLOG_COMMENT_REPLY = "ui_microblogcommentreply";
    #endregion

    #region 输入控制

    public const string INPUT_MOVE = "input_move";
    public const string INPUT_MOVE_SLOW = "input_move_slow";
    public const string INPUT_JUMP = "input_jump";
    public const string INPUT_JUMP_END = "input_jump_end";
    public const string INPUT_STAND = "input_stand";
    public const string INPUT_CROUCH = "input_crouch";
    public const string INPUT_FIRE = "input_fire_start";
    public const string INPUT_MOUSERIGHT_ACTION = "input_mouseright_action";
    public const string INPUT_MOUSELEFT_ACTION = "input_mouseleft_action";
    public const string INPUT_FIRE_SUB = "input_fire_sub_start";
    public const string INPUT_ZOOM = "input_zoom_in";
    public const string INPUT_RELOAD_AMMO = "input_reload_ammo";
    public const string INPUT_SWITCH_WEAPON = "input_switch_weapon";
    public const string INPUT_ENABLE_GUN_SCOPE = "input_enable_gun_scope";
    public const string INPUT_FIRE_GRENADE = "input_fire_greande_start";
    public const string INPUT_FIRE_FLASHBOMB = "input_fire_flashbomb_start";
    public const string INPUT_FLY = "input_fly";

    /// <summary>
    /// INPUT_KEY 这些变量名的值要和变量名一致
    /// </summary>
    public const string INPUT_KEY_SHIFT = "INPUT_KEY_SHIFT";//蹲下站起控制
    public const string INPUT_KEY_MOVE = "INPUT_KEY_MOVE";//输入移动按键 awsd 箭头
    public const string INPUT_KEY_F_DOWN = "INPUT_KEY_F_DOWN";//装备穿甲弹<setup_AP>、发动技能
    public const string INPUT_KEY_E_DOWN = "INPUT_KEY_E_DOWN";//使用（放置/拆除C4，补充弹药，获取BUFF等）
    //public const string INPUT_KEY_R_DOWN = "INPUT_KEY_R_DOWN";//换弹夹<change_magazine>
    public const string INPUT_KEY_Q_DOWN = "INPUT_KEY_Q_DOWN";//换枪<change_gun>
    public const string INPUT_KEY_G_DOWN = "INPUT_KEY_G_DOWN";//丢枪<drop_weapon>
    public const string INPUT_KEY_B_DOWN = "INPUT_KEY_B_DOWN";//打开背包

    public const string INPUT_KEY_LEFTPAREN_DOWN = "INPUT_KEY_LEFTPAREN_DOWN";//左括号
    public const string INPUT_KEY_RIGHTPAREN_DOWN = "INPUT_KEY_RIGHTPAREN_DOWN";//右括号


    public const string INPUT_KEY_T_DOWN = "INPUT_KEY_T_DOWN";//快捷键使用穿甲弹

    public const string INPUT_KEY_Y_DOWN = "INPUT_KEY_Y_DOWN";//快捷键同意踢人投票
    public const string INPUT_KEY_N_DOWN = "INPUT_KEY_N_DOWN";//快捷键反对踢人投票

    public const string INPUT_KEY_R_DOWN = "INPUT_KEY_R_DOWN";
    public const string INPUT_KEY_S_DOWN = "INPUT_KEY_S_DOWN";
    public const string INPUT_KEY_A_DOWN = "INPUT_KEY_A_DOWN";
    public const string INPUT_KEY_D_DOWN = "INPUT_KEY_D_DOWN";
    public const string INPUT_KEY_W_DOWN = "INPUT_KEY_W_DOWN";

    public const string INPUT_KEY_H_DOWN = "INPUT_KEY_H_DOWN";//使用复活币
    public const string INPUT_KEY_V_DOWN = "INPUT_KEY_V_DOWN";//战斗中语音键


    public const string INPUT_KEY_ALT_DOWN = "INPUT_KEY_ALT_DOWN";//慢走


    public const string INPUT_KEY_SPACE_DOWN = "INPUT_KEY_SPACE_DOWN";
    public const string INPUT_KEY_SPACE_UP = "INPUT_KEY_SPACE_UP";
    public const string INPUT_KEY_BACKQUOTE_DOWN = "INPUT_KEY_BACKQUOTE_DOWN";//作战指示
    public const string INPUT_KEY_BATTLESPECTATOR_DOWN = "INPUT_KEY_BATTLESPECTATOR_DOWN";//F11弹出观战信息界面

    public const string INPUT_KEY_UPARROW_UP = "INPUT_KEY_UP";//前进、后退，若聊天输入栏激活则切换聊天频道
    public const string INPUT_KEY_DOWNARROW_UP = "INPUT_KEY_DOWN";
    public const string INPUT_KEY_LEFTARROW_UP = "INPUT_KEY_LEFT";//左移、右移，若聊天输入栏激活则移动输入光标位置
    public const string INPUT_KEY_RIGHTARROW_UP = "INPUT_KEY_RIGHT";


    public const string INPUT_KEY_1_DOWN = "INPUT_KEY_1_DOWN";//切换到主武器
    public const string INPUT_KEY_2_DOWN = "INPUT_KEY_2_DOWN";//切换到手枪
    public const string INPUT_KEY_3_DOWN = "INPUT_KEY_3_DOWN";//切换到近战武器
    public const string INPUT_KEY_4_DOWN = "INPUT_KEY_4_DOWN";//切换到手雷/若当前手持手雷或没有手雷则切换到烟雾弹 
    public const string INPUT_KEY_5_DOWN = "INPUT_KEY_5_DOWN";//切换到C4
    public const string INPUT_KEY_6_DOWN = "INPUT_KEY_6_DOWN";//





    public const string INPUT_KEY_NUM_DOWN = "INPUT_KEY_NUM_DOWN";//键盘数字按键事件
    public const string INPUT_KEY_PAD_NUM_DOWN = "INPUT_KEY_PAD_NUM_DOWN";//小键盘数字按键事件
    public const string INPUT_KEY_PLUS_DOWN = "INPUT_KEY_PLUS_DOWN";//+号按键

    public const string INPUT_KEY_ENTER_UP = "INPUT_KEY_ENTER_DOWN";//打开聊天输入栏，若输入栏已打开则发送消息并关闭输入栏
    public const string INPUT_KEY_TAB_DOWN = "INPUT_KEY_TAB_DOWN";//查看战绩
    public const string INPUT_KEY_ESC_DOWN = "INPUT_KEY_ESC_DOWN";//弹出设置界面

    public const string INPUT_KEY_AUDIO_DOWN = "INPUT_KEY_AUDIO_DOWN";//按住语音聊天键
    public const string INPUT_KEY_AUDIO_UP = "INPUT_KEY_AUDIO_UP";//松开语音聊天键
    public const string INPUT_KEY_P_DOWN = "INPUT_KEY_P_DOWN";//自动开火/战斗开关
    public const string INPUT_KEY_F8_DOWN = "INPUT_KEY_F8_DOWN";
   // public const string INPUT_KEY_MOUSELEFT_DOWN = "INPUT_KEY_MOUSELEFT_DOWN";//瞄准及射击<select&aim>、投掷手雷<grenades>、投掷烟雾弹<somke_bomb>
   // public const string INPUT_KEY_MOUSELEFT_UP = "INPUT_KEY_MOUSELEFT_UP";
   // public const string INPUT_KEY_MOUSERIGHT = "INPUT_KEY_MOUSERIGHT";//重刀<>、进入/取消瞄准状态瞄准<enter_cancel_zoommodes>










#endregion

    #region 工具用消息
    public const string TOOL_UPDATE_WEAPON_CONFIG_REFERENCE = "tool_update_weapon_config_reference";
    #endregion

    #region 掉落物事件

    public const string DROPITEM_GAEMOBJECT_ENTER_PICK_UP_DISTANCE = "dropitem_gameobject_enter_pick_up_distance";
    public const string DROPITEM_GAEMOBJECT_LEAVE_PICK_UP_DISTANCE = "dropitem_gameobject_leave_pick_up_distance";

    public const string DROPSYS_ADD_DROP = "dropsys_add_drop";
    public const string DROPSYS_DEL_DROP = "dropsys_del_drop";
    public const string DROPSYS_PRE_PICKUP = "dropsys_pre_pickup";
    #endregion

    #region SDK

    public const string SDK_CHECK_UPDATE_COMPLETE = "sdk_check_update_complete";
    public const string SDK_SPEECH_COMPLETE = "sdk_speech_complete";
    public const string SDK_VALIDATE_UPLOAD_PIC_COMPLETE = "sdk_validate_upload_pic_complete";

    #endregion

    #region 军团战

    public const string LEGION_WAR_INFO_UPDATE = "legion_war_info_update";
    public const string LEGION_MYLEGION_INFO_UPDATE = "legion_mylegion_info_update";//我的军团信息
    public const string LEGION_OFFICERS_INFO_UPDATE = "legion_officers_info_update";//军官列表
    public const string LEGION_FAMOUS_INFO_UPDATE = "legion_famous_info_update";//名人堂
    public const string LEGION_BUBBLE_TIP = "legion_bubble_tip";//更新军团战排行榜可以领取奖励红点

    #endregion

    #region 剧情系统
    public const string STORY_SLEEP_END = "story_sleep_end";
    #endregion

    #region 战斗数据模型中阵营数目的更新
    public const string BATTLE_MODEL_CAMP_UPDATE = "battle_model_camp_update";
    #endregion

    #region 武器库
    //放入武器
    public const string MASTER_REWARD_PUT_WEAPON = "master_reward_put_weapon";
    #endregion

    #region 观战信息
    public const string SPECTATOR_JOIN = "spectator_join";
    public const string SPECTATOR_LEAVE = "spectator_leave";
    #endregion
}
