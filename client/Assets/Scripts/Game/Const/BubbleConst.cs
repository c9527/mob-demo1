﻿class BubbleConst
{
    public const string WelfareLevelReward = "WelfareLevelReward";
    public const string WelfareOnlineReward = "WelfareOnlineReward";
    public const string WelfareSignReward = "WelfareSignReward";
    public const string FriendApply = "FriendApply";
    public const string CorpsApply = "CorpsApply";
    public const string LegionApply = "LegionApply";
    public const string StudentApply = "StudentApply";
    public const string MasterGift = "MasterGift";//收到导师的礼包
    public const string MasterGrade = "MasterGrade";
    public const string ApprenticeGift = "ApprenticeGift";//收到徒弟的礼包
    public const string StrengthenUpgrade = "StrengthenUpgrade";
    public const string StrengthenTrain = "StrengthenTrain";
    public const string StrengthenAwaken = "StrengthenAwaken";
    public const string CorpsTaskReward = "CorpsTaskReward";
    public const string CorpsGuardRunning = "CorpsGuardRunning";
    public const string MallSPNotice = "MallSPNotice";
    public const string MailUnread = "UnreadMail";
    public const string LotteryFree = "LotteryFree";
    public const string SevenDayReward = "SevenDayReward"; //新人七天签到
    public const string NewHandTask = "NewHandTask";
    public const string ChatOfflineMsg = "ChatOfflineMsg";//离线消息
    public const string HeroCardReward = "HeroCardReward";//英雄卡有礼包可领取
    public const string Investment1 = "Investment1";
    public const string Investment2 = "Investment2";
    public const string Investment3 = "Investment3";
    public const string WelfareLeijiReward = "WelfareLeijiReward";
    public const string HallChatCorps = "HallChatCorps";
    public const string HallChatLegion = "HallChatLegion";
    public const string HallChatFriend = "HallChatFriend";
    public const string HallChatPrivate = "HallChatPrivate";
}