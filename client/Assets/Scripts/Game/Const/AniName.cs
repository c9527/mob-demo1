﻿public class AniName
{
    public const string JUMP = "jump";

    public const string WALK = "walk";

    public const string DEAD = "dead";

    public const string IDLE = "idle";

    public const string FIRE = "fire";

}
