﻿using UnityEngine;
using System.Collections;


public class GameSetting
{
    static public ConfigGameSettingLine configGameSettingLine;

    static public void Init()
    {
        ConfigGameSetting cgs = ConfigManager.GetConfig<ConfigGameSetting>();
        if (cgs != null)
            configGameSettingLine = cgs.m_dataArr[0];

        LAYER_VALUE_MAIN_PLAYER = LayerMask.NameToLayer(LAYER_NAME_MAIN_PLAYER);
        LAYER_VALUE_OTHER_PLAYER_FRIEND = LayerMask.NameToLayer(LAYER_NAME_OTHER_PLAYER_FRIEND);
        LAYER_VALUE_OTHER_PLAYER_ENEMY = LayerMask.NameToLayer(LAYER_NAME_OTHER_PLAYER_ENEMY);

        LAYER_VALUE_AUTOTARGET = LayerMask.NameToLayer(LAYER_NAME_AUTOTARGET);
        LAYER_VALUE_BUILDING = LayerMask.NameToLayer(LAYER_NAME_BUILDING);
        LAYER_VALUE_BULLET = LayerMask.NameToLayer(LAYER_NAME_BULLET);
        LAYER_VALUE_UI = LayerMask.NameToLayer(LAYER_NAME_UI);

        LAYER_MASK_MAIN_PLAYER = LayerMask.GetMask(LAYER_NAME_MAIN_PLAYER);
        LAYER_MASK_OTHER_PLAYER_FRIEND = LayerMask.GetMask(LAYER_NAME_OTHER_PLAYER_FRIEND);
        LAYER_MASK_OTHER_PLAYER_ENEMY = LayerMask.GetMask(LAYER_NAME_OTHER_PLAYER_ENEMY);

        LAYER_MASK_AUTOTARGET = LayerMask.GetMask(LAYER_NAME_AUTOTARGET);
        LAYER_MASK_BUILDING = LayerMask.GetMask(LAYER_NAME_BUILDING);
        LAYER_MASK_UI = LayerMask.GetMask(LAYER_NAME_UI);

        // 新添加的层，如果老版本没有整包更新，是取不到值的
        LAYER_VALUE_OTHER_PLAYER = LayerMask.NameToLayer(LAYER_NAME_OTHER_PLAYER);
        LAYER_VALUE_OTHER_PLAYER = LAYER_VALUE_OTHER_PLAYER < 0 ? 16 : LAYER_VALUE_OTHER_PLAYER;
        
        LAYER_MASK_OTHER_PLAYER = LayerMask.GetMask(LAYER_NAME_OTHER_PLAYER);
        LAYER_MASK_OTHER_PLAYER = LAYER_MASK_OTHER_PLAYER < 0 ? 65536 : LAYER_MASK_OTHER_PLAYER;

        

        LAYER_VALUE_AIR_WALL = LayerMask.NameToLayer(LAYER_NAME_AIR_WALL);
        LAYER_VALUE_AIR_WALL = LAYER_VALUE_AIR_WALL < 0 ? 17 : LAYER_VALUE_AIR_WALL;
        LAYER_MASK_AIR_WALL = LayerMask.GetMask(LAYER_NAME_AIR_WALL);
        LAYER_MASK_AIR_WALL = LAYER_MASK_AIR_WALL < 0 ? 131072 : LAYER_MASK_AIR_WALL;

        LAYER_VALUE_SKYBOX = LayerMask.NameToLayer(LAYER_NAME_SKYBOX);
        LAYER_VALUE_SKYBOX = LAYER_VALUE_SKYBOX < 0 ? 8 : LAYER_VALUE_SKYBOX;

        LAYER_VALUE_BATTLE_EFFECT = LayerMask.NameToLayer(LAYER_NAME_BATTLE_EFFECT);
        LAYER_VALUE_BATTLE_EFFECT = LAYER_VALUE_BATTLE_EFFECT < 0 ? 18 : LAYER_VALUE_BATTLE_EFFECT;

        LAYER_VALUE_SCENE_EFFECT = LayerMask.NameToLayer(LAYER_NAME_SCENE_EFFECT);
        LAYER_VALUE_SCENE_EFFECT = LAYER_VALUE_SCENE_EFFECT < 0 ? 19 : LAYER_VALUE_SCENE_EFFECT;

        //躲猫猫玩家层
        LAYER_VALUE_DMM_PLAYER = LayerMask.NameToLayer(LAYER_NAME_DMM_PLAYER);
        LAYER_VALUE_DMM_PLAYER = LAYER_VALUE_DMM_PLAYER < 0 ? 20 : LAYER_VALUE_DMM_PLAYER;
        LAYER_MASK_DMM_PLAYER = LayerMask.GetMask(LAYER_NAME_DMM_PLAYER);
        LAYER_MASK_DMM_PLAYER = LAYER_MASK_DMM_PLAYER < 0 ? 1048576 : LAYER_MASK_DMM_PLAYER;

        LAYER_VALUE_DEATH_BOX = LayerMask.NameToLayer(LAYER_NAME_DEATH_BOX);
        LAYER_VALUE_DEATH_BOX = LAYER_VALUE_DEATH_BOX < 0 ? 21 : LAYER_VALUE_DEATH_BOX;

        LAYER_MASK_DEATH_BOX = LayerMask.GetMask(LAYER_NAME_DEATH_BOX);
        LAYER_MASK_DEATH_BOX = LAYER_MASK_DEATH_BOX < 0 ? 2097152 : LAYER_MASK_DEATH_BOX;

        LAYER_VALUE_FREEVIEW_PLAYER = LayerMask.NameToLayer(LAYER_NAME_FREEVIEW_PLAYER);
        LAYER_VALUE_FREEVIEW_PLAYER = LAYER_VALUE_FREEVIEW_PLAYER < 0 ? 22 : LAYER_VALUE_FREEVIEW_PLAYER;
        LAYER_MASK_FREEVIEW_PLAYER = LayerMask.GetMask(LAYER_NAME_FREEVIEW_PLAYER);
        LAYER_MASK_FREEVIEW_PLAYER = LAYER_MASK_FREEVIEW_PLAYER < 0 ? 4194304 : LAYER_MASK_FREEVIEW_PLAYER;

        //剧情玩家层
        LAYER_VALUE_STORY_PLAYER = LayerMask.NameToLayer(LAYER_NAME_STORY_PLAYER);
        LAYER_VALUE_STORY_PLAYER = LAYER_VALUE_STORY_PLAYER < 0 ? 23 : LAYER_VALUE_STORY_PLAYER;
        LAYER_MASK_STORY_PLAYER = LayerMask.GetMask(LAYER_NAME_STORY_PLAYER);
        LAYER_MASK_STORY_PLAYER = LAYER_MASK_STORY_PLAYER < 0 ? 8388608 : LAYER_MASK_STORY_PLAYER;

        LAYER_VALUE_MAINPLAYER_TRIGGER = LayerMask.NameToLayer(LAYER_NAME_MAINPLAYER_TRIGGER);
        LAYER_VALUE_MAINPLAYER_TRIGGER = LAYER_VALUE_MAINPLAYER_TRIGGER < 0 ? 24 : LAYER_VALUE_MAINPLAYER_TRIGGER;

        ConfigGameSetting kCGS = ConfigManager.GetConfig<ConfigGameSetting>();
        if (kCGS.m_dataArr == null || kCGS.m_dataArr.Length < 0)
        {
            Logger.Error("ConfigGameSetting 配置文件读取失败");
            return;
        }

        ConfigGameSettingLine kSL = kCGS.m_dataArr[0];

        MAIN_PLAYER_SCENECAMERA_LOCALPOS = kSL.SceneCameraPos;
        MAIN_PLAYER_SCENECAMERA_LOCALROT = kSL.SceneCameraRot;
        MAIN_PLAYER_CAMERA_LOCALPOS = kSL.MainPlayerCameraPos;
        MAIN_PLAYER_CAMERA_LOCALROT = kSL.MainPlaerCameraRot;

        EffectPosZOffset = kSL.EffectPosZOffset;
        MPColliderRadius = kSL.MPColliderRadius;
        MPStepOffset = kSL.MPStepOffset;
        OPColliderRadius = kSL.OPColliderRadius;
        PlayerColliderSlope = kSL.PlayerColliderSlope;
        //GRAVITY = kSL.GRAVITY;
        //GRAVITY_JUMPUP = kSL.GRAVITY_JUMPUP;

        var configRoleConst = ConfigManager.GetConfig<ConfigRoleConst>().m_dataArr[0];
        PLAYER_AIM_BACK_CENTER_DELAY = configRoleConst.autoAimBackCenterDelay;
        PLAYER_AIM_BACK_CENTER_SPEED = configRoleConst.autoAimBackCenterSpeed;
        PLAYER_AIM_ROTATE_SPEED = configRoleConst.autoAimPlayerRotateSpeed;
        PLAYER_CROSSHAIR_AIM_SPEED = configRoleConst.autoAimCrosshairSpeed;

        PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET = configGameSettingLine.MPCCCenterOffset;
        PLAYER_GROUNDCHECK_HEIGHT_OFFSET = configGameSettingLine.OPGroundCheckHeightOffset;

        PLAYER_JUMP_MOVE_SLOW_TIME = configGameSettingLine.JumpMoveSlowTime;

        //PLAYER_JUMP_MAX_HEIGHT = configGameSettingLine.JumpMaxHeight;

        FOLLOW_PLAYER_DEFAULT_VERTICAL_ROTATE = configGameSettingLine.FollowPlayerDefaultVerticalRotate;

        DamageTipsTotalTime = configGameSettingLine.DamageTipsTotalTime;
        DamageTipsTweenTime = configGameSettingLine.DamageTipsTweenTime;
        DamageTipsFontSize = configGameSettingLine.DamageTipsFontSize;
        DamageTipsPosRamdomX = configGameSettingLine.DamageTipsPosRamdomX;
        DamageTipsPosRamdomY = configGameSettingLine.DamageTipsPosRamdomY;
        DamageTipsScaleChange = configGameSettingLine.DamageTipsScaleChange;

        IodLevelDistance = configGameSettingLine.IodLevelDistance;

        FightAchieveEffectDelay = configGameSettingLine.FightAchieveEffectDelay;
    }

#region 系统相关

    static public bool DEBUG = false;

#if UNITY_WEBPLAYER || UNITY_STANDALONE
    static public int FRAME_RATE = 60;
#else
    static public int FRAME_RATE = 40;
#endif

    static public string CONTROL_TYPE = "";     // "input" "touch"
    
#endregion
   

#region 声音相关

    static public float SOUND_VOLUME_EXPLORE = 2.0F;//爆炸声音
    static public float SOUND_VOLUME_BATTLE = 1.0F;
    static public float SOUND_VOLUME_UI = 1.0f;

    static public float SWITCH_WEAPON_SOUND_PLAY_MIN_INTERVAL = 2f;
#endregion
    

#region 射线与层

    // Ray
    public const float FIRE_RAYTEST_LENGTH = 1000f;
    public const float FIRE_LINE_EFFECT_DISTANCE = 100f;
    public const float GUN_RAYHITPOS_DISTANCE = 20f;

    // Layer Name
    static public string LAYER_NAME_DEFAULT = "Default";
    static public string LAYER_NAME_MAIN_PLAYER = "MainPlayer";
    static public string LAYER_NAME_OTHER_PLAYER_FRIEND = "OtherPlayer_Friend";     // 玩家不同部位
    static public string LAYER_NAME_OTHER_PLAYER_ENEMY = "OtherPlayer_Enemy";       // 玩家不同部位
    static public string LAYER_NAME_OTHER_PLAYER = "OtherPlayer";                   // 玩家胶囊体，别人可以站在上面
    static public string LAYER_NAME_DMM_PLAYER = "DmmPlayer";                     // 躲猫猫第三人称
    static public string LAYER_NAME_STORY_PLAYER = "StoryPlayer"; 
    static public string LAYER_NAME_AUTOTARGET = "AutoTarget";
    static public string LAYER_NAME_BUILDING = "Building";
    static public string LAYER_NAME_AIR_WALL = "AirWall";
    static public string LAYER_NAME_BULLET = "Bullet";
    static public string LAYER_NAME_UI = "UI";
    //static public string LAYER_NAME_SKYBOX = "SkyBox";
    static public string LAYER_NAME_BATTLE_EFFECT = "BattleEffect";
    static public string LAYER_NAME_SCENE_EFFECT = "SceneEffect";
    static public string LAYER_NAME_DEATH_BOX = "DeathBox";
    static public string LAYER_NAME_FREEVIEW_PLAYER = "FreeviewPlayer";
    static public string LAYER_NAME_SKYBOX = "SkyBox";
    static public string LAYER_NAME_MAINPLAYER_TRIGGER = "MainPlayerTrigger";
   
    // Layer Value
    static public int LAYER_VALUE_DEFAULT = 0;
    static public int LAYER_VALUE_MAIN_PLAYER = 0;
    static public int LAYER_VALUE_OTHER_PLAYER_FRIEND = 0;
    static public int LAYER_VALUE_OTHER_PLAYER_ENEMY = 0;
    static public int LAYER_VALUE_OTHER_PLAYER = 0;
    static public int LAYER_VALUE_DMM_PLAYER = 0;
    static public int LAYER_VALUE_STORY_PLAYER = 0;
    static public int LAYER_VALUE_AUTOTARGET = 0;
    static public int LAYER_VALUE_BUILDING = 0;
    static public int LAYER_VALUE_AIR_WALL = 0;
    static public int LAYER_VALUE_BULLET = 0;
    static public int LAYER_VALUE_UI = 0;
    static public int LAYER_VALUE_SKYBOX = 0;
    static public int LAYER_VALUE_BATTLE_EFFECT = 0;
    static public int LAYER_VALUE_SCENE_EFFECT = 0;
    static public int LAYER_VALUE_DEATH_BOX = 0;
    static public int LAYER_VALUE_FREEVIEW_PLAYER = 0;
    static public int LAYER_VALUE_MAINPLAYER_TRIGGER = 0;

    // Layer Mask
    static public int LAYER_MASK_DEFAULT = 0;
    static public int LAYER_MASK_MAIN_PLAYER = 0;
    static public int LAYER_MASK_OTHER_PLAYER_FRIEND = 0;
    static public int LAYER_MASK_OTHER_PLAYER_ENEMY = 0;
    static public int LAYER_MASK_OTHER_PLAYER = 0;
    static public int LAYER_MASK_DMM_PLAYER = 0;
    static public int LAYER_MASK_STORY_PLAYER = 0;
    static public int LAYER_MASK_AUTOTARGET = 0;
    static public int LAYER_MASK_BUILDING = 0;
    static public int LAYER_MASK_AIR_WALL = 0;
    static public int LAYER_MASK_UI = 0;
    //static public int LAYER_MASK_SKYBOX = 0;
    //static public int LAYER_MASK_BATTLE_EFFECT = 0;
    //static public int LAYER_MASK_SCENE_EFFECT = 0;
    static public int LAYER_MASK_DEATH_BOX = 0;
    static public int LAYER_MASK_FREEVIEW_PLAYER = 0;
#endregion


#region 相机相关

    static public int CAMERA_DEEPTH_SCENECAMERA = 0;
    static public int CAMERA_DEEPTH_MAINPLAYER_CAMERA = 1;
    static public int CAMERA_DEEPTH_SCENE_TEX_CAMERA = 2;

    static public int FOV_SCENE_CAMERA_DEFAULT = 45;
    static public int FOV_MAINPLAYER_CAMERA = 60;
    static public int FOV_SNIPER_MAINPLAYER_CAMERA = 32;

    static public float MAINPLAYER_CAMERA_NEARPLANE = 0.001f;
    static public float MAINPLAYER_CAMERA_FARPLANE = 10f;

    static public EptVector3 MAIN_PLAYER_SCENECAMERA_LOCALPOS;
    static public Vector3 MAIN_PLAYER_SCENECAMERA_LOCALROT;

    static public Vector3 MAIN_PLAYER_CAMERA_LOCALPOS = new Vector3();
    static public Vector3 MAIN_PLAYER_CAMERA_LOCALROT = new Vector3();

    static public Vector3 MAIN_PLAYER_CAMERA_MOVEDOWNPOS = new Vector3(0, -0.15f, 0);
    static public float MAIN_PLAYER_CAMERA_MOVEDOWN_TIME = 0.15f;
    static public float MAIN_PLAYER_CAMERA_MOVEUP_TIME = 0.1f;
    static public float MAIN_PLAYER_CAMERA_MOVE_GRAVITY_TIME = 0.25f;

    public const float CAMERA_DRAG_FACTOR = 0.2f;
    public const float CAMERA_MODE_FOLLOW_ROTY_MIN = 10;
    public const float CAMERA_MODE_FOLLOW_ROTY_MAX = 170;

    public const float CAMERA_MODE_FOLLOW_DELTA_HEIGHT = 1.7f;
    public const float CAMERA_MODE_FOLLOW_DISTANCE = 2f;

    public const float CAMERA_MODE_FIXEDPOS_HEIGHT = 2f;
    public const float CAMERA_MODE_FIXEDPOS_DISTANCE = 6.0F;

    public const float CAMERA_MODE_RESCURE_ROTY_MIN = 20;
    public const float CAMERA_MODE_RESCURE_ROTY_MAX = 80;

    public const float CAMERA_RAYTEST_OFFSET = 0.3f;

    static public float WATCH_DEAD_CAMERA_DISTANCE = 5f;
    static public float WATCH_DEAD_CAMERA_HEIGHT = 1.5f;
    static public float WATCH_DEAD_CAMERA_ROTATION_X = 10.0f;
    //static public float WATCH_DEAD_CAMERA_TIME = 3000f;

    //static public float WATCH_DEAD_CAMERA_MOVETIME = 0.05f;
    //static public float WATCH_DEAD_CAMERA_FOLLOWKILLERTIME = 300000f;
    //static public float WATCH_DEAD_CAMERA_ROTTIME = 0.4f;       // 不能太快，否则相机容易穿墙
    //static public float WATCH_DEAD_CAMERA_MOVETIME = 0.6f;


#endregion


#region 角色相关

    // Player
    static public float PLAYER_COLLIDER_HEIGHT_STAND = 1.7f;
    static public float PLAYER_COLLIDER_HEIGHT_CROUCH = 1.2f;
    //static public float PLAYER_COLLIDER_RADIUS = 0.4f;          // 太小的话
    //static public float PLAYER_COLLIDER_SLOPE_LIMIT = 45f;

    static public float PLAYER_CHARACTERCONTROLLER_CENTER_OFFSET = -0.01f;
    static public float PLAYER_GROUNDCHECK_HEIGHT_OFFSET = 0.1f;
    static public float PLAYER_GROUNDED_OFFSET = 0.04f;
    static public float PLAYER_HEAD_SLOT_OFFSET = 0f;
    //static public float PLAYER_SEND_POS_INTERVAL = 0.1f;               // 发送坐标数据到服务器间隔时间 秒
    static public float PLAYER_NOTMOVE_SEND_POS_INTERVAL = 2f;       // 主角静止时发送坐标间隔
    static public float PLAYER_SETPOS_TIMEOUT = 3.0f;       // 坐标同步超时时间
    public const float PLAYER_SETPOS_JUMP_TIMEOUT = 1.0f;

    //会被RoleConst中配置的数据覆盖
    static public float PLAYER_AIM_BACK_CENTER_DELAY = 0.5f;            //自动瞄准后枪口归正的延迟时间
    static public float PLAYER_AIM_BACK_CENTER_SPEED = 6f;             //自动瞄准后枪口归正的速度 （度/秒）
    static public float PLAYER_AIM_ROTATE_SPEED = 40f;
    static public float PLAYER_CROSSHAIR_AIM_SPEED = 200f;             //准星自动瞄准的速度 （度/秒）

    static public float OTHER_PLAYER_JUMP_COMPLETE_CHECK_INTERVAL = 0.2f;
    static public float OTHER_PLAYER_DEAD_DISAPPEAR_TIME = 2.5f;

    static public float PLAYER_SWITCHWEAPON_SEND_PROTO_INTERVAL = 0.2f; //切换发送协议最小时间间隔

    static public float PLAYER_JUMP_MOVE_SLOW_TIME = 0.7f;        //跳跃后按后方向键后角色由移动到停止的时间（秒）
    static public float PLAYER_SET_ALPHA_TIME = 1f;               //透明度从0到1变化的时间

    static public float PLAYER_SET_SCALE_TIME = 1f;               //大小从0到1变化的时间
    static public float PLAYER_SET_FOV_TIME = 1f;                 //相机FOV变化的时间

    static public float PLAYER_BORN_CHECK_OTHERPLAYER_DISTANCE = 5f;

    static public float FOLLOW_PLAYER_DEFAULT_VERTICAL_ROTATE = -20f;

    static public float PLAYER_JUMP_MAX_HEIGHT
    {
        get { return 1.0f; }
    }

    static public float MPSpeedFactor = 1.01f;
    static public float MPFireFactor = 1.01f;
#endregion


#region 物理相关

    static public float AIM_POINT_RAYTEST_INTERVAL = 0.1f;  // 主角准心选中的角色检测频率
    //static public float GRAVITY_JUMPUP = 10f;
    static public float GRAVITY_JUMPUP
    {
        get{ return 10;}
    }
    static public float GRAVITY
    {
        get { return 25; }
    }
    static public float GRAVITY_RAY_DISTANCE = 50;
#endregion

#region UI相关

    // 瞄准时，枪口下名字的占整个高度的距离比
    public const float AIM_NAME_HEGIHT_RATE = 0.1f;
#endregion

#region ConfigGameSetting

    static public float EffectPosZOffset;

    static public float MPColliderRadius;
    static public float MPStepOffset;
    static public float OPColliderRadius;
    static public float PlayerColliderSlope;

#endregion

#region 调试开关变量

    public static bool enableGunFire = true;
    public static bool enableGunShell = true;
    public static bool enableDanheng = true;
    public static bool enableOPRayTest = true;
    public static bool enableBattleUI = true;
    public static bool enableSmallMap = true;
    public static bool enableSound = true;
    public static bool enableUIWorldAchievement = true;
    public static bool enableUISelfAchievement = true;
    public static bool enableUIKillList = true;
    public static bool enableRigRot = true;

    public static bool enableCameraLog = false;
    public static bool checkCheat = true;
    public static bool openGravity = true;
    public static bool openJumpUp = true;
    public static bool jumpUpMsg = false;
    public static bool openMove = true;
    //public static bool openMoveStack = false;
    public static bool fireRateMsg = false;
    public static bool openCheckPosMsg = false;
    public static bool openFireHitInfo = false;

#endregion

#region 伤害提示相关
    static public float DamageTipsTotalTime = 0.8f;
    static public float DamageTipsTweenTime = 0.5f;
    static public int DamageTipsFontSize = 22;
    static public Vector2 DamageTipsPosRamdomX = new Vector2(-1f, 1f);
    static public Vector2 DamageTipsPosRamdomY = new Vector2(1f, 2f);
    static public Vector2 DamageTipsScaleChange = new Vector2(0.5f, 1f);
#endregion

#region 手雷
    public const int MAX_EXIST_PROJECTILE_SIZE = 5;        //最大同时存在的投射物数量
#endregion

    #region 小地图 Update 执行频率
    public static int smallMapUpdateFrameInterval = 6;
    #endregion

    #region LOD
    public static float[] IodLevelDistance;
    #endregion

    #region 圣兽枪
    public static float FightAchieveEffectDelay = 0.5f;
    #endregion


}


