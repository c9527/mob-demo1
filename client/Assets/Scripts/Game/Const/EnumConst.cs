﻿public enum EnumMoneyType
{
    NONE,
    ALL,
    DIAMOND,
    COIN,
    MEDAL,
    CASH,
    CORPS_COIN,
    EVENTCOIN_002,
    NON_SALE,
    ACTIVE_COIN, 
}

public enum EnumPlatform
{
    android,
    ios,
    web,
    pc,
    other
}


/// <summary>
/// 频道优化  PC端和手机端划分
/// </summary>
public enum EnumPlatType
{
    other =0,
    phone = 1,
    pc = 2,
}

public enum EnumItemTag
{
    EQUIP,      //装备
    ACCESSORY,  //配件（分可装备的和未鉴定的）
    ARMOR,      //防弹衣
    HELMET,     //头盔
    BOOTS,      //靴子
    GRENADE,    //手雷
    DAGGER,     //匕首
    FLASHBOMB,  //闪光弹
    MACHINEGUN, //机枪
    PISTOL,     //手枪
    RIFLE,      //步枪
    RIFLEGRENADE,   //榴弹枪
    SHOTGUN,    //散弹枪
    SNIPER,     //狙击枪
    SUBMACHINE, //冲锋枪
    PAW,        //爪
    OTHER,      //其他
    BULLET,     //子弹
    WEAPON1,    //主武器
    WEAPON2,    //副武器
    BOM,        //C4炸弹
    SKILLWEAPON,//带技能的武器

    MATERIAL,   //材料
    STRENGTHEN, //强化用碎片

    MONEY,      //货币道具
    MEDAL,      //奖章
    COIN,       //金币
    DIAMOND,    //钻石
    CASH,       //分享券
    CORPS_COIN, //战队币
    EVENTCOIN_001,  //圣诞帽

    EXP,        //经验

    CORPS,      //战队
    FIGHTING,   //战队贡献

    DECORATION, //装饰品
    HEAD,       //头部
    FACE,       //面部
    SHOULDER,   //肩部
    WAIST,      //腰部

    ITEM,       //常规道具
    RENAMECARD, //改名卡
    BUFFCARD,   //Buff卡
    EVENT,      //活动类道具
    AVATAR,     //头像

    ROLE,       //角色
    BOSS,       //BOSS
    OBJECT,     //躲猫猫物体
}

public enum EnumSpeechStatus
{
    NONE = 0,
    INITIALIZING = 1,
    INITIALIZED = 2,
}

public enum EnumTag
{
    LEGION_WAR_FIRST =0,
}

public enum EnumSaveLog
{
    STAR_COMMENT = 0,
    WEIXIN_SHARE,
    QQ_SHARE,
    WEIBO_SHARE
}

public enum EnumFrom
{
    BATTLERESULT = 0,
    LOTTOTY,
    RECHARGE,
}

public enum EnumStarComment
{
    COMMENT = 0,
    COMPLAINT,
    NEXTTIME,
}

public enum EnumWeixinShare
{
    FRIEND = 0,
    FRIENDLINE,
}

public enum EnumChannelNation
{
    KORO_PHONE = 201,
    NATION_PHONE = 202,
    NATION_PC_WEB = 203,
    KORO_PC_WEB = 211,
}