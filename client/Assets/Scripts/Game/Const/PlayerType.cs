﻿public enum PlayerType
{
    All,
    AllNoAI,
    AllNoAIButBase,
    CampAll,
    CampNoAI,
    CampNoAIButBase,
}
