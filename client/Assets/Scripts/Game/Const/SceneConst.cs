﻿public class SceneConst
{

    #region Scene Name

    public const string SCENE_EMPTY = "Empty";
        
    #endregion

     

    #region Scene Child GameObject Name

        public const string SCENE_CHILD_COLLIDERGROUP = "ColliderGroup";
        //public const string SCENE_CHILD_SKYBOX = "skybox";

        //public const string EFFECT_GROUP_EFFECT1 = "effect1";
        //public const string EFFECT_GROUP_EFFECT2 = "effect2";

    #endregion

    #region Scene Child GameObject Path
        public const string SCENE_CHILD_BUILDING_PATH = "Scene/Building";
        public const string SCENE_CHILD_LIGHTGROUP_PATH = "Scene/LightGroup";
        public const string SCENE_CHILD_SCENEITEM_PATH = "Scene/SceneItem";
        public const string SCENE_CHILD_EFFECT_PATH = "Scene/Effect";

        public const string SCENE_CHILD_BOMBPOINT_PATH = "Config/BombPoint";
        public const string SCENE_CHILD_OWPOINT_PATH = "Config/JDPoint";//据点

        public const string SCENE_CHILD_GUNPOINT_PATH = "Config/BuyPoint/GunPoint";
        public const string SCENE_CHILD_BUFFPOINT_PATH = "Config/BuyPoint/BuffPoint";
        public const string SCENE_CHILD_BULLETPOINT_PATH = "Config/BuyPoint/BulletPoint";
        public const string SCENE_CHILD_BUFFPOINT1_PATH = "Config/BuyPoint/BuffPoint1";
        public const string SCENE_CHILD_BULLETPOINT1_PATH = "Config/BuyPoint/BulletPoint1";

        public const string SCENE_CHILD_DIRCTION_PATH = "Config/Direction";

        public const string SCENE_CHILD_COLLIDER_BUILDING_PATH = "ColliderGroup/building";
        public const string SCENE_CHILD_COLLIDER_AIRWALL_PATH = "ColliderGroup/airwall";
        public const string SCENE_CHILD_COLLIDER_LADDER_PATH = "ColliderGroup/ladder";
        public const string SCENE_CHILD_COLLIDER_DEATHBOX_PATH = "ColliderGroup/deathbox";
        public const string SCENE_CHILD_COLLIDER_SPRING_PATH = "ColliderGroup/spring";
        
    #endregion

}
