﻿using System.Collections.Generic;
using UnityEngine;

public class AniConst
{

#region Animator Trigger Value

    // animation triger name
    public const string TriggerStrafeFire = "strafeFire";

    // animation float name
    public const string FLOAT_DIR_FORWARD_BACK = "dirForwardBack";
    public const string FLOAT_DIR_LEFT_RIGHT = "dirLeftRight";

    //用于boss技能中三个动作中间动作的循环控制
    public const string OTHERPLAYER_IS_END_LOOP = "isEndLoop";

    #endregion

    #region Animator Layer

    //public const string ANIMATOR_LAYER_BODY = "Body";
    //public const string ANIMATOR_LAYER_UPBODY = "UpBody";
    //public const string ANIMATOR_LAYER_BEHIT = "Behit";
    //public const string ANIMATOR_LAYER_BREST = "Brest";

    public const int ANIMATOR_LAYER_NUM_BOY = 3;
    public const int ANIMATOR_LAYER_NUM_GIRL = 4;    
#endregion

#region 动画名
    public const string stand = "stand";
    public const string crouch = "crouch";
    public const string jumpStart = "jumpStart";
    public const string jumpEnd = "jumpEnd";
    public const string jumpHuanchong = "jumpHuanchong";
    public const string poweron = "poweron";
#endregion


    static public string GetPlayerDeadAni(Vector3 faceDir, Vector3 curPos, Vector3 fromPos, int dieAniType, bool stand, bool shotHead)
    {
        if (dieAniType <= 0)
            dieAniType = GameConst.WEAPON_DIE_ANI_TYPE_LIGHT;

        // 要注意的是，受击方向跟动画方向相反

        Vector3 shotDir = fromPos - curPos;
        shotDir.y = 0;
        shotDir.Normalize();

        float angle = Vector3.Angle(faceDir, shotDir);
        string dirName;

        string ani = "dead";
        if (dieAniType == GameConst.WEAPON_DIE_ANI_TYPE_GRENADE)
        {
            if (angle <= 90)
                dirName = "Back";
            else
                dirName = "Forward";

            ani += stand ? "" : "Crouch";
            ani += dirName;
            ani += dieAniType;
        }
        else
        {
            if (angle <= 45)
                dirName = "Back";
            else if (angle >= 135)
                dirName = "Forward";
            else
            {
                Vector3 corssDir = Vector3.Cross(faceDir, shotDir);
                if (corssDir.y > 0)
                    dirName = "Left";
                else
                    dirName = "Right";
            }

            ani += stand ? "" : "Crouch";
            ani += shotHead ? "Head" : "";
            ani += dirName;
            ani += dieAniType;
        }

        return ani;
    }


    static public string GetSimpleDeadAni(Vector3 faceDir, Vector3 curPos, Vector3 fromPos, bool stand)
    {
        Vector3 shotDir = fromPos - curPos;
        shotDir.y = 0;
        shotDir.Normalize();

        float angle = Vector3.Angle(faceDir, shotDir);
        string dirName = string.Empty;

        string ani = "dead";

        if (angle <= 45)
            dirName = "Back";
        else if (angle >= 135)
            dirName = "Forward";
        else
        {
            Vector3 corssDir = Vector3.Cross(faceDir, shotDir);
            if (corssDir.y > 0)
                dirName = "Left";
            else
                dirName = "Right";
        }

        ani += stand ? "" : "Crouch";
        ani += dirName;
        return ani;
    }

}