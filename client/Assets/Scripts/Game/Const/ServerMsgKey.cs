﻿using UnityEngine;
using System.Collections;

public class ServerMsgKey
{

#region fight

    static public string from = "from";

    static public string id = "id";

    //static public string rayHitPos = "rayHitPos";

    static public string firePose = "firePose";

    static public string fireDir = "fireDir";

    static public string zooming = "zooming";

    static public string arrHitData = "list";

    static public string hitPos = "hitPos";     // 如果击中的是玩家，那坐标为相对于玩家的坐标，如果击中的是建筑，则是世界坐标
    static public string rayHitPos = "rayHitPos"; // 如果没有击中任何东西，则这里取射击方向上的一个点
    static public string matType = "matType";

    static public string fromPos = "fromPos";

    static public string distance = "distance";

    static public string targetType = "targetType";
    static public int TargetType_Player = 1;
    static public int TargetType_Building = 2;

    static public string notPlayAni = "notPlayAni";   // 用于区别是不是从同一次射击发射出来的，如果是，则这些收到这些消息后，只播放一次动画

    // HitPlayer
    static public string bodyPart = "bodyPart";

    static public string reloadTime = "reloadTime";
#endregion

#region static_info

    static public string bombHolder = "bomb_holder";
#endregion
	 
}
