﻿public class LanguageConst
{
    public const string LANG_RU = "ru";
    public const string LANG_EN = "en";
    public const string LANG_KR = "kr";
    public const string LANG_ZH = "zh";
    public const string LANG_HK = "hk";
}
