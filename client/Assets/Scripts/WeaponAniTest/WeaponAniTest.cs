﻿using UnityEngine;
using System.IO;

public class WeaponAniTest : MonoBehaviour
{

    private bool isShowAllSelect = true;
    private bool isShowModelSelect = true;
    private bool isShowWeaponSelect = true;
    private bool isShowActionSelect = true;

    private Vector2 scrollPosition = Vector3.zero;

    public const int WEAPON_TYPE_PISTOL = 1;
    public const int WEAPON_TYPE_RIFLE = 2;
    public const int WEAPON_TYPE_SUBMACHINE_GUN = 3;
    public const int WEAPON_TYPE_SHOT_GUN = 4;
    public const int WEAPON_TYPE_MACHINE_GUN = 5;
    public const int WEAPON_TYPE_SNIPER_GUN = 6;
    public const int WEAPON_TYPE_KNIFE = 7;
    public const int WEAPON_TYPE_GRENADE = 8;
    public const int WEAPON_TYPE_BOM = 9;
    public const int WEAPON_TYPE_PAW = 10;

    public string[] allRoles;

    private Camera m_camrea;

    private GameObject m_go;
    private string m_roleModel;
    private Animator m_animator;
    private int m_kWeaponId;
    private GameObject m_kWeapon;
    private GameObject m_kWeaponLeft;
    private Transform m_transLeftHandSlot;
    private Transform m_transRightHandSlot;

    private Transform m_transMainPlayerCameraSlot;
    private Transform m_transMainPlayerCameraShakeTarget;
    private Transform m_transMainPlayerCamera;
    private Camera m_cameraMainPlayer;
    private int m_iModelSex;

    private FPSAnimation m_kAni;
    private FPSAnimation m_kAni2;

    private bool m_bInit;

    private ConfigItemWeaponLine m_kWeaponLine;
    private Animation m_kMainPlayerCameraAnimation;

    private MainPlayerCameraShaker m_kMPCS;

    private Transform m_transBipRoot;
    private Transform m_transBipRootLeft;
    protected EffectGunFire m_kEffectGunFire;
    protected EffectGunFire m_kEffectGunFire2;
    protected Transform m_transFirePoint;
    protected Transform m_transFirePoint2;
    protected Transform m_transCartridge;
    protected Transform m_transCartridge2;

    private bool m_bIsWalk;
    private bool m_bIsCrouch;

    private bool m_bIsMainPlayer;
    private bool m_bIsBothHandSlot;

    private float m_fFireTick;
    private float m_fFireRate;

    private bool m_bIsWalkWaggle;

    public const int WEAPON_SLOTTYPE_RIGHTHAND = 0;
    public const int WEAPON_SLOTTYPE_LEFTHAND = 1;
    public const int WEAPON_SLOTTYPE_BOTHHAND = 2;

    public const int GUI_LINE_COUNT = 4;

    public string m_modelFilter;
    public string m_weaponFilter;

    void Awake()
    {
        m_go = null;
        m_animator = null;
        m_kWeapon = null;
        m_roleModel = "";
        m_kWeaponId = -1;
        m_bInit = false;
        m_bIsMainPlayer = true;
        m_bIsWalkWaggle = false;
        m_camrea = GetComponent<Camera>();
        m_kAni = new FPSAnimation();
        m_kAni2 = new FPSAnimation();
        new EffectManager();
        ResourceManager.Init(this, 
            delegate{ 
                ConfigManager.LoadAllConfig(LoadConfigComplete);
            }
       );
    }

    void LoadRoleRes(string roleModel)
    {
        if (string.IsNullOrEmpty(roleModel) || m_roleModel == roleModel)
        {
            return;
        }

        m_roleModel = roleModel;
        
        Object go = Resources.Load(PathHelper.GetRolePrefabPath(roleModel)) as GameObject;
        if (go == null)
        {
           Logger.Error("Player model load failed, path: " + roleModel);
            return;
        }

        if (m_go != null)
        {
            DestroyImmediate(m_go);
        }

        m_go = GameObject.Instantiate(go) as GameObject;

        //查找左右手挂载点
        Transform transBipRightHandCenter = GameObjectHelper.FindChildByTraverse(m_go.transform, ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
        if (transBipRightHandCenter == null)
           Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_RIGHT_HAND_CENTER);
        else
        {
            m_transRightHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipRightHandCenter, ModelConst.PLAYER_SLOT_RIGHT_HAND).transform;
            m_transRightHandSlot.ResetLocal();
        }

        Transform transBipLeftHandCenter = GameObjectHelper.FindChildByTraverse(m_go.transform, ModelConst.BIP_NAME_LEFT_HAND_CENTER);
        if (transBipLeftHandCenter == null)
           Logger.Error("Can't find Player Bip : " + ModelConst.BIP_NAME_LEFT_HAND_CENTER);
        else
        {
            m_transLeftHandSlot = GameObjectHelper.CreateChildIfNotExisted(transBipLeftHandCenter, ModelConst.PLAYER_SLOT_LEFT_HAND).transform;
            m_transLeftHandSlot.ResetLocal();
        }

        m_go.transform.localPosition = Vector3.zero;
        m_go.transform.localEulerAngles = Vector3.zero;
        m_animator = m_go.GetComponent<Animator>();

        m_bIsMainPlayer = roleModel.StartsWith("arm");

        if(!m_bIsMainPlayer)
        {
            InitOtherPlayer();
            m_bIsWalkWaggle = (m_iModelSex == ModelConst.MODEL_PLAYER_BOY_SEX) && m_animator.layerCount > 2
                && m_animator.GetLayerName(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX) == GameConst.OTHERPLAYER_WALKWAGGLE_LAYER_NAME;
        }
        else
        {
            InitMainPlayer();
            m_bIsWalkWaggle = false;
        }
        
    }

    void InitOtherPlayer()
    {
        m_iModelSex = GetOPModelSex(m_roleModel);

        m_camrea.enabled = true;

        m_go.AddComponent<MouseDrag>();

        m_kMainPlayerCameraAnimation = null;

        if (m_kWeaponId != -1)
        {
            int weaponId = m_kWeaponId;
            m_kWeaponId = -1;
            SwitchWeapon(weaponId);
        }
    }

    void InitMainPlayer()
    {
        m_camrea.enabled = false;

        m_transMainPlayerCameraSlot = GameObjectHelper.CreateChildIfNotExisted(m_go.transform, ModelConst.SLOT_MAINPLAYER_CAMERA).transform;
        m_transMainPlayerCameraSlot.localPosition = GameSetting.MAIN_PLAYER_CAMERA_LOCALPOS;
        m_transMainPlayerCameraSlot.localEulerAngles = GameSetting.MAIN_PLAYER_CAMERA_LOCALROT;

        GameObject goMainPlayerCameraShakeTarget = new GameObject("MainPlayerCameraShakeTarget");
        m_transMainPlayerCameraShakeTarget = goMainPlayerCameraShakeTarget.transform;
        m_transMainPlayerCameraShakeTarget.SetParent(m_transMainPlayerCameraSlot);
        m_transMainPlayerCameraShakeTarget.ResetLocal();

        GameObject goMainPlayerCamera = new GameObject("MainPlayerCamera");
        m_transMainPlayerCamera = goMainPlayerCamera.transform;
        m_transMainPlayerCamera.SetParent(m_transMainPlayerCameraShakeTarget);
        m_transMainPlayerCamera.ResetLocal();
        m_cameraMainPlayer = GameObjectHelper.GetComponent<Camera>(goMainPlayerCamera);

        m_cameraMainPlayer.orthographic = false;
        m_cameraMainPlayer.fieldOfView = GameSetting.FOV_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.nearClipPlane = GameSetting.MAINPLAYER_CAMERA_NEARPLANE;
        m_cameraMainPlayer.farClipPlane = GameSetting.MAINPLAYER_CAMERA_FARPLANE;
        //m_cameraMainPlayer.cullingMask = GameSetting.LAYER_MASK_MAIN_PLAYER;
        m_cameraMainPlayer.depth = GameSetting.CAMERA_DEEPTH_MAINPLAYER_CAMERA;
        m_cameraMainPlayer.clearFlags = CameraClearFlags.Skybox;

        m_kMainPlayerCameraAnimation = GameObjectHelper.GetComponent<Animation>(goMainPlayerCamera);
        m_kMainPlayerCameraAnimation.playAutomatically = false;

        m_kMPCS = GameObjectHelper.GetComponent<MainPlayerCameraShaker>(m_go);
        m_kMPCS.BindMainPalyerCamera(m_transMainPlayerCameraShakeTarget);

        if (m_kWeaponId != -1)
        {
            int weaponId = m_kWeaponId;
            m_kWeaponId = -1;
            SwitchWeapon(weaponId);
        }
    }

    int GetOPModelSex(string model)
    {
        if (string.IsNullOrEmpty(model) == true)
            return 0;

        string modelName = model.ToLower();
        if(modelName.Contains(ModelConst.MODEL_PLAYER_GIRL_CHAR))
            return ModelConst.MODEL_PLAYER_GIRL_SEX;
        else if(model.Contains(ModelConst.MODEL_PLAYER_BOY_CHAR))
            return ModelConst.MODEL_PLAYER_BOY_SEX;

        return 0;
    }

    void OnGUI()
    {
        if (m_bInit)
        {
            m_fFireTick += Time.deltaTime;
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            if (isShowAllSelect = GUILayout.Toggle(isShowAllSelect, "人物武器列表"))
            {
                GUILayout.Space(20);
                scrollPosition = GUILayout.BeginScrollView(scrollPosition);

                GUILayout.BeginVertical();
                if (isShowModelSelect = GUILayout.Toggle(isShowModelSelect, "模型选择"))
                {
                    GUILayout.BeginHorizontal();
                    int ri = 0;
                    for (int i = 0, imax = allRoles.Length; i < imax; ++i)
                    {
                        if (!string.IsNullOrEmpty(m_modelFilter) && !allRoles[i].Contains(m_modelFilter))
                            continue;
                        ri++;
                        if (GUILayout.Button(allRoles[i]))
                        {
                            LoadRoleRes(allRoles[i]);
                        }
                        if (ri != 0 && i != imax - 1 && ri % GUI_LINE_COUNT == 0)
                        {
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                        }
                    }
                    GUILayout.EndHorizontal();
                }

                GUILayout.Space(20);
                if (isShowWeaponSelect = GUILayout.Toggle(isShowWeaponSelect, "武器选择"))
                {
                    GUILayout.BeginHorizontal();
                    int wi = 0;
                    for (int i = 0, imax = ConfigManager.GetConfig<ConfigItemWeapon>().m_dataArr.Length; i < imax; ++i)
                    {
                        ConfigItemWeaponLine configItem = ConfigManager.GetConfig<ConfigItemWeapon>().m_dataArr[i];
                        if (configItem.ID > 10000 || (!string.IsNullOrEmpty(m_weaponFilter) && !configItem.DispName.Contains(m_weaponFilter)))
                            continue;
                        wi++;

                        if (GUILayout.Button(configItem.DispName))
                        {
                            SwitchWeapon(configItem.ID);
                        }
                        if (wi != 0 && i != imax - 1 && wi % GUI_LINE_COUNT == 0)
                        {
                            GUILayout.EndHorizontal();
                            GUILayout.BeginHorizontal();
                        }
                    }
                    GUILayout.EndHorizontal();
                }
                GUILayout.EndVertical();
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
            GUILayout.Space(20f);

            GUILayout.BeginVertical();

            #region  动作
            if (isShowActionSelect = GUILayout.Toggle(isShowActionSelect, "动作"))
            {
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                if (m_bIsMainPlayer)
                {
                    if (m_kWeaponLine != null)
                    {
                        if (!string.IsNullOrEmpty(m_kWeaponLine.MPFireBeginAni) && GUILayout.Button("开火前"))
                        {
                            FireBegin();
                        }

                        if (!string.IsNullOrEmpty(m_kWeaponLine.MPFireEndAni) && GUILayout.Button("开火后"))
                        {
                            FireEnd();
                        }

                        for (int i = 0; i < m_kWeaponLine.MPFireAni.Length; ++i)
                        {
                            if (m_kWeaponLine.MPFireAni[i] == null || (m_kWeaponLine.MPFireAni[i].StartsWith(" ")))
                                continue;
                            if (i == 0)
                            {
                                if (GUILayout.Button("单射fire" + i))
                                {
                                    if (m_fFireTick > (60f / m_kWeaponLine.FireRate))
                                    {
                                        m_fFireTick = 0;
                                        Fire(i);
                                    }
                                }
                            }
                            else
                            {
                                if (GUILayout.RepeatButton("连射fire" + i))
                                {
                                    if (m_fFireTick > (60f / m_kWeaponLine.FireRate))
                                    {
                                        m_fFireTick = 0;
                                        Fire(i);
                                    }
                                }
                            }
                        }
                        if (m_kWeaponLine.SubWeapon > 0)
                        {
                            if (GUILayout.Button("开枪fire 子武器"))
                            {
                                var subWeaponLine = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(m_kWeaponLine.SubWeapon);
                                FireSub(subWeaponLine);
                            }
                        }
                    }
                }
                else
                {
                    if (m_kWeaponLine != null && m_kWeaponLine.OPFireAni.Length > 0)
                    {
                        for (int i = 0; i < m_kWeaponLine.OPFireAni.Length; ++i)
                        {
                            if (GUILayout.Button("开枪fire" + i))
                            {
                                if (m_fFireTick > (60f / m_kWeaponLine.FireRate))
                                {
                                    m_fFireTick = 0;
                                    Fire(i);
                                }
                            }
                        }

                        if (m_kWeaponLine.SubWeapon > 0)
                        {
                            if (GUILayout.Button("开枪fire 子武器"))
                            {
                                var subWeaponLine = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(m_kWeaponLine.SubWeapon);
                                FireSub(subWeaponLine);
                            }
                        }

                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                if (m_bIsMainPlayer)
                {
                    if (m_kWeaponLine != null && m_kWeaponLine.MPReloadAni.Length > 0)
                    {
                        for (int i = 0; i < m_kWeaponLine.MPReloadAni.Length; ++i)
                        {
                            if (m_kWeaponLine.Class != 4 || i % 2 == 0)
                                if (GUILayout.Button("上子弹reload" + i))
                                {
                                    Reload(i);
                                }
                        }
                    }
                }
                else
                {
                    if (GUILayout.Button("上子弹reload"))
                    {
                        Reload(0);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("换枪switch"))
                {
                    Switch();
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                if (GUILayout.Button(m_bIsWalk ? "停下" : "走动"))
                {
                    Walk(!m_bIsWalk);
                }

                if (!m_bIsMainPlayer)
                {
                    if (GUILayout.Button(m_bIsCrouch ? "站立" : "蹭下"))
                    {
                        Crouch(!m_bIsCrouch);
                    }
                }
                GUILayout.Space(10f);
                GUILayout.EndHorizontal();

                if (m_animator != null && !m_bIsMainPlayer)
                {
                    GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("deadBack"))
                    {
                        PlayAni("deadBack");
                    }
                    if (GUILayout.Button("deadForward"))
                    {
                        PlayAni("deadForward");
                    }
                    if (GUILayout.Button("deadLeft"))
                    {
                        PlayAni("deadLeft");
                    }
                    if (GUILayout.Button("deadRight"))
                    {
                        PlayAni("deadRight");
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("deadCrouchBack"))
                    {
                        PlayAni("deadCrouchBack");
                    }
                    if (GUILayout.Button("deadCrouchForward"))
                    {
                        PlayAni("deadCrouchForward");
                    }
                    if (GUILayout.Button("deadCrouchLeft"))
                    {
                        PlayAni("deadCrouchLeft");
                    }
                    if (GUILayout.Button("deadCrouchRight"))
                    {
                        PlayAni("deadCrouchRight");
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                }

                GUILayout.EndVertical();
            }
            #endregion

            GUILayout.Label("人物过滤：");
            m_modelFilter = GUILayout.TextField(m_modelFilter);
            GUILayout.Label("武器过滤：");
            m_weaponFilter = GUILayout.TextField(m_weaponFilter);

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    void SwitchWeapon(int id)
    {
        if (m_kWeaponId == id)
            return;

        if (m_kWeapon != null)
        {
            DestroyImmediate(m_kWeapon);
        }

        m_kWeaponLine = ConfigManager.GetConfig<ConfigItemWeapon>().GetLine(id);

        Object go = Resources.Load(PathHelper.GetWeaponPrefabPath(m_bIsMainPlayer ? m_kWeaponLine.MPModel : m_kWeaponLine.OPModel)) as GameObject;

        if (go == null)
        {
            return;
        }

        if (m_kEffectGunFire != null)
        {
            m_kEffectGunFire.Release();
            m_kEffectGunFire = null;
        }

        if (m_kEffectGunFire2 != null)
        {
            m_kEffectGunFire2.Release();
            m_kEffectGunFire2 = null;
        }

        m_transFirePoint = null;
        m_transCartridge = null;
        m_transFirePoint2 = null;
        m_transCartridge2 = null;

        m_kWeapon = GameObject.Instantiate(go) as GameObject;

        m_transBipRoot = GameObjectHelper.FindChildByTraverse(m_kWeapon.transform, ModelConst.WEAPON_BIP_ROOT);       
        
        if (m_bIsMainPlayer)
        {
            Animation ani = GameObjectHelper.GetComponent<Animation>(m_kWeapon);
            m_kAni.SetAnimation(ani);
            AddMainPlayerCameraWalkAni();
            LoadWeaponAni(m_kAni);
        }

        if (GetIsHadGunEffect(m_kWeaponLine.Class))
        {
            m_transFirePoint = GameObjectHelper.FindChildByTraverse(m_kWeapon.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
            if (m_transFirePoint == null)
                Logger.Error("找不到枪口火花挂接点!");

            m_transCartridge = GameObjectHelper.FindChildByTraverse(m_kWeapon.transform, ModelConst.WEAPON_SLOT_CARTRIDGE);
            if (m_transCartridge == null)
                Logger.Error("找不到弹壳挂接点!");

            AttachFireEffect(false);
        }

        if (m_kWeaponLeft != null)
        {
            DestroyImmediate(m_kWeaponLeft);
        }

        m_bIsBothHandSlot = m_kWeaponLine.SlotType == WEAPON_SLOTTYPE_BOTHHAND;
        if (m_bIsBothHandSlot)
        {
            Object leftGo = Resources.Load(PathHelper.GetWeaponPrefabPath(m_bIsMainPlayer ? m_kWeaponLine.MPModel2 : m_kWeaponLine.OPModel2)) as GameObject;

            if (leftGo == null)
            {
                return;
            }

            m_kWeaponLeft = GameObject.Instantiate(leftGo) as GameObject;
            m_transBipRootLeft = GameObjectHelper.FindChildByTraverse(m_kWeaponLeft.transform, ModelConst.WEAPON_BIP_ROOT);

            if (GetIsHadGunEffect(m_kWeaponLine.Class))
            {
                m_transFirePoint2 = GameObjectHelper.FindChildByTraverse(m_kWeaponLeft.transform, ModelConst.WEAPON_SLOT_FIREPOINT);
                if (m_transFirePoint2 == null)
                    Logger.Error("找不到枪口火花挂接点!");

                m_transCartridge2 = GameObjectHelper.FindChildByTraverse(m_kWeaponLeft.transform, ModelConst.WEAPON_SLOT_CARTRIDGE);
                if (m_transCartridge2 == null)
                    Logger.Error("找不到弹壳挂接点!");
            }

            AttachFireEffect(true);

            if (m_bIsMainPlayer)
            {
                Animation ani = GameObjectHelper.GetComponent<Animation>(m_kWeaponLeft);
                m_kAni2.SetAnimation(ani);
                LoadWeaponAni(m_kAni2);
            }
        }

        AttachToPlayer();
        WeaponAttachComplete();
        Walk(false);
        Crouch(false);
    }

    public void WeaponAttachComplete()
    {
        m_kMPCS.apply = false;
        m_kMPCS._RecoilValue = m_kWeaponLine.RecoilDis;
        m_kMPCS._RecoilRecoverSpeed = m_kWeaponLine.RecoilRecoverSpeed;

        if (m_bIsMainPlayer)
        {
            if (!string.IsNullOrEmpty(m_kWeaponLine.MPSwitchAni))
                m_animator.Play(m_kWeaponLine.MPSwitchAni);
        }
        else
        {
            if (!string.IsNullOrEmpty(m_kWeaponLine.OPSwitchAni))
                m_animator.Play(m_kWeaponLine.OPSwitchAni);
        }
    }

    virtual public void AttachToPlayer()
    {
        bool isBothHandSlot = false;

        Transform transWeapon = m_kWeapon.transform;

        Transform transSlot = null;

        switch (m_kWeaponLine.SlotType)
        {
            case WEAPON_SLOTTYPE_RIGHTHAND:
                transSlot = m_transRightHandSlot;
                break;
            case WEAPON_SLOTTYPE_LEFTHAND:
                transSlot = m_transLeftHandSlot;
                break;
            case WEAPON_SLOTTYPE_BOTHHAND:
                isBothHandSlot = true;
                break;
        }

        if (!isBothHandSlot) //单手挂载
        {
            SlotHand(transWeapon, m_transBipRoot, transSlot, 0);
        }
        else
        {
            SlotHand(transWeapon, m_transBipRoot, m_transRightHandSlot, 0);
            SlotHand(m_kWeaponLeft.transform, m_transBipRootLeft, m_transLeftHandSlot, 1);
        }
    }

    private void SlotHand(Transform tranWeapon, Transform transRoot, Transform transSlot, int index)
    {
        if (tranWeapon.parent == transSlot)
            return;

        Vector3 vec3Pos = Vector3.zero;
        Vector3 vec3Rotation = Vector3.zero;

        if (m_bIsMainPlayer)
        {
            if (m_kWeaponLine.MPPos != null && m_kWeaponLine.MPPos.Length > 0)
            {
                vec3Pos = m_kWeaponLine.MPPos[index];
                vec3Rotation = m_kWeaponLine.MPRot[index];
            }
        }
        else if (m_iModelSex == ModelConst.MODEL_PLAYER_GIRL_SEX)
        {
            if (m_kWeaponLine.OPGirlPos != null && m_kWeaponLine.OPGirlPos.Length > 0)
            {
                vec3Pos = m_kWeaponLine.OPGirlPos[index];
                vec3Rotation = m_kWeaponLine.OPGirlRot[index];
            }
        }
        else if (m_kWeaponLine.OPBoyPos != null && m_kWeaponLine.OPBoyRot.Length > 0)
        {
            vec3Pos = m_kWeaponLine.OPBoyPos[index];
            vec3Rotation = m_kWeaponLine.OPBoyRot[index];
        }

        tranWeapon.parent = transSlot;

        if (transRoot != null)
            transRoot.ResetLocal();

        tranWeapon.localPosition = vec3Pos;
        tranWeapon.localEulerAngles = vec3Rotation;
    }

    void LoadConfigComplete()
    {
        gameObject.AddComponent<SceneManager>();
        GameSetting.Init();
        InitAllRole();
        m_bInit = true;
        LoadRoleRes(allRoles[0]);
    }

    void InitAllRole()
    {
#if !UNITY_WEBPLAYER
        DirectoryInfo kDI = new DirectoryInfo("Assets/Resources/Roles/");
        FileInfo[] arrFI = kDI.GetFiles("*.prefab", SearchOption.TopDirectoryOnly);
        allRoles = new string[arrFI.Length];
        for (int i = 0; i < arrFI.Length; ++i)
            allRoles[i] = arrFI[i].Name.Substring(0, arrFI[i].Name.IndexOf('.'));
#endif
    }

    void Fire(int index)
    {
        if(m_animator != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(m_kWeaponLine.MPFireAni[index]);
                if (!m_bIsBothHandSlot)
                {
                    if (m_kWeaponLine.FireAni.Length > index)
                        m_kAni.Play(m_kWeaponLine.FireAni[index]);
                }
                else
                {
                    if(index % 2 == 0)
                    {
                        if (m_kWeaponLine.FireAni.Length > index)
                            m_kAni.Play(m_kWeaponLine.FireAni[index]);
                    }
                    else
                    {
                        if (m_kWeaponLine.FireAni.Length > index)
                            m_kAni2.Play(m_kWeaponLine.FireAni[index]);
                    }
                }
            }
            else
            {
                if (m_animator.GetBool(GameConst.OTHERPLAYER_MOVE_STATE_NAME))
                    m_animator.SetLayerWeight(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX, 0f);
                if (m_kWeaponLine.OPFireAni.Length > index)
                {
                    
                    m_animator.Play(m_kWeaponLine.OPFireAni[index]);
                }
            }

            isAnotherHand = index % 2 != 0;

            if (!isAnotherHand)
            {
                if (m_kEffectGunFire != null)
                    m_kEffectGunFire.Play();
                AttachAndPlayCartridgeEffect(false);
            }
            else
            {
                if (m_kEffectGunFire2 != null)
                    m_kEffectGunFire2.Play();
                AttachAndPlayCartridgeEffect(true);
            }
            
            isStart = true;
        }
    }

    void FireSub(ConfigItemWeaponLine configLine)
    {
        if (m_animator != null && m_kWeapon != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(configLine.MPFireAni[0]);
                if (m_kWeaponLine.FireAni.Length != 0)
                    m_kAni.Play(m_kWeaponLine.FireAni[0]);
            }
            else
            {
                if (m_animator.GetBool("move"))
                    m_animator.SetLayerWeight(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX, 0f);
                m_animator.Play(configLine.OPFireAni[0]);
            }

            if (m_kEffectGunFire != null)
                m_kEffectGunFire.Play();
            AttachAndPlayCartridgeEffect(false);
            isAnotherHand = false;
            isStart = true;
        }
    }

    void FireBegin()
    {
        if (m_animator != null && m_kWeapon != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(m_kWeaponLine.MPFireBeginAni);
                if (m_kWeaponLine.FireBeginAni.Length != 0)
                {
                    if (m_kAni.GetClip(m_kWeaponLine.FireBeginAni[0]) != null)
                        m_kAni.Play(m_kWeaponLine.FireBeginAni[0]);
                    if (m_bIsBothHandSlot)
                    {
                        var index = m_kWeaponLine.FireBeginAni.Length == 1 ? 0 : 1;
                        if (m_kAni.GetClip(m_kWeaponLine.FireBeginAni[index]) != null)
                            m_kAni.Play(m_kWeaponLine.FireBeginAni[index]);
                    }
                }
            }
        }
    }

    void FireEnd()
    {
        if (m_animator != null && m_kWeapon != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(m_kWeaponLine.MPFireEndAni);
                if (m_kWeaponLine.FireEndAni.Length != 0)
                {
                    if (m_kAni.GetClip(m_kWeaponLine.FireEndAni[0]) != null)
                        m_kAni.Play(m_kWeaponLine.FireEndAni[0]);
                    if (m_bIsBothHandSlot)
                    {
                        var index = m_kWeaponLine.FireEndAni.Length == 1 ? 0 : 1;
                        if (m_kAni.GetClip(m_kWeaponLine.FireEndAni[index]) != null)
                            m_kAni.Play(m_kWeaponLine.FireEndAni[index]);
                    }
                }
            }
        }
    }


    float m_flastTime = 0;
    void Update()
    {
        ResourceManager.Update();
        if (Time.time - m_flastTime >= 1f)
        {
            m_flastTime = Time.time;
            if (m_bIsWalkWaggle && m_animator != null && m_kWeaponLine != null && m_animator.GetCurrentAnimatorStateInfo(GameConst.PLAYER_UPBODY_LAYER_INDEX).IsName(m_kWeaponLine.OPIdleAni))
            {
                m_animator.SetLayerWeight(GameConst.PLAYER_WALKWAGGLE_LAYER_INDEX, 0.5f);
            }
        }
    }

    bool isStart = false;
    bool isAnotherHand = false;
    float timer_txt = 0f;

    void LateUpdate()
    {
        if (isStart)
        {
            timer_txt += Time.deltaTime;
            if (timer_txt >= 0.3f)
            {
                Logger.Log("time: "+timer_txt);
                AttachAndPlayFireEffect(isAnotherHand);
                isStart = false;
                timer_txt = 0f;
            }
        }
    }

    void Reload(int index)
    {
        if (m_animator != null && m_kWeapon != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(m_kWeaponLine.MPReloadAni[index]);
                if (m_kWeaponLine.ReloadAni.Length > index)
                    m_kAni.Play(m_kWeaponLine.ReloadAni[index]);
            }
            else
            {
                m_animator.Play(m_kWeaponLine.OPReloadAni);
            }
        }
    }
    
    void Switch()
    {
        if (m_animator != null && m_kWeapon != null)
        {
            if (m_bIsMainPlayer)
            {
                m_animator.Play(m_kWeaponLine.MPSwitchAni);

                if (m_kWeaponLine.SwitchAni.Length  != 0 && !string.IsNullOrEmpty(m_kWeaponLine.SwitchAni[0]))
                    m_kAni.Play(m_kWeaponLine.SwitchAni[0]);

                if (m_kWeaponLine.SlotType == 2 && m_kWeaponLine.SwitchAni.Length > 1 && !string.IsNullOrEmpty(m_kWeaponLine.SwitchAni[1]))
                    m_kAni.Play(m_kWeaponLine.SwitchAni[1]);

            }
            else
            {
                m_animator.Play(m_kWeaponLine.OPSwitchAni);
            }
        }
    }

    void Walk(bool isWalk)
    {
        m_bIsWalk = isWalk;
        if (m_animator != null)
        {
            if (m_bIsMainPlayer)
            {
                if (m_bIsWalk)
                {
                    PlayCameraWalkAni();
                    StopCameraIdleAni();
                }
                else
                {
                    PlayCameraIdleAni();
                    StopCameraWalkAni();
                }
            }
            else
            {
                m_animator.SetBool("move", m_bIsWalk);
                if (isWalk)
                {
                    m_animator.SetFloat("dirForwardBack", 1.0f);
                }
                else
                {
                    m_animator.SetFloat("dirForwardBack", 0f);
                }
            }
        }
    }

    void Crouch(bool isCrouch)
    {
        m_bIsCrouch = isCrouch;
        if (m_animator != null && !m_bIsMainPlayer)
        {
            m_animator.SetBool("crouch", m_bIsCrouch);
            if (m_bIsCrouch)
            {
                m_animator.Play("crouch");
            }
            else if (!m_bIsCrouch && !m_bIsWalk)
            {
                m_animator.Play("stand");
            }
        }
    }

    void PlayAni(string ani)
    {
        if (m_animator != null && !m_bIsMainPlayer)
        {
            m_animator.Play(ani);
        }
    }


    virtual protected void LoadWeaponAni(FPSAnimation fpsAni)
    {
        if (m_kWeaponLine != null)
        {
            LoadWeaponAni(fpsAni, m_kWeaponLine.ReloadAni);
            LoadWeaponAni(fpsAni, m_kWeaponLine.SwitchAni);
            LoadWeaponAni(fpsAni, m_kWeaponLine.FireBeginAni);
            LoadWeaponAni(fpsAni, m_kWeaponLine.FireEndAni);
            if (m_kWeaponLine.FireAni != null)
            {
                foreach (string ani in m_kWeaponLine.FireAni)
                    LoadWeaponAni(fpsAni, ani);
            }
        }
    }

    private void LoadWeaponAni(FPSAnimation fpsAni, string aniName)
    {
        if (string.IsNullOrEmpty(aniName) == false && m_kAni.GetClip(aniName) == null)
        {
            string strPath = PathHelper.GetWeaponAni(aniName);
            ResourceManager.LoadAnimation(strPath, (kAniClip) =>
            {
                if (fpsAni != null && kAniClip != null)
                {
                    fpsAni.AddClip(kAniClip, aniName);
                    Walk(m_bIsWalk);
                }
            });
        }
    }

    private void LoadWeaponAni(FPSAnimation fpsAni, string[] arrName)
    {
        if (arrName == null)
            return;

        foreach (string str in arrName)
        {
            LoadWeaponAni(fpsAni, str);
        }
    }

    private void AddMainPlayerCameraWalkAni()
    {
        if (m_kWeaponLine != null)
        {
            string strCameraWalkAni = m_kWeaponLine.CameraWalkAni;
            if (string.IsNullOrEmpty(strCameraWalkAni) == false)
            {
                string strPath = PathHelper.GetCameraAni(strCameraWalkAni);
                if (m_kMainPlayerCameraAnimation.GetClip(strCameraWalkAni) == null)
                {
                    ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                    {
                        if (kAniClip != null && m_kMainPlayerCameraAnimation != null)
                        {
                            kAniClip.wrapMode = WrapMode.Loop;
                            m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraWalkAni);
                        }
                    });
                }
            }

            string strCameraIdleAni = m_kWeaponLine.CameraIdleAni;
            if (string.IsNullOrEmpty(strCameraIdleAni) == false)
            {
                string strPath = PathHelper.GetCameraAni(strCameraIdleAni);
                if (m_kMainPlayerCameraAnimation.GetClip(strPath) == null)
                {
                    ResourceManager.LoadAnimation(strPath, (kAniClip) =>
                    {
                        if (m_kMainPlayerCameraAnimation != null && kAniClip != null)
                        {
                            kAniClip.wrapMode = WrapMode.Loop;
                            m_kMainPlayerCameraAnimation.AddClip(kAniClip, strCameraIdleAni);
                        }
                    });
                }
            }
        }
    }

    public void PlayCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            if (m_kWeaponLine != null)
            {
                string strCameraAni = m_kWeaponLine.CameraWalkAni;
                AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
                if (state != null)
                {
                    state.speed = 1;
                    m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                    m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
                }
            }
        }
    }

    public void PlayCameraIdleAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            m_kMainPlayerCameraAnimation.enabled = false;
            m_kMainPlayerCameraAnimation.enabled = true;
            string strCameraAni = m_kWeaponLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
            {
                state.speed = 1;
                m_kMainPlayerCameraAnimation.CrossFade(strCameraAni, 0.3f);
                m_kMainPlayerCameraAnimation.clip = m_kMainPlayerCameraAnimation.GetClip(strCameraAni);
            }
        }
    }

    public void StopCameraIdleAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            string strCameraAni = m_kWeaponLine.CameraIdleAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    public void StopCameraWalkAni()
    {
        if (m_kMainPlayerCameraAnimation != null)
        {
            string strCameraAni = m_kWeaponLine.CameraWalkAni;
            AnimationState state = m_kMainPlayerCameraAnimation[strCameraAni];
            if (state != null)
                state.speed = 0;
        }
    }

    public void AttachFireEffect(bool isAnotherHand)
    {

        EffectGunFire effectGunFire = isAnotherHand ? m_kEffectGunFire2 : m_kEffectGunFire;

        if (effectGunFire != null)
            return;

        string fireEffect = "";
        if (m_bIsMainPlayer)
        {
            if (!isAnotherHand)
                fireEffect = m_kWeaponLine.FireEffect.Length > 0 ? m_kWeaponLine.FireEffect[0] : "";
            else
                fireEffect = m_kWeaponLine.FireEffect.Length > 0 ? (m_kWeaponLine.FireEffect.Length == 1 ? m_kWeaponLine.FireEffect[0] : m_kWeaponLine.FireEffect[1]) : "";
        }
        else
        {
            if (!isAnotherHand)
                fireEffect = m_kWeaponLine.FireEffect2.Length > 0 ? m_kWeaponLine.FireEffect2[0] : "";
            else
                fireEffect = m_kWeaponLine.FireEffect2.Length > 0 ? (m_kWeaponLine.FireEffect2.Length == 1 ? m_kWeaponLine.FireEffect2[0] : m_kWeaponLine.FireEffect2[1]) : "";
        }

        if (string.IsNullOrEmpty(fireEffect))
            return;

        EffectManager.singleton.GetEffect<EffectGunFire>(fireEffect, (kEffect) =>
        {
            effectGunFire = kEffect;
            kEffect.Attach(!isAnotherHand ? m_transFirePoint : m_transFirePoint);
            if (m_bIsMainPlayer)
            {
                kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            }
            else
            {
                kEffect.SetFadeOut(5f, 0.5f);
            }
        });
    }

    protected void AttachAndPlayCartridgeEffect(bool isAnotherHand)
    {
        string CartridgeEffect = !isAnotherHand ? m_kWeaponLine.CartridgeEffect : m_kWeaponLine.CartridgeEffect2;


        if (string.IsNullOrEmpty(CartridgeEffect))
            return;

        EffectManager.singleton.GetEffect<EffectTimeout>(CartridgeEffect, (kEffect) =>
        {
            kEffect.Attach(!isAnotherHand ? m_transCartridge : m_transCartridge2);
            kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            kEffect.SetTimeOut(-1);
            kEffect.Play();
        });
    }

    protected void AttachAndPlayFireEffect(bool isAnotherHand)
    {
        Transform firePoint = !isAnotherHand ? m_transFirePoint : m_transFirePoint2;
        if (firePoint == null)
            return;

        string strBulletEffect = m_bIsMainPlayer ? m_kWeaponLine.BulletEffect : m_kWeaponLine.BulletEffect2;

        if (string.IsNullOrEmpty(strBulletEffect))
            return;

        EffectManager.singleton.GetEffect<EffectTimeout>(strBulletEffect, (kEffect) =>
        {
            //float angle = VectorHelper.Angle360(m_transFirePoint.up, kEffect.transform.up);
            float angle = firePoint.up.Angle2(Vector3.up);
            //Debug.LogError("angle: "+angle+", up: " + m_transFirePoint.up+""+kEffect.transform.up);
            kEffect.Attach(firePoint.position, !isAnotherHand ?m_kWeapon.transform.forward : m_kWeaponLeft.transform.forward, Space.World, m_go.transform, 0f, angle);
            kEffect.SetLayer(GameSetting.LAYER_VALUE_MAIN_PLAYER);
            kEffect.SetTimeOut(-1);
            kEffect.Play();
        });
    }

    public static bool GetIsHadGunEffect(int weaponType)
    {
        switch (weaponType)
        {
            case WEAPON_TYPE_PISTOL:
            case WEAPON_TYPE_RIFLE:
            case WEAPON_TYPE_SUBMACHINE_GUN:
            case WEAPON_TYPE_MACHINE_GUN:
            case WEAPON_TYPE_SHOT_GUN:
            case WEAPON_TYPE_SNIPER_GUN:
                return true;
            case WEAPON_TYPE_KNIFE:
            case WEAPON_TYPE_GRENADE:
            case WEAPON_TYPE_BOM:
            case WEAPON_TYPE_PAW:
                return false;
        }

        return false;
    }
}
