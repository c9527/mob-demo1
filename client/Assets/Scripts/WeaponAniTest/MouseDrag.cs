﻿using UnityEngine;
using System.Collections;

public class MouseDrag : MonoBehaviour
{

    public int speed = 1000;
    bool m_bIsCanDrag;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            m_bIsCanDrag = true;
        if (Input.GetMouseButtonUp(0))
            m_bIsCanDrag = false;

        if (m_bIsCanDrag)
        {
            transform.Rotate(Vector3.down * Time.deltaTime * Input.GetAxis("Mouse X") * speed, Space.Self);
        }
    }

}
