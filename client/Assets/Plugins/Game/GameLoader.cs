﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
#if UNITY_IPHONE
using System.Runtime.InteropServices;
#endif

/// <summary>
/// 启动脚本1/3  
///
/// IDE模式下，加载与主程序集【程序集重名】的dll文件Client.dll.bytes，加载会失败但会返回原本无法拿到的主程序集对象，进而可以反射调用其上的脚本
/// (1/3)   文件：Assembly-CSharp-firstpass.dll    程序集：Assembly-CSharp-firstpass       脚本：GameLoader
/// (2/3)   文件：UpdateWorker.dll.bytes           程序集：Assembly-CSharp-UpdateWorker    脚本：UpdateWorker
/// (3/3)   文件：Client.dll.bytes                 程序集：Assembly-CSharp                 脚本：Driver
/// </summary>
class GameLoader : MonoBehaviour
{
#if ActiveSDK && UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern void OCappNSLogWorker (string msg);
#endif

    private void Awake()
    {
#if UNITY_EDITOR
        Application.LoadLevel("Driver");
#elif UNITY_ANDROID
        StartCoroutine(LoadLocalAssembly("UpdateWorker", "UpdateWorker"));
#elif UNITY_STANDALONE
        StartCoroutine(LoadLocalAssembly("UpdateWorker", "UpdateWorker"));
#elif UNITY_IPHONE
    #if ActiveSDK
        Application.RegisterLogCallback((condition, stackTrace, type) =>
            OCappNSLogWorker(string.Format("{0} - [{1}]: {2}\t[TRACE]: {3}", DateTime.Now.ToString("MM-dd HH:mm:ss"), type, condition.TrimEnd(), stackTrace)));
    #endif
        Application.LoadLevel("UpdateWorker");
#elif UNITY_WEBPLAYER
        Application.LoadLevel("UpdateWorker");
#endif
    }

    private IEnumerator LoadLocalAssembly(string assetmblyName, string mainClassName)
    {
        WWW www = null;
#if UNITY_ANDROID || UNITY_STANDALONE
        www = new WWW(AppRootPathWww + InfoDir + "GameDefine.txt");
        yield return www;
        while (!www.isDone && string.IsNullOrEmpty(www.error)) yield return null;
        var appVersion = "";
        if (string.IsNullOrEmpty(www.error))
        {
            var text = www.text;
            var dic = ReadSimpleConfig(text);
            dic.TryGetValue("Version", out appVersion);
            LogDebug("包内版本号:" + appVersion);
        }
        if (www.assetBundle != null)
            www.assetBundle.Unload(true);
        www.Dispose();

        var sdAppVersion = "";
        if (File.Exists(SdRootPath + InfoDir + "AppVersion.txt"))
        {
            var text = File.ReadAllText(SdRootPath + InfoDir + "AppVersion.txt");
            var dic = ReadSimpleConfig(text);
            dic.TryGetValue("Version", out sdAppVersion);
            LogDebug("资源版本版本号:" + sdAppVersion);
        }

        if ((string.IsNullOrEmpty(sdAppVersion) || sdAppVersion != appVersion))
            ClearSdDll(assetmblyName);
#endif

        LogDebug("加载本地Assembly：", GetResFullPathWww("Managed/" + assetmblyName + ".dll.bytes"));
        www = new WWW(GetResFullPathWww("Managed/" + assetmblyName + ".dll.bytes"));
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            var driver = Assembly.Load(www.bytes).GetType(mainClassName, true);
            gameObject.AddComponent(driver);
            LogDebug("本地Assembly启动完毕");
        }
        else
            LogError("本地Assembly加载失败", www.url, www.error);
        if (www.assetBundle != null)
            www.assetBundle.Unload(false);
        www.Dispose();
    }

    private static Dictionary<string, string> ReadSimpleConfig(string text)
    {
        var dic = new Dictionary<string, string>();
        var lines = text.Split(new char[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            var kv = lines[i].Split(new char[] {'='});
            if (kv.Length > 0)
                dic[kv[0]] = kv.Length > 1 ? kv[1] : "";
        }
        return dic;
    }

    #region 日志
    private void LogDebug(string main, string debug = "")
    {
        Debug.Log(DateTime.Now.ToString("HH:mm:ss ") + "【GameLoader】" + main + " " + debug + "\n");
    }

    private void LogError(string main, string debug, string errorRaw = "")
    {
        Debug.LogError(DateTime.Now.ToString("HH:mm:ss ") + "【GameLoader】" + main + " " + debug + "\n" + errorRaw);
    }
    #endregion

    #region 各种目录
    private string AppRootPathWww { get { return GetStreamingAssetsPath(); } }
    private string SdRootPath { get { return Application.persistentDataPath + "/"; } }
    private string SdRootPathWww { get { return "file:///" + Application.persistentDataPath + "/"; } }

    private string ResDir { get { return "Resources/"; } }
    private string InfoDir { get { return "VersionInfo/"; } }

    private string GetResFullPathWww(string path)
    {
        if (File.Exists(SdRootPath + ResDir + path))
            return SdRootPathWww + ResDir + path;
        else
            return AppRootPathWww + path;
    }

    private string GetStreamingAssetsPath()
    {
        string s = null;
#if UNITY_EDITOR
        s = "file://" + Application.dataPath + "/StreamingAssets/";
#elif UNITY_IPHONE
        s = "file://" + Application.dataPath + "/Raw/";          
#elif UNITY_ANDROID
        s="jar:file://" + Application.dataPath + "!/assets/";
#elif UNITY_STANDALONE
        s = "file://" + Application.dataPath + "/StreamingAssets/";
#elif UNITY_WEBPLAYER
        s = Application.dataPath + "/StreamingAssets/";
#endif
        return s;
    }

    private void ClearSdDll(string assetmblyName)
    {
        var path = "Managed/" + assetmblyName + ".dll.bytes";
        if (File.Exists(SdRootPath + ResDir + path))
        {
            LogDebug("清除sd卡上的" + assetmblyName);
            File.Delete(SdRootPath + ResDir + path);
        }
    }
    #endregion
}