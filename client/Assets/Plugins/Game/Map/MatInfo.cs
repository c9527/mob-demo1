﻿using UnityEngine;
using System.Collections;

public enum EMat
{
    None = 0,
    Metal = 1,
    Wood = 2,
    Cement = 3,
    Rock = 4,
    Glass = 5,
    Soil = 6,
    Head = 7,
    Body =8,
    Limbs = 9,
    Shield = 10
}

public enum ESceneItemType
{
    None = 0,
    DMM = 1
}

public class MatInfo : MonoBehaviour
{
    public EMat matValue = EMat.None;
    public ESceneItemType itemType = ESceneItemType.None; 
}
