﻿using UnityEngine;

public class AIZone : MonoBehaviour
{
    static Color msColor = new Color(1, 0, 0, 0.4f);
    static Quaternion msCylinderQua = Quaternion.Euler(new Vector3(90, 0, 0));

    public enum Shape
    {
        Cube,
        Cylinder,
    }

    public Shape _zoneShape = Shape.Cube;
    public bool _lookAt = false;

    public Transform[] _targetPos = new Transform[0];

#if UNITY_EDITOR
    void DrawShape()
    {
        switch (_zoneShape)
        {
            case Shape.Cube:
                Gizmos.color = msColor;
                Gizmos.DrawCube(transform.position, transform.lossyScale);
                break;
            case Shape.Cylinder:
                Vector3 scale = transform.localScale;
                float s = Mathf.Max(Mathf.Abs(scale.x), Mathf.Abs(scale.z));
                UnityEditor.Handles.color = msColor;
                UnityEditor.Handles.matrix = Matrix4x4.TRS(transform.position, msCylinderQua, new Vector3(s, s, scale.y));
                UnityEditor.Handles.CylinderCap(0, Vector3.zero, Quaternion.identity, 1);
                UnityEditor.Handles.matrix = Matrix4x4.identity;
                break;
            default:
                break;
        }
    }

    void OnDrawGizmos()
    {
        DrawShape();
    }

    void OnDrawGizmosSelected()
    {
        DrawShape();

        Gizmos.color = Color.green;
        foreach (Transform target in _targetPos)
        {
            if (target == null)
                continue;
            Gizmos.DrawLine(transform.position, target.position);
            Gizmos.DrawSphere(target.position, 0.5f);
        }

        if (_lookAt)
        {
            Gizmos.color = Color.blue;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.Euler(0, transform.eulerAngles.y, 0), Vector3.one);
            Gizmos.DrawFrustum(Vector3.zero, 50, 10, 0, 1.5f);
        }
    }

    [UnityEditor.MenuItem("GameObject/3D Object/AIZone", false, 0)]
    public static void CreateAIZone()
    {
        GameObject active = UnityEditor.Selection.activeGameObject;
        if (active == null)
            return;
        GameObject go = new GameObject("AIZone");
        go.AddComponent<AIZone>();
        go.transform.SetParent(active.transform, false);
    }
#endif

    public int IndexID { get; set; }
}
