﻿using System.Collections.Generic;

public static class Global
{
    public static string currentVersion = "";   //游戏版本
    public static string appVersion = "";       //包版本
    public static bool useDebugServerlistUrl;    //测试用服务器列表地址
    public static string gameDefineServerlistUrl = "";    //正式服务器列表地址
    public static readonly Dictionary<string, object> Dic = new Dictionary<string, object>(); //临时传递参数用
}