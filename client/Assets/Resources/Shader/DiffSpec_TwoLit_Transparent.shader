Shader "FPS/DiffSpec_TwoLit_Transparent" {
	// 强化里显示的武器用到
	
	Properties
	{
		_MainTex("MainTex", 2D) = "white"{}		
		
		_LightColor("平行光颜色", Color) = (1, 1, 1, 1)
		_LightDir_X("平行光方向X", Range(-1.0, 1.0)) = -1.0
		_LightDir_Y("平行光方向Y", Range(-1.0, 1.0)) = 0
		_LightDir_Z("平行光方向Z", Range(-1.0, 1.0)) = 0	

		_LightColor2("平行光颜色", Color) = (1, 1, 1, 1)
		_LightDir2_X("平行光方向X", Range(-1.0, 1.0)) = 1.0
		_LightDir2_Y("平行光方向Y", Range(-1.0, 1.0)) = 0
		_LightDir2_Z("平行光方向Z", Range(-1.0, 1.0)) = 0	

		_AmbientBrightness("环境光亮度", Range(0, 2.0)) = 1			
		_DiffBrightness("漫反射亮度", Range(0, 2.0)) = 1
		_SpecularBrightness("高光亮度", Range(0, 2.0)) = 1
		_SpecularPower("高光指数", Float) = 1					// 值越大，高光区域范围越小，亮度越高
	}

	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Back
		LOD 200

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float4 _MainTex_ST;

				fixed4 _LightColor;
				half _LightDir_X;
				half _LightDir_Y;
				half _LightDir_Z;

				fixed4 _LightColor2;
				half _LightDir2_X;
				half _LightDir2_Y;
				half _LightDir2_Z;

				fixed _AmbientBrightness;
				fixed _DiffBrightness;
		 		fixed _SpecularBrightness;			 
				half _SpecularPower;

				struct V2F
				{
					float4 pos:POSITION;
					half2 uv:TEXCOORD0;							
					float3 normal:TEXCOORD1;

					float3 halfVector:TEXCOORD2;	
					fixed3 lightDir:TEXCOORD3;

					float3 halfVector2:TEXCOORD4;
					fixed3 lightDir2:TEXCOORD5;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = TRANSFORM_TEX(IN.texcoord, _MainTex);					
					o.normal = IN.normal;
					 
					
					float3 lightDir = float3(_LightDir_X, _LightDir_Y, _LightDir_Z) * -1;
					lightDir = normalize(mul((float3x3)_World2Object, lightDir).xyz);

					float3 lightDir2 = float3(_LightDir2_X, _LightDir2_Y, _LightDir2_Z) * -1;
					lightDir2 = normalize(mul((float3x3)_World2Object, lightDir2).xyz);

					o.lightDir = lightDir;
					o.lightDir2 = lightDir2;

					o.halfVector = normalize(normalize(ObjSpaceViewDir(IN.vertex)) + lightDir);		
					o.halfVector2 = normalize(normalize(ObjSpaceViewDir(IN.vertex)) + lightDir2);					
					 
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);	

					fixed3 normal = normalize(IN.normal);

					float dotNL = saturate(dot(normal, IN.lightDir));
					fixed3 diff = dotNL * _DiffBrightness;		

					float dotNL2 = saturate(dot(normal, IN.lightDir2));
					fixed3 diff2 = dotNL2 * _DiffBrightness;		 
					
					float dotVR = saturate(dot(normalize(IN.normal), IN.halfVector));					 
					fixed3 spec = color.rgb * _LightColor.rgb * pow(dotVR, _SpecularPower) * _SpecularBrightness;

					float dotVR2 = saturate(dot(normalize(IN.normal), IN.halfVector2));					 
					fixed3 spec2 = color.rgb * _LightColor.rgb * pow(dotVR2, _SpecularPower) * _SpecularBrightness;

					//color.rgb = color.rgb * _AmbientBrightness + color.rgb * (_LightColor.rgb * (diff + spec) + _LightColor2.rgb * (diff2 + spec2) );
					color.rgb = color.rgb * _AmbientBrightness + color.rgb * (_LightColor.rgb * diff + _LightColor2.rgb * diff2) + _LightColor.rgb * spec + _LightColor2.rgb * spec2;				 
					
					return color;
				}

			ENDCG
		}
	}

	FallBack "Unlit/Transparent"
}
