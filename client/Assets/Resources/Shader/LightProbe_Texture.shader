Shader "FPS/LightProbe_Texture" {


	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader {
		Tags { "Queue"="Geometry" "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 200

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase
				//#pragma multi_compile LIGHTPROBE_OFF LIGHTPROBE_ON 
				
				sampler2D _MainTex;

				struct V2F
				{
					float4 pos:POSITION;
					half2 uv:TEXCOORD0;
					float3 normal:TEXCOORD1;
					 
				};
					
				float MyNormalize(float3 normal)
				{
					float rLength = rsqrt(dot(normal, normal));
					
					normal.x *= rLength;
					normal.y *= rLength;
					normal.z *= rLength;	

					return normal;
				}
					
				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					o.normal = mul((float3x3)_Object2World, IN.normal);						 						 								 
					 
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);

					IN.normal = MyNormalize(IN.normal);					

					//#ifdef LIGHTPROBE_ON
						fixed3 lpColor = ShadeSH9(float4(IN.normal, 1));	
						color.rgb = color.rgb * lpColor.rgb;
					//#endif

					return color;
				}

			ENDCG

		}		
		
	} 

	FallBack "Unlit/Texture"
}
