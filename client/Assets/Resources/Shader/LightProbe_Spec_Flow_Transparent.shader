Shader "FPS/LightProbe_Spec_Flow_Transparent" {

	// LightProbe & Specular For Weapon In Battle

	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}		
		_BaseBrightness("BaseBrightness", float) = 1.2
		_SpecBrightness("SpecBrightness(0-1)", float) = 1
		_SpecPow("SpecPow", float) = 20		

		_FlowTex("UVTex", 2D) = "black"{}
		_FlowColor("FlowColor", Color) = (1, 1, 1, 1)
		_FlowDir_X("UVAniDir_X", Range(-1, 1)) = 1
		_FlowDir_Y("UVAniDir_Y", Range(-1, 1)) = 1
		_FlowSpeed("UVAniSpeed", float) = 5
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "LightMode"="ForwardBase"}
		LOD 200
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase
				#pragma multi_compile LIGHTPROBE_OFF LIGHTPROBE_ON
				
				sampler2D _MainTex;
				
				float _BaseBrightness;
				float _SpecBrightness;
				float _SpecPow;			

				sampler2D _FlowTex;
				fixed4 _FlowColor;
				half _FlowDir_X;
				half _FlowDir_Y;
				half _FlowSpeed;


				struct V2F
				{
					float4 pos:POSITION;
					half4 uv:TEXCOORD0;					
					float3 normal:TEXCOORD1;
					float3 halfVector:TEXCOORD2;	
				};

				float3 MyNormalize(float3 normal)
				{
					float3 rLength = rsqrt(dot(normal, normal));
					
					normal.x *= rLength;
					normal.y *= rLength;
					normal.z *= rLength;	

					return normal;
				}

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv.xy = IN.texcoord;					
					o.normal = mul((float3x3)_Object2World, IN.normal);

					half2 uvDir = normalize(half2(_FlowDir_X, -_FlowDir_Y));
					o.uv.zw = IN.texcoord + uvDir * _Time.x * _FlowSpeed;
					
					//o.halfVector = normalize( normalize(WorldSpaceLightDir(IN.vertex)) + normalize(WorldSpaceViewDir(IN.vertex)) );

					float3 worldPos = mul(_Object2World, IN.vertex).xyz;
					float3 lightDir = normalize(_WorldSpaceLightPos0.xyz - worldPos.xyz);					

					o.halfVector = normalize(normalize(WorldSpaceViewDir(IN.vertex)) + lightDir);					 								 
					 
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv.xy);					
					fixed3 colorUV = tex2D(_FlowTex, IN.uv.zw).rgb;

					fixed flag = max( max(_FlowColor.r, _FlowColor.g), _FlowColor.b);
					flag = step(flag, 0.24);

					colorUV = (1 - colorUV + _FlowColor.rgb) * flag + colorUV * _FlowColor.rgb * (1 - flag);
					colorUV = saturate(colorUV);					
					color.rgb = color.rgb * colorUV * flag + (color.rgb + colorUV) * (1 - flag);	

					float3 worldN = MyNormalize(IN.normal);
					fixed3 specColor = fixed3(1, 1, 1) * pow(saturate(dot(IN.halfVector, worldN)), _SpecPow);

					#ifdef LIGHTPROBE_ON
						fixed3 lpColor = ShadeSH9(float4(worldN, 1));		//曝光问题
						color.rgb = color.rgb * lpColor.rgb * _BaseBrightness + specColor * _SpecBrightness * lpColor.rgb ;	
					#endif

					#ifdef LIGHTPROBE_OFF
						color.rgb = color.rgb * _BaseBrightness + specColor * _SpecBrightness;	
					#endif										
					
					return color;
				}

			ENDCG

		}		
		
	} 

	FallBack "Unlit/Texture"
}
