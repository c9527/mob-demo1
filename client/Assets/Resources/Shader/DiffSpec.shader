Shader "FPS/DiffSpec" {
	// 大厅里角色展示
	// 高光使用非金属效果
	
	Properties
	{
		_MainTex("MainTex", 2D) = "white"{}		
		
		_LightColor("平行光颜色", Color) = (1, 1, 1, 1)
		_LightDir_X("平行光方向X", Range(-1.0, 1.0)) = 1.0
		_LightDir_Y("平行光方向Y", Range(-1.0, 1.0)) = -1.0
		_LightDir_Z("平行光方向Z", Range(-1.0, 1.0)) = -1.0	

		_AmbientBrightness("环境光亮度", Range(0, 2.0)) = 1			
		_DiffBrightness("漫反射亮度", Range(0, 2.0)) = 1
		_SpecularBrightness("高光亮度", float) = 1
		_SpecularPower("高光指数", Float) = 1	// 值越大，高光区域范围越小，亮度越高
	}

	SubShader
	{
		Tags { "Queue" = "Geometry" "RenderType" = "Opaque" }

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _LightColor;
				half _LightDir_X;
				half _LightDir_Y;
				half _LightDir_Z;
				fixed _AmbientBrightness;
				fixed _DiffBrightness;
		 		fixed _SpecularBrightness;			 
				half _SpecularPower;

				struct V2F
				{
					float4 pos:POSITION;
					half2 uv:TEXCOORD0;							
					float3 normal:TEXCOORD1;				
					float3 halfVector:TEXCOORD2;	
					fixed3 lightDir:TEXCOORD3;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = TRANSFORM_TEX(IN.texcoord, _MainTex);					
					o.normal = IN.normal;
					 
					
					float3 lightDir = float3(_LightDir_X, _LightDir_Y, _LightDir_Z) * -1;
					lightDir = normalize(mul((float3x3)_World2Object, lightDir).xyz);

					o.lightDir = lightDir;

					o.halfVector = normalize(normalize(ObjSpaceViewDir(IN.vertex)) + lightDir);					
					 
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);	

					fixed3 normal = normalize(IN.normal);

					float dotNL = saturate(dot(normal, IN.lightDir));
					fixed3 diff = dotNL * _DiffBrightness;		
					
					float dotVR = saturate(dot(normalize(IN.normal), IN.halfVector));					 
					fixed3 spec = color.rgb * _LightColor.rgb * pow(dotVR, _SpecularPower) * _SpecularBrightness;

					color.rgb = color.rgb * _AmbientBrightness + color.rgb * _LightColor.rgb * (diff + spec);				
					//color.rgb = color.rgb * _AmbientBrightness + color.rgb * _LightColor.rgb * diff + _LightColor.rgb * spec;	
					
					return color;
				}

			ENDCG
		}
	}
}
