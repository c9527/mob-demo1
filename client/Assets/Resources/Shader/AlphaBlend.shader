﻿Shader "FPS/AlphaBlend"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader
	{
		Tags{"Queue" = "Transparent" "RenderType" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		ZWrite Off
		Fog{Mode Off}

		Pass
		{
			CGPROGRAM
				
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float4 _MainTex_ST;

				struct appdata_t 
				{
					float4 vertex : POSITION;
					fixed4 color : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct V2F
				{
					float4 pos:SV_POSITION;
					half2 uv:TEXCOORD0;
					fixed4 color:COLOR;
				};

				V2F vert(appdata_t IN)
				{
					V2F o;

					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = TRANSFORM_TEX(IN.texcoord, _MainTex);
					o.color = IN.color;

					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);
					return color * IN.color;
				}

			ENDCG
		}
	}
}