Shader "UI/RichText" 
{
	Properties
	{	
		_MainTex ("Base", 2D) = "black" {}
		_Emoticons ("Emoticons", 2D) = "black" {}
		_StencilComp ("Stencil Comparison", float) = 80
		_Stencil ("Stencil ID", float) = 0
		_StencilOp ("Stencil Operation", float) = 0
		_StencilWriteMask ("Stencil Write Mask", float) = 255
		_StencilReadMask("Stencil Read Mask", float) = 255
		_ColorMask ("Color Mask", float) = 15
	}	
		
	// ---- Fragment program cards
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True" "PreviewType"="Plane"} 
		Cull Off  
		ZWrite Off 
		ZTest [unity_GUIZTestMode]
		Fog { Mode Off } 
		Stencil {
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOp]
		}
		Blend SrcAlpha OneMinusSrcAlpha		
		ColorMask [_ColorMask]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			sampler2D _Emoticons;
			float4 _MaskArea;

			half4 _MainTex_ST;

			void vert (
				float4 vertex : POSITION, 
				float4 color : COLOR, 
				half2 texcoord : TEXCOORD0, 
				half2 emo_texcoord : TEXCOORD1,
				out float4 o_vertex : POSITION, 
				out float4 o_color : COLOR, 
				out half2 o_texcoord : TEXCOORD0,
				out half2 o_emo_texcoord : TEXCOORD1,
				out float4 o_frag_pos : TEXCOORD2
			){
				o_vertex = mul(UNITY_MATRIX_MVP, vertex);
				o_frag_pos = o_vertex;
				o_texcoord = TRANSFORM_TEX(texcoord, _MainTex);
				o_emo_texcoord = emo_texcoord;
				o_color = color;
			}

			fixed4 frag (float4 o_frag_pos: TEXCOORD2, float4 color : COLOR, half2 texcoord : TEXCOORD0, half2 emo_texcoord : TEXCOORD1) : COLOR
			{
				fixed4 colorOut;
				fixed4 texColor;

				if (emo_texcoord.x > 0.0 && emo_texcoord.y > 0.0)
				{
					texColor = tex2D(_Emoticons, emo_texcoord);
					colorOut = texColor;
				}
				else if (texcoord.x > 0.0 && texcoord.y > 0.0)
				{
					texColor = tex2D(_MainTex, texcoord);
					colorOut = texColor.a * color;
				}
				else
				{
					colorOut = color;
				}

				return colorOut;
			}
			ENDCG
		}
	} 

	Fallback Off
}
