Shader "FPS/Transparent_AlphaSet_OneSide" {

	Properties
	{
		_MainTex("MainTex", 2D) = "white"{} 
		_Alpha("Alpha", Range(0.0, 1.0)) = 0.8 
	}

	SubShader
	{
		Tags{"Queue" = "Transparent" "RenderType" = "Transparent"}	
		
		Cull Back			
		LOD 200	

		Pass
		{
			ZWrite On
			ColorMask 0			

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct V2F 
				{
					float4 pos:SV_POSITION;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					return fixed4(0, 0, 0, 1);
				}
			ENDCG
		}

		Pass
		{
			Blend SrcAlpha OneMinusSrcALpha
			ColorMask RGB
			ZWrite Off			
			ZTest LEqual		 

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float _Alpha;
				 
				struct V2F
				{
					float4 pos:SV_POSITION;
					float2 uv:TEXCOORD0;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					return o;
				}

				fixed4 frag(V2F IN) : COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);
					color.a = _Alpha;

					return color;
				}

			ENDCG
			

		}
	}

	FallBack "Unlit/Texture"
}
