Shader "FPS/Texture_Lightmap"
{
	Properties
	{
		_FuckKC("Check", INT) = 1
		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader
	{
		Tags{"Queue"="Geometry" "RenderType"="Opaque"}
		LOD 200
		Cull Back
		ZTest Less
		ZWrite On

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON 


				sampler2D _MainTex;
				sampler2D unity_Lightmap;
				float4 unity_LightmapST;
				int _FuckKC;

				struct appdata_t
				{
					float4 vertex:POSITION;
					float2 uv:TEXCOORD0;
					//#ifdef LIGHTMAP_ON
					float2 uvLM:TEXCOORD1;
					//#endif
				};

				struct V2F
				{
					float4 pos:POSITION;
					float2 uv:TEXCOORD0;
					//#ifdef LIGHTMAP_ON
					float2 uvLM:TEXCOORD1;
					//#endif
				};

				V2F vert(appdata_t IN)
				{
					V2F o;

					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.uv;
					//#ifdef LIGHTMAP_ON
					o.uvLM = IN.uvLM * unity_LightmapST.xy + unity_LightmapST.zw;
					//#endif

					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);

					//#ifdef LIGHTMAP_ON
					fixed3 lmColor = DecodeLightmap(tex2D(unity_Lightmap, IN.uvLM));
					color.rgb = color.rgb * lmColor * _FuckKC + color.rgb * (1 - _FuckKC);
					//#endif

					return color;
				}
				
			ENDCG
		}
	}	 
}