Shader "FPS/Texture_Color" 
{
	// 第三人称，无敌时用到，闪红

	Properties{
		_Color("_Color", Color) = (1, 1, 1, 1)
		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader{
		Tags{ "Queue" = "Geometry" "RenderType" = "Opaque" }
		Cull Back

		Pass{

			ZWrite On

			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float4 _Color;

				struct V2F{
					float4 pos:POSITION;
					float2 uv:TEXCOORD0;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);
					color.rgb *= _Color.rgb;
					return color;
				}

			ENDCG

		}

	}


}
