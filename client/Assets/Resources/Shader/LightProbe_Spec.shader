Shader "FPS/LightProbe_Spec" {

	// LightProbe & Specular For Weapon In Battle

	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BaseBrightness("BaseBrightness", float) = 1.2
		_SpecBrightness("SpecBrightness(0-1)", float) = 1
		_SpecPow("SpecPow", float) = 20		
	}

	SubShader {
		Tags { "Queue"="Geometry" "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 200
		Cull Off

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase
				#pragma multi_compile LIGHTPROBE_OFF LIGHTPROBE_ON
				
				sampler2D _MainTex;
				float _BaseBrightness;
				float _SpecBrightness;
				float _SpecPow;				

				struct V2F
				{
					float4 pos:POSITION;
					half2 uv:TEXCOORD0;
					float3 halfVector:TEXCOORD1;
					float3 n:TEXCOORD2;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					o.n= normalize(mul((float3x3)_Object2World, IN.normal));
					
					//o.halfVector = normalize( normalize(WorldSpaceLightDir(IN.vertex)) + normalize(WorldSpaceViewDir(IN.vertex)) );

					float3 worldPos = mul(_Object2World, IN.vertex).xyz;
					float3 lightDir = normalize(_WorldSpaceLightPos0.xyz - worldPos.xyz);					

					o.halfVector = normalize(normalize(WorldSpaceViewDir(IN.vertex)) + lightDir);					 								 
					 
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);
					float3 worldN=IN.n;																																				
					fixed3 specColor = fixed3(1, 1, 1) * pow(saturate(dot(IN.halfVector, worldN)), _SpecPow);

					#ifdef LIGHTPROBE_ON
						fixed3 lpColor = ShadeSH9(float4(worldN, 1));		//曝光问题
						color.rgb = color.rgb * lpColor.rgb * _BaseBrightness + specColor * _SpecBrightness * lpColor.rgb ;	
					#endif

					#ifdef LIGHTPROBE_OFF
						color.rgb = color.rgb * _BaseBrightness + specColor * _SpecBrightness;	
					#endif							
					
					return color;
				}

			ENDCG

		}		
		
	} 

	FallBack "Unlit/Texture"
}
