﻿Shader "FPS/SkyBox"{
	
	Properties{

		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader{

		Tags{"Queue" = "Background" "RenderType" = "Background"}
		LOD 200
		ZWrite Off
		Fog { Mode Off }
		//Cull Off
		
		Pass{
			
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				
				struct appdata_t{
					float4 vertex:POSITION;
					half2 uv:TEXCOORD0;
				};

				struct V2F{
					float4 pos:SV_POSITION;
					half2 uv:TEXCOORD0;
				};

				V2F vert(appdata_t IN){

					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.uv;

					return o;
				}

				fixed4 frag(V2F IN):COLOR{
					fixed4 color = tex2D(_MainTex, IN.uv);
					return color;
				}

			ENDCG
		}
		
	}
}