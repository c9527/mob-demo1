Shader "FPS/Texture_Brightness" 
{
	Properties{
		_MainTex("MainTex", 2D) = "white"{}
		_Brightness("Brightness", Float) = 1
	}

	SubShader{
		Tags{ "Queue" = "Geometry" "RenderType" = "Opaque" }
		Cull Back

		Pass{

			ZWrite On

			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;
				float _Brightness;

				struct V2F{
					float4 pos:POSITION;
					float2 uv:TEXCOORD0;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv) * _Brightness;					
					return color;
				}

			ENDCG

		}

	}


}
