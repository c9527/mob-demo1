Shader "FPS/PlayerHeadFlag" {
	// 友方头顶上的标识

	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags{"Queue" = "Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		ZTest Always

		Pass
		{
			CGPROGRAM

				#include "UnityCG.CGINC"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;

				struct V2F
				{
					float4 pos:SV_POSITION;
					float2 uv:TEXCOORD0;
				};

				V2F vert(appdata_base IN)
				{
					V2F o;
					o.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
					o.uv = IN.texcoord;
					return o;
				}

			    fixed4 frag(V2F IN):COLOR
			    {
			    	fixed4 color = tex2D(_MainTex, IN.uv);
			    	return color;
			    }		 

				
			ENDCG
		}
	}
}
