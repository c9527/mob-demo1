﻿Shader "FPS/Texture"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white"{}
	}

	SubShader
	{
		Tags{"Queue"="Geometry" "RenderType"="Opaque"}
		LOD 200
		Cull Back
		ZWrite On
		ZTest Less

		Pass
		{
			CGPROGRAM
				
				#include "UnityCG.cginc"
				#pragma vertex vert
				#pragma fragment frag

				sampler2D _MainTex;

				struct V2F
				{
					float4 pos:POSITION;
					float2 uv:TEXCOORD0;
				};

				V2F vert(V2F IN)
				{
					V2F o;

					o.pos = mul(UNITY_MATRIX_MVP, IN.pos);
					o.uv = IN.uv;

					return o;
				}

				fixed4 frag(V2F IN):COLOR
				{
					fixed4 color = tex2D(_MainTex, IN.uv);

					return color;
				}

			ENDCG
		}
	}
}